﻿using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace PosiflexTabletScannerWinService
{
    public class WebsocketCommunication
    {
        private HubConnection connection;
        public ILogger<Worker> _logger;

        private static string URL_ENDPOINT = null;
        private static string WEBSOCKET_SEND_METHOD = "SendScannedEan";

        public WebsocketCommunication(ILogger<Worker> logger, string urlEndpoint, string websocketSendMethod)
        {
            _logger = logger;
            URL_ENDPOINT = urlEndpoint;

            if (string.IsNullOrEmpty(urlEndpoint))
                throw new ArgumentNullException("Url endpoint is null");

            if (!string.IsNullOrEmpty(websocketSendMethod))
                WEBSOCKET_SEND_METHOD = websocketSendMethod;

            _logger.LogInformation($"-- opening socket on url {URL_ENDPOINT}");

            // create connection
            connection = new HubConnectionBuilder()
                    .WithUrl(URL_ENDPOINT, httpConnectionOptions =>
                    {
                        httpConnectionOptions.Headers.Add("user-agent", "posiflexTablet");
                    })
                    .Build();

            connection.Closed += async (error) =>
            {
                _logger.LogInformation($"-- socket connection closed");
                _logger.LogInformation($"-- socket is restarting...");
                Program.EVENT_LOG.WriteEntry("WebSocket closed", System.Diagnostics.EventLogEntryType.Information);

                await _Reconnect();
            };

            _Reconnect().Wait();
        }

        public async void Send(string data)
        {
            await connection.InvokeAsync(WEBSOCKET_SEND_METHOD, data);
        }

        private async Task _Reconnect()
        {
            HubConnectionState state;
            do
            {
                Task.Delay(1000).Wait();

                _logger.LogInformation("-- trying reconnect");

                state = _Connect();
            } while (state != HubConnectionState.Connected);

            await Task.CompletedTask;
        }

        private HubConnectionState _Connect()
        {
            try
            {
                connection.StartAsync().Wait();
                _logger.LogInformation("-- connection started");
                Program.EVENT_LOG.WriteEntry($"WebSocket connection succeed url={URL_ENDPOINT}", System.Diagnostics.EventLogEntryType.Information);
                return connection.State;
            }
            catch (AggregateException e)
            {
                _logger.LogInformation("-- connection failed. " + e.Message);
                Program.EVENT_LOG.WriteEntry($"WebSocket failed url={URL_ENDPOINT}, message={e.Message}", System.Diagnostics.EventLogEntryType.Error);
                return connection.State;
            }
        }
    }
}

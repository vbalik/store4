using System;
using System.IO.Ports;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace PosiflexTabletScannerWinService
{
    public class Worker : BackgroundService
    {
        public IConfiguration _configuration;
        public ILogger<Worker> _logger;
        public WebsocketCommunication WEBSOCKET;
        private static SerialPort SERIAL_PORT;
        private static string PORT_NAME = null; //"COM5";
        private static string METHOD_NAME = null;
        private static string URL_ENDPOINT = null; // = "http://192.168.0.5:5000/posiflex-tablet-hub";
        public static readonly object SerialPortHeartBeatCheckerLock = new object();

        public Worker(ILogger<Worker> logger, IConfiguration configuration)
        {
            _configuration = configuration;
            _logger = logger;

            var port = _configuration.GetValue<string>("port");
            if (!string.IsNullOrEmpty(port)) PORT_NAME = port;

            METHOD_NAME = _configuration.GetValue<string>("method");
            URL_ENDPOINT = _configuration.GetValue<string>("url");
        }

        public override Task StartAsync(CancellationToken cancellationToken)
        {
            if (string.IsNullOrEmpty(URL_ENDPOINT))
            {
                _logger.LogError("WebSocket url is not set (--url)");
                Program.EVENT_LOG.WriteEntry("WebSocket url is not set (--url)", System.Diagnostics.EventLogEntryType.Error);
            }

            // create websocket connection
            WEBSOCKET = new WebsocketCommunication(_logger, URL_ENDPOINT, METHOD_NAME);

            // create serial port connection
            _logger.LogInformation($"-- open serial port {PORT_NAME}");

            SERIAL_PORT = SerialPortFactory.Create(PORT_NAME, WEBSOCKET);
            SERIAL_PORT.Open();

            _logger.LogInformation($"-- serial port {PORT_NAME} opened");
            Program.EVENT_LOG.WriteEntry($"Service started with url={URL_ENDPOINT}, port={PORT_NAME}", System.Diagnostics.EventLogEntryType.Information);

            Task.Run(() => SerialPortHeartBeatChecker(_logger, WEBSOCKET));

            _logger.LogInformation($"Worker started at: {DateTime.Now}");
            return base.StartAsync(cancellationToken);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {

        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation($"Worker stopped at: {DateTime.Now}");
            return base.StopAsync(cancellationToken);
        }

        public static void SerialPortHeartBeatChecker(ILogger<Worker> _logger, WebsocketCommunication webSocket)
        {
            for (; ; )
            {
                Thread.Sleep(1000);
                lock (SerialPortHeartBeatCheckerLock)
                {
                    try
                    {
                        _logger.LogInformation(SERIAL_PORT.IsOpen.ToString());
                        if (!SERIAL_PORT.IsOpen)
                        {
                            SERIAL_PORT.Close();
                            Thread.Sleep(100);

                            SERIAL_PORT = SerialPortFactory.Create(PORT_NAME, webSocket);
                            SERIAL_PORT.Open();

                            _logger.LogInformation($"-- serial port {PORT_NAME} re-opened");
                            Program.EVENT_LOG.WriteEntry($"Serial port reopened and started with url={URL_ENDPOINT}, port={PORT_NAME}", System.Diagnostics.EventLogEntryType.Information);
                        }
                    }
                    catch (Exception e)
                    {
                        _logger.LogError(e.Message);
                        Program.EVENT_LOG.WriteEntry($"Serial port error {e.Message}", System.Diagnostics.EventLogEntryType.Error);
                    }
                }
            }
        }
    }
}

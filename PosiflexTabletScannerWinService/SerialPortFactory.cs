﻿using System.IO.Ports;
using System.Text;

namespace PosiflexTabletScannerWinService
{
    class SerialPortFactory
    {
        private static WebsocketCommunication WEBSOCKET;

        public static SerialPort Create(string portName, WebsocketCommunication websocket)
        {
            WEBSOCKET = websocket;
            var sp = new SerialPort(portName);

            sp.BaudRate = 9600;
            sp.Parity = Parity.None;
            sp.StopBits = StopBits.One;
            sp.DataBits = 8;
            sp.Handshake = Handshake.None;
            sp.RtsEnable = true;
            sp.Encoding = Encoding.ASCII;

            sp.DataReceived += SerialPort_DataReceived;

            return sp;
        }

        private static void SerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            lock (Worker.SerialPortHeartBeatCheckerLock)
            {
                SerialPort sp = (SerialPort)sender;
                string indata = sp.ReadExisting();
                Program.EVENT_LOG.WriteEntry($"Read data={indata}", System.Diagnostics.EventLogEntryType.Information);
                WEBSOCKET.Send(indata);
            }
        }
    }
}


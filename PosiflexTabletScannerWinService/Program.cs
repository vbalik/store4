using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Diagnostics;
using System.Security;

namespace PosiflexTabletScannerWinService
{
    public class Program
    {
        public static EventLog EVENT_LOG;
        public static string LOGGING_SOURCE = "S4.ComPortListener";
        public static string LOGGING_LOG = "S4.ComPortListenerLog";

        public static void Main(string[] args)
        {
            EVENT_LOG = CreateEventLog();

            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
            .UseWindowsService()
            .ConfigureServices((hostContext, services) =>
            {
                services.AddHostedService<Worker>();
            });

        private static EventLog CreateEventLog()
        {
            var evl = new EventLog();
            evl.Source = LOGGING_SOURCE;
            evl.Log = LOGGING_LOG;

            try
            {
                // searching the source throws a security exception ONLY if not exists!
                if (!EventLog.SourceExists(LOGGING_SOURCE))
                    EventLog.CreateEventSource(LOGGING_SOURCE, LOGGING_LOG);

                return evl;
            }
            catch (SecurityException)
            {
                Program.EVENT_LOG.WriteEntry($"EventLog failed", EventLogEntryType.Error);
                throw;
            }
        }
    }
}

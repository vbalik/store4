﻿using S4.Entities.Helpers;
using S4.ProcedureModels.Dispatch;
using S4mp.Client.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace S4mp.Client
{
    public class DispatchClient : ClientBase, IDispatchClient
    {
        private const string CONTROLLER_NAME = "procedures";

        public virtual async Task<List<DocumentInfo>> GetListDirections(DispatchListDirections model)
        {
            var result = await base.PostAsync<DispatchListDirections>(CONTROLLER_NAME,
                nameof(DispatchListDirections),
                model);

            return result.Directions;
        }

        public virtual async Task<DispatchShowItems> GetListItems(DispatchShowItems model)
        {
            var result = await base.PostAsync<DispatchShowItems>(CONTROLLER_NAME,
                nameof(DispatchShowItems),
                model);

            return result;
        }

        public virtual async Task<DispatchItemInstruct> GetItemInstruction(DispatchItemInstruct model)
        {
            var result = await base.PostAsync<DispatchItemInstruct>(CONTROLLER_NAME,
                nameof(DispatchItemInstruct),
                model);

            return result;
        }

        public virtual async Task<DispatchPosError> PosError(DispatchPosError model)
        {
            var result = await base.PostAsync<DispatchPosError>(CONTROLLER_NAME,
                nameof(DispatchPosError),
                model);

            return result;
        }

        public virtual async Task<DispatchConfirmItem> ConfirmItem(DispatchConfirmItem model)
        {
            var result = await base.PostAsync<DispatchConfirmItem>(CONTROLLER_NAME,
                nameof(DispatchConfirmItem),
                model);

            return result;
        }
        
        public virtual async Task<DispatchSaveItem> SaveItem(DispatchSaveItem model)
        {
            var result = await base.PostAsync<DispatchSaveItem>(CONTROLLER_NAME,
                nameof(DispatchSaveItem),
                model);

            return result;
        }
        
        public virtual async Task<DispatchTestDirect> TestDirect(DispatchTestDirect model)
        {
            var result = await base.PostAsync<DispatchTestDirect>(CONTROLLER_NAME,
                nameof(DispatchTestDirect),
                model);

            return result;
        }

        public virtual async Task PrintBoxLabel(DispatchPrintBoxLabel model)
        {
            var result = await base.PostAsync<DispatchPrintBoxLabel>(CONTROLLER_NAME,
                nameof(DispatchPrintBoxLabel),
                model);
        }

        public virtual async Task<DispatchCalcBoxLabels> GetCalcBoxLabels(DispatchCalcBoxLabels model)
        {
            var result = await base.PostAsync<DispatchCalcBoxLabels>(CONTROLLER_NAME,
                nameof(DispatchCalcBoxLabels),
                model);

            return result;
        }

        public virtual async Task<DispatchPrint> DispatchPrint(DispatchPrint model)
        {
            var result = await base.PostAsync<DispatchPrint>(CONTROLLER_NAME,
                nameof(DispatchPrint),
                model);

            return result;
        }

        public virtual async Task<DispatchSetXtraValues> SetXtraValues(DispatchSetXtraValues model)
        {
            var result = await base.PostAsync<DispatchSetXtraValues>(CONTROLLER_NAME,
                nameof(DispatchSetXtraValues),
                model);

            return result;
        }

        public async Task<DispatchAvailableDirectionsDocNumber> FindDirectionsByDocNumber(DispatchAvailableDirectionsDocNumber model)
        {
            var result = await base.PostAsync<DispatchAvailableDirectionsDocNumber>(CONTROLLER_NAME,
                nameof(DispatchAvailableDirectionsDocNumber),
                model);

            return result;
        }

        public async Task<DispatchWait> AssignDispatchToUser(DispatchWait model)
        {
            var result = await base.PostAsync<DispatchWait>(CONTROLLER_NAME,
                nameof(DispatchWait),
                model);

            return result;
        }

        public virtual async Task<bool> GetSetWeightBoxCountDataSetting()
        {
            var result = await base.PostAsync<bool, object>("Direction",
                "Settings/SetWeightBoxCountData",
                null);

            return result;
        }
    }
}

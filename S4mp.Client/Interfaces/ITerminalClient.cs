﻿using S4.Entities.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace S4mp.Client.Interfaces
{
    public interface ITerminalClient 
    {
        Task<List<TerminalMenuItem>> MenuItems();
        Task<TerminalDefaultSettings> GetDefaultSettings();
    }
}

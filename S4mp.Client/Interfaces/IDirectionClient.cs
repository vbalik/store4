﻿using S4.Entities.Helpers;
using S4.Entities.Models;
using S4.ProcedureModels.Exped;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace S4mp.Client.Interfaces
{
    public interface IDirectionClient
    {
        Task<List<DocumentInfo>> ExpeditionListDirects();
        Task<ExpedDone> ExpeditionDone(ExpedDone model);
    }
}

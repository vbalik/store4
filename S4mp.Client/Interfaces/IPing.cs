﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace S4mp.Client.Interfaces
{
    public interface IPing
    {
        Task<bool> PingWSAsync(string apiUrl);
        bool PingWSSync(string apiUrl);
    }
}

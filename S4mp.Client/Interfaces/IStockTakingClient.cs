﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using Models = S4.ProcedureModels.StockTaking;

namespace S4mp.Client.Interfaces
{
    public interface IStockTakingClient
    {
        Task<Models.StockTakingListOpened> GetStockTakingListOpened();
        Task<Models.StockTakingScanPos> GetStockTakingPositionItems(Models.StockTakingScanPos model);
        Task<Models.StockTakingAddPosItem> AddStockTakingItem(Models.StockTakingAddPosItem model);
        Task<Models.StockTakingDeletePosItem> DeleteStockTakingItem(Models.StockTakingDeletePosItem model);
        Task<Models.StockTakingBegin> StockTakingBegin(Models.StockTakingBegin model);
        Task<Models.StockTakingPositionOK> RollbackStockTakingPositionItems(Models.StockTakingPositionOK model);
    }
}

﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace S4mp.Client.Interfaces
{
    public interface IPositionClient
    {
        Task<ObservableCollection<S4.Entities.Position>> FindPosition(string posCodePart);

        Task<S4.Entities.Position> GetPosition(string posCode);
    }
}

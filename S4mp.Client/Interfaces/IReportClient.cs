﻿using System.Collections.Generic;
using System.Threading.Tasks;
using S4.Entities;

namespace S4mp.Client.Interfaces
{
    public interface IReportClient
    {
        Task<List<PrinterLocation>> GetPrinterLocation(string reportType);
        Task<List<Reports>> GetReports(string reportType);
    }
}

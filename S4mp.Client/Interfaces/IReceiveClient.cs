﻿using S4.ProcedureModels.Receive;
using System.Threading.Tasks;

namespace S4mp.Client.Interfaces
{
    public interface IReceiveClient
    {
        Task<ReceiveProcMan> GetDocument(ReceiveProcMan model);
        Task<ReceiveSaveItem> SaveItem(ReceiveSaveItem model);
        Task<ReceiveCancelItem> CancelItem(ReceiveCancelItem model);
        Task Done(ReceiveDone model);
        Task TestDirection(ReceiveTestDirection model);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using S4.Entities;
using S4.Entities.Models;

namespace S4mp.Client.Interfaces
{
    public interface IArticleClient
    {
        Task<List<S4.Entities.Models.ArticlePackingInfo>> SearchByCode(string articleCodePart);
        Task<List<S4.Entities.Models.ArticlePackingInfo>> SearchByDesc(string articleDescPart);
        Task<List<S4.Entities.Models.ArticlePackingInfo>> SearchByBarCode(string barCode);
        Task<ArticlePacking> GetMovablePacking(int articleID);
        Task<ArticlePackingInfo> GetArtPacking(int artPackingID);
        Task<ArticleModel> GetArticleModel(int articleID);
    }
}
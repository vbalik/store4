﻿using System.Collections.Generic;
using System.Threading.Tasks;
using S4.Entities.Helpers;
using S4.ProcedureModels.Dispatch;

namespace S4mp.Client.Interfaces
{
    public interface IDispatchClient
    {
        Task<List<DocumentInfo>> GetListDirections(DispatchListDirections model);

        Task<DispatchShowItems> GetListItems(DispatchShowItems model);

        Task<DispatchItemInstruct> GetItemInstruction(DispatchItemInstruct model);

        Task<DispatchPosError> PosError(DispatchPosError model);

        Task<DispatchConfirmItem> ConfirmItem(DispatchConfirmItem model);

        Task<DispatchSaveItem> SaveItem(DispatchSaveItem model);

        Task<DispatchTestDirect> TestDirect(DispatchTestDirect model);

        Task PrintBoxLabel(DispatchPrintBoxLabel model);

        Task<DispatchCalcBoxLabels> GetCalcBoxLabels(DispatchCalcBoxLabels model);

        Task<DispatchPrint> DispatchPrint(DispatchPrint model);

        Task<DispatchSetXtraValues> SetXtraValues(DispatchSetXtraValues model);

        Task<DispatchAvailableDirectionsDocNumber> FindDirectionsByDocNumber(DispatchAvailableDirectionsDocNumber model);

        Task<DispatchWait> AssignDispatchToUser(DispatchWait model);

        Task<bool> GetSetWeightBoxCountDataSetting();
    }
}
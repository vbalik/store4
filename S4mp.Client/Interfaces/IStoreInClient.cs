﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using S4.ProcedureModels.StoreIn;

namespace S4mp.Client.Interfaces
{
    public interface IStoreInClient
    {
        Task<StoreInListDirects> GetListDirects(StoreInListDirects model);
        Task<StoreInProc> StoreInDocument(StoreInProc model);
        Task<StoreInListNoCarr> ListNoCarr(StoreInListNoCarr model);
        Task<StoreInCheckCarrier> CheckCarrier(StoreInCheckCarrier model);
        Task<StoreInCheckPos> CheckPos(StoreInCheckPos model);
        Task<StoreInSaveItem> SaveItem(StoreInSaveItem model);
        Task<StoreInTestDirect> TestDirect(StoreInTestDirect model);
        Task<StoreInAcceptPos> AcceptPos(StoreInAcceptPos model);
        Task<StoreInAcceptPosNoCarr> AcceptPosNoCarr(StoreInAcceptPosNoCarr model);
    }
}

﻿using System.Threading.Tasks;
using Models = S4.ProcedureModels.BarCode;

namespace S4mp.Client.Interfaces
{
    public interface IBarcodeClient
    {
        Task<Models.BarcodeSet> BarcodeSet(Models.BarcodeSet model);

        Task<Models.BarcodeDelete> BarcodeDelete(Models.BarcodeDelete model);

        Task<Models.BarcodeMakeCode> BarcodeMakeCode(Models.BarcodeMakeCode model);

        Task<Models.BarcodeListPrinters> BarcodeListPrinters(Models.BarcodeListPrinters model);

        Task<Models.BarcodePrintLabel> BarcodePrintLabel(Models.BarcodePrintLabel model);

        Task<Models.BarcodePrintCarrierLabels> BarcodePrintCarrierLabels(Models.BarcodePrintCarrierLabels model);
    }
}

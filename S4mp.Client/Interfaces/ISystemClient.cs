﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using S4mp.Core.Logger;

namespace S4mp.Client.Interfaces
{
    public interface ISystemClient
    {
        Task<ReportErrorModel> SendErrorLogs(ReportErrorModel errorReportModel);
    }
}

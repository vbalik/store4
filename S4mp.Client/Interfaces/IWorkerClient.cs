﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace S4mp.Client.Interfaces
{
    public interface IWorkerClient
    {
        Task<S4.ProcedureModels.Login.LoginModel> TryLogOn(S4.ProcedureModels.Login.LoginModel loginModel);
    }
}

﻿using S4.ProcedureModels.DispCheck;
using System.Threading.Tasks;

using Models = S4.ProcedureModels.DispCheck;

namespace S4mp.Client.Interfaces
{
    public interface IDispatchCheckClient
    {
        Task<Models.DispCheckListDirections> DispCheckListDirections(DispCheckListDirections model);
        Task<Models.DispCheckProc> DispCheckProc(DispCheckProc model);
        Task<Models.DispCheckGetItems> DispCheckGetItems(DispCheckGetItems model);
        Task<Models.DispCheckDone> DispCheckDone(DispCheckDone model);
        Task<DispCheckWait> DispCheckWait(DispCheckWait model);
        Task<DispCheckAvailableDirectionsDocNumber> FindDirectionsByDocNumber(DispCheckAvailableDirectionsDocNumber model);
    }
}

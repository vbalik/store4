﻿using System.Collections.Generic;
using System.Threading.Tasks;

using S4.Entities.Views;

namespace S4mp.Client
{
    public interface IArtOnStoreClient
    {
        Task<List<ArtOnStoreView>> GetOnPosition(int positionID);

        Task<List<ArtOnStoreView>> GetByArticle(int articleID);

        Task<List<ArtOnStoreView>> GetByCarrier(string carrierNum);
    }
}

﻿using S4.ProcedureModels.DispCheck;
using System.Threading.Tasks;
using Models = S4.ProcedureModels.DispCheck;

namespace S4mp.Client
{
    public class DispatchCheckClient : ClientBase, Interfaces.IDispatchCheckClient
    {
        private const string CONTROLLER_NAME = "procedures";

        public async Task<DispCheckListDirections> DispCheckListDirections(DispCheckListDirections model)
        {
            return await base.PostAsync<Models.DispCheckListDirections>(CONTROLLER_NAME, nameof(Models.DispCheckListDirections), model);
        }

        public async Task<DispCheckProc> DispCheckProc(DispCheckProc model)
        {
            return await base.PostAsync<Models.DispCheckProc>(CONTROLLER_NAME, nameof(Models.DispCheckProc), model);
        }

        public async Task<DispCheckGetItems> DispCheckGetItems(DispCheckGetItems model)
        {
            return await base.PostAsync<Models.DispCheckGetItems>(CONTROLLER_NAME, nameof(Models.DispCheckGetItems), model);
        }

        public async Task<DispCheckDone> DispCheckDone(DispCheckDone model)
        {
            return await base.PostAsync<Models.DispCheckDone>(CONTROLLER_NAME, nameof(Models.DispCheckDone), model);
        }

        public async Task<DispCheckWait> DispCheckWait(DispCheckWait model)
        {
            return await base.PostAsync<DispCheckWait>(CONTROLLER_NAME, nameof(DispCheckWait), model);
        }

        public async Task<DispCheckAvailableDirectionsDocInfo> FindDirectionsByDocInfo(DispCheckAvailableDirectionsDocInfo model)
        {
            var result = await base.PostAsync<DispCheckAvailableDirectionsDocInfo>(CONTROLLER_NAME,
                nameof(DispCheckAvailableDirectionsDocInfo),
                model);

            return result;
        }

        public async Task<DispCheckAvailableDirectionsDocNumber> FindDirectionsByDocNumber(DispCheckAvailableDirectionsDocNumber model)
        {
            var result = await base.PostAsync<DispCheckAvailableDirectionsDocNumber>(CONTROLLER_NAME,
                nameof(DispCheckAvailableDirectionsDocNumber),
                model);

            return result;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S4mp.Client
{
    public class ApiException_Forbidden : Exception
    {
        public ApiException_Forbidden(string url) : base(MakeMessage(url))
        { }


        #region private

        private static string MakeMessage(string url)
        {
            return String.Format("Url: '{0}'", url);
        }

        #endregion
    }
}

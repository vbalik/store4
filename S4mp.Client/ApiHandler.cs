﻿#define ENCRYPT_TOKEN

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;



namespace S4mp.Client
{
    class ApiHandler : DelegatingHandler
    {
        internal const string API_TOKEN = "S4_API_TOKEN";

        public ApiHandler()
        {
            InnerHandler = new HttpClientHandler();
        }


        protected async override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, System.Threading.CancellationToken cancellationToken)
        {
            // add token value
            request.Headers.Add(API_TOKEN, ClientStatus.Current.TokenValue);

            HttpResponseMessage response = await base.SendAsync(request, cancellationToken);

            // save token value
            if (response.IsSuccessStatusCode)
            {
                if (response.Headers.Contains(API_TOKEN))
                {
                    string token = response.Headers.GetValues(API_TOKEN).FirstOrDefault<string>();
                    ClientStatus.Current.LoggedOn(token);
                }
            }

            return response;
        }
    }
}

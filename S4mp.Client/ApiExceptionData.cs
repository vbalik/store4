﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S4mp.Client
{
    public class ApiExceptionData
    {
        public string Message { get; set; }
        public string ExceptionMessage { get; set; }
        public string ExceptionType { get; set; }
        public string StackTrace { get; set; }

        // procedure response
        public string Error { get; set; }
        public object Details { get; set; }
    }
}

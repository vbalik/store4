﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;


namespace S4mp.Client
{
    public class ApiException : Exception
    {
        private string _url;
        private ApiExceptionData _apiExceptionData;



        public ApiException(string message) : base(message) { }

        public ApiException(string message, params object[] args)
            : base(String.Format(message, args))
        { }

        public ApiException(System.Net.Http.HttpResponseMessage response, string url)
            : base(MakeMessage(response))
        {
            _url = url;
        }


        public ApiException(System.Net.Http.HttpResponseMessage response, ApiExceptionData apiExceptionData, string url)
            : base(MakeMessage(response, apiExceptionData))
        {
            this._apiExceptionData = apiExceptionData;
            _url = url;
        }


        public string Url
        { get { return _url; } }


        public ApiExceptionData ApiExceptionData
        { get { return _apiExceptionData; } }



        #region private

        private static string MakeMessage(System.Net.Http.HttpResponseMessage response)
        {
            return String.Format("{0} - {1}", response.StatusCode, response.ReasonPhrase);
        }


        private static string MakeMessage(System.Net.Http.HttpResponseMessage response, Client.ApiExceptionData apiExceptionData)
        {
            return String.Format("{0} {1} - {2} ({3}), procErr: {4}, procIn: {5}",
                response.StatusCode,
                response.RequestMessage.RequestUri.AbsolutePath,
                apiExceptionData.ExceptionMessage,
                apiExceptionData.ExceptionType,
                apiExceptionData.Error,
                JsonConvert.SerializeObject(apiExceptionData.Details));
        }

        #endregion
    }
}

﻿using S4mp.Client.Interfaces;
using System.Threading.Tasks;
using Models = S4.ProcedureModels.BarCode;

namespace S4mp.Client
{
    public class BarcodeClient : ClientBase, IBarcodeClient
    {
        private const string CONTROLLER_NAME = "procedures";

        public async Task<Models.BarcodeSet> BarcodeSet(Models.BarcodeSet model)
        {
            return await base.PostAsync<Models.BarcodeSet>(CONTROLLER_NAME, nameof(Models.BarcodeSet), model);
        }

        public async Task<Models.BarcodeDelete> BarcodeDelete(Models.BarcodeDelete model)
        {
            return await base.PostAsync<Models.BarcodeDelete>(CONTROLLER_NAME, nameof(Models.BarcodeDelete), model);
        }

        public async Task<Models.BarcodeMakeCode> BarcodeMakeCode(Models.BarcodeMakeCode model)
        {
            return await base.PostAsync<Models.BarcodeMakeCode>(CONTROLLER_NAME, nameof(Models.BarcodeMakeCode), model);
        }

        public async Task<Models.BarcodeListPrinters> BarcodeListPrinters(Models.BarcodeListPrinters model)
        {
            return await base.PostAsync<Models.BarcodeListPrinters>(CONTROLLER_NAME, nameof(Models.BarcodeListPrinters), model);
        }

        public async Task<Models.BarcodePrintLabel> BarcodePrintLabel(Models.BarcodePrintLabel model)
        {
            return await base.PostAsync<Models.BarcodePrintLabel>(CONTROLLER_NAME, nameof(Models.BarcodePrintLabel), model);
        }

        public async Task<Models.BarcodePrintCarrierLabels> BarcodePrintCarrierLabels(Models.BarcodePrintCarrierLabels model)
        {
            return await base.PostAsync<Models.BarcodePrintCarrierLabels>(CONTROLLER_NAME, nameof(Models.BarcodePrintCarrierLabels), model);
        }
    }
}

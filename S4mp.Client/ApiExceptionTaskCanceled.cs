﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S4mp.Client
{
    public class ApiExceptionTaskCanceled : Exception
    {
        public ApiExceptionTaskCanceled(string msg) : base(MakeMessage(msg))
        { }


        #region private

        private static string MakeMessage(string msg)
        {
            return String.Format("ApiExceptionTaskCanceled: {0}", msg);
        }

        #endregion
    }
}

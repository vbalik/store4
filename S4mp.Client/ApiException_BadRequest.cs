﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S4mp.Client
{
    public class ApiException_BadRequest: Exception
    {
        public ApiException_BadRequest(System.Net.Http.HttpRequestMessage httpRequestMessage, object parameters)
            : base(MakeMessage(httpRequestMessage, parameters))
        { }

        public ApiException_BadRequest(System.Net.Http.HttpResponseMessage httpResponseMessage, object parameters, ApiExceptionData exceptionData)
            : base(MakeMessage2(httpResponseMessage, parameters, exceptionData))
        { }


        #region private

        private static string MakeMessage(System.Net.Http.HttpRequestMessage httpRequestMessage, object parameters)
        {
            return String.Format("Url: '{0}', Parameters: '{1}'" + httpRequestMessage.RequestUri, parameters);
        }

        private static string MakeMessage2(System.Net.Http.HttpResponseMessage httpResponseMessage, object parameters, ApiExceptionData exceptionData)
        {
            return String.Format("{0} - {1}, Inputs: {2}, RespData: {3}",
                httpResponseMessage.ReasonPhrase,
                httpResponseMessage.RequestMessage.RequestUri.AbsolutePath,
                JsonConvert.SerializeObject(parameters ?? new { }),
                JsonConvert.SerializeObject(exceptionData));
        }

        #endregion
    }
}

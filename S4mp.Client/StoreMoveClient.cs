﻿using S4.ProcedureModels.Move;
using System.Threading.Tasks;

namespace S4mp.Client
{
    public class StoreMoveClient : ClientBase
    {
        private const string CONTROLLER_NAME = "procedures";

        public virtual async Task<MoveGetPosition> MoveGetPosition(MoveGetPosition model)
        {
            var document = await base.PostAsync<MoveGetPosition>(CONTROLLER_NAME,
                nameof(MoveGetPosition),
                model);

            return document;
        }

        public virtual async Task<MoveLockPos> MoveLockPosition(MoveLockPos model)
        {
            var document = await base.PostAsync<MoveLockPos>(CONTROLLER_NAME,
                nameof(MoveLockPos),
                model);

            return document;
        }

        public virtual async Task<MoveSave> MoveSave(MoveSave model)
        {
            var document = await base.PostAsync<MoveSave>(CONTROLLER_NAME,
                nameof(MoveSave),
                model);

            return document;
        }
    }
}
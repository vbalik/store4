﻿using S4.ProcedureModels.StoreIn;
using System;
using System.Threading.Tasks;

namespace S4mp.Client
{
    public class StoreInClient : ClientBase, Interfaces.IStoreInClient
    {
        private const string CONTROLLER_NAME = "procedures";

        public virtual async Task<StoreInListDirects> GetListDirects(StoreInListDirects model)
        {
            var result = await base.PostAsync<StoreInListDirects>(CONTROLLER_NAME,
                nameof(StoreInListDirects),
                model);

            return result;
        }

        public virtual async Task<StoreInProc> StoreInDocument(StoreInProc model)
        {
            var result = await base.PostAsync<StoreInProc>(CONTROLLER_NAME,
                nameof(StoreInProc),
                model);

            return result;
        }

        public virtual async Task<StoreInListNoCarr> ListNoCarr(StoreInListNoCarr model)
        {
            var result = await base.PostAsync<StoreInListNoCarr>(CONTROLLER_NAME,
                nameof(StoreInListNoCarr),
                model);

            return result;
        }

        public virtual async Task<StoreInCheckCarrier> CheckCarrier(StoreInCheckCarrier model)
        {
            var result = await base.PostAsync<StoreInCheckCarrier>(CONTROLLER_NAME,
                nameof(StoreInCheckCarrier),
                model);

            return result;
        }

        public virtual async Task<StoreInCheckPos> CheckPos(StoreInCheckPos model)
        {
            var result = await base.PostAsync<StoreInCheckPos>(CONTROLLER_NAME,
                nameof(StoreInCheckPos),
                model);

            return result;
        }

        public virtual async Task<StoreInSaveItem> SaveItem(StoreInSaveItem model)
        {
            var result = await base.PostAsync<StoreInSaveItem>(CONTROLLER_NAME,
                nameof(StoreInSaveItem),
                model);

            return result;
        }

        public virtual async Task<StoreInTestDirect> TestDirect(StoreInTestDirect model)
        {
            var result = await base.PostAsync<StoreInTestDirect>(CONTROLLER_NAME,
                nameof(StoreInTestDirect),
                model);

            return result;
        }

        public virtual async Task<StoreInAcceptPos> AcceptPos(StoreInAcceptPos model)
        {
            var result = await base.PostAsync<StoreInAcceptPos>(CONTROLLER_NAME,
                nameof(StoreInAcceptPos),
                model);

            return result;
        }

        public virtual async Task<StoreInAcceptPosNoCarr> AcceptPosNoCarr(StoreInAcceptPosNoCarr model)
        {
            var result = await base.PostAsync<StoreInAcceptPosNoCarr>(CONTROLLER_NAME,
                nameof(StoreInAcceptPosNoCarr),
                model);

            return result;
        }
    }
}

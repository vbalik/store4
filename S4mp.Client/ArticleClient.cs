﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using S4.Entities;
using S4.Entities.Models;
using S4.Entities.Views;

namespace S4mp.Client
{
    public class ArticleClient : ClientBase, Interfaces.IArticleClient
    {
        public virtual async Task<List<S4.Entities.Models.ArticlePackingInfo>> SearchByCode(string articleCodePart)
        {
            return await base.PostAsync<List<S4.Entities.Models.ArticlePackingInfo>, string>("Article", "SearchByCodeP", articleCodePart);
        }

        public virtual async Task<List<S4.Entities.Models.ArticlePackingInfo>> SearchByDesc(string articleDescPart)
        {
            return await base.PostAsync<List<S4.Entities.Models.ArticlePackingInfo>, string>("Article", "SearchByDescP", articleDescPart);
        }

        public virtual async Task<List<S4.Entities.Models.ArticlePackingInfo>> SearchByBarCode(string barCode)
        {
            return await base.PostAsync<List<S4.Entities.Models.ArticlePackingInfo>, string>("Article", "SearchByBarCodeP", barCode);
        }

        public virtual async Task<ArticlePacking> GetMovablePacking(int articleID)
        {
            var article = await GetArticleModel(articleID);
            return article.Packings.SingleOrDefault(p => p.MovablePack);
        }

        public virtual async Task<ArticlePackingInfo> GetArtPacking(int artPackingID)
        {
            return await base.GetAsync<ArticlePackingInfo>("Article/GetArtPacking/", artPackingID);
        }

        public virtual async Task<ArticleModel> GetArticleModel(int articleID)
        {
            return await base.GetAsync<ArticleModel>("Article/GetArticleModel/", articleID);
        }
    }
}

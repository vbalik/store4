﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using S4.Entities;



namespace S4mp.Client
{

	public class WorkerClient : ClientBase, Interfaces.IWorkerClient
	{	
		public async Task<S4.ProcedureModels.Login.LoginModel> TryLogOn(S4.ProcedureModels.Login.LoginModel loginModel)
		{
            return await PostAsync("procedures", "login", loginModel);
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S4mp.Client
{
    public class ClientStatus
    {
        internal enum ClientStatusEnum
        {
            NotSet,
            InProc,
            LoggedOn
        }


        private object _lock = new object();
        private ClientStatusEnum _status = ClientStatusEnum.NotSet;
        private string _tokenValue = null;



        public void NotSet()
        {
            lock (_lock)
            {
                _tokenValue = null;
                _status = ClientStatusEnum.NotSet;
            }
        }


        internal void InProc()
        {
            lock (_lock)
            {
                _status = ClientStatusEnum.InProc;
            }
        }


        internal void LoggedOn(string newToken)
        {
            lock (_lock)
            {
                _tokenValue = newToken;
                _status = ClientStatusEnum.LoggedOn;
            }
        }


        internal ClientStatusEnum Status
        {
            get
            {
                lock (_lock)
                {
                    return _status;
                }
            }
        }


        internal string TokenValue
        {
            get
            {
                lock (_lock)
                {
                    return _tokenValue;
                }
            }
        }


        public static ClientStatus Current = new ClientStatus();
    }
}

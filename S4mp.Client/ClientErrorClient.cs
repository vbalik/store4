﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace S4mp.Client
{
    public class ClientErrorClient : ClientBase
    {
        public virtual async Task<int> Insert(S4.Entities.ClientError clientError)
        {
            return await base.PostAsync("clienterror/inserterrorm", clientError);
        }
    }
}

﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace S4mp.Client
{
    public class PositionClient : ClientBase, Interfaces.IPositionClient
    {
        private const string CONTROLLER_NAME = "position";

        public virtual async Task<ObservableCollection<S4.Entities.Position>> FindPosition(string posCodePart)
        {
            var positions = await base.PostAsync<ObservableCollection<S4.Entities.Position>, object>(CONTROLLER_NAME,
                "searchpositions/",
                new { part = posCodePart });

            return positions;
        }

        public virtual async Task<S4.Entities.Position> GetPosition(string posCode)
        {
            return await base.PostAsync<S4.Entities.Position, string>("position", "getpositionp", posCode);
        }
    }
}

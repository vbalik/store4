﻿using S4.Core;
using S4.ProcedureModels.Receive;
using S4mp.Client.ClientNavigation;
using S4mp.Settings;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace S4mp.Client
{
    public class ReceiveClient : ClientBase, Interfaces.IReceiveClient
    {
        private const string CONTROLLER_NAME = "procedures";

        public virtual async Task<ReceiveProcMan> GetDocument(ReceiveProcMan model)
        {
            var document = await base.PostAsync<ReceiveProcMan>(CONTROLLER_NAME,
                nameof(ReceiveProcMan),
                model);

            return document;
        }

        public virtual async Task<ReceiveSaveItem> SaveItem(ReceiveSaveItem model)
        {
            var item = await base.PostAsync<ReceiveSaveItem>(CONTROLLER_NAME,
                nameof(ReceiveSaveItem),
                model);

            return item;
        }

        public virtual async Task<ReceiveCancelItem> CancelItem(ReceiveCancelItem model)
        {
            var item = await base.PostAsync<ReceiveCancelItem>(CONTROLLER_NAME,
                nameof(ReceiveCancelItem),
                model);

            return item;
        }

        public virtual async Task Done(ReceiveDone model)
        {
            await base.PostAsync<ReceiveDone>(CONTROLLER_NAME,
                nameof(ReceiveDone),
                model);
        }

        public virtual async Task TestDirection(ReceiveTestDirection model)
        {
            await base.PostAsync<dynamic>(CONTROLLER_NAME,
                nameof(ReceiveTestDirection),
                model);
        }
    }
}

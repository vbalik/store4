﻿using S4.Entities.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace S4mp.Client
{
    public class TerminalClient : ClientBase, Interfaces.ITerminalClient
    {
        public virtual async Task<List<TerminalMenuItem>> MenuItems()
        {
            return await base.GetAsync<List<S4.Entities.Helpers.TerminalMenuItem>>("terminal/menuitems");
        }

        public virtual async Task<TerminalDefaultSettings> GetDefaultSettings()
        {
            return await base.GetAsync<TerminalDefaultSettings>("terminal/get-default-settings");
        }
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace S4mp.Client
{
    public class Serializer
    {
        public static T Deserialize<T>(string jValue)
        {
            T res = JsonConvert.DeserializeObject<T>(jValue);

            return res;
        }

        public static string Serialize<T>(T jObject)
        {
            var res = JsonConvert.SerializeObject(jObject);

            return res;
        }
    }
}
﻿using System;
using System.Threading.Tasks;
using S4mp.Core.Logger;

namespace S4mp.Client
{
    public class SystemClient : ClientBase, Interfaces.ISystemClient
    {
        public virtual async Task<ReportErrorModel> SendErrorLogs(ReportErrorModel errorReportModel)
        {
            return await base.PostAsync<ReportErrorModel>(
                "Terminal",
                "ReportError",
                errorReportModel);
        }
    }
}

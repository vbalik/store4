﻿using System.Collections.Generic;
using System.Threading.Tasks;

using S4.Entities.Views;

namespace S4mp.Client
{
    public class ArtOnStoreClient : ClientBase, IArtOnStoreClient
    {
        public virtual async Task<List<ArtOnStoreView>> GetOnPosition(int positionID)
        {
            return await base.GetAsync<List<ArtOnStoreView>>("OnStore/GetOnPositionM/", positionID);
        }

        public virtual async Task<List<ArtOnStoreView>> GetByArticle(int articleID)
        {
            return await base.GetAsync<List<ArtOnStoreView>>("OnStore/GetByArticleM/", articleID);
        }

        public virtual async Task<List<ArtOnStoreView>> GetByCarrier(string carrierNum)
        {
            return await base.PostAsync<List<ArtOnStoreView>, string>("OnStore", "GetByCarrierMP", carrierNum);
        }
    }
}

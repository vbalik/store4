﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S4mp.Client
{
    public class ApiException_NotFound: Exception
    {
        public ApiException_NotFound(System.Net.Http.HttpRequestMessage httpRequestMessage)
            : base(MakeMessage(httpRequestMessage))
        { }


        #region private

        private static string MakeMessage(System.Net.Http.HttpRequestMessage httpRequestMessage)
        {
            return String.Format("Url: '{0}'", httpRequestMessage.RequestUri);
        }

        #endregion
    }
}

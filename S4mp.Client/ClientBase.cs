﻿using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using S4.Core;
using S4mp.Client.Utils;
using S4.ProcedureModels;
using Xamarin.Forms;
using S4mp.Client.ClientNavigation;
using Newtonsoft.Json;

namespace S4mp.Client
{
    public abstract class ClientBase
	{
		public const string MEDIA_TYPE_JSON = "application/json";

		private const string API_PATH_1 = "api/{0}";
		private const string API_PATH_2 = "api/{0}/{1}";
		private const string API_PATH_3 = "api/{0}{1}";
		private const string API_PATH_4 = "api/{0}?pk1={1}&pk2={2}";

		protected bool _noUserAction = false;


		public ClientBase()
		{

		}

		public static string ApiUrl
		{
			get
			{
				var settingURL = Singleton<Settings.LocalSettings>.Instance[Core.Consts.XMLPATH_API_URL];

                if (string.IsNullOrWhiteSpace(settingURL))
                    return null;

                var url = settingURL.Replace(" ", string.Empty);
				return url.Substring(url.Length - 1, 1).Equals("/") ? url : string.Format("{0}/", url);
			}
		}


		protected async Task<T> GetAsync<T>(string controllerName, string txt)
		{
			string path = String.Format(API_PATH_3, controllerName, txt);

			return await GetAsyncPrivate<T>(path);
		}

		protected async Task<T> GetAsync<T>(string controllerName, int id)
		{
			string path = String.Format(API_PATH_3, controllerName, id.ToString());

			return await GetAsyncPrivate<T>(path);
		}


		protected async Task<T> GetAsync<T>(string controllerName, string pk1, string pk2)
		{
			string path = String.Format(API_PATH_4, controllerName, pk1, pk2);

			HttpResponseMessage response = await CallApi(HttpMethod.Get, path);
			if (response.IsSuccessStatusCode)
			{
                try
                {
                    string o = await response.Content.ReadAsStringAsync();
                    return Serializer.Deserialize<T>(o);
                }
                catch (JsonReaderException)
                {
                    var loginPage = DependencyService.Get<ILoginPage>();
                    var navigation = DependencyService.Get<IClientNavigationService>();
                    await navigation.PopToLogin(loginPage);
                    return default;
                }
			}

			await ThrowException(response, null);
			return default(T);
		}


		protected async Task<T> GetAsync<T>(string controllerName, int? id)
		{
			string path = String.Format(API_PATH_2, controllerName, id);

			HttpResponseMessage response = await CallApi(HttpMethod.Get, path);
			if (response.IsSuccessStatusCode)
			{
                try
                {
                    string o = await response.Content.ReadAsStringAsync();
                    return Serializer.Deserialize<T>(o);
                }
                catch (JsonReaderException)
                {
                    var loginPage = DependencyService.Get<ILoginPage>();
                    var navigation = DependencyService.Get<IClientNavigationService>();
                    await navigation.PopToLogin(loginPage);
                    return default;
                }
            }

			await ThrowException(response, null);
			return default(T);
		}


		protected async Task<T> GetAsync<T>(string controllerName)
		{
			string path = String.Format(API_PATH_1, controllerName);

			HttpResponseMessage response = await CallApi(HttpMethod.Get, path);
			if (response.IsSuccessStatusCode)
			{
                try
                {
                    string o = await response.Content.ReadAsStringAsync();
                    return Serializer.Deserialize<T>(o);
                }
                catch (JsonReaderException)
                {
                    var loginPage = DependencyService.Get<ILoginPage>();
                    var navigation = DependencyService.Get<IClientNavigationService>();
                    await navigation.PopToLogin(loginPage);
                    return default;
                }
            }

			await ThrowException(response, null);
			return default(T);
		}


		protected async Task<int> PostAsync(string controllerName)
		{
			string path = String.Format(API_PATH_1, controllerName);

			HttpResponseMessage response = await CallApi(HttpMethod.Post, path);
			if (response.IsSuccessStatusCode)
			{
                try
                {
                    string o = await response.Content.ReadAsStringAsync();
                    return Serializer.Deserialize<int>(o);
                }
                catch (JsonReaderException)
                {
                    var loginPage = DependencyService.Get<ILoginPage>();
                    var navigation = DependencyService.Get<IClientNavigationService>();
                    await navigation.PopToLogin(loginPage);
                    return default;
                }
            }

			await ThrowException(response, null);
			return default(int);
		}

		protected async Task<int> PostAsync(string controllerName, object ob)
		{
			string path = String.Format(API_PATH_1, controllerName);

			HttpResponseMessage response = await CallApiWithObject(HttpMethod.Post, path, ob);
			if (response.IsSuccessStatusCode)
			{
                try
                {
                    string o = await response.Content.ReadAsStringAsync();
                    return Serializer.Deserialize<int>(o);
                }
                catch (JsonReaderException)
                {
                    var loginPage = DependencyService.Get<ILoginPage>();
                    var navigation = DependencyService.Get<IClientNavigationService>();
                    await navigation.PopToLogin(loginPage);
                    return default;
                }
            }

            await ThrowException(response, ob);
			return default(int);
		}

		protected async Task<T> PostAsync<T>(string controllerName, string action, T obj)
		{
            string path = String.Format(API_PATH_2, controllerName, action);

            HttpResponseMessage response = await CallApiWithObject<T>(HttpMethod.Post, path, obj);

			if (response.IsSuccessStatusCode)
			{
                try
                {
                    string responseContent = await response.Content.ReadAsStringAsync();
                    return Serializer.Deserialize<T>(responseContent);
                }
                catch (JsonReaderException)
                {
                    var loginPage = DependencyService.Get<ILoginPage>();
                    var navigation = DependencyService.Get<IClientNavigationService>();
                    await navigation.PopToLogin(loginPage);
                    return default;
                }
			}

            await ThrowException(response, obj);
			return default;
		}

        protected async Task<T> PostAsync<T, T2>(string controllerName, string action, T2 obj)
        {
            string path = String.Format(API_PATH_2, controllerName, action);

            using (var client = new HttpClient(new ApiHandler()))
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(MEDIA_TYPE_JSON));

                HttpResponseMessage response = new HttpResponseMessage();

                try
                {
                    response = await client.PostAsync(ApiUrl + path, new StringContent(Serializer.Serialize<T2>(obj), Encoding.UTF8, MEDIA_TYPE_JSON));
                }
                catch (TaskCanceledException)
                {
                    throw new ApiExceptionTaskCanceled(string.Format("{0} - {1} - {2}", "POST", path, GetProperties(obj)));
                }

                if (response.IsSuccessStatusCode)
                {
                    try
                    {
                        RefreshToken(response);
                        string o = await response.Content.ReadAsStringAsync();
                        return Serializer.Deserialize<T>(o);
                    }
                    catch (JsonReaderException)
                    {
                        var loginPage = DependencyService.Get<ILoginPage>();
                        var navigation = DependencyService.Get<IClientNavigationService>();
                        await navigation.PopToLogin(loginPage);
                        return default;
                    }
                }

                await ThrowException(response, obj);
                return default(T);
            }
        }


        protected async Task PutAsync(string controllerName, object ob, int ID)
		{
			string path = String.Format(API_PATH_2, controllerName, ID);

			HttpResponseMessage response = await CallApiWithObject(HttpMethod.Put, path, ob);
			if (response.IsSuccessStatusCode)
			{
				return;
			}

            await ThrowException(response, ob);
			return;
		}


		protected async Task PutAsync(string controllerName, object ob)
		{
			string path = String.Format(API_PATH_1, controllerName);

			HttpResponseMessage response = await CallApiWithObject(HttpMethod.Put, path, ob);
			if (response.IsSuccessStatusCode)
			{
				return;
			}

            await ThrowException(response, ob);
			return;
		}


		protected async Task DeleteAsync(string controllerName, int id)
		{
			string path = String.Format(API_PATH_2, controllerName, id);

			HttpResponseMessage response = await CallApiWithObject<object>(HttpMethod.Delete, path, null);

			if (response.IsSuccessStatusCode)
			{
				return;
			}

            await ThrowException(response, id);
			return;
		}


		protected async Task DeleteAsyncExtraDataDictionary(string controllerName, string path)
		{
			string path1 = String.Format(API_PATH_1, controllerName) + path;

			HttpResponseMessage response = await CallApiWithObject<object>(HttpMethod.Delete, path1, null);

			if (response.IsSuccessStatusCode)
			{
				return;
			}

            await ThrowException(response, null);
			return;
		}


		protected async Task<HttpResponseMessage> CallApiWithObject<T>(HttpMethod method, string path, T ob)
		{
			using (var client = new HttpClient(new ApiHandler()))
			{
				client.DefaultRequestHeaders.Accept.Clear();
				client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(MEDIA_TYPE_JSON));

				HttpResponseMessage response = new HttpResponseMessage();

				// set unique id for backend request per application lifetime, if is procedure call
				if (ob is ProceduresModel pm)
					pm.ExternalRequestId = CounterRequestId.GetId();

				try
				{
					if (method == HttpMethod.Put) //put update
						response = await client.PostAsync(ApiUrl + path, new StringContent(Serializer.Serialize<T>(ob), Encoding.UTF8, MEDIA_TYPE_JSON));
					else if (method == HttpMethod.Post) //post insert
						response = await client.PostAsync(ApiUrl + path, new StringContent(Serializer.Serialize<T>(ob), Encoding.UTF8, MEDIA_TYPE_JSON));
					else if (method == HttpMethod.Delete) //delete
						response = await client.DeleteAsync(ApiUrl + path);

                    RefreshToken(response);

                    return response;
				}
				catch (TaskCanceledException)
				{
					throw new ApiExceptionTaskCanceled(string.Format("{0} - {1} - {2}", method, path, GetProperties(ob)));
				}
			}
		}


		protected async Task<HttpResponseMessage> CallApi(HttpMethod method, string path)
		{
			using (var client = new HttpClient(new ApiHandler()))
			{
				client.DefaultRequestHeaders.Accept.Clear();
				client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(MEDIA_TYPE_JSON));

				var request = new HttpRequestMessage(method, ApiUrl + path);
				try
				{
					HttpResponseMessage response = await client.SendAsync(request);
					RefreshToken(response);

                    return response;
				}
				catch (TaskCanceledException)
				{

					throw new ApiExceptionTaskCanceled(string.Format("{0} - {1}", method, path));
				}

			}
		}


		protected async Task ThrowException(HttpResponseMessage response, object parameters)
		{
			string url = response.RequestMessage.RequestUri.ToString();

			// try to read server exception data
			if (response.Content.Headers.ContentType != null)
			{
				if (response.Content.Headers.ContentType.MediaType == ClientBase.MEDIA_TYPE_JSON)
				{
					var excDataContent = await response.Content.ReadAsStringAsync();
					ApiExceptionData excData = Serializer.Deserialize<ApiExceptionData>(excDataContent);

					if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
						throw new ApiException_BadRequest(response, parameters, excData);

					if (excData != null)
						throw new ApiException(response, excData, url);
				}
			}

			if (response.StatusCode == System.Net.HttpStatusCode.Forbidden)
				throw new ApiException_Forbidden(url);
			else if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
				throw new ApiException_NotFound(response.RequestMessage);
			else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
				throw new ApiException_BadRequest(response.RequestMessage, parameters);
			else
				throw new ApiException(response, url);
		}


		protected async Task<bool> IsServiceMode(HttpClient client)
		{
			//api/user/servicemode
			HttpResponseMessage response = await client.GetAsync(ApiUrl + "api/user/servicemode");

			if (response.IsSuccessStatusCode)
			{
				// set status & token
				string token = response.Headers.GetValues(ApiHandler.API_TOKEN).First();
				ClientStatus.Current.LoggedOn(token);

				string o = await response.Content.ReadAsStringAsync();
				return Serializer.Deserialize<bool>(o);
			}
			else
				return false;
		}

		#region private

		private async Task<T> GetAsyncPrivate<T>(string path)
		{
			HttpResponseMessage response = await CallApi(HttpMethod.Get, path);
			if (response.IsSuccessStatusCode)
			{
                try
                {
                    string o = await response.Content.ReadAsStringAsync();
                    return Serializer.Deserialize<T>(o);
                }
                catch (JsonReaderException)
                {
                    var loginPage = DependencyService.Get<ILoginPage>();
                    var navigation = DependencyService.Get<IClientNavigationService>();
                    await navigation.PopToLogin(loginPage);
                    return default;
                }
            }

			await ThrowException(response, null);
			return default(T);
		}

		private string GetProperties(object ob)
		{
			StringBuilder sb = new StringBuilder();
			Type t = ob.GetType();

			var props = t.GetRuntimeProperties();

			foreach (System.Reflection.PropertyInfo prp in props)
			{
				object value = prp.GetValue(ob, new object[] { });
				sb.Append(string.Format("{0} - {1}, ", prp.Name, value));
			}
			return sb.ToString();
		}

		private void RefreshToken(HttpResponseMessage response)
		{
			if (!response.Headers.Contains(ApiHandler.API_TOKEN))
				return;

            string token = response.Headers.GetValues(ApiHandler.API_TOKEN).First();
            ClientStatus.Current.LoggedOn(token);
        }

		#endregion

	}
}

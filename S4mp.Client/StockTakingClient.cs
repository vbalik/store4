﻿using System.Threading.Tasks;
using StockTakingS4 = S4.ProcedureModels.StockTaking;

namespace S4mp.Client
{
    public class StockTakingClient : ClientBase, Interfaces.IStockTakingClient
    {
        private const string CONTROLLER_NAME = "procedures";

        public virtual async Task<StockTakingS4.StockTakingListOpened> GetStockTakingListOpened()
        {
            var list = await base.PostAsync<StockTakingS4.StockTakingListOpened>(CONTROLLER_NAME,
                nameof(StockTakingS4.StockTakingListOpened),
                new StockTakingS4.StockTakingListOpened());

            return list;
        }

        public virtual async Task<StockTakingS4.StockTakingScanPos> GetStockTakingPositionItems(StockTakingS4.StockTakingScanPos model)
        {
            var item = await base.PostAsync<StockTakingS4.StockTakingScanPos>(CONTROLLER_NAME,
                nameof(StockTakingS4.StockTakingScanPos),
                model);

            return item;
        }

        public virtual async Task<StockTakingS4.StockTakingAddPosItem> AddStockTakingItem(StockTakingS4.StockTakingAddPosItem model)
        {
            var item = await base.PostAsync<StockTakingS4.StockTakingAddPosItem>(CONTROLLER_NAME,
                nameof(StockTakingS4.StockTakingAddPosItem),
                model);

            return item;
        }

        public virtual async Task<StockTakingS4.StockTakingDeletePosItem> DeleteStockTakingItem(StockTakingS4.StockTakingDeletePosItem model)
        {
            var item = await base.PostAsync<StockTakingS4.StockTakingDeletePosItem>(CONTROLLER_NAME,
                nameof(StockTakingS4.StockTakingDeletePosItem),
                model);

            return item;
        }

        public virtual async Task<StockTakingS4.StockTakingBegin> StockTakingBegin(StockTakingS4.StockTakingBegin model)
        {
            var item = await base.PostAsync<StockTakingS4.StockTakingBegin>(CONTROLLER_NAME,
                nameof(StockTakingS4.StockTakingBegin),
                model);

            return item;
        }

        public virtual async Task<StockTakingS4.StockTakingPositionOK> RollbackStockTakingPositionItems(StockTakingS4.StockTakingPositionOK model)
        {
            var item = await base.PostAsync<StockTakingS4.StockTakingPositionOK>(CONTROLLER_NAME,
                nameof(StockTakingS4.StockTakingPositionOK),
                model);

            return item;
        }
    }
}

﻿using System;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

namespace S4mp.Client.Utils
{
	public class Ping : Interfaces.IPing
    {
		public const string MEDIA_TYPE_JSON = "application/json";

		public async Task<bool> PingWSAsync(string apiUrl) 
        {
			
            string url = apiUrl + "ping";
            bool result = false;

            try
			{
				WebRequest request = WebRequest.Create(url);
				request.Method = "HEAD";

				Task<WebResponse> task = Task.Factory.FromAsync(
					request.BeginGetResponse,
					asyncResult => request.EndGetResponse(asyncResult),
					(object)null);

				var response = await task;

				result = true;
			}
			catch
			{
			}

			return result;
        }

		public bool PingWSSync(string apiUrl)
		{
			Task<bool> task = Task.Run(async () => await PingWSAsync(apiUrl));
			task.Wait();
			return task.Result;
		}
	}
}

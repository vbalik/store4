﻿using System.Threading;

namespace S4mp.Client.Utils
{
    class CounterRequestId
    {
        protected static long _numCounter = 0;

        internal static long GetId()
        {
            return Interlocked.Increment(ref _numCounter);
        }
    }
}

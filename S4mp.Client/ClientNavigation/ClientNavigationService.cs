﻿using S4.Core;
using S4mp.Core;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace S4mp.Client.ClientNavigation
{
    public class ClientNavigationService : IClientNavigationService
    {
        public async Task PopToLogin(ILoginPage loginPage)
        {
            Singleton<BaseInfo>.Instance.Clear();
            
            var contentPage = (ContentPage)loginPage;
            contentPage.ToolbarItems.Clear();

            NavigationPage.SetHasNavigationBar(contentPage, false);

            Application.Current.MainPage = new NavigationPage(contentPage);

            await Application.Current.MainPage.Navigation.PopToRootAsync();
        } 
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace S4mp.Client.ClientNavigation
{
    public interface IClientNavigationService
    {
        Task PopToLogin(ILoginPage loginPage);
    }
}

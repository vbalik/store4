﻿using S4.Entities.Helpers;
using S4.ProcedureModels.Exped;
using S4mp.Client.Interfaces;
using S4.ProcedureModels.Receive;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace S4mp.Client
{
    public class DirectionClient : ClientBase, IDirectionClient
    {
        private const string CONTROLLER_NAME = "procedures";

        public virtual async Task<List<DocumentInfo>> ExpeditionListDirects()
        {
            var result = await base.PostAsync<ExpedListDirects>(CONTROLLER_NAME,
                nameof(ExpedListDirects),
                new ExpedListDirects());

            return result.Directions;
        }

        public virtual async Task<ExpedDone> ExpeditionDone(ExpedDone model)
        {
            var result = await base.PostAsync<ExpedDone>(CONTROLLER_NAME,
                nameof(ExpedDone),
                model);

            return result;
        }

        public virtual async Task<ReceivePrintByDirection> ReceivePrintByDirection(ReceivePrintByDirection model)
        {
            var result = await base.PostAsync<ReceivePrintByDirection>(CONTROLLER_NAME,
                nameof(ReceivePrintByDirection),
                model);

            return result;
        }
    }
}

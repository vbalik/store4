﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NPoco;

using S4.Core.Data;

namespace S4mp.Client
{
    public class CommonClient<T> : ClientBase where T : new()
    {
        public virtual async Task<IEnumerable<T>> Get()
        {
            return await base.GetAsync<IEnumerable<T>>(ControllerName);
        }

        public virtual async Task<T> Get(int id)
        {
            return await base.GetAsync<T>(ControllerName, id);
        }

        public async Task<IEnumerable<T>> GetByProperty(string fieldName, int id)
        {
            string path = string.Format("/getbyproperty?fieldname={0}&value={1}", fieldName, id);
            return await base.GetAsync<IEnumerable<T>>(ControllerName, path);
        }

        public virtual async Task<int> Insert(T poco)
        {
            return await base.PostAsync(ControllerName, poco);
        }

        public virtual async Task Update(T poco)
        {
            int id = (int)poco.GetValueByName(KeyName);
            await base.PutAsync(ControllerName, poco, id);
        }

        public virtual async Task Delete(int id)
        {
            await base.DeleteAsync(ControllerName, id);
        }

        internal string KeyName
        {
            get
            {
                return TableInfo.FromPoco(typeof(T)).PrimaryKey;
            }
        }

        internal string ControllerName
        {
            get
            {
                return typeof(T).Name;
            }
        }
    }
}

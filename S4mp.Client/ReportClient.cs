﻿using S4.Entities;
using S4mp.Client.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace S4mp.Client
{
    public class ReportClient : ClientBase, IReportClient
    {
        private const string CONTROLLER_NAME = "Report";

        public virtual async Task<List<PrinterLocation>> GetPrinterLocation(string reportType)
        {
            return await PostAsync<List<PrinterLocation>, string>(CONTROLLER_NAME,
                "getprinterlocation",
                reportType);
        }

        public virtual async Task<List<Reports>> GetReports(string reportType)
        {
            return await GetAsync<List<Reports>>(CONTROLLER_NAME, "/getreports?reportType=" + reportType);
        }
    }
}

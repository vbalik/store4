using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Internal;
using Moq;
using System;
using System.Threading.Tasks;
using Xunit;

namespace S4.Messages.Tests
{
    public class MessagesTests
    {
        internal class Msg1 : MessageBase
        {
            public int Val1 { get; set; }
        }


        internal class Msg2 : MessageBase
        {
            public string Val2 { get; set; }
        }


        [Fact]
        public void Subscribe()
        {
            var router = new MessageRouter();

            Assert.True(router.Subscribe<Msg1>("xxx", (msg) => { }));
            Assert.False(router.Subscribe<Msg1>("xxx", (msg) => { }));
            Assert.True(router.Subscribe<Msg1>("yyy", (msg) => { }));

            Assert.True(router.Subscribe<Msg2>("xxx", (msg) => { }));
        }


        [Fact]
        public void Publish1()
        {
            var router = new MessageRouter();
            var res = 0;

            router.Subscribe<Msg1>("xxx", (msg) => res += ((Msg1)msg).Val1);

            router.Publish(new Msg1() { Val1 = 1 });
            router.Publish(new Msg2());

            Task.Delay(100).Wait();
            Assert.Equal(1, res);
        }


        [Fact]
        public void Publish2()
        {
            var router = new MessageRouter();
            var res1 = 0;
            var res2x = 0;
            var res2y = 0;

            router.Subscribe<Msg1>("xxx", (msg) => res1++);
            router.Subscribe<Msg2>("xxx", (msg) => res2x++);
            router.Subscribe<Msg2>("yyy", (msg) => res2y++);

            router.Publish(new Msg1());
            router.Publish(new Msg2());

            Task.Delay(100).Wait();
            Assert.Equal(1, res1);
            Assert.Equal(1, res2x);
            Assert.Equal(1, res2x);
        }


        [Fact]
        public void Publish_Log()
        {
            var logger = new Mock<ILogger<MessageRouter>>();
            logger.Setup(m => m.Log(It.IsAny<LogLevel>(), It.IsAny<EventId>(), It.IsAny<FormattedLogValues>(), It.IsAny<Exception>(), It.IsAny<Func<object, Exception, string>>()))
                .Callback((LogLevel logLevel, EventId eventId, object state, Exception exception, Func<object, Exception, string> formatter) =>
                {
                    Assert.Equal(LogLevel.Trace, logLevel);
                    var st = (Microsoft.Extensions.Logging.Internal.FormattedLogValues)state;
                    Assert.StartsWith("New message S4.Messages.Tests.MessagesTests+Msg1; id: ", (string)st[0].Value);
                })
                .Verifiable();

            var router = new MessageRouter(logger.Object);
            router.Publish(new Msg1() { Val1 = 1 });
        }


        [Fact]
        public void Publish_Exception()
        {
            var logger = new Mock<ILogger<MessageRouter>>();
            logger.Setup(m => m.Log(LogLevel.Error, It.IsAny<EventId>(), It.IsAny<FormattedLogValues>(), It.IsAny<Exception>(), It.IsAny<Func<object, Exception, string>>()))
                .Callback((LogLevel logLevel, EventId eventId, object state, Exception exception, Func<object, Exception, string> formatter) =>
                {
                    Assert.Equal(LogLevel.Error, logLevel);
                    var st = (Microsoft.Extensions.Logging.Internal.FormattedLogValues)state;
                    Assert.Equal("Publish error; msgType: S4.Messages.Tests.MessagesTests+Msg1", st[0].Value);
                })
                .Verifiable();

            var router = new MessageRouter(logger.Object);

            router.Subscribe<Msg1>("xxx", (msg) => throw new Exception());

            router.Publish(new Msg1() { Val1 = 1 });
        }
    }
}

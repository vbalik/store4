﻿using OfficeOpenXml;
using OfficeOpenXml.Table;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;

namespace S4.ToExcel
{
    public static class Helper
    {

        public static byte[] DataTableToExcel(DataTable dataTable, string worksheetName = "Report")
        {
            using (var package = new ExcelPackage())
            {
                package.Workbook.Properties.Title = "S4 Report";
                package.Workbook.Properties.Author = "Promedica Praha Group a.s.";

                var worksheet = package.Workbook.Worksheets.Add(worksheetName);
                worksheet.Cells["A1"].LoadFromDataTable(dataTable, PrintHeaders: true);
                                
                var headerCells = worksheet.Cells[1, 1, 1, dataTable.Columns.Count];
                var headerFont = headerCells.Style.Font;
                headerFont.Bold = true;
                for (var col = 1; col < dataTable.Columns.Count + 1; col++)
                {
                    //header bold
                    worksheet.Cells[1, col].Style.Font = headerFont;
                    //AutoFit
                    worksheet.Column(col).AutoFit();
                                        
                }

                //Columns format
                for (int column = 0; column < dataTable.Columns.Count; column++)
                {
                    //set DateTime format
                    if (dataTable.Columns[column].DataType == typeof(System.DateTime))
                        for (int row = 2; row <= dataTable.Rows.Count + 1; row++)
                            worksheet.Cells[row, column + 1].Style.Numberformat.Format = System.Globalization.DateTimeFormatInfo.CurrentInfo.ShortDatePattern;


                }
                
                return package.GetAsByteArray();
            }

        }
    }
}

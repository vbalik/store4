﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S4.Core.Data
{
    public static class StringExtensions
    {
        /// <summary>
        /// Contains with StringComparison option.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="toCheck">To check.</param>
        /// <param name="comp">The comp.</param>
        /// <returns></returns>
        public static bool Contains(this string source, string toCheck, StringComparison comp)
        {
            return (source ?? string.Empty).IndexOf(toCheck, comp) >= 0;
        }



        /// <summary>
        /// Returns left part of source.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="length">The part length.</param>
        /// <returns></returns>
        public static string Left(this string source, int length)
        {
            return (source ?? string.Empty).Substring(0, Math.Min(length, source?.Length ?? 0));
        }


        /// <summary>
        /// Gets the cutted string.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="lenght">The lenght.</param>
        /// <returns></returns>
        public static string GetCuttedString(this string source, int lenght)
        {
            if ((source ?? string.Empty).Length <= lenght)
                return source;

            return (source ?? string.Empty).Substring(0, lenght - 4) + " ...";
        }

        /// <summary>
        /// Splits the string into parts of specified length, separated by \r\n
        /// </summary>
        /// <param name="source">the string</param>
        /// <param name="lenght">length of parts</param>
        /// <returns>splitted string</returns>
        public static string GetSplitedString(this string source, int lenght)
        {
            if ((source ?? string.Empty).Length <= lenght)
                return source;

            string result = string.Empty;
            int i = 0;
            while (i <= source.Length - lenght)
            {
                result += $"{(source ?? string.Empty).Substring(i, lenght)}\r\n";
                i += lenght;
            }
            result += (source ?? string.Empty).Substring(i);

            return result;
        }
    }
}

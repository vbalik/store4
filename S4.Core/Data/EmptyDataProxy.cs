﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Core.Data
{
    public class EmptyDataProxy
    {
        public void ToSave<T>(T value, T emptyConstant, Action<T> setOutput)
        {
            if (value == null || value.Equals(default(T)))
                setOutput(emptyConstant);
        }


        public void ToShow<T>(T value, T emptyConstant, Action<T> setOutput)
        {
            if (value == null || value.Equals(emptyConstant))
                setOutput(default(T));
        }
    }
}

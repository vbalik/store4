﻿using System;



namespace S4.Core.Data
{
    public class FieldError
    {
        public enum ErrorReasonEnum
        {
            IsNull,
            TooLong,
            TooShort
        }

        public string FieldName { get; set; }
        public ErrorReasonEnum ErrorReason { get; set; }
        public string ErrorText { get; set; }
    }

}

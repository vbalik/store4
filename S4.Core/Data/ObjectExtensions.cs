﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Collections;
using System.Collections.Concurrent;
using System.Linq.Expressions;


namespace S4.Core.Data
{
	public static class ObjectExtensions
	{
		/// <summary>
		/// Gets the value of the property by property name.
		/// </summary>
		/// <param name="source">The source object.</param>
		/// <param name="propertyName">Name of the property.</param>
		/// <returns></returns>
		public static object GetValueByName(this object source, string propertyName, bool caseSensitive = false)
        {
            if (caseSensitive)
                return source.GetType().GetRuntimeProperty(propertyName).GetValue(source, null);
            else
                return source.GetType().GetRuntimeProperties().Where(p => p.Name.ToUpper() == propertyName.ToUpper()).First().GetValue(source, null);
        }
    }
}

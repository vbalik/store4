﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S4.Core.Data
{
    public class DataException : Exception
    {
        private static object _lock = new object();
        private FieldError[] _fieldErrors = null;
        


        public DataException(string message) : base(message) { }

        public DataException(string message, params object[] args)
            : base(String.Format(message, args))
        { }

        public DataException(string message, FieldError[] fieldErrors)
            : base(MessageFromFieldErrors(message, fieldErrors))
        {
            _fieldErrors = fieldErrors;
        }



        private static string MessageFromFieldErrors(string message, Core.Data.FieldError[] fieldErrors)
        {
            // lock, because of multithreading
            lock (_lock)
            {
                var sb = new StringBuilder();
                sb.Append(message);
                foreach(var fe in fieldErrors)
                {
                    sb.AppendLine();
                    sb.AppendFormat("Field: {0} - {1}", fe.FieldName, fe.ErrorReason);
                }
                return sb.ToString();
            }
        }
    }
}

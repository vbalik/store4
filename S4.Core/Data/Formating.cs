﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace S4.Core.Data
{
    public class Formating
    {
        public static readonly string DateTimeFormat = "d/M/yyyy";
        public static readonly string DoubleFormat = "0.00";
        public static readonly string ExpirationFormat = "dd.MM.yy";

        public static readonly string[] BooleanTrueString = new string[] { "Ano", "A", "ano", "a", "1", "T", "True" };
        public static readonly string[] BooleanFalseString = new string[] { "Ne", "N", "ne", "n", "0", "F", "False" };

        public static readonly string[] ShortDayNames = new string[] { "Ne", "Po", "Út", "St", "Čt", "Pá", "So" };
        public static readonly string[] DayNames = new string[] { "Neděle", "Pondělí", "Úterý", "Středa", "Čtvrtek", "Pátek", "Sobota" };
        public static readonly string[] ShortMonthNames = new string[] { "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII" };
        public static readonly string[] MonthNames = new string[] { "Leden", "Únor", "Březen", "Duben", "Květen", "Červen", "Červenec", "Srpen", "Září", "Říjen", "Listopad", "Prosinec" };

        public static readonly Dictionary<string, Type> TypeCache = new Dictionary<string, Type>((int)Core.Data.PortableStringComparison.InvariantCultureIgnoreCase)
        {
            { "System.String", typeof(System.String) },
            { "System.Byte", typeof(System.Byte) },
            { "System.Int32", typeof(System.Int32) },
            { "System.Double", typeof(System.Double) },
            { "System.Boolean", typeof(System.Boolean) },
            { "System.DateTime", typeof(System.DateTime) }
        };

        public static byte CzechDayToSystem(byte day)
        {
            if (day == 7)
                return 0;
            return day;
        }

        public static byte SystemDayToCzech(byte day)
        {
            if (day == 0)
                return 7;
            return day;
        }

        /// <summary>
        /// Formats the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static string Format(object value)
        {
            if (value == null || value == DBNull.Value)
                return null;
            else if (value is System.DateTime)
                return ((System.DateTime)value).ToString(DateTimeFormat);
            else if (value is System.Double)
                return ((System.Double)value).ToString(DoubleFormat);
            else if (value is System.Boolean)
                return ((System.Boolean)value) ? BooleanTrueString[0] : BooleanFalseString[0];
            else
                return value.ToString();
        }



        /// <summary>
        /// Parses the specified string value.
        /// </summary>
        /// <param name="strValue">The string value.</param>
        /// <param name="typeName">Name of the type.</param>
        /// <returns></returns>
        /// <exception cref="System.NotSupportedException"></exception>
        public static object Parse(string strValue, string typeName)
        {
            Type type;
            if (TypeCache.TryGetValue(typeName, out type))
                return Parse(strValue, type);
            else
                throw new NotSupportedException(typeName);
        }



        /// <summary>
        /// Parses the specified string value.
        /// </summary>
        /// <param name="strValue">The string value.</param>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">strValue</exception>
        /// <exception cref="System.NotSupportedException"></exception>
        public static object Parse(string strValue, Type type)
        {
            if (strValue == null)
                throw new ArgumentNullException("strValue");

            else if (type == typeof(System.String))
                return strValue;

            else if (type == typeof(System.Byte))
            {
                byte result;
                if (byte.TryParse(strValue, out result))
                    return result;
                else
                    return null;
            }
            else if (type == typeof(System.Int32))
            {
                Int32 result;
                if (Int32.TryParse(strValue, out result))
                    return result;
                else
                    return null;
            }
            else if (type == typeof(System.Double))
            {
                double result;
                if (double.TryParse(strValue, out result))
                    return result;
                else
                    return null;
            }
            else if (type == typeof(System.Boolean))
                return ParseBoolean(strValue);

            else if (type == typeof(System.DateTime))
                return ParseDateTime(strValue);

            else
                throw new NotSupportedException(type.ToString());
        }


        /// <summary>
        /// Parses FieldsCheck Errors
        /// </summary>
        /// <param name="errors"></param>
        /// <returns></returns>
        public static string ParseError(List<FieldError> errors)
        {
            return errors.Select(x => x.FieldName + ' ' + x.ErrorReason.ToString() + ' ' + x.ErrorText).ToList().Aggregate((a, b) => a + ", " + b);
        }

        /// <summary>
        /// Converts DateTime to Expiration string
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string DateTimeToExpStr(DateTime? value)
        {
            return value?.ToString(ExpirationFormat);
        }

        public static DateTime ExpStrToDateTime(string value)
        {
            return DateTime.ParseExact(value, ExpirationFormat, System.Globalization.CultureInfo.InvariantCulture);
        }

        private static object ParseBoolean(string strValue)
        {
            if (BooleanTrueString.Contains(strValue))
                return true;
            else if (BooleanFalseString.Contains(strValue))
                return false;
            else
                return null;
        }


        private static object ParseDateTime(string strValue)
        {
            DateTime result;
            if (DateTime.TryParse(strValue, out result))
                return result;
            else
                return null;
        }


    }
}

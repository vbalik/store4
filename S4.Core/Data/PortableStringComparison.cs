﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Core.Data
{
    public class PortableStringComparison
    {
        public static StringComparison CurrentCulture
        {
            get { return StringComparison.CurrentCulture; }
        }

        public static StringComparison CurrentCultureIgnoreCase
        {
            get { return StringComparison.CurrentCultureIgnoreCase; }
        }

        public static StringComparison InvariantCulture
        {
            get { return StringComparison.InvariantCulture; }
        }

        public static StringComparison InvariantCultureIgnoreCase
        {
            get { return StringComparison.InvariantCultureIgnoreCase; }
        }

        public static StringComparison Ordinal
        {
            get { return StringComparison.Ordinal; }
        }

        public static StringComparison OrdinalIgnoreCase
        {
            get { return StringComparison.OrdinalIgnoreCase; }
        }
    }
}

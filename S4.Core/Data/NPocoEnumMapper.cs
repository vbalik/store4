﻿using NPoco;
using System;
using System.Reflection;

namespace S4.Core.Data
{
    // NPoco mapper for enums stored as tinyint
    public class NPocoEnumMapper : DefaultMapper
    {
        public override Func<object, object> GetFromDbConverter(Type DestType, Type SourceType)
        {
            if (DestType.IsEnum && SourceType == typeof(byte))
                return x => Enum.ToObject(DestType, (byte)x);
            return base.GetFromDbConverter(DestType, SourceType);
        }
    }
}

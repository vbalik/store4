﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace S4.Core.Data
{
    public class ExtraDataProxy
    {
        public const char SEPARATOR = '|';
        public const string SEPARATOR_STRING = "|";

        /// <summary>
        /// Gets value from the string representation.
        /// </summary>
        /// <param name="strValue">The string value.</param>
        /// <param name="valueType">Type of the value.</param>
        /// <returns></returns>
        public object FromString(string strValue, string valueType)
        {
            Type type = Formating.TypeCache[valueType];
            return FromString(strValue, type);

        }


        /// <summary>
        /// Gets value from the string representation.
        /// </summary>
        /// <param name="strValue">The string value.</param>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public object FromString(string strValue, Type type)
        {
            return Convert.ChangeType(strValue, type, System.Globalization.CultureInfo.InvariantCulture);
        }


        /// <summary>
        /// Gets array of values from the string representation.
        /// </summary>
        /// <param name="strValue">The string value.</param>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public object[] FromString_Array(string strValue, Type type)
        {
            string[] items = strValue.Split(SEPARATOR);
            object[] result = new object[items.Length];

            // do conversion af array
            for (int pos = 0; pos < items.Length; pos++)
            {
                result[pos] = FromString(items[pos], type);
            }

            return result;
        }


        /// <summary>
        /// Returns a <see cref="System.String" /> that represents the value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="valueType">Type of the value.</param>
        /// <returns>
        /// System.String
        /// </returns>
        public string ToString(object value, string valueType)
        {
            Type type = Formating.TypeCache[valueType];
            return ToString(value, type);
        }


        /// <summary>
        /// Returns a <see cref="System.String" /> that represents the value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="type">The type.</param>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public string ToString(object value, Type type)
        {
            if (type == typeof(System.DateTime))
                return ((DateTime)value).ToString("s");
            else
                return Convert.ToString(value, System.Globalization.CultureInfo.InvariantCulture);
        }


        /// <summary>
        /// Returns a <see cref="System.String" /> that represents the array of values.
        /// </summary>
        /// <param name="values">The values.</param>
        /// <param name="valueType">Type of the value.</param>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public string ToString(IEnumerable values, string valueType)
        {
            Type type = Formating.TypeCache[valueType];
            return ToString(values, type);
        }


        /// <summary>
        /// Returns a <see cref="System.String" /> that represents the array of values.
        /// </summary>
        /// <param name="values">The values.</param>
        /// <param name="valueType">Type of the value.</param>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public string ToString(IEnumerable values, Type type)
        {
            var sb = new StringBuilder();
            var enumerator = values.GetEnumerator();
            bool first = true;
            while (enumerator.MoveNext())
            {
                if (first)
                    first = false;
                else
                    sb.Append(SEPARATOR);

                string strValue = ToString(enumerator.Current, type);
                sb.Append(strValue);
            }

            return sb.ToString();
        }

        /// <summary>
        /// tries to parse string value to decimal using czech and invariant culture info
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public decimal? ParseDecimal(string source)
        {
            if (string.IsNullOrEmpty(source))
                return null;

            var provider = System.Globalization.CultureInfo.GetCultureInfo("cs-CZ");
            if (decimal.TryParse(source, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.GetCultureInfo("cs-CZ"), out decimal result))
                return result;

            return decimal.Parse(source, System.Globalization.CultureInfo.InvariantCulture);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Core.Data
{
    public static class ConnectionHelper
    {
        private static string _connectionString = null;
        private static object _lock = new object();

        public static string ConnectionString
        {
            get => _connectionString;
            set
            {
                if (String.IsNullOrWhiteSpace(value))
                    throw new ArgumentNullException("ConnectionString");
                _connectionString = value;
            }
        }


        public static NPoco.Database GetDB()
        {
            lock (_lock)
            {
                var db = new NPoco.Database(ConnectionString, NPoco.DatabaseType.SqlServer2012, System.Data.SqlClient.SqlClientFactory.Instance);
                db.Mappers.Add(new NPocoEnumMapper());
                return db;
            }
        }
    }
}

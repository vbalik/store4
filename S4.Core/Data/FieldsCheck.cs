﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Reflection;



namespace S4.Core.Data
{
    public class FieldsCheck
    {
        private static Type requiredAttributeType = typeof(System.ComponentModel.DataAnnotations.RequiredAttribute);
        private static Type computedAttributeType = typeof(NPoco.ComputedColumnAttribute);
        private static Type minLengthAttributeType = typeof(System.ComponentModel.DataAnnotations.MinLengthAttribute);
        private static Type maxLengthAttributeType = typeof(System.ComponentModel.DataAnnotations.MaxLengthAttribute);


        private List<FieldError> _errors = new List<FieldError>();


        /// <summary>
        /// Does the check of object - by attributes from System.ComponentModel.DataAnnotations;.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        /// <exception cref="System.InvalidOperationException">Not valid attribute for the property</exception>
        public bool DoCheck(object obj)
        {
            if (obj == null)
                throw new ArgumentNullException("obj");

            Type objType = obj.GetType();
            var properties = objType.GetProperties();
            return DoCheck(obj, properties);
        }


        public bool DoCheck(object obj, PropertyInfo[] properties)
        {
            if (obj == null)
                throw new ArgumentNullException("obj");

            bool result = true;
            foreach (var prop in properties)
                DoCheck(obj, prop, false, ref result);

            return result;
        }


        public int GetMaxLengthAttribute(PropertyInfo prop)
        {
            var maxLengthAttribute = prop.GetCustomAttribute(maxLengthAttributeType);
            if (maxLengthAttribute != null)
                return ((MaxLengthAttribute) maxLengthAttribute).Length;
            else
                return 0;
        }

        public void DoCheck(object obj, PropertyInfo prop, bool canBeZero, ref bool result)
        {
            // check if is primary key
            var primaryKeyAttr = obj.GetType().GetCustomAttribute(typeof(NPoco.PrimaryKeyAttribute));
            if (primaryKeyAttr != null)
            {
                // if prop is PK and AutoIncrement then can be zero - is insert
                var columnAttr = prop.GetCustomAttribute(typeof(NPoco.ColumnAttribute));
                if (columnAttr != null &&
                    (columnAttr as NPoco.ColumnAttribute).Name?.ToLowerInvariant() == (primaryKeyAttr as NPoco.PrimaryKeyAttribute)?.Value.ToLowerInvariant() &&
                    (primaryKeyAttr as NPoco.PrimaryKeyAttribute).AutoIncrement)
                    canBeZero = true;
            }
            // RequiredAttribute
            if (prop.GetCustomAttribute(requiredAttributeType) != null && prop.GetCustomAttribute(computedAttributeType) == null)
            {
                if (!CheckNotNull(obj, prop, canBeZero))
                    result = false;
            }

            // length
            int minLen = 0, maxLen = 0;
            // MinLengthAttribute
            var minLengthAttribute = prop.GetCustomAttribute(minLengthAttributeType);
            if (minLengthAttribute != null)
                minLen = ((MinLengthAttribute)minLengthAttribute).Length;
            // MaxLengthAttribute
            var maxLengthAttribute = prop.GetCustomAttribute(maxLengthAttributeType);
            if (maxLengthAttribute != null)
                maxLen = ((MaxLengthAttribute)maxLengthAttribute).Length;
            if (minLen > 0 || maxLen > 0)
            {
                if (prop.PropertyType != typeof(string))
                    throw new InvalidOperationException("Not valid attribute for the property");

                if (!CheckLength(obj, prop, minLen, maxLen))
                    result = false;
            }
        }


        public List<FieldError> Errors
        {
            get { return _errors; }
        }


        internal FieldError ErrorByFieldName(string fieldName)
        {
            foreach (var item in _errors)
                if (String.Compare(fieldName, item.FieldName, true) == 0)
                    return item;
            return null;
        }


        private bool CheckNotNull(object obj, PropertyInfo prop, bool canBeZero)
        {
            if (!canBeZero && prop.PropertyType == typeof(int))
            {
                int iValue = (int)prop.GetValue(obj);
                if (iValue == 0)
                {
                    _errors.Add(new FieldError()
                    {
                        FieldName = prop.Name,
                        ErrorReason = FieldError.ErrorReasonEnum.IsNull
                    });

                    return false;
                }
            }

            object value = prop.GetValue(obj);
            if (value == null)
            {
                _errors.Add(new FieldError()
                {
                    FieldName = prop.Name,
                    ErrorReason = FieldError.ErrorReasonEnum.IsNull
                });

                return false;
            }
            else
            {
                //--kontrola na Empty string
                if (prop.PropertyType.Name.ToLower() == "string")
                {
                    if (string.IsNullOrEmpty(value.ToString().Trim()))
                    {
                        _errors.Add(new FieldError()
                        {
                            FieldName = prop.Name,
                            ErrorReason = FieldError.ErrorReasonEnum.IsNull
                        });

                        return false;
                    }
                }
                return true;
            }
        }


        private bool CheckLength(object obj, PropertyInfo prop, int minLen, int maxLen)
        {
            object value = prop.GetValue(obj);
            if (value == null)
            {
                return true;
            }

            int len = ((string)value).Length;
            if (maxLen > 0 && len > maxLen)
            {
                _errors.Add(new FieldError()
                {
                    FieldName = prop.Name,
                    ErrorReason = FieldError.ErrorReasonEnum.TooLong
                });

                return false;
            }

            if (minLen > 0 && len < minLen)
            {
                _errors.Add(new FieldError()
                {
                    FieldName = prop.Name,
                    ErrorReason = FieldError.ErrorReasonEnum.TooShort
                });

                return false;
            }

            return true;
        }



    }
}

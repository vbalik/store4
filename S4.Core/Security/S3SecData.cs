﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace S4.Core.Security
{
    public class S3SecData
    {
        private const int SALT_LEN = 16;


        public static bool CheckPassword(string workerSecData, string userID, string password)
        {
            if (userID.Length < 8)
                userID = userID + new string(' ', 8 - userID.Length);

            byte[] hash = GetSaltedHash(password, userID);
            byte[] secDataBytes = ToBytes(workerSecData);
            return CompareByteArray(hash, secDataBytes, 0, hash.Length);
        }


        public static string MakeSecData(string userID, string password)
        {
            if (userID.Length < 8)
                userID = userID + new string(' ', 8 - userID.Length);

            byte[] hash = GetSaltedHash(password, userID);
            return BitConverter.ToString(hash).Replace("-", "");
        }


        private static byte[] ToBytes(string hex)
        {

            int numberChars = hex.Length;
            byte[] bytes = new byte[numberChars / 2];
            for (int i = 0; i < numberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }


        private static byte[] GetSaltedHash(string cleanString, string saltString)
        {
            Byte[] clearBytes = new UnicodeEncoding().GetBytes(cleanString);

            // Create a salt value.
            byte[] saltValue = new byte[SALT_LEN];
            UnicodeEncoding unicode = new UnicodeEncoding();
            byte[] salt = unicode.GetBytes(saltString);
            int len = Math.Min(salt.Length, SALT_LEN);
            Array.Copy(salt, saltValue, len);

            // Add salt
            byte[] rawSalted = new byte[clearBytes.Length + saltValue.Length];
            clearBytes.CopyTo(rawSalted, 0);
            saltValue.CopyTo(rawSalted, clearBytes.Length);

            //Create the salted hash.         
            SHA1 sha1 = SHA1.Create();
            byte[] hash = sha1.ComputeHash(rawSalted);

            return hash;
        }


        private static bool CompareByteArray(byte[] array1, byte[] array2, int start, int stop)
        {
            for (int i = start; i < stop; i++)
            {
                if (array1[i] != array2[i])
                    return false;
            }
            return true;
        }
    }
}

﻿using S4.Core.EAN;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace S4.Core.HIBCC
{
    public class HIBCC
    {
        /// <summary>
        /// Parsed parts from code
        /// </summary>
        public List<HibccPart> Parts { get; set; } = new List<HibccPart>();

        /// <summary>
        /// Ordered identifiers because we want match $$ before $
        /// </summary>
        public static List<HibccIdentifier> EANIdentifiers { get; set; }

        /// <summary>
        /// List of available identifiers
        /// </summary>
        private List<HibccIdentifier> EANIdentifierList { get; }
            = new List<HibccIdentifier> {
                new HibccIdentifier("$", "Lot Number Only", null, IdentifierEnum.Lot, 19, 18, IdentifierEnum.None, 0, 0),
                new HibccIdentifier("$$7", "Lot Number Only (Alternate Option)", null, IdentifierEnum.Lot, 21, 18, IdentifierEnum.None, 0, 0),

                new HibccIdentifier("$$", "ExpirationDate Date (MMYY) followed by Lot Number", "MMyy", IdentifierEnum.ExpirationDate, 6, 4, IdentifierEnum.Lot, 18, 18),
                new HibccIdentifier("$$2", "ExpirationDate Date (MMDDYY) followed by Lot Number", "MMddyy", IdentifierEnum.ExpirationDate, 9, 6, IdentifierEnum.Lot, 18, 18),
                new HibccIdentifier("$$3", "ExpirationDate Date (YYMMDD) followed by Lot Number", "yyMMdd", IdentifierEnum.ExpirationDate, 9, 6, IdentifierEnum.Lot, 18, 18),
                new HibccIdentifier("$$4", "ExpirationDate Date (YYMMDDHH) followed by Lot Number", "yyMMddHH", IdentifierEnum.ExpirationDate, 11, 8, IdentifierEnum.Lot, 18, 18),

                new HibccIdentifier("$+", "Serial Number only", null, IdentifierEnum.SerialNumber, 20, 18, IdentifierEnum.None, 0, 0),
                new HibccIdentifier("$$+7", "Serial Number only", null, IdentifierEnum.SerialNumber, 22, 18, IdentifierEnum.None, 0, 0),
                new HibccIdentifier("$$+", "ExpirationDate Date (MMYY) followed by Serial Number", "MMyy", IdentifierEnum.ExpirationDate, 7, 4, IdentifierEnum.SerialNumber, 18, 18),
                new HibccIdentifier("$$+2", "ExpirationDate Date (MMDDYY) followed by Serial Number", "MMddyy", IdentifierEnum.ExpirationDate, 10, 6, IdentifierEnum.SerialNumber, 18, 18),
                new HibccIdentifier("$$+3", "ExpirationDate Date (YYMMDD) followed by Serial Number", "yyMMdd", IdentifierEnum.ExpirationDate, 10, 6, IdentifierEnum.SerialNumber, 18, 18),

                new HibccIdentifier("$$+4", "ExpirationDate Date (YYMMDDHH) followed by Serial Number", "yyMMddHH", IdentifierEnum.ExpirationDate, 12, 8, IdentifierEnum.SerialNumber, 18, 18),
                new HibccIdentifier("$$+5", "ExpirationDate Date (YYMMDD) followed by Serial Number", "yyMMdd", IdentifierEnum.ExpirationDate, 9, 5, IdentifierEnum.SerialNumber, 18, 18),
                new HibccIdentifier("$$+6", "ExpirationDate Date (YYJJJ) followed by Serial Number", "yyJJJ", IdentifierEnum.ExpirationDate, 10, 6, IdentifierEnum.SerialNumber, 18, 18),

                // omit (/) because parts are splitted by slash
                new HibccIdentifier("S", "Supplemental Serial Number, where lot number also required and included in main secondary data string", "YYJJJ",
                    IdentifierEnum.SerialNumber, 21, 18, IdentifierEnum.None, 0, 0),
                new HibccIdentifier("16D", "Manufacturing Date (YYYYMMDD) (supplemental to secondary barcode)", "yyyyMMdd", IdentifierEnum.ManufacturingDate, 11, 8, IdentifierEnum.None, 0, 0),
                new HibccIdentifier("14D", "ExpirationDate Date (YYYYMMDD) (supplemental to secondary barcode)", "yyyyMMdd", IdentifierEnum.ExpirationDate, 11, 8, IdentifierEnum.None, 0, 0),
                new HibccIdentifier("Q", "Quantity Identifier", null, IdentifierEnum.Quantity, 8, 7, IdentifierEnum.None, 0, 0),
            };

        public HIBCC(string rawCode)
        {
            if (string.IsNullOrWhiteSpace(rawCode) || !rawCode.StartsWith("+"))
                throw new ArgumentNullException($"Code {rawCode} is empty");

            // cache
            if (EANIdentifiers == null)
                EANIdentifiers = EANIdentifierList.OrderByDescending(_ => _.ReadableSize - _.Size).ToList();

            try
            {
                if (!rawCode.Contains("/"))
                {
                    _ParseSinglePartCode(rawCode);
                }
                else
                {
                    _ParseMultiplePartCode(rawCode);
                }
            }
            catch (ArgumentNullException ex)
            {
                throw new ArgumentException(ex.Message);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        private HibccIdentifier _FindIdentifier(string data)
        {
            // omit +
            if (data.StartsWith("+"))
                data = data.Substring(1);

            foreach (var identifier in EANIdentifiers)
            {
                if (data.StartsWith(identifier.Qualifier))
                    return identifier;
            }

            return null;
        }

        private void _ParseSinglePartCode(string rawCode)
        {
            var identifier = _FindIdentifier(rawCode);
            if (identifier == null)
            {
                _ParsePrimaryData(rawCode);
            }
            else
            {
                rawCode = rawCode.Substring(1); // omit +
                _ParseSecondaryData(rawCode, true, true);
            }
        }

        private void _ParseMultiplePartCode(string rawCode)
        {
            var codeParts = rawCode.Split('/');

            _ParsePrimaryData(codeParts[0], false);

            var cnt = codeParts.Count();
            for (int i = 1; i < cnt; i++)
            {
                _ParseSecondaryData(codeParts[i], (cnt - 1) == i);
            }
        }

        private (string LIC, string PCN, string UM, string CheckDigit) _ParsePrimaryData(string data, bool hasCheckDigit = true)
        {
            if (data.Length > 25)
                throw new ArgumentException($"Code is too long {data.Length}");

            var startIdentifier = data.Substring(0, 1);
            var LIC = data.Substring(1, 4);

            var pcnLength = data.Length - 5; // remove identifier (1) + LIC (4)
            pcnLength -= 1; // remove UM
            if (hasCheckDigit) pcnLength -= 1; // remove check digit
            pcnLength = pcnLength < 18 ? pcnLength : 18;
            var PCN = data.Substring(5, pcnLength);
            Parts.Add(new HibccPart
            {
                EANCode = IdentifierEnum.ProductNumber,
                Value = PCN
            });

            var UM = data.Substring(data.Length - (hasCheckDigit ? 2 : 1), 1);
            Parts.Add(new HibccPart
            {
                EANCode = IdentifierEnum.UnitOfMeasure,
                Value = UM
            });

            var checkDigit = "";
            if (hasCheckDigit)
                checkDigit = data.Substring(data.Length - 1);

            return (LIC, PCN, UM, checkDigit);
        }

        private (string data1, string data2, string linkChar, string checkDigit) _ParseSecondaryData(string data, bool hasCheckDigit = false, bool hasLinkChar = false)
        {
            var identifier = _FindIdentifier(data);

            if (identifier == null)
                return (null, null, null, null);

            var maxLength = identifier.ReadableSize + identifier.FollowedReadableSize;
            if (hasCheckDigit) maxLength += 1;
            if (hasLinkChar) maxLength += 1;

            if (data.Length > maxLength)
                throw new ArgumentNullException($"Code part {data} has not valid size");

            var readableSize = (data.Length - identifier.StartIndex) < identifier.Size ? data.Length - identifier.StartIndex : identifier.Size; // fixed less length
            var data1 = data.Substring(identifier.StartIndex, readableSize);

            if(data1.EndsWith("$")) data1 = data1.Substring(0, data1.Length - 1);

            if (identifier.Part1Type == IdentifierEnum.ExpirationDate || identifier.Part1Type == IdentifierEnum.ManufacturingDate)
            {
                if (!DateTime.TryParseExact(data1, identifier.DateFormat, null, DateTimeStyles.None, out var date))
                    throw new ArgumentException($"Date {data1} can not be parsed as DateTime");

                // correct last day if not specified
                if (!identifier.DateFormat.Contains("dd") && !identifier.DateFormat.Contains("JJ"))
                    date = new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));

                Parts.Add(new HibccPart
                {
                    EANCode = identifier.Part1Type,
                    Value = data1,
                    ValueDate = date
                });
            }
            else
            {
                Parts.Add(new HibccPart
                {
                    EANCode = identifier.Part1Type,
                    Value = data1,
                });
            }

            string data2 = null;
            if (identifier.HasFollow)
            {
                var followedReadableSize = (data.Length - identifier.ReadableSize) < identifier.FollowedReadableSize ? data.Length - identifier.ReadableSize : identifier.FollowedReadableSize; // fixed less length
                if (hasLinkChar) followedReadableSize--; // remove link char
                if (hasCheckDigit) followedReadableSize--; // remove check digit

                data2 = data.Substring(identifier.ReadableSize, followedReadableSize);
                Parts.Add(new HibccPart
                {
                    EANCode = identifier.FollowedPart2Type,
                    Value = data2
                });
            }

            string linkChar = "";
            if (hasLinkChar)
                linkChar = data.Substring(data.Length - 2, 1);

            string checkDigit = "";
            if (hasCheckDigit)
                checkDigit = data.Substring(data.Length - 1);

            return (data1, data2, linkChar, checkDigit);
        }
    }
}
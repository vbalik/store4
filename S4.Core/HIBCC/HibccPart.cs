﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Core.HIBCC
{
    public class HibccPart
    {
        public IdentifierEnum EANCode { get; set; }
        public string Value { get; set; }
        public DateTime? ValueDate { get; set; }
    }
}

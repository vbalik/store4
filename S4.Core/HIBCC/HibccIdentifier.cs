﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Core.HIBCC
{
    public class HibccIdentifier
    {
        // basic
        public string Qualifier { get; set; }
        public string Description { get; set; }
        public string DateFormat { get; set; }

        // part 1
        public IdentifierEnum Part1Type { get; set; }
        public int ReadableSize { get; set; }
        public int Size { get; set; }

        // part 2
        public IdentifierEnum FollowedPart2Type { get; set; }
        public int FollowedReadableSize { get; set; }
        public int FollowedSize { get; set; }


        public int StartIndex => ReadableSize - Size;

        public bool HasFollow => FollowedReadableSize > 0 && FollowedPart2Type != IdentifierEnum.None;

        public HibccIdentifier(
            string qualifier,
            string identifierDesc,
            string dateFormat,

            IdentifierEnum part1Type,
            int readableSize,
            int size,

            IdentifierEnum followedPart2,
            int followedReadableSize,
            int fillowedSize)
        {
            Qualifier = qualifier;
            Description = identifierDesc;
            DateFormat = dateFormat;

            Part1Type = part1Type;
            ReadableSize = readableSize;
            Size = size;

            FollowedPart2Type = followedPart2;
            FollowedReadableSize = followedReadableSize;
            FollowedSize = fillowedSize;
        }
    }
}
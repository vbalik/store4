﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Core.HIBCC
{
    public enum IdentifierEnum
    {
        None = 0,
        Lot = 1,
        ExpirationDate = 2,
        SerialNumber = 3,
        ManufacturingDate = 4,
        ProductNumber = 5,
        UnitOfMeasure = 6,
        Quantity = 7
    }
}

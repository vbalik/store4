﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Core.NLoging
{
    public class NLogSettingTargets
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string FileName { get; set; }
        public string Layout { get; set; }
    }
}

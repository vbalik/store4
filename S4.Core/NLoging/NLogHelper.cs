﻿using System.Collections.Generic;
using System.Linq;
using NLog;
using NLog.Targets;


namespace S4.Core.NLoging
{
    public static class NLogHelper
    {
        public static NLog.Config.LoggingConfiguration NLogGetConfig(List<NLogSettingRules> rules, List<NLogSettingTargets> targets)
        {
            //Nlog
            var config = new NLog.Config.LoggingConfiguration();

            //add rule to Nlog
            foreach (var rule in rules)
            {
                var levels = rule.Level.Split(',').ToList();
                foreach (var levelItem in levels)
                {
                    LogLevel level = LogLevel.FromString(levelItem.Trim());
                    var t = (from ta in targets where ta.Name.Equals(rule.WriteTo) select ta).FirstOrDefault();
                    Target target = null;

                    switch (t.Type)
                    {
                        case "Console":
                            target = new NLog.Targets.ConsoleTarget(t.Name);
                            break;
                        case "File":
                            if (!string.IsNullOrEmpty(t.Layout))
                                target = new NLog.Targets.FileTarget(t.Name) { FileName = t.FileName, Layout = t.Layout };
                            else
                                target = new NLog.Targets.FileTarget(t.Name) { FileName = t.FileName };

                            break;
                        default:
                            break;
                    }

                    //Add rule
                    if (target != null)
                        config.AddRuleForOneLevel(level, target);

                }
            }

            return config;
        }
    }
}

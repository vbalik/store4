﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Core.ADR
{
    public static class ADR
    {
        public static int SafetyPointCalculate(int category, int quantity, float objemKoeficient)
        {
            int coeficient;
            switch (category)
            {
                case 1:
                    coeficient = 50;
                    break;
                case 2:
                    coeficient = 3;
                    break;
                case 3:
                    coeficient = 1;
                    break;
                case 4:
                    coeficient = 0;
                    break;
                default:
                    throw new Exception("Category must be in the range of 1-4");
            }

            return (int)(coeficient * quantity * objemKoeficient);
        }

        public static int SafetyPointCalculateBulk(List<Tuple<int, int, float>> items)
        {
            int result = 0;
            foreach (var item in items)
            {
                result = result + SafetyPointCalculate(item.Item1, item.Item2, item.Item3);
            }

            return result;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Core.Utils
{
    public class ProgressUtils
    {
        private int _lastStep = 0;
        public int GetProgressValue(int processed, int count, int step)
        {
            if (processed == 0 || count == 0)
                return 0;

            var value = (processed * 100 / count);

            if (value < step)
                return 0;

            if (value % step == 0)
            {
                if (_lastStep == value)
                    return 0;

                if (value == 100)
                {
                    _lastStep = value;
                    return 99; //if 100 then the progress ends (that's why it is 99)
                }
                else
                {
                    _lastStep = value;
                    return value;
                }

            }
            else
                return 0;

        }

        public void ResetLastStep()
        {
            _lastStep = 0;
        }
    }
}

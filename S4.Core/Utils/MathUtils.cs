﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S4.Core.Utils
{
    public static class MathUtils
    {
        public static double CustomRound(double value, double span)
        {
            if (span == 0) return 0;

            return span * Math.Round(value / span);
        }

        public static int CustomCeiling(double value, int span)
        {
            if (span == 0) return 0;

            return Convert.ToInt32(span * Math.Ceiling(value / span));
        }
    }
}

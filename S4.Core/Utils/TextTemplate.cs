﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;


namespace S4.Core.Utils
{
    public class TextTemplate
    {
        public enum FieldSource
        {
            Property,
            Params
        }


        private const string FORMAT_PATTERN = @"\x7B.+?\x7D";
        private const char FIELD_PARTS_SEP = ':';
        private const char EXPRESION_PARTS_SEP = '.';



        private readonly string _template;
        private readonly bool _toASCII;
        private readonly string[] _removeChars;
        private readonly bool _enablePathNulls = false;
        private readonly CultureInfo _cultureInfo = CultureInfo.InvariantCulture;

        private object[] _sourceObjects;
        private Type _sourceType;

        private object[] _params;

        private Func<string, string> _replaceUnPrintableCharacter;



        public TextTemplate(string template, bool toASCII = true, string[] removeChars = null, bool enablePathNulls = false, CultureInfo cultureInfo = null)
        {
            _template = template;
            _toASCII = toASCII;
            _removeChars = removeChars;
            _enablePathNulls = enablePathNulls;
            _cultureInfo = (cultureInfo == null) ? CultureInfo.InvariantCulture : cultureInfo;
        }

        public string Render(FieldSource fieldSource, Func<string, string> ReplaceUnPrintableCharacter, params object[] values)
        {
            _replaceUnPrintableCharacter = ReplaceUnPrintableCharacter;
            return Render(fieldSource, values);
        }


        public string Render(FieldSource fieldSource, params object[] values)
        {
            if (fieldSource == FieldSource.Params)
                return RenderParams(values);
            else
                return RenderProperty(values);
        }


        public string RenderProperty(params object[] values)
        {
            _sourceObjects = values;
            _sourceType = values.GetType();
            string result = Regex.Replace(_template, FORMAT_PATTERN, GetMatchProc(true));
            return result;
        }


        public string RenderParams(params object[] values)
        {
            _params = values;
            string result = Regex.Replace(_template, FORMAT_PATTERN, GetMatchProc(false));
            return result;
        }



        #region static

        public static string Make(string template, FieldSource fieldSource, params object[] values)
        { return Make(template, fieldSource, false, new string[0], values); }

        public static string Make(string template, FieldSource fieldSource, bool toASCII, string[] removeChars, CultureInfo cultureInfo, params object[] values)
        {
            var templ = new TextTemplate(template, toASCII, removeChars, false, cultureInfo);
            return templ.Render(fieldSource, values);

        }

        #endregion



        #region evaluators		

        private string GetProperty(Match m)
        {
            try
            {
                string fieldDef = m.Value.Substring(1, m.Value.Length - 2).Trim();
                string[] fieldDefParts = fieldDef.Split(FIELD_PARTS_SEP);

                // check if first part of fieldParts[0] is number
                string[] expresParts = fieldDefParts[0].Split(EXPRESION_PARTS_SEP);

                int paramIndex = GetParamIndex(expresParts);
                if (paramIndex < 0 || paramIndex >= _sourceObjects.Length)
                    throw new Exception("Param number is out of params array");

                if (expresParts.Length == 1)
                {
                    return TemplateValue(_sourceObjects[paramIndex], fieldDefParts);
                }
                else if (expresParts.Length > 1)
                {
                    string newExpression = String.Join(EXPRESION_PARTS_SEP.ToString(), expresParts, 1, expresParts.Length - 1);
                    object val = ObjectEval(newExpression, _sourceObjects[paramIndex], _enablePathNulls);
                    return TemplateValue(val, fieldDefParts);
                }
                else
                {
                    object val = ObjectEval(fieldDefParts[0], _sourceObjects[0], _enablePathNulls);
                    return TemplateValue(val, fieldDefParts);
                }

            }
            catch (Exception exc)
            {
                throw new Exception(String.Format("Formater error on field '{0}'; object '{1}'", m.Value, _sourceType), exc);
            }
        }


        private object ObjectEval(string propertyPath, object srcObject, bool enablePathNulls)
        {
            Type objType;

            try
            {
                var pathParts = propertyPath.Split(EXPRESION_PARTS_SEP);
                for (int i = 0; i < pathParts.Length - 1; i++)
                {
                    objType = srcObject.GetType();
                    srcObject = objType.InvokeMember(pathParts[i], BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.GetProperty | BindingFlags.IgnoreCase, null, srcObject, new object[] { });
                    if (srcObject == null)
                    {
                        if (enablePathNulls)
                            return null;
                        else
                            throw new Exception("Null object in propery path");
                    }
                }

                objType = srcObject.GetType();
                var args = MakeArgs(ref pathParts[pathParts.Length - 1], objType);
                var res = objType.InvokeMember(pathParts[pathParts.Length - 1], BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.GetProperty | BindingFlags.IgnoreCase, null, srcObject, args);

                return res;
            }
            catch (Exception exc)
            {
                throw new Exception(String.Format("Property evaluation exception; property: {0}; src type: {1}", propertyPath, srcObject.GetType()), exc);
            }
        }


        private object[] MakeArgs(ref string property, Type objType)
        {
            int sepPos = property.IndexOf('|');
            if (sepPos == -1)
                return new object[] { };
            else
            {
                var aArgs = property.Substring(sepPos + 1).Split(',');
                property = property.Substring(0, sepPos);

                // find method info
                var info = objType.GetMethod(property, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.GetProperty | BindingFlags.IgnoreCase);
                if (info == null)
                {
                    PropertyInfo prInfo = objType.GetProperty(property, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.GetProperty | BindingFlags.IgnoreCase);
                    info = prInfo.GetGetMethod();
                }
                ParameterInfo[] aParam = info.GetParameters();

                // create args list
                object[] res = new object[aArgs.Length];
                int cnt = (aParam.Length < aArgs.Length) ? aParam.Length : aArgs.Length;
                for (int i = 0; i < cnt; i++)
                    res[i] = Convert.ChangeType(aArgs[i], aParam[i].ParameterType);

                return res;
            }
        }


        private string GetParam(Match m)
        {
            try
            {
                string fieldDef = m.Value.Substring(1, m.Value.Length - 2).Trim();
                string[] fieldParts = fieldDef.Split(FIELD_PARTS_SEP);

                int paramIndex = GetParamIndex(fieldParts);
                if (paramIndex < 0 || paramIndex >= _params.Length)
                    throw new Exception("Param number is out of params array");

                return TemplateValue(_params[paramIndex], fieldParts);
            }
            catch (Exception exc)
            {
                throw new Exception(String.Format("Formater error on field '{0}'", m.Value), exc);
            }
        }


        private int GetParamIndex(string[] fieldParts)
        {
            int posNum = -1;
            if (!Int32.TryParse(fieldParts[0], out posNum))
                throw new Exception("Param number is not valid");
            return posNum;
        }

        #endregion



        #region private

        private MatchEvaluator GetMatchProc(bool propertyEval)
        {
            if (propertyEval)
                return new MatchEvaluator(GetProperty);
            else
                return new MatchEvaluator(GetParam);
        }


        private string TemplateValue(object value, string[] formats)
        {
            string result = null;

            if (value == null)
                return null;
            if (formats.Length <= 1)
                result = value.ToString();
            else
                result = String.Format(_cultureInfo, "{0:" + formats[1] + "}", value);

            PostProc(ref result);

            if (_replaceUnPrintableCharacter != null)
                result = _replaceUnPrintableCharacter(result);

            // parts - characters from - to
            if (formats.Length > 2 && !String.IsNullOrWhiteSpace(formats[2]))
            {
                string[] aFromTo = formats[2].Split(',');
                if (aFromTo[0].StartsWith("!"))
                {
                    int index = Int32.Parse(aFromTo[0].Substring(1));
                    int subLen = Int32.Parse(aFromTo[1]);
                    // split by whitespaces
                    string[] parts = SplitByWhitespaces(result, subLen);
                    if (index < parts.Length)
                        result = parts[index];
                    else
                        result = String.Empty;
                }
                else
                {
                    int fromPos = Int32.Parse(aFromTo[0]);
                    int subLen = Int32.Parse(aFromTo[1]);

                    if (result.Length <= fromPos)
                        result = String.Empty;
                    else
                    {
                        subLen = Math.Min(subLen, result.Length - fromPos);
                        result = result.Substring(fromPos, subLen);
                    }
                }
            }

            return result;
        }


        private string[] SplitByWhitespaces(string text, int partLen)
        {
            var result = new List<string>();

            string[] words = text.Split(' ', '\t');
            int charCnt = 0, lastPos = 0;
            for (int i = 0; i < words.Length; i++)
            {
                charCnt += words[i].Length + 1;
                if ((charCnt - 1) > partLen)
                {
                    if (i == lastPos)
                    {
                        result.Add(words[i].Substring(0, partLen));
                        lastPos = i + 1;
                    }
                    else
                    {
                        result.Add(String.Join(" ", words, lastPos, i - lastPos));
                        lastPos = i;
                    }

                    charCnt = 0;
                }

            }

            // do last
            if (lastPos < words.Length)
                result.Add(String.Join(" ", words, lastPos, words.Length - lastPos));

            return result.ToArray();
        }


        private string TemplateValue(object value, string format)
        {
            string result = null;

            if (value == null)
                return null;
            if (format == null)
                result = value.ToString();
            else
                result = String.Format(_cultureInfo, "{0:" + format + "}", value);

            PostProc(ref result);

            return result;
        }


        private void PostProc(ref string text)
        {
            if (_toASCII)
                text = Core.Utils.StrUtils.ToASCII(text, _removeChars);
        }

        #endregion
    }
}

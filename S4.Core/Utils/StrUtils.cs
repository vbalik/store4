﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace S4.Core.Utils
{
    public class StrUtils
    {

        public static string RandomDigits(int count)
        {
            char[] result = new char[count];

            Random random = new Random();
            for (int i = 0; i < count; i++)
                result[i] = (char)random.Next(0x30, 0x39);

            return new string(result);
        }

        public static string OnlyLetters(string text)
        {
            char[] result = new char[text.Length];
            int resPos = 0;

            for (int pos = 0; pos < text.Length; pos++)
            {
                if (Char.IsLetterOrDigit(text, pos) && ((int)text[pos] < 128))
                {
                    result[resPos] = Char.ToUpper(text[pos]);
                    resPos++;
                }
            }

            return new string(result, 0, resPos);
        }

        public static object ObjectToASCII(object obj, params string[] toRemove)
        {
            Type t = obj.GetType();
            PropertyInfo[] props = t.GetProperties();
            foreach (PropertyInfo prp in props)
            {
                if (prp.PropertyType == typeof(string) && prp.CanWrite)
                {
                    var v = prp.GetValue(obj, new object[] { });

                    if (v != null)
                    {
                        var value = ToASCII(v.ToString(), "\"");
                        prp.SetValue(obj, value);
                    }
                }
            }

            return obj;
        }

        public static string ToASCII(string text, params string[] toRemove)
        {
            if (string.IsNullOrWhiteSpace(text))
                return text;

            byte[] _852toAscii = new byte[] {
                0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b,
                0x0c, 0x0d, 0x0e, 0x0f, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17,
                0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f, 0x20, 0x21, 0x22, 0x23,
                0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2a, 0x2b, 0x2c, 0x2d, 0x2e, 0x2f,
                0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3a, 0x3b,
                0x3c, 0x3d, 0x3e, 0x3f, 0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47,
                0x48, 0x49, 0x4a, 0x4b, 0x4c, 0x4d, 0x4e, 0x4f, 0x50, 0x51, 0x52, 0x53,
                0x54, 0x55, 0x56, 0x57, 0x58, 0x59, 0x5a, 0x5b, 0x5c, 0x5d, 0x5e, 0x5f,
                0x60, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69, 0x6a, 0x6b,
                0x6c, 0x6d, 0x6e, 0x6f, 0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77,
                0x78, 0x79, 0x7a, 0x7b, 0x7c, 0x7d, 0x7e, 0x7f, 0x43, 0x75, 0x65, 0x61,
                0x61, 0x75, 0x63, 0x63, 0x6c, 0x65, 0x4f, 0x6f, 0x69, 0x5a, 0x41, 0x43,
                0x45, 0x4c, 0x49, 0x6f, 0x6f, 0x4c, 0x6c, 0x53, 0x73, 0x4f, 0x55, 0x54,
                0x74, 0x4c, 0x9e, 0x63, 0x61, 0x69, 0x6f, 0x75, 0x41, 0x61, 0x5a, 0x7a,
                0x45, 0x65, 0xaa, 0x7a, 0x43, 0x73, 0xae, 0xaf, 0xb0, 0xb1, 0xb2, 0xb3,
                0xb4, 0x41, 0x41, 0x45, 0x53, 0xb9, 0xba, 0xbb, 0xbc, 0x5a, 0x7a, 0xbf,
                0xc0, 0xc1, 0xc2, 0xc3, 0xc4, 0xc5, 0x41, 0x61, 0xc8, 0xc9, 0xca, 0xcb,
                0xcc, 0xcd, 0xce, 0xcf, 0x64, 0x44, 0x44, 0x45, 0x64, 0x4e, 0x49, 0x49,
                0x65, 0xd9, 0xda, 0xdb, 0xdc, 0x54, 0x55, 0xdf, 0x4f, 0xe1, 0x4f, 0x4e,
                0x6e, 0x6e, 0x53, 0x73, 0x52, 0x55, 0x72, 0x55, 0x79, 0x59, 0x74, 0xef,
                0xf0, 0xf1, 0xf2, 0xf3, 0xf4, 0xf5, 0xf6, 0xf7, 0xf8, 0xf9, 0xfa, 0x75,
                0x52, 0x72, 0xfe, 0xff};


            if (text == null)
                return null;

            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
            // replace CP 852 chars
            System.Text.Encoding encASCII = System.Text.Encoding.ASCII;
            System.Text.Encoding enc852 = System.Text.Encoding.GetEncoding(852);

            byte[] bytes = enc852.GetBytes(text);
            for (int pos = 0; pos < bytes.Length; pos++)
                bytes[pos] = _852toAscii[bytes[pos]];

            string result = encASCII.GetString(bytes);


            // remove 'bad chars'
            if (toRemove != null && toRemove.Length > 0)
                for (int i = 0; i < toRemove.Length; i++)
                    result = result.Replace(toRemove[i], String.Empty);

            return result;
        }

        public static string ReplaceUnPrintableCharacter(string template)
        {
            return template.Replace("\"", "");
        }

        public static string ReplaceNonAlphaNumericCharacter(string template)
        {
            if (string.IsNullOrWhiteSpace(template))
                return template;

            Regex rgx = new Regex("[^a-zA-Z0-9]");
            template = rgx.Replace(template, string.Empty);
            return template;
        }

        public static string ReplaceNonCSVCharacter(string template)
        {
            if (string.IsNullOrWhiteSpace(template))
                return template;

            Regex rgx = new Regex(@"\r\n?|\n");
            template = rgx.Replace(template, "").Replace(" ", string.Empty).Replace(";", string.Empty);

            return template;
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace S4.Core.Utils
{
    public static class DateUtils
    {
        private static object _lock = new object();

        public static DateTime? GetDateByMask(string Value, string Mask)
        {
            lock (_lock)
            {
                DateTime? res = null;

                if (!string.IsNullOrEmpty(Value))
                {
                    //day------------------------------------------
                    int iDay = GetDatePart(Value, Mask, 'd');
                    //month----------------------------------------
                    int iMonth = GetDatePart(Value, Mask, 'M');
                    //year------------------------------------------
                    int iYear = GetDatePart(Value, Mask, 'y');
                    //hour---------------------------------------
                    int iHour = GetDatePart(Value, Mask, 'H');
                    //minute---------------------------------------
                    int iMinute = GetDatePart(Value, Mask, 'm');

                    DateTime TheDate = new DateTime(iYear, iMonth, iDay, iHour, iMinute, 0);

                    res = TheDate;
                }

                return res;
            }
        }

        private static int GetDatePart(string Value, string Param, char Key)
        {
            string sPart = string.Empty;
            int iPos = -1;

            iPos = Param.IndexOf(Key, iPos + 1);
            while (iPos != -1)
            {
                sPart = sPart + Value.Substring(iPos, 1);
                iPos = Param.IndexOf(Key, iPos + 1);
            }
            if (Key == 'y')
            {
                sPart = "2000".Substring(0, 4 - sPart.Length) + sPart;
            }

            int res = 0;
            int.TryParse(sPart, out res);

            return res;
        }

        public static string GetShortDayName(byte wday)
        {
            lock (_lock)
            {
                return System.Globalization.DateTimeFormatInfo.CurrentInfo.GetShortestDayName((DayOfWeek)CZ2ENDayNumber(wday));
            }
        }

        public static DayOfWeek CZ2ENDayNumber(byte wday)
        {
            lock (_lock)
            {
                if (wday == 7)
                    return 0;
                return (DayOfWeek)wday;
            }
        }

        public static byte EN2CZDayNumber(DayOfWeek wday)
        {
            lock (_lock)
            {
                if (wday == DayOfWeek.Sunday)
                    return 7;
                return (byte)wday;
            }
        }

        public static DateTime EncodeDateWWd(int year, int weekOfYear, DayOfWeek wday)
        {
            DateTime jan1 = new DateTime(year, 1, 1);
            int daysOffset = DayOfWeek.Thursday - jan1.DayOfWeek;

            // Use first Thursday in January to get first week of the year as
            // it will never be in Week 52/53
            DateTime firstThursday = jan1.AddDays(daysOffset);
            var cal = CultureInfo.CurrentCulture.Calendar;
            int firstWeek = cal.GetWeekOfYear(firstThursday, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            var weekNum = weekOfYear;
            // As we're adding days to a date in Week 1,
            // we need to subtract 1 in order to get the right date for week #1
            if (firstWeek == 1)
            {
                weekNum -= 1;
            }

            // Using the first Thursday as starting week ensures that we are starting in the right year
            // then we add number of weeks multiplied with days
            var result = firstThursday.AddDays(weekNum * 7);

            var czWDay = EN2CZDayNumber(wday);

            // Subtract 3 days from Thursday to get Monday, which is the first weekday in ISO8601
            return result.AddDays(wday - DayOfWeek.Thursday);
        }
    }
}

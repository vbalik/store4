﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Core.Utils
{
    public class DataUtils
    {

        public static bool CompareVal(object val1, object val2)
        {
            if (val1 == null && val2 == null)
                return true;
            if (val1 == null ^ val2 == null)
                return false;
            if (val1.GetType() != val2.GetType())
                return false;

            // compare by types
            if (val1 is System.Byte)
                return ((System.Byte)val1 == (System.Byte)val2);
            if (val1 is System.Int16)
                return ((System.Int16)val1 == (System.Int16)val2);
            if (val1 is System.Int32)
                return ((System.Int32)val1 == (System.Int32)val2);
            if (val1 is System.Int64)
                return ((System.Int64)val1 == (System.Int64)val2);

            if (val1 is System.Decimal)
                return (System.Decimal.Compare((System.Decimal)val1, (System.Decimal)val2) == 0);
            if (val1 is System.Single)
                return ((System.Single)val1 == (System.Single)val2);
            if (val1 is System.Double)
                return ((System.Double)val1 == (System.Double)val2);

            if (val1 is System.String)
                return System.String.Equals((string)val1, (string)val2);

            return (val1.ToString() == val2.ToString());
        }


    }
}


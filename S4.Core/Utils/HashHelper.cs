﻿using S4.Core.Extensions;


namespace S4.Core.Utils
{
    public class HashHelper
    {
        private const int PRIME_NUMBER = 486187739;
        private const int DEFAULT_HASH_VALUE = 31;

        public static int GetHashCode<T>(T param) => param.IsDefault() ? 0 : param.GetHashCode();

        public static int GetHashCode<T1, T2>(T1 param1, T2 param2)
        {
            unchecked
            {
                var hash = DEFAULT_HASH_VALUE;
                hash = hash * PRIME_NUMBER + GetHashCode(param1);
                return hash * PRIME_NUMBER + GetHashCode(param2);
            }
        }

        public static int GetHashCode<T1, T2, T3>(T1 param1, T2 param2, T3 param3)
        {
            unchecked
            {
                var hash = DEFAULT_HASH_VALUE;
                hash = hash * PRIME_NUMBER + GetHashCode(param1);
                hash = hash * PRIME_NUMBER + GetHashCode(param2);
                return hash * PRIME_NUMBER + GetHashCode(param3);
            }
        }


        public static int GetHashCode<T1, T2, T3, T4>(T1 param1, T2 param2, T3 param3, T4 param4)
        {
            unchecked
            {
                var hash = DEFAULT_HASH_VALUE;
                hash = hash * PRIME_NUMBER + GetHashCode(param1);
                hash = hash * PRIME_NUMBER + GetHashCode(param2);
                hash = hash * PRIME_NUMBER + GetHashCode(param3);
                return hash * PRIME_NUMBER + GetHashCode(param4);
            }
        }

        public static int GetHashCode<T>(params T[] parameters)
        {
            unchecked
            {
                var hash = DEFAULT_HASH_VALUE;
                for (var i = 0; i < parameters.Length; i++)
                {
                    var param = parameters[i];
                    hash = hash * PRIME_NUMBER + GetHashCode(param);
                }
                return hash;
            }
        }
    }
}

﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace S4.Core.Utils
{
    public static class EntitiesUtils
    {
        public static void RepairMaxLengthModelValues<T>(T obj, params string[] ignored)
        {
            var properties = obj.GetType().GetProperties();

            foreach (var item in properties)
            {
                if ((from v in ignored where v.Equals(item.Name) select true).FirstOrDefault())
                    continue;

                var maxLengthAttribute = item.GetCustomAttributes(typeof(MaxLengthAttribute)).FirstOrDefault();

                if (maxLengthAttribute == null)
                    continue;

                var maxLength = ((MaxLengthAttribute)maxLengthAttribute).Length;
                var value = obj.GetType().GetProperty(item.Name).GetValue(obj);

                if (value != null && value.ToString().Length > maxLength)
                    obj.GetType().GetProperty(item.Name).SetValue(obj, value.ToString().Substring(0, maxLength));
            }
        }
    }
}

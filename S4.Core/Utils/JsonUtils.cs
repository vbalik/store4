﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Text;

namespace S4.Core.Utils
{
    public static class JsonUtils
    {

        public static string ToJsonString(params dynamic[] values)
        {
            var dict = new Dictionary<string, dynamic>();

            for (int i = 0; i < values.Length; i++)
            {
                dict.Add(values[i], values.Length > i + 1 ? values[i + 1] : "");
                i++;
            }
            
            return JsonConvert.SerializeObject(dict);
        }
        
    }


}



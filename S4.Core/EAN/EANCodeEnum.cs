﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Core.EAN
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum EANCodeEnum : int
    {
        Unknown = -1,
        //00  	Serial Shipping Container Code(SSCC-18)  	 18 digits
        SerialShippingContainer = 0,

        //01 	Shipping Container Code (SSCC-14) 	14 digits
        ShippingContainerCode = 1,
        
        //02 	Number of containers 	14 digits
        NumberOfContainers = 2,
        
        //10 	Batch Number 	1-20 alphanumeric
        BatchNumber = 10,
        
        //11 	Production Date 	6 digits: YYMMDD
        ProductionDate = 11,
        
        //12	Due Date (YYMMDD)	n2+n6	8	DUE DATE
        DueDate = 12,
        
        //13 	Packaging Date 	6 digits: YYMMDD
        PackagingDate = 13,
        
        //15 	Sell by Date (Quality Control) 	6 digits: YYMMDD
        SellByDate = 15,
        
        //17 	ExpirationDate Date 	6 digits: YYMMDD
        ExpirationDate = 17,
        
        //20 	Product Variant 	2 digits
        ProductVariant = 20,
        
        //21 	Serial Number 	1-20 alphanumeric
        SerialNumber = 21,
        
        //22 	HIBCC Quantity, Date, Batch and Link 	1-29 alphanumeric
        HIBCC = 22,
        
        //30 	Quantity Each 	-
        QuantityEach = 30,
        
        //37  	Number of Units Contained  	 1-8 digits
        NumberOfUnitsContained = 37,

        //240  Additional product identification assigned by the manufacturer
        AdditionalProductID = 240
    }
}

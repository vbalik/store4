﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Core.EAN
{
    internal class EANDictionary
    {
        public static Dictionary<int, EANDictionaryItem> Dictionary { get; } =
            new Dictionary<int, EANDictionaryItem>()
            {
                {  (int)EANCodeEnum.SerialShippingContainer, new EANDictionaryItem(EANCodeEnum.SerialShippingContainer, 18) },
                {  (int)EANCodeEnum.ShippingContainerCode, new EANDictionaryItem(EANCodeEnum.ShippingContainerCode, 14) },
                {  (int)EANCodeEnum.NumberOfContainers, new EANDictionaryItem(EANCodeEnum.NumberOfContainers, 14) },
                {  (int)EANCodeEnum.BatchNumber, new EANDictionaryItem(EANCodeEnum.BatchNumber) },
                {  (int)EANCodeEnum.ProductionDate, new EANDictionaryItem(EANCodeEnum.ProductionDate, 6) },
                {  (int)EANCodeEnum.DueDate, new EANDictionaryItem(EANCodeEnum.DueDate, 6) },
                {  (int)EANCodeEnum.PackagingDate, new EANDictionaryItem(EANCodeEnum.PackagingDate, 6) },
                {  (int)EANCodeEnum.SellByDate, new EANDictionaryItem(EANCodeEnum.SellByDate, 6) },
                {  (int)EANCodeEnum.ExpirationDate, new EANDictionaryItem(EANCodeEnum.ExpirationDate, 6) },
                {  (int)EANCodeEnum.ProductVariant, new EANDictionaryItem(EANCodeEnum.ProductVariant, 2) },
                {  (int)EANCodeEnum.SerialNumber, new EANDictionaryItem(EANCodeEnum.SerialNumber) },
                {  (int)EANCodeEnum.HIBCC, new EANDictionaryItem(EANCodeEnum.HIBCC) },
                {  (int)EANCodeEnum.QuantityEach, new EANDictionaryItem(EANCodeEnum.QuantityEach) },
                {  (int)EANCodeEnum.NumberOfUnitsContained, new EANDictionaryItem(EANCodeEnum.NumberOfUnitsContained) },
                {  (int)EANCodeEnum.AdditionalProductID, new EANDictionaryItem(EANCodeEnum.AdditionalProductID) }
            };


        internal class EANDictionaryItem
        {
            internal EANDictionaryItem(EANCodeEnum eanCode, int fixedLength = -1)
            {
                EANCode = eanCode;
                FixedLength = fixedLength;
            }


            public EANCodeEnum EANCode { get; private set; }
            public int FixedLength { get; private set; }
        }
    }
}

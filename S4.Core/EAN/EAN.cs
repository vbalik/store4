﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using static S4.Core.EAN.EANDictionary;

namespace S4.Core.EAN
{
    public class EAN
    {
        private const string EAN128_PREFIX = "]C1";
        private const string QRCODE_PREFIX = "]d2";
        private const string EAN128_GS_MARK = "[GS]";
        private const char EAN128_FNC1 = (char) 0x15;
        private const char EAN128_FNC2 = (char) 29;
        public const int EAN128_MIN_LEN = 16;


        public List<EANPart> Parts { get; set; } = new List<EANPart>();

        public EAN()
        {

        }

        public EAN(string rawCode)
        {
            if (string.IsNullOrWhiteSpace(rawCode))
                throw new ArgumentNullException("rawCode");

            // remove unwanted characters
            rawCode = Regex.Replace(rawCode, @"\t|\n|\r", string.Empty);
            rawCode = Regex.Replace(rawCode, $"{EAN128_PREFIX}|{QRCODE_PREFIX}", string.Empty); // QR code


            // parse EAN started with EAN128_PREFIX, QRCODE_PREFIX, number, (*) [because data-matrix type does not start with QR prefix but still can be EAN part]
            if (rawCode.Length >= EAN128_MIN_LEN
                && (Regex.IsMatch(rawCode, $"^({EAN128_PREFIX}|{QRCODE_PREFIX}|{EAN128_FNC2}|\\d)") || Regex.IsMatch(rawCode, "^\\(.*\\).*")))
            {
                // prepare
                rawCode = rawCode.Replace(EAN128_GS_MARK, new string(EAN128_FNC2, 1));

                // decode
                DecodeFlat(rawCode);
            }
            else
            {
                // !20 is not carrier number
                if (rawCode.Length != 20 && rawCode.StartsWith("0"))
                    rawCode = rawCode.Substring(1, rawCode.Length - 1);

                // one part code
                Parts.Add(new EANPart() { EANCode = EANCodeEnum.ShippingContainerCode, Value = rawCode });
            }
        }


        public EANPart this[EANCodeEnum eanCode]
        {
            get
            {
                return Parts.FirstOrDefault(i => i.EANCode == eanCode);
            }
        }

        public bool GetEANCode(out string ean, out string error)
        {
            ean = "";
            error = "";
            var isOK = true;
            foreach (var part in Parts.OrderBy(p => (int) p.EANCode))
            {
                EANDictionaryItem dictItem = null;
                if (Dictionary.TryGetValue((int) part.EANCode, out dictItem))
                {
                    var end = "";
                    if (dictItem.FixedLength == -1)
                        end = new string(EAN128_FNC2, 1);
                    else
                    {
                        if (part.Value.Length != dictItem.FixedLength)
                        {
                            error += $"Chybná délka {part.EANCode}. Délka je ({part.Value.Length}), má být ({dictItem.FixedLength}). ";
                            isOK = false;
                            ean = "";
                        }
                    }

                    if (isOK)
                        ean += $"{((int) part.EANCode).ToString("00")}{part.Value}{end}";
                }
            }

            return isOK;
        }

        #region private

        private void DecodeFlat(string rawCode)
        {
            // main loop
            int pos = (rawCode.StartsWith(EAN128_PREFIX)) ? EAN128_PREFIX.Length : 0;

            while (pos < rawCode.Length)
            {
                if(rawCode[pos] == '(')
                    pos++;

                // Batist exception
                // and other bad codes with <GS> after fixed length
                if ((int)rawCode[pos] == (int) EAN128_FNC2)
                    pos += 1;
                                
                string codePrefix = rawCode.Substring(pos, 2);

                // code prefixes of different len
                switch (codePrefix)
                {
                    case "24":
                    case "25":
                    case "40":
                    case "41":
                    case "42":
                        codePrefix = rawCode.Substring(pos, 3);
                        pos += 3;
                        break;

                    case "31":
                    case "32":
                    case "33":
                    case "34":
                    case "35":
                    case "36":
                    case "38":
                    case "39":
                    case "70":
                    case "80":
                        codePrefix = rawCode.Substring(pos, 4);
                        pos += 4;
                        break;

                    default:
                        pos += 2;
                        break;
                }
                int codeNum = Int32.Parse(codePrefix);
                string partText = null;

                if (rawCode[pos] == ')')
                    pos++;

                // try to get definition
                EANDictionary.EANDictionaryItem dictItem = null;
                if (!EANDictionary.Dictionary.TryGetValue(codeNum, out dictItem))
                {
                    dictItem = new EANDictionary.EANDictionaryItem(EANCodeEnum.Unknown);
                    partText = ReadPartVariable(ref rawCode, ref pos);
                }
                else
                {
                    if (dictItem.FixedLength == -1)
                        partText = ReadPartVariable(ref rawCode, ref pos);
                    else
                    {
                        // exceptions
                        // CellaVisionAB
                        if (dictItem.EANCode == EANCodeEnum.ExpirationDate
                            && Parts.Any(_ => _.EANCode == EANCodeEnum.ShippingContainerCode && _.Value.StartsWith("735004097")))
                            dictItem = new EANDictionaryItem(dictItem.EANCode, 9);

                        partText = ReadPartFixed(ref rawCode, ref pos, dictItem.FixedLength);

                        // exceptions
                        // CellaVisionAB
                        if (dictItem.EANCode == EANCodeEnum.ExpirationDate
                            && Parts.Any(_ => _.EANCode == EANCodeEnum.ShippingContainerCode && _.Value.StartsWith("735004097")))
                            partText = partText.Replace("-", "").Substring(2);

                        // Ecolab SNC - Anios
                        if (Parts.Count > 0
                            && Parts[0].Value.StartsWith("359761")
                            && dictItem.EANCode == EANCodeEnum.ExpirationDate)
                        {
                            if (partText.Contains("-"))
                            {
                                // 5-1020 -> 2025-10 -> 311025
                                var yearEnd = int.Parse(partText.Substring(0, 1));
                                var month = int.Parse(partText.Substring(2, 2));
                                var year = int.Parse(partText.Substring(4, 2)) + yearEnd;
                                var day = DateTime.DaysInMonth((2000 + year), month);
                                partText = $"{(day < 9 ? "0" : "")}{day}{month}{year}";
                            }
                            else if (int.Parse(partText.Substring(0, 2)) < 13)
                            {
                                // 102026 -> 311026
                                var month = int.Parse(partText.Substring(0, 2));
                                var year = int.Parse(partText.Substring(2, 4));
                                var day = DateTime.DaysInMonth(year, month);
                                partText = $"{(day < 9 ? "0" : "")}{day}{(month < 9 ? "0" : "")}{month}{year - 2000}";
                            }
                            else
                            {
                                // 202610 -> 311026
                                var year = int.Parse(partText.Substring(0, 4));
                                var month = int.Parse(partText.Substring(4, 2));
                                var day = DateTime.DaysInMonth(year, month);
                                partText = $"{(day < 9 ? "0" : "")}{day}{(month < 9 ? "0" : "")}{month}{year - 2000}";
                            }
                        }
                    }

                    // exceptions
                    // GAMA exception
                    if (dictItem.EANCode == EANCodeEnum.ShippingContainerCode && partText.StartsWith("85935404"))
                    {
                        partText = partText.Substring(0, 13);
                        pos--;
                    }

                    // Fresenius exception
                    if (dictItem.EANCode == EANCodeEnum.ShippingContainerCode && partText.StartsWith("40462410"))
                    {
                        partText = partText.Substring(0, 13);
                        pos--;
                    }

                    // ean system normalize - always remove 0 from start
                    if (dictItem.EANCode == EANCodeEnum.ShippingContainerCode && partText.StartsWith("0"))
                        partText = partText.Substring(1, 13);

                    // set additionalProductID as shippingContainerCode if shippingContainerCode is not included
                    // go to next iteration
                    if (dictItem.EANCode == EANCodeEnum.AdditionalProductID && !Parts.Any(i => i.EANCode == EANCodeEnum.ShippingContainerCode))
                    {
                        Parts.Add(new EANPart() { EANCode = EANCodeEnum.ShippingContainerCode, Value = partText });
                        continue;
                    }
                }

                // add to list
                Parts.Add(new EANPart() { EANCode = dictItem.EANCode, Value = partText });
            }
        }


        private string ReadPartVariable(ref string barCode, ref int pos)
        {
            int startPos = pos;
            while (pos < barCode.Length && barCode[pos] != EAN128_FNC2 && barCode[pos] != '(')
                pos++;
            // skip end char
            pos++;
            return barCode.Substring(startPos, pos - startPos - 1);
        }


        private string ReadPartFixed(ref string barCode, ref int pos, int len)
        {
            // trimLeft
            while (Char.IsWhiteSpace(barCode[pos]))
            {
                pos++;
                if (pos > barCode.Length)
                    return null;
            }

            if ((pos + len) > barCode.Length)
                len = barCode.Length - pos;
            string result = barCode.Substring(pos, len);
            pos += len;
            return result;
        }

        #endregion
    }
}

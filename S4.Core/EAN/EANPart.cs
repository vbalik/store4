﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Core.EAN
{
    public class EANPart
    {
        public EANCodeEnum EANCode { get; set; }
        public string Value { get; set; }
    }
}

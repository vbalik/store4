﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace S4.Core.Interfaces
{
    public interface IBackgroundTaskQueue
    {
        string QueueBackgroundWorkItem(Func<CancellationToken, Task> workTask, Func<CancellationToken, Task> inCaseOfErrorTask = null);

        Task<QueueTaskItem> DequeueAsync(CancellationToken cancellationToken);
    }
}

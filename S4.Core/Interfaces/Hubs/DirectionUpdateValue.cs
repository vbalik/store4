namespace S4.Core.Interfaces.Hubs
{
    public class DirectionUpdateValue
    {
        public dynamic Id { get; set; }
        public dynamic Value { get; set; }

        public DirectionUpdateValue(dynamic id, dynamic value)
        {
            Id = id;
            Value = value;
        }
    }
}
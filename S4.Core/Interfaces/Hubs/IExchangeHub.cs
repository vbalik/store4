using System.Threading.Tasks;

namespace S4.Core.Interfaces.Hubs
{
    public interface IExchangeHub
    {
        Task UpdateValue(ExchangeUpdateValue value);
        
    }
}
namespace S4.Core.Interfaces.Hubs
{
    public class ExchangeUpdateValue
    {
        public string[] JobNames { get; set; }

        public ExchangeUpdateValue(string[] jobNames)
        {
            JobNames = jobNames;
        }
    }
}
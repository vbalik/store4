using System.Collections.Generic;
using System.Threading.Tasks;

namespace S4.Core.Interfaces.Hubs
{
    public interface IDirectionHub
    {
        Task UpdateDirectionItemsCompleted(List<DirectionUpdateValue> values);
        Task UpdateDirectionStatus(List<DirectionUpdateValue> values);
    }
}
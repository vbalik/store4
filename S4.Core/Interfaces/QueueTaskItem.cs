﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace S4.Core.Interfaces
{
    public class QueueTaskItem
    {
        public string TaskID { get; set; }
        public Func<CancellationToken, Task> WorkTask { get; set; }
        public Func<CancellationToken, Task> InCaseOfErrorTask { get; set; }
    }
}

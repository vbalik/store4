﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;


namespace S4.Core.PropertiesComparer
{
    public class PropertyValuesCompare
    {
        [JsonProperty("changes")]
        public List<PropertyChangesItem> Items { get; set; } = new List<PropertyChangesItem>();
    }
}
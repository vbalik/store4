﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using System.Reflection;

namespace S4.Core.PropertiesComparer
{
    public class PropertiesComparer<T> where T: new()
    {
        public PropertiesComparer()
        {

        }

        public PropertiesComparer(T origin, T current)
        {
            DoCompare(origin, current);
        }


        public PropertyValuesCompare PropertyValuesCompare { get; private set; } = new PropertyValuesCompare();

        public string JsonText
        {
            get
            {
                return JsonConvert.SerializeObject(PropertyValuesCompare);
            }
        }


        public void DoCompare(T origin, T current)
        {
            foreach(var prop in typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                var originStr = ToStringInvariant(prop.GetValue(origin));
                var currentStr = ToStringInvariant(prop.GetValue(current));
                if (originStr != currentStr)
                {
                    PropertyValuesCompare.Items.Add(new PropertyChangesItem() { PropName = prop.Name, OriginValue = originStr, CurrentValue = currentStr });
                }
            }
        }


        public void DoCompare<T2>(T origin, T2 current, params string[] properties)
        {
            foreach (var propertyName in properties)
            {
                var originStr = ToStringInvariant(origin.GetType().GetProperty(propertyName).GetValue(origin));
                var currentStr = ToStringInvariant(current.GetType().GetProperty(propertyName).GetValue(current));
                if (originStr != currentStr)
                {
                    PropertyValuesCompare.Items.Add(new PropertyChangesItem() { PropName = propertyName, OriginValue = originStr, CurrentValue = currentStr });
                }
            }
        }


        public string ToStringInvariant(object val)
        {
            return FormattableString.Invariant($"{val}");
        }
    }
}

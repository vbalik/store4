﻿using System;
using Newtonsoft.Json;


namespace S4.Core.PropertiesComparer
{
    public class PropertyChangesItem
    {
        [JsonProperty("property")]
        public string PropName { get; set; }
        [JsonProperty("originValue")]
        public string OriginValue { get; set; }
        [JsonProperty("currentValue")]
        public string CurrentValue { get; set; }
    }
}

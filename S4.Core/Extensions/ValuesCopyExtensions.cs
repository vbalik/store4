﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace S4.Core.Extensions
{
    public static class ValuesCopyExtensions
    {
        public static List<T2> ToS3<T1,T2>(this List<T1> source, Action<T1, T2> action = null) where T2 : new()
        {
            return source.Select(i => {
                var newItem = new T2();
                newItem.FieldsFromProperties(i);
                action?.Invoke(i, newItem);
                return newItem;
            }).ToList();
        }


        public static void FieldsFromProperties<T1, T2>(this T1 destination, T2 source)
        {
            var sourceProperties = new List<PropertyInfo>(typeof(T2).GetProperties(BindingFlags.Public | BindingFlags.Instance));
            if (typeof(T2).BaseType != null)
                sourceProperties.AddRange(typeof(T2).BaseType.GetProperties(BindingFlags.Public | BindingFlags.Instance));

            foreach (var srcProperty in sourceProperties)
            {
                if (srcProperty.PropertyType.IsPrimitive || srcProperty.PropertyType == typeof(string))
                {
                    // find dest property
                    var destField = typeof(T1).GetFields().Where(f => String.Equals(f.Name, srcProperty.Name, StringComparison.InvariantCultureIgnoreCase)).SingleOrDefault();
                    if (destField == null)
                        continue;

                    if (srcProperty.PropertyType != destField.FieldType)
                        continue;

                    // get value
                    object value = null;
                    var getMethod = srcProperty.GetGetMethod();
                    if (getMethod == null)
                        continue;

                    value = getMethod.Invoke(source, new object[] { });

                    // set value
                    destField.SetValue(destination, value);
                }
            }
        }


    }
}

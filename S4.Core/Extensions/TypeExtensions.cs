﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;


namespace S4.Core.Extensions
{
    public static class TypeExtensions
    {
        public static PropertyInfo[] PropertiesWithAttr(this Type type, Type attrType)
        {
            var props = from p in type.GetProperties()
                          let attrs = p.GetCustomAttributes(attrType, true)
                          where attrs.Length != 0
                          select p;

            return props.ToArray();
        }
    }
}

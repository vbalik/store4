﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using Newtonsoft.Json;

namespace S4.Core.Extensions
{
    public static class ObjectExtension
    {
        /// <summary>
        /// Gets the value of the property by property name.
        /// </summary>
        /// <param name="source">The source object.</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <returns></returns>
        public static object GetValueByName(this object source, string propertyName
#if TABLET
            , bool caseSensitive = false 
#else
            , BindingFlags bindingFlags = BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance
#endif
            )
        {
#if TABLET
            if (caseSensitive)
                return source.GetType().GetRuntimeProperty(propertyName).GetValue(source, null);
            else
                return source.GetType().GetRuntimeProperties().Where(p => p.Name.ToUpper() == propertyName.ToUpper()).First().GetValue(source, null);
#else
            return source.GetType().GetProperty(propertyName, bindingFlags).GetValue(source, null);
#endif
        }


        /// <summary>
        /// Sets the value of the property by property name.
        /// </summary>
        /// <param name="source">The source object.</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="value">New value.</param>
        /// <returns></returns>
        public static void SetValueByName(this object source, string propertyName, object value
#if TABLET
            , bool caseSensitive = false 
#else
            , BindingFlags bindingFlags = BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance
#endif
            )
        {
#if TABLET
            if (caseSensitive)
                source.GetType().GetRuntimeProperty(propertyName).SetValue(source, value);
            else
                source.GetType().GetRuntimeProperties().Where(p => p.Name.ToUpper() == propertyName.ToUpper()).First().SetValue(source, value);
#else
            source.GetType().GetProperty(propertyName, bindingFlags).SetValue(source, value);
#endif
        }


        /// <summary>
        /// Gets the type of the property by property name.
        /// </summary>
        /// <param name="source">The source object.</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <returns></returns>
        public static Type GetPropertyTypeByName(this object source, string propertyName
#if TABLET
            , bool caseSensitive = false
#else
            , BindingFlags bindingFlags = BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance
#endif
            )
        {
#if TABLET
            if (caseSensitive)
                return source.GetType().GetRuntimeProperty(propertyName).PropertyType;
            else
                return source.GetType().GetRuntimeProperties().Where(p => p.Name.ToUpper() == propertyName.ToUpper()).First().PropertyType;
#else
            return source.GetType().GetProperty(propertyName, bindingFlags).PropertyType;
#endif
        }


        /// <summary>
        /// Is property exist ?
        /// </summary>
        /// <param name="source">The source object.</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <returns></returns>
        public static bool PropertyExist(this object source, string propertyName)
        {
#if TABLET
            return (source.GetType().GetRuntimeProperty(propertyName) != null);
#else
            return (source.GetType().GetProperty(propertyName) != null);
#endif
        }

        public static Type GetPropertyType(this PropertyInfo source)
        {
            var type = Nullable.GetUnderlyingType(source.PropertyType);
            if (type == null)
                type = source.PropertyType;
            return type;
        }


        public static FieldInfo GetFieldAllBases(this Type source, string fieldName)
        {
            var theType = source;
            while (theType != null)
            {
                var fieldInfo = theType.GetField(fieldName);
                if (fieldInfo != null)
                    return fieldInfo;

                theType = theType.BaseType;
            }

            return null;
        }


        public static T Clone<T>(this T source)
        {
            var serialized = JsonConvert.SerializeObject(source);
            return JsonConvert.DeserializeObject<T>(serialized);
        }


        public static bool IsDefault<T>(this T @object) => EqualityComparer<T>.Default.Equals(@object, default);

        public static bool PublicPropertiesEquals<T>(this object self, object to, params string[] ignore)
        {
            if (self != null && to != null)
            {
                List<string> ignoreList = new List<string>(ignore);
                foreach (System.Reflection.PropertyInfo pi in typeof(T).GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance))
                {
                    if (!ignoreList.Contains(pi.Name))
                    {
                        object selfValue = typeof(T).GetProperty(pi.Name).GetValue(self, null);
                        object toValue = typeof(T).GetProperty(pi.Name).GetValue(to, null);

                        if (selfValue != toValue && (selfValue == null || !selfValue.Equals(toValue)))
                        {
                            return false;
                        }
                    }
                }
                return true;
            }
            return self == to;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Core.Cache
{
    public interface ICache<T, TKey>
    {
        T GetItem(TKey key);

        void Remove(TKey key);
    }
}

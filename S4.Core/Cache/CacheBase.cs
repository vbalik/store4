﻿using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Specialized;


namespace S4.Core.Cache
{
    public class CacheBase<T>
    {
        private MemoryCache _cache = null;
        private int _expireSeconds = 0;


        public CacheBase()
        {
            _init(60);
        }

        public CacheBase(int expireSeconds)
        {
            _init(expireSeconds);
        }


        private void _init(int expireSeconds)
        {
            _expireSeconds = expireSeconds;
            _cache = new MemoryCache(
                new MemoryCacheOptions
                {
                    ExpirationScanFrequency = new TimeSpan(0, 0, 5)
                });
        }


        protected T GetOrAddExisting(string key, Func<T> valueFactory)
        {
            var value = _cache.GetOrCreate(key, entry =>
            {
                entry.AbsoluteExpiration = DateTime.Now.AddSeconds(_expireSeconds);
                return valueFactory();
            });
            return value;
        }

        protected void Remove(string key)
        {
            _cache.Remove(key);
        }
    }
}

using System;
using Xunit;
using S4mp.Core.Misc;
using S4mp.Core.EANCode;

namespace S4mp.Test
{
    public class ViewModelsTest
    {
        [Fact]
        public void GetBatchNumberTest()
        {
            var ean = EANCodeFactory.Create("]C10105944706000986[GS]10JE0107[GS]17230731");
            Assert.Equal(3, ean.Parts.Count);
            Assert.Equal("JE0107", ViewModelHelpers.GetBatchNumber(ean));

            var ean2 = EANCodeFactory.Create("]C101059447060009862191020938534673[GS]17230731");
            Assert.Equal(2, ean2.Parts.Count);
            Assert.Equal("5944706000986", ViewModelHelpers.GetBatchNumber(ean2));

            var ean3 = EANCodeFactory.Create("]C101059447060009862191020938534673[GS]10JE0107[GS]17230731");
            Assert.Equal(3, ean3.Parts.Count);
            Assert.Equal("JE0107", ViewModelHelpers.GetBatchNumber(ean3));
        }

        [Fact]
        public void GetBatchNumberNullTest()
        {
            var x = new S4.Core.EAN.EAN("]C10105944706000986[GS]17230731");
            var ean = EANCodeFactory.Create("]C10105944706000986[GS]17230731");
            Assert.Equal(2, ean.Parts.Count);
            // serial number instead of batch, used for Medtronik
            Assert.Equal("5944706000986", ViewModelHelpers.GetBatchNumber(ean));
        }

        [Theory]
        [InlineData("02.02.2022")]
        [InlineData("02.02.22")]
        [InlineData("2.02.22")]
        [InlineData("02.2.22")]
        [InlineData("2.2.22")]
        [InlineData("2.22")]
        public void ParseExpirationDateTest(string date)
        {
            Assert.NotNull(ViewModelHelpers.ParseExpirationDate(date));
        }

        [Theory]
        [InlineData("022.02.2022")]
        [InlineData("02.022.22")]
        [InlineData("2.02.222")]
        [InlineData("222.2.22")]
        [InlineData("2,2,22")]
        [InlineData("2")]
        public void ParseExpirationBadDateTest(string date)
        {
            Assert.Null(ViewModelHelpers.ParseExpirationDate(date));
        }

        [Fact]
        public void ParseExpirationDateWithoutYearTest()
        {
            Assert.Equal("2.2.2022", ViewModelHelpers.ParseExpirationDate("2.2.22").Value.ToString("d.M.yyyy"));
        }
    }
}

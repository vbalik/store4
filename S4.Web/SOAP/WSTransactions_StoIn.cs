﻿using S4.Web.SOAP.S3Params;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;
using S4.Core.Extensions;
using S4.Core.Data;
using S4.Entities;
using S4.ProcedureModels.StoreIn;
using S4.Procedures.StoreIn;


namespace S4.Web.SOAP
{
    public partial class WSTransactions
    {
        [OperationContract(Action = ACTION_URL + nameof(StoIn_ListDirects))]
        public Store3.BLL.TransResult StoIn_ListDirects(ref StoIn_ListDirectsParams pars)
        {
            var proc = new StoreInListDirectsProc(GetUserID());
            var result = proc.DoProcedure(new StoreInListDirects() { WorkerID = pars.workerID });

            if (result.Directions != null)
                pars.directions = result.Directions.Select(s => new DocumentInfo() { ID = s.DocumentID, Description = s.DocumentName, XtraDescription = s.DocumentDetail }).ToArray();
            else
                pars.directions = new DocumentInfo[0];
            SetTransResult(pars, result);

            return pars.trResult;
        }


        [OperationContract(Action = ACTION_URL + nameof(StoIn_Proc))]
        public Store3.BLL.TransResult StoIn_Proc(ref StoIn_ProcParams pars)
        {
            var proc = new StoreInProcProc(GetUserID());
            var result = proc.DoProcedure(new StoreInProc() { DirectionID = pars.directionID });            
            SetTransResult(pars, result);

            return pars.trResult;
        }


        [OperationContract(Action = ACTION_URL + nameof(StoIn_ListNoCarr))]
        public Store3.BLL.TransResult StoIn_ListNoCarr(ref StoIn_ListNoCarrParams pars)
        {
            var proc = new StoreInListNoCarrProc(GetUserID());
            var result = proc.DoProcedure(new StoreInListNoCarr() { DirectionID = pars.directionID, SrcPositionID = pars.srcPositionID });

            if (result.Success)
                pars.articleList = ToDocItemInfo(result.ArticleList);
            SetTransResult(pars, result);

            return pars.trResult;
        }


        [OperationContract(Action = ACTION_URL + nameof(StoIn_SaveItem))]
        public Store3.BLL.TransResult StoIn_SaveItem(ref StoIn_SaveItemParams pars)
        {
            var proc = new StoreInSaveItemProc(GetUserID());
            var result = proc.DoProcedure(new StoreInSaveItem() { StoMoveID = pars.stoMoveID, FinalPositionID = pars.finalPositionID, FinalCarrierNum = pars.finalCarrierNum });
            SetTransResult(pars, result);

            return pars.trResult;
        }


        [OperationContract(Action = ACTION_URL + nameof(StoIn_TestDirect))]
        public Store3.BLL.TransResult StoIn_TestDirect(ref StoIn_TestDirectParams pars)
        {
            var proc = new StoreInTestDirectProc(GetUserID());
            var result = proc.DoProcedure(new StoreInTestDirect() { DirectionID = pars.directionID });

            if (result.Success)
                pars.isCompleted = result.IsCompleted;
            SetTransResult(pars, result);

            return pars.trResult;
        }


        [OperationContract(Action = ACTION_URL + nameof(StoIn_AcceptPos))]
        public Store3.BLL.TransResult StoIn_AcceptPos(ref StoIn_AcceptPosParams pars)
        {
            var proc = new StoreInAcceptPosProc(GetUserID());
            var result = proc.DoProcedure(new StoreInAcceptPos()
            {
                DirectionID = pars.directionID, SrcPositionID = pars.srcPositionID,
                DestPositionID = pars.destPositionID, CarrierNum = pars.carrierNum
            });

            if (result.Success)
                pars.stoMoveID = result.StoMoveID;
            SetTransResult(pars, result);

            return pars.trResult;
        }


        [OperationContract(Action = ACTION_URL + nameof(StoIn_AcceptPosNoCarr))]
        public Store3.BLL.TransResult StoIn_AcceptPosNoCarr(ref StoIn_AcceptPosNoCarrParams pars)
        {
            var proc = new StoreInAcceptPosNoCarrProc(GetUserID());
            var result = proc.DoProcedure(new StoreInAcceptPosNoCarr()
            {
                DirectionID = pars.directionID, SrcPositionID = pars.srcPositionID,
                DestPositionID = pars.destPositionID,
                Article = ToS4ArticleInfo(pars.article)
            });

            if (result.Success)
                pars.stoMoveID = result.StoMoveID;

            SetTransResult(pars, result);

            return pars.trResult;
        }


        [OperationContract(Action = ACTION_URL + nameof(StoIn_CancelItem))]
        public Store3.BLL.TransResult StoIn_CancelItem(ref StoIn_CancelItemParams pars)
        {
            var proc = new StoreInCancelItemProc(GetUserID());
            var result = proc.DoProcedure(new StoreInCancelItem() { StoMoveID = pars.stoMoveID });
            SetTransResult(pars, result);

            return pars.trResult;
        }


        [OperationContract(Action = ACTION_URL + nameof(StoIn_CheckCarrier))]
        public Store3.BLL.TransResult StoIn_CheckCarrier(ref StoIn_CheckCarrierParams pars)
        {
            var proc = new StoreInCheckCarrierProc(GetUserID());
            var result = proc.DoProcedure(new StoreInCheckCarrier()
            {
                DirectionID = pars.directionID,
                SrcPositionID = pars.srcPositionID,
                CarrierNum = pars.carrierNum
            });

            if (result.Success)
            {
                pars.isCarrierFound = result.IsCarrierFound;
                pars.articleList = ToDocItemInfo(result.ArticleList);
            }
            SetTransResult(pars, result);

            return pars.trResult;
        }


        [OperationContract(Action = ACTION_URL + nameof(StoIn_CheckPos))]
        public Store3.BLL.TransResult StoIn_CheckPos(ref StoIn_CheckPosParams pars)
        {
            var proc = new StoreInCheckPosProc(GetUserID());
            var result = proc.DoProcedure(new StoreInCheckPos()
            {
                PositionID = pars.positionID,
                ArtPackID = pars.artPackID
            });

            if (result.Success)
            {
                pars.enabled = (result.Enabled == StoreInCheckPos.PosEnabled.Yes) ? StoIn_CheckPosParams.PosEnabled.Yes : ((result.Enabled == StoreInCheckPos.PosEnabled.Warn) ? StoIn_CheckPosParams.PosEnabled.Warn : StoIn_CheckPosParams.PosEnabled.No);
                pars.resReason = result.ResReason;
            }
            SetTransResult(pars, result);

            return pars.trResult;
        }

        
    }
}

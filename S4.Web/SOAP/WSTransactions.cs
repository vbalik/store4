﻿using S4.Core.Cache;
using S4.Core.Extensions;
using S4.Entities;
using S4.Entities.Helpers;
using S4.Entities.Models;
using S4.StoreCore.Interfaces;
using S4.Web.SOAP.S3Params;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Threading.Tasks;


namespace S4.Web.SOAP
{
    [ServiceContract(Namespace = ACTION_URL)]
    public partial class WSTransactions
    {
        public const string ACTION_URL = "http://www.servis2.com/store3/";

        public const string S3_EMPTY_CARRIER = "_NONE_";
        public const string S3_EMPTY_BATCH = "?";


        private NLog.Logger Logger = NLog.LogManager.GetLogger("WSTransactions");


        private readonly StoreCore.Interfaces.IOnStore _onStore;
        private readonly DAL.ArticleDAL _articleDAL;     
        private readonly DAL.PositionDAL _positionDAL;
        private readonly DAL.DirectionAssignDAL _directionAssignDAL;
        private readonly ICache<Entities.ArticlePacking, int> _articlePackingCache;
        private readonly ICache<Entities.Models.ArticleModel, int> _articleModelCache;
        private readonly ICache<Entities.Models.SectionModel, string> _sectionModelCache;
        private readonly ICache<PartnerAddress, int> _partnerAddressCache;

        public WSTransactions(StoreCore.Interfaces.IOnStore onStore, DAL.ArticleDAL articleDAL, DAL.PositionDAL positionDAL, DAL.DirectionAssignDAL directionAssignDAL, ICache<Entities.ArticlePacking, int> articlePackingCache, ICache<Entities.Models.ArticleModel, int> articleModelCache, ICache<Entities.Models.SectionModel, string> sectionModelCache, ICache<PartnerAddress, int> partnerAddressCache)
        {
            _onStore = onStore;
            _articleDAL = articleDAL;
            _positionDAL = positionDAL;
            _directionAssignDAL = directionAssignDAL;
            _articlePackingCache = articlePackingCache;
            _articleModelCache = articleModelCache;
            _sectionModelCache = sectionModelCache;
            _partnerAddressCache = partnerAddressCache;
        }


        #region headers

        [DataContract(Namespace = WSTransactions.ACTION_URL)]
        public class AccessSoapHeader
        {
            [DataMember]
            public string userID;
        }

        public MessageHeaders MessageHeaders { get; set; }

        private string GetUserID()
        {
            var accessSoapHeader = MessageHeaders.GetHeader<AccessSoapHeader>(nameof(AccessSoapHeader), ACTION_URL);
            return accessSoapHeader?.userID;
        }

        #endregion


        [OperationContract(Action = ACTION_URL + "Access_CheckUser")]
        public string Access_CheckUser(string userName, string userPassword)
        {
            var worker = (new DAL.WorkerDAL()).GetByLogon(userName);
            if (worker == null)
                return null;
            if (worker.WorkerStatus == Entities.Worker.WK_STATUS_DISABLED)
                return null;
            if (!String.IsNullOrWhiteSpace(userPassword) && Core.Security.S3SecData.CheckPassword(worker.WorkerSecData, worker.WorkerID, userPassword))
                return worker.WorkerID;
            else
                return null;
        }


        [OperationContract(Action = ACTION_URL + "VersionExchange")]
        public void VersionExchange(string clientID, string clientVersion, out string serverVersion, out DateTime serverDateTime)
        {
            // log info
            Logger.Info($"Client info; clientID: {clientID}; clientVersion: {clientVersion}");

            // return version
            System.Version ver = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
            serverVersion = ver.ToString(4);

            // return datetime
            serverDateTime = DateTime.UtcNow;
        }



        [OperationContract(Action = ACTION_URL + "Admin_WorkerTodo")]
        public Store3.BLL.TransResult Admin_WorkerTodo(ref WorkerTodoParams pars)
        {
            var assignmets = _directionAssignDAL.ScanAssignments(pars.workerID);

            pars.receiveDocs = assignmets.Where(a => a.JobID == DirectionAssign.JOB_ID_RECEIVE).Sum(a => a.CntDocs);
            pars.stoinDocs = assignmets.Where(a => a.JobID == DirectionAssign.JOB_ID_STORE_IN).Sum(a => a.CntDocs);
            pars.dispatchDocs = assignmets.Where(a => a.JobID == DirectionAssign.JOB_ID_DISPATCH).Sum(a => a.CntDocs);
            pars.dispcheckDocs = assignmets.Where(a => a.JobID == DirectionAssign.JOB_ID_DISPATCH_CHECK).Sum(a => a.CntDocs);
            pars.trResult = Store3.BLL.TransResult.OK;

            return pars.trResult;
        }


        #region helpers

        private static string RepairCarrierNum(string carrierNum)
        {
            return (carrierNum == S3_EMPTY_CARRIER) ? null : carrierNum;
        }

        private static string RepairBatchNum(string batchNum)
        {
            return (batchNum == S3_EMPTY_BATCH) ? null : batchNum;
        }

        private static DateTime? RepairExpirationDate(DateTime expirationDate)
        {
            return (expirationDate == DateTime.MinValue) ? (DateTime?)null : expirationDate;
        }

        private static readonly DateTime MIN_EXPIRATION_DATE = new DateTime(2000, 1, 1);
        private static readonly DateTime MAX_EXPIRATION_DATE = new DateTime(2200, 1, 1);
        private static DateTime? CheckExpirationDate(DateTime? expirationDate)
        {
            if (expirationDate.HasValue)
            {
                if (expirationDate.Value < MIN_EXPIRATION_DATE || expirationDate.Value > MAX_EXPIRATION_DATE)
                    throw new Exception($"ExpirationDate out of range; {expirationDate.Value}");
            }
            return expirationDate;
        }


        private static void SetTransResult(S3ParamsBase pars, ProcedureModels.ProceduresModel result)
        {
            pars.trResult = (result.Success) ? Store3.BLL.TransResult.OK : Store3.BLL.TransResult.BussinesLogicError;
            pars.trErrorDesc = result.ErrorText;
        }


        private static ArticleFullInfo[] ToArticleFullInfo(List<ArticleModel> articles)
        {
            return articles.ToS3((Action<ArticleModel, ArticleFullInfo>)((s, d) =>
            {
                ToS3_Article(s, d);
            })).ToArray();
        }


        private static void ToS3_Article(ArticleModel s, ArticleFullInfo d)
        {
            d.articleStatus = (s.ArticleStatus == Article.AR_STATUS_OK) ? (byte)0 : (byte)255;
            d.packingList = s.Packings.ToS3<ArticlePacking, PackingInfo>((s1, d1) =>
            {
                d1.packStatus = (s1.PackStatus == ArticlePacking.AP_STATUS_OK) ? (byte)0 : (byte)255;
                d1.packRelation = s1.PackRelation;
            }).ToArray();
        }


        private static void ToS3_Position(PositionModel s, PositionInfo d)
        {
            switch (s.PosStatus)
            {
                case Position.POS_STATUS_OK:
                    d.posStatus = 0;
                    break;

                case Position.POS_STATUS_BLOCKED:
                    d.posStatus = 200;
                    break;

                case Position.POS_STATUS_STOCKTAKING:
                    d.posStatus = 210;
                    break;
            }

            d.sections = s.PositionSections.ToS3<PositionSection, SectionInfo>((s1, d1) =>
            {
                d1.sectDesc = s1.SectID;
            }).ToArray();
        }


        private DocItemInfo[] ToDocItemInfo(IEnumerable<OnStoreItemFull> storeContent, bool includeAllPackings = false)
        {
            var result = storeContent.Select(i => new DocItemInfo()
            {
                artPackID = i.ArtPackID,
                quantity = i.Quantity,
                batchNum = i.BatchNum,
                expirationDate = i.ExpirationDate ?? DateTime.MinValue,
                carrierNum = i.CarrierNum,
                articleID = i.ArticleID,
                packDesc = i.ArticlePacking.PackDesc,
                articleCode = i.Article.ArticleCode,
                articleDesc = i.Article.ArticleDesc,
                useBatch = i.Article.UseBatch,
                useExpiration = i.Article.UseExpiration,
                from_positionID = i.PositionID,
                from_posCode = i.Position.PosCode,
                xtraString = i.Position.PosCode
            }).ToArray();

            //if (includeAllPackings)
            //{
            //    foreach (var item in result)
            //    {

            //    }
            //    //info.packingList = new PackingInfo[article.PackingList.Count];
            //    //for (int i = 0; i < article.PackingList.Count; i++)
            //    //    info.packingList[i] = new PackingInfo(article.PackingList[i]);
            //}

            return result;
        }


        private DocItemInfo[] ToDocItemInfo(List<ArticleInfo> articles)
        {
            var result = articles.ToS3<Entities.Helpers.ArticleInfo, DocItemInfo>((s, d) =>
            {
                d.quantity = s.Quantity;
                d.carrierNum = s.CarrierNum ?? S3_EMPTY_CARRIER;
                d.batchNum = s.BatchNum ?? S3_EMPTY_BATCH;
                d.expirationDate = s.ExpirationDate ?? DateTime.MinValue;
            }).ToArray();

            foreach (var item in result)
            {
                var article = _articleModelCache.GetItem(item.articleID);
                item.useBatch = article.UseBatch;
                item.useExpiration = article.UseExpiration;
            }

            return result;
        }


        private DocItemInfo[] ToDocItemInfo(List<ItemInfo> items)
        {
            var result = items.ToS3<Entities.Helpers.ItemInfo, DocItemInfo>((s, d) =>
            {
                d.quantity = s.Quantity;
                d.carrierNum = s.CarrierNum ?? S3_EMPTY_CARRIER;
                d.batchNum = s.BatchNum ?? S3_EMPTY_BATCH;
                d.expirationDate = s.ExpirationDate ?? DateTime.MinValue;
                d.remark = s.Remark;
            }).ToArray();

            foreach (var item in result)
            {
                if (item.articleID == 0)
                {
                    var packing = _articlePackingCache.GetItem(item.artPackID);
                    item.articleID = packing.ArticleID;
                }
                var article = _articleModelCache.GetItem(item.articleID);
                item.useBatch = article.UseBatch;
                item.useExpiration = article.UseExpiration;
            }

            return result;
        }


        private List<ArticleInfo> ToArticleInfo(DocItemInfo[] docItemInfo)
        {
            var result = docItemInfo.Select(i => ToS4ArticleInfo(i)).ToList();
            return result;
        }


        private ArticleInfo ToS4ArticleInfo(DocItemInfo docItemInfo)
        {
            return new ArticleInfo()
            {
                ArtPackID = docItemInfo.artPackID,
                Quantity = docItemInfo.quantity,
                BatchNum = RepairBatchNum(docItemInfo.batchNum),
                ExpirationDate = CheckExpirationDate(RepairExpirationDate(docItemInfo.expirationDate)),
                CarrierNum = RepairCarrierNum(docItemInfo.carrierNum),
                ArticleID = docItemInfo.articleID,
                PackDesc = docItemInfo.packDesc,
                ArticleCode = docItemInfo.articleCode,
                ArticleDesc = docItemInfo.articleDesc
            };
        }


        private ProcedureModels.PositionErrorReason ToS4PositionErrorReason(S3Params.PositionErrorReason reason)
        {
            switch (reason)
            {
                case PositionErrorReason.IsFullOrEmpty:
                    return ProcedureModels.PositionErrorReason.IsFullOrEmpty;
                case PositionErrorReason.NotUsable:
                    return ProcedureModels.PositionErrorReason.NotUsable;
                case PositionErrorReason.Other:
                    return ProcedureModels.PositionErrorReason.Other;
                case PositionErrorReason.None:
                default:
                    throw new NotSupportedException(reason.ToString());
            }
        }

        #endregion
    }
}

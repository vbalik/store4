﻿using S4.Web.SOAP.S3Params;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;

namespace S4.Web.SOAP
{
    public partial class WSTransactions
    {
        [OperationContract(Action = ACTION_URL + nameof(DirectedMove_ListDirects))]
        public Store3.BLL.TransResult DirectedMove_ListDirects(ref DirectedMove_ListDirectsParams pars)
        {
            // fake implementation
            pars.directions = new DocumentInfo[0];
            pars.trResult = Store3.BLL.TransResult.OK;
            return pars.trResult;
        }
    }
}

﻿using S4.Web.SOAP.S3Params;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;
using S4.Core.Extensions;
using S4.Core.Data;
using S4.Entities;
using S4.ProcedureModels.Exped;
using S4.Procedures.Exped;


namespace S4.Web.SOAP
{
    public partial class WSTransactions
    {
        [OperationContract(Action = ACTION_URL + nameof(Exped_ListDirects))]
        public Store3.BLL.TransResult Exped_ListDirects(ref Exped_ListDirectsParams pars)
        {
            var proc = new ExpedListDirectsProc(GetUserID());
            var result = proc.DoProcedure(new ExpedListDirects());

            if (result.Directions != null)
                pars.directions = result.Directions.Select(s => new DocumentInfo() { ID = s.DocumentID, Description = s.DocumentName, XtraDescription = s.DocumentDetail }).ToArray();
            else
                pars.directions = new DocumentInfo[0];
            SetTransResult(pars, result);

            return pars.trResult;
        }


        [OperationContract(Action = ACTION_URL + nameof(Exped_Done))]
        public Store3.BLL.TransResult Exped_Done(ref Exped_DoneParams pars)
        {
            var proc = new ExpedDoneProc(GetUserID());
            var result = proc.DoProcedure(new ExpedDone() { DirectionID = pars.directionID, ExpedTruck = pars.expedTruck });
            SetTransResult(pars, result);

            return pars.trResult;
        }
    }
}

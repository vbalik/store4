﻿using System;
using System.Runtime.Serialization;


namespace S4.Web.SOAP.S3Params
{
    [DataContract(Namespace = WSTransactions.ACTION_URL)]
    public class PackingInfo
    {
        [DataMember]
        public int artPackID;
        [DataMember(EmitDefaultValue = false)]
        public string packDesc;
        [DataMember]
        public decimal packRelation;
        [DataMember]
        public bool movablePack;
        [DataMember(EmitDefaultValue = false)]
        public string source_id;
        [DataMember]
        public byte packStatus;
        [DataMember]
        public byte barCodeType;
        [DataMember(EmitDefaultValue = false)]
        public string barCode;
        [DataMember]
        public bool inclCarrier;
        [DataMember]
        public decimal packWeight;
        [DataMember]
        public decimal packVolume;
    }
}

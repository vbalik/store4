﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace S4.Web.SOAP.S3Params
{
    [DataContract(Namespace = WSTransactions.ACTION_URL)]
    public class PositionInfo
    {
        [DataMember]
        public int positionID;
        [DataMember(EmitDefaultValue = false)]
        public string posCode;
        [DataMember(EmitDefaultValue = false)]
        public string posDesc;
        [DataMember(EmitDefaultValue = false)]
        public string houseID;
        [DataMember(EmitDefaultValue = false)]
        public string houseDesc;
        [DataMember]
        public byte posCateg;
        [DataMember]
        public byte posStatus;
        [DataMember]
        public byte posHeight;
        [DataMember]
        public int posX;
        [DataMember]
        public int posY;
        [DataMember]
        public int posZ;
        [DataMember(EmitDefaultValue = false)]
        public string mapPos;
        [DataMember]
        public SectionInfo[] sections;
    }
}

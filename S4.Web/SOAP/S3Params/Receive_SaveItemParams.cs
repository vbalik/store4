﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace S4.Web.SOAP.S3Params
{
    [DataContract(Namespace = WSTransactions.ACTION_URL)]
    public class Receive_SaveItemParams : S3ParamsBase
    {
        // in
        [DataMember(EmitDefaultValue = false, Order = 1)]
        public int stoMoveID;
        [DataMember(EmitDefaultValue = false, Order = 2)]
        public int positionID;
        [DataMember(EmitDefaultValue = false, Order = 3)]
        public int artPackID;
        [DataMember(EmitDefaultValue = false, Order = 4)]
        public string carrierNum;
        [DataMember(EmitDefaultValue = false, Order = 5)]
        public decimal quantity;
        [DataMember(EmitDefaultValue = false, Order = 6)]
        public string batchNum;
        [DataMember(EmitDefaultValue = false, Order = 7)]
        public DateTime expirationDate;

        // out
        [DataMember(EmitDefaultValue = false, Order = 8)]
        public bool repeatedItem = false;
        [DataMember(EmitDefaultValue = false, Order = 9)]
        public int stoMoveItemID;
    }
}

﻿using System;
using System.Runtime.Serialization;


namespace S4.Web.SOAP.S3Params
{
    [DataContract(Namespace = WSTransactions.ACTION_URL)]
    public class Stocktaking_ListOpenedParams : S3ParamsBase
    {
        // out
        [DataMember(Order = 1)]
        public DocumentInfo[] stocktakings;
    }
}

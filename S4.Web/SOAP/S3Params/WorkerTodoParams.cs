﻿using System;
using System.Runtime.Serialization;


namespace S4.Web.SOAP.S3Params
{
    [DataContract(Namespace = WSTransactions.ACTION_URL)]
    public class WorkerTodoParams : S3ParamsBase
    {
        // in
        [DataMember]
        public string workerID;

        // out
        [DataMember]
        public int receiveDocs;
        [DataMember]
        public int repackDocs;
        [DataMember]
        public int stoinDocs;
        [DataMember]
        public int dispatchDocs;
        [DataMember]
        public int dispcheckDocs;
        [DataMember]
        public int directMoves;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace S4.Web.SOAP.S3Params
{
    [DataContract(Namespace = WSTransactions.ACTION_URL)]
    public abstract class S3ParamsBase
    {
        [DataMember(EmitDefaultValue = false)]
        public Store3.BLL.TransResult trResult = Store3.BLL.TransResult.None;

        [DataMember(EmitDefaultValue = false)]
        public string trErrorDesc = null;

        [DataMember(EmitDefaultValue = false)]
        public int trErrorCode;

        public string userID;
        //public Exception trException;
    }
}

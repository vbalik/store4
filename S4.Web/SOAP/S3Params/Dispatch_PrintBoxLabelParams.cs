﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace S4.Web.SOAP.S3Params
{
    [DataContract(Namespace = WSTransactions.ACTION_URL)]
    public class Dispatch_PrintBoxLabelParams : S3ParamsBase
    {
        // in
        [DataMember(Order = 1)]
        public string labelText;

        [DataMember(Order = 2)]
        public int labelsQuant;

        [DataMember(Order = 3)]
        public int directionID;

        [DataMember(Order = 4)]
        public int docPosition;

        [DataMember(Order = 5)]
        public string printerPath;

        [DataMember(Order = 6)]
        public string reportSource;

        // out
        // none
    }
}

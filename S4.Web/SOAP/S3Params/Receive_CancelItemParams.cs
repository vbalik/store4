﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace S4.Web.SOAP.S3Params
{
    [DataContract(Namespace = WSTransactions.ACTION_URL)]
    public class Receive_CancelItemParams : S3ParamsBase
    {
        // in
        [DataMember(EmitDefaultValue = false, Order = 1)]
        public int stoMoveID;
        [DataMember(EmitDefaultValue = false, Order = 2)]
        public int docPosition;

        // out
        // none
    }
}

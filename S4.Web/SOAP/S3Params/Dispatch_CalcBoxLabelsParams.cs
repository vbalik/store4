﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace S4.Web.SOAP.S3Params
{
    [DataContract(Namespace = WSTransactions.ACTION_URL)]
    public class Dispatch_CalcBoxLabelsParams : S3ParamsBase
    {
        // in
        [DataMember(Order = 1)]
        public int directionID;

        // out
        [DataMember(Order = 2)]
        public BoxLabelInfo[] boxLabels;
        [DataMember(Order = 3)]
        public ReportInfo[] reportList;
        [DataMember(Order = 4)]
        public PrinterInfo[] printersList;
    }
}

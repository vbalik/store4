﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace S4.Web.SOAP.S3Params
{
    [DataContract(Namespace = WSTransactions.ACTION_URL)]
    public class Dispatch_ItemInstructParams : S3ParamsBase
    {
        // in
        [DataMember(EmitDefaultValue = false, Order = 1)]
        public int stoMoveID;
        [DataMember(EmitDefaultValue = false, Order = 2)]
        public int stoMoveItemID;

        // out
        [DataMember(EmitDefaultValue = false, Order = 3)]
        public bool wasFound;

        [DataMember(EmitDefaultValue = false, Order = 4)]
        public PositionInfo scrPosition;
        [DataMember(EmitDefaultValue = false, Order = 5)]
        public PositionInfo destPosition;
        [DataMember(EmitDefaultValue = false, Order = 6)]
        public DocItemInfo article;
    }
}

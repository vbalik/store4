﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace S4.Web.SOAP.S3Params
{
    [DataContract(Namespace = WSTransactions.ACTION_URL)]
    public class StoIn_CheckPosParams : S3ParamsBase
    {
        public enum PosEnabled
        {
            Yes,
            Warn,
            No
        }


        // in
        [DataMember(Order = 1)]
        public int positionID;

        [DataMember(Order = 2)]
        public int artPackID;

        // out
        [DataMember(Order = 3)]
        public PosEnabled enabled;

        [DataMember(Order = 4)]
        public string resReason;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace S4.Web.SOAP.S3Params
{
    [DataContract(Namespace = WSTransactions.ACTION_URL)]
    public class BoxLabelInfo
    {
        [DataMember(Order = 1)]
        public string labelText;
        [DataMember(Order = 2)]
        public int labelQuantity;
        [DataMember(Order = 3)]
        public int directionID;
        [DataMember(Order = 4)]
        public int docPosition;
    }
}

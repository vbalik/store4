﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace S4.Web.SOAP.S3Params
{
    [DataContract(Namespace = WSTransactions.ACTION_URL)]
    public class Receive_ProcManParams : S3ParamsBase
    {
        // in
        [DataMember(EmitDefaultValue = false, Order = 1)]
        public string workerID;
        [DataMember(EmitDefaultValue = false, Order = 2)]
        public string docNumPrefix;

        // out
        [DataMember(EmitDefaultValue = false, Order = 3)]
        public int directionID;
        [DataMember(EmitDefaultValue = false, Order = 4)]
        public int stoMoveID;
    }
}

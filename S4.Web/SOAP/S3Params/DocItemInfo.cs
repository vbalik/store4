﻿using System;
using System.Runtime.Serialization;


namespace S4.Web.SOAP.S3Params
{
    [DataContract(Namespace = WSTransactions.ACTION_URL)]
    public class DocItemInfo
    {
        [DataMember(Order = 1)]
        public int articleID;
        [DataMember(Order = 2)]
        public int artPackID;
        [DataMember(EmitDefaultValue = false, Order = 3)]
        public string articleCode;
        [DataMember(EmitDefaultValue = false, Order = 4)]
        public string articleDesc;
        [DataMember(EmitDefaultValue = false, Order = 5)]
        public string packDesc;
        [DataMember(Order = 6)]
        public bool useBatch;
        [DataMember(Order = 7)]
        public bool useExpiration;
        [DataMember(Order = 8)]
        public decimal quantity;
        [DataMember(EmitDefaultValue = false, Order = 9)]
        public string batchNum;
        [DataMember(EmitDefaultValue = false, Order = 10)]
        public System.DateTime expirationDate;
        [DataMember(EmitDefaultValue = false, Order = 11)]
        public string carrierNum;
        [DataMember(Order = 12)]
        public bool wholeCarrier;
        [DataMember(Order = 13)]
        public int from_positionID;
        [DataMember(EmitDefaultValue = false, Order = 14)]
        public string from_posCode;
        [DataMember(Order = 15)]
        public int to_positionID;
        [DataMember(EmitDefaultValue = false, Order = 16)]
        public string to_posCode;
        [DataMember(EmitDefaultValue = false, Order = 17)]
        public string remark;
        [DataMember(EmitDefaultValue = false, Order = 18)]
        public string xtraString;
        [DataMember(Order = 19)]
        public int xtraInt;
        [DataMember(Order = 20)]
        public bool xtraBool;
        [DataMember(EmitDefaultValue = false, Order = 21)]
        public System.DateTime xtraDate;
        [DataMember(Order = 22)]
        public bool specialDelivery;
        [DataMember(Order = 23)]
        public PackingInfo[] packingList;
    }
}

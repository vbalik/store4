﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace S4.Web.SOAP.S3Params
{
    [DataContract(Namespace = WSTransactions.ACTION_URL)]
    public class Move_GetPositionParams : S3ParamsBase
    {
        // in
        [DataMember(EmitDefaultValue = false, Order = 1)]
        public int positionID;

        // out
        [DataMember(EmitDefaultValue = false, Order = 2)]
        public PositionStatus posResult;
        [DataMember(EmitDefaultValue = false, Order = 3)]
        public DocItemInfo[] articles;
    }
}

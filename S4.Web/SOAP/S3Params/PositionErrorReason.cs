﻿namespace S4.Web.SOAP.S3Params
{
    public enum PositionErrorReason : byte
    {
        None = 0,
        IsFullOrEmpty = 1,
        NotUsable = 2,
        Other = 3
    }
}
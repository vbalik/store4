﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace S4.Web.SOAP.S3Params
{
    [DataContract(Namespace = WSTransactions.ACTION_URL)]
    public class Move_CheckItemsParams : S3ParamsBase
    {
        // in
        [DataMember(EmitDefaultValue = false, Order = 1)]
        public int positionID;
        [DataMember(EmitDefaultValue = false, Order = 2)]
        public DocItemInfo[] articles;

        // out
        [DataMember(EmitDefaultValue = false, Order = 3)]
        public PositionStatus posResult;
        [DataMember(EmitDefaultValue = false, Order = 4)]
        public bool itemExists;

    }
}

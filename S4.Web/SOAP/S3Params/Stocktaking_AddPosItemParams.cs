﻿using System;
using System.Runtime.Serialization;


namespace S4.Web.SOAP.S3Params
{
    [DataContract(Namespace = WSTransactions.ACTION_URL)]
    public class Stocktaking_AddPosItemParams : S3ParamsBase
    {
        // in
        [DataMember(Order = 1)]
        public int stotakID;
        [DataMember(Order = 2)]
        public int positionID;
        [DataMember(Order = 3)]
        public int artPackID;
        [DataMember(EmitDefaultValue = false, Order = 4)]
        public string carrierNum;
        [DataMember(Order = 5)]
        public decimal quantity;
        [DataMember(EmitDefaultValue = false, Order = 6)]
        public string batchNum;
        [DataMember(Order = 7)]
        public System.DateTime expirationDate;
        [DataMember(Order = 8)]
        public bool badPosition;
        [DataMember(Order = 9)]
        public int stotakItemID;
    }
}

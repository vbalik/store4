﻿using System;
using System.Runtime.Serialization;


namespace S4.Web.SOAP.S3Params
{
    [DataContract(Namespace = WSTransactions.ACTION_URL)]
    public class Stocktaking_DeletePosItemParams : S3ParamsBase
    {
        // in
        [DataMember(Order = 1)]
        public int stotakID;
        [DataMember(Order = 2)]
        public int positionID;
        [DataMember(Order = 3)]
        public int stotakItemID;
        [DataMember(Order = 4)]
        public bool badPosition;
    }
}

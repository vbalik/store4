﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace S4.Web.SOAP.S3Params
{
    [DataContract(Namespace = WSTransactions.ACTION_URL)]
    public class Dispatch_ProcParams : S3ParamsBase
    {
        // in
        [DataMember(Order = 1)]
        public int directionID;
        [DataMember(Order = 2)]
        public bool doNotSave;

        // out
        [DataMember(Order = 3)]
        public bool isPrepared;
        [DataMember(Order = 4)]
        public bool isFromPrepPos;
        [DataMember(Order = 5)]
        public string notFoundInfo;
        [DataMember(Order = 6)]
        public int stoMoveID;
        [DataMember(Order = 7)]
        public bool useRemarks;
        //[DataMember(Order = 8)]
        //public NotFoudInfo[] notFound;
        [DataMember(Order = 9)]
        public string[] moveRemarks;
        [DataMember(Order = 10)]
        public string dispatchLogText;
    }
}

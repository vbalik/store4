﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace S4.Web.SOAP.S3Params
{
    [DataContract(Namespace = WSTransactions.ACTION_URL)]
    public class PrinterInfo
    {
        [DataMember]
        public string printerName;
        [DataMember]
        public string printerPath;
        [DataMember]
        public string printerSelector;
        [DataMember]
        public string printerCategory;
        [DataMember]
        public bool preferedPrinter;
    }
}

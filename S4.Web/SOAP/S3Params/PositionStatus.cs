﻿namespace S4.Web.SOAP.S3Params
{
    public enum PositionStatus
    {
        OK = 0,
        Locked = 1,
        NotSaved = 2
    }
}
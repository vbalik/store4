﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace S4.Web.SOAP.S3Params
{
    [DataContract(Namespace = WSTransactions.ACTION_URL)]
    public class Dispatch_PosErrorParams : S3ParamsBase
    {
        // in
        [DataMember(Order = 1)]
        public int stoMoveID;

        [DataMember(Order = 2)]
        public int stoMoveItemID;

        [DataMember(Order = 3)]
        public int positionID;

        [DataMember(Order = 4)]
        public PositionErrorReason reason;

        // out
        // none
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace S4.Web.SOAP.S3Params
{
    [DataContract(Namespace = WSTransactions.ACTION_URL)]
    public class SetBarcode_RePrintCarrierLabelParams : S3ParamsBase
    {
        // in
        [DataMember(Order = 1)]
        public string carrierNum;
        [DataMember(Order = 2)]
        public string printerPath;

        // out
        // none
    }
}

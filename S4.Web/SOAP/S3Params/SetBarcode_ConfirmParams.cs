﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace S4.Web.SOAP.S3Params
{
    [DataContract(Namespace = WSTransactions.ACTION_URL)]
    public class SetBarcode_ConfirmParams : S3ParamsBase
    {
        // in
        [DataMember(Order = 1)]
        public int artPackID;

        // out
        // none
    }
}

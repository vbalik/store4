﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace S4.Web.SOAP.S3Params
{
    [DataContract(Namespace = WSTransactions.ACTION_URL)]
    public class Move_SaveParams : S3ParamsBase
    {
        // in
        [DataMember(EmitDefaultValue = false, Order = 1)]
        public int source_positionID;
        [DataMember(EmitDefaultValue = false, Order = 2)]
        public int dest_positionID;
        [DataMember(EmitDefaultValue = false, Order = 3)]
        public bool changeCarrier;
        [DataMember(EmitDefaultValue = false, Order = 4)]
        public string dest_carrierNum;
        [DataMember(EmitDefaultValue = false, Order = 5)]
        public DocItemInfo[] articles;

        // out
        [DataMember(EmitDefaultValue = false, Order = 6)]
        public PositionStatus source_posResult;
        [DataMember(EmitDefaultValue = false, Order = 7)]
        public PositionStatus dest_posResult;
        [DataMember(EmitDefaultValue = false, Order = 8)]
        public int stoMoveID;
    }
}

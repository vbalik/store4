﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace S4.Web.SOAP.S3Params
{
    [DataContract(Namespace = WSTransactions.ACTION_URL)]
    public class DispCheck_DoneParams : S3ParamsBase
    {
        // in
        [DataMember(Order = 1)]
        public int directionID;

        // out
        // none
    }
}

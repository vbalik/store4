﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace S4.Web.SOAP.S3Params
{
    [DataContract(Namespace = WSTransactions.ACTION_URL)]
    public class StoIn_AcceptPosParams : S3ParamsBase
    {
        // in
        [DataMember(Order = 1)]
        public string carrierNum;

        [DataMember(Order = 2)]
        public int directionID;

        [DataMember(Order = 3)]
        public int srcPositionID;

        [DataMember(Order = 4)]
        public int destPositionID;

        // out
        [DataMember(Order = 5)]
        public int stoMoveID;
    }
}

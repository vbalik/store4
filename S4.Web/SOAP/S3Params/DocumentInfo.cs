﻿using System;
using System.Runtime.Serialization;


namespace S4.Web.SOAP.S3Params
{
    [DataContract(Namespace = WSTransactions.ACTION_URL)]
    public class DocumentInfo
    {
        [DataMember(Order = 1)]
        public int ID;

        [DataMember(EmitDefaultValue = false, Order = 2)]
        public string Description;

        [DataMember(EmitDefaultValue = false, Order = 3)]
        public string XtraDescription;

        [DataMember(EmitDefaultValue = false, Order = 4)]
        public string DocStatus;
    }
}

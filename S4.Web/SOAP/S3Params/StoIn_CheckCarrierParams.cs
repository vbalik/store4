﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace S4.Web.SOAP.S3Params
{
    [DataContract(Namespace = WSTransactions.ACTION_URL)]
    public class StoIn_CheckCarrierParams : S3ParamsBase
    {
        // in
        [DataMember(Order = 1)]
        public int directionID;

        [DataMember(Order = 2)]
        public string carrierNum;

        [DataMember(Order = 3)]
        public int srcPositionID;

        // out
        [DataMember(Order = 4)]
        public bool isCarrierFound;

        [DataMember(Order = 5)]
        public DocItemInfo[] articleList;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace S4.Web.SOAP.S3Params
{
    [DataContract(Namespace = WSTransactions.ACTION_URL)]
    public class ArticleFullInfo
    {
        [DataMember]
        public int articleID;
        [DataMember(EmitDefaultValue = false)]
        public string articleCode;
        [DataMember(EmitDefaultValue = false)]
        public string articleDesc;
        [DataMember]
        public byte articleStatus;
        [DataMember]
        public byte articleType;
        [DataMember(EmitDefaultValue = false)]
        public string source_id;
        [DataMember(EmitDefaultValue = false)]
        public string sectID;
        [DataMember(EmitDefaultValue = false)]
        public string unitDesc;
        [DataMember(EmitDefaultValue = false)]
        public string manufID;
        [DataMember]
        public bool useBatch;
        [DataMember]
        public bool mixBatch;
        [DataMember]
        public bool useExpiration;
        [DataMember]
        public bool checkIncome;
        [DataMember(EmitDefaultValue = false)]
        public string incomeReport;
        [DataMember]
        public PackingInfo[] packingList;
    }
}

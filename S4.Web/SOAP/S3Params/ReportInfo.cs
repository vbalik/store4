﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace S4.Web.SOAP.S3Params
{
    [DataContract(Namespace = WSTransactions.ACTION_URL)]
    public class ReportInfo
    {
        [DataMember(Order = 1)]
        public string reportName;
        [DataMember(Order = 2)]
        public string reportSource;
        [DataMember(Order = 3)]
        public string printerCategory;
        [DataMember(Order = 4)]
        public string printerPath;
    }
}

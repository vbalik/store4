﻿using S4.Web.SOAP.S3Params;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;
using S4.Procedures.StockTaking;
using S4.ProcedureModels.StockTaking;
using S4.Core.Extensions;
using S4.Core.Data;
using S4.Entities;


namespace S4.Web.SOAP
{
    public partial class WSTransactions
    {
        [OperationContract(Action = ACTION_URL + "Stocktaking_ListOpened")]
        public Store3.BLL.TransResult Stocktaking_ListOpened(ref Stocktaking_ListOpenedParams pars)
        {
            var proc = new StockTakingListOpenedProc(GetUserID());
            var result = proc.DoProcedure(new StockTakingListOpened());

            pars.stocktakings = result.StockTakingList.Select(s => new DocumentInfo() { ID = s.StotakID, Description = s.StotakDesc, XtraDescription = s.BeginDate.ToLongDateString() }).ToArray();
            SetTransResult(pars, result);

            return pars.trResult;
        }


        [OperationContract(Action = ACTION_URL + "Stocktaking_AddPosItem")]
        public Store3.BLL.TransResult Stocktaking_AddPosItem(ref Stocktaking_AddPosItemParams pars)
        {
            var proc = new StockTakingAddPosItemProc(GetUserID());
            var proxy = new EmptyDataProxy();
            var result = proc.DoProcedure(new StockTakingAddPosItem() {
                StotakID = pars.stotakID,
                PositionID = pars.positionID,
                ArtPackID = pars.artPackID,
                CarrierNum = RepairCarrierNum(pars.carrierNum),
                Quantity = pars.quantity,
                BatchNum = RepairBatchNum(pars.batchNum),
                ExpirationDate = CheckExpirationDate(RepairExpirationDate(pars.expirationDate))
            } );

            pars.badPosition = result.BadPosition;
            pars.stotakItemID = result.StotakItemID;
            SetTransResult(pars, result);

            return pars.trResult;
        }


        [OperationContract(Action = ACTION_URL + "Stocktaking_ClearPos")]
        public Store3.BLL.TransResult Stocktaking_ClearPos(ref Stocktaking_ClearPosParams pars)
        {
            var proc = new StockTakingClearPosProc(GetUserID());
            var result = proc.DoProcedure(new StockTakingClearPos() {
                StotakID = pars.stotakID,
                PositionID = pars.positionID,
            });

            pars.badPosition = result.BadPosition;
            SetTransResult(pars, result);

            return pars.trResult;
        }


        [OperationContract(Action = ACTION_URL + "Stocktaking_DeletePosItem")]
        public Store3.BLL.TransResult Stocktaking_DeletePosItem(ref Stocktaking_DeletePosItemParams pars)
        {
            var proc = new StockTakingDeletePosItemProc(GetUserID());
            var result = proc.DoProcedure(new StockTakingDeletePosItem() {
                StotakID = pars.stotakID,
                PositionID = pars.positionID,
                StotakItemID = pars.stotakItemID
            });

            pars.badPosition = result.BadPosition;
            SetTransResult(pars, result);

            return pars.trResult;
        }


        [OperationContract(Action = ACTION_URL + "Stocktaking_PositionOK")]
        public Store3.BLL.TransResult Stocktaking_PositionOK(ref Stocktaking_PositionOKParams pars)
        {
            var proc = new StockTakingPositionOKProc(GetUserID());
            var result = proc.DoProcedure(new StockTakingPositionOK() {
                StotakID = pars.stotakID,
                PositionID = pars.positionID
            });

            pars.badPosition = result.BadPosition;
            SetTransResult(pars, result);

            return pars.trResult;
        }


        [OperationContract(Action = ACTION_URL + "Stocktaking_ScanPos")]
        public Store3.BLL.TransResult Stocktaking_ScanPos(ref Stocktaking_ScanPosParams pars)
        {
            var proc = new StockTakingScanPosProc(GetUserID());
            var result = proc.DoProcedure(new StockTakingScanPos() {
                StotakID = pars.stotakID,
                PositionID = pars.positionID
            });

            pars.badPosition = result.BadPosition;
            if (!result.BadPosition)
            {
                pars.articles = result.StockTakingItems.ToS3<Entities.StockTakingItem, DocItemInfo>((s, d) =>
                {
                    var packing = _articlePackingCache.GetItem(s.ArtPackID);
                    var article = _articleModelCache.GetItem(packing.ArticleID);
                    d.articleID = article.ArticleID;
                    d.articleCode = article.ArticleCode;
                    d.articleDesc = article.ArticleDesc;

                    d.expirationDate = (s.ExpirationDate.HasValue) ? s.ExpirationDate.Value : DateTime.MinValue;
                    d.quantity = s.Quantity;
                    d.xtraInt = s.StotakItemID;
                }).ToArray();
            }
            SetTransResult(pars, result);

            return pars.trResult;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Store3.BLL
{
    public enum TransResult
    {
        None,

        OK,

        BussinesLogicError,
        NotApproved,
        ErrNoConnection,
        ErrDbInternal,
        ErrInternal,

        NotImplemented
    }
}

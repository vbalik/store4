﻿using S4.Web.SOAP.S3Params;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;
using S4.Core.Extensions;
using S4.Core.Data;
using S4.Entities;
using S4.ProcedureModels.Receive;
using S4.Procedures.Receive;


namespace S4.Web.SOAP
{
    public partial class WSTransactions
    {
        [OperationContract(Action = ACTION_URL + nameof(Receive_ProcMan))]
        public Store3.BLL.TransResult Receive_ProcMan(ref Receive_ProcManParams pars)
        {
            var proc = new ReceiveProcManProc(GetUserID());
            var result = proc.DoProcedure(new ReceiveProcMan() { WorkerID = pars.workerID, DocNumPrefix = pars.docNumPrefix });

            if (result.Success)
            {
                pars.directionID = result.DirectionID;
                pars.stoMoveID = result.StoMoveID;
            }
            SetTransResult(pars, result);

            return pars.trResult;
        }


        [OperationContract(Action = ACTION_URL + nameof(Receive_SaveItem))]
        public Store3.BLL.TransResult Receive_SaveItem(ref Receive_SaveItemParams pars)
        {
            var proc = new ReceiveSaveItemProc(GetUserID());
            var result = proc.DoProcedure(new ReceiveSaveItem()
            {
                StoMoveID = pars.stoMoveID,
                PositionID = pars.positionID,
                ArtPackID = pars.artPackID,
                CarrierNum = RepairCarrierNum(pars.carrierNum),
                Quantity = pars.quantity,
                BatchNum = RepairBatchNum(pars.batchNum),
                ExpirationDate = CheckExpirationDate(RepairExpirationDate(pars.expirationDate))
            });

            if (result.Success)
            {
                pars.repeatedItem = result.RepeatedItem;
                pars.stoMoveItemID = result.StoMoveItemID;
            }
            SetTransResult(pars, result);

            return pars.trResult;
        }

        
        [OperationContract(Action = ACTION_URL + nameof(Receive_CancelItem))]
        public Store3.BLL.TransResult Receive_CancelItem(ref Receive_CancelItemParams pars)
        {
            var proc = new ReceiveCancelItemProc(GetUserID());
            var result = proc.DoProcedure(new ReceiveCancelItem()
            {
                StoMoveID = pars.stoMoveID,
                DocPosition = pars.docPosition
            });
            SetTransResult(pars, result);

            return pars.trResult;
        }


        [OperationContract(Action = ACTION_URL + nameof(Receive_Done))]
        public Store3.BLL.TransResult Receive_Done(ref Receive_DoneParams pars)
        {
            var proc = new ReceiveDoneProc(GetUserID());
            var result = proc.DoProcedure(new ReceiveDone() { StoMoveID = pars.stoMoveID });
            SetTransResult(pars, result);

            return pars.trResult;
        }


        [OperationContract(Action = ACTION_URL + nameof(Receive_TestDirect))]
        public Store3.BLL.TransResult Receive_TestDirect(ref Receive_TestDirectParams pars)
        {
            var proc = new ReceiveTestDirectionProc(GetUserID());
            var result = proc.DoProcedure(new ReceiveTestDirection() { DirectionID = pars.directionID });
            SetTransResult(pars, result);

            return pars.trResult;
        }


        [OperationContract(Action = ACTION_URL + nameof(Receive_Print))]
        public Store3.BLL.TransResult Receive_Print(ref Receive_PrintParams pars)
        {
            var proc = new ReceivePrintProc(GetUserID());
            var result = proc.DoProcedure(new ReceivePrint() { StoMoveID = pars.stoMoveID });
            SetTransResult(pars, result);

            return pars.trResult;
        }
    }
}

﻿using S4.Web.SOAP.S3Params;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;
using S4.Core.Data;
using S4.Entities;
using S4.ProcedureModels.Move;
using S4.Procedures.Move;
using S4.Entities.Helpers;

namespace S4.Web.SOAP
{
    public partial class WSTransactions
    {
        [OperationContract(Action = ACTION_URL + nameof(Move_LockPos))]
        public Store3.BLL.TransResult Move_LockPos(ref Move_LockPosParams pars)
        {
            var proc = new MoveLockPosProc(GetUserID());
            var result = proc.DoProcedure(new MoveLockPos() { PositionID = pars.positionID });

            if (result.Success)
                pars.posResult = ConvertPosResult(result.PositionResult);
            SetTransResult(pars, result);

            return pars.trResult;
        }


        [OperationContract(Action = ACTION_URL + nameof(Move_CheckItems))]
        public Store3.BLL.TransResult Move_CheckItems(ref Move_CheckItemsParams pars)
        {
            var proc = new MoveCheckItemsProc(GetUserID());
            var result = proc.DoProcedure(new MoveCheckItems()
            {
                PositionID = pars.positionID,
                Articles = ToArticleInfo(pars.articles)
            });

            if (result.Success)
            {
                pars.posResult = ConvertPosResult(result.PositionResult);
                pars.itemExists = result.ItemExists;
            }
            SetTransResult(pars, result);

            return pars.trResult;
        }


        [OperationContract(Action = ACTION_URL + nameof(Move_GetPosition))]
        public Store3.BLL.TransResult Move_GetPosition(ref Move_GetPositionParams pars)
        {
            var proc = new MoveGetPositionProc(GetUserID());
            var result = proc.DoProcedure(new MoveGetPosition() { PositionID = pars.positionID });

            if (result.Success)
            {
                pars.posResult = ConvertPosResult(result.PositionResult);
                pars.articles = ToDocItemInfo(result.Articles);
            }
            SetTransResult(pars, result);

            return pars.trResult;
        }


        [OperationContract(Action = ACTION_URL + nameof(Move_Save))]
        public Store3.BLL.TransResult Move_Save(ref Move_SaveParams pars)
        {
            var proc = new MoveSaveProc(GetUserID());
            var result = proc.DoProcedure(new MoveSave()
            {
                SourcePositionID = pars.source_positionID, DestPositionID = pars.dest_positionID,
                ChangeCarrier = pars.changeCarrier, DestCarrierNum = pars.dest_carrierNum,
                Articles = ToArticleInfo(pars.articles)
            });

            if (result.Success)
            {
                pars.source_posResult = ConvertPosResult(result.PositionResultSource);
                pars.dest_posResult = ConvertPosResult(result.PositionResultDest);
            }
            SetTransResult(pars, result);

            return pars.trResult;
        }


        [OperationContract(Action = ACTION_URL + nameof(Move_UnlockPos))]
        public Store3.BLL.TransResult Move_UnlockPos(ref Move_UnlockPosParams pars)
        {
            // do nothing

            pars.trResult = Store3.BLL.TransResult.OK;
            return pars.trResult;
        }


        #region helpers

        private PositionStatus ConvertPosResult(MoveStatusEnum positionResult)
        {
            switch (positionResult)
            {
                case MoveStatusEnum.OK:
                    return PositionStatus.OK;
                case MoveStatusEnum.Locked:
                    return PositionStatus.Locked;
                case MoveStatusEnum.NotSaved:
                    return PositionStatus.NotSaved;
                case MoveStatusEnum.NotFound:
                    return PositionStatus.NotSaved;
                default:
                    throw new NotImplementedException(positionResult.ToString());
            }
        }

        #endregion
    }
}

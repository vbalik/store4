﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.Extensions.Logging;
using Microsoft.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S4.Web.SOAP
{
    public class SOAPRequestLoggingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;
        private readonly RecyclableMemoryStreamManager _recyclableMemoryStreamManager;
        private const int ReadChunkBufferLength = 4096;


        public SOAPRequestLoggingMiddleware(RequestDelegate next, ILoggerFactory loggerFactory)
        {
            _next = next;
            _logger = loggerFactory.CreateLogger<SOAPRequestLoggingMiddleware>();
            _recyclableMemoryStreamManager = new RecyclableMemoryStreamManager();
        }


        public async Task Invoke(HttpContext context)
        {
            LogRequest(context.Request);
            //await LogResponseAsync(context);
            await _next.Invoke(context);
        }


        private void LogRequest(HttpRequest request)
        {
            request.EnableRewind();
            using (var requestStream = _recyclableMemoryStreamManager.GetStream())
            {
                request.Body.CopyTo(requestStream);
                _logger.LogInformation($"SOAP request body: {ReadStreamInChunks(requestStream)}");
            }
            request.Body.Position = 0;
        }


        //private async Task LogResponseAsync(HttpContext context)
        //{
        //    var originalBody = context.Response.Body;
        //    using (var responseStream = _recyclableMemoryStreamManager.GetStream())
        //    {
        //        context.Response.Body = responseStream;
        //        await _next.Invoke(context);
        //        await responseStream.CopyToAsync(originalBody);
        //        _logger.LogInformation($"Http Response Information:{Environment.NewLine}" +
        //                               $"Schema:{context.Request.Scheme} " +
        //                               $"Host: {context.Request.Host} " +
        //                               $"Path: {context.Request.Path} " +
        //                               $"QueryString: {context.Request.QueryString} " +
        //                               $"Response Body: {ReadStreamInChunks(responseStream)}");
        //    }

        //    context.Response.Body = originalBody;
        //    context.Response.Body.Position = 0;
        //}


        private static string ReadStreamInChunks(Stream stream)
        {
            stream.Seek(0, SeekOrigin.Begin);
            string result;
            using (var textWriter = new StringWriter())
            using (var reader = new StreamReader(stream))
            {
                var readChunk = new char[ReadChunkBufferLength];
                int readChunkLength;
                //do while: is useful for the last iteration in case readChunkLength < chunkLength
                do
                {
                    readChunkLength = reader.ReadBlock(readChunk, 0, ReadChunkBufferLength);
                    textWriter.Write(readChunk, 0, readChunkLength);
                } while (readChunkLength > 0);

                result = textWriter.ToString();
            }

            return result;
        }
    }
}

﻿using S4.Web.SOAP.S3Params;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;
using S4.Core.Extensions;
using S4.Core.Data;
using S4.Entities;
using S4.ProcedureModels.DispCheck;
using S4.Procedures.DispCheck;


namespace S4.Web.SOAP
{
    public partial class WSTransactions
    {
        [OperationContract(Action = ACTION_URL + nameof(DispCheck_ListDirects))]
        public Store3.BLL.TransResult DispCheck_ListDirects(ref DispCheck_ListDirectsParams pars)
        {
            var proc = new DispCheckListDirectionsProc(GetUserID());
            var result = proc.DoProcedure(new DispCheckListDirections() { WorkerID = pars.workerID });

            if (result.Directions != null)
                pars.directions = result.Directions.Select(s => new DocumentInfo() { ID = s.DocumentID, Description = s.DocumentName, XtraDescription = s.DocumentDetail }).ToArray();
            else
                pars.directions = new DocumentInfo[0];
            SetTransResult(pars, result);

            return pars.trResult;
        }


        [OperationContract(Action = ACTION_URL + nameof(DispCheck_Proc))]
        public Store3.BLL.TransResult DispCheck_Proc(ref DispCheck_ProcParams pars)
        {
            var proc = new DispCheckProcProc(GetUserID());
            var result = proc.DoProcedure(new DispCheckProc() { DirectionID = pars.directionID });

            if (result.Success)
            {
                switch (result.CheckMethod)
                {
                    case Direction.DispCheckMethodsEnum.EnterQuantity:
                        // AllItemsQuantity = 0,
                        pars.checkMethod = 0;
                        break;
                    case Direction.DispCheckMethodsEnum.ReadBarCodes:
                        // AllItemsCodes = 1,
                        pars.checkMethod = 1;
                        break;
                    case Direction.DispCheckMethodsEnum.ReadBarCodes2:
                        // AllItemsCodesWithBarCodeCheck = 2,
                        pars.checkMethod = 2;
                        break;
                    case Direction.DispCheckMethodsEnum.NotSet:
                    default:
                        throw new NotImplementedException(result.CheckMethod.ToString());
                }
            }
                
            SetTransResult(pars, result);

            return pars.trResult;
        }


        [OperationContract(Action = ACTION_URL + nameof(DispCheck_GetItems))]
        public Store3.BLL.TransResult DispCheck_GetItems(ref DispCheck_GetItemsParams pars)
        {
            var proc = new DispCheckGetItemsProc(GetUserID());
            var result = proc.DoProcedure(new DispCheckGetItems() { DirectionID = pars.directionID });

            if (result.Success)
            {
                pars.items = ToDocItemInfo(result.ItemInfoList);
                foreach (var item in pars.items)
                {
                    var articleModel = _articleModelCache.GetItem(item.articleID);
                    item.packingList = articleModel.Packings.Select(p => new PackingInfo() {
                        artPackID = p.ArtPackID,
                        packRelation = p.PackRelation,
                        movablePack = p.MovablePack,
                        source_id = p.Source_id,
                        packStatus = (p.PackStatus == ArticlePacking.AP_STATUS_OK) ? (byte)0 : (byte)255,
                        barCodeType = p.BarCodeType,
                        barCode = p.BarCode,
                        inclCarrier = p.InclCarrier,
                        packWeight = p.PackWeight,
                        packVolume = p.PackVolume,
                    }).ToArray();
                }

                pars.useDepartments = result.UseDepartments;
            }
            SetTransResult(pars, result);

            return pars.trResult;
        }


        [OperationContract(Action = ACTION_URL + nameof(DispCheck_Done))]
        public Store3.BLL.TransResult DispCheck_Done(ref DispCheck_DoneParams pars)
        {
            var proc = new DispCheckDoneProc(GetUserID());
            var result = proc.DoProcedure(new DispCheckDone() { DirectionID = pars.directionID });
            SetTransResult(pars, result);

            return pars.trResult;
        }
    }
}

﻿using S4.Web.SOAP.S3Params;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;
using S4.Core.Extensions;
using S4.Core.Data;
using S4.Entities;
using S4.ProcedureModels.BarCode;
using S4.Procedures.Barcode;


namespace S4.Web.SOAP
{
    public partial class WSTransactions
    {
        [OperationContract(Action = ACTION_URL + nameof(SetBarcode_MakeCode))]
        public Store3.BLL.TransResult SetBarcode_MakeCode(ref SetBarcode_MakeCodeParams pars)
        {
            var proc = new BarcodeMakeCodeProc(GetUserID());
            var result = proc.DoProcedure(new BarcodeMakeCode() { ArtPackID = pars.artPackID } );

            pars.barCode = result.BarCode;
            SetTransResult(pars, result);

            return pars.trResult;
        }


        [OperationContract(Action = ACTION_URL + nameof(SetBarcode_Delete))]
        public Store3.BLL.TransResult SetBarcode_Delete(ref SetBarcode_DeleteParams pars)
        {
            var proc = new BarcodeDeleteProc(GetUserID());
            var result = proc.DoProcedure(new BarcodeDelete() { ArtPackID = pars.artPackID });

            SetTransResult(pars, result);

            return pars.trResult;
        }


        [OperationContract(Action = ACTION_URL + nameof(SetBarcode_Confirm))]
        public Store3.BLL.TransResult SetBarcode_Confirm(ref SetBarcode_ConfirmParams pars)
        {
            var proc = new BarcodeConfirmProc(GetUserID());
            var result = proc.DoProcedure(new BarcodeConfirm() { ArtPackID = pars.artPackID });

            SetTransResult(pars, result);

            return pars.trResult;
        }


        [OperationContract(Action = ACTION_URL + nameof(SetBarcode_Set))]
        public Store3.BLL.TransResult SetBarcode_Set(ref SetBarcode_SetParams pars)
        {
            var proc = new BarcodeSetProc(GetUserID());
            var result = proc.DoProcedure(new BarcodeSet() { ArtPackID = pars.artPackID, BarCode = pars.barCode });

            SetTransResult(pars, result);

            return pars.trResult;
        }


        [OperationContract(Action = ACTION_URL + nameof(SetBarcode_ListPrinters))]
        public Store3.BLL.TransResult SetBarcode_ListPrinters(ref SetBarcode_ListPrintersParams pars)
        {
            Entities.PrinterType.PrinterClassEnum S4printerClass = PrinterType.PrinterClassEnum.PrinterClassA4;
            switch (pars.printerClass)
            {
                case "CARIER_LABEL":
                    S4printerClass = PrinterType.PrinterClassEnum.PrinterBarcodeWide;
                    break;
                case "PACKING_LABEL":
                    S4printerClass = PrinterType.PrinterClassEnum.PrinterBarcodeNarrow;
                    break;
                default:
                    break;
            }
            
            var proc = new BarcodeListPrintersProc(GetUserID());
            var result = proc.DoProcedure(new BarcodeListPrinters() { PrinterClass = S4printerClass });

            pars.printersList = result.PrintersList.Select(i => new PrinterInfo() { printerName = i.PrinterLocationDesc ?? "???", printerPath = i.PrinterPath }).ToArray();
            SetTransResult(pars, result);

            return pars.trResult;
        }


        [OperationContract(Action = ACTION_URL + nameof(SetBarcode_PrintLabel))]
        public Store3.BLL.TransResult SetBarcode_PrintLabel(ref SetBarcode_PrintLabelParams pars)
        {
            int printerLocationID = FindPrinterByPath(pars.printerPath);

            var proc = new BarcodePrintLabelProc(GetUserID());
            var result = proc.DoProcedure(new BarcodePrintLabel() { ArtPackID = pars.artPackID, LabelsQuant = pars.labelsQuant, PrinterLocationID = printerLocationID });

            SetTransResult(pars, result);

            return pars.trResult;
        }


        [OperationContract(Action = ACTION_URL + nameof(SetBarcode_PrintCarrierLabels))]
        public Store3.BLL.TransResult SetBarcode_PrintCarrierLabels(ref SetBarcode_PrintCarrierLabelsParams pars)
        {
            int printerLocationID = FindPrinterByPath(pars.printerPath);

            var proc = new BarcodePrintCarrierLabelsProc(GetUserID());
            var result = proc.DoProcedure(new BarcodePrintCarrierLabels() { LabelsQuant = pars.labelsQuant, PrinterLocationID = printerLocationID });

            SetTransResult(pars, result);

            return pars.trResult;
        }


        [OperationContract(Action = ACTION_URL + nameof(SetBarcode_RePrintCarrierLabel))]
        public Store3.BLL.TransResult SetBarcode_RePrintCarrierLabel(ref SetBarcode_RePrintCarrierLabelParams pars)
        {
            int printerLocationID = FindPrinterByPath(pars.printerPath);

            var proc = new BarcodeRePrintCarrierLabelProc(GetUserID());
            var result = proc.DoProcedure(new BarcodeRePrintCarrierLabel() { CarrierNum = pars.carrierNum, PrinterLocationID = printerLocationID });

            SetTransResult(pars, result);

            return pars.trResult;
        }


        private int FindPrinterByPath(string printerPath)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var printer = db.Query<PrinterLocation>().Where(p => p.PrinterPath == printerPath).SingleOrDefault();
                if (printer == null)
                    throw new Exception($"Printer not found; printerPath: '{printerPath}'");
                return printer.PrinterLocationID;
            }
        }
    }
}

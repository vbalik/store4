﻿using NPoco.ArrayExtensions;
using S4.Core.Extensions;
using S4.Entities;
using S4.Entities.Models;
using S4.StoreCore.Interfaces;
using S4.Web.SOAP.S3Params;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;


namespace S4.Web.SOAP
{
    public partial class WSTransactions
    {
        #region article

        [OperationContract(Action = ACTION_URL + nameof(GetArticle))]
        public ArticleFullInfo GetArticle(int articleID)
        {
            var article = _articleDAL.GetModel(articleID);
            if (article != null)
            {
                var result = new ArticleFullInfo();
                result.FieldsFromProperties(article);
                ToS3_Article(article, result);
                return result;
            }
            else
                return null;
        }


        [OperationContract(Action = ACTION_URL + nameof(ScanArticle_ArticleCode))]
        public ArticleFullInfo[] ScanArticle_ArticleCode(string articleCode, bool incLocked, out int resultCount)
        {
            var articles = _articleDAL.ScanByArticleCode(articleCode, incLocked);

            resultCount = articles.Count;
            return ToArticleFullInfo(articles);
        }


        [OperationContract(Action = ACTION_URL + nameof(ScanArticle_ArticleDesc))]
        public ArticleFullInfo[] ScanArticle_ArticleDesc(string articleDesc, bool incLocked, out int resultCount)
        {
            var articles = _articleDAL.ScanByArticleDesc(articleDesc, incLocked);

            resultCount = articles.Count;
            return ToArticleFullInfo(articles);
        }


        [OperationContract(Action = ACTION_URL + nameof(ScanArticle_BarCode))]
        public ArticleFullInfo[] ScanArticle_BarCode(string barCode, bool incLocked, out int resultCount)
        {
            var articles = _articleDAL.ScanByBarCode(barCode, incLocked);

            resultCount = articles.Count;
            return ToArticleFullInfo(articles);
        }

        #endregion


        #region positions

        [OperationContract(Action = ACTION_URL + nameof(ScanPosition_PosCode))]
        public PositionInfo[] ScanPosition_PosCode(string posCode)
        {
            var positions = _positionDAL.ScanByPosCode(posCode);

            var posCodeResult = positions.ToS3((Action<PositionModel, PositionInfo>)((s, d) =>
            {
                ToS3_Position(s, d);
            })).ToArray();

            return posCodeResult;
        }


        [OperationContract(Action = ACTION_URL + nameof(ScanPosition_PositionID))]
        public PositionInfo[] ScanPosition_PositionID(int positionID)
        {
            var position = _positionDAL.GetModel(positionID);
            if (position != null)
            {
                var result = new PositionInfo();
                result.FieldsFromProperties(position);
                ToS3_Position(position, result);
                return new PositionInfo[] { result };
            }
            else
                return new PositionInfo[0];
        }

        #endregion


        #region show

        [OperationContract(Action = ACTION_URL + nameof(ShowContent_ByPosition))]
        public DocItemInfo[] ShowContent_ByPosition(int positionID)
        {
            var storeContent = _onStore.OnPosition(positionID, ResultValidityEnum.Valid);
            storeContent = storeContent.OrderBy(i => i.Article.ArticleCode).ToList();

            return ToDocItemInfo(storeContent);
        }


        [OperationContract(Action = ACTION_URL + nameof(ShowContent_ByCarrier))]
        public DocItemInfo[] ShowContent_ByCarrier(string carrNumber)
        {
            var storeContent = _onStore.ByCarrier(carrNumber, ResultValidityEnum.Valid);
            storeContent = storeContent.OrderBy(i => i.Article.ArticleCode).ToList();

            return ToDocItemInfo(storeContent);
        }


        [OperationContract(Action = ACTION_URL + nameof(ShowContent_ByArticle))]
        public DocItemInfo[] ShowContent_ByArticle(int articleID, string secID, byte showType)
        {
            // ignoring showType

            var storeContent = _onStore.ByArticle(articleID, ResultValidityEnum.Valid);
            // filter by secID
            var sectionModel = _sectionModelCache.GetItem(secID);
            var result = storeContent
                .Where(i => sectionModel.PositionIDs.Contains(i.PositionID))
                .OrderBy(i => i.Position.PosCode);

            return ToDocItemInfo(result);
        }

        #endregion
    }
}

﻿using S4.Web.SOAP.S3Params;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;
using S4.Core.Extensions;
using S4.Core.Data;
using S4.Entities;
using S4.ProcedureModels.Dispatch;
using S4.Procedures.Dispatch;
using S4.ProcedureModels;

namespace S4.Web.SOAP
{
    public partial class WSTransactions
    {
        [OperationContract(Action = ACTION_URL + nameof(Dispatch_ListDirects))]
        public Store3.BLL.TransResult Dispatch_ListDirects(ref Dispatch_ListDirectsParams pars)
        {
            var proc = new DispatchListDirectionsProc(GetUserID());
            var result = proc.DoProcedure(new DispatchListDirections() { WorkerID = pars.workerID });

            if (result.Directions != null)
                pars.directions = result.Directions.Select(s => new DocumentInfo() { ID = s.DocumentID, Description = s.DocumentName, XtraDescription = s.DocumentDetail }).ToArray();
            else
                pars.directions = new DocumentInfo[0];
            SetTransResult(pars, result);

            return pars.trResult;
        }


        [OperationContract(Action = ACTION_URL + nameof(Dispatch_Proc))]
        public Store3.BLL.TransResult Dispatch_Proc(ref Dispatch_ProcParams pars)
        {
            // just a dummy implementation
            var direction = (new DAL.DirectionDAL()).GetDirectionAllData(pars.directionID);

            pars.isPrepared = true;
            if (direction.XtraData.IsNull(Direction.XTRA_DISPATCH_STORE_MOVE_ID, 0))
                throw new Exception($"Dispatch_Proc - XTRA_DISPATCH_STORE_MOVE_ID není nastaveno; directionID: {pars.directionID}");

            pars.stoMoveID = (int)direction.XtraData[Direction.XTRA_DISPATCH_STORE_MOVE_ID];            
            pars.useRemarks = true; // always true

            pars.trResult = Store3.BLL.TransResult.OK;

            return pars.trResult;
        }


        [OperationContract(Action = ACTION_URL + nameof(Dispatch_ShowItems))]
        public Store3.BLL.TransResult Dispatch_ShowItems(ref Dispatch_ShowItemsParams pars)
        {
            var proc = new DispatchShowItemsProc(GetUserID());
            var result = proc.DoProcedure(new DispatchShowItems() { StoMoveID = pars.stoMoveID });

            pars.items = result.Items.ToS3<Entities.Helpers.ItemInfo, DocItemInfo>((s, d) =>
            {
                d.quantity = s.Quantity;
                d.carrierNum = s.CarrierNum ?? S3_EMPTY_CARRIER;
                d.batchNum = s.BatchNum ?? S3_EMPTY_BATCH;
                d.expirationDate = (s.ExpirationDate.HasValue) ? s.ExpirationDate.Value : DateTime.MinValue;
            }).ToArray();
            pars.useRemarks = result.UseRemarks;

            SetTransResult(pars, result);

            return pars.trResult;
        }


        [OperationContract(Action = ACTION_URL + nameof(Dispatch_ItemInstruct))]
        public Store3.BLL.TransResult Dispatch_ItemInstruct(ref Dispatch_ItemInstructParams pars)
        {
            var proc = new DispatchItemInstructProc(GetUserID());
            var result = proc.DoProcedure(new DispatchItemInstruct() { StoMoveID = pars.stoMoveID, StoMoveItemID = pars.stoMoveItemID });

            pars.scrPosition = new PositionInfo();
            pars.scrPosition.FieldsFromProperties(result.ScrPosition);
            pars.destPosition = new PositionInfo();
            pars.destPosition.FieldsFromProperties(result.DestPosition);

            pars.article = new DocItemInfo();
            pars.article.FieldsFromProperties(result.Item);
            pars.article.quantity = result.Item.Quantity;
            pars.article.carrierNum = result.Item.CarrierNum;
            pars.article.expirationDate = result.Item.ExpirationDate ?? DateTime.MinValue;
            pars.wasFound = true;

            SetTransResult(pars, result);

            return pars.trResult;
        }


        [OperationContract(Action = ACTION_URL + nameof(Dispatch_SaveItem))]
        public Store3.BLL.TransResult Dispatch_SaveItem(ref Dispatch_SaveItemParams pars)
        {
            var proc = new DispatchSaveItemProc(GetUserID());
            var result = proc.DoProcedure(new DispatchSaveItem() { StoMoveID = pars.stoMoveID, StoMoveItemID = pars.stoMoveItemID, DestCarrierNum = pars.destCarrierNum });
            SetTransResult(pars, result);

            return pars.trResult;
        }


        [OperationContract(Action = ACTION_URL + nameof(Dispatch_CancelItem))]
        public Store3.BLL.TransResult Dispatch_CancelItem(ref Dispatch_CancelItemParams pars)
        {
            var proc = new DispatchCancelItemProc(GetUserID());
            var result = proc.DoProcedure(new DispatchCancelItem() { StoMoveID = pars.stoMoveID, StoMoveItemID = pars.stoMoveItemID });
            SetTransResult(pars, result);

            return pars.trResult;
        }


        [OperationContract(Action = ACTION_URL + nameof(Dispatch_ConfirmItem))]
        public Store3.BLL.TransResult Dispatch_ConfirmItem(ref Dispatch_ConfirmItemParams pars)
        {
            var proc = new DispatchConfirmItemProc(GetUserID());
            var result = proc.DoProcedure(new DispatchConfirmItem() { StoMoveID = pars.stoMoveID, StoMoveItemID = pars.stoMoveItemID });
            SetTransResult(pars, result);

            return pars.trResult;
        }


        [OperationContract(Action = ACTION_URL + nameof(Dispatch_TestDirect))]
        public Store3.BLL.TransResult Dispatch_TestDirect(ref Dispatch_TestDirectParams pars)
        {
            var proc = new DispatchTestDirectProc(GetUserID());
            var result = proc.DoProcedure(new DispatchTestDirect() { DirectionID = pars.directionID });

            if (result.Success)
                pars.isCompleted = result.IsCompleted;
            SetTransResult(pars, result);

            return pars.trResult;
        }


        [OperationContract(Action = ACTION_URL + nameof(Dispatch_PosError))]
        public Store3.BLL.TransResult Dispatch_PosError(ref Dispatch_PosErrorParams pars)
        {
            var proc = new DispatchPosErrorProc(GetUserID());
            var result = proc.DoProcedure(new DispatchPosError()
            {
                StoMoveID = pars.stoMoveID,
                PositionID = pars.positionID,
                Reason = ToS4PositionErrorReason(pars.reason)
            });
            SetTransResult(pars, result);

            return pars.trResult;
        }


        [OperationContract(Action = ACTION_URL + nameof(Dispatch_Print))]
        public Store3.BLL.TransResult Dispatch_Print(ref Dispatch_PrintParams pars)
        {
            var proc = new DispatchPrintProc(GetUserID());
            var result = proc.DoProcedure(new DispatchPrint() { DirectionID = pars.directionID, PrinterByPosition = true });
            SetTransResult(pars, result);

            return pars.trResult;
        }


        [OperationContract(Action = ACTION_URL + nameof(Dispatch_CalcBoxLabels))]
        public Store3.BLL.TransResult Dispatch_CalcBoxLabels(ref Dispatch_CalcBoxLabelsParams pars)
        {
            var proc = new DispatchCalcBoxLabelsProc(GetUserID());
            var result = proc.DoProcedure(new DispatchCalcBoxLabels()
            {
                DirectionID = pars.directionID
            });

            if (result.Success)
            {
                pars.boxLabels = result.BoxLabels.ToS3<ProcedureModels.Dispatch.DispatchCalcBoxLabels.BoxLabel, BoxLabelInfo>().ToArray();

                pars.reportList = result.Reports.Select(r => new ReportInfo()
                {
                    reportName = r.Settings.ReportName ?? "?",
                    printerCategory = r.PrinterTypeID.ToString(),
                    printerPath = "path",
                    reportSource = r.ReportID.ToString()
                }).ToArray();

                pars.printersList = result.Printers.Select(p => new PrinterInfo()
                {
                    printerName = p.PrinterLocationDesc,
                    printerPath = p.PrinterPath,
                    printerSelector = String.Join('|', p.PrinterSettings.DispatchPositionCodes.ToArray()),
                    printerCategory = p.PrinterTypeID.ToString()
                }).ToArray();
            }
            SetTransResult(pars, result);

            return pars.trResult;
        }


        [OperationContract(Action = ACTION_URL + nameof(Dispatch_PrintBoxLabel))]
        public Store3.BLL.TransResult Dispatch_PrintBoxLabel(ref Dispatch_PrintBoxLabelParams pars)
        {
            var proc = new DispatchPrintBoxLabelProc(GetUserID());
            var result = proc.DoProcedure(new DispatchPrintBoxLabel()
            {
                DirectionID = pars.directionID,
                LabelText = pars.labelText,
                LabelsQuant = pars.labelsQuant,
                DocPosition = pars.docPosition,
                PrinterLocationID = FindPrinterByPath(pars.printerPath),
                ReportID = int.Parse(pars.reportSource)
            });
            SetTransResult(pars, result);

            return pars.trResult;
        }
        //
    }
}

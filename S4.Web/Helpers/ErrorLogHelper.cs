﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace S4.Web.Helpers
{
    public class ErrorLogHelper
    {

        public void ReportProcException(string requestBody, object statusData, Exception exc, string path, ILogger logger)
        {
            // to logs
            logger.LogError(exc, path);
            ReportProcException(requestBody, statusData, exc, path);
        }

        public void ReportProcException(string requestBody, object statusData, Exception exc, string path)
        {
            var statusJson = (statusData == null) ? null : JsonConvert.SerializeObject(statusData);

            var appVersion = Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion;
            var error = new Entities.ClientError()
            {
                AppVersion = appVersion.ToString(),
                ErrorClass = exc.GetType().Name,
                ErrorDateTime = DateTime.Now,
                ErrorMessage = exc.ToString(),
                Path = path,
                StatusData = statusJson,
                RequestBody = string.IsNullOrWhiteSpace(requestBody) ? null : requestBody
            };

            // write error to DB
            (new DAL.ErrorDAL()).Insert(error);
        }
    }
}

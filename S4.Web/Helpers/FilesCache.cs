﻿using System;
using System.Collections.Specialized;
using System.Runtime.Caching;

namespace S4.Web.Helpers
{
    public partial class FilesCache
    {
        private const int FILE_EXPIRATION = 900; // in seconds

        private MemoryCache _cache = null;



        public FilesCache()
        {
            _cache = new MemoryCache(
                "FilesCache",
                new NameValueCollection
                {
                    //{ "CacheMemoryLimitMegabytes", "512" },
                    //{ "PhysicalMemoryLimitPercentage", "20" },
                    { "PollingInterval", "00:00:05" }
                });
        }


        public string AddToCache(string fileName, string fileType, byte[] fileContent)
        {
            var fileID = Guid.NewGuid().ToString();
            var fileCacheItem = new FileCacheItem() { FileName = fileName, FileType = fileType, FileContent = fileContent };
            _cache.Add(
                new CacheItem(fileID, fileCacheItem),
                new CacheItemPolicy() { AbsoluteExpiration = DateTime.Now.AddSeconds(FILE_EXPIRATION) }
                );

            return fileID;
        }


        public bool GetFromCache(string fileID, ref FileCacheItem fileCacheItem)
        {
            var item = _cache.Get(fileID);
            if (item != null)
            {
                fileCacheItem = (FileCacheItem)item;
                return true;
            }
            return false;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace S4.Web.Helpers
{
    internal class ParamsDuplicityChecker
    {
        private const long TIME_TO_KEEP_TICKS = 60L * 10_000_000L;               // 60 seconds
        private const long TIME_TO_CLEAN_UP_TICKS = 5L * 60L * 10_000_000L;      // 5 minutes

        private readonly object _lock = new object();
        private long _cleanUpTimeTicks = DateTime.Now.Ticks + TIME_TO_CLEAN_UP_TICKS;
        private Dictionary<string, long> _dict = new Dictionary<string, long>(1024);


        internal bool CheckDuplicity(string procedureName, object param)
        {
            var hash = param.GetHashCode();
            var key = $"{procedureName}_{hash}";

            var currentTicks = DateTime.Now.Ticks;
            bool isDuplicit = false;
            lock(_lock)
            {                
                if (_dict.TryGetValue(key, out long endTimeTicks))
                {
                    // if not ended yet
                    isDuplicit = (currentTicks < endTimeTicks);
                }
                else
                    _dict.Add(key, currentTicks + TIME_TO_KEEP_TICKS);

                // clean up
                if (currentTicks > _cleanUpTimeTicks)
                    CleanUp();
            }

            return isDuplicit;
        }


        private void CleanUp()
        {
            var timeLimitTicks = DateTime.Now.Ticks;
            _dict = _dict.Where(i => i.Value >= timeLimitTicks).ToDictionary(i => i.Key, i => i.Value);
            _cleanUpTimeTicks = timeLimitTicks + TIME_TO_CLEAN_UP_TICKS;
        }
    }
}

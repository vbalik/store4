﻿using Microsoft.Extensions.Configuration;
using S4.Core;
using S4.Procedures.Messages;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace S4.Web.PerfMeasurement
{
    public class ProcedureLogReceiver
    {
        private List<ProcedureLogItem> _list = new List<ProcedureLogItem>();
        private readonly object _lock = new object();
        private readonly IConfigurationRoot _configuration;

        public string SaveBaseDir { get; internal set; }

        public ProcedureLogReceiver()
        {
            _configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appSettings.json")
                .Build();

            SaveBaseDir = _configuration.GetSection("DefaultProcedureLogsFolder").Value;
            if (!string.IsNullOrEmpty(SaveBaseDir) && !Directory.Exists(SaveBaseDir))
                Directory.CreateDirectory(SaveBaseDir);
        }

        #region IProcedureLogSaver

        public void Save(ProcedureLogItem item)
        {
            lock (_lock)
            {
                _list.Add(item);
            }

            Singleton<PerformanceStatus>.Instance.AddItem(item);
        }

        #endregion

        public async Task DoFlushAsync()
        {
            List<ProcedureLogItem> listToFlush = null;

            lock (_lock)
            {
                if (_list.Count > 0)
                {
                    // store list
                    listToFlush = _list;
                    // new list
                    _list = new List<ProcedureLogItem>();
                }
            }

            if (listToFlush != null)
                await SaveToFileAsync(listToFlush);
        }

        private async Task SaveToFileAsync(List<ProcedureLogItem> listToFlush)
        {
            var fileName = String.Format("perf_{0:yyyyMMdd_HH}.txt", DateTime.Now);

            string baseDir;
            if (SaveBaseDir == null)
            {
                baseDir = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
                if (String.IsNullOrEmpty(baseDir))
                    baseDir = System.IO.Path.GetTempPath();
            }
            else
            {
                baseDir = SaveBaseDir;
            }
            var dir = System.IO.Path.Combine(baseDir, "PerfData");

            if (!System.IO.Directory.Exists(dir))
                System.IO.Directory.CreateDirectory(dir);
            var path = System.IO.Path.Combine(dir, fileName);

            using (var file = System.IO.File.AppendText(path))
            {
                await file.WriteLineAsync($"# {DateTime.Now:yyyy-MM-dd HH:mm:ss}");
                foreach (var item in listToFlush)
                    await file.WriteAsync(item.ToString());
                file.Flush();
            }
        }
    }
}

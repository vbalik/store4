﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using S4.Procedures.Messages;


namespace S4.Web.PerfMeasurement
{
    public class PerformanceStatus
    {
        private readonly PerformanceStatusCounter _successCounter = new PerformanceStatusCounter();
        private readonly PerformanceStatusCounter _errorCounter = new PerformanceStatusCounter();
        private readonly ConcurrentDictionary<string, PerformanceStatusProcedureItem> _procedures = new ConcurrentDictionary<string, PerformanceStatusProcedureItem>();


        internal void AddItem(ProcedureLogItem item)
        {
            // update counters
            if (item.ProcedureSuccess)
                _successCounter.Update(item.RunTime);
            else
                _errorCounter.Update(item.RunTime);


            // update procedures statistics
            _procedures.AddOrUpdate(
                item.ProcedureName,
                new PerformanceStatusProcedureItem() { ProcedureName = item.ProcedureName, TotalRunCnt = 1, TotalRunTime = (double)item.RunTime },
                (key, oldValue) =>
                {
                    oldValue.TotalRunCnt++;
                    oldValue.TotalRunTime += (double)item.RunTime;
                    return oldValue;
                });
        }


        public object GetStatus()
        {
            return new {
                success = new {
                    _successCounter.RunCnt,
                    _successCounter.RunTimeSum,
                    _successCounter.TimePerProc
                },
                errors = new
                {
                    _errorCounter.RunCnt,
                    _errorCounter.RunTimeSum,
                    _errorCounter.TimePerProc
                },
                procedures = _procedures
                    .Values
                    .OrderByDescending(i => i.TotalRunCnt)
                    .ToList()
            };
        }
    }
}

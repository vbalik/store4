﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.PerfMeasurement
{
    public class PerformanceStatusProcedureItem
    {
        public string ProcedureName { get; set; }
        public long TotalRunCnt { get; set; }
        public double TotalRunTime { get; set; }

        public double TimePerProc => TotalRunTime / (double)TotalRunCnt;
    }
}

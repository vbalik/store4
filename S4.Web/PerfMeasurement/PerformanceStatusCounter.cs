﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.PerfMeasurement
{
    public class PerformanceStatusCounter
    {
        public long RunCnt { get; private set; }
        public double RunTimeSum { get; private set; }

        private readonly object _lock = new object();


        public void Update(long runTime)
        {
            lock (_lock)
            {
                RunCnt++;
                RunTimeSum += (double)runTime;
            }
        }

        public double TimePerProc => RunTimeSum / (double)RunCnt;
    }
}

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using S4.Core;
using S4.Messages;
using S4.Procedures.Messages;
using S4.Web.Hubs.Direction;
using S4.Web.Hubs.Exchange;
using S4.Web.Hubs.Progress;


namespace S4.Web
{
    /// <summary>
    /// Register subscribers to message router
    /// </summary>
    public partial class Startup
    {
        private void RegisterMessageSubscribers(IMessageRouter messageRouter, IApplicationBuilder app,
            IHostingEnvironment env)
        {
            // messages
            // set ProcedureLogReceiver dir
            Singleton<PerfMeasurement.ProcedureLogReceiver>.Instance.SaveBaseDir = env.ContentRootPath;

            // subscribe to 'Procedures.Messages.ProcedureLogItem'
            messageRouter.Subscribe<ProcedureLogItem>(nameof(PerfMeasurement.ProcedureLogReceiver),
                (msg) =>
                {
                    Singleton<PerfMeasurement.ProcedureLogReceiver>.Instance.Save((ProcedureLogItem)msg);
                });

            // subscribe to 'Procedures.Messages.DirectionItemUpdated'
            messageRouter.Subscribe<DirectionItemUpdated>(nameof(DirectionItemUpdated),
                async (msg) =>
                {
                    var directionSubscriber = app.ApplicationServices.GetService<DirectionSubscriber>();
                    await directionSubscriber.DirectionItemsChanged((DirectionItemUpdated)msg);
                });

            // subscribe to 'Procedures.Messages.DirectionStatusUpdated'
            messageRouter.Subscribe<DirectionStatusUpdated>(nameof(DirectionStatusUpdated),
                async (msg) =>
                {
                    var directionSubscriber = app.ApplicationServices.GetService<DirectionSubscriber>();
                    await directionSubscriber.DirectionStatusChanged((DirectionStatusUpdated)msg);
                });

            // subscribe to 'Procedures.Messages.DirectionAssigmentUpdated'
            messageRouter.Subscribe<DirectionAssigmentUpdated>(nameof(DirectionAssigmentUpdated),
                async (msg) =>
                {
                    var directionSubscriber = app.ApplicationServices.GetService<Hubs.Terminal.TerminalSubscriberHostedService>();
                    await directionSubscriber.DirectionAssigmentUpdated((DirectionAssigmentUpdated)msg);
                });

            // subscribe to 'Procedures.Messages.ProgressMessage'
            messageRouter.Subscribe<ProgressMessage>(nameof(ProgressMessage), async (msg) =>
            {
                var progressSubscriber = app.ApplicationServices.GetService<ProgressSubscriber>();
                await progressSubscriber.Changed((ProgressMessage)msg);
            });

            // subscribe to 'Procedures.Messages.ExchangeMessage'
            messageRouter.Subscribe<ExchangeMessage>(nameof(ExchangeMessage), async (msg) =>
            {
                var exchangeSubscriber = app.ApplicationServices.GetService<ExchangeSubscriber>();
                await exchangeSubscriber.Changed((ExchangeMessage)msg);
            });
        }
    }
}
﻿
using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;


namespace S4.Web.Controllers
{
    public class CertificateController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Index(IList<IFormFile> files)
        {
           
             var bytes = default(byte[]);
             var file = files[0];
             using (var reader = new StreamReader(file.OpenReadStream()))
             {
                 using (var memstream = new MemoryStream())
                 {
                     reader.BaseStream.CopyTo(memstream);
                     bytes = memstream.ToArray();
                 }
             }
             
             return new JsonResult(new { bytes });

        }



    }
}

﻿using Microsoft.AspNetCore.Mvc;
using S4.Web.Models;
using System.IO;
using System.Data;
using System;
using System.Collections.Generic;
using S4.Report.Helper;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Reflection;
using Newtonsoft.Json;
using S4.Entities.Settings;
using static S4.Entities.Settings.SQL4Filters;
using combit.ListLabel24.Web;
using S4.Web.AccessControl;
using static S4.Entities.Worker;
using S4.Web.Helpers;
using System.Text;
using S4.Report.Model;
using Microsoft.AspNetCore.Http;
using NPoco;
using S4.Entities;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;

#if INCLUDE_COMBIT
using combit.ListLabel24;
#endif

namespace S4.Web.Controllers
{
    public class ReportController : Controller
    {

#if INCLUDE_COMBIT

        public IActionResult Report(int id)
        {
            return View();
        }

        public IActionResult Index(int id)
        {
            try
            {
                if (id == 6) //práce skladníků
                {
                    // check path with auditor
                    var userID = HttpContext.Request.HttpContext?.User?.FindFirst(System.Security.Claims.ClaimTypes.Sid)?.Value;
                    var auditor = Core.Singleton<AuditorCache>.Instance.GetItem(userID);

                    if (!auditor.Worker.WorkerClass.HasFlag(WorkerClassEnum.CanPrintWorkerReport))
                    {
                        return View("/Views/Home/Index.cshtml");
                    }
                }
                else if (id == 0)
                {
                    return View("/Views/Home/Index.cshtml");
                }

                ReportModel model = new ReportModel();
                model.ReportID = id;

                using (var db = Core.Data.ConnectionHelper.GetDB())
                {
                    var report = db.SingleOrDefaultById<Entities.Reports>(model.ReportID);

                    if (report == null)
                    {
                        throw new Exception($"Nenalezen report id: {model.ReportID}");
                    }

                    model.ReportName = report.Settings.ReportName;
                    model.FiltersReportList = new List<FiltersReport>();
                    model.FiltersReportTextList = new List<string>();

                    var reportHelper = new ReportHelper();
                    foreach (var item in report.Settings.SQL4FiltersList)
                    {
                        model.FiltersReportList.Add(item.Type == TypeFilterEnum.SQL || item.Type == TypeFilterEnum.SQLMulti ? ReadDataTable(reportHelper.SQL2DataTable(item.SQL), item.Name, item.Type, item.Width, item.Mandatory, item.ID) : new FiltersReport() { Name = item.Name, Type = item.Type, Width = item.Width, Mandatory = item.Mandatory, ID = item.ID });
                        model.FiltersReportTextList.Add("");
                    }
                }

                return View(model);
            }
            catch (Exception exc)
            {
                LogException(exc);
                throw new Exception($"Chyba při pokusu vytvořit report id: {id}", exc);
            }
        }
        
        public ActionResult Html5Viewer(string filledFilters, int reportID)
        {
            ReportModel model = new ReportModel();
            model.ViewerOptions.UseUIMiniStyle = true;
            model.ViewerOptions.ShowThumbnails = false;
            model.ViewerOptions.OnListLabelRequest += ViewerOptions_OnListLabelRequest; ;
            
            var val = filledFilters == null ? null : JsonConvert.DeserializeObject<List<FilterValuesReport>>(filledFilters);
            model.ViewerOptions.CustomData = new Tuple<List<FilterValuesReport>, int>(val, reportID);

            return View("Html5Viewer", model);
        }

        private void ViewerOptions_OnListLabelRequest(object sender, ListLabelRequestEventArgs e)
        {
            var customData = e.CustomData as Tuple<List<FilterValuesReport>, int>;
            var reportID = customData.Item2;
            var filterValuesReport = customData.Item1;
            Reports report = null;

            try
            {
                MemoryStream autoProjectStream = null;
                DataSet reportDataset = null;
                var rowCount = 0;
                string reportTitle = "";

                if (!e.Initializing)
                {
                    using (var db = Core.Data.ConnectionHelper.GetDB())
                    {
                        report = db.SingleOrDefaultById<Entities.Reports>(reportID);

                        if (report == null)
                        {
                            throw new Exception($"Nenalezen report id: {reportID}");
                        }

                        var reportHelper = new ReportHelper();
                        var ds = reportHelper.GetDatasetFromSQL(report.Settings.SQL4FiltersList, report.Settings.SQL, filterValuesReport);
                        autoProjectStream = GetReportStream(report.Template);
                        reportDataset = ds.Item1;
                        reportTitle = ds.Item2;
                        rowCount = reportDataset.Tables[0].Rows.Count;
                    }
                }

                ListLabel LL = new ListLabel();
                LL.AutoProjectType = LlProject.List;
                LL.DataSource = e.Initializing == false ? reportDataset : null;
                LL.LicensingInfo = ReportHelper.LICENCE_INFO;
                LL.PageExported += LL_PageExported;
                LL.AutoProjectStream = autoProjectStream;
                //LL.AutoProjectFile = @"c:\Projects\store4\Tools\Reporty\VyrobceNaSklade.srt"; //for debug
                LL.Core.LlSetOptionString(LlOptionString.Exports_Allowed, "PDF;DOCX;RTF;TXT;");

                //add variables
                LL.Variables.Add($"RowCount", rowCount);
                LL.Variables.Add($"Filters", reportTitle);
                LL.Variables.Add("Version", Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion);

                e.ExportPath = $"{Path.GetTempPath()}/combit/reportsTemp";
                e.NewInstance = LL;
            }
            catch (Exception exc)
            {
                var txt = "";
                if (e.Initializing)
                    txt = $"Chyba při pokusu inicializovat report";
                else
                    txt = $"Chyba při pokusu vytvořit report id: {reportID}";

                var sqlSB = new StringBuilder();
                sqlSB.Append(report.Settings.SQL);
                sqlSB.AppendLine("");

                if (report.Settings.SQL4FiltersList != null)
                {
                    foreach (var item in report.Settings.SQL4FiltersList)
                    {
                        var id = item.ID != null ? item.ID.ToString() : string.Empty;
                        var value = item.Value != null ? item.Value.ToString() : string.Empty;
                        sqlSB.Append($"{id}: {value}; ");
                    }
                }

                var ex = new Exception($"{exc.ToString()} - {txt} - error: {exc.Message} - SQL: {sqlSB}");
                LogException(ex);
                throw new Exception($"{txt} {exc.Message}", exc);
            }
        }

        #region helper

        private void LL_PageExported(object sender, PageExportedEventArgs e)
        {
            ListLabel LL = (ListLabel)sender;
            LL.Core.LlXSetParameter(LlExtensionType.Export, "XLS", "Export.ShowResult", "1");
        }

        private void LogException(Exception exc)
        {
            var errorLogHelper = new ErrorLogHelper();
            errorLogHelper.ReportProcException("", new { userID = HttpContext.Request.HttpContext?.User?.FindFirst(System.Security.Claims.ClaimTypes.Sid)?.Value }, exc, "/Report/Html5Viewer");
        }

        private FiltersReport ReadDataTable(DataTable dt, string name, TypeFilterEnum type, string width, bool mandatory, string id)
        {
            FiltersReport dataList = new FiltersReport();
            dataList.Name = name;
            dataList.Type = type;
            dataList.Width = width;
            dataList.Mandatory = mandatory;
            dataList.ID = id;
            dataList.FilterReportList = new List<FilterValuesReport>();
            foreach (DataRow row in dt.Rows)
            {
                FilterValuesReport item = new FilterValuesReport();
                item.Text = row["name"].ToString();
                item.Value = row["id"].ToString();
                dataList.FilterReportList.Add(item);
            }
            return dataList;
        }

        private MemoryStream GetReportStream(string template)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                byte[] bytes = new byte[template.Length * sizeof(char)];
                System.Buffer.BlockCopy(template.ToCharArray(), 0, bytes, 0, bytes.Length);
                return new MemoryStream(bytes, true);
            }
        }

        #endregion

#else
        public IActionResult Index(int id)
        {
            return RedirectToAction("Index", "Home");
        }
#endif

    }

}
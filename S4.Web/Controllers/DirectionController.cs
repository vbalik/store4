﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace S4.Web.Controllers
{
    public class DirectionController : Controller
    {
        [Route("direction/{dir}")]
        public IActionResult Index(Models.DirectionSettingsModel.DirectionEnum dir)
        {
            return View(dir);
        }

        [Route("direction/dirdetail")]
        public IActionResult DirDetail(int directionID)
        {
            return View(directionID);
        }
    }
}

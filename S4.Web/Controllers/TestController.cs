﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace S4.Web.Controllers
{
    public class TestController : Controller
    {
        private IHostingEnvironment _hostingEnv { get; }
        public TestController(IHostingEnvironment hostingEnv)
        {
            _hostingEnv = hostingEnv;
        }

        [AllowAnonymous]
        public IActionResult Index()
        {
            if (_hostingEnv.IsDevelopment())
            {
                return View();
            }
            else
            {
                return new NotFoundResult();
            }

        }


        [AllowAnonymous]
        public IActionResult ProcTester()
        {
            return View();
        }



        [AllowAnonymous]
        public IActionResult ClientErrorTester()
        {
            return View();
        }
    }
}
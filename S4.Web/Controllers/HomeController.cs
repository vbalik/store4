﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using S4.Web.Models;

namespace S4.Web.Controllers
{    
    public class HomeController : Controller
    {
        [Authorize]
        public IActionResult Index()
        {
            return View();            
        }


        [AllowAnonymous]
        public IActionResult Error(string code)
        {
            return View(new ErrorViewModel() { ErrorDesc = $"Chyba {code}" });
        }


        [AllowAnonymous]
        public IActionResult InternalError()
        {
            return View("Error", new ErrorViewModel { ErrorDesc = "Interní chyba", RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

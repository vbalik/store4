﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace S4.Web.Controllers
{
    [Authorize]
    public class AuthController : Controller
    {
        [AllowAnonymous]
        public IActionResult Login(string returnUrl)
        {
            ViewBag.LoginError = false;
            ViewBag.Username = "";
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<IActionResult> Login(string returnUrl, string username, string password)
        {
            if (ModelState.IsValid)
            {
                var worker = (new DAL.WorkerDAL()).GetByLogon(username);
                if (worker != null)
                {
                    if (worker.WorkerStatus == Entities.Worker.WK_STATUS_DISABLED)
                    {
                        ViewBag.LoginError = true;
                        ViewBag.Username = username;
                        ModelState.AddModelError("", "Uživatel nemá přístup.");
                        return View();
                    }
                    else
                    {
                        if (!String.IsNullOrWhiteSpace(password) && Core.Security.S3SecData.CheckPassword(worker.WorkerSecData, worker.WorkerID, password))
                        {
                            var claims = new List<Claim> {
                                new Claim(ClaimTypes.Name, worker.WorkerName, ClaimValueTypes.String, "S4"),
                                new Claim(ClaimTypes.Sid, worker.WorkerID, ClaimValueTypes.String, "S4"),
                            };

                            var userIdentity = new ClaimsIdentity(claims, "SecureLogin");
                            var userPrincipal = new ClaimsPrincipal(userIdentity);

                            await HttpContext.SignOutAsync();
                            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme,
                                userPrincipal,
                                new AuthenticationProperties
                                {
                                    ExpiresUtc = DateTime.UtcNow.AddHours(Startup.LOGIN_DURATION_HOURS),
                                    IsPersistent = false,
                                    AllowRefresh = false

                                });

                            return GoToReturnUrl(returnUrl);
                        }
                    }
                }
            }

            // redisplay form
            ViewBag.LoginError = true;
            ViewBag.Username = username;
            ModelState.AddModelError("", "Nesprávné jméno nebo heslo.");
            return View();
        }


        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync();
            return RedirectToAction(nameof(Login));
        }


        public IActionResult Password()
        {
            return View();
        }


        [AllowAnonymous]
        public IActionResult NotAllowed()
        {
            return View();
        }


        private IActionResult GoToReturnUrl(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
                return Redirect(returnUrl);
            return RedirectToAction("Index", "Home");
        }
    }
}
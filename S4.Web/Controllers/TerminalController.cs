﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using QRCoder;
using S4.Entities.Helpers;
using S4.Web.Hubs;

namespace S4.Web.Controllers
{
    public class TerminalController : Controller
    {
        private readonly IConfiguration _config;

        public TerminalController(IConfiguration config)
        {
            _config = config;
        }


        public IActionResult Settings()
        {
            string serverUrl = GetServerUrl();
            ViewBag.ServerUrl = serverUrl;
            return View();
        }


        public IActionResult QrCode(string user)
        {
            string serverUrl = GetServerUrl();

            // make config file content
            var instanceName = _config["instanceName"];
            var settings = new TerminalSettings() { ApiUrl = serverUrl, InstanceName = instanceName };
            if (user != null)
                settings.User = user;
            var settingsJson = JsonConvert.SerializeObject(settings, Newtonsoft.Json.Formatting.None, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            var qrCodeContent = $"{TerminalSettings.SETTINGS_MARK}{settingsJson}";

            // make QR code image
            string qrCodeAsSvg = null;
            using (var qrGenerator = new QRCodeGenerator())
            using (var qrCodeData = qrGenerator.CreateQrCode(qrCodeContent, QRCodeGenerator.ECCLevel.Q))
            using (var qrCode = new SvgQRCode(qrCodeData))
                qrCodeAsSvg = qrCode.GetGraphic(10);

            return new OkObjectResult(new { qrCodeAsSvg });
        }


        public IActionResult Connections()
        {
            return View();
        }

        public IActionResult Tablet()
        {
            var matchedDevices = new List<Tuple<Client, Client>>();
            var unMatchedDevices = new List<Tuple<Client, Client>>();

            // scan devices
            foreach(var device in PosiflexTabletHub.ConnectedClients.Where(_ => !_.Value.IsScannerService))
            {
                var scanner = PosiflexTabletHub.ConnectedClients.SingleOrDefault(_ => _.Value.IsScannerService && _.Value.IpAddress == device.Value.IpAddress);
                if (scanner.Key != null)
                {
                    matchedDevices.Add(Tuple.Create(device.Value, scanner.Value));
                }
                else
                {
                    unMatchedDevices.Add(Tuple.Create<Client, Client>(device.Value, null));
                }
            }

            // scan scanners
            foreach (var scanner in PosiflexTabletHub.ConnectedClients.Where(_ => _.Value.IsScannerService))
            {
                var device = PosiflexTabletHub.ConnectedClients.SingleOrDefault(_ => !_.Value.IsScannerService && _.Value.IpAddress == scanner.Value.IpAddress);
                if(device.Key == null)
                    unMatchedDevices.Add(Tuple.Create<Client, Client>(null, scanner.Value));
            }

            return View(Tuple.Create(matchedDevices, unMatchedDevices));
        }

        private string GetServerUrl()
        {
            // get server url
            var serverUrl = "?";
            var serverUrlsSection = _config.GetSection("serverUrls");
            var urls = serverUrlsSection.AsEnumerable().Select(i => i.Value).Where(v => v != null && !v.Contains("localhost")).ToArray();
            if (urls.Length > 0)
                serverUrl = urls.Last();
            else 
            {
                var terminalUrl = _config["terminalUrl"];
                if (terminalUrl != null)
                    serverUrl = terminalUrl;
            }
            return serverUrl;
        }

    }
}
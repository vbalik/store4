﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using CsvHelper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using S4.DAL;
using S4.ProcedureModels.Logistics;
using S4.Procedures.Logistics;
using S4.Web.Models;

namespace S4.Web.Controllers
{
    public class ActionController : Controller
    {
        private readonly ArticleDAL _articleDAL;

        public ActionController(ArticleDAL articleDAL)
        {
            _articleDAL = articleDAL;
        }

        [Authorize]
        public IActionResult StoreMoveAction()
        {
            return View();
        }


        [Authorize]
        public IActionResult SetDispCheckAction()
        {
            return View();
        }


        [Authorize]
        public IActionResult ExpeditionAction()
        {
            return View();
        }


        [Authorize]
        public IActionResult PrintExpeditionAction()
        {
            return View();
        }

        [Authorize]
        public IActionResult ReceiveImportCsvAction()
        {
            return View(new ReceiveImportCsvModel());
        }
        
        [Authorize]
        public IActionResult DispatchManual(int directionID)
        {
            return View(directionID);
        }

        [Authorize]
        public IActionResult ReceiveManual()
        {
            return View();
        }

        [Authorize]
        public IActionResult ReceiveAndStoreIn()
        {
            return View();
        }

        [Authorize]
        public IActionResult DeliveryManifest()
        {
            return View();
        }

        [Authorize]
        public IActionResult StockTakingRemote()
        {
            return View();
        }

        [Authorize]
        public IActionResult DeliveryPackagesPrintSheet()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public IActionResult ReceiveImportCsvAction([FromForm] ReceiveImportCsvModel model)
        {
            // check valid file type
            string[] permittedExtensions = {".csv"};
            var fileExtension = Path.GetExtension(model.File?.FileName)?.ToLowerInvariant();

            if (model.File == null
                || string.IsNullOrEmpty(fileExtension)
                || !permittedExtensions.Contains(fileExtension))
            {
                ModelState.AddModelError("badType", "Špatný typ souboru. Soubor není typu .csv");
                return View("ReceiveImportCsvAction", model);
            }

            try
            {
                model.Data = _ReceiveImportCsvParse(model.File);
            }
            catch (CsvHelperException)
            {
                ModelState.AddModelError("badFormat", "Špatný formát souboru.");
            }

            // check item
            foreach (var item in model.Data)
            {
                var articlesFound = _articleDAL.ScanByArticleCode(item.ItemId, true);
                
                if (!articlesFound.Any())
                {
                    item.ErrorMessage = "Položka nenalezena";
                    continue;
                }
                if (articlesFound.Count() > 1)
                {
                    item.ErrorMessage = "Nenalezeno více položek";
                    continue;
                }

                item.ArtPackID = articlesFound.FirstOrDefault().Packings.FirstOrDefault(_ => _.MovablePack).ArtPackID;
            }

            return View(model);
        }

        private static IEnumerable<ReceiveImportCsvItemModel> _ReceiveImportCsvParse(IFormFile file)
        {
            using (var reader = new StreamReader(file.OpenReadStream()))
            {
                using (var csv = new CsvReader(reader))
                {
                    csv.Configuration.PrepareHeaderForMatch = (string header, int index) => header.Trim(' ', '"');
                    csv.Configuration.HasHeaderRecord = true;
                    csv.Configuration.Delimiter = ";";
                    csv.Configuration.IgnoreQuotes = true;

                    return csv.GetRecords<ReceiveImportCsvItemModel>().ToList();
                }
            }
        }
    }
}
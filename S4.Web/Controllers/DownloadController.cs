﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using S4.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Controllers
{
    public class DownloadController
    {
        // GET: Files
        [AllowAnonymous]
        [HttpGet]
        public ActionResult Index(string fileID)
        {
            if (String.IsNullOrWhiteSpace(fileID))
                return new NotFoundResult();

            Helpers.FileCacheItem fileCacheItem = null;
            if (Singleton<Helpers.FilesCache>.Instance.GetFromCache(fileID, ref fileCacheItem))
                return new FileContentResult(fileCacheItem.FileContent, fileCacheItem.FileType) { FileDownloadName = fileCacheItem.FileName };
            else
                return new NotFoundResult();
        }


        // GET
        // without file name - no 'attachement' header
        [AllowAnonymous]
        [HttpGet]
        [Route("/download/direct")]
        public IActionResult Direct(string fileID)
        {
            if (String.IsNullOrWhiteSpace(fileID))
                return new NotFoundResult();

            Helpers.FileCacheItem fileCacheItem = null;
            if (Singleton<Helpers.FilesCache>.Instance.GetFromCache(fileID, ref fileCacheItem))
                return new FileContentResult(fileCacheItem.FileContent, fileCacheItem.FileType);
            else
                return new NotFoundResult();
        }

    }
}

﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using S4.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;


namespace S4.Web.ProcQueue
{
    public class QueuedHostedService : BackgroundService
    {
        private readonly ILogger _logger;


        public QueuedHostedService(IBackgroundTaskQueue taskQueue, ILoggerFactory loggerFactory)
        {
            TaskQueue = taskQueue;
            _logger = loggerFactory.CreateLogger<QueuedHostedService>();
        }


        public IBackgroundTaskQueue TaskQueue { get; }


        protected async override Task ExecuteAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("QueuedHostedService is starting.");

            // main loop
            while (!cancellationToken.IsCancellationRequested)
            {
                var workItem = await TaskQueue.DequeueAsync(cancellationToken);
                _logger.LogDebug($"Start of task {workItem.TaskID}");

                try
                {
                    await workItem.WorkTask(cancellationToken);
                }
                catch (Exception ex)
                {
                    // log error
                    _logger.LogError(ex, $"Error occurred executing; TaskID: {workItem.TaskID}.");

                    // try to reconcile with InCaseOfErrorTask
                    if (workItem.InCaseOfErrorTask != null)
                        await workItem.InCaseOfErrorTask(cancellationToken);
                }

                _logger.LogDebug($"End of task {workItem.TaskID}");
            }

            _logger.LogInformation("QueuedHostedService is stopping.");
        }
    }
}

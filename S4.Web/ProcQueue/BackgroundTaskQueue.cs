﻿using S4.Core.Interfaces;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;


namespace S4.Web.ProcQueue
{
    public class BackgroundTaskQueue : IBackgroundTaskQueue
    {
        private ConcurrentQueue<QueueTaskItem> _workItems = new ConcurrentQueue<QueueTaskItem>();
        private SemaphoreSlim _signal = new SemaphoreSlim(0);


        public string QueueBackgroundWorkItem(Func<CancellationToken, Task> workTask, Func<CancellationToken, Task> inCaseOfErrorTask = null)
        {
            if (workTask == null)
                throw new ArgumentNullException(nameof(workTask));

            var newItem = new QueueTaskItem() { TaskID = Guid.NewGuid().ToString(), WorkTask = workTask, InCaseOfErrorTask = inCaseOfErrorTask };

            _workItems.Enqueue(newItem);
            _signal.Release();

            return newItem.TaskID;
        }


        public async Task<QueueTaskItem> DequeueAsync(CancellationToken cancellationToken)
        {
            await _signal.WaitAsync(cancellationToken);
            _workItems.TryDequeue(out var workItem);

            return workItem;
        }
    }
}

using System.Threading;
using System.Threading.Tasks;


namespace S4.Web.Scheduling
{
    public interface IScheduledTask
    {
        int ScheduledPeriod { get; }
        Task ExecuteAsync(CancellationToken cancellationToken);
    }
}
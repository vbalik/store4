﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using S4.Web.Models.DataList;
using System.Drawing;

using S4.Web.Models;

namespace S4.Web.DataList
{
    public class SectionSettings : DataListSettings<Entities.Section, SectionSettingsModel>
    {
        internal override void CreateColumns(bool isSuperuser)
        {
            base.CreateColumns(isSuperuser);

            SettingsModel.Columns["sectID"].Visible = true;
            SettingsModel.Columns["sectID"].Width = 2;
            SettingsModel.Columns["sectID"].Order = 0;
            SettingsModel.Columns["sectID"].Bold = true;

            SettingsModel.Columns["sectDesc"].Visible = true;
            SettingsModel.Columns["sectDesc"].Order = 1;
            SettingsModel.Columns["sectDesc"].SortMode = DataListColumnModel.SortModeEnum.smAscending;
        }
    }
}

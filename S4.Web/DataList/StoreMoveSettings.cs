﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using S4.Web.Models.DataList;
using System.Drawing;

using S4.Web.Models;

namespace S4.Web.DataList
{
    public class StoreMoveSettings : DataListSettings<Entities.Views.StoreMoveView, StoreMoveSettingsModel>
    {
        internal override void CreateColumns(bool isSuperuser)
        {
            base.CreateColumns(isSuperuser);

            SettingsModel.Columns["stoMoveID"].SortMode = DataListColumnModel.SortModeEnum.smDescending;

            SettingsModel.Columns.AddColumn()
                .SetVisible()
                .SetCaption("Doklad")
                .SetBold()
                .SetOrder(0)
                .SetWidth(4)
                .SetCommand(new DataListCommand() { CommandType = DataListCommand.CommandTypeEnum.cmDetail })
                .SetSortMode(DataListColumnModel.SortModeEnum.smDescending)
                .SetAllowHtmlFormating()
                .SetMultiColSep("<span class='docInfoSeparator'>/</span>")
                .AddColumn(SettingsModel.Columns["docNumPrefix"])
                .AddColumn(SettingsModel.Columns["docNumber"]);

            SettingsModel.Columns["docNumPrefix"].SetDisallowAutoWildcard(true);

            SettingsModel.Columns["docStatus"]
                .SetVisible()
                .SetWidth(4)
                .SetOrder(3)
                .SetAllowHtmlFormating();

            SettingsModel.Columns["docDate"]
                .SetVisible()
                .SetOrder(4)
                .SetWidth(2);

            SettingsModel.Columns["parentDoc"]
                .SetVisible()
                .SetDisallowFiltering()
                .SetOrder(5)
                .SetWidth(4);

            SettingsModel.Columns["entryUserID"]
                .SetVisible()
                .SetOrder(6)
                .SetWidth(2);

            SettingsModel.Columns["docRemark_prev"]
                .SetVisible()
                .SetDisallowFiltering()
                .SetDisallowSorting()
                .SetOrder(7);

            SettingsModel.Columns["entryDateTime"]
                .SetDataType(DataListColumnModel.DataTypeEnum.tpDateTime);
        }

        internal override void CreateFilterOptions()
        {
            this.SettingsModel.FilterOptions = new DataListFilterOption[]
            {
                new DataListFilterOption() { ItemCode = StoreMoveSettingsModel.StoreMoveFilterEnum.Receive, ItemDesc = "Příjem" },
                new DataListFilterOption() { ItemCode = StoreMoveSettingsModel.StoreMoveFilterEnum.StoreIn, ItemDesc = "Naskladnění" },
                new DataListFilterOption() { ItemCode = StoreMoveSettingsModel.StoreMoveFilterEnum.Move, ItemDesc = "Přeskladnění" },
                new DataListFilterOption() { ItemCode = StoreMoveSettingsModel.StoreMoveFilterEnum.Dispatch, ItemDesc = "Vychystání" },
                new DataListFilterOption() { ItemCode = StoreMoveSettingsModel.StoreMoveFilterEnum.Expedition, ItemDesc = "Expedice" },
                new DataListFilterOption() { ItemCode = StoreMoveSettingsModel.StoreMoveFilterEnum.Preparation, ItemDesc = "Příprava" },
                new DataListFilterOption() { ItemCode = StoreMoveSettingsModel.StoreMoveFilterEnum.Stotak, ItemDesc = "Inventura" }
            };
            this.SettingsModel.FilterOption = DirectionSettingsModel.DirectionFilterEnum.Receive;
        }
    }
}

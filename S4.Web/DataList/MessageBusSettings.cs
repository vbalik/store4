﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using S4.Web.Models.DataList;
using System.ComponentModel.DataAnnotations;

using S4.Web.Models;

namespace S4.Web.DataList
{
    public class MessageBusSettings : DataListSettings<Entities.Views.MessageBusView, MessageBusSettingsModel>
    {
        internal override void CreateColumns(bool isSuperuser)
        {
            base.CreateColumns(isSuperuser);

            SettingsModel.Columns["messageBusID"].Visible = true;
            SettingsModel.Columns["messageBusID"].Width = 2;
            SettingsModel.Columns["messageBusID"].Order = 0;
            SettingsModel.Columns["messageBusID"].Caption = "ID";
            SettingsModel.Columns["messageBusID"].SetCommand(new DataListCommand() { CommandType = DataListCommand.CommandTypeEnum.cmDetail });

            SettingsModel.Columns.AddColumn()
                .SetVisible()
                .SetCaption("Doklad")
                .SetBold()
                .SetOrder(1)
                .SetWidth(3)
                .SetCommand(new DataListCommand() { CommandType = DataListCommand.CommandTypeEnum.cmDetail })
                .SetAllowHtmlFormating()
                .SetMultiColSep("<span class='docInfoSeparator'>/</span>")
                .AddColumn(SettingsModel.Columns["docNumPrefix"])
                .AddColumn(SettingsModel.Columns["docYear"].SetDataType(DataListColumnModel.DataTypeEnum.tpNumber))
                .AddColumn(SettingsModel.Columns["docNumber"]);

            SettingsModel.Columns["messageStatus"].Visible = true;
            SettingsModel.Columns["messageStatus"].Width = 3;
            SettingsModel.Columns["messageStatus"].Order = 2;
            SettingsModel.Columns["messageStatus"].SetAllowHtmlFormating();

            SettingsModel.Columns["messageType"].Visible = true;
            SettingsModel.Columns["messageType"].Width = 3;
            SettingsModel.Columns["messageType"].Order = 3;
            SettingsModel.Columns["messageType"].Command = new DataListCommand() { CommandType = DataListCommand.CommandTypeEnum.cmDetail };

            SettingsModel.Columns["tryCount"].Visible = true;
            SettingsModel.Columns["tryCount"].Width = 1;
            SettingsModel.Columns["tryCount"].Order = 4;
            SettingsModel.Columns["tryCount"].Caption = "Pok.";

            SettingsModel.Columns["errorDesc"].Visible = true;
            SettingsModel.Columns["errorDesc"].Width = 3;
            SettingsModel.Columns["errorDesc"].Order = 5;

            SettingsModel.Columns["nextTryTime"].Visible = true;
            SettingsModel.Columns["nextTryTime"].Width = 3;
            SettingsModel.Columns["nextTryTime"].Order = 6;
            SettingsModel.Columns["nextTryTime"].DataType = DataListColumnModel.DataTypeEnum.tpDateTime;
            SettingsModel.Columns["nextTryTime"].SetAllowHtmlFormating();

            SettingsModel.Columns["lastDateTime"].Visible = true;
            SettingsModel.Columns["lastDateTime"].Width = 3;
            SettingsModel.Columns["lastDateTime"].Order = 7;
            SettingsModel.Columns["lastDateTime"].DataType = DataListColumnModel.DataTypeEnum.tpDateTime;
            SettingsModel.Columns["lastDateTime"].SetAllowHtmlFormating();

            SettingsModel.Columns["entryDateTime"].Visible = true;
            SettingsModel.Columns["entryDateTime"].Width = 3;
            SettingsModel.Columns["entryDateTime"].Order = 8;
            SettingsModel.Columns["entryDateTime"].DataType = DataListColumnModel.DataTypeEnum.tpDateTime;
            SettingsModel.Columns["entryDateTime"].SortMode = DataListColumnModel.SortModeEnum.smDescending;
            SettingsModel.Columns["entryDateTime"].SetAllowHtmlFormating();



        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using S4.Web.Models.DataList;

namespace S4.Web.DataList
{
    public class ColumnSettingsList : List<DataListColumnModel>
    {
        public event EventHandler WidthRecalcRequest;
        public DataListColumnModel this[string fieldName]
        {
            get
            {
                if (!GetItem(fieldName, out DataListColumnModel item))
                {
                    item = new DataListColumnModel() { FieldName = fieldName };
                    item.WidthRecalcRequest += Item_WidthRecalcRequest;
                    Add(item);
                }
                return item;
            }
            set
            {
                if (GetItem(fieldName, out DataListColumnModel item))
                    item = value;
                else
                    Add(item);
            }
        }

        public void SortByOrder()
        {
            Sort((item1, item2) => { return item1.Order.CompareTo(item2.Order); });
        }

        public DataListColumnModel AddColumn()
        {
            var newColumn = new DataListColumnModel();
            this.Add(newColumn);
            return newColumn;
        }

        private void Item_WidthRecalcRequest(object sender, EventArgs e)
        {
            WidthRecalcRequest?.Invoke(sender, e);
        }

        private bool GetItem(string fieldName, out DataListColumnModel item)
        {
            item = this.Where(c => c.FieldName == fieldName).FirstOrDefault();
            return item != null;
        }
    }
}

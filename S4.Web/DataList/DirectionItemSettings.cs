﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using S4.Web.Models.DataList;

using S4.Web.Models;

namespace S4.Web.DataList
{
    public class DirectionItemSettings : DataListSettings<Entities.Views.DirectionItemView, DirectionItemSettingsModel>
    {
        internal override void CreateColumns(bool isSuperuser)
        {
            base.CreateColumns(isSuperuser);

            SettingsModel.Columns["docPosition"]
                .SetVisible()
                .SetWidth(1)
                .SetOrder(1);

            SettingsModel.Columns["itemStatus"]
                .SetVisible()
                .SetWidth(3)
                .SetOrder(2)
                .SetAllowHtmlFormating();

            SettingsModel.Columns["articleCode"]
                .SetVisible()
                .SetWidth(4)
                .SetOrder(3)
                .SetBold()
                .SetSortMode()
                .SetCommand(new DataListCommand()
                {
                    CommandType = DataListCommand.CommandTypeEnum.cmCustom,
                    CommandId = "article_detail",
                    Title = "Zobrazit detail karty"
                });

            SettingsModel.Columns["articleDesc"]
                .SetVisible()
                .SetOrder(4)
                .SetBold();

            SettingsModel.Columns["quantity"]
                .SetVisible()
                .SetWidth(2)
                .SetOrder(5);

            SettingsModel.Columns["unitDesc"]
                .SetVisible()
                .SetWidth(2)
                .SetOrder(6);

            SettingsModel.Columns["partnerDepart"]
                .SetVisible()
                .SetOrder(7);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using S4.Web.Models.DataList;
using System.ComponentModel.DataAnnotations;

using S4.Web.Models;

namespace S4.Web.DataList
{
    public class ExchangeErrorSettings : DataListSettings<Entities.ExchangeError, ExchangeErrorSettingsModel>
    {
        internal override void CreateColumns(bool isSuperuser)
        {
            base.CreateColumns(isSuperuser);

            SettingsModel.Columns["exchangeErrorID"].Visible = true;
            SettingsModel.Columns["exchangeErrorID"].Width = 2;
            SettingsModel.Columns["exchangeErrorID"].Order = 0;
            SettingsModel.Columns["exchangeErrorID"].SortMode = DataListColumnModel.SortModeEnum.smDescending;

            SettingsModel.Columns["appVersion"].Visible = true;
            SettingsModel.Columns["appVersion"].Width = 2;
            SettingsModel.Columns["appVersion"].Order = 1;

            SettingsModel.Columns["errorClass"].Visible = true;
            SettingsModel.Columns["errorClass"].Width = 4;
            SettingsModel.Columns["errorClass"].Order = 2;

            SettingsModel.Columns["errorDateTime"].Visible = true;
            SettingsModel.Columns["errorDateTime"].Width = 4;
            SettingsModel.Columns["errorDateTime"].DataType = DataListColumnModel.DataTypeEnum.tpDateTime;
            SettingsModel.Columns["errorDateTime"].Order = 3;

            SettingsModel.Columns["errorMessage"].Visible = true;
            SettingsModel.Columns["errorMessage"].Width = 8;
            SettingsModel.Columns["errorMessage"].Command = new DataListCommand() { CommandType = DataListCommand.CommandTypeEnum.cmDetail };
            SettingsModel.Columns["errorMessage"].AllowHtmlFormating = false;
            SettingsModel.Columns["errorMessage"].Order = 4;

            SettingsModel.Columns["jobClass"].Visible = false;
            SettingsModel.Columns["jobClass"].Width = 2;
            SettingsModel.Columns["jobClass"].AllowHtmlFormating = false;
            SettingsModel.Columns["jobClass"].Order = 5;

            SettingsModel.Columns["jobName"].Visible = true;
            SettingsModel.Columns["jobName"].Width = 4;
            SettingsModel.Columns["jobName"].AllowHtmlFormating = false;
            SettingsModel.Columns["jobName"].Order = 6;

        }
    }
}

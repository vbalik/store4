﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using S4.Web.Models.DataList;
using System.ComponentModel.DataAnnotations;

using S4.Web.Models;

namespace S4.Web.DataList
{
    public class StotakArticleDiffsSettings : DataListSettings<Entities.Models.StotakArticleDifferences, StotakArticleDiffsSettingsModel>
    {
        internal override void CreateColumns(bool isSuperuser)
        {
            base.CreateColumns(isSuperuser);

            SettingsModel.Columns["articleCode"].Visible = true;
            SettingsModel.Columns["articleCode"].Width = 2;
            SettingsModel.Columns["articleCode"].Order = 0;

            SettingsModel.Columns["articleDesc"].Visible = true;
            SettingsModel.Columns["articleDesc"].Order = 1;

            SettingsModel.Columns["quantOnSt"].Visible = true;
            SettingsModel.Columns["quantOnSt"].Width = 2;
            SettingsModel.Columns["quantOnSt"].Order = 2;

            SettingsModel.Columns["quantStTa"].Visible = true;
            SettingsModel.Columns["quantStTa"].Width = 2;
            SettingsModel.Columns["quantStTa"].Order = 3;

            SettingsModel.Columns["quantDiff"].Visible = true;
            SettingsModel.Columns["quantDiff"].Width = 2;
            SettingsModel.Columns["quantDiff"].Order = 4;

            SettingsModel.Columns["unitPrice"].Visible = true;
            SettingsModel.Columns["unitPrice"].Width = 2;
            SettingsModel.Columns["unitPrice"].Order = 5;

            SettingsModel.Columns["priceDiff"].Visible = true;
            SettingsModel.Columns["priceDiff"].Width = 2;
            SettingsModel.Columns["priceDiff"].Order = 6;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using S4.Web.Models.DataList;

namespace S4.Web.DataList
{
    public class DataListData<T, TSettings, TSettingsModel>
        where T : new()
        where TSettings : DataListSettings<T, TSettingsModel>, new()
        where TSettingsModel: DataListSettingsModel<T>, new()
    {
        public string Error { get; set; }

        public DataListData()
        {

        }

        public DataListData(TSettingsModel settings)
        {
            DataModel.Settings = settings;
        }

        public DataListDataModel<T, TSettingsModel> DataModel { get; } = new DataListDataModel<T, TSettingsModel>();

        public void CreateSettings(bool isSuperUser)
        {
            if (DataModel.Settings == null)
                DataModel.Settings = new TSettings().SettingsModel;

            // user is logged as superuser
            DataModel.Settings.IsSuperUser = isSuperUser;

            if (DataModel.Settings.Columns == null || DataModel.Settings.FilterOptions == null)
            {
                var settings = new TSettings();
                settings.SettingsModel = DataModel.Settings;
                if (DataModel.Settings.Columns == null)
                {
                    settings.CreateColumns(isSuperUser);
                    settings.SettingsModel.Columns.SortByOrder();
                    DataModel.Settings.Columns = settings.SettingsModel.Columns;
                }
                if (DataModel.Settings.FilterOptions == null)
                {
                    settings.CreateFilterOptions();
                    DataModel.Settings.FilterOptions = settings.SettingsModel.FilterOptions;
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using S4.Web.Models.DataList;
using System.Drawing;

using S4.Web.Models;

namespace S4.Web.DataList
{
    public class TruckSettings : DataListSettings<Entities.Truck, TruckSettingsModel>
    {
        internal override void CreateColumns(bool isSuperuser)
        {
            base.CreateColumns(isSuperuser);

            SettingsModel.Columns["truckRegist"].Visible = true;
            SettingsModel.Columns["truckRegist"].Width = 4;
            SettingsModel.Columns["truckRegist"].Order = 0;
            SettingsModel.Columns["truckRegist"].SortMode = DataListColumnModel.SortModeEnum.smAscending;
            SettingsModel.Columns["truckRegist"].Bold = true;

            SettingsModel.Columns["truckCapacity"].Visible = true;
            SettingsModel.Columns["truckCapacity"].Order = 1;
            SettingsModel.Columns["truckCapacity"].Width = 2;

            SettingsModel.Columns["truckComment"].Visible = true;
            SettingsModel.Columns["truckComment"].Order = 2;

            SettingsModel.Columns["truckRegion"].Visible = true;
            SettingsModel.Columns["truckRegion"].Order = 3;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using S4.Web.Models.DataList;
using System.Drawing;

using S4.Web.Models;

namespace S4.Web.DataList
{
    public class PositionSettings : DataListSettings<Entities.Views.PositionView, PositionSettingsModel>
    {
        internal override void CreateColumns(bool isSuperuser)
        {
            base.CreateColumns(isSuperuser);

            SettingsModel.Columns["posCode"].Visible = true;
            SettingsModel.Columns["posCode"].Width = 2;
            SettingsModel.Columns["posCode"].Order = 0;
            SettingsModel.Columns["posCode"].SortMode = DataListColumnModel.SortModeEnum.smAscending;
            SettingsModel.Columns["posCode"].Command = new DataListCommand() { CommandType = DataListCommand.CommandTypeEnum.cmDetail };
            SettingsModel.Columns["posCode"].Bold = true;

            SettingsModel.Columns["posDesc"].Visible = true;
            SettingsModel.Columns["posDesc"].Order = 1;

            SettingsModel.Columns["posStatus"].Visible = true;
            SettingsModel.Columns["posStatus"].Width = 4;
            SettingsModel.Columns["posStatus"].Order = 2;
            SettingsModel.Columns["posStatus"].AllowHtmlFormating = true;

            SettingsModel.Columns["houseID"].Visible = true;
            SettingsModel.Columns["houseID"].Width = 2;
            SettingsModel.Columns["houseID"].Order = 3;

            SettingsModel.Columns["posCateg"].Visible = true;
            SettingsModel.Columns["posCateg"].Width = 4;
            SettingsModel.Columns["posCateg"].Order = 4;

            SettingsModel.Columns["posAttributes"].Visible = true;
            SettingsModel.Columns["posAttributes"].Width = 2;
            SettingsModel.Columns["posAttributes"].Order = 5;
            SettingsModel.Columns["posAttributes"].AllowHtmlFormating = true;

            SettingsModel.Columns["itemsCount"].Visible = true;
            SettingsModel.Columns["itemsCount"].Width = 2;
            SettingsModel.Columns["itemsCount"].Order = 6;
            SettingsModel.Columns["itemsCount"].DisallowSorting = true;
            SettingsModel.Columns["itemsCount"].DisallowFiltering = true;
        }

        internal override void CreateFilterOptions()
        {
            this.SettingsModel.FilterOptions = new DataListFilterOption[]
            {
                new DataListFilterOption() { ItemCode = PositionSettingsModel.PositionFilterEnum.Store, ItemDesc = "Sklad" },
                new DataListFilterOption() { ItemCode = PositionSettingsModel.PositionFilterEnum.Receive, ItemDesc = "Příjem" },
                new DataListFilterOption() { ItemCode = PositionSettingsModel.PositionFilterEnum.Dispatch, ItemDesc = "Expedice" },
                new DataListFilterOption() { ItemCode = PositionSettingsModel.PositionFilterEnum.Blocked, ItemDesc = "Zablokované" },
                new DataListFilterOption() { ItemCode = PositionSettingsModel.PositionFilterEnum.Empty, ItemDesc = "Prázdné" },
                new DataListFilterOption() { ItemCode = PositionSettingsModel.PositionFilterEnum.NotEmpty, ItemDesc = "Použité" },
                new DataListFilterOption() { ItemCode = PositionSettingsModel.PositionFilterEnum.All, ItemDesc = "Všechny" },
            };

            this.SettingsModel.FilterOption = PositionSettingsModel.PositionFilterEnum.Store;
        }
    }
}

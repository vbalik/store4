﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using S4.Web.Models.DataList;
using System.ComponentModel.DataAnnotations;

using S4.Web.Models;

namespace S4.Web.DataList
{
    public class InvoiceXtraSettings : DataListSettings<Entities.InvoiceXtra, InvoiceXtraSettingsModel>
    {
        internal override void CreateColumns(bool isSuperuser)
        {
            base.CreateColumns(isSuperuser);

            SettingsModel.Columns["invoiceExID"].Visible = true;
            SettingsModel.Columns["invoiceExID"].Width = 3;
            SettingsModel.Columns["invoiceExID"].Order = 0;
            SettingsModel.Columns["invoiceExID"].Caption = "ID";
            SettingsModel.Columns["invoiceExID"].SortMode = DataListColumnModel.SortModeEnum.smAscending;

            SettingsModel.Columns["xtraCode"].Visible = true;
            SettingsModel.Columns["xtraCode"].Width = 3;
            SettingsModel.Columns["xtraCode"].Order = 1;
            SettingsModel.Columns["xtraCode"].Caption = "Kód";

            SettingsModel.Columns["xtraValue"].Visible = true;
            SettingsModel.Columns["xtraValue"].Width = 4;
            SettingsModel.Columns["xtraValue"].Order = 2;
            SettingsModel.Columns["xtraValue"].Caption = "Hodnota";
                       
        }
    }
}

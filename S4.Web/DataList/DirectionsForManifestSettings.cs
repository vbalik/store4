﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using S4.Web.Models.DataList;
using System.Drawing;

using S4.Web.Models;

namespace S4.Web.DataList
{
    public class DirectionsForManifestSettings : DataListSettings<Entities.Models.DirectionsForManifestModel, DirectionsForManifestSettingsModel>
    {
        internal override void CreateColumns(bool isSuperUser)
        {
            base.CreateColumns(isSuperUser);

            SettingsModel.Columns["directionID"].SortMode = DataListColumnModel.SortModeEnum.smDescending;

            SettingsModel.Columns.AddColumn()
                .SetVisible()
                .SetCaption("Doklad")
                .SetBold()
                .SetOrder(0)
                .SetWidth(4)
                .SetCommand(new DataListCommand() { CommandType = DataListCommand.CommandTypeEnum.cmDetail })
                .SetAllowHtmlFormating()
                .SetMultiColSep("<span class='docInfoSeparator'>/</span>")
                .AddColumn(SettingsModel.Columns["docNumPrefix"])
                .AddColumn(SettingsModel.Columns["docYear"].SetDataType(DataListColumnModel.DataTypeEnum.tpNumber))
                .AddColumn(SettingsModel.Columns["docNumber"]);

            SettingsModel.Columns["docDate"]
                .SetVisible()
                .SetWidth(2)
                .SetOrder(1);

            SettingsModel.Columns["partnerName"]
                .SetVisible()
                .SetWidth(8)
                .SetOrder(2);

            SettingsModel.Columns["deliveryInst"]
                .SetVisible()
                .SetOrder(3);

            SettingsModel.Columns["parcels"]
                .SetVisible()
                .SetWidth(2)
                .SetOrder(3);
        }
    }
}

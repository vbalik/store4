﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using S4.Web.Models.DataList;

using S4.Web.Models;

namespace S4.Web.DataList
{
    public class InvoiceDirectionSettings : DataListSettings<Entities.Direction, InvoiceDirectionSettingsModel>
    {
        internal override void CreateColumns(bool isSuperuser)
        {
            base.CreateColumns(isSuperuser);

            SettingsModel.Columns["directionID"].SortMode = DataListColumnModel.SortModeEnum.smDescending;
            SettingsModel.Columns["directionID"].Visible = false;

            SettingsModel.Columns.AddColumn()
                .SetVisible()
                .SetCaption("Doklad")
                .SetBold()
                .SetOrder(0)
                .SetWidth(4)
                .SetCommand(new DataListCommand() { CommandType = DataListCommand.CommandTypeEnum.cmCustom, CommandId = "direction_detail", Title = "Zobrazit detail dokladu" })
                .SetAllowHtmlFormating()
                .SetMultiColSep("<span class='docInfoSeparator'>/</span>")
                .AddColumn(SettingsModel.Columns["docNumPrefix"])
                .AddColumn(SettingsModel.Columns["docYear"].SetDisallowAutoWildcard())
                .AddColumn(SettingsModel.Columns["docNumber"]);

            SettingsModel.Columns["docNumPrefix"].SetDisallowAutoWildcard(true);

            SettingsModel.Columns["docStatus"].Visible = true;
            SettingsModel.Columns["docStatus"].Width = 3;
            SettingsModel.Columns["docStatus"].Order = 1;
            SettingsModel.Columns["docStatus"].SetAllowHtmlFormating();
            
            SettingsModel.Columns["docDate"].Visible = true;
            SettingsModel.Columns["docDate"].Width = 3;
            SettingsModel.Columns["docDate"].Order = 2;

        }

       
    }
}

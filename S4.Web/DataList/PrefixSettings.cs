﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using S4.Web.Models.DataList;
using System.ComponentModel.DataAnnotations;

using S4.Web.Models;

namespace S4.Web.DataList
{
    public class PrefixSettings : DataListSettings<Entities.Views.PrefixView, PrefixSettingsModel>
    {
        internal override void CreateColumns(bool isSuperuser)
        {
            base.CreateColumns(isSuperuser);

            SettingsModel.Columns["docClass"].Visible = true;
            SettingsModel.Columns["docClass"].Width = 2;
            SettingsModel.Columns["docClass"].Bold = true;
            SettingsModel.Columns["docClass"].Order = 0;

            SettingsModel.Columns["docNumPrefix"].Visible = true;
            SettingsModel.Columns["docNumPrefix"].Width = 2;
            SettingsModel.Columns["docNumPrefix"].Bold = true;
            SettingsModel.Columns["docNumPrefix"].Order = 1;

            SettingsModel.Columns["prefixDesc"].Visible = true;
            SettingsModel.Columns["prefixDesc"].Width = 8;
            SettingsModel.Columns["prefixDesc"].SortMode = DataListColumnModel.SortModeEnum.smDescending;
            SettingsModel.Columns["prefixDesc"].Order = 2;

            SettingsModel.Columns["docDirection"].Visible = true;
            SettingsModel.Columns["docDirection"].Width = 2;
            SettingsModel.Columns["docDirection"].Order = 3;

            SettingsModel.Columns["docType"].Visible = true;
            SettingsModel.Columns["docType"].Width = 2;
            SettingsModel.Columns["docType"].Order = 4;

            SettingsModel.Columns["prefixEnabled"].Visible = true;
            SettingsModel.Columns["prefixEnabled"].Width = 2;
            SettingsModel.Columns["prefixEnabled"].Order = 5;

            SettingsModel.Columns["sectDesc"].Visible = true;
            SettingsModel.Columns["sectDesc"].Order = 6;

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

using S4.Web.Models.DataList;
using S4.Web.Models;

namespace S4.Web.DataList
{
    public class StockTakingItemSettings : DataListSettings<Entities.Views.StockTakingItemView, StockTakingItemSettingsModel>
    {
        internal override void CreateColumns(bool isSuperuser)
        {
            base.CreateColumns(isSuperuser);

            SettingsModel.Columns["posCode"].Visible = true;
            SettingsModel.Columns["posCode"].Width = 2;
            SettingsModel.Columns["posCode"].Order = 2;
            SettingsModel.Columns["posCode"].Command = new DataListCommand()
            {
                CommandType = DataListCommand.CommandTypeEnum.cmCustom,
                CommandId = "position_detail",
                Title = "Zobrazit detail pozice"
            };

            SettingsModel.Columns["articleCode"].Visible = true;
            SettingsModel.Columns["articleCode"].Width = 2;
            SettingsModel.Columns["articleCode"].Order = 3;
            SettingsModel.Columns["articleCode"].SortMode = DataListColumnModel.SortModeEnum.smAscending;
            SettingsModel.Columns["articleCode"].Command = new DataListCommand()
            {
                CommandType = DataListCommand.CommandTypeEnum.cmCustom,
                CommandId = "article_detail",
                Title = "Zobrazit detail karty"
            };

            SettingsModel.Columns["articleDesc"].Visible = true;
            SettingsModel.Columns["articleDesc"].Order = 4;

            SettingsModel.Columns["quantity"].Visible = true;
            SettingsModel.Columns["quantity"].Width = 2;
            SettingsModel.Columns["quantity"].Order = 5;

            SettingsModel.Columns["carrierNumView"].Visible = true;
            SettingsModel.Columns["carrierNumView"].Width = 4;
            SettingsModel.Columns["carrierNumView"].Order = 6;

            SettingsModel.Columns["batchNumView"].Visible = true;
            SettingsModel.Columns["batchNumView"].Width = 2;
            SettingsModel.Columns["batchNumView"].Order = 7;

            SettingsModel.Columns["expirationDesc"].Visible = true;
            SettingsModel.Columns["expirationDesc"].Width = 2;
            SettingsModel.Columns["expirationDesc"].Order = 8;

            SettingsModel.Columns["entryDateTime"].Visible = true;
            SettingsModel.Columns["entryDateTime"].Width = 2;
            SettingsModel.Columns["entryDateTime"].Order = 9;
            SettingsModel.Columns["entryDateTime"].DataType = DataListColumnModel.DataTypeEnum.tpDateTime;

            SettingsModel.Columns["entryUserID"].Visible = true;
            SettingsModel.Columns["entryUserID"].Width = 2;
            SettingsModel.Columns["entryUserID"].Order = 10;
        }
    }
}

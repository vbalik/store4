﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using S4.Web.Models.DataList;

namespace S4.Web.DataList
{
    public class DataListColumn
    {
        private DataListColumnModel _column;

        public DataListColumn(DataListColumnModel column)
        {
            _column = column;
        }

        public ComparisonTypeEnum GetOperation(string filterValue, out object value1, out object value2)
        {
            if (_column.DataType == DataListColumnModel.DataTypeEnum.tpString)
            {
                var operation = GetStringOperation(filterValue, out string value);
                value1 = value;
                value2 = null;
                return operation;
            }
            if (filterValue.IndexOf("=") == 0)
            {
                value1 = ConvertTo(filterValue.Substring(1));
                value2 = null;
                return ComparisonTypeEnum.ctEquals;
            }
            if (filterValue.IndexOf("<=") == 0)
            {
                value1 = ConvertTo(filterValue.Substring(2));
                value2 = null;
                return ComparisonTypeEnum.ctLesOrEquals;
            }
            if (filterValue.IndexOf(">=") == 0)
            {
                value1 = ConvertTo(filterValue.Substring(2));
                value2 = null;
                return ComparisonTypeEnum.ctBiggerOrEquals;
            }
            if (filterValue.IndexOf("<") == 0)
            {
                value1 = ConvertTo(filterValue.Substring(1));
                value2 = null;
                return ComparisonTypeEnum.ctLes;
            }
            if (filterValue.IndexOf(">") == 0)
            {
                value1 = ConvertTo(filterValue.Substring(1));
                value2 = null;
                return ComparisonTypeEnum.ctBigger;
            }
            if (filterValue.IndexOf("-") > 0 && filterValue.IndexOf("-") < filterValue.Length - 1)
            {
                var parts = filterValue.Split("-");
                value1 = ConvertTo(parts[0]);
                value2 = ConvertTo(parts[1]);
                if (_column.DataType == DataListColumnModel.DataTypeEnum.tpDate || _column.DataType == DataListColumnModel.DataTypeEnum.tpDateTime || _column.DataType == DataListColumnModel.DataTypeEnum.tpSmallDate)
                {
                    var to = (DateTime)value2;
                    value2 = to.AddDays(1).AddMilliseconds(-1);
                }

                return ComparisonTypeEnum.ctRange;
            }

            value1 = ConvertTo(filterValue);
            value2 = null;
            return ComparisonTypeEnum.ctEquals;
        }

        public enum ComparisonTypeEnum : short
        {
            ctUnknown = 0,
            ctEquals = 1,
            ctLesOrEquals = 2,
            ctLes = 3,
            ctBiggerOrEquals = 4,
            ctBigger = 5,
            ctContains = 6,
            ctStarts = 7,
            ctEnds = 8,
            ctRange = 9
        }

        private object ConvertTo(string value)
        {
            switch (_column.DataType)
            {
                case DataListColumnModel.DataTypeEnum.tpNumber:
                    return decimal.Parse(value);
                case DataListColumnModel.DataTypeEnum.tpDateTime:
                    var now = DateTime.Now;
                    var dateTimeParts = value.Split(" ");
                    var dateParts = dateTimeParts[0].Split(".");
                    var dtDay = int.Parse(dateParts[0]);
                    var dtMonth = dateParts.Length > 1 ? int.Parse(dateParts[1]) : now.Month;
                    var dtYear = dateParts.Length > 2 ? int.Parse(dateParts[2]) : now.Year;
                    var dtHour = 0;
                    var dtMinute = 0;
                    var dtSecond = 0;
                    if (dateTimeParts.Length > 1)
                    {
                        var timeParts = dateTimeParts[1].Split(":");
                        dtHour = int.Parse(timeParts[0]);
                        if (timeParts.Length > 1)
                            dtMinute = int.Parse(timeParts[1]);
                        if (timeParts.Length > 2)
                            dtSecond = int.Parse(timeParts[2]);
                    }
                    return new DateTime(dtYear, dtMonth, dtDay, dtHour, dtMinute, dtSecond);
                case DataListColumnModel.DataTypeEnum.tpSmallDate:
                case DataListColumnModel.DataTypeEnum.tpDate:
                    var today = DateTime.Today;
                    var parts = value.Split(".");
                    var tDay = int.Parse(parts[0]);
                    var tMonth = parts.Length > 1 ? int.Parse(parts[1]) : today.Month;
                    var tYear = parts.Length > 2 ? int.Parse(parts[2]) : today.Year;
                    return new DateTime(tYear, tMonth, tDay);
                default:
                    return value;
            }
        }
        private ComparisonTypeEnum GetStringOperation(string filterValue, out string value)
        {
            var isStarts = IsStarts(filterValue, out value);
            var isEnds = IsEnds(value, out value);
            if (isStarts && isEnds)
                return ComparisonTypeEnum.ctContains;
            if (isStarts)
                return ComparisonTypeEnum.ctStarts;
            if (isEnds)
                return ComparisonTypeEnum.ctEnds;

            value = filterValue;
            return ComparisonTypeEnum.ctEquals;
        }
        private bool IsStarts(string filterValue, out string value)
        {
            if (filterValue.LastIndexOf("*") == filterValue.Length - 1)
            {
                value = filterValue.Substring(0, filterValue.Length - 1);
                return true;
            }

            value = filterValue;
            return false;
        }
        private bool IsEnds(string filterValue, out string value)
        {
            if (filterValue.IndexOf("*") == 0)
            {
                value = filterValue.Substring(1);
                return true;
            }

            value = filterValue;
            return false;
        }
    }
}

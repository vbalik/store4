﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using S4.Web.Models.DataList;
using System.ComponentModel.DataAnnotations;

using S4.Web.Models;
using S4.Entities.Views;

namespace S4.Web.DataList
{
    public class InvoiceSettings : DataListSettings<InvoiceView, InvoiceSettingsModel>
    {
        internal override void CreateColumns(bool isSuperuser)
        {
            base.CreateColumns(isSuperuser);
                     
            SettingsModel.Columns.AddColumn()
               .SetVisible()
               .SetCaption("Doklad")
               .SetBold()
               .SetOrder(0)
               .SetWidth(4)
               .SetCommand(new DataListCommand() { CommandType = DataListCommand.CommandTypeEnum.cmDetail })
               .SetAllowHtmlFormating()
               .SetMultiColSep("<span class='docInfoSeparator'>/</span>")
               .AddColumn(SettingsModel.Columns["docPrefix"])
               .AddColumn(SettingsModel.Columns["docYear"].SetDisallowAutoWildcard())
               .AddColumn(SettingsModel.Columns["docNumber"]);

            SettingsModel.Columns["docStatus"].Visible = true;
            SettingsModel.Columns["docStatus"].Width = 4;
            SettingsModel.Columns["docStatus"].Order = 1;
            SettingsModel.Columns["docStatus"].SetAllowHtmlFormating();

            SettingsModel.Columns["lastDateTime"].Visible = true;
            SettingsModel.Columns["lastDateTime"].Order = 2;
            SettingsModel.Columns["lastDateTime"].DataType = DataListColumnModel.DataTypeEnum.tpDateTime;
            SettingsModel.Columns["lastDateTime"].SetAllowHtmlFormating();
            SettingsModel.Columns["lastDateTime"].Caption = "Změněno";

            SettingsModel.Columns["entryDateTime"].Visible = true;
            SettingsModel.Columns["entryDateTime"].Order = 3;
            SettingsModel.Columns["entryDateTime"].DataType = DataListColumnModel.DataTypeEnum.tpDateTime;
            SettingsModel.Columns["entryDateTime"].SetAllowHtmlFormating();
            SettingsModel.Columns["invoiceID"].SortMode = DataListColumnModel.SortModeEnum.smDescending;

            SettingsModel.Columns["directionCount"].Visible = true;
            SettingsModel.Columns["directionCount"].Order = 4;
            SettingsModel.Columns["directionCount"].Width = 3;
            SettingsModel.Columns["directionCount"].DisallowFiltering = true;


        }
    }
}

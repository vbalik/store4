﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using S4.Web.Models.DataList;
using System.ComponentModel.DataAnnotations;

using S4.Web.Models;
using S4.Entities.Views;
using S4.Entities;

namespace S4.Web.DataList
{
    public class BookingSettings : DataListSettings<BookingView, BookingSettingsModel>
    {
        internal override void CreateColumns(bool isSuperuser)
        {
            base.CreateColumns(isSuperuser);

            SettingsModel.Columns["articleCode"].Visible = true;
            SettingsModel.Columns["articleCode"].Command = new DataListCommand() { CommandType = DataListCommand.CommandTypeEnum.cmDetail };
            SettingsModel.Columns["articleCode"].Order = 0;

            SettingsModel.Columns["articleDesc"].Visible = true;
            SettingsModel.Columns["articleDesc"].Order = 1;

            SettingsModel.Columns["batchNum"].Visible = true;
            SettingsModel.Columns["batchNum"].Order = 2;

            SettingsModel.Columns["bookingStatus"].Visible = true;
            SettingsModel.Columns["bookingStatus"].Width = 2;
            SettingsModel.Columns["bookingStatus"].Order = 3;
            SettingsModel.Columns["bookingStatus"].SetAllowHtmlFormating();
                       
            SettingsModel.Columns["addrInfo2"].Visible = true;
            SettingsModel.Columns["addrInfo2"].Order = 4;
            SettingsModel.Columns["addrInfo2"].Width = 4;

            SettingsModel.Columns["requiredQuantity"].Visible = true;
            SettingsModel.Columns["requiredQuantity"].Order = 5;
            SettingsModel.Columns["requiredQuantity"].Width = 2;

            SettingsModel.Columns["bookingTimeout"].Visible = true;
            SettingsModel.Columns["bookingTimeout"].Order = 6;
            SettingsModel.Columns["bookingTimeout"].DataType = DataListColumnModel.DataTypeEnum.tpDate;
            SettingsModel.Columns["bookingTimeout"].SetAllowHtmlFormating();
            SettingsModel.Columns["bookingTimeout"].Width = 2;


            
            /*
            SettingsModel.Columns.AddColumn()
                .SetVisible()
                .SetCaption("Doklad")
                .SetBold()
                .SetOrder(7)
                .SetWidth(2)
                //.SetCommand(new DataListCommand() { CommandType = DataListCommand.CommandTypeEnum.cmDetail })
                .SetAllowHtmlFormating()
                .SetMultiColSep("<span class='docInfoSeparator'>/</span>")
                .AddColumn(SettingsModel.Columns["docNumPrefix"])
                .AddColumn(SettingsModel.Columns["docYear"].SetDataType(DataListColumnModel.DataTypeEnum.tpNumber))
                .AddColumn(SettingsModel.Columns["docNumber"]);
*/
        }

        internal override void CreateFilterOptions()
        {
            var items = new List<DataListFilterOption>();
            items.Add(new DataListFilterOption() { ItemCode = BookingSettingsModel.BookingFilterEnum.Valid, ItemDesc = "Platné" });
            items.Add(new DataListFilterOption() { ItemCode = BookingSettingsModel.BookingFilterEnum.All, ItemDesc = "Všechny" });

            this.SettingsModel.FilterOptions = items.ToArray();
            this.SettingsModel.FilterOption = BookingSettingsModel.BookingFilterEnum.Valid;
        }

        
    }
}

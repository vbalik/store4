﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using S4.Web.Models.DataList;
using System.Drawing;

using S4.Web.Models;

namespace S4.Web.DataList
{
    public class StockTakingSettings : DataListSettings<Entities.Views.StockTakingView, StockTakingSettingsModel>
    {
        internal override void CreateColumns(bool isSuperuser)
        {
            base.CreateColumns(isSuperuser);

            SettingsModel.Columns["stotakID"].Visible = false;
            SettingsModel.Columns["stotakID"].Width = 2;
            SettingsModel.Columns["stotakID"].Order = 0;
            //SettingsModel.Columns["stotakID"].Command = new DataListCommand() { CommandType = DataListCommand.CommandTypeEnum.cmDetail };

            SettingsModel.Columns["stotakDesc"].Visible = true;
            SettingsModel.Columns["stotakDesc"].Order = 1;
            SettingsModel.Columns["stotakDesc"].Command = new DataListCommand() { CommandType = DataListCommand.CommandTypeEnum.cmDetail };
            SettingsModel.Columns["stotakDesc"].Bold = true;

            SettingsModel.Columns["stotakStatus"].Visible = true;
            SettingsModel.Columns["stotakStatus"].Width = 4;
            SettingsModel.Columns["stotakStatus"].Order = 2;
            SettingsModel.Columns["stotakStatus"].AllowHtmlFormating = true;

            SettingsModel.Columns["beginDate"].Order = 3;
            SettingsModel.Columns["beginDate"].Width = 4;
            SettingsModel.Columns["beginDate"].Visible = true;
            SettingsModel.Columns["beginDate"].SortMode = DataListColumnModel.SortModeEnum.smDescending;

            SettingsModel.Columns["endDate"].Order = 4;
            SettingsModel.Columns["endDate"].Width = 4;
            SettingsModel.Columns["endDate"].Visible = true;

            SettingsModel.Columns["items"].Order = 5;
            SettingsModel.Columns["items"].Width = 2;
            SettingsModel.Columns["items"].Visible = true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using S4.Web.Models.DataList;
using System.ComponentModel.DataAnnotations;

using S4.Web.Models;

namespace S4.Web.DataList
{
    public class ArticleXtraSettings : DataListSettings<Entities.ArticleXtra, ArticleXtraSettingsModel>
    {
        internal override void CreateColumns(bool isSuperuser)
        {
            base.CreateColumns(isSuperuser);
            
            SettingsModel.Columns["articleExID"].Visible = true;
            SettingsModel.Columns["articleExID"].Width = 3;
            SettingsModel.Columns["articleExID"].Order = 0;
            SettingsModel.Columns["articleExID"].Caption = "ID";
            SettingsModel.Columns["articleExID"].SortMode = DataListColumnModel.SortModeEnum.smAscending;

            SettingsModel.Columns["xtraCode"].Visible = true;
            SettingsModel.Columns["xtraCode"].Width = 3;
            SettingsModel.Columns["xtraCode"].Order = 1;
            SettingsModel.Columns["xtraCode"].Caption = "Kód";

            SettingsModel.Columns["docPosition"].Visible = true;
            SettingsModel.Columns["docPosition"].Width = 2;
            SettingsModel.Columns["docPosition"].Order = 2;
            SettingsModel.Columns["docPosition"].Caption = "Pozice";

            SettingsModel.Columns["xtraValue"].Visible = true;
            SettingsModel.Columns["xtraValue"].Width = 4;
            SettingsModel.Columns["xtraValue"].Order = 3;
            SettingsModel.Columns["xtraValue"].Caption = "Hodnota";
                       
        }
    }
}

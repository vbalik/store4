﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using S4.Web.Models.DataList;
using System.Drawing;

using S4.Web.Models;

namespace S4.Web.DataList
{
    public class ArtOnStoreSettings : DataListSettings<Entities.Views.ArtOnStoreView, ArtOnStoreSettingsModel>
    {
        internal override void CreateColumns(bool isSuperuser)
        {
            base.CreateColumns(isSuperuser);

            if (this.SettingsModel.ArtOnStoreStyle != ArtOnStoreSettingsModel.ArtOnStoreStyleEnum.ByArticle)
            {
                SettingsModel.Columns["articleCode"].Visible = true;
                SettingsModel.Columns["articleCode"].Width = 4;
                SettingsModel.Columns["articleCode"].Order = 0;
                SettingsModel.Columns["articleCode"].Bold = true;
                SettingsModel.Columns["articleCode"].SortMode = DataListColumnModel.SortModeEnum.smAscending;
                SettingsModel.Columns["articleCode"].Command = new DataListCommand()
                {
                    CommandType = DataListCommand.CommandTypeEnum.cmCustom,
                    CommandId = "article_detail",
                    Title = "Zobrazit detail karty"
                };

                SettingsModel.Columns["articleDesc"].Visible = true;
                SettingsModel.Columns["articleDesc"].Order = 1;
            }

            if (this.SettingsModel.ArtOnStoreStyle != ArtOnStoreSettingsModel.ArtOnStoreStyleEnum.ByPosition)
            {
                SettingsModel.Columns["posCode"].Visible = true;
                SettingsModel.Columns["posCode"].Width = 2;
                SettingsModel.Columns["posCode"].Order = 2;
                SettingsModel.Columns["posCode"].SortMode = DataListColumnModel.SortModeEnum.smAscending;
                SettingsModel.Columns["posCode"].SortOrder = 0;
                SettingsModel.Columns["posCode"].Command = new DataListCommand()
                {
                    CommandType = DataListCommand.CommandTypeEnum.cmCustom,
                    CommandId = "position_detail",
                    Title = "Zobrazit detail pozice"
                };
            }

            SettingsModel.Columns["quantity"].Visible = true;
            SettingsModel.Columns["quantity"].Width = 2;
            SettingsModel.Columns["quantity"].Order = 3;

            SettingsModel.Columns["batchNum"].Visible = true;
            SettingsModel.Columns["batchNum"].Width = 2;
            SettingsModel.Columns["batchNum"].Order = 4;

            SettingsModel.Columns["expirationDesc"].Visible = true;
            SettingsModel.Columns["expirationDesc"].Width = 2;
            SettingsModel.Columns["expirationDesc"].Order = 5;
            SettingsModel.Columns["expirationDesc"].SortOrder = 1;
            SettingsModel.Columns["expirationDesc"].SortMode = DataListColumnModel.SortModeEnum.smAscending;

            SettingsModel.Columns["carrierNum"].Visible = true;
            SettingsModel.Columns["carrierNum"].Order = 6;
        }
    }
}

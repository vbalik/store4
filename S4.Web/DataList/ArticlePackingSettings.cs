﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using S4.Entities.Views;
using S4.Web.Models;

namespace S4.Web.DataList
{
    public class ArticlePackingSettings : DataListSettings<ArticlePackingView, ArticlePackingSettingsModel>
    {
        internal override void CreateColumns(bool isSuperuser)
        {
            base.CreateColumns(isSuperuser);

            SettingsModel.Columns["artPackID"].Visible = true;
            SettingsModel.Columns["artPackID"].Width = 2;
            SettingsModel.Columns["artPackID"].Order = 0;

            SettingsModel.Columns["packDesc"].Visible = true;
            SettingsModel.Columns["packDesc"].Width = 2;
            SettingsModel.Columns["packDesc"].Order = 1;

            SettingsModel.Columns["packRelation"].Visible = true;
            SettingsModel.Columns["packRelation"].Width = 2;
            SettingsModel.Columns["packRelation"].Order = 2;

            SettingsModel.Columns["movablePack"].Visible = true;
            SettingsModel.Columns["movablePack"].Width = 2;
            SettingsModel.Columns["movablePack"].Order = 3;

            SettingsModel.Columns["source_id"].Visible = true;
            SettingsModel.Columns["source_id"].Width = 2;
            SettingsModel.Columns["source_id"].Order = 4;

            SettingsModel.Columns["packStatus"].Visible = true;
            SettingsModel.Columns["packStatus"].Width = 4;
            SettingsModel.Columns["packStatus"].Order = 5;
            SettingsModel.Columns["packStatus"].Bold = true;
            SettingsModel.Columns["packStatus"].AllowHtmlFormating = true;

            SettingsModel.Columns["barCodes"].Visible = true;
            SettingsModel.Columns["barCodes"].Order = 6;
            SettingsModel.Columns["barCodes"].DisallowFiltering = true;
            SettingsModel.Columns["barCodes"].DisallowSorting = true;
            SettingsModel.Columns["barCodes"].SetAllowHtmlFormating();
            SettingsModel.Columns["barCodes"].SetDataType(Models.DataList.DataListColumnModel.DataTypeEnum.tpString);

            SettingsModel.Columns["inclCarrier"].Visible = true;
            SettingsModel.Columns["inclCarrier"].Width = 2;
            SettingsModel.Columns["inclCarrier"].Order = 7;

            SettingsModel.Columns["packWeight"].Visible = true;
            SettingsModel.Columns["packWeight"].Width = 2;
            SettingsModel.Columns["packWeight"].Order = 8;

            SettingsModel.Columns["packVolume"].Visible = true;
            SettingsModel.Columns["packVolume"].Width = 2;
            SettingsModel.Columns["packVolume"].Order = 9;
        }
    }
}

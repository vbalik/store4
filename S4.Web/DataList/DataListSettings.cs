﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection;

using S4.Web.Models.DataList;
using S4.Core.Extensions;
using S4.Entities.Helpers;

namespace S4.Web.DataList
{
    public class DataListSettings<T, TSettingsModel> 
        where T : new()
        where TSettingsModel: DataListSettingsModel<T>, new()
    {
        public TSettingsModel SettingsModel { get; set; } = new TSettingsModel();

        internal virtual void CreateFilterOptions() { }

        internal virtual void CreateColumns(bool isSuperUser)
        {
            if (SettingsModel.Columns == null)
                SettingsModel.Columns = new ColumnSettingsList();
            SettingsModel.Columns.WidthRecalcRequest += Columns_WidthRecalcRequest;

            foreach (var property in GetPublicProperies())
            {
                CreateColumn(property, isSuperUser);
            }

            //recalc width
            RecalcWidth();
        }

        internal virtual DataListColumnModel CreateColumn(PropertyInfo property, bool isSuperUser)
        {
            var fieldName = property.Name[0].ToString().ToLower() + property.Name.Substring(1);
            var column = SettingsModel.Columns[fieldName];

            if (string.IsNullOrEmpty(column.Caption))
            {
                var display = property.GetCustomAttribute(typeof(System.ComponentModel.DataAnnotations.DisplayAttribute));
                var name = display == null ? null : (display as System.ComponentModel.DataAnnotations.DisplayAttribute).Name;
                column.Caption = string.IsNullOrEmpty(name) ? property.Name : name;
            }
            if (column.DataType == DataListColumnModel.DataTypeEnum.tpUnknown)
            {
                column.DataType = GetDataType(property);
                if (column.DataType == DataListColumnModel.DataTypeEnum.tpList || column.DataType == DataListColumnModel.DataTypeEnum.tpFlag)
                {
                    column.ValuesList = GetEnumList(property, isSuperUser, column.DataType == DataListColumnModel.DataTypeEnum.tpFlag);
                }
            }

            if (string.IsNullOrEmpty(column.RawProperty))
            {
                var rawPropertyAttribute = property.GetCustomAttribute(typeof(Entities.Attributes.RawPropertyAttribute));
                column.RawProperty = rawPropertyAttribute == null ? null : (rawPropertyAttribute as Entities.Attributes.RawPropertyAttribute).Name;
            }

            return column;
        }

        private void Columns_WidthRecalcRequest(object sender, EventArgs e)
        {
            RecalcWidth();
        }

        private void RecalcWidth()
        {
            var fixedWidth = SettingsModel.Columns.Where(c => c.fixedWidth).Sum(c => c.Width);
            var restWidth = 24 - fixedWidth;
            var restColumns = SettingsModel.Columns.Where(c => c.Visible && !c.fixedWidth).ToArray();
            if (restColumns.Count() == 0)
                return;
            var width = (int)Math.Floor((decimal)restWidth / restColumns.Count());
            if (width == 0)
                width = 1;
            for (int i = 0; i < restColumns.Length - 1; i++)
            {
                restColumns[i]._width = width;
                restWidth -= width;
            }
            //last column
            restColumns[restColumns.Length - 1]._width = restWidth > 0 ? restWidth : 1;
        }

        private PropertyInfo[] GetPublicProperies()
        {
            return typeof(T).GetProperties(BindingFlags.Instance | BindingFlags.Public);
        }

        private DataListColumnModel.DataTypeEnum GetDataType(PropertyInfo property)
        {
            var propertyType = property.GetPropertyType();
            if (property.GetCustomAttribute(typeof(Entities.Attributes.IsFlagAttribute)) != null)
                return DataListColumnModel.DataTypeEnum.tpFlag;
            if (propertyType.BaseType != null && propertyType.BaseType == typeof(Enum))
                return DataListColumnModel.DataTypeEnum.tpList;
            if (property.GetCustomAttribute(typeof(Entities.Attributes.StatusSetAttribute)) != null)
                return DataListColumnModel.DataTypeEnum.tpList;
            if (propertyType == typeof(string))
                return DataListColumnModel.DataTypeEnum.tpString;
            if (propertyType == typeof(DateTime))
                return DataListColumnModel.DataTypeEnum.tpDate;
            if (propertyType == typeof(float) ||
                propertyType == typeof(double) ||
                propertyType == typeof(byte) ||
                propertyType == typeof(short) ||
                propertyType == typeof(int) ||
                propertyType == typeof(long))
                return DataListColumnModel.DataTypeEnum.tpNumber;
            if (propertyType == typeof(bool))
                return DataListColumnModel.DataTypeEnum.tpBool;
            if (propertyType == typeof(decimal))
                return DataListColumnModel.DataTypeEnum.tpDecimal;
            return DataListColumnModel.DataTypeEnum.tpUnknown;
        }

        protected Entities.StatusEngine.Status[] GetEnumList(PropertyInfo property, bool isSuperUser, bool isFlag)
        {
            if (property.GetCustomAttribute(typeof(Entities.Attributes.StatusSetAttribute)) != null)
            {
                var listProperty = (property.GetCustomAttribute(typeof(Entities.Attributes.StatusSetAttribute)) as Entities.Attributes.StatusSetAttribute).ListProperty;
                var events = (property.GetCustomAttribute(typeof(Entities.Attributes.StatusSetAttribute)) as Entities.Attributes.StatusSetAttribute).Events;
                var list = events ? 
                    (typeof(T).GetFieldAllBases(listProperty).GetValue(null) as Entities.StatusEngine.StatusSet).Events.Select(e => new Entities.StatusEngine.Status() { Code = e.Code, Name = e.Name, Color = e.Color }).ToArray(): 
                    (typeof(T).GetFieldAllBases(listProperty).GetValue(null) as Entities.StatusEngine.StatusSet).Items.ToArray();
                // done if superuser
                if (isSuperUser)
                    return list;
                // else remove superuser actions
                var resList = new List<Entities.StatusEngine.Status>();
                foreach (var item in list)
                {
                    var newItem = new Entities.StatusEngine.Status()
                    {
                        Actions = item.Actions.Where(a => a.ActionAuthorization != Entities.StatusEngine.ActionAuthorizationEnum.SuperUser).ToList(),
                        Code = item.Code,
                        Color = item.Color,
                        CssClass = item.CssClass,
                        CssStyle = item.CssStyle,
                        DocDirection = item.DocDirection,
                        Html = item.Html,
                        Name = item.Name,
                        NextStatuses = item.NextStatuses
                    };
                    resList.Add(newItem);
                }
                return resList.ToArray();
            }

            return GetEnumList(property.PropertyType, isFlag);
        }

        protected Entities.StatusEngine.Status[] GetEnumList(Type type, bool isFlag)
        {
            return Enum.GetValues(type)
                .OfType<object>()
                .Select(i => new { Item = i, Display = (System.ComponentModel.DataAnnotations.DisplayAttribute)type.GetField(i.ToString()).GetCustomAttribute(typeof(System.ComponentModel.DataAnnotations.DisplayAttribute)) })
                .Select(i => new Entities.StatusEngine.Status() { Code = isFlag ? ((int)i.Item).ToString() : i.Item.ToString(), Name = i.Display == null || string.IsNullOrEmpty(i.Display.Name) ? i.Item.ToString() : i.Display.Name })
                .Where(i => isFlag ? i.Code != (0).ToString() : true)
                .ToArray();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using S4.Web.Models.DataList;
using System.ComponentModel.DataAnnotations;

using S4.Web.Models;
using S4.Entities.Views;

namespace S4.Web.DataList
{
    public class CertificateSettings : DataListSettings<Entities.Certificate, CertificateSettingsModel>
    {
        internal override void CreateColumns(bool isSuperuser)
        {
            base.CreateColumns(isSuperuser);

            SettingsModel.Columns["certificateID"].Visible = true;
            SettingsModel.Columns["certificateID"].SortMode = DataListColumnModel.SortModeEnum.smAscending;
            SettingsModel.Columns["certificateID"].DataType = DataListColumnModel.DataTypeEnum.tpNumber;
            SettingsModel.Columns["certificateInfo"].Caption = "ID";
            SettingsModel.Columns["certificateID"].Width = 1;
            SettingsModel.Columns["certificateID"].Order = 0;

            SettingsModel.Columns["expirationDateFrom"].Visible = true;
            SettingsModel.Columns["expirationDateFrom"].SortMode = DataListColumnModel.SortModeEnum.smAscending;
            SettingsModel.Columns["expirationDateFrom"].DataType = DataListColumnModel.DataTypeEnum.tpDateTime;
            SettingsModel.Columns["expirationDateFrom"].Width = 4;
            SettingsModel.Columns["expirationDateFrom"].Order = 1;

            SettingsModel.Columns["expirationDateTo"].Visible = true;
            SettingsModel.Columns["expirationDateTo"].SortMode = DataListColumnModel.SortModeEnum.smAscending;
            SettingsModel.Columns["expirationDateTo"].DataType = DataListColumnModel.DataTypeEnum.tpDateTime;
            SettingsModel.Columns["expirationDateTo"].Width = 4;
            SettingsModel.Columns["expirationDateTo"].Order = 2;

            SettingsModel.Columns["certificateType"].Visible = true;
            SettingsModel.Columns["certificateType"].Command = new DataListCommand() { CommandType = DataListCommand.CommandTypeEnum.cmEdit };
            SettingsModel.Columns["certificateType"].Width = 4;
            SettingsModel.Columns["certificateType"].Order = 3;

            SettingsModel.Columns["certificateInfo"].Visible = true;
            SettingsModel.Columns["certificateInfo"].Caption = "Certifikát info";
            SettingsModel.Columns["certificateInfo"].AllowHtmlFormating = true;
            SettingsModel.Columns["certificateInfo"].Width = 4;
            SettingsModel.Columns["certificateInfo"].Order = 4;


        }
    }
}

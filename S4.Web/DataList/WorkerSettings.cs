﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using S4.Web.Models.DataList;
using System.ComponentModel.DataAnnotations;

using S4.Web.Models;

namespace S4.Web.DataList
{
    public class WorkerSettings : DataListSettings<Entities.Worker, WorkerSettingsModel>
    {
        internal override void CreateColumns(bool isSuperuser)
        {
            base.CreateColumns(isSuperuser);

            SettingsModel.Columns["workerID"].Visible = true;
            SettingsModel.Columns["workerID"].Command = new DataListCommand() { CommandType = DataListCommand.CommandTypeEnum.cmDetail };
            SettingsModel.Columns["workerID"].Width = 2;
            SettingsModel.Columns["workerID"].Order = 0;
            SettingsModel.Columns["workerID"].Bold = true;

            SettingsModel.Columns["workerName"].Visible = true;
            SettingsModel.Columns["workerName"].Width = 6;
            SettingsModel.Columns["workerName"].Order = 1;

            SettingsModel.Columns["workerStatus"].Visible = true;
            SettingsModel.Columns["workerStatus"].Width = 4;
            SettingsModel.Columns["workerStatus"].Order = 2;
            SettingsModel.Columns["workerStatus"].AllowHtmlFormating = true;

            SettingsModel.Columns["workerClassString"].Visible = true;
            SettingsModel.Columns["workerClassString"].Order = 3;
        }

        internal override void CreateFilterOptions()
        {
            this.SettingsModel.FilterOptions = new DataListFilterOption[]
            {
                new DataListFilterOption() { ItemCode = WorkerSettingsModel.WorkerFilterEnum.Active, ItemDesc = "Ve stavu" },
                new DataListFilterOption() { ItemCode = WorkerSettingsModel.WorkerFilterEnum.All, ItemDesc = "Všichni" }
            };

            this.SettingsModel.FilterOption = WorkerSettingsModel.WorkerFilterEnum.Active;
        }
    }
}

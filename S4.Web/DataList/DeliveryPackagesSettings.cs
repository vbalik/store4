﻿using S4.Web.Models.DataList;

using S4.Web.Models;

namespace S4.Web.DataList
{
    public class DeliveryPackagesSettings : DataListSettings<Entities.Views.DeliveryPackagesView, DeliveryPackagesSettingsModel>
    {
        internal override void CreateColumns(bool isSuperuser)
        {
            base.CreateColumns(isSuperuser);

            SettingsModel.Columns["parcelRefNumber"]
                .SetVisible()
                .SetOrder(0)
                .SetDisallowSorting();

            SettingsModel.Columns["price"]
                .SetVisible()
                .SetOrder(1)
                .SetDisallowSorting();

            SettingsModel.Columns["address"]
                .SetVisible()
                .SetOrder(2)
                .SetDisallowSorting();

            SettingsModel.Columns["zip"]
                .SetVisible()
                .SetOrder(3)
                .SetDisallowSorting();

            SettingsModel.Columns["contact"]
                .SetVisible()
                .SetOrder(4)
                .SetDisallowSorting();

            SettingsModel.Columns["phone"]
                .SetVisible()
                .SetOrder(5)
                .SetDisallowSorting();

            SettingsModel.Columns["invoiceNumber"]
                .SetVisible()
                .SetOrder(6)
                .SetDisallowSorting();

            SettingsModel.Columns["docInfo"]
                .SetVisible()
                .SetOrder(7)
                .SetDisallowSorting();
        }
    }
}

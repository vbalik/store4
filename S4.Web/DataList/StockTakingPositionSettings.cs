﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using S4.Web.Models.DataList;
using S4.Web.Models;

namespace S4.Web.DataList
{
    public class StockTakingPositionSettings : DataListSettings<Entities.Views.StockTakingPositionView, StockTakingPositionSettingsModel>
    { 
        internal override void CreateColumns(bool isSuperuser)
        {
            base.CreateColumns(isSuperuser);

            SettingsModel.Columns["posCode"].Visible = true;
            SettingsModel.Columns["posCode"].Width = 2;
            SettingsModel.Columns["posCode"].Order = 0;
            SettingsModel.Columns["posCode"].SortMode = Models.DataList.DataListColumnModel.SortModeEnum.smAscending;
            SettingsModel.Columns["posCode"].Command = new DataListCommand()
            {
                CommandType = DataListCommand.CommandTypeEnum.cmCustom,
                CommandId = "position_detail",
                Title = "Zobrazit detail pozice"
            };

            SettingsModel.Columns["houseID"].Visible = true;
            SettingsModel.Columns["houseID"].Width = 2;
            SettingsModel.Columns["houseID"].Order = 1;

            SettingsModel.Columns["checkCounter"].Visible = true;
            SettingsModel.Columns["checkCounter"].Width = 4;
            SettingsModel.Columns["checkCounter"].Order = 2;
        }
    }
}

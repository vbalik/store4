﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using S4.Web.Models.DataList;
using System.ComponentModel.DataAnnotations;

using S4.Web.Models;

namespace S4.Web.DataList
{
    public class StockTakingHistorySettings : DataListSettings<Entities.Views.StockTakingHistoryView, StockTakingHistorySettingsModel>
    {
        internal override void CreateColumns(bool isSuperuser)
        {
            base.CreateColumns(isSuperuser);

            SettingsModel.Columns["eventCode"].Visible = true;
            SettingsModel.Columns["eventCode"].Width = 2;
            SettingsModel.Columns["eventCode"].Order = 0;
            SettingsModel.Columns["eventCode"].AllowHtmlFormating = true;

            SettingsModel.Columns["posCode"].Visible = true;
            SettingsModel.Columns["posCode"].Width = 2;
            SettingsModel.Columns["posCode"].Order = 1;

            SettingsModel.Columns["articleCode"].Visible = true;
            SettingsModel.Columns["articleCode"].Width = 2;
            SettingsModel.Columns["articleCode"].Order = 2;

            SettingsModel.Columns["articleDesc"].Visible = true;
            SettingsModel.Columns["articleDesc"].Order = 3;

            SettingsModel.Columns["quantity"].Visible = true;
            SettingsModel.Columns["quantity"].Width = 2;
            SettingsModel.Columns["quantity"].Order = 4;

            SettingsModel.Columns["carrierNumView"].Visible = true;
            SettingsModel.Columns["carrierNumView"].Width = 4;
            SettingsModel.Columns["carrierNumView"].Order = 5;

            SettingsModel.Columns["batchNumView"].Visible = true;
            SettingsModel.Columns["batchNumView"].Width = 2;
            SettingsModel.Columns["batchNumView"].Order = 6;

            SettingsModel.Columns["expirationDate"].Visible = true;
            SettingsModel.Columns["expirationDate"].Width = 2;
            SettingsModel.Columns["expirationDate"].Order = 7;
            SettingsModel.Columns["expirationDate"].DataType = DataListColumnModel.DataTypeEnum.tpSmallDate;

            SettingsModel.Columns["entryDateTime"].Visible = true;
            SettingsModel.Columns["entryDateTime"].Width = 2;
            SettingsModel.Columns["entryDateTime"].Order = 8;
            SettingsModel.Columns["entryDateTime"].DataType = DataListColumnModel.DataTypeEnum.tpDateTime;

            SettingsModel.Columns["entryUserID"].Visible = true;
            SettingsModel.Columns["entryUserID"].Width = 2;
            SettingsModel.Columns["entryUserID"].Order = 9;
        }
    }
}

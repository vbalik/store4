﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using S4.Web.Models.DataList;
using System.Drawing;

using S4.Web.Models;

namespace S4.Web.DataList
{
    public class ArticleSettings : DataListSettings<Entities.Views.ArticleView, ArticleSettingsModel>
    {
        internal override void CreateColumns(bool isSuperuser)
        {
            base.CreateColumns(isSuperuser);

            SettingsModel.Columns["articleCode"]
                .SetVisible()
                .SetWidth(4)
                .SetOrder(1)
                .SetBold()
                .SetSortMode(DataListColumnModel.SortModeEnum.smAscending)
                .SetCommand(new DataListCommand() { CommandType = DataListCommand.CommandTypeEnum.cmDetail })
                .SetTrimColumnInCondition();

            SettingsModel.Columns["articleDesc"]
                .SetVisible()
                .SetOrder(2)
                .SetTrimColumnInCondition();

            SettingsModel.Columns["articleStatus"]
                .SetVisible()
                .SetWidth(2)
                .SetOrder(3)
                .SetAllowHtmlFormating();

            SettingsModel.Columns["articleType"]
                .SetOrder(4);

            SettingsModel.Columns["source_id"]
                .SetOrder(5);

            SettingsModel.Columns["sectID"]
                .SetVisible()
                .SetWidth(2)
                .SetOrder(6);

            SettingsModel.Columns["manufID"]
                .SetVisible()
                .SetWidth(2)
                .SetOrder(7);

            SettingsModel.Columns["brand"]
                .SetVisible()
                .SetWidth(2)
                .SetOrder(8);

            SettingsModel.Columns["specFeatures"]
                .SetOrder(9)
                .SetVisible()
                .SetAllowHtmlFormating()
                .SetDisallowSorting()
                .SetWidth(2)
                .Caption = "Spec.vlastn.";

            SettingsModel.Columns["quantity"]
                .SetVisible()
                .SetWidth(2)
                .SetOrder(10)
                .SetDisallowFiltering()
                .SetDisallowSorting();

            SettingsModel.Columns["unitDesc"]
                .SetVisible()
                .SetWidth(2)
                .SetOrder(11);

            SettingsModel.Columns["useBatch"]
                .SetOrder(12);

            SettingsModel.Columns["mixBatch"]
                .SetOrder(13);

            SettingsModel.Columns["useExpiration"]
                .SetOrder(14);

            SettingsModel.Columns["useSerialNumber"]
                .SetOrder(15);

            SettingsModel.Columns["userRemark"]
                .SetOrder(16);
        }

        internal override void CreateFilterOptions()
        {
            this.SettingsModel.FilterOptions = new DataListFilterOption[]
            {
                new DataListFilterOption() { ItemCode = ArticleSettingsModel.ArticleFilterEnum.Used, ItemDesc = "Používané" },
                new DataListFilterOption() { ItemCode = ArticleSettingsModel.ArticleFilterEnum.UsedNoActiva, ItemDesc = "Používané bez ACTIVA" },
                new DataListFilterOption() { ItemCode = ArticleSettingsModel.ArticleFilterEnum.OnStore, ItemDesc = "Skladem" },
                new DataListFilterOption() { ItemCode = ArticleSettingsModel.ArticleFilterEnum.All, ItemDesc = "Všechny" }
            };

            this.SettingsModel.FilterOption = ArticleSettingsModel.ArticleFilterEnum.Used;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using S4.Web.Models.DataList;

using S4.Web.Models;

namespace S4.Web.DataList
{
    public class DirectionHistorySettings : DataListSettings<Entities.DirectionHistory, DirectionHistorySettingsModel>
    {
        internal override void CreateColumns(bool isSuperuser)
        {
            base.CreateColumns(isSuperuser);

            SettingsModel.Columns["docPosition"].Visible = true;
            SettingsModel.Columns["docPosition"].Width = 2;
            SettingsModel.Columns["docPosition"].Order = 1;

            SettingsModel.Columns["eventCode"].Visible = true;
            SettingsModel.Columns["eventCode"].Width = 4;
            SettingsModel.Columns["eventCode"].Order = 2;
            SettingsModel.Columns["eventCode"].Bold = true;
            SettingsModel.Columns["eventCode"].AllowHtmlFormating = true;

            SettingsModel.Columns["eventData"].Visible = true;
            SettingsModel.Columns["eventData"].Order = 3;
            SettingsModel.Columns["eventData"].DisallowFiltering = true;
            SettingsModel.Columns["eventData"].DisallowSorting = true;
            SettingsModel.Columns["eventData"].VueColumn = "history_parser_column";

            SettingsModel.Columns["entryDateTime"].Visible = true;
            SettingsModel.Columns["entryDateTime"].Width = 4;
            SettingsModel.Columns["entryDateTime"].DataType = DataListColumnModel.DataTypeEnum.tpDateTime;
            SettingsModel.Columns["entryDateTime"].SortMode = DataListColumnModel.SortModeEnum.smAscending;
            SettingsModel.Columns["entryDateTime"].Order = 4;

            SettingsModel.Columns["entryUserID"].Visible = true;
            SettingsModel.Columns["entryUserID"].Width = 2;
            SettingsModel.Columns["entryUserID"].Order = 5;
        }
    }
}

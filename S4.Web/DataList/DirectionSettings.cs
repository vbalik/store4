﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using S4.Web.Models.DataList;

using S4.Web.Models;
using S4.Core;

namespace S4.Web.DataList
{
    public class DirectionSettings : DataListSettings<Entities.Views.DirectionView, DirectionSettingsModel>
    {
        internal override void CreateColumns(bool isSuperuser)
        {
            base.CreateColumns(isSuperuser);

            SettingsModel.Columns["directionID"].SortMode = DataListColumnModel.SortModeEnum.smDescending;

            SettingsModel.Columns.AddColumn()
                .SetVisible()
                .SetCaption("Doklad")
                .SetBold()
                .SetOrder(0)
                .SetWidth(2)
                .SetCommand(new DataListCommand() { CommandType = DataListCommand.CommandTypeEnum.cmDetail })
                .SetAllowHtmlFormating()
                .SetMultiColSep("<span class='docInfoSeparator'>/</span>")
                .AddColumn(SettingsModel.Columns["docNumPrefix"])
                .AddColumn(SettingsModel.Columns["docYear"].SetDisallowAutoWildcard())
                .AddColumn(SettingsModel.Columns["docNumber"]);

            SettingsModel.Columns["docNumPrefix"].SetDisallowAutoWildcard(true);

            SettingsModel.Columns["docStatus"].Visible = true;
            SettingsModel.Columns["docStatus"].Width = 2;
            SettingsModel.Columns["docStatus"].Order = 4;
            SettingsModel.Columns["docStatus"].SetAllowHtmlFormating();

            SettingsModel.Columns["docDate"].Visible = true;
            SettingsModel.Columns["docDate"].Width = 2;
            SettingsModel.Columns["docDate"].Order = 5;

            SettingsModel.Columns["partnerName"].Visible = true;
            SettingsModel.Columns["partnerName"].Width = 8;
            SettingsModel.Columns["partnerName"].Order = 6;
            
            SettingsModel.Columns["directionOutCompleted"]
                .SetVisible()
                .SetDisallowSorting()
                .SetDisallowFiltering()
                .SetVueColumn("status_progressbar")
                .SetWidth(1)
                .SetOrder(7);

            SettingsModel.Columns["signs"].Visible = true;
            SettingsModel.Columns["signs"].Width = 2;
            SettingsModel.Columns["signs"].DisallowSorting = true;
            SettingsModel.Columns["signs"].Order = 8;
            SettingsModel.Columns["signs"].AllowHtmlFormating = true;

            Func<Entities.Article.SpecFeaturesEnum, Entities.StatusEngine.Status> getStatusItem = (item) =>
            {
                string code;
                switch (item)
                {
                    case Entities.Article.SpecFeaturesEnum.Any:
                        code = null;
                        break;
                    case Entities.Article.SpecFeaturesEnum.Cooled:
                        code = Entities.Direction.XTRA_ARTICLE_COOLED;
                        break;
                    case Entities.Article.SpecFeaturesEnum.Cytostatics:
                        code = Entities.Direction.XTRA_ARTICLE_CYTOSTATICS;
                        break;
                    case Entities.Article.SpecFeaturesEnum.Medicines:
                        code = Entities.Direction.XTRA_ARTICLE_MEDICINES;
                        break;
                    case Entities.Article.SpecFeaturesEnum.CheckOnReceive:
                        code = Entities.Direction.XTRA_ARTICLE_CHECK_ON_RECEIVE;
                        break;
                    default:
                        throw new NotImplementedException($"Direction sign xtra code \"{item}\"");
                }
                return new Entities.StatusEngine.Status()
                {
                    Code = item.ToString(),
                    Name = (item.GetType().GetField(item.ToString()).GetCustomAttribute(typeof(System.ComponentModel.DataAnnotations.DisplayAttribute)) as System.ComponentModel.DataAnnotations.DisplayAttribute).Name,
                    FilterSubquery = item == Entities.Article.SpecFeaturesEnum.Any ?
                    $"NOT EXISTS (select directionID from s4_direction_xtra where directionID = d.directionID and (xtraCode = '{Entities.Direction.XTRA_ARTICLE_COOLED}' or xtraCode = '{Entities.Direction.XTRA_ARTICLE_CYTOSTATICS}' or xtraCode = '{Entities.Direction.XTRA_ARTICLE_MEDICINES}') or xtraCode = '{Entities.Direction.XTRA_ARTICLE_CHECK_ON_RECEIVE}' and xtraValue_checksum = checksum('True') and xtraValue = 'True')" :
                    $"d.directionID in (select directionID from s4_direction_xtra where xtraCode = '{code}' and xtraValue_checksum = checksum('True') and xtraValue = 'True')"
                };
            };

            var valuesList = new List<Entities.StatusEngine.Status>();

            foreach (Entities.Article.SpecFeaturesEnum item in Enum.GetValues(typeof(Entities.Article.SpecFeaturesEnum)))
            {
                valuesList.Add(getStatusItem(item));
            }
            valuesList.Add(new Entities.StatusEngine.Status()
            {
                Code = Entities.Direction.XTRA_MANUF_ICON,
                Name = "Výrobce",
                FilterSubquery = $"d.directionID in (select directionID from s4_direction_xtra where xtraCode = '{Entities.Direction.XTRA_MANUF_ICON}')"
            });

            SettingsModel.Columns["signs"].SetValuesList(valuesList.ToArray()).SetDataType(DataListColumnModel.DataTypeEnum.tpList);


            SettingsModel.Columns["deliveryInst"].Visible = true;
            SettingsModel.Columns["deliveryInst"].Order = 9;

            SettingsModel.Columns["workerID"].Visible = true;
            SettingsModel.Columns["workerID"].Width = 1;
            SettingsModel.Columns["workerID"].DisallowFiltering = true;
            SettingsModel.Columns["workerID"].DisallowSorting = true;
            SettingsModel.Columns["workerID"].Order = 9;
            SettingsModel.Columns["workerID"].Caption = this.SettingsModel.Dir == DirectionSettingsModel.DirectionEnum.In ? "Přijal" : "Vyskladnil";

            SettingsModel.Columns["entryDateTime"].DataType = DataListColumnModel.DataTypeEnum.tpDateTime;

            // delivery service provider only from settings
            if (SettingsModel.Dir == DirectionSettingsModel.DirectionEnum.Out)
            {
                var isColumnShowed = Singleton<DAL.SettingsSet>.Instance.GetValue_Boolean(Entities.SettingsDictionary.DELIVERY_SHOW_COLUMN_GRID);
                
                if(isColumnShowed)
                    SettingsModel.Columns["deliveryProvider"]
                        .SetVisible()
                        .SetCaption("Dopravce")
                        .SetDisallowSorting()
                        .SetDisallowFiltering()
                        .SetOrder(12)
                        .SetWidth(1);
            }
        }

        internal override void CreateFilterOptions()
        {
            var items = new List<DataListFilterOption>();
            items.Add(new DataListFilterOption() { ItemCode = DirectionSettingsModel.DirectionFilterEnum.NotProcessed, ItemDesc = "Nevyřízené" });
            if (this.SettingsModel.Dir == DirectionSettingsModel.DirectionEnum.In)
            {
                items.Add(new DataListFilterOption() { ItemCode = DirectionSettingsModel.DirectionFilterEnum.Receive, ItemDesc = "Příjem" });
                items.Add(new DataListFilterOption() { ItemCode = DirectionSettingsModel.DirectionFilterEnum.StoreIn, ItemDesc = "Naskladnění" });
            }
            if (this.SettingsModel.Dir == DirectionSettingsModel.DirectionEnum.Out)
            {
                items.Add(new DataListFilterOption() { ItemCode = DirectionSettingsModel.DirectionFilterEnum.Loaded, ItemDesc = "Nahráno" });
                items.Add(new DataListFilterOption() { ItemCode = DirectionSettingsModel.DirectionFilterEnum.Dispatch, ItemDesc = "Vychystání" });
                items.Add(new DataListFilterOption() { ItemCode = DirectionSettingsModel.DirectionFilterEnum.Check, ItemDesc = "Kontrola" });
            }
            items.Add(new DataListFilterOption() { ItemCode = DirectionSettingsModel.DirectionFilterEnum.All, ItemDesc = "Všechny" });

            this.SettingsModel.FilterOptions = items.ToArray();
            this.SettingsModel.FilterOption = DirectionSettingsModel.DirectionFilterEnum.NotProcessed;
        }

        internal override DataListColumnModel CreateColumn(PropertyInfo property, bool isSuperuser)
        {
            var column = base.CreateColumn(property, isSuperuser);

            if (column.FieldName == "docStatus")
            {
                var actionList = column.ValuesList.Where(i => i.Code == Entities.Direction.DR_STATUS_LOAD).Single().Actions;
                switch (SettingsModel.Dir)
                {
                    case DirectionSettingsModel.DirectionEnum.Out:
                        if (!actionList.Contains(Entities.Direction.dispatchAction))
                            actionList.Insert(0, Entities.Direction.dispatchAction);
                        if (!actionList.Contains(Entities.Direction.testReservationAction))
                            actionList.Insert(1, Entities.Direction.testReservationAction);
                        break;
                    case DirectionSettingsModel.DirectionEnum.In:
                        if (actionList.Contains(Entities.Direction.dispatchAction))
                            actionList.Remove(Entities.Direction.dispatchAction);
                        if (actionList.Contains(Entities.Direction.testReservationAction))
                            actionList.Remove(Entities.Direction.testReservationAction);
                        break;
                }
                if (SettingsModel.Dir == DirectionSettingsModel.DirectionEnum.Out && !actionList.Contains(Entities.Direction.dispatchAction))
                    actionList.Insert(0, Entities.Direction.dispatchAction);
                else if (SettingsModel.Dir == DirectionSettingsModel.DirectionEnum.In && actionList.Contains(Entities.Direction.dispatchAction))
                    actionList.Remove(Entities.Direction.dispatchAction);
            }

            return column;
        }
    }
}

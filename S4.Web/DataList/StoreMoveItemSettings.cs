﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using S4.Web.Models.DataList;
using System.ComponentModel.DataAnnotations;

using S4.Web.Models;

namespace S4.Web.DataList
{
    public class StoreMoveItemSettings : DataListSettings<Entities.Views.StoreMoveItemView, StoreMoveItemSettingsModel>
    {
        internal override void CreateColumns(bool isSuperuser)
        {
            base.CreateColumns(isSuperuser);

            if (SettingsModel.StoMoveID == 0)
            {
                SettingsModel.Columns["doc"].SetVisible().SetWidth(this.SettingsModel.PositionID == 0 ? 3 : 2).SetOrder(2).SetVueColumn("doc_column");
                if (SettingsModel.DirectionID == 0)
                {
                    SettingsModel.Columns["dir"].SetVisible().SetWidth(this.SettingsModel.PositionID == 0 ? 3 : 2).SetOrder(3).SetVueColumn("dir_column");
                }
            }
            if (this.SettingsModel.StoMoveID != 0)
            {
                SettingsModel.Columns["docPosition"].SetVisible().SetWidth(1).SetOrder(4);
            }
            SettingsModel.Columns["docStatus"].SetVisible().SetWidth(SettingsModel.ArticleID != 0 ? 0 : 3).SetOrder(5).SetAllowHtmlFormating();
            if (this.SettingsModel.StoMoveID == 0)
            {
                SettingsModel.Columns["docDate"].SetVisible().SetWidth(SettingsModel.PositionID == 0 ? 3 : 2).SetOrder(6)
                    .SetSortMode(DataListColumnModel.SortModeEnum.smDescending)
                    .SetDataType(DataListColumnModel.DataTypeEnum.tpDateTime);
            }
            if (this.SettingsModel.PositionID == 0)
            {
                SettingsModel.Columns["posCode"].SetVisible().SetWidth(2).SetOrder(6)
                    .SetCommand(new DataListCommand()
                    {
                        CommandType = DataListCommand.CommandTypeEnum.cmCustom,
                        CommandId = "position_detail",
                        Title = "Zobrazit detail pozice"
                    });
            }
            SettingsModel.Columns["itemDirection"].SetVisible().SetWidth(1).SetOrder(7)
                .SetDataType(DataListColumnModel.DataTypeEnum.tpList)
                .SetSortMode(DataListColumnModel.SortModeEnum.smAscending)
                .SetValuesList(new Entities.StatusEngine.Status[]
                {
                    new Entities.StatusEngine.Status() { Code = "-1", Name = "-" },
                    new Entities.StatusEngine.Status() { Code = "1", Name = "+" }
                });
            SettingsModel.Columns["quantity"].SetVisible().SetWidth(2).SetOrder(8);
            if (this.SettingsModel.ArticleID == 0 && SettingsModel.DirectionID == 0)
            {
                SettingsModel.Columns["articleCode"].SetVisible().SetWidth(4).SetOrder(9).SetBold()
                    .SetCommand(new DataListCommand()
                    {
                        CommandType = DataListCommand.CommandTypeEnum.cmCustom,
                        CommandId = "article_detail",
                        Title = "Zobrazit detail karty"
                    });
                SettingsModel.Columns["articleDesc"].SetVisible().SetOrder(10);
            }
            if (SettingsModel.DirectionID != 0)
            {
                SettingsModel.Columns["carrierNum"].SetVisible().SetOrder(11);
            }
            SettingsModel.Columns["batchNum"].SetVisible().SetWidth(2).SetOrder(12);
            SettingsModel.Columns["expirationDate"].SetVisible().SetWidth(2).SetOrder(13).SetDataType(DataListColumnModel.DataTypeEnum.tpSmallDate);
            SettingsModel.Columns["entryUserID"].SetVisible().SetWidth(2).SetOrder(14);
        }
    }
}

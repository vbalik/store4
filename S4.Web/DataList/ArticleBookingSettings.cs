﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using S4.Web.Models.DataList;
using System.ComponentModel.DataAnnotations;

using S4.Web.Models;
using S4.Entities.Views;
using S4.Entities;

namespace S4.Web.DataList
{
    public class ArticleBookingSettings : DataListSettings<BookingView, ArticleBookingSettingsModel>
    {
        internal override void CreateColumns(bool isSuperuser)
        {
            base.CreateColumns(isSuperuser);

            SettingsModel.Columns["batchNum"].Visible = true;
            SettingsModel.Columns["batchNum"].Order = 2;

            SettingsModel.Columns["bookingStatus"].Visible = true;
            SettingsModel.Columns["bookingStatus"].Width = 2;
            SettingsModel.Columns["bookingStatus"].Order = 3;
            SettingsModel.Columns["bookingStatus"].AllowHtmlFormating = true;

            SettingsModel.Columns["partnerName"].Visible = true;
            SettingsModel.Columns["partnerName"].Order = 4;

            SettingsModel.Columns["requiredQuantity"].Visible = true;
            SettingsModel.Columns["requiredQuantity"].Order = 5;
            SettingsModel.Columns["requiredQuantity"].Width = 2;

            SettingsModel.Columns["batchNum"].Visible = true;
            SettingsModel.Columns["batchNum"].Order = 6;
            SettingsModel.Columns["batchNum"].Width = 2;

            SettingsModel.Columns["bookingTimeout"].Visible = true;
            SettingsModel.Columns["bookingTimeout"].Order = 7;
            SettingsModel.Columns["bookingTimeout"].DataType = DataListColumnModel.DataTypeEnum.tpDate;
            SettingsModel.Columns["bookingTimeout"].AllowHtmlFormating = true;
            SettingsModel.Columns["bookingTimeout"].Width = 2;

            SettingsModel.Columns["entryDateTime"].Visible = true;
            SettingsModel.Columns["entryDateTime"].Order = 8;
            SettingsModel.Columns["entryDateTime"].DataType = DataListColumnModel.DataTypeEnum.tpDate;
            SettingsModel.Columns["entryDateTime"].Width = 2;

            SettingsModel.Columns["entryUserName"].Visible = true;
            SettingsModel.Columns["entryUserName"].Order = 9;
                        
         

        }

      

        
    }
}

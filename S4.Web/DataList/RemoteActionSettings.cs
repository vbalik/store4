﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using S4.Web.Models.DataList;
using System.ComponentModel.DataAnnotations;

using S4.Web.Models;
using S4.Entities;

namespace S4.Web.DataList
{
    public class RemoteActionSettings : DataListSettings<RemoteAction, RemoteActionSettingsModel>
    {
        internal override void CreateColumns(bool isSuperuser)
        {
            base.CreateColumns(isSuperuser);

            SettingsModel.Columns["raID"].Visible = false;
            SettingsModel.Columns["raID"].Width = 1;
            SettingsModel.Columns["raID"].SortMode = DataListColumnModel.SortModeEnum.smDescending;
            SettingsModel.Columns["raID"].Order = 0;

            SettingsModel.Columns["runStatus"].Visible = true;
            SettingsModel.Columns["runStatus"].Width = 2;
            SettingsModel.Columns["runStatus"].AllowHtmlFormating = true;
            SettingsModel.Columns["runStatus"].Order = 0;

            SettingsModel.Columns["actionType"].Visible = true;
            SettingsModel.Columns["actionType"].Width = 2;
            SettingsModel.Columns["actionType"].Command = new DataListCommand() { CommandType = DataListCommand.CommandTypeEnum.cmDetail };
            SettingsModel.Columns["actionType"].Order = 1;

            SettingsModel.Columns["actionParams"].Visible = false;
            SettingsModel.Columns["actionParams"].Width = 2;
            SettingsModel.Columns["actionParams"].Order = 2;

            SettingsModel.Columns["sourceUserID"].Visible = true;
            SettingsModel.Columns["sourceUserID"].Width = 2;
            SettingsModel.Columns["sourceUserID"].Order = 3;

            SettingsModel.Columns["sourceDeviceID"].Visible = true;
            SettingsModel.Columns["sourceDeviceID"].Width = 2;
            SettingsModel.Columns["sourceDeviceID"].Order = 4;

            SettingsModel.Columns["actionDate"].Visible = true;
            SettingsModel.Columns["actionDate"].Width = 3;
            SettingsModel.Columns["actionDate"].DataType = DataListColumnModel.DataTypeEnum.tpDateTime;
            SettingsModel.Columns["actionDate"].Order = 5;

            SettingsModel.Columns["received"].Visible = true;
            SettingsModel.Columns["received"].Width = 3;
            SettingsModel.Columns["received"].DataType = DataListColumnModel.DataTypeEnum.tpDateTime;
            SettingsModel.Columns["received"].Order = 6;

            SettingsModel.Columns["completed"].Visible = true;
            SettingsModel.Columns["completed"].Width = 3;
            SettingsModel.Columns["completed"].DataType = DataListColumnModel.DataTypeEnum.tpDateTime;
            SettingsModel.Columns["completed"].Order = 7;

            SettingsModel.Columns["errorDesc"].Visible = true;
            SettingsModel.Columns["errorDesc"].Width = 7;
            SettingsModel.Columns["errorDesc"].Order = 8;

            SettingsModel.Columns["actionParams"].Visible = false;
            SettingsModel.Columns["actionParams"].Width = 3;
            SettingsModel.Columns["actionParams"].Order = 9;

        }
        
     
        
    }
}

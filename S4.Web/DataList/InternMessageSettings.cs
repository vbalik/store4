﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using S4.Web.Models.DataList;
using System.ComponentModel.DataAnnotations;

using S4.Web.Models;

namespace S4.Web.DataList
{
    public class InternMessageSettings : DataListSettings<Entities.InternMessage, InternMessageSettingsModel>
    {
        internal override void CreateColumns(bool isSuperuser)
        {
            base.CreateColumns(isSuperuser);

            SettingsModel.Columns["msgClass"].Visible = true;
            SettingsModel.Columns["msgClass"].Width = 4;
            SettingsModel.Columns["msgClass"].Order = 0;
            SettingsModel.Columns["msgClass"].AllowHtmlFormating = true;

            SettingsModel.Columns["msgSeverity"].Visible = true;
            SettingsModel.Columns["msgSeverity"].Bold = true;
            SettingsModel.Columns["msgSeverity"].Width = 2;
            SettingsModel.Columns["msgSeverity"].Order = 1;
            SettingsModel.Columns["msgSeverity"].AllowHtmlFormating = true;

            SettingsModel.Columns["msgHeader"].Visible = true;
            SettingsModel.Columns["msgHeader"].Width = 11;
            SettingsModel.Columns["msgHeader"].Order = 2;
            SettingsModel.Columns["msgHeader"].Command = new DataListCommand() { CommandType = DataListCommand.CommandTypeEnum.cmDetail };

            SettingsModel.Columns["msgDateTime"].Visible = true;
            SettingsModel.Columns["msgDateTime"].SortMode = DataListColumnModel.SortModeEnum.smDescending;
            SettingsModel.Columns["msgDateTime"].Order = 3;
            SettingsModel.Columns["msgDateTime"].DataType = DataListColumnModel.DataTypeEnum.tpDateTime;

            SettingsModel.Columns["msgUserID"].Visible = true;
            SettingsModel.Columns["msgUserID"].Caption = "Uživatel";
            SettingsModel.Columns["msgUserID"].Width = 3;
            SettingsModel.Columns["msgUserID"].Order = 4;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using S4.Web.Models.DataList;
using System.ComponentModel.DataAnnotations;

using S4.Web.Models;

namespace S4.Web.DataList
{
    public class BlockationSettings : DataListSettings<Entities.Views.BlockationView, BlockationSettingsModel>
    {
        internal override void CreateColumns(bool isSuperuser)
        {
            base.CreateColumns(isSuperuser);

            int orderCnt = -1;

            SettingsModel.Columns["doc"]
                .SetVisible()
                .SetOrder(orderCnt++)
                .SetWidth(4);

            SettingsModel.Columns["dir"]
                .SetVisible()
                .SetOrder(orderCnt++)
                .SetWidth(4);

            SettingsModel.Columns["docStatus"]
                .SetVisible()
                .SetOrder(orderCnt++)
                .SetWidth(4)
                .SetAllowHtmlFormating();

            if (SettingsModel.ArticleID == 0)
            {
                SettingsModel.Columns["articleCode"]
                    .SetVisible()
                    .SetOrder(orderCnt++)
                    .SetWidth(2)
                    .SetCommand(new DataListCommand()
                    {
                        CommandType = DataListCommand.CommandTypeEnum.cmCustom,
                        CommandId = "article_detail",
                        Title = "Zobrazit detail karty"
                    });

                SettingsModel.Columns["articleDesc"]
                    .SetVisible()
                    .SetOrder(orderCnt++);
            }

            if (SettingsModel.PositionID == 0)
            {
                SettingsModel.Columns["posCodeFrom"]
                    .SetVisible()
                    .SetOrder(orderCnt++)
                    .SetWidth(2)
                    .SetCommand(new DataListCommand()
                    {
                        CommandType = DataListCommand.CommandTypeEnum.cmCustom,
                        CommandId = "position_detail_from",
                        Title = "Zobrazit detail karty"
                    });

                SettingsModel.Columns["posCodeTo"]
                    .SetVisible()
                    .SetOrder(orderCnt++)
                    .SetWidth(2)
                    .SetCommand(new DataListCommand()
                    {
                        CommandType = DataListCommand.CommandTypeEnum.cmCustom,
                        CommandId = "position_detail_to",
                        Title = "Zobrazit detail karty"
                    });
            }

            SettingsModel.Columns["quantity"]
                .SetVisible()
                .SetOrder(orderCnt++)
                .SetWidth(2);

            SettingsModel.Columns["batchNum"]
                .SetVisible()
                .SetOrder(orderCnt++)
                .SetWidth(2);

            SettingsModel.Columns["expirationDesc"]
                .SetVisible()
                .SetOrder(orderCnt++)
                .SetWidth(2);

            SettingsModel.Columns["entryUserID"]
                .SetVisible()
                .SetOrder(orderCnt++)
                .SetWidth(2);
        }
    }
}

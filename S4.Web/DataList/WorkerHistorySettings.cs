﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

using S4.Web.Models.DataList;
using S4.Web.Models;

namespace S4.Web.DataList
{
    public class WorkerHistorySettings : DataListSettings<Entities.WorkerHistory, WorkerHistorySettingsModel>
    {
        internal override void CreateColumns(bool isSuperuser)
        {
            base.CreateColumns(isSuperuser);

            SettingsModel.Columns["eventCode"].Visible = true;
            SettingsModel.Columns["eventCode"].Width = 4;

            SettingsModel.Columns["eventData"].Visible = true;
            SettingsModel.Columns["eventData"].DisallowFiltering = true;
            SettingsModel.Columns["eventData"].DisallowSorting = true;

            SettingsModel.Columns["entryDateTime"].Visible = true;
            SettingsModel.Columns["entryDateTime"].DataType = DataListColumnModel.DataTypeEnum.tpDateTime;
            SettingsModel.Columns["entryDateTime"].SortMode = DataListColumnModel.SortModeEnum.smDescending;
            SettingsModel.Columns["entryDateTime"].Width = 4;

            SettingsModel.Columns["entryUserID"].Visible = true;
            SettingsModel.Columns["entryUserID"].Width = 2;
        }
    }
}

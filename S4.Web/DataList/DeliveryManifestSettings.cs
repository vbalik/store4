﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using S4.Web.Models.DataList;
using System.Drawing;

using S4.Web.Models;

namespace S4.Web.DataList
{
    public class DeliveryManifestSettings : DataListSettings<Entities.Views.DeliveryManifestView, DeliveryManifestSettingsModel>
    {
        internal override void CreateColumns(bool isSuperuser)
        {
            base.CreateColumns(isSuperuser);

            SettingsModel.Columns["deliveryProvider"]
                .SetVisible()
                .SetOrder(0);

            SettingsModel.Columns["manifestStatus"]
                .SetVisible()
                .SetOrder(1)
                .SetAllowHtmlFormating();

            SettingsModel.Columns["manifestRefNumber"]
                .SetVisible()
                .SetOrder(2);

            SettingsModel.Columns["docs"]
                .SetVisible()
                .SetOrder(3);

            SettingsModel.Columns["parcels"]
                .SetVisible()
                .SetOrder(4);

            SettingsModel.Columns["manifestCreated"]
                .SetVisible()
                .SetSortMode(DataListColumnModel.SortModeEnum.smDescending)
                .SetWidth(4)
                .SetDataType(DataListColumnModel.DataTypeEnum.tpDateTime)
                .SetOrder(5);

            SettingsModel.Columns["manifestChanged"]
                .SetVisible()
                .SetWidth(4)
                .SetDataType(DataListColumnModel.DataTypeEnum.tpDateTime)
                .SetOrder(6);
        }
    }
}

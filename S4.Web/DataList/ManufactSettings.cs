﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using S4.Web.Models.DataList;
using System.Drawing;

using S4.Web.Models;

namespace S4.Web.DataList
{
    public class ManufactSettings : DataListSettings<Entities.Manufact, ManufactSettingsModel>
    {
        internal override void CreateColumns(bool isSuperuser)
        {
            base.CreateColumns(isSuperuser);

            SettingsModel.Columns["manufID"]
                .SetVisible()
                .SetWidth(4)
                .SetOrder(0)
                .SetBold();

            SettingsModel.Columns["manufName"]
                .SetVisible()
                .SetOrder(1)
                .SetSortMode(DataListColumnModel.SortModeEnum.smAscending);

            SettingsModel.Columns["manufStatus"]
                .SetVisible()
                .SetWidth(2)
                .SetOrder(2)
                .SetAllowHtmlFormating();

            SettingsModel.Columns["source_id"]
                .SetVisible()
                .SetOrder(3)
                .SetWidth(4);

            SettingsModel.Columns["useBatch"]
                .SetVisible()
                .SetWidth(2)
                .SetOrder(4);

            SettingsModel.Columns["useExpiration"]
                .SetVisible()
                .SetWidth(2)
                .SetOrder(5);

            SettingsModel.Columns["sectID"]
                .SetVisible()
                .SetWidth(2)
                .SetOrder(6);

            SettingsModel.Columns["manufSign"]
                .SetVisible()
                .SetWidth(2)
                .SetOrder(7);
        }
    }
}

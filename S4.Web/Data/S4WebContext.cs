﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using S4.Entities;

namespace S4.Web.Models
{
    public class S4WebContext : DbContext
    {
        public S4WebContext (DbContextOptions<S4WebContext> options)
            : base(options)
        {
        }

        public DbSet<S4.Entities.Partner> Partner { get; set; }
    }
}

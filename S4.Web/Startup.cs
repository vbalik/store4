﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using SoapCore;
using System.ServiceModel;
using S4.Picking;
using S4.Core.Cache;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using S4.Web.ProcQueue;
using System.Reflection;
using Microsoft.AspNetCore.Mvc.Formatters;
using S4.Web.Scheduling;
using S4.Core;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.DataProtection;
using S4.Messages;
using S4.Web.Hubs.Direction;
using S4.Web.Hubs.Terminal;
using S4.Web.Hubs;
using S4.Web.Hubs.Progress;
using S4.Web.Hubs.Exchange;
using S4.DeliveryServices;

#if INCLUDE_COMBIT
using combit.ListLabel24.Web;
using combit.ListLabel24.Web.WebDesigner.Server;
#endif


namespace S4.Web
{
    public partial class Startup
    {
        public const string API_PATH_START = "/api";
        public const string EXTERNAL_API_PATH_START = "/api/external";
        public const string SOAP_PATH_START = "/WSTransactions.asmx";

        public const int LOGIN_DURATION_HOURS = 12;


        public IConfiguration Configuration { get; }
        private ILogger<Startup> _logger;


        public static IServiceProvider ServiceProvider { get; private set; }
        public static T GetService<T>() { return ServiceProvider.GetRequiredService<T>(); }


        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

            // set connection string for procedures
            Core.Data.ConnectionHelper.ConnectionString = configuration.GetValue<string>("S4_LOCALDB_CN");

        }


        public void ConfigureServices(IServiceCollection services)
        {
            // application services
            services.AddSingleton<IConfiguration>(Configuration);
            services.AddSignalR();
            services.AddDataProtection()
                .SetApplicationName("S4.Web")
                .PersistKeysToFileSystem(new System.IO.DirectoryInfo(System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location)))
                .SetDefaultKeyLifetime(TimeSpan.FromDays(365));

            // security services
            services.AddTransient<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTransient<AccessControl.ISecProvider, AccessControl.SecProvider>();
            services.AddScoped<AccessControl.IAuditor, AccessControl.Auditor>();

            // db services
            services.AddScoped<DAL.ArticleDAL>();
            services.AddScoped<DAL.PositionDAL>();
            services.AddScoped<DAL.WorkerDAL>();
            services.AddScoped<DAL.DirectionDAL>();
            services.AddScoped<DAL.DirectionAssignDAL>();
            services.AddScoped<DAL.InternMessageDAL>();
            services.AddScoped<DAL.RemoteActionDAL>();
            services.AddScoped<DAL.DAL<Entities.Truck, int>>();
            services.AddSingleton<ICache<Entities.ArticlePacking, int>, DAL.Cache.ArticlePackingCache>();
            services.AddSingleton<ICache<Entities.Models.ArticleModel, int>, DAL.Cache.ArticleModelCache>();
            services.AddSingleton<ICache<Entities.Manufact, string>, DAL.Cache.ManufactCache>();
            services.AddSingleton<ICache<Entities.Models.SectionModel, string>, DAL.Cache.SectionModelCache>();
            services.AddSingleton<ICache<Entities.PartnerAddress, int>, DAL.Cache.PartnerAddressCache>();
            services.AddSingleton<ICache<Entities.Partner, int>, DAL.Cache.PartnerCache>();
            services.AddSingleton<ICache<Entities.Position, int>, DAL.Cache.PositionCache>();
            services.AddSingleton<ICache<Entities.Position, string>, DAL.Cache.PositionCache_ByCode>();
            services.AddSingleton<DAL.Cache.PrefixCache>();
            services.AddSingleton<DAL.Cache.PositionByCategoryModelCache>();
            services.AddScoped<DAL.Interfaces.IBookings, DAL.BookingDAL>();

            // business logic services
            services.AddScoped<StoreCore.Interfaces.IOnStore, StoreCore.OnStore_Basic.OnStore>();
            services.AddScoped<StoreCore.Interfaces.ISave, StoreCore.Save_Basic.Save>();
            services.AddScoped<IPlanMaker, PlanMaker>();

            // background queue
            services.AddHostedService<QueuedHostedService>();
            services.AddSingleton<Core.Interfaces.IBackgroundTaskQueue, BackgroundTaskQueue>();

            // scheduled tasks & scheduler
            services.AddSingleton<IScheduledTask, Tasks.FlushProcedureLogTask>();
            services.AddSingleton<IScheduledTask, Tasks.SendJobAssingUpdate>();
            services.AddScheduler((sender, args) =>
            {
                _logger.LogError(args.Exception, "Task error");
                args.SetObserved();
            }, 5); // every 5 seconds check for tasks to run

            // messages service
            services.AddSingleton<IMessageRouter, MessageRouter>();
            services.AddTransient<DirectionSubscriber>();
            services.AddTransient<ProgressSubscriber>();
            services.AddTransient<ExchangeSubscriber>();
            // workaround from https://stackoverflow.com/a/59089881
            services.AddSingleton<TerminalSubscriberHostedService>();
            services.AddSingleton<Microsoft.Extensions.Hosting.IHostedService>(p => p.GetService<TerminalSubscriberHostedService>());

            // SOAP interface
            services.AddTransient<SOAP.WSTransactions>();
            services.AddSoapCore();
            services.AddSoapExceptionTransformer((ex) =>
            {
                // Build the intermediate service provider
                var sp = services.BuildServiceProvider();
                var logService = sp.GetService<ILogger<SoapExceptionSaver>>();
                var loghttp = sp.GetService<IHttpContextAccessor>();

                // save exception to db
                SoapExceptionSaver.SaveException(logService, ex, loghttp);


                return ex.Message;
            });

            //delivery
            services.AddSingleton<IDeliveryCommon, DeliveryService>();

            // mvc
#if INCLUDE_COMBIT
            var assembly = typeof(Html5ViewerController).GetTypeInfo().Assembly;
#endif
            services
                .AddMvc(options =>
                {
                    // basic logon/logoff filter
                    var policy = new AuthorizationPolicyBuilder()
                        .RequireAuthenticatedUser()
                        .Build();
                    options.Filters.Add(new AuthorizeFilter(policy));

                    // access rights filter
                    options.Filters.Add<MvcExceptionFilter>();
                    options.Filters.Add<AccessControl.MvcPageFilter>();
                    options.OutputFormatters.RemoveType<HttpNoContentOutputFormatter>();
                })
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
                    options.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
                })
#if INCLUDE_COMBIT
                .AddWebApiConventions().AddApplicationPart(assembly); //NuGet Microsoft.AspNetCore.Mvc.WebApiCompatShim;
#else
            ;
#endif

            // access control
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(CookieAuthenticationDefaults.AuthenticationScheme,
                    options =>
                    {
                        options.Cookie.Expiration = TimeSpan.FromHours(4);
                        options.SlidingExpiration = true;
                        options.LoginPath = new PathString("/auth/login");
                        options.AccessDeniedPath = new PathString("/auth/denied");
                    });
        }


        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILogger<Startup> logger, IMessageRouter messageRouter)
        {
            ServiceProvider = app.ApplicationServices;

            // store logger - for SchedulerHostedService exception logging
            _logger = logger;

            // set service provider in procedures
            S4.Procedures.ServicesFactory.ServiceProvider = app.ApplicationServices;

            // register message subscribers
            RegisterMessageSubscribers(messageRouter, app, env);


            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseStatusCodePagesWithRedirects("/home/error?code={0}");
                app.UseExceptionHandler("/home/internalerror");
            }

            app.UseStaticFiles();

            app.UseSignalR(routes =>
            {
                routes.MapHub<DirectionHub>(DirectionHub.CLIENT_HUB_URL);
                routes.MapHub<TerminalHub>(TerminalHub.CLIENT_HUB_URL);
                routes.MapHub<PosiflexTabletHub>(PosiflexTabletHub.CLIENT_HUB_URL);
                routes.MapHub<HoneywellHub>(HoneywellHub.CLIENT_HUB_URL);
                routes.MapHub<ProgressHub>(ProgressHub.CLIENT_HUB_URL);
                routes.MapHub<ExchangeHub>(ExchangeHub.CLIENT_HUB_URL);
            });

            // access control
            app.UseAuthentication();
            // API
            bool isApiRequest(HttpContext context) => 
                context.Request.Path.StartsWithSegments(API_PATH_START, StringComparison.OrdinalIgnoreCase)
                && !context.Request.Path.StartsWithSegments(EXTERNAL_API_PATH_START, StringComparison.OrdinalIgnoreCase);
            app.UseWhen(context => isApiRequest(context), app2 =>
            {
                app2.UseMiddleware<AccessControl.ApiAuthenticationMiddleware>();
            });

            // SOAP interface
            app.UseWhen(x => (x.Request.Path.StartsWithSegments(SOAP_PATH_START, StringComparison.OrdinalIgnoreCase)), app2 =>
            {
                app2.UseMiddleware<SOAP.SOAPRequestLoggingMiddleware>();
            });
            app.UseSoapEndpoint<SOAP.WSTransactions>(path: SOAP_PATH_START, binding: new BasicHttpBinding(), serializer: SoapSerializer.DataContractSerializer, caseInsensitivePath: true);

            // routes
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

#if INCLUDE_COMBIT
                routes.MapRoute("Report", "{controller=report}/{action=Index}/{id?}");
                Html5ViewerConfig.RegisterRoutes(routes);
                WebDesignerConfig.RegisterRoutes(routes);
#endif
            });


        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class StockTakingSettingsModel : DataList.DataListSettingsModel<Entities.Views.StockTakingView>
    {
        public StockTakingSettingsModel() : base()
        {
            PageSize = 20;
            DisplayName = "Inventury";
            StatusField = "stotakStatus";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class RemoteActionSettingsModel : DataList.DataListSettingsModel<Entities.RemoteAction>
    {
        public RemoteActionSettingsModel() : base()
        {
            PageSize = 20;
            DisplayName = "Vzdálené akce";
            DataController = "api/remoteaction";
            StatusField = "runStatus";
        }

        public int RaID { get; set; }

      
    }
}

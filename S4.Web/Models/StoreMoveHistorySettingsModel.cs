﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class StoreMoveHistorySettingsModel : DataList.DataListSettingsModel<Entities.StoreMoveHistory>
    {
        public StoreMoveHistorySettingsModel() : base()
        {
            PageSize = 15;
            DisplayName = "Historie";
            StatusField = "eventCode";
            DataController = "api/storemovehistory";
        }

        public int StoMoveID { get; set; }
    }
}

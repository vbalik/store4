﻿using OfficeOpenXml.FormulaParsing.Excel.Functions.Text;
using S4.Entities;
using S4.Entities.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static S4.Entities.Booking;

namespace S4.Web.Models
{
    public class ArticleBookingSettingsModel : DataList.DataListSettingsModel<BookingView>
    {
        public ArticleBookingSettingsModel() : base()
        {
            PageSize = 20;
            DisplayName = "Rezervace";
            StatusField = "bookingStatus";
        }
        
        public int ArticleID { get; set; }
        public bool BookingValid { get; set; }

        
                
    }
}

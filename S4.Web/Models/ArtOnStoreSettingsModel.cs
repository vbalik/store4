﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class ArtOnStoreSettingsModel : DataList.DataListSettingsModel<Entities.Views.ArtOnStoreView>
    {
        public ArtOnStoreSettingsModel() : base()
        {
            PageSize = 10;
            DisplayName = "Karty skladem";
            StatusField = "articleStatus";
            DataController = "api/onstore";
        }

        public int ID { get; set; }
        public enum ArtOnStoreStyleEnum { ByArticle, ByPosition }
        public ArtOnStoreStyleEnum ArtOnStoreStyle { get; set; }
        public bool StorePosOnly { get; set; }
    }
}

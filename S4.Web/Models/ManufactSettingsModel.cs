﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class ManufactSettingsModel : DataList.DataListSettingsModel<Entities.Manufact>
    {
        public ManufactSettingsModel() : base()
        {
            PageSize = 15;
            DisplayName = "Výrobce";
            StatusField = "manufStatus";
        }
    }
}

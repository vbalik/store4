﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class StoreMoveSettingsModel : DataList.DataListSettingsModel<Entities.Views.StoreMoveView>
    {
        public StoreMoveSettingsModel() : base()
        {
            RowSelection = true;
            PageSize = 20;
            DisplayName = "Pohyby";
            StatusField = "docStatus";
            DataController = "api/storemove";
        }

        public enum StoreMoveFilterEnum { Receive, StoreIn, Move, Dispatch, Expedition, Preparation, Stotak }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class MessageBusItemModel
    {
        public DateTime DateTime { get; set; }
        public int MessageNew { get; set; }
        public int MessageDone { get; set; }
        public int MessageWait { get; set; }
        public int MessageWaitComplete { get; set; }
        public int MessageError { get; set; }
        public int MessageNoComplete { get; set; }
    }
}

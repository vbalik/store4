﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class InvoiceDirectionSettingsModel : DataList.DataListSettingsModel<Entities.Direction>
    {
        public InvoiceDirectionSettingsModel() : base()
        {
            RowSelection = true;
            StatusField = "docStatus";
            PageSize = 15;
            DataController = "api/invoicedirection";
        }
        

        public int InvoiceID { get; set; }
    }
}

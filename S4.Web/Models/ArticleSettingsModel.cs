﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class ArticleSettingsModel : DataList.DataListSettingsModel<Entities.Views.ArticleView>
    {
        public ArticleSettingsModel() : base()
        {
            PageSize = 20;
            DisplayName = "Karty";
            StatusField = "articleStatus";
        }

        public string ManufID { get; set; }
        public enum ArticleFilterEnum { Used, UsedNoActiva, OnStore, All }
    }
}

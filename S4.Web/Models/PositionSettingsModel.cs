﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class PositionSettingsModel : DataList.DataListSettingsModel<Entities.Views.PositionView>
    {
        public PositionSettingsModel() : base()
        {
            RowSelection = true;
            PageSize = 15;
            DisplayName = "Pozice";
            StatusField = "posStatus";
        }

        public string SectID { get; set; }
        public enum PositionFilterEnum { Store, Receive, Dispatch, Blocked, Empty, NotEmpty, All }
    }
}

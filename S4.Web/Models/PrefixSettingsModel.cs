﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class PrefixSettingsModel : DataList.DataListSettingsModel<Entities.Views.PrefixView>
    {
        public PrefixSettingsModel() : base()
        {
            PageSize = 20;
            DisplayName = "Prefixy";
        }
                
        public string DocClass { get; set; }
        public string DocNumPrefix { get; set; }
    }
}

using System;
using System.Collections.Generic;
using S4.Entities;

namespace S4.Web.Models
{
    public class DirectionManualModel
    {
        public int DirectionID { get; set; }
        public string DocInfo { get; set; }
        public string PartnerName { get; set; }
        public List<DirectionManualItemModel> Items { get; set; }
        public Position Position { get; set; }
        public bool BadStatus { get; set; }
    }

    public class DirectionManualItemModel
    {
        public int? DirectionItemID { get; set; }
        public int DocPosition { get; set; }
        public int DirectionID { get; set; }
        public int ArticleID { get; set; }
        public int ArtPackID { get; set; }
        public string ArticleCode { get; set; }
        public string ArticleDesc { get; set; }
        public string PackDesc { get; set; }
        public decimal Quantity { get; set; }
        public bool UseBatch { get; set; }
        public string BatchNum { get; set; }
        public bool UseExpiration { get; set; }
        public DateTime? ExpirationDate { get; set; }
    }
}
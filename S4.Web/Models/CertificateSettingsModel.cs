﻿using OfficeOpenXml.FormulaParsing.Excel.Functions.Text;
using S4.Entities.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class CertificateSettingsModel : DataList.DataListSettingsModel<Entities.Certificate>
    {
        public CertificateSettingsModel() : base()
        {
            PageSize = 20;
            DisplayName = "Certifikáty";
        }

        public int CertificateID { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class StoreMoveItemSettingsModel : DataList.DataListSettingsModel<Entities.Views.StoreMoveItemView>
    {
        public StoreMoveItemSettingsModel() : base()
        {
            PageSize = 0;
            DisplayName = "Pohyby";
            StatusField = "docStatus";
            DataController = "api/storemoveitem";
            RowSelection = false;
        }

        public int ArticleID { get; set; }
        public int PositionID { get; set; }
        public int DirectionID { get; set; }
        public int StoMoveID { get; set; }
        public bool ValidOnly { get; set; }
    }
}
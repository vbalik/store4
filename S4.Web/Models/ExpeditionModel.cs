﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class ExpeditionModel
    {
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public string TruckRegion { get; set; }

        public Entities.Truck.TruckRegionEnum TruckRegionEnum {
            get
            {
                if (string.IsNullOrWhiteSpace(TruckRegion) || TruckRegion.Equals("Vše"))
                    return Entities.Truck.TruckRegionEnum.None;
                else if (TruckRegion.Equals("Čechy"))
                    return Entities.Truck.TruckRegionEnum.Czech;
                else
                    return Entities.Truck.TruckRegionEnum.Moravia;
            }
        }
    }
}

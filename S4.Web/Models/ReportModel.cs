﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

#if INCLUDE_COMBIT
using combit.ListLabel24.Web;
#endif

using Microsoft.AspNetCore.Mvc.Rendering;
using S4.Report.Helper;
using static S4.Entities.Settings.SQL4Filters;

namespace S4.Web.Models
{
    public class FiltersReport
    {
        public string Name { get; set; }
        public TypeFilterEnum Type { get; set; }
        public List<FilterValuesReport> FilterReportList { get; set; }
        public string Width { get; set; }
        public bool Mandatory { get; set; }
        public string ID { get; set; }
    }
    
    public class ReportModel
    {
#if INCLUDE_COMBIT

        public ReportModel()
        {
            ViewerOptions = new Html5ViewerOptions();
        }
        public Html5ViewerOptions ViewerOptions { get; set; }
#endif

        public string ReportName { get; set; }
        public List<FiltersReport> FiltersReportList { get; set; } = null;
        public List<string> FiltersReportTextList { get; set; } = null;
        public List<FilterValuesReport> FilterIDs { get; set; }
        public int ReportID { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class StockTakingPositionSettingsModel : DataList.DataListSettingsModel<Entities.Views.StockTakingPositionView>
    {
        public StockTakingPositionSettingsModel() : base()
        {
            PageSize = 15;
            DataController = "api/stocktakingposition";
        }

        public enum PositionsViewRangeEnum { All, Checked, NotChecked}
        public PositionsViewRangeEnum PositionsViewRange { get; set; }
        public int StotakID { get; set; }
    }
}
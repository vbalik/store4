﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class PositionsPrintModel
    {
        public int[] PositionIDs { get; set; }
        public int PrinterLocationID { get; set; }

        public string FileID { get; set; }
        public bool Success { get; set; } = false;
        public string ErrorText { get; set; }
        public bool LocalPrint { get; set; }
        public byte[] FileData { get; set; }
        

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static S4.Export.Export;

namespace S4.Web.Models
{
    public class ExportDirectionModel
    {
        public int DirectionID { get; set; }
        public ExportFormat ExportFormat { get; set; }
        public string MailAdresaKomu { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class DirectionSettingsModel : DataList.DataListSettingsModel<Entities.Views.DirectionView>
    {
        public DirectionSettingsModel() : base()
        {
            RowSelection = true;
            StatusField = "docStatus";
            PageSize = 15;
            DataController = "api/direction";
        }

        public enum DirectionEnum { In, Out }
        public enum DirectionFilterEnum { NotProcessed, Receive, StoreIn, Loaded, Dispatch, Check, All }

        private DirectionEnum _dir;
        public DirectionEnum Dir
        {
            get { return _dir; }
            set
            {
                _dir = value;
                DisplayName = _dir == DirectionEnum.In ? "Příjem" : "Vyskladnění";
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class InvoiceHistorySettingsModel : DataList.DataListSettingsModel<Entities.InvoiceHistory>
    {
        public InvoiceHistorySettingsModel() : base()
        {
            PageSize = 10;
            DisplayName = "Historie";
            StatusField = "eventCode";
            DataController = "api/invoicehistory";
        }

        public int InvoiceID { get; set; }
    }
}

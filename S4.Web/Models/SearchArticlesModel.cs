﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class SearchArticlesModel
    {
        public string Part { get; set; }
        public bool DisabledIncl { get; set; }
        public bool NotMovableInc { get; set; }
    }
}

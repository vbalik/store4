﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class NewsModel
    {
        public bool ShowNews { get; set; }
        public string MD5 { get; set; }
        public string AppVersion { get; set; }
        public List<Message> MessageList { get; set; }
    }

    public class Message
    {
        public string Header { get; set; }
        public List<string> TextList { get; set; }
    }
}

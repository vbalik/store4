﻿using OfficeOpenXml.FormulaParsing.Excel.Functions.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class DirectionXtraSettingsModel : DataList.DataListSettingsModel<Entities.DirectionXtra>
    {
        public DirectionXtraSettingsModel() : base()
        {
            PageSize = 20;
            DisplayName = "Doklady Extra Data";
            ExportToExcelDisallowed = true;
        }

        public int DirectionID { get; set; }
    }
}

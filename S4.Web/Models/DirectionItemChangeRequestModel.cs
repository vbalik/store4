﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class DirectionItemChangeRequestModel
    {
        public int DirectionID { get; set; }
        public int DirectionItemID { get; set; }
        public int ArtPackID { get; set; }
        public int DocPos { get; set; }
    }
}

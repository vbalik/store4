using System;

namespace S4.Web.Models
{
    public class ErrorViewModel
    {
        public string ErrorDesc { get; set; }

        public string RequestId { get; set; }

        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }
}
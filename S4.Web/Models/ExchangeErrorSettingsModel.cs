﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class ExchangeErrorSettingsModel : DataList.DataListSettingsModel<Entities.ExchangeError>
    {
        public ExchangeErrorSettingsModel() : base()
        {
            PageSize = 20;
            DisplayName = "Chyby exchange";
        }

        public int ExchangeErrorID { get; set; }
    }
}

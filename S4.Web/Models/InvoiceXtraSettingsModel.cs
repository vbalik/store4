﻿using OfficeOpenXml.FormulaParsing.Excel.Functions.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class InvoiceXtraSettingsModel : DataList.DataListSettingsModel<Entities.InvoiceXtra>
    {
        public InvoiceXtraSettingsModel() : base()
        {
            PageSize = 20;
            DisplayName = "Faktury Extra Data";
            ExportToExcelDisallowed = true;
        }

        public int InvoiceID { get; set; }
    }
}

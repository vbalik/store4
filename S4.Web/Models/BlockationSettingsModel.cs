﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class BlockationSettingsModel : DataList.DataListSettingsModel<Entities.Views.BlockationView>
    {
        public BlockationSettingsModel() : base()
        {
            PageSize = 10;
            DisplayName = "Blokace";
            StatusField = "docStatus";
            ServerSide = false;
            DataController = "api/blockation";
        }

        public int ArticleID { get; set; }
        public int PositionID { get; set; }
    }
}
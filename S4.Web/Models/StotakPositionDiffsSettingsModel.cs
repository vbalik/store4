﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class StotakPositionDiffsSettingsModel : DataList.DataListSettingsModel<Entities.Models.StotakPositionDifferences>
    {
        public StotakPositionDiffsSettingsModel() : base()
        {
            PageSize = 15;
            DataController = "api/stotakpositiondiffs";
        }

        public int StotakID { get; set; }
        public byte SnapshotClass { get; set; }

        /// <summary>
        /// Add no-different quantity items to result
        /// </summary>
        public bool IncludeNoDiff { get; set; }
    }
}
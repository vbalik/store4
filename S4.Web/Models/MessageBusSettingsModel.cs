﻿using OfficeOpenXml.FormulaParsing.Excel.Functions.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class MessageBusSettingsModel : DataList.DataListSettingsModel<Entities.Views.MessageBusView>
    {
        public MessageBusSettingsModel() : base()
        {
            PageSize = 20;
            DisplayName = "Zprávy aplikace";
            StatusField = "messageStatus";
        }

        public int MessageBusID { get; set; }
    }
}

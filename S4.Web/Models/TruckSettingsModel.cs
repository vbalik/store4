﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class TruckSettingsModel : DataList.DataListSettingsModel<Entities.Truck>
    {
        public TruckSettingsModel() : base()
        {
            PageSize = 15;
            DisplayName = "Auta";
        }
    }
}

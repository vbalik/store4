using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace S4.Web.Models
{
    public class ReceiveImportCsvModel
    {
        public IEnumerable<ReceiveImportCsvItemModel> Data { get; set; } = new List<ReceiveImportCsvItemModel>();
        public IFormFile File { get; set; }
    }
}
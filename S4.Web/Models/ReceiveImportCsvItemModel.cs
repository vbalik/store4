using CsvHelper.Configuration.Attributes;

namespace S4.Web.Models
{
    public class ReceiveImportCsvItemModel
    {
        private static readonly char[] FILTER_CHARS = {' ', '"'};

        private string _packingSlipId;

        /// <summary>
        /// Číslo DLV
        /// </summary>
        public string PackingSlipId
        {
            get => _packingSlipId;
            set => _packingSlipId = value.Trim(FILTER_CHARS);
        }

        private string _customerRef;
        /// <summary>
        /// Číslo objednávky CZZ
        /// </summary>
        public string CustomerRef
        {
            get => _customerRef;
            set => _customerRef = value.Trim(FILTER_CHARS);
        }

        private string _salesId;

        /// <summary>
        /// Číslo objednávky Büroprofi
        /// </summary>
        public string SalesId
        {
            get => _salesId;
            set => _salesId = value.Trim(FILTER_CHARS);
        }

        private string _itemId;

        /// <summary>
        /// Číslo položky BP
        /// </summary>
        public string ItemId
        {
            get => _itemId;
            set => _itemId = value.Trim(FILTER_CHARS);
        }

        private string _extItemId;

        /// <summary>
        /// Externí číslo položky
        /// </summary>
        public string ExtItemId
        {
            get => _extItemId;
            set => _extItemId = value.Trim(FILTER_CHARS);
        }

        private string _lineText;

        /// <summary>
        /// Text položky
        /// </summary>
        public string LineText
        {
            get => _lineText;
            set => _lineText = value.Trim(FILTER_CHARS);
        }

        private string _barcode;

        /// <summary>
        /// EAN položky
        /// </summary>
        public string Barcode
        {
            get => _barcode;
            set => _barcode = value.Trim(FILTER_CHARS);
        }

        private string _qty;

        /// <summary>
        /// Vyskladněné zboží
        /// </summary>
        public string Qty
        {
            get => _qty;
            set => _qty = value.Trim(FILTER_CHARS);
        }

        private string _remain;

        /// <summary>
        /// Zbývá dodat (pokud by se vyskladnil menší počet než byl objednán)
        /// </summary>
        public string Remain
        {
            get => _remain;
            set => _remain = value.Trim(FILTER_CHARS);
        }

        private string _unitId;

        /// <summary>
        /// Jednotka položky
        /// </summary>
        public string UnitId
        {
            get => _unitId;
            set => _unitId = value.Trim(FILTER_CHARS);
        }

        /// <summary>
        /// Item error message
        /// </summary>
        [Ignore]
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Is item valid
        /// </summary>
        [Ignore]
        public bool IsItemValid => string.IsNullOrEmpty(ErrorMessage);
        
        [Ignore]
        public int ArtPackID { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class DirectionsForManifestSettingsModel : DataList.DataListSettingsModel<Entities.Models.DirectionsForManifestModel>
    {
        public DirectionsForManifestSettingsModel() : base()
        {
            PageSize = 15;
            DisplayName = "Manifest pro spediční služby";
        }

        public Entities.DeliveryDirection.DeliveryProviderEnum DeliveryProvider { get; set; }
    }
}

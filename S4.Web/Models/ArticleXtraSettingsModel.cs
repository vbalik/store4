﻿using OfficeOpenXml.FormulaParsing.Excel.Functions.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class ArticleXtraSettingsModel : DataList.DataListSettingsModel<Entities.ArticleXtra>
    {
        public ArticleXtraSettingsModel() : base()
        {
            PageSize = 20;
            DisplayName = "Karty Extra Data";
            ExportToExcelDisallowed = true;
        }

        public int ArticleID { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class SearchPositionsModel
    {
        public string Part { get; set; }
        public bool LimitToCategory { get; set; }
        public Entities.Position.PositionCategoryEnum PositionCategory { get; set; }
    }
}

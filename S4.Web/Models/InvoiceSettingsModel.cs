﻿using OfficeOpenXml.FormulaParsing.Excel.Functions.Text;
using S4.Entities.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class InvoiceSettingsModel : DataList.DataListSettingsModel<InvoiceView>
    {
        public InvoiceSettingsModel() : base()
        {
            PageSize = 20;
            DisplayName = "Faktury";
        }

        public int InvoiceID { get; set; }
    }
}

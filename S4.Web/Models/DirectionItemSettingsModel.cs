﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class DirectionItemSettingsModel : DataList.DataListSettingsModel<Entities.Views.DirectionItemView>
    {
        public DirectionItemSettingsModel() : base()
        {
            PageSize = 10;
            DisplayName = "Položky";
            StatusField = "itemStatus";
            DataController = "api/directionitem";
        }

        public int DirectionID { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class WorkerSettingsModel : DataList.DataListSettingsModel<Entities.Worker>
    {
        public WorkerSettingsModel() : base()
        {
            PageSize = 20;
            DisplayName = "Pracovníci";
            StatusField = "workerStatus";
        }

        public enum WorkerFilterEnum { Active, All }
    }
}

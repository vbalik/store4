﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class ArticleHistorySettingsModel : DataList.DataListSettingsModel<Entities.ArticleHistory>
    {
        public ArticleHistorySettingsModel() : base()
        {
            PageSize = 10;
            DisplayName = "Historie";
            DataController = "api/articlehistory";
        }

        public int ArticleID { get; set; }
    }
}

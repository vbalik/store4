﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class ActionModel
    {
        public string Action { get; set; }
        public object Item { get; set; }
    }
}

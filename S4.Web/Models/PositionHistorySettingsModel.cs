﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class PositionHistorySettingsModel : DataList.DataListSettingsModel<Entities.PositionHistory>
    {
        public PositionHistorySettingsModel() : base()
        {
            PageSize = 10;
            DisplayName = "Historie";
            DataController = "api/positionhistory";
        }

        public int PositionID { get; set; }
    }
}

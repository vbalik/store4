﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class StotakArticleDiffsSettingsModel : DataList.DataListSettingsModel<Entities.Models.StotakArticleDifferences>
    {
        public StotakArticleDiffsSettingsModel() : base()
        {
            PageSize = 15;
            DataController = "api/stotakarticlediffs";
        }

        public int StotakID { get; set; }
        public byte SnapshotClass { get; set; }
    }
}
﻿using S4.Entities.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class ArticlePackingSettingsModel : DataList.DataListSettingsModel<ArticlePackingView>
    {
        public ArticlePackingSettingsModel() : base()
        {
            PageSize = 10;
            DisplayName = "Balení";
            StatusField = "packStatus";
            DataController = "api/articlepacking";
        }

        public int ArticleID { get; set; }
    }
}

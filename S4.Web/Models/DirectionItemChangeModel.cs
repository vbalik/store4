﻿using S4.Entities.Views;
using S4.ProcedureModels.Admin;
using System.Collections.Generic;

namespace S4.Web.Models
{
    public class DirectionItemChangeModel
    {
        public DirectionItemView DirectionItem { get; set; }
        public List<StoreMoveItemView> DirectionItemStoMoves { get; set; } = new List<StoreMoveItemView>();
    }
}

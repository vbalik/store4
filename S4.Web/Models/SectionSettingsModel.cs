﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class SectionSettingsModel : DataList.DataListSettingsModel<Entities.Section>
    {
        public SectionSettingsModel() : base()
        {
            PageSize = 15;
            DisplayName = "Sekce";
        }
    }
}

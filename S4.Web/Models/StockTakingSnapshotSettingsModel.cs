﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class StockTakingSnapshotSettingsModel : DataList.DataListSettingsModel<Entities.Views.StockTakingSnapshotView>
    {
        public StockTakingSnapshotSettingsModel() : base()
        {
            PageSize = 15;
            DataController = "api/stocktakingsnapshot";
        }

        public int StotakID { get; set; }
        public byte SnapshotClass { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class StockTakingItemSettingsModel : DataList.DataListSettingsModel<Entities.Views.StockTakingItemView>
    {
        public StockTakingItemSettingsModel() : base()
        {
            PageSize = 15;
            DataController = "api/stocktakingitem";
        }

        public int StotakID { get; set; }
    }
}

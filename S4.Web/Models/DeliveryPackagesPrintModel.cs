﻿using S4.Entities;
using S4.ProcedureModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace S4.Web.Models
{
    public class DeliveryPackagesPrintModel
    {
        public DeliveryPackagesSettingsModel DeliveryPackagesSettingsModel { get; set; }
        public int PrinterLocationID { get; set; }
        public int ReportID { get; set; }
        public byte[] FileData { get; set; }
        public string FileID { get; set; }
        public bool LocalPrint { get; set; }
        public DateTime? Date { get; set; }
    }
}

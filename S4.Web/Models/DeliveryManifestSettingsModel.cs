﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class DeliveryManifestSettingsModel : DataList.DataListSettingsModel<Entities.Views.DeliveryManifestView>
    {
        public DeliveryManifestSettingsModel() : base()
        {
            PageSize = 15;
            DisplayName = "Manifest pro spediční služby";
            RowSelection = true;
            StatusField = "manifestStatus";
        }
    }
}

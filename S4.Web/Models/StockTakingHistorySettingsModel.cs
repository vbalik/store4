﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class StockTakingHistorySettingsModel : DataList.DataListSettingsModel<Entities.Views.StockTakingHistoryView>
    {
        public StockTakingHistorySettingsModel() : base()
        {
            PageSize = 15;
            DataController = "api/stocktakinghistory";
        }

        public int StotakID { get; set; }
    }
}
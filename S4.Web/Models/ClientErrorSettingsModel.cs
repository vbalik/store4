﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class ClientErrorSettingsModel : DataList.DataListSettingsModel<Entities.ClientError>
    {
        public ClientErrorSettingsModel() : base()
        {
            PageSize = 20;
            DisplayName = "Chyby aplikace";
        }

        public int ClientErrorID { get; set; }
    }
}

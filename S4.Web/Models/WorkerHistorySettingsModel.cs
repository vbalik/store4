﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class WorkerHistorySettingsModel : DataList.DataListSettingsModel<Entities.WorkerHistory>
    {
        public WorkerHistorySettingsModel() : base()
        {
            DisplayName = "Historie";
            DataController = "api/workerhistory";
        }

        public string WorkerID { get; set; }
    }
}

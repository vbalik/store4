﻿using S4.Report.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class ExportXLSModel
    {
        public int ReportID { get; set; }
        public List<FilterValuesReport> Filters { get; set; }
    }
}

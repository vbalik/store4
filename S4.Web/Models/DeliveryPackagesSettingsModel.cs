﻿using S4.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace S4.Web.Models
{
    public class DeliveryPackagesSettingsModel : DataList.DataListSettingsModel<Entities.Views.DeliveryPackagesView>
    {
        [Required]
        public DateTime? Date { get; set; }
        public int? DeliveryProviderFilterSelected { get; set; }
        public List<(int, string)> DeliveryProvidersFilter
        {
            get
            {
                var names = typeof(DeliveryDirection.DeliveryProviderEnum).GetFields(BindingFlags.Static | BindingFlags.Public).Select(fi => fi.Name).ToList();
                List<(int, string)> values = names.Select(name =>
                {
                    var providerEnumValue = Enum.Parse(typeof(DeliveryDirection.DeliveryProviderEnum), name);

                    return ((int)providerEnumValue,
                        providerEnumValue.GetType()?
                            .GetMember(providerEnumValue.ToString())?
                            .First()?
                            .GetCustomAttribute<DisplayAttribute>()?
                            .Name);
                }).ToList();

                // change name of first element
                values[0] = (values[0].Item1, "Vše");

                return values;
            }
        }

        public DeliveryPackagesSettingsModel() : base()
        {
            PageSize = 15;
            DisplayName = "Seznam balíků spediční služby";
            RowSelection = false;
            ShowFilterRow = false;
            ExportToExcelDisallowed = true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models.DataList
{
    public class DataListFilterValue
    {
        public string Value { get; set; }
        public bool ValueInvalid { get; set; }
    }
}

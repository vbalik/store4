﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Drawing;

namespace S4.Web.Models.DataList
{
    public class DataListColumnModel
    {
        public string FieldName { get; set; }
        public string Caption { get; set; }
        public DataListColumnModel SetCaption(string caption)
        {
            Caption = caption;
            return this;
        }
        public string Class { get { return $"column col-lg-1 col-ldl-{Width} col-md-1 col-mdl-{Width} col-sm-1 col-sdl-{Width}"; } }
        public enum SortModeEnum : short
        {
            smNone = 0,
            smAscending = 1,
            smDescending = 2
        }
        public SortModeEnum SortMode { get; set; }
        public DataListColumnModel SetSortMode(SortModeEnum sortMode = SortModeEnum.smAscending)
        {
            SortMode = sortMode;
            return this;
        }
        public short SortOrder { get; set; }
        public enum DataTypeEnum : short
        {
            tpUnknown = 0,
            tpString = 1,
            tpNumber = 2,
            tpDate = 3,
            tpDateTime = 4,
            tpList = 5,
            tpBool = 6,
            tpDecimal = 7,
            tpFlag = 8,
            tpSmallDate = 9,
            tpSelection = 255
        }
        public DataTypeEnum DataType { get; set; }
        public DataListColumnModel SetDataType(DataTypeEnum dataType)
        {
            DataType = dataType;
            return this;
        }
        private bool _visible;
        public bool Visible
        {
            get { return _visible; }
            set
            {
                if (value == _visible)
                    return;
                _visible = value;

                WidthRecalcRequest?.Invoke(this, new EventArgs());
            }
        }
        public DataListColumnModel SetVisible(bool visible = true)
        {
            Visible = visible;
            return this;
        }
        internal int _width;
        public event EventHandler WidthRecalcRequest;
        public int Width
        {
            get { return _width; }
            set
            {
                if (value == _width)
                    return;
                _width = value;
                fixedWidth = value > 0;

                WidthRecalcRequest?.Invoke(this, new EventArgs());
            }
        }
        public DataListColumnModel SetWidth(int width)
        {
            Width = width;
            return this;
        }
        internal bool fixedWidth;
        public DataListFilterValue[] FilterList { get; set; }
        public DataListColumnModel SetFilterList(DataListFilterValue[] filterList)
        {
            FilterList = filterList;
            return this;
        }
        public Entities.StatusEngine.Status[] ValuesList { get; set; }
        public DataListColumnModel SetValuesList(Entities.StatusEngine.Status[] valuesList)
        {
            ValuesList = valuesList;
            return this;
        }
        public DataListCommand Command { get; set; }
        public DataListColumnModel SetCommand(DataListCommand command)
        {
            Command = command;
            return this;
        }
        public int Order { get; set; }
        public DataListColumnModel SetOrder(int order)
        {
            Order = order;
            return this;
        }
        public bool Bold { get; set; }
        public DataListColumnModel SetBold(bool bold = true)
        {
            Bold = bold;
            return this;
        }
        public Color TextColor { get; set; }
        public DataListColumnModel SetTextColor(Color textColor)
        {
            TextColor = textColor;
            return this;
        }
        public int Cut { get; set; }
        public DataListColumnModel SetCut(int cut)
        {
            Cut = cut;
            return this;
        }
        public int IntoLines { get; set; }
        public DataListColumnModel SetIntoLines(int intoLines)
        {
            IntoLines = intoLines;
            return this;
        }
        public bool DisallowSorting { get; set; }
        public DataListColumnModel SetDisallowSorting(bool disallowSorting = true)
        {
            DisallowSorting = disallowSorting;
            return this;
        }
        public bool DisallowFiltering { get; set; }
        public DataListColumnModel SetDisallowFiltering(bool disallowFiltering = true)
        {
            DisallowFiltering = disallowFiltering;
            return this;
        }
        public bool DisallowAutoWildcard { get; set; }
        public DataListColumnModel SetDisallowAutoWildcard(bool disallowAutoWildcard = true)
        {
            DisallowAutoWildcard = disallowAutoWildcard;
            return this;
        }
        //multicolumn (splitted columns)
        public List<DataListColumnModel> Columns { get; set; } = new List<DataListColumnModel>();
        public DataListColumnModel AddColumn(DataListColumnModel column)
        {
            Columns.Add(column);
            return this;
        }
        public string MultiColSep { get; set; }
        public DataListColumnModel SetMultiColSep(string separator)
        {
            MultiColSep = separator;
            return this;
        }
        public bool AllowHtmlFormating { get; set; }
        public DataListColumnModel SetAllowHtmlFormating(bool allowHtmlFormating = true)
        {
            AllowHtmlFormating = allowHtmlFormating;
            return this;
        }
        public string VueColumn { get; set; }
        public DataListColumnModel SetVueColumn(string vueColumn)
        {
            VueColumn = vueColumn;
            return this;
        }
        public string RawProperty { get; set; }
        public DataListColumnModel SetRawProperty(string rawProperty)
        {
            RawProperty = rawProperty;
            return this;
        }
        public bool TrimColumnInCondition { get; set; }
        public DataListColumnModel SetTrimColumnInCondition(bool trimColumnInCondition = true)
        {
            TrimColumnInCondition = trimColumnInCondition;
            return this;
        }
    }
}

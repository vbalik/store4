﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models.DataList
{
    public class DataListCommand
    {
        public enum CommandTypeEnum : short
        {
            cmCustom = 0,
            cmDetail = 1,
            cmEdit = 2,
            cmInsert = 3,
            cmDelete = 4,
            cmMenu = 5,
            cmRefresh = 6,
            cmAction = 7,
            cmCommon = 8,
            cmToClipboard = 9,
            cmToExcell = 10
        }

        public CommandTypeEnum CommandType { get; set; }
        public string CommandId { get; set; }
        public string CssClass { get; set; }
        public string Title { get; set; }
        public string Action { get; set; }
        public Entities.StatusEngine.ActionTypeEnum ActionType { get; set; }
        public DataListCommand[] MenuCommands { get; set; }
        public bool ConfirmRequired { get; set; }
    }
}

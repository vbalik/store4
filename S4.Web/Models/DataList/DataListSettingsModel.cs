﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection;

using S4.Core.Extensions;
using S4.Entities.Helpers;
using S4.Web.DataList;

namespace S4.Web.Models.DataList
{
    public class DataListSettingsModel<T>
    {
        public DataListSettingsModel()
        {
            ServerSide = true;
            ShowFilterRow = true;
            AllowSorting = true;
            PageSize = 10;
            Page = 1;
            ActionController = "api/procedures";
            MultiColSep = "/";
            RefreshVisible = true;

            IsSuperUser = false;

            KeyFields = typeof(T).GetKeyColumns();
        }

        public string DisplayName { get; set; }

        public bool ServerSide { get; set; }
        public string ActionController { get; set; }
        public string DataController { get; set; }

        public bool ShowFilterRow { get; set; }
        public bool AllowSorting { get; set; }
        public bool RowSelection { get; set; }
        public bool NoDetails { get; set; } //when displayed detail from detail - dont create detail hrefs
        public ColumnSettingsList Columns { get; set; }
        //pager
        public int PageSize { get; set; }
        public int Page { get; set; }
        public int Pages { get; set; }
        public int TotalItems { get; set; }
        public bool PagerInColumn { get; set; }
        //
        public DataListCommand[] Commands { get; set; }
        public string[] KeyFields { get; set; }
        public string StatusField { get; set; }
        //static filters
        public object FilterOption { get; set; }
        public DataListFilterOption[] FilterOptions { get; set; }
        //multicolumn (splitted columns)
        public string MultiColSep { get; set; }
        //export to excel
        public bool ExportToExcelDisallowed { get; set; }
        //selection
        public string SelectiongCheckboxClass { get; set; }
        //refresh command
        public bool RefreshVisible { get; set; }
        // user logged as superuser
        public bool IsSuperUser { get; set; }
    }
}

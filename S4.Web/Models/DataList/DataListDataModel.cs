﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection;

using S4.Core.Extensions;

namespace S4.Web.Models.DataList
{
    public class DataListDataModel<T, TSettingsModel> where T : new()
    {
        public TSettingsModel Settings { get; set; }
        public IEnumerable<T> Data { get; set; }
        public string Error { get; set; }
    }
}

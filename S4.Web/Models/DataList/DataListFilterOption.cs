﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models.DataList
{
    public class DataListFilterOption
    {
        public object ItemCode { get; set; }
        public string ItemDesc { get; set; }
    }
}

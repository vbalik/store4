﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class InternMessageSettingsModel : DataList.DataListSettingsModel<Entities.InternMessage>
    {
        public InternMessageSettingsModel() : base()
        {
            PageSize = 20;
            DisplayName = "Interní zprávy";
            DataController = "api/internmessages";
            StatusField = "msgClass";
        }

        public int IntMessID { get; set; }
    }
}

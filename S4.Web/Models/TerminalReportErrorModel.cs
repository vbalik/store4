﻿using System;
using System.Collections.Generic;

namespace S4.Web.Models
{
    public class TerminalReportErrorModel
    {
        public AppInfoModel AppInfo { get; set; }
        public List<CustomEventLoggerLogModel> Logs { get; set; }
    }

    public class CustomEventLoggerLogModel
    {
        public int ID { get; set; }
        public string Type { get; set; }
        public string Message { get; set; }
        public string Parameters { get; set; }
        public DateTime Created { get; set; }
    }

    public class AppInfoModel
    {
        public DateTime Created { get; set; }
        public string UserID { get; set; }
        public string Version { get; set; }
        public string OS_ver { get; set; }
        public string MONO_ver { get; set; }
        public string DeviceID { get; set; }
    }
}

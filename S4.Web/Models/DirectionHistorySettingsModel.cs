﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class DirectionHistorySettingsModel : DataList.DataListSettingsModel<Entities.DirectionHistory>
    {
        public DirectionHistorySettingsModel() : base()
        {
            PageSize = 10;
            DisplayName = "Historie";
            StatusField = "eventCode";
            DataController = "api/directionhistory";
        }

        public int DirectionID { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class ClientErrorModel
    {
        public string ErrorMessage { get; set; }
        public string ErrorClass { get; set; }
        public string Url { get; set; }
        public int Column { get; set; }
        public int Line { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Models
{
    public class MailModel
    {
        public bool IsError { get; set; }
        public string Message { get; set; }
    }
}

﻿namespace S4.Web.Models
{
    public class DirectionItemsCompletedModel
    {
        public int DirectionID { get; set; }
        public int ItemsCompleted { get; set; }
    }
}

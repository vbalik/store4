﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using S4.Core;
using S4.Core.Data;
using S4.Core.Extensions;
using S4.Core.Interfaces;
using S4.Web.ProcQueue;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Collections.Generic;
using S4.Web.AccessControl;
using System.Reflection;
using Microsoft.Extensions.Logging;
using S4.Web.Helpers;

namespace S4.Web.API
{
    public class ProceduresController : Controller
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IBackgroundTaskQueue _backgroundTaskQueue;
        private readonly ILogger<ProceduresController> _logger;

        public ProceduresController(IHttpContextAccessor httpContextAccessor, IBackgroundTaskQueue backgroundTaskQueue, ILogger<ProceduresController> logger)
        {
            _httpContextAccessor = httpContextAccessor;
            _backgroundTaskQueue = backgroundTaskQueue;
            _logger = logger;
        }


        [AllowAnonymous]
        [Route("/api/procedures/{procedureName}")]
        [HttpPost]
        public async Task<IActionResult> Index(string procedureName)
        {
            // get user id
            string userId;
            if (Request.HttpContext?.User.Identity.IsAuthenticated == true)
                userId = Request.HttpContext?.User.FindFirst(ClaimTypes.Sid)?.Value;
            else
                userId = Request.Headers[ApiAuthenticationMiddleware.API_USER_ID].ToString();

            // get procedure definition
            var procDef = Procedures.ProceduresCatalog.Current.GetProcedure(procedureName);
            if (procDef == null)
                return NotFound();

            // get params & try to convert
            string paramJson = await Request.GetRawBodyStringAsync();
            if (String.IsNullOrWhiteSpace(paramJson))
                return BadRequest(new { error = "Procedure params are not provided" });
            var param = JsonConvert.DeserializeObject(paramJson, procDef.ParameterType);

            // check params
            var propsIn = procDef.ParameterType.PropertiesWithAttr(typeof(S4.ProcedureModels.ParameterInAttribute));
            var check = new FieldsCheck();
            if (!check.DoCheck(param, propsIn))
                return BadRequest(new { error = "Parametry procedury nejsou platné", details = check.Errors });

            // check for duplicity in params - as a check for "doubled operations" from android terminals - STO4-632
            if (procDef.ProcedureType.IsDefined(typeof(Procedures.CheckParamsDuplicityAttribute), false))
            {
                if (Singleton<ParamsDuplicityChecker>.Instance.CheckDuplicity(procedureName, param))
                {
                    // repeated request with same params
                    (new ErrorLogHelper()).ReportProcException(paramJson, new { procedureName }, new Exception("Repeated request"), "/api/procedures", _logger);
                    return BadRequest(new { error = "Parametry procedury se opakují", details = new { } });
                }
            }

            try
            {
                // run procedure
                var procedureInstance = Activator.CreateInstance(procDef.ProcedureType, new object[] { userId });
                var method = procDef.ProcedureType.GetMethod("DoProcedure");
                if (procDef.ProcedureType.IsDefined(typeof(Procedures.RunInQueueAttribute), false))
                {
                    // enqueue procedure
                    var inCaseOfErrorMethod = procDef.ProcedureType.GetMethod("InCaseOfError");                    
                    var taskID = _backgroundTaskQueue.QueueBackgroundWorkItem(
                        // workTask
                        async token => await Task.Run(() => method.Invoke(procedureInstance, new object[] { param })),
                        // inCaseOfErrorTask
                        async token => await Task.Run(() => inCaseOfErrorMethod.Invoke(procedureInstance, new object[] { param })));

                    return Accepted(new { Success = true, TaskID = taskID });
                }
                else if (procDef.ProcedureType.IsDefined(typeof(Procedures.RunAsyncAttribute), false))
                {
                    // run procedure async 
                    var proc = (procedureInstance as Procedures.IProceduresBase);
                    var taskID = proc.TaskID; 

                    // run async
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
                    Task.Run(() =>
                    {
                        try
                        {
                            method.Invoke(procedureInstance, new object[] { param });
                        }
                        catch (Exception exc)
                        {
                            // try to show error to users - via messageRouter
                            proc.SendMessageToClient($"{proc.LastHeader} - CHYBA!", "Chyba byla zaznamenána a bude předána vývojovému oddělení", proc.LastProgressValue, Procedures.Messages.NotifyType.Danger);

                            // save exception
                            if (exc is System.Reflection.TargetInvocationException && exc.InnerException != null)
                                ReportProcException(userId, paramJson, exc.InnerException);
                            else
                                ReportProcException(userId, paramJson, exc);
                        }
                    });
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed

                    // return ok
                    return Accepted(new { Success = true, TaskID = taskID });
                }
                else
                {
                    // run procedure
                    var result = method.Invoke(procedureInstance, new object[] { param });

                    // check print procedures
                    if (result.GetType().IsSubclassOf(typeof(ProcedureModels.PrintProceduresModel)))
                    {
                        // save pdf data to download cache
                        var printProcModel = (ProcedureModels.PrintProceduresModel)result;
                        if (printProcModel.LocalPrint && printProcModel.Success)
                            printProcModel.FileID = Singleton<Helpers.FilesCache>.Instance.AddToCache("print.pdf", "application/pdf", printProcModel.FileData);
                    }

                    // return procedure result
                    return Ok(result);
                }
            }
            catch (Exception exc)
            {
                if (exc is System.Reflection.TargetInvocationException && exc.InnerException != null)
                {
                    ReportProcException(userId, paramJson, exc.InnerException);
                    return StatusCode(500);
                }
                else
                    throw exc;
            }
        }


        [Route("/api/procedures/login")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<ProcedureModels.Login.LoginModel> Login([FromBody] ProcedureModels.Login.LoginModel loginModel)
        {
            if (string.IsNullOrEmpty(loginModel.UserName))
                return new ProcedureModels.Login.LoginModel()
                {
                    ErrorText = "Musíte vyplnit uživatelské jméno!"
                };

            if (string.IsNullOrEmpty(loginModel.Password))
                return new ProcedureModels.Login.LoginModel()
                {
                    ErrorText = "Musíte vyplnit heslo!"
                };

            var worker = (new DAL.WorkerDAL()).GetByLogon(loginModel.UserName);
            if (worker == null)
                return new ProcedureModels.Login.LoginModel()
                {
                    ErrorText = $"Uživatel '{loginModel.UserName}' neexistuje"
                };

            if (worker.WorkerStatus == Entities.Worker.WK_STATUS_DISABLED)
            {
                return new ProcedureModels.Login.LoginModel()
                {
                    ErrorText = "Uživatel nemá přístup."
                };
            }


            if (Core.Security.S3SecData.CheckPassword(worker.WorkerSecData, worker.WorkerID, loginModel.Password))
            {
                var claims = new List<Claim> {
                                new Claim(ClaimTypes.Name, worker.WorkerName, ClaimValueTypes.String, "S4"),
                                new Claim(ClaimTypes.Sid, worker.WorkerID, ClaimValueTypes.String, "S4"),
                            };

                var userIdentity = new ClaimsIdentity(claims, "SecureLogin");
                var userPrincipal = new ClaimsPrincipal(userIdentity);

                await _httpContextAccessor.HttpContext.SignOutAsync();
                await _httpContextAccessor.HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme,
                    userPrincipal,
                    new AuthenticationProperties
                    {
                        ExpiresUtc = DateTime.UtcNow.AddHours(4),
                        IsPersistent = false,
                        AllowRefresh = false
                    });
            }
            else
            {
                return new ProcedureModels.Login.LoginModel()
                {
                    Success = false,
                    ErrorText = "Špatný uživatel nebo heslo."
                };
            }

            var secProvider = (AccessControl.ISecProvider)_httpContextAccessor.HttpContext.RequestServices.GetService(typeof(AccessControl.ISecProvider));
            _httpContextAccessor.HttpContext.Response.Headers.Add("S4_API_TOKEN", secProvider.GetApiToken(worker.WorkerID));

            return new ProcedureModels.Login.LoginModel() { Success = true, Worker = worker };
        }


        private void ReportProcException(string userID, string requestBody, Exception exc)
        {
            // status data
            var requestId = HttpContext.TraceIdentifier;

            var errorLogHelper = new ErrorLogHelper();
            errorLogHelper.ReportProcException(requestBody, new { userID, requestId }, exc, "/api/procedures error", _logger);
        }
    }
}
﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NPoco;
using S4.Core;
using S4.Entities;
using S4.Entities.Models;
using S4.Report.Model.Expedition;
using S4.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using static S4.Entities.Settings.SQL4Filters;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/action")]
    public class ActionController : Controller
    {
        [HttpPost]
        [Route("prepareexpeditionreport")]
        public List<PrintExpeditionRowReportModel> PrepareExpeditionReport([FromBody]ExpeditionModel model)
        {
            var dtFrom = model.DateFrom.ToLocalTime();
            var dtTo = model.DateTo.ToLocalTime();
            dtFrom = new DateTime(dtFrom.Year, dtFrom.Month, dtFrom.Day, 0, 0, 0);
            dtTo = new DateTime(dtTo.Year, dtTo.Month, dtTo.Day, 23, 59, 59);

            if (dtFrom < new DateTime(1753, 1, 1) || dtFrom > new DateTime(9999, 12, 13) || dtTo < new DateTime(1753, 1, 1) || dtTo > new DateTime(9999, 12, 13))
            {
                return new List<PrintExpeditionRowReportModel>();
            }

            List<PrintExpeditionReportModel> data = null;
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var sql = new Sql();
                sql.Select("d.docInfo, h.eventData as SPZ, MAX(p.partnerCity) as partnerCity");
                sql.From("s4_direction_history h WITH (NOLOCK)");
                sql.InnerJoin("s4_direction d WITH (NOLOCK) on d.directionID = h.directionID");
                sql.LeftJoin("s4_partnerList p WITH (NOLOCK) on p.partnerID = d.partnerID");

                if (model.TruckRegionEnum == Entities.Truck.TruckRegionEnum.Czech || model.TruckRegionEnum == Entities.Truck.TruckRegionEnum.Moravia)
                {
                    sql.InnerJoin("s4_truckList t WITH (NOLOCK) on lower(t.truckRegist) = lower(h.eventData)");
                    sql.Where("t.truckRegion = @0", (byte)model.TruckRegionEnum);
                }

                sql.Where("d.docStatus = 'EXPE' AND h.eventCode = 'EXPE' AND h.eventData IS NOT NULL AND len(h.eventData) = 7");
                sql.Where("h.entryDateTime between @0 AND @1", dtFrom, dtTo);
                sql.GroupBy("h.directionID,d.docInfo, h.eventData, d.partnerID");
                sql.OrderBy("h.eventData, MAX(p.partnerCity)");

                data = db.Fetch<PrintExpeditionReportModel>(sql);
            }

            var rowsGroupBySPZ = from d in data group d by d.SPZ into r select new { SPZ = r.Key, Other = r.ToList() };
            var dataToReport = new List<PrintExpeditionRowReportModel>();
            var rowNum = 0;
            foreach (var item in rowsGroupBySPZ)
            {
                var rowGroupByPartner = from i in item.Other group i by i.PartnerCity into r select new { PartnerName = r.Key, Other = r.ToList() };
                var rowToReport = new PrintExpeditionRowReportModel();
                rowToReport.SPZ = item.SPZ;
                StringBuilder dlSB = new StringBuilder();
                foreach (var item2 in rowGroupByPartner)
                {
                    var dl = (from d in item2.Other select d).OrderBy(d => d.DocInfo).Select(d => d.DocInfo).ToList();
                    dlSB.Append($"{item2.PartnerName} - {string.Join(", ", dl.ToArray())}{Environment.NewLine}");
                }
                rowToReport.DL = dlSB.ToString();
                rowToReport.RowNum = rowNum;
                dataToReport.Add(rowToReport);
                rowNum++;
            }

            return dataToReport;
        }


        [HttpPost]
        [Route("printexpeditionreport")]
        public PrintExpeditionRowsToPrintReportModel PrintExpeditionReport([FromBody]PrintExpeditionRowsToPrintReportModel model)
        {

            //variables
            var variables = new List<Tuple<string, object, Type>>();
            variables.Add(new Tuple<string, object, Type>("Header", model.Header, typeof(string)));
            variables.Add(new Tuple<string, object, Type>("DateDeparture", model.DateDeparture, typeof(string)));
            variables.Add(new Tuple<string, object, Type>("Truck", model.Truck, typeof(string)));
            variables.Add(new Tuple<string, object, Type>("DeparturePlace", model.DeparturePlace, typeof(string)));
            variables.Add(new Tuple<string, object, Type>("ArrivalPlace", model.ArrivalPlace, typeof(string)));
            variables.Add(new Tuple<string, object, Type>("DtFrom", model.DtFrom, typeof(string)));
            variables.Add(new Tuple<string, object, Type>("DtTo", model.DtTo, typeof(string)));

            //Find Report

            var reportID = Singleton<DAL.SettingsSet>.Instance.GetValue_Int32(Entities.SettingsDictionary.PRINT_EXPEDITION_REPORT_ID);
            Reports report = null;
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                report = db.SingleOrDefault<Reports>("where reportID = @0", reportID);
                if (report == null)
                    throw new Exception($"Report ID: {reportID} nebyl nalezen.");
            }

            //do print
            Report.Print print = new Report.Print();
            print.Variables = variables;
            print.ReportDefinition = report.Template; //šablona z DB
            //print.DesignMode = true; //pouze pro úpravy šablony
            //print.ReportDefFilePath = @"c:\Projects\store4\Tools\Reporty\RozvozExpedice.srt"; //pouze pro úpravy šablony

            byte[] pdfData = null;
            var fileID = "";
            if (model.LocalPrint)
            {
                pdfData = print.DoPrint2PDF(model.Rows);
                fileID = Singleton<Helpers.FilesCache>.Instance.AddToCache("print.pdf", "application/pdf", pdfData);
            }
            else
            {
                using (var db = Core.Data.ConnectionHelper.GetDB())
                {
                    // get printer location
                    var printerlocation = db.SingleOrDefault<PrinterLocation>("where printerLocationID = @0", model.PrinterLocationID);
                    if (printerlocation == null)
                        throw new Exception($"Nebyla nalezena tiskárna printerLocationID: {model.PrinterLocationID}");

                    print.DoPrint(printerlocation.PrinterPath, model.Rows);
                }
            }


            if (print.IsError)
            {
                return new PrintExpeditionRowsToPrintReportModel() { ErrorText = print.ErrorText };
            }

            return new PrintExpeditionRowsToPrintReportModel() { ErrorText = "", PdfData = pdfData, LocalPrint = model.LocalPrint, FileID = fileID };
        }


        [HttpPost]
        [Route("dispchecklist")]
        public object DispCheckList()
        {
            // return all directions with status DR_STATUS_DISPATCH_SAVED
            //var dirs = (new DAL.DirectionDAL()).ListByStatus(Direction.DR_STATUS_DISPATCH_SAVED);
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var dirs = db.Query<DirectionModel>().Where(d => d.DocStatus == Direction.DR_STATUS_DISPATCH_SAVED && d.DocDirection == Direction.DOC_DIRECTION_OUT).ToList();
                var IDs = dirs.Select(d => d.DirectionID).ToList();

                var positions = db.Query<DirectionXtra>().Where(x => IDs.Contains(x.DirectionID) && x.XtraCode == Direction.XTRA_DISPATCH_POSITION_ID).ToList();
                var workers = db.Query<DirectionAssign>().Where(a => IDs.Contains(a.DirectionID) && a.JobID == DirectionAssign.JOB_ID_DISPATCH).ToList();

                dirs.ForEach(d =>
                {
                    d.XtraData.Data.AddRange(positions.Where(p => p.DirectionID == d.DirectionID));
                    d.Assigns = new List<DirectionAssign>(workers.Where(p => p.DirectionID == d.DirectionID));
                });


                return dirs.Select(d => new
                {
                    d.DirectionID,
                    d.DocNumPrefix,
                    d.DocNumber,
                    d.DocYear,
                    PartnerName = Singleton<DAL.Cache.PartnerCache>.Instance.GetItem(d.PartnerID)?.PartnerName,
                    Position = d.XtraData.IsNull(Direction.XTRA_DISPATCH_POSITION_ID, 0) ? new Position() : Singleton<DAL.Cache.PositionCache>.Instance.GetItem((int)d.XtraData[Direction.XTRA_DISPATCH_POSITION_ID]),
                    WorkerCode = d.Assigns.Where(a => a.JobID == DirectionAssign.JOB_ID_DISPATCH).SingleOrDefault()?.WorkerID
                });
            }
        }

    }
}

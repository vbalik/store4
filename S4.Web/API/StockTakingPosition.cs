﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NPoco;

using S4.Entities.Views;
using S4.Web.DataList;
using S4.Web.Models;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/StockTakingPosition")]
    public class StockTakingPositionController : DataListController<StockTakingPositionView, StockTakingPositionSettings, StockTakingPositionSettingsModel>
    {
        public StockTakingPositionController(IHttpContextAccessor httpContextAccessor, ILogger<DataListController<StockTakingPositionView, StockTakingPositionSettings, StockTakingPositionSettingsModel>> logger) : base(httpContextAccessor, logger)
        {

        }

        protected override Sql GetSql(StockTakingPositionSettingsModel settings)
        {
            var sql = base.GetSql(settings);
            if (settings.StotakID != 0)
            {
                sql.Where("stotakID = @0", settings.StotakID);
            }
            switch (settings.PositionsViewRange)
            {
                case StockTakingPositionSettingsModel.PositionsViewRangeEnum.Checked:
                    sql.Where("checkCounter > 0");
                    break;
                case StockTakingPositionSettingsModel.PositionsViewRangeEnum.NotChecked:
                    sql.Where("checkCounter = 0");
                    break;
            }
            return sql;
        }
    }
}

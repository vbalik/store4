﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NPoco;

using S4.Entities;
using S4.Web.DataList;
using S4.Web.Models;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/ArticleHistory")]
    public class ArticleHistoryController : DataListController<ArticleHistory, ArticleHistorySettings, ArticleHistorySettingsModel>
    {
        public ArticleHistoryController(IHttpContextAccessor httpContextAccessor, ILogger<DataListController<ArticleHistory, ArticleHistorySettings, ArticleHistorySettingsModel>> logger) : base(httpContextAccessor, logger)
        {

        }

        protected override Sql GetSql(ArticleHistorySettingsModel settings)
        {
            var sql = base.GetSql(settings);
            if (settings.ArticleID != 0)
            {
                sql.Where("articleID = @0", settings.ArticleID);
            }
            return sql;
        }
    }
}

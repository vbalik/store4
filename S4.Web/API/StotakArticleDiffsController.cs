﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NPoco;

using S4.Entities.Models;
using S4.Web.DataList;
using S4.Web.Models;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/StotakArticleDiffs")]
    public class StotakArticleDiffsController : DataListController<StotakArticleDifferences, StotakArticleDiffsSettings, StotakArticleDiffsSettingsModel>
    {
        public StotakArticleDiffsController(IHttpContextAccessor httpContextAccessor, ILogger<DataListController<StotakArticleDifferences, StotakArticleDiffsSettings, StotakArticleDiffsSettingsModel>> logger) : base(httpContextAccessor, logger)
        {

        }

        protected override Sql GetSql(StotakArticleDiffsSettingsModel settings)
        {
            var sql = new Sql()
                .Append("SELECT SAD.* FROM")
                .Append("(")
                .Append("   SELECT S.*, (quantStTa - quantOnSt) AS quantDiff, (quantStTa - quantOnSt) *unitPrice AS priceDiff, A.articleID, A.articleCode, A.articleDesc, A.packDesc FROM")
                .Append("   (")
                .Append("       SELECT artPackID, SUM(quantOnSt) AS quantOnSt, SUM(quantStTa) AS quantStTa, SUM(unitPrice) AS unitPrice FROM")
                .Append("       (")
                .Append("           SELECT artPackID, SUM(quantity) AS quantOnSt, 0 AS quantStTa, ISNULL(SUM(quantity * unitPrice) / SUM(quantity), 0) AS unitPrice")
                .Append("           FROM dbo.s4_stocktaking_snapshots")
                .Append("           WHERE(stotakID = @0) AND(snapshotClass = @1)", settings.StotakID, settings.SnapshotClass)
                .Append("           GROUP BY artPackID")
                .Append("           UNION ALL")
                .Append("           SELECT artPackID, 0 as quantOnSt, SUM(quantity) AS quantStTa, 0 AS unitPrice")
                .Append("           FROM dbo.s4_stocktaking_items")
                .Append("           WHERE(stotakID = @0)", settings.StotakID)
                .Append("           GROUP BY artPackID")
                .Append("       )")
                .Append("       Derived GROUP BY artPackID")
                .Append("       HAVING ISNULL(SUM(quantOnSt), 0) != ISNULL(SUM(quantStTa), 0)")
                .Append("   ) S")
                .Append("   LEFT OUTER JOIN s4_vw_article_base A ON A.artPackID = S.artPackID")
                .Append(") SAD");

            return sql;
        }
    }
}

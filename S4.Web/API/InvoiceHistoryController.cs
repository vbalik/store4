﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using NPoco;

using S4.Entities;
using S4.Web.Models;
using S4.Web.DataList;
using Microsoft.Extensions.Logging;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/invoicehistory")]
    public class InvoiceHistoryController : DataListController<InvoiceHistory, InvoiceHistorySettings, InvoiceHistorySettingsModel>
    {
        public InvoiceHistoryController(IHttpContextAccessor httpContextAccessor, ILogger<DataListController<InvoiceHistory, InvoiceHistorySettings, InvoiceHistorySettingsModel>> logger) : base(httpContextAccessor, logger)
        {

        }

        protected override Sql GetSql(InvoiceHistorySettingsModel settings)
        {
            var sql = base.GetSql(settings);
            if (settings.InvoiceID != 0)
            {
                sql.Where("invoiceID = @0", settings.InvoiceID);
            }
            return sql;
        }
    }
}

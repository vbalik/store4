﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NPoco.Expressions;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using S4.Core;
using S4.Web.Models;
using S4.Web.PerfMeasurement;

namespace S4.Web.API
{
    [Produces("application/json")]
    [ApiController]
    public class StatusController : ControllerBase
    {
        [HttpPost]
        [Route("api/status/procedures")]
        public object Procedures()
        {
            return Singleton<PerformanceStatus>.Instance.GetStatus();
        }

        [HttpPost]
        [Route("api/status/messagebus")]
        public object Messagebus()
        {
            var dal = new DAL.MessageBusDAL();
            var count10 = dal.Last10DaysCount();
            var start = DateTime.Today.AddDays(-9).ToLocalTime();
            var days = Enumerable.Range(0, 10).Select(offset => start.AddDays(offset)).ToList();

            var rows = (from d in days
                        join c in count10 on d.Date equals c.Item1.Date into groupMain
                        from c in groupMain.DefaultIfEmpty()
                        group c by d.Date into g
                        select new { Date = g.Key, Other = g.ToList() }).ToList();

            var data = new List<MessageBusItemModel>();
            for (int i = 0; i < 10; i++) //for each day
            {
                var messageNew = rows[i].Other.Where(x => x != null && x.Item3 == Entities.MessageBus.NEW).Select(x => x.Item2).Sum();
                var messageDone = rows[i].Other.Where(x => x != null && x.Item3 == Entities.MessageBus.DONE).Select(x => x.Item2).Sum();
                var messageNoComplete = rows[i].Other.Where(x => x != null && x.Item3 == Entities.MessageBus.NOCOMPLETE).Select(x => x.Item2).Sum();
                var messageWait = rows[i].Other.Where(x => x != null && x.Item3 == Entities.MessageBus.WAITING_FOR_REPEAT).Select(x => x.Item2).Sum();
                var messageWaitCompl = rows[i].Other.Where(x => x != null && x.Item3 == Entities.MessageBus.WAITING_FOR_COMPLETE).Select(x => x.Item2).Sum();
                var messageError = rows[i].Other.Where(x => x != null && x.Item3 == Entities.MessageBus.ERROR).Select(x => x.Item2).Sum();
                data.Add(new MessageBusItemModel { DateTime = rows[i].Date, MessageDone = messageDone, MessageError = messageError, MessageNew = messageNew, MessageWait = messageWait, MessageNoComplete = messageNoComplete, MessageWaitComplete = messageWaitCompl });
            }

            return data;

        }

        [HttpPost]
        [Route("api/status/errors")]
        public object Errors()
        {
            //CLIENT
            var dal = new DAL.ErrorDAL();
            // last 10 days
            var count10 = dal.Last10DaysCount();
            var start = DateTime.Today.AddDays(-9).ToLocalTime();
            var days = Enumerable.Range(0, 10).Select(offset => start.AddDays(offset)).ToList();

            // top 10 errors in last 10 days
            var top10 = dal.Top10In10Days();

            //EXCHANGE
            var dalExchange = new DAL.ExchangeErrorDAL();
            // last 10 days
            var count10Exchange = dalExchange.Last10DaysCount();
            var startExchange = DateTime.Today.AddDays(-9).ToLocalTime();
            var daysExchange = Enumerable.Range(0, 10).Select(offset => startExchange.AddDays(offset)).ToList();

            // top 10 errors in last 10 days
            var top10Exchange = dalExchange.Top10In10Days();

            //By JobName
            var jobName = dalExchange.JobNameIn10Days();

            return new
            {
                clientError = new
                {
                    last10days = days.Select(d => new
                    {
                        date = d,
                        cnt = count10.Where(c => c.Item1 == d).Select(c => c.Item2).SingleOrDefault()
                    }),
                    top10 = top10.Select(t => new
                    {
                        errorClass = t.Item1,
                        cnt = t.Item2
                    })
                },
                exchangeError = new
                {
                    last10days = daysExchange.Select(d => new
                    {
                        date = d,
                        cnt = count10Exchange.Where(c => c.Item1 == d).Select(c => c.Item2).SingleOrDefault()
                    }),
                    top10 = top10Exchange.Select(t => new
                    {
                        errorClass = t.Item1,
                        cnt = t.Item2
                    }),
                    jobName = jobName.Select(d => new
                    {
                        jobName = d.Item1,
                        cnt = d.Item2
                    }),
                }
            };
        }

    }


}
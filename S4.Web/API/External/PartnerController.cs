﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using S4.Core;
using S4.Entities;
using S4.Entities.Helpers.Xml.Article;
using S4.ProcedureModels.Receive;
using S4.Procedures;
using S4.Report.Model.Dispatch;
using S4.StoreCore.Interfaces;
using S4.StoreCore.Save_Basic;
using S4.Web.API.External.Helper;
using S4.Web.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static S4.Web.API.External.ABRAUploadController;
using Microsoft.Extensions.Configuration;
using S4.StoreCore.OnStore_Basic;
using OfficeOpenXml.Drawing.Chart;
using S4.Entities.Helpers.Xml.Dispatch;
using System.IO;
using System.Xml.Serialization;
using System.Xml;


namespace S4.Web.API.External
{
    [ApiController]
    [AllowAnonymous]
    public class PartnerController : ControllerBase
    {
        private static NLog.Logger _nLogger = NLog.LogManager.GetLogger("S4.ABRA.PARTNER");
        private IConfiguration _configuration;

        public PartnerController(IConfiguration configurationRoot, IOnStore onStore)
        {
            _configuration = configurationRoot;
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("api/external/partner/uploadpartner")]
        public async Task<ContentResult> UploadPartner()
        {
            //http://localhost:5000/api/external/partner/uploadpartner
            return ImportXMLPartner(await Request.GetRawBodyBytesAsync());
        }

        #region private

        private ContentResult ImportXMLPartner(byte[] contents)
        {
            var helper = new ExternalHelper(_nLogger);
            var pathError = _configuration.GetSection($"exchange:read:partner:path:pathError").Value;
            var pathArchive = _configuration.GetSection($"exchange:read:partner:path:pathArchive").Value;
            string responseXML = string.Empty;

            if (string.IsNullOrEmpty(pathArchive) || string.IsNullOrEmpty(pathError))
            {
                var msg = "Path must be specified!";
                _nLogger.Error(msg);
                return new ContentResult { Content = msg, ContentType = "text/plain", StatusCode = 500 };
            }

            string xmlText;
            if (!GetXMLText(contents, out xmlText))
            {
                _nLogger.Error(xmlText);
                return new ContentResult { Content = xmlText, ContentType = "text/plain", StatusCode = 500 };
            }

            _nLogger.Info("Partner - load start");

            if (!Entities.Helpers.Xml.Dispatch.DispatchCustomerS3.Serializer.Deserialize(xmlText, out Entities.Helpers.Xml.Dispatch.DispatchCustomerS3 customerS3, out Exception exception))
            {
                //error parse
                var errorParse = $"Partner: {exception.Message} - {exception.InnerException.Message}";
                helper.DoError(xmlText, $"Error parse partner : {errorParse}", pathError, "");
                _nLogger.Error(errorParse);
                _nLogger.Info($"Partner load end with error! No save!");
                return new ContentResult { Content = errorParse, ContentType = "text/plain", StatusCode = 500 };
            }

            //set and save partner
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                using (var tr = db.GetTransaction())
                {
                    var dispatchLoadHelper = new S4.Procedures.Load.Helper.DispatchLoadHelper();
                    var partner = dispatchLoadHelper.SetPartner(customerS3, db);

                    //check 
                    if (string.IsNullOrEmpty(partner.PartnerName))
                    {
                        _nLogger.Info($"Partner load end with error! No save!");
                        return new ContentResult { Content = "Customer name is cannot be empty", ContentType = "text/plain", StatusCode = 500 };
                    }
                                            
                    S3CustomerResponse subReq = new S3CustomerResponse();

                    //save partner
                    if (partner.PartnerID == 0)
                    {
                        //insert Partner
                        Core.Utils.EntitiesUtils.RepairMaxLengthModelValues(partner, "source_id");
                        db.Insert(partner);
                        _nLogger.Info($"Insert partner {partner.PartnerID}");
                    }
                    else
                    {
                        //save Partner
                        Core.Utils.EntitiesUtils.RepairMaxLengthModelValues(partner, "source_id");
                        db.Save(partner);
                        _nLogger.Info($"Update partner {partner.PartnerID}");
                    }

                    subReq.CustomerId = partner.Source_id;
                    subReq.S4CustomerId = partner.PartnerID;
                    subReq.DeliveryAddresses = new DeliveryAddresses();

                    //set adresses
                    List<DeliveryAddress> adressList = new List<DeliveryAddress>();
                    foreach (var deliveryAddress in customerS3.DeliveryAddresses)
                    {
                        //check 
                        if (string.IsNullOrEmpty(deliveryAddress.Name))
                        {
                            _nLogger.Info($"Partner load end with error! No save!");
                            return new ContentResult { Content = "Address name is cannot be empty", ContentType = "text/plain", StatusCode = 500 };
                        }
                            
                        var address = db.FirstOrDefault<PartnerAddress>("where source_id = @0", deliveryAddress.AddressID);
                        if (address == null)
                            address = new PartnerAddress();
                        
                        address.AddrCity = deliveryAddress.City;
                        address.AddrLine_1 = deliveryAddress.Adress1;
                        address.AddrLine_2 = deliveryAddress.Adress2;
                        address.AddrName = deliveryAddress.Name;
                        address.AddrPhone = deliveryAddress.Phone;
                        address.AddrRemark = deliveryAddress.Remark;
                        address.DeliveryInst = deliveryAddress.Deliveryinstructions;
                        address.Source_id = deliveryAddress.AddressID;
                        address.PartnerID = partner.PartnerID;

                        //save address
                        if (address.PartAddrID == 0)
                        {
                            //insert address
                            Core.Utils.EntitiesUtils.RepairMaxLengthModelValues(address, "source_id");
                            db.Insert(address);
                            _nLogger.Info($"Insert address {address.PartAddrID}");
                        }
                        else
                        {
                            //update address
                            Core.Utils.EntitiesUtils.RepairMaxLengthModelValues(address, "source_id");
                            db.Save(address);
                            _nLogger.Info($"Update address {address.PartAddrID}");
                        }

                        adressList.Add(new DeliveryAddress() { S4DeliveryAddressId = address.PartAddrID, AddressId = deliveryAddress.AddressID });
                    }

                    subReq.DeliveryAddresses.DeliveryAddressList = adressList;

                    tr.Complete();
                    _nLogger.Info($"Partner save!");
                    responseXML = CreateRespons(subReq);
                    helper.DoProcessed(xmlText, pathArchive, $"Request_{subReq.CustomerId}", $"Partner customer_id: {subReq.CustomerId}");
                    helper.DoProcessed(responseXML, pathArchive, $"Response_{subReq.CustomerId}", $"Partner customer_id: {subReq.CustomerId}");
                }
            }

            _nLogger.Info("Partner - load end");

            return new ContentResult { Content = responseXML, ContentType = "application/xml", StatusCode = 200 };
        }

        private bool GetXMLText(byte[] contents, out string xmlText)
        {
            var helper = new ExternalHelper(_nLogger);
            return helper.GetXMLText(contents, out xmlText);
        }

        private string CreateRespons(S3CustomerResponse subReq)
        {
            XmlSerializer xsSubmit = new XmlSerializer(typeof(S3CustomerResponse));
            //var subReq = new S3CustomerResponse();
            var xml = "";

            using (var sww = new StringWriter())
            {
                using (XmlWriter writer = XmlWriter.Create(sww))
                {
                    xsSubmit.Serialize(writer, subReq);
                    xml = sww.ToString(); // Your XML
                }
            }

            return xml;
        }

        #endregion

    }
}

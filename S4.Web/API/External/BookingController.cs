﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NPoco;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using OfficeOpenXml.FormulaParsing.Excel.Functions.RefAndLookup;
using Org.BouncyCastle.Asn1.Tsp;
using S4.Core;
using S4.DAL;
using S4.DeliveryServices.Models.CPost;
using S4.Entities;
using S4.Entities.Models;
using S4.ProcedureModels.Booking;
using S4.ProcedureModels.Receive;
using S4.Procedures;
using S4.Report.Model.Dispatch;
using S4.StoreCore.Interfaces;
using S4.StoreCore.OnStore_Basic;
using S4.StoreCore.Save_Basic;
using S4.Web.API.External.Helper;
using S4.Web.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace S4.Web.API.External
{
    public class BookingController : ControllerBase
    {
        internal static TimeZoneInfo TzInfo = TimeZoneInfo.FindSystemTimeZoneById("Central European Standard Time");
        private IConfiguration _configuration;

        public BookingController(IConfiguration configurationRoot)
        {
            _configuration = configurationRoot;
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("api/external/booking/valid")]
        public IActionResult GetValidBookings()
        {
            var trans = new DAL.BookingDAL();
            var data = trans.GetValid();
            return Ok(data);
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("api/external/booking/available")]
        public IActionResult GetAvailableBatchNums(string artCode)
        {
            var transArticle = new DAL.ArticleDAL();
            var articles = transArticle.ScanByArticleCode(artCode, false);

            if (articles.Count < 1)
                return BadRequest("Karta nebyla nalezena!");

            var model = new BookingListAvailableLots();
            var proc = new Procedures.Booking.BookingListAvailableLotsProc(GetUserID());
            var rows = new List<BookingAvailableExternalModel>();
            foreach (var article in articles)
            {
                foreach (var packing in article.Packings)
                {
                    model = new BookingListAvailableLots { ArtPackID = packing.ArtPackID };
                    BookingListAvailableLots res = proc.DoProcedure(model);

                    if (res.Success)
                    {
                        foreach (var lot in res.StoreMoveLots)
                        {
                            var row = new BookingAvailableExternalModel
                            {
                                ArticleCode = article.ArticleCode,
                                ArticleDesc = article.ArticleDesc,
                                ArticleID = article.ArticleID,
                                ArtPackID = lot.ArtPackID,
                                BatchNum = lot.BatchNum,
                                ExpirationDate = lot.ExpirationDate,
                                Quantity = lot.Quantity,
                                StoMoveLotID = lot.StoMoveLotID,
                            };
                            rows.Add(row);
                        }
                    }
                    else
                    {
                        return BadRequest(res.ErrorText);
                    }
                }
            }

            return Ok(rows.OrderBy(_ => _.ExpirationDate).ToList());
        }

        [AllowAnonymous]
        [Route("/api/external/booking")]
        [HttpDelete]
        public IActionResult DeleteBooking(int id)
        {
            var model = new BookingDeleteItem
            {
                BookingID = id,
            };

            var proc = new Procedures.Booking.BookingDeleteItemProc(GetUserID());
            var res = proc.DoProcedure(model);

            if (res.Success)
                return Ok();
            else
                return BadRequest(res.ErrorText);
        }

        [AllowAnonymous]
        [Route("/api/external/booking")]
        [HttpPost]
        public IActionResult CreateBooking([FromBody] BookingAddItem model)
        {
            model.BookingTimeout = TimeZoneInfo.ConvertTimeFromUtc(model.BookingTimeout, TzInfo);
            var proc = new Procedures.Booking.BookingAddItemProc(GetUserID());
            var res = proc.DoProcedure(model);

            if (res.Success)
                return Ok();
            else
                return BadRequest(res.ErrorText);
        }

        #region private

        private string GetUserID()
        {
            return _configuration.GetSection($"exchange:read:userID").Value;
        }

        #endregion 
    }



}


﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using S4.Core;
using S4.Entities;
using S4.ProcedureModels.Receive;
using S4.Procedures;
using S4.Report.Model.Dispatch;
using S4.StoreCore.Interfaces;
using S4.StoreCore.Save_Basic;
using S4.Web.API.External.Helper;
using S4.Web.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace S4.Web.API.External
{
    public class DirectionController : ControllerBase
    {
        public enum DocDirection
        {
            IN = 1,
            OUT = -1,
            MOVE = 0,
        }

        public class TopModel
        {
            public int DirectionID { get; set; }
            public string DocStatus { get; set; }
            public string DocInfo { get; set; }
            public string DocNumPrefix { get; set; }
            public string DocYear { get; set; }
            public int DocNumber { get; set; }
            public DateTime DocDate { get; set; }
            public DateTime EntryDateTime { get; set; }
            public string UserName { get; set; }
        }

        public class DirectionModel
        {
            public TopModel Detail { get; set; }
            public List<DirectionModelItem> Items { get; set; }
        }

        public class DirectionModelItem
        {
            public string ArticleCode { get; set; }
            public string ArticleName { get; set; }
            public string BatchNum { get; set; }
            public DateTime? ExpirationDate { get; set; }
            public string CarrierNum { get; set; }
            public decimal Quantity { get; set; }
            public List<string> SerialsNumber { get; set; }
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("api/external/direction/top")]
        public List<TopModel> Top(DocDirection docDirection, int records)
        {
            //http://localhost:5000/api/external/direction/top?docDirection=out&records=10
                       
            return Core.Singleton<DirectionsCache>.Instance.GetList((int)docDirection, records);
        }



        [AllowAnonymous]
        [HttpGet]
        [Route("api/external/direction")]
        public DirectionModel Detail(int directionID)
        {
            //http://localhost:5000/api/external/direction?directionID=123
          
            return Core.Singleton<DirectionCache>.Instance.GetItem(directionID); 
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("api/external/directionbydocinfo")]
        public DirectionModel DetailByDocInfo(string docPrefix, int year, int nr)
        {
            //http://localhost:5000/api/external/directionbydocinfo?docprefix=PVKS&year=23&nr=3242
            //http://localhost:5000/api/external/directionbydocinfo?docprefix=PRIJ&year=23&nr=197105

            return Core.Singleton<DirectionByDocInfoCache>.Instance.GetItem(docPrefix, year, nr);
        }
    }
}

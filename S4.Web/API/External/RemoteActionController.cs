﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using NPoco;
using S4.Core;
using S4.Core.Data;
using S4.Core.Interfaces;
using S4.DAL;
using S4.Entities;
using S4.Entities.Helpers;
using S4.Entities.Models;
using S4.Procedures.DispCheck;
using S4.SharedLib.RemoteAction;
using S4.SharedLib.Security;
using S4.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;


namespace S4.Web.API.External
{
    public class RemoteActionController : Controller
    {
        private readonly IBackgroundTaskQueue _backgroundTaskQueue;
        private readonly ILogger<RemoteActionController> _logger;
        private readonly RemoteActionDAL _remoteActionDAL;
        private readonly DirectionDAL _directionDAL;

        public RemoteActionController(IBackgroundTaskQueue backgroundTaskQueue, ILogger<RemoteActionController> logger, RemoteActionDAL remoteActionDAL, DirectionDAL directionDAL)
        {
            _backgroundTaskQueue = backgroundTaskQueue;
            _logger = logger;
            _remoteActionDAL = remoteActionDAL;
            _directionDAL = directionDAL;
        }


        [AllowAnonymous]
        [Route("/api/external/remoteaction/setupdata")]
        [HttpGet]
        public IActionResult SetupData([FromHeader] string apiAuthorization, [FromHeader] string datetime, [FromServices] DAL.DAL<Entities.Truck, int> dalTruck)
        {
            // check authorization
            if (!DateTime.TryParseExact(datetime, "O", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.AssumeLocal, out DateTime dateTimeValue))
                return Unauthorized();
            if (String.IsNullOrWhiteSpace(apiAuthorization) || !RemoteAccessAuthorization.CheckKey(String.Empty, dateTimeValue, apiAuthorization))
                return Unauthorized();

            var truckInfos = dalTruck.List().Select(_ => new SharedLib.S4Truck.SetupDataModel.TruckInfo() { TruckID = _.TruckID, TruckRegist = _.TruckRegist }).ToList();
            var appVersion = Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion;
            var result = new SharedLib.S4Truck.SetupDataModel()
            {
                ServerVersion = appVersion,
                ServerTime = DateTime.Now,
                TruckInfos = truckInfos
            };
            return Ok(result);
        }


        [AllowAnonymous]
        [Route("/api/external/remoteaction/{actionType}")]
        [HttpPost]
        public async Task<IActionResult> Index(string actionType, [FromHeader] string apiAuthorization, [FromHeader] string datetime)
        {
            // check authorization
            string paramJson = await Request.GetRawBodyStringAsync();
            if (!DateTime.TryParseExact(datetime, "O", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.AssumeLocal, out DateTime dateTimeValue))
                return Unauthorized();
            if (String.IsNullOrWhiteSpace(apiAuthorization) || !RemoteAccessAuthorization.CheckKey(paramJson, dateTimeValue, apiAuthorization))
                return Unauthorized();


            // get action def
            (var actType, var paramsType, var runAction) = GetActionDefs(actionType);
            if (actType == null)
                return NotFound();

            // get params & try to convert
            if (string.IsNullOrWhiteSpace(paramJson))
                return BadRequest(new { error = "Action params are not provided" });
            var param = JsonConvert.DeserializeObject(paramJson, paramsType);

            // check params
            var check = new FieldsCheck();
            if (!check.DoCheck(param))
                return BadRequest(new { error = "Action params are not valid", details = check.Errors });
            var paramsBase = (RemoteActionModelBase)param;

            _logger.LogInformation("New RemoteAction - type: {actionType}; {paramJson}", actionType, paramJson);

            var directionExist = GetDirectionByDocumentID(paramsBase.DocIDSource, paramsBase.DocInfo);
            if (directionExist == null)
                return BadRequest(new { error = $"Direction {paramsBase.DocInfo} of type {paramsBase.DocIDSource} not found", details = "" });

            try
            {
                // save to db
                var ra = new RemoteAction()
                {
                    RunStatus = RemoteAction.RA_STATUS_NEW,
                    ActionType = actType.ToString(),
                    ActionParams = paramJson,
                    SourceUserID = paramsBase.UserID,
                    SourceDeviceID = paramsBase.DeviceID,
                    ActionDate = paramsBase.ActionDate,
                    Received = DateTime.Now
                };
                _remoteActionDAL.Insert(ra);

                // send to queue
                var taskID = _backgroundTaskQueue.QueueBackgroundWorkItem(async token => runAction(ra));

                _logger.LogDebug("RemoteAction - added to queue; RaID: {RaID}", ra.RaID);

                // return ok
                return Accepted(new { success = true, taskID, raID = ra.RaID, docInfo = directionExist.DocumentName, partnerName = directionExist.DocumentDetail2, partnerAddress = directionExist.DocumentDetail3 });
            }
            catch (Exception exc)
            {
                ReportException(actionType, paramJson, exc);
                return StatusCode(500);
            }
        }


        private (ActionTypeEnum? type, Type paramsType, Action<RemoteAction> runAction) GetActionDefs(string actionType)
        {
            switch (actionType)
            {
                case nameof(ActionTypeEnum.TruckLoadUp):
                    return (ActionTypeEnum.TruckLoadUp, typeof(RemoteActionModel_TruckLoadUp), (ra) => TruckLoadUp(ra));
                case nameof(ActionTypeEnum.TruckUnload):
                    return (ActionTypeEnum.TruckUnload, typeof(RemoteActionModel_TruckUnload), (ra) => TruckUnload(ra));
                default:
                    return (null, null, null);
            }
        }


        public void TruckLoadUp(RemoteAction ra)
        {
            // get params & find direction
            var raParams = JsonConvert.DeserializeObject<RemoteActionModel_TruckLoadUp>(ra.ActionParams);
            var direction = GetDirectionByDocumentID(raParams.DocIDSource, raParams.DocIDSource == DocIDSourceEnum.DirectionID ? raParams.DirectionID.ToString() : raParams.DocInfo);
            if (direction == null)
            {
                DirectionNotFound(ra);
                return;
            }

            // run procedure
            var procParams = new ProcedureModels.Exped.ExpedDone()
            {
                DirectionID = direction.DocumentID,
                ExpedTruck = raParams.ExpedTruck
            };
            var procedure = new Procedures.Exped.ExpedDoneProc(ra.SourceUserID);
            var res = procedure.DoProcedure(procParams);

            // update
            UpdateRemoteAction(ra, res.Success, res.ErrorText);
        }


        public void TruckUnload(RemoteAction ra)
        {
            // get params & find direction
            var raParams = JsonConvert.DeserializeObject<RemoteActionModel_TruckUnload>(ra.ActionParams);
            var direction = GetDirectionByDocumentID(raParams.DocIDSource, raParams.DocIDSource == DocIDSourceEnum.DirectionID ? raParams.DirectionID.ToString() : raParams.DocInfo);
            if (direction == null)
            {
                DirectionNotFound(ra);
                return;
            }

            // run procedure
            var procParams = new ProcedureModels.Exped.DirectionSetDeliveredCustomer
            {
                DirectionID = direction.DocumentID,
                DateTimeDeliveredCustomer = raParams.ActionDate
            };

            var procedure = new Procedures.Exped.DirectionSetDeliveredCustomerProc(ra.SourceUserID);
            var res = procedure.DoProcedure(procParams);

            // update
            UpdateRemoteAction(ra, res.Success, res.ErrorText);
        }


        private void DirectionNotFound(RemoteAction ra)
        {
            _logger.LogDebug("RemoteAction - DirectionNotFound; RaID: {RaID}", ra.RaID);

            ra.RunStatus = RemoteAction.RA_STATUS_ERROR;
            ra.ErrorDesc = "Doklad neexistuje";
            ra.Completed = DateTime.Now;

            // save
            _remoteActionDAL.Update(ra);
        }


        private void UpdateRemoteAction(RemoteAction ra, bool success, string errorText)
        {
            if (success)
            {
                ra.RunStatus = RemoteAction.RA_STATUS_DONE;
            }
            else
            {
                ra.RunStatus = RemoteAction.RA_STATUS_ERROR;
                ra.ErrorDesc = errorText;
            }
            ra.Completed = DateTime.Now;

            // save
            _remoteActionDAL.Update(ra);

            _logger.LogDebug("RemoteAction - updated; RaID: {RaID}; success: {success}", ra.RaID, success);
        }


        private void ReportException(string actionType, string requestBody, Exception exc)
        {
            // status data
            var requestId = HttpContext.TraceIdentifier;

            var errorLogHelper = new ErrorLogHelper();
            errorLogHelper.ReportProcException(requestBody, new { actionType, requestId }, exc, "/api/remoteaction error", _logger);
        }


        private DocumentInfo GetDirectionByDocumentID(DocIDSourceEnum docIDSource, string documentID)
        {
            using (var db = ConnectionHelper.GetDB())
            {
                // 1) find by docInfo
                if (docIDSource == DocIDSourceEnum.DocInfo)
                {
                    var docInfoList = new DirectionDAL().FindDirectionsByDocInfo(documentID, null);

                    var docInfo = FilterDocByStatus(docInfoList);

                    if (docInfo != null)
                        return docInfo;
                }

                // 2) find by abraDocID
                if (docIDSource == DocIDSourceEnum.AbraID)
                {
                    var abraDocList = new DirectionDAL().GetDirectionsByAbraDocID(documentID);

                    var abraDoc = FilterDocByStatus(abraDocList);

                    if (abraDoc != null)
                        return abraDoc;
                }

                // 3) find by directionID
                if (docIDSource == DocIDSourceEnum.DirectionID)
                {
                    if (!int.TryParse(documentID, out int directionID)) return null;

                    var directionList = new List<DirectionModel>();
                    var direction = new DirectionDAL().GetDirectionAllData(directionID);
                    if (direction != null) directionList.Add(direction);

                    var docInfo = FilterDocByStatus(directionList);

                    if (docInfo != null)
                        return docInfo;
                }
            }

            return null;
        }

        private DocumentInfo FilterDocByStatus(List<DirectionModel> docList)
        {
            var documentInfos = docList.Where(_ => _.DocDirection == Direction.DOC_DIRECTION_OUT
                        && (_.DocStatus == Direction.DR_STATUS_DISP_CHECK_DONE
                            || _.DocStatus == Direction.DR_STATUS_DELIVERY_PRINT
                            || _.DocStatus == Direction.DR_STATUS_DELIVERY_SHIPMENT_FAILED
                            || _.DocStatus == Direction.DR_STATUS_DELIVERY_MANIFEST
                            || _.DocStatus == Direction.DR_STATUS_NOT_PROCESSED
                            || _.DocStatus == Direction.DR_STATUS_DISP_EXPED))
                                .Select(_ => new DocumentInfo
                                {
                                    DocumentID = _.DirectionID,
                                    DocumentName = _.DocInfo,
                                    DocumentDetail2 = Singleton<DAL.Cache.PartnerCache>.Instance.GetItem(_.PartnerID)?.PartnerName,
                                    DocumentDetail3 = Singleton<DAL.Cache.PartnerAddressCache>.Instance.GetItem(_.PartAddrID)?.AddrName
                                })
                                .ToList();

            if (documentInfos.Count == 1)
                return documentInfos.FirstOrDefault();

            return null;
        }
    }
}

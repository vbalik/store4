﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NPoco;
using S4.DAL;
using S4.Entities.Helpers;
using S4.Entities.Helpers.Xml.Article;
using S4.Entities.Helpers.Xml.Cancellation;
using S4.Entities.Helpers.Xml.Dispatch;
using S4.Entities.Helpers.Xml.Invoice;
using S4.Entities.Helpers.Xml.Receive;
using S4.ProcedureModels.Load;
using S4.StoreCore.Interfaces;
using S4.Web.AccessControl;
using S4.Web.API.External.Helper;

namespace S4.Web.API.External
{
    [ApiController]
    [AllowAnonymous]
    public class ABRAUploadController : ControllerBase
    {
        private static NLog.Logger _nLogger = NLog.LogManager.GetLogger("S4.ABRA.ReadXML");
        private IConfiguration _configuration;
        private readonly IOnStore _onStore;

        public enum UploadStatusEnum
        {
            OK,
            DataError,
            AlreadyExists,
            InternalError
        }

        public class UploadResult
        {
            public UploadStatusEnum Result { get; set; }
            public string Error { get; set; }
        }

        public ABRAUploadController(IConfiguration configurationRoot, IOnStore onStore)
        {
            _configuration = configurationRoot;
            _onStore = onStore;
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("api/external/article/getfirstexpiration")]
        public IActionResult GetFirstExpiration(string id, string sectID)
        {
            //PPG section = "DP", ARG = "VYSK"
            var error = false;
            StringBuilder result = new StringBuilder();

            //Article
            var transArticle = new ArticleDAL();
            var article = transArticle.GetBySourceID(id);

            if (article == null)
            {
                error = true;
                result.Append($"Karta '{id}' nenalezena! ");
            }

            //Section
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var sql = new Sql();
                sql.Where("sectID = @0", sectID);
                var section = db.FirstOrDefault<Entities.Section>(sql);

                if (section == null)
                {
                    error = true;
                    result.Append($"Sekce '{sectID}' nenalezena! ");
                }
            }

            OnStoreItemFull data = new OnStoreItemFull();
            if (error)
            {
                var ret = new
                {
                    Error = true,
                    ErrorMessage = result.ToString(),
                };

                return Ok(ret);
            }
            else
            {
                //Expiration
                data = _onStore.ByArticle(article.ArticleID, ResultValidityEnum.Valid, true, sectID).OrderBy(d => d.ExpirationDate).FirstOrDefault();

                if (data == null)
                    data = new OnStoreItemFull();

                var ret = new
                {
                    Error = false,
                    ErrorMessage = "",
                    ExpirationIsEmpty = !data.ExpirationDate.HasValue,
                    ExpirationDate = data.ExpirationDate.HasValue ? data.ExpirationDate.Value : default(DateTime),
                    ExpirationYear = data.ExpirationDate.HasValue ? data.ExpirationDate.Value.Year.ToString() : "",
                    ExpirationMonth = data.ExpirationDate.HasValue ? data.ExpirationDate.Value.Month.ToString() : "",
                    ExpirationDay = data.ExpirationDate.HasValue ? data.ExpirationDate.Value.Day.ToString() : "",
                    ArticleCode = article.ArticleCode
                };

                return Ok(ret);
            }
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("api/external/article/uploadarticle")]
        public async Task<UploadResult> UploadArticle()
        {
            try
            {
                return ImportXMLArticle(await Request.GetRawBodyBytesAsync());
            }
            catch (Exception ex)
            {
                _nLogger.Error(ex);
                return new UploadResult { Result = UploadStatusEnum.InternalError, Error = ex.Message };
            }

        }

        [AllowAnonymous]
        [HttpPost]
        [Route("api/external/direction/uploaddispatch")]
        public async Task<UploadResult> UploadDispatch()
        {
            try
            {
                return ImportXMLDispatch(await Request.GetRawBodyBytesAsync());
            }
            catch (Exception ex)
            {
                _nLogger.Error(ex);
                return new UploadResult { Result = UploadStatusEnum.InternalError, Error = ex.Message };
            }
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("api/external/direction/uploadreceive")]
        public async Task<UploadResult> UploadReceive()
        {
            try
            {
                return ImportXMLReceive(await Request.GetRawBodyBytesAsync());
            }
            catch (Exception ex)
            {
                _nLogger.Error(ex);
                return new UploadResult { Result = UploadStatusEnum.InternalError, Error = ex.Message };
            }
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("api/external/direction/uploadinvoice")]
        public async Task<UploadResult> UploadInvoice()
        {
            try
            {
                return ImportXMLInvoice(await Request.GetRawBodyBytesAsync());
            }
            catch (Exception ex)
            {
                _nLogger.Error(ex);
                return new UploadResult { Result = UploadStatusEnum.InternalError, Error = ex.Message };
            }
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("api/external/direction/uploadcancel")]
        public async Task<UploadResult> UploadCancel()
        {
            try
            {
                return ImportXMLCancellation(await Request.GetRawBodyBytesAsync());
            }
            catch (Exception ex)
            {
                _nLogger.Error(ex);
                return new UploadResult { Result = UploadStatusEnum.InternalError, Error = ex.Message };
            }
        }

        #region import private

        private UploadResult ImportXMLArticle(byte[] contents)
        {
            string xmlText;
            if (!GetXMLText(contents, out xmlText))
                return new UploadResult() { Result = UploadStatusEnum.InternalError, Error = xmlText };

            var type = "Article";
            //read config
            var pathArchive = GetPathArchive(type);
            var pathError = GetPathError(type);

            if (string.IsNullOrEmpty(pathArchive) || string.IsNullOrEmpty(pathError))
                throw new Exception("Path must be specified!");

            System.Exception exception = null;
            ArticleS3 articleS3 = null;

            var helper = new ExternalHelper(_nLogger);

            _nLogger.Info($"{type} load start");

            if (ArticleS3.Serializer.Deserialize(xmlText, out articleS3, out exception))
            {
                var model = new ArticleLoad()
                {
                    ArticleS3 = articleS3,
                    Xml = xmlText
                };

                var proc = new Procedures.Load.ArticleLoadProc(GetUserID());
                var res = proc.DoProcedure(model);
                
                if (!res.Success)
                {
                    helper.DoError(xmlText, $"ArticleID: {articleS3.ArticleID} - {res.ErrorText}", pathError, articleS3.ArticleID);
                    _nLogger.Info($"{type} load end");
                    return new UploadResult { Result = UploadStatusEnum.InternalError, Error = res.ErrorText };

                }
                else
                    helper.DoProcessed(xmlText, pathArchive, articleS3.ArticleID, $"ArticleID: {articleS3.ArticleID}");

                _nLogger.Info($"{type} load end");
                return new UploadResult { Result = UploadStatusEnum.OK, Error = "" };
            }

            //error parse
            var errorParse = $"{type}: {exception.Message} - {exception.InnerException.Message}";
            helper.DoError(xmlText, $"Error parse {type} : {errorParse}", pathError, "");
            _nLogger.Info($"{type} load end");
            return new UploadResult { Result = UploadStatusEnum.DataError, Error = errorParse };
        }

        private UploadResult ImportXMLReceive(byte[] contents)
        {
            string xmlText;
            if (!GetXMLText(contents, out xmlText))
                return new UploadResult() { Result = UploadStatusEnum.InternalError, Error = xmlText };

            var type = "Receive";
            //read config
            var pathArchive = GetPathArchive(type);
            var pathError = GetPathError(type);

            if (string.IsNullOrEmpty(pathArchive) || string.IsNullOrEmpty(pathError))
                throw new Exception("Path must be specified!");

            System.Exception exception = null;
            ReceiveS3 receiveS3 = null;

            var helper = new ExternalHelper(_nLogger);

            _nLogger.Info($"{type} load start");

            if (ReceiveS3.Serializer.Deserialize(xmlText, out receiveS3, out exception))
            {
                var model = new ReceiveDirLoad()
                {
                    ReceiveS3 = receiveS3,
                    Xml = xmlText
                };

                var proc = new Procedures.Load.ReceiveDirLoadProc(GetUserID());
                var res = proc.DoProcedure(model);

                if (!res.Success)
                {
                    helper.DoError(xmlText, $"DocPrefix/DocNumber: {receiveS3.DocPrefix}/{receiveS3.DocNumber} - {res.ErrorText}", pathError, $"{receiveS3.DocPrefix}{receiveS3.DocNumber}");
                    _nLogger.Info($"{type} load end");

                    if (res.Result == ReceiveDirLoad.ResultEnum.AlreadyLoaded)
                        return new UploadResult { Result = UploadStatusEnum.AlreadyExists, Error = res.ErrorText };
                    else
                        return new UploadResult { Result = UploadStatusEnum.InternalError, Error = res.ErrorText };

                }
                else
                    helper.DoProcessed(xmlText, pathArchive, $"{receiveS3.DocPrefix}{receiveS3.DocNumber}", $"DocPrefix/DocNumber: {receiveS3.DocPrefix}/{receiveS3.DocNumber}");

                _nLogger.Info($"{type} load end");
                return new UploadResult { Result = UploadStatusEnum.OK, Error = "" };
            }

            //error parse
            var errorParse = $"{type}: {exception.Message} - {exception.InnerException.Message}";
            helper.DoError(xmlText, $"Error parse {type} : {errorParse}", pathError, "");
            _nLogger.Info($"{type} load end");
            return new UploadResult { Result = UploadStatusEnum.DataError, Error = errorParse };
        }

        private UploadResult ImportXMLDispatch(byte[] contents)
        {
            string xmlText;
            if (!GetXMLText(contents, out xmlText))
                return new UploadResult() { Result = UploadStatusEnum.InternalError, Error = xmlText };

            var type = "Dispatch";
            //read config
            var pathArchive = GetPathArchive(type);
            var pathError = GetPathError(type);

            if (string.IsNullOrEmpty(pathArchive) || string.IsNullOrEmpty(pathError))
                throw new Exception("Path must be specified!");

            System.Exception exception = null;
            DispatchS3 dispatchS3 = null;

            var helper = new ExternalHelper(_nLogger);

            _nLogger.Info($"{type} load start");

            if (DispatchS3.Serializer.Deserialize(xmlText, out dispatchS3, out exception))
            {
                var model = new DispatchDirLoad()
                {
                    DispatchS3 = dispatchS3,
                    Xml = xmlText
                };

                var proc = new Procedures.Load.DispatchDirLoadProc(GetUserID());
                var res = proc.DoProcedure(model);
                              
                if (!res.Success)
                {
                    helper.DoError(xmlText, $"DocPrefix/DocNumber: {dispatchS3.DocPrefix}/{dispatchS3.DocNumber} - {res.ErrorText}", pathError, $"{dispatchS3.DocPrefix}{dispatchS3.DocNumber}");
                    _nLogger.Info($"{type} load end");

                    if (res.Result == DispatchDirLoad.ResultEnum.AlreadyLoaded)
                        return new UploadResult { Result = UploadStatusEnum.AlreadyExists, Error = res.ErrorText };
                    else
                        return new UploadResult { Result = UploadStatusEnum.InternalError, Error = res.ErrorText };

                }
                else
                    helper.DoProcessed(xmlText, pathArchive, $"{dispatchS3.DocPrefix}{dispatchS3.DocNumber}", $"DocPrefix/DocNumber: {dispatchS3.DocPrefix}/{dispatchS3.DocNumber}");

                _nLogger.Info($"{type} load end");
                return new UploadResult { Result = UploadStatusEnum.OK, Error = "" };
            }

            //error parse
            var errorParse = $"{type}: {exception.Message} - {exception.InnerException.Message}";
            helper.DoError(xmlText, $"Error parse {type} : {errorParse}", pathError, "");
            _nLogger.Info($"{type} load end");
            return new UploadResult { Result = UploadStatusEnum.DataError, Error = errorParse };
        }

        private UploadResult ImportXMLInvoice(byte[] contents)
        {
            string xmlText;
            if (!GetXMLText(contents, out xmlText))
                return new UploadResult() { Result = UploadStatusEnum.InternalError, Error = xmlText };

            var type = "Invoice";
            //read config
            var pathArchive = GetPathArchive(type);
            var pathError = GetPathError(type);

            if (string.IsNullOrEmpty(pathArchive) || string.IsNullOrEmpty(pathError))
                throw new Exception("Path must be specified!");

            System.Exception exception = null;
            InvoiceS3 invoiceS3 = null;

            var helper = new ExternalHelper(_nLogger);

            _nLogger.Info($"{type} load start");

            if (InvoiceS3.Serializer.Deserialize(xmlText, out invoiceS3, out exception))
            {
                var model = new InvoiceLoad()
                {
                    InvoiceS3 = invoiceS3,
                    Xml = xmlText
                };

                var proc = new Procedures.Load.InvoiceLoadProc(GetUserID());
                var res = proc.DoProcedure(model);

                if (!string.IsNullOrEmpty(res.WarningText))
                    _nLogger.Warn(res.WarningText);

                if (!res.Success)
                {
                    helper.DoError(xmlText, $"DocPrefix/DocNumber: {invoiceS3.DocPrefix}/{invoiceS3.DocNumber} - {res.ErrorText}", pathError, $"{invoiceS3.DocPrefix}{invoiceS3.DocNumber}");
                    _nLogger.Info($"{type} load end");

                    return new UploadResult { Result = UploadStatusEnum.InternalError, Error = res.ErrorText };
                }
                else
                    helper.DoProcessed(xmlText, pathArchive, $"{invoiceS3.DocPrefix}{invoiceS3.DocNumber}", $"DocPrefix/DocNumber: {invoiceS3.DocPrefix}/{invoiceS3.DocNumber}");

                _nLogger.Info($"{type} load end");
                return new UploadResult { Result = UploadStatusEnum.OK, Error = "" };
            }

            //error parse
            var errorParse = $"{type}: {exception.Message} - {exception.InnerException.Message}";
            helper.DoError(xmlText, $"Error parse {type} : {errorParse}", pathError, "");
            _nLogger.Info($"{type} load end");
            return new UploadResult { Result = UploadStatusEnum.DataError, Error = errorParse };
        }

        private UploadResult ImportXMLCancellation(byte[] contents)
        {
            string xmlText;
            if (!GetXMLText(contents, out xmlText))
                return new UploadResult() { Result = UploadStatusEnum.InternalError, Error = xmlText };

            var type = "Cancellation";
            //read config
            var pathArchive = GetPathArchive(type);
            var pathError = GetPathError(type);

            if (string.IsNullOrEmpty(pathArchive) || string.IsNullOrEmpty(pathError))
                throw new Exception("Path must be specified!");

            System.Exception exception = null;
            CancellationS3 cancellationS3 = null;

            var helper = new Helper.ExternalHelper(_nLogger);

            _nLogger.Info($"{type} load start");

            if (CancellationS3.Serializer.Deserialize(xmlText, out cancellationS3, out exception))
            {
                var model = new CancellationLoad()
                {
                    CancellationS3 = cancellationS3,
                    Xml = xmlText
                };

                var proc = new Procedures.Load.CancellationLoadProc(GetUserID());
                var res = proc.DoProcedure(model);

                if (!res.Success)
                {
                    helper.DoError(xmlText, $"DocPrefix/DocNumber: {cancellationS3.DocPrefix}/{cancellationS3.DocNumber} - {res.ErrorText}", pathError, $"{cancellationS3.DocPrefix}{cancellationS3.DocNumber}");
                    _nLogger.Info($"{type} load end");
                    return new UploadResult { Result = UploadStatusEnum.InternalError, Error = res.ErrorText };
                }
                else
                    helper.DoProcessed(xmlText, pathArchive, $"{cancellationS3.DocPrefix}{cancellationS3.DocNumber}", $"DocPrefix/DocNumber: {cancellationS3.DocPrefix}/{cancellationS3.DocNumber}");


                _nLogger.Info($"{type} load end");
                return new UploadResult { Result = UploadStatusEnum.OK, Error = "" };
            }

            //error parse
            var errorParse = $"{type}: {exception.Message} - {exception.InnerException.Message}";
            helper.DoError(xmlText, $"Error parse {type} : {errorParse}", pathError, "");
            _nLogger.Info($"{type} load end");
            return new UploadResult { Result = UploadStatusEnum.DataError, Error = errorParse };
        }

        private string GetUserID()
        {
            return _configuration.GetSection($"exchange:read:userID").Value;
        }

        private string GetPathArchive(string type)
        {
            return _configuration.GetSection($"exchange:read:{type.ToLower()}:path:pathArchive").Value;
        }

        private string GetPathError(string type)
        {
            return _configuration.GetSection($"exchange:read:{type.ToLower()}:path:pathError").Value;
        }

        private bool GetXMLText(byte[] contents, out string xmlText)
        {
            var helper = new ExternalHelper(_nLogger);
            return helper.GetXMLText(contents, out xmlText);
        }

        #endregion
    }
}
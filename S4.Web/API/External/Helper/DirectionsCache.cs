﻿using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using S4.Entities;
using S4.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using static S4.Web.API.External.DirectionController;

namespace S4.Web.API.External.Helper
{
    public class DirectionsCache : Core.Cache.CacheBase<List<TopModel>>
    {
        public DirectionsCache() : base(5) //5s
        {

        }

        public List<TopModel> GetList(int docDirection, int records)
        {
            return base.GetOrAddExisting($"{docDirection}_{records}", () =>
            {
                return GetDirections(docDirection, records);
            });
        }

        private List<TopModel> GetDirections(int docDirection, int records)
        {
            var clientDirection = new DAL.DirectionDAL();
            var clientWorker = new DAL.WorkerDAL();
            var directions = clientDirection.ListByDocDirection(docDirection, records);
            var resultList = new List<TopModel>();
            var workers = clientWorker.ListByWorkerClass(Entities.Worker.WorkerClassEnum.Any);

            directions.ForEach(_ =>
            {
                var status = Entities.Direction.Statuses.Items.FindLast(x => x.Code.Equals(_.DocStatus));
                var worker = workers.Where(w => w.WorkerID.Equals(_.EntryUserID)).FirstOrDefault();
                resultList.Add(new TopModel
                {
                    DirectionID = _.DirectionID,
                    DocStatus = status != null ? status.Name : _.DocStatus,
                    DocNumPrefix = _.DocNumPrefix,
                    DocYear = _.DocYear,
                    DocNumber = _.DocNumber,
                    DocDate = _.DocDate,
                    EntryDateTime = _.EntryDateTime,
                    UserName = worker != null ? worker.WorkerName : _.EntryUserID,
                    DocInfo = $"{_.DocNumPrefix}/{_.DocYear}/{_.DocNumber}",
                });
            });

            return resultList;
        }
    }
}

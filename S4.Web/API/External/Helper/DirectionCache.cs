﻿using S4.Core;
using S4.Entities;
using S4.Procedures;
using S4.StoreCore.Interfaces;
using S4.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using static S4.Web.API.External.DirectionController;

namespace S4.Web.API.External.Helper
{
    public class DirectionCache : Core.Cache.CacheBase<DirectionModel>
    {
        public DirectionCache() : base(5) //5s
        {

        }

        public DirectionModel GetItem(int directionID)
        {
            return base.GetOrAddExisting(directionID.ToString(), () =>
            {
                return GetDirection(directionID);
            });
        }

        private DirectionModel GetDirection(int directionID)
        {
            var clientDirection = new DAL.DirectionDAL();
            var clientWorker = new DAL.WorkerDAL();
            var direction = clientDirection.GetDirectionAllData(directionID);

            if (direction == null)
                return null;

            var worder = clientWorker.Get(direction.EntryUserID);

            //Items
            var rowsDirectionItems = from c in direction.Items
                                     where c.ItemStatus != DirectionItem.DI_STATUS_CANCEL
                                     orderby c.ArticleCode
                                     group c by c.ArtPackID into g
                                     select new { ArtPackID = g.Key, Other = g.ToList() };

            var items = new List<DirectionModelItem>();
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var save = Singleton<ServicesFactory>.Instance.GetSave();
                List<MoveResult> moveResult = null;

                if (direction.DocDirection == -1)
                {
                    var dispatchPositions = Singleton<DAL.Cache.PositionByCategoryModelCache>.Instance.GetItem(Entities.Position.PositionCategoryEnum.Dispatch);
                    moveResult = save.MoveResult_ByDirection(db, direction.DirectionID, StoreMove.MO_DOCTYPE_DISPATCH, (mi) => dispatchPositions.PositionIDs.Contains(mi.PositionID));
                }
                else
                    moveResult = save.MoveResult_ByDirection(db, direction.DirectionID, StoreMove.MO_DOCTYPE_RECEIVE, null);

                foreach (var directionItem in rowsDirectionItems)
                {
                    var articlePacking = Singleton<DAL.Cache.ArticlePackingCache>.Instance.GetItem(directionItem.ArtPackID);
                    var articleModel = Singleton<DAL.Cache.ArticleModelCache>.Instance.GetItem(articlePacking.ArticleID);

                    //expirace, šarže 
                    var mrList = (from m in moveResult where m.ArtPackID == directionItem.ArtPackID select m).ToList();
                    if (mrList != null)
                    {
                        foreach (var mr in mrList)
                        {
                            var serialNumbers = (new DAL.StoreMoveSerialsDAL()).GetStoreMoveSerialsByStoMoveItemID(mr.StoMoveItemID).Select(_ => _.SerialNumber).ToList();
                            items.Add(new DirectionModelItem
                            {
                                BatchNum = mr.BatchNum,
                                CarrierNum = mr.CarrierNum,
                                ExpirationDate = mr.ExpirationDate,
                                Quantity = mr.Quantity,
                                ArticleCode = articleModel.ArticleCode,
                                ArticleName = articleModel.ArticleDesc,
                                SerialsNumber = serialNumbers
                            });
                        }
                    }


                }
            }

            var result = new DirectionModel
            {
                Detail = new TopModel
                {
                    DirectionID = direction.DirectionID,
                    DocStatus = Entities.Direction.Statuses.Items.FindLast(x => x.Code.Equals(direction.DocStatus)).Name,
                    DocNumPrefix = direction.DocNumPrefix,
                    DocYear = direction.DocYear,
                    DocNumber = direction.DocNumber,
                    DocDate = direction.DocDate,
                    EntryDateTime = direction.EntryDateTime,
                    UserName = worder.WorkerName,
                    DocInfo = $"{direction.DocNumPrefix}/{direction.DocYear}/{direction.DocNumber}"
                },
                Items = items
            };
                       
            return result;
        }
    }
}

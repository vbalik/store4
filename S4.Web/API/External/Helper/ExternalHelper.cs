﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace S4.Web.API.External.Helper
{
    public class ExternalHelper
    {
        private NLog.Logger _nLogger;

        public ExternalHelper(NLog.Logger nLogger)
        {
            _nLogger = nLogger;
        }

        public void DoError(string xml, string errorMsg, string pathError, string header)
        {
            _nLogger.Info(errorMsg);
            var fileName = GetFileName(header);

            // create file
            //create new dir
            var newDir = string.Format($"{pathError}{DateTime.Now:yyyy}\\{DateTime.Now:MMdd}");
            if (!Directory.Exists(newDir))
                Directory.CreateDirectory(newDir);

            var path = $@"{newDir}\\{fileName}";
            var fp = CheckFilePath(path);

            System.IO.File.WriteAllText(fp, xml);
            _nLogger.Info($"{fp} created");
        }

        public void DoProcessed(string xml, string pathArchive, string toFileName, string result)
        {
            var fileName = GetFileName(toFileName);
            _nLogger.Info($"{fileName} created. {result}");

            // create file
            //create new dir
            var newDir = string.Format($"{pathArchive}{DateTime.Now:yyyy}\\{DateTime.Now:MMdd}");
            if (!Directory.Exists(newDir))
                Directory.CreateDirectory(newDir);

            var path = $"{newDir}\\{fileName}";
            var fp = CheckFilePath(path);
            System.IO.File.WriteAllText(fp, xml);
        }

        public bool GetXMLText(byte[] contents, out string xmlText)
        {
            try
            {
                Encoding encoding;
                using (var stream = new MemoryStream(contents))
                {
                    using (var xmlreader = new XmlTextReader(stream))
                    {
                        xmlreader.MoveToContent();
                        encoding = xmlreader.Encoding;
                    }
                }

                xmlText = encoding.GetString(contents);
                return true;
            }
            catch (Exception exc)
            {
                _nLogger.Error(exc);
                xmlText = exc.Message;
                return false;
            }
        }

        #region private

        
        private string CheckFilePath(string filePath)
        {
            if (!File.Exists(filePath))
                return filePath;

            var guid = Guid.NewGuid();
            var fileName = Path.GetFileNameWithoutExtension(filePath);
            var extension = Path.GetExtension(filePath);
            var directoryName = Path.GetDirectoryName(filePath);

            return $"{directoryName}\\{fileName}_{guid}{extension}";

        }

        private string GetFileName(string toFileName)
        {
            toFileName = string.IsNullOrWhiteSpace(toFileName) ? "" : $"_{toFileName}";
            return $"{DateTime.Now.ToString("yyyyMMddHHmmss")}{toFileName}.xml";
        }

        #endregion

    }
}

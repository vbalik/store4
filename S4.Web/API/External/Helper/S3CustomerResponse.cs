﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace S4.Web.API.External.Helper
{
    [XmlRoot(ElementName = "s3_customer_response")]
    public class S3CustomerResponse
    {

        [XmlElement(ElementName = "s4_customer_id")]
        public int S4CustomerId { get; set; }

        [XmlElement(ElementName = "delivery_addresses")]
        public DeliveryAddresses DeliveryAddresses { get; set; }

        [XmlAttribute(AttributeName = "customer_id")]
        public string CustomerId { get; set; }

    }

    [XmlRoot(ElementName = "delivery_addresses")]
    public class DeliveryAddresses
    {

        [XmlElement(ElementName = "delivery_address")]
        public List<DeliveryAddress> DeliveryAddressList { get; set; }
    }

    [XmlRoot(ElementName = "delivery_address")]
    public class DeliveryAddress
    {

        [XmlElement(ElementName = "s4_delivery_address_id")]
        public int S4DeliveryAddressId { get; set; }

        [XmlAttribute(AttributeName = "address_id")]
        public string AddressId { get; set; }

    }
}

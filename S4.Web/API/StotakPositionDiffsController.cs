﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NPoco;

using S4.Entities.Models;
using S4.Web.DataList;
using S4.Web.Models;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/StotakPositionDiffs")]
    public class StotakPositionDiffsController : DataListController<StotakPositionDifferences, StotakPositionDiffsSettings, StotakPositionDiffsSettingsModel>
    {
        public StotakPositionDiffsController(IHttpContextAccessor httpContextAccessor, ILogger<DataListController<StotakPositionDifferences, StotakPositionDiffsSettings, StotakPositionDiffsSettingsModel>> logger) : base(httpContextAccessor, logger)
        {

        }

        protected override Sql GetSql(StotakPositionDiffsSettingsModel settings)
        {
            var query =
                "SELECT S.*, (quantStTa - quantOnSt) AS quantDiff, P.posCode, P.houseID, A.articleID, A.articleCode, A.articleDesc, A.packDesc FROM\n"
                + "(\n"
                + "SELECT positionID, artPackID, SUM(quantOnSt) AS quantOnSt, SUM(quantStTa) AS quantStTa\n"
                + "FROM\n"
                + "(SELECT positionID, artPackID, SUM(quantity) AS quantOnSt, 0 AS quantStTa\n"
                + "FROM dbo.s4_stocktaking_snapshots\n"
                + "WHERE(stotakID = @0) AND(snapshotClass = @1)\n"
                + "GROUP BY positionID, artPackID\n"
                + "UNION ALL\n"
                + "SELECT positionID, artPackID, 0 as quantOnSt, SUM(quantity) AS quantStTa\n"
                + "FROM dbo.s4_stocktaking_items\n"
                + "WHERE(stotakID = @0)\n"
                + "GROUP BY positionID, artPackID) Derived GROUP BY positionID, artPackID\n";
            
            query += settings.IncludeNoDiff ? ") S\n" : "HAVING ISNULL(SUM(quantOnSt), 0) != ISNULL(SUM(quantStTa), 0)) S\n";
            
            query += "LEFT OUTER JOIN s4_positionList P ON P.positionID = S.positionID\n"
                + "LEFT OUTER JOIN s4_vw_article_base A ON A.artPackID = S.artPackID";
            var sql = new NPoco.Sql(query, settings.StotakID, settings.SnapshotClass);

            return sql;
        }
    }
}

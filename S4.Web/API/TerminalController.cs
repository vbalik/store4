﻿
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using S4.Core;
using S4.Entities.Helpers;
using S4.Web.Hubs.Terminal;
using S4.Web.Models;
using System.Security.Claims;

namespace S4.Web.API
{
    [Route("api/terminal")]
    [Produces("application/json")]
    [ApiController]
    public class TerminalController : Controller
    {
        public string SaveBaseDir { get; internal set; }

        public ILogger<TerminalController> _logger;
        private readonly IConfiguration _configuration;

        public TerminalController(ILogger<TerminalController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
            SaveBaseDir = _configuration.GetSection("UserReportsFolder").Value;
            if(!string.IsNullOrEmpty(SaveBaseDir) && !Directory.Exists(SaveBaseDir))
                Directory.CreateDirectory(SaveBaseDir);
        }

        [HttpGet]
        [Route("menuitems")]
        [AllowAnonymous]
        public List<TerminalMenuItem> MenuItems()
        {
            // try to get settings
            var menuItemsJson = Singleton<DAL.SettingsSet>.Instance.GetValue_String(Entities.SettingsDictionary.TERMINAL_MENU_ITEMS);
            if (!String.IsNullOrWhiteSpace(menuItemsJson))
            {
                var menuItems = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TerminalMenuItem>>(menuItemsJson);
                return menuItems;
            }
            else
                return new List<TerminalMenuItem>()
                {
                    new TerminalMenuItem() { Group = "Příjem", Text = "Volný příjem", Image = "arrowcirclerightsolid.png", PageName = "Pages.Direction.Receive.ReceivePage" },
                    new TerminalMenuItem() { Group = "Příjem", Text = "Naskladnění", Image = "arrowcirclerightsolid.png", PageName = "Pages.Direction.StoreIn.StoreInPage", JobID = Entities.DirectionAssign.JOB_ID_STORE_IN },
                    new TerminalMenuItem() { Group = "Výdej", Text = "Vychystání", Image = "arrowcircleleftsolid.png", PageName = "Pages.Direction.Dispatch.DispatchPage", JobID = Entities.DirectionAssign.JOB_ID_DISPATCH },
                    new TerminalMenuItem() { Group = "Výdej", Text = "Kontrola", Image = "taskssolid.png", PageName = "Pages.Direction.DispatchCheck.DispatchCheckPage", JobID = Entities.DirectionAssign.JOB_ID_DISPATCH_CHECK },
                    new TerminalMenuItem() { Group = "Výdej", Text = "Expedice", Image = "truckloadingsolid.png", PageName = "Pages.Direction.Exped.ExpeditionPage" },
                    new TerminalMenuItem() { Group = "Ukázat", Text = "Obsah pozice", Image = "stackexchangebrands.png", PageName = "Pages.OnStock.OnPositionPage" },
                    new TerminalMenuItem() { Group = "Ukázat", Text = "Obsah palety", Image = "stackexchangebrands.png", PageName = "Pages.OnStock.OnCarrierPage" },
                    new TerminalMenuItem() { Group = "Ukázat", Text = "Ukázat kartu", Image = "cubesolid.png", PageName = "Pages.Article.ShowArticlePage" },
                    new TerminalMenuItem() { Group = "Ukázat", Text = "Skladem", Image = "warehousesolid.png", PageName = "Pages.OnStock.ArtOnStockPage" },
                    new TerminalMenuItem() { Group = "Servis", Text = "Vratky", Image = "arrowcirclerightsolid.png", PageName = "Pages.Direction.Receive.ReceivePage", PageParams = "VRAT" },
                    new TerminalMenuItem() { Group = "Servis", Text = "Inventura", Image = "questioncirclesolid.png", PageName = "Pages.StockTaking.StockTakingListPage" },
                    new TerminalMenuItem() { Group = "Servis", Text = "Přeskladnění", Image = "arrowsaltsolid.png", PageName = "Pages.Direction.StoreMove.StoreMovePage" },
                    new TerminalMenuItem() { Group = "Servis", Text = "Nastavení kódů", Image = "cog.png", PageName = "Pages.Article.SetArticlePage" },
                    new TerminalMenuItem() { Group = "Servis", Text = "Tisk paletových štítků", Image = "print.png", PageName = "Pages.Print.PrintBarcodeCarrierLabelsPage" },
                    new TerminalMenuItem() { Group = "Servis", Text = "Nahlásit chybu", Image = "warning2.png", PageName = "Pages.ReportError.ReportErrorPage" }
                };
        }

        [HttpGet]
        [Route("get-default-settings")]
        [AllowAnonymous]
        public TerminalDefaultSettings GetDefaultSettings()
        {
            return new TerminalDefaultSettings
            {
                EnableAssignDirectionUser = Singleton<DAL.SettingsSet>.Instance.GetValue_Boolean(Entities.SettingsDictionary.TERMINAL_ENABLE_ASSIGN_DIRECTION_USER),
                BarcodeDisallowedValuePattern = Singleton<DAL.SettingsSet>.Instance.GetValue_String(Entities.SettingsDictionary.TERMINAL_BARCODE_DISALLOWED_VALUE_PATTERN),
                DispatchDefaultPositionID = Singleton<DAL.SettingsSet>.Instance.GetValue_Int32(Entities.SettingsDictionary.DISPATCH_DEFAULT_POSITION_ID),
                ShowSpecFeaturesAlertInfoList = Singleton<DAL.SettingsSet>.Instance.GetValue_Boolean(Entities.SettingsDictionary.SHOW_SPEC_FEATURES_ALERT_INFO_LIST),
            };
        }

        [HttpPost]
        [Route("connections")]
        [AllowAnonymous]
        public List<TerminalConnectionInfo> Connections()
        {
            return Hubs.Terminal.TerminalHub.Connections();
        }


        [HttpPost]
        [Route("infototerminal")]
        [AllowAnonymous]
        public async Task InfoToTerminal([FromBody]string message)
        {
            _logger.LogInformation("InfoToTerminal; '{message}'; IP: {RemoteIpAddress}; User: {Value}", message, Request.HttpContext.Connection.RemoteIpAddress, Request.HttpContext.User.FindFirst(ClaimTypes.Sid)?.Value);

            var terminalHub = Startup.GetService<IHubContext<TerminalHub, ITerminalHub>>();
            var sender = new TerminalSender(terminalHub);
            await sender.InfoToTerminal(message);
        }


        [HttpPost]
        [Route("reporterror")]
        [AllowAnonymous]
        public async Task<IActionResult> ReportError([FromBody] TerminalReportErrorModel model)
        {
            try
            {
                await SaveToFileAsync(model);
                return Json(new
                {
                    Success = true
                });
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
            }

            return Json(new
            {
                Success = false
            });
        }


        private async Task SaveToFileAsync(TerminalReportErrorModel terminalReportError)
        {
            if (terminalReportError.Logs.Count == 0) return;

            var fileName = String.Format("log_{0:yyyyMMdd_HHmmss}_{1}.txt", terminalReportError.AppInfo.Created, terminalReportError.AppInfo.DeviceID);

            string baseDir;
            if (SaveBaseDir == null)
            {
                baseDir = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
                if (String.IsNullOrEmpty(baseDir))
                    baseDir = System.IO.Path.GetTempPath();
            }
            else
            {
                baseDir = SaveBaseDir;
            }
            var dir = System.IO.Path.Combine(baseDir, "TerminalLogs");

            if (!System.IO.Directory.Exists(dir))
                System.IO.Directory.CreateDirectory(dir);
            var path = System.IO.Path.Combine(dir, fileName);

            using (var file = System.IO.File.AppendText(path))
            {
                // add report info
                await file.WriteLineAsync($"# Terminal error report created {terminalReportError.AppInfo.Created:yyyy-MM-dd HH:mm:ss.ffffff}" +
                    $" |UserID: {terminalReportError.AppInfo.UserID}" +
                    $" |Version: {terminalReportError.AppInfo.Version} " +
                    $" |OS_ver: {terminalReportError.AppInfo.OS_ver}" +
                    $" |MONO_ver: {terminalReportError.AppInfo.MONO_ver}");

                foreach (var log in terminalReportError.Logs)
                    await file.WriteLineAsync($"{log.Created:yyyy-MM-dd HH:mm:ss.ffffff}|{log.ID}|Message: {log.Message} |Parameters: {log.Parameters}");
                file.Flush();
            }

            _logger.LogWarning("Received user error report. Saved to file: {0}", path);
        }
    }
}
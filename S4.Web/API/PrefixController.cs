﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Net.Http;

using S4.Entities;
using S4.Web.Models;
using S4.Web.DataList;
using NPoco;
using S4.Entities.Views;
using Microsoft.Extensions.Logging;
using S4.Web.API.Helper;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/prefix")]
    [ExportName("Prefixy")]
    public class PrefixController : DataListController<Entities.Views.PrefixView, PrefixSettings, PrefixSettingsModel>
    {
        public PrefixController(IHttpContextAccessor httpContextAccessor, ILogger<DataListController<PrefixView, PrefixSettings, PrefixSettingsModel>> logger)
        : base(httpContextAccessor, logger)
        {

        }

        protected override Sql GetSql(PrefixSettingsModel settings)
        {
            var sql = base.GetSql(settings);
            sql.Select("p.*, s.sectDesc");
            sql.From("s4_prefixList p");
            sql.LeftJoin("s4_sectionList s on s.sectID = p.prefixSection");
            
            return sql;
        }

        protected override PrefixView SaveItem(PrefixView item)
        {
            if (item.IsNew)
            {
                using (var db = Core.Data.ConnectionHelper.GetDB())
                {
                    if (db.Exists<Prefix>(new { docClass = item.DocClass, docNumPrefix = item.DocNumPrefix }))
                        throw new UserException($"Položka \"{item.DocClass}/{item.DocNumPrefix}\" již existuje!");
                }
            }

            if (CheckDataValidity(item))
            {
                Prefix prefix = new Prefix()
                {
                    DocClass = item.DocClass,
                    DocDirection = item.DocDirection,
                    DocNumPrefix = item.DocNumPrefix,
                    DocType = item.DocType,
                    PrefixDesc = item.PrefixDesc,
                    PrefixEnabled = item.PrefixEnabled,
                    PrefixSection = item.PrefixSection,
                    PrefixSettings = item.PrefixSettings
                };

                using (var db = Core.Data.ConnectionHelper.GetDB())
                    db.Save(prefix);

            }

            return item;
        }
    }
}

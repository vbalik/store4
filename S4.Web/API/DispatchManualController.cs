﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using S4.Core.Cache;
using S4.Core.Data;
using S4.DAL;
using S4.Entities;
using S4.Entities.Models;
using S4.StoreCore.Interfaces;
using S4.Web.Helpers;
using S4.Web.Models;


namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/dispatchmanual")]
    public class DispatchManualController : Controller
    {
        private readonly DirectionDAL _directionDal;
        private readonly ICache<ArticlePacking, int> _articlePackingCache;
        private readonly PositionDAL _positionDal;
        private readonly ICache<Position, int> _positionCache;
        private readonly ICache<Partner, int> _partnerCache;
        private readonly ICache<ArticleModel, int> _articleModelCache;
        private readonly IOnStore _onStore;

        public DispatchManualController(DirectionDAL directionDal, ICache<Partner, int> partnerCache,
            ICache<ArticleModel, int> articleModelCache, ICache<ArticlePacking, int> articlePackingCache,
            PositionDAL positionDal,
            ICache<Position, int> positionCache, IOnStore onStore)
        {
            _directionDal = directionDal;
            _articlePackingCache = articlePackingCache;
            _positionDal = positionDal;
            _positionCache = positionCache;
            _partnerCache = partnerCache;
            _articleModelCache = articleModelCache;
            _onStore = onStore;
        }

        [HttpPost]
        [Route("get-direction-items")]
        public IActionResult GetDirectionItems([FromBody] JObject data)
        {
            var direction = _directionDal.GetDirectionAllData(data["directionID"].ToObject<int>());

            if (direction == null)
                return NotFound();

            var proxy = new EmptyDataProxy();
            
            var itemsFullInfo = new List<DirectionManualItemModel>();
            
            foreach (var item in direction.Items)
            {
                var pack = _articlePackingCache.GetItem(item.ArtPackID);
                if (pack == null)
                    throw new Exception($"Article ID {item.ArtPackID} not found");

                var article = _articleModelCache.GetItem(pack.ArticleID);

                var dmItem = new DirectionManualItemModel
                {
                    DirectionItemID = item.DirectionItemID,
                    DirectionID = item.DirectionID,
                    ArticleID = article.ArticleID,
                    ArtPackID = item.ArtPackID,
                    ArticleCode = item.ArticleCode,
                    ArticleDesc = article.ArticleDesc,
                    PackDesc = pack.PackDesc,
                    Quantity = item.Quantity,
                    UseBatch = article.UseBatch,
                    UseExpiration = article.UseExpiration,
                    DocPosition = item.DocPosition
                };

                proxy.ToShow(dmItem.BatchNum, StoreMoveItem.EMPTY_BATCHNUM, value => dmItem.BatchNum = value);
                proxy.ToShow(dmItem.ExpirationDate, StoreMoveItem.EMPTY_EXPIRATION, value => dmItem.ExpirationDate = value);

                itemsFullInfo.Add(dmItem);
            }

            // get position
            Position position = null;
            var positionID = (int?) direction.XtraData[Direction.XTRA_DISPATCH_POSITION_ID];
            if (positionID.HasValue)
                position = _positionCache.GetItem(positionID.Value);

            var model = new DirectionManualModel
            {
                DirectionID = direction.DirectionID,
                DocInfo = direction.DocInfo,
                PartnerName = _partnerCache.GetItem(direction.PartnerID)?.PartnerName,
                Items = itemsFullInfo,
                Position = position,
                BadStatus = direction.DocStatus != Direction.DR_STATUS_DISPATCH_MANUAL 
            };

            return Json(model);
        }

        [HttpPost]
        [Route("get-dispatched-items")]
        public IActionResult GetDispatchedItems([FromBody] JObject data)
        {
            var direction = _directionDal.GetDirectionAllData(data["directionID"].ToObject<int>());
            if (direction == null)
                return NotFound();

            // get position
            var positionID = (int?) direction.XtraData[Direction.XTRA_DISPATCH_POSITION_ID];
            if (!positionID.HasValue)
                throw new Exception($"Position ID not found");

            var items = _onStore.ScanSMPositionDirection(positionID.Value, direction.DirectionID, false);

            var proxy = new EmptyDataProxy();
            
            var itemsFullInfo = new List<DirectionManualItemModel>();
            
            foreach (var item in items)
            {
                // get article
                var pack = _articlePackingCache.GetItem(item.ArtPackID);
                if (pack == null)
                    throw new Exception($"Article ID {item.ArtPackID} not found");

                var article = _articleModelCache.GetItem(pack.ArticleID);

                var dmItem = new DirectionManualItemModel
                {
                    DirectionID = direction.DirectionID,
                    ArticleID = article.ArticleID,
                    ArtPackID = item.ArtPackID,
                    ArticleCode = article.ArticleCode,
                    ArticleDesc = article.ArticleDesc,
                    PackDesc = pack.PackDesc,
                    Quantity = item.Quantity,
                    BatchNum = item.BatchNum,
                    UseBatch = article.UseBatch,
                    UseExpiration = article.UseExpiration,
                    ExpirationDate = item.ExpirationDate
                };
                
                proxy.ToShow(dmItem.BatchNum, StoreMoveItem.EMPTY_BATCHNUM, value => dmItem.BatchNum = value);
                proxy.ToShow(dmItem.ExpirationDate, StoreMoveItem.EMPTY_EXPIRATION, value => dmItem.ExpirationDate = value);

                itemsFullInfo.Add(dmItem);
            }

            return Json(itemsFullInfo);
        }

        [HttpPost]
        [Route("check-item-on-position")]
        public IActionResult CheckItemOnPosition([FromBody] JObject data)
        {
            var positions = _positionDal.ScanByPosCode(data["positionCode"].ToObject<string>());

            if (positions.Count == 0)
                return Json(new
                {
                    availableQuantity = (decimal?) null
                });

            var onPositionItems =
                new StoreCore.OnStore_Basic.OnStore().OnPosition(positions.Select(_ => _.PositionID).ToList(),
                    ResultValidityEnum.Valid);

            var artPackID = data["artPackID"].ToObject<int>();

            var availableQuantity = onPositionItems.Where(item => item.ArtPackID == artPackID).Sum(item => item.Quantity);

            return Json(new
            {
                availableQuantity = availableQuantity
            });
        }

        [HttpPost]
        [Route("check-item-parameters")]
        public IActionResult CheckItemParameters([FromBody] JObject data)
        {
            var positionCode = data["positionCode"].ToObject<string>();
            var artPackID = data["artPackID"].ToObject<int>();
            var carrierNum = data["carrierNum"].ToObject<string>();
            var batchNum = data["batchNum"].ToObject<string>();
            DateTime? expirationDate = null;
            try
            {
                expirationDate = data["expirationDate"].ToObject<DateTime?>();
            }
            catch (FormatException)
            {
            }

            decimal.TryParse(data["quantity"].ToObject<string>(), out var quantity);

            var positions = _positionDal.ScanByPosCode(positionCode);

            if (positions.Count == 0)
                return Json(new
                {
                    isValid = false
                });

            var onPositionItems =
                new StoreCore.OnStore_Basic.OnStore().OnPosition(positions.Select(_ => _.PositionID).ToList(),
                    ResultValidityEnum.Valid);

            // filter item by ArtPackID
            var onPositionItemsFiltered = onPositionItems.Where(item => item.ArtPackID == artPackID).ToList();
            var onPositionItemsQuery = onPositionItemsFiltered.AsQueryable();
            var firstItem = onPositionItemsFiltered.FirstOrDefault();

            if (!string.IsNullOrEmpty(carrierNum))
                onPositionItemsQuery = onPositionItemsQuery.Where(item => item.CarrierNum == carrierNum);

            if (firstItem.Article.UseBatch)
                onPositionItemsQuery = onPositionItemsQuery.Where(item => item.BatchNum == batchNum);

            if (firstItem.Article.UseExpiration)
                onPositionItemsQuery = onPositionItemsQuery
                    .Where(item => item.ExpirationDate.GetValueOrDefault().ToString("yyyy-MM-dd") == (expirationDate ?? DateTime.Now).ToString("yyyy-MM-dd"));

            OnStoreItemFull itemFound = null;

            try
            {
                itemFound = onPositionItemsQuery.SingleOrDefault();
            }
            catch (InvalidOperationException)
            {
                return Json(new
                {
                    isValid = false,
                    errorMessage = "Nalezeno více položek"
                });
            }

            // check validity against itemFound
            var isValid = itemFound?.Position.PosCode == positionCode.ToUpper();
            var isQuantityValid = true;
            var isBatchNumValid = true;
            var isExpirationDateValid = true;

            if (itemFound == null || quantity > itemFound?.Quantity)
            {
                isValid = false;
                isQuantityValid = false;
            }

            if (itemFound == null || (itemFound.Article.UseBatch && itemFound?.BatchNum != batchNum))
            {
                isValid = false;
                isBatchNumValid = false;
            }

            if (itemFound == null
                || (itemFound.Article.UseExpiration
                    && itemFound.ExpirationDate?.ToString("yyyy-MM-dd") != expirationDate?.ToString("yyyy-MM-dd")))
            {
                isValid = false;
                isExpirationDateValid = false;
            }

            return Json(new
            {
                positionID = itemFound?.Position.PositionID,
                isValid = isValid,
                isQuantityValid = isQuantityValid,
                isBatchNumValid = isBatchNumValid,
                isExpirationDateValid = isExpirationDateValid
            });
        }
    }
}
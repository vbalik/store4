﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Net.Http;

using S4.Entities;
using S4.Web.Models;
using S4.Web.DataList;
using NPoco;
using Microsoft.Extensions.Logging;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/error")]
    public class ErrorController : DataListController<ClientError, ClientErrorSettings, ClientErrorSettingsModel>
    {
        public ErrorController(IHttpContextAccessor httpContextAccessor, ILogger<DataListController<ClientError, ClientErrorSettings, ClientErrorSettingsModel>> logger) : base(httpContextAccessor, logger)
        {

        }

        protected override Sql GetSql(ClientErrorSettingsModel settings)
        {
            var sql = base.GetSql(settings);
            if (settings.ClientErrorID != 0)
            {
                sql.Where("clientErrorID = @0", settings.ClientErrorID);
            }
            return sql;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Net.Http;

using S4.Entities;
using S4.Web.Models;
using S4.Web.DataList;
using NPoco;
using Microsoft.Extensions.Logging;
using S4.Entities.Views;
using SQLitePCL;
using S4.DAL;
using S4.ProcedureModels.Booking;
using Org.BouncyCastle.Asn1.Tsp;
using S4.Entities.Helpers.Xml.Article;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/articlebooking")]
    public class ArticleBookingController : DataListController<BookingView, ArticleBookingSettings, ArticleBookingSettingsModel>
    {
        internal static TimeZoneInfo TzInfo = TimeZoneInfo.FindSystemTimeZoneById("Central European Standard Time");

        public ArticleBookingController(IHttpContextAccessor httpContextAccessor, ILogger<DataListController<BookingView, ArticleBookingSettings, ArticleBookingSettingsModel>> logger) : base(httpContextAccessor, logger)
        {

        }

        protected override Sql GetSql(ArticleBookingSettingsModel model)
        {
            var sql = base.GetSql(model);
            sql = new Sql();
            sql.Select("b.*, l.batchNum, a.articleCode, a.articleDesc, p.partnerName, dir.docNumPrefix, dir.docYear, dir.docNumber, w1.workerName as entryUserName, w2.workerName as deletedByEntryUserName");
            sql.From("s4_vw_article_base a");
            sql.LeftJoin("s4_storeMove_lots l on l.artPackID = a.artPackID");
            sql.LeftJoin("s4_bookings b on b.stoMoveLotID = l.stoMoveLotID");
            sql.LeftJoin("s4_direction dir on dir.directionID = b.usedInDirectionID");
            sql.LeftJoin("s4_partnerList p on p.partnerID = b.partnerID");
            sql.LeftJoin("s4_workerList w1 on w1.workerID = b.entryUserID");
            sql.LeftJoin("s4_workerList w2 on w2.workerID = b.deletedByEntryUserID");
            sql.Where("a.articleID = @0 AND b.bookingID IS NOT NULL ", model.ArticleID);

            if (model.BookingValid)
            {
                sql.Where("b.bookingStatus = @0", Booking.VALID);
            }

            return sql;
        }

    }

}




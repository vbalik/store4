﻿using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using S4.Core.Cache;
using S4.Entities.Models;
using S4.Web.AccessControl;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/storein")]
    [Authorize]
    public class StoreInController : Controller
    {
        public readonly ICache<Entities.ArticlePacking, int> _articlePackingCache;
        public readonly ICache<ArticleModel, int> _articleModelCache;

        public StoreInController(ICache<Entities.ArticlePacking, int> articlePackingCache, ICache<ArticleModel, int> articleModelCache)
        {
            _articlePackingCache = articlePackingCache;
            _articleModelCache = articleModelCache;
        }

        protected string GetUserID()
        {
            if (Request.HttpContext?.User.Identity.IsAuthenticated == true)
                return Request.HttpContext.User.FindFirst(ClaimTypes.Sid)?.Value;

            return Request.HttpContext.Request.Headers[ApiAuthenticationMiddleware.API_USER_ID].ToString();
        }
    }
}

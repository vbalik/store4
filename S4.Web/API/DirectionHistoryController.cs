﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using NPoco;

using S4.Entities;
using S4.Web.Models;
using S4.Web.DataList;
using Microsoft.Extensions.Logging;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/DirectionHistory")]
    public class DirectionHistoryControler : DataListController<DirectionHistory, DirectionHistorySettings, DirectionHistorySettingsModel>
    {
        public DirectionHistoryControler(IHttpContextAccessor httpContextAccessor, ILogger<DataListController<DirectionHistory, DirectionHistorySettings, DirectionHistorySettingsModel>> logger) : base(httpContextAccessor, logger)
        {

        }

        protected override Sql GetSql(DirectionHistorySettingsModel settings)
        {
            var sql = base.GetSql(settings);
            if (settings.DirectionID != 0)
            {
                sql.Where("directionID = @0", settings.DirectionID);
            }
            return sql;
        }
    }
}

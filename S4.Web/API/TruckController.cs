﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NPoco;
using S4.Entities;
using S4.Web.API.Helper;
using S4.Web.DataList;
using S4.Web.Models;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/Truck")]
    [ExportName("Auta")]
    public class TruckController : DataListController<Truck, TruckSettings, TruckSettingsModel>
    {
        public TruckController(IHttpContextAccessor httpContextAccessor, ILogger<DataListController<Truck, TruckSettings, TruckSettingsModel>> logger)
        : base(httpContextAccessor, logger)
        {
           
        }

        protected override Truck SaveItem(Truck item)
        {
            if (item.TruckID == 0)
            {
                using (var db = Core.Data.ConnectionHelper.GetDB())
                {
                    if (db.SingleOrDefault<Truck>("where truckRegist = @0", item.TruckRegist) != null)
                        throw new UserException($"Položka \"{item.TruckRegist}\" již existuje!");
                }
            }

            return base.SaveItem(item);
        }
    }
}

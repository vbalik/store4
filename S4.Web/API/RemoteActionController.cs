﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Net.Http;

using S4.Entities;
using S4.Web.Models;
using S4.Web.DataList;
using NPoco;
using Microsoft.Extensions.Logging;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/remoteaction")]
    public class RemoteActionController : DataListController<RemoteAction, RemoteActionSettings, RemoteActionSettingsModel>
    {
        public RemoteActionController(IHttpContextAccessor httpContextAccessor, ILogger<DataListController<RemoteAction, RemoteActionSettings, RemoteActionSettingsModel>> logger) : base(httpContextAccessor, logger)
        {

        }

        protected override Sql GetSql(RemoteActionSettingsModel settings)
        {
            var sql = base.GetSql(settings);
            if (settings.RaID != 0)
            {
                sql.Where("raID = @0", settings.RaID);
            }
            return sql;
        }

        [HttpPost]
        [Route("/api/remoteaction/getstatuses")]
        public object GetStatuses()
        {
            return S4.Entities.RemoteAction.Statuses.Items;
        }
                

    }
}

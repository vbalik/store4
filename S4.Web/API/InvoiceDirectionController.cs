﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Net.Http;

using S4.Entities;
using S4.Web.Models;
using S4.Web.DataList;
using NPoco;
using Microsoft.Extensions.Logging;
using S4.Entities.Views;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/invoicedirection")]
    public class InvoiceDirectionController : DataListController<Direction, InvoiceDirectionSettings, InvoiceDirectionSettingsModel>
    {
        public InvoiceDirectionController(IHttpContextAccessor httpContextAccessor, ILogger<DataListController<Direction, InvoiceDirectionSettings, InvoiceDirectionSettingsModel>> logger) : base(httpContextAccessor, logger)
        {

        }

        protected override Sql GetSql([FromBody] InvoiceDirectionSettingsModel model)
        {
            var sql = base.GetSql(model);
            if (model.InvoiceID != 0)
            {
                sql.Where("InvoiceID = @0", model.InvoiceID);
            }
            return sql;
        }
               
    }

    
}

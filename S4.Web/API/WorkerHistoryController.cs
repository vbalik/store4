﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NPoco;

using S4.Entities;
using S4.Web.Models;
using S4.Web.DataList;
using S4.Web.Models.DataList;
using Microsoft.Extensions.Logging;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/WorkerHistory")]
    public class WorkerHistoryControler : DataListController<WorkerHistory, WorkerHistorySettings, WorkerHistorySettingsModel>
    {
        public WorkerHistoryControler(IHttpContextAccessor httpContextAccessor, ILogger<DataListController<WorkerHistory, WorkerHistorySettings, WorkerHistorySettingsModel>> logger) : base(httpContextAccessor, logger)
        {

        }

        protected override Sql GetSql(WorkerHistorySettingsModel settings)
        {
            var sql = base.GetSql(settings);
            if (settings.WorkerID != null)
            {
                sql.Where("workerID = @0", settings.WorkerID);
            }
            return sql;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NPoco;
using S4.Core.Cache;
using S4.Entities.Models;
using S4.Entities.Views;
using S4.Web.DataList;
using S4.Web.Models;
using S4.Web.SOAP;
using S4.Web.SOAP.S3Params;
using S4.Core;
using S4.Procedures;
using Microsoft.Extensions.Logging;
using S4.Entities;
using System.Net.WebSockets;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/article")]
    [Authorize]
    [ExportName("Karty")]
    public class ArticleController : DataListController<ArticleView, ArticleSettings, ArticleSettingsModel>
    {
        private readonly DAL.ArticleDAL _articleDAL;
        private readonly ICache<Entities.ArticlePacking, int> _articlePackingCache;
        private readonly ICache<Entities.Models.ArticleModel, int> _articleModelCache;

        public ArticleController(DAL.ArticleDAL articleDAL, ICache<Entities.ArticlePacking, int> articlePackingCache, ICache<Entities.Models.ArticleModel, int> articleModelCache, IHttpContextAccessor httpContextAccessor, ILogger<DataListController<ArticleView, ArticleSettings, ArticleSettingsModel>> logger)
        : base(httpContextAccessor, logger)
        {
            _articleDAL = articleDAL;
            _articlePackingCache = articlePackingCache;
            _articleModelCache = articleModelCache;
        }


        // for seekArticleCombo.js component
        [Route("searcharticles")]
        public IEnumerable<Entities.Views.ArticleBaseView> SearchArticles([FromBody] Models.SearchArticlesModel searchArticlesModel)
        {
            var sql = new NPoco.Sql();
            var part = $"%{searchArticlesModel.Part}%";
            if (!searchArticlesModel.DisabledIncl)
                sql.Where("articleStatus <> @0", Entities.Article.AR_STATUS_DISABLED);
            if (!searchArticlesModel.NotMovableInc)
                sql.Where("movablePack = 1");
            // ignore czech characters in articleDesc
            sql.Where("articleCode LIKE @0 or articleDesc LIKE @0 COLLATE Latin1_General_CI_AI", part);
            sql.OrderBy("articleCode");

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var result = db.Fetch<Entities.Views.ArticleBaseView>(sql);
                return result;
            }
        }


        [Route("SearchByCode/{articleCodePart}")]
        [AllowAnonymous]
        public List<ArticlePackingInfo> SearchByCode(string articleCodePart)
        {
            var pattern = $"{articleCodePart}%";
            var articles = _articleDAL.ScanByArticleCode(pattern, true);
            return articles.Select(a => ArticlePackingInfo.FromArticleModel(a)).ToList();
        }

        [Route("SearchByCodeP")]
        [HttpPost]
        [AllowAnonymous]
        public List<ArticlePackingInfo> SearchByCodeP([FromBody] string articleCodePart)
        {
            var pattern = $"{articleCodePart}%";
            var articles = _articleDAL.ScanByArticleCode(pattern, true);
            return articles.Select(a => ArticlePackingInfo.FromArticleModel(a)).ToList();
        }


        [Route("SearchByDesc/{articleDescPart}")]
        [AllowAnonymous]
        public List<ArticlePackingInfo> SearchByDesc(string articleDescPart)
        {
            var pattern = $"%{articleDescPart}%";
            var articles = _articleDAL.ScanByArticleDesc(pattern, true);
            return articles.Select(a => ArticlePackingInfo.FromArticleModel(a)).ToList();
        }

        [Route("SearchByDescP")]
        [HttpPost]
        [AllowAnonymous]
        public List<ArticlePackingInfo> SearchByDescP([FromBody] string articleDescPart)
        {
            var pattern = $"%{articleDescPart}%";
            var articles = _articleDAL.ScanByArticleDesc(pattern, true);
            return articles.Select(a => ArticlePackingInfo.FromArticleModel(a)).ToList();
        }


        [Route("getartpacking/{artPackingID}")]
        [AllowAnonymous]
        public ArticlePackingInfo GetArtPacking(int artPackingID)
        {
            var packing = _articlePackingCache.GetItem(artPackingID);
            if (packing == null)
                return null;

            var articleModel = _articleModelCache.GetItem(packing.ArticleID);
            if (articleModel == null)
                return null;

            var result = ArticlePackingInfo.FromArticleModel(articleModel);
            result.ArticlePacking = packing;

            return result;
        }


        [Route("searchbybarcode/{barCode}")]
        [AllowAnonymous]
        public List<ArticlePackingInfo> SearchByBarCode(string barCode)
        {
            _logger.LogDebug("SearchByBarCode - '{barCode}'", barCode);


            var articles = _articleDAL.ScanByBarCode(barCode, true);
            return articles.Select(a => ArticlePackingInfo.FromArticleModel(a)).ToList();
        }

        [Route("searchbybarcodep")]
        [HttpPost]
        [AllowAnonymous]
        public List<ArticlePackingInfo> SearchByBarCodeP([FromBody] string barCode)
        {
            _logger.LogDebug("SearchByBarCodeP - '{barCode}'", barCode);

            var articles = _articleDAL.ScanByBarCode(barCode, true);
            return articles.Select(a => ArticlePackingInfo.FromArticleModel(a)).ToList();
        }


        [Route("getarticlemodel/{articleID}")]
        [AllowAnonymous]
        public ArticleModel GetArticleModel(int articleID)
        {
            return _articleDAL.GetModel(articleID);
        }


        #region DataListController<ArticleView, ArticleSettings, ArticleSettingsModel>

        protected override Sql GetSql(ArticleSettingsModel settings)
        {
            var filterOption = settings.FilterOption == null ?
                ArticleSettingsModel.ArticleFilterEnum.All : Enum.Parse<ArticleSettingsModel.ArticleFilterEnum>(settings.FilterOption.ToString());

            Sql sql = null;
            if (settings.PageSize == 0 || filterOption == ArticleSettingsModel.ArticleFilterEnum.OnStore)
            {
                sql = new Sql();
                sql.Append("SELECT * FROM (");
                sql.Append(" SELECT ar.*, quantity = (SELECT SUM(quantity) FROM s4_vw_onStore_Summary_Valid s JOIN s4_positionList p ON p.positionID = s.positionID WHERE s.artPackID = pa.artPackID AND p.posCateg = 0)");
                sql.Append(" FROM s4_articleList ar LEFT JOIN [s4_articleList_packings] pa ON (ar.articleID = pa.articleID) WHERE (pa.movablePack = 1)");
                sql.Append(" ) art");
            }
            else
                sql = new Sql("SELECT *, 0 as quantity FROM s4_articleList art");

            switch (filterOption)
            {
                case ArticleSettingsModel.ArticleFilterEnum.Used:
                    sql.Where("articleStatus <> @0", Entities.Article.AR_STATUS_DISABLED);
                    break;
                case ArticleSettingsModel.ArticleFilterEnum.UsedNoActiva:
                    sql.Where("articleStatus <> @0 AND manufID <> @1", Entities.Article.AR_STATUS_DISABLED, "ACTIVA");
                    break;
                case ArticleSettingsModel.ArticleFilterEnum.OnStore:
                    sql.Where("quantity <> 0");
                    break;
            }

            if (!string.IsNullOrEmpty(settings.ManufID))
            {
                sql.Where("art.manufID = @0", settings.ManufID);
            }

            return sql;
        }


        protected override ArticleView GetItem(string id)
        {
            var sql = new Sql("SELECT *, 0 as quantity FROM s4_articleList");
            sql.Where($"articleID = @0", id);

            using (var db = Core.Data.ConnectionHelper.GetDB())
                return db.Fetch<ArticleView>(sql).FirstOrDefault();
        }


        protected override void FillData(DataListData<ArticleView, ArticleSettings, ArticleSettingsModel> dataListData, bool toExcel)
        {
            base.FillData(dataListData, toExcel);

            if (dataListData.DataModel.Settings.PageSize == 0)
                return;

            //the quantity has been just loaded in case of OnStore filter option
            if (dataListData.DataModel.Settings.FilterOption != null &&
                Enum.Parse<ArticleSettingsModel.ArticleFilterEnum>(dataListData.DataModel.Settings.FilterOption.ToString()) == ArticleSettingsModel.ArticleFilterEnum.OnStore)
            {
                dataListData.DataModel.Settings.Columns["quantity"].SetDisallowFiltering(false).SetDisallowSorting(false);
                return;
            }

            dataListData.DataModel.Settings.Columns["quantity"].SetDisallowFiltering(true).SetDisallowSorting(true);
            var articles = dataListData.DataModel.Data.Select(i => i.ArticleID).ToList();
            var onStore = Singleton<ServicesFactory>.Instance.GetOnStore(true)
                .ByArticle(articles, StoreCore.Interfaces.ResultValidityEnum.Valid)
                .Where(s => s.Position.PosCateg == Entities.Position.PositionCategoryEnum.Store);

            foreach (var item in dataListData.DataModel.Data)
                item.Quantity = onStore.Where(i => i.ArticleID == item.ArticleID).Sum(s => s.Quantity);
        }


        protected override ArticleView SaveItem(ArticleView item)
        {
            //throws an exception if data are not valid
            CheckDataValidity(item);

            var isAdding = false;
            var transArticle = new DAL.ArticleDAL();
            var originItem = transArticle.Get(item.ArticleID);
            if (originItem == null)
            {
                originItem = new Entities.Article();
                isAdding = true;
            }

            //check Source_id
            var sql = new NPoco.Sql();
            sql.Where("source_id = @0 AND articleID != @1", item.Source_id, item.ArticleID);
            var articles = transArticle.List(sql);

            if (articles != null && articles.Count > 0)
                throw new Exception($"ID zdroje již použito u jiné karty {articles.First().ArticleCode} {articles.First().ArticleDesc}! Karta nebyla uložena!");

            var currentItem = new Entities.Article()
            {
                ArticleCode = item.ArticleCode,
                ArticleDesc = item.ArticleDesc,
                ArticleID = item.ArticleID,
                ArticleStatus = item.ArticleStatus,
                ArticleType = item.ArticleType,
                ManufID = item.ManufID,
                MixBatch = item.MixBatch,
                SectID = item.SectID,
                Source_id = item.Source_id,
                UnitDesc = item.UnitDesc,
                UseBatch = item.UseBatch,
                UseExpiration = item.UseExpiration,
                UseSerialNumber = item.UseSerialNumber,
                UserRemark = item.UserRemark,
                Brand = item.Brand,
                SpecFeatures = originItem.SpecFeatures,
                Sign = item.Sign
            };



            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // test if usebatch or use expiration has changed
                var batchOrExpSwithedOn = (currentItem.UseBatch && !originItem.UseBatch) || (currentItem.UseExpiration && !originItem.UseExpiration);
                if (batchOrExpSwithedOn)
                {
                    var result = ProceduresHelpersController.CanEnableExpBatch(originItem.ArticleID, Singleton<ServicesFactory>.Instance.GetOnStore());
                    if (!result.CanEnable)
                        throw new Exception(result.Reason);
                }

                // make history
                var history = new Entities.ArticleHistory
                {
                    ArticleID = item.ArticleID,
                    EventCode = Entities.Article.AR_EVENT_UPDATE,
                    EventData = new Core.PropertiesComparer.PropertiesComparer<Entities.Article>(originItem, currentItem).JsonText,
                    EntryUserID = GetUser(),
                    EntryDateTime = DateTime.Now
                };

                // save
                using (var tr = db.GetTransaction())
                {
                    // save 
                    db.Save(currentItem);

                    // if it's a new article, add default packing too
                    if (isAdding)
                    {
                        var articlePacking = new ArticlePacking()
                        {
                            ArticleID = currentItem.ArticleID,
                            MovablePack = true,
                            Source_id = $"PA_{item.Source_id}",
                            PackStatus = ArticlePacking.AP_STATUS_OK,
                            PackRelation = 1,
                            PackDesc = "ks",
                        };
                        db.Save(articlePacking);
                    }

                    // history
                    history.ArticleID = currentItem.ArticleID;
                    db.Insert(history);

                    // do save
                    tr.Complete();
                }
            }

            return item;
        }

        #endregion

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace S4.Web.API
{
    [Produces("application/json")]    
    public class AuthControler : Controller
    {
        public class SetPasswordModel
        {
            public string WorkerID { get; set; }
            public string OldPassword { get; set; }
            public string NewPassword { get; set; }
            public bool CheckOld { get; set; } = true;
        }


        [HttpPost]
        [Route("api/auth/setpassword")]
        public object SetPassword([FromBody]SetPasswordModel setPasswordModel)
        {
            if (setPasswordModel == null)
                throw new ArgumentNullException("setPasswordModel");
            if (setPasswordModel.WorkerID == null)
                throw new ArgumentNullException("WorkerID");
            if (setPasswordModel.CheckOld && setPasswordModel.OldPassword == null)
                throw new ArgumentNullException("OldPassword");
            if (setPasswordModel.NewPassword == null)
                throw new ArgumentNullException("NewPassword");

            var dal = new DAL.WorkerDAL();
            var worker = dal.Get(setPasswordModel.WorkerID);
            if (worker == null)
                throw new Exception($"Worker not found; workerID: {setPasswordModel.WorkerID}");

            // check old password
            if (setPasswordModel.CheckOld && !Core.Security.S3SecData.CheckPassword(worker.WorkerSecData, worker.WorkerID, setPasswordModel.OldPassword))
                return new
                {
                    Success = true,
                    Saved = false,
                    Message = "Staré heslo není správné"
                };

            // save new password
            worker.WorkerSecData = Core.Security.S3SecData.MakeSecData(worker.WorkerID, setPasswordModel.NewPassword);
            dal.Update(worker);

            return new {
                Success = true,
                Saved = true,
                Message = "Heslo bylo uloženo"
            };
        }

    }
}

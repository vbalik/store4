﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NPoco;

using S4.Entities.Views;
using S4.Web.DataList;
using S4.Web.Models;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/StockTakingHistory")]
    public class StockTakingHistoryController : DataListController<StockTakingHistoryView, StockTakingHistorySettings, StockTakingHistorySettingsModel>
    {
        public StockTakingHistoryController(IHttpContextAccessor httpContextAccessor, ILogger<DataListController<StockTakingHistoryView, StockTakingHistorySettings, StockTakingHistorySettingsModel>> logger) : base(httpContextAccessor, logger)
        {

        }

        protected override Sql GetSql(StockTakingHistorySettingsModel settings)
        {
            var sql = base.GetSql(settings);
            if (settings.StotakID != 0)
            {
                sql.Where("stotakID = @0", settings.StotakID);
            }
            return sql;
        }
    }
}

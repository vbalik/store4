﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.AspNetCore.Mvc;
using System.Reflection;
using System.Security.Claims;

using S4.Entities.Helpers;
using S4.Web.DataList;
using S4.Web.Models.DataList;
using S4.Core;
using S4.Core.Data;
using S4.Core.Extensions;
using S4.Web.AccessControl;
using S4.Web.Helpers;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.IO;
using S4.Web.API.Helper;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/DataList")]
    public class DataListController<T, TSettings, TSettingsModel> : Controller
        where T : new()
        where TSettings : DataListSettings<T, TSettingsModel>, new()
        where TSettingsModel : DataListSettingsModel<T>, new()
    {
        protected const string _NEW = "_NEW_";
        protected const string _UNKNOWN_VALUE = "< neznámá hodnota >";
        protected const string _TRUE_VALUE = "Ano";
        protected const string _FALSE_VALUE = "Ne";
        protected string _userID;
        protected bool _isSuperUser;
        private List<FieldError> _errors = new List<FieldError>();
        protected readonly ILogger<DataListController<T, TSettings, TSettingsModel>> _logger;
        protected IHttpContextAccessor _httpContextAccessor;


        public DataListController(IHttpContextAccessor httpContextAccessor, ILogger<DataListController<T, TSettings, TSettingsModel>> logger)
        {
            // get user id
            if (httpContextAccessor.HttpContext?.User?.Identity?.IsAuthenticated == true)
                _userID = httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.Sid)?.Value;
            else
                _userID = httpContextAccessor.HttpContext.Request.Headers[ApiAuthenticationMiddleware.API_USER_ID].ToString();

            var worker = (new DAL.WorkerDAL()).Get(_userID);
            _isSuperUser = worker.WorkerClass.HasFlag(Entities.Worker.WorkerClassEnum.IsSuperUser);
            _logger = logger;
            _httpContextAccessor = httpContextAccessor;
        }

        /// <summary>
        /// returns data and settings
        /// </summary>
        /// <param name="settings"></param>
        /// <returns>returns DataListDataModel(T, TSettingsModel) or error></returns>
        //api/DataList
        public object GetData([FromBody] TSettingsModel settings)
        {
            try
            {
                var dataListData = new DataListData<T, TSettings, TSettingsModel>(settings);
                FillData(dataListData, false);
                //new RealTimeProgressBar.ProgressHub().DoSendMessage("test", "msg", 100, 10);
                //new RealTimeProgressBar.ProgressHub().SendMessage("test", "msg", 100);
                return dataListData.DataModel;
            }
            catch (Exception ex)
            {
                ReportProcException(ex);
                return FormatError(ex.Message);
            }
        }

        /// <summary>
        /// returns settings and one row data for selected id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>DataListDataModel(T, TSettingsModel) or error</returns>
        [HttpPost("getwithsettings/{id}")]
        public object GetWithSettings(string id)
        {
            try
            {
                var dataListData = new DataListData<T, TSettings, TSettingsModel>();
                dataListData.CreateSettings(_isSuperUser);
                dataListData.DataModel.Data = new T[] { GetItem(id) };
                return dataListData.DataModel;
            }
            catch (Exception ex)
            {
                ReportProcException(ex);
                return FormatError(ex.Message);
            }
        }

        /// <summary>
        /// returns item by selected id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>T or error</returns>
        // GET: api/DataList/5
        [HttpPost("get/{id}")]
        public object Get(string id)
        {
            try
            {
                return GetItem(id);
            }
            catch (Exception ex)
            {
                ReportProcException(ex);
                return FormatError(ex.Message);
            }
        }

        /// <summary>
        /// saves edited item
        /// </summary>
        /// <param name="item"></param>
        /// <returns>returns T or error</returns>
        [HttpPost("save")]
        public object Save([FromBody] T item)
        {
            try
            {
                return SaveItem(item);
            }
            catch (UserException userEx)
            {
                return new { Success = true, FieldError = true, FieldErrorText = userEx.Message, FieldName = userEx.FieldName };
            }
            catch (Exception ex)
            {
                ReportProcException(ex);
                return new { Success = false, ErrorText = ex.Message };
            }
        }

        /// <summary>
        /// deletes selected item
        /// </summary>
        /// <param name="item"></param>
        /// <returns>returns null or error</returns>
        [HttpPost("delete")]
        public object Delete([FromBody] T item)
        {
            try
            {
                DeleteItem(item);
                return new object();
            }
            catch (Exception ex)
            {
                ReportProcException(ex);
                if (ex is System.Data.SqlClient.SqlException && ex.Data["HelpLink.EvtID"].ToString() == "547")
                    return new { Success = false, ErrorText = "Položku nelze vymazat, protože je používána!" };
                else
                    return new { Success = false, ErrorText = ex.Message };
            }
        }

        /// <summary>
        /// creates excel file in memory and returns link to it
        /// </summary>
        /// <returns></returns>
        [HttpPost("toexcel")]
        public object ToExcel([FromBody] TSettingsModel settings)
        {
            var ok = true;
            var fileID = "";
            var limit = 10000;

            /*if (settings.TotalItems > limit)
                ok = false;*/

            if (ok)
            {
                //get data with no paging
                settings.PageSize = 0;
                //set server side for sorting and filtering
                settings.ServerSide = true;
                var dataListData = new DataListData<T, TSettings, TSettingsModel>(settings);
                dataListData.DataModel.Settings.PageSize = 0;
                FillData(dataListData, true);

                //create and fill data table for selected columns - default all visible
                var dataTable = new System.Data.DataTable();
                var visibleColumns = GetExcelColumns(settings);
                foreach (var column in visibleColumns)
                    dataTable.Columns.Add(GetExcelDataColumn(column));

                //add the datarow for all items in data list
                foreach (T item in dataListData.DataModel.Data)
                {
                    dataTable.Rows.Add(GetExcelDataRow(dataTable, item, visibleColumns));
                }

                // store file & return file id
                var exportName = this.GetType().GetCustomAttribute<ExportNameAttribute>(false);
                var fileName = (exportName == null) ? "Export" : exportName.Name;
                fileID = Singleton<Helpers.FilesCache>.Instance.AddToCache($"{fileName}.xlsx", "application/octet-stream", S4.ToExcel.Helper.DataTableToExcel(dataTable, worksheetName: "Data"));
            }

            return new { fileID, ok, totalItems = settings.TotalItems, limit };
        }

        /// <summary>
        /// returns collumn list for creating excel
        /// </summary>
        /// <param name="settings"></param>
        /// <returns></returns>
        protected virtual DataListColumnModel[] GetExcelColumns(TSettingsModel settings)
        {
            return settings.Columns.Where(c => c.Visible).OrderBy(c => c.Order).ToArray();
        }

        /// <summary>
        /// creates table column by datalist column
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        protected virtual System.Data.DataColumn GetExcelDataColumn(DataListColumnModel column)
        {
            switch (column.DataType)
            {
                case DataListColumnModel.DataTypeEnum.tpDecimal:
                case DataListColumnModel.DataTypeEnum.tpNumber:
                    return new System.Data.DataColumn(column.Caption, typeof(decimal));
                default:
                    return new System.Data.DataColumn(column.Caption);
            }
        }

        /// <summary>
        /// creates and fills table data row by item
        /// </summary>
        /// <param name="table">the table for excel</param>
        /// <param name="item">datalist item</param>
        /// <param name="columns">datalist columns for excel</param>
        /// <returns></returns>
        protected virtual System.Data.DataRow GetExcelDataRow(System.Data.DataTable table, T item, DataListColumnModel[] columns)
        {
            var dataRow = table.NewRow();

            for (var i = 0; i < columns.Length; i++)
            {
                var val = GetExcelColumnValue(item, columns[i]);
                dataRow[i] = (val == null || val == string.Empty) ? DBNull.Value : (object)val;
            }

            return dataRow;
        }

        /// <summary>
        /// gets value for table cell
        /// </summary>
        /// <param name="item">datalist item</param>
        /// <param name="column">datalist column</param>
        /// <returns></returns>
        protected virtual string GetExcelColumnValue(T item, DataListColumnModel column)
        {
            //multicolumn
            if (column.Columns != null && column.Columns.Count > 0)
            {
                var values = column.Columns.Select(c => $"{GetExcelColumnValue(item, c)}").ToArray();
                return string.Join('/', values);
            }

            var val = GetPropertyByFieldName(column.FieldName).GetValue(item);

            //list column (enums or status)
            if (column.ValuesList != null && column.ValuesList.Length > 0)
            {
                if (column.DataType == DataListColumnModel.DataTypeEnum.tpFlag)
                {
                    var values = new List<string>();
                    var intValue = (int)val;
                    foreach (var option in column.ValuesList)
                    {
                        var intOption = int.Parse(option.Code);
                        if ((intOption & intValue) == intOption)
                            values.Add(option.Name);
                    }
                    return string.Join(", ", values);
                }
                else
                {
                    var code = $"{val}";
                    var value = column.ValuesList.Where(v => v.Code == code).FirstOrDefault();
                    return value?.Name ?? _UNKNOWN_VALUE;
                }
            }

            switch (column.DataType)
            {
                case DataListColumnModel.DataTypeEnum.tpBool:
                    return (val == null ? false : Convert.ToBoolean(val)) ? _TRUE_VALUE : _FALSE_VALUE;
                case DataListColumnModel.DataTypeEnum.tpDate:
                case DataListColumnModel.DataTypeEnum.tpSmallDate:
                    return val == null ? null : string.Format("{0:d.M.yyyy}", val);
                case DataListColumnModel.DataTypeEnum.tpDateTime:
                    return val == null ? null : string.Format("{0:d.M.yyyy HH:mm:ss}", val);
                default:
                    return $"{val}";
            }
        }

        protected virtual T GetItem(string id)
        {
            if (id == _NEW)
                return new T();

            var keyFieldsList = typeof(T).GetKeyColumns();
            var keyValuesList = id.Split("|");
            if (keyFieldsList.Length != keyValuesList.Length)
                throw new InvalidOperationException("Invalid Key Length");

            var sql = GetDetailSql(id);

            var columnParts = new List<object>();
            for (int i = 0; i < keyFieldsList.Count(); i++)
            {
                columnParts.Add($"{keyFieldsList[i]} = @{i}");
            }
            sql.Where(string.Join(" and ", columnParts.ToArray()), keyValuesList.ToArray());

            using (var db = Core.Data.ConnectionHelper.GetDB())
                return db.Fetch<T>(sql).FirstOrDefault();
        }

        protected virtual NPoco.Sql GetDetailSql(string id)
        {
            return new NPoco.Sql();
        }

        protected virtual T SaveItem(T item)
        {
            if (CheckDataValidity(item))
                using (var db = Core.Data.ConnectionHelper.GetDB())
                    db.Save(item);
            return item;
        }

        protected virtual void DeleteItem(T item)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
                db.Delete(item);
        }

        protected virtual bool CheckDataValidity(T item)
        {
            var check = new FieldsCheck();
            if (!check.DoCheck(item))
                throw new DataException(string.Format("{0} data are not valid", item.GetType().Name), check.Errors.ToArray());

            return true;
        }

        protected virtual NPoco.Sql GetSql(TSettingsModel settings)
        {
            return new NPoco.Sql();
        }

        protected virtual void FillData(DataListData<T, TSettings, TSettingsModel> dataListData, bool toExcel)
        {
            dataListData.CreateSettings(_isSuperUser);

            var sql = GetSql(dataListData.DataModel.Settings);

            if (dataListData.DataModel.Settings.ServerSide)
            {
                //filter

                //columns with filter
                var filteredColumns = dataListData.DataModel.Settings.Columns.Where(c => c.FilterList != null && c.FilterList.Length > 0)
                    .Union(dataListData.DataModel.Settings.Columns.SelectMany(c => c.Columns).Where(c => c.FilterList != null && c.FilterList.Length > 0));
                //fields conditions list
                var fields = new List<string>();
                //argument list
                var values = new List<object>();
                //param id counter
                int paramId = 0;
                foreach (var column in filteredColumns)
                {
                    //test if property is en enum
                    var property = GetPropertyByFieldName(column.FieldName);
                    var propertyType = property.GetPropertyType();
                    //set flag conversion to int required
                    var convertToInt = propertyType.BaseType != null && propertyType.BaseType == typeof(Enum);

                    var dataListColumn = new DataListColumn(column);
                    //list of condition for one column
                    var columnParts = new List<object>();
                    var sqlFieldName = GetSQLColName(column.FieldName);

                    foreach (var filterValue in column.FilterList)
                    {
                        try
                        {
                            var operation = dataListColumn.GetOperation(filterValue.Value, out object value1, out object value2);

                            if (column.DataType == DataListColumnModel.DataTypeEnum.tpDate || column.DataType == DataListColumnModel.DataTypeEnum.tpDateTime || column.DataType == DataListColumnModel.DataTypeEnum.tpSmallDate)
                            {
                                //check MSSQL Datetime
                                value1 = CheckDate(value1, column.DataType);
                            }

                            //test if subquery apply required
                            if (column.DataType == DataListColumnModel.DataTypeEnum.tpList)
                            {
                                var listItem = column.ValuesList.Where(i => i.Code == value1.ToString()).First();
                                if (!string.IsNullOrWhiteSpace(listItem.FilterSubquery))
                                {
                                    columnParts.Add(listItem.FilterSubquery);
                                    continue;
                                }
                            }
                            //convert filter values to int if needed
                            if (convertToInt)
                            {
                                value1 = (int)Enum.Parse(propertyType, value1.ToString());
                                if (value2 != null)
                                    value2 = (int)Enum.Parse(propertyType, value2.ToString());
                            }
                            //
                            if (propertyType == typeof(decimal) || propertyType == typeof(double))
                            {
                                value1 = new ExtraDataProxy().ParseDecimal(value1.ToString());
                                if (value2 != null)
                                    value2 = new ExtraDataProxy().ParseDecimal(value2.ToString());
                            }
                            if (operation == DataListColumn.ComparisonTypeEnum.ctRange ||
                                (operation == DataListColumn.ComparisonTypeEnum.ctEquals &&
                                (column.DataType == DataListColumnModel.DataTypeEnum.tpDate ||
                                column.DataType == DataListColumnModel.DataTypeEnum.tpDateTime ||
                                column.DataType == DataListColumnModel.DataTypeEnum.tpSmallDate)))
                            {
                                if (operation != DataListColumn.ComparisonTypeEnum.ctRange &&
                                    (column.DataType == DataListColumnModel.DataTypeEnum.tpDate || column.DataType == DataListColumnModel.DataTypeEnum.tpDateTime ||
                                     column.DataType == DataListColumnModel.DataTypeEnum.tpSmallDate))
                                {

                                    var from = (DateTime)value1;
                                    value2 = from.AddDays(1).AddMilliseconds(-1);
                                }

                                columnParts.Add($"({sqlFieldName} >= @{paramId} and {sqlFieldName} <= @{paramId + 1})");
                                values.Add(value1);
                                values.Add(value2);
                                paramId += 2;
                            }
                            else if (column.DataType == DataListColumnModel.DataTypeEnum.tpFlag)
                            {
                                columnParts.Add($"{sqlFieldName} & @{paramId} = @{paramId}");
                                values.Add(value1);
                                paramId += 1;
                            }
                            else
                            {
                                object value = null;
                                string operatorStr = null;
                                switch (operation)
                                {
                                    case DataListColumn.ComparisonTypeEnum.ctStarts:
                                        value = $"{value1}%";
                                        operatorStr = "LIKE";
                                        break;
                                    case DataListColumn.ComparisonTypeEnum.ctEnds:
                                        value = $"%{value1}";
                                        operatorStr = "LIKE";
                                        break;
                                    case DataListColumn.ComparisonTypeEnum.ctContains:
                                        value = $"%{value1}%";
                                        operatorStr = "LIKE";
                                        break;
                                    case DataListColumn.ComparisonTypeEnum.ctBigger:
                                        value = value1;
                                        if (column.DataType == Models.DataList.DataListColumnModel.DataTypeEnum.tpDate || column.DataType == Models.DataList.DataListColumnModel.DataTypeEnum.tpSmallDate ||
                                            column.DataType == Models.DataList.DataListColumnModel.DataTypeEnum.tpDateTime)
                                        {
                                            var date = (DateTime)value;
                                            value = date.AddDays(1);
                                        }
                                        operatorStr = ">";
                                        break;
                                    case DataListColumn.ComparisonTypeEnum.ctBiggerOrEquals:
                                        value = value1;
                                        operatorStr = ">=";
                                        break;
                                    case DataListColumn.ComparisonTypeEnum.ctLes:
                                        value = value1;
                                        operatorStr = "<";
                                        break;
                                    case DataListColumn.ComparisonTypeEnum.ctLesOrEquals:
                                        value = value1;
                                        operatorStr = "<=";
                                        break;
                                    case DataListColumn.ComparisonTypeEnum.ctEquals:
                                        value = value1;
                                        operatorStr = "=";
                                        break;
                                    default:
                                        throw new NotImplementedException();
                                }

                                //TRIM SPACE
                                if (column.DataType.Equals(DataListColumnModel.DataTypeEnum.tpString) && column.TrimColumnInCondition)
                                    sqlFieldName = $"TRIM({sqlFieldName})";

                                columnParts.Add($"({sqlFieldName} {operatorStr} @{paramId})");
                                values.Add(value);
                                paramId++;
                            }
                        }
                        catch (Exception ex)
                        {
                            filterValue.ValueInvalid = true;
                            dataListData.DataModel.Error = $"Neplatná hodnota \"{filterValue.Value}\" pro filtr sloupce \"{column.Caption}\"!";
                            dataListData.DataModel.Data = new T[] { };
                            return;
                        }
                    }
                    fields.Add($"({string.Join(" or ", columnParts.ToArray())})");
                }

                if (fields.Count > 0)
                {
                    sql.Where(string.Join(" and ", fields.ToArray()), values.ToArray());
                }


                List<string> orderParams = new List<string>();
                foreach (var column in dataListData.DataModel.Settings.Columns
                    .Where(c => c.SortMode != Models.DataList.DataListColumnModel.SortModeEnum.smNone)
                    .OrderByDescending(c => c.Visible)
                    .ThenBy(c => c.SortOrder))
                {
                    Action<DataListColumnModel, DataListColumnModel.SortModeEnum> addFn = (col, mode) =>
                    {
                        var sqlFieldName = GetSQLColName(col.FieldName);
                        orderParams.Add($"{sqlFieldName}{(mode == DataListColumnModel.SortModeEnum.smDescending ? " DESC" : null)}");
                    };

                    if (column.Columns.Count > 0)
                    {
                        foreach (var col in column.Columns)
                        {
                            addFn(col, column.SortMode);
                        }
                    }
                    else
                    {
                        addFn(column, column.SortMode);
                    }
                }
                if (orderParams.Count == 0)
                {
                    if (dataListData.DataModel.Settings.KeyFields.Length == 0)
                    {
                        sql.OrderBy(
                            (typeof(T)
                            .GetProperties()
                            .Where(p => p.GetCustomAttribute(typeof(NPoco.ColumnAttribute)) != null).First()
                            .GetCustomAttribute(typeof(NPoco.ColumnAttribute)) as NPoco.ColumnAttribute).Name);
                    }
                    else
                    {
                        sql.OrderBy(dataListData.DataModel.Settings.KeyFields);
                    }
                }
                else
                {
                    sql.OrderBy(orderParams.ToArray());
                }

                using (var db = Core.Data.ConnectionHelper.GetDB())
                {
                    if (dataListData.DataModel.Settings.PageSize > 0)
                    {
                        var page = db.Page<T>(dataListData.DataModel.Settings.Page, dataListData.DataModel.Settings.PageSize, sql);
                        dataListData.DataModel.Settings.Pages = (int)page.TotalPages;
                        dataListData.DataModel.Settings.TotalItems = (int)page.TotalItems;
                        dataListData.DataModel.Data = page.Items;
                    }
                    else
                    {
                        dataListData.DataModel.Data = db.Fetch<T>(sql);
                    }
                }
            }
            else
            {
                using (var db = Core.Data.ConnectionHelper.GetDB())
                    dataListData.DataModel.Data = db.Fetch<T>(sql);
            }
        }

        protected object FormatError(string message)
        {
            return new { Success = false, ErrorText = message };
        }

        protected string GetUser()
        {
            if (Request.HttpContext?.User.Identity.IsAuthenticated == true)
                return Request.HttpContext.User.FindFirst(ClaimTypes.Sid)?.Value;

            return Request.Headers[ApiAuthenticationMiddleware.API_USER_ID].ToString();
        }

        private string EntityName()
        {
            return this.ToString().Replace("S4.Web.API.", "").Replace("Controler", "");
        }

        private PropertyInfo GetPropertyByFieldName(string fieldName)
        {
            return typeof(T).GetProperties()
                 .Where(p => p.Name.ToUpper() == fieldName.ToUpper())
                 .FirstOrDefault();
        }

        private string GetSQLColName(string fieldName)
        {
            var prop = GetPropertyByFieldName(fieldName);
            if (prop == null)
                return fieldName;
            var rawColumnAttr = prop.GetCustomAttribute(typeof(Entities.Attributes.RawColumnNameAttribute)) as Entities.Attributes.RawColumnNameAttribute;
            return rawColumnAttr != null ? rawColumnAttr.Name : fieldName;
        }

        private void ReportProcException(Exception exc)
        {
            // status data
            var requestId = HttpContext.TraceIdentifier;
            var body = GetRequestBody(_httpContextAccessor.HttpContext.Request.Body);

            var errorLogHelper = new ErrorLogHelper();
            errorLogHelper.ReportProcException(body, new { userID = _userID, requestId }, exc, HttpContext.Request.Path, _logger);
        }

        private static string GetRequestBody(Stream body)
        {
            try
            {
                using (StreamReader stream = new StreamReader(body))
                {
                    stream.BaseStream.Seek(0, SeekOrigin.Begin);
                    return stream.ReadToEnd();

                }
            }
            catch
            {
                return null;
            }
        }

        private static DateTime CheckDate(object value, DataListColumnModel.DataTypeEnum dataType)
        {
            var date = (DateTime)value;
            DateTime min = new DateTime(1753, 01, 01);
            DateTime max = new DateTime(9999, 12, 31, 23, 59, 59, 997);

            //check MSSQL Datetime
            if (dataType == DataListColumnModel.DataTypeEnum.tpSmallDate)
            {
                min = new DateTime(1900, 01, 01);
                max = new DateTime(2079, 06, 06, 23, 59, 0);
            }
            
            if (date > max)
                return max;

            if (date < min)
                return min;

            return date;
        }
    }
}
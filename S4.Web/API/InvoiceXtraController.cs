﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Net.Http;

using S4.Entities;
using S4.Web.Models;
using S4.Web.DataList;
using NPoco;
using Microsoft.Extensions.Logging;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/invoicextra")]
    public class InvoiceXtraController : DataListController<InvoiceXtra, InvoiceXtraSettings, InvoiceXtraSettingsModel>
    {
        public InvoiceXtraController(IHttpContextAccessor httpContextAccessor, ILogger<DataListController<InvoiceXtra, InvoiceXtraSettings, InvoiceXtraSettingsModel>> logger) : base(httpContextAccessor, logger)
        {

        }

        protected override Sql GetSql([FromBody]InvoiceXtraSettingsModel model)
        {
            var sql = base.GetSql(model);
            if (model.InvoiceID != 0)
            {
                sql.Where("InvoiceID = @0", model.InvoiceID);
            }
            return sql;
        }
               
    }

    
}

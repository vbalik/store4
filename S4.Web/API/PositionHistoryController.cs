﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NPoco;

using S4.Entities;
using S4.Web.DataList;
using S4.Web.Models;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/positionhistory")]
    public class PositionHistoryController : DataListController<PositionHistory, PositionHistorySettings, PositionHistorySettingsModel>
    {
        public PositionHistoryController(IHttpContextAccessor httpContextAccessor, ILogger<DataListController<PositionHistory, PositionHistorySettings, PositionHistorySettingsModel>> logger) : base(httpContextAccessor, logger)
        {

        }

        protected override Sql GetSql(PositionHistorySettingsModel settings)
        {
            var sql = base.GetSql(settings);
            if (settings.PositionID != 0)
            {
                sql.Where("positionID = @0", settings.PositionID);
            }
            return sql;
        }
    }
}

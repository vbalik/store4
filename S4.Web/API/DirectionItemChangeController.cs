﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NPoco;
using S4.Entities;
using S4.Entities.Views;
using S4.Web.Models;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/directionitemchange")]
    public class DirectionItemChangeController : Controller
    {
        private Sql _GetArticleSql(int directionID)
        {
            var sqlStr = "SELECT i.*, a.articleID, a.articleDesc, a.useBatch, a.useExpiration from s4_direction_items i";
            sqlStr += " LEFT OUTER JOIN s4_vw_article_base a ON a.artPackID = i.artPackID";

            var sql = new NPoco.Sql(sqlStr);
            sql.Where("directionID = @0", directionID);

            return sql;
        }

        private Sql _GetArticleMovesSql(int directionID, int parentDocPosistion)
        {
            var sqlStr = "SELECT mit.*, pos.posCode, lots.artPackID, art.packDesc, art.articleID, art.articleCode, art.articleDesc, lots.batchNum, lots.expirationDate, smo.docInfo as 'doc', dir.docInfo as 'dir', dir.directionID, smo.docStatus, smo.docDate, smo.docType";
            sqlStr += " FROM s4_storeMove_items mit";
            sqlStr += " LEFT OUTER JOIN s4_storeMove_lots lots ON lots.stoMoveLotID = mit.stoMoveLotID";
            sqlStr += " LEFT OUTER JOIN s4_vw_article_base art ON art.artPackID = lots.artPackID";
            sqlStr += " LEFT OUTER JOIN s4_positionList pos ON pos.positionID = mit.positionID";
            sqlStr += " LEFT OUTER JOIN s4_storeMove smo ON smo.stoMoveID = mit.stoMoveID";
            sqlStr += " LEFT OUTER JOIN s4_direction dir ON dir.directionID = smo.parent_directionID";

            var sql = new NPoco.Sql(sqlStr);
            sql.Where("dir.directionID = @0", directionID);
            sql.Where("mit.parent_docPosition = @0", parentDocPosistion);
            sql.Where("smo.docType = @0", StoreMove.MO_DOCTYPE_RECEIVE);
            sql.Where("mit.itemValidity = @0", Entities.StoreMoveItem.MI_VALIDITY_VALID);

            return sql;
        }

        [Produces("application/json")]
        [Route("get-direction-item-moves")]
        public async Task<DirectionItemChangeModel> GetDirectionItemMoves([FromBody] DirectionItemChangeRequestModel data)
        {
            var model = new DirectionItemChangeModel();

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                model.DirectionItem = db.Fetch<DirectionItemView>(_GetArticleSql(data.DirectionID)).FirstOrDefault();
                model.DirectionItemStoMoves = db.Fetch<StoreMoveItemView>(_GetArticleMovesSql(data.DirectionID, data.DocPos));
            }

            return model;
        }
    }
}

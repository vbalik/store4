﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using S4.Core;
using S4.DAL;
using S4.Entities;
using S4.Entities.Helpers.Xml.Dispatch;
using S4.Export;
using S4.Export.Settings;
using S4.ProcedureModels.Dispatch;
using S4.Web.Models;

namespace S4.Web.API
{
    [Produces("application/json")]
    public class ExportDirectionController : Controller
    {
        private readonly IConfiguration _configuration;

        public ExportDirectionController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpPost]
        [Route("api/exportdirection/readextradata")]
        public object ReadExtraData([FromBody]int directionID)
        {
            // get source direction
            DispatchS3 dispatchS3 = null;
            var srcDirection = (new DirectionDAL()).GetDirectionAllData(directionID);

            string xmlSource = GetSource(srcDirection.History);
            if (!string.IsNullOrWhiteSpace(xmlSource))
            {
                //load XML
                var load = DispatchS3.Serializer.Deserialize(xmlSource, out dispatchS3);
                if (load)
                {
                    return dispatchS3.Customer.Extradata;
                }
            }

            return new DispatchS3();
        }

        [HttpPost]
        [Route("api/exportdirection/reademailto")]
        public string ReadMailTo()
        {
            //read setting
            var settingsExport = S4.Export.Settings.Helper.GetSettingsExport();
            return settingsExport.SettingMail.MailTo;

        }

        [HttpPost]
        [Route("api/exportdirection/export")]
        public object Export([FromBody]ExportDirectionModel model)
        {
            //read setting
            var settingsExport = S4.Export.Settings.Helper.GetSettingsExport();

            //make export
            Encoding encoding = Encoding.Default;
            var result = S4.Export.Export.MakeExport(settingsExport, model.DirectionID, model.ExportFormat, out encoding);
            List<Tuple<string, string>> files = new List<Tuple<string, string>>();

            if (result.ResultEnum == S4.Export.Export.ResultEnum.Sended)
            {
                foreach (var file in result.ExpResults)
                {
                    var bytes = encoding.GetBytes(file.fileContent);
                    // store file & return file id
                    var fileID = Singleton<Helpers.FilesCache>.Instance.AddToCache(file.fileName, "application/octet-stream", bytes);
                    files.Add(new Tuple<string, string>(file.fileName, $"/download?fileID={fileID}"));
                }
            }

            return new
            {
                succes = result.ResultEnum == S4.Export.Export.ResultEnum.Sended ? true : false,
                messageText = result.Message,
                files,
                isWarning = result.IsWarning,
                warningMsg = result.WarningMsg,
            };
        }

        [HttpPost]
        [Route("api/exportdirection/sendmail")]
        public MailModel SendMail([FromBody]ExportDirectionModel model)
        {
            //read setting
            var settingsExport = S4.Export.Settings.Helper.GetSettingsExport();

            //read direction
            Direction direction = null;
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                direction = db.SingleOrDefaultById<Direction>(model.DirectionID);
            }

            //prepare mail
            var odeslatHlavicka = "Promedica Praha - Soubory k dodacímu listu {0}";
            var odeslatText = "Promedica Praha - Soubory s daty k dodacímu listu {0}; formát {1}\r\n";
            var docInfo = $"{direction.DocNumPrefix}/{direction.DocYear}/{direction.DocNumber}";
            var message = new MailMessage();
            message.From = new MailAddress(settingsExport.SettingMail.MailFrom);
            message.Subject = string.Format(odeslatHlavicka, docInfo, model.ExportFormat);
            message.Body = string.Format(odeslatText, docInfo, model.ExportFormat);

            //make export
            Encoding encoding = Encoding.Default;
            var result = S4.Export.Export.MakeExport(settingsExport, model.DirectionID, model.ExportFormat, out encoding);
            List<Tuple<string, string>> files = new List<Tuple<string, string>>();

            if (result.ResultEnum == S4.Export.Export.ResultEnum.Sended)
            {
                foreach (var file in result.ExpResults)
                {
                    var bytes = encoding.GetBytes(file.fileContent);
                    Attachment fileAtt = new Attachment(new MemoryStream(bytes), file.fileName);
                    fileAtt.TransferEncoding = TransferEncoding.Base64;
                    // Add the file attachment to this e-mail message.
                    message.Attachments.Add(fileAtt);
                }


                // send mail
                var mailSetting = new Export.MailHelper.MailSetting();
                mailSetting.Message = message;
                mailSetting.Server = settingsExport.SettingMail.MailServer;
                mailSetting.User = settingsExport.SettingMail.MailUzivatel;
                mailSetting.Password = settingsExport.SettingMail.MailHeslo;
                mailSetting.To = model.MailAdresaKomu;

                var mail = new Export.MailHelper.Mail();
                var send = mail.SendMailViaSMTP(mailSetting, out string sendError);

                if (send)
                    return new MailModel() { IsError = false, Message = $"Mail byl odeslán na adresu {model.MailAdresaKomu} počet příloh: ({message.Attachments.Count})" };
                else
                    return new MailModel() { IsError = true, Message = $"Při odesílání mailu se vyskytla chyba: {sendError}" };
            }
            else
                return new MailModel() { IsError = true, Message = $"Při exportu se vyskytla chyba: {result.Message}" };
        }

        #region Helper

        private static string GetSource(List<DirectionHistory> historyList)
        {
            var data = (from h in historyList where h.EventCode.Equals(Direction.DR_STATUS_LOAD) && h.DocPosition == 0 orderby h.EntryDateTime select h.EventData).LastOrDefault();

            if (data != null)
                return data;
            else
                return String.Empty;

        }

        #endregion


    }
        

}

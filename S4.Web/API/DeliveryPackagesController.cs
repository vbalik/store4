﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NPoco;
using S4.Core;
using S4.DAL;
using S4.Entities;
using S4.Web.DataList;
using S4.Web.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/delivery-packages")]
    [ExportName("Seznam balíků spediční služby")]
    public class DeliveryPackagesController : DataListController<Entities.Views.DeliveryPackagesView, DeliveryPackagesSettings, DeliveryPackagesSettingsModel>
    {
        private string _providerName { get; set; }

        public DeliveryPackagesController(IHttpContextAccessor httpContextAccessor,
            ILogger<DataListController<Entities.Views.DeliveryPackagesView, DeliveryPackagesSettings, DeliveryPackagesSettingsModel>> logger)
        : base(httpContextAccessor, logger)
        {

        }

        protected override Sql GetSql(DeliveryPackagesSettingsModel settings)
        {
            // get provider
            if (settings.DeliveryProviderFilterSelected != null)
                _providerName = settings.DeliveryProvidersFilter.SingleOrDefault(_ => _.Item1 == settings.DeliveryProviderFilterSelected).Item2;

            if (settings.Date == null)
                settings.Date = DateTime.Now;

            var sql = new Sql();
            sql.Select(@"dd.directionID as directionID,
                di.parcelRefNumber as parcelRefNumber,
                d.docInfo,
                p.partnerName as contact,
                p.partnerPhone as phone,
                CONCAT(p.partnerName, ', ', pa.addrLine_1, ', ', pa.addrLine_2, ', ', pa.addrCity) as address,
                pa.addrLine_2 as zip");
            sql.From("s4_deliveryDirection dd");
            sql.InnerJoin("s4_deliveryDirection_items di ON di.deliveryDirectionID = dd.deliveryDirectionID");
            sql.InnerJoin("s4_direction d ON d.directionID = dd.directionID");
            sql.InnerJoin("s4_partnerList_addresses pa ON pa.partAddrID = d.partAddrID");
            sql.InnerJoin("s4_partnerList p ON p.partnerID = d.partnerID");
            sql.Where("CAST(dd.deliveryCreated as DATE) = @0", settings.Date?.ToString("yyyy-MM-dd"));
            sql.Where($"dd.deliveryStatus = @0", DeliveryDirection.DD_STATUS_OK);

            if (settings.DeliveryProviderFilterSelected != null && settings.DeliveryProviderFilterSelected != 0)
                sql.Where($"dd.deliveryProvider = @0", settings.DeliveryProviderFilterSelected ?? 0);
            
            return sql;
        }

        protected override void FillData(DataListData<Entities.Views.DeliveryPackagesView, DeliveryPackagesSettings, DeliveryPackagesSettingsModel> dataListData, bool toExcel)
        {
            base.FillData(dataListData, toExcel);
            
            // fill xtra data
            var IDs = dataListData.DataModel.Data.Select(d => d.DirectionID);

            if (!IDs.Any()) return;

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var sql = new NPoco.Sql()
                    .Append($"SELECT * FROM s4_direction_xtra WHERE directionID IN ({string.Join(',', IDs.ToArray())})");

                var xtraList = db.Query<DirectionXtra>(sql).ToList();

                var groupedXtraList = xtraList.GroupBy(_ => _.DirectionID,
                    (key, g) => new
                    {
                        Key = key,
                        Xtras = g
                    }).ToList();

                foreach (var model in dataListData.DataModel.Data)
                {
                    var xtra = groupedXtraList.SingleOrDefault(_ => _.Key == model.DirectionID);
                    if (xtra == null) continue; // skip xtra data

                    var currency = xtra.Xtras.SingleOrDefault(_ => _.XtraCode == Direction.XTRA_DELIVERY_CURRENCY.ToString())?.XtraValue;
                    var price = xtra.Xtras.SingleOrDefault(_ => _.XtraCode == Direction.XTRA_CASH_ON_DELIVERY.ToString())?.XtraValue;
                    var isFirst = xtra.Xtras.Any(_ => _.XtraCode == Direction.XTRA_DELIVERY_PACK_NUMBER.ToString()
                        && _.DocPosition == 0
                        && _.XtraValue == model.ParcelRefNumber);
                    if(price != null && isFirst)
                        model.Price = $"{price} {currency ?? "CZK"}";

                    model.InvoiceNumber = xtra.Xtras.SingleOrDefault(_ => _.XtraCode == Direction.XTRA_INVOICE_NUMBER.ToString())?.XtraValue;
                }
            }
        }

        [Route("print-package-list")]
        public object Print([FromBody] DeliveryPackagesPrintModel model)
        {
            if (model.ReportID == 0)
                model.ReportID = Singleton<DAL.SettingsSet>.Instance.GetValue_Int32(Entities.SettingsDictionary.DELIVERY_PACKAGES_REPORT_ID);

            var settings = model.DeliveryPackagesSettingsModel;
            settings.Page = 1;
            settings.PageSize = 10000;
            settings.Date = model.Date;

            var dataListData = new DataListData<Entities.Views.DeliveryPackagesView, DeliveryPackagesSettings, DeliveryPackagesSettingsModel>(settings);
            FillData(dataListData, false);

            var packagesCnt = dataListData.DataModel.Data.Count();
            var packagesCODCnt = dataListData.DataModel.Data.Where(_ => !string.IsNullOrEmpty(_.Price)).Count();

            //P R I N T
            //get report
            var transReport = new ReportDAL();
            var report = transReport.Get(model.ReportID);
            if (report == null)
                return new { Success = false, ErrorText = $"Report nebyl nalezen; reportID: {model.ReportID}" };

            var printerLocation = Singleton<DAL.Cache.PrinterLocationCache>.Instance.GetItem(model.PrinterLocationID);

            if (printerLocation == null)
                return new { Success = false, ErrorText = $"PrinterLocation nebyl nalezen; printerLocationID: {model.PrinterLocationID}" };

            //variables
            var variables = new List<Tuple<string, object, Type>>();
            variables.Add(new Tuple<string, object, Type>("ProviderName", _providerName, typeof(string)));
            variables.Add(new Tuple<string, object, Type>("PackagesCnt", packagesCnt, typeof(int)));
            variables.Add(new Tuple<string, object, Type>("PackagesCODCnt", packagesCODCnt, typeof(int)));

            //do print
            Report.Print print = new Report.Print();
            print.Variables = variables;
            print.ReportDefinition = report.Template; //šablona z DB
            //print.DesignMode = true; //pouze pro úpravy šablony
            //print.ReportDefFilePath = @"c:\Projects\store4\Tools\Reporty\DopravciSeznamBalicku.srt"; //pouze pro úpravy šablony
            //print.DoPrint("Adobe PDF", dataListData.DataModel.Data); //pouze pro úpravy šablony

            byte[] fileData = null;
            if (model.LocalPrint)
                fileData = print.DoPrint2PDF(dataListData.DataModel.Data);
            else
                print.DoPrint(printerLocation.PrinterPath, dataListData.DataModel.Data);

            if (print.IsError)
                return new { Success = false, ErrorText = print.ErrorText };
            else
            {
                var fileID = "";
                if (model.LocalPrint)
                    fileID = Singleton<Helpers.FilesCache>.Instance.AddToCache("print.pdf", "application/pdf", fileData);

                return new { Success = true, LocalPrint = model.LocalPrint, FileID = fileID, ErrorText = "" };
            }
                

        }
    }
}

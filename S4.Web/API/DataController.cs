﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Reflection;
using Newtonsoft.Json;

namespace S4.Web.API
{
    [Produces("application/json")]
    public class DataController : Controller
    {
        [Route("api/Data")]
        public IEnumerable<object> Data([FromBody]Entities.Models.GetListModel getListModel)
        {
            Assembly asm = typeof(Entities.Article).Assembly;
            Type type = asm.GetType(getListModel.TypeName);
            var sql = new NPoco.Sql();
            if (!string.IsNullOrEmpty(getListModel.Where))
                sql.Where(getListModel.Where, getListModel.Args);
            if (!string.IsNullOrEmpty(getListModel.Order))
                sql.OrderBy(getListModel.Order);

            using (var db = Core.Data.ConnectionHelper.GetDB())
                return db.Fetch(type, sql);
        }
    }
}

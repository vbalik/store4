﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NPoco;

using S4.Entities.Views;
using S4.Web.DataList;
using S4.Web.Models;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/StockTakingSnapshot")]
    public class StockTakingSnapshotController : DataListController<StockTakingSnapshotView, StockTakingSnapshotSettings, StockTakingSnapshotSettingsModel>
    {
        public StockTakingSnapshotController(IHttpContextAccessor httpContextAccessor, ILogger<DataListController<StockTakingSnapshotView, StockTakingSnapshotSettings, StockTakingSnapshotSettingsModel>> logger) : base(httpContextAccessor, logger)
        {

        }

        protected override Sql GetSql(StockTakingSnapshotSettingsModel settings)
        {
            var sql = base.GetSql(settings);
            if (settings.StotakID != 0)
            {
                sql.Where("stotakID = @0", settings.StotakID);
            }
            sql.Where("snapshotClass = @0", settings .SnapshotClass);
            return sql;
        }
    }
}

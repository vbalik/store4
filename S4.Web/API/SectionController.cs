﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NPoco;
using S4.Entities;
using S4.Web.API.Helper;
using S4.Web.DataList;
using S4.Web.Models;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/Section")]
    public class SectionController : DataListController<Section, SectionSettings, SectionSettingsModel>
    {
        public SectionController(IHttpContextAccessor httpContextAccessor, ILogger<DataListController<Section, SectionSettings, SectionSettingsModel>> logger) : base(httpContextAccessor, logger)
        {

        }

        [Route("PositionSections/{positionID}")]
        public IEnumerable<Entities.Section> PositionSections(int positionID)
        {
            var sql = new NPoco.Sql("SELECT * FROM s4_sectionList");
            sql.Where("sectID IN(SELECT sectID FROM s4_positionList_sections WHERE positionID=@0)", positionID);
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                return db.Fetch<Entities.Section>(sql);
            }
        }

        protected override Section SaveItem(Section item)
        {
            if (item.IsNew)
            {
                using (var db = Core.Data.ConnectionHelper.GetDB())
                {
                    if (db.Exists<Section>(item.SectID))
                        throw new UserException($"Položka \"{item.SectID}\" již existuje!");
                }
            }

            return base.SaveItem(item);
        }
    }
}

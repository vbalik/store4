﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using S4.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using System.Runtime.Caching;

namespace S4.Web.API
{
    [Produces("application/json")]
    public class ClientErrorController : Controller
    {
        private const int HASH_CACHE_EXPIRATION = 5; // seconds

        private static NLog.Logger NLogger = NLog.LogManager.GetLogger("JavaScriptError");
        private static MemoryCache _hashCache = new MemoryCache(nameof(ClientErrorController)); 


        [HttpPost]
        [Route("api/clienterror/inserterror")]
        public int InsertError([FromBody] ClientErrorModel model)
        {
            //Program
            var appVersion = Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion;
            var userID = HttpContext.Request.HttpContext?.User?.FindFirst(ClaimTypes.Sid)?.Value;

            string statusData;
            if (model.Column > 0 || model.Line > 0)
                statusData = JsonConvert.SerializeObject(new { userID, model.Column, model.Line });
            else
                statusData = JsonConvert.SerializeObject(new { userID });

            var error = new Entities.ClientError()
            {
                AppVersion = appVersion.ToString(),
                ErrorClass = model.ErrorClass,
                ErrorDateTime = DateTime.Now,
                ErrorMessage = model.ErrorMessage,
                Path = model.Url,
                StatusData = statusData
            };

            return InsertError(error);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("api/clienterror/inserterrorm")]
        public int InsertErrorM([FromBody] Entities.ClientError clientError)
        {
            if (clientError != null)
            {
                //because maybe wrong ErrorDateTime from terminal
                clientError.ErrorDateTime = DateTime.Now;
                return InsertError(clientError);
            }
            else
                return 0;
        }

        private int InsertError(Entities.ClientError clientError)
        {
            // NLOG
            NLogger.Error($"ErrorMessage: {clientError.ErrorMessage} Path: {clientError.Path}");

            // check if error is unique, in last 5 seconds
            if(IsRepeated(clientError))
            {
                // not save to db
                return 0;
            }    

            // write Error to DB
            var id = (new DAL.ErrorDAL()).Insert(clientError);
            return int.Parse(id.ToString());
        }


        private bool IsRepeated(Entities.ClientError clientError)
        {
            // calc hash
            long hash = 397;
            hash = CombineHash(hash, clientError.ErrorClass.GetHashCode());
            hash = CombineHash(hash, clientError.ErrorMessage.GetHashCode());
            hash = CombineHash(hash, clientError.Path.GetHashCode());
            if (clientError.StatusData != null)
                hash = CombineHash(hash, clientError.StatusData.GetHashCode());
            if (clientError.RequestBody != null)
                hash = CombineHash(hash, clientError.RequestBody.GetHashCode());

            // check in cache
            var hashStr = hash.ToString();
            if (_hashCache.GetCacheItem(hashStr) != null)
                return true;
            else
            {
                _hashCache.Add(hashStr, hashStr, new CacheItemPolicy() { SlidingExpiration = TimeSpan.FromSeconds(HASH_CACHE_EXPIRATION) });
                return false;
            }
        }


        private long CombineHash(long accumulator, long hash)
        {
            unchecked
            {
                accumulator = (accumulator * 397) ^ hash;
            }

            return accumulator;
        }
    }
}

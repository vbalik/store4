﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Net.Http;

using S4.Entities;
using S4.Web.Models;
using S4.Web.DataList;
using NPoco;
using Microsoft.Extensions.Logging;
using S4.Web.API.Helper;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/Worker")]
    [ExportName("Pracovnici")]
    public class WorkerControler : DataListController<Worker, WorkerSettings, WorkerSettingsModel>
    {
        public WorkerControler(IHttpContextAccessor httpContextAccessor, ILogger<DataListController<Worker, WorkerSettings, WorkerSettingsModel>> logger) : base(httpContextAccessor, logger)
        {

        }

        protected override Sql GetSql(WorkerSettingsModel settings)
        {
            var sql = base.GetSql(settings);

            if (settings.FilterOption != null)
            {
                var filterOption = Enum.Parse<WorkerSettingsModel.WorkerFilterEnum>(settings.FilterOption.ToString());
                switch (filterOption)
                {
                    case WorkerSettingsModel.WorkerFilterEnum.Active:
                        sql.Where("workerStatus <> @0", Worker.WK_STATUS_DISABLED);
                        break;
                }
            }

            return sql;
        }

        protected override Worker SaveItem(Worker item)
        {
            if (item.IsNew)
            {
                using (var db = Core.Data.ConnectionHelper.GetDB())
                {
                    if (db.Exists<Worker>(item.WorkerID))
                        throw new UserException($"Položka \"{item.WorkerID}\" již existuje!", "WorkerID");
                }
            }

            if (!string.IsNullOrEmpty(item.Password)) //its a new record
            {
                if (new DAL.WorkerDAL().Get(item.WorkerID) != null)
                {
                    throw new UserException($"Uživatel s ID \"{item.WorkerID}\" již existuje, zvolte jiné ID!", "WorkerID");
                }
                if (new DAL.WorkerDAL().GetByLogon(item.WorkerLogon) != null)
                {
                    throw new UserException($"Uživatel s přihlášením \"{item.WorkerLogon}\" již existuje, zvolte jiné přihlašovací jméno!", "WorkerLogon");
                }
                // save new password
                item.WorkerSecData = Core.Security.S3SecData.MakeSecData(item.WorkerID, item.Password);
            }
            else
            {
                //set actual secData - the password might be changed
                var dal = new DAL.WorkerDAL();
                var worker = dal.Get(item.WorkerID);
                item.WorkerSecData = worker?.WorkerSecData;
            }

            base.SaveItem(item);
            item.Password = null;
            return item;
        }
    }
}

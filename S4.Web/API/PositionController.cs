﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NPoco;
using S4.Core;
using S4.Core.Cache;
using S4.Entities.Views;
using S4.ProcedureModels.Info;
using S4.Web.DataList;
using S4.Web.Models;
using Microsoft.Extensions.Logging;
using S4.Web.API.Helper;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/Position")]
    [ExportName("Pozice")]
    public class PositionController : DataListController<PositionView, PositionSettings, PositionSettingsModel>
    {
        private readonly ICache<Entities.Position, string> _positionCache;

        public PositionController(IHttpContextAccessor httpContextAccessor, ICache<Entities.Position, string> positionCache, ILogger<DataListController<PositionView, PositionSettings, PositionSettingsModel>> logger) : base(httpContextAccessor, logger)
        {
            _positionCache = positionCache;
        }


        // for seekArticleCombo.js component
        [AllowAnonymous]
        [Route("searchpositions")]
        public IEnumerable<Entities.Position> SearchPositions([FromBody]SearchPositionsModel searchPositionsModel)
        {
            var sql = new Sql();
            var part = $"%{searchPositionsModel.Part}%";
            if (searchPositionsModel.LimitToCategory)
                sql.Where("posCateg = @0", (byte)searchPositionsModel.PositionCategory);
            sql.Where("posCode LIKE @0 OR posDesc LIKE @0", part);
            sql.OrderBy("posCode");

            using (var db = Core.Data.ConnectionHelper.GetDB())
                return db.Fetch<Entities.Position>(sql);
        }


        [AllowAnonymous]
        [Route("getposition/{posCode}")]
        public Entities.Position GetPosition(string posCode)
        {
            return _positionCache.GetItem(posCode);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("getpositionp")]
        public Entities.Position GetPositionP([FromBody] string posCode)
        {
            return _positionCache.GetItem(posCode);
        }


        [Route("printpositions")]
        public PositionsPrintModel PrintPositions([FromBody]PositionsPrintModel modelPrint)
        {
            var documentList = new List<byte[]>();
            for (int i = 0; i < modelPrint.PositionIDs.Length; i++)
            {
                var model = new OnStorePrint()
                {
                    LocalPrint = true,
                    OnStorePrintSource = OnStorePrint.OnStorePrintSourceEnum.OnStorePrintSourcePosition,
                    SourceID = modelPrint.PositionIDs[i],
                    PrinterLocationID = modelPrint.PrinterLocationID
                };

                var proc = new S4.Procedures.Info.OnStorePrintProc(_userID);
                var res = proc.DoProcedure(model);

                if (res.Success)
                {
                    documentList.Add(res.FileData);
                }
                else
                {
                    return new PositionsPrintModel() { Success = false, ErrorText = res.ErrorText };
                }
            }

            var print = new S4.Report.Print();
            var mergeDocument = print.DoMergePDF(documentList);
            var fileID = Singleton<Helpers.FilesCache>.Instance.AddToCache("print.pdf", "application/pdf", mergeDocument);

            return new PositionsPrintModel() { Success = true, LocalPrint = modelPrint.LocalPrint, FileData = mergeDocument, FileID = fileID };
        }


        #region DataListController<PositionView, PositionSettings, PositionSettingsModel>

        protected override void FillData(DataListData<PositionView, PositionSettings, PositionSettingsModel> dataListData, bool toExcel)
        {
            base.FillData(dataListData, toExcel);

            if (dataListData.DataModel.Settings.PageSize == 0)
                return;

            var filterOption = dataListData.DataModel.Settings.FilterOption == null ?
                PositionSettingsModel.PositionFilterEnum.All :
                Enum.Parse<PositionSettingsModel.PositionFilterEnum>(dataListData.DataModel.Settings.FilterOption.ToString());

            dataListData.DataModel.Settings.Columns["itemsCount"]
                .SetDisallowFiltering(filterOption != PositionSettingsModel.PositionFilterEnum.NotEmpty)
                .SetDisallowSorting(filterOption != PositionSettingsModel.PositionFilterEnum.NotEmpty);

            //the quantity has been just loaded in case of OnStore filter option
            if (dataListData.DataModel.Settings.FilterOption != null &&
                (filterOption == PositionSettingsModel.PositionFilterEnum.Empty ||
                filterOption == PositionSettingsModel.PositionFilterEnum.NotEmpty))
            {
                return;
            }

            var positions = dataListData.DataModel.Data.Select(d => d.PositionID).Distinct().ToList();
            var onStore = new StoreCore.OnStore_Summary.OnStore_Summary()
                .OnPosition(positions, StoreCore.Interfaces.ResultValidityEnum.Valid);

            foreach (var item in dataListData.DataModel.Data)
                item.ItemsCount = onStore.Where(i => i.PositionID == item.PositionID).Select(i => i.ArticleID).Distinct().Count();
        }

        protected override Sql GetSql(PositionSettingsModel settings)
        {
            var filterOption = settings.FilterOption == null ?
                PositionSettingsModel.PositionFilterEnum.All : Enum.Parse<PositionSettingsModel.PositionFilterEnum>(settings.FilterOption.ToString());

            Sql sql = null;
            if (settings.PageSize == 0 || filterOption == PositionSettingsModel.PositionFilterEnum.Empty || filterOption == PositionSettingsModel.PositionFilterEnum.NotEmpty)
            {
                sql = new Sql();
                sql.Append("SELECT pos.*, onPos.itemsCount FROM s4_positionList pos");
                sql.Append(" LEFT JOIN (SELECT positionID, COUNT(*) AS itemsCount FROM [dbo].[s4_vw_onStore_Summary_Valid] GROUP BY positionID) onPos");
                sql.Append(" ON (pos.positionID = onPos.positionID)");
            }
            else
                sql = new Sql("SELECT *, 0 as itemsCount FROM s4_positionList pos");

            switch (filterOption)
            {
                case PositionSettingsModel.PositionFilterEnum.Store:
                    sql.Where("posCateg = @0", Entities.Position.PositionCategoryEnum.Store);
                    break;
                case PositionSettingsModel.PositionFilterEnum.Receive:
                    sql.Where("posCateg = @0", Entities.Position.PositionCategoryEnum.Receive);
                    break;
                case PositionSettingsModel.PositionFilterEnum.Dispatch:
                    sql.Where("posCateg = @0", Entities.Position.PositionCategoryEnum.Dispatch);
                    break;
                case PositionSettingsModel.PositionFilterEnum.Empty:
                    sql.Where("onPos.itemsCount IS NULL");
                    break;
                case PositionSettingsModel.PositionFilterEnum.NotEmpty:
                    sql.Where("onPos.itemsCount <> 0");
                    break;
                case PositionSettingsModel.PositionFilterEnum.Blocked:
                    sql.Where("posStatus = @0", Entities.Position.POS_STATUS_BLOCKED);
                    break;
            }

            if (!string.IsNullOrEmpty(settings.SectID))
            {
                sql.Where("pos.positionID IN (SELECT positionID FROM s4_positionList_sections WHERE sectID=@0)", settings.SectID);
            }

            return sql;
        }

        protected override Sql GetDetailSql(string id)
        {
            return new Sql("SELECT *, 0 as itemsCount FROM s4_positionList");
        }

        protected override PositionView SaveItem(PositionView item)
        {
            var currentItem = new Entities.Position()
            {
                HouseID = item.HouseID,
                MapPos = item.MapPos,
                PosCateg = item.PosCateg,
                PosCode = item.PosCode,
                PosDesc = item.PosDesc,
                PosAttributes = item.PosAttributes,
                PosHeight = item.PosHeight,
                PositionID = item.PositionID,
                PosStatus = item.PosStatus,
                PosX = item.PosX,
                PosY = item.PosY,
                PosZ = item.PosZ
            };

            // make history
            var originItem = new DAL.PositionDAL().Get(item.PositionID);
            if (originItem == null)
                originItem = new Entities.Position();

            var history = new Entities.PositionHistory
            {
                PositionID = item.PositionID,
                EventCode = Entities.Position.POS_EVENT_UPDATE,
                EventData = new Core.PropertiesComparer.PropertiesComparer<Entities.Position>(originItem, currentItem).JsonText,
                EntryUserID = GetUser(),
                EntryDateTime = DateTime.Now
            };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var existingItem = db
                    .Fetch<Entities.Position>("select * from s4_positionList where houseID = @0 and posX = @1 and posY = @2 and posZ = @3", item.HouseID, item.PosX, item.PosY, item.PosZ)
                    .FirstOrDefault();

                if (existingItem != null && existingItem.PositionID != item.PositionID)
                    throw new UserException($"Pozice se stejnými souřadnicemi již existuje!");

                using (var transaction = db.GetTransaction())
                {
                    //save the position
                    db.Save(currentItem);
                    item.PositionID = currentItem.PositionID;

                    //his
                    history.PositionID = item.PositionID;
                    db.Insert(history);

                    //update sections
                    var sql = new NPoco.Sql();
                    sql.Where("positionID=@0", item.PositionID);
                    var sections = db.Fetch<Entities.PositionSection>(sql);
                    //deleting sections
                    foreach (var section in sections.Where(s => !item.SectionIDs.Contains(s.SectID)))
                    {
                        db.Delete(section);
                    }
                    //inserting sections
                    var existingIDs = sections.Select(s => s.SectID).ToArray();
                    foreach (var id in item.SectionIDs.Where(i => !existingIDs.Contains(i)))
                    {
                        db.Insert(new Entities.PositionSection() { PositionID = item.PositionID, SectID = id });
                    }
                                        
                    transaction.Complete();
                }

                return item;
            }
        }

        #endregion
    }
}

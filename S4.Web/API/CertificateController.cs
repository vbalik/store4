﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Net.Http;

using S4.Entities;
using S4.Web.Models;
using S4.Web.DataList;
using NPoco;
using Microsoft.Extensions.Logging;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/certificate")]
    public class CertificateController : DataListController<Certificate, CertificateSettings, CertificateSettingsModel>
    {
        internal static TimeZoneInfo TzInfo = TimeZoneInfo.FindSystemTimeZoneById("Central European Standard Time");

        public CertificateController(IHttpContextAccessor httpContextAccessor, ILogger<DataListController<Certificate, CertificateSettings, CertificateSettingsModel>> logger) : base(httpContextAccessor, logger)
        {

        }

        protected override Sql GetSql(CertificateSettingsModel settings)
        {
            var sql = base.GetSql(settings);
            if (settings.CertificateID != 0)
            {
                sql.Where("certificateID = @0", settings.CertificateID);
            }
            return sql;
        }

        [HttpPost]
        [Route("savecertificate")]
        
        public Certificate SaveCertificate([FromBody] Certificate certificate)
        {
            certificate.ExpirationDateFrom = TimeZoneInfo.ConvertTimeFromUtc(certificate.ExpirationDateFrom, TzInfo);
            certificate.ExpirationDateTo = TimeZoneInfo.ConvertTimeFromUtc(certificate.ExpirationDateTo, TzInfo);
            
            //throws an exception if data are not valid
            CheckDataValidity(certificate);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                db.Save(certificate);
                return certificate;
            }

        }

        [HttpPost]
        [Route("infocertificate")]
        public IActionResult GetInfoCertificate([FromBody] Certificate certificate)
        {
            try
            {
                //check certificate
                X509Certificate2Collection certificates = new X509Certificate2Collection();
                certificates.Import(certificate.CertificateBinary, certificate.CertificatePassword, X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.PersistKeySet);

                StringBuilder sb = new StringBuilder();
                foreach (var item in certificates)
                {
                    sb.AppendLine($"Subject: {item.Subject}; Datum od: {item.NotBefore}; Datum do: {item.NotAfter}");
                }

                var expirationDateFrom = certificates[certificates.Count - 1].NotBefore;
                var expirationDateTo = certificates[certificates.Count - 1].NotAfter;

                return new JsonResult(
                    new
                    {
                        Message = sb.ToString(),
                        DateFrom = certificates.Count > 0 ? expirationDateFrom : DateTime.MinValue,
                        DateTo = certificates.Count > 0 ? expirationDateTo : DateTime.MinValue
                    }
                    );
            }
            catch (Exception ex)
            {
                return new JsonResult(new { Message = ex.Message, DateFrom = "", DateTo = "" });
            }


        }

    }


}

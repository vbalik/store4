﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NPoco;
using S4.Core;
using S4.Entities;
using S4.Report.Helper;
using S4.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Threading.Tasks;
using static S4.Entities.Settings.SQL4Filters;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/Report")]
    public class ReportController : Controller
    {
        [HttpPost]
        [Route("exportxls")]
        public object ExportXLS([FromBody] ExportXLSModel model)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var report = db.SingleOrDefaultById<Entities.Reports>(model.ReportID);

                if (report == null)
                {
                    throw new Exception($"Nenalezen report id: {model.ReportID}");
                }

                var reportHelper = new ReportHelper();
                var ds = reportHelper.GetDatasetFromSQL(report.Settings.SQL4FiltersList, report.Settings.SQL, model.Filters);
                // store file & return file id
                var fileID = Singleton<Helpers.FilesCache>.Instance.AddToCache("Report.xlsx", "application/octet-stream", S4.ToExcel.Helper.DataTableToExcel(ds.Item1.Tables[0]));
                return new { fileID };
            }
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("getprinterlocation")]
        public List<PrinterLocation> GetPrinterLocation([FromBody] string reportType)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //report
                var sql = new Sql();
                sql.Select("printerTypeID");
                sql.From("s4_reports");
                sql.Where("reportType = @0", reportType);
                var printerTypeIDs = db.Fetch<int>(sql);
                //printer
                sql = new Sql();
                sql.Where("printerTypeID in (@0)", printerTypeIDs);
                sql.OrderBy("printerLocationDesc");
                return db.Fetch<PrinterLocation>(sql);
            }
        }

        // GET: api/getreports
        [AllowAnonymous]
        [Route("getreports")]
        public List<Reports> GetReports(string reportType = null)
        {
            var reports = new List<Reports>();
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                reports = db.Fetch<Reports>();
            }

            // filter reports by type
            if (!string.IsNullOrEmpty(reportType))
            {
                return reports.Where(r => r.ReportType == reportType).ToList();
            }

            return reports;
        }
              

    }
}

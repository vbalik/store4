﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using S4.Entities.Models;
using S4.Web.Helper;
using S4.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;


namespace S4.Web.API
{
    [Produces("application/json")]
    public class DashboardController : Controller
    {
        private Microsoft.AspNetCore.Hosting.IHostingEnvironment _env;

        private readonly DAL.InternMessageDAL _internMessageDAL;
        private readonly DAL.DirectionAssignDAL _directionAssignDAL;
        private readonly DAL.DirectionDAL _directionDAL;

        public DashboardController(DAL.InternMessageDAL internMessageDAL, DAL.DirectionAssignDAL directionAssignDAL, DAL.DirectionDAL directionDAL, Microsoft.AspNetCore.Hosting.IHostingEnvironment env)
        {
            _internMessageDAL = internMessageDAL;
            _directionAssignDAL = directionAssignDAL;
            _directionDAL = directionDAL;
            _env = env;
        }

        // POST: api/dashboard/messages
        [HttpPost]
        [Route("api/dashboard/messages")]
        public IEnumerable<Entities.InternMessage> Messages()
        {
            return _internMessageDAL.Last20();
        }


        // POST: api/dashboard/dispatchwarnings
        [HttpPost]
        [Route("api/dashboard/dispatchwarnings")]
        public object DispatchWarnings(int dayLimit = 2)
        {
            var unprocessedDisps = _directionDAL.ScanUnprocessed(dayLimit);

            return unprocessedDisps.Select(i => new
            {
                Warning = "Nezpracovaný doklad",
                i.DirectionID,
                i.DocDate,
                DocInfo = $"{i.DocNumPrefix}/{i.DocYear}/{i.DocNumber}"
            });
        }


        // POST: api/dashboard/charts?daysBack=10
        [HttpPost]
        [Route("api/dashboard/charts")]
        public object Charts(int daysBack = 2)
        {
            var scanResult = _directionAssignDAL.Statistics(daysBack);

            var jobs = new string[] { Entities.DirectionAssign.JOB_ID_DISPATCH, Entities.DirectionAssign.JOB_ID_DISPATCH_CHECK, Entities.DirectionAssign.JOB_ID_RECEIVE, Entities.DirectionAssign.JOB_ID_STORE_IN };
            var resultDict = new Dictionary<string, object>();
            foreach (var jobID in jobs)
                resultDict.Add(jobID, CreateChartJSData(scanResult, jobID));

            return resultDict;
        }

        // POST: api/dashboard/news
        [HttpPost]
        [Route("api/dashboard/news")]
        public NewsModel NewsInfo()
        {
            return Core.Singleton<NewsInfoCache>.Instance.GetItem(_env.WebRootPath);
        }
               
        #region Private

        private object CreateChartJSData(List<DirectionAssignStatisticsModel> scanResult, string jobID)
        {
            var items = scanResult.Where(i => i.JobID == jobID).OrderByDescending(i => i.JobsCnt);
            return new
            {
                datasets = new object[] {
                    new
                    {
                        data = items.Select(i => i.JobsCnt),
                        backgroundColor = items.Select((i, index) => NextColor(index))
                    }
                },
                labels = items.Select(i => i.WorkerName)
            };
        }


        private readonly string[] COLORS = new string[] { "#003f5c", "#2f4b7c", "#665191", "#a05195", "#d45087", "#f95d6a", "#ff7c43", "#ffa600" };

        private string NextColor(int index)
        {
            return COLORS[index % COLORS.Length];
        }

        #endregion
    }
}

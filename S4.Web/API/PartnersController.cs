﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using S4.DAL;
using S4.Entities;
using S4.Web.Models;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/Partners")]
    [ExportName("Firmy")]
    [ApiController]
    public class PartnersController : Controller
    {
        // for seekPartnerCombo.js component
        [Route("searchpartners")]
        [AllowAnonymous]
        public IEnumerable<Entities.Views.PartnerView> SearchPartners([FromBody] SearchPartnersModel searchPartnerModel)
        {
            var partners = new PartnerDAL().FindAllByName(searchPartnerModel.Part);

            return partners.Select(_ => new Entities.Views.PartnerView {
                PartnerID = _.PartnerID,
                PartnerName = _.PartnerName,
                PartnerPhone = _.PartnerPhone
            }).ToList();
        }
    }
}
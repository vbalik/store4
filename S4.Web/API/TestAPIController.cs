﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using S4.Entities;
using S4.ProcedureModels.Dispatch;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/TestAPI")]
    public class TestAPIController : Controller
    {
        // GET: api/TestAPI
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [Route("print5")]
        public void Print5()
        {
            var sql = @"
            SELECT	onp.positionID, art.articleCode, art.articleDesc, quantity, CASE WHEN batchNum = '_NONE_' THEN NULL ELSE batchNum END as batchNum, expirationDate, pos.posCode, m.source_id
	FROM	dbo.s4_vw_onStore_Summary_Valid onp
	JOIN dbo.s4_positionList pos ON (onp.positionID = pos.positionID)
	JOIN dbo.s4_vw_article_base art ON (onp.artPackID = art.artPackID)
	JOIN s4_manufactList m ON (m.manufID = art.manufID)
	            WHERE	(expirationDate > '2017-01-01 00:00:00')
		            AND (pos.posCateg = 0) AND (art.manufID = 'MEDTRONI') --AND (art.sectID = 'MAT')
            ";

            Report.Print p = new Report.Print();
            p.ReportDefFilePath = @"c:\Projects\store4\Tools\Reporty\Expirace.srt";
            p.DesignMode = true;

            var variables = new List<Tuple<string, object, Type>>();
            var appVersion = Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion;
            variables.Add(new Tuple<string, object, Type>("Filters", "F1", typeof(string)));
            variables.Add(new Tuple<string, object, Type>("Version", appVersion, typeof(string)));
            variables.Add(new Tuple<string, object, Type>("RowCount", 0, typeof(int)));
            p.Variables = variables;

            p.DoPrint("Adobe PDF", sql, new List<Tuple<string, object>>());

        }

        [Route("print1")]
        public void Print1()
        {
            var direction = new Entities.Direction();
            var directionItems = new List<Entities.DirectionItem>();

            Report.Print p = new Report.Print();
            p.ReportDefFilePath = @"c:\temp\test.srt";
            p.DesignMode = true;
            p.DoPrint("Adobe PDF", direction, directionItems);

        }

        [Route("print2")]
        public void Print2()
        {
            var sql = @"
            SELECT	art.articleID,
		            lot.batchNum,
		            lot.expirationDate, 
		            SUM(onpos.quantity) AS quantity,
		            MAX(art.articleCode) AS articleCode,
		            MAX(art.articleDesc) AS articleDesc,
                    art.unitDesc
	            FROM dbo.s4_vw_onStore_Summary_Valid onpos
			            JOIN dbo.s4_vw_article_base art ON (onpos.artPackID = art.artPackID)
			            JOIN dbo.s4_positionList pos ON (onpos.positionID = pos.positionID)
			            JOIN dbo.s4_storeMove_lots lot ON (lot.artPackID = art.artPackID)
	            WHERE	(manufID = 'HARTMCZZ') AND (pos.posCateg = 0)
	            GROUP BY
		            art.articleID, 
		            lot.batchNum, 
		            lot.expirationDate,
                    art.unitDesc
	            ORDER BY
		            MAX(art.articleCode),
		            lot.batchNum
            ";

            Report.Print p = new Report.Print();
            p.ReportDefFilePath = @"c:\Projects\store4\Tools\Reporty\StavSkladuPodleVyrobce.srt";
            p.DesignMode = true;

            var variables = new List<Tuple<string, object, Type>>();
            var appVersion = Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion;
            variables.Add(new Tuple<string, object, Type>("Filters", "F1", typeof(string)));
            variables.Add(new Tuple<string, object, Type>("Version", appVersion, typeof(string)));
            variables.Add(new Tuple<string, object, Type>("RowCount", 0, typeof(int)));
            p.Variables = variables;

            p.DoPrint("Adobe PDF", sql, new List<Tuple<string, object>>());

        }

        [Route("print2b")]
        public void Print2b()
        {
            var sql = @"
            SELECT	
		MAX(art.articleCode) AS articleCode,
		MAX(art.articleDesc) AS articleDesc,
		SUM(onpos.quantity) AS quantity,
		CASE WHEN onpos.batchNum = '_NONE_' THEN NULL ELSE onpos.batchNum END as batchNum,
		CASE WHEN onpos.expirationDate = '01.01.1900' THEN NULL ELSE onpos.expirationDate END as expirationDate,
		manuf.manufName,
        art.unitDesc
	FROM dbo.s4_vw_onStore_Summary_Valid onpos
			JOIN dbo.s4_vw_article_base art ON (onpos.artPackID = art.artPackID)
			JOIN dbo.s4_positionList pos ON (onpos.positionID = pos.positionID)
			JOIN dbo.s4_positionList_sections vaz ON vaz.positionID = onpos.positionID
			LEFT JOIN dbo.s4_manufactList manuf ON manuf.manufID = art.manufID
	WHERE	
			(onpos.positionID IN (SELECT positionID FROM [dbo].[s4_positionList_sections] WHERE vaz.sectID = 'MAT'))
			AND (pos.posCateg = 0)
	GROUP BY
		art.articleID, 
		onpos.batchNum, 
		onpos.expirationDate,
		manuf.manufName,
        art.unitDesc
	ORDER BY
		MAX(art.articleCode),
		onpos.batchNum
            ";

            Report.Print p = new Report.Print();
            p.ReportDefFilePath = @"c:\Projects\store4\Tools\Reporty\StavSkladuPodleSekce.srt";
            p.DesignMode = true;

            var variables = new List<Tuple<string, object, Type>>();
            var appVersion = Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion;
            variables.Add(new Tuple<string, object, Type>("Filters", "F1", typeof(string)));
            variables.Add(new Tuple<string, object, Type>("Version", appVersion, typeof(string)));
            variables.Add(new Tuple<string, object, Type>("RowCount", 0, typeof(int)));
            p.Variables = variables;

            p.DoPrint("Adobe PDF", sql, new List<Tuple<string, object>>());

        }

        [Route("print3")]
        public void Print3()
        {
            var sql = @"
SELECT	art.articleID,
		lot.batchNum,
		lot.expirationDate, 
		SUM(onpos.quantity) AS quantity,
		MAX(art.articleCode) AS articleCode,
		MAX(art.articleDesc) AS articleDesc,
        art.unitDesc
	FROM dbo.s4_vw_onStore_Summary_Valid onpos
			JOIN dbo.s4_vw_article_base art ON (onpos.artPackID = art.artPackID)
			JOIN dbo.s4_positionList pos ON (onpos.positionID = pos.positionID)
			JOIN dbo.s4_storeMove_lots lot ON (lot.artPackID = art.artPackID)
	WHERE	(manufID = 'ABBOTT') AND (pos.posCateg = 0)
	GROUP BY
		art.articleID, 
		lot.batchNum, 
		lot.expirationDate,
        art.unitDesc
	ORDER BY
		MAX(art.articleCode),
		lot.batchNum
";

            var v = new List<Tuple<string, object, Type>>();
            v.Add(new Tuple<string, object, Type>("Filters", "F1", typeof(string)));


            Report.Print p = new Report.Print();
            p.ReportDefFilePath = @"c:\Projects\store4\Tools\Reporty\StavSkladuPodleVyrobce.srt";
            p.Variables = v;
            p.DesignMode = true;
            p.DoPrint("adobe pdf", sql);
            //p.DoPrint2PDF("select * from s4_direction where entryDateTime between '2019-02-21 00:00:00' AND '2019-02-21 23:59:59'", "Pokus");

        }

        [Route("print4")]
        public void Print4()
        {
            var sql = @"
        DECLARE @from as datetime
        DECLARE @to as datetime

        set @from = dateadd(DD,-30,getdate());
        set @to = getdate();

        DECLARE @base TABLE
        (
        workerID	char(8),
        jobID		char(4),
        docs		int,
        items		int
        )

        INSERT INTO @base
        SELECT
        workerID,
        ass.jobID,
        COUNT(DISTINCT dr.directionID),
        COUNT(*)
        FROM	dbo.s4_direction dr WITH (NOLOCK) JOIN dbo.s4_direction_items di WITH (NOLOCK) ON (dr.directionID = di.directionID)
        JOIN dbo.s4_direction_assign ass WITH (NOLOCK) ON (di.directionID = ass.directionID)
        WHERE	(docDirection = 1)
        AND (docStatus != 'CANC')
        AND (docDate_d BETWEEN @from AND @to)
        GROUP BY
        workerID,
        ass.jobID


        INSERT INTO @base
        SELECT
        workerID,
        ass.jobID,
        COUNT(DISTINCT dr.directionID),
        COUNT(*)
        FROM	dbo.s4_direction dr WITH (NOLOCK) JOIN dbo.s4_direction_items di WITH (NOLOCK) ON (dr.directionID = di.directionID)
        JOIN dbo.s4_direction_assign ass WITH (NOLOCK) ON (di.directionID = ass.directionID)
        WHERE	(docDirection = -1)
        AND (docStatus != 'CANC')
        AND (docDate_d BETWEEN @from AND @to)
        GROUP BY
        workerID,
        ass.jobID


        INSERT INTO @base
        SELECT
        si.entryUserID,
        'MOVE',
        COUNT(DISTINCT sm.stoMoveID),
        COUNT(*) / 2
        FROM	dbo.s4_storeMove sm JOIN dbo.s4_storeMove_items si ON (sm.stoMoveID = si.stoMoveID)
        WHERE	(docType IN (2, 4, 9))
        AND (docDate_d BETWEEN @from AND @to)
        GROUP BY
        si.entryUserID


        SELECT
        workerID,
        (SELECT workerName FROM dbo.s4_workerList WHERE workerID COLLATE SQL_Czech_CP1250_CI_AS = ba.workerID) AS Jmeno,

        (SELECT SUM(docs) FROM @base WHERE workerID = ba.workerID AND jobID = 'RECE') AS Prijem_dokl,
        (SELECT SUM(items) FROM @base WHERE workerID = ba.workerID AND jobID = 'RECE') AS Prijem_pol,

        (SELECT SUM(docs) FROM @base WHERE workerID = ba.workerID AND jobID = 'STIN') AS Nasklad_dokl,
        (SELECT SUM(items) FROM @base WHERE workerID = ba.workerID AND jobID = 'STIN') AS Nasklad_pol,

        (SELECT SUM(docs) FROM @base WHERE workerID = ba.workerID AND jobID = 'DISP') AS Vychyst_dokl,
        (SELECT SUM(items) FROM @base WHERE workerID = ba.workerID AND jobID = 'DISP') AS Vychyst_pol,

        (SELECT SUM(docs) FROM @base WHERE workerID = ba.workerID AND jobID = 'DICH') AS Kontrola_dokl,
        (SELECT SUM(items) FROM @base WHERE workerID = ba.workerID AND jobID = 'DICH') AS Kontrola_pol,

        (SELECT SUM(docs) FROM @base WHERE workerID = ba.workerID AND jobID = 'MOVE') AS Preskl_dokl,
        (SELECT SUM(items) FROM @base WHERE workerID = ba.workerID AND jobID = 'MOVE') AS Preskl_pol
        FROM	@base ba
        GROUP BY
            workerID
        ORDER BY
            (SELECT workerName FROM dbo.s4_workerList WHERE workerID COLLATE SQL_Czech_CP1250_CI_AS = ba.workerID)

";

            var v = new List<Tuple<string, object, Type>>();
            v.Add(new Tuple<string, object, Type>("Filters", "F1", typeof(string)));

            Report.Print p = new Report.Print();
            p.ReportDefFilePath = @"c:\Projects\store4\Tools\Reporty\praceSkladniku.srt";
            p.Variables = v;
            p.DesignMode = true;
            p.DoPrint("adobe pdf", sql);
            //p.DoPrint2PDF("select * from s4_direction where entryDateTime between '2019-02-21 00:00:00' AND '2019-02-21 23:59:59'", "Pokus");

        }

        [Route("vyrobcenasklade")]
        public void Vyrobcenasklade()
        {
            var sql = @"
            DECLARE @manufID varchar(100);
            DECLARE @sectID  varchar(100);
            SET @manufID = 'HARTMANN';
            SET @sectID = 'MAT';

            DECLARE @base TABLE (    articleID int,    positionID int,    quantity numeric(38,4)   )
            INSERT INTO @base (articleID, positionID, quantity)

            SELECT    art.articleID,    onpos.positionID,    SUM(quantity) AS quantity
            FROM dbo.s4_vw_onStore_Summary_Valid onpos
            JOIN dbo.s4_vw_article_base art ON (onpos.artPackID = art.artPackID)
            JOIN dbo.s4_positionList pos ON (onpos.positionID = pos.positionID)
            WHERE (art.manufID = @manufID)    AND (pos.posCateg = 0) 
            AND (     (onpos.positionID IN (SELECT positionID FROM dbo.s4_positionList_sections WHERE sectID = @sectID))     OR (@sectID IS NULL)    )
            GROUP BY    articleID,    onpos.positionID

            SELECT art.articleCode, art.articleDesc, onpos.quantity, onpos.posCnt
            FROM (    SELECT     articleID,     SUM(quantity) AS quantity,     COUNT(positionID) AS posCnt
            FROM @base base
            GROUP BY     articleID    ) onpos
            JOIN dbo.s4_articleList art ON (onpos.articleID = art.articleID)
            ORDER BY    art.articleCode
            ";

            Report.Print p = new Report.Print();
            p.ReportDefFilePath = @"c:\Projects\store4\Tools\Reporty\vyrobceNaSklade.srt";
            p.DesignMode = true;

            var variables = new List<Tuple<string, object, Type>>();
            var appVersion = Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion;
            variables.Add(new Tuple<string, object, Type>("Filters", "F1", typeof(string)));
            variables.Add(new Tuple<string, object, Type>("Version", appVersion, typeof(string)));
            variables.Add(new Tuple<string, object, Type>("RowCount", 0, typeof(int)));
            p.Variables = variables;

            p.DoPrint("Adobe PDF", sql);

        }

        [Route("obsahpozice")]
        public void ObsahPozice()
        {
            var sql = @"
            DECLARE @sectID VARCHAR(100);
            DECLARE @manufID VARCHAR(100);
            DECLARE @posCode VARCHAR(16);

            SET @sectID = 'MAT';
            SET @manufID = 'ABIOMED';
            SET @posCode = '1T01A';

            DECLARE @base TABLE (    articleID int,    posCode varchar(16),    quantity numeric(38,4)   )
            INSERT INTO @base (articleID, posCode, quantity)

            SELECT    art.articleID,    pos.posCode,    SUM(quantity) AS quantity
            FROM dbo.s4_vw_onStore_Summary_Valid onpos
            JOIN dbo.s4_vw_article_base art ON (onpos.artPackID = art.artPackID)
            JOIN dbo.s4_positionList pos ON (onpos.positionID = pos.positionID)
            WHERE (LEN(@posCode) = 0 OR pos.posCode = @posCode)
            AND (LEN(@manufID) = 0 OR art.manufID = @manufID)    AND (pos.posCateg = 0) 
            AND (     (onpos.positionID IN (SELECT positionID FROM dbo.s4_positionList_sections WHERE (LEN(@sectID) = 0 OR sectID = @sectID)))     OR (@sectID IS NULL)    )
            GROUP BY    articleID,    pos.posCode

            SELECT art.articleCode, art.articleDesc, onpos.quantity
            FROM (    SELECT     articleID,     SUM(quantity) AS quantity
            FROM @base base
            GROUP BY     articleID    ) onpos
            JOIN dbo.s4_articleList art ON (onpos.articleID = art.articleID)
            ORDER BY    art.articleCode
            ";

            Report.Print p = new Report.Print();
            p.ReportDefFilePath = @"c:\Projects\store4\Tools\Reporty\obsahpozice.srt";
            p.DesignMode = true;

            var variables = new List<Tuple<string, object, Type>>();
            var appVersion = Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion;
            variables.Add(new Tuple<string, object, Type>("Filters", "F1", typeof(string)));
            variables.Add(new Tuple<string, object, Type>("Version", appVersion, typeof(string)));
            variables.Add(new Tuple<string, object, Type>("RowCount", 0, typeof(int)));
            p.Variables = variables;

            p.DoPrint("Adobe PDF", sql, new List<Tuple<string, object>>());

        }

        [Route("obsahpozice2")]
        public void ObsahPozice2()
        {
            var sql = @"
            DECLARE @posCode VARCHAR(16);

            SET @posCode = '1T01A';

            SELECT art.articleCode, MAX(art.articleDesc) as articleDesc, onpos.quantity, CASE WHEN batchNum = '_NONE_' THEN NULL ELSE onpos.batchNum END as batchNum, 
            CASE WHEN onpos.expirationDate = '1900-01-01 00:00:00' THEN NULL ELSE onpos.expirationDate END as expirationDate, art.unitDesc
            FROM dbo.s4_vw_onStore_Summary_Valid onpos
            JOIN dbo.s4_vw_article_base art ON (onpos.artPackID = art.artPackID)
            JOIN dbo.s4_positionList pos ON (onpos.positionID = pos.positionID)
            WHERE (pos.posCode = @posCode)  AND (pos.posCateg = 0) 
            GROUP BY art.articleCode, art.unitDesc, onpos.quantity, onpos.batchNum, onpos.expirationDate
            ORDER BY art.articleCode
            ";

            Report.Print p = new Report.Print();
            p.ReportDefFilePath = @"c:\Projects\store4\Tools\Reporty\obsahpozice2.srt";
            p.DesignMode = true;

            var variables = new List<Tuple<string, object, Type>>();
            var appVersion = Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion;
            variables.Add(new Tuple<string, object, Type>("Filters", "F1", typeof(string)));
            variables.Add(new Tuple<string, object, Type>("Version", appVersion, typeof(string)));
            variables.Add(new Tuple<string, object, Type>("RowCount", 0, typeof(int)));
            p.Variables = variables;

            p.DoPrint("Adobe PDF", sql, new List<Tuple<string, object>>());

        }

        [Route("obsahpozice3")]
        public void ObsahPozice3()
        {
            var sql = @"
           
            declare @emptyRow as int;
            set @emptyRow = 1;

            SELECT CASE @emptyRow WHEN 1 THEN ROW_NUMBER() OVER(ORDER BY articleCode ASC) ELSE 1 END AS RowNum, pos.posCode, art.articleCode, MAX(art.articleDesc) as articleDesc, SUM(onpos.quantity) as quantity, CASE WHEN batchNum = '_NONE_' THEN NULL ELSE onpos.batchNum END as batchNum,
            art.unitDesc, CASE WHEN onpos.expirationDate = '1900-01-01 00:00:00' THEN NULL ELSE onpos.expirationDate END as expirationDate
            FROM dbo.s4_vw_onStore_Summary_Valid onpos
            JOIN dbo.s4_vw_article_base art ON (onpos.artPackID = art.artPackID)
            JOIN dbo.s4_positionList pos ON (onpos.positionID = pos.positionID)
            WHERE (pos.posCateg = 0) AND art.manufID = 'CHEIRON'
            GROUP BY art.articleCode, art.unitDesc, onpos.batchNum, onpos.expirationDate, pos.posCode, pos.posDesc
            ORDER BY art.articleCode, onpos.batchNum
            ";

            Report.Print p = new Report.Print();
            p.ReportDefFilePath = @"c:\Projects\store4\Tools\Reporty\obsahpozice3.srt";
            p.DesignMode = true;

            var variables = new List<Tuple<string, object, Type>>();
            var appVersion = Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion;
            variables.Add(new Tuple<string, object, Type>("Filters", "F1", typeof(string)));
            variables.Add(new Tuple<string, object, Type>("Version", appVersion, typeof(string)));
            variables.Add(new Tuple<string, object, Type>("RowCount", 0, typeof(int)));
            p.Variables = variables;

            p.DoPrint("Adobe PDF", sql, new List<Tuple<string, object>>());

        }

        [Route("prijemvydej")]
        public void prijemVydej()
        {
            var sql = @"
            declare @docDirection as int;
            declare @batchNum as varchar(100);
            declare @articleID as int;
            declare @od as datetime;
            declare @do as datetime;

            set @docDirection = -1 --out
            set @batchNum = '3511218';
            --set @batchNum = '';
            set @articleID = 101865;
            set @od = '2019-04-29';
            set @do = '2019-05-03';

          SELECT CASE WHEN lot.batchNum = '_NONE_' THEN NULL ELSE lot.batchNum END as batchNum,
	        lot.expirationDate, smi.quantity, sm.docDate, sm.docInfo as doklad, d.docInfo as prikaz, p.partnerName, ap.packDesc
	        FROM s4_storeMove_items smi (nolock)
	        LEFT JOIN s4_storeMove sm on smi.stoMoveID = sm.stoMoveID
	        LEFT JOIN s4_direction d on d.directionID = sm.parent_directionID
	        LEFT JOIN s4_storeMove_lots lot on lot.stoMoveLotID = smi.stoMoveLotID
	        LEFT JOIN s4_articleList_packings ap on ap.artPackID = lot.artPackID
	        LEFT JOIN s4_partnerList p on p.partnerID = d.partnerID
	        WHERE d.docDirection = @docDirection AND lot.batchNum = @batchNum AND ap.articleID = @articleID AND sm.docDate between @od AND @do
	        AND sm.docType = CASE WHEN @docDirection = -1 THEN 5 ELSE 1 END
	        AND smi.itemValidity = 100
	        GROUP BY lot.batchNum, lot.expirationDate, smi.quantity, sm.docDate, sm.docInfo, d.docInfo, p.partnerName, ap.packDesc
	        ORDER BY sm.docDate;
            ";

            Report.Print p = new Report.Print();
            p.ReportDefFilePath = @"c:\Projects\store4\Tools\Reporty\PrijemVydejDleSarzeKarty.srt";
            p.DesignMode = true;

            var variables = new List<Tuple<string, object, Type>>();
            var appVersion = Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion;
            variables.Add(new Tuple<string, object, Type>("Filters", "F1", typeof(string)));
            variables.Add(new Tuple<string, object, Type>("Version", appVersion, typeof(string)));
            variables.Add(new Tuple<string, object, Type>("RowCount", 0, typeof(int)));
            p.Variables = variables;

            p.DoPrint("Adobe PDF", sql, new List<Tuple<string, object>>());

        }

        [Route("pohybykekartam")]
        public void PrintPohybyKeKartam()
        {
            var sql = @"
           

            SELECT TOP 100
	        --Kod výrobku, název výrobku, šarže, expirace, partner, datum, množství, doklad
	        ar.articleCode, ar.articleDesc,
	        lot.batchNum, lot.expirationDate,
	        pa.partnerName,
	        mo.docDate,
	        CASE mo.docType WHEN 5 THEN - moi.quantity ELSE moi.quantity END AS quantity,
	        dir.docInfo,
            pa.partnerName,
	        paa.addrName + ' ' + paa.addrLine_1 + ' ' + paa.addrLine_2 + ' ' + paa.addrCity + ' ' + paa.addrPhone + ' ' + paa.addrRemark + ' ' + paa.deliveryInst as partnerAddress,
	        ar.brand,
            ar.unitDesc

            FROM	dbo.s4_storeMove mo
		    JOIN dbo.s4_storeMove_Items moi ON mo.stoMoveID = moi.stoMoveID
		    JOIN dbo.s4_storeMove_lots lot ON moi.stoMoveLotID = lot.stoMoveLotID
		    JOIN dbo.s4_vw_article_Base ar ON (lot.artPackID = ar.artPackID)
		    JOIN dbo.s4_direction dir ON (mo.parent_directionID = dir.directionID)
		    JOIN dbo.s4_partnerList pa ON (dir.partnerID = pa.partnerID)
		    LEFT JOIN dbo.s4_partnerList_addresses paa ON (dir.partAddrID = paa.partAddrID)
		    JOIN dbo.s4_manufactList man ON (man.manufID = ar.manufID)
        WHERE	
	        (
		        (
			        (mo.docType = 5) 
			        AND (moi.itemDirection = -1) 
			        AND (moi.itemValidity = 100)
			        AND (mo.parent_directionID IN (SELECT directionID FROM dbo.s4_direction WHERE docNumPrefix <> 'DLL'))
		        )
		        OR 
		        (
			        (mo.docType = 3) 
			        AND (moi.itemDirection = 1) 
			        AND (moi.itemValidity = 100)
		        )
	        )
	        
        ORDER BY
	        ar.articleCode,
	        lot.batchNum,
	        mo.docDate
            ";

            Report.Print p = new Report.Print();
            p.ReportDefFilePath = @"c:\Projects\store4\Tools\Reporty\PohybyKeKartam.srt";
            p.DesignMode = true;

            var variables = new List<Tuple<string, object, Type>>();
            var appVersion = Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion;
            variables.Add(new Tuple<string, object, Type>("Filters", "F1", typeof(string)));
            variables.Add(new Tuple<string, object, Type>("Version", appVersion, typeof(string)));
            variables.Add(new Tuple<string, object, Type>("RowCount", 0, typeof(int)));
            p.Variables = variables;

            p.DoPrint("Adobe PDF", sql, new List<Tuple<string, object>>());

        }

        [Route("vyuzitipozic")]
        public void VyuzitiPozic()
        {
            var sql = @"
            SELECT r.positionID, r.posCode, r.posDesc, r.houseID, r.posCateg, r.posStatus, r.posHeight, r.posAttributes, r.posX, r.posY, r.posZ, r.mapPos, r.movesCnt,r.onStore, h.houseDesc
	            FROM [dbo].[prom_fn_posUseReportBase] ('2019-01-01', '2019-02-01') as r
	            LEFT JOIN s4_houseList as h on h.houseID = r.houseID
            ";

            Report.Print p = new Report.Print();
            p.ReportDefFilePath = @"c:\Projects\store4\Tools\Reporty\VyuzitiPozic.srt";
            p.DesignMode = true;

            var variables = new List<Tuple<string, object, Type>>();
            var appVersion = Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion;
            variables.Add(new Tuple<string, object, Type>("Filters", "F1", typeof(string)));
            variables.Add(new Tuple<string, object, Type>("Version", appVersion, typeof(string)));
            variables.Add(new Tuple<string, object, Type>("RowCount", 0, typeof(int)));
            p.Variables = variables;

            p.DoPrint("Adobe PDF", sql, new List<Tuple<string, object>>());

        }

        [Route("stavskladukdatu")]
        public void StavSKladuKDatu()
        {
            var sql = @"
            DECLARE @date VARCHAR(100);
            DECLARE @sectID VARCHAR(100);
            DECLARE @manufID VARCHAR(100);
            DECLARE @articleID VARCHAR(100);
            
            SELECT SUM(quantity) as quantity, batchNum, expirationDate, articleCode, articleDesc, manufName, manufID, packDesc
            FROM
            (
	            SELECT SUM(i.quantity * i.itemDirection) as quantity, CASE WHEN lots.batchNum = '_NONE_' THEN NULL ELSE lots.batchNum END as batchNum,
	            CASE WHEN CONVERT(date, lots.expirationDate) = '1900-01-01' THEN NULL ELSE lots.expirationDate END as expirationDate, a.articleCode, a.articleDesc, man.manufName, man.manufID, p.packDesc

	            FROM dbo.s4_storeMove_items as i (nolock)
	            LEFT JOIN s4_storeMove as m on m.stoMoveID = i.stoMoveID
	            LEFT JOIN dbo.s4_positionList pos ON (i.positionID = pos.positionID)

	            JOIN dbo.s4_storeMove_lots lots ON (i.stoMoveLotID = lots.stoMoveLotID)
	            LEFT JOIN s4_articleList_packings as p on lots.artPackID = p.artPackID
	            LEFT JOIN s4_articleList as a on a.articleID = p.articleID
	            LEFT JOIN s4_manufactList as man on man.manufID = a.manufID

	            WHERE i.itemValidity = 100 AND m.docDate < DATEADD(day, 1, CAST(@date AS DATETIME)) AND
	            (LEN(@sectID) = 0 OR i.positionID IN (SELECT positionID FROM [dbo].[s4_positionList_sections] WHERE sectID = @sectID)) AND (pos.posCateg = 0)
	            AND (LEN(@articleID) = 0 OR a.articleID = @articleID) AND (LEN(@manufID) = 0 OR man.manufID = @manufID) 
	            GROUP BY lots.batchNum, m.docDate, lots.expirationDate, a.articleCode, a.articleDesc, man.manufName, man.manufID, p.packDesc
	            HAVING SUM(i.quantity * itemDirection) <> 0

            ) a
            GROUP BY batchNum, expirationDate, articleCode, articleDesc, manufName, manufID, packDesc
            HAVING SUM(quantity) <> 0
	        
            ";

            Report.Print p = new Report.Print();
            p.ReportDefFilePath = @"c:\Projects\store4\Tools\Reporty\StavSkladuKDatu.srt";
            p.DesignMode = true;

            var variables = new List<Tuple<string, object, Type>>();
            var appVersion = Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion;
            variables.Add(new Tuple<string, object, Type>("Filters", "F1", typeof(string)));
            variables.Add(new Tuple<string, object, Type>("Version", appVersion, typeof(string)));
            variables.Add(new Tuple<string, object, Type>("RowCount", 0, typeof(int)));
            p.Variables = variables;

            p.DoPrint("Adobe PDF", sql, new List<Tuple<string, object>>());

        }

        [Route("exchangedirectionold")]
        public void ExchangeDirectionOld()
        {
            var sql = @"
            select docInfo, docDate from s4_direction
            where docStatus not in ('CANC', 'EXPE', 'DCHE') AND docDirection = -1 AND docDate < '2019-01-01'
            order by docDate asc
	        
            ";

            Report.Print p = new Report.Print();
            p.ReportDefFilePath = @"c:\Projects\store4\Tools\Reporty\ExchangeDirectionOld.srt";
            p.DesignMode = true;

            var variables = new List<Tuple<string, object, Type>>();
            var appVersion = Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion;
            variables.Add(new Tuple<string, object, Type>("Filters", "F1", typeof(string)));
            variables.Add(new Tuple<string, object, Type>("Version", appVersion, typeof(string)));
            variables.Add(new Tuple<string, object, Type>("RowCount", 0, typeof(int)));
            p.Variables = variables;

            p.DoPrint("Adobe PDF", sql, new List<Tuple<string, object>>());

        }

        [Route("seriovacisla")]
        public void SeriovaCisla()
        {
            var sql = @"
            
            select 'a123456' as Code2D, '???' as katCislo, MAX(a.articleDesc) as articleDesc, '???' as SUKL, MAX(a.articleCode) as articleCode, MAX(sms.serialNumber) as serialNumber, MAX(sml.batchNum) as batchNum,
            CASE WHEN MAX(sml.expirationDate) = '01.01.1900' THEN NULL ELSE MAX(sml.expirationDate) END as expirationDate, 1 as overeni
            from s4_direction d
            left join s4_storeMove sm on sm.parent_directionID = d.directionID
            left join s4_storeMove_items smi on smi.stoMoveID = sm.stoMoveID
            left join s4_storeMove_lots sml on sml.stoMoveLotID = smi.stoMoveLotID
            left join s4_articleList_packings ap on ap.artPackID = sml.artPackID
            left join s4_articleList a on a.articleID = ap.articleID
            left join s4_stoMoveItem_serials sms on sms.stoMoveItemID = smi.stoMoveItemID
            where d.directionID = 1631585
            group by a.articleID
            ";

            Report.Print p = new Report.Print();
            p.ReportDefFilePath = @"c:\Projects\store4\Tools\Reporty\SeriovaCisla.srt";
            p.DesignMode = true;

            var variables = new List<Tuple<string, object, Type>>();
            var appVersion = Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion;
            variables.Add(new Tuple<string, object, Type>("Filters", "F1", typeof(string)));
            variables.Add(new Tuple<string, object, Type>("Version", appVersion, typeof(string)));

            p.Variables = variables;

            p.DoPrint("Adobe PDF", sql, new List<Tuple<string, object>>());

        }

        [Route("print13")]
        public void Print13()
        {
            var sql = @"
            select a.articleCode, LTRIM(a.articleDesc) as articleDesc, p.packDesc, p.barCode, p.movablePack
            from s4_articleList_packings p
            left join s4_articleList a on a.articleID = p.articleID
            where p.barCode is not null and packStatus = 'AP_OK'
            order by LTRIM(a.articleDesc)
            ";

            Report.Print p = new Report.Print();
            p.ReportDefFilePath = @"c:\Projects\store4\Tools\Reporty\EAN.srt";
            p.DesignMode = true;

            var variables = new List<Tuple<string, object, Type>>();
            var appVersion = Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion;
            variables.Add(new Tuple<string, object, Type>("Filters", "F1", typeof(string)));
            variables.Add(new Tuple<string, object, Type>("Version", appVersion, typeof(string)));
            variables.Add(new Tuple<string, object, Type>("RowCount", 0, typeof(int)));
            p.Variables = variables;

            p.DoPrint("Adobe PDF", sql, new List<Tuple<string, object>>());

        }

        [Route("print14")]
        public void Print14()
        {
            var sql = @"
           DECLARE	@date	smalldatetime;
SET	@date = DATEADD(hh, -24, GETDATE())

SELECT d.docNumPrefix + '/' + docYear + '/' + CAST(docNumber AS VARCHAR(50)) as doc, d.docDate, p.partnerName, h2.workerName as userName
FROM [dbo].[s4_direction] d
left join [dbo].[s4_partnerList] p on p.partnerID = d.partnerID
left join 
	(select MAX(h.directionHistID) as directionHistID, MAX(h.entryUserID) as entryUserID, h.directionID, h.eventCode, h.eventData, w.workerName
	from s4_direction_history h
	left join s4_workerList w on w.workerID = h.entryUserID
	WHERE h.eventCode = 'STATUS_CHANGED' AND h.eventData like '%EXPE'
	group by h.directionID, h.eventCode, h.eventData, w.workerName) as h2 on h2.directionID = d.directionID 
left join s4_messageBus b on b.directionID = d.directionID
WHERE d.docDirection = -1 AND d.docStatus = 'EXPE' AND d.docNumPrefix = 'DL' AND d.docDate >= @date
AND NOT EXISTS (SELECT 1 FROM s4_direction_history WHERE directionID = d.directionID AND eventCode = 'EXPORT_DONE')
AND h2.directionHistID IS NOT NULL
AND b.messageBusID IS NULL
group by d.directionID, d.docNumPrefix, docYear, docNumber, d.docDate, p.partnerName, h2.workerName
order by d.directionID desc
            ";

            Report.Print p = new Report.Print();
            p.ReportDefFilePath = @"c:\Projects\store4\Tools\Reporty\ChybneExpediceSklad.srt";
            p.DesignMode = true;

            var variables = new List<Tuple<string, object, Type>>();
            var appVersion = Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion;
            variables.Add(new Tuple<string, object, Type>("Filters", "F1", typeof(string)));
            variables.Add(new Tuple<string, object, Type>("Version", appVersion, typeof(string)));
            variables.Add(new Tuple<string, object, Type>("RowCount", 0, typeof(int)));
            p.Variables = variables;

            p.DoPrint("Adobe PDF", sql, new List<Tuple<string, object>>());

        }

        [Route("printid37")]
        public void PrintID37()
        {
            var sql = @"
            DECLARE	@expirationDate	smalldatetime;
	        SET	@expirationDate = DATEADD(m, CAST(12 AS INT), GETDATE())

	        SELECT	onp.positionID, art.articleCode, art.articleDesc, quantity, CASE WHEN batchNum = '_NONE_' THEN NULL ELSE batchNum END as batchNum, expirationDate, pos.posCode, m.source_id
	        FROM	dbo.s4_vw_onStore_Summary_Valid onp
	        JOIN dbo.s4_positionList pos ON (onp.positionID = pos.positionID)
	        JOIN dbo.s4_vw_article_base art ON (onp.artPackID = art.artPackID)
	        JOIN s4_manufactList m ON (m.manufID = art.manufID)
	        WHERE (expirationDate <= @expirationDate AND expirationDate > '1900-01-01 00:00:00') AND (pos.posCateg = 0) 
	        ORDER BY expirationDate, art.articleCode, pos.posCode
            ";

            Report.Print p = new Report.Print();
            p.ReportDefFilePath = @"c:\Projects\store4\Tools\Reporty\Expirace.srt";
            p.DesignMode = true;

            var variables = new List<Tuple<string, object, Type>>();
            var appVersion = Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion;
            variables.Add(new Tuple<string, object, Type>("Filters", "F1", typeof(string)));
            variables.Add(new Tuple<string, object, Type>("Version", appVersion, typeof(string)));
            variables.Add(new Tuple<string, object, Type>("RowCount", 0, typeof(int)));
            p.Variables = variables;

            p.DoPrint("Adobe PDF", sql, new List<Tuple<string, object>>());

        }

        [Route("testprintid47")]
        public void Test47()
        {
            var sql = @"
            declare @od as datetime
            declare @do as datetime
            declare @sectID as varchar(100)

            set @od = '2023-09-01'
            set @do = '2023-10-01'
            set @sectID = 'NLOG,CZZ';

            SELECT * into #temp2 FROM STRING_SPLIT(@sectID, ',');

            SELECT
		            --Kod výrobku, název výrobku, výrobce, šarže, expirace, datum, množství, doklad
		            ar.articleCode,
		            ar.articleDesc,
		            man.manufName,
		            CASE WHEN lot.batchNum = '_NONE_' THEN NULL ELSE lot.batchNum END as batchNum,
		            lot.expirationDate,
		            mo.docDate,
		            CASE mo.docType WHEN 5 THEN - moi.quantity ELSE moi.quantity END AS quantity,
		            dir.docInfo
	            FROM	dbo.s4_storeMove mo
			            JOIN dbo.s4_storeMove_Items moi (nolock) ON mo.stoMoveID = moi.stoMoveID
			            JOIN dbo.s4_storeMove_lots lot (NOLOCK) ON moi.stoMoveLotID = lot.stoMoveLotID
			            JOIN dbo.s4_vw_article_Base ar (NOLOCK) ON (lot.artPackID = ar.artPackID)
			            JOIN dbo.s4_direction dir (NOLOCK) ON (mo.parent_directionID = dir.directionID)
			            JOIN dbo.s4_manufactList (NOLOCK) man ON man.manufID = ar.manufID
	            WHERE   dir.docDirection = 1 
			            AND dir.docStatus = 'CSTI' --Naskladnění - hotovo
		                AND (@sectID IS NULL OR moi.positionID IN (SELECT positionID FROM dbo.s4_positionList_sections WHERE (sectID IN (SELECT value FROM #temp2 WHERE RTRIM(value) <> '''') ))) 
			            AND dir.docDate between @od AND @do
			            AND moi.itemValidity = 100
               ORDER BY
			            ar.articleCode,
			            lot.batchNum,
			            mo.docDate

                drop table #temp2
            ";

            Report.Print p = new Report.Print();
            p.ReportDefFilePath = @"c:\Projects\store4\Tools\Reporty\ExpiracePrijemNemLog.srt";
            p.DesignMode = true;

            var variables = new List<Tuple<string, object, Type>>();
            var appVersion = Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion;
            variables.Add(new Tuple<string, object, Type>("Filters", "F1", typeof(string)));
            variables.Add(new Tuple<string, object, Type>("Version", appVersion, typeof(string)));
            variables.Add(new Tuple<string, object, Type>("Header", "Test1", typeof(string)));
            variables.Add(new Tuple<string, object, Type>("RowCount", 0, typeof(int)));
            p.Variables = variables;

            try
            {
                p.DoPrint("Adobe PDF", sql, new List<Tuple<string, object>>());
            }
            catch (Exception e)
            {

            }

        }

        [Route("testprintid50")]
        public void Test50()
        {
            var sql = @"
            --kód , název karty, poznámka, znak, stav skladu
            select a.articleCode
                     , a.articleDesc
                     , a.sign
                     , qty.quantity
                     , qty.position
                     , qty.docRemark
                from s4_articleList a

                         left join (SELECT SUM(si.[quantity] * si.[itemDirection]) AS [quantity]
                                         , lots.artPackID
                                         , ap.articleID
                                         , mov.docRemark
                                         , pos.posCode                             as position
                                    FROM [dbo].[s4_storeMove_items] si
                                             inner join s4_storeMove_lots lots on si.stoMoveLotID = lots.stoMoveLotID
                                             inner join s4_articleList_packings ap ON ap.artPackID = lots.artPackID
                                             inner join s4_storeMove mov on mov.stoMoveID = si.stoMoveID
                                             left join s4_positionList pos on si.positionID = pos.positionID
                                    WHERE [itemValidity] = 100
                                    GROUP BY lots.artPackID, ap.articleID, mov.docRemark, pos.posCode
                                    HAVING SUM(si.[quantity] * si.[itemDirection]) <> 0) qty ON qty.articleID = a.articleID

                where a.sectID = 'ARCHIV'
                  and qty.quantity > 0
            ";

            Report.Print p = new Report.Print();
            p.ReportDefFilePath = @"c:\Projects\store4\Tools\Reporty\ArchivDokumentu.srt";
            p.DesignMode = true;

            var variables = new List<Tuple<string, object, Type>>();
            var appVersion = Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion;
            variables.Add(new Tuple<string, object, Type>("Filters", "F1", typeof(string)));
            variables.Add(new Tuple<string, object, Type>("Version", appVersion, typeof(string)));
            variables.Add(new Tuple<string, object, Type>("Header", "Test1", typeof(string)));
            variables.Add(new Tuple<string, object, Type>("RowCount", 0, typeof(int)));
            p.Variables = variables;

            try
            {
                p.DoPrint("Adobe PDF", sql, new List<Tuple<string, object>>());
            }
            catch (Exception e)
            {

            }

        }
    }



}


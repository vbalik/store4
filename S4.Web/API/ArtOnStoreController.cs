﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NPoco;

using S4.Entities.Views;
using S4.StoreCore.Interfaces;
using S4.Web.DataList;
using S4.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/OnStore")]
    public class ArtOnStoreController : DataListController<ArtOnStoreView, ArtOnStoreSettings, ArtOnStoreSettingsModel>
    {
        public ArtOnStoreController(IHttpContextAccessor httpContextAccessor, ILogger<DataListController<ArtOnStoreView, ArtOnStoreSettings, ArtOnStoreSettingsModel>> logger) : base(httpContextAccessor, logger)
        {

        }

        [HttpPost("getonposition/{positionID}")]
        public Models.DataList.DataListDataModel<ArtOnStoreView, ArtOnStoreSettingsModel> GetOnPosition(int positionID)
        {
            var dataListData = new DataListData<ArtOnStoreView, ArtOnStoreSettings, ArtOnStoreSettingsModel>(
                new ArtOnStoreSettingsModel() { ArtOnStoreStyle = ArtOnStoreSettingsModel.ArtOnStoreStyleEnum.ByPosition });
            dataListData.CreateSettings(_isSuperUser);

            var onPosition = new StoreCore.OnStore_Basic.OnStore().OnPosition(positionID, ResultValidityEnum.Valid);
            dataListData.DataModel.Data = GetData(onPosition);
            dataListData.DataModel.Settings.ID = positionID;

            return dataListData.DataModel;
        }

        [Route("getonpositionm/{positionID}")]
        [AllowAnonymous]
        public List<ArtOnStoreView> GetOnPositionM(int positionID)
        {
            var onPosition = new StoreCore.OnStore_Basic.OnStore().OnPosition(positionID, ResultValidityEnum.Valid);
            return GetData(onPosition).ToList();
        }

        [HttpPost("getbyarticle/{articleID}")]
        public Models.DataList.DataListDataModel<ArtOnStoreView, ArtOnStoreSettingsModel> GetByArticle(int articleID)
        {
            return GetDataByArticle(articleID, false);
        }

        [HttpPost("getbyarticlestorepos/{articleID}")]
        public Models.DataList.DataListDataModel<ArtOnStoreView, ArtOnStoreSettingsModel> GetByArticleStorePos(int articleID)
        {
            return GetDataByArticle(articleID, true);
        }

        [Route("getbyarticlem/{articleID}")]
        [AllowAnonymous]
        public List<ArtOnStoreView> GetByArticleM(int articleID)
        {
            return GetData(articleID, true).ToList();
        }

        [Route("getbycarrierm/{carrienum}")]
        [AllowAnonymous]
        public List<ArtOnStoreView> GetByCarrierM(string carrieNum)
        {
            var onStore = new StoreCore.OnStore_Basic.OnStore().ByCarrier(carrieNum, ResultValidityEnum.Valid);
            return GetData(onStore).ToList();
        }

        [Route("getbycarriermp")]
        [HttpPost]
        [AllowAnonymous]
        public List<ArtOnStoreView> GetByCarrierMP([FromBody] string carrieNum)
        {
            var onStore = new StoreCore.OnStore_Basic.OnStore().ByCarrier(carrieNum, ResultValidityEnum.Valid);
            return GetData(onStore).ToList();
        }

        protected override void FillData(DataListData<ArtOnStoreView, ArtOnStoreSettings, ArtOnStoreSettingsModel> dataListData, bool toExcel)
        {
            dataListData.CreateSettings(_isSuperUser);

            List<OnStoreItemFull> onPosition = null;

            switch (dataListData.DataModel.Settings.ArtOnStoreStyle)
            {
                case ArtOnStoreSettingsModel.ArtOnStoreStyleEnum.ByArticle:
                    onPosition = new StoreCore.OnStore_Basic.OnStore().ByArticle(dataListData.DataModel.Settings.ID, ResultValidityEnum.Valid);
                    break;
                case ArtOnStoreSettingsModel.ArtOnStoreStyleEnum.ByPosition:
                    onPosition = new StoreCore.OnStore_Basic.OnStore().OnPosition(dataListData.DataModel.Settings.ID, ResultValidityEnum.Valid);
                    break;
            }

            if (dataListData.DataModel.Settings.StorePosOnly)
                dataListData.DataModel.Data = GetData(onPosition.Where(i => i.Position.PosCateg == Entities.Position.PositionCategoryEnum.Store));
            else
                dataListData.DataModel.Data = GetData(onPosition);

            if (toExcel)
            {
                var ordFields = dataListData.DataModel.Settings.Columns
                   .Where(c => c.SortMode != Models.DataList.DataListColumnModel.SortModeEnum.smNone)
                   .OrderByDescending(c => c.SortOrder)
                   .ToList();
                //apply sorting by setting
                dataListData.DataModel.Settings.Columns
                    .Where(c => c.SortMode != Models.DataList.DataListColumnModel.SortModeEnum.smNone)
                    .OrderByDescending(c => c.SortOrder)
                    .ToList()
                    .ForEach(c =>
                    {
                        //prop name by field name
                        var propName = c.FieldName[0].ToString().ToUpper() + c.FieldName.Substring(1);
                        //in case of expiration sort by expiration date
                        if (propName == "ExpirationDesc")
                            propName = "ExpirationDate";
                        var property = typeof(ArtOnStoreView).GetProperty(propName);
                        if (property != null)
                        {
                            if (c.SortMode == Models.DataList.DataListColumnModel.SortModeEnum.smAscending)
                                dataListData.DataModel.Data = dataListData.DataModel.Data.OrderBy(d => property.GetValue(d)).ToList();
                            else
                                dataListData.DataModel.Data = dataListData.DataModel.Data.OrderByDescending(d => property.GetValue(d)).ToList();
                        }
                    });
                dataListData.DataModel.Data = dataListData.DataModel.Data.ToList();
            }
        }

        private Models.DataList.DataListDataModel<ArtOnStoreView, ArtOnStoreSettingsModel> GetDataByArticle(int articleID, bool storePosOnly)
        {
            var dataListData = new DataListData<ArtOnStoreView, ArtOnStoreSettings, ArtOnStoreSettingsModel>(
                new ArtOnStoreSettingsModel() { ArtOnStoreStyle = ArtOnStoreSettingsModel.ArtOnStoreStyleEnum.ByArticle });
            dataListData.CreateSettings(_isSuperUser);

            dataListData.DataModel.Data = GetData(articleID, storePosOnly);

            dataListData.DataModel.Settings.ID = articleID;

            return dataListData.DataModel;
        }

        private IEnumerable<ArtOnStoreView> GetData(int articleID, bool storePosOnly)
        {
            var onPosition = new StoreCore.OnStore_Basic.OnStore().ByArticle(articleID, ResultValidityEnum.Valid);
            if (storePosOnly)
                return GetData(onPosition.Where(i => i.Position.PosCateg == Entities.Position.PositionCategoryEnum.Store));
            else
                return GetData(onPosition);
        }

        private IEnumerable<ArtOnStoreView> GetData(IEnumerable<StoreCore.Interfaces.OnStoreItemFull> onStore)
        {
            var data = new List<ArtOnStoreView>();
            foreach (var item in onStore)
            {
                data.Add(new ArtOnStoreView()
                {
                    ArticleCode = item.Article.ArticleCode,
                    ArticleDesc = item.Article.ArticleDesc,
                    UnitDesc = item.Article.UnitDesc,
                    ArticleID = item.ArticleID,
                    PositionID = item.PositionID,
                    PosCode = item.Position.PosCode,
                    Quantity = item.Quantity,
                    UseBatch = item.Article.UseBatch,
                    BatchNum = item.BatchNum,
                    UseExpiration = item.Article.UseExpiration,
                    ExpirationDate = item.ExpirationDate,
                    CarrierNum = item.CarrierNum,
                    SpecFeatures = item.Article.SpecFeatures,
                    ArtPackID = item.ArticlePacking.ArtPackID,
                    PackDesc = item.ArticlePacking.PackDesc
                });
            }
            return data;
        }
    }
}

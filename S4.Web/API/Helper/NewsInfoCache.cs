﻿using Newtonsoft.Json;
using S4.Web.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Web;

namespace S4.Web.Helper
{
    public class NewsInfoCache : Core.Cache.CacheBase<NewsModel>
    {
        public NewsInfoCache() : base(600) //10 min
        {
        }

        public NewsModel GetItem(string webRootPath)
        {
            return base.GetOrAddExisting(webRootPath, () =>
            {
                var newsInfo = ReadNewsInfo(webRootPath);
                return newsInfo;
            });
        }

        private NewsModel ReadNewsInfo(string webRootPath)
        {
            var path = System.IO.Path.Combine(webRootPath, "news.json");
            var appVersion = Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion;

            if (!System.IO.File.Exists(path))
                return new NewsModel() { ShowNews = false };

            var jsonString = System.IO.File.ReadAllText(path);
            NewsModel model = null;

            try
            {
                model = Newtonsoft.Json.JsonConvert.DeserializeObject<NewsModel>(jsonString);
            }
            catch (JsonException) { }
            catch (Exception)
            {
                throw;
            }

            if (model == null)
                model = new NewsModel() { ShowNews = false };

            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(path))
                {
                    var hash = md5.ComputeHash(stream);
                    model.MD5 = BitConverter.ToString(hash).Replace("-", "").ToLowerInvariant();
                }
            }
            
            model.AppVersion = appVersion;
            return model;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.API.Helper
{
    public class UserException : Exception
    {
        public string FieldName { get; set; }
        public UserException()
        {
        }

        public UserException(string message, string fieldName = "")
            : base(message)
        {
            FieldName = fieldName;
        }

        public UserException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}

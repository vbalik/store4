﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Net.Http;

using S4.Entities;
using S4.Web.Models;
using S4.Web.DataList;
using NPoco;
using Microsoft.Extensions.Logging;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/directionxtra")]
    public class DirectionXtraController : DataListController<DirectionXtra, DirectionXtraSettings, DirectionXtraSettingsModel>
    {
        public DirectionXtraController(IHttpContextAccessor httpContextAccessor, ILogger<DataListController<DirectionXtra, DirectionXtraSettings, DirectionXtraSettingsModel>> logger) : base(httpContextAccessor, logger)
        {

        }

        protected override Sql GetSql([FromBody] DirectionXtraSettingsModel model)
        {
            var sql = base.GetSql(model);
            if (model.DirectionID != 0)
            {
                sql.Where("DirectionID = @0", model.DirectionID);
            }
            return sql;
        }
               
    }

    
}

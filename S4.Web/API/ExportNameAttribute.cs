﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.API
{
    public class ExportNameAttribute : Attribute
    {
        public ExportNameAttribute(string name)
        {
            Name = name;
        }

        public string Name { get; set; }
    }
}

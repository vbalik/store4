﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NPoco;

using S4.Entities.Views;
using S4.Web.DataList;
using S4.Web.Models;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/StockTakingItem")]
    public class StockTakingItemController : DataListController<StockTakingItemView, StockTakingItemSettings, StockTakingItemSettingsModel>
    {
        public StockTakingItemController(IHttpContextAccessor httpContextAccessor, ILogger<DataListController<StockTakingItemView, StockTakingItemSettings, StockTakingItemSettingsModel>> logger) : base(httpContextAccessor, logger)
        {

        }

        protected override Sql GetSql(StockTakingItemSettingsModel settings)
        {
            var sql = base.GetSql(settings);
            if (settings.StotakID != 0)
            {
                sql.Where("stotakID = @0", settings.StotakID);
            }
            return sql;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using S4.Entities;
using S4.Web.Models;
using S4.Web.DataList;
using NPoco;
using Microsoft.Extensions.Logging;
using S4.Entities.Views;


namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/booking")]
    public class BookingController : DataListController<BookingView, BookingSettings, BookingSettingsModel>
    {
        public BookingController(IHttpContextAccessor httpContextAccessor, ILogger<DataListController<BookingView, BookingSettings, BookingSettingsModel>> logger) : base(httpContextAccessor, logger)
        {

        }

        protected override Sql GetSql(BookingSettingsModel model)
        {
            var sql = base.GetSql(model);
            sql = new Sql();
            sql.Select("b.*, l.batchNum, l.expirationDate, a.articleCode, a.articleDesc, p.partnerName, pa.addrInfo2, dir.docInfo, w1.workerName as entryUserName, w2.workerName as deletedByEntryUserName");

            sql.From("s4_bookings b");
            sql.LeftJoin("s4_storeMove_lots l on l.stoMoveLotID = b.stoMoveLotID");
            sql.LeftJoin("s4_articleList_packings ap on ap.artPackID = l.artPackID");
            sql.LeftJoin("s4_articleList a on a.articleID = ap.articleID");
            sql.LeftJoin("s4_partnerList p on p.partnerID = b.partnerID");
            sql.LeftJoin("s4_partnerList_addresses pa on pa.partAddrID = b.partAddrID");
            sql.LeftJoin("s4_direction dir on dir.directionID = b.usedInDirectionID");
            sql.LeftJoin("s4_workerList w1 on w1.workerID = b.entryUserID");
            sql.LeftJoin("s4_workerList w2 on w2.workerID = b.deletedByEntryUserID");

            if (model.FilterOption != null)
            {
                var filterOption = Enum.Parse<BookingSettingsModel.BookingFilterEnum>(model.FilterOption.ToString());

                if (filterOption == BookingSettingsModel.BookingFilterEnum.Valid)
                    sql.Where("b.bookingStatus = @0", Booking.VALID);
            }

            if (model.BookingID != 0)
            {
                sql.Where("bookingID = @0", model.BookingID);
            }

            return sql;
        }
    }

}


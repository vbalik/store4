﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Net.Http;

using S4.Entities;
using S4.Web.Models;
using S4.Web.DataList;
using NPoco;
using Microsoft.Extensions.Logging;
using S4.Entities.Views;
using S4.Core;
using S4.DAL;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/message")]
    public class MessageBusController : DataListController<MessageBusView, MessageBusSettings, MessageBusSettingsModel>
    {
        public MessageBusController(IHttpContextAccessor httpContextAccessor, ILogger<DataListController<MessageBusView, MessageBusSettings, MessageBusSettingsModel>> logger) : base(httpContextAccessor, logger)
        {

        }

        protected override Sql GetSql(MessageBusSettingsModel settings)
        {
            var sqlStr = "SELECT m.*, d.docNumPrefix, d.docYear, d.docNumber from s4_messageBus m";
            sqlStr += " LEFT OUTER JOIN s4_direction d ON d.directionID = m.directionID";

            var sql = new NPoco.Sql(sqlStr);
            if (settings.MessageBusID != 0)
            {
                sql.Where("messageBusID = @0", settings.MessageBusID);
            }
            return sql;
        }
        
        [HttpPost]
        [Route("exportfile")]
        public object ExportFile([FromBody] int messageBusDocumentID)
        {
            var trans = new MessageBusDocumentDAL();
            var document = trans.GetDocumentByID(messageBusDocumentID);

            // store file & return file id
            var fileID = Singleton<Helpers.FilesCache>.Instance.AddToCache(document.DocumentName, "application/octet-stream", document.Document);
            return new { fileID };
        }

        [HttpPost]
        [Route("getids")]
        public List<int> GetIDs([FromBody] int messageBusID)
        {
            var trans = new MessageBusDocumentDAL();
            var data = trans.GetDocumentsByMessageBusID(messageBusID);

            var ids = new List<int>();
            foreach (var row in data)
            {
                ids.Add(row.MessageBusDocumentID);
            }

            return ids;
        }
    }
}

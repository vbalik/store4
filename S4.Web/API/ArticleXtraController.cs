﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Net.Http;

using S4.Entities;
using S4.Web.Models;
using S4.Web.DataList;
using NPoco;
using Microsoft.Extensions.Logging;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/articlextra")]
    public class ArticleXtraController : DataListController<ArticleXtra, ArticleXtraSettings, ArticleXtraSettingsModel>
    {
        public ArticleXtraController(IHttpContextAccessor httpContextAccessor, ILogger<DataListController<ArticleXtra, ArticleXtraSettings, ArticleXtraSettingsModel>> logger) : base(httpContextAccessor, logger)
        {

        }

        protected override Sql GetSql([FromBody] ArticleXtraSettingsModel model)
        {
            var sql = base.GetSql(model);
            if (model.ArticleID != 0)
            {
                sql.Where("ArticleID = @0", model.ArticleID);
            }
            return sql;
        }
               
    }

    
}

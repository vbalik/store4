﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NPoco;

using S4.Entities;
using S4.Entities.Views;
using S4.Web.DataList;
using S4.Web.Models;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/storemove")]
    [Authorize]
    [ExportName("Pohyby")]
    public class StoreMoveController : DataListController<StoreMoveView, StoreMoveSettings, StoreMoveSettingsModel>
    {
        public StoreMoveController(IHttpContextAccessor httpContextAccessor, ILogger<DataListController<StoreMoveView, StoreMoveSettings, StoreMoveSettingsModel>> logger) : base(httpContextAccessor, logger)
        {

        }

        protected override Sql GetSql(StoreMoveSettingsModel settings)
        {
            var sql = new Sql("select m.*, d.docNumPrefix + '/' + d.docYear + '/' + convert(nvarchar,d.docNumber) as parentDoc from s4_storeMove m left outer join s4_direction d on d.directionID = m.parent_directionID");

            if (settings.FilterOption != null)
            {
                var filterOption = Enum.Parse<StoreMoveSettingsModel.StoreMoveFilterEnum>(settings.FilterOption.ToString());
                switch (filterOption)
                {
                    case StoreMoveSettingsModel.StoreMoveFilterEnum.Receive:
                        sql.Where($"docType = {StoreMove.MO_DOCTYPE_RECEIVE}");
                        break;
                    case StoreMoveSettingsModel.StoreMoveFilterEnum.Dispatch:
                        sql.Where($"docType = {StoreMove.MO_DOCTYPE_DISPATCH}");
                        break;
                    case StoreMoveSettingsModel.StoreMoveFilterEnum.Expedition:
                        sql.Where($"docType = {StoreMove.MO_DOCTYPE_EXPED}");
                        break;
                    case StoreMoveSettingsModel.StoreMoveFilterEnum.Move:
                        sql.Where($"docType = {StoreMove.MO_DOCTYPE_MOVE}");
                        break;
                    case StoreMoveSettingsModel.StoreMoveFilterEnum.Preparation:
                        sql.Where($"docType = {StoreMove.MO_DOCTYPE_DIR_MOVE}");
                        break;
                    case StoreMoveSettingsModel.StoreMoveFilterEnum.StoreIn:
                        sql.Where($"docType = {StoreMove.MO_DOCTYPE_STORE_IN}");
                        break;
                    case StoreMoveSettingsModel.StoreMoveFilterEnum.Stotak:
                        sql.Where($"docType = {StoreMove.MO_DOCTYPE_STOCKTAKING}");
                        break;
                }
            }

            return sql;
        }

        protected override Sql GetDetailSql(string id)
        {
            return new Sql("select m.*, d.docNumPrefix + '/' + d.docYear + '/' + convert(nvarchar,d.docNumber) as parentDoc from s4_storeMove m left outer join s4_direction d on d.directionID = m.parent_directionID");
        }

        protected override void FillData(DataListData<StoreMoveView, StoreMoveSettings, StoreMoveSettingsModel> dataListData, bool toExcel)
        {
            base.FillData(dataListData, toExcel);

            var lastStoMoveItemID = Core.Singleton<DAL.Cache.TemporaryConsts>.Instance.GetConst<int>(DAL.Cache.TemporaryConsts.ConstTypeEnum.LastStoMoveItemID);
            var IDs = dataListData.DataModel.Data.Select(d => d.StoMoveID);
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //get all locked items for displayed directions
                var moveItems = new int[] { };
                if (IDs.Count() > 0)
                {
                    var sql = new NPoco.Sql($"select stoMoveID from s4_storeMove_items where stoMoveID in ({string.Join(',', IDs.ToArray())}) and stoMoveItemID <= @0", lastStoMoveItemID);

                    moveItems = db.Query<int>(sql).ToArray();
                }
                //test if in summary
                foreach (var storeMoveView in dataListData.DataModel.Data)
                {
                    storeMoveView.OnSummary = moveItems.Contains(storeMoveView.StoMoveID);
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Net.Http;

using S4.Entities;
using S4.Web.Models;
using S4.Web.DataList;
using NPoco;
using Microsoft.Extensions.Logging;
using S4.Entities.Views;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/invoice")]
    public class InvoiceController : DataListController<InvoiceView, InvoiceSettings, InvoiceSettingsModel>
    {
        public InvoiceController(IHttpContextAccessor httpContextAccessor, ILogger<DataListController<InvoiceView, InvoiceSettings, InvoiceSettingsModel>> logger) : base(httpContextAccessor, logger)
        {

        }

        protected override Sql GetSql(InvoiceSettingsModel model)
        {
            var sql = new NPoco.Sql();
            sql = new Sql("SELECT i.*, directionCount = (select COUNT(1) from s4_direction where invoiceID = i.invoiceID) FROM s4_invoice i");
                        
            if (model.InvoiceID != 0)
            {
                sql.Where("i.invoiceID = @0", model.InvoiceID);
            }
            return sql;
        }
               
    }

    
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Net.Http;

using S4.Entities;
using S4.Web.Models;
using S4.Web.DataList;
using NPoco;
using Microsoft.Extensions.Logging;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/internmessages")]
    public class InternMessagesController : DataListController<InternMessage, InternMessageSettings, InternMessageSettingsModel>
    {
        public InternMessagesController(IHttpContextAccessor httpContextAccessor, ILogger<DataListController<InternMessage, InternMessageSettings, InternMessageSettingsModel>> logger) : base(httpContextAccessor, logger)
        {

        }

        protected override Sql GetSql(InternMessageSettingsModel settings)
        {
            var sql = base.GetSql(settings);
            if (settings.IntMessID != 0)
            {
                sql.Where("intMessID = @0", settings.IntMessID);
            }
            return sql;
        }

        [HttpPost]
        [Route("getstatuses")]
        public object GetStatuses()
        {
            return S4.Entities.InternMessage.Statuses.Items;
        }
    }

    
}

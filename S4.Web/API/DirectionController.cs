﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NPoco;
using S4.Entities;
using S4.Entities.Models;
using S4.Entities.Views;
using S4.Web.Models;
using S4.Web.DataList;
using S4.Procedures.Exped;
using S4.ProcedureModels.Exped;
using S4.Entities.Helpers;
using S4.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using static S4.Entities.DeliveryDirection;
using S4.Procedures;
using S4.ProcedureModels.Delivery;
using S4.DeliveryServices;
using static S4.DeliveryServices.DataParserHelper;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/Direction")]
    [ExportName("Doklady")]
    public class DirectionController : DataListController<DirectionView, DirectionSettings, DirectionSettingsModel>
    {
        private readonly IConfiguration _configuration;
        private ILogger<DataListController<DirectionView, DirectionSettings, DirectionSettingsModel>> _logger;

        public DirectionController(IHttpContextAccessor httpContextAccessor, ILogger<DataListController<DirectionView, DirectionSettings, DirectionSettingsModel>> logger, IConfiguration configuration) : base(httpContextAccessor, logger)
        {
            _configuration = configuration;
            _logger = logger;
        }

        [HttpPost]
        [Route("downloadrequestresponse")]
        public object ExportRequestResponse([FromBody] ExportRequestResponseModel model)
        {
            try
            {
                string fileID = string.Empty;

                var requestResponseFolder = _configuration.GetSection("RequestResponseFolder").Value;

                if (string.IsNullOrEmpty(requestResponseFolder) || !Directory.Exists(requestResponseFolder))
                    throw new Exception("Není nastaven adresář *RequestResponseFolder*!");

                // store file & return file id
                var result = GetRequestResponseFile(model.DirectionID, model.Type, requestResponseFolder);

                if (result.Item1 == null)
                {
                    _logger.LogError(result.Item2);
                    return new { fileID, msg = result.Item2 };
                }
                    
                fileID = Singleton<Helpers.FilesCache>.Instance.AddToCache(result.Item2, "application/octet-stream", result.Item1);

                _logger.LogInformation(result.Item2);
                return new { fileID, msg = result.Item2 };
            }
            catch (Exception ex)
            {
                string fileID = string.Empty;
                _logger.LogError(ex.Message);
                return new { fileID, msg = ex.Message };
            }
        }

        /// <summary>
        /// returns data from assign and xtra
        /// </summary>
        /// <param name="directionID"></param>
        /// <returns>DirectionInfoBoxModel or Error</returns>
        [Route("infobox/{directionID}")]
        public object InfoBox(int directionID)
        {
            try
            {
                var infoBox = new DirectionInfoBoxModel();

                using (var db = Core.Data.ConnectionHelper.GetDB())
                {
                    var sql = new Sql().Where("directionID = @0", directionID);

                    var direction = db.SingleOrDefault<Direction>(sql);
                    if (direction == null)
                        return FormatError("Záznam nebyl nalezen!");

                    if (direction.DoNotProcess)
                    {
                        infoBox.Warning = "Zpracování se neprovádí!";
                    }

                    var assigns = db.Fetch<DirectionAssign>(sql);
                    direction.XtraData.Load(db, sql);

                    foreach (var item in assigns)
                    {
                        var workerName = new DAL.WorkerDAL().Get(item.WorkerID)?.WorkerName;
                        switch (item.JobID)
                        {
                            case DirectionAssign.JOB_ID_DISPATCH:
                                infoBox.DispatchWorker = workerName;
                                break;
                            case DirectionAssign.JOB_ID_DISPATCH_CHECK:
                                infoBox.CheckWorker = workerName;
                                break;
                            case DirectionAssign.JOB_ID_RECEIVE:
                                infoBox.ReceiveWorker = workerName;
                                break;
                            case DirectionAssign.JOB_ID_STORE_IN:
                                infoBox.StoreInWorker = workerName;
                                break;
                        }
                    }

                    var positionID = (int?)direction.XtraData[Direction.XTRA_DISPATCH_POSITION_ID];
                    if (positionID.HasValue)
                        infoBox.PosCode = new DAL.PositionDAL().Get(positionID.Value)?.PosCode;

                    var methodId = (int?)direction.XtraData[Direction.XTRA_DISP_CHECK_METHOD];
                    if (methodId.HasValue)
                        infoBox.CheckMethod = Direction.DISP_CHECK_METHODS[methodId.Value];

                    infoBox.ExpedTruck = (string)direction.XtraData[Direction.XTRA_EXPED_TRUCK];

                    if (direction.XtraData[Direction.XTRA_DISPATCH_WEIGHT] != null
                        || direction.XtraData[Direction.XTRA_DISPATCH_BOXES_CNT] != null)
                    {
                        infoBox.WeightBoxCount = new WeightBoxCount
                        {
                            Weight = (decimal?)direction.XtraData[Direction.XTRA_DISPATCH_WEIGHT],
                            BoxCount = (int?)direction.XtraData[Direction.XTRA_DISPATCH_BOXES_CNT]
                        };
                    }

                    if (!direction.XtraData.IsNull(Direction.XTRA_TRANSPORTATION_TYPE))
                        infoBox.TransportationType = direction.XtraData[Direction.XTRA_TRANSPORTATION_TYPE].ToString();

                    //ParcelRefNumber
                    if (!direction.XtraData.IsNull(Direction.XTRA_TRANSPORTATION_TYPE))
                    {
                        var deliveryProvider = new DeliveryDirection().TTYPEToDeliveryProvider(direction.XtraData[Direction.XTRA_TRANSPORTATION_TYPE].ToString());
                        var parcelRefNumbers = (from d in direction.XtraData.Data where d.XtraCode.Equals(Direction.XTRA_DELIVERY_PACK_NUMBER) select d.XtraValue).ToList();
                        foreach (var parcelRefNumber in parcelRefNumbers)
                        {
                            if (!infoBox.ParcelRefNumbers.Exists(x => x.Equals(parcelRefNumber)))
                            {
                                infoBox.ParcelRefNumbers.Add(parcelRefNumber);
                                infoBox.TrackURLs.Add($"{new ServicesFactory().GetDeliveryService().GetTrackURL(deliveryProvider.ToString())}{parcelRefNumber}");
                            }
                        }

                        if (parcelRefNumbers.Count > 0)
                        {
                            infoBox.RequestResponseFile = true;
                        }

                    }
                }

                return infoBox;
            }
            catch (Exception ex)
            {
                return FormatError(ex.Message);
            }
        }

        /// <summary>
        /// returns string with dir identification
        /// </summary>
        /// <param name="directionID"></param>
        /// <returns>string or Error</returns>
        [Route("DirectionInfo/{directionID}")]
        public object DirectionInfo(int directionID)
        {
            try
            {
                var direction = new DAL.DirectionDAL().Get(directionID);
                if (direction == null)
                    return string.Empty;
                return $"{direction.DocNumPrefix}/{direction.DocYear}/{direction.DocNumber}";
            }
            catch (Exception ex)
            {
                return FormatError(ex.Message);
            }
        }

        /// <summary>
        /// returns expedition list
        /// </summary>
        /// <returns></returns>
        [Route("Expedition/ExpedListDirects")]
        public List<DocumentInfo> ExpedListDirects()
        {
            var proc = new ExpedListDirectsProc(GetUser());
            var result = proc.DoProcedure(new ExpedListDirects());

            return result.Directions;
        }

        /// <summary>
        /// expedition done
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("Expedition/ExpedDone")]
        [HttpPost]
        public ExpedDone ExpedDone([FromBody] ExpedDone model)
        {
            var proc = new ExpedDoneProc(GetUser());
            var result = proc.DoProcedure(model);

            return result;
        }

        /// <summary>
        /// Get setting if can set weight box count data
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("Settings/SetWeightBoxCountData")]
        public bool GetSetWeightBoxCountDataSetting()
        {
            return Singleton<DAL.SettingsSet>.Instance.GetValue_Boolean(SettingsDictionary.DISPATCH_SET_WEIGHT_BOXES_CNT);
        }

        protected override Sql GetSql(DirectionSettingsModel settings)
        {
            var completedStatuses = new List<string>
            {
                DirectionItem.DI_STATUS_RECEIVE_COMPLET,
                DirectionItem.DI_STATUS_DISPATCH_SAVED,
                DirectionItem.DI_STATUS_DISP_CHECKED
            };

            var sql = new NPoco.Sql();
            sql.Append("SELECT d.*, p.partnerName, a.deliveryInst, a.addrInfo, a.addrLine_1 as deliveryAddress1, a.addrLine_2 as deliveryAddress2, a.addrCity as deliveryAddressCity, a.addrInfo2,");
            sql.Append(" mi.cnt as itemsCount");
            sql.Append(" FROM s4_direction d WITH (NOLOCK)");
            sql.Append(" LEFT OUTER JOIN s4_partnerList p WITH (NOLOCK) ON p.partnerID = d.partnerID");
            sql.Append(" LEFT OUTER JOIN s4_partnerList_addresses a WITH (NOLOCK) ON a.partAddrID = d.partAddrID");
            sql.Append(" LEFT OUTER JOIN (SELECT COUNT(*) as cnt, directionID FROM s4_direction_items WITH (NOLOCK) GROUP BY directionID) mi ON d.directionID = mi.directionID");
            sql.Where("docDirection = @0", settings.Dir == DirectionSettingsModel.DirectionEnum.In ? 1 : -1);

            if (settings.FilterOption != null)
            {
                var filterOption = Enum.Parse<DirectionSettingsModel.DirectionFilterEnum>(settings.FilterOption.ToString());
                IEnumerable<string> statuses = null;
                switch (filterOption)
                {
                    case DirectionSettingsModel.DirectionFilterEnum.NotProcessed:
                        if (settings.Dir == DirectionSettingsModel.DirectionEnum.In)
                            statuses = Direction.STATUSES_RECEIVE.Union(Direction.STATUSES_STOIN).Except(new List<string>() { Direction.DR_STATUS_STOIN_COMPLET, Direction.DR_STATUS_CANCEL });
                        else
                            statuses = Direction.STATUSES_DISPATCH.Union(Direction.STATUSES_CHECK).Except(new List<string>() { Direction.DR_STATUS_DISP_EXPED, Direction.DR_STATUS_CANCEL });
                        break;
                    case DirectionSettingsModel.DirectionFilterEnum.Receive:
                        statuses = Direction.STATUSES_RECEIVE;
                        break;
                    case DirectionSettingsModel.DirectionFilterEnum.StoreIn:
                        statuses = Direction.STATUSES_STOIN;
                        break;
                    case DirectionSettingsModel.DirectionFilterEnum.Loaded:
                        statuses = new List<string>() { Direction.DR_STATUS_LOAD };
                        break;
                    case DirectionSettingsModel.DirectionFilterEnum.Dispatch:
                        statuses = Direction.STATUSES_DISPATCH;
                        break;
                    case DirectionSettingsModel.DirectionFilterEnum.Check:
                        statuses = Direction.STATUSES_CHECK;
                        break;
                }

                if (statuses != null)
                    sql.Where($"docStatus IN ({StatusesToSQL(statuses)})");
            }

            return sql;
        }

        protected override Sql GetDetailSql(string id)
        {
            var sql = new NPoco.Sql();
            sql.Append("SELECT d.*, p.partnerName, a.deliveryInst, a.addrInfo, a.addrLine_1 as deliveryAddress1, a.addrLine_2 as deliveryAddress2, a.addrCity as deliveryAddressCity,");
            sql.Append(" (SELECT COUNT(*) FROM s4_direction_items WITH (NOLOCK) WHERE directionID = d.directionID) as itemsCount");
            sql.Append(" FROM s4_direction d WITH (NOLOCK)");
            sql.Append(" LEFT OUTER JOIN s4_partnerList p WITH (NOLOCK) ON p.partnerID = d.partnerID");
            sql.Append(" LEFT OUTER JOIN s4_partnerList_addresses a WITH (NOLOCK) ON a.partAddrID = d.partAddrID");
            return sql;
        }


        protected override void FillData(DataListData<DirectionView, DirectionSettings, DirectionSettingsModel> dataListData, bool toExcel)
        {
            base.FillData(dataListData, toExcel);

            var lastStoMoveItemID = Core.Singleton<DAL.Cache.TemporaryConsts>.Instance.GetConst<int>(DAL.Cache.TemporaryConsts.ConstTypeEnum.LastStoMoveItemID);

            // create signs
            var IDs = dataListData.DataModel.Data.Select(d => d.DirectionID);
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //get all locked items for displayed directions
                var moveItems = new int[] { };
                var conditionsStr = "directionField IN (@0)";
                object[] arguments = IDs.Cast<object>().ToArray();

                if (IDs.Count() == 0) //nothing to fill
                    return;

                (Sql condition, Sql prepareSql) = StoreCore.Helpers.MakeListCondition(conditionsStr, IDs.ToList());

                var sql = new NPoco.Sql()
                    .Append("select m.parent_directionID from s4_storeMove m WITH (NOLOCK)")
                    .Append(condition.SQL.Replace("directionField", "m.parent_directionID"), condition.Arguments)
                    .Append("and exists(select i.stoMoveItemID from s4_storeMove_items i WITH (NOLOCK) where i.stoMoveID = m.stoMoveID and i.stoMoveItemID <= @0)", lastStoMoveItemID);

                if (prepareSql != null)
                {
                    db.KeepConnectionAlive = true;
                    db.Execute(prepareSql);
                }

                moveItems = db.Query<int>(sql).ToArray();

                conditionsStr = condition.SQL;
                arguments = condition.Arguments;

                conditionsStr = conditionsStr.Replace("directionField", "directionID");

                // get xtra data from db
                var xtraDataCodes = Direction.XTRA_ITEMS_SIGNS.Union(new string[] { Direction.XTRA_MANUF_ICON, Direction.XTRA_TRANSPORTATION_TYPE });
                var xtraSql = new Sql()
                    .Append(conditionsStr, arguments)
                    .Append($"and xtraCode IN ({StatusesToSQL(xtraDataCodes)})");
                var xtraList = db.Query<DirectionXtra>(xtraSql).ToList();

                // select signs
                var xtraSigns = xtraList.Where(_ => Direction.XTRA_ITEMS_SIGNS.Contains(_.XtraCode)).ToList();
                // select manufacturer icons
                var xtraManufIcons = xtraList.Where(_ => _.XtraCode.Equals(Direction.XTRA_MANUF_ICON, StringComparison.InvariantCultureIgnoreCase)).ToList();
                // select delivery providers
                var xtraTransTypeList = xtraList.Where(_ => _.XtraCode.Equals(Direction.XTRA_TRANSPORTATION_TYPE, StringComparison.InvariantCultureIgnoreCase)).ToList();

                // get assigns
                var assSql = new Sql()
                    .Append(conditionsStr, arguments);
                var assigns = db.Query<DirectionAssign>(assSql).ToList();

                ////////////////////////////////////////////////////
                /// VERSION A
                ////////////////////////////////////////////////////
                // get items completed
                var ids = dataListData.DataModel.Data.Where(_ => _.DocStatus == Direction.DR_STATUS_STOIN_PROC || _.DocStatus == Direction.DR_STATUS_RECEIVE_PROC || _.DocStatus == Direction.DR_STATUS_DISPATCH_PROC || _.DocStatus == Direction.DR_STATUS_DISP_CHECK_PROC).Select(_ => _.DirectionID);
                var completedStatuses = new List<string>
                {
                    DirectionItem.DI_STATUS_RECEIVE_COMPLET,
                    DirectionItem.DI_STATUS_DISPATCH_SAVED,
                    DirectionItem.DI_STATUS_DISP_CHECKED
                };

                List<DirectionItemsCompletedModel> itemsCompletedCnt = null;
                if (ids.Any())
                {
                    itemsCompletedCnt = db.Fetch<DirectionItemsCompletedModel>(@"SELECT directionID, ISNULL(COUNT(*), 0) AS itemsCompleted
                         FROM s4_direction_items
                         WHERE itemStatus IN (@0)
                         AND directionID IN (@1)
                         GROUP BY directionID",
                        completedStatuses, ids);
                }
                /// END A /////////////////////////////////////////////////

                var implementedProviders = new S4.DeliveryServices.DeliveryService().GetDeliveryProviders();

                // set Signs field
                // set completed items
                foreach (var directionView in dataListData.DataModel.Data)
                {
                    var codes = xtraSigns
                        .Where(de => de.DirectionID == directionView.DirectionID)
                        .GroupBy(de => de.XtraCode, (key, items) => key)
                        .OrderBy(code => code);

                    // manufacturer signs
                    var mfSigns = xtraManufIcons
                        .Where(de => de.DirectionID == directionView.DirectionID)
                        .GroupBy(de => de.XtraValue, (key, items) => key)
                        .OrderBy(sign => sign)
                        .Select(sign => $"{Direction.XTRA_MANUF_ICON}:{sign}");

                    directionView.Signs = String.Join('|', codes.Union(mfSigns));

                    // set items completed

                    ////////////////////////////////////////////////////
                    /// VERSION B
                    ////////////////////////////////////////////////////
                    //if (directionView.DocStatus == Direction.DR_STATUS_STOIN_PROC
                    //    || directionView.DocStatus == Direction.DR_STATUS_RECEIVE_PROC
                    //    || directionView.DocStatus == Direction.DR_STATUS_DISPATCH_PROC
                    //    || directionView.DocStatus == Direction.DR_STATUS_DISP_CHECK_PROC)
                    //{
                    //    var items = new DirectionDAL().GetItemsStatus(directionView.DirectionID);
                    //    var itemsCompleted = items.Where(_ =>
                    //            _.ItemStatus == DirectionItem.DI_STATUS_RECEIVE_COMPLET
                    //            || _.ItemStatus == DirectionItem.DI_STATUS_DISPATCH_SAVED
                    //            || _.ItemStatus == DirectionItem.DI_STATUS_DISP_CHECKED)
                    //        .Sum(_ => _.Cnt);
                    //    directionView.ItemsCompleted = itemsCompleted;
                    //}
                    /// END B /////////////////////////////////////////////////

                    ////////////////////////////////////////////////////
                    /// VERSION A
                    ////////////////////////////////////////////////////
                    if (directionView.DocStatus == Direction.DR_STATUS_STOIN_PROC
                       || directionView.DocStatus == Direction.DR_STATUS_RECEIVE_PROC
                       || directionView.DocStatus == Direction.DR_STATUS_DISPATCH_PROC
                       || directionView.DocStatus == Direction.DR_STATUS_DISP_CHECK_PROC)
                    {
                        if (itemsCompletedCnt != null)
                            directionView.ItemsCompleted = itemsCompletedCnt.SingleOrDefault(_ => _.DirectionID == directionView.DirectionID)?.ItemsCompleted ?? 0;
                    }
                    /// END A /////////////////////////////////////////////////

                    // all items are completed
                    else if (directionView.DocStatus == Direction.DR_STATUS_RECEIVE_COMPLET
                    || directionView.DocStatus == Direction.DR_STATUS_STOIN_COMPLET
                    || directionView.DocStatus == Direction.DR_STATUS_DISPATCH_SAVED
                    || directionView.DocStatus == Direction.DR_STATUS_DISP_CHECK_DONE
                    || directionView.DocStatus == Direction.DR_STATUS_DISP_EXPED)
                    {
                        directionView.ItemsCompleted = directionView.ItemsCount;
                    }
                    // all items are incomplete
                    else
                    {
                        directionView.ItemsCompleted = 0;
                    }

                    // set assigned worker
                    var jobID = dataListData.DataModel.Settings.Dir == DirectionSettingsModel.DirectionEnum.In ? DirectionAssign.JOB_ID_RECEIVE : DirectionAssign.JOB_ID_DISPATCH;
                    directionView.WorkerID = assigns.Where(d => d.DirectionID == directionView.DirectionID && d.JobID == jobID).FirstOrDefault()?.WorkerID;

                    //set DeliveryImplemented
                    var tTypeItem = xtraTransTypeList.Where(de => de.DirectionID == directionView.DirectionID).FirstOrDefault();
                    if (tTypeItem == null)
                        continue;

                    var deliveryProvider = new DeliveryDirection().TTYPEToDeliveryProvider(tTypeItem.XtraValue);
                    directionView.DeliveryImplemented = implementedProviders.Contains(deliveryProvider);
                    directionView.DeliveryProvider = deliveryProvider != DeliveryProviderEnum.NONE ? deliveryProvider.ToString() : string.Empty;
                }
            }
        }


        private string StatusesToSQL(IEnumerable<string> statusList)
        {
            return string.Join(',', statusList.Select(i => $"'{i}'"));
        }

        public class ExportRequestResponseModel
        {
            public int DirectionID { get; set; }
            public SaveXMLEnum Type { get; set; }
        }

    }
}

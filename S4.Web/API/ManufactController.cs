﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NPoco;
using S4.Entities;
using S4.Web.API.Helper;
using S4.Web.DataList;
using S4.Web.Models;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/Manufact")]
    [ExportName("Vyrobci")]
    public class ManufactController : DataListController<Manufact, ManufactSettings, ManufactSettingsModel>
    {
        public ManufactController(IHttpContextAccessor httpContextAccessor, ILogger<DataListController<Manufact, ManufactSettings, ManufactSettingsModel>> logger)
        : base(httpContextAccessor, logger)
        {

        }

        protected override Manufact SaveItem(Manufact item)
        {
            if (item.IsNew)
            {
                using (var db = Core.Data.ConnectionHelper.GetDB())
                {
                    if (db.Exists<Manufact>(item.ManufID))
                        throw new UserException($"Položka \"{item.ManufID}\" již existuje!", "ManufID");
                }
            }

            return base.SaveItem(item);
        }
    }
}

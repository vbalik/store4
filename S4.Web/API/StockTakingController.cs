﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using S4.Entities;
using S4.Entities.Views;
using S4.Procedures;
using S4.Web.DataList;
using S4.Web.Models;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/StockTaking")]
    [ExportName("Inventury")]
    public class StockTakingController : DataListController<StockTakingView, StockTakingSettings, StockTakingSettingsModel>
    {
        public StockTakingController(IHttpContextAccessor httpContextAccessor, ILogger<DataListController<StockTakingView, StockTakingSettings, StockTakingSettingsModel>> logger)
        : base(httpContextAccessor, logger)
        {

        }


        [Route("ManufPositions/{manufID}")]
        public IEnumerable<Entities.Position> ManufPositions(string manufID)
        {
            return new StoreCore.OnStore_Basic.OnStore().ByManufact(manufID, StoreCore.Interfaces.ResultValidityEnum.Valid).Select(i => i.Position);
        }


        [Route("StotakPositions/{stotakID}")]
        public IEnumerable<Entities.Position> StotakPositions(int stotakID)
        {
            var sql = new NPoco.Sql("SELECT * FROM s4_positionList");
            sql.Where("positionID IN (SELECT positionID FROM s4_stocktaking_positions WHERE stotakID=@0)", stotakID);
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                return db.Fetch<Entities.Position>(sql);
            }
        }


        [Route("ArticlePositions/{articleID}")]
        public IEnumerable<Entities.Position> ArticlePositions(int articleID)
        {
            return new StoreCore.OnStore_Basic.OnStore().ByArticle(articleID, StoreCore.Interfaces.ResultValidityEnum.Valid).Select(i => i.Position);
        }

        [Route("getcanstoreadorigstate")]
        public bool GetRight()
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //check user right
                var canStoReadOrigState = db.SingleOrDefaultById<Worker>(base._userID).WorkerClass.HasFlag(Entities.Worker.WorkerClassEnum.CanStoReadOrigState);
                return canStoReadOrigState;
            }
        }

    }
}

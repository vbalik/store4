﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NPoco;

using S4.Entities.Views;
using S4.Web.DataList;
using S4.Web.Models;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/DirectionItem")]
    public class DirectionItemController : DataListController<DirectionItemView, DirectionItemSettings, DirectionItemSettingsModel>
    {
        public DirectionItemController(IHttpContextAccessor httpContextAccessor, ILogger<DataListController<DirectionItemView, DirectionItemSettings, DirectionItemSettingsModel>> logger) : base(httpContextAccessor, logger)
        {

        }

        protected override Sql GetSql(DirectionItemSettingsModel settings)
        {
            var sqlStr = "SELECT i.*, a.articleID, a.articleDesc, a.useBatch, a.useExpiration from s4_direction_items i";
            sqlStr += " LEFT OUTER JOIN s4_vw_article_base a ON a.artPackID = i.artPackID";

            var sql = new NPoco.Sql(sqlStr);
            if (settings.DirectionID != 0)
            {
                sql.Where("directionID = @0", settings.DirectionID);
            }
            return sql;
        }
    }
}

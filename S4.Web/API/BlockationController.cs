﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NPoco;
using S4.Entities;
using S4.Entities.Views;
using S4.Web.DataList;
using S4.Web.Models;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/Blockation")]
    public class BlockationController : DataListController<BlockationView, BlockationSettings, BlockationSettingsModel>
    {
        public BlockationController(IHttpContextAccessor httpContextAccessor, ILogger<DataListController<BlockationView, BlockationSettings, BlockationSettingsModel>> logger) : base(httpContextAccessor, logger)
        {

        }

        protected override void FillData(DataListData<BlockationView, BlockationSettings, BlockationSettingsModel> dataListData, bool toExcel)
        {
            dataListData.CreateSettings(_isSuperUser);

            var sqlStr = "SELECT mit.*, pos.posCode, lots.artPackID, art.packDesc, art.articleID, art.articleCode, art.articleDesc, lots.batchNum, lots.expirationDate, smo.docInfo as 'doc', dir.docInfo as 'dir', smo.docStatus";
            sqlStr += " FROM s4_storeMove_items mit";
            sqlStr += " LEFT OUTER JOIN s4_storeMove_lots lots ON lots.stoMoveLotID = mit.stoMoveLotID";
            sqlStr += " LEFT OUTER JOIN s4_vw_article_base art ON art.artPackID = lots.artPackID";
            sqlStr += " LEFT OUTER JOIN s4_positionList pos ON pos.positionID = mit.positionID";
            sqlStr += " LEFT OUTER JOIN s4_storeMove smo ON smo.stoMoveID = mit.stoMoveID";
            sqlStr += " LEFT OUTER JOIN s4_direction dir ON dir.directionID = smo.parent_directionID";

            var sql = new NPoco.Sql(sqlStr);
            sql.Where("mit.itemValidity = @0", Entities.StoreMoveItem.MI_VALIDITY_NOT_VALID);
            if (dataListData.DataModel.Settings.ArticleID != 0)
            {
                sql.Where("articleID = @0", dataListData.DataModel.Settings.ArticleID);
            }
            if (dataListData.DataModel.Settings.PositionID != 0)
            {
                sql.Where("mit.positionID = @0", dataListData.DataModel.Settings.PositionID);
            }

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var data = db.Fetch<StoreMoveItemView>(sql);
                
                // make BlockationView
                List<BlockationView> result = null;
                if (dataListData.DataModel.Settings.ArticleID != 0)
                {
                    // group by StoMoveItemID & BrotherID
                    var grouped = data.GroupBy(
                        x => (x.ItemDirection == 1) ? x.StoMoveItemID : x.BrotherID, 
                        (key, items) => new { IsMultiple = (items.Count() > 1),  First = items.First(), From = items.Where(i => i.ItemDirection == StoreMoveItem.MI_DIRECTION_OUT).SingleOrDefault(), To = items.Where(i => i.ItemDirection == StoreMoveItem.MI_DIRECTION_IN).SingleOrDefault() });

                    result = grouped.Select(i => new BlockationView()
                    {
                        StoMoveItemID = i.First.StoMoveItemID,
                        ArtPackID = i.First.ArtPackID,
                        BatchNum = i.First.BatchNum,
                        CarrierNum = i.First.CarrierNum,
                        Dir = i.First.Dir,
                        Doc = i.First.Doc,
                        DocStatus = i.First.DocStatus,
                        ExpirationDate = i.First.ExpirationDate,
                        Quantity = i.First.Quantity,
                        PositionIDFrom = (i.From == null) ? 0 : i.From.PositionID,
                        PosCodeFrom = (i.From == null) ? null : i.From.PosCode,
                        PositionIDTo = (i.To == null) ? 0 : i.To.PositionID,
                        PosCodeTo = (i.To == null) ? null : i.To.PosCode,
                        EntryUserID = i.First.EntryUserID
                    }).ToList();
                }
                else if (dataListData.DataModel.Settings.PositionID != 0)
                {
                    result = data.Select(i => new BlockationView()
                    {
                        StoMoveItemID = i.StoMoveItemID,
                        ArtPackID = i.ArtPackID,
                        ArticleID = i.ArticleID,
                        ArticleCode = i.ArticleCode,
                        ArticleDesc = i.ArticleDesc,
                        BatchNum = i.BatchNum,
                        CarrierNum = i.CarrierNum,
                        Dir = i.Dir,
                        Doc = i.Doc,
                        DocStatus = i.DocStatus,
                        ExpirationDate = i.ExpirationDate,
                        Quantity = i.Quantity,
                        EntryUserID = i.EntryUserID
                    }).ToList();
                }
                else
                    throw new NotImplementedException();

                dataListData.DataModel.Data = result.OrderBy(x => x.StoMoveItemID);
            }
        }
    }
}

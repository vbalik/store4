﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NPoco;

using S4.Entities.Views;
using S4.Web.DataList;
using S4.Web.Models;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/StoreMoveItem")]
    public class StoreMoveItemController : DataListController<StoreMoveItemView, StoreMoveItemSettings, StoreMoveItemSettingsModel>
    {
        public StoreMoveItemController(IHttpContextAccessor httpContextAccessor, ILogger<DataListController<StoreMoveItemView, StoreMoveItemSettings, StoreMoveItemSettingsModel>> logger) : base(httpContextAccessor, logger)
        {

        }

        protected override void FillData(DataListData<StoreMoveItemView, StoreMoveItemSettings, StoreMoveItemSettingsModel> dataListData, bool toExcel)
        {
            base.FillData(dataListData, toExcel);

            //clear command list if in OnStoreSummary
            if (dataListData.DataModel.Settings.StoMoveID > 0)
            {
                var storeMoveItemIDsInSummary = new StoreCore.OnStore_Summary.OnStore_Summary().GetStoreMoveItemIDsInSummary(dataListData.DataModel.Data.Select(i => i.StoMoveItemID).ToList());
                foreach (var item in dataListData.DataModel.Data)
                {
                    item.IsInSummary = storeMoveItemIDsInSummary.Contains(item.StoMoveItemID);
                }
            }
        }

        protected override Sql GetSql(StoreMoveItemSettingsModel settings)
        {
            var sqlStr = "SELECT mit.*, pos.posCode, lots.artPackID, art.packDesc, art.articleID, art.articleCode, art.articleDesc, lots.batchNum, lots.expirationDate, smo.docInfo as 'doc', dir.docInfo as 'dir', dir.directionID, smo.docStatus, smo.docDate, smo.docType";
            sqlStr += " FROM s4_storeMove_items mit WITH (NOLOCK)";
            sqlStr += " LEFT OUTER JOIN s4_storeMove_lots lots ON lots.stoMoveLotID = mit.stoMoveLotID";
            sqlStr += " LEFT OUTER JOIN s4_vw_article_base art ON art.artPackID = lots.artPackID";
            sqlStr += " LEFT OUTER JOIN s4_positionList pos ON pos.positionID = mit.positionID";
            sqlStr += " LEFT OUTER JOIN s4_storeMove smo ON smo.stoMoveID = mit.stoMoveID";
            sqlStr += " LEFT OUTER JOIN s4_direction dir ON dir.directionID = smo.parent_directionID";

            var sql = new NPoco.Sql(sqlStr);
            if (settings.ArticleID != 0)
            {
                sql.Where("articleID = @0", settings.ArticleID);
            }
            if (settings.PositionID != 0)
            {
                sql.Where("mit.positionID = @0", settings.PositionID);
            }
            if (settings.DirectionID != 0)
            {
                sql.Where("dir.directionID = @0", settings.DirectionID);
            }
            if (settings.StoMoveID != 0)
            {
                sql.Where("mit.stoMoveID = @0", settings.StoMoveID);
            }
            if (settings.ValidOnly)
            {
                sql.Where("mit.itemValidity != @0", Entities.StoreMoveItem.MI_VALIDITY_CANCELED);
            }
            return sql;
        }
    }
}

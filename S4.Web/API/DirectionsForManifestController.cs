﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NPoco;
using S4.Entities;
using S4.Web.DataList;
using S4.Web.Models;

using S4.Entities.Models;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/DirectionsForManifest")]
    [ExportName("Manifest pro spediční služby")]
    public class DirectionsForManifestController : DataListController<DirectionsForManifestModel, DirectionsForManifestSettings, DirectionsForManifestSettingsModel>
    {
        public DirectionsForManifestController(IHttpContextAccessor httpContextAccessor, ILogger<DataListController<Entities.Models.DirectionsForManifestModel, DirectionsForManifestSettings, DirectionsForManifestSettingsModel>> logger)
        : base(httpContextAccessor, logger)
        {

        }

        protected override Sql GetSql(DirectionsForManifestSettingsModel settings)
        {
            var getDirectionsForManifest = new ProcedureModels.Delivery.GetDirectionsForManifest()
            {
                DeliveryProvider = settings.DeliveryProvider
            };
            getDirectionsForManifest = new Procedures.Delivery.GetDirectionsForManifestProc(_userID).DoProcedure(getDirectionsForManifest);
            if (!getDirectionsForManifest.Success)
                throw new Exception(getDirectionsForManifest.ErrorText);

            var sql = new NPoco.Sql();
            sql.Append("SELECT d.*, p.partnerName, a.deliveryInst, a.addrInfo, 0 as itemsCount, x.xtraValue as parcels");
            sql.Append(" FROM s4_direction d WITH (NOLOCK)");
            sql.Append(" LEFT OUTER JOIN s4_partnerList p ON p.partnerID = d.partnerID");
            sql.Append(" LEFT OUTER JOIN s4_partnerList_addresses a ON a.partAddrID = d.partAddrID");
            sql.Append(" LEFT OUTER JOIN s4_direction_xtra x ON x.directionID = d.directionID AND x.xtraCode = @0", Direction.XTRA_DISPATCH_BOXES_CNT);
            sql.Where("d.directionID in (@0)", getDirectionsForManifest.Directions.ToArray());

            return sql;
        }
    }
}

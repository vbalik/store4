﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using S4.DAL;
using S4.Entities;
using S4.Web.Models;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/partnersaddress")]
    [ExportName("Adresy")]
    [ApiController]
    public class PartnersAddressController : Controller
    {
        // for seekPartnerAddressCombo.js component
        [Route("searchaddress")]
        [AllowAnonymous]
        public IEnumerable<object> SearchAddress([FromBody] SearchPartnersAddressModel searchPartnerModel)
        {
            var dal = new PartnerAddressDAL();

            List<PartnerAddress> partners;
            PartnerAddress addr = null;
            if (searchPartnerModel.Part != null && int.TryParse(searchPartnerModel.Part, out int id))
                addr = dal.GetModel(id);
            if (addr != null)
                partners = new List<PartnerAddress>() { addr };
            else
                partners = new PartnerAddressDAL().FindAllByName(searchPartnerModel.Part);

            return partners.Select(_ => new 
            {
                _.PartAddrID,
                _.PartnerID,
                _.AddrName,
                _.DeliveryInst
            }).ToList();
        }
    }
}
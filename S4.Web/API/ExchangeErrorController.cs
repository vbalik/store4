﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Net.Http;

using S4.Entities;
using S4.Web.Models;
using S4.Web.DataList;
using NPoco;
using Microsoft.Extensions.Logging;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/exchangeerror")]
    public class ExchangeErrorController : DataListController<ExchangeError, ExchangeErrorSettings, ExchangeErrorSettingsModel>
    {
        public ExchangeErrorController(IHttpContextAccessor httpContextAccessor, ILogger<DataListController<ExchangeError, ExchangeErrorSettings, ExchangeErrorSettingsModel>> logger) : base(httpContextAccessor, logger)
        {

        }

        protected override Sql GetSql(ExchangeErrorSettingsModel settings)
        {
            var sql = base.GetSql(settings);
            if (settings.ExchangeErrorID != 0)
            {
                sql.Where("exchangeErrorID = @0", settings.ExchangeErrorID);
            }
            return sql;
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using S4.Entities.StatusEngine;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using S4.Entities.Models;

namespace S4.Web.API
{
    [Produces("application/json")]
    public class ProceduresHelpersController : Controller
    {
        private readonly DAL.PositionDAL _positionDAL;
        private readonly DAL.WorkerDAL _workerDAL;
        private readonly DAL.DirectionDAL _directionDAL;
        private readonly DAL.DirectionAssignDAL _directionAssignDAL;
        private readonly DAL.Cache.PrefixCache _prefixCache;
        private readonly DAL.Cache.PositionByCategoryModelCache _positionByCategoryModelCache;
        private readonly Picking.IPlanMaker _planMaker;
        private readonly StoreCore.Interfaces.IOnStore _onStore;
        private readonly StoreCore.Interfaces.ISave _save;
        private readonly ILogger<ProceduresHelpersController> _logger;

        public ProceduresHelpersController(
            DAL.PositionDAL positionDAL, DAL.WorkerDAL workerDAL, DAL.DirectionDAL directionDAL,
            DAL.DirectionAssignDAL directionAssignDAL, DAL.Cache.PrefixCache prefixCache, DAL.Cache.PositionByCategoryModelCache positionByCategoryModelCache,
            Picking.IPlanMaker planMaker,
            StoreCore.Interfaces.IOnStore onStore,
            StoreCore.Interfaces.ISave save,
            ILogger<ProceduresHelpersController> logger)
        {
            _positionDAL = positionDAL;
            _workerDAL = workerDAL;
            _directionDAL = directionDAL;
            _directionAssignDAL = directionAssignDAL;
            _prefixCache = prefixCache;
            _positionByCategoryModelCache = positionByCategoryModelCache;
            _planMaker = planMaker;
            _onStore = onStore;
            _save = save;
            _logger = logger;
        }


        #region workers

        // POST: api/procedureshelpers/dispatchworkers
        [HttpPost]
        [Route("api/procedureshelpers/dispatchworkers")]
        public IEnumerable<object> DispatchWorkers(bool canDispatchMedicines = false, bool canDispatchCytostatics = false)
        {
            return ListWorkers(Entities.Worker.WorkerClassEnum.CanDispatch, Entities.DirectionAssign.JOB_ID_DISPATCH, Entities.Worker.WK_STATUS_SET_TO_DISPATCH, canDispatchMedicines, canDispatchCytostatics);
        }

        // POST: api/procedureshelpers/dispatchcheckworkers
        [HttpPost]
        [Route("api/procedureshelpers/dispatchcheckworkers")]
        public IEnumerable<object> DispatchCheckWorkers()
        {
            return ListWorkers(Entities.Worker.WorkerClassEnum.CanDispatch, Entities.DirectionAssign.JOB_ID_DISPATCH_CHECK, Entities.Worker.WK_STATUS_SET_TO_DISPCHECK);
        }

        // POST: api/procedureshelpers/storeinworkers
        [HttpPost]
        [Route("api/procedureshelpers/storeinworkers")]
        public IEnumerable<object> StoreInWorkers()
        {
            return ListWorkers(Entities.Worker.WorkerClassEnum.CanStoIn, Entities.DirectionAssign.JOB_ID_STORE_IN, Entities.Worker.WK_STATUS_SET_TO_RECEIVE);
        }

        #endregion


        #region positions

        // POST: api/procedureshelpers/dispatchpositions
        [HttpPost]
        [Route("api/procedureshelpers/dispatchpositions")]
        public IEnumerable<object> DispatchPositions()
        {
            return ListPositions(Entities.Position.PositionCategoryEnum.Dispatch);
        }

        // POST: api/procedureshelpers/receivepositions
        [HttpPost]
        [Route("api/procedureshelpers/receivepositions")]
        public IEnumerable<object> ReceivePositions()
        {
            return ListPositions(Entities.Position.PositionCategoryEnum.Receive);
        }


        // POST: api/procedureshelpers/receivepositions
        [HttpPost]
        [Route("api/procedureshelpers/storepositions")]
        public IEnumerable<object> StorePositions()
        {
            return ListPositions(Entities.Position.PositionCategoryEnum.Store);
        }

        #endregion


        #region statuses

        // POST: api/procedureshelpers/directionstatuslist
        [HttpPost]
        [Route("api/procedureshelpers/directionstatuslist")]
        public IEnumerable<object> DirectionStatusList(int docDirection = 0)
        {
            List<Status> result = Entities.Direction.Statuses.Items;
            if (docDirection != 0)
                result = result.Where(i => i.DocDirection == docDirection || i.DocDirection == 0).ToList();

            return result.Select(s => new { s.Code, s.Name });
        }

        // POST: api/procedureshelpers/movestatuslist
        [HttpPost]
        [Route("api/procedureshelpers/movestatuslist")]
        public IEnumerable<object> MoveStatusList()
        {
            return Entities.StoreMove.Statuses.Items.Select(s => new { s.Code, s.Name });
        }

        // POST: api/procedureshelpers/workerstatuslist
        [HttpPost]
        [Route("api/procedureshelpers/workerstatuslist")]
        public IEnumerable<object> WorkerStatusList()
        {
            return Entities.Worker.Statuses.Items.Select(s => new { s.Code, s.Name });
        }

        // POST: api/procedureshelpers/positionstatuslist
        [HttpPost]
        [Route("api/procedureshelpers/positionstatuslist")]
        public IEnumerable<object> PositionStatusList()
        {
            return Entities.Position.Statuses.Items.Select(s => new { s.Code, s.Name });
        }

        #endregion


        [HttpPost]
        [Route("api/procedureshelpers/testpicking/{id}")]
        public Picking.PickingResult TestPicking(int id)
        {
            var direction = _directionDAL.GetDirectionAllData(id);
            var prefix = _prefixCache.GetItem(Entities.Direction.DOC_CLASS, direction.DocNumPrefix);
            var prefixSettings = (prefix != null) ? prefix.PrefixSettings : Entities.Direction.DefaultPrefixSettings;

            // remove already done items
            var dispatchPositionsModel = _positionByCategoryModelCache.GetItem(Entities.Position.PositionCategoryEnum.Dispatch);
            List<StoreCore.Interfaces.MoveResult> moveResult = null;
            using (var db = Core.Data.ConnectionHelper.GetDB())
                moveResult = _save.MoveResult_ByDirection(db, direction.DirectionID, Entities.StoreMove.MO_DOCTYPE_DISPATCH, (mi) => dispatchPositionsModel.PositionIDs.Contains(mi.PositionID));

            if (moveResult.Any())
            {
                // calc sums
                var currentlyDoneSum = moveResult
                    .GroupBy(k => k.DocPosition, (k, e) => new { DocPosition = k, Quantity = e.Sum(i => i.Quantity) })
                    .ToList();

                // update directions
                direction.Items.ForEach(di =>
                {
                    di.Quantity = di.Quantity - currentlyDoneSum.Where(sum => sum.DocPosition == di.DocPosition).Select(sum => sum.Quantity).SingleOrDefault();
                });

                direction.Items = direction.Items.Where(di => di.Quantity > 0).ToList();
            }

            _planMaker.SaveOnStoreToHistory = true;
            var planResult = _planMaker.CreatePlan(direction, prefixSettings, DateTime.Today, prefix?.PrefixSection);
            return planResult;
        }


        [HttpPost]
        [Route("api/procedureshelpers/positioncontent/{id}")]
        public List<StoreCore.Interfaces.OnStoreItemFull> PositionContent(int id)
        {
            return _onStore.OnPosition(id, StoreCore.Interfaces.ResultValidityEnum.Valid)
                .OrderBy(i => i.Article.ArticleCode)
                .ThenBy(i => i.BatchNum)
                .ToList();
        }


        [HttpPost]
        [Route("api/procedureshelpers/abrastores")]
        public object ABRAStores()
        {
            var abraStocktaking = new S4.Abra.Stocktaking();
            var stores = abraStocktaking.ListStores();
            return stores.Select(i => new { id = i.Item1, text = i.Item2 });
        }

        [HttpPost]
        [Route("api/procedureshelpers/canenableexpbatch/{articleID}")]
        public CanEnableExpBatchModel CanEnableExpBatch(int articleID)
        {
            return CanEnableExpBatch(articleID, _onStore);
        }


        #region helpers

        private static object _lock = new object();
        public static CanEnableExpBatchModel CanEnableExpBatch(int articleID, StoreCore.Interfaces.IOnStore onStore)
        {
            lock(_lock)
            {
                // test if on store or in reservation
                var onStoreList = onStore
                    .ByArticle(articleID, StoreCore.Interfaces.ResultValidityEnum.Valid)
                    .Where(i => i.Position.PosCateg != Entities.Position.PositionCategoryEnum.Special)
                    .ToList();
                var canChangeBatchOrExp = onStoreList.Count == 0;
                if (!canChangeBatchOrExp)
                    return new CanEnableExpBatchModel() { Reason = "Nelze měnit nastavení položky - položka je skladem!" };

                onStoreList = onStore
                    .ByArticle(articleID, StoreCore.Interfaces.ResultValidityEnum.Reservation)
                    .Where(i => i.Position.PosCateg != Entities.Position.PositionCategoryEnum.Special)
                    .ToList();
                canChangeBatchOrExp = onStoreList.Count == 0;
                if (!canChangeBatchOrExp)
                    return new CanEnableExpBatchModel() { Reason = "Nelze měnit nastavení položky - položka je v neuzavřených pohybech!" };

                return new CanEnableExpBatchModel() { CanEnable = true };
            }
        }

        private IEnumerable<object> ListWorkers(Entities.Worker.WorkerClassEnum workerClass, string jobID, string preferedStatus, bool canDispatchMedicines = false, bool canDispatchCytostatics = false)
        {
            var workers = _workerDAL
                .ListByWorkerClass(workerClass)
                .Where(w => w.WorkerStatus != Entities.Worker.WK_STATUS_DISABLED && w.WorkerStatus != Entities.Worker.WK_STATUS_ON_HOLIDAY);

            if (canDispatchMedicines)
                workers = workers.Where(w => w.WorkerClass.HasFlag(Entities.Worker.WorkerClassEnum.CanDispatchMedicines));
            if (canDispatchCytostatics)
                workers = workers.Where(w => w.WorkerClass.HasFlag(Entities.Worker.WorkerClassEnum.CanDispatchCytostatics));

            var assignments = _directionAssignDAL.ScanAssignments().
                Where(a => a.JobID == jobID);

            return workers
                .Select(w => new { worker = w, assignment = assignments.Where(a => a.WorkerID == w.WorkerID).SingleOrDefault() })
                .Select(r => new
                {
                    r.worker.WorkerID,
                    r.worker.WorkerName,
                    r.worker.WorkerStatus,
                    WorkerStatusName = Entities.Worker.Statuses.Items.Where(s => s.Code == r.worker.WorkerStatus).Select(s => s.Name).SingleOrDefault(),
                    CntDocs = (r.assignment == null) ? 0 : r.assignment.CntDocs,
                    CntItems = (r.assignment == null) ? 0 : r.assignment?.CntItems,
                    IsPrefered = (r.worker.WorkerStatus == preferedStatus),
                    CanDispatchMedicines = r.worker.WorkerClass.HasFlag(Entities.Worker.WorkerClassEnum.CanDispatchMedicines),
                    CanDispatchCytostatics = r.worker.WorkerClass.HasFlag(Entities.Worker.WorkerClassEnum.CanDispatchCytostatics)
                })
                .OrderByDescending(r2 => r2.IsPrefered)
                .ThenBy(r2 => r2.CntItems)
                .ThenBy(r2 => r2.WorkerName);
        }

        private IEnumerable<object> ListPositions(Entities.Position.PositionCategoryEnum category)
        {
            var positions = _positionDAL.ListByCategory(category);
            return positions
                .Where(p => p.PosStatus == Entities.Position.POS_STATUS_OK)
                .OrderBy(p => p.PosCode)
                .Select(p => new { p.PosCode, p.PosDesc, p.PositionID });
        }

        #endregion
    }
}

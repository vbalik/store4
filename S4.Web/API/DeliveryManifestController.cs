﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NPoco;
using S4.Entities;
using S4.Web.DataList;
using S4.Web.Models;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/DeliveryManifest")]
    [ExportName("Manifest pro spediční služby")]
    public class DeliveryManifestController : DataListController<Entities.Views.DeliveryManifestView, DeliveryManifestSettings, DeliveryManifestSettingsModel>
    {
        public DeliveryManifestController(IHttpContextAccessor httpContextAccessor, ILogger<DataListController<Entities.Views.DeliveryManifestView, DeliveryManifestSettings, DeliveryManifestSettingsModel>> logger)
        : base(httpContextAccessor, logger)
        {

        }

        protected override Sql GetSql(DeliveryManifestSettingsModel settings)
        {
            var sql = new Sql()
                .Append("select m.*")
                .Append(", (select count(*) from s4_deliveryDirection where deliveryManifestID = m.deliveryManifestID) as docs")
                .Append(", (select count(*) from s4_deliveryDirection_items where deliveryDirectionID in (select deliveryDirectionID from s4_deliveryDirection where deliveryManifestID = m.deliveryManifestID)) as parcels")
                .Append("from s4_deliveryManifest m");

            return sql;
        }
    }
}

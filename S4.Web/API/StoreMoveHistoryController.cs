﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using NPoco;

using S4.Entities;
using S4.Web.Models;
using S4.Web.DataList;
using Microsoft.Extensions.Logging;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/StoreMoveHistory")]
    public class StoreMoveHistoryController : DataListController<StoreMoveHistory, StoreMoveHistorySettings, StoreMoveHistorySettingsModel>
    {
        public StoreMoveHistoryController(IHttpContextAccessor httpContextAccessor, ILogger<DataListController<StoreMoveHistory, StoreMoveHistorySettings, StoreMoveHistorySettingsModel>> logger) : base(httpContextAccessor, logger)
        {

        }

        protected override Sql GetSql(StoreMoveHistorySettingsModel settings)
        {
            var sql = base.GetSql(settings);
            if (settings.StoMoveID != 0)
            {
                sql.Where("stoMoveID = @0", settings.StoMoveID);
            }
            return sql;
        }
    }
}

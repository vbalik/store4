﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NPoco;

using S4.Entities;
using S4.Entities.Views;
using S4.Web.DataList;
using S4.Web.Models;

namespace S4.Web.API
{
    [Produces("application/json")]
    [Route("api/ArticlePacking")]
    public class ArticlePackingControler : DataListController<ArticlePackingView, ArticlePackingSettings, ArticlePackingSettingsModel>
    {
        private int? _ArticleID = null;

        public ArticlePackingControler(IHttpContextAccessor httpContextAccessor, ILogger<DataListController<ArticlePackingView, ArticlePackingSettings, ArticlePackingSettingsModel>> logger) : base(httpContextAccessor, logger)
        {

        }

        protected override Sql GetSql(ArticlePackingSettingsModel settings)
        {
            _ArticleID = settings.ArticleID;

            var sql = base.GetSql(settings);
            if (settings.ArticleID != 0)
            {
                sql.Where("articleID = @0", settings.ArticleID);
            }
            return sql;
        }

        protected override ArticlePackingView SaveItem(ArticlePackingView item)
        {
            //throws an exception if data are not valid
            CheckDataValidity(item);

            var originItem = new DAL.DAL<ArticlePacking, int>().Get(item.ArticleID);
            if (originItem == null)
                originItem = new ArticlePacking();

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // make history
                var history = new Entities.ArticleHistory
                {
                    ArticleID = item.ArticleID,
                    EventCode = Entities.Article.AR_EVENT_UPDATE,
                    EventData = new Core.PropertiesComparer.PropertiesComparer<Entities.ArticlePacking>(originItem, item).JsonText,
                    EntryUserID = GetUser(),
                    EntryDateTime = DateTime.Now
                };

                // save
                using (var tr = db.GetTransaction())
                {
                    // save 
                    db.Save(item);

                    //his
                    db.Insert(history);

                    // do save
                    tr.Complete();
                }
            }

            return item;
        }

        protected override void FillData(DataListData<ArticlePackingView, ArticlePackingSettings, ArticlePackingSettingsModel> dataListData, bool toExcel)
        {
            base.FillData(dataListData, toExcel);

            if (!dataListData.DataModel.Data.Any()) return;

            var article = new DAL.ArticleDAL().GetModel((int) _ArticleID);

            foreach (var item in dataListData.DataModel.Data)
            {
                item.BarCodes = article.PackingBarCodes.Where(_ => _.ArtPackID == item.ArtPackID).Select(_ => _.BarCode).ToList();
            }
        }
    }
}

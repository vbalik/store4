﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using S4.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Threading.Tasks;


namespace S4.Web
{
    public class MvcExceptionFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            var userID = context.HttpContext?.User?.FindFirst(ClaimTypes.Sid)?.Value;
            var requestId = Activity.Current?.Id ?? context.HttpContext.TraceIdentifier;
            var requestBody = GetRequestBody(context.HttpContext.Request.Body);

            var errorLogHelper = new ErrorLogHelper();
            errorLogHelper.ReportProcException(requestBody, new { userID, requestId }, context.Exception, context.HttpContext.Request.Path);

        }


        private static string GetRequestBody(Stream body)
        {
            try
            {
                using (StreamReader stream = new StreamReader(body))
                {
                    stream.BaseStream.Seek(0, SeekOrigin.Begin);
                    return HidePassword(stream.ReadToEnd());

                }
            }
            catch
            {
                return null;
            }
        }

        private static string HidePassword(string requestBody)
        {
            const string PASSWORD = "password=";

            if (string.IsNullOrWhiteSpace(requestBody))
                return requestBody;

            var posPassword = requestBody.IndexOf(PASSWORD);

            if (posPassword == -1)
                return requestBody;

            var posReturnUrl = requestBody.IndexOf("&returnUrl=");

            if (posReturnUrl == -1)
                return requestBody;

            var password = requestBody.Substring(posPassword + 9, posReturnUrl - (posPassword + 9));

            return requestBody.Replace($"{PASSWORD}{password}", PASSWORD);
        }
    }
}

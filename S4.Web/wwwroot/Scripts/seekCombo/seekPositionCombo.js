﻿Vue.component('seek_position_combo', {
    props: ['value', 'positionCategory'],
    data: function () {
        return {
            positions: [],
            searchedStr: null,
            loading: null,
            selectedPosition: this.value
        };
    },
    watch: {
        searchedStr: function (newVal, oldVal) {
            this.searchTimer();
        },
        value: function (newVal, oldVal) {
            this.selectedPosition = newVal;
        }
    },
    methods: {
        onSearch: function (search, loading) {
            this.searchedStr = search;
            this.loading = loading;
        },
        searchTimer: _.debounce(
            function () {
                this.doSearch();
            },
            1000
        ),
        doSearch: function () {
            if (this.searchedStr.length < 2) {
                this.positions = [];
                return;
            }
            var self = this;
            this.loading(true);
            var searchPositionsModel = {
                part: this.searchedStr,
                limitToCategory: (this.positionCategory != null),
                positionCategory: this.positionCategory
            };
            callAJAX(window.location.origin + "/api/position/searchpositions", searchPositionsModel, function (data) {
                var positions = [];
                data.forEach(function (item) {
                    positions.push({
                        name: item.posCode + " - " + item.posDesc,
                        value: item.positionID,
                        position: item
                    });
                });
                self.$data.positions = positions;
                self.loading(false);
            }, null, false);
        },
        positionChanged: function (val) {
            this.$emit('positionselected', val);
            this.$emit('input', val);
            if (!val)
                this.positions = [];
        },
        resetValue: function () {
            this.$emit('input', null);
            this.positions = [];
        }
    },
    template: ' <v-select  v-model="selectedPosition" label="name" placeholder="Vyhledat ..." :options="positions" :on-search="onSearch" :on-change="positionChanged">\
                    <div slot="no-options">Žádné shody!</div>\
                    <template slot="option" slot-scope="option">\
                        <span class="seek-list-head">{{ option.position.posCode }}</span>\
                        <span class="seek-list-sep"> - </span>\
                        <span>{{ option.position.posDesc }}</span>\
                    </template>\
                </v-select>'
});
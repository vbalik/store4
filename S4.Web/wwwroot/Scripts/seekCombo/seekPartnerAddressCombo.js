﻿Vue.component('seek_partner_address_combo', {
    props: ['value'],
    data: function () {
        return {
            items: [],
            searchedStr: null,
            loading: null,
            selectedItem: null
        };
    },
    watch: {
        searchedStr: function (newVal, oldVal) {
            this.searchTimer();
        },
        value: function (newVal, oldVal) {
            this.selectedItem = newVal;
        }
    },
    methods: {
        onSearch: function (search, loading) {
            this.searchedStr = search;
            this.loading = loading;
        },
        searchTimer: _.debounce(
            function () {
                this.doSearch();
            },
            1000
        ),
        doSearch: function () {
            if (this.searchedStr.length < 3) {
                this.items = [];
                return;
            }
            var self = this;
            this.loading(true);
            var searchModel = {
                part: this.searchedStr
            };
            callAJAX(window.location.origin + "/api/partnersaddress/searchaddress", searchModel, function (data) {
                var items = [];
                data.forEach(function (item) {
                    items.push({
                        partAddrID: item.partAddrID,
                        partnerID: item.partnerID,
                        addrName: item.addrName,
                        deliveryInst: item.deliveryInst
                    });
                });
                self.$data.items = items;
                self.loading(false);
            }, null, false);
        },
        itemChanged: function (val) {
            this.$emit('addressselected', val);
            this.$emit('input', val);
            if (!val)
                this.partners = [];
        }
    },
    template: ' <v-select label="addrName" v-model="selectedItem" placeholder="Vyhledat adresu..." :options="items" :on-search="onSearch" :on-change="itemChanged">\
                    <div slot="no-options">Žádné shody!</div>\
                    <template slot="option" slot-scope="option">\
                        <span class="seek-list-id2">{{ option.partnerID }} / {{ option.partAddrID }}</span>\
                        <span class="seek-list-head">{{ option.addrName }}</span>\
                        <span>{{ option.deliveryInst }}</span>\
                    </template>\
                    <template slot="selected-option" slot-scope="option">\
                        <span class="seek-list-id2">{{ option.partnerID }} / {{ option.partAddrID }}</span>\
                        <span class="seek-list-head">{{ option.addrName }}</span>\
                        <span>{{ option.deliveryInst }}</span>\
                    </template >\
                </v-select>'
});
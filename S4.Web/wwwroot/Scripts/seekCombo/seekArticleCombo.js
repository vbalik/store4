﻿Vue.component('seek_article_combo', {
    props: ['value'],
    data: function () {
        return {
            articles: [],
            searchedStr: null,
            loading: null,
            selectedArticle: this.value
        };
    },
    watch: {
        searchedStr: function (newVal, oldVal) {
            this.searchTimer();
        },
        value: function (newVal, oldVal) {
            this.selectedArticle = newVal;
        }
    },
    methods: {
        onSearch: function (search, loading) {
            this.searchedStr = search;
            this.loading = loading;
        },
        searchTimer: _.debounce(
            function () {
                this.doSearch();
            },
            1000
        ),
        doSearch: function () {
            if (this.searchedStr.length < 3) {
                this.articles = [];
                return;
            }
            var self = this;
            this.loading(true);
            var searchArticlesModel = {
                part: this.searchedStr
            };
            callAJAX(window.location.origin + "/api/article/searcharticles", searchArticlesModel, function (data) {
                var articles = [];
                data.forEach(function (item) {
                    articles.push({
                        name: item.articleCode + " - " + item.articleDesc,
                        articleCode: item.articleCode,
                        value: item.articleID,
                        artPackID: item.artPackID,
                        unitDesc: item.unitDesc,
                        useBatch: item.useBatch,
                        useExpiration: item.useExpiration,
                        article: item
                    });
                });
                self.$data.articles = articles;
                self.loading(false);
            }, null, false);
        },
        articleChanged: function (val) {
            this.$emit('articleselected', val);
            this.$emit('input', val);
            if (!val)
                this.articles = [];
        }
    },
    template: ' <v-select label="name" v-model="selectedArticle" placeholder="Vyhledat ..." :options="articles" :on-search="onSearch" :on-change="articleChanged">\
                    <div slot="no-options">Žádné shody!</div>\
                    <template slot="option" slot-scope="option">\
                        <span :set="option.article !== undefined ? option = option.article : option = option"></span >\
                        <span class="seek-list-head">{{ option.articleCode }}</span>\
                        <span class="seek-list-sep"> - </span>\
                        <span>{{ option.articleDesc }}</span>\
                    </template>\
                    <template slot="selected-option" slot-scope="option">\
                        <span :set="option.article !== undefined ? option = option.article : option = option"></span >\
                        <span class="seek-list-head">{{ option.articleCode }}</span>\
                        <span class="seek-list-sep"> - </span>\
                        <span>{{ option.articleDesc }}</span>\
                    </template >\
                </v-select>'
});
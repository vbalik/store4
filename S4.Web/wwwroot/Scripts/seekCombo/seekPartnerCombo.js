﻿Vue.component('seek_partner_combo', {
    props: ['value'],
    data: function () {
        return {
            partners: [],
            searchedStr: null,
            loading: null,
            selectedPartner: null
        };
    },
    watch: {
        searchedStr: function (newVal, oldVal) {
            this.searchTimer();
        },
        value: function (newVal, oldVal) {
            this.selectedPartner = newVal;
        }
    },
    methods: {
        onSearch: function (search, loading) {
            this.searchedStr = search;
            this.loading = loading;
        },
        searchTimer: _.debounce(
            function () {
                this.doSearch();
            },
            1000
        ),
        doSearch: function () {
            if (this.searchedStr.length < 3) {
                this.partners = [];
                return;
            }
            var self = this;
            this.loading(true);
            var searchPartnersModel = {
                part: this.searchedStr
            };
            callAJAX(window.location.origin + "/api/partners/searchpartners", searchPartnersModel, function (data) {
                var partners = [];
                data.forEach(function (item) {
                    partners.push({
                        partnerID: item.partnerID,
                        partnerName: item.partnerName,
                        partnerPhone: item.partnerPhone
                    });
                });
                self.$data.partners = partners;
                self.loading(false);
            }, null, false);
        },
        partnerChanged: function (val) {
            this.$emit('partnerselected', val);
            this.$emit('input', val);
            if (!val)
                this.partners = [];
        }
    },
    template: ' <v-select label="partnerName" v-model="selectedPartner" placeholder="Vyhledat ..." :options="partners" :on-search="onSearch" :on-change="partnerChanged">\
                    <div slot="no-options">Žádné shody!</div>\
                    <template slot="option" slot-scope="option">\
                        <span class="seek-list-id">{{ option.partnerID }}</span>\
                        <span class="seek-list-head">{{ option.partnerName }}</span>\
                        <span>{{ option.partnerPhone }}</span>\
                    </template>\
                    <template slot="selected-option" slot-scope="option">\
                        <span class="seek-list-id">{{ option.partnerID }}</span>\
                        <span class="seek-list-head">{{ option.partnerName }}</span>\
                    </template >\
                </v-select>'
});
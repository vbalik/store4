﻿function htmlDecode(s) {
    return $('<div>').html(s).html();
}

function callAJAXBatch(url, items, paramsFunction, doneFunction, showWorking, showProgress = true, getResultFunction) {
    // recursive function
    var callAction = function (i) {
        var onSuccess = (i === items.length - 1) ? doneFunction : function () { callAction(i + 1); };
        var callParams = paramsFunction(items[i]);
        callAJAX(url, callParams, function (data) {
            if (getResultFunction)
                getResultFunction(data);
            if (onSuccess)
                onSuccess(data);
        }, null, showWorking, false, (showProgress) ? ((i + 1) / items.length) : -1);
    };
    // first item
    callAction(0);
}

function callAJAX(url, params, showFunction, showFunctionParam, showWorking, dontShowError, currentProgress) {
    if (showWorking) {
        $('#modal_working').modal('show');
        $('#working_icon').addClass("fa-spin");
        if (currentProgress !== null && currentProgress !== undefined && currentProgress !== -1) {
            $('.progress').show();
            $('.progress-bar').css("width", (currentProgress * 100).toFixed() + "%");
        }
    }

    function notAutentificatedRedirection() {
        var path = window.location.origin + "/auth/login?ReturnUrl=" + window.location.pathname;
        var desiredText = ' Přihlaste se prosím a opakujte akci';
        var html = '<a href="' + path + '">' + desiredText + '</a>';
        $('#modal_error_text').html("<b>Nejste přihlášeni</b>. Může to být způsobeno <b>automatickým odhlášením</b> při nečinnosti nebo pokusem o <b>neoprávněný přístup</b>.");
        $('#modal_error_text').append(html);
        $('#modal_error').modal('show');
    }

    $.ajax({
        url: url,
        data: JSON.stringify(params),
        type: 'POST',
        contentType: 'application/json',
        cache: false,
        beforeSend: function (request) {
            if (typeof (S4_API_TOKEN) === "undefined") {
                notAutentificatedRedirection();
            } else {
                request.setRequestHeader("S4_API_TOKEN", S4_API_TOKEN);
            }
        },
        success: function (data) {
            if (showWorking) {
                $('#modal_working').modal('hide');
                $('#working_icon').removeClass("fa-spin");
            }
            //showProgress(null, -1);
            if (!dontShowError && data.success !== null && data.success === false) {
                $('#modal_error_text').text("Chyba: " + data.errorText);
                $('#modal_error').modal('show');
            } else {
                showFunction(data, showFunctionParam);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (showWorking) {
                $('#modal_working').modal('hide');
                $('#working_icon').removeClass("fa-spin");
            }
            //showProgress(null, -1);
            var errorDesc;
            if (jqXHR.readyState === 4)
                if (jqXHR.status === 401) {
                    //Not Authenticated 
                    notAutentificatedRedirection();
                    return;
                }
                else {
                    var det = "";
                    if (jqXHR.responseJSON !== undefined && jqXHR.responseJSON.error !== undefined) {
                        det = " - " + jqXHR.responseJSON.error;
                    }
                    errorDesc = jqXHR.status + " - " + jqXHR.statusText + det;
                }
            else if (jqXHR.readyState === 0)
                errorDesc = "ERR_CONNECTION_REFUSED";
            else
                errorDesc = errorThrown;

            $('#modal_error_text').text("Chyba: " + errorDesc);
            $('#modal_error').modal('show');
        },
        complete: function (jqXHR) {
            var newToken = jqXHR.getResponseHeader("S4_API_TOKEN");
            if (newToken)
                S4_API_TOKEN = newToken;
        }
    });
}


function formatStatusField(displayValue, listItem, canceled = false, notValid = false) {
    let icon = '';
    if (canceled)
        icon = '<i class="fas fa-minus-circle" style="color:red;margin-left:.4em;" title="Zrušeno"></i>';
    else if (notValid)
        icon = '<i class="far fa-question-circle" style="color:green;margin-left:.4em;" title="Naplánováno - není platné"></i>';
    let lbl = 'label-primary';
    if (listItem)
        switch (listItem.color) {
            case 'Green':
                lbl = 'label-success';
                break;
            case 'Blue':
                lbl = 'label-info';
                break;
            case 'Gray':
                lbl = 'label-default';
                break;
            case 'Red':
                lbl = 'label-danger';
                break;
            case 'Orange':
                lbl = 'label-warning';
                break;
            case 'Violet':
                lbl = 'label-danger';
                break;
        }
    return '<span title="" class="label ' + lbl + '">' + displayValue + '</span>' + icon;
}


function formatDocInfoField(displayValue) {
    if (!displayValue)
        return null;
    let parts = displayValue.split('/');
    return parts.join('<span class="docInfoSeparator">/</span>');
}


function formatExpiration(displayValue) {
    let parts = displayValue.split('.');
    return parts.join('<span class="expirationSeparator">.</span>');
}


function dateToExpiration(value) {
    if (!value)
        return null;
    else {
        value = new Date(value);
        var month = value.getUTCMonth() + 1;
        return numberToDigits(value.getUTCDate()) + '.' + numberToDigits(month) + '.' + (value.getUTCFullYear() - 2000);
    }
}

function eanExpirationToExpiration(value) {
    const parts = [...value.matchAll(/\d{2}/g)];
    return `${parts[2]}.${parts[1]}.${parts[0]}`;
}

function convertExpirationToDate(value) {
    value = value === null ? '' : value;
    const _today = new Date();
    const date = new Date(_today.getFullYear(), _today.getMonth() + 1, 1, 12);
    const dateParts = ((value.indexOf('/') !== -1)
        ? value.split('/')
        : value.split('.'))
            .map(function (v) {
                return parseInt(v);
            });

    // validate
    const reg = /^([0-9]{1,2}(\.|\/)[0-9]{1,2}(\.|\/)[0-9]{4}|[0-9]{1,2}(\.|\/)[0-9]{1,2}(\.|\/)[0-9]{2}|[0-9]{1,2}(\.|\/)[0-9]{2})$/;

    if (!reg.test(value)) return null;

    if (dateParts.length > 2) { // 1.1.20
        date.setUTCFullYear(dateParts[2] < 2000 ? 2000 + dateParts[2] : dateParts[2]);
        date.setUTCMonth(parseInt(dateParts[1]) - 1);
        date.setUTCDate(dateParts[0]);
    }
    else if (dateParts.length > 1) { // 1.1
        date.setUTCFullYear(dateParts[1] < 2000 ? 2000 + dateParts[1] : dateParts[1]);
        date.setUTCMonth(dateParts[0]);
        date.setUTCDate(0);
    }
    return date instanceof Date && !isNaN(date) ? date : null;
}


function numberToDigits(value, digits = 2) {
    var number = value.toString();
    var pref = "";
    for (i = number.length; i < digits; i++) {
        pref += "0";
    }
    return pref + number;
}

function expirationDescToDate(expirationDesc) {
    var dateParts = expirationDesc.split(".");
    var dtYear = dateParts[2];
    var dtMonth = dateParts[1];
    var dtDay = dateParts[0];
    var dtResult = new Date;
    dtResult.setUTCFullYear('20' + dtYear);
    dtResult.setUTCMonth(dtMonth - 1);
    dtResult.setUTCDate(dtDay);
    return dtResult;
}

function rollOnWheelVue(e, app, item, list) {
    if (!list)
        return;

    var itemsList = app.$data[list];
    var index = itemsList.indexOf(app.$data[item]);

    if (e.deltaY) {
        if (e.deltaY > 0 && index < (itemsList.length - 1)) {
            app.$data[item] = itemsList[index + 1];
        }
        else if (e.deltaY < 0 && index > 0) {
            app.$data[item] = itemsList[index - 1];
        }
    }
}

function rollOnWheel(e, select) {
    if (e.deltaY) {
        if (e.deltaY > 0) {
            select.selectedIndex++;
        }
        else if (e.deltaY < 0) {
            select.selectedIndex--;
        }
    }
}

//async call locker
var functionCallbacks = [];
var functionLock = false;
var getData = function (url, callback) {
    if (functionLock) {
        functionCallbacks.push(callback);
    } else {
        functionLock = true;
        functionCallbacks.push(callback);
        $.getJSON(url, function (data) {
            while (functionCallbacks.length) {
                var thisCallback = functionCallbacks.pop();
                thisCallback(data);
            }
            functionLock = false;
        });
    }
};

function showError(errorID) {
    $.notify({
        // options
        title: 'Něco se porouchalo...',
        message: 'Chyba byla zaznamenána a bude předána vývojovému oddělení. ID: ' + errorID
    }, {
        // settings
        z_index: '2147483647',
        type: 'minimalist',
        mouse_over: 'pause',
        delay: '20000',
        placement: {
            from: "bottom",
            align: "right"
        },
        template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
            '<button type="button" aria-hidden="true" class="close alert-close" data-notify="dismiss">×</button>' +
            '<span data-notify="title"><span class="fas fa-exclamation-circle fa-lg" style="margin-right: 9px;"></span>{1}</span>' +
            '<span data-notify="message">{2}</span>' +
            '</div>'
    });
}

function showInfo(text) {
    $.notify({
        // options
        message: text
    }, {
        // settings
        z_index: '2147483647',
        type: 'minimalist',
        mouse_over: 'pause',
        delay: '20000',
        placement: {
            from: "bottom",
            align: "right"
        },
        template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-success" role="alert">' +
            '<button type="button" aria-hidden="true" class="close alert-close" data-notify="dismiss">×</button>' +
            '<span data-notify="title"><span class="fas fa-info-circle fa-lg" style="margin-right: 9px;"></span>{2}</span></div>'
    });
}

function showInfoSignalR(notifyType, title, message, notifyID, progressValue) {
    var color = "success";
    if (notifyType === 0) {
        color = "success";
    }
    else if (notifyType === 1) {
        color = "info";
    }
    else if (notifyType === 2) {
        color = "warning";
    }
    else if (notifyType === 3) {
        color = "danger";
    }

    //find notify 
    var obj = dictNotify.find(x => x.key === notifyID);
    var index = dictNotify.indexOf(obj);
    if (index > -1) {
        //update
        var notify = dictNotify[index].value;
        notify.update('title', title);
        notify.update('message', message);
        notify.update('type', color);
        notify.update('progress', progressValue);
    }
    else {
        //new notification
        var n = $.notify({
            // options
            title: title,
            message: message,
            icon: 'fa fa-info-circle'
        }, {
            // settings
            z_index: '2147483647',
            type: color,
            showProgressbar: progressValue > -1 ? true : false,
            mouse_over: 'pause',
            newest_on_top: false,
            delay: '0',
            allow_dismiss: true,
            offset: {
                x: 10,
                y: 55
            },
            placement: {
                from: "top",
                align: "right"
            },
            onClose: function () {
                //delete from aray
                var obj = dictNotify.find(x => x.key === this[0].id);
                var index = dictNotify.indexOf(obj);
                if (index > -1) {
                    dictNotify.splice(index, 1);
                }

            },
            template: '<div id=' + notifyID + ' data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
                '<button id="dismissID" type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                '<span data-notify="icon"></span>' +
                '<span id="title" style="margin-left: 9px; font-weight: bold;" data-notify="title">{1}</span>' +
                '<div id="message" class="row" style="margin-left: 9px;margin-top: 12px;" data-notify="message">{2}</div>' +
                '<div class="progress" data-notify="progressbar" style="margin-top: 20px;height: 15px;">' +
                '<div class="progress-bar progress-bar-striped progress-bar-animated progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                '</div>' +
                '</div>'

        });
        //progress
        n.update('progress', progressValue);
        //add notify to array
        dictNotify.push({ key: notifyID.toString(), value: n });

    }
    //dismiss button
    var n2 = document.getElementById(notifyID);
    if (n2 !== null) {
        var chs = n2.children;
        if (color === "success" || color === "danger") {
            n2.childNodes.item("dismissID").style.visibility = 'visible';
        }
        else {
            n2.childNodes.item("dismissID").style.visibility = 'hidden';
        }
    }
}

function setVueError(err, msg, info) {

    if (err !== null) {
        msg = 'Vue ErrorMessage: ' + err.message + ' Vue Info: ' + info + ' Vue Stack: ' + err.stack;
    }
    else {
        msg = 'Vue WarnMessage: ' + msg + ' Vue Info: ' + info;
    }

    var model = { ErrorMessage: msg, Url: window.location.href, Column: 0, Line: 0, ErrorClass: 'JavaScriptVueError' };
    sendErrorToServer(model);

    console.error(msg);
}

function sendErrorToServer(model) {
    callAJAX(window.location.origin + '/api/clienterror/inserterror', model, function (data) {
        showError(data);
    }, null, false, true);

}
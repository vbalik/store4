﻿"use strict";

$(function () {
    // Enables popover
    $("[data-toggle=popover]").popover({
        container: 'body'
    });
});

//var connection = new signalR.HubConnectionBuilder().withUrl("/progressHub").build();

//var connectionId = connection.id;

//connection.on("progress", showProgress(msg, perc));

// Reference the auto-generated proxy for the hub.
//var progress = connection.progressHub;

// Create a function that the hub can call back to display messages.
//progress.client.AddProgress = showProgress;

//connection.on("progress", alert("pokus"));

/*connection.start().catch(function (err) {
    return console.error(err.toString());
});*/

/*document.getElementById("sendButton").addEventListener("click", function (event) {
    var user = document.getElementById("userInput").value;
    var message = document.getElementById("messageInput").value;
    connection.invoke("SendMessage", user, message).catch(function (err) {
        return console.error(err.toString());
    });
    event.preventDefault();
});*/

function showProgress(message, percentage) {
    console.log(message + ' ' + percentage);

    if (percentage >= 0) {
        $('#mod-progress').modal('show');
        $('#progressBarParagraph').text(message);

        window.progressBarActive = true;
        $('#ProgressMessage').width(percentage + '%');
    }
    else {
        $('#mod-progress').modal('hide');
        window.progressBarActive = false;
    }
};

/**
 * Websocket factory
 * 
 * url           [string] - hub url
 * subscribers   [object] - has same property name as hub subscriber
 * startCallback [function] - called on start websocket connection
 * 
 * <example>
 *     Websocket().create('/hub', {
 *       'updateValue': function(data) { ... },
 *       'updateValue2': function(data) { ... },
 *      },
 *      function() { ... }
 *     );
 * </example>
 */
var WebsocketFactory = (function() {
    WebsocketFactory = function() {};
    WebsocketFactory.prototype = {
        create: function(url, subscribers, startCallback) {
            const connection = new signalR.HubConnectionBuilder().withUrl(url).build();
            const subNames = Object.getOwnPropertyNames(subscribers);

            for(let i = 0; i < subNames.length; i++) {
                connection.on(subNames[i], subscribers[subNames[i]]);
            }

            connection.start()
                .then(startCallback)
                .catch(function (err) {
                    return console.error(err.toString());
                });

            return connection;
        }
    };
    return WebsocketFactory;
})();
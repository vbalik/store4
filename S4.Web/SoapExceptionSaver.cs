﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using S4.Web.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace S4.Web
{
    public class SoapExceptionSaver
    {
        public static void SaveException(ILogger<SoapExceptionSaver> logService, Exception exc, IHttpContextAccessor loghttp)
        {
            if (exc is TargetInvocationException && exc.InnerException != null)
                exc = exc.InnerException;

            var errorLogHelper = new ErrorLogHelper();
            errorLogHelper.ReportProcException(GetRequestBody(loghttp.HttpContext.Request.Body), new { }, exc, "SOAP error", logService);
        }

        private static string GetRequestBody(Stream body)
        {
            try
            {
                using (StreamReader stream = new StreamReader(body))
                {
                    stream.BaseStream.Seek(0, SeekOrigin.Begin);
                    return stream.ReadToEnd();
                }
            }
            catch
            {
                return null;
            }
        }
    }
}

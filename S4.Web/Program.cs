﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using NLog.Web;
using S4.Core.NLoging;

namespace S4.Web
{
    public class Program
    {

        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }


        public static IWebHost BuildWebHost(string[] args)
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: true)
                .AddJsonFile("appsettings.Development.json", optional: true)
                .AddJsonFile("hosting.json", optional: true)
                .AddCommandLine(args)
                .AddEnvironmentVariables()
                .Build();

            ConfigNLogger(config);

            // get url list
            var serverUrlsSection = config.GetSection("serverUrls");
            var urls = serverUrlsSection.AsEnumerable().Select(i => i.Value).Where(v => v != null).ToArray();
            if (urls.Length == 0)
                urls = new string[] { "http://localhost:5000" };

            return WebHost.CreateDefaultBuilder(args)
                .UseConfiguration(config)
                .ConfigureAppConfiguration((context, builder) =>
                {
                    builder.AddJsonFile("hosting.json", optional: true);
                })
                .UseStartup<Startup>()
                .UseUrls(urls)
                .ConfigureLogging(logging =>
                {
                    logging.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace);
                })
                .UseIISIntegration()
                .UseKestrel(options => 
                {
                    options.Limits.MinRequestBodyDataRate = new MinDataRate(bytesPerSecond: 80, gracePeriod: TimeSpan.FromSeconds(20));
                    options.Limits.MinResponseDataRate = new MinDataRate(bytesPerSecond: 80, gracePeriod: TimeSpan.FromSeconds(20));
                })
                .UseNLog()
                .Build();
        }


        private static void ConfigNLogger(IConfigurationRoot config)
        {
            var rules = new List<NLogSettingRules>();
            var targets = new List<NLogSettingTargets>();
            config.GetSection("nlog:rules").Bind(rules);
            config.GetSection("nlog:targets").Bind(targets);

            NLog.LogManager.Configuration = NLogHelper.NLogGetConfig(rules, targets);
        }
    }
}

﻿using S4.Core;
using S4.Web.Scheduling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace S4.Web.Tasks
{
    public class FlushProcedureLogTask : IScheduledTask
    {
        public int ScheduledPeriod => 10; // every 10 seconds


        public async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            await Singleton<PerfMeasurement.ProcedureLogReceiver>.Instance.DoFlushAsync();
        }
    }
}

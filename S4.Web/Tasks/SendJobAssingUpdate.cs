﻿using Microsoft.AspNetCore.SignalR;
using S4.Entities.Helpers;
using S4.Web.Hubs.Terminal;
using S4.Web.Scheduling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace S4.Web.Tasks
{
    public class SendJobAssingUpdate : IScheduledTask
    {
        public int ScheduledPeriod => 60; // every minute


        public async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            // get assigment data
            var dal = new DAL.DirectionAssignDAL();
            var assignments = dal.ScanAssignments();

            var toSend = assignments.GroupBy(
                k => k.WorkerID,
                (k, items) => new TerminalJobAssignUpdate() { WorkerID = k, Items = items.Select(i => new TerminalJobAssignItem(i.JobID, i.CntDocs, i.CntItems)).ToList() }
                );


            // send to terminals
            var terminalHub = Startup.GetService<IHubContext<TerminalHub, ITerminalHub>>();
            var sender = new TerminalSender(terminalHub);

            foreach (var update in toSend)
                await sender.AssignmentUpdated(update);

            var workers = toSend.Select(_ => _.WorkerID).ToList();
            await sender.ResetAssignment(workers);
        }
    }
}

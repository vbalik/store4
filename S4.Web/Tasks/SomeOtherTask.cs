﻿using S4.Web.Scheduling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace S4.Web.Tasks
{
    public class SomeOtherTask : IScheduledTask
    {
        public int ScheduledPeriod => 5; // every 5 seconds


        public async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            await Task.Delay(500, cancellationToken);
        }
    }
}

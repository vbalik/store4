﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace S4.Web
{
    public class MenuItem
    {
        public int Column { get; set; }
        public string MenuText { get; set; }
        public string MenuPath { get; set; }
        public string IconHtml { get; set; }
        public bool Disabled { get; set; }
        
        public List<MenuItem> SubItems { get; set; }
    }
}
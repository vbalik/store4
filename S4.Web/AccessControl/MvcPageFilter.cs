﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;


namespace S4.Web.AccessControl
{
    public class MvcPageFilter : IActionFilter
    {
        private static readonly string[] ALLWAYS_ALLOWED = new string[] {
            "/",
            "/home",
            "/home/error",
            "/home/internalerror",
            "/auth/login",
            "/auth/logout",
            "/auth/password",
            "/auth/notallowed",
            "/error",
            "/test",
            "/ping",
            "/ping/getservertime",
            "/message",
            "/test/proctester",
            "/test/clienterrortester",
            "/download",
            "/download/direct",
            "/report",
            "/report/report",
            "/report/html5viewer",
            "/status",
            "/terminal/qrcode",
            "/terminal/tablet",
            "/action/receiveimportcsvcheckfileaction",
            "/action/dispatchmanual",
            "/action/receivemanual",
            "/action/receiveandstorein",
            "/invoice",
            "/certificate"
        };


        public void OnActionExecuting(ActionExecutingContext context)
        {
            // ignore /api/* and /api/external/* requests
            if (context.HttpContext.Request.Path.StartsWithSegments(Startup.API_PATH_START, StringComparison.InvariantCultureIgnoreCase))
                return;
            if (context.HttpContext.Request.Path.StartsWithSegments(Startup.EXTERNAL_API_PATH_START, StringComparison.InvariantCultureIgnoreCase))
                return;

            // combit HTML report viewer api
            if (context.HttpContext.Request.Path.StartsWithSegments("/cmbtapi", StringComparison.InvariantCultureIgnoreCase))
                return;

            var path = context.HttpContext.Request.Path.ToString();

            // ignore home, logon, ...
            if (ALLWAYS_ALLOWED.Contains(path, StringComparer.OrdinalIgnoreCase))
                return;

            var userID = context.HttpContext?.User.FindFirst(ClaimTypes.Sid)?.Value;
            if (userID == null)
            {
                context.Result = new RedirectResult("/auth/login");
            }
            else
            {
                // check path with auditor
                var auditor = Core.Singleton<AuditorCache>.Instance.GetItem(userID);
                if (!auditor.CheckPath(path))
                {
                    context.Result = new RedirectResult("/auth/notallowed");
                }
            }
        }


        public void OnActionExecuted(ActionExecutedContext context)
        {
            // do nothing
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace S4.Web.AccessControl
{
    public class AuditorCache : Core.Cache.CacheBase<Auditor>
    {
        public AuditorCache() : base(10)
        { }

        public Auditor GetItem(string workerID)
        {
            return base.GetOrAddExisting(workerID, () =>
            {
                var worker = (new DAL.WorkerDAL()).Get(workerID);
                return new Auditor(worker);
            });
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace S4.Web.AccessControl
{
    public class Auditor : IAuditor
    {
        private HashSet<string> _allowedPaths = new HashSet<string>();


        public Auditor(Entities.Worker worker)
        {
            Worker = worker;
            if (worker != null)
            {
                MenuItems = new MenuBuilder().CreateMenu(this);
                MakePathList();
            }
        }


        private void MakePathList()
        {
            var list = MenuItems
                .Select(s => s.MenuPath)
                .Union(MenuItems.Where(s => s.SubItems != null)
                .SelectMany(s => s.SubItems)
                .Where(s => !s.Disabled && s.MenuPath != null)
                .Select(s1 => s1.MenuPath))
                .Union(new string[] { "/move/docdetail", "/direction/dirdetail" });
            _allowedPaths = new HashSet<string>(list, StringComparer.OrdinalIgnoreCase);
        }



        #region public

        public List<MenuItem> MenuItems { get; }

        public Entities.Worker Worker { get; }

        public bool CheckPath(string currPath) => _allowedPaths.Contains(currPath, StringComparer.OrdinalIgnoreCase);

        #endregion
    }
}
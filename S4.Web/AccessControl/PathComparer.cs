﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace S4.Web.AccessControl
{
    class PathComparer : IEqualityComparer<string>
    {

        public bool Equals(string pattern, string mask)
        {
            return mask.StartsWith(pattern, StringComparison.OrdinalIgnoreCase);
        }


        public int GetHashCode(string obj)
        {
            return obj.GetHashCode();
        }
    }
}
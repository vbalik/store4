﻿using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Http;
using System;
using System.Text;
using System.Threading.Tasks;


namespace S4.Web.AccessControl
{
    public class ApiAuthenticationMiddleware
    {
        public const string API_USER_ID = "S4_API_USER_ID";
        private const string API_TOKEN = "S4_API_TOKEN";

        private readonly RequestDelegate _next;
        private readonly ISecProvider _secProvider;


        public ApiAuthenticationMiddleware(RequestDelegate next, ISecProvider secProvider)
        {
            _next = next;
            _secProvider = secProvider;
        }


        public async Task Invoke(HttpContext context)
        {
            if(context.Request.Path == "/api/procedures/login")
            {
                await _next.Invoke(context);
                return;
            }

            string authHeader = context.Request.Headers[API_TOKEN];
            (string userName, string newToken) = _secProvider.CheckToken(authHeader);

            if (!String.IsNullOrEmpty(authHeader) && !string.IsNullOrEmpty(userName))
            {
                context.Request.Headers.Add(API_USER_ID, userName);
                if (newToken != null)
                    context.Response.Headers.Add(API_TOKEN, newToken);

                await _next.Invoke(context);                
            }
            else
            {
                // no authorization header
                context.Response.StatusCode = 401; //Unauthorized
                return;
            }
        }
    }
}

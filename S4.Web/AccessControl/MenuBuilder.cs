﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using static S4.Entities.Worker;

namespace S4.Web.AccessControl
{
    public class MenuBuilder
    {
        public List<MenuItem> CreateMenu(Auditor auditor)
        {
            // common
            var result = new List<MenuItem>()
            {
                new MenuItem() {
                    MenuText = "Doklady", Column = 0, SubItems = new List<MenuItem>() {
                        new MenuItem() { MenuText = "Vyskladnění", MenuPath = "/direction/out", IconHtml = "<i class='fas fa-arrow-circle-left fa-fw' aria-hidden='true'></i>" },
                        new MenuItem() { MenuText = "Příjem", MenuPath = "/direction/in", IconHtml = "<i class='fas fa-arrow-circle-right fa-fw' aria-hidden='true'></i>" },
                        new MenuItem() { MenuText = "Pohyby", MenuPath = "/move", IconHtml = "<i class='fas fa-exchange-alt fa-fw' aria-hidden='true'></i>" },
                        new MenuItem() { MenuText = "Inventury", MenuPath = "/stocktaking", IconHtml = "<i class='fas fa-question-circle fa-fw' aria-hidden='true'></i>" },
                        new MenuItem() { MenuText = "Faktury", MenuPath = "/invoice", IconHtml = "<i class='fa fa-list-alt fa-fw' aria-hidden='true'></i>" },
                        new MenuItem() { MenuText = "Rezervace", MenuPath = "/booking", IconHtml = "<i class='fa fa-book fa-fw' aria-hidden='true'></i>" }
                    } },
                new MenuItem() {
                    MenuText = "Akce", Column = 0, SubItems = new List<MenuItem>() {
                        new MenuItem() { MenuText = "Přeskladnění", MenuPath = "/action/storemoveaction", IconHtml = "<i class='fas fa-arrows-alt fa-fw' aria-hidden='true'></i>" },
                        new MenuItem() { MenuText = "Přiřazení kontroly", MenuPath = "/action/setdispcheckaction", IconHtml = "<i class='fas fa-tasks fa-fw' aria-hidden='true'></i>" },
                        new MenuItem() { MenuText = "Hromadná expedice", MenuPath = "/action/expeditionaction", IconHtml = "<i class='fas fa-truck-loading fa-fw' aria-hidden='true'></i>" },
                        new MenuItem() { MenuText = "Tisk evidence rozvozů", MenuPath = "/action/printexpeditionAction", IconHtml = "<i class='fa fa-truck' aria-hidden='true'></i>" },
                        new MenuItem() { MenuText = "Příjem - import souborů", MenuPath = "/action/receiveimportcsvAction", IconHtml = "<i class='fa fa-download' aria-hidden='true'></i>" },
                        new MenuItem() { MenuText = "Příjem - manuální", MenuPath = "/action/receivemanual", IconHtml = "<i class='fas fa-dolly-flatbed' aria-hidden='true'></i>" },
                        new MenuItem() { MenuText = "Zjednodušený příjem", MenuPath = "/action/receiveandstorein", IconHtml = "<i class='fas fa-dolly' aria-hidden='true'></i>" },
                        new MenuItem() { MenuText = "Vzdálená inventura", MenuPath = "/action/stocktakingremote", IconHtml = "<i class='fas fa-question-circle fa-fw' aria-hidden='true'></i>" },
                        new MenuItem() { MenuText = "Seznam balíků spediční služby", MenuPath = "/action/deliverypackagesprintsheet", IconHtml = "<i class='fas fa-box-open' aria-hidden='true'></i>" },
                    } },
                new MenuItem() {
                    MenuText = "Seznamy", Column = 1, SubItems = new List<MenuItem>() {
                        new MenuItem() { MenuText = "Karty", MenuPath = "/article", IconHtml = "<i class='fas fa-cube fa-fw' aria-hidden='true'></i>" },
                        new MenuItem() { MenuText = "Pozice", MenuPath = "/position", IconHtml = "<i class='fas fa-building fa-fw' aria-hidden='true'></i>" },
                        new MenuItem() { MenuText = "Pracovníci", MenuPath = "/worker", Disabled = !auditor.Worker.WorkerClass.HasFlag(WorkerClassEnum.CanEditUsers), IconHtml = "<i class='fas fa-users fa-fw' aria-hidden='true'></i>" },
                        new MenuItem() { MenuText = "Sekce", MenuPath = "/section", IconHtml = "<i class='fas fa-folder fa-fw' aria-hidden='true'></i>" },
                        new MenuItem() { MenuText = "Výrobci", MenuPath = "/manufact", IconHtml = "<i class='fas fa-industry fa-fw' aria-hidden='true'></i>" },
                        new MenuItem() { MenuText = "Auta", MenuPath = "/truck", IconHtml = "<i class='fas fa-truck-moving fa-fw' aria-hidden='true'></i>" },
                        new MenuItem() { MenuText = "Prefixy", MenuPath = "/prefix", IconHtml = "<i class='fa fa-tags' aria-hidden='true'></i>" }
                    } },
                new MenuItem() {
                    MenuText = "Nastavení", Column = 2, SubItems = new List<MenuItem>() {
                        new MenuItem() { MenuText = "Interní zprávy", MenuPath = "/internmessage", Disabled = !auditor.Worker.WorkerClass.HasFlag(WorkerClassEnum.CanViewReports), IconHtml = "<i class='fas fa-comment-dots' aria-hidden='true'></i>" },
                        new MenuItem() { MenuText = "Nastavení terminálu", MenuPath = "/terminal/settings",  IconHtml = "<i class='fas fa-cog fa-fw' aria-hidden='true'></i>"},
                        new MenuItem() { MenuText = "Seznam terminálů", MenuPath = "/terminal/connections",  IconHtml = "<i class='fas fa-align-justify fa-fw' aria-hidden='true'></i>"},
                        new MenuItem() { MenuText = "Odesílání dat", MenuPath = "/message",  IconHtml = "<i class='fas fa-exchange-alt fa-fw' aria-hidden='true'></i>"},
                        new MenuItem() { MenuText = "Vzdálené akce", MenuPath = "/remoteaction", IconHtml = "<i class='fas fa-truck-monster' aria-hidden='true'></i>" },
                    } },
                new MenuItem() {
                    MenuText = "Reporty", Column = 2, SubItems = GetReportMenu(auditor)
                }
            };

            return result;
        }

        private List<MenuItem> GetReportMenu(Auditor auditor)
        {
            var reports = new S4.DAL.ReportDAL().ListReportType("WebPrint");
            var menuItemsList = new List<MenuItem>();
            var iconHtml = "<i class='fas fa-print' aria-hidden='true'></i>";
            var disable = false;

            foreach (var report in reports.Where(r => !string.IsNullOrWhiteSpace(r.Settings.ReportName)).OrderBy(_ => _.Settings.ReportName))
            {
                if (report.Settings.Flag != WorkerClassEnum.Any)
                    disable = !auditor.Worker.WorkerClass.HasFlag(report.Settings.Flag);
                else
                    disable = false;

                var menuItem = new MenuItem() { MenuText = report.Settings.ReportName, MenuPath = $"/report?id={report.ReportID}", IconHtml = iconHtml, Disabled = disable };
                menuItemsList.Add(menuItem);
            }

            return menuItemsList;
        }
    }
}
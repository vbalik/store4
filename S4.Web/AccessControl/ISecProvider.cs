﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.AccessControl
{
    public interface ISecProvider
    {
        Auditor Auditor { get; }

        string ApiToken { get; }

        string GetApiToken(string userID);

        (string userName, string newToken) CheckToken(string token);
    }
}

﻿using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;


namespace S4.Web.AccessControl
{
    public class SecProvider : ISecProvider
    {
        private const string TOKEN_HEADER = "S4_WEB_API";
        private const string TOKEN_PASSWORD = "})P5em}ge>![{Qxg";
        private const int TOKEN_TIMEOUT_MINUTES = 240;


        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IDataProtectionProvider _dataProtectionProvider;

        private string _userID;


        public SecProvider(IHttpContextAccessor httpContextAccessor, IDataProtectionProvider dataProtectionProvider)
        {
            _httpContextAccessor = httpContextAccessor;
            _dataProtectionProvider = dataProtectionProvider;
            _userID = _httpContextAccessor.HttpContext?.User.FindFirst(ClaimTypes.Sid)?.Value;
        }


        #region ISecProvider

        public Auditor Auditor
        {
            get
            {
                if (_userID == null)
                    return null;
                return Core.Singleton<AuditorCache>.Instance.GetItem(_userID);
            }
        }


        public string ApiToken
        {
            get
            {
                return MakeToken();
            }
        }

        public string GetApiToken(string userID)
        {
            return MakeToken(userID);
        }


        public (string userName, string newToken) CheckToken(string token)
        {
            var crypt = _dataProtectionProvider.CreateProtector(TOKEN_PASSWORD);
            var decrypted = crypt.Unprotect(token);

            var parts = decrypted.Split('\n');
            if (parts.Length != 3)
                return (null, null);

            // check header
            if (parts[0] != TOKEN_HEADER)
                return (null, null);

            // get userID
            string userID = parts[1];
            if (String.IsNullOrWhiteSpace(userID))
                return (null, null);

            // get validity
            if (!DateTime.TryParseExact(parts[2], "O", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.AssumeUniversal, out DateTime validTo))
                return (null, null);

            // check validity
            if (validTo < DateTime.Now)
                return (null, null);

            // if close to timeout, create new token
            string newToken = null;
            if (validTo.AddMinutes(- (TOKEN_TIMEOUT_MINUTES / 4)) < DateTime.Now)
                newToken = MakeToken(userID);

            return (userID, newToken);
        }

        #endregion


        private string MakeToken()
        {
            return MakeToken(_userID);
        }

        private string MakeToken(string userID)
        {
            _userID = userID;
            var validityTo = DateTime.Now.AddMinutes(TOKEN_TIMEOUT_MINUTES).ToUniversalTime();

            string token = String.Join("\n",
                TOKEN_HEADER,
                userID,
                validityTo.ToString("O", System.Globalization.CultureInfo.InvariantCulture)
                );

            var crypt = _dataProtectionProvider.CreateProtector(TOKEN_PASSWORD);
            return crypt.Protect(token);
        }
    }
}

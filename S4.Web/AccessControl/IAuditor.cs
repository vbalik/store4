﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.AccessControl
{
    public interface IAuditor
    {
        List<MenuItem> MenuItems { get; }
    }
}

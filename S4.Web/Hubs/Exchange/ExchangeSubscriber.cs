using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using S4.Core.Interfaces.Hubs;
using S4.Procedures.Messages;


namespace S4.Web.Hubs.Exchange
{
    public class ExchangeSubscriber
    {
        private readonly IHubContext<ExchangeHub, IExchangeHub> _exchangeHub;

        public ExchangeSubscriber(IHubContext<ExchangeHub, IExchangeHub> exchangeHub)
        {
            _exchangeHub = exchangeHub;
        }

        public async Task Changed(ExchangeMessage msg)
        {
            //send to exchange
            var jobName = new ExchangeUpdateValue(msg.JobNames);
            await _exchangeHub.Clients.All.UpdateValue(jobName);
        }

    }
}
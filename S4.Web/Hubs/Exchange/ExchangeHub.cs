using Microsoft.AspNetCore.SignalR;
using S4.Core.Interfaces.Hubs;
using System;
using System.Threading.Tasks;


namespace S4.Web.Hubs.Exchange
{
    public class ExchangeHub : Hub<IExchangeHub>
    {
        public const string CLIENT_HUB_URL = "/exchange-hub"; 
    }

}
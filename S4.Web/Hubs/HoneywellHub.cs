﻿using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using S4.Core.EAN;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Hubs
{
    public class HoneywellHub : Hub<IHoneywellHub>
    {
        public const string CLIENT_HUB_URL = "/honeywell-hub";

        public static ConcurrentDictionary<string, ClientHoneywell> ConnectedClients = new ConcurrentDictionary<string, ClientHoneywell>();

        public async override Task OnConnectedAsync()
        {
            var isScannerClient = !string.IsNullOrEmpty(Context.GetHttpContext().Request.Headers["user-agent"])
                                                && Context.GetHttpContext().Request.Headers["user-agent"] == "honeywellScanner";

            var isWebClient = !string.IsNullOrEmpty(Context.GetHttpContext().Request.Headers["user-agent"])
                                                && (Context.GetHttpContext().Request.Headers["user-agent"].ToString().Contains("Chrome")
                                                    || Context.GetHttpContext().Request.Headers["user-agent"].ToString().Contains("Moz"));

            var isS4AppClient = !string.IsNullOrEmpty(Context.GetHttpContext().Request.Headers["user-agent"])
                                                && Context.GetHttpContext().Request.Headers["user-agent"] == "s4App";

            // add connection
            ConnectedClients.TryAdd(Context.ConnectionId, new ClientHoneywell
            {
                ConnectionId = Context.ConnectionId,
                IpAddress = Context.GetHttpContext().Connection.RemoteIpAddress.ToString(),
                IsScannerClient = isScannerClient,
                IsWebClient = isWebClient,
                IsS4AppClient = isS4AppClient
            });

            // send scanner activity
            await AskDeviceScannerInUse();

            // send scanner status
            await SendDeviceScannerConnected();

            await base.OnConnectedAsync();
        }

        public async override Task OnDisconnectedAsync(Exception exception)
        {
            var ipAddressDevice = ConnectedClients.SingleOrDefault(_ => _.Value.ConnectionId == Context.ConnectionId).Value.IpAddress;

            ConnectedClients.TryRemove(Context.ConnectionId, out ClientHoneywell notUsed);

            // send scanner activity
            await AskDeviceScannerInUse();

            // send scanner status
            await SendDeviceScannerConnected();

            await base.OnDisconnectedAsync(exception);
        }

        public async Task SendScannedEan(string value)
        {
            try
            {
                var ean = new EAN(value);

                var clients = ConnectedClients
                            .Where(_ => _.Value.IpAddress == Context.GetHttpContext().Connection.RemoteIpAddress.ToString())
                            .Select(_ => _.Value.ConnectionId)
                            .ToArray();

                // add raw data eg. for carrier number
                var parts = ean.Parts.Select(_ => new { EANCode = _.EANCode.ToString(), Value = _.Value }).ToList();
                parts.Add(new { EANCode = "rawData", Value = value });

                await Clients.Clients(clients).ReceiveEan(JsonConvert.SerializeObject(parts));
            }
            catch(FormatException)
            {
                return;
            }
        }

        /// <summary>
        /// Ask if scanner is beign used by S4 android app
        /// </summary>
        /// <returns></returns>
        public async Task AskDeviceScannerInUse()
        {
            var clients = ConnectedClients
                            .Where(_ => _.Value.IpAddress == Context.GetHttpContext().Connection.RemoteIpAddress.ToString())
                            .ToList();

            var scannerInUse = clients.Any(_ => _.Value.IsS4AppClient);

            var ids = clients.Select(_ => _.Value.ConnectionId).ToArray();

            await Clients.Clients(ids).ReceiveDeviceScannerInUse(scannerInUse);
        }

        /// <summary>
        /// Send skenner status to web
        /// </summary>
        /// <returns></returns>
        public async Task SendDeviceScannerConnected()
        {
            var clients = ConnectedClients
                            .Where(_ => _.Value.IpAddress == Context.GetHttpContext().Connection.RemoteIpAddress.ToString())
                            .ToList();

            var ids = clients.Select(_ => _.Value.ConnectionId).ToArray();

            await Clients.Clients(ids).ReceiveDeviceScannerConnected(clients.Any(_ => _.Value.IsScannerClient));
        }
    }

    public interface IHoneywellHub
    {
        // send ean string
        Task SendScannedEan(string value);

        // web client
        Task ReceiveEan(string value);

        // ask for scanner activity
        Task AskDeviceScannerInUse();

        // receive scanner activity
        Task ReceiveDeviceScannerInUse(bool value);

        // receive scaner connected web
        Task ReceiveDeviceScannerConnected(bool value);
    }

    public class ClientHoneywell
    {
        public string ConnectionId { get; set; }
        public string IpAddress { get; set; }
        public bool IsScannerClient { get; set; }
        public bool IsWebClient { get; set; }
        public bool IsS4AppClient { get; set; }
    }
}
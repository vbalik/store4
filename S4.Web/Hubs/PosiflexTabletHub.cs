﻿using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using S4.Core.EAN;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Hubs
{
    public class PosiflexTabletHub : Hub<IPosiflexTabletHub>
    {
        public const string CLIENT_HUB_URL = "/posiflex-tablet-hub";

        public static ConcurrentDictionary<string, Client> ConnectedClients = new ConcurrentDictionary<string, Client>();

        public async override Task OnConnectedAsync()
        {
            var isPosiflexScannerService = !string.IsNullOrEmpty(Context.GetHttpContext().Request.Headers["user-agent"])
                                                && Context.GetHttpContext().Request.Headers["user-agent"] == "posiflexTablet";

            var ipAddressDevice = Context.GetHttpContext().Connection.RemoteIpAddress.ToString();

            ConnectedClients.TryAdd(Context.ConnectionId, new Client
            {
                ConnectionId = Context.ConnectionId,
                IpAddress = ipAddressDevice,
                IsScannerService = isPosiflexScannerService
            });

            // filtered client by ip address
            var clients = ConnectedClients
                            .Where(_ => _.Value.IpAddress == ipAddressDevice)
                            .ToList();

            // scanner service on
            await Clients.Clients(clients.Select(_ => _.Value.ConnectionId).ToArray()).ReceiveDeviceScannerStatus(clients.Any(_ => _.Value.IsScannerService));

            await base.OnConnectedAsync();
        }

        public async override Task OnDisconnectedAsync(Exception exception)
        {
            var ipAddressDevice = ConnectedClients.SingleOrDefault(_ => _.Value.ConnectionId == Context.ConnectionId).Value.IpAddress;

            ConnectedClients.TryRemove(Context.ConnectionId, out Client notUsed);

            // filtered client by ip address
            var clients = ConnectedClients
                            .Where(_ => _.Value.IpAddress == ipAddressDevice)
                            .ToList();

            // scanner service on
            await Clients.Clients(clients.Select(_ => _.Value.ConnectionId).ToArray()).ReceiveDeviceScannerStatus(clients.Any(_ => _.Value.IsScannerService));

            await base.OnDisconnectedAsync(exception);
        }

        public async Task SendScannedEan(string value)
        {
            EAN ean = null;

            try
            {
                ean = new EAN(value);
            }
            catch(FormatException)
            {
                return;
            }

            var clients = ConnectedClients
                            .Where(_ => _.Value.IpAddress == Context.GetHttpContext().Connection.RemoteIpAddress.ToString())
                            .Select(_ => _.Value.ConnectionId)
                            .ToArray();

            await Clients.Clients(clients).ReceiveEan(JsonConvert.SerializeObject(ean.Parts));
        }
    }

    public interface IPosiflexTabletHub
    {
        // send
        Task SendScannedEan(string value);

        // client
        Task ReceiveEan(string value);

        Task ReceiveDeviceScannerStatus(bool value);
    }

    public class Client
    {
        public string ConnectionId { get; set; }
        public string IpAddress { get; set; }
        public bool IsScannerService { get; set; }
    }
}
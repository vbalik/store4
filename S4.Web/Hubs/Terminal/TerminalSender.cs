﻿using Microsoft.AspNetCore.SignalR;
using S4.Entities.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Hubs.Terminal
{
    public class TerminalSender
    {
        private readonly IHubContext<TerminalHub, ITerminalHub> _terminalHub;

        public TerminalSender(IHubContext<TerminalHub, ITerminalHub> terminalHub)
        {
            _terminalHub = terminalHub;
        }


        public async Task AssignmentUpdated(TerminalJobAssignUpdate update)
        {
            var IDs = TerminalHub.FindIDsByWorkerID(update.WorkerID);
            if(IDs.Length > 0)
                await _terminalHub.Clients.Clients(IDs).AssignmentUpdated(update);
        }

        public async Task ResetAssignment(List<string> workers)
        {
            var IDs = TerminalHub.Connections().Where(_ => workers.Any(w => w == _.WorkerID)).Select(_ => _.ConnectionId).ToArray();
            await _terminalHub.Clients.AllExcept(IDs).ResetAssignment(null);
        }

        internal async Task InfoToTerminal(string message)
        {
            await _terminalHub.Clients.All.InfoToTerminal(message);
        }
    }
}

﻿using Microsoft.AspNetCore.SignalR;
using S4.Entities.Helpers;
using S4.Procedures.Messages;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;


namespace S4.Web.Hubs.Terminal
{
    public class TerminalSubscriberHostedService : Scheduling.HostedService
    {
        // caches messages in queue and send then (only once per workeID) every second
        private static readonly ConcurrentQueue<DirectionAssigmentUpdated> _msgQueue = new ConcurrentQueue<DirectionAssigmentUpdated>();

        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                await RunByTimer(cancellationToken);

                await Task.Delay(TimeSpan.FromSeconds(1), cancellationToken);
            }
        }


#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
        internal async Task DirectionAssigmentUpdated(DirectionAssigmentUpdated msg)
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
        {
            _msgQueue.Enqueue(msg);
        }


        private async Task RunByTimer(CancellationToken cancellationToken)
        {
            if (!_msgQueue.TryPeek(out var _))
                return;

            // group messages by workerID
            var workers = new HashSet<string>();
            while (_msgQueue.TryDequeue(out var msg))
            {
                if (!workers.Contains(msg.WorkerID))
                    workers.Add(msg.WorkerID);
            }

            // send info
            foreach (var workerID in workers)
            {
                if (cancellationToken.IsCancellationRequested)
                    break;
                await SendInfo(workerID);
            }
        }


        private async Task SendInfo(string workerID)
        {
            var dal = new DAL.DirectionAssignDAL();
            var assignments = dal.ScanAssignments(workerID);

            var terminalHub = Startup.GetService<IHubContext<TerminalHub, ITerminalHub>>();
            var sender = new TerminalSender(terminalHub);
            var update = new TerminalJobAssignUpdate()
            {
                WorkerID = workerID,
                Items = assignments.Select(_ => new TerminalJobAssignItem(_.JobID, _.CntDocs, _.CntItems)).ToList()
            };

            await sender.AssignmentUpdated(update);
        }
    }
}

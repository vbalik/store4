﻿using S4.Entities.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Web.Hubs.Terminal
{
    public interface ITerminalHub
    {
        Task LogOn(string workerID, string clientVersion, string terminalType);
        Task AssignmentUpdated(Entities.Helpers.TerminalJobAssignUpdate update);
        Task InfoToTerminal(string message);
        Task ResetAssignment(string update);
    }
}

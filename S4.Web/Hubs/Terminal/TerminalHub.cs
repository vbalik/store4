﻿using Microsoft.AspNetCore.SignalR;
using S4.Entities.Helpers;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace S4.Web.Hubs.Terminal
{
    public class TerminalHub : Hub<ITerminalHub>
    {
        public const string CLIENT_HUB_URL = "/terminal-hub";

        private static readonly ConcurrentDictionary<string, TerminalConnectionInfo> _conections = new ConcurrentDictionary<string, TerminalConnectionInfo>();


        public override Task OnConnectedAsync()
        {
            _conections.TryAdd(Context.ConnectionId, new TerminalConnectionInfo(Context.ConnectionId));
            return base.OnConnectedAsync();
        }


        internal static List<TerminalConnectionInfo> Connections()
        {
            return _conections.Values.ToList();
        }

        internal static string[] FindIDsByWorkerID(string workerID)
        {
            return _conections.Values.Where(i => i.WorkerID == workerID).Select(i => i.ConnectionId).ToArray();
        }


        public override Task OnDisconnectedAsync(Exception exception)
        {
            _conections.TryRemove(Context.ConnectionId, out _);
            return base.OnDisconnectedAsync(exception);
        }


#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
        public async Task LogOn(string workerID, string clientVersion, string terminalType)
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
        {
            _conections.AddOrUpdate(
                Context.ConnectionId, 
                new TerminalConnectionInfo(Context.ConnectionId) { WorkerID = workerID, ClientVersion = clientVersion, TerminalType = terminalType },
                (connectionId, item) =>
                {
                    item.WorkerID = workerID;
                    item.ClientVersion = clientVersion;
                    item.TerminalType = terminalType;
                    return item;
                }
                );
        }
    }
}

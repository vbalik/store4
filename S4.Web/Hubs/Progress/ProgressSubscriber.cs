using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using S4.DAL;
using S4.Entities;
using S4.Procedures.Messages;

namespace S4.Web.Hubs.Progress
{
    public class ProgressSubscriber
    {
        private readonly IHubContext<ProgressHub, IProgressHub> _progressHub;

        public ProgressSubscriber(IHubContext<ProgressHub, IProgressHub> progressHub)
        {
            _progressHub = progressHub;
        }

        public async Task Changed(ProgressMessage msg)
        {
            var ids = ProgressHub.FindIDsByUserID(msg.User);
            ProgressHub.SetMessage(msg);
            var message = new ProgressUpdateValue(msg.User, msg.Header, msg.Message, msg.NotifyType, msg.TaskID, msg.ProgressValue, msg.TransName);
            if (ids.Length > 0)
                await _progressHub.Clients.Clients(ids).UpdateValue(message);

        }

    }
}
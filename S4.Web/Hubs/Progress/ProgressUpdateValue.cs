using S4.Procedures.Messages;

namespace S4.Web.Hubs.Progress
{
    public class ProgressUpdateValue
    {
        public string UserID { get; set; }
        public string Header { get; set; }
        public string Message { get; set; }
        public int Color { get; set; }
        public string TaskID { get; set; }
        public int ProgressValue { get; set; }
        public string TransName { get; set; }

        public ProgressUpdateValue(string userID, string header, string message, NotifyType notifyType, string taskID, int progressValue, string transName)
        {
            UserID = userID;
            Header = header;
            Message = message;
            Color = (int)notifyType;
            TaskID = taskID;
            ProgressValue = progressValue;
            TransName = transName;
        }
    }
}
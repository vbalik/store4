using System.Collections.Generic;
using System.Threading.Tasks;

namespace S4.Web.Hubs.Progress
{
    public interface IProgressHub
    {
        Task UpdateValue(ProgressUpdateValue value);
        
    }
}
using Microsoft.AspNetCore.SignalR;
using S4.Procedures.Messages;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;


namespace S4.Web.Hubs.Progress
{
    public class ProgressHub : Hub<IProgressHub>
    {
        public const string CLIENT_HUB_URL = "/progress-hub"; 
        private static ConcurrentDictionary<string, string> _connectionsUsers = new ConcurrentDictionary<string, string>(); //key = ConnectionId, value = UserName
        private static ConcurrentDictionary<string, ProgressMessagesDict> _lastMessage = new ConcurrentDictionary<string, ProgressMessagesDict>(); //key = TaskId, value = UserMessages

        public override Task OnConnectedAsync()
        {
            RemoveOldMessagess();
            var userName = Context.GetHttpContext().Request.Query["userName"].ToString();
            if (!_connectionsUsers.ContainsKey(Context.ConnectionId))
                _connectionsUsers.TryAdd(Context.ConnectionId, userName);

            //send last message to user
            var ids = FindIDsByUserID(userName);
            if (ids.Length > 0)
            {
                var lastMessages = GetLastMessage(userName);
                if (lastMessages.Messages.Count > 0)
                {
                    foreach (var lastMessage in lastMessages.Messages.Values)
                    {
                        var message = new ProgressUpdateValue(userName, lastMessage.Header, lastMessage.Message, lastMessage.NotifyType, lastMessage.TaskID, lastMessage.ProgressValue, lastMessage.TransName);
                        Clients.Clients(ids).UpdateValue(message);
                    }
                }
            }
            return base.OnConnectedAsync();
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            if (_connectionsUsers.ContainsKey(Context.ConnectionId))
            {
                RemoveOldMessagess();
                _connectionsUsers.TryRemove(Context.ConnectionId, out _);
            }

            return base.OnDisconnectedAsync(exception);
        }

        internal static string[] FindIDsByUserID(string userID)
        {
            return _connectionsUsers.Where(i => i.Value == userID).Select(i => i.Key).ToArray();
        }

        internal static void SetMessage(ProgressMessage msg)
        {
            RemoveOldMessagess();
            ProgressMessagesDict userMessages = null;
            if (_lastMessage.TryGetValue(msg.User, out userMessages))
            {
                userMessages.AddOrUpdateMessage(msg.TaskID, msg);
                if (_lastMessage.TryGetValue(msg.User, out ProgressMessagesDict userMessagesOld))
                {
                    _lastMessage.TryUpdate(msg.User, userMessages, userMessagesOld);
                }
            }
            else
            {
                var userMessagesNew = new ProgressMessagesDict();
                userMessagesNew.AddOrUpdateMessage(msg.TaskID, msg);
                _lastMessage.TryAdd(msg.User, userMessagesNew);
            }
        }

        private static ProgressMessagesDict GetLastMessage(string userName)
        {
            RemoveOldMessagess();
            if (_lastMessage.TryGetValue(userName, out ProgressMessagesDict userMessages))
                return userMessages;
            else
                return new ProgressMessagesDict();
        }

        private static void RemoveOldMessagess()
        {
            foreach (var message in _lastMessage)
            {
                message.Value.RemoveOldMessagess();
                if (message.Value.Messages.Count == 0) //if empty, remove from dictionary
                    _lastMessage.TryRemove(message.Key, out _);
            }
        }

    }

}
using Microsoft.AspNetCore.SignalR;
using S4.Core.Interfaces.Hubs;

namespace S4.Web.Hubs.Direction
{
    public class DirectionHub : Hub<IDirectionHub>
    {
        public const string CLIENT_HUB_URL = "/direction-hub";
    }
}
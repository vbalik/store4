using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using S4.Core.Interfaces.Hubs;
using S4.DAL;
using S4.Entities;
using S4.Procedures.Messages;

namespace S4.Web.Hubs.Direction
{
    public class DirectionSubscriber
    {
        private readonly IHubContext<DirectionHub, IDirectionHub> _directionHub;

        public DirectionSubscriber(IHubContext<DirectionHub, IDirectionHub> directionHub)
        {
            _directionHub = directionHub;
        }

        public async Task DirectionItemsChanged(DirectionItemUpdated msg)
        {
            var dal = new DirectionDAL();
            var items = dal.GetItemsStatus(msg.DirectionID);

            var itemsTotalCnt = items.Sum(_ => _.Cnt);
            var itemsCompleted = items.Where(_ =>
                    _.ItemStatus == DirectionItem.DI_STATUS_RECEIVE_COMPLET
                    || _.ItemStatus == DirectionItem.DI_STATUS_DISPATCH_SAVED
                    || _.ItemStatus == DirectionItem.DI_STATUS_DISP_CHECKED)
                .Sum(_ => _.Cnt);

            await _directionHub.Clients.All.UpdateDirectionItemsCompleted(new List<DirectionUpdateValue>
            {
                new DirectionUpdateValue(msg.DirectionID, $"{itemsCompleted}/{itemsTotalCnt}")
            });
        }

        public async Task DirectionStatusChanged(DirectionStatusUpdated msg)
        {
            var dal = new DirectionDAL();
            var status = dal.GetStatus(msg.DirectionID);

            await _directionHub.Clients.All.UpdateDirectionStatus(new List<DirectionUpdateValue>
            {
                new DirectionUpdateValue(msg.DirectionID, status)
            });
        }
    }
}
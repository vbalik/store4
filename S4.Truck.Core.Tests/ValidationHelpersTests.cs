using S4.SharedLib.RemoteAction;
using S4.Truck.Core.Helpers;
using System;
using Xunit;

namespace S4.Truck.Core.Tests
{
    public class ValidationHelpersTests
    {
        [Theory]
        [InlineData("xxx", null)]
        [InlineData("xxx/cc", null)]
        [InlineData("xxx/cc/124", null)]
        [InlineData("xxx//124", null)]
        [InlineData(null, null)]
        [InlineData("dl/22/123", "DL/22/123")]
        [InlineData("dl/22/123/", null)]
        [InlineData("DOCINF*TYPE:DL*ID:123*INFO:DL/12/34", null)]
        [InlineData("DL-/12/34", null)]
        public void DocInfoValidation_OK(string value, string result)
        {
            var res = ValidationHelpers.DocInfoValidation(value);
            Assert.Equal(result, res);
        }


        [Theory]
        [InlineData("dl/22/123", "code128", DocIDSourceEnum.DocInfo, "DL/22/123", "DL/22/123")]
        [InlineData("DOCINFO*TYPE:DL*ID:123*INFO:DL/12/34", "QRcode", DocIDSourceEnum.DirectionID, "123", "DL/12/34")]
        public void TryParseScan_OK(string textData, string codeType, DocIDSourceEnum source, string id, string info)
        {
            var (isOK, docIDSource, docID, docInfo) = ValidationHelpers.TryParseScan(textData, codeType);
            Assert.True(isOK);
            Assert.Equal(source, docIDSource);
            Assert.Equal(id, docID);
            Assert.Equal(info, docInfo);
        }


        [Theory]
        [InlineData(null, null)]
        [InlineData("", null)]
        [InlineData("xxxxxxx", null)]
        public void TryParseScan_Fail(string textData, string codeType)
        {
            var (isOK, _, _, _) = ValidationHelpers.TryParseScan(textData, codeType);
            Assert.False(isOK);
        }

    }
}

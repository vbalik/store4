﻿using S4Truck.Core.Storages;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace S4.Truck.Core.Tests
{
    public class LocalDataStoreTests
    {
        [Fact]
        public async Task Add_Get_Count()
        {
            LocalDataStore lds = await InitDB();

            var da = new Entities.DirectionAction()
            {
                ActionType = SharedLib.RemoteAction.ActionTypeEnum.TruckLoadUp,
                DocIDSource = SharedLib.RemoteAction.DocIDSourceEnum.DocInfo,
                DocInfo = "aaa",
                DocID = "bbb"
            };

            var res = await lds.AddDirectionActionAsync(da);
            Assert.True(res);

            var res2 = await lds.AddDirectionActionAsync(da);
            Assert.False(res2);

            var cnt = await lds.CountDirectionActionsAsync();
            Assert.Equal(1, cnt);

            var da1 = await lds.GetDirectionActionAsync(da.Id);
            Assert.NotNull(da1);
            Assert.Equal("AAA", da1.DocInfo);
            Assert.Equal("BBB", da1.DocID);

            var list1 = await lds.GetDirectionActionsAsync(SharedLib.RemoteAction.ActionTypeEnum.TruckLoadUp);
            Assert.NotEmpty(list1);
            var list2 = await lds.GetDirectionActionsAsync(SharedLib.RemoteAction.ActionTypeEnum.TruckUnload);
            Assert.Empty(list2);
        }


        [Fact]
        public async Task Update()
        {
            LocalDataStore lds = await InitDB();
            var da = new Entities.DirectionAction()
            {
                ActionType = SharedLib.RemoteAction.ActionTypeEnum.TruckLoadUp,
                DocIDSource = SharedLib.RemoteAction.DocIDSourceEnum.DocInfo,
                DocInfo = "aaa",
                DocID = "bbb"
            };
            await lds.AddDirectionActionAsync(da);

            da.DocInfo = "1234";
            await lds.UpdateDirectionActionAsync(da);

            var da1 = await lds.GetDirectionActionAsync(da.Id);
            Assert.Equal("1234", da1.DocInfo);
        }


        [Fact]
        public async Task Cancel_NotSynced()
        {
            LocalDataStore lds = await InitDB();
            var da = new Entities.DirectionAction()
            {
                ActionType = SharedLib.RemoteAction.ActionTypeEnum.TruckLoadUp,
                DocIDSource = SharedLib.RemoteAction.DocIDSourceEnum.DocInfo,
                DocInfo = "aaa",
                DocID = "bbb"
            };
            await lds.AddDirectionActionAsync(da);

            await lds.CancelDirectionActionAsync(da);

            var da1 = await lds.GetDirectionActionAsync(da.Id);
            Assert.Null(da1);
        }

        [Fact]
        public async Task Cancel_Synced()
        {
            LocalDataStore lds = await InitDB();
            var da = new Entities.DirectionAction()
            {
                ActionType = SharedLib.RemoteAction.ActionTypeEnum.TruckLoadUp,
                DocIDSource = SharedLib.RemoteAction.DocIDSourceEnum.DocInfo,
                DocInfo = "aaa",
                DocID = "bbb",
                IsSynced = true
            };
            await lds.AddDirectionActionAsync(da);

            await lds.CancelDirectionActionAsync(da);

            var da1 = await lds.GetDirectionActionAsync(da.Id);
            Assert.NotNull(da1);
            Assert.True(da1.IsCanceled);
        }


        [Fact]
        public async Task TruckList_Set_Get()
        {
            LocalDataStore lds = await InitDB();
            // init
            await lds.SetTruckListAsync(new List<Entities.Truck>()
            {
                new Entities.Truck() { TruckID = 1, TruckRegist = "1"},
                new Entities.Truck() { TruckID = 2, TruckRegist = "2"},
                new Entities.Truck() { TruckID = 3, TruckRegist = "3"},
            });


            // act
            await lds.SetTruckListAsync(new List<Entities.Truck>()
            {
                new Entities.Truck() { TruckID = 2, TruckRegist = "2"},
                new Entities.Truck() { TruckID = 3, TruckRegist = "33"},
                new Entities.Truck() { TruckID = 4, TruckRegist = "4"},
            });

            // assert
            var list = await lds.GetTruckListAsync();
            Assert.Collection<Entities.Truck>(list,
                (i) => 
                {
                    Assert.Equal(2, i.TruckID);
                    Assert.Equal("2", i.TruckRegist);
                },
                (i) =>
                {
                    Assert.Equal(3, i.TruckID);
                    Assert.Equal("33", i.TruckRegist);
                },
                (i) =>
                {
                    Assert.Equal(4, i.TruckID);
                    Assert.Equal("4", i.TruckRegist);
                }
            );
        }


        private static async Task<LocalDataStore> InitDB()
        {
            var dbPath = $"{Guid.NewGuid()}.db3";
            LocalDataStore.CheckAndCreateDatabase(dbPath);
            var lds = new LocalDataStore(dbPath);
            return lds;
        }
    }
}

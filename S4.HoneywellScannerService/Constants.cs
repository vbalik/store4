﻿namespace S4.HoneywellScannerService
{
	public static class Constants
	{
		public const int DELAY_WEBSOCKET_CONNECT = 300; // milliseconds
		public const int DELAY_SCANNER_INIT = 1500; // milliseconds
		public const int DELAY_BETWEEN_LOG_MESSAGES = 5000; // milliseconds
		public const int SERVICE_RUNNING_NOTIFICATION_ID = 10000;
		public const string SERVICE_STARTED_KEY = "has_service_been_started";
		public const string BROADCAST_MESSAGE_KEY = "broadcast_message";
		public const string NOTIFICATION_BROADCAST_ACTION = "S4ScannerService.Notification.Action";

		public const string ACTION_START_SERVICE = "S4ScannerService.action.START_SERVICE";
		public const string ACTION_STOP_SERVICE = "S4ScannerService.action.STOP_SERVICE";
		public const string ACTION_RESTART_TIMER = "S4ScannerService.action.RESTART_TIMER";
		public const string ACTION_MAIN_ACTIVITY = "S4ScannerService.action.MAIN_ACTIVITY";

		/// <summary>
		/// Set signalR connection
		/// </summary>
		public const string WS_KEY_URL = "ServerUrl";
		public const string WS_METHOD = "SendScannedEan";
	}
}

﻿using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;

namespace S4.HoneywellScannerService
{
    [Service]
    public class ScannerService : Service
    {
        static readonly string TAG = typeof(ScannerService).FullName;
        static readonly string CHANNEL_ID = "scrApp1";
        private static HoneywellScanner _honeywellScanner;

        

        bool isStarted;
        Handler handler;
        Action connectScanner;
        Action scannerInit;

        public override void OnCreate()
        {
            base.OnCreate();

            WebSocketCommunication.GetInstance().WebSocketEvent += WebSocketScannerReInit;

            handler = new Handler();

            connectScanner = new Action(async () =>
            {
                string msg = "Skener připojen";
                Log.Debug(TAG, msg);
                await WebSocketCommunication.GetInstance().ConnectAsync();
                Intent i = new Intent(Constants.NOTIFICATION_BROADCAST_ACTION);
                i.PutExtra(Constants.BROADCAST_MESSAGE_KEY, msg);
                Android.Support.V4.Content.LocalBroadcastManager.GetInstance(this).SendBroadcast(i);
            });

            scannerInit = new Action(() =>
            {
                _scannerInit();

                string msg = "Skener je aktivní";
                Log.Debug(TAG, msg);
                Intent i = new Intent(Constants.NOTIFICATION_BROADCAST_ACTION);
                i.PutExtra(Constants.BROADCAST_MESSAGE_KEY, msg);
                Android.Support.V4.Content.LocalBroadcastManager.GetInstance(this).SendBroadcast(i);
            });
        }

        public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId)
        {
            if (intent.Action.Equals(Constants.ACTION_START_SERVICE))
            {
                if (isStarted)
                {
                    Log.Info(TAG, "OnStartCommand: The service is already running.");
                    handler.PostDelayed(connectScanner, Constants.DELAY_WEBSOCKET_CONNECT);
                }
                else
                {
                    Log.Info(TAG, "OnStartCommand: The service is starting.");
                    RegisterForegroundService();

                    handler.PostDelayed(connectScanner, Constants.DELAY_WEBSOCKET_CONNECT);
                    handler.PostDelayed(scannerInit, Constants.DELAY_SCANNER_INIT);

                    isStarted = true;
                }
            }
            else if (intent.Action.Equals(Constants.ACTION_STOP_SERVICE))
            {
                Log.Info(TAG, "OnStartCommand: The service is stopping.");
                StopForeground(true);
                StopSelf();
                isStarted = false;
            }
            else if (intent.Action.Equals(Constants.ACTION_RESTART_TIMER))
            {
                Log.Info(TAG, "OnStartCommand: Restarting the timer.");
            }

            // This tells Android not to restart the service if it is killed to reclaim resources.
            return StartCommandResult.Sticky;
        }

        public override IBinder OnBind(Intent intent)
        {
            // Return null because this is a pure started service. A hybrid service would return a binder.
            return null;
        }

        public override void OnDestroy()
        {
            // We need to shut things down.
            Log.Debug(TAG, "The TimeStamper has been disposed.");
            Log.Info(TAG, "OnDestroy: The started service is shutting down.");

            // Stop the handler.
            handler.RemoveCallbacks(connectScanner);

            // Remove the notification from the status bar.
            var notificationManager = (NotificationManager)GetSystemService(NotificationService);
            notificationManager.Cancel(Constants.SERVICE_RUNNING_NOTIFICATION_ID);

            isStarted = false;
            base.OnDestroy();
        }

        void RegisterForegroundService()
        {
            var channel = new NotificationChannel(CHANNEL_ID, "Channel", NotificationImportance.Default)
            {
                Description = "Foreground Service Channel"
            };

            var notificationManager = (NotificationManager)GetSystemService(NotificationService);
            notificationManager.CreateNotificationChannel(channel);

            var notification = new Notification.Builder(this, CHANNEL_ID)
            .SetContentTitle("S4 služba skeneru")
            .SetContentText("Po kliknutí zobrazíte stav")
            .SetSmallIcon(Resource.Drawable.Barcodescanner_5125)
            .SetContentIntent(BuildIntentToShowMainActivity())
            .SetOngoing(true)
            //.AddAction(BuildRestartTimerAction())
            //.AddAction(BuildStopServiceAction())
            .Build();

            // Enlist this instance of the service as a foreground service
            StartForeground(Constants.SERVICE_RUNNING_NOTIFICATION_ID, notification);
        }

        PendingIntent BuildIntentToShowMainActivity()
        {
            var notificationIntent = new Intent(this, typeof(MainActivity));
            notificationIntent.SetAction(Constants.ACTION_MAIN_ACTIVITY);
            notificationIntent.SetFlags(ActivityFlags.SingleTop | ActivityFlags.ClearTask);
            notificationIntent.PutExtra(Constants.SERVICE_STARTED_KEY, true);

            var pendingIntent = PendingIntent.GetActivity(this, 0, notificationIntent, PendingIntentFlags.UpdateCurrent);
            return pendingIntent;
        }

        private async void _honeywellScanner_CodeScanned(object sender, HoneywellScanner.HoneywellScannedEventArgs e)
        {
            await WebSocketCommunication.GetInstance().SendAsync(Constants.WS_METHOD, e.Text);
        }

        private void _scannerInit()
        {
            try
            {
                _honeywellScanner = new HoneywellScanner();
                _honeywellScanner.OpenBarcodeReader();
                _honeywellScanner.CodeScanned += _honeywellScanner_CodeScanned;
            }
            catch (Exception ex)
            {
                Log.Error(TAG, ex.Message);
            }
        }

        public void WebSocketScannerReInit(object sender, bool e)
        {
            if (!e) _scannerInit();
        }
    }
}
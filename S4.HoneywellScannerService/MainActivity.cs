﻿using Android.App;
using Android.OS;
using Android.Widget;
using Android.Content;
using Android.Util;
using System;
using Android.Preferences;

namespace S4.HoneywellScannerService
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true, Icon = "@drawable/Barcodescanner_5125")]
    public class MainActivity : Activity
    {
        static readonly string TAG = typeof(MainActivity).FullName;
        Intent startServiceIntent;
        bool isStarted = false;
        Button btn_start;
        TextView txt_scanner_activity;
        TextView txt_server_url;
        EditText entry_server_url;
        Button btn_save_server_url;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.activity_main);

            // init controls
            btn_start = FindViewById<Button>(Resource.Id.btn_start);
            btn_start.Click += _StartServiceButton_Click;
            txt_scanner_activity = FindViewById<TextView>(Resource.Id.txt_scanner_activity);
            FindViewById<TextView>(Resource.Id.txt_title).Text = "S4 služba skeneru";
            txt_server_url = FindViewById<TextView>(Resource.Id.txt_server_url);
            txt_server_url.Click += _ShowEditServerUrl_Click;
            entry_server_url = FindViewById<EditText>(Resource.Id.entry_server_url);
            btn_save_server_url = FindViewById<Button>(Resource.Id.btn_save_server_url);
            btn_save_server_url.Click += _SaveServerUrl_Click;

            if (savedInstanceState != null)
            {
                isStarted = savedInstanceState.GetBoolean(Constants.SERVICE_STARTED_KEY, false);
            }

            startServiceIntent = new Intent(this, typeof(ScannerService));
            startServiceIntent.SetAction(Constants.ACTION_START_SERVICE);

            var prefs = PreferenceManager.GetDefaultSharedPreferences(ApplicationContext);

            // web socket create instance
            WebSocketCommunication.CreateInstance(prefs.GetString(Constants.WS_KEY_URL, "http://localhost"));
            WebSocketCommunication.GetInstance().ScannerConnectedEvent += _IsScannerConnected;
            _IsScannerConnected(this, WebSocketCommunication.GetInstance().IsStatusConnected());
        }

        protected override void OnNewIntent(Intent intent)
        {
            if (intent == null)
            {
                return;
            }

            var bundle = intent.Extras;
            if (bundle != null)
            {
                if (bundle.ContainsKey(Constants.SERVICE_STARTED_KEY))
                {
                    isStarted = true;
                }
            }
        }

        protected override void OnSaveInstanceState(Bundle outState)
        {
            outState.PutBoolean(Constants.SERVICE_STARTED_KEY, isStarted);
            base.OnSaveInstanceState(outState);
        }

        private void _StartServiceButton_Click(object sender, System.EventArgs e)
        {
            StartService(startServiceIntent);
            Log.Info(TAG, "User requested that the service be started.");

            isStarted = true;

            txt_scanner_activity.Text = "Restartuje se ...";
            txt_scanner_activity.SetTextColor(Android.Graphics.Color.Black);

            btn_start.Enabled = false;
        }

        private void _IsScannerConnected(object sender, bool connected)
        {
            btn_start.Enabled = true;

            if(connected) {
                txt_scanner_activity.Text = "Připojen";
                txt_scanner_activity.SetTextColor(Android.Graphics.Color.ForestGreen);
            }
            else
            {
                txt_scanner_activity.Text = "Nepřipojen";
                txt_scanner_activity.SetTextColor(Android.Graphics.Color.Red);
            }
        }

        private void _ShowEditServerUrl_Click(object sender, EventArgs e)
        {
            txt_server_url.Visibility = Android.Views.ViewStates.Invisible;
            entry_server_url.Visibility = Android.Views.ViewStates.Visible;
            btn_save_server_url.Visibility = Android.Views.ViewStates.Visible;

            var prefs = PreferenceManager.GetDefaultSharedPreferences(ApplicationContext);
            var value = prefs.GetString(Constants.WS_KEY_URL, string.Empty);
            entry_server_url.Text = value;
        }

        private void _SaveServerUrl_Click(object sender, EventArgs e)
        {
            txt_server_url.Visibility = Android.Views.ViewStates.Visible;
            entry_server_url.Visibility = Android.Views.ViewStates.Invisible;
            btn_save_server_url.Visibility = Android.Views.ViewStates.Invisible;

            var value = entry_server_url.Text;

            if (string.IsNullOrEmpty(value)) return;

            var prefs = PreferenceManager.GetDefaultSharedPreferences(ApplicationContext);
            prefs.Edit()
                .PutString(Constants.WS_KEY_URL, value)
                .Apply();
        }
    }
}
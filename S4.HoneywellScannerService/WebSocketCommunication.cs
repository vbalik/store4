﻿using System;
using Microsoft.AspNetCore.SignalR.Client;
using Android.Util;
using System.Threading.Tasks;

namespace S4.HoneywellScannerService
{
    public class WebSocketCommunication
    {
        static readonly string TAG = typeof(WebSocketCommunication).FullName;

        HubConnection _connection;

        public event EventHandler<bool> WebSocketEvent;

        public event EventHandler<bool> ScannerConnectedEvent;

        private static WebSocketCommunication CONNECTION;

        /// <summary>
        /// Singleton create websocket instance
        /// </summary>
        /// <param name="urlEndpoint"></param>
        /// <returns></returns>
        public static WebSocketCommunication CreateInstance(string urlEndpoint)
        {
            if (CONNECTION == null)
                CONNECTION = new WebSocketCommunication(urlEndpoint);

            return CONNECTION;
        }

        /// <summary>
        /// Singleton get websocket instance
        /// </summary>
        /// <returns></returns>
        public static WebSocketCommunication GetInstance() => CONNECTION;

        public WebSocketCommunication(string urlEndpoint)
        {
            if (string.IsNullOrEmpty(urlEndpoint))
                throw new ArgumentNullException("Url endpoint is null");

            if (!urlEndpoint.StartsWith("http://") && !urlEndpoint.StartsWith("https://"))
                urlEndpoint = $"http://{urlEndpoint}";

            // create connection
            _connection = new HubConnectionBuilder()
                    .WithUrl(urlEndpoint, httpConnectionOptions =>
                    {
                        httpConnectionOptions.Headers.Add("user-agent", "honeywellScanner");
                    })
                    .Build();

            _connection.Closed += async (error) =>
            {
                Log.Info(TAG, "-- socket connection closed");
                ScannerConnectedEvent.Invoke(this, false);
                await Task.Delay(1500);
                await ConnectAsync();
            };

            // register receive listeners
            AddReceiveDeviceScannerInUse();
        }

        public bool IsStatusConnected() => _connection.State == HubConnectionState.Connected;

        public async Task ConnectAsync()
        {
            if(_connection.State.Equals(HubConnectionState.Disconnected))
            {
                try
                {
                    await _connection.StartAsync();
                    ScannerConnectedEvent.Invoke(this, true);
                }
                catch(Exception)
                {
                    ScannerConnectedEvent.Invoke(this, false);
                }
            }
            else
            {
                ScannerConnectedEvent.Invoke(this, true);
            }
        }

        public async Task SendAsync(string methodName, object data)
        {
            if(_connection.State.Equals(HubConnectionState.Connected))
                await _connection.InvokeAsync(methodName, data);
        }
        private void AddReceiveDeviceScannerInUse()
        {
            _connection.On<bool>("ReceiveDeviceScannerInUse", inUse =>
            {
                WebSocketEvent.Invoke(this, inUse);
            });
        }
    }
}
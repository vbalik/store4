﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Honeywell.AIDC.CrossPlatform;

namespace S4.HoneywellScannerService
{
    public class HoneywellScanner
    {
        private BarcodeReader _barcodeReader;
        private SynchronizationContext mUIContext = SynchronizationContext.Current;

        public class HoneywellScannedEventArgs : EventArgs
        {
            public uint ID { get; set; }
            public BarCodeTypeEnum BarCodeType { get; set; }
            public string Text { get; set; }
        }
        public event EventHandler<HoneywellScannedEventArgs> CodeScanned;

        /// <summary>
        /// Opens the barcode reader. This method should be called from the
        /// main UI thread because it also updates the button states.
        /// </summary>
        public async void OpenBarcodeReader()
        {
            if (_barcodeReader == null)
                _barcodeReader = await GetBarCodeReader();
            if (_barcodeReader != null && !_barcodeReader.IsReaderOpened)
            {
                var result = await _barcodeReader.OpenAsync();
                if (result.Code == BarcodeReader.Result.Codes.SUCCESS ||
                    result.Code == BarcodeReader.Result.Codes.READER_ALREADY_OPENED)
                {
                    SetScannerAndSymbologySettings();
                }
                else
                {
                    throw new Exception($"OpenAsync failed, Code:{ result.Code } Message:{result.Message}");
                }

                result = await _barcodeReader.EnableAsync(true); // Enables or disables barcode reader
                if (result.Code != BarcodeReader.Result.Codes.SUCCESS)
                {
                    throw new Exception($"EnableAsync failed, Code:{ result.Code } Message:{result.Message}");
                }
            }
        }

        /// <summary>
        /// Closes the barcode reader. This method should be called from the
        /// main UI thread because it also updates the button states.
        /// </summary>
        public async void CloseBarcodeReader()
        {
            if (_barcodeReader != null && _barcodeReader.IsReaderOpened)
            {
                BarcodeReader.Result result = await _barcodeReader.EnableAsync(false); // Enables or disables barcode reader
                if (result.Code != BarcodeReader.Result.Codes.SUCCESS)
                {
                    throw new Exception($"EnableAsync failed, Code:{ result.Code } Message:{result.Message}");
                }

                result = await _barcodeReader.CloseAsync();
                if (result.Code != BarcodeReader.Result.Codes.SUCCESS)
                {
                    throw new Exception($"CloseAsync failed, Code:{ result.Code } Message:{result.Message}");
                }
            }
        }

        private async Task<BarcodeReader> GetBarCodeReader()
        {
            // Queries the list of readers that are connected to the mobile computer.
            BarcodeReaderInfo readerInfo = (await BarcodeReader.GetConnectedBarcodeReaders()).FirstOrDefault();
            if (readerInfo != null)
            {
                var barcodeReader = new BarcodeReader(readerInfo.ScannerName);
                // Add an event handler to receive barcode data.
                // Even though we may have multiple reader sessions, we only
                // have one event handler. In this app, no matter which reader
                // the data come frome it will update the same UI controls.
                barcodeReader.BarcodeDataReady += _barcodeReader_BarcodeDataReady;

                return barcodeReader;
            }
            return null;
        }

        private void _barcodeReader_BarcodeDataReady(object sender, BarcodeDataArgs e)
        {
            mUIContext.Post(_ =>
            {
                OnCodeScanned(e);
            }
                , null);
        }

        private void OnCodeScanned(BarcodeDataArgs e)
        {
            BarCodeTypeEnum barCodeType = EanTypes.Contains(e.SymbologyType) ? BarCodeTypeEnum.Ean : BarCodeTypeEnum.Undefined;
            CodeScanned?.Invoke(this, new HoneywellScannedEventArgs() { ID = e.SymbologyType, BarCodeType = barCodeType, Text = e.Data });
        }

        private uint[] EanTypes => new uint[]
        {
            BarcodeSymbologies.Maxicode,
            BarcodeSymbologies.Interleaved2Of5,
            BarcodeSymbologies.Tlc39,
            BarcodeSymbologies.Trioptic39,
            BarcodeSymbologies.Upca,
            BarcodeSymbologies.Upce,
            BarcodeSymbologies.UpcCoupon,
            BarcodeSymbologies.DotCode,
            BarcodeSymbologies.Qr,
            BarcodeSymbologies.GridMatrix,
            BarcodeSymbologies.Codabar,
            BarcodeSymbologies.CodablockA,
            BarcodeSymbologies.CodablockF,
            BarcodeSymbologies.Code11,
            BarcodeSymbologies.Code39,
            BarcodeSymbologies.Code93,
            BarcodeSymbologies.Gs1128,
            BarcodeSymbologies.Isbt128,
            BarcodeSymbologies.DataMatrix,
            BarcodeSymbologies.Ean8,
            BarcodeSymbologies.Ean13,
            BarcodeSymbologies.Gs1DataBarExpanded,
            BarcodeSymbologies.Gs1DataBarLimited,
            BarcodeSymbologies.Gs1DataBarOmniDir,
            BarcodeSymbologies.Code128
        };


        private async void SetScannerAndSymbologySettings()
        {
            if (_barcodeReader.IsReaderOpened)
            {
                Dictionary<string, object> settings = new Dictionary<string, object>()
                    {
                        {_barcodeReader.SettingKeys.TriggerScanMode, _barcodeReader.SettingValues.TriggerScanMode_OneShot },
                        {_barcodeReader.SettingKeys.Code128Enabled, true },
                        {_barcodeReader.SettingKeys.Code39Enabled, true },
                        {_barcodeReader.SettingKeys.Ean8Enabled, true },
                        {_barcodeReader.SettingKeys.Ean8CheckDigitTransmitEnabled, true },
                        {_barcodeReader.SettingKeys.Ean13Enabled, true },
                        {_barcodeReader.SettingKeys.Ean13CheckDigitTransmitEnabled, true },
                        {_barcodeReader.SettingKeys.UpcAEnable, true },
                        {_barcodeReader.SettingKeys.UpcACheckDigitTransmitEnabled, true },
                        {_barcodeReader.SettingKeys.UpcATranslateEan13, true },
                        {_barcodeReader.SettingKeys.Interleaved25Enabled, true },
                        {_barcodeReader.SettingKeys.Interleaved25MaximumLength, 100 },
                        {_barcodeReader.SettingKeys.Postal2DMode, _barcodeReader.SettingValues.Postal2DMode_Usps }
                    };

                BarcodeReader.Result result = await _barcodeReader.SetAsync(settings);
                if (result.Code != BarcodeReader.Result.Codes.SUCCESS)
                {
                    throw new Exception($"Symbology settings failed, Code:{ result.Code } Message:{result.Message}");
                }
            }
        }
    }

    public enum BarCodeTypeEnum { Undefined, Ean }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace S4mp.Core
{
    public interface IColorsResolver
    {
        Color GetNamedColor(Colors.NamedColorEnum color);
    }
}

﻿namespace S4mp.Core
{
    /// <summary>
    /// Used for cacheing printers by type
    /// </summary>
    public enum PrinterCacheTypeEnum
    {
        Default,
        Dispatch,
        PrintCode,
        PrintCarrierLabels
    }
}

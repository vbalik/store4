﻿using S4mp.Core.ViewModel;
using Xamarin.Forms;

namespace S4mp.Core
{
    public class SelectableListViewItem<T> : BaseViewModel
    {
        public SelectableListViewItem()
        {
            IsDisabled = false;
        }

        public T Item { get; set; }

        /// <summary>
        /// Set item selected (switch)
        /// </summary>
        private bool _isSelected;
        public bool IsSelected
        {
            get => _isSelected;
            set => SetField(ref _isSelected, value);
        }

        /// <summary>
        /// Set item disabled then change text color
        /// </summary>
        private bool _isDisabled;
        public bool IsDisabled
        {
            get => _isDisabled;
            set
            {
                SetField(ref _isDisabled, value);
                DisabledColor = Color.Black; // only for fire changed value
            }
        }

        /// <summary>
        /// Get disabled text color
        /// </summary>
        private Color _disabledColor;
        public Color DisabledColor
        {
            get => _disabledColor;
            set => SetField(ref _disabledColor, IsDisabled ? Color.Gray : Color.Black);
        }
    }
}

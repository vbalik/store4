﻿namespace S4mp.Core
{
    public interface IDevice
    {
        string GetIdentifier();
    }
}
﻿namespace S4mp.Core
{
    public interface IRefreshable
    {
        void RefreshData();
    }
}
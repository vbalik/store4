﻿using S4mp.Core;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace S4mp.Core.Misc
{
    public class EmptyValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var fs = new FormattedString();
            if (value == null)
                fs.Spans.Add(new Span { Text = ((string)parameter) ?? "?", ForegroundColor = Color.Gray, FontSize = Consts.TEXT_SIZE - 1 });
            else
            {
                var fontSize = (((string)value).Length > 10) ? Consts.TEXT_SIZE - 1 : Consts.TEXT_SIZE;
                fs.Spans.Add(new Span { Text = (string)value, FontSize = fontSize });
            }
            return fs;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using S4mp.Core;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace S4mp.Core.Misc
{
    public class ExpirationDateValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (targetType == typeof(FormattedString))
                return ConvertToFormattedString(value, parameter);
            else if (targetType == typeof(string))
                return ConvertToString(value, parameter);
            else
                throw new NotImplementedException();

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();

        }

        private object ConvertToString(object value, object parameter)
        {
            if (value == null)
                return null;
            else
                return (value is DateTime?) ? S4.Core.Data.Formating.DateTimeToExpStr((DateTime?)value) : value.ToString();
        }

        private object ConvertToFormattedString(object value, object parameter)
        {
            var fs = new FormattedString();
            if (value == null)
                fs.Spans.Add(new Span { Text = ((string)parameter) ?? "?", ForegroundColor = Color.Gray, FontSize = Consts.TEXT_SIZE - 1 });
            else
            {
                var text = (value is DateTime?) ? S4.Core.Data.Formating.DateTimeToExpStr((DateTime?)value) : value.ToString();
                fs.Spans.Add(new Span { Text = text, FontSize = Consts.TEXT_SIZE });
            }
            return fs;
        }

    }
}

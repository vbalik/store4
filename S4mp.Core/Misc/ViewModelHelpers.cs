﻿using S4.Core.EAN;
using S4mp.Core.EANCode;
using System;
using System.Globalization;

namespace S4mp.Core.Misc
{
	public static class ViewModelHelpers
	{
		/// <summary>
		/// Get batch number from batch or serial
		/// [used for Medtronik]
		/// </summary>
		/// <param name="ean"></param>
		/// <returns></returns>
		public static string GetBatchNumber(EANCode.EANCode ean)
		{
			var batchNum = ean[EANCodePartEnum.BatchNumber]?.Value;

			if (string.IsNullOrEmpty(batchNum))
				batchNum = ean[EANCodePartEnum.SerialNumber]?.Value;

			return batchNum ?? string.Empty;
		}

		public static DateTime? ParseExpirationDate(string value)
		{
            string[] formats = new string[] { "d.M.yyyy", "d/M/yyyy", "d.M.yy", "d/M/yy", "d.M", "d/M" };

            if (string.IsNullOrWhiteSpace(value))
                return null;

            // check for 'x.1' situation
            string[] parts = value.Split('.', '/');

            // if second value is more than 12 (months) then change format to M.yy
            if (parts.Length == 2 && int.TryParse(parts[1], out var year) && year > 12)
            {
                formats = new string[] { "M.yy" };
            }

            // set Expiration date as date resolved from string
            DateTime result;
            if (DateTime.TryParseExact(value, formats, new CultureInfo("cs-CZ"), DateTimeStyles.None, out result))
            {
                if (int.Parse(result.ToString("yyyy")) < 2000)
                    result = result.AddYears(100);

                return result;
            }
            else
            {
                return null;
            }
        }
	}
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace S4mp.Core.Misc
{
    public class DocInfoValueConverter : IValueConverter
    {
        private const string SEPARATOR = "/";

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;

            var parts = ((string)value).Split(new string[] { SEPARATOR }, StringSplitOptions.RemoveEmptyEntries);
            var fs = new FormattedString();
            fs.Spans.Add(new Span { Text = parts[0].Trim(), ForegroundColor = Color.Black, FontSize = Consts.TEXT_SIZE, FontAttributes = FontAttributes.Bold });
            if (parts.Length > 1)
            {
                for (int i = 1; i < parts.Length; i++)
                {
                    fs.Spans.Add(new Span { Text = "/ ", ForegroundColor = Color.Gray, FontSize = Consts.TEXT_SIZE - 1 });
                    fs.Spans.Add(new Span { Text = parts[i].Trim(), ForegroundColor = Color.Black, FontSize = Consts.TEXT_SIZE, FontAttributes = FontAttributes.Bold });
                }
            }
            return fs;
        }


        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

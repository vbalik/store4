﻿using System;
using System.Globalization;

namespace S4mp.Core.Misc
{
    public sealed class ExpirationDateHelper
    {
        public static string ParseExpirationDate(string expirationDate)
        {
            var yearMonth = expirationDate?.Substring(0, 4);
            var day = expirationDate?.Substring(4, 2);

            if (day.Equals("00"))
            {
                DateTime dateResult;
                DateTime.TryParseExact(yearMonth, "yyMM", new CultureInfo("cs-CZ"), DateTimeStyles.None, out dateResult);

                var lastDayInMonth = DateTime.DaysInMonth(dateResult.Year, dateResult.Month);

                expirationDate = yearMonth + lastDayInMonth;
            }

            if (DateTime.TryParseExact(expirationDate, "yyMMdd", new CultureInfo("cs-CZ"), DateTimeStyles.None, out var expirationDateResult))
                return expirationDateResult.ToString("d.M.yy");

            return string.Empty;
        }

        public static string ParseExpirationDateEntry(string expirationDate)
        {
            if (!expirationDate.Contains(".")) return expirationDate ?? string.Empty;

            var parts = expirationDate.Split('.');

            if (parts.Length == 2 && int.TryParse(parts[1], out var year) && year > 12)
            {
                // add last day in month
                if (DateTime.TryParseExact(expirationDate, new string[] { "MM.yy", "M.yy" }, new CultureInfo("cs-CZ"), DateTimeStyles.None, out var dateResult))
                {
                    var lastDayInMonth = DateTime.DaysInMonth(dateResult.Year, dateResult.Month);
                    expirationDate = $"{lastDayInMonth}.{ string.Join(".", parts)}";
                }

                if (DateTime.TryParseExact(expirationDate, new string[] { "d.M.yy", "dd.M.yy", "dd.MM.yy" }, new CultureInfo("cs-CZ"), DateTimeStyles.None, out var expirationDateResult))
                    expirationDate = expirationDateResult.ToString("d.M.yy");
            }

            return expirationDate ?? string.Empty;
        }
    }
}

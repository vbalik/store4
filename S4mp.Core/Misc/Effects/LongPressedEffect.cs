﻿using System.Windows.Input;
using Xamarin.Forms;

/// <summary>
/// Long pressed effect. Used for invoking commands on long press detection cross platform
/// </summary>
namespace S4mp.Core.Misc.Effects
{
    public class LongPressedEffect : RoutingEffect
    {
        public LongPressedEffect() : base("S4mp.LongPressedEffect")
        {
        }




        public static readonly BindableProperty Command_LongPressProperty = BindableProperty.CreateAttached("Command_LongPress", typeof(ICommand), typeof(LongPressedEffect), (object)null);
        public static ICommand GetCommand_LongPress(BindableObject view)
        {
            return (ICommand)view.GetValue(Command_LongPressProperty);
        }

        public static void SetCommand_LongPress(BindableObject view, ICommand value)
        {
            view.SetValue(Command_LongPressProperty, value);
        }


        public static readonly BindableProperty Command_ClickProperty = BindableProperty.CreateAttached("Command_Click", typeof(ICommand), typeof(LongPressedEffect), (object)null);
        public static ICommand GetCommand_Click(BindableObject view)
        {
            return (ICommand)view.GetValue(Command_ClickProperty);
        }

        public static void SetCommand_Click(BindableObject view, ICommand value)
        {
            view.SetValue(Command_ClickProperty, value);
        }


        public static readonly BindableProperty CommandParameterProperty = BindableProperty.CreateAttached("CommandParameter", typeof(object), typeof(LongPressedEffect), (object)null);
        public static object GetCommandParameter(BindableObject view)
        {
            return view.GetValue(CommandParameterProperty);
        }

        public static void SetCommandParameter(BindableObject view, object value)
        {
            view.SetValue(CommandParameterProperty, value);
        }
    }
}
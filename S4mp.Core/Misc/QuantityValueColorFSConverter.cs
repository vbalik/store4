﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace S4mp.Core.Misc
{
    /// <summary>
    /// Quantity value colored FormatedString
    /// </summary>
    public class QuantityValueColorFSConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;

            var fs = new FormattedString();

            if(value is decimal v)
            {
                var color = v == 0 ? Color.Black : Color.Green;
                var val = (v).ToString("n2", System.Globalization.CultureInfo.GetCultureInfo("cs-cz"));

                fs.Spans.Add(new Span { Text = val, ForegroundColor = color, FontSize = Consts.TEXT_SIZE, FontAttributes = FontAttributes.Bold });

                return fs;
            }

            return value.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

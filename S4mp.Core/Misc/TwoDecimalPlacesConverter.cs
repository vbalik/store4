﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace S4mp.Core.Misc
{
    public class TwoDecimalPlacesConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is decimal val)
            {
                return val.ToString("0.##");
            }
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is decimal val)
            {
                return val.ToString("0.##");
            }
            return value;
        }
    }
}

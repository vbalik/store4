﻿using S4mp.Core;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Xamarin.Forms;

namespace S4mp.Core.Misc
{
    public class ArticleInfoConverter : IValueConverter
    {
        private const string SEPARATOR = " - ";

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;

            var parts = ((string)value).Split(new string[] { SEPARATOR }, StringSplitOptions.RemoveEmptyEntries);
            var fs = new FormattedString();
            fs.Spans.Add(new Span { Text = parts[0], ForegroundColor = Color.Black, FontSize = Consts.TEXT_SIZE, FontAttributes = FontAttributes.Bold });
            if (parts.Length > 1)
            {
                var rest = String.Join(SEPARATOR, parts.Skip(1).ToArray());
                fs.Spans.Add(new Span { Text = SEPARATOR, ForegroundColor = Color.Gray, FontSize = Consts.TEXT_SIZE - 1 });
                fs.Spans.Add(new Span { Text = rest, ForegroundColor = Color.Black, FontSize = Consts.TEXT_SIZE - 1 });
            }
            return fs;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace S4mp.Core.Misc
{
    public class EmptyValueToZeroConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return 0;

            var sValue = value as string;

            if (string.IsNullOrEmpty(sValue) || !int.TryParse(sValue, out var result))
                return 0;

            return value;
        }
    }
}

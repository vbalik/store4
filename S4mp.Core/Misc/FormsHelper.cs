﻿using System;

using Xamarin.Forms;

namespace S4mp.Core.Misc
{
	public class FormsHelper
	{
		public FormsHelper()
		{
			
		}

		public Page GetParentPage(Element element)
		{
			while (!(element is Page))
			{
				element = element.Parent;
			}

			return element as Page;
		}
	}
}

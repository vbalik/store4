﻿using System;

namespace S4mp.Core
{
	public class Consts
	{
		//application properties
        public const string PROP_HW_DESCRIPTION = "HWDescription";
        public const string PROP_APP_VERSION = "AppVersion";
        public const string PROP_OS_DESCRIPTION = "OSDescription";
        public const string PROP_MONO_DESCRIPTION = "MonoDescription";
        public const string PROP_STARTUP_TIME= "StartupTime";

		//xml settings paths
		public const string XMLPATH_API_URL = "api_url";
		public const string XMLPATH_USER = "user";
		public const string XMLPATH_PASSWORD = "password";
		public const string XMLPATH_TEXTSIZE = "textsize";
		public const string XMLPATH_INSTANCE_NAME = "instance_name";
		public const string XMLPATH_CONNECT_SCANNER_WEB = "connect_scanner_web";
		public const string XMLPATH_ARGOMED_FEATURES = "argomed_features";
		public const string XMLPATH_DISPATCH_SCANNER_ASSIGN_USER = "dispatch_scanner";
		public const string XMLPATH_BARCODE_DISALLOWED_VALUE_PATTERN = "barcode_disallowed_value_pattern";
        public const string XMLPATH_DISPATCH_DEFAULT_POSITION_ID = "dispatch_default_position_id";
        public const string XMLPATH_SHOW_SPEC_FEATURES_ALERT_INFO_LIST = "show_spec_features_alert_info_list";

        //font size
        public static double TEXT_SIZE;
        public static double HEADER_TEXT_SIZE;
        public static double RULLER_TEXT_SIZE;

        //strings
        public const string USER_NAME_PLACEHOLDER = "Uživatelské jméno";
        public const string PASSWORD_PLACEHOLDER = "Heslo";
        public const string OK_BUTTON = "OK";
        public const string CANCEL_BUTTON = "Zruš";

        //display
        public static double DENSITY_COEF = 1;

        //printing
        public static string PRINTER_CLASS = "PrinterClass";
        public static string PRINTER_REPORT = "PrinterReport";

        // barcode
        public static string BARCODE_UNKNOWN_VALUE = "unknown";
    }
}


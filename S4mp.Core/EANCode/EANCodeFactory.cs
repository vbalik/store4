﻿using S4.Core.EAN;
using S4.Core.HIBCC;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace S4mp.Core.EANCode
{
    public static class EANCodeFactory
    {
        public static EANCode Create(string text)
        {
            if (string.IsNullOrEmpty(text))
                return null;

            // if start with + then parse hibcc, if result is null then continue
            if (text.StartsWith("+"))
            {
                var result = _ParseHIBCC(text);
                if (result != null)
                    return result;
            }

            return _ParseEAN(text);
        }

        internal static EANCode _ParseEAN(string text)
        {
            DateTime getDateTimeCorrectDay(string dateString)
            {
                var yearMonth = dateString?.Substring(0, 4);
                var day = dateString?.Substring(4, 2);

                DateTime dateResult;

                if (day.Equals("00"))
                {
                    DateTime.TryParseExact(yearMonth, "yyMM", new CultureInfo("cs-CZ"), DateTimeStyles.None, out dateResult);
                    var lastDayInMonth = DateTime.DaysInMonth(dateResult.Year, dateResult.Month);
                    return new DateTime(dateResult.Year, dateResult.Month, lastDayInMonth);
                }

                DateTime.TryParseExact(dateString, "yyMMdd", new CultureInfo("cs-CZ"), DateTimeStyles.None, out dateResult);
                return dateResult;
            }

            var code = new EANCode(text);

            var ean = new EAN(text);

            var eanMap = new Dictionary<EANCodeEnum, EANCodePartEnum>
            {
                { EANCodeEnum.ShippingContainerCode, EANCodePartEnum.ShippingCode },
                { EANCodeEnum.BatchNumber, EANCodePartEnum.BatchNumber },
                { EANCodeEnum.ExpirationDate, EANCodePartEnum.ExpirationDate },
                { EANCodeEnum.SerialNumber, EANCodePartEnum.SerialNumber },
            };

            foreach (var part in ean.Parts)
            {
                if (eanMap.TryGetValue(part.EANCode, out var valueCode))
                    code.AddPart(new EANCodePart
                    {
                        EANCode = valueCode,
                        Value = part.Value,
                        ValueDate = valueCode == EANCodePartEnum.ExpirationDate
                            ? (DateTime?) getDateTimeCorrectDay(part.Value)
                            : null
                    });
            }

            if (code.Parts.Count == 0)
                return null;

            return code;
        }

        internal static EANCode _ParseHIBCC(string text)
        {
            var code = new EANCode(text);

            var hibcc = new HIBCC(text);

            var hibccMap = new Dictionary<IdentifierEnum, EANCodePartEnum>
            {
                { IdentifierEnum.ProductNumber, EANCodePartEnum.ShippingCode },
                { IdentifierEnum.Lot, EANCodePartEnum.BatchNumber },
                { IdentifierEnum.ExpirationDate, EANCodePartEnum.ExpirationDate },
                { IdentifierEnum.SerialNumber, EANCodePartEnum.SerialNumber },
            };

            foreach (var part in hibcc.Parts)
            {
                if (hibccMap.TryGetValue(part.EANCode, out var valueCode))
                    code.AddPart(new EANCodePart
                    {
                        EANCode = valueCode,
                        Value = part.Value,
                        ValueDate = part.ValueDate,
                    });
            }

            if (code.Parts.Count == 0)
                return null;

            return code;
        }
    }
}

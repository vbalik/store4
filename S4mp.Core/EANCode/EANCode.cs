﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace S4mp.Core.EANCode
{
    public class EANCode
    {
        public List<EANCodePart> Parts { get; private set; } = new List<EANCodePart>();

        public string RawData { get; private set; }

        public EANCodePart this[EANCodePartEnum eanCode] => Parts.FirstOrDefault(_ => _.EANCode == eanCode);

        public void AddPart(EANCodePart part)
        {
            Parts.Add(part);
        }

        public EANCode(string rawData)
        {
            RawData = rawData;
        }
    }
}

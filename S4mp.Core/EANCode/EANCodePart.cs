﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4mp.Core.EANCode
{
    public class EANCodePart
    {
        public EANCodePartEnum EANCode { get; set; }
        public string Value { get; set; }
        public DateTime? ValueDate { get; set; }
    }
}

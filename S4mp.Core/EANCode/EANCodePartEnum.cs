﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4mp.Core.EANCode
{
    public enum EANCodePartEnum
    {
        None = 0,
        BatchNumber = 1,
        ExpirationDate = 2,
        ShippingCode = 3,
        SerialNumber = 4,
    }
}

﻿using System.Threading.Tasks;
using System.Collections.Generic;
using S4mp.Entities;

namespace S4mp.Core.Logger
{
    public interface ICustomEventLogger
    {
        /// <summary>
        /// Log event with message
        /// </summary>
        /// <param name="message"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        Task LogWarning(string message, object parameters);

        /// <summary>
        /// Log event with message
        /// </summary>
        /// <param name="message"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        Task LogInfo(string message, object parameters);

        /// <summary>
        /// Log event with message
        /// </summary>
        /// <param name="message"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        Task LogError(string message, object parameters);

        /// <summary>
        /// Get backward logs by retention days
        /// </summary>
        /// <param name="retentionDays"></param>
        /// <returns></returns>
        Task<List<CustomEventLoggerLog>> GetLogs(int? retentionDays);

        /// <summary>
        /// Clear older logs
        /// </summary>
        /// <returns></returns>
        void ClearLogs();
    }
}

﻿using System;
using System.Collections.Generic;
using S4mp.Entities;

namespace S4mp.Core.Logger
{
    public class ReportErrorModel
    {
        public AppInfoModel AppInfo { get;set; }
        public List<CustomEventLoggerLog> Logs { get; set; }
    }

    public class AppInfoModel
    {
        public DateTime Created { get; set; }
        public string UserID { get; set; }
        public string Version { get; set; }
        public string OS_ver { get; set; }
        public string MONO_ver { get; set; }
        public string DeviceID { get; set; }
    }
}

﻿namespace S4mp.Core.Logger
{
    public enum CustomEventLoggerTypeEnum
    {
        Info,
        Warning,
        Error
    }
}

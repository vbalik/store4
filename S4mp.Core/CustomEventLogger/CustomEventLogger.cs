﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Collections.Generic;
using S4mp.Entities;

namespace S4mp.Core.Logger
{
    public class CustomEventLogger : ICustomEventLogger
    {
        public async Task LogInfo(string message, object parameters)
        {
            await _LogEvent(CustomEventLoggerTypeEnum.Info, message, parameters);
        }

        public async Task LogError(string message, object parameters)
        {
            await _LogEvent(CustomEventLoggerTypeEnum.Error, message, parameters);
        }

        public async Task LogWarning(string message, object parameters)
        {
            await _LogEvent(CustomEventLoggerTypeEnum.Warning, message, parameters);
        }

        public Task<List<CustomEventLoggerLog>> GetLogs(int? retentionDays)
        {
            return DependencyService.Get<DAL.ILocalDatabase>().Query<CustomEventLoggerLog>($"select * from {nameof(CustomEventLoggerLog)} where {nameof(CustomEventLoggerLog.Created)} >= @0", DateTime.Now.AddDays((retentionDays ?? 7) * -1)); //(double) (retentionDays ?? 7 * -1)
        }

        public async void ClearLogs()
        {
            await DependencyService.Get<DAL.ILocalDatabase>().Execute($"delete from {nameof(CustomEventLoggerLog)} where {nameof(CustomEventLoggerLog.Created)} < '@0'", DateTime.Now.AddDays(-7));
        }

        private async Task _LogEvent(CustomEventLoggerTypeEnum type, string message, object parameters)
        {
            await DependencyService.Get<DAL.ILocalDatabase>().SaveAsync(new CustomEventLoggerLog
            {
                Type = type.ToString(),
                Message = message,
                Parameters = parameters != null ? Newtonsoft.Json.JsonConvert.SerializeObject(parameters) : null,
                Created = DateTime.Now
            });
        }
    }
}

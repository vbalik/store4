﻿using System;
using System.Collections.Generic;
using System.Text;

using Xamarin.Forms;

namespace S4mp.Core
{
    public static class Colors
    {
        public enum NamedColorEnum
        {
            ColorPrimary,
            TooltipForeground
        }
        public static Color ColorPrimary { get => DependencyService.Get<IColorsResolver>().GetNamedColor(NamedColorEnum.ColorPrimary); }
        public static Color TooltipForeground { get => DependencyService.Get<IColorsResolver>().GetNamedColor(NamedColorEnum.TooltipForeground); }
    }
}

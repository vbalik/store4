﻿using S4.DAL;
using S4.Entities;
using S4.Entities.Helpers.Xml.Dispatch;
using System;
using System.IO;
using System.Linq;
using System.Text;
using TestHelpers;
using Xunit;

namespace S4.Export.Tests
{
    [Collection("ExportTest")]
    public class ExportTest
    {
        private const int DIRECTIONID = 34;
        private const int STOMOVELOTID1 = 45;
        private const int STOMOVELOTID2 = 46;

        protected DatabaseFixture fixture;

        public ExportTest(DatabaseFixture fixture)
        {
            this.fixture = fixture;
            Core.Data.ConnectionHelper.ConnectionString = fixture.ConnectionString;
        }


        [Fact]
        public void ExportsTest_Success()
        {
            //prepare data
            var direction = (new DirectionDAL()).GetDirectionAllData(DIRECTIONID);
            var exportRows = new ExportRow[2];
            var storeMoveItem1 = new StoreMoveItem() { StoMoveLotID = STOMOVELOTID1 };
            var storeMoveItem2 = new StoreMoveItem() { StoMoveLotID = STOMOVELOTID2 };
            exportRows[0] = new ExportRow();
            exportRows[0].moveItem = storeMoveItem1;
            exportRows[0].xmlData = new Entities.Helpers.Xml.Dispatch.DispatchItemS3();
            exportRows[1] = new ExportRow();
            exportRows[1].moveItem = storeMoveItem2;
            exportRows[1].xmlData = new Entities.Helpers.Xml.Dispatch.DispatchItemS3();

            //run
            Encoding encoding = Encoding.Default;

            //test MEDICOLSTest
            var expResults = S4.Export.ExportyInternal.MEDICOLS(direction, exportRows, "", "", out encoding);
            byte[] file = encoding.GetBytes(expResults[0].fileContent);

            //read test data
            var path = @"..\..\..\TestFiles\MEDICOLSTest";
            var testData = File.ReadAllBytes(path);
            //File.WriteAllBytes(@"c:\temp\MEDICOLSTest", file);
            Assert.Equal(file, testData);

            //test LEKIS5
            var config = new S4.Export.Settings.SettingsExport() { SettingExportLekarny = new Settings.SettingExportLekarny() { DodavatelICO = "123" } };
            var dispatch = new DispatchS3() { Customer = new DispatchCustomerS3() { Extradata = new DispatchCustomerExtradataS3() }, SoucetCenaBezDPH = "1", SoucetCenaSDPH = "1.21" };
            expResults = S4.Export.ExportyInternal.LEKIS5(config, dispatch, direction, exportRows, "", "", out encoding);
            file = encoding.GetBytes(expResults[0].fileContent);

            //read test data
            path = @"..\..\..\TestFiles\LEKIS5Test";
            testData = File.ReadAllBytes(path);
            //File.WriteAllBytes(@"c:\temp\LEKIS5Test", file);
            Assert.Equal(file, testData);

            //test LEKIS6
            expResults = S4.Export.ExportyInternal.LEKIS6(config, dispatch, direction, exportRows, "", "", out encoding);
            file = encoding.GetBytes(expResults[0].fileContent);
                      
            //read test data
            path = @"..\..\..\TestFiles\LEKIS6Test";
            testData = File.ReadAllBytes(path);
            //File.WriteAllBytes(@"c:\temp\LEKIS6Test", file);
            Assert.Equal(file, testData);

            //test PAENIUM
            expResults = S4.Export.ExportyInternal.PAENIUM(config, dispatch, direction, exportRows, "", "", out encoding);
            file = encoding.GetBytes(expResults[0].fileContent);
                      
            //read test data
            path = @"..\..\..\TestFiles\PAENIUM";
            testData = File.ReadAllBytes(path);

            Assert.Equal(file, testData);

            //test MEDIOX
            var table = new System.Data.DataTable();
            table.Columns.Add("CODE").DefaultValue = 1;
            expResults = S4.Export.ExportyInternal.MEDIOX(config, dispatch, direction, exportRows, "", "", out encoding, table);
            file = encoding.GetBytes(expResults[0].fileContent);
                     
            //read test data
            path = @"..\..\..\TestFiles\MEDIOX";
            testData = File.ReadAllBytes(path);

            Assert.Equal(file, testData);

            //test PDK5
            expResults = S4.Export.ExportyInternal.PDK5(config, dispatch, direction, exportRows, "", "", out encoding, table);
            file = encoding.GetBytes(expResults[0].fileContent);
                      
            //read test data
            path = @"..\..\..\TestFiles\PDK5";
            testData = File.ReadAllBytes(path);
            //File.WriteAllBytes(@"c:\temp\PDK5", file);
            Assert.Equal(file, testData);

            //test PDK4
            expResults = S4.Export.ExportyInternal.PDK4(config, dispatch, direction, exportRows, "", "", out encoding, table);
            file = encoding.GetBytes(expResults[0].fileContent);
                       
            //read test data
            path = @"..\..\..\TestFiles\PDK4";
            testData = File.ReadAllBytes(path);
            //File.WriteAllBytes(@"c:\temp\PDK4", file);
            Assert.Equal(file, testData);

            //test PDK8
            expResults = S4.Export.ExportyInternal.PDK8(config, dispatch, direction, exportRows, "", "", out encoding, table);
            file = encoding.GetBytes(expResults[0].fileContent);
                      
            //read test data
            path = @"..\..\..\TestFiles\PDK8";
            testData = File.ReadAllBytes(path);
            //File.WriteAllBytes(@"c:\temp\PDK8", file);
            Assert.Equal(file, testData);

            //test PDKX
            expResults = S4.Export.ExportyInternal.PDKX(config, dispatch, direction, exportRows, "", "", out encoding, table);
            file = encoding.GetBytes(expResults[0].fileContent);
            
            //read test data
            path = @"..\..\..\TestFiles\PDKX";
            testData = File.ReadAllBytes(path);
            //File.WriteAllBytes(@"c:\temp\PDKX", file);
            Assert.Equal(file, testData);

            //test PDK12
            expResults = S4.Export.ExportyInternal.PDK12(config, dispatch, direction, exportRows, "", "", out encoding, table);
            file = encoding.GetBytes(expResults[0].fileContent);
            
            //read test data
            path = @"..\..\..\TestFiles\PDK12";
            testData = File.ReadAllBytes(path);
            //File.WriteAllBytes(@"c:\temp\PDK12", file);
            Assert.Equal(file, testData);

            //test PDK15
            expResults = S4.Export.ExportyInternal.PDK15(config, dispatch, direction, exportRows, "", "", out encoding, table);
            file = encoding.GetBytes(expResults[0].fileContent);

            //read test data
            path = @"..\..\..\TestFiles\PDK15";
            testData = File.ReadAllBytes(path);
            //File.WriteAllBytes(@"c:\temp\PDK15", file);
            Assert.Equal(file, testData);

            //test PDK18
            expResults = S4.Export.ExportyInternal.PDK18(config, dispatch, direction, exportRows, "", "", out encoding, table);
            file = encoding.GetBytes(expResults[0].fileContent);

            //read test data
            path = @"..\..\..\TestFiles\PDK18";
            testData = File.ReadAllBytes(path);
            //File.WriteAllBytes(@"c:\temp\PDK18", file);
            Assert.Equal(file, testData);
                       

        }


    }
}

﻿using S4.Core.Utils;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace S4.Report
{
    public class BarCodeReport
    {
                
        private TextTemplate _templateReport = null;


        public BarCodeReport(string template)
        {
            _templateReport = new TextTemplate(template, true, null, true, CultureInfo.InvariantCulture);
        }

        public string Render(params object[] data)
        {
            return _templateReport.Render(TextTemplate.FieldSource.Property, data);
        }

        public string Render(Func<string, string> ReplaceUnPrintableCharacter, params object[] data)
        {
            return _templateReport.Render(TextTemplate.FieldSource.Property, ReplaceUnPrintableCharacter, data);
        }
    }
}

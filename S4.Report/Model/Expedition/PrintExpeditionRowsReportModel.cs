﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Report.Model.Expedition
{
    public class PrintExpeditionRowReportModel
    {
        public int RowNum { get; set; }
        public string SPZ { get; set; }
        public string DL { get; set; } = null;
    }
}

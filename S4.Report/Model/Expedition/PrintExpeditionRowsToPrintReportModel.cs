﻿using S4.Report.Model.Expedition;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Report.Model.Expedition
{
    public class PrintExpeditionRowsToPrintReportModel
    {
        public int PrinterLocationID { get; set; }
        public List<PrintExpeditionRowReportModel> Rows { get; set; }
        public string Header { get; set; }
        public string DateDeparture { get; set; }
        public string Truck { get; set; }
        public string DeparturePlace { get; set; }
        public string ArrivalPlace { get; set; }
        public string DtFrom { get; set; }
        public string DtTo { get; set; }

        public bool LocalPrint { get; set; }
        public byte[] PdfData { get; set; }
        public string ErrorText { get; set; }
        public string FileID { get; set; }
    }
}




﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Report.Model.Expedition
{
    public class PrintExpeditionReportModel
    {
        public string DocInfo { get; set; }
        public string SPZ { get; set; }
        public string PartnerCity { get; set; }
    }
}

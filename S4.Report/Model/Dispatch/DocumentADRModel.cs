﻿using combit.ListLabel24;
using combit.ListLabel24.DataProviders;
using S4.Entities.Models;
using S4.StoreCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Text;

namespace S4.Report.Model.Dispatch
{
    public class DocumentADRModel
    {
        
        public string UNKod { get; set; }
        public string Nazev { get; set; }
        public string NazevKarty { get; set; }
        public string Znacka { get; set; }
        public string ObalSkupina { get; set; }
        public string KodTunelu { get; set; }
        public string DruhObalu { get; set; }
        public decimal Mnozstvi { get; set; }
        public int? PrepravKategorie { get; set; }
        public int? PocetBodu { get; set; }
        public string OhrozujeZivProstredi { get; set; }
    }
}

    
﻿using combit.ListLabel24;
using combit.ListLabel24.DataProviders;
using S4.Entities.Models;
using S4.StoreCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Text;

namespace S4.Report.Model.Dispatch
{
    public class DispatchSNReportModel
    {
        [FieldType(LlFieldType.Drawing_hBitmap)]
        public Image Code2D { get; set; }
        public string ArticleDesc { get; set; }
        public string BarCode { get; set; }
        public string SerialNumber { get; set; }
        public string BatchNum { get; set; }
        public DateTime? ExpirationDate { get; set; }
    }
}

    
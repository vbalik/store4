﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace S4.Report.Printers
{
    // PrintDirect.cs
    // Shows how to write data directly to the printer using Win32 API's
    // Written 17th October 2002 By J O'Donnell - csharpconsulting@hotmail.com
    // Adapted from Microsoft Support article Q298141
    // This code assumes you have a printer at share \\192.168.1.101\hpl
    // This code sends Hewlett Packard PCL5 codes to the printer to print
    // out a rectangle in the middle of the page.   



    [StructLayout(LayoutKind.Sequential)]
    public struct DOCINFO
    {
        [MarshalAs(UnmanagedType.LPWStr)] public string pDocName;
        [MarshalAs(UnmanagedType.LPWStr)] public string pOutputFile;
        [MarshalAs(UnmanagedType.LPWStr)] public string pDataType;
    }

    public class PrintDirect
    {
        [DllImport("winspool.drv", CharSet = CharSet.Unicode, ExactSpelling = false, SetLastError = true, CallingConvention = CallingConvention.StdCall)]
        public static extern long OpenPrinter(string pPrinterName, ref IntPtr phPrinter, int pDefault);

        [DllImport("winspool.drv", CharSet = CharSet.Unicode, ExactSpelling = false,
              CallingConvention = CallingConvention.StdCall)]
        public static extern long StartDocPrinter(IntPtr hPrinter, int Level, ref DOCINFO pDocInfo);

        [DllImport("winspool.drv", CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern long StartPagePrinter(IntPtr hPrinter);

        [DllImport("winspool.drv", CharSet = CharSet.Ansi, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern long WritePrinter(IntPtr hPrinter, string data, int buf, ref int pcWritten);

        [DllImport("winspool.drv", CharSet = CharSet.Ansi, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern long WritePrinter(IntPtr hPrinter, byte[] data, int buf, ref int pcWritten);

        [DllImport("winspool.drv", CharSet = CharSet.Unicode, ExactSpelling = true,
              CallingConvention = CallingConvention.StdCall)]
        public static extern long EndPagePrinter(IntPtr hPrinter);

        [DllImport("winspool.drv"
              , CharSet = CharSet.Unicode, ExactSpelling = true,
              CallingConvention = CallingConvention.StdCall)]
        public static extern long EndDocPrinter(IntPtr hPrinter);

        [DllImport(
              "winspool.drv", CharSet = CharSet.Unicode, ExactSpelling = true,
              CallingConvention = CallingConvention.StdCall)]
        public static extern long ClosePrinter(IntPtr hPrinter);



        public static void PrintRAW(string printerPath, string jobName, string data)
        {

            System.IntPtr lhPrinter = new System.IntPtr();
            int pcWritten = 0;

            long isOpen = PrintDirect.OpenPrinter(printerPath, ref lhPrinter, 0);
            if (isOpen == 0)
                throw new Exception(String.Format("Can't open printer {0}; error: {1}", printerPath, Marshal.GetLastWin32Error()));

            DOCINFO di = new DOCINFO();
            di.pDocName = jobName;
            //di.pDataType;

            PrintDirect.StartDocPrinter(lhPrinter, 1, ref di);
            PrintDirect.StartPagePrinter(lhPrinter);

            PrintDirect.WritePrinter(lhPrinter, data, data.Length, ref pcWritten);

            PrintDirect.EndPagePrinter(lhPrinter);
            PrintDirect.EndDocPrinter(lhPrinter);
            PrintDirect.ClosePrinter(lhPrinter);
        }



        public static void PrintRAW(string printerPath, string jobName, byte[] data)
        {

            System.IntPtr lhPrinter = new System.IntPtr();
            int pcWritten = 0;

            long isOpen = PrintDirect.OpenPrinter(printerPath, ref lhPrinter, 0);
            if (isOpen == 0)
                throw new Exception(String.Format("Can't open printer {0}; error: {1}", printerPath, Marshal.GetLastWin32Error()));

            DOCINFO di = new DOCINFO();
            di.pDocName = jobName;
            //di.pDataType;

            PrintDirect.StartDocPrinter(lhPrinter, 1, ref di);
            PrintDirect.StartPagePrinter(lhPrinter);

            PrintDirect.WritePrinter(lhPrinter, data, data.Length, ref pcWritten);

            PrintDirect.EndPagePrinter(lhPrinter);
            PrintDirect.EndDocPrinter(lhPrinter);
            PrintDirect.ClosePrinter(lhPrinter);
        }
    }

}

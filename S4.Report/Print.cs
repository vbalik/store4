﻿#if INCLUDE_COMBIT 
using combit.ListLabel24;
using combit.ListLabel24.DataProviders;
using S4.Report.Helper;
#endif
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Kernel.Utils;
using System.Configuration.Provider;
using System.Linq;

namespace S4.Report
{
    public class Print
    {
        /* http://localhost:5000/test */

        public bool DesignMode { get; set; } = false;
        public string ReportDefinition;
        public string ReportDefFilePath { get; set; }
        public string ErrorText { get; private set; }
        public bool IsError { get; private set; } = false;
        public List<Tuple<string, object, Type>> Variables { get; set; } = null;
        public LlProject AutoProjectType { get; set; } = LlProject.List;

        #region Public

        public Print()
        {
#if !INCLUDE_COMBIT
            throw new Exception("COMBIT DLL není aktivní");
#endif
        }

        public byte[] DoPrint2PDF(string sql, string reportName, List<Tuple<string, object>> parameters, out int rowsCount)
        {
            var reportHelper = new ReportHelper();
            var dataSource = reportHelper.GetDataFromSQL(sql, parameters);

            //rows count
            rowsCount = 0;
            foreach (System.Data.DataTable table in dataSource.Tables)
                rowsCount += table.Rows.Count;

            return Print2File(reportName, LlExportTarget.Pdf, dataSource);

        }

        public byte[] DoPrint2PDF(params object[] dataSource)
        {
            return Print2File("", LlExportTarget.Pdf, dataSource);
        }
                
        public byte[] DoPrint2Text(object dataSource)
        {
            return Print2File("", LlExportTarget.Text, dataSource);
        }

        public void DoPrint(string printerName, string sql, List<Tuple<string, object>> parameters)
        {
#if INCLUDE_COMBIT
            var reportHelper = new ReportHelper();
            var dataSource = reportHelper.GetDataFromSQL(sql, parameters);
            DoPrintInternal(printerName, dataSource);
#endif
        }

        public void DoPrint(string printerName, params object[] dataSource)
        {
#if INCLUDE_COMBIT
            DataProviderCollection providerCollection = new DataProviderCollection();
            foreach (var item in dataSource)
            {
                ObjectDataProvider provider = new ObjectDataProvider(item);
                provider.FlattenStructure = true;
                providerCollection.Add(provider);
            }

            DoPrintInternal(printerName, providerCollection);
#endif
        }

        public byte[] DoMergePDF(List<byte[]> pdfFiles)
        {
            return DoMergePDFInternal(pdfFiles);
        }

        #endregion

        #region helpers
#if INCLUDE_COMBIT

        private byte[] Print2File(string reportName, LlExportTarget exportType, params object[] dataSources)
        {
#if INCLUDE_COMBIT
            if (string.IsNullOrWhiteSpace(ReportDefinition) && string.IsNullOrWhiteSpace(ReportDefFilePath))
                new Exception("Stream can't be created");

            try
            {
                // create export configuration
                using (Stream exportStream = new MemoryStream())
                {
                    DataProviderCollection providerCollection = new DataProviderCollection();

                    if (dataSources.Length > 1)
                    {
                        foreach (var item in dataSources)
                        {
                            ObjectDataProvider provider = new ObjectDataProvider(item);
                            provider.FlattenStructure = true;
                            providerCollection.Add(provider);
                        }
                    }

                    ExportConfiguration exportConfiguration = new ExportConfiguration(exportType, exportStream, GetStream());
                    exportConfiguration.ShowResult = false;
                    exportConfiguration.ExportOptions.Clear();

                    if (exportType == LlExportTarget.Pdf)
                    {
                        exportConfiguration.ExportOptions.Add(LlExportOption.PdfAuthor, "Promedica Praha Group a.s");
                        exportConfiguration.ExportOptions.Add(LlExportOption.PdfSubject, reportName);
                        exportConfiguration.ExportOptions.Add(LlExportOption.PdfFontMode, "7"); //fonty vykreslit vektorově
                    }

                    using (ListLabel LL = new ListLabel())
                    {
                        // add licence
                        SetLicence(LL);

                        // add variables
                        SetVariables(LL);
                        LL.AddVarsToFields = true;

                        LL.DataSource = dataSources.Length > 1 ? providerCollection : dataSources.First();
                        LL.AutoDestination = LlPrintMode.Export;
                        LL.AutoShowPrintOptions = false;
                        LL.AutoProjectType = AutoProjectType;

                        LL.Export(exportConfiguration);
                        LL.Dispose();
                    }

                    byte[] buffer = new byte[16 * 1024];
                    using (MemoryStream ms = new MemoryStream())
                    {
                        int read;
                        while ((read = exportStream.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            ms.Write(buffer, 0, read);
                        }
                        return ms.ToArray();
                    }
                }
            }
            catch (ListLabelException LlException)
            {
                // Catch Exceptions
                ErrorText = LlException.Message + " This information was generated by a List & Label custom exception.";
                IsError = true;
            }
#endif
            return null;
        }


        private void DoPrintInternal(string printerName, object dataSource)
        {
            if (string.IsNullOrWhiteSpace(ReportDefinition) && string.IsNullOrWhiteSpace(ReportDefFilePath))
                new Exception("Stream can't be created");

            using (ListLabel LL = new ListLabel())
            {
                //add licence
                SetLicence(LL);
                //add variables
                SetVariables(LL);

                LL.AddVarsToFields = true;

                using (Stream reportDefStream = GetStream())
                {
                    try
                    {
                        LL.DataSource = dataSource;
                        LL.AutoDestination = LlPrintMode.Normal;
                        LL.AutoShowPrintOptions = false;
                        LL.AutoProjectType = AutoProjectType;

                        if (DesignMode)
                            LL.Design(AutoProjectType, reportDefStream);
                        else
                        {
                            LL.AutoProjectStream = reportDefStream;
                            LL.Print(printerName, AutoProjectType);
                        }

                    }
                    catch (LL_User_Aborted_Exception)
                    {
                    }
                    catch (Exception ex)
                    {
                        ErrorText = ex.Message;
                        IsError = true;
                    }
                    finally
                    {
                        reportDefStream.Close();
                        LL.Dispose();
                    }
                }
            }
        }


        private void SetLicence(ListLabel LL)
        {
            LL.Core.LlSetOptionString(LlOptionString.LicensingInfo, ReportHelper.LICENCE_INFO); //Licence

        }


        private void SetVariables(ListLabel LL)
        {
            //add version
            LL.Variables.Add("Version", Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion);

            if (Variables == null)
                return;

            Type t = typeof(int);
            foreach (var variable in Variables)
            {
                if (variable.Item3 == typeof(string))
                    LL.Variables.Add(variable.Item1, variable.Item2 == null ? "" : variable.Item2.ToString());
                else if (variable.Item3 == typeof(bool))
                    LL.Variables.Add(variable.Item1, variable.Item2 == null ? false : (bool)variable.Item2);
                else if (variable.Item3 == typeof(int))
                    LL.Variables.Add(variable.Item1, variable.Item2 == null ? 0 : (int)variable.Item2);
                else if (variable.Item3 == typeof(DateTime))
                    LL.Variables.Add(variable.Item1, variable.Item2 == null ? new DateTime() : (DateTime)variable.Item2);
                else if (variable.Item3 == typeof(System.Drawing.Image))
                    LL.Variables.Add(variable.Item1, variable.Item2 == null ? null : (System.Drawing.Image)variable.Item2);
            }
        }
#endif

        private Stream GetStream()
        {
            if (!string.IsNullOrWhiteSpace(ReportDefFilePath))
                return File.Open(ReportDefFilePath, FileMode.Open, FileAccess.ReadWrite);
            else
                return GenerateStreamFromString(ReportDefinition);
        }

        private MemoryStream GenerateStreamFromString(string value)
        {
            byte[] bytes = new byte[value.Length * sizeof(char)];
            System.Buffer.BlockCopy(value.ToCharArray(), 0, bytes, 0, bytes.Length);

            return new MemoryStream(bytes, true);
        }


        private byte[] DoMergePDFInternal(List<byte[]> pdfFiles)
        {
            using (var msFinalPdf = new MemoryStream())
            {
                using (var pdf = new PdfDocument(new PdfWriter(msFinalPdf)))
                {
                    var merger = new PdfMerger(pdf);
                    foreach (var pdfFile in pdfFiles)
                    {
                        using (var msfilePdf = new MemoryStream(pdfFile))
                        using (var pdfReader = new PdfReader(msfilePdf, new ReaderProperties()))
                        using (var sourcePdf = new PdfDocument(pdfReader))
                        {
                            merger.Merge(sourcePdf, 1, sourcePdf.GetNumberOfPages());
                            sourcePdf.Close();
                        }
                    }

                    //close all
                    pdf.Close();
                    merger.Close();
                }
                return msFinalPdf.ToArray();
            }
        }

        #endregion

    }
}

﻿using S4.Entities.Models;
using S4.StoreCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.Report.Model.Dispatch
{
    public class ReceiveReportModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public decimal Quantity { get; set; }
        public string Unit { get; set; }
        public string Expiration { get; set; }

        public DateTime ExpirationDate { get; set; }
        public string BatchNum { get; set; }

        public int GroupNum { get; set; }
        public bool Header { get; set; } = true;
    }
}

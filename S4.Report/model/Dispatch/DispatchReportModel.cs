﻿using S4.Entities.Models;
using S4.StoreCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.Report.Model.Dispatch
{
    public class DispatchReportModel
    {
        public bool Head { get; set; } = false;
        public string Col1 { get; set; }
        public string Col2 { get; set; }
        public string Col3 { get; set; }
        public string Col4 { get; set; }
        public string Col5 { get; set; }
        public string Col6 { get; set; }
        public int GroupNum { get; set; }
    }
}

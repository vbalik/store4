﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Report.Helper
{
    public class FilterValuesReport
    {
        public string ID { get; set; }
        public string Name{ get; set; }
        public string Text { get; set; }
        public object Value { get; set; }
    }
}

﻿using NLog.Filters;
using S4.Entities.Settings;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using static S4.Entities.Settings.SQL4Filters;

namespace S4.Report.Helper
{
    public class ReportHelper
    {
        public const string LICENCE_INFO = "Q6uuG";

        public DataSet GetDataFromSQL(string sql, List<Tuple<string, object>> parameters)
        {
            SqlConnection connection = new SqlConnection(Core.Data.ConnectionHelper.ConnectionString);
            SqlCommand selectCMD = new SqlCommand(sql, connection);

            //add parameters
            foreach (var parameter in parameters)
                selectCMD.Parameters.AddWithValue(parameter.Item1, parameter.Item2);

            selectCMD.CommandTimeout = 60;
            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = selectCMD;
            connection.Open();
            DataSet dataSet = new DataSet();
            dataAdapter.Fill(dataSet);
            connection.Close();
            return dataSet;
        }

        public DataTable SQL2DataTable(string sql)
        {
            var table = new DataTable();
            using (var da = new SqlDataAdapter(sql, Core.Data.ConnectionHelper.ConnectionString))
            {
                da.Fill(table);
            }
            return table;
        }
               
        public (DataSet, string) GetDatasetFromSQL(List<SQL4Filters> sql4FiltersList, string sql, List<FilterValuesReport> valuesList)
        {
            //set value and text
            var errorMsg = "Použijte klávesu F5 pro znovunačtení stránky a poté zkuste vytvořit report znovu!";

            if (sql4FiltersList == null)
                throw new Exception($"{errorMsg}!");

            if (valuesList != null)
            {
                sql4FiltersList.ForEach(f =>
                {
                    var val = (from v in valuesList where v.ID.Equals(f.ID) && v.Name.Equals(f.Name) select v).FirstOrDefault();
                    if (val == null)
                        throw new Exception($"Chyba parametru {f.Name} - {f.ID}{System.Environment.NewLine}{errorMsg}");

                    f.Text = val.Text;
                    f.Value = val.Value;
                });
            }

            //check
            var valuesCount = valuesList == null ? 0 : valuesList.Count;
            var sql4FiltersCount = sql4FiltersList.Count;

            if (valuesCount != sql4FiltersCount)
                throw new Exception($"Nesouhlasí počet parametrů!{System.Environment.NewLine}{errorMsg}");

            var reportTitle = "";
            var parameters = new List<Tuple<string, object>>();

            foreach (var filter in sql4FiltersList)
            {
                string filter2SQL = "";

                //check data type
                var dataType = filter.DataType == DataTypeFilterEnum.Any && filter.Type == TypeFilterEnum.Date ? DataTypeFilterEnum.DateTime : filter.DataType;
                if (CheckDataType(filter.Value.ToString(), dataType))
                    filter2SQL = filter.Value.ToString();
                else
                    throw new Exception($"Chyba parametru {filter.Name}. Hodnota {filter.Value.ToString()} je neplatná.{System.Environment.NewLine}{errorMsg}");

                if (!string.IsNullOrWhiteSpace(filter.Value.ToString()))
                    reportTitle += $"{filter.Name.ToLower()}: '{filter.Text}' ";

                parameters.Add(new Tuple<string, object>($"@{filter.ID}", filter2SQL));

            }

            var ds = GetDataFromSQL(sql, parameters);
            return (ds, reportTitle);
        }

        private bool CheckDataType(string value, DataTypeFilterEnum dataTypeFilterEnum)
        {
            if (string.IsNullOrWhiteSpace(value))
                return true;

            switch (dataTypeFilterEnum)
            {
                case DataTypeFilterEnum.DateTime:
                    if (!DateTime.TryParse(value.ToString(), out var _))
                        return false;
                    else
                        return true;
                case DataTypeFilterEnum.Number:
                    if (!int.TryParse(value.ToString(), out var _))
                        return false;
                    else
                        return true;
                default:
                    return true;
            }
        }
    }
}

﻿using Newtonsoft.Json;
using S4.Entities.Helpers.Xml.Dispatch;
using S4.Entities.StatusEngine;
using S4.Entities.Helpers;
using System;
using System.IO;
using System.Text;
using Xunit;
using S4.Entities.Helpers.Xml.Invoice;

namespace S4.Entities.Tests.XmlTests
{
    public class InvoiceS3Test
    {
        [Fact]
        public void Xml_InvoiceS3Test_Success()
        {
            var xmlPath = @"..\..\..\XmlTestData\Invoice.xml";
            var xml = XmlHelper.GetStringFromXMLFile(xmlPath);

            InvoiceS3 di = null;
            var load = InvoiceS3.Serializer.Deserialize(xml, out _);

            Assert.True(load, $"{xmlPath} is not valid XML");

        }
    }
}

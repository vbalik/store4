﻿using Newtonsoft.Json;
using S4.Entities.Helpers.Xml.Dispatch;
using S4.Entities.StatusEngine;
using S4.Entities.Helpers;
using System;
using System.IO;
using System.Text;
using Xunit;
using S4.Entities.Helpers.Xml.Article;

namespace S4.Entities.Tests.XmlTests
{
    public class ArticleS3Test
    {
        [Fact]
        public void Xml_ArticleS3Test_Success()
        {
            //check ID201701021120416F30000101.XML
            var xmlPath = @"..\..\..\XmlTestData\ArticleTest.xml";
            var xml = XmlHelper.GetStringFromXMLFile(xmlPath);

            ArticleS3 ar = null;
            var load = ArticleS3.Serializer.Deserialize(xml, out ar);

            Assert.True(load, $"{xmlPath} is not valid XML");
            Assert.Equal("6F30000101", ar.ArticleID);
            Assert.Equal("52001M", ar.ArticleCode);
            Assert.Equal("RUKAVICE vyš. NITRIL SAFESKIN PURPLE bezprašné  S", ar.ArticleName);
            Assert.Equal("ks", ar.ArticleUnit);
            Assert.Equal("Halyard G", ar.ArticleManuf);
            Assert.Equal("EFFEKTLINE", ar.ArticleBrandname);
            Assert.False(ar.ArticleUseBatch);
            Assert.False(ar.ArticleUseExpiration);

            // Check Equal ArticlePacking
            Assert.Equal(5, ar.ArticlePacking.Count);
            var articlePacking = new ArticlePackingS3()
            {
                ArticlePackingID = "T0C0000101",
                PackingBarCode = "",
                PackingName = "pal",
                PackingRelation = "40000"
            };

            var xmlArticlePackingStr = JsonConvert.SerializeObject(ar.ArticlePacking[0]);
            var articlePackingStr = JsonConvert.SerializeObject(articlePacking);
            Assert.Equal(xmlArticlePackingStr, articlePackingStr);


        }
    }
}

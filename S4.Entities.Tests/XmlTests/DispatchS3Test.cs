﻿using Newtonsoft.Json;
using S4.Entities.Helpers.Xml.Dispatch;
using S4.Entities.StatusEngine;
using S4.Entities.Helpers;
using System;
using System.IO;
using System.Text;
using Xunit;

namespace S4.Entities.Tests.XmlTests
{
    public class DispatchS3Test
    {
        [Fact]
        public void Xml_DispatchS3Test_Success()
        {
            var xmlPath = @"..\..\..\XmlTestData\DispatchTest.xml";
            var xml = XmlHelper.GetStringFromXMLFile(xmlPath);

            DispatchS3 di = null;
            var load = DispatchS3.Serializer.Deserialize(xml, out di);

            Assert.True(load, $"{xmlPath} is not valid XML");
            Assert.Equal(new DateTime(2018,12,13), di.DocDate);
                       
            Assert.Equal("OP-621/2020", di.DocReference);
            Assert.Equal("FV-661/2020", di.InvoiceNumber);
            Assert.Equal("Doprava PPL", di.TransportationType);
            Assert.Equal("Prachy", di.PaymentType);
            Assert.Equal(123.1m, di.SoucetCenaBezDPHDec);
            Assert.Equal(456.2m, di.SoucetCenaSDPHDec);
            Assert.Equal("", di.DocRemark);
            Assert.Equal("true", di.MandatoryDirectionPrint);

            // Check Equal Customer
            var cus = new DispatchCustomerS3()
            {
                CustomerID = "EL10000101",
                Adress1 = "Gen. R. Tesaříka 80",
                Adress2 = "261 01",
                City = "Příbram",
                Phone = "318641521 Dr. Mejstříková",
                Name = "Oblastní nemocnice Příbram, a.s.",
                OrgIdentNumber = "1234",
                Extradata = new DispatchCustomerExtradataS3()
                {
                    FormatDiskety = "MEDIOX",
                    PoslatMailem = "True",
                    MailovaAdresa = "lekprip@onp.cz",
                    ICO = "27085031",
                    DIC = "CZ27085031",
                    PoslatDisketuSouhrnne = "True"
                },
                DeliveryAddresses = new System.Collections.Generic.List<DispatchDeliveryAddressS3>(),
            };

            var cusStr = JsonConvert.SerializeObject(cus);
            var xmlCusStr = JsonConvert.SerializeObject(di.Customer);
            Assert.Equal(cusStr, xmlCusStr);

            Assert.Equal("DL", di.DocPrefix);
            Assert.Equal(92900, di.DocNumber);

            // Check DeliveryAddress
            var ad = new DispatchDeliveryAddressS3()
            {
                Name = "Provozovna 1",
                Adress1 = "Gen. R. Tesaříka 80",
                Adress2 = "261 01",
                City = "Příbram",
                Phone = "318641521 Dr. Mejstříková",
                Deliveryinstructions = "Lékárna",
                Remark = "lekprip@onp.cz",
                AddressID = "S530000101",
                ReceivingPerson = "přijímací osoba X"
            };

            var xmlAddressStr = JsonConvert.SerializeObject(di.DeliveryAddress);
            var addressStr = JsonConvert.SerializeObject(ad);
            Assert.Equal(xmlAddressStr, addressStr);

            //Check Items
            Assert.Equal(24, di.Items.Count);

            var directSourceSection1 = new DirectSourceSection();
            directSourceSection1.Number = 1;
            directSourceSection1.Value = "VYSK";

            var directSourceSection2 = new DirectSourceSection();
            directSourceSection2.Number = 2;
            directSourceSection2.Value = "VYS2";

            DirectSourceSection[] directSourceSectionArray = { directSourceSection1, directSourceSection2 };

            var it = new DispatchItemS3()
            {
                ArticlePackingID = "I08F000101",
                Department = " AREÁL 1 / BUDOVA D / DĚTSKÉ JIP / PŘÍZEMÍ",
                Extradata = new DispatchItemExtradataS3()
                {
                    SkupinaZbozi = "4",
                    SazbaDPH = 21,
                    VyrobniCena = "0",
                    CenaBezDPH = "1632.28",
                    CenaSDPH = "1975.06",
                    ProdejniCenaBezDPH = "0.00",
                    ProdejniCenaSDPH = "1975.06",
                    ArticleMedicines = "Ne",
                    ArticleCooled = "Ano",
                    KodPDK = "",
                    KodVZP = "",
                    KodSUKL = null,
                    ArticleCode = null,
                    ProvideRowID = "ABCD",
                    RiskClass = "I",
                    Udidi = "4029577000018U"
                },
                Position = 0,
                Quantity = 1,
                DirectSourceSection = directSourceSectionArray,
                AlterSourceSection = "Test1"

            };
            var xmlItemStr = JsonConvert.SerializeObject(di.Items[0]);
            var itemStr = JsonConvert.SerializeObject(it);
            Assert.Equal(xmlItemStr, itemStr);
        }
    }
}

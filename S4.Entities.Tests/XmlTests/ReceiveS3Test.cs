﻿using Newtonsoft.Json;
using S4.Entities.Helpers;
using S4.Entities.Helpers.Xml.Receive;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace S4.Entities.Tests.XmlTests
{
    public class ReceiveS3Test
    {
        [Fact]
        public void Xml_ReceiveS3Test_Success()
        {
            //check PR423.xml
            var xmlPath = @"..\..\..\XmlTestData\Receive.xml";
            var xml = XmlHelper.GetStringFromXMLFile(xmlPath);

            ReceiveS3 re = null;
            var load = ReceiveS3.Serializer.Deserialize(xml, out re);

            Assert.True(load, $"{xmlPath} is not valid XML");
            Assert.Equal(new DateTime(2017, 1, 2), re.DocDate);

            Assert.Equal("", re.DocReference);
            Assert.Equal("", re.DocRemark);

            //Check Items
            Assert.Equal(43, re.Items.Count);

            var articlePacking1 = new ReceiveArticlePackingS3()
            {
                ArticlePackingID = "0MA0000101",
                PackingName = "ks",
                PackingRelation = "1",
                PackingBarCode = ""
            };

            var articlePacking2 = new ReceiveArticlePackingS3()
            {
                ArticlePackingID = "1MA0000101",
                PackingName = "min",
                PackingRelation = "1",
                PackingBarCode = ""
            };

            var articlePackingList = new List<ReceiveArticlePackingS3>();
            articlePackingList.Add(articlePacking1);
            articlePackingList.Add(articlePacking2);

            var ar = new ReceiveArticleS3()
            {
                ArticleID = "2130000101",
                ArticleCode = "010100",
                ArticleName = "DÁVKOVAČ nástěnný, pákový DERMADOS S pro lahve 0,5 L   ECO",
                ArticleUnit = "ks",
                ArticleManuf = "ECOLAB",
                ArticleUseBatch = false,
                ArticleUseExpiration = false,
                ArticlePacking = articlePackingList,
                ArticleCooled = true,
                ArticleCytostatics = true,
                ArticleMedicines = true,
                ArticleCheckOnReceive = true,
                ArticleBrandname = "EFFEKTLINE"
            };

            var ri = new ReceiveItemsS3();
            ri.Article = ar;
            ri.Quantity = 6;
            ri.Position = 0;
            ri.ArticlePackingID = "0MA0000101";

            var xmlItemStr = JsonConvert.SerializeObject(re.Items[0]);
            var itemStr = JsonConvert.SerializeObject(ri);

            Assert.Equal(xmlItemStr, itemStr);
        }
    }
}

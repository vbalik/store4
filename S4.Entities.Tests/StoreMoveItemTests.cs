﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace S4.Entities.Tests
{
    public class StoreMoveItemTests
    {
        const string BATCH = "BATCH123";
        private DateTime EXPIRATION = new DateTime(2011, 12, 15);


        [Fact]
        public void CarrierNum_New()
        {
            var item = new StoreMoveItem();

            Assert.Equal(StoreMoveItem.EMPTY_CARRIERNUM, item._carrierNum);
        }


        [Fact]
        public void CarrierNum_NotEmpty()
        {
            var item = new StoreMoveItem();
            item.CarrierNum = BATCH;

            Assert.Equal(BATCH, item._carrierNum);
        }


        [Fact]
        public void CarrierNum_Empty()
        {
            var item = new StoreMoveItem();
            item.CarrierNum = null;

            Assert.Equal(StoreMoveItem.EMPTY_CARRIERNUM, item._carrierNum);
        }

    }
}

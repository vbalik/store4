﻿using Newtonsoft.Json;
using S4.Entities.Helpers.Xml.Dispatch;
using S4.Entities.StatusEngine;
using S4.Entities.Helpers;
using System;
using System.IO;
using System.Text;
using Xunit;
using S4.Entities.Helpers.Xml.Article;
using S4.Entities.Settings;

namespace S4.Entities.Tests
{
    public class SettingsTests
    {
        [Fact]
        public void PrefixSettings()
        {
            var prefix = new Prefix();
            prefix.PrefixSettings.DispatchDistribution = Settings.PrefixSettings.DispatchDistributionEnum.AllFromOneSection;
            prefix.PrefixSettings.DispatchSections = new System.Collections.Generic.List<Settings.PrefixSettings.DispatchSection>() {
                new Settings.PrefixSettings.DispatchSection() { Order = 0, SectID = "TEST1" },
                new Settings.PrefixSettings.DispatchSection() { Order = 1, SectID = "TEST2" }
            };

            var json = System.IO.File.ReadAllText(@"..\..\..\JsonTestData\PrefixSettings.json");
            Assert.Equal(json, prefix._prefixSettingsJson);
        }


        [Fact]
        public void ManufactSettings()
        {
            var manufact = new Manufact();
            manufact.ManufactSettings.DispatchAlgoritm = Settings.ManufactSettings.DispatchAlgoritmEnum.BigOrSmallFirst;

            var json = System.IO.File.ReadAllText(@"..\..\..\JsonTestData\ManufactSettings.json");
            Assert.Equal(json, manufact._manufactSettingsJson);
        }

        [Fact]
        public void ReportSettings()
        {
            var sql4Filter = new SQL4Filters();
            sql4Filter.ID = "id_1";
            sql4Filter.Name = "name_1";
            sql4Filter.SQL = "select_2";
            sql4Filter.Width = "1";
            sql4Filter.Type = SQL4Filters.TypeFilterEnum.SQL;
            sql4Filter.Text = "Test1";
            sql4Filter.Value = "Value1";

            var report = new Reports();
            report.Settings.ReportName = "name_1";
            report.Settings.SQL = "select_1";
            report.Settings.TypeQuery = Settings.ReportSettings.TypeQueryEnum.None;
            report.Settings.TypeExport = Settings.ReportSettings.TypeExportEnum.Pdf;
            report.Settings.Flag = Worker.WorkerClassEnum.Any;
            report.Settings.SQL4FiltersList = new System.Collections.Generic.List<SQL4Filters>();
            report.Settings.SQL4FiltersList.Add(sql4Filter);

            var json = System.IO.File.ReadAllText(@"..\..\..\JsonTestData\ReportSettings.json");
            Assert.Equal(json, report._settingsJson);
        }
    }
}

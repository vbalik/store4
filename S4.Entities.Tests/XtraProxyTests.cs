﻿using S4.Entities.XtraData;
using System;
using Xunit;

namespace S4.Entities.Tests
{
    public class XtraProxyTests
    {
        public const string XTRA_EXPED_TRUCK = "EXPT";
        public const string XTRA_DISPATCH_POSITION = "DIPO";
        public const string XTRA_DISPATCH_DIRECT_SECTION = "DIDS";

        public static XtraDataDict XtraDataDict = new XtraDataDict(x =>
        {
            x.AddField(XTRA_EXPED_TRUCK, "Vozidlo", typeof(string));
            x.AddField(XTRA_DISPATCH_DIRECT_SECTION, "yyyyy", typeof(int), true);
            x.AddField(XTRA_DISPATCH_POSITION, "xxxx", typeof(DateTime));

        });


        [Fact]
        public void XtraProxyTest_Set()
        {
            var xtraData = new XtraDataProxy<DirectionXtra>(XtraDataDict);
            xtraData[XTRA_EXPED_TRUCK] = "123";
            Assert.Single(xtraData.Data);
            xtraData[XTRA_EXPED_TRUCK] = null;
            Assert.Empty(xtraData.Data);

            // bad code
            Assert.Throws<Exception>(() => xtraData["?"] = "");
            // bad position
            Assert.Throws<Exception>(() => xtraData[XTRA_EXPED_TRUCK, 1] = "");
            // bad type
            Assert.Throws<Exception>(() => xtraData[XTRA_EXPED_TRUCK] = 333);


            xtraData = new XtraDataProxy<DirectionXtra>(XtraDataDict);
            xtraData[XTRA_DISPATCH_DIRECT_SECTION, 1] = 123;
            Assert.Single(xtraData.Data);
            xtraData[XTRA_DISPATCH_DIRECT_SECTION, 1] = null;
            Assert.Empty(xtraData.Data);

            // bad code
            Assert.Throws<Exception>(() => xtraData["?", 1] = "");
            // bad type
            Assert.Throws<Exception>(() => xtraData[XTRA_DISPATCH_DIRECT_SECTION, 1] = "333");
        }


        [Fact]
        public void XtraProxyTest_Get()
        {
            var xtraData = new XtraDataProxy<DirectionXtra>(XtraDataDict);
            var dt = DateTime.Today;

            xtraData[XTRA_EXPED_TRUCK] = "123";
            xtraData[XTRA_DISPATCH_DIRECT_SECTION, 1] = 123;
            xtraData[XTRA_DISPATCH_POSITION] = dt;

            Assert.Equal("123", xtraData[XTRA_EXPED_TRUCK]);
            Assert.Equal(123, xtraData[XTRA_DISPATCH_DIRECT_SECTION, 1]);
            Assert.Equal(dt, xtraData[XTRA_DISPATCH_POSITION]);

            // bad code
            Assert.Throws<Exception>(() =>
            {
                var x = xtraData["?"];
            });
            // bad position
            Assert.Throws<Exception>(() =>
            {
                var x = xtraData[XTRA_EXPED_TRUCK, 1];
            });
        }


        [Fact]
        public void XtraProxyTest_KeepID()
        {
            var xtraData = new XtraDataProxy<DirectionXtra>(XtraDataDict);
            xtraData[XTRA_EXPED_TRUCK] = "123";
            xtraData.Data[0].DirectionID = 555;
            xtraData[XTRA_EXPED_TRUCK] = "456";
            Assert.Equal(555, xtraData.Data[0].DirectionID);
        }


        [Fact]
        public void XtraProxyTest_IsNull()
        {
            var xtraData = new XtraDataProxy<DirectionXtra>(XtraDataDict);

            xtraData[XTRA_DISPATCH_DIRECT_SECTION, 1] = 123;
            Assert.False(xtraData.IsNull(XTRA_DISPATCH_DIRECT_SECTION, 1));
            xtraData[XTRA_DISPATCH_DIRECT_SECTION, 1] = null;
            Assert.True(xtraData.IsNull(XTRA_DISPATCH_DIRECT_SECTION, 1));

            xtraData[XTRA_EXPED_TRUCK] = "123";
            Assert.False(xtraData.IsNull(XTRA_EXPED_TRUCK));
            xtraData[XTRA_EXPED_TRUCK] = null;
            Assert.True(xtraData.IsNull(XTRA_EXPED_TRUCK));
        }
    }
}

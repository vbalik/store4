﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace S4.Entities.Tests
{
    public class StoreMoveLotTests
    {
        const string BATCH = "BATCH123";
        private DateTime EXPIRATION = new DateTime(2011, 12, 15);
        const string UDICODE = "1234";


        [Fact]
        public void BatchNum_New()
        {
            var lot = new StoreMoveLot();

            Assert.Equal(StoreMoveItem.EMPTY_BATCHNUM, lot._batchNum);
        }


        [Fact]
        public void BatchNum_NotEmpty()
        {
            var lot = new StoreMoveLot();
            lot.BatchNum = BATCH;

            Assert.Equal(BATCH, lot._batchNum);
        }


        [Fact]
        public void BatchNum_Empty()
        {
            var lot = new StoreMoveLot();
            lot.BatchNum = null;

            Assert.Equal(StoreMoveItem.EMPTY_BATCHNUM, lot._batchNum);
        }


        [Fact]
        public void ExpirationDate_New()
        {
            var lot = new StoreMoveLot();

            Assert.Equal(StoreMoveItem.EMPTY_EXPIRATION, lot._expirationDate);
        }


        [Fact]
        public void ExpirationDate_NotEmpty()
        {
            var lot = new StoreMoveLot();
            lot.ExpirationDate = EXPIRATION;

            Assert.Equal(EXPIRATION, lot._expirationDate);
        }


        [Fact]
        public void ExpirationDate_Empty()
        {
            var lot = new StoreMoveLot();
            lot.ExpirationDate = null;

            Assert.Equal(StoreMoveItem.EMPTY_EXPIRATION, lot._expirationDate);
        }
    }
}

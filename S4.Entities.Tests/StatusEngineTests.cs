﻿using S4.Entities.StatusEngine;
using System;
using Xunit;

namespace S4.Entities.Tests
{
    public class StatusEngineTests
    {
        public const string DR_STATUS_CANCEL = "CANC";
        public const string DR_STATUS_RECEIVE_COMPLET = "CREC";
        public const string DR_STATUS_STOIN_WAIT = "WSTI";
        public const string DR_STATUS_STOIN_PROC = "PSTI";
        public const string DR_STATUS_STOIN_COMPLET = "CSTI";
        public const string DR_STATUS_LOAD = "LOAD";
        public const string DR_STATUS_DISPATCH_WAIT = "WDIS";
        public const string DR_STATUS_DISPATCH_PROC = "PDIS";
        public const string DR_STATUS_DISPATCH_SAVED = "SDIS";
        public const string DR_EVENT_FIELD_CHANGE = "FLCH";
        public const string DR_EVENT_DISP_SECT_CHANGE = "DSCH";
        public const string DR_EVENT_RESERVATION_DONE = "RESR";
        public const string DR_EVENT_DISP_TO_STOIN = "DTSI";


        private StatusSet Statuses = new StatusSet(s =>
        {
            // store in
            s.AddStatus(DR_STATUS_RECEIVE_COMPLET, "Příjem - hotovo")
                .SetEnabledStatuses(DR_STATUS_STOIN_WAIT, DR_STATUS_CANCEL)
                .AddAction("Naskladnění", ActionTypeEnum.Dialog, "StoIn_Wait")
                .AddAction("Změnit stav", ActionTypeEnum.Dialog, "Direction_SetStatus").AuthorizationSuperUser()
                .AddAction("Zrušit doklad", ActionTypeEnum.ProcedureWithConfirmation, "direction/cancel");

            s.AddStatus(DR_STATUS_STOIN_WAIT, "Naskladnění - přiřazeno")
                .SetColor(System.Drawing.Color.Green)
                .SetEnabledStatuses()
                .AddAction("Jiný pracovník", ActionTypeEnum.Dialog, "StoIn_Wait")
                .AddAction("Změnit stav", ActionTypeEnum.Dialog, "Direction_SetStatus").AuthorizationSuperUser()
                .AddAction("Zrušit doklad", ActionTypeEnum.ProcedureWithConfirmation, "direction/cancel");

            s.AddStatus(DR_STATUS_STOIN_PROC, "Naskladnění - v běhu")
                .SetColor(System.Drawing.Color.Blue)
                .SetEnabledStatuses()
                .AddAction("Změnit stav", ActionTypeEnum.Dialog, "Direction_SetStatus").AuthorizationSuperUser()
                .AddAction("Zrušit doklad", ActionTypeEnum.ProcedureWithConfirmation, "direction/cancel");

            s.AddStatus(DR_STATUS_STOIN_COMPLET, "Naskladnění - hotovo")
                .SetColor(System.Drawing.Color.FromArgb(136, 136, 136))
                .SetEnabledStatuses()
                .AddAction("Tisk", "receive/print")
                .AddAction("Změnit stav", ActionTypeEnum.Dialog, "Direction_SetStatus").AuthorizationSuperUser()
                .AddAction("Zrušit doklad", ActionTypeEnum.ProcedureWithConfirmation, "direction/cancel");


            // dispatch 
            s.AddStatus(DR_STATUS_LOAD, "Nahráno")
                .SetEnabledStatuses(DR_STATUS_DISPATCH_WAIT, DR_STATUS_CANCEL)
                .AddAction("Vychystání", ActionTypeEnum.Dialog, "Dispatch_Wait")
                .AddAction("Zrušit doklad", ActionTypeEnum.ProcedureWithConfirmation, "direction/cancel");

            s.AddStatus(DR_STATUS_DISPATCH_WAIT, "Vychystání - přiřazeno")
                .SetColor(System.Drawing.Color.Green)
                .SetEnabledStatuses()
                .AddAction("Jiný pracovník", ActionTypeEnum.Dialog, "Dispatch_Wait")
                .AddAction("Test rezervace", ActionTypeEnum.Dialog, "Dispatch_ProcTry")
                .AddAction("Tisk", "dispatch/print")
                .AddAction("Změnit stav", ActionTypeEnum.Dialog, "Direction_SetStatus").AuthorizationSuperUser()
                .AddAction("Zrušit doklad", ActionTypeEnum.ProcedureWithConfirmation, "direction/cancel");

            s.AddStatus(DR_STATUS_DISPATCH_PROC, "Vychystání - v běhu")
                .SetColor(System.Drawing.Color.Blue)
                .SetEnabledStatuses()
                .AddAction("Tisk", "dispatch/print")
                .AddAction("Změnit stav", ActionTypeEnum.Dialog, "Direction_SetStatus").AuthorizationSuperUser()
                .AddAction("Zrušit doklad", ActionTypeEnum.ProcedureWithConfirmation, "direction/cancel");

            s.AddStatus(DR_STATUS_DISPATCH_SAVED, "Vychystání - hotovo")
                .SetEnabledStatuses()
                .AddAction("Tisk", "dispatch/print")
                .AddAction("Kontrola", ActionTypeEnum.Dialog, "DispCheck_Wait")
                .AddAction("Změnit stav", ActionTypeEnum.Dialog, "Direction_SetStatus").AuthorizationSuperUser()
                .AddAction("Export", ActionTypeEnum.Dialog, "Direction_Export")
                .AddAction("Zrušit doklad", ActionTypeEnum.ProcedureWithConfirmation, "direction/cancel");

            // cancel
            s.AddStatus(DR_STATUS_CANCEL, "Zrušeno")
                .SetColor(System.Drawing.Color.Gray)
                .AddAction("Změnit stav", ActionTypeEnum.Dialog, "Direction_SetStatus").AuthorizationSuperUser()
                .AddAction("Smazat doklad", ActionTypeEnum.ProcedureWithConfirmation, "direction/delete");

            // events
            s.AddEvent(DR_EVENT_FIELD_CHANGE, "Změna pole");
            s.AddEvent(DR_EVENT_DISP_SECT_CHANGE, "Změna sekce vyskladnění");
            s.AddEvent(DR_EVENT_RESERVATION_DONE, "Rezervace vytvořena");
            s.AddEvent(DR_EVENT_DISP_TO_STOIN, "Logistika - převod na příjemku");

        });


        [Fact]
        public void TestInit()
        {
            Assert.Equal(9, Statuses.Items.Count);
            Assert.Equal(13, Statuses.Events.Count);
        }


        [Fact]
        public void Test_NextStatusEnabled()
        {
            Assert.True(Statuses.NextStatusEnabled(DR_STATUS_RECEIVE_COMPLET, DR_STATUS_STOIN_WAIT));
            Assert.False(Statuses.NextStatusEnabled(DR_STATUS_RECEIVE_COMPLET, DR_STATUS_STOIN_COMPLET));
            Assert.Throws<Exception>(() => Statuses.NextStatusEnabled("xxx", DR_STATUS_STOIN_COMPLET));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using S4.Picking.Method;
using S4.StoreCore.Interfaces;
using S4.Entities;

namespace S4.Picking.Tests
{
    public class ExtensionsTests : PlanMakerTestsBase
    {
        [Fact]
        public void Extensions_GetNext()
        {
            // arange
            List<OnStoreItemFull> onStore = GetOnStore();

            // act
            var res1 = onStore.GetNext(5);

            // assert
            Assert.Equal(5, res1.moveQuantity);
            Assert.Equal(POS2, res1.foundItem.PositionID);
            Assert.Equal(5, res1.foundItem.Quantity);

            var res2 = onStore.GetNext(7);

            Assert.Equal(5, res2.moveQuantity);
            Assert.Equal(POS2, res2.foundItem.PositionID);
            Assert.Equal(0, res2.foundItem.Quantity);
        }


        [Fact]
        public void Extensions_GetNextWithQuantity()
        {
            // arange
            List<OnStoreItemFull> onStore = new List<OnStoreItemFull>()
            {
                    new OnStoreItemFull() { ArtPackID = ARTICLE1_PACK1_ID, PositionID = POS1, CarrierNum = CARRIER1, BatchNum = BATCH1, Quantity = 5, ExpirationDate = EXP2, Position = new Position() { PosCode = nameof(POS1) } },
                    new OnStoreItemFull() { ArtPackID = ARTICLE1_PACK1_ID, PositionID = POS2, CarrierNum = CARRIER1, BatchNum = BATCH2, Quantity = 10, ExpirationDate = EXP1, Position = new Position() { PosCode = nameof(POS2) } },
                    new OnStoreItemFull() { ArtPackID = ARTICLE1_PACK1_ID, PositionID = POS3, CarrierNum = CARRIER1, BatchNum = BATCH2, Quantity = 2, ExpirationDate = EXP1, Position = new Position() { PosCode = nameof(POS3) } },
            };

            // act
            var res1 = onStore.GetNextWithQuantity(5);

            // assert
            Assert.Equal(2, res1.moveQuantity);
            Assert.Equal(POS3, res1.foundItem.PositionID);
            Assert.Equal(0, res1.foundItem.Quantity);

            var res2 = onStore.GetNext(5 - res1.moveQuantity);

            Assert.Equal(3, res2.moveQuantity);
            Assert.Equal(POS2, res2.foundItem.PositionID);
            Assert.Equal(7, res2.foundItem.Quantity);
        }


        [Fact]
        public void Extensions_CheckOneBatch()
        {
            // arange
            List<OnStoreItemFull> onStore = GetOnStore();

            // act
            var res1 = onStore.CheckOneBatch(7);

            // assert
            Assert.Equal(10, res1.foundQuantity);
            Assert.Equal(BATCH2, res1.foundBatchNum);            
            Assert.Equal(10, onStore[1].Quantity);
        }


        [Fact]
        public void Extensions_CheckOneBatch_2()
        {
            // arange
            List<OnStoreItemFull> onStore = new List<OnStoreItemFull>()
            {
                    new OnStoreItemFull() { BatchNum = "4183", Quantity = 27, ExpirationDate = new DateTime(2020, 02, 01) },
                    new OnStoreItemFull() { BatchNum = "4022", Quantity = 1, ExpirationDate = new DateTime(2019, 11, 01) },
                    new OnStoreItemFull() { BatchNum = "4081", Quantity = 41, ExpirationDate = new DateTime(2019, 12, 01) },
            };

            // act
            var (foundQuantity, foundBatchNum) = onStore.CheckOneBatch(3);

            // assert
            Assert.Equal(41, foundQuantity);
            Assert.Equal("4081", foundBatchNum);
        }


        [Fact]
        public void Extensions_GetByBatch()
        {
            // arange
            List<OnStoreItemFull> onStore = GetOnStore();

            // act
            var res1 = onStore.GetByBatch(7, BATCH2);
            
            // assert
            Assert.Equal(7, res1.moveQuantity);
            Assert.Equal(POS2, res1.foundItem.PositionID);
            Assert.Equal(BATCH2, res1.foundItem.BatchNum);
            Assert.Equal(3, res1.foundItem.Quantity);


            var res2 = onStore.GetByBatch(3, BATCH2);
            Assert.Equal(3, res2.moveQuantity);
            Assert.Equal(POS2, res1.foundItem.PositionID);
            Assert.Equal(BATCH2, res1.foundItem.BatchNum);
            Assert.Equal(0, res1.foundItem.Quantity);
        }


        [Fact]
        public void Extensions_GetEqualItem()
        {
            // arange
            List<OnStoreItemFull> onStore = GetOnStore();

            // act
            var res1 = onStore.GetEqualItem(10);
            var res2 = onStore.GetEqualItem(10);

            // assert
            Assert.Equal(10, res1.moveQuantity);
            Assert.Equal(POS2, res1.foundItem.PositionID);
            Assert.Equal(0, res1.foundItem.Quantity);

            Assert.Equal(0, res2.moveQuantity);
        }


        [Fact]
        public void Extensions_GetNearestItem()
        {
            // arange
            List<OnStoreItemFull> onStore = GetOnStore();

            // act
            var res1 = onStore.GetNearestItem(7);
            var res2 = onStore.GetNearestItem(7);

            // assert
            Assert.Equal(5, res1.moveQuantity);
            Assert.Equal(POS1, res1.foundItem.PositionID);
            Assert.Equal(0, res1.foundItem.Quantity);

            Assert.Equal(7, res2.moveQuantity);
            Assert.Equal(POS2, res2.foundItem.PositionID);
            Assert.Equal(3, res2.foundItem.Quantity);
        }


        [Fact]
        public void Extensions_GetSmallerItem()
        {
            // arange
            List<OnStoreItemFull> onStore = GetOnStore();

            // act
            var res1 = onStore.GetSmallerItem(3, 7);

            // assert
            Assert.Equal(3, res1.moveQuantity);
            Assert.Equal(POS1, res1.foundItem.PositionID);
            Assert.Equal(2, res1.foundItem.Quantity);

            var res2 = onStore.GetSmallerItem(3, 7);
            Assert.Equal(2, res2.moveQuantity);
            Assert.Equal(POS1, res2.foundItem.PositionID);
            Assert.Equal(0, res2.foundItem.Quantity);
        }


        [Fact]
        public void Extensions_GetBiggerItem()
        {
            // arange
            List<OnStoreItemFull> onStore = GetOnStore();

            // act
            var res1 = onStore.GetBiggerItem(5);

            // assert
            Assert.Equal(5, res1.moveQuantity);
            Assert.Equal(POS2, res1.foundItem.PositionID);
            Assert.Equal(5, res1.foundItem.Quantity);
        }


        [Fact]
        public void Extensions_GetNearestAliquotItem()
        {
            // arange
            List<OnStoreItemFull> onStore = new List<OnStoreItemFull>()
            {
                    new OnStoreItemFull() { ArtPackID = ARTICLE1_PACK1_ID, PositionID = POS1, CarrierNum = CARRIER1, BatchNum = BATCH1, Quantity = 3, ExpirationDate = EXP2, Position = new Position() { PosCode = nameof(POS1) } },
                    new OnStoreItemFull() { ArtPackID = ARTICLE1_PACK1_ID, PositionID = POS2, CarrierNum = CARRIER1, BatchNum = BATCH2, Quantity = 10, ExpirationDate = EXP1, Position = new Position() { PosCode = nameof(POS2) } },
                    new OnStoreItemFull() { ArtPackID = ARTICLE1_PACK1_ID, PositionID = POS3, CarrierNum = CARRIER1, BatchNum = BATCH2, Quantity = 20, ExpirationDate = EXP1, Position = new Position() { PosCode = nameof(POS3) } },
            };

            // act
            var res1 = onStore.GetNearestAliquotItem(11, 10);

            // assert
            Assert.Equal(10, res1.moveQuantity);
            Assert.Equal(POS2, res1.foundItem.PositionID);
            Assert.Equal(0, res1.foundItem.Quantity);
        }
        

        private static List<OnStoreItemFull> GetOnStore()
        {
            return new List<OnStoreItemFull>()
            {
                    new OnStoreItemFull() { ArtPackID = ARTICLE1_PACK1_ID, PositionID = POS1, CarrierNum = CARRIER1, BatchNum = BATCH1, Quantity = 5, ExpirationDate = EXP2, Position = new Position() { PosCode = nameof(POS1) } },
                    new OnStoreItemFull() { ArtPackID = ARTICLE1_PACK1_ID, PositionID = POS2, CarrierNum = CARRIER1, BatchNum = BATCH2, Quantity = 10, ExpirationDate = EXP1, Position = new Position() { PosCode = nameof(POS2) } },
            };
        }
    }
}

﻿using Moq;
using S4.Core.Cache;
using S4.DAL.Interfaces;
using S4.Entities;
using S4.Entities.Models;
using S4.Entities.Settings;
using S4.StoreCore.Interfaces;
using System;
using System.Collections.Generic;
using Xunit;


namespace S4.Picking.Tests
{
    public class PlanMakerTests_BigOrSmallFirst : PlanMakerTestsBase
    {
        [Fact]
        public void PlanMakerTest_BigPart_10()
        {
            // arrange
            Mock<IOnStore> onStore = PrepareOnStore();
            Mock<ICache<ArticlePacking, int>> packingCache = PreparePackingCache();
            Mock<ICache<SectionModel, string>> sectionModelCache = PrepareSectionModelCache();
            var articleCache = new Mock<ICache<ArticleModel, int>>();
            articleCache.Setup(m => m.GetItem(ARTICLE1)).Returns(new ArticleModel()
            {
                ArticleID = ARTICLE1,
                ArticleCode = nameof(ARTICLE1),
                ManufID = MANUF_1,
                Packings = new List<ArticlePacking>()
                {
                    new ArticlePacking() { ArtPackID = ARTICLE1_PACK1_ID, ArticleID = ARTICLE1 },
                    new ArticlePacking() { ArtPackID = ARTICLE1_PACK2_ID, ArticleID = ARTICLE1, PackDesc = "pal", PackRelation = 10 }
                }
            });
            Mock<ICache<Manufact, string>> manufactCache = PrepareManufactCache(ManufactSettings.DispatchAlgoritmEnum.BigOrSmallFirst);
            Mock<IBookings> bookings = EmptyBookings();


            // act
            var planMaker = new PlanMaker(packingCache.Object, articleCache.Object, manufactCache.Object, sectionModelCache.Object, onStore.Object, bookings.Object);
            var dirModel = new DirectionModel()
            {
                Items = new List<DirectionItem>()
                {
                    new DirectionItem() { ArtPackID = ARTICLE1_PACK1_ID, ArticleCode = nameof(ARTICLE1), Quantity = 10, DocPosition = 11 }
                }
            };
            var prefixSetings = GetPrefixSettings_AllFromOneSection();

            var result = planMaker.CreatePlan(dirModel, prefixSetings, DateTime.Today);

            // assert
            Assert.True(result.Result);
            Assert.Single(result.ResultItems);
            CheckResultItem(result.ResultItems[0], POS2, ARTICLE1_PACK1_ID, 10, EXP1, CARRIER1, BATCH2, 11);
            CheckHistoryResultJson(result.History, nameof(PlanMakerTest_BigPart_10));
        }


        [Fact]
        public void PlanMakerTest_BigPart_12()
        {
            // arrange
            Mock<IOnStore> onStore = PrepareOnStore();
            Mock<ICache<ArticlePacking, int>> packingCache = PreparePackingCache();
            var articleCache = new Mock<ICache<ArticleModel, int>>();
            articleCache.Setup(m => m.GetItem(ARTICLE1)).Returns(new ArticleModel()
            {
                ArticleID = ARTICLE1,
                ArticleCode = nameof(ARTICLE1),
                ManufID = MANUF_1,
                Packings = new List<ArticlePacking>()
                {
                    new ArticlePacking() { ArtPackID = ARTICLE1_PACK1_ID, ArticleID = ARTICLE1 },
                    new ArticlePacking() { ArtPackID = ARTICLE1_PACK2_ID, ArticleID = ARTICLE1, PackDesc = "pal", PackRelation = 10 }
                }
            });
            Mock<ICache<Manufact, string>> manufactCache = PrepareManufactCache(ManufactSettings.DispatchAlgoritmEnum.BigOrSmallFirst);
            Mock<IBookings> bookings = EmptyBookings();


            // act
            var planMaker = new PlanMaker(packingCache.Object, articleCache.Object, manufactCache.Object, PrepareSectionModelCache().Object, onStore.Object, bookings.Object);
            var dirModel = new DirectionModel()
            {
                Items = new List<DirectionItem>()
                {
                    new DirectionItem() { ArtPackID = ARTICLE1_PACK1_ID, ArticleCode = nameof(ARTICLE1), Quantity = 12, DocPosition = 99 }
                }
            };
            var prefixSetings = GetPrefixSettings_AllFromOneSection();

            var result = planMaker.CreatePlan(dirModel, prefixSetings, DateTime.Today);

            // assert
            Assert.True(result.Result);
            Assert.Equal(2, result.ResultItems.Count);
            CheckResultItem(result.ResultItems[0], POS2, ARTICLE1_PACK1_ID, 10, EXP1, CARRIER1, BATCH2, 99);
            CheckResultItem(result.ResultItems[1], POS1, ARTICLE1_PACK1_ID, 2, EXP2, CARRIER1, BATCH1, 99);
            CheckHistoryResultJson(result.History, nameof(PlanMakerTest_BigPart_12));
        }



        [Fact]
        public void PlanMakerTest_SmallPart_3()
        {
            // arrange
            var onStore = new Mock<IOnStore>();
            onStore.Setup(m => m.ByArticle(ARTICLE1, ResultValidityEnum.NotValid, true, null)).Returns(new List<OnStoreItemFull>() {
                new OnStoreItemFull() { ArtPackID = ARTICLE1_PACK1_ID, ArticleID = ARTICLE1, PositionID = POS1, CarrierNum = CARRIER1, BatchNum = BATCH1, Quantity = 3, ExpirationDate = EXP2, Position = new Position() { PosCode = nameof(POS1), PosStatus = Entities.Position.POS_STATUS_OK } },
                new OnStoreItemFull() { ArtPackID = ARTICLE1_PACK1_ID, ArticleID = ARTICLE1, PositionID = POS2, CarrierNum = CARRIER1, BatchNum = BATCH2, Quantity = 10, ExpirationDate = EXP1, Position = new Position() { PosCode = nameof(POS2), PosStatus = Entities.Position.POS_STATUS_OK } },
            });
            Mock<ICache<ArticlePacking, int>> packingCache = PreparePackingCache();
            var articleCache = new Mock<ICache<ArticleModel, int>>();
            articleCache.Setup(m => m.GetItem(ARTICLE1)).Returns(new ArticleModel()
            {
                ArticleID = ARTICLE1,
                ArticleCode = nameof(ARTICLE1),
                ManufID = MANUF_1,
                Packings = new List<ArticlePacking>()
                {
                    new ArticlePacking() { ArtPackID = ARTICLE1_PACK1_ID, ArticleID = ARTICLE1 },
                    new ArticlePacking() { ArtPackID = ARTICLE1_PACK2_ID, ArticleID = ARTICLE1, PackDesc = "kar", PackRelation = 10 }
                }
            });
            Mock<ICache<Manufact, string>> manufactCache = PrepareManufactCache(ManufactSettings.DispatchAlgoritmEnum.BigOrSmallFirst);
            Mock<IBookings> bookings = EmptyBookings();


            // act
            var planMaker = new PlanMaker(packingCache.Object, articleCache.Object, manufactCache.Object, PrepareSectionModelCache().Object, onStore.Object, bookings.Object);
            var dirModel = new DirectionModel()
            {
                Items = new List<DirectionItem>()
                {
                    new DirectionItem() { ArtPackID = ARTICLE1_PACK1_ID, ArticleCode = nameof(ARTICLE1), Quantity = 3, DocPosition = 99 }
                }
            };
            var prefixSetings = GetPrefixSettings_AllFromOneSection();

            var result = planMaker.CreatePlan(dirModel, prefixSetings, DateTime.Today);

            // assert
            Assert.True(result.Result);
            Assert.Single(result.ResultItems);
            CheckResultItem(result.ResultItems[0], POS1, ARTICLE1_PACK1_ID, 3, EXP2, CARRIER1, BATCH1, 99);
            CheckHistoryResultJson(result.History, nameof(PlanMakerTest_SmallPart_3));
        }


        [Fact]
        public void PlanMakerTest_SmallPart_10()
        {
            // arrange
            var onStore = new Mock<IOnStore>();
            onStore.Setup(m => m.ByArticle(ARTICLE1, ResultValidityEnum.NotValid, true, null)).Returns(new List<OnStoreItemFull>() {
                new OnStoreItemFull() { ArtPackID = ARTICLE1_PACK1_ID, ArticleID = ARTICLE1, PositionID = POS1, CarrierNum = CARRIER1, BatchNum = BATCH1, Quantity = 5, ExpirationDate = EXP2, Position = new Position() { PosCode = nameof(POS1), PosStatus = Entities.Position.POS_STATUS_OK } },
                new OnStoreItemFull() { ArtPackID = ARTICLE1_PACK1_ID, ArticleID = ARTICLE1, PositionID = POS2, CarrierNum = CARRIER1, BatchNum = BATCH2, Quantity = 10, ExpirationDate = EXP1, Position = new Position() { PosCode = nameof(POS2), PosStatus = Entities.Position.POS_STATUS_OK } },
            });
            Mock<ICache<ArticlePacking, int>> packingCache = PreparePackingCache();
            var articleCache = new Mock<ICache<ArticleModel, int>>();
            articleCache.Setup(m => m.GetItem(ARTICLE1)).Returns(new ArticleModel()
            {
                ArticleID = ARTICLE1,
                ArticleCode = nameof(ARTICLE1),
                ManufID = MANUF_1,
                Packings = new List<ArticlePacking>()
                {
                    new ArticlePacking() { ArtPackID = ARTICLE1_PACK1_ID, ArticleID = ARTICLE1 },
                    new ArticlePacking() { ArtPackID = ARTICLE1_PACK2_ID, ArticleID = ARTICLE1, PackDesc = "kar", PackRelation = 10 }
                }
            });
            Mock<ICache<Manufact, string>> manufactCache = PrepareManufactCache(ManufactSettings.DispatchAlgoritmEnum.BigOrSmallFirst);
            Mock<IBookings> bookings = EmptyBookings();


            // act
            var planMaker = new PlanMaker(packingCache.Object, articleCache.Object, manufactCache.Object, PrepareSectionModelCache().Object, onStore.Object, bookings.Object);
            var dirModel = new DirectionModel()
            {
                Items = new List<DirectionItem>()
                {
                    new DirectionItem() { ArtPackID = ARTICLE1_PACK1_ID, ArticleCode = nameof(ARTICLE1), Quantity = 10, DocPosition = 99 }
                }
            };
            var prefixSetings = GetPrefixSettings_AllFromOneSection();

            var result = planMaker.CreatePlan(dirModel, prefixSetings, DateTime.Today);

            // assert
            Assert.True(result.Result);
            Assert.Single(result.ResultItems);
            CheckResultItem(result.ResultItems[0], POS2, ARTICLE1_PACK1_ID, 10, EXP1, CARRIER1, BATCH2, 99);
            CheckHistoryResultJson(result.History, nameof(PlanMakerTest_SmallPart_10));
        }


        [Fact]
        public void PlanMakerTest_SmallPart_10_2()
        {
            // arrange
            var onStore = new Mock<IOnStore>();
            onStore.Setup(m => m.ByArticle(ARTICLE1, ResultValidityEnum.NotValid, true, null)).Returns(new List<OnStoreItemFull>() {
                new OnStoreItemFull() { ArtPackID = ARTICLE1_PACK1_ID, ArticleID = ARTICLE1, PositionID = POS1, CarrierNum = CARRIER1, BatchNum = BATCH1, Quantity = 5, ExpirationDate = EXP2, Position = new Position() { PosCode = nameof(POS1), PosStatus = Entities.Position.POS_STATUS_OK } },
                new OnStoreItemFull() { ArtPackID = ARTICLE1_PACK1_ID, ArticleID = ARTICLE1, PositionID = POS2, CarrierNum = CARRIER1, BatchNum = BATCH2, Quantity = 20, ExpirationDate = EXP1, Position = new Position() { PosCode = nameof(POS2), PosStatus = Entities.Position.POS_STATUS_OK } },
            });
            Mock<ICache<ArticlePacking, int>> packingCache = PreparePackingCache();
            var articleCache = new Mock<ICache<ArticleModel, int>>();
            articleCache.Setup(m => m.GetItem(ARTICLE1)).Returns(new ArticleModel()
            {
                ArticleID = ARTICLE1,
                ArticleCode = nameof(ARTICLE1),
                ManufID = MANUF_1,
                Packings = new List<ArticlePacking>()
                {
                    new ArticlePacking() { ArtPackID = ARTICLE1_PACK1_ID, ArticleID = ARTICLE1 },
                    new ArticlePacking() { ArtPackID = ARTICLE1_PACK2_ID, ArticleID = ARTICLE1, PackDesc = "kar", PackRelation = 10 }
                }
            });
            Mock<ICache<Manufact, string>> manufactCache = PrepareManufactCache(ManufactSettings.DispatchAlgoritmEnum.BigOrSmallFirst);
            Mock<IBookings> bookings = EmptyBookings();


            // act
            var planMaker = new PlanMaker(packingCache.Object, articleCache.Object, manufactCache.Object, PrepareSectionModelCache().Object, onStore.Object, bookings.Object);
            var dirModel = new DirectionModel()
            {
                Items = new List<DirectionItem>()
                {
                    new DirectionItem() { ArtPackID = ARTICLE1_PACK1_ID, ArticleCode = nameof(ARTICLE1), Quantity = 10, DocPosition = 99 }
                }
            };
            var prefixSetings = GetPrefixSettings_AllFromOneSection();

            var result = planMaker.CreatePlan(dirModel, prefixSetings, DateTime.Today);

            // assert
            Assert.True(result.Result);
            Assert.Single(result.ResultItems);
            CheckResultItem(result.ResultItems[0], POS2, ARTICLE1_PACK1_ID, 10, EXP1, CARRIER1, BATCH2, 99);
            CheckHistoryResultJson(result.History, nameof(PlanMakerTest_SmallPart_10_2));
        }



        [Fact]
        public void PlanMakerTest_NoSecondary()
        {
            // arrange
            var onStore = new Mock<IOnStore>();
            onStore.Setup(m => m.ByArticle(ARTICLE1, ResultValidityEnum.NotValid, true, null)).Returns(new List<OnStoreItemFull>() {
                new OnStoreItemFull() { ArtPackID = ARTICLE1_PACK1_ID, ArticleID = ARTICLE1, PositionID = POS1, CarrierNum = CARRIER1, BatchNum = BATCH1, Quantity = 5, ExpirationDate = EXP2, Position = new Position() { PosCode = nameof(POS1), PosStatus = Entities.Position.POS_STATUS_OK } },
                new OnStoreItemFull() { ArtPackID = ARTICLE1_PACK1_ID, ArticleID = ARTICLE1, PositionID = POS2, CarrierNum = CARRIER1, BatchNum = BATCH2, Quantity = 11, ExpirationDate = EXP1, Position = new Position() { PosCode = nameof(POS2), PosStatus = Entities.Position.POS_STATUS_OK } },
            });
            Mock<ICache<ArticlePacking, int>> packingCache = PreparePackingCache();
            var articleCache = new Mock<ICache<ArticleModel, int>>();
            articleCache.Setup(m => m.GetItem(ARTICLE1)).Returns(new ArticleModel()
            {
                ArticleID = ARTICLE1,
                ArticleCode = nameof(ARTICLE1),
                ManufID = MANUF_1,
                Packings = new List<ArticlePacking>()
                {
                    new ArticlePacking() { ArtPackID = ARTICLE1_PACK1_ID, ArticleID = ARTICLE1 },
                }
            });
            Mock<ICache<Manufact, string>> manufactCache = PrepareManufactCache(ManufactSettings.DispatchAlgoritmEnum.BigOrSmallFirst);
            Mock<IBookings> bookings = EmptyBookings();


            // act
            var planMaker = new PlanMaker(packingCache.Object, articleCache.Object, manufactCache.Object, PrepareSectionModelCache().Object, onStore.Object, bookings.Object);
            var dirModel = new DirectionModel()
            {
                Items = new List<DirectionItem>()
                {
                    new DirectionItem() { ArtPackID = ARTICLE1_PACK1_ID, ArticleCode = nameof(ARTICLE1), Quantity = 10, DocPosition = 99 }
                }
            };
            var prefixSetings = GetPrefixSettings_AllFromOneSection();

            var result = planMaker.CreatePlan(dirModel, prefixSetings, DateTime.Today);

            // assert
            Assert.True(result.Result);
            Assert.Single(result.ResultItems);
            CheckResultItem(result.ResultItems[0], POS2, ARTICLE1_PACK1_ID, 10, EXP1, CARRIER1, BATCH2, 99);
            CheckHistoryResultJson(result.History, nameof(PlanMakerTest_NoSecondary));
        }
    }
}

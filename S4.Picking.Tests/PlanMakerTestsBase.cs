﻿using Moq;
using S4.StoreCore.Interfaces;
using S4.Picking;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using S4.Core.Cache;
using S4.Entities;
using S4.Entities.Models;
using S4.Entities.Settings;
using Newtonsoft.Json;
using S4.DAL.Interfaces;


namespace S4.Picking.Tests
{
    public class PlanMakerTestsBase
    {
        public const int ARTICLE1 = 1;
        public const int ARTICLE1_PACK1_ID = 100;
        public const int ARTICLE1_PACK2_ID = 101;
        public const int ARTICLE2 = 2;
        public const int ARTICLE2_PACK1_ID = 200;
        public const int ARTICLE2_PACK2_ID = 201;
        public const int ARTICLE3 = 3;
        public const int ARTICLE3_PACK1_ID = 300;
        public const int ARTICLE4 = 4;
        public const int ARTICLE4_PACK1_ID = 400;

        public const int LOT4_1_ID = 40_001;
        public const int LOT4_2_ID = 40_002;

        public const int POS1 = 1000;
        public const int POS2 = 2000;
        public const int POS3 = 3000;
        public const int POS4 = 4000;
        public const int POS5 = 5000;

        public const string CARRIER1 = "CARR1";
        public const string CARRIER2 = "CARR2";

        public const string BATCH1 = "BATCH1";
        public const string BATCH2 = "BATCH2";

        public const string SEC_1 = "SEC1";
        public const string SEC_2 = "SEC2";
        public const string SEC_3 = "SEC3";
        public const string SEC_DIRECT = "SEC_DIRECT";
        public const string SEC_ALTERNATIVE = "SEC_ALTERNATIVE";

        public const string MANUF_1 = "MAN1"; //DispatchAlgoritm = ByExpiration

        public const int PARTNER1 = -1;
        public const int PARTNER2 = -2;

        public const int BOOKING1 = 1000;
        public const int BOOKING2 = 1001;

        public static readonly DateTime? EXP1 = new DateTime(2033, 01, 01);
        public static readonly DateTime? EXP2 = new DateTime(2034, 01, 01);
        public static readonly DateTime? EXP3 = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(1);
        public static readonly DateTime? EXP4 = new DateTime(2035, 02, 02);


        public static void CheckHistoryResultJson(PickingHistoryNode history, string testName)
        {
            var json = JsonConvert.SerializeObject(history, Formatting.Indented);
            var fromFile = System.IO.File.ReadAllText($@"..\..\..\HistoryResult\{testName}.json");
            fromFile = fromFile.Replace("$EXP3$", EXP3.Value.ToString("yyyy-MM-dd"));
            Assert.Equal(fromFile, json);
        }


        public static void CheckResultItem(ResultItem resultItem, int positionID, int artPackID, int quantity, DateTime? expirationDate, string carrierNum, string batchNum, int docPosition)
        {
            Assert.Equal(positionID, resultItem.PositionID);
            Assert.Equal(artPackID, resultItem.ArtPackID);
            Assert.Equal(quantity, resultItem.Quantity);
            Assert.Equal(expirationDate, resultItem.ExpirationDate);
            Assert.Equal(carrierNum, resultItem.CarrierNum);
            Assert.Equal(batchNum, resultItem.BatchNum);
            Assert.Equal(docPosition, resultItem.DocPosition);
        }


        public static Mock<IOnStore> PrepareOnStore()
        {
            var onStore = new Mock<IOnStore>();

            onStore.Setup(m => m.ByArticle(ARTICLE1, ResultValidityEnum.NotValid, true, null)).Returns(new List<OnStoreItemFull>()
            {
                new OnStoreItemFull() { ArtPackID = ARTICLE1_PACK1_ID, ArticleID = ARTICLE1, PositionID = POS1, CarrierNum = CARRIER1, BatchNum = BATCH1, Quantity = 5, ExpirationDate = EXP2, Position = new Position() { PosCode = nameof(POS1), PosStatus = Entities.Position.POS_STATUS_OK } },
                new OnStoreItemFull() { ArtPackID = ARTICLE1_PACK1_ID, ArticleID = ARTICLE1, PositionID = POS2, CarrierNum = CARRIER1, BatchNum = BATCH2, Quantity = 10, ExpirationDate = EXP1, Position = new Position() { PosCode = nameof(POS2), PosStatus = Entities.Position.POS_STATUS_OK } },
                new OnStoreItemFull() { ArtPackID = ARTICLE1_PACK1_ID, ArticleID = ARTICLE1, PositionID = POS3, CarrierNum = CARRIER1, BatchNum = BATCH2, Quantity = 1000, ExpirationDate = EXP1, Position = new Position() { PosCode = nameof(POS3), PosStatus = Entities.Position.POS_STATUS_OK } }
            });
            onStore.Setup(m => m.ByArticle(ARTICLE2, ResultValidityEnum.NotValid, true, null)).Returns(new List<OnStoreItemFull>()
            {
                new OnStoreItemFull() { ArtPackID = ARTICLE2_PACK1_ID, ArticleID = ARTICLE2, PositionID = POS1, CarrierNum = CARRIER1, BatchNum = BATCH1, Quantity = 5, ExpirationDate = EXP2, Position = new Position() { PosCode = nameof(POS1), PosStatus = Entities.Position.POS_STATUS_OK } },
                new OnStoreItemFull() { ArtPackID = ARTICLE2_PACK1_ID, ArticleID = ARTICLE2, PositionID = POS2, CarrierNum = CARRIER1, BatchNum = BATCH2, Quantity = 10, ExpirationDate = EXP1, Position = new Position() { PosCode = nameof(POS2), PosStatus = Entities.Position.POS_STATUS_OK } },
                new OnStoreItemFull() { ArtPackID = ARTICLE2_PACK1_ID, ArticleID = ARTICLE2, PositionID = POS4, CarrierNum = CARRIER1, BatchNum = BATCH2, Quantity = 10, ExpirationDate = EXP1, Position = new Position() { PosCode = nameof(POS4), PosStatus = Entities.Position.POS_STATUS_OK } },
            });
            onStore.Setup(m => m.ByArticle(ARTICLE3, ResultValidityEnum.NotValid, true, null)).Returns(new List<OnStoreItemFull>()
            {
                new OnStoreItemFull() { ArtPackID = ARTICLE3_PACK1_ID, ArticleID = ARTICLE3, PositionID = POS5, CarrierNum = CARRIER2, BatchNum = BATCH1, Quantity = 3, ExpirationDate = EXP3, Position = new Position() { PosCode = nameof(POS5), PosStatus = Entities.Position.POS_STATUS_OK } },
                new OnStoreItemFull() { ArtPackID = ARTICLE3_PACK1_ID, ArticleID = ARTICLE3, PositionID = POS5, CarrierNum = CARRIER2, BatchNum = BATCH1, Quantity = 2, ExpirationDate = EXP2, Position = new Position() { PosCode = nameof(POS5), PosStatus = Entities.Position.POS_STATUS_OK } },
                new OnStoreItemFull() { ArtPackID = ARTICLE3_PACK1_ID, ArticleID = ARTICLE3, PositionID = POS5, CarrierNum = CARRIER2, BatchNum = BATCH2, Quantity = 10, ExpirationDate = EXP1, Position = new Position() { PosCode = nameof(POS5), PosStatus = Entities.Position.POS_STATUS_OK } },
            });
            onStore.Setup(m => m.ByArticle(ARTICLE4, ResultValidityEnum.NotValid, true, null)).Returns(new List<OnStoreItemFull>()
            {
                new OnStoreItemFull() { ArtPackID = ARTICLE4_PACK1_ID, ArticleID = ARTICLE4, PositionID = POS5, StoMoveLotID = LOT4_1_ID, CarrierNum = CARRIER2, BatchNum = BATCH1, Quantity = 20, ExpirationDate = EXP4, Position = new Position() { PosCode = nameof(POS5), PosStatus = Entities.Position.POS_STATUS_OK } },
                new OnStoreItemFull() { ArtPackID = ARTICLE4_PACK1_ID, ArticleID = ARTICLE4, PositionID = POS5, StoMoveLotID = LOT4_2_ID, CarrierNum = CARRIER1, BatchNum = BATCH2, Quantity = 50, ExpirationDate = EXP2, Position = new Position() { PosCode = nameof(POS5), PosStatus = Entities.Position.POS_STATUS_OK } },
                new OnStoreItemFull() { ArtPackID = ARTICLE4_PACK1_ID, ArticleID = ARTICLE4, PositionID = POS5, StoMoveLotID = LOT4_2_ID, CarrierNum = CARRIER1, BatchNum = BATCH2, Quantity = 10, ExpirationDate = EXP1, Position = new Position() { PosCode = nameof(POS5), PosStatus = Entities.Position.POS_STATUS_OK } },
            });

            return onStore;
        }


        public static Mock<ICache<Manufact, string>> PrepareManufactCache(ManufactSettings.DispatchAlgoritmEnum dispatchAlgoritm)
        {
            var manufactCache = new Mock<ICache<Manufact, string>>();
            manufactCache.Setup(m => m.GetItem(MANUF_1)).Returns(new Manufact() { ManufID = MANUF_1, ManufactSettings = new ManufactSettings() { DispatchAlgoritm = dispatchAlgoritm } });
            return manufactCache;
        }


        public static Mock<ICache<ArticleModel, int>> PrepareArticleCache()
        {
            var articleCache = new Mock<ICache<ArticleModel, int>>();
            articleCache.Setup(m => m.GetItem(ARTICLE1)).Returns(new ArticleModel() { ArticleID = ARTICLE1, ArticleCode = nameof(ARTICLE1), ManufID = MANUF_1, Packings = new List<ArticlePacking>() { new ArticlePacking() { ArtPackID = ARTICLE1_PACK1_ID, ArticleID = ARTICLE1 } } });
            articleCache.Setup(m => m.GetItem(ARTICLE2)).Returns(new ArticleModel() { ArticleID = ARTICLE2, ArticleCode = nameof(ARTICLE2), ManufID = MANUF_1, Packings = new List<ArticlePacking>() { new ArticlePacking() { ArtPackID = ARTICLE2_PACK1_ID, ArticleID = ARTICLE2 } } });
            articleCache.Setup(m => m.GetItem(ARTICLE3)).Returns(new ArticleModel() { ArticleID = ARTICLE3, ArticleCode = nameof(ARTICLE3), ManufID = MANUF_1, Packings = new List<ArticlePacking>() { new ArticlePacking() { ArtPackID = ARTICLE3_PACK1_ID, ArticleID = ARTICLE3 } } });
            articleCache.Setup(m => m.GetItem(ARTICLE4)).Returns(new ArticleModel() { ArticleID = ARTICLE4, ArticleCode = nameof(ARTICLE4), ManufID = MANUF_1, Packings = new List<ArticlePacking>() { new ArticlePacking() { ArtPackID = ARTICLE4_PACK1_ID, ArticleID = ARTICLE4 } } });
            return articleCache;
        }


        public static Mock<ICache<SectionModel, string>> PrepareSectionModelCache()
        {
            var sectionModelCache = new Mock<ICache<SectionModel, string>>();
            sectionModelCache.Setup(m => m.GetItem(SEC_1)).Returns(new SectionModel() { SectID = SEC_1, PositionIDs = new HashSet<int>() { POS1, POS2 } });
            sectionModelCache.Setup(m => m.GetItem(SEC_2)).Returns(new SectionModel() { SectID = SEC_2, PositionIDs = new HashSet<int>() });
            sectionModelCache.Setup(m => m.GetItem(SEC_ALTERNATIVE)).Returns(new SectionModel() { SectID = SEC_ALTERNATIVE, PositionIDs = new HashSet<int>() { POS3 } });            
            sectionModelCache.Setup(m => m.GetItem(SEC_DIRECT)).Returns(new SectionModel() { SectID = SEC_DIRECT, PositionIDs = new HashSet<int>() { POS4 } });
            sectionModelCache.Setup(m => m.GetItem(SEC_3)).Returns(new SectionModel() { SectID = SEC_3, PositionIDs = new HashSet<int>() { POS5 } });
            return sectionModelCache;
        }


        public static Mock<ICache<ArticlePacking, int>> PreparePackingCache()
        {
            var packingCache = new Mock<ICache<ArticlePacking, int>>();
            packingCache.Setup(m => m.GetItem(ARTICLE1_PACK1_ID)).Returns(new ArticlePacking() { ArtPackID = ARTICLE1_PACK1_ID, ArticleID = ARTICLE1 });
            packingCache.Setup(m => m.GetItem(ARTICLE2_PACK1_ID)).Returns(new ArticlePacking() { ArtPackID = ARTICLE2_PACK1_ID, ArticleID = ARTICLE2 });
            packingCache.Setup(m => m.GetItem(ARTICLE3_PACK1_ID)).Returns(new ArticlePacking() { ArtPackID = ARTICLE3_PACK1_ID, ArticleID = ARTICLE3 });
            packingCache.Setup(m => m.GetItem(ARTICLE4_PACK1_ID)).Returns(new ArticlePacking() { ArtPackID = ARTICLE4_PACK1_ID, ArticleID = ARTICLE4 });
            return packingCache;
        }


        public static PrefixSettings GetPrefixSettings_AllFromOneSection()
        {
            return new PrefixSettings()
            {
                DispatchDistribution = PrefixSettings.DispatchDistributionEnum.AllFromOneSection,
                DispatchSections = new List<PrefixSettings.DispatchSection>()
                {
                    new PrefixSettings.DispatchSection() { Order = 0, SectID = SEC_1 },
                    new PrefixSettings.DispatchSection() { Order = 1, SectID = SEC_2 },
                    new PrefixSettings.DispatchSection() { Order = 2, SectID = SEC_3 },
                }
            };
        }


        public static PrefixSettings GetPrefixSettings_OneFromOneSection()
        {
            return new PrefixSettings()
            {
                DispatchDistribution = PrefixSettings.DispatchDistributionEnum.OneFromOneSection,
                DispatchSections = new List<PrefixSettings.DispatchSection>()
                {
                    new PrefixSettings.DispatchSection() { Order = 0, SectID = SEC_1 },
                    new PrefixSettings.DispatchSection() { Order = 1, SectID = SEC_2 },
                    new PrefixSettings.DispatchSection() { Order = 2, SectID = SEC_3 },
                }
            };
        }


        protected Mock<IBookings> EmptyBookings()
        {
            var bookings = new Mock<IBookings>();
            bookings.Setup(m => m.ListValidBookings(It.IsAny<int>())).Returns(new List<BookingInfo>());
            return bookings;
        }



    }
}

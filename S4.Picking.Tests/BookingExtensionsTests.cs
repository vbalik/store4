﻿using S4.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace S4.Picking.Tests
{
    public class BookingExtensionsTests
    {
        [Fact]
        public void BookingExtensions_RemoveUsedBookings_OK()
        {
            var bookings = new List<BookingInfo>() 
            { 
                new BookingInfo() { BookingID = 1, RequiredQuantity = 1 },
                new BookingInfo() { BookingID = 2, RequiredQuantity = 2 },
            };
            var usedBookings = new List<UsedBookingItem>()
            {
                new UsedBookingItem() { BookingID = 1, UsedQuantity = 1 }
            };

            var res = bookings.RemoveUsedBookings(usedBookings);

            Assert.NotNull(res);
            Assert.Single(res);
            Assert.Equal(2, res[0].BookingID);
            Assert.Equal(2, res[0].RequiredQuantity);
        }
    }
}

﻿using Moq;
using S4.Core.Cache;
using S4.DAL.Interfaces;
using S4.Entities;
using S4.Entities.Models;
using S4.Entities.Settings;
using S4.StoreCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace S4.Picking.Tests
{
    public class PlanMakerTests_Bookings : PlanMakerTestsBase
    {
        [Fact]
        public void PlanMakerTest_Booking_Found_Part()
        {
            // arrange
            Mock<IOnStore> onStore = PrepareOnStore();
            Mock<ICache<ArticlePacking, int>> packingCache = PreparePackingCache();
            Mock<ICache<ArticleModel, int>> articleCache = PrepareArticleCache();
            Mock<ICache<Manufact, string>> manufactCache = PrepareManufactCache(ManufactSettings.DispatchAlgoritmEnum.ByExpiration);
            Mock<IBookings> bookings = PrepareBookings();


            // act
            var planMaker = new PlanMaker(packingCache.Object, articleCache.Object, manufactCache.Object, PrepareSectionModelCache().Object, onStore.Object, bookings.Object);
            var dirModel = new DirectionModel() { PartnerID = PARTNER1, Items = new List<DirectionItem>() { new DirectionItem() { ArtPackID = ARTICLE4_PACK1_ID, ArticleCode = nameof(ARTICLE4), Quantity = 10, DocPosition = 99 } } };
            var prefixSetings = new PrefixSettings()
            {
                DispatchDistribution = PrefixSettings.DispatchDistributionEnum.AllFromOneSection,
                DispatchSections = new List<PrefixSettings.DispatchSection>()
                {              
                    new PrefixSettings.DispatchSection() { Order = 0, SectID = SEC_3 },
                }
            };

            var result = planMaker.CreatePlan(dirModel, prefixSetings, DateTime.Today);


            // assert
            Assert.True(result.Result);
            Assert.Single(result.ResultItems);
            CheckResultItem(result.ResultItems[0], POS5, ARTICLE4_PACK1_ID, 10, EXP4, CARRIER2, BATCH1, 99);
            
            Assert.Single(result.UsedBookings);
            Assert.Equal(10, result.UsedBookings[0].UsedQuantity);
            Assert.Equal(BOOKING1, result.UsedBookings[0].BookingID);

            CheckHistoryResultJson(result.History, nameof(PlanMakerTest_Booking_Found_Part));
        }


        [Fact]
        public void PlanMakerTest_Booking_Found_Unsufficient()
        {
            // arrange
            Mock<IOnStore> onStore = PrepareOnStore();
            Mock<ICache<ArticlePacking, int>> packingCache = PreparePackingCache();
            Mock<ICache<ArticleModel, int>> articleCache = PrepareArticleCache();
            Mock<ICache<Manufact, string>> manufactCache = PrepareManufactCache(ManufactSettings.DispatchAlgoritmEnum.ByExpiration);
            Mock<IBookings> bookings = PrepareBookings();


            // act
            var planMaker = new PlanMaker(packingCache.Object, articleCache.Object, manufactCache.Object, PrepareSectionModelCache().Object, onStore.Object, bookings.Object);
            var dirModel = new DirectionModel() { PartnerID = PARTNER1, Items = new List<DirectionItem>() { new DirectionItem() { ArtPackID = ARTICLE4_PACK1_ID, ArticleCode = nameof(ARTICLE4), Quantity = 30, DocPosition = 99 } } };
            var prefixSetings = new PrefixSettings()
            {
                DispatchDistribution = PrefixSettings.DispatchDistributionEnum.AllFromOneSection,
                DispatchSections = new List<PrefixSettings.DispatchSection>()
                {
                    new PrefixSettings.DispatchSection() { Order = 0, SectID = SEC_3 },
                }
            };

            var result = planMaker.CreatePlan(dirModel, prefixSetings, DateTime.Today);


            // assert
            Assert.True(result.Result);
            Assert.Equal(2, result.ResultItems.Count);
            CheckResultItem(result.ResultItems[0], POS5, ARTICLE4_PACK1_ID, 20, EXP4, CARRIER2, BATCH1, 99);
            CheckResultItem(result.ResultItems[1], POS5, ARTICLE4_PACK1_ID, 10, EXP1, CARRIER1, BATCH2, 99);
            Assert.Single(result.UsedBookings);
            Assert.Equal(20, result.UsedBookings[0].UsedQuantity);
            Assert.Equal(BOOKING1, result.UsedBookings[0].BookingID);

            CheckHistoryResultJson(result.History, nameof(PlanMakerTest_Booking_Found_Unsufficient));
        }


        [Fact]
        public void PlanMakerTest_Booking_NotFound()
        {
            // arrange
            Mock<IOnStore> onStore = PrepareOnStore();
            Mock<ICache<ArticlePacking, int>> packingCache = PreparePackingCache();
            Mock<ICache<ArticleModel, int>> articleCache = PrepareArticleCache();
            Mock<ICache<Manufact, string>> manufactCache = PrepareManufactCache(ManufactSettings.DispatchAlgoritmEnum.ByExpiration);
            Mock<IBookings> bookings = PrepareBookings();


            // act
            var planMaker = new PlanMaker(packingCache.Object, articleCache.Object, manufactCache.Object, PrepareSectionModelCache().Object, onStore.Object, bookings.Object);
            var dirModel = new DirectionModel() { PartnerID = PARTNER2, Items = new List<DirectionItem>() { new DirectionItem() { ArtPackID = ARTICLE4_PACK1_ID, ArticleCode = nameof(ARTICLE4), Quantity = 30, DocPosition = 99 } } };
            var prefixSetings = new PrefixSettings()
            {
                DispatchDistribution = PrefixSettings.DispatchDistributionEnum.AllFromOneSection,
                DispatchSections = new List<PrefixSettings.DispatchSection>()
                {
                    new PrefixSettings.DispatchSection() { Order = 0, SectID = SEC_3 },
                }
            };

            var result = planMaker.CreatePlan(dirModel, prefixSetings, DateTime.Today);


            // assert
            Assert.True(result.Result);
            Assert.Equal(2, result.ResultItems.Count);
            CheckResultItem(result.ResultItems[0], POS5, ARTICLE4_PACK1_ID, 10, EXP1, CARRIER1, BATCH2, 99);
            CheckResultItem(result.ResultItems[1], POS5, ARTICLE4_PACK1_ID, 20, EXP2, CARRIER1, BATCH2, 99);
            Assert.Empty(result.UsedBookings);

            CheckHistoryResultJson(result.History, nameof(PlanMakerTest_Booking_NotFound));
        }


        [Fact]
        public void PlanMakerTest_Booking_TwoLots()
        {
            // arrange
            Mock<IOnStore> onStore = PrepareOnStore();
            Mock<ICache<ArticlePacking, int>> packingCache = PreparePackingCache();
            Mock<ICache<ArticleModel, int>> articleCache = PrepareArticleCache();
            Mock<ICache<Manufact, string>> manufactCache = PrepareManufactCache(ManufactSettings.DispatchAlgoritmEnum.ByExpiration);
            Mock<IBookings> bookings = PrepareBookings_TwoLots();


            // act
            var planMaker = new PlanMaker(packingCache.Object, articleCache.Object, manufactCache.Object, PrepareSectionModelCache().Object, onStore.Object, bookings.Object);
            var dirModel = new DirectionModel() { PartnerID = PARTNER2, Items = new List<DirectionItem>() { 
                    new DirectionItem() { ArtPackID = ARTICLE4_PACK1_ID, ArticleCode = nameof(ARTICLE4), Quantity = 40, DocPosition = 98 },
            } };
            var prefixSetings = new PrefixSettings()
            {
                DispatchDistribution = PrefixSettings.DispatchDistributionEnum.AllFromOneSection,
                DispatchSections = new List<PrefixSettings.DispatchSection>()
                {
                    new PrefixSettings.DispatchSection() { Order = 0, SectID = SEC_3 },
                }
            };

            var result = planMaker.CreatePlan(dirModel, prefixSetings, DateTime.Today);


            // assert
            Assert.True(result.Result);
            Assert.Equal(2, result.ResultItems.Count);
            CheckResultItem(result.ResultItems[0], POS5, ARTICLE4_PACK1_ID, 20, EXP4, CARRIER2, BATCH1, 98);
            CheckResultItem(result.ResultItems[1], POS5, ARTICLE4_PACK1_ID, 20, EXP2, CARRIER1, BATCH2, 98);
            Assert.NotEmpty(result.UsedBookings);
            Assert.Equal(2, result.UsedBookings.Count);
            Assert.Equal(BOOKING1, result.UsedBookings[0].BookingID);
            Assert.Equal(BOOKING2, result.UsedBookings[1].BookingID);

            CheckHistoryResultJson(result.History, nameof(PlanMakerTest_Booking_TwoLots));
        }


        protected Mock<IBookings> PrepareBookings()
        {
            var bookings = new Mock<IBookings>();
            bookings.Setup(m => m.ListValidBookings(It.IsAny<int>())).Returns(new List<BookingInfo>()
            {
                new BookingInfo() { PartnerID = PARTNER1, BookingID = BOOKING1, StoMoveLotID = LOT4_1_ID, RequiredQuantity = 100 }
            });
            return bookings;
        }


        protected Mock<IBookings> PrepareBookings_TwoLots()
        {
            var bookings = new Mock<IBookings>();
            bookings.Setup(m => m.ListValidBookings(It.IsAny<int>())).Returns(new List<BookingInfo>()
            {
                new BookingInfo() { PartnerID = PARTNER2, BookingID = BOOKING1, StoMoveLotID = LOT4_1_ID, RequiredQuantity = 20 },
                new BookingInfo() { PartnerID = PARTNER2, BookingID = BOOKING2, StoMoveLotID = LOT4_2_ID, RequiredQuantity = 20 }
            });
            return bookings;
        }
    }
}

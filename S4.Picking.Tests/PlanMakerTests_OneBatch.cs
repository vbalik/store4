﻿using Moq;
using S4.StoreCore.Interfaces;
using S4.Picking;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using S4.Core.Cache;
using S4.Entities;
using S4.Entities.Models;
using S4.Entities.Settings;
using Newtonsoft.Json;
using S4.DAL.Interfaces;

namespace S4.Picking.Tests
{
    public class PlanMakerTests_OneBatch : PlanMakerTestsBase
    {

        [Fact]
        public void PlanMakerTest_OneBatch_7()
        {
            // arrange
            Mock<IOnStore> onStore = PrepareOnStore();
            Mock<ICache<ArticlePacking, int>> packingCache = PreparePackingCache();
            Mock<ICache<ArticleModel, int>> articleCache = PrepareArticleCache();
            Mock<ICache<Manufact, string>> manufactCache = PrepareManufactCache(ManufactSettings.DispatchAlgoritmEnum.OneBatchFirst);
            Mock<IBookings> bookings = EmptyBookings();


            // act
            var planMaker = new PlanMaker(packingCache.Object, articleCache.Object, manufactCache.Object, PrepareSectionModelCache().Object, onStore.Object, bookings.Object);
            var dirModel = new DirectionModel()
            {
                Items = new List<DirectionItem>() { new DirectionItem() {
                ArtPackID = ARTICLE1_PACK1_ID,
                ArticleCode = nameof(ARTICLE1),
                Quantity = 7,
                DocPosition = 11
            } }
            };
            var prefixSetings = GetPrefixSettings_AllFromOneSection();

            var result = planMaker.CreatePlan(dirModel, prefixSetings, DateTime.Today);

            // assert
            Assert.True(result.Result);
            Assert.Single(result.ResultItems);
            CheckResultItem(result.ResultItems[0], POS2, ARTICLE1_PACK1_ID, 7, EXP1, CARRIER1, BATCH2, 11);
            CheckHistoryResultJson(result.History, nameof(PlanMakerTest_OneBatch_7));
        }


        [Fact]
        public void PlanMakerTest_OneBatch_11()
        {
            // arrange
            Mock<IOnStore> onStore = PrepareOnStore();
            Mock<ICache<ArticlePacking, int>> packingCache = PreparePackingCache();
            Mock<ICache<ArticleModel, int>> articleCache = PrepareArticleCache();
            Mock<ICache<Manufact, string>> manufactCache = PrepareManufactCache(ManufactSettings.DispatchAlgoritmEnum.OneBatchFirst);
            Mock<IBookings> bookings = EmptyBookings();


            // act
            var planMaker = new PlanMaker(packingCache.Object, articleCache.Object, manufactCache.Object, PrepareSectionModelCache().Object, onStore.Object, bookings.Object);
            var dirModel = new DirectionModel()
            {
                Items = new List<DirectionItem>() { new DirectionItem() {
                ArtPackID = ARTICLE1_PACK1_ID,
                ArticleCode = nameof(ARTICLE1),
                Quantity = 11,
                DocPosition = 22
            } }
            };
            var prefixSetings = GetPrefixSettings_AllFromOneSection();

            var result = planMaker.CreatePlan(dirModel, prefixSetings, DateTime.Today);

            // assert
            Assert.True(result.Result);
            Assert.Equal(2, result.ResultItems.Count);
            CheckResultItem(result.ResultItems[0], POS2, ARTICLE1_PACK1_ID, 10, EXP1, CARRIER1, BATCH2, 22);
            CheckResultItem(result.ResultItems[1], POS1, ARTICLE1_PACK1_ID, 1, EXP2, CARRIER1, BATCH1, 22);
            CheckHistoryResultJson(result.History, nameof(PlanMakerTest_OneBatch_11));
        }


        [Fact]
        public void PlanMakerTest_OneBatch_21()
        {
            // arrange
            Mock<IOnStore> onStore = PrepareOnStore();
            Mock<ICache<ArticlePacking, int>> packingCache = PreparePackingCache();
            Mock<ICache<ArticleModel, int>> articleCache = PrepareArticleCache();
            Mock<ICache<Manufact, string>> manufactCache = PrepareManufactCache(ManufactSettings.DispatchAlgoritmEnum.OneBatchFirst);
            Mock<IBookings> bookings = EmptyBookings();


            // act
            var planMaker = new PlanMaker(packingCache.Object, articleCache.Object, manufactCache.Object, PrepareSectionModelCache().Object, onStore.Object, bookings.Object);
            var dirModel = new DirectionModel()
            {
                Items = new List<DirectionItem>() { new DirectionItem() {
                ArtPackID = ARTICLE1_PACK1_ID,
                ArticleCode = nameof(ARTICLE1),
                Quantity = 21,
                DocPosition = 99
            } }
            };
            var prefixSetings = GetPrefixSettings_AllFromOneSection();

            var result = planMaker.CreatePlan(dirModel, prefixSetings, DateTime.Today);

            // assert
            Assert.False(result.Result);
            Assert.Null(result.ResultItems);
            CheckHistoryResultJson(result.History, nameof(PlanMakerTest_OneBatch_21));
        }

    }
}

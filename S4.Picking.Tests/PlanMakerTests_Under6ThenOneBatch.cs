﻿using Moq;
using S4.StoreCore.Interfaces;
using S4.Picking;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using S4.Core.Cache;
using S4.Entities;
using S4.Entities.Models;
using S4.Entities.Settings;
using Newtonsoft.Json;
using S4.DAL.Interfaces;

namespace S4.Picking.Tests
{
    public class PlanMakerTests_Under6ThenOneBatch : PlanMakerTestsBase
    {
        [Fact]
        public void PlanMakerTest_Under6ThenOneBatch_3()
        {
            // arrange
            Mock<IOnStore> onStore = PrepareOnStore();
            Mock<ICache<ArticlePacking, int>> packingCache = PreparePackingCache();
            Mock<ICache<ArticleModel, int>> articleCache = PrepareArticleCache();
            Mock<ICache<Manufact, string>> manufactCache = PrepareManufactCache(ManufactSettings.DispatchAlgoritmEnum.Under6ThenOneBatchFirst);
            Mock<IBookings> bookings = EmptyBookings();


            // act
            var planMaker = new PlanMaker(packingCache.Object, articleCache.Object, manufactCache.Object, PrepareSectionModelCache().Object, onStore.Object, bookings.Object);
            var dirModel = new DirectionModel()
            {
                Items = new List<DirectionItem>() { new DirectionItem() {
                ArtPackID = ARTICLE3_PACK1_ID,
                ArticleCode = nameof(ARTICLE3),
                Quantity = 7,
                DocPosition = 111
            } }
            };
            var prefixSetings = GetPrefixSettings_AllFromOneSection();

            var result = planMaker.CreatePlan(dirModel, prefixSetings, DateTime.Today);

            // assert
            Assert.True(result.Result);
            Assert.Equal(2, result.ResultItems.Count);
            CheckResultItem(result.ResultItems[0], POS5, ARTICLE3_PACK1_ID, 3, EXP3, CARRIER2, BATCH1, 111);
            CheckResultItem(result.ResultItems[1], POS5, ARTICLE3_PACK1_ID, 4, EXP1, CARRIER2, BATCH2, 111);
            CheckHistoryResultJson(result.History, nameof(PlanMakerTest_Under6ThenOneBatch_3));
        }

        [Fact]
        public void PlanMakerTest_Under6ThenOneBatch_7()
        {
            // arrange
            Mock<IOnStore> onStore = PrepareOnStore();
            Mock<ICache<ArticlePacking, int>> packingCache = PreparePackingCache();
            Mock<ICache<ArticleModel, int>> articleCache = PrepareArticleCache();
            Mock<ICache<Manufact, string>> manufactCache = PrepareManufactCache(ManufactSettings.DispatchAlgoritmEnum.Under6ThenOneBatchFirst);
            Mock<IBookings> bookings = EmptyBookings();


            // act
            var planMaker = new PlanMaker(packingCache.Object, articleCache.Object, manufactCache.Object, PrepareSectionModelCache().Object, onStore.Object, bookings.Object);
            var dirModel = new DirectionModel()
            {
                Items = new List<DirectionItem>() { new DirectionItem() {
                ArtPackID = ARTICLE3_PACK1_ID,
                ArticleCode = nameof(ARTICLE3),
                Quantity = 7,
                DocPosition = 112
            } }
            };
            var prefixSetings = GetPrefixSettings_AllFromOneSection();

            var result = planMaker.CreatePlan(dirModel, prefixSetings, DateTime.Today);

            // assert
            Assert.True(result.Result);
            Assert.Equal(2, result.ResultItems.Count);
            CheckResultItem(result.ResultItems[0], POS5, ARTICLE3_PACK1_ID, 3, EXP3, CARRIER2, BATCH1, 112);
            CheckResultItem(result.ResultItems[1], POS5, ARTICLE3_PACK1_ID, 4, EXP1, CARRIER2, BATCH2, 112);
            CheckHistoryResultJson(result.History, nameof(PlanMakerTest_Under6ThenOneBatch_7));
        }
        
        [Fact]
        public void PlanMakerTest_Under6ThenOneBatch_14()
        {
            // arrange
            Mock<IOnStore> onStore = PrepareOnStore();
            Mock<ICache<ArticlePacking, int>> packingCache = PreparePackingCache();
            Mock<ICache<ArticleModel, int>> articleCache = PrepareArticleCache();
            Mock<ICache<Manufact, string>> manufactCache = PrepareManufactCache(ManufactSettings.DispatchAlgoritmEnum.Under6ThenOneBatchFirst);
            Mock<IBookings> bookings = EmptyBookings();


            // act
            var planMaker = new PlanMaker(packingCache.Object, articleCache.Object, manufactCache.Object, PrepareSectionModelCache().Object, onStore.Object, bookings.Object);
            var dirModel = new DirectionModel()
            {
                Items = new List<DirectionItem>() { new DirectionItem() {
                ArtPackID = ARTICLE3_PACK1_ID,
                ArticleCode = nameof(ARTICLE3),
                Quantity = 14,
                DocPosition = 113
            } }
            };
            var prefixSetings = GetPrefixSettings_AllFromOneSection();

            var result = planMaker.CreatePlan(dirModel, prefixSetings, DateTime.Today);

            // assert
            Assert.True(result.Result);
            Assert.Equal(3, result.ResultItems.Count);
            CheckResultItem(result.ResultItems[0], POS5, ARTICLE3_PACK1_ID, 3, EXP3, CARRIER2, BATCH1, 113);
            CheckResultItem(result.ResultItems[1], POS5, ARTICLE3_PACK1_ID, 10, EXP1, CARRIER2, BATCH2, 113);
            CheckResultItem(result.ResultItems[2], POS5, ARTICLE3_PACK1_ID, 1, EXP2, CARRIER2, BATCH1, 113);
            CheckHistoryResultJson(result.History, nameof(PlanMakerTest_Under6ThenOneBatch_14));
        }

        [Fact]
        public void PlanMakerTest_Under6ThenOneBatch_4()
        {
            // arrange
            Mock<IOnStore> onStore = PrepareOnStore();
            Mock<ICache<ArticlePacking, int>> packingCache = PreparePackingCache();
            Mock<ICache<ArticleModel, int>> articleCache = PrepareArticleCache();
            Mock<ICache<Manufact, string>> manufactCache = PrepareManufactCache(ManufactSettings.DispatchAlgoritmEnum.Under6ThenOneBatchFirst);
            Mock<IBookings> bookings = EmptyBookings();


            // act
            var planMaker = new PlanMaker(packingCache.Object, articleCache.Object, manufactCache.Object, PrepareSectionModelCache().Object, onStore.Object, bookings.Object);
            var dirModel = new DirectionModel()
            {
                Items = new List<DirectionItem>() { new DirectionItem() {
                ArtPackID = ARTICLE3_PACK1_ID,
                ArticleCode = nameof(ARTICLE3),
                Quantity = 4,
                DocPosition = 114
            } }
            };
            var prefixSetings = GetPrefixSettings_AllFromOneSection();

            var result = planMaker.CreatePlan(dirModel, prefixSetings, new DateTime(2019, 5, 1));

            // assert
            Assert.True(result.Result);
            Assert.Single(result.ResultItems);
            CheckResultItem(result.ResultItems[0], POS5, ARTICLE3_PACK1_ID, 4, EXP1, CARRIER2, BATCH2, 114);
            CheckHistoryResultJson(result.History, nameof(PlanMakerTest_Under6ThenOneBatch_4));
        }

        [Fact]
        public void PlanMakerTest_Under6ThenOneBatch_16()
        {
            // arrange
            Mock<IOnStore> onStore = PrepareOnStore();
            Mock<ICache<ArticlePacking, int>> packingCache = PreparePackingCache();
            Mock<ICache<ArticleModel, int>> articleCache = PrepareArticleCache();
            Mock<ICache<Manufact, string>> manufactCache = PrepareManufactCache(ManufactSettings.DispatchAlgoritmEnum.Under6ThenOneBatchFirst);
            Mock<IBookings> bookings = EmptyBookings();


            // act
            var planMaker = new PlanMaker(packingCache.Object, articleCache.Object, manufactCache.Object, PrepareSectionModelCache().Object, onStore.Object, bookings.Object);
            var dirModel = new DirectionModel()
            {
                Items = new List<DirectionItem>() { new DirectionItem() {
                ArtPackID = ARTICLE3_PACK1_ID,
                ArticleCode = nameof(ARTICLE3),
                Quantity = 16,
                DocPosition = 115
            } }
            };
            var prefixSetings = GetPrefixSettings_AllFromOneSection();

            var result = planMaker.CreatePlan(dirModel, prefixSetings, new DateTime(2020, 1, 1));

            // assert
            Assert.False(result.Result);
            Assert.Null(result.ResultItems);
            CheckHistoryResultJson(result.History, nameof(PlanMakerTest_Under6ThenOneBatch_16));
        }
    }
}

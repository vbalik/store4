﻿using Moq;
using S4.StoreCore.Interfaces;
using S4.Picking;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using S4.Core.Cache;
using S4.Entities;
using S4.Entities.Models;
using S4.Entities.Settings;
using Newtonsoft.Json;
using S4.DAL.Interfaces;

namespace S4.Picking.Tests
{
    public class PlanMakerTests : PlanMakerTestsBase
    {

        [Fact]
        public void PlanMakerTest_OnStoreCache()
        {
            // arrange
            Mock<IOnStore> onStore = PrepareOnStore();
            Mock<ICache<ArticlePacking, int>> packingCache = PreparePackingCache();
            Mock<ICache<ArticleModel, int>> articleCache = PrepareArticleCache();
            Mock<ICache<Manufact, string>> manufactCache = PrepareManufactCache(ManufactSettings.DispatchAlgoritmEnum.ByExpiration);
            Mock<IBookings> bookings = EmptyBookings();


            // act
            var planMaker = new PlanMaker(packingCache.Object, articleCache.Object, manufactCache.Object, PrepareSectionModelCache().Object, onStore.Object, bookings.Object);
            planMaker.SaveOnStoreToHistory = true;
            var dirModel = new DirectionModel()
            {
                Items = new List<DirectionItem>() {
                    new DirectionItem() {
                        ArtPackID = ARTICLE1_PACK1_ID, ArticleCode = nameof(ARTICLE1), Quantity = 15
                    },
                    new DirectionItem() {
                        ArtPackID = ARTICLE1_PACK1_ID, ArticleCode = nameof(ARTICLE1), Quantity = 1
                    }
                }
            };
            var prefixSetings = GetPrefixSettings_AllFromOneSection();
            var result = planMaker.CreatePlan(dirModel, prefixSetings, DateTime.Today);

            // assert
            Assert.False(result.Result);
            CheckHistoryResultJson(result.History, nameof(PlanMakerTest_OnStoreCache));
        }


        [Fact]
        public void PlanMakerTest_FilterExpired()
        {
            Mock<IOnStore> onStore = new Mock<IOnStore>();
            onStore.Setup(m => m.ByArticle(ARTICLE1, ResultValidityEnum.NotValid, true, null)).Returns(new List<OnStoreItemFull>() {
                new OnStoreItemFull() { ArtPackID = ARTICLE1_PACK1_ID, ArticleID = ARTICLE1, PositionID = POS1, CarrierNum = CARRIER1, BatchNum = BATCH1, Quantity = 5, ExpirationDate = new DateTime(1900, 1, 1), Position = new Position() { PosCode = nameof(POS1), PosStatus = Entities.Position.POS_STATUS_OK } },
                new OnStoreItemFull() { ArtPackID = ARTICLE1_PACK1_ID, ArticleID = ARTICLE1, PositionID = POS2, CarrierNum = CARRIER1, BatchNum = BATCH2, Quantity = 10, ExpirationDate = EXP2, Position = new Position() { PosCode = nameof(POS2), PosStatus = Entities.Position.POS_STATUS_OK } },
            });
            Mock<ICache<ArticlePacking, int>> packingCache = PreparePackingCache();
            Mock<ICache<ArticleModel, int>> articleCache = PrepareArticleCache();
            Mock<ICache<Manufact, string>> manufactCache = PrepareManufactCache(ManufactSettings.DispatchAlgoritmEnum.ByExpiration);
            Mock<IBookings> bookings = EmptyBookings();


            // act
            var planMaker = new PlanMaker(packingCache.Object, articleCache.Object, manufactCache.Object, PrepareSectionModelCache().Object, onStore.Object, bookings.Object);
            var dirModel = new DirectionModel() { Items = new List<DirectionItem>() { new DirectionItem() { ArtPackID = ARTICLE1_PACK1_ID, ArticleCode = nameof(ARTICLE1), Quantity = 1 } } };
            var prefixSetings = GetPrefixSettings_AllFromOneSection();

            var result = planMaker.CreatePlan(dirModel, prefixSetings, DateTime.Today);

            // assert
            Assert.True(result.Result);
            CheckHistoryResultJson(result.History, nameof(PlanMakerTest_FilterExpired));
        }


        [Fact]
        public void PlanMakerTest_SaveOnStoreToHistory()
        {
            // arrange
            Mock<IOnStore> onStore = PrepareOnStore();
            Mock<ICache<ArticlePacking, int>> packingCache = PreparePackingCache();
            Mock<ICache<ArticleModel, int>> articleCache = PrepareArticleCache();
            Mock<ICache<Manufact, string>> manufactCache = PrepareManufactCache(ManufactSettings.DispatchAlgoritmEnum.ByExpiration);
            Mock<IBookings> bookings = EmptyBookings();


            // act
            var planMaker = new PlanMaker(packingCache.Object, articleCache.Object, manufactCache.Object, PrepareSectionModelCache().Object, onStore.Object, bookings.Object);
            planMaker.SaveOnStoreToHistory = true;
            var dirModel = new DirectionModel() { Items = new List<DirectionItem>() { new DirectionItem() { ArtPackID = ARTICLE1_PACK1_ID, ArticleCode = nameof(ARTICLE1), Quantity = 1 } } };
            var prefixSetings = GetPrefixSettings_AllFromOneSection();

            var result = planMaker.CreatePlan(dirModel, prefixSetings, DateTime.Today);

            // assert
            CheckHistoryResultJson(result.History, nameof(PlanMakerTest_SaveOnStoreToHistory));
        }


        [Fact]
        public void PlanMakerTest_NotFound()
        {
            // arrange
            Mock<IOnStore> onStore = PrepareOnStore();
            Mock<ICache<ArticlePacking, int>> packingCache = PreparePackingCache();
            Mock<ICache<ArticleModel, int>> articleCache = PrepareArticleCache();
            Mock<ICache<Manufact, string>> manufactCache = PrepareManufactCache(ManufactSettings.DispatchAlgoritmEnum.ByExpiration);
            Mock<IBookings> bookings = EmptyBookings();


            // act
            var planMaker = new PlanMaker(packingCache.Object, articleCache.Object, manufactCache.Object, PrepareSectionModelCache().Object, onStore.Object, bookings.Object);
            var dirModel = new DirectionModel()
            {
                Items = new List<DirectionItem>()
                {
                    new DirectionItem() { ArtPackID = ARTICLE1_PACK1_ID, ArticleCode = nameof(ARTICLE1), Quantity = 100 },
                }
            };
            var prefixSetings = GetPrefixSettings_AllFromOneSection();

            var result = planMaker.CreatePlan(dirModel, prefixSetings, DateTime.Today);

            // assert
            Assert.False(result.Result);
            Assert.NotEmpty(result.NotFound);
            Assert.Equal(ARTICLE1_PACK1_ID, result.NotFound[0].ArtPackID);
            Assert.Equal("ARTICLE1 - ", result.NotFound[0].ArticleInfo);
            Assert.Equal(SEC_1, result.NotFound[0].SecID);
            Assert.Equal(100, result.NotFound[0].Quantity);
            Assert.Equal(ARTICLE1_PACK1_ID, result.NotFound[1].ArtPackID);
            Assert.Equal("ARTICLE1 - ", result.NotFound[1].ArticleInfo);
            Assert.Equal(SEC_2, result.NotFound[1].SecID);
            Assert.Equal(100, result.NotFound[1].Quantity);
            CheckHistoryResultJson(result.History, nameof(PlanMakerTest_NotFound));
        }


        [Fact]
        public void PlanMakerTest_OneFromOneSection()
        {
            // arrange
            Mock<IOnStore> onStore = PrepareOnStore();
            Mock<ICache<ArticlePacking, int>> packingCache = PreparePackingCache();
            Mock<ICache<ArticleModel, int>> articleCache = PrepareArticleCache();
            Mock<ICache<Manufact, string>> manufactCache = PrepareManufactCache(ManufactSettings.DispatchAlgoritmEnum.ByExpiration);
            Mock<IBookings> bookings = EmptyBookings();


            var sectionModelCache = new Mock<ICache<SectionModel, string>>();
            sectionModelCache.Setup(m => m.GetItem(SEC_1)).Returns(new SectionModel() { SectID = SEC_1, PositionIDs = new HashSet<int>() { POS1 } });
            sectionModelCache.Setup(m => m.GetItem(SEC_2)).Returns(new SectionModel() { SectID = SEC_2, PositionIDs = new HashSet<int>() { POS2 } });

            // act
            var planMaker = new PlanMaker(packingCache.Object, articleCache.Object, manufactCache.Object, sectionModelCache.Object, onStore.Object, bookings.Object);
            var dirModel = new DirectionModel()
            {
                Items = new List<DirectionItem>()
                {
                    new DirectionItem() { ArtPackID = ARTICLE1_PACK1_ID, ArticleCode = nameof(ARTICLE1), Quantity = 10, DocPosition = 88 },
                    new DirectionItem() { ArtPackID = ARTICLE2_PACK1_ID, ArticleCode = nameof(ARTICLE2), Quantity = 5, DocPosition = 99 },
                }
            };
            var prefixSetings = GetPrefixSettings_OneFromOneSection();

            var result = planMaker.CreatePlan(dirModel, prefixSetings, DateTime.Today);

            // assert
            Assert.True(result.Result);
            Assert.Equal(2, result.ResultItems.Count);
            CheckResultItem(result.ResultItems[0], POS2, ARTICLE1_PACK1_ID, 10, EXP1, CARRIER1, BATCH2, 88);
            CheckResultItem(result.ResultItems[1], POS1, ARTICLE2_PACK1_ID, 5, EXP2, CARRIER1, BATCH1, 99);
            CheckHistoryResultJson(result.History, nameof(PlanMakerTest_OneFromOneSection));
        }


        [Fact]
        public void PlanMakerTest_DirectSection()
        {
            // arrange
            Mock<IOnStore> onStore = PrepareOnStore();
            Mock<ICache<ArticlePacking, int>> packingCache = PreparePackingCache();
            Mock<ICache<ArticleModel, int>> articleCache = PrepareArticleCache();
            Mock<ICache<Manufact, string>> manufactCache = PrepareManufactCache(ManufactSettings.DispatchAlgoritmEnum.ByExpiration);
            Mock<IBookings> bookings = EmptyBookings();


            // act
            var planMaker = new PlanMaker(packingCache.Object, articleCache.Object, manufactCache.Object, PrepareSectionModelCache().Object, onStore.Object, bookings.Object);
            var dirModel = new DirectionModel()
            {
                Items = new List<DirectionItem>()
                {
                    new DirectionItem() { ArtPackID = ARTICLE1_PACK1_ID, ArticleCode = nameof(ARTICLE1), Quantity = 10 },
                }
            };
            dirModel.XtraData[Direction.XTRA_DISPATCH_DIRECT_SECTION_LIST, 0] = SEC_DIRECT;
            var prefixSetings = GetPrefixSettings_OneFromOneSection();

            var result = planMaker.CreatePlan(dirModel, prefixSetings, DateTime.Today);

            // assert
            Assert.False(result.Result);
            CheckHistoryResultJson(result.History, nameof(PlanMakerTest_DirectSection));
        }


        [Fact]
        public void PlanMakerTest_AlternativeSection()
        {
            // arrange
            Mock<IOnStore> onStore = PrepareOnStore();
            Mock<ICache<ArticlePacking, int>> packingCache = PreparePackingCache();
            Mock<ICache<ArticleModel, int>> articleCache = PrepareArticleCache();
            Mock<ICache<Manufact, string>> manufactCache = PrepareManufactCache(ManufactSettings.DispatchAlgoritmEnum.ByExpiration);
            Mock<IBookings> bookings = EmptyBookings();


            // act
            var planMaker = new PlanMaker(packingCache.Object, articleCache.Object, manufactCache.Object, PrepareSectionModelCache().Object, onStore.Object, bookings.Object);
            var dirModel = new DirectionModel()
            {
                Items = new List<DirectionItem>()
                {
                    new DirectionItem() { ArtPackID = ARTICLE1_PACK1_ID, ArticleCode = nameof(ARTICLE1), Quantity = 1000, DocPosition = 99 },
                }
            };
            dirModel.XtraData[Direction.XTRA_DISPATCH_ALTER_SECTION, 99] = SEC_ALTERNATIVE;
            var prefixSetings = GetPrefixSettings_OneFromOneSection();

            var result = planMaker.CreatePlan(dirModel, prefixSetings, DateTime.Today);

            // assert
            Assert.True(result.Result);
            Assert.Single(result.ResultItems);
            CheckResultItem(result.ResultItems[0], POS3, ARTICLE1_PACK1_ID, 1000, EXP1, CARRIER1, BATCH2, 99);
            CheckHistoryResultJson(result.History, nameof(PlanMakerTest_AlternativeSection));
        }


        [Fact]
        public void WithoutBookedLots_WithRest_OK()
        {
            var onStore = new List<OnStoreItemFull>() 
            { 
                new OnStoreItemFull() { StoMoveLotID = 1, Quantity = 2 },
                new OnStoreItemFull() { StoMoveLotID = 2, Quantity = 10 },
                new OnStoreItemFull() { StoMoveLotID = 1, Quantity = 4 },
            };

            var validBookings = new List<BookingInfo>() 
            { 
                new BookingInfo() { StoMoveLotID = 1, RequiredQuantity = 3 },
                new BookingInfo() { StoMoveLotID = 10, RequiredQuantity = 10 },
            };

            var res = PlanMaker.WithoutBookedLots(onStore, validBookings);

            Assert.Equal(2, res.Count);
            Assert.Equal(2, res[0].StoMoveLotID);
            Assert.Equal(10, res[0].Quantity);
            Assert.Equal(1, res[1].StoMoveLotID);
            Assert.Equal(3, res[1].Quantity);
        }


        [Fact]
        public void WithoutBookedLots_NotEnough_OK()
        {
            var onStore = new List<OnStoreItemFull>()
            {
                new OnStoreItemFull() { StoMoveLotID = 1, Quantity = 2 },
                new OnStoreItemFull() { StoMoveLotID = 2, Quantity = 10 },
                new OnStoreItemFull() { StoMoveLotID = 1, Quantity = 4 },
            };

            var validBookings = new List<BookingInfo>()
            {
                new BookingInfo() { StoMoveLotID = 1, RequiredQuantity = 30 },
                new BookingInfo() { StoMoveLotID = 10, RequiredQuantity = 10 },
            };

            var res = PlanMaker.WithoutBookedLots(onStore, validBookings);

            Assert.Single(res);
            Assert.Equal(2, res[0].StoMoveLotID);
            Assert.Equal(10, res[0].Quantity);
        }
    }
}

using System;
using Xunit;

namespace S4.SharedLib.Tests
{
    public class DocQRCodeTests
    {
        [Fact]
        public void DocQRCode_Formated()
        {
            var qr = new DocQRCode() { DocType = DocQRCode.DocTypeEnum.DL, DocID = "123", DocInfo = "DL/12/34" };
            Assert.Equal("DOCINFO*TYPE:DL*ID:123*INFO:DL/12/34", qr.Formated);
        }


        [Theory]
        [InlineData(null, false)]
        [InlineData("xxx", false)]
        [InlineData("DOCINFO*", false)]
        [InlineData("DOCINFO*TYPE:DL", true)]
        [InlineData("DOCINFO*TYPE:XXX", false)]
        public void DocQRCode_TryParse_Checks(string value, bool result)
        {
            var (parsed, _) = DocQRCode.TryParse(value);
            Assert.Equal(result, parsed);
        }


        [Theory]
        [InlineData("DOCINFO*TYPE:DL", DocQRCode.DocTypeEnum.DL, null, null)]
        [InlineData("DOCINFO*TYPE:DL*ID:123", DocQRCode.DocTypeEnum.DL, "123", null)]
        [InlineData("DOCINFO*TYPE:DL*ID:123*INFO:DL/12/34", DocQRCode.DocTypeEnum.DL, "123", "DL/12/34")]
        public void DocQRCode_TryParse_Values(string value, DocQRCode.DocTypeEnum docType, string docID, string docInfo)
        {
            var (parsed, result) = DocQRCode.TryParse(value);
            Assert.True(parsed);
            Assert.Equal(docType, result.DocType);
            Assert.Equal(docID, result.DocID);
            Assert.Equal(docInfo, result.DocInfo);
        }
    }
}

using S4.SharedLib.Security;
using System;
using Xunit;

namespace S4.SharedLib.Tests
{
    public class RemoteAccessAuthorizationTests
    {
        
        [Theory]
        [InlineData("123456789", "2010-01-01T00:00:00.0000000", "769A7936522653ECF9A1DCC6EDDEFF14C12A139E7351DD8847696ACFFE9DE50F")]
        public void RemoteAccessAuthorization_CreateKey(string value, string dateTimeString, string key)
        {
            var dateTime = DateTime.ParseExact(dateTimeString, "O", System.Globalization.CultureInfo.InvariantCulture);
            var key2 = RemoteAccessAuthorization.CreateKey(value, dateTime);
            Assert.Equal(key, key2);
        }


        [Fact]
        public void RemoteAccessAuthorization_CheckKey()
        {
            Assert.True(RemoteAccessAuthorization.CheckKey("123456789", new DateTime(2020, 1, 2, 3, 4, 5), "234ED8CD8E495FDF85F5DAAFBDF8871B67379608B58B8B1165FC5AA4E7B24483"));
        }
    }
}

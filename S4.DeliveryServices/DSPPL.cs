﻿using PPLWebService;
using S4.DeliveryServices.DPDClients;
using S4.Entities;
using S4.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.DeliveryServices
{
    public class DSPPL : DeliveryServiceBase, IDeliveryService
    {
        // settings
        private const string _CUSTID = "custId";
        private const string _PASSWORD = "password";
        private const string _USER_NAME = "userName";
        private const string _ROUTE_CODE = "routeCodePPL";

        private (int custId, string password, string userName, string routeCodePPL)? _settings;

        public DeliveryDirection.DeliveryProviderEnum DeliveryProvider => DeliveryDirection.DeliveryProviderEnum.PPL;

        public DSPPL()
        {
            _InitSettings();
        }

        public DSPPL(string connectionString) : base(connectionString)
        {
            _InitSettings();
        }

        private void _InitSettings()
        {
            var setting = Settings.Providers.Where(i => i.ProviderName == DeliveryProvider.ToString()).First();
            if (setting != null)
                _settings = (int.Parse(setting.Properties[_CUSTID]), setting.Properties[_PASSWORD], setting.Properties[_USER_NAME], setting.Properties[_ROUTE_CODE]);
        }

        public Task CreateManifest(DeliveryManifest manifest)
        {
            throw new NotImplementedException();
        }

        public async Task CreateShipment(DirectionModel directionModel, Partner partner, PartnerAddress address)
        {
            SendToLog(this, $"Create shipment; ShipmentReferenceID: {directionModel.DirectionID}; PartnerID: {partner.PartnerID}; PartnerName: {partner.PartnerName}; PartAddrID: {address.PartAddrID}; AddrName: {address.AddrName}", LogLevelEnum.Info);

            var parcelsCount = DataParserHelper.GetParcelsCount(directionModel);
            
            try
            {
                // check API alive
                if (!await new PPLServiceClient().IsHealtly())
                {
                    SendToLog(this, "PPL Service API is unavailable.", LogLevelEnum.Warn);
                    return;
                }

                var client = new PPLServiceClient((int)_settings?.custId, _settings?.password, _settings?.userName);

                var citiesRouting = await client.GetCitiesRoutingAsync(await client.GetAuthData(), new PPLWebService.CitiesRoutingFilter
                {
                    CountryCode = DataParserHelper.GetCountry(directionModel).ToString(),
                    ZipCode = address.AddrLine_2.Replace(" ", ""),
                    Street = address.AddrLine_1
                });


                // create packages
                var packages = new List<PPLWebService.MyApiPackageIn>();

                for (int i = 0; i < parcelsCount; i++)
                {
                    var pack = new PackageFactory().CreatePackage(parcelsCount, i + 1, directionModel, partner, address, citiesRouting, _settings?.routeCodePPL);
                    SendToLog(this, $"DirectionID: {directionModel.DirectionID}, PartnerName: {partner.PartnerName}, PackProductType: {pack.PackProductType} ---> new PackNumber: {pack.PackNumber}", LogLevelEnum.Info);
                    packages.Add(pack);
                }
                
                /*Save Request */
                var saveRequest = DataParserHelper.SaveDeliveryXML(DataParserHelper.SaveXMLEnum.REQUEST, DeliveryDirection.DeliveryProviderEnum.PPL, packages, directionModel.DirectionID.ToString(), SavePathRequestResponse);
                SendToLog(this, saveRequest.Item2, saveRequest.Item1 ? LogLevelEnum.Info : LogLevelEnum.Warn);

                var result = await client.CreatePackagesAsync(await client.GetAuthData(), string.Empty, packages.ToArray(), new PPLWebService.ReturnChannelSettings());

                /*Save Response */
                var saveResponse = DataParserHelper.SaveDeliveryXML(DataParserHelper.SaveXMLEnum.RESPONSE, DeliveryDirection.DeliveryProviderEnum.PPL, result, directionModel.DirectionID.ToString(), SavePathRequestResponse);
                SendToLog(this, saveResponse.Item2, saveResponse.Item1 ? LogLevelEnum.Info : LogLevelEnum.Warn);
                
                var errors = result.ResultData.Where(_ => _.Code != "0");
                if (errors.Count() > 0)
                {
                    foreach (var error in errors)
                    {
                        var msg = $"Code: {error.Code}; Text: {error.Message};";
                        SendToLog(this, msg, LogLevelEnum.Warn);
                        InsertShipmentError(this, directionModel.DirectionID, msg);
                    }

                    return;
                }

                SendToLog(this, $"Create shipment done; ShipmentReferenceID: {directionModel.DirectionID}; " +
                    $"TransactionID: {result.ResultData[0].ItemKey};",
                    LogLevelEnum.Info);

                //save to db
                if (!InsertShipment(this, directionModel.DirectionID, directionModel.DirectionID.ToString(), packages.Select(_ => new Tuple<int, string>((int)_.PackageSet.PackageInSetNr, _.PackNumber)).ToArray(), out string errorText))
                {
                    SendToLog(this, errorText, LogLevelEnum.Warn);
                }

                //add xtra data
                using (var db = Core.Data.ConnectionHelper.GetDB())
                {
                    var citiesRoutingData = citiesRouting.ResultData.FirstOrDefault();
                    var direction = new DAL.DirectionDAL().GetDirectionAllData(directionModel.DirectionID, db);

                    direction.XtraData[Direction.XTRA_DELIVERY_ROUTE_CODE] = citiesRoutingData?.RouteCode;
                    direction.XtraData[Direction.XTRA_DELIVERY_DEPO_CODE] = citiesRoutingData?.DepoCode;
                    direction.XtraData[Direction.XTRA_DELIVERY_ZIP_CODE] = citiesRoutingData?.ZipCode;
                    direction.XtraData[Direction.XTRA_DELIVERY_ROUTING_SQUARE] = citiesRoutingData?.Highlighted ?? false;

                    //save pack numbers, truck URLs
                    var deliveryDirection = new DAL.DeliveryDirectionDAL().GetDeliveryDirectionAllData(direction.DirectionID, db);
                    var packNumbers = deliveryDirection.Items.Select(x => x.ParcelRefNumber).ToArray();

                    var position = 0;
                    foreach (var packNumber in packNumbers)
                    {
                        direction.XtraData[Direction.XTRA_DELIVERY_PACK_NUMBER, position] = packNumber;
                        position++;
                    }
                                        
                    direction.XtraData.Save(db, direction.DirectionID);
                    SendToLog(this, $"Save XtraData DirectionID: {direction.DirectionID}", LogLevelEnum.Info);
                }

                await GetLabel(directionModel.DirectionID.ToString());
            }
            catch (DeliveryException de)
            {
                SendToLog(this, de.Message, LogLevelEnum.Warn, de);
                Console.WriteLine(de.ToString());
            }
            catch (Exception e)
            {
                SendToLog(this, e.Message, LogLevelEnum.Error, e);
                Console.WriteLine(e.ToString());
            }
        }

        public Task DeleteShipment(string shipmentReference)
        {
            //rozhraní PPL zatím neposkytuje
            return Task.CompletedTask;
        }

        public Task<Tuple<bool, string>> GetLabel(string shipmentReference)
        {
            InvokeLabelEvent(this, shipmentReference, null);
            return Task.FromResult(Tuple.Create(true, string.Empty));
        }

        public Task PrintManifest(string manifestReference)
        {
            throw new NotImplementedException();
        }

        public const string SHIP_TIME = "SHIP_TIME";
        public const string PACK_NUMBER = "PACK_NUMBER";
        public const string DELIVERY_TIME = "DELIVERY_TIME";
        public const string TRACK_URL = "TRACK_URL";
        public const string SCAN_ITEM = "SCAN_ITEM";

        public const string TRACK_URL_PPL = "https://klient.ppl.cz";

        public async Task GetShipmentStatus(string shipmentReference)
        {
            var client = new PPLServiceClient((int)_settings?.custId, _settings?.password, _settings?.userName);
            string[] packNumbers;

            try
            {
                using (var db = Core.Data.ConnectionHelper.GetDB())
                {
                    var direction = new DAL.DeliveryDirectionDAL().GetDeliveryDirectionAllData(int.Parse(shipmentReference), db);
                    packNumbers = direction.Items.Select(_ => _.ParcelRefNumber).ToArray();
                }

                var filter = new PackagesFilter
                {
                    PackNumbers = packNumbers
                };

                var result = await client.GetPackagesAsync(await client.GetAuthData(), filter);

                var content = new List<Entities.Models.LabeledValue>();

                foreach (var pack in result.ResultData)
                {
                    content.Add(new Entities.Models.LabeledValue(SHIP_TIME, "Převzetí zásilky", pack.TakeDate.HasValue ? pack.TakeDate.Value.ToString("dd.MM.yyyy HH:mm") : "" ));
                    content.Add(new Entities.Models.LabeledValue(PACK_NUMBER, "Číslo balíku", pack.PackNumber));
                    content.Add(new Entities.Models.LabeledValue(TRACK_URL, "Sledování zásilky", TRACK_URL_PPL));

                    content.Add(new Entities.Models.LabeledValue() { Key = BEGIN_OF_GROUP });

                    foreach (var stat in pack.PackageStatuses)
                    {
                        content.Add(new Entities.Models.LabeledValue(SCAN_ITEM, "Stav", $"<div class='col-sm-10'>{stat.StatusName}</div><div class='col-sm-2'>{stat.StatusDate.ToString("dd.MM.yyyy HH:mm")}</div>"));
                    }

                    content.Add(new Entities.Models.LabeledValue() { Key = END_OF_GROUP });
                }

                OnGetShipmentStatus(this, DeliveryDirection.DeliveryProviderEnum.PPL, content.ToArray());

                SendToLog(this, $"Get Shipment Status done; TransactionID:", LogLevelEnum.Info);
            }
            catch (DeliveryException dex)
            {
                SendToLog(this, dex.Message, LogLevelEnum.Warn, dex);
                Console.WriteLine(dex.ToString());

            }
            catch (Exception e)
            {
                SendToLog(this, e.Message, LogLevelEnum.Error, e);
                Console.WriteLine(e.ToString());
            }
        }
    }

    internal class PackageFactory
    {
        public PPLWebService.MyApiPackageIn CreatePackage(int packCount,
            int packOrderNum,
            DirectionModel directionModel,
            Partner partner,
            PartnerAddress address,
            GetCitiesRoutingResult citiesRouting,
            string routeCodePPL)
        {
            const bool isParcelShop = false;
            //TODO parcel shop

            PPLWebService.PaymentInfo paymentInfo = null;
            var paymentType = DataParserHelper.GetPaymentType(directionModel);

            if (paymentType == DeliveryDirection.DeliveryPaymentEnum.CAOD || paymentType == DeliveryDirection.DeliveryPaymentEnum.CHOD)
            {
                // only first package has price others does not
                if (packOrderNum == 1)
                {
                    var codPrice = DataParserHelper.GetCashOnDelivery(directionModel);
                    if (codPrice < 0) throw new Exception("Price can not be less than zero.");

                    // Částka dobírky. Částka je oddělena tečkou. Pouze kladné číslo. Pokud je v CZK a země CZ, pak zaokrouhlená na celé částky.
                    if (DataParserHelper.GetCurrency(directionModel).ToString() == DeliveryDirection.DeliveryCurrencyEnum.CZK.ToString()
                        && DataParserHelper.GetCountry(directionModel).ToString() == DeliveryDirection.DeliveryCountryEnum.CZ.ToString())
                    {
                        codPrice = Math.Round(codPrice, 0);
                    }

                    paymentInfo = new PPLWebService.PaymentInfo
                    {
                        CodCurrency = DataParserHelper.GetCurrency(directionModel).ToString(),
                        CodPrice = codPrice,
                        CodVarSym = DataParserHelper.GetPaymentReference(directionModel).ToString()
                    };
                }
                else
                {
                    paymentInfo = new PPLWebService.PaymentInfo
                    {
                        CodCurrency = DataParserHelper.GetCurrency(directionModel).ToString(),
                        CodPrice = 0,
                        CodVarSym = DataParserHelper.GetPaymentReference(directionModel).ToString()
                    };
                }
            }


            PPLWebService.WeightedPackageInfoIn weightedPackage = null;

            var weight = DataParserHelper.GetWeight(directionModel, packOrderNum);
            if (weight != null)
            {
                weightedPackage = new PPLWebService.WeightedPackageInfoIn
                {
                    Weight = weight,
                    Routes = new PPLWebService.Route[]
                    {
                        new PPLWebService.Route {
                            RouteType = RouteType.IN,
                            RouteCode = routeCodePPL
                        },
                        new PPLWebService.Route {
                            RouteType = RouteType.OUT,
                            RouteCode = citiesRouting.ResultData.FirstOrDefault().RouteCode
                        }
                    }
                };
            }

            // custom package number
            var packageExtNums = new List<PPLWebService.MyApiPackageExtNum>
            {
                new PPLWebService.MyApiPackageExtNum {
                    Code = PackExtNumberType.Externi,
                    ExtNumber = $"{directionModel.DirectionID}_{packOrderNum}"
                }
            };

            var productType = GetProductType(directionModel, partner);

            return new PPLWebService.MyApiPackageIn
            {
                PackNumber = GetPackNumber(productType).ToString(),
                PackProductType = productType,
                Recipient = new PPLWebService.MyApiPackageInRecipient
                {
                    City = address.AddrCity,
                    Contact = isParcelShop ? partner.PartnerName : string.Empty,
                    Country = DataParserHelper.GetCountry(directionModel).ToString(),
                    Email = string.Empty,
                    Name = address.AddrName,
                    Phone = address.AddrPhone,
                    Street = address.AddrLine_1,
                    ZipCode = address.AddrLine_2.Replace(" ", "")
                },
                SpecDelivery = GetParcelShop(directionModel),
                PaymentInfo = paymentInfo,
                PackagesExtNums = packageExtNums.ToArray(),
                PackageServices = GetServiceType(DataParserHelper.GetTransportationType(directionModel)),
                Flags = (new List<PPLWebService.MyApiFlag>() {
                                            new PPLWebService.MyApiFlag { Code = "SL", Value = true }
                                        }).ToArray(),
                PackageSet = new PPLWebService.MyApiPackageSet
                {
                    PackageInSetNr = packOrderNum,
                    PackagesInSet = packCount
                },
                WeightedPackageInfo = weightedPackage
            };
        }

        private PPLWebService.MyApiPackageInServices[] GetServiceType(string serviceType)
        {
            string type = null;

            switch (serviceType)
            {
                case TransportationType.VecerniDoruceni:
                    type = PackageInServicesType.VecerniDoruceni;
                    break;
            }

            var packages = new List<PPLWebService.MyApiPackageInServices>();

            if (!string.IsNullOrEmpty(type))
            {
                packages.Add(new PPLWebService.MyApiPackageInServices
                {
                    SvcCode = type
                });
            }

            return packages.ToArray();
        }

        private PPLWebService.SpecDeliveryInfo GetParcelShop(DirectionModel directionModel)
        {
            if (DataParserHelper.GetParcelShop(directionModel, out string parcelShopCode))
            {
                return new PPLWebService.SpecDeliveryInfo()
                {
                    ParcelShopCode = parcelShopCode,
                };
            }

            return null;
        }

        private long GetPackNumber(string productType)
        {
            return new DSPPLSeries().GetParcelNumber(productType);
        }

        private string GetProductType(DirectionModel directionModel, Partner partner)
        {
            string productType;

            // is ICO 
            if (!string.IsNullOrEmpty(partner.PartnerOrgIdentNumber))
            {
                productType = ProductType.PPLParcel_CZ_Business;
            }
            else
            {
                productType = ProductType.PPLParcel_CZ_Private;
            }

            // is COD
            var paymentType = DataParserHelper.GetPaymentType(directionModel);
            if (paymentType == DeliveryDirection.DeliveryPaymentEnum.CAOD
                || paymentType == DeliveryDirection.DeliveryPaymentEnum.CHOD
                && productType == ProductType.PPLParcel_CZ_Business)
            {
                productType = ProductType.PPLParcel_CZ_Business_dobírka;
            }
            else if (paymentType == DeliveryDirection.DeliveryPaymentEnum.CAOD || paymentType == DeliveryDirection.DeliveryPaymentEnum.CHOD)
            {
                productType = ProductType.PPLParcel_CZ_Private_dobírka;
            }

            return productType;
        }
    }

    public sealed class PackageInServicesType
    {
        public const string Dobirka = "COD";
        public const string VecerniDoruceni = "ED";
        public const string PlatbaVHotovostiPrijemcem = "EXW";
        public const string ADR = "ADR";
        public const string Pojisteni = "INSR";
        public const string OsobniOdber = "PPCK";
        public const string DopoledniBalik = "MD";
        public const string AgeCheck15 = "A15";
        public const string AgeCheck18 = "A18";
    }

    public sealed class GetPackagesType
    {
        public const string Externi = "B2CO";
        public const string Zakaznicke = "CUST";
        public const string SobotniSvoz = "SOSV";
        public const string ZdravotnickeProstredky = "ZDRP";
    }

    public sealed class ProductType
    {
        public const string PPLParcel_CZ_Business = "1";
        public const string PPLParcel_CZ_Business_dobírka = "2";
        public const string PPLParcel_CZ_Private = "13";
        public const string PPLParcel_CZ_Private_dobírka = "14";
    }

    public sealed class PackExtNumberType
    {
        public const string Externi = "B2CO";
        public const string Zakaznicke = "CUST";
        public const string VarSymbolNonCoD = "VARS";
    }

    public sealed class RouteType
    {
        public const string IN = "IN";
        public const string OUT = "OUT";
    }

    public sealed class TransportationType
    {
        public const string NormalniDoruceni = "Doprava PPL";                   // normalni doprava (PL)
        public const string ParcelShop = "Doprava PPL Parcel Shop";             // PPL Parcel Shop (PS)
        public const string VecerniDoruceni = "Doprava PPL večerní doručení";   // PPL večerní doručení (PV)
    }
}
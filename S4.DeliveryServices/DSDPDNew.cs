﻿using S4.Core.Data;
using S4.DAL;
using S4.DeliveryServices.DPDNewAPI;
using S4.DeliveryServices.DPDNewAPI.DpdApi_v1_1;
using S4.Entities;
using S4.Entities.Helpers.Xml;
using S4.Entities.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace S4.DeliveryServices
{
    public class DSDPDNew : DeliveryServiceBase, IDeliveryService
    {
        private (string Token, string CustomerId, string SenderAddressId, string MainServiceCode) _settings;
        private bool _settingsLoaded;
        private bool _requestSaveLabel = false;

        private const string SHIP_TIME = "SHIP_TIME";
        private const string PACK_NUMBER = "PACK_NUMBER";
        private const string DELIVERY_TIME = "DELIVERY_TIME";
        private const string TRACK_URL = "TRACK_URL";

        public DSDPDNew()
        {

        }

        public DSDPDNew(string connectionString) : base(connectionString)
        {

        }


        public DeliveryDirection.DeliveryProviderEnum DeliveryProvider => DeliveryDirection.DeliveryProviderEnum.DPDNew;


        public Task CreateManifest(DeliveryManifest manifest)
        {
            throw new NotImplementedException();
        }


        public async Task CreateShipment(DirectionModel directionModel, Partner partner, PartnerAddress address)
        {
            try
            {
                GetSettings();
                SendToLog(this, $"Create shipment; PartnerID: {partner.PartnerID}; PartnerName: {partner.PartnerName}; PartAddrID: {address.PartAddrID}; AddrName: {address.AddrName}", LogLevelEnum.Info);

                var parcelsCount = DataParserHelper.GetParcelsCount(directionModel);
                var parcels = new List<Parcel>();
                var now = DateTime.Now.ToString("ddMMyyHHmmss");
                for (int i = 0; i < parcelsCount; i++)
                {
                    parcels.Add(new Parcel()
                    {
                        reference1 = $"{directionModel.DirectionID}_{i + 1}_{now}",
                        reference2 = "test x",
                        weight = System.Convert.ToDouble(DataParserHelper.GetWeight(directionModel, i + 1) ?? 1)
                    });
                }

                var shipmentRefNumber = $"{directionModel.DocInfo}_{directionModel.DirectionID}_{now}";
                var phoneNum = address.AddrPhone?.Replace(" ", "").Split('+').Last();
                var country = DataParserHelper.GetCountry(directionModel).ToString();
                var email = DataParserHelper.GetEmail(directionModel, address);
                var receivingPerson = DataParserHelper.GetReceivingPerson(directionModel, null, false);

                var createRequest = new DpdCreateShipmentRequest()
                {
                    buCode = "015",
                    customerId = _settings.CustomerId,
                    shipments = new List<Shipment>() {
                    new Shipment()
                    {
                        numOrder = 1,
                        senderAddressId = _settings.SenderAddressId,
                        reference1 = shipmentRefNumber,
                        reference2 = directionModel.DocInfo,
                        reference3 = DataParserHelper.GetInvoice(directionModel),
                        receiver = new Receiver()
                        {
                            additionalAddressInfo = address.DeliveryInst,
                            city = address.AddrCity,
                            contactEmail = email,
                            contactMobile = phoneNum,
                            contactName = (!string.IsNullOrEmpty(receivingPerson) ? receivingPerson : address.AddrName).Left(35),
                            contactPhone = phoneNum,
                            countryCode = country,
                            name = address.AddrName.Left(35),
                            street = address.AddrLine_1,
                            zipCode = address.AddrLine_2.Replace(" ", "")
            },
                        parcels = parcels,
                        service = new Service()
                        {
                            mainServiceElementCodes = new List<string> { _settings.MainServiceCode },
                            additionalService = new AdditionalService()
                        },
                        extendShipmentData = true
                    }
                }
                };

                // set payment
                var paymentType = DataParserHelper.GetPaymentType(directionModel);
                if (paymentType == DeliveryDirection.DeliveryPaymentEnum.CAOD || paymentType == DeliveryDirection.DeliveryPaymentEnum.CHOD)
                {
                    var cod = DataParserHelper.GetCashOnDelivery(directionModel);

                    createRequest.shipments[0].service.additionalService.cod = new Cod()
                    {
                        amount = System.Convert.ToDouble(cod),
                        currency = DataParserHelper.GetCurrency(directionModel).ToString(),
                        paymentType = paymentType == DeliveryDirection.DeliveryPaymentEnum.CHOD ? "Cash" : "CreditCard",
                        reference = DataParserHelper.GetPaymentReference(directionModel).ToString(),
                        split = "Even"
                    };
                }


                // set parcelshop if pickup
                if (DataParserHelper.GetParcelShop(directionModel, out string parcelShopCode))
                {
                    createRequest.shipments[0].service.additionalService.pudoId = parcelShopCode;

                    // services
                    createRequest.shipments[0].service.mainServiceElementCodes.Add("013");
                    createRequest.shipments[0].service.mainServiceElementCodes.Add("200");

                    // predicts
                    createRequest.shipments[0].service.additionalService.predicts = new List<Predict>()
                    {
                        new Predict()
                        {
                            destinationType = "Consignee",
                            destination = email,
                            type = "EMAIL"
                        },
                        new Predict()
                        {
                            destinationType = "Consignee",
                            destination = phoneNum,
                            type = "SMS"
                        },
                    };
                }


                // Save Request
                var saveRequest = DataParserHelper.SaveDeliveryXML(DataParserHelper.SaveXMLEnum.REQUEST, DeliveryDirection.DeliveryProviderEnum.DPDNew, createRequest, shipmentRefNumber, SavePathRequestResponse);
                SendToLog(this, saveRequest.Item2, saveRequest.Item1 ? LogLevelEnum.Info : LogLevelEnum.Warn);

                var shipmentManager = new DpdShipmentService().SetJwtToken(_settings.Token);
                var createResult = await shipmentManager.CreateDpdShipment(createRequest);

                // Save Response
                var saveResponse = DataParserHelper.SaveDeliveryXML(DataParserHelper.SaveXMLEnum.RESPONSE, DeliveryDirection.DeliveryProviderEnum.DPDNew, shipmentManager.ResponseJson, shipmentRefNumber, SavePathRequestResponse);
                SendToLog(this, saveResponse.Item2, saveResponse.Item1 ? LogLevelEnum.Info : LogLevelEnum.Warn);

                var errors = createResult.shipmentResults.First().errors;
                if (errors != null && errors.Any())
                {
                    foreach (var error in errors)
                    {
                        var msg = $"Code: {error.errorCode}; Text: {error.errorContent}";
                        SendToLog(this, msg, LogLevelEnum.Warn);
                        InsertShipmentError(this, directionModel.DirectionID, msg);
                    }

                    return;
                }

                var shipmentId = createResult.shipmentResults.FirstOrDefault().shipment.shipmentId;

                SendToLog(this,
                        $"Create shipment done; ShipmentId: {shipmentId}; " +
                        $"TransactionID: {createResult.transactionId}",
                        LogLevelEnum.Info);


                // get label
                _requestSaveLabel = true;
                var resultLabel = await GetLabel(shipmentId.ToString());
                if (!resultLabel.Item1)
                {
                    InsertShipmentError(this, directionModel.DirectionID, resultLabel.Item2);
                    return;
                }

                // save to db
                int parcCnt = 1;
                var resParcels = createResult.shipmentResults[0].shipment.parcels.Select(_ => new Tuple<int, string>(parcCnt++, _.parcelId.ToString())).ToArray();
                if (!InsertShipment(this, directionModel.DirectionID, shipmentId.ToString(), resParcels, out string errorText))
                {
                    SendToLog(this, errorText, LogLevelEnum.Warn);
                    await DeleteShipment(shipmentId.ToString());
                }


                GetShipmentStatusEvent += (s, provider, args) =>
                {
                    if (args != null)
                    {
                        var packNumber = (from a in args where a.Key.Equals(PACK_NUMBER) select a.Value).FirstOrDefault();
                        using (var db = Core.Data.ConnectionHelper.GetDB())
                        {
                            var direction = new DAL.DirectionDAL().GetDirectionAllData(directionModel.DirectionID, db);
                            direction.XtraData[Direction.XTRA_DELIVERY_PACK_NUMBER, 0] = packNumber;

                            db.Update(direction);
                            direction.XtraData.Save(db, direction.DirectionID);
                            SendToLog(this, $"Save XtraData DirectionID: {direction.DirectionID}", LogLevelEnum.Info);

                            //save to db
                            InsertShipment(this,
                                directionModel.DirectionID,
                                shipmentRefNumber,
                                resParcels.Select(i => new Tuple<int, string>(i.Item1, packNumber.ToString())).ToArray(),
                                out string handleStatusErrorText);
                        }
                    }
                };

                await GetShipmentStatus(shipmentId.ToString());
            }
            catch (DPDRequestException dpdEx)
            {
                SendToLog(this, dpdEx.Message, LogLevelEnum.Warn, dpdEx);
            }
            catch (DeliveryException dex)
            {
                SendToLog(this, dex.Message, LogLevelEnum.Warn, dex);
            }
            catch (Exception ex)
            {
                SendToLog(this, ex.Message, LogLevelEnum.Error, ex);
            }
        }


        public async Task DeleteShipment(string shipmentReference)
        {
            try
            {
                GetSettings();

                SendToLog(this, $"Delete shipment; Shipment reference: {shipmentReference}", LogLevelEnum.Info);

                var shipmentManager = new DpdShipmentService().SetJwtToken(_settings.Token);
                var result = await shipmentManager.CancelDpdShipments(_settings.CustomerId, new List<int>() { int.Parse(shipmentReference) });

                var errors = result.resultList[0].errors;
                if (errors != null && errors.Any())
                {
                    foreach (var error in errors)
                        SendToLog(this, $"An error occurred while trying to delete the shipment request - Code: {error.errorCode}; Text: {error.errorContent}", LogLevelEnum.Warn);
                    return;
                }

                SendToLog(this, $"Delete shipment done; ShipmentId: {result.resultList[0].shipmentId}; " +
                    $"TransactionID: {result.transactionId}; ",
                    LogLevelEnum.Info);
            }
            catch (DPDRequestException dpdEx)
            {
                SendToLog(this, dpdEx.Message, LogLevelEnum.Warn, dpdEx);
            }
            catch (DeliveryException dex)
            {
                SendToLog(this, dex.Message, LogLevelEnum.Warn, dex);
            }
            catch (Exception ex)
            {
                SendToLog(this, ex.Message, LogLevelEnum.Error, ex);
            }
        }


        public async Task<Tuple<bool, string>> GetLabel(string shipmentReference)
        {
            try
            {
                GetSettings();

                SendToLog(this, $"Get label; Shipment reference: {shipmentReference}", LogLevelEnum.Info);

                var shipmentManager = new DpdShipmentService().SetJwtToken(_settings.Token);
                var result = await shipmentManager.GetLabelPdf(int.Parse(shipmentReference));

                var errors = result.errors;
                if (errors != null && errors.Any())
                {
                    foreach (var error in errors)
                        SendToLog(this, $"An error occurred while GetLabel - shipmentReference: {shipmentReference}; Code: {error.errorCode}; Text: {error.errorContent}", LogLevelEnum.Warn);
                    return new Tuple<bool, string>(false, string.Join(", ", errors.Select(_ => _.errorContent)));
                }

                const string PDF_HEADER = "data:application/pdf;base64,";
                if (result.pdfFile == null || !result.pdfFile.StartsWith(PDF_HEADER))
                    throw new Exception("pdfFile format error");
                var base64 = result.pdfFile.Substring(PDF_HEADER.Length);
                byte[] fileData = System.Convert.FromBase64String(base64);

                if (_requestSaveLabel)
                    SaveLabelToFile(shipmentReference, fileData);

                InvokeLabelEvent(this, shipmentReference, fileData);

                int directionID = GetDirectionIDFromShipmentRef(shipmentReference);
                if (directionID != 0)
                    LabelPrinted(this, directionID);

                SendToLog(this, $"Get label done; TransactionID: {result.transactionId}", LogLevelEnum.Info);
                return new Tuple<bool, string>(true, String.Empty);
            }
            catch (Exception ex)
            {
                SendToLog(this, ex.Message, LogLevelEnum.Error, ex);
                return new Tuple<bool, string>(false, ex.Message);
            }
        }


        public async Task GetShipmentStatus(string shipmentReference)
        {
            SendToLog(this, $"Get Shipment Status; ShipmentReference: {shipmentReference}", LogLevelEnum.Info);

            try
            {
                GetSettings();
                var shipmentManager = new DpdShipmentService().SetJwtToken(_settings.Token);
                var shipmentStatus = await shipmentManager.RequestShipmentStatus(shipmentReference);
                var parcels = shipmentStatus.shipment.parcels;

                // save status
                var dataToSave = new List<Entities.Models.LabeledValue>();
                string trackUrl = "https://www.dpdgroup.com/cz/mydpd/my-parcels/incoming?parcelNumber=";

                foreach (var parcel in parcels)
                {
                    dataToSave.Add(new Entities.Models.LabeledValue(SHIP_TIME, "Převzetí zásilky", FormatDate(shipmentStatus.shipment.printDate, shipmentStatus.shipment.printTime)));
                    dataToSave.Add(new Entities.Models.LabeledValue(PACK_NUMBER, "Číslo balíku", parcel.parcelNumber));
                    dataToSave.Add(new Entities.Models.LabeledValue(TRACK_URL, "Sledování zásilky", $"{trackUrl}{parcel.parcelNumber}"));
                }

                OnGetShipmentStatus(this, DeliveryDirection.DeliveryProviderEnum.DPD, dataToSave.ToArray());

                SendToLog(this, $"Get Shipment Status done; ShipmentReference: {shipmentReference}", LogLevelEnum.Info);
            }
            catch (Exception ex)
            {
                SendToLog(this, ex.Message, LogLevelEnum.Error, ex);
            }
            
            return;


        }


        private string FormatDate(string dateStr, string timeStr)
        {
                if (string.IsNullOrEmpty(dateStr))
                    return null;
                if (string.IsNullOrEmpty(timeStr))
                    timeStr = "0000";
                var dateTimeStr = $"{dateStr}{timeStr}";
                return Core.Utils.DateUtils.GetDateByMask(dateTimeStr, "yyyyMMddHHmm")?.ToString("dd.MM.yyyy HH:mm");
        }


        public Task PrintManifest(string manifestReference)
        {
            throw new NotImplementedException();
        }



        private void GetSettings()
        {
            if (!_settingsLoaded)
            {
                var setting = Settings.Providers.Where(i => i.ProviderName == DeliveryProvider.ToString()).First();
                _settings.Token = setting.Properties["token"];
                _settings.CustomerId = setting.Properties["customerId"];
                _settings.SenderAddressId = setting.Properties["senderAddressId"];
                _settings.MainServiceCode = setting.Properties["mainServiceCode"];

                _settingsLoaded = true;
            }
        }


        private void SaveLabelToFile(string shipmentRefNumber, byte[] fileBytes)
        {
            shipmentRefNumber = shipmentRefNumber.Replace("/", "_");
            //create new dir?
            string directory = $@"{SavePathRequestResponse}\{DateTime.Now:yyyy}\{DateTime.Now:MMdd}";
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);

            var savePath = $"{directory}\\{shipmentRefNumber}_label_DPD.pdf";
            if (File.Exists(savePath))
                File.Delete(savePath);

            File.WriteAllBytes(savePath, fileBytes);

            SendToLog(this, $"Save label done; Path: {savePath}; FileLength: {fileBytes.Length}", LogLevelEnum.Info);
            _requestSaveLabel = false;
        }


        private int GetDirectionIDFromShipmentRef(string shipmentReference)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var delivery = new DeliveryDirectionDAL().GetByRefNumber(DeliveryProvider, shipmentReference, db);
                return delivery?.DirectionID ?? 0;
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using S4.Entities;

namespace S4.DeliveryServices
{
    public enum LogLevelEnum : byte
    {
        Info = 1,
        Warn = 2,
        Error = 4,
        Fatal = 8,
        Debug = 16,
        Trace = 32
    }
    public delegate void LogEventHandler(IDeliveryService sender, string message, LogLevelEnum logLevel, Exception exception = null);
    public class GetLabelEventArgs : EventArgs
    {
        public IDeliveryService DeliveryService { get; set; }
        public string ReferenceId { get; set; }
        public byte[] Data { get; set; }
    }
    public delegate void GetShipmentStatusEventHandler(object sender, DeliveryDirection.DeliveryProviderEnum provider, Entities.Models.LabeledValue[] content);

    public interface IDeliveryCommon
    {
        Task GetShipmentLabel(int directionID);
        event LogEventHandler LogEvent;
        event EventHandler<GetLabelEventArgs> GetLabelEvent;
        event EventHandler<GetLabelEventArgs> GetManifestReportEvent;
        event GetShipmentStatusEventHandler GetShipmentStatusEvent;
        DeliveryDirection.DeliveryProviderEnum[] GetDeliveryProviders();
        int? GetDefaultPrinterLocationID(string deliveryProvider);
        (string name, string address, string zipCode) GetSenderAddress(string deliveryProvider);
        Task GetManifestReport(DeliveryDirection.DeliveryProviderEnum deliveryProvider, string manifestRefNumber);
        Task GetShipmentStatus(int directionID, int deliveryDirectionID);
        string GetTrackURL(string deliveryProvider);
    }
}

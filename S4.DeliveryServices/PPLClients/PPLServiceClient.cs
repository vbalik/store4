﻿using Microsoft.Extensions.Caching.Memory;
using PPLWebService;
using System;
using System.ServiceModel;
using System.Threading.Tasks;
using System.Xml;

namespace S4.DeliveryServices.DPDClients
{
    public class PPLServiceClient : ClientBase<IMyApi2>, IMyApi2
    {
        private static string ENDPOINT_URL = "https://myapi.ppl.cz/MyAPI.svc";
        private static int TOKEN_EXPIRE_SECONDS = 1500;

        /// <summary>
        /// Login parameters
        /// </summary>
        private readonly int _custId;
        private readonly string _password;
        private readonly string _userName;

        private static BasicHttpBinding _GetHttpBinding()
        {
            var binding = new BasicHttpBinding
            {
                SendTimeout = TimeSpan.FromSeconds(180),
                MaxBufferSize = int.MaxValue,
                MaxReceivedMessageSize = int.MaxValue,
                AllowCookies = true,
                ReaderQuotas = XmlDictionaryReaderQuotas.Max,
            };

            // allow https
            binding.Security.Mode = BasicHttpSecurityMode.Transport;

            return binding;
        }

        public PPLServiceClient() :
            base(_GetHttpBinding(),
            new EndpointAddress(ENDPOINT_URL))
        {
        }

        public PPLServiceClient(int custId, string password, string userName) :
            base(_GetHttpBinding(),
                new EndpointAddress(ENDPOINT_URL))
        {
            _custId = custId;
            _password = password;
            _userName = userName;
        }


        /// <summary>
        /// Get Authentication data with logged token
        /// </summary>
        public async Task<AuthenticationData> GetAuthData()
        {
            const string KEY = "cache_ppl_token";
            
            var cache = new MemoryCache(new MemoryCacheOptions {
                ExpirationScanFrequency = new TimeSpan(0, 0, 5)
            });

            if(cache.TryGetValue<AuthenticationData>(KEY, out var cachedToken))
                return cachedToken;

            var token = await _GetToken();

            cache.GetOrCreate(KEY, entry => {
                entry.AbsoluteExpiration = DateTime.Now.AddSeconds(TOKEN_EXPIRE_SECONDS);
                return token;
            });

            return token;
        }

        private async Task<AuthenticationData> _GetToken()
        {
            var loginResult = await LoginAsync(new AuthenticationData
            {
                CustId = _custId,
                Password = _password,
                UserName = _userName
            });

            return new AuthenticationData
            {
                AuthToken = loginResult.AuthToken
            };
        }

        [OperationContractAttribute(Action = "http://myapi.ppl.cz/v1/IMyApi2/CreatePackages", ReplyAction = "http://myapi.ppl.cz/v1/IMyApi2/CreatePackagesResponse")]
        [FaultContractAttribute(typeof(PPLWebService.GeneralFault), Action = "http://myapi.ppl.cz/v1/GeneralFault", Name = "GeneralFault")]
        [FaultContractAttribute(typeof(PPLWebService.AuthenticationFault), Action = "http://myapi.ppl.cz/v1/AuthenticationFault", Name = "AuthenticationFault")]
        [FaultContractAttribute(typeof(PPLWebService.ValidationFault), Action = "http://myapi.ppl.cz/v1/ValidationFault", Name = "ValidationFault")]
        public System.Threading.Tasks.Task<PPLWebService.CreatePackagesResult> CreatePackagesAsync(PPLWebService.AuthenticationData Auth, string CustomerUniqueImportId, PPLWebService.MyApiPackageIn[] Packages, PPLWebService.ReturnChannelSettings ReturnChannel)
        {
            return Channel.CreatePackagesAsync(Auth, CustomerUniqueImportId, Packages, ReturnChannel);
        }

        [OperationContractAttribute(Action = "http://myapi.ppl.cz/v1/IMyApi2/CreatePackageLabel", ReplyAction = "http://myapi.ppl.cz/v1/IMyApi2/CreatePackageLabelResponse")]
        [FaultContractAttribute(typeof(PPLWebService.GeneralFault), Action = "http://myapi.ppl.cz/v1/GeneralFault", Name = "GeneralFault")]
        [FaultContractAttribute(typeof(PPLWebService.AuthenticationFault), Action = "http://myapi.ppl.cz/v1/AuthenticationFault", Name = "AuthenticationFault")]
        [FaultContractAttribute(typeof(PPLWebService.ValidationFault), Action = "http://myapi.ppl.cz/v1/ValidationFault", Name = "ValidationFault")]
        public System.Threading.Tasks.Task<PPLWebService.CreatePackagesLabelsResult> CreatePackageLabelAsync(PPLWebService.AuthenticationData Auth, bool FullLabelUrl, PPLWebService.MyApiPackageIn2[] Packages, PPLWebService.LabelReturnChannelSettings ReturnChannel)
        {
            return Channel.CreatePackageLabelAsync(Auth, FullLabelUrl, Packages, ReturnChannel);
        }

        [OperationContractAttribute(Action = "http://myapi.ppl.cz/v1/IMyApi2/GetCplResult", ReplyAction = "http://myapi.ppl.cz/v1/IMyApi2/GetCplResultResponse")]
        [FaultContractAttribute(typeof(PPLWebService.GeneralFault), Action = "http://myapi.ppl.cz/v1/GeneralFault", Name = "GeneralFault")]
        [FaultContractAttribute(typeof(PPLWebService.AuthenticationFault), Action = "http://myapi.ppl.cz/v1/AuthenticationFault", Name = "AuthenticationFault")]
        [FaultContractAttribute(typeof(PPLWebService.ValidationFault), Action = "http://myapi.ppl.cz/v1/ValidationFault", Name = "ValidationFault")]
        public System.Threading.Tasks.Task<PPLWebService.CreatePackageLabelRequestResult> GetCplResultAsync(PPLWebService.AuthenticationData Auth, int RequestId)
        {
            return Channel.GetCplResultAsync(Auth, RequestId);
        }

        [OperationContractAttribute(Action = "http://myapi.ppl.cz/v1/IMyApi2/GetPackages", ReplyAction = "http://myapi.ppl.cz/v1/IMyApi2/GetPackagesResponse")]
        [FaultContractAttribute(typeof(PPLWebService.GeneralFault), Action = "http://myapi.ppl.cz/v1/GeneralFault", Name = "GeneralFault")]
        [FaultContractAttribute(typeof(PPLWebService.AuthenticationFault), Action = "http://myapi.ppl.cz/v1/AuthenticationFault", Name = "AuthenticationFault")]
        public System.Threading.Tasks.Task<PPLWebService.GetPackagesResult> GetPackagesAsync(PPLWebService.AuthenticationData Auth, PPLWebService.PackagesFilter Filter)
        {
            return Channel.GetPackagesAsync(Auth, Filter);
        }

        [OperationContractAttribute(Action = "http://myapi.ppl.cz/v1/IMyApi2/Login", ReplyAction = "http://myapi.ppl.cz/v1/IMyApi2/LoginResponse")]
        [FaultContractAttribute(typeof(PPLWebService.GeneralFault), Action = "http://myapi.ppl.cz/v1/GeneralFault", Name = "GeneralFault")]
        [FaultContractAttribute(typeof(PPLWebService.AuthenticationFault), Action = "http://myapi.ppl.cz/v1/AuthenticationFault", Name = "AuthenticationFault")]
        public System.Threading.Tasks.Task<PPLWebService.LoginResult> LoginAsync(PPLWebService.AuthenticationData Auth)
        {
            return Channel.LoginAsync(Auth);
        }

        [OperationContractAttribute(Action = "http://myapi.ppl.cz/v1/IMyApi2/CreateOrders", ReplyAction = "http://myapi.ppl.cz/v1/IMyApi2/CreateOrdersResponse")]
        [FaultContractAttribute(typeof(PPLWebService.GeneralFault), Action = "http://myapi.ppl.cz/v1/GeneralFault", Name = "GeneralFault")]
        [FaultContractAttribute(typeof(PPLWebService.AuthenticationFault), Action = "http://myapi.ppl.cz/v1/AuthenticationFault", Name = "AuthenticationFault")]
        public System.Threading.Tasks.Task<PPLWebService.CreateOrdersResult> CreateOrdersAsync(PPLWebService.AuthenticationData Auth, PPLWebService.MyApiOrderIn[] Orders, PPLWebService.ReturnChannelSettings ReturnChannel)
        {
            return Channel.CreateOrdersAsync(Auth, Orders, ReturnChannel);
        }

        [OperationContractAttribute(Action = "http://myapi.ppl.cz/v1/IMyApi2/CreatePickupOrders", ReplyAction = "http://myapi.ppl.cz/v1/IMyApi2/CreatePickupOrdersResponse")]
        [FaultContractAttribute(typeof(PPLWebService.GeneralFault), Action = "http://myapi.ppl.cz/v1/GeneralFault", Name = "GeneralFault")]
        [FaultContractAttribute(typeof(PPLWebService.AuthenticationFault), Action = "http://myapi.ppl.cz/v1/AuthenticationFault", Name = "AuthenticationFault")]
        public System.Threading.Tasks.Task<PPLWebService.CreatePickupOrdersResult> CreatePickupOrdersAsync(PPLWebService.AuthenticationData Auth, PPLWebService.MyApiPickupOrderIn[] Orders, PPLWebService.ReturnChannelSettings ReturnChannel)
        {
            return Channel.CreatePickupOrdersAsync(Auth, Orders, ReturnChannel);
        }

        [OperationContractAttribute(Action = "http://myapi.ppl.cz/v1/IMyApi2/GetCitiesRouting", ReplyAction = "http://myapi.ppl.cz/v1/IMyApi2/GetCitiesRoutingResponse")]
        [FaultContractAttribute(typeof(PPLWebService.GeneralFault), Action = "http://myapi.ppl.cz/v1/GeneralFault", Name = "GeneralFault")]
        [FaultContractAttribute(typeof(PPLWebService.AuthenticationFault), Action = "http://myapi.ppl.cz/v1/AuthenticationFault", Name = "AuthenticationFault")]
        public System.Threading.Tasks.Task<PPLWebService.GetCitiesRoutingResult> GetCitiesRoutingAsync(PPLWebService.AuthenticationData Auth, PPLWebService.CitiesRoutingFilter Filter)
        {
            return Channel.GetCitiesRoutingAsync(Auth, Filter);
        }

        [OperationContractAttribute(Action = "http://myapi.ppl.cz/v1/IMyApi2/GetParcelShops", ReplyAction = "http://myapi.ppl.cz/v1/IMyApi2/GetParcelShopsResponse")]
        [FaultContractAttribute(typeof(PPLWebService.GeneralFault), Action = "http://myapi.ppl.cz/v1/GeneralFault", Name = "GeneralFault")]
        public System.Threading.Tasks.Task<PPLWebService.GetParcelShopsResult> GetParcelShopsAsync(PPLWebService.ParcelShopFilter Filter)
        {
            return Channel.GetParcelShopsAsync(Filter);
        }

        [OperationContractAttribute(Action = "http://myapi.ppl.cz/v1/IMyApi2/Version", ReplyAction = "http://myapi.ppl.cz/v1/IMyApi2/VersionResponse")]
        [FaultContractAttribute(typeof(PPLWebService.GeneralFault), Action = "http://myapi.ppl.cz/v1/GeneralFault", Name = "GeneralFault")]
        public System.Threading.Tasks.Task<string> VersionAsync()
        {
            return Channel.VersionAsync();
        }

        [OperationContractAttribute(Action = "http://myapi.ppl.cz/v1/IMyApi2/PartnerSaveStatuses", ReplyAction = "http://myapi.ppl.cz/v1/IMyApi2/PartnerSaveStatusesResponse")]
        [FaultContractAttribute(typeof(PPLWebService.GeneralFault), Action = "http://myapi.ppl.cz/v1/GeneralFault", Name = "GeneralFault")]
        [FaultContractAttribute(typeof(PPLWebService.AuthenticationFault), Action = "http://myapi.ppl.cz/v1/AuthenticationFault", Name = "AuthenticationFault")]
        [FaultContractAttribute(typeof(PPLWebService.AuthorizationFault), Action = "http://myapi.ppl.cz/v1/AuthorizationFault", Name = "AuthorizationFault")]
        public System.Threading.Tasks.Task<PPLWebService.PartnerSaveStatusesResult> PartnerSaveStatusesAsync(PPLWebService.AuthenticationData Auth, PPLWebService.StatusData[] Request)
        {
            return Channel.PartnerSaveStatusesAsync(Auth, Request);
        }

        [OperationContractAttribute(Action = "http://myapi.ppl.cz/v1/IMyApi2/SavePartners", ReplyAction = "http://myapi.ppl.cz/v1/IMyApi2/SavePartnersResponse")]
        [FaultContractAttribute(typeof(PPLWebService.GeneralFault), Action = "http://myapi.ppl.cz/v1/GeneralFault", Name = "GeneralFault")]
        [FaultContractAttribute(typeof(PPLWebService.AuthenticationFault), Action = "http://myapi.ppl.cz/v1/AuthenticationFault", Name = "AuthenticationFault")]
        [FaultContractAttribute(typeof(PPLWebService.AuthorizationFault), Action = "http://myapi.ppl.cz/v1/AuthorizationFault", Name = "AuthorizationFault")]
        public System.Threading.Tasks.Task<PPLWebService.SavePartnersResult> SavePartnersAsync(PPLWebService.AuthenticationData Auth, PPLWebService.PartnerData Request)
        {
            return Channel.SavePartnersAsync(Auth, Request);
        }

        [OperationContractAttribute(Action = "http://myapi.ppl.cz/v1/IMyApi2/GetCodCurrency", ReplyAction = "http://myapi.ppl.cz/v1/IMyApi2/GetCodCurrencyResponse")]
        [FaultContractAttribute(typeof(PPLWebService.GeneralFault), Action = "http://myapi.ppl.cz/v1/GeneralFault", Name = "GeneralFault")]
        public System.Threading.Tasks.Task<PPLWebService.GetCodCurrencyResult> GetCodCurrencyAsync()
        {
            return Channel.GetCodCurrencyAsync();
        }

        [OperationContractAttribute(Action = "http://myapi.ppl.cz/v1/IMyApi2/GetSprintRoutes", ReplyAction = "http://myapi.ppl.cz/v1/IMyApi2/GetSprintRoutesResponse")]
        [FaultContractAttribute(typeof(PPLWebService.GeneralFault), Action = "http://myapi.ppl.cz/v1/GeneralFault", Name = "GeneralFault")]
        public System.Threading.Tasks.Task<PPLWebService.GetSprintRoutesResult> GetSprintRoutesAsync()
        {
            return Channel.GetSprintRoutesAsync();
        }

        [OperationContractAttribute(Action = "http://myapi.ppl.cz/v1/IMyApi2/GetProductCountry", ReplyAction = "http://myapi.ppl.cz/v1/IMyApi2/GetProductCountryResponse")]
        [FaultContractAttribute(typeof(PPLWebService.GeneralFault), Action = "http://myapi.ppl.cz/v1/GeneralFault", Name = "GeneralFault")]
        public System.Threading.Tasks.Task<PPLWebService.GetProductCountryResult> GetProductCountryAsync(PPLWebService.ProductCountryFilter Filter)
        {
            return Channel.GetProductCountryAsync(Filter);
        }

        [OperationContractAttribute(Action = "http://myapi.ppl.cz/v1/IMyApi2/SaveProductTransports", ReplyAction = "http://myapi.ppl.cz/v1/IMyApi2/SaveProductTransportsResponse")]
        [FaultContractAttribute(typeof(PPLWebService.GeneralFault), Action = "http://myapi.ppl.cz/v1/GeneralFault", Name = "GeneralFault")]
        public System.Threading.Tasks.Task<bool> SaveProductTransportsAsync(PPLWebService.AuthenticationData Auth, PPLWebService.MyApiCountryProducts[] CountryProducts)
        {
            return Channel.SaveProductTransportsAsync(Auth, CountryProducts);
        }

        [OperationContractAttribute(Action = "http://myapi.ppl.cz/v1/IMyApi2/GetPackProducts", ReplyAction = "http://myapi.ppl.cz/v1/IMyApi2/GetPackProductsResponse")]
        [FaultContractAttribute(typeof(PPLWebService.GeneralFault), Action = "http://myapi.ppl.cz/v1/GeneralFault", Name = "GeneralFault")]
        public System.Threading.Tasks.Task<PPLWebService.GetPackProductsResult> GetPackProductsAsync(PPLWebService.PackProductFilter Filter)
        {
            return Channel.GetPackProductsAsync(Filter);
        }

        [OperationContractAttribute(Action = "http://myapi.ppl.cz/v1/IMyApi2/GetPackNumberRowTypes", ReplyAction = "http://myapi.ppl.cz/v1/IMyApi2/GetPackNumberRowTypesResponse")]
        [FaultContractAttribute(typeof(PPLWebService.GeneralFault), Action = "http://myapi.ppl.cz/v1/GeneralFault", Name = "GeneralFault")]
        public System.Threading.Tasks.Task<PPLWebService.GetPackNumberRowTypesResult> GetPackNumberRowTypesAsync(PPLWebService.PackNumberRowTypeFilter Filter)
        {
            return Channel.GetPackNumberRowTypesAsync(Filter);
        }

        [OperationContractAttribute(Action = "http://myapi.ppl.cz/v1/IMyApi2/GetNumberRange", ReplyAction = "http://myapi.ppl.cz/v1/IMyApi2/GetNumberRangeResponse")]
        [FaultContractAttribute(typeof(PPLWebService.GeneralFault), Action = "http://myapi.ppl.cz/v1/GeneralFault", Name = "GeneralFault")]
        public System.Threading.Tasks.Task<PPLWebService.GetNumberRangeResult> GetNumberRangeAsync(PPLWebService.AuthenticationData Auth, PPLWebService.NumberRangeRequest[] NumberRanges)
        {
            return Channel.GetNumberRangeAsync(Auth, NumberRanges);
        }

        [OperationContractAttribute(Action = "http://myapi.ppl.cz/v1/IMyApi2/UpdatePackage", ReplyAction = "http://myapi.ppl.cz/v1/IMyApi2/UpdatePackageResponse")]
        [FaultContractAttribute(typeof(PPLWebService.GeneralFault), Action = "http://myapi.ppl.cz/v1/GeneralFault", Name = "GeneralFault")]
        public System.Threading.Tasks.Task<PPLWebService.UpdatePackageResult> UpdatePackageAsync(PPLWebService.AuthenticationData Auth, PPLWebService.MyApiPackageUpdate Update)
        {
            return Channel.UpdatePackageAsync(Auth, Update);
        }

        [OperationContractAttribute(Action = "", ReplyAction = "*")]
        [FaultContractAttribute(typeof(PPLWebService.GeneralFault), Action = "http://myapi.ppl.cz/v1/GeneralFault", Name = "GeneralFault")]
        public System.Threading.Tasks.Task<string> IsHealtlyAsync()
        {
            return Channel.IsHealtlyAsync();
        }

        public async System.Threading.Tasks.Task<bool> IsHealtly()
        {
            var result = await Channel.IsHealtlyAsync();
            return result == "Healthy";
        }
    }
}

﻿using Newtonsoft.Json;
using NPoco;
using NPoco.fastJSON;
using S4.DAL;
using S4.DeliveryServices.Models.CPost;
using S4.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Xml;
using System.Xml.Serialization;

namespace S4.DeliveryServices
{
    public class DataParserHelper
    {
        private const DeliveryDirection.DeliveryCountryEnum _DEFAULT_COUNTRY = DeliveryDirection.DeliveryCountryEnum.CZ;
        private const DeliveryDirection.DeliveryCurrencyEnum _DEFAULT_CURRENCY = DeliveryDirection.DeliveryCurrencyEnum.CZK;

        public enum SaveXMLEnum : byte
        {
            REQUEST,
            RESPONSE
        }

        public static string GetEmail(S4.Entities.Models.DirectionModel directionModel, PartnerAddress addres)
        {
            Func<string, (bool, string)> validateEmail = (email) =>
            {
                if (string.IsNullOrWhiteSpace(email))
                    return (false, null);

                var email1 = email.Split(';')[0];
                if (new EmailAddressAttribute().IsValid(email1))
                    return (true, email1);

                return (false, null);
            };

            //try to get it from direction
            var res = validateEmail(directionModel.DocRemark);
            if (res.Item1)
                return res.Item2;

            //try to get it from address
            res = validateEmail(addres.AddrRemark);
            if (res.Item1)
                return res.Item2;

            return null;
        }

        public static string GetInvoice(S4.Entities.Models.DirectionModel directionModel)
        {
            if (directionModel.XtraData.IsNull(Direction.XTRA_INVOICE_NUMBER))
                return null;

            return directionModel.XtraData[Direction.XTRA_INVOICE_NUMBER].ToString();
        }

        public static long? GetPaymentReference(S4.Entities.Models.DirectionModel directionModel)
        {
            if (directionModel.XtraData.IsNull(Direction.XTRA_PAYMENT_REFERENCE))
                return null;

            return Int64.Parse(directionModel.XtraData[Direction.XTRA_PAYMENT_REFERENCE].ToString());
        }

        public static decimal? GetWeight(S4.Entities.Models.DirectionModel directionModel, int parcelPosition)
        {
            if (directionModel.XtraData.IsNull(Direction.XTRA_DISPATCH_WEIGHT, parcelPosition))
                return null;

            return (decimal)directionModel.XtraData[Direction.XTRA_DISPATCH_WEIGHT, parcelPosition];
        }

        public static int GetParcelsCount(S4.Entities.Models.DirectionModel directionModel)
        {
            if (directionModel.XtraData.IsNull(Direction.XTRA_DISPATCH_BOXES_CNT))
                return 1;

            return (int)directionModel.XtraData[Direction.XTRA_DISPATCH_BOXES_CNT];
        }

        public static DeliveryDirection.DeliveryPaymentEnum GetPaymentType(S4.Entities.Models.DirectionModel directionModel)
        {
            if (directionModel.XtraData.IsNull(Direction.XTRA_PAYMENT_TYPE))
                throw new DeliveryException("Payment type is missing!");

            return new DeliveryDirection().PTYPEToDeliveryPayment(directionModel.XtraData[Direction.XTRA_PAYMENT_TYPE].ToString());
        }

        public static decimal GetCashOnDelivery(S4.Entities.Models.DirectionModel directionModel)
        {
            if (directionModel.XtraData.IsNull(Direction.XTRA_CASH_ON_DELIVERY))
                return 0;

            return (decimal)directionModel.XtraData[Direction.XTRA_CASH_ON_DELIVERY];
        }

        public static DeliveryDirection.DeliveryCountryEnum GetCountry(S4.Entities.Models.DirectionModel directionModel)
        {
            var tType = directionModel.XtraData[Direction.XTRA_TRANSPORTATION_TYPE].ToString();
            if (new DeliveryDirection().IsSK(tType))
                return DeliveryDirection.DeliveryCountryEnum.SK;

            if (directionModel.XtraData.IsNull(Direction.XTRA_DELIVERY_COUNTRY))
                return _DEFAULT_COUNTRY;

            return (DeliveryDirection.DeliveryCountryEnum)Enum.Parse(typeof(DeliveryDirection.DeliveryCountryEnum), directionModel.XtraData[Direction.XTRA_DELIVERY_COUNTRY].ToString());
        }

        public static DeliveryDirection.DeliveryCurrencyEnum GetCurrency(S4.Entities.Models.DirectionModel directionModel)
        {
            if (directionModel.XtraData.IsNull(Direction.XTRA_DELIVERY_CURRENCY))
                return _DEFAULT_CURRENCY;

            return (DeliveryDirection.DeliveryCurrencyEnum)Enum.Parse(typeof(DeliveryDirection.DeliveryCurrencyEnum), directionModel.XtraData[Direction.XTRA_DELIVERY_CURRENCY].ToString());
        }

        /// <summary>
        /// returns true if delivery transportation type is PickUp and gives back the parcelshopcode if is
        /// </summary>
        /// <param name="directionModel"></param>
        /// <param name="parcelShopCode"></param>
        /// <returns></returns>
        public static bool GetParcelShop(S4.Entities.Models.DirectionModel directionModel, out string parcelShopCode)
        {
            parcelShopCode = null;

            var tType = directionModel.XtraData[Direction.XTRA_TRANSPORTATION_TYPE].ToString();
            if (!new DeliveryDirection().IsPickUp(tType))
                return false;

            if (directionModel.XtraData.IsNull(Direction.XTRA_DELIVERY_PARCEL_SHOP))
                throw new DeliveryException("ParcelShopCode is missing!");

            parcelShopCode = directionModel.XtraData[Direction.XTRA_DELIVERY_PARCEL_SHOP].ToString();

            return true;
        }

        /// <summary>
        /// returns shipment information
        /// </summary>
        /// <param name="directionModel"></param>
        /// <returns></returns>
        public static string GetShipInfo(Entities.Models.DirectionModel directionModel)
        {
            if (!directionModel.XtraData.IsNull(Direction.XTRA_DELIVERY_SHIPMENTINFO))
                return directionModel.XtraData[Direction.XTRA_DELIVERY_SHIPMENTINFO].ToString();

            return null;
        }

        /// <summary>
        /// returns address remark and shipment information
        /// </summary>
        /// <param name="address"></param>
        /// <param name="directionModel"></param>
        /// <returns></returns>
        public static string GetShipInfo(Entities.Models.DirectionModel directionModel, Entities.PartnerAddress address)
        {
            var items = new List<string>();
            if (!string.IsNullOrWhiteSpace(address.AddrRemark))
                items.Add(address.AddrRemark);

            var shipInfo = GetShipInfo(directionModel);
            if (shipInfo != null)
                items.Add(shipInfo);

            return string.Join("\n", items.ToArray());
        }

        public static (string PhoneNumber, string CellPhoneNumber) GetPhoneNumbers(string number)
        {
            if (string.IsNullOrWhiteSpace(number))
                return (null, null);

            //remove spaces
            var noSpaces = number.Replace(" ", "");
            //too short
            if (noSpaces.Length < 9)
                return (noSpaces, null);
            //too long and not czech prefix
            if (noSpaces.Length > 9 && noSpaces.IndexOf("+420") != 0)
                return (noSpaces, null);
            //position of key char in number
            int pos = 0;
            //czech prefix - key char on pos 4
            if (noSpaces.IndexOf("+420") == 0)
                pos = 4;
            var keyChar = noSpaces[pos];
            //is cell phone
            if (keyChar == '6' || keyChar == '7')
                return (null, noSpaces);

            return (noSpaces, null);
        }

        public static string GetDepoCode(S4.Entities.Models.DirectionModel directionModel)
        {
            if (directionModel.XtraData.IsNull(Direction.XTRA_DELIVERY_DEPO_CODE))
                return null;

            return directionModel.XtraData[Direction.XTRA_DELIVERY_DEPO_CODE].ToString();
        }

        public static string GetRouteCode(S4.Entities.Models.DirectionModel directionModel)
        {
            if (directionModel.XtraData.IsNull(Direction.XTRA_DELIVERY_ROUTE_CODE))
                return null;

            return directionModel.XtraData[Direction.XTRA_DELIVERY_ROUTE_CODE].ToString();
        }

        public static string GetZipCode(S4.Entities.Models.DirectionModel directionModel)
        {
            if (directionModel.XtraData.IsNull(Direction.XTRA_DELIVERY_ZIP_CODE))
                return null;

            return directionModel.XtraData[Direction.XTRA_DELIVERY_ZIP_CODE].ToString();
        }

        public static string GetTransportationType(S4.Entities.Models.DirectionModel directionModel)
        {
            if (directionModel.XtraData.IsNull(Direction.XTRA_TRANSPORTATION_TYPE))
                return null;

            return directionModel.XtraData[Direction.XTRA_TRANSPORTATION_TYPE].ToString();
        }

        public static bool GetRoutingSquare(S4.Entities.Models.DirectionModel directionModel)
        {
            if (directionModel.XtraData.IsNull(Direction.XTRA_DELIVERY_ROUTING_SQUARE))
                return false;

            return bool.Parse(directionModel.XtraData[Direction.XTRA_DELIVERY_ROUTING_SQUARE].ToString());
        }

        public static string GetReceivingPerson(S4.Entities.Models.DirectionModel directionModel, string phone = "", bool showPrependText = false)
        {
            if (directionModel.XtraData.IsNull(Direction.XTRA_DELIVERY_RECEIVING_PERSON))
                return null;

            return (showPrependText ? "přebír. os. " : "") + (!string.IsNullOrEmpty(phone) ? phone + " " : "") + directionModel.XtraData[Direction.XTRA_DELIVERY_RECEIVING_PERSON].ToString();
        }

        public static (bool, string, string) SaveDeliveryXML(SaveXMLEnum XMLType, DeliveryDirection.DeliveryProviderEnum provider, object data, string shipmentRefNumber, string path, bool testing = false)
        {
            if (string.IsNullOrWhiteSpace(path))
                return (false, $"Parameter 'path' must not be empty!", "");

            shipmentRefNumber = shipmentRefNumber?.Replace("/", "_");

            string savePath = "";
            XmlSerializer xmlSerializer = null;
            var pdfCPOSTCreated = "";
            string toSave = "";
            switch (provider)
            {
                case DeliveryDirection.DeliveryProviderEnum.PPL:
                    if (XMLType == SaveXMLEnum.REQUEST)
                        xmlSerializer = new XmlSerializer(typeof(List<PPLWebService.MyApiPackageIn>));
                    else if (XMLType == SaveXMLEnum.RESPONSE)
                        xmlSerializer = new XmlSerializer(typeof(PPLWebService.CreatePackagesResult));
                    else
                        return (false, $"Unexpected type {XMLType}", "");
                    break;

                case DeliveryDirection.DeliveryProviderEnum.DPD:
                    if (XMLType == SaveXMLEnum.REQUEST)
                        xmlSerializer = new XmlSerializer(typeof(DPDShipmentService.createShipment));
                    else if (XMLType == SaveXMLEnum.RESPONSE)
                        xmlSerializer = new XmlSerializer(typeof(DPDShipmentService.createShipmentResponse1));
                    else
                        return (false, $"Unexpected type {XMLType}", "");
                    break;

                case DeliveryDirection.DeliveryProviderEnum.CPOST:
                    if (XMLType == SaveXMLEnum.REQUEST)
                        xmlSerializer = new XmlSerializer(typeof(parcelServiceSyncRequest));
                    else if (XMLType == SaveXMLEnum.RESPONSE)
                        xmlSerializer = new XmlSerializer(typeof(b2bSyncResponse));
                    else
                        return (false, $"Unexpected type {XMLType}", "");
                    break;

                case DeliveryDirection.DeliveryProviderEnum.DPDNew:
                    toSave = JsonConvert.SerializeObject(data, new JsonSerializerSettings() { DefaultValueHandling = DefaultValueHandling.Ignore });
                    break;

                default:
                    return (false, $"Unexpected provider {provider}", "");
            }


            if (xmlSerializer != null)
            {
                using (StringWriter textWriter = new StringWriter())
                {
                    XmlWriter xmlWriter = XmlWriter.Create(textWriter);
                    xmlSerializer.Serialize(xmlWriter, data);
                    toSave = textWriter.ToString();
                }
            }

            if (testing)
                return (true, "", toSave);

            var ext = (xmlSerializer != null) ? "xml" : "json";

            //create new dir?
            string directory = $@"{path}\{DateTime.Now:yyyy}\{DateTime.Now:MMdd}";
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);

            savePath = $"{directory}\\{shipmentRefNumber}_{XMLType.ToString().ToLower()}_{provider.ToString()}.{ext}";
            if (File.Exists(savePath))
                File.Delete(savePath);

            File.WriteAllText(savePath, toSave, System.Text.Encoding.Unicode);

            //CPOST response - save responsePrintParams.file
            if (provider == DeliveryDirection.DeliveryProviderEnum.CPOST && XMLType == SaveXMLEnum.RESPONSE)
            {
                var response = (b2bSyncResponse)data;
                if (!string.IsNullOrWhiteSpace(response.serviceData.parcelServiceSyncResponse.responsePrintParams.file))
                {
                    byte[] fileBytes = Convert.FromBase64String(response.serviceData.parcelServiceSyncResponse.responsePrintParams.file);
                    var savePathFileBytes = $"{directory}\\{shipmentRefNumber}_{XMLType.ToString().ToLower()}_{provider.ToString()}.pdf";
                    File.WriteAllBytes(savePathFileBytes, fileBytes);
                    pdfCPOSTCreated = $" and {savePathFileBytes} ";
                }
            }

            return (true, $"Type: {XMLType.ToString()} Provider: {provider.ToString()} File {savePath} {pdfCPOSTCreated} has been created.", "");
        }

        public static (byte[], string) GetRequestResponseFile(int directionID, SaveXMLEnum type, string path)
        {

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var sql = new Sql().Where("directionID = @0", directionID);
                var direction = db.SingleOrDefault<Direction>(sql);

                if (direction == null)
                    return (null, $"Nenalezen doklad DirectionID: {directionID}");

                if (string.IsNullOrWhiteSpace(path))
                    return (null, "Cesta k souborům musí být nastavena!");

                direction.XtraData.Load(db, sql);

                if (direction.XtraData.IsNull(Direction.XTRA_TRANSPORTATION_TYPE))
                    return (null, "Typ přepravy není nastaven!");

                //get history
                var history = (new DirectionDAL()).GetDirectionHistory(directionID);

                var dtShipment = (from h in history
                                  where h.EventCode.Equals(Direction.DR_STATUS_DELIVERY_SHIPMENT)
                                  orderby h.EntryDateTime
                                  select h.EntryDateTime).LastOrDefault();

                path = $"{path}\\{dtShipment.Year}\\{dtShipment.Month.ToString("00")}{dtShipment.Day.ToString("00")}";
                
                var deliveryProvider = new DeliveryDirection().TTYPEToDeliveryProvider(direction.XtraData[Direction.XTRA_TRANSPORTATION_TYPE].ToString());
                byte[] file2Read = null;

                switch (deliveryProvider)
                {
                    case DeliveryDirection.DeliveryProviderEnum.CPOST:
                    case DeliveryDirection.DeliveryProviderEnum.PPL:
                        var fileName = $"{directionID.ToString()}_{type.ToString().ToLower()}_{deliveryProvider.ToString()}.xml";
                        var filePath = $"{path}\\{fileName}";

                        if (!File.Exists(filePath))
                            return (null, $"Nenalezen soubor {filePath}");

                        file2Read = System.IO.File.ReadAllBytes(filePath);
                        return (file2Read, fileName);
                    case DeliveryDirection.DeliveryProviderEnum.DPD:
                    case DeliveryDirection.DeliveryProviderEnum.DPDNew:
                        var fileNameToFind = $"DL_{dtShipment.ToString("yy")}_{direction.DocNumber}_{directionID.ToString()}_*_{type.ToString().ToLower()}_{deliveryProvider.ToString()}.json";
                        var files = Directory.EnumerateFiles(path, fileNameToFind, SearchOption.AllDirectories).ToList();

                        if (files.Count > 0)
                        {
                            file2Read = System.IO.File.ReadAllBytes(files[0]);
                            fileName = Path.GetFileName(files[0]);
                            return (file2Read, fileName);
                        }
                        else
                        {
                            return (null,$"Nenalezen soubor {path}\\{fileNameToFind}");
                        }
                    default:
                        return (null, $"Neznámý typ přepravy {deliveryProvider.ToString()}");
                }

            }


        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using S4.Core.Data;
using S4.Entities;
using System.Xml.Serialization;
using System.IO;

namespace S4.DeliveryServices
{
    public class DSDPD : DeliveryServiceBase, IDeliveryService
    {
        private const string _APPLICATION_TYPE = "applicationType";
        private const string _LANGUAGE = "language";
        private const string _PASSWORD = "password";
        private const string _PAYER_ID = "payerId";
        private const string _SENDER_ADDRESS_ID = "senderAddressId";
        private const string _USER_NAME = "userName";
        private const string _MAIN_SERVICE_CODE = "mainServiceCode";

        private const int _SERVICE_PARCELSHOP = 50101;

        private (string ApplicationType, string Language, string Password, long PayerId, long SenderAddressId, string UserName, long MainServiceCode) _settings;
        private bool _settingsLoaded;
        private bool _requestSaveLabel = false;

        public DeliveryDirection.DeliveryProviderEnum DeliveryProvider { get => DeliveryDirection.DeliveryProviderEnum.DPD; }

        /// <summary>
        /// call only in case of previous initilaliyation of base
        /// </summary>
        public DSDPD()
        {

        }

        public DSDPD(string connectionString) : base(connectionString)
        {

        }

        public async Task CreateShipment(S4.Entities.Models.DirectionModel directionModel, Partner partner, PartnerAddress address)
        {
            try
            {
                //load settings
                var settings = GetSettings();
                SendToLog(this, $"Create shipment; PartnerID: {partner.PartnerID}; PartnerName: {partner.PartnerName}; PartAddrID: {address.PartAddrID}; AddrName: {address.AddrName}", LogLevelEnum.Info);

                var parcelsCount = DataParserHelper.GetParcelsCount(directionModel);
                var parcels = new List<DPDShipmentService.ParcelVO>();
                var now = DateTime.Now.ToString("ddMMyyHHmmss");
                for (int i = 0; i < parcelsCount; i++)
                {
                    var parcelNo = i + 1;
                    parcels.Add(new DPDShipmentService.ParcelVO()
                    {
                        parcelNo = parcelNo.ToString(),
                        parcelReferenceNumber = $"{directionModel.DirectionID}_{parcelNo}_{now}",
                        weight = Convert.ToDouble(DataParserHelper.GetWeight(directionModel, parcelNo) ?? 1)
                    });
                }
                var country = DataParserHelper.GetCountry(directionModel).ToString();

                var shipmentRefNumber = $"{directionModel.DocInfo}_{directionModel.DirectionID}_{now}";
                var createShipment = new DPDShipmentService.createShipment()
                {
                    applicationType = settings.ApplicationType,
                    priceOption = DPDShipmentService.shipmentPriceOption.WithoutPrice,
                    priceOptionSpecified = true,
                    wsLang = settings.Language,
                    wsPassword = settings.Password,
                    wsUserName = settings.UserName,
                    shipmentList = new DPDShipmentService.ShipmentVO[]
                    {
                        new DPDShipmentService.ShipmentVO()
                        {
                            senderAddressId = settings.SenderAddressId,
                            mainServiceCode = settings.MainServiceCode,
                            payerId = settings.PayerId,
                            receiverAdditionalAddressInfo = address.DeliveryInst,
                            receiverCity = address.AddrCity,
                            receiverCountryCode = country,
                            receiverEmail = DataParserHelper.GetEmail(directionModel, address),
                            receiverName = address.AddrName,
                            receiverPhoneNo = address.AddrPhone,
                            receiverStreet = address.AddrLine_1,
                            receiverZipCode = address.AddrLine_2.Replace(" ", ""),
                            shipmentReferenceNumber = shipmentRefNumber,
                            shipmentReferenceNumber1 = directionModel.DocInfo,
                            shipmentReferenceNumber2 = DataParserHelper.GetInvoice(directionModel),
                            parcels = parcels.ToArray()
                        }
                    }
                };

                DPDShipmentService.AdditionalServiceVO additionalService = null;
                Func<DPDShipmentService.AdditionalServiceVO> getAdditionalService = () =>
                {
                    if (additionalService == null)
                        additionalService = new DPDShipmentService.AdditionalServiceVO();
                    return additionalService;
                };

                //set payment
                var paymentType = DataParserHelper.GetPaymentType(directionModel);
                if (paymentType == DeliveryDirection.DeliveryPaymentEnum.CAOD || paymentType == DeliveryDirection.DeliveryPaymentEnum.CHOD)
                {
                    var cod = DataParserHelper.GetCashOnDelivery(directionModel);
                    getAdditionalService().cod = new DPDShipmentService.CodVO()
                    {
                        amount = Convert.ToDouble(cod),
                        currency = DataParserHelper.GetCurrency(directionModel).ToString(),
                        paymentType = paymentType == DeliveryDirection.DeliveryPaymentEnum.CHOD ? DPDShipmentService.paymentType.Cash : DPDShipmentService.paymentType.CreditCard,
                        referenceNumber = DataParserHelper.GetPaymentReference(directionModel).ToString()
                    };
                }

                //set parcelshop if is pickup
                if (DataParserHelper.GetParcelShop(directionModel, out string parcelShopCode))
                {
                    getAdditionalService().parcelShop = new DPDShipmentService.ParcelShopShipmentVO()
                    {
                        parcelShopId = parcelShopCode,
                        fetchGsPUDOpoint = false,
                        fetchGsPUDOpointSpecified = false
                    };

                    createShipment.shipmentList[0].mainServiceCode = _SERVICE_PARCELSHOP;
                }

                if (additionalService != null)
                    createShipment.shipmentList[0].additionalServices = additionalService;

                /*Save Request */
                var saveRequest = DataParserHelper.SaveDeliveryXML(DataParserHelper.SaveXMLEnum.REQUEST, DeliveryDirection.DeliveryProviderEnum.DPD, createShipment, shipmentRefNumber, SavePathRequestResponse);
                SendToLog(this, saveRequest.Item2, saveRequest.Item1 ? LogLevelEnum.Info : LogLevelEnum.Warn);

                var client = new DPDClients.ShipmentServiceClient();
                var result = await client.createShipmentAsync(new DPDShipmentService.createShipmentRequest(createShipment));

                /*Save Response */
                var saveResponse = DataParserHelper.SaveDeliveryXML(DataParserHelper.SaveXMLEnum.RESPONSE, DeliveryDirection.DeliveryProviderEnum.DPD, result, shipmentRefNumber, SavePathRequestResponse);
                SendToLog(this, saveResponse.Item2, saveResponse.Item1 ? LogLevelEnum.Info : LogLevelEnum.Warn);

                var errors = result.createShipmentResponse.result.resultList.Where(i => i.error != null);
                if (errors.Count() > 0)
                {
                    foreach (var error in errors)
                    {
                        var msg = $"Code: {error.error.code}; Text: {error.error.text}; Solution: {error.error.solution}";
                        SendToLog(this, msg, LogLevelEnum.Warn);
                        InsertShipmentError(this, directionModel.DirectionID, msg);
                    }

                    return;
                }

                SendToLog(this, $"Create shipment done; ShipmentReferenceID: {result.createShipmentResponse.result.resultList[0].shipmentReference?.id}; " +
                    $"TransactionID: {result.createShipmentResponse.result.transactionId}; " +
                    $"ParcelReferenceNumbers: {string.Join(",", result.createShipmentResponse.result.resultList[0].parcelResultList.Select(i => i.parcelReferenceNumber))}; " +
                    $"ParcelIds {string.Join(",", result.createShipmentResponse.result.resultList[0].parcelResultList.Select(i => i.parcelId))}; ",
                    LogLevelEnum.Info);

                _requestSaveLabel = true;

                //Get label
                var resultLabel = await GetLabel(shipmentRefNumber);

                if (!resultLabel.Item1)
                {
                    InsertShipmentError(this, directionModel.DirectionID, resultLabel.Item2);
                    return;
                }

                //get info, pack numbers, truck URLs
                GetShipmentStatusEvent += (s, p, args) =>
                {
                    if (args != null)
                    {
                        var packNumber = (from a in args where a.Key.Equals(PACK_NUMBER) select a.Value).FirstOrDefault();
                        using (var db = Core.Data.ConnectionHelper.GetDB())
                        {
                            var direction = new DAL.DirectionDAL().GetDirectionAllData(directionModel.DirectionID, db);
                            direction.XtraData[Direction.XTRA_DELIVERY_PACK_NUMBER, 0] = packNumber;

                            db.Update(direction);
                            direction.XtraData.Save(db, direction.DirectionID);
                            SendToLog(this, $"Save XtraData DirectionID: {direction.DirectionID}", LogLevelEnum.Info);

                            //save to db
                            if (!InsertShipment(this, directionModel.DirectionID, shipmentRefNumber, parcels.Select(i => new Tuple<int, string>(int.Parse(i.parcelNo), packNumber.ToString())).ToArray(), out string errorText))
                            {
                                SendToLog(this, errorText, LogLevelEnum.Warn);
                                _ = DeleteShipment(shipmentRefNumber);
                            }

                        }
                    }
                };

                await GetShipmentStatus(shipmentRefNumber);

            }
            catch (DeliveryException dex)
            {
                SendToLog(this, dex.Message, LogLevelEnum.Warn, dex);
            }
            catch (Exception ex)
            {
                SendToLog(this, ex.Message, LogLevelEnum.Error, ex);
            }
        }


        public async Task<Tuple<bool, string>> GetLabel(string shipmentReference)
        {
            try
            {
                //load settings
                var settings = GetSettings();

                SendToLog(this, $"Get label; Shipment reference: {shipmentReference}", LogLevelEnum.Info);

                var getShipmentLabel = new DPDShipmentService.getShipmentLabel()
                {
                    applicationType = settings.ApplicationType,
                    wsLang = settings.Language,
                    wsPassword = settings.Password,
                    wsUserName = settings.UserName,
                    shipmentReferenceList = new DPDShipmentService.ReferenceVO[] { new DPDShipmentService.ReferenceVO() { referenceNumber = shipmentReference } },
                    printOption = DPDShipmentService.printOption.Pdf,
                    printOptionSpecified = true,
                    printFormat = DPDShipmentService.printFormat.A6,
                    printFormatSpecified = true
                };

                var result = await new DPDClients.ShipmentServiceClient().getShipmentLabelAsync(new DPDShipmentService.getShipmentLabelRequest(getShipmentLabel));

                if (result.getShipmentLabelResponse.result.error != null)
                {
                    var errorMsg = $"Code: {result.getShipmentLabelResponse.result.error.code}; Text: {result.getShipmentLabelResponse.result.error.text}; Solution: {result.getShipmentLabelResponse.result.error.solution}";
                    SendToLog(this, $"shipmentReference: {shipmentReference} error: {errorMsg}", LogLevelEnum.Warn);

                    return new Tuple<bool, string>(false, errorMsg);
                }

                if (_requestSaveLabel)
                    SaveLabelToFile(shipmentReference, result.getShipmentLabelResponse.result.pdfFile);

                InvokeLabelEvent(this, shipmentReference, result.getShipmentLabelResponse.result.pdfFile);

                var strings = shipmentReference.Split('_');
                int directionID = int.Parse(strings[1]); //the second part is directionID
                LabelPrinted(this, directionID);

                SendToLog(this, $"Get label done; TransactionID: {result.getShipmentLabelResponse.result.transactionId}", LogLevelEnum.Info);
                return new Tuple<bool, string>(true, String.Empty);
            }
            catch (Exception ex)
            {
                SendToLog(this, ex.Message, LogLevelEnum.Error, ex);
                return new Tuple<bool, string>(false, ex.Message);
            }
        }

        public async Task DeleteShipment(string shipmentReference)
        {
            try
            {
                //load settings
                var settings = GetSettings();

                SendToLog(this, $"Delete shipment; Shipment reference: {shipmentReference}", LogLevelEnum.Info);

                var deleteShipment = new DPDShipmentService.deleteShipment()
                {
                    applicationType = settings.ApplicationType,
                    wsLang = settings.Language,
                    wsPassword = settings.Password,
                    wsUserName = settings.UserName,
                    shipmentReferenceList = new DPDShipmentService.ReferenceVO[] { new DPDShipmentService.ReferenceVO() { referenceNumber = shipmentReference } }
                };

                var result = await new DPDClients.ShipmentServiceClient().deleteShipmentAsync(new DPDShipmentService.deleteShipmentRequest(deleteShipment));

                var errors = result.deleteShipmentResponse.result.resultList.Where(i => i.error != null);
                if (errors.Count() > 0)
                {
                    foreach (var error in errors)
                    {
                        SendToLog(this, $"An error occurred while trying to delete the shipment request - Code: {error.error.code}; Text: {error.error.text}; Solution: {error.error.solution}", LogLevelEnum.Warn);
                    }

                    return;
                }

                SendToLog(this, $"Delete shipment done; ShipmentReferenceID: {result.deleteShipmentResponse.result.resultList[0].shipmentReference?.id}; " +
                    $"TransactionID: {result.deleteShipmentResponse.result.transactionId}; ",
                    LogLevelEnum.Info);
            }
            catch (Exception ex)
            {
                SendToLog(this, ex.Message, LogLevelEnum.Error, ex);
            }
        }

        public async Task CreateManifest(DeliveryManifest manifest)
        {
            try
            {
                //load settings
                var settings = GetSettings();

                SendToLog(this, "Create manifest", LogLevelEnum.Info);

                //get manifest reference number
                int nextReference;
                //list of deliveryDirections
                List<DeliveryDirection> deliveryDirections = null;
                using (var db = Core.Data.ConnectionHelper.GetDB())
                {
                    deliveryDirections = new DAL.DeliveryDirectionDAL().GetByManifestID(manifest.DeliveryManifestID, db);
                    if (deliveryDirections.Count() == 0)
                    {
                        SendToLog(this, $"Create manifest; No items found", LogLevelEnum.Info);
                        return;
                    }
                    SendToLog(this, $"Create manifest; {deliveryDirections.Count()} items found", LogLevelEnum.Info);
                    var references = db.Fetch<string>("select manifestRefNumber from S4_deliveryManifest where manifestRefNumber is not null and deliveryProvider = @0", DeliveryProvider);
                    nextReference = references.Count() == 0 ? 1 : references.Select(i => int.Parse(i)).Max() + 1;
                }

                var closeManifest = new DPDManifestService.closeManifest
                {
                    applicationType = settings.ApplicationType,
                    wsLang = settings.Language,
                    wsPassword = settings.Password,
                    wsUserName = settings.UserName,
                    manifestPrintOption = DPDManifestService.manifestPrintOption.PrintOnlyManifest,
                    manifestPrintOptionSpecified = true,
                    printOption = DPDManifestService.printOption.Pdf,
                    printOptionSpecified = true,
                    manifest = new DPDManifestService.ManifestVO()
                    {
                        manifestReferenceNumber = nextReference.ToString(),
                        shipmentReferenceList = deliveryDirections
                            .Select(i => new DPDManifestService.ReferenceVO() { referenceNumber = i.DirectionID.ToString() })
                            .ToArray()
                    }
                };

                var client = new DPDClients.ManifestServiceClient();
                var result = await client.closeManifestAsync(new DPDManifestService.closeManifestRequest(closeManifest));

                if (result.closeManifestResponse.@return.error != null)
                {
                    var msg = $"Code: {result.closeManifestResponse.@return.error.code}; Text: {result.closeManifestResponse.@return.error.text}; Solution: {result.closeManifestResponse.@return.error.solution}";
                    SendToLog(this, msg, LogLevelEnum.Warn);
                    InsertManifestError(this, manifest, msg);
                    return;
                }

                InvokeManifestReportEvent(this, nextReference.ToString(), result.closeManifestResponse.@return.pdfManifestFile);
                ManifestCreated(this, manifest.DeliveryManifestID, nextReference.ToString());

                SendToLog(this, $"CreateManifest done; TransactionID: {result.closeManifestResponse.@return.transactionId}", LogLevelEnum.Info);
            }
            catch (Exception ex)
            {
                SendToLog(this, ex.Message, LogLevelEnum.Error, ex);
            }
        }

        public async Task PrintManifest(string manifestReference)
        {
            try
            {
                //load settings
                var settings = GetSettings();

                SendToLog(this, $"Print manifest; ManifestReference: {manifestReference}", LogLevelEnum.Info);

                var reprintManifest = new DPDManifestService.reprintManifest
                {
                    applicationType = settings.ApplicationType,
                    wsLang = settings.Language,
                    wsPassword = settings.Password,
                    wsUserName = settings.UserName,
                    manifestPrintOption = DPDManifestService.manifestPrintOption.PrintOnlyManifest,
                    manifestPrintOptionSpecified = true,
                    printOption = DPDManifestService.printOption.Pdf,
                    printOptionSpecified = true,
                    manifestReference = new DPDManifestService.ReferenceVO()
                    {
                        referenceNumber = manifestReference
                    }
                };

                var client = new DPDClients.ManifestServiceClient();
                var result = await client.reprintManifestAsync(new DPDManifestService.reprintManifestRequest(reprintManifest));

                if (result.reprintManifestResponse.@return.error != null)
                {
                    SendToLog(this, $"Code: {result.reprintManifestResponse.@return.error.code}; Text: {result.reprintManifestResponse.@return.error.text}; Solution: {result.reprintManifestResponse.@return.error.solution}", LogLevelEnum.Warn);

                    return;
                }

                InvokeLabelEvent(this, manifestReference, result.reprintManifestResponse.@return.pdfManifestFile);

                SendToLog(this, $"ReprintManifest done; TransactionID: {result.reprintManifestResponse.@return.transactionId}", LogLevelEnum.Info);
            }
            catch (Exception ex)
            {
                SendToLog(this, ex.Message, LogLevelEnum.Error, ex);
            }
        }

        public const string SHIP_TIME = "SHIP_TIME";
        public const string PACK_NUMBER = "PACK_NUMBER";
        public const string DELIVERY_TIME = "DELIVERY_TIME";
        public const string TRACK_URL = "TRACK_URL";
        public const string SCAN_ITEM = "SCAN_ITEM";

        public async Task GetShipmentStatus(string shipmentReference)
        {
            try
            {
                //load settings
                var settings = GetSettings();

                SendToLog(this, $"Get Shipment Status; ShipmentReference: {shipmentReference}", LogLevelEnum.Info);

                var getShipmentStatus = new DPDShipmentService.getShipmentStatus()
                {
                    applicationType = settings.ApplicationType,
                    wsLang = settings.Language,
                    wsPassword = settings.Password,
                    wsUserName = settings.UserName,
                    shipmentReferenceList = new DPDShipmentService.ReferenceVO[] { new DPDShipmentService.ReferenceVO() { referenceNumber = shipmentReference } }
                };

                var client = new DPDClients.ShipmentServiceClient();
                var result = await client.getShipmentStatusAsync(new DPDShipmentService.getShipmentStatusRequest(getShipmentStatus));

                var errors = result.getShipmentStatusResponse.result.statusInfoList.Where(i => i.error != null);
                if (errors.Count() > 0)
                {
                    foreach (var error in errors)
                    {
                        var msg = $"Code: {error.error.code}; Text: {error.error.text}; Solution: {error.error.solution}";
                        SendToLog(this, msg, LogLevelEnum.Warn);
                    }

                    return;
                }

                var content = new List<Entities.Models.LabeledValue>();
                Func<string, string, string> formatDate = (dateStr, timeStr) =>
                {
                    if (string.IsNullOrEmpty(dateStr))
                        return null;
                    if (string.IsNullOrEmpty(timeStr))
                        timeStr = "0000";
                    var dateTimeStr = $"{dateStr}{timeStr}";
                    return Core.Utils.DateUtils.GetDateByMask(dateTimeStr, "yyyyMMddHHmm")?.ToString("dd.MM.yyyy HH:mm");
                };

                foreach (var statusInfo in result.getShipmentStatusResponse.result.statusInfoList)
                {
                    content.Add(new Entities.Models.LabeledValue(SHIP_TIME, "Převzetí zásilky", formatDate(statusInfo.statusInfo.shipDate, statusInfo.statusInfo.shipTime)));
                    content.Add(new Entities.Models.LabeledValue(DELIVERY_TIME, "Doručení zásilky", formatDate(statusInfo.statusInfo.deliveryDate, statusInfo.statusInfo.deliveryTime)));
                    content.Add(new Entities.Models.LabeledValue(PACK_NUMBER, "Číslo balíku", statusInfo.statusInfo.parcelNo));
                    content.Add(new Entities.Models.LabeledValue(TRACK_URL, "Sledování zásilky", statusInfo.statusInfo.dpdUrl));
                    if (statusInfo.statusInfo.scans != null)
                    {
                        if (statusInfo.statusInfo.scans.Length > 0)
                            content.Add(new Entities.Models.LabeledValue() { Key = BEGIN_OF_GROUP });

                        var allScan = statusInfo.statusInfo.scans.ToList();
                        allScan.Reverse();
                        foreach (var scan in allScan.OrderBy(_ => int.Parse(_.time)))
                        {
                            content.Add(new Entities.Models.LabeledValue(SCAN_ITEM, "Stav", $"<div class='col-sm-2'>{formatDate(scan.date, scan.time)}</div><div class='col-sm-1'>{scan.countryCode}</div><div class='col-sm-1'>{scan.zipCode}</div><div class='col-sm-2'>{scan.city}</div><div class='col-sm-6'>{scan.scanDescription}</div>"));
                        }
                        if (statusInfo.statusInfo.scans.Length > 0)
                            content.Add(new Entities.Models.LabeledValue() { Key = END_OF_GROUP });
                    }
                }

                OnGetShipmentStatus(this, DeliveryDirection.DeliveryProviderEnum.DPD, content.ToArray());

                SendToLog(this, $"Get Shipment Status done; TransactionID: {result.getShipmentStatusResponse.result.transactionId}", LogLevelEnum.Info);
            }
            catch (Exception ex)
            {
                SendToLog(this, ex.Message, LogLevelEnum.Error, ex);
            }
        }

        /// <summary>
        /// returns settings params for communication with this provider
        /// </summary>
        /// <returns></returns>
        private (
                string ApplicationType,
                string Language,
                string Password,
                long PayerId,
                long SenderAddressId,
                string UserName,
                long MainServiceCode
                )
                GetSettings()
        {
            if (!_settingsLoaded)
            {
                var setting = Settings.Providers.Where(i => i.ProviderName == DeliveryProvider.ToString()).First();
                _settings.ApplicationType = setting.Properties[_APPLICATION_TYPE];
                _settings.Language = setting.Properties[_LANGUAGE];
                _settings.Password = setting.Properties[_PASSWORD];
                _settings.PayerId = Int64.Parse(setting.Properties[_PAYER_ID]);
                _settings.SenderAddressId = Int64.Parse(setting.Properties[_SENDER_ADDRESS_ID]);
                _settings.UserName = setting.Properties[_USER_NAME];
                _settings.MainServiceCode = Int64.Parse(setting.Properties[_MAIN_SERVICE_CODE]);

                _settingsLoaded = true;
            }
            return _settings;
        }

        private void SaveLabelToFile(string shipmentRefNumber, byte[] fileBytes)
        {
            shipmentRefNumber = shipmentRefNumber.Replace("/", "_");
            //create new dir?
            string directory = $@"{SavePathRequestResponse}\{DateTime.Now:yyyy}\{DateTime.Now:MMdd}";
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);

            var savePath = $"{directory}\\{shipmentRefNumber}_label_DPD.pdf";
            if (File.Exists(savePath))
                File.Delete(savePath);

            File.WriteAllBytes(savePath, fileBytes);

            SendToLog(this, $"Save label done; Path: {savePath}; FileLength: {fileBytes.Length}", LogLevelEnum.Info);
            _requestSaveLabel = false;
        }
    }
}

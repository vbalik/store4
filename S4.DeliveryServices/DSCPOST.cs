﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using S4.Core.Data;
using S4.Entities;
using S4.DeliveryServices.Models.CPost;
using S4.DeliveryServices.CPostClients;
using S4.Core;

namespace S4.DeliveryServices
{
    public class DSCPOST : DeliveryServiceBase, IDeliveryService
    {
        private const string _CERTIFICATE_PATH = "certificatePath";
        private const string _CERTIFICATE_PASSWORD = "certificatePassword";
        private const string _CONTRACT_ID = "contractID";
        private const string _CUSTOMER_ID = "customerID";
        private const string _POST_CODE = "postCode";
        private const string _LOCATION_NUMBER = "locationNumber";
        private const string _PREFIX_PARCEL_CODE_DR = "DR";
        private const string _PREFIX_PARCEL_CODE_NP = "NP";
        private const string _SERVICE_COD = "41";
        private const string _SERVICE_MULTI_PARCELS = "70";
        private const byte _ID_FORM = 101;
        private const byte _SHIFT_HORIZONTAL = 0;
        private const byte _SHIFT_VERTICAL = 0;
        private const string _SIZE_CATEGORY_S = "S";
        private const string _SIZE_CATEGORY_M = "M";
        private const string _SIZE_CATEGORY_L = "L";
        private const string _SIZE_CATEGORY_XL = "XL";
        private const string _LANGUAGE = "language";
        private const string _CERTIFICATE_TYPE = "cpost";

        private (ulong ContractID, string CustomerID, string PostCode, byte LocationNumber, string Language) _settings;
        private bool _settingsLoaded;

        public DeliveryDirection.DeliveryProviderEnum DeliveryProvider { get => DeliveryDirection.DeliveryProviderEnum.CPOST; }

        /// <summary>
        /// call only in case of previous initilaliyation of base
        /// </summary>
        public DSCPOST()
        {

        }

        public DSCPOST(string connectionString) : base(connectionString)
        {

        }

        public async Task<b2bSyncResponse> GetStats(DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                //load settings
                var getStats = new getStats()
                {
                    dateBegin = dateFrom.ToUniversalTime(),
                    dateEnd = dateTo.ToUniversalTime()
                };

                var client = GetPOLServiceClient(out (ulong ContractID, string CustomerID, string PostCode, byte LocationNumber, string Language) settings);
                return await client.GetStats(getStats);
            }
            catch (DeliveryException dex)
            {
                SendToLog(this, dex.Message, LogLevelEnum.Warn, dex);
            }
            catch (Exception ex)
            {
                SendToLog(this, ex.Message, LogLevelEnum.Error, ex);
            }

            return null;
        }

        public async Task CreateShipment(S4.Entities.Models.DirectionModel directionModel, Partner partner, PartnerAddress address)
        {
            try
            {
                SendToLog(this, $"Create shipment; PartnerID: {partner.PartnerID}; PartnerName: {partner.PartnerName}; PartAddrID: {address.PartAddrID}; AddrName: {address.AddrName}", LogLevelEnum.Info);

                var parcelsCount = DataParserHelper.GetParcelsCount(directionModel);
                var parcels = new List<parcelServiceSyncRequestDoPOLSyncParcelDataDoPOLSyncParcelParams>();
                for (int i = 1; i <= parcelsCount; i++)
                {
                    var parcelNo = i;
                    var notePrint = $"{directionModel.DocInfo} {address.DeliveryInst}";
                    parcels.Add(new parcelServiceSyncRequestDoPOLSyncParcelDataDoPOLSyncParcelParams()
                    {
                        recordID = i.ToString(),
                        prefixParcelCode = _PREFIX_PARCEL_CODE_DR,
                        weight = DataParserHelper.GetWeight(directionModel, parcelNo) ?? 0,
                        sequenceParcel = (sbyte)(i),
                        quantityParcel = (sbyte)parcelsCount,
                        notePrint = notePrint.Left(50)
                    });
                }

                if (parcels.Count > 0)
                {
                    var firstParcelServices = new List<parcelServiceSyncRequestDoPOLSyncParcelDataDoPOLSyncParcelParamsDoPOLParcelServices>();
                    var commonParcelServices = new List<parcelServiceSyncRequestDoPOLSyncParcelDataDoPOLSyncParcelParamsDoPOLParcelServices>()
                    {
                        new parcelServiceSyncRequestDoPOLSyncParcelDataDoPOLSyncParcelParamsDoPOLParcelServices() { service = _SIZE_CATEGORY_S }
                    };

                    if (parcels.Count > 1)
                        commonParcelServices.Add(new parcelServiceSyncRequestDoPOLSyncParcelDataDoPOLSyncParcelParamsDoPOLParcelServices() { service = _SERVICE_MULTI_PARCELS });

                    var price = 1;
                    var paymentType = DataParserHelper.GetPaymentType(directionModel);
                    if (paymentType == DeliveryDirection.DeliveryPaymentEnum.CAOD || paymentType == DeliveryDirection.DeliveryPaymentEnum.CHOD)
                    {
                        price = (int)DataParserHelper.GetCashOnDelivery(directionModel);
                        parcels[0].amount = price;
                        parcels[0].currency = DataParserHelper.GetCurrency(directionModel).ToString();
                        parcels[0].vsVoucher = DataParserHelper.GetPaymentReference(directionModel).Value;
                        firstParcelServices.Add(new parcelServiceSyncRequestDoPOLSyncParcelDataDoPOLSyncParcelParamsDoPOLParcelServices() { service = _SERVICE_COD });
                    }

                    foreach (var parcel in parcels)
                    {
                        parcel.insuredValue = price;
                    }

                    if (firstParcelServices.Count > 0 || commonParcelServices.Count > 0)
                        parcels[0].doPOLParcelServices = firstParcelServices.Union(commonParcelServices).ToArray();
                    if (commonParcelServices.Count > 0)
                        for (int i = 1; i < parcels.Count; i++)
                            parcels[i].doPOLParcelServices = commonParcelServices.ToArray();
                }

                var client = GetPOLServiceClient(out (ulong ContractID, string CustomerID, string PostCode, byte LocationNumber, string Language) settings);

                var phones = DataParserHelper.GetPhoneNumbers(address.AddrPhone);
                var parcelServiceSyncRequest = new parcelServiceSyncRequest()
                {
                    doPOLSyncParcelHeader = new parcelServiceSyncRequestDoPOLSyncParcelHeader()
                    {
                        transmissionEnd = true,
                        transmissionDate = DateTime.Today.ToString("dd.MM.yyyy"),
                        customerID = settings.CustomerID,
                        postCode = settings.PostCode,
                        locationNumber = settings.LocationNumber,
                        printParams = new parcelServiceSyncRequestDoPOLSyncParcelHeaderPrintParams()
                        {
                            idForm = _ID_FORM,
                            shiftHorizontal = _SHIFT_HORIZONTAL,
                            shiftVertical = _SHIFT_VERTICAL
                        }
                    },
                    doPOLSyncParcelData = new parcelServiceSyncRequestDoPOLSyncParcelData()
                    {
                        doPOLSyncParcelParams = parcels.ToArray(),
                        doPOLParcelAddress = new parcelServiceSyncRequestDoPOLSyncParcelDataDoPOLParcelAddress()
                        {
                            surname = address.AddrName.Left(50),
                            adviceNote = (string.IsNullOrWhiteSpace(address.AddrRemark) ? null : address.AddrRemark).Left(15),
                            city = address.AddrCity.Left(40),
                            isoCountry = DataParserHelper.GetCountry(directionModel).ToString(),
                            emailAddress = DataParserHelper.GetEmail(directionModel, address),
                            phoneNumber = phones.PhoneNumber,
                            mobileNumber = phones.PhoneNumber,
                            street = address.AddrLine_1.Left(40),
                            zipCode = address.AddrLine_2.Replace(" ", "").Left(25),
                            aditionAddress = DataParserHelper.GetReceivingPerson(directionModel, phones.PhoneNumber, true)
                        }
                    }
                };

                /*Save Request */
                var saveRequest = DataParserHelper.SaveDeliveryXML(DataParserHelper.SaveXMLEnum.REQUEST, DeliveryDirection.DeliveryProviderEnum.CPOST, parcelServiceSyncRequest, directionModel.DirectionID.ToString(), SavePathRequestResponse);
                SendToLog(this, saveRequest.Item2, saveRequest.Item1 ? LogLevelEnum.Info : LogLevelEnum.Warn);

                var result = await client.ParcelServiceSync(parcelServiceSyncRequest);

                /*Save Response */
                var saveResponse = DataParserHelper.SaveDeliveryXML(DataParserHelper.SaveXMLEnum.RESPONSE, DeliveryDirection.DeliveryProviderEnum.CPOST, result, directionModel.DirectionID.ToString(), SavePathRequestResponse);
                SendToLog(this, saveResponse.Item2, saveResponse.Item1 ? LogLevelEnum.Info : LogLevelEnum.Warn);

                if (result.serviceData.parcelServiceSyncResponse.responseHeader.resultHeader.responseCode != 1)
                {
                    var responseCode = result.serviceData.parcelServiceSyncResponse.responseHeader.resultHeader.responseCode;
                    var responseText = result.serviceData.parcelServiceSyncResponse.responseHeader.resultHeader.responseText;
                    var responseCodeSB = new StringBuilder();
                    var responseTextSB = new StringBuilder();

                    if (result.serviceData.parcelServiceSyncResponse.responseHeader.resultParcelData != null)
                    {
                        foreach (var resultParcelData in result.serviceData.parcelServiceSyncResponse.responseHeader.resultParcelData)
                        {
                            foreach (var parcelDataResponse in resultParcelData.parcelDataResponse)
                            {
                                responseCodeSB.Append(parcelDataResponse.responseCode);
                                responseCodeSB.Append(";");
                                responseTextSB.Append(parcelDataResponse.responseText);
                                responseTextSB.Append(";");
                            }
                        }
                    }

                    var msg = $"Code: {responseCode}; Chyba: {responseText} Code: {responseCodeSB.ToString()} Chyba: {responseTextSB.ToString()}";
                    SendToLog(this, msg, LogLevelEnum.Warn);
                    InsertShipmentError(this, directionModel.DirectionID, msg);

                    return;
                }

                SendToLog(this, $"Create shipment done; ShipmentReferenceID: {directionModel.DirectionID}; " +
                    $"TransactionID: {result.header.b2bRequestHeader.idExtTransaction}; " +
                    $"ParcelCodes: {string.Join(",", result.serviceData.parcelServiceSyncResponse.responseHeader.resultParcelData.Select(i => i.parcelCode))}; ",
                    LogLevelEnum.Info);

                //save to db
                if (!InsertShipment(this, directionModel.DirectionID, directionModel.DirectionID.ToString(),
                    result.serviceData.parcelServiceSyncResponse.responseHeader.resultParcelData.Select(i => new Tuple<int, string>(int.Parse(i.recordNumber), i.parcelCode)).ToArray(),
                    out string errorText))
                {
                    SendToLog(this, errorText, LogLevelEnum.Warn);
                }

                byte[] bytes = Convert.FromBase64String(result.serviceData.parcelServiceSyncResponse.responsePrintParams.file);
                InvokeLabelEvent(this, directionModel.DirectionID.ToString(), bytes);
                LabelPrinted(this, directionModel.DirectionID);

                //add xtra data
                using (var db = Core.Data.ConnectionHelper.GetDB())
                {
                    var direction = new DAL.DirectionDAL().GetDirectionAllData(directionModel.DirectionID, db);
                    //save pack numbers, truck URLs
                    var deliveryDirection = new DAL.DeliveryDirectionDAL().GetDeliveryDirectionAllData(direction.DirectionID, db);
                    var packNumbers = deliveryDirection.Items.Select(x => x.ParcelRefNumber).ToArray();

                    var position = 0;
                    foreach (var packNumber in packNumbers)
                    {
                        direction.XtraData[Direction.XTRA_DELIVERY_PACK_NUMBER, position] = packNumber;
                        position++;
                    }

                    direction.XtraData.Save(db, direction.DirectionID);
                    SendToLog(this, $"Save XtraData DirectionID: {direction.DirectionID}", LogLevelEnum.Info);
                }

            }
            catch (DeliveryException dex)
            {
                SendToLog(this, dex.Message, LogLevelEnum.Warn, dex);
                InsertShipmentError(this, directionModel.DirectionID, dex.Message);
            }
            catch (Exception ex)
            {
                SendToLog(this, ex.Message, LogLevelEnum.Error, ex);
                InsertShipmentError(this, directionModel.DirectionID, ex.Message);
            }
        }

        public async Task GetShipmentStatus(string shipmentReference)
        {
            try
            {
                using (var db = Core.Data.ConnectionHelper.GetDB())
                {
                    var directionModel = new DAL.DeliveryDirectionDAL().GetDeliveryDirectionAllData(int.Parse(shipmentReference), db);
                    await GetShipmentStatus(shipmentReference, directionModel.Items.Select(i => i.ParcelRefNumber).ToArray());
                }
            }
            catch (DeliveryException dex)
            {
                SendToLog(this, dex.Message, LogLevelEnum.Warn, dex);
            }
            catch (Exception ex)
            {
                SendToLog(this, ex.Message, LogLevelEnum.Error, ex);
            }
        }

        public async Task<Tuple<bool, string>> GetLabel(string shipmentReference)
        {
            try
            {
                using (var db = Core.Data.ConnectionHelper.GetDB())
                {
                    var directionModel = new DAL.DeliveryDirectionDAL().GetDeliveryDirectionAllData(int.Parse(shipmentReference), db);
                    var result = await GetLabel(shipmentReference, directionModel.Items.Select(i => i.ParcelRefNumber).ToArray());
                    return new Tuple<bool, string>(result.Item1, result.Item2);
                }
            }
            catch (DeliveryException dex)
            {
                SendToLog(this, dex.Message, LogLevelEnum.Warn, dex);
                return new Tuple<bool, string>(false, dex.Message);
            }
            catch (Exception ex)
            {
                SendToLog(this, ex.Message, LogLevelEnum.Error, ex);
                return new Tuple<bool, string>(false, ex.Message);
            }
        }

        public Task DeleteShipment(string shipmentReference)
        {
            //rozhraní CPOST zatím neposkytuje
            return Task.CompletedTask;
        }

        public Task CreateManifest(DeliveryManifest manifest)
        {
            throw new NotImplementedException("Create manifest is not omplemented for CPOST provider!");
        }

        public Task PrintManifest(string manifestReference)
        {
            throw new NotImplementedException("Print manifest is not omplemented for CPOST provider!");
        }

        public const string PARCEL_INFO = "PARCEL_INFO";
        public const string STATE_ITEM = "STATE_ITEM";
        private async Task GetShipmentStatus(string shipmentReference, string[] parcels)
        {
            try
            {
                SendToLog(this, $"Get Shipment Status; ShipmentReference: {shipmentReference}", LogLevelEnum.Info);

                var content = new List<Entities.Models.LabeledValue>();

                if (parcels.Count() == 0)
                {
                    //the shipment has no content - maybe had been sent other way
                    content.Add(new Entities.Models.LabeledValue(ERROR_ITEM, null, "K zásilce neexistují žádné záznamy, možná byla odeslána do systému České pošty ručně!"));
                }
                else
                {
                    var client = GetPOLServiceClient(out (ulong ContractID, string CustomerID, string PostCode, byte LocationNumber, string Language) settings);

                    var getParcelsState = new getParcelState()
                    {
                        idParcel = parcels,
                        language = _settings.Language
                    };

                    var result = await client.GetParcelState(getParcelsState);

                    foreach (var parcel in result.serviceData.getParcelStateResponse.parcel)
                    {
                        content.Add(new Entities.Models.LabeledValue() { Key = BEGIN_OF_GROUP });
                        content.Add(new Entities.Models.LabeledValue(GROUP_HEADER, null, $"<div class='col-sm-4'>Balík č.: {parcel.idParcel}</div><div class='col-sm-1'>Typ: {parcel.parcelType}</div><div class='col-sm-2'>Hmotnost: {parcel.weight}</div><div class='col-sm-3'>Počet balíků v zásilce: {parcel.quantityParcel}</div><div class='col-sm-2'>Uloženo do: {parcel.depositTo:d.M.yyyy}</div>"));
                        foreach (var state in parcel.states.OrderBy(i => i.date))
                        {
                            content.Add(new Entities.Models.LabeledValue(STATE_ITEM, "Stav", $"<div class='col-sm-2'>{state.date:dd.MM.yyyy}</div><div class='col-sm-1'>{state.postCode}</div><div class='col-sm-2'>{state.name}</div><div class='col-sm-7'>{state.text}</div>"));
                        }
                        content.Add(new Entities.Models.LabeledValue() { Key = END_OF_GROUP });
                    }

                    SendToLog(this, $"Get Shipment Status done; TransactionID: {result.header.b2bRequestHeader.idExtTransaction}", LogLevelEnum.Info);
                }

                OnGetShipmentStatus(this, DeliveryDirection.DeliveryProviderEnum.CPOST, content.ToArray());
            }
            catch (DeliveryException dex)
            {
                SendToLog(this, dex.Message, LogLevelEnum.Warn, dex);
            }
            catch (Exception ex)
            {
                SendToLog(this, ex.Message, LogLevelEnum.Error, ex);
            }
        }

        private async Task<Tuple<bool, string>> GetLabel(string shipmentReference, string[] parcels)
        {
            try
            {
                SendToLog(this, $"Get label; Shipment reference: {shipmentReference}", LogLevelEnum.Info);

                var client = GetPOLServiceClient(out (ulong ContractID, string CustomerID, string PostCode, byte LocationNumber, string Language) settings);

                var getParcelsPrinting = new getParcelsPrinting()
                {
                    doPrintingHeader = new getParcelsPrintingDoPrintingHeader()
                    {
                        customerID = settings.CustomerID,
                        idForm = _ID_FORM,
                        shiftHorizontal = _SHIFT_HORIZONTAL,
                        shiftVertical = _SHIFT_VERTICAL
                    },
                    doPrintingData = parcels
                };

                var result = await client.GetParcelsPrinting(getParcelsPrinting);
                if (result.serviceData.getParcelsPrintingResponse.doPrintingHeaderResult.doPrintingStateResponse.responseCode != 1)
                {
                    var msg = $"Code: {result.serviceData.getParcelsPrintingResponse.doPrintingHeaderResult.doPrintingStateResponse.responseCode}; Text: {result.serviceData.getParcelsPrintingResponse.doPrintingHeaderResult.doPrintingStateResponse.responseText}";
                    SendToLog(this, msg, LogLevelEnum.Warn);

                    return new Tuple<bool, string>(false, msg);
                }

                byte[] bytes = Convert.FromBase64String(result.serviceData.getParcelsPrintingResponse.doPrintingDataResult.file);

                InvokeLabelEvent(this, shipmentReference, bytes);
                LabelPrinted(this, int.Parse(shipmentReference));

                SendToLog(this, $"Get label done; TransactionID: {result.header.b2bRequestHeader.idExtTransaction}", LogLevelEnum.Info);
                return new Tuple<bool, string>(true, string.Empty);
            }
            catch (DeliveryException dex)
            {
                SendToLog(this, dex.Message, LogLevelEnum.Warn, dex);
                return new Tuple<bool, string>(false, dex.Message);
            }
            catch (Exception ex)
            {
                SendToLog(this, ex.Message, LogLevelEnum.Error, ex);
                return new Tuple<bool, string>(false, ex.Message);
            }
        }

        /// <summary>
        /// returns settings params for communication with this provider
        /// </summary>
        /// <returns></returns>
        private (ulong ContractID, string CustomerID, string PostCode, byte LocationNumber, string Language) GetSettings()
        {
            if (!_settingsLoaded)
            {
                var setting = Settings.Providers.Where(i => i.ProviderName == DeliveryProvider.ToString()).First();
                if (UInt64.TryParse(setting.Properties[_CONTRACT_ID], out ulong contractID))
                    _settings.ContractID = contractID;
                _settings.CustomerID = setting.Properties[_CUSTOMER_ID];
                _settings.PostCode = setting.Properties[_POST_CODE];
                if (Byte.TryParse(setting.Properties[_LOCATION_NUMBER], out byte locationNumber))
                    _settings.LocationNumber = locationNumber;
                _settings.Language = setting.Properties[_LANGUAGE];
                _settingsLoaded = true;
            }
            return _settings;
        }

        private POLServiceClient GetPOLServiceClient(out (ulong ContractID, string CustomerID, string PostCode, byte LocationNumber, string Language) settings)
        {
            settings = GetSettings();
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var certificate = Singleton<DAL.Cache.CertificateCache>.Instance.GetItem(_CERTIFICATE_TYPE);

                if (certificate == null)
                    throw new Exception("Nebyl nalezen žádný platný certifikát!");

                return new POLServiceClient(certificate, settings.ContractID);
            }


        }
    }
}

﻿using NPoco;
using S4.Entities;
using System;

namespace S4.DeliveryServices
{
    public abstract class DSSeriesBase
    {
        private readonly DeliveryDirection.DeliveryProviderEnum _provider;

        public DSSeriesBase(DeliveryDirection.DeliveryProviderEnum provider)
        {
            _provider = provider;
        }

        public virtual long GetParcelNumber(string productType)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var currentSerials = db.SingleOrDefault<DeliveryServiceSerial>("where DeliveryPartnerName = @0 AND status = @1 AND productType = @2", _provider.ToString(), DeliveryServiceSerial.STATUS_ACTIVE, productType);
                if (currentSerials == null)
                    throw new Exception($"No active series for {_provider}");

                // increase number
                currentSerials.CurrentNumber += 1;
                currentSerials.SeriesChanged = DateTime.Now;

                db.Update(currentSerials);

                // change series
                if (currentSerials.CurrentNumber == currentSerials.SeriesTo)
                {
                    currentSerials.Status = DeliveryServiceSerial.STATUS_CLOSED;
                    db.Update(currentSerials);

                    var serials = db.Query<DeliveryServiceSerial>()
                        .Where(_ => _.DeliveryPartnerName == _provider.ToString())
                        .Where(_ => _.ProductType == productType)
                        .Where(_ => _.Status == DeliveryServiceSerial.STATUS_UNUSED)
                        .OrderBy(_ => _.DeliveryServiceSeriesID)
                        .Limit(0,1)
                        .ToList();
                    if (serials.Count == 0)
                        throw new Exception($"No unused series found for {_provider}");

                    serials[0].Status = DeliveryServiceSerial.STATUS_ACTIVE;
                    
                    db.Update(serials[0]);
                }

                return currentSerials.CurrentNumber;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.DeliveryServices
{
    [Serializable]
    public class DeliveryException : Exception
    {
        public DeliveryException(string message)
        : base(message)
        { }
    }
}

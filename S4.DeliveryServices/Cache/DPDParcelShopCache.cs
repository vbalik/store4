﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S4.DeliveryServices.Cache
{
    public class DPDParcelShopCache
    {
        //private int _EXPIRATION = 86400;
        private int _EXPIRATION = 1;
        private DateTime _lastLoaded = DateTime.MinValue;
        private Models.parcelshops _parcelshops;

        public async Task<Models.parcelshopsParcelshop> GetByID(string id)
        {
            return (await GetParcelShops()).parcelshop.Where(i => i.id == id).FirstOrDefault();
        }

        private async Task<Models.parcelshops> GetParcelShops()
        {
            if ((DateTime.Now - _lastLoaded).TotalSeconds > _EXPIRATION)
            {
                _parcelshops = await LoadData();
            }

            return _parcelshops;
        }

        private async Task<Models.parcelshops> LoadData()
        {
            var parcelShopsXML = await new DPDClients.DPDApiClient().GetParcelShops();
            if (parcelShopsXML == null)
            {
                if (!File.Exists(XmlFilename))
                    throw new Exception("Cannot load DPDParcelShops");
                parcelShopsXML = File.ReadAllText(XmlFilename);
            }
            else
            {
                _lastLoaded = DateTime.Now;
                if (!Directory.Exists("Data"))
                    Directory.CreateDirectory("Data");
                File.WriteAllText(XmlFilename, parcelShopsXML);
            }

            return new Models.SerializerBase<Models.parcelshops>().Deserialize(parcelShopsXML);
        }

        private string XmlFilename
        {
            get => $"Data\\DPDParcelShops.xml";
        }
    }
}

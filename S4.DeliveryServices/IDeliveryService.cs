﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using S4.Entities;

namespace S4.DeliveryServices
{
    public interface IDeliveryService
    {
        DeliveryDirection.DeliveryProviderEnum DeliveryProvider { get; }
        short ErrorsLimit { get; }

        Task CreateShipment(S4.Entities.Models.DirectionModel directionModel, Partner partner, PartnerAddress address);
        Task DeleteShipment(string shipmentReference);
        Task<Tuple<bool, string>> GetLabel(string shipmentReference);
        Task CreateManifest(DeliveryManifest manifest);
        Task PrintManifest(string manifestReference);
        Task GetShipmentStatus(string shipmentReference);

        event LogEventHandler LogEvent;
    }
}

﻿using S4.Entities;

namespace S4.DeliveryServices
{
    public class DSPPLSeries : DSSeriesBase, IDSPPLSeries
    {
        public DSPPLSeries() : base(DeliveryDirection.DeliveryProviderEnum.PPL)
        {

        }
    }
}

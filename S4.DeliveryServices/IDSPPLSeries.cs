﻿namespace S4.DeliveryServices
{
    public interface IDSPPLSeries
    {
        long GetParcelNumber(string productType);
    }
}

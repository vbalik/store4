﻿using NPoco;
using S4.DAL;
using S4.Entities;
using S4.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S4.DeliveryServices
{
    public class DeliveryService : DeliveryServiceBase, IDeliveryCommon
    {
        private static string _SENDER_NAME = "senderName";
        private static string _SENDER_ADDRESS = "senderAddress";
        private static string _SENDER_ZIPCODE = "senderZipCode";
        private static string _DEFAULT_PRINTER_LOCATION_ID = "defaultPrinterLocationID";
        private static string _DEFAULT_TRACK_URL = "trackURL";

        public DeliveryService()
        {

        }

        public DeliveryService(string connectionString) : base(connectionString)
        {

        }

        public async Task ScanDirectionsForShipment()
        {
            try
            {
                SendToLog(null, "ScanDirectionsForShipment", LogLevelEnum.Info);

                using (var db = Core.Data.ConnectionHelper.GetDB())
                {
                    var directionsWithError = new DAL.DeliveryDirectionDAL().GetDirectionsWithErrors(ErrorsLimit, db);
                    SendToLog(null, $"Found {directionsWithError.Count()} DirectionsWithError", LogLevelEnum.Debug);
                    foreach (var direction in directionsWithError)
                    {
                        await CreateShipment(direction, db);
                    }

                    //directions ready to process
                    var sql = new Sql()
                        .Append("select directionID from s4_direction where docStatus = @0", Direction.DR_STATUS_DISP_CHECK_DONE)
                        .Append("and directionID in (select directionID from s4_direction_xtra where xtraCode = @0)", Direction.XTRA_TRANSPORTATION_TYPE)
                        .Append("and directionID not in (select directionID from s4_deliveryDirection where deliveryStatus = @0)", DeliveryDirection.DD_STATUS_OK);
                    var diretionIDs = db.Fetch<int>(sql);
                    var directionModels = diretionIDs.Select(i => new DAL.DirectionDAL().GetDirectionAllData(i, db));

                    SendToLog(null, $"Found {directionModels.Count()} directions ready to process", LogLevelEnum.Debug);
                    foreach (var direction in directionModels)
                    {
                        await CreateShipment(direction, db);
                    }
                }
            }
            catch (Exception ex)
            {
                SendToLog(null, ex.ToString(), LogLevelEnum.Error, ex);
            }
        }

        public async Task CreateWaitingManifests()
        {
            try
            {
                SendToLog(null, "CreateWaitingManifests", LogLevelEnum.Info);
                var manifests = new DAL.DeliveryManifestDAL().GetWaitingManifests(Core.Data.ConnectionHelper.GetDB());
                SendToLog(null, $"Found {manifests.Count()} WaitingManifests", LogLevelEnum.Debug);

                foreach (var manifest in manifests)
                {
                    await CreateManifest(manifest);
                }
            }
            catch (Exception ex)
            {
                SendToLog(null, ex.ToString(), LogLevelEnum.Error, ex);
            }
        }

        public async Task GetShipmentLabel(int directionID)
        {
            try
            {
                var direction = new DAL.DirectionDAL().GetDirectionAllData(directionID);
                var deliveryProvider = new DeliveryDirection().TTYPEToDeliveryProvider(direction.XtraData[Direction.XTRA_TRANSPORTATION_TYPE].ToString());
                if (!GetDeliveryProviders().Contains(deliveryProvider))
                {
                    SendToLog(null, $"Provider {deliveryProvider} is missing in settings!", LogLevelEnum.Warn);
                    return;
                }
                IDeliveryService deliveryService = null;
                string referenceId = null;
                switch (deliveryProvider)
                {
                    case DeliveryDirection.DeliveryProviderEnum.DPD:
                        var dsPDPD = new DSDPD();
                        dsPDPD.LogEvent += (s, m, l, e) => SendToLog(s, m, l, e);
                        dsPDPD.GetLabelEvent += (s, e) => InvokeLabelEvent(dsPDPD, e.ReferenceId, e.Data);
                        dsPDPD.LabelPrintedEvent += LabelPrinted;

                        //find reference by directionID
                        referenceId = GetReferenceByDirectionID(directionID);

                        deliveryService = dsPDPD;
                        break;

                    case DeliveryDirection.DeliveryProviderEnum.CPOST:
                        var dsCPOST = new DSCPOST();
                        dsCPOST.LogEvent += (s, m, l, e) => SendToLog(s, m, l, e);
                        dsCPOST.GetLabelEvent += (s, e) => InvokeLabelEvent(dsCPOST, e.ReferenceId, e.Data);
                        dsCPOST.LabelPrintedEvent += LabelPrinted;
                        referenceId = directionID.ToString();
                        deliveryService = dsCPOST;
                        break;

                    case DeliveryDirection.DeliveryProviderEnum.PPL:
                        var dsPPL = new DSPPL();
                        dsPPL.LogEvent += (s, m, l, e) => SendToLog(s, m, l, e);
                        dsPPL.GetLabelEvent += (s, e) => InvokeLabelEvent(dsPPL, e.ReferenceId, e.Data);
                        dsPPL.LabelPrintedEvent += LabelPrinted;
                        referenceId = directionID.ToString();
                        deliveryService = dsPPL;
                        break;

                    case DeliveryDirection.DeliveryProviderEnum.DPDNew:
                        var dsPDPD2 = new DSDPDNew();
                        dsPDPD2.LogEvent += (s, m, l, e) => SendToLog(s, m, l, e);
                        dsPDPD2.GetLabelEvent += (s, e) => InvokeLabelEvent(dsPDPD2, e.ReferenceId, e.Data);
                        dsPDPD2.LabelPrintedEvent += LabelPrinted;

                        //find reference by directionID
                        referenceId = GetReferenceByDirectionID(directionID);

                        deliveryService = dsPDPD2;
                        break;

                    default:
                        SendToLog(null, $"Delivery provider \"{deliveryProvider}\" is not implemented!", LogLevelEnum.Error);
                        break;
                }
                if (deliveryService != null)
                {
                    await deliveryService.GetLabel(referenceId);
                }
            }
            catch (Exception ex)
            {
                SendToLog(null, ex.ToString(), LogLevelEnum.Error, ex);
            }
        }

        public DeliveryDirection.DeliveryProviderEnum[] GetDeliveryProviders()
        {
            return Settings.Providers.Select(i => (DeliveryDirection.DeliveryProviderEnum)Enum.Parse(typeof(DeliveryDirection.DeliveryProviderEnum), i.ProviderName)).ToArray();
        }

        public int? GetDefaultPrinterLocationID(string deliveryProvider)
        {
            var setting = Settings.Providers.Where(i => i.ProviderName == deliveryProvider).First();
            if (setting == null) return null;

            return int.Parse(setting.Properties[_DEFAULT_PRINTER_LOCATION_ID]);
        }

        public (string name, string address, string zipCode) GetSenderAddress(string deliveryProvider)
        {
            var setting = Settings.Providers.Where(i => i.ProviderName == deliveryProvider).First();
            if (setting == null)
                return (null, null, null);

            return (setting.Properties[_SENDER_NAME], setting.Properties[_SENDER_ADDRESS], setting.Properties[_SENDER_ZIPCODE]);
        }

        public string GetTrackURL(string deliveryProvider)
        {
            var setting = Settings.Providers.Where(i => i.ProviderName == deliveryProvider).First();
            if (setting == null)
                return null;

            return setting.Properties[_DEFAULT_TRACK_URL];
        }

        public async Task GetManifestReport(DeliveryDirection.DeliveryProviderEnum deliveryProvider, string manifestRefNumber)
        {
            if (!GetDeliveryProviders().Contains(deliveryProvider))
            {
                SendToLog(null, $"Provider {deliveryProvider} is missing in settings!", LogLevelEnum.Warn);
                return;
            }
            try
            {
                IDeliveryService deliveryService = null;
                switch (deliveryProvider)
                {
                    case DeliveryDirection.DeliveryProviderEnum.DPD:
                        var dsPDPD = new DSDPD();
                        dsPDPD.LogEvent += (s, m, l, e) => SendToLog(s, m, l, e);
                        dsPDPD.GetLabelEvent += (s, e) => InvokeManifestReportEvent(dsPDPD, e.ReferenceId, e.Data);
                        deliveryService = dsPDPD;
                        break;
                    case DeliveryDirection.DeliveryProviderEnum.CPOST:
                        var dsCPOST = new DSCPOST();
                        dsCPOST.LogEvent += (s, m, l, e) => SendToLog(s, m, l, e);
                        dsCPOST.GetLabelEvent += (s, e) => InvokeManifestReportEvent(dsCPOST, e.ReferenceId, e.Data);
                        deliveryService = dsCPOST;
                        break;
                    default:
                        SendToLog(null, $"Deliverry provider \"{deliveryProvider}\" is not implemented!", LogLevelEnum.Error);
                        break;
                }

                if (deliveryService != null)
                {
                    await deliveryService.PrintManifest(manifestRefNumber);
                }
            }
            catch (Exception ex)
            {
                SendToLog(null, ex.ToString(), LogLevelEnum.Error, ex);
            }
        }

        public async Task GetShipmentStatus(int directionID, int deliveryDirectionID)
        {
            try
            {
                var direction = new DAL.DirectionDAL().GetDirectionAllData(directionID);
                var deliveryProvider = new DeliveryDirection().TTYPEToDeliveryProvider(direction.XtraData[Direction.XTRA_TRANSPORTATION_TYPE].ToString());
                if (!GetDeliveryProviders().Contains(deliveryProvider))
                {
                    SendToLog(null, $"Provider {deliveryProvider} is missing in settings!", LogLevelEnum.Warn);
                    return;
                }
                IDeliveryService deliveryService = null;
                string referenceId = null;
                switch (deliveryProvider)
                {
                    case DeliveryDirection.DeliveryProviderEnum.DPD:
                        var dsPDPD = new DSDPD();
                        dsPDPD.LogEvent += (s, m, l, e) => SendToLog(s, m, l, e);
                        dsPDPD.GetShipmentStatusEvent += (s, p, e) => OnGetShipmentStatus(s, p, e);

                        using (var db = Core.Data.ConnectionHelper.GetDB())
                        {
                            DeliveryDirection delivery = null;
                            if (deliveryDirectionID > 0)
                            {
                                delivery = new DeliveryDirectionDAL().GetByPK(deliveryDirectionID, db);
                                referenceId = delivery != null ? delivery.ShipmentRefNumber : "";
                            }
                            else
                            {
                                delivery = new DeliveryDirectionDAL().GetByDirectionID(directionID, db);
                                referenceId = delivery != null ? delivery.ShipmentRefNumber : "";
                            }
                        }

                        deliveryService = dsPDPD;
                        break;

                    case DeliveryDirection.DeliveryProviderEnum.DPDNew:
                        var dsPDPDNew = new DSDPDNew();
                        dsPDPDNew.LogEvent += (s, m, l, e) => SendToLog(s, m, l, e);
                        dsPDPDNew.GetShipmentStatusEvent += (s, p, e) => OnGetShipmentStatus(s, p, e);

                        using (var db = Core.Data.ConnectionHelper.GetDB())
                        {
                            DeliveryDirection delivery = null;
                            if (deliveryDirectionID > 0)
                            {
                                delivery = new DeliveryDirectionDAL().GetByPK(deliveryDirectionID, db);
                                referenceId = delivery != null ? delivery.ShipmentRefNumber : "";
                            }
                            else
                            {
                                delivery = new DeliveryDirectionDAL().GetByDirectionID(directionID, db);
                                referenceId = delivery != null ? delivery.ShipmentRefNumber : "";
                            }
                        }

                        deliveryService = dsPDPDNew;
                        break;

                    case DeliveryDirection.DeliveryProviderEnum.CPOST:
                        var dsCPOST = new DSCPOST();
                        dsCPOST.LogEvent += (s, m, l, e) => SendToLog(s, m, l, e);
                        dsCPOST.GetShipmentStatusEvent += (s, p, e) => OnGetShipmentStatus(s, p, e);
                        referenceId = directionID.ToString();
                        deliveryService = dsCPOST;
                        break;

                    case DeliveryDirection.DeliveryProviderEnum.PPL:
                        var dsPPL = new DSPPL();
                        dsPPL.LogEvent += (s, m, l, e) => SendToLog(s, m, l, e);
                        dsPPL.GetShipmentStatusEvent += (s, p, e) => OnGetShipmentStatus(s, p, e);
                        referenceId = directionID.ToString();
                        deliveryService = dsPPL;
                        break;

                    default:
                        SendToLog(null, $"Delivery provider \"{deliveryProvider}\" is not implemented!", LogLevelEnum.Error);
                        break;
                }
                if (deliveryService != null)
                {
                    await deliveryService.GetShipmentStatus(referenceId);
                }
            }
            catch (Exception ex)
            {
                SendToLog(null, ex.ToString(), LogLevelEnum.Error, ex);
            }
        }

        private async Task CreateShipment(DirectionModel direction, Database db)
        {
            if (direction.XtraData.IsNull(Direction.XTRA_TRANSPORTATION_TYPE))
            {
                SendToLog(null, $"Transportation type is not set in DirectionID = {direction.DirectionID}!", LogLevelEnum.Warn);
                return;
            }

            var partner = db.Single<Partner>(new Sql().Where("partnerID = @0", direction.PartnerID));
            var address = db.Single<PartnerAddress>(new Sql().Where("partAddrID = @0", direction.PartAddrID));
            IDeliveryService deliveryService = null;
            var deliveryProvider = new DeliveryDirection().TTYPEToDeliveryProvider(direction.XtraData[Direction.XTRA_TRANSPORTATION_TYPE].ToString());
            if (!GetDeliveryProviders().Contains(deliveryProvider))
            {
                SendToLog(null, $"Provider {deliveryProvider} is missing in settings! DirectionID: {direction.DirectionID}", LogLevelEnum.Warn);
                return;
            }

            //check weigh
            if (!Settings.SendWithZeroWeight)
            {
                if (!WeighIsOK(direction))
                {
                    SendToLog(null, $"Weigh is missing! DirectionID: {direction.DirectionID}", LogLevelEnum.Warn);
                    return;
                }
            }

            switch (deliveryProvider)
            {
                case DeliveryDirection.DeliveryProviderEnum.DPD:
                    var dsPDPD = new DSDPD();
                    dsPDPD.LogEvent += (s, m, l, e) => SendToLog(s, m, l, e);
                    dsPDPD.GetLabelEvent += (s, e) => InvokeLabelEvent(dsPDPD, e.ReferenceId, e.Data);
                    dsPDPD.InsertShipmentEvent += InsertShipment;
                    dsPDPD.GetManifestReportEvent += (s, e) => InvokeManifestReportEvent(dsPDPD, e.ReferenceId, e.Data);
                    dsPDPD.LabelPrintedEvent += LabelPrinted;
                    dsPDPD.ManifestCreatedEvent += ManifestCreated;
                    dsPDPD.SetShipmentFailedEvent += SetShipmentFailed;
                    deliveryService = dsPDPD;
                    break;

                case DeliveryDirection.DeliveryProviderEnum.CPOST:
                    var dsCPOST = new DSCPOST();
                    dsCPOST.LogEvent += (s, m, l, e) => SendToLog(s, m, l, e);
                    dsCPOST.GetLabelEvent += (s, e) => InvokeLabelEvent(dsCPOST, e.ReferenceId, e.Data);
                    dsCPOST.InsertShipmentEvent += InsertShipment;
                    dsCPOST.GetManifestReportEvent += (s, e) => InvokeManifestReportEvent(dsCPOST, e.ReferenceId, e.Data);
                    dsCPOST.LabelPrintedEvent += LabelPrinted;
                    dsCPOST.ManifestCreatedEvent += ManifestCreated;
                    dsCPOST.SetShipmentFailedEvent += SetShipmentFailed;
                    deliveryService = dsCPOST;
                    break;

                case DeliveryDirection.DeliveryProviderEnum.PPL:
                    var dsPPL = new DSPPL();
                    dsPPL.LogEvent += (s, m, l, e) => SendToLog(s, m, l, e);
                    dsPPL.GetLabelEvent += (s, e) => InvokeLabelEvent(dsPPL, e.ReferenceId, e.Data);
                    dsPPL.InsertShipmentEvent += InsertShipment;
                    dsPPL.GetManifestReportEvent += (s, e) => InvokeManifestReportEvent(dsPPL, e.ReferenceId, e.Data);
                    dsPPL.LabelPrintedEvent += LabelPrinted;
                    dsPPL.ManifestCreatedEvent += ManifestCreated;
                    dsPPL.SetShipmentFailedEvent += SetShipmentFailed;
                    deliveryService = dsPPL;
                    break;

                case DeliveryDirection.DeliveryProviderEnum.DPDNew:
                    var dsPDPD2 = new DSDPDNew();
                    dsPDPD2.LogEvent += (s, m, l, e) => SendToLog(s, m, l, e);
                    dsPDPD2.GetLabelEvent += (s, e) => InvokeLabelEvent(dsPDPD2, e.ReferenceId, e.Data);
                    dsPDPD2.InsertShipmentEvent += InsertShipment;
                    dsPDPD2.GetManifestReportEvent += (s, e) => InvokeManifestReportEvent(dsPDPD2, e.ReferenceId, e.Data);
                    dsPDPD2.LabelPrintedEvent += LabelPrinted;
                    dsPDPD2.ManifestCreatedEvent += ManifestCreated;
                    dsPDPD2.SetShipmentFailedEvent += SetShipmentFailed;
                    deliveryService = dsPDPD2;
                    break;
                default:
                    SendToLog(null, $"Delivery provider \"{deliveryProvider}\" is not implemented!", LogLevelEnum.Error);
                    break;
            }

            if (deliveryService != null)
            {
                await deliveryService.CreateShipment(direction, partner, address);
            }
        }

        private static string GetReferenceByDirectionID(int directionID)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var delivery = new DeliveryDirectionDAL().GetByDirectionID(directionID, db);
                return delivery != null ? delivery.ShipmentRefNumber : "";
            }
        }


        private async Task CreateManifest(DeliveryManifest manifest)
        {
            IDeliveryService deliveryService = null;
            var deliveryProvider = (DeliveryDirection.DeliveryProviderEnum)manifest.DeliveryProvider;
            if (!GetDeliveryProviders().Contains(deliveryProvider))
            {
                SendToLog(null, $"Provider {deliveryProvider} is missing in settings!", LogLevelEnum.Warn);
                return;
            }
            switch (deliveryProvider)
            {
                case DeliveryDirection.DeliveryProviderEnum.DPD:
                    var dsPDPD = new DSDPD();
                    dsPDPD.LogEvent += (s, m, l, e) => SendToLog(s, m, l, e);
                    dsPDPD.GetLabelEvent += (s, e) => InvokeLabelEvent(dsPDPD, e.ReferenceId, e.Data);
                    dsPDPD.InsertShipmentEvent += InsertShipment;
                    dsPDPD.GetManifestReportEvent += (s, e) => InvokeManifestReportEvent(dsPDPD, e.ReferenceId, e.Data);
                    dsPDPD.LabelPrintedEvent += LabelPrinted;
                    dsPDPD.ManifestCreatedEvent += ManifestCreated;
                    deliveryService = dsPDPD;
                    break;
                case DeliveryDirection.DeliveryProviderEnum.CPOST:
                    var dsCPOST = new DSCPOST();
                    dsCPOST.LogEvent += (s, m, l, e) => SendToLog(s, m, l, e);
                    dsCPOST.GetLabelEvent += (s, e) => InvokeLabelEvent(dsCPOST, e.ReferenceId, e.Data);
                    dsCPOST.InsertShipmentEvent += InsertShipment;
                    dsCPOST.GetManifestReportEvent += (s, e) => InvokeManifestReportEvent(dsCPOST, e.ReferenceId, e.Data);
                    dsCPOST.LabelPrintedEvent += LabelPrinted;
                    dsCPOST.ManifestCreatedEvent += ManifestCreated;
                    deliveryService = dsCPOST;
                    break;
                default:
                    SendToLog(null, $"Delivery provider \"{deliveryProvider}\" is not implemented!", LogLevelEnum.Error);
                    break;
            }

            if (deliveryService != null)
            {
                await deliveryService.CreateManifest(manifest);
            }
        }

        private bool WeighIsOK(DirectionModel direction)
        {
            var deliveryTypes = new List<string>() { "Doprava Česká pošta", "Doprava DPD", "Doprava DPD SK", "Doprava PPL", "Doprava PickUp" };
            var deliveryMustBeWeighed = (from x in direction.XtraData.Data where x.XtraCode.Equals(Direction.XTRA_TRANSPORTATION_TYPE) && deliveryTypes.Contains(x.XtraValue) select true).FirstOrDefault();

            if (!deliveryMustBeWeighed)
                return true;

            decimal weigh = 0;
            if (!direction.XtraData.IsNull(Direction.XTRA_DISPATCH_WEIGHT))
                decimal.TryParse(direction.XtraData[Direction.XTRA_DISPATCH_WEIGHT].ToString(), out weigh);

            if (weigh == 0)
                return false;
            else
                return true;
        }
    }
}

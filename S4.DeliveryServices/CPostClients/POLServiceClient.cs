﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Xml;
using System.Threading.Tasks;

using S4.DeliveryServices.Models;
using S4.DeliveryServices.Models.CPost;
using System.Net.Http;
using S4.Entities;

namespace S4.DeliveryServices.CPostClients
{
    public class POLServiceClient
    {
        private const string _BASE_URL = "https://b2b.postaonline.cz/services/POLService/v1/";
        private Certificate _certificate;
        private ulong _contractID;

        public POLServiceClient(Certificate certificate, ulong contractID)
        {
            _certificate = certificate;
            _contractID = contractID;
        }

        public async Task<b2bSyncResponse> ParcelServiceSync(parcelServiceSyncRequest parcelServiceSyncRequest)
        {
            var request = GetRequest();
            request.serviceData.parcelServiceSyncRequest = parcelServiceSyncRequest;
            var xmlStr = new SerializerBase<b2bRequest>().Serialize(request);
            var xmlStrRes = await GetResponse("parcelServiceSync", xmlStr);

            return GetResponse(xmlStrRes);
        }

        public async Task<b2bSyncResponse> GetParcelsPrinting(getParcelsPrinting getParcelsPrinting)
        {
            var request = GetRequest();
            request.serviceData.getParcelsPrinting = getParcelsPrinting;
            var xmlStr = new SerializerBase<b2bRequest>().Serialize(request);
            var xmlStrRes = await GetResponse("getParcelsPrinting", xmlStr);

            return GetResponse(xmlStrRes);
        }

        public async Task<b2bSyncResponse> GetParcelState(getParcelState getParcelState)
        {
            var request = GetRequest();
            request.serviceData.getParcelState = getParcelState;
            var xmlStr = new SerializerBase<b2bRequest>().Serialize(request);
            var xmlStrRes = await GetResponse("getParcelState", xmlStr);

            return GetResponse(xmlStrRes);
        }

        /// <summary>
        /// returns info of processed parcels
        /// </summary>
        /// <param name="getStats"></param>
        /// <returns></returns>
        public async Task<b2bSyncResponse> GetStats(getStats getStats)
        {
            var request = GetRequest();
            request.serviceData.getStats = getStats;
            var xmlStr = new SerializerBase<b2bRequest>().Serialize(request);
            var xmlStrRes = await GetResponse("getStats", xmlStr);

            return GetResponse(xmlStrRes);
        }

        /// <summary>
        /// returns common request parts
        /// </summary>
        /// <returns></returns>
        private b2bRequest GetRequest()
        {
            var request = new b2bRequest();
            request.header = new b2bRequestHeader()
            {
                idContract = _contractID,
                idExtTransaction = 1,
                timeStamp = DateTime.Now.ToUniversalTime(),
            };
            request.serviceData = new b2bRequestServiceData();
            return request;
        }

        /// <summary>
        /// returns response xmlstring
        /// </summary>
        /// <param name="method">endpoint</param>
        /// <param name="xmlStr">serialized xml request</param>
        /// <returns></returns>
        private async Task<string> GetResponse(string method, string xmlStr)
        {
            X509Certificate2Collection certificates = new X509Certificate2Collection();
            certificates.Import(_certificate.CertificateBinary, _certificate.CertificatePassword, X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.PersistKeySet);

            ServicePointManager.ServerCertificateValidationCallback = (a, b, c, d) => true;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(_BASE_URL + method);
            request.AllowAutoRedirect = true;
            request.ClientCertificates = certificates;
            var bytes = System.Text.Encoding.UTF8.GetBytes(xmlStr);
            request.ContentType = "text/xml; encoding='utf-8'";
            request.ContentLength = bytes.Length;
            request.Method = "POST";
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);
            requestStream.Close();

            var response = (HttpWebResponse)(await request.GetResponseAsync());
            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream responseStream = response.GetResponseStream();
                string responseStr = new StreamReader(responseStream).ReadToEnd();
                return responseStr;
            }

            return null;
        }

        /// <summary>
        /// returns deserialized response or throws an exception with error details
        /// </summary>
        /// <param name="xmlStrRes"></param>
        /// <returns></returns>
        private b2bSyncResponse GetResponse(string xmlStrRes)
        {
            if (string.IsNullOrEmpty(xmlStrRes))
                return null;

            if (GetFaultMessage(xmlStrRes, out B2BFaultMessage faultMessage))
                throw new HttpRequestException($"B2B Request Error: errorCode={faultMessage.errorCode}; errorDetail={faultMessage.errorDetail}");

            return new SerializerBase<b2bSyncResponse>().Deserialize(xmlStrRes);
        }

        /// <summary>
        /// analyses result and tests if fault has been returned
        /// </summary>
        /// <param name="strResult">raw result xml string</param>
        /// <param name="faultMessage">faultMessage object if fault has been returned</param>
        /// <returns>true if error is returned</returns>
        private bool GetFaultMessage(string strResult, out B2BFaultMessage faultMessage)
        {
            var xml = new XmlDocument();
            xml.LoadXml(strResult);
            if (xml.DocumentElement.Name.Contains(nameof(B2BFaultMessage)))
            {
                faultMessage = new SerializerBase<B2BFaultMessage>().Deserialize(strResult);
                return true;
            }

            faultMessage = null;
            return false;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.DeliveryServices.Models
{
    public class DPDOpeningHours
    {
        public int Day { get; set; }
        public string DayName { get; set; }
        public string OpenMorning { get; set; }
        public string CloseMorning { get; set; }
        public string OpenAfternoon { get; set; }
        public string CloseAfternoon { get; set; }
    }
}

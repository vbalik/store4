﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.DeliveryServices.Models
{
    public class ProviderSettingModel
    {
        public string ProviderName { get; set; }
        public Dictionary<string, string> Properties { get; set; }
    }
}

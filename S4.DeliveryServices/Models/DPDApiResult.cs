﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.DeliveryServices.Models
{
    public class DPDApiResult<T> where T: new()
    {
        public string Status { get; set; }
        public int Code { get; set; }
        public int Count { get; set; }
        public string Hash { get; set; }
        public T Data { get; set; }
    }
}

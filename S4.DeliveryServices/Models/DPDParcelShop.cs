﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.DeliveryServices.Models
{
    public class DPDParcelShop
    {
        public string ID { get; set; }
        public string Company { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string HouseNumber { get; set; }
        public string Postcode { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Homepage { get; set; }
        public bool Pickup_allowed { get; set; }
        public bool Return_allowed { get; set; }
        public bool Express_allowed { get; set; }
        public bool Cardpayment_allowed { get; set; }
        public bool Cod_allowed { get; set; }
        public int Service { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public DPDOpeningHours[] Hours { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.DeliveryServices.Models
{
    public class DeliveryServicesSettings
    {
        public short ErrorLimit { get; set; }
        public List<ProviderSettingModel> Providers { get; set; } = new List<ProviderSettingModel>();
        public string SavePathRequestResponse { get; set; }
        public bool SendWithZeroWeight { get; set; } = false;
    }
}

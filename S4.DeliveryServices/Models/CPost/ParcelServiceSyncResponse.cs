﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.DeliveryServices.Models.CPost
{

    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1", IsNullable = false)]
    public partial class parcelServiceSyncResponse
    {

        private parcelServiceSyncResponseResponseHeader responseHeaderField;

        private parcelServiceSyncResponseResponsePrintParams responsePrintParamsField;

        /// <remarks/>
        public parcelServiceSyncResponseResponseHeader responseHeader
        {
            get
            {
                return this.responseHeaderField;
            }
            set
            {
                this.responseHeaderField = value;
            }
        }

        /// <remarks/>
        public parcelServiceSyncResponseResponsePrintParams responsePrintParams
        {
            get
            {
                return this.responsePrintParamsField;
            }
            set
            {
                this.responsePrintParamsField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1")]
    public partial class parcelServiceSyncResponseResponseHeader
    {

        private parcelServiceSyncResponseResponseHeaderResultHeader resultHeaderField;

        private parcelServiceSyncResponseResponseHeaderResultParcelData[] resultParcelDataField;

        private parcelServiceSyncResponseResponseHeaderResultParcelCustomsGoods[] resultParcelCustomsGoodsField;

        private parcelServiceSyncResponseResponseHeaderResultParcelCustomsDocuments[] resultParcelCustomsDocumentsField;

        /// <remarks/>
        public parcelServiceSyncResponseResponseHeaderResultHeader resultHeader
        {
            get
            {
                return this.resultHeaderField;
            }
            set
            {
                this.resultHeaderField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("resultParcelData")]
        public parcelServiceSyncResponseResponseHeaderResultParcelData[] resultParcelData
        {
            get
            {
                return this.resultParcelDataField;
            }
            set
            {
                this.resultParcelDataField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("resultParcelCustomsGoods")]
        public parcelServiceSyncResponseResponseHeaderResultParcelCustomsGoods[] resultParcelCustomsGoods
        {
            get
            {
                return this.resultParcelCustomsGoodsField;
            }
            set
            {
                this.resultParcelCustomsGoodsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("resultParcelCustomsDocuments")]
        public parcelServiceSyncResponseResponseHeaderResultParcelCustomsDocuments[] resultParcelCustomsDocuments
        {
            get
            {
                return this.resultParcelCustomsDocumentsField;
            }
            set
            {
                this.resultParcelCustomsDocumentsField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1")]
    public partial class parcelServiceSyncResponseResponseHeaderResultHeader
    {

        private byte responseCodeField;

        private string responseTextField;

        /// <remarks/>
        public byte responseCode
        {
            get
            {
                return this.responseCodeField;
            }
            set
            {
                this.responseCodeField = value;
            }
        }

        /// <remarks/>
        public string responseText
        {
            get
            {
                return this.responseTextField;
            }
            set
            {
                this.responseTextField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1")]
    public partial class parcelServiceSyncResponseResponseHeaderResultParcelData
    {

        private string recordNumberField;

        private string parcelCodeField;

        private parcelServiceSyncResponseResponseHeaderResultParcelDataParcelDataResponse[] parcelDataResponseField;

        /// <remarks/>
        public string recordNumber
        {
            get
            {
                return this.recordNumberField;
            }
            set
            {
                this.recordNumberField = value;
            }
        }

        /// <remarks/>
        public string parcelCode
        {
            get
            {
                return this.parcelCodeField;
            }
            set
            {
                this.parcelCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("parcelDataResponse")]
        public parcelServiceSyncResponseResponseHeaderResultParcelDataParcelDataResponse[] parcelDataResponse
        {
            get
            {
                return this.parcelDataResponseField;
            }
            set
            {
                this.parcelDataResponseField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1")]
    public partial class parcelServiceSyncResponseResponseHeaderResultParcelDataParcelDataResponse
    {

        private short responseCodeField;

        private string responseTextField;

        /// <remarks/>
        public short responseCode
        {
            get
            {
                return this.responseCodeField;
            }
            set
            {
                this.responseCodeField = value;
            }
        }

        /// <remarks/>
        public string responseText
        {
            get
            {
                return this.responseTextField;
            }
            set
            {
                this.responseTextField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1")]
    public partial class parcelServiceSyncResponseResponseHeaderResultParcelCustomsGoods
    {

        private byte sequenceField;

        private parcelServiceSyncResponseResponseHeaderResultParcelCustomsGoodsCustomsGoodsResponse[] customsGoodsResponseField;

        /// <remarks/>
        public byte sequence
        {
            get
            {
                return this.sequenceField;
            }
            set
            {
                this.sequenceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("customsGoodsResponse")]
        public parcelServiceSyncResponseResponseHeaderResultParcelCustomsGoodsCustomsGoodsResponse[] customsGoodsResponse
        {
            get
            {
                return this.customsGoodsResponseField;
            }
            set
            {
                this.customsGoodsResponseField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1")]
    public partial class parcelServiceSyncResponseResponseHeaderResultParcelCustomsGoodsCustomsGoodsResponse
    {

        private short responseCodeField;

        private string responseTextField;

        /// <remarks/>
        public short responseCode
        {
            get
            {
                return this.responseCodeField;
            }
            set
            {
                this.responseCodeField = value;
            }
        }

        /// <remarks/>
        public string responseText
        {
            get
            {
                return this.responseTextField;
            }
            set
            {
                this.responseTextField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1")]
    public partial class parcelServiceSyncResponseResponseHeaderResultParcelCustomsDocuments
    {

        private string recordIDField;

        private parcelServiceSyncResponseResponseHeaderResultParcelCustomsDocumentsCustomsDocumentsResponse[] customsDocumentsResponseField;

        /// <remarks/>
        public string recordID
        {
            get
            {
                return this.recordIDField;
            }
            set
            {
                this.recordIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("customsDocumentsResponse")]
        public parcelServiceSyncResponseResponseHeaderResultParcelCustomsDocumentsCustomsDocumentsResponse[] customsDocumentsResponse
        {
            get
            {
                return this.customsDocumentsResponseField;
            }
            set
            {
                this.customsDocumentsResponseField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1")]
    public partial class parcelServiceSyncResponseResponseHeaderResultParcelCustomsDocumentsCustomsDocumentsResponse
    {

        private short responseCodeField;

        private string responseTextField;

        /// <remarks/>
        public short responseCode
        {
            get
            {
                return this.responseCodeField;
            }
            set
            {
                this.responseCodeField = value;
            }
        }

        /// <remarks/>
        public string responseText
        {
            get
            {
                return this.responseTextField;
            }
            set
            {
                this.responseTextField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1")]
    public partial class parcelServiceSyncResponseResponsePrintParams
    {

        private string fileField;

        private parcelServiceSyncResponseResponsePrintParamsPrintParamsResponse[] printParamsResponseField;

        /// <remarks/>
        public string file
        {
            get
            {
                return this.fileField;
            }
            set
            {
                this.fileField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("printParamsResponse")]
        public parcelServiceSyncResponseResponsePrintParamsPrintParamsResponse[] printParamsResponse
        {
            get
            {
                return this.printParamsResponseField;
            }
            set
            {
                this.printParamsResponseField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1")]
    public partial class parcelServiceSyncResponseResponsePrintParamsPrintParamsResponse
    {

        private short responseCodeField;

        private string responseTextField;

        /// <remarks/>
        public short responseCode
        {
            get
            {
                return this.responseCodeField;
            }
            set
            {
                this.responseCodeField = value;
            }
        }

        /// <remarks/>
        public string responseText
        {
            get
            {
                return this.responseTextField;
            }
            set
            {
                this.responseTextField = value;
            }
        }
    }


}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.DeliveryServices.Models.CPost
{
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1", IsNullable = false)]
    public partial class getStatsResponse
    {

        private byte importAllField;

        private byte importErrField;

        private byte importOkField;

        private byte parcelsField;

        /// <remarks/>
        public byte importAll
        {
            get
            {
                return this.importAllField;
            }
            set
            {
                this.importAllField = value;
            }
        }

        /// <remarks/>
        public byte importErr
        {
            get
            {
                return this.importErrField;
            }
            set
            {
                this.importErrField = value;
            }
        }

        /// <remarks/>
        public byte importOk
        {
            get
            {
                return this.importOkField;
            }
            set
            {
                this.importOkField = value;
            }
        }

        /// <remarks/>
        public byte parcels
        {
            get
            {
                return this.parcelsField;
            }
            set
            {
                this.parcelsField = value;
            }
        }
    }
}

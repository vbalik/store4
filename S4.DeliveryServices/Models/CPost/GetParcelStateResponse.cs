﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.DeliveryServices.Models.CPost
{

    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1", IsNullable = false)]
    public partial class getParcelStateResponse
    {

        private getParcelStateResponseParcel[] parcelField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("parcel")]
        public getParcelStateResponseParcel[] parcel
        {
            get
            {
                return this.parcelField;
            }
            set
            {
                this.parcelField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1")]
    public partial class getParcelStateResponseParcel
    {

        private string idParcelField;

        private string parcelTypeField;

        private string weightField;

        private string amountField;

        private string currencyField;

        private string quantityParcelField;

        private System.DateTime depositToField;

        private string timeDepositField;

        private string countryOfOriginField;

        private string countryOfDestinationField;

        private getParcelStateResponseParcelState[] statesField;

        /// <remarks/>
        public string idParcel
        {
            get
            {
                return this.idParcelField;
            }
            set
            {
                this.idParcelField = value;
            }
        }

        /// <remarks/>
        public string parcelType
        {
            get
            {
                return this.parcelTypeField;
            }
            set
            {
                this.parcelTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "integer")]
        public string weight
        {
            get
            {
                return this.weightField;
            }
            set
            {
                this.weightField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "integer")]
        public string amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>
        public string currency
        {
            get
            {
                return this.currencyField;
            }
            set
            {
                this.currencyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "integer")]
        public string quantityParcel
        {
            get
            {
                return this.quantityParcelField;
            }
            set
            {
                this.quantityParcelField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime depositTo
        {
            get
            {
                return this.depositToField;
            }
            set
            {
                this.depositToField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "integer")]
        public string timeDeposit
        {
            get
            {
                return this.timeDepositField;
            }
            set
            {
                this.timeDepositField = value;
            }
        }

        /// <remarks/>
        public string countryOfOrigin
        {
            get
            {
                return this.countryOfOriginField;
            }
            set
            {
                this.countryOfOriginField = value;
            }
        }

        /// <remarks/>
        public string countryOfDestination
        {
            get
            {
                return this.countryOfDestinationField;
            }
            set
            {
                this.countryOfDestinationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("state", IsNullable = false)]
        public getParcelStateResponseParcelState[] states
        {
            get
            {
                return this.statesField;
            }
            set
            {
                this.statesField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1")]
    public partial class getParcelStateResponseParcelState
    {

        private string idField;

        private System.DateTime dateField;

        private string textField;

        private string postCodeField;

        private string nameField;

        /// <remarks/>
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>
        public string text
        {
            get
            {
                return this.textField;
            }
            set
            {
                this.textField = value;
            }
        }

        /// <remarks/>
        public string postCode
        {
            get
            {
                return this.postCodeField;
            }
            set
            {
                this.postCodeField = value;
            }
        }

        /// <remarks/>
        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }
    }


}

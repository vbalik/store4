﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.DeliveryServices.Models.CPost
{
    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1", IsNullable = false)]
    public partial class getParcelState
    {

        private string[] idParcelField;

        private string languageField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("idParcel")]
        public string[] idParcel
        {
            get
            {
                return this.idParcelField;
            }
            set
            {
                this.idParcelField = value;
            }
        }

        /// <remarks/>
        public string language
        {
            get
            {
                return this.languageField;
            }
            set
            {
                this.languageField = value;
            }
        }
    }
}

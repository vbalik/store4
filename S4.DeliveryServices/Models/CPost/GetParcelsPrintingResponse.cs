﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.DeliveryServices.Models.CPost
{

    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1", IsNullable = false)]
    public partial class getParcelsPrintingResponse
    {

        private getParcelsPrintingResponseDoPrintingHeaderResult doPrintingHeaderResultField;

        private getParcelsPrintingResponseDoPrintingDataResult doPrintingDataResultField;

        /// <remarks/>
        public getParcelsPrintingResponseDoPrintingHeaderResult doPrintingHeaderResult
        {
            get
            {
                return this.doPrintingHeaderResultField;
            }
            set
            {
                this.doPrintingHeaderResultField = value;
            }
        }

        /// <remarks/>
        public getParcelsPrintingResponseDoPrintingDataResult doPrintingDataResult
        {
            get
            {
                return this.doPrintingDataResultField;
            }
            set
            {
                this.doPrintingDataResultField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1")]
    public partial class getParcelsPrintingResponseDoPrintingHeaderResult
    {

        private getParcelsPrintingResponseDoPrintingHeaderResultDoPrintingHeader doPrintingHeaderField;

        private getParcelsPrintingResponseDoPrintingHeaderResultDoPrintingStateResponse doPrintingStateResponseField;

        /// <remarks/>
        public getParcelsPrintingResponseDoPrintingHeaderResultDoPrintingHeader doPrintingHeader
        {
            get
            {
                return this.doPrintingHeaderField;
            }
            set
            {
                this.doPrintingHeaderField = value;
            }
        }

        /// <remarks/>
        public getParcelsPrintingResponseDoPrintingHeaderResultDoPrintingStateResponse doPrintingStateResponse
        {
            get
            {
                return this.doPrintingStateResponseField;
            }
            set
            {
                this.doPrintingStateResponseField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1")]
    public partial class getParcelsPrintingResponseDoPrintingHeaderResultDoPrintingHeader
    {

        private string customerIDField;

        private string contractNumberField;

        private byte idFormField;

        private byte shiftHorizontalField;

        private byte shiftVerticalField;

        private byte positionField;

        /// <remarks/>
        public string customerID
        {
            get
            {
                return this.customerIDField;
            }
            set
            {
                this.customerIDField = value;
            }
        }

        /// <remarks/>
        public string contractNumber
        {
            get
            {
                return this.contractNumberField;
            }
            set
            {
                this.contractNumberField = value;
            }
        }

        /// <remarks/>
        public byte idForm
        {
            get
            {
                return this.idFormField;
            }
            set
            {
                this.idFormField = value;
            }
        }

        /// <remarks/>
        public byte shiftHorizontal
        {
            get
            {
                return this.shiftHorizontalField;
            }
            set
            {
                this.shiftHorizontalField = value;
            }
        }

        /// <remarks/>
        public byte shiftVertical
        {
            get
            {
                return this.shiftVerticalField;
            }
            set
            {
                this.shiftVerticalField = value;
            }
        }

        /// <remarks/>
        public byte position
        {
            get
            {
                return this.positionField;
            }
            set
            {
                this.positionField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1")]
    public partial class getParcelsPrintingResponseDoPrintingHeaderResultDoPrintingStateResponse
    {

        private byte responseCodeField;

        private string responseTextField;

        /// <remarks/>
        public byte responseCode
        {
            get
            {
                return this.responseCodeField;
            }
            set
            {
                this.responseCodeField = value;
            }
        }

        /// <remarks/>
        public string responseText
        {
            get
            {
                return this.responseTextField;
            }
            set
            {
                this.responseTextField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1")]
    public partial class getParcelsPrintingResponseDoPrintingDataResult
    {

        private string fileField;

        /// <remarks/>
        public string file
        {
            get
            {
                return this.fileField;
            }
            set
            {
                this.fileField = value;
            }
        }
    }


}

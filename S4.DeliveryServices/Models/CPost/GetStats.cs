﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.DeliveryServices.Models.CPost
{
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1", IsNullable = false)]
    public partial class getStats
    {

        private System.DateTime dateBeginField;

        private System.DateTime dateEndField;

        /// <remarks/>
        public System.DateTime dateBegin
        {
            get
            {
                return this.dateBeginField;
            }
            set
            {
                this.dateBeginField = value;
            }
        }

        /// <remarks/>
        public System.DateTime dateEnd
        {
            get
            {
                return this.dateEndField;
            }
            set
            {
                this.dateEndField = value;
            }
        }
    }
}

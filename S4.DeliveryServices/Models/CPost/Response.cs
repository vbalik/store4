﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.DeliveryServices.Models.CPost
{

    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://b2b.postaonline.cz/schema/B2BCommon-v1")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "https://b2b.postaonline.cz/schema/B2BCommon-v1", IsNullable = false)]
    public partial class b2bSyncResponse
    {

        private b2bSyncResponseHeader headerField;

        private b2bSyncResponseServiceData serviceDataField;

        /// <remarks/>
        public b2bSyncResponseHeader header
        {
            get
            {
                return this.headerField;
            }
            set
            {
                this.headerField = value;
            }
        }

        /// <remarks/>
        public b2bSyncResponseServiceData serviceData
        {
            get
            {
                return this.serviceDataField;
            }
            set
            {
                this.serviceDataField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://b2b.postaonline.cz/schema/B2BCommon-v1")]
    public partial class b2bSyncResponseHeader
    {

        private System.DateTime timeStampField;

        private b2bSyncResponseHeaderB2bRequestHeader b2bRequestHeaderField;

        /// <remarks/>
        public System.DateTime timeStamp
        {
            get
            {
                return this.timeStampField;
            }
            set
            {
                this.timeStampField = value;
            }
        }

        /// <remarks/>
        public b2bSyncResponseHeaderB2bRequestHeader b2bRequestHeader
        {
            get
            {
                return this.b2bRequestHeaderField;
            }
            set
            {
                this.b2bRequestHeaderField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://b2b.postaonline.cz/schema/B2BCommon-v1")]
    public partial class b2bSyncResponseHeaderB2bRequestHeader
    {

        private byte idExtTransactionField;

        private System.DateTime timeStampField;

        private ulong idContractField;

        /// <remarks/>
        public byte idExtTransaction
        {
            get
            {
                return this.idExtTransactionField;
            }
            set
            {
                this.idExtTransactionField = value;
            }
        }

        /// <remarks/>
        public System.DateTime timeStamp
        {
            get
            {
                return this.timeStampField;
            }
            set
            {
                this.timeStampField = value;
            }
        }

        /// <remarks/>
        public ulong idContract
        {
            get
            {
                return this.idContractField;
            }
            set
            {
                this.idContractField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://b2b.postaonline.cz/schema/B2BCommon-v1")]
    public partial class b2bSyncResponseServiceData
    {

        private getStatsResponse getStatsResponseField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1")]
        public getStatsResponse getStatsResponse
        {
            get
            {
                return this.getStatsResponseField;
            }
            set
            {
                this.getStatsResponseField = value;
            }
        }

        private parcelServiceSyncResponse parcelServiceSyncResponseField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1")]
        public parcelServiceSyncResponse parcelServiceSyncResponse
        {
            get
            {
                return this.parcelServiceSyncResponseField;
            }
            set
            {
                this.parcelServiceSyncResponseField = value;
            }
        }

        private getParcelsPrintingResponse getParcelsPrintingResponseField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1")]
        public getParcelsPrintingResponse getParcelsPrintingResponse
        {
            get
            {
                return this.getParcelsPrintingResponseField;
            }
            set
            {
                this.getParcelsPrintingResponseField = value;
            }
        }

        private getParcelStateResponse getParcelStateResponseField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1")]
        public getParcelStateResponse getParcelStateResponse
        {
            get
            {
                return this.getParcelStateResponseField;
            }
            set
            {
                this.getParcelStateResponseField = value;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.DeliveryServices.Models.CPost
{

    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1", IsNullable = false)]
    public partial class getParcelsPrinting
    {

        private getParcelsPrintingDoPrintingHeader doPrintingHeaderField;

        private string[] doPrintingDataField;

        /// <remarks/>
        public getParcelsPrintingDoPrintingHeader doPrintingHeader
        {
            get
            {
                return this.doPrintingHeaderField;
            }
            set
            {
                this.doPrintingHeaderField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("parcelCode", IsNullable = false)]
        public string[] doPrintingData
        {
            get
            {
                return this.doPrintingDataField;
            }
            set
            {
                this.doPrintingDataField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1")]
    public partial class getParcelsPrintingDoPrintingHeader
    {

        private string customerIDField;

        private string contractNumberField;

        private byte idFormField;

        private byte shiftHorizontalField;

        private byte shiftVerticalField;

        private byte positionField;

        /// <remarks/>
        public string customerID
        {
            get
            {
                return this.customerIDField;
            }
            set
            {
                this.customerIDField = value;
            }
        }

        /// <remarks/>
        public string contractNumber
        {
            get
            {
                return this.contractNumberField;
            }
            set
            {
                this.contractNumberField = value;
            }
        }

        /// <remarks/>
        public byte idForm
        {
            get
            {
                return this.idFormField;
            }
            set
            {
                this.idFormField = value;
            }
        }

        /// <remarks/>
        public byte shiftHorizontal
        {
            get
            {
                return this.shiftHorizontalField;
            }
            set
            {
                this.shiftHorizontalField = value;
            }
        }

        /// <remarks/>
        public byte shiftVertical
        {
            get
            {
                return this.shiftVerticalField;
            }
            set
            {
                this.shiftVerticalField = value;
            }
        }

        /// <remarks/>
        public byte position
        {
            get
            {
                return this.positionField;
            }
            set
            {
                this.positionField = value;
            }
        }
    }


}

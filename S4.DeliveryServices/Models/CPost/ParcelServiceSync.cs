﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace S4.DeliveryServices.Models.CPost
{

    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1", IsNullable = false)]
    public partial class parcelServiceSyncRequest
    {

        private parcelServiceSyncRequestDoPOLSyncParcelHeader doPOLSyncParcelHeaderField;

        private parcelServiceSyncRequestDoPOLSyncParcelData doPOLSyncParcelDataField;

        /// <remarks/>
        public parcelServiceSyncRequestDoPOLSyncParcelHeader doPOLSyncParcelHeader
        {
            get
            {
                return this.doPOLSyncParcelHeaderField;
            }
            set
            {
                this.doPOLSyncParcelHeaderField = value;
            }
        }

        /// <remarks/>
        public parcelServiceSyncRequestDoPOLSyncParcelData doPOLSyncParcelData
        {
            get
            {
                return this.doPOLSyncParcelDataField;
            }
            set
            {
                this.doPOLSyncParcelDataField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1")]
    public partial class parcelServiceSyncRequestDoPOLSyncParcelHeader
    {

        private string transmissionDateField;

        private string customerIDField;

        private string postCodeField;

        private string contractNumberField;

        private string frankingNumberField;

        private bool transmissionEndField;

        private byte locationNumberField;

        private string senderCustCardNumField;

        private parcelServiceSyncRequestDoPOLSyncParcelHeaderPrintParams printParamsField;

        /// <remarks/>
        public string transmissionDate
        {
            get
            {
                return this.transmissionDateField;
            }
            set
            {
                this.transmissionDateField = value;
            }
        }

        /// <remarks/>
        public string customerID
        {
            get
            {
                return this.customerIDField;
            }
            set
            {
                this.customerIDField = value;
            }
        }

        /// <remarks/>
        public string postCode
        {
            get
            {
                return this.postCodeField;
            }
            set
            {
                this.postCodeField = value;
            }
        }

        /// <remarks/>
        public string contractNumber
        {
            get
            {
                return this.contractNumberField;
            }
            set
            {
                this.contractNumberField = value;
            }
        }

        /// <remarks/>
        public string frankingNumber
        {
            get
            {
                return this.frankingNumberField;
            }
            set
            {
                this.frankingNumberField = value;
            }
        }

        /// <remarks/>
        public bool transmissionEnd
        {
            get
            {
                return this.transmissionEndField;
            }
            set
            {
                this.transmissionEndField = value;
            }
        }

        /// <remarks/>
        public byte locationNumber
        {
            get
            {
                return this.locationNumberField;
            }
            set
            {
                this.locationNumberField = value;
            }
        }

        /// <remarks/>
        public string senderCustCardNum
        {
            get
            {
                return this.senderCustCardNumField;
            }
            set
            {
                this.senderCustCardNumField = value;
            }
        }

        /// <remarks/>
        public parcelServiceSyncRequestDoPOLSyncParcelHeaderPrintParams printParams
        {
            get
            {
                return this.printParamsField;
            }
            set
            {
                this.printParamsField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1")]
    public partial class parcelServiceSyncRequestDoPOLSyncParcelHeaderPrintParams
    {

        private byte idFormField;

        private byte shiftHorizontalField;

        private byte shiftVerticalField;

        private byte positionField;

        /// <remarks/>
        public byte idForm
        {
            get
            {
                return this.idFormField;
            }
            set
            {
                this.idFormField = value;
            }
        }

        /// <remarks/>
        public byte shiftHorizontal
        {
            get
            {
                return this.shiftHorizontalField;
            }
            set
            {
                this.shiftHorizontalField = value;
            }
        }

        /// <remarks/>
        public byte shiftVertical
        {
            get
            {
                return this.shiftVerticalField;
            }
            set
            {
                this.shiftVerticalField = value;
            }
        }

        /// <remarks/>
        [DefaultValue(0)]
        public byte position
        {
            get
            {
                return this.positionField;
            }
            set
            {
                this.positionField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1")]
    public partial class parcelServiceSyncRequestDoPOLSyncParcelData
    {

        private parcelServiceSyncRequestDoPOLSyncParcelDataDoPOLSyncParcelParams[] doPOLSyncParcelParamsField;

        private parcelServiceSyncRequestDoPOLSyncParcelDataDoPOLParcelAddress doPOLParcelAddressField;

        private parcelServiceSyncRequestDoPOLSyncParcelDataDoPOLParcelAddressDocument doPOLParcelAddressDocumentField;

        private parcelServiceSyncRequestDoPOLSyncParcelDataDoPOLParcelCustomsDeclaration doPOLParcelCustomsDeclarationField;

        private parcelServiceSyncRequestDoPOLSyncParcelDataDoPOLParcelCustomsDocument[] doPOLParcelCustomsDocumentField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("doPOLSyncParcelParams")]
        public parcelServiceSyncRequestDoPOLSyncParcelDataDoPOLSyncParcelParams[] doPOLSyncParcelParams
        {
            get
            {
                return this.doPOLSyncParcelParamsField;
            }
            set
            {
                this.doPOLSyncParcelParamsField = value;
            }
        }

        /// <remarks/>
        public parcelServiceSyncRequestDoPOLSyncParcelDataDoPOLParcelAddress doPOLParcelAddress
        {
            get
            {
                return this.doPOLParcelAddressField;
            }
            set
            {
                this.doPOLParcelAddressField = value;
            }
        }

        /// <remarks/>
        public parcelServiceSyncRequestDoPOLSyncParcelDataDoPOLParcelAddressDocument doPOLParcelAddressDocument
        {
            get
            {
                return this.doPOLParcelAddressDocumentField;
            }
            set
            {
                this.doPOLParcelAddressDocumentField = value;
            }
        }

        /// <remarks/>
        public parcelServiceSyncRequestDoPOLSyncParcelDataDoPOLParcelCustomsDeclaration doPOLParcelCustomsDeclaration
        {
            get
            {
                return this.doPOLParcelCustomsDeclarationField;
            }
            set
            {
                this.doPOLParcelCustomsDeclarationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("doPOLParcelCustomsDocument")]
        public parcelServiceSyncRequestDoPOLSyncParcelDataDoPOLParcelCustomsDocument[] doPOLParcelCustomsDocument
        {
            get
            {
                return this.doPOLParcelCustomsDocumentField;
            }
            set
            {
                this.doPOLParcelCustomsDocumentField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1")]
    public partial class parcelServiceSyncRequestDoPOLSyncParcelDataDoPOLSyncParcelParams
    {

        private string recordIDField;

        private string parcelCodeField;

        private string prefixParcelCodeField;

        private decimal weightField;

        private int insuredValueField;

        private int amountField;

        private string currencyField;

        private long vsVoucherField;

        private long vsParcelField;

        private sbyte sequenceParcelField;

        private sbyte quantityParcelField;

        private string noteField;

        private string notePrintField;

        private short lengthField;

        private short widthField;

        private short heightField;

        private string mrnField;

        private string referenceNumberField;

        private byte palletsField;

        private string specSymField;

        private string note2Field;

        private string numSignField;

        private string scoreField;

        private string orderNumberZPROField;

        private string returnNumDaysField;

        private parcelServiceSyncRequestDoPOLSyncParcelDataDoPOLSyncParcelParamsDoPOLParcelServices[] doPOLParcelServicesField;

        /// <remarks/>
        public string recordID
        {
            get
            {
                return this.recordIDField;
            }
            set
            {
                this.recordIDField = value;
            }
        }

        /// <remarks/>
        public string parcelCode
        {
            get
            {
                return this.parcelCodeField;
            }
            set
            {
                this.parcelCodeField = value;
            }
        }

        /// <remarks/>
        public string prefixParcelCode
        {
            get
            {
                return this.prefixParcelCodeField;
            }
            set
            {
                this.prefixParcelCodeField = value;
            }
        }

        /// <remarks/>
        [DefaultValue(0)]
        public decimal weight
        {
            get
            {
                return this.weightField;
            }
            set
            {
                this.weightField = value;
            }
        }

        /// <remarks/>
        [DefaultValue(0)]
        public int insuredValue
        {
            get
            {
                return this.insuredValueField;
            }
            set
            {
                this.insuredValueField = value;
            }
        }

        /// <remarks/>
        [DefaultValue(0)]
        public int amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>
        public string currency
        {
            get
            {
                return this.currencyField;
            }
            set
            {
                this.currencyField = value;
            }
        }

        /// <remarks/>
        [DefaultValue(0)]
        public long vsVoucher
        {
            get
            {
                return this.vsVoucherField;
            }
            set
            {
                this.vsVoucherField = value;
            }
        }

        /// <remarks/>
        [DefaultValue(0)]
        public long vsParcel
        {
            get
            {
                return this.vsParcelField;
            }
            set
            {
                this.vsParcelField = value;
            }
        }

        /// <remarks/>
        [DefaultValue(0)]
        public sbyte sequenceParcel
        {
            get
            {
                return this.sequenceParcelField;
            }
            set
            {
                this.sequenceParcelField = value;
            }
        }

        /// <remarks/>
        [DefaultValue(0)]
        public sbyte quantityParcel
        {
            get
            {
                return this.quantityParcelField;
            }
            set
            {
                this.quantityParcelField = value;
            }
        }

        /// <remarks/>
        public string note
        {
            get
            {
                return this.noteField;
            }
            set
            {
                this.noteField = value;
            }
        }

        /// <remarks/>
        public string notePrint
        {
            get
            {
                return this.notePrintField;
            }
            set
            {
                this.notePrintField = value;
            }
        }

        /// <remarks/>
        [DefaultValue(0)]
        public short length
        {
            get
            {
                return this.lengthField;
            }
            set
            {
                this.lengthField = value;
            }
        }

        /// <remarks/>
        [DefaultValue(0)]
        public short width
        {
            get
            {
                return this.widthField;
            }
            set
            {
                this.widthField = value;
            }
        }

        /// <remarks/>
        [DefaultValue(0)]
        public short height
        {
            get
            {
                return this.heightField;
            }
            set
            {
                this.heightField = value;
            }
        }

        /// <remarks/>
        public string mrn
        {
            get
            {
                return this.mrnField;
            }
            set
            {
                this.mrnField = value;
            }
        }

        /// <remarks/>
        public string referenceNumber
        {
            get
            {
                return this.referenceNumberField;
            }
            set
            {
                this.referenceNumberField = value;
            }
        }

        /// <remarks/>
        [DefaultValue(0)]
        public byte pallets
        {
            get
            {
                return this.palletsField;
            }
            set
            {
                this.palletsField = value;
            }
        }

        /// <remarks/>
        public string specSym
        {
            get
            {
                return this.specSymField;
            }
            set
            {
                this.specSymField = value;
            }
        }

        /// <remarks/>
        public string note2
        {
            get
            {
                return this.note2Field;
            }
            set
            {
                this.note2Field = value;
            }
        }

        /// <remarks/>
        public string numSign
        {
            get
            {
                return this.numSignField;
            }
            set
            {
                this.numSignField = value;
            }
        }

        /// <remarks/>
        public string score
        {
            get
            {
                return this.scoreField;
            }
            set
            {
                this.scoreField = value;
            }
        }

        /// <remarks/>
        public string orderNumberZPRO
        {
            get
            {
                return this.orderNumberZPROField;
            }
            set
            {
                this.orderNumberZPROField = value;
            }
        }

        /// <remarks/>
        public string returnNumDays
        {
            get
            {
                return this.returnNumDaysField;
            }
            set
            {
                this.returnNumDaysField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("doPOLParcelServices")]
        public parcelServiceSyncRequestDoPOLSyncParcelDataDoPOLSyncParcelParamsDoPOLParcelServices[] doPOLParcelServices
        {
            get
            {
                return this.doPOLParcelServicesField;
            }
            set
            {
                this.doPOLParcelServicesField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1")]
    public partial class parcelServiceSyncRequestDoPOLSyncParcelDataDoPOLSyncParcelParamsDoPOLParcelServices
    {

        private string serviceField;

        /// <remarks/>
        public string service
        {
            get
            {
                return this.serviceField;
            }
            set
            {
                this.serviceField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1")]
    public partial class parcelServiceSyncRequestDoPOLSyncParcelDataDoPOLParcelAddress
    {

        private string recordIDField;

        private string firstNameField;

        private string surnameField;

        private string companyNameField;

        private string aditionAddressField;

        private string subjectField;

        private byte icField;

        private string dicField;

        private string specificationField;

        private string streetField;

        private string houseNumberField;

        private string sequenceNumberField;

        private string partCityField;

        private string cityField;

        private string zipCodeField;

        private string isoCountryField;

        private string subIsoCountryField;

        private byte bankField;

        private byte prefixAccountField;

        private byte accountField;

        private string mobileNumberField;

        private string phoneNumberField;

        private string emailAddressField;

        private string custCardNumField;

        private string adviceInformation1Field;

        private string adviceInformation2Field;

        private string adviceInformation3Field;

        private string adviceInformation4Field;

        private string adviceInformation5Field;

        private string adviceInformation6Field;

        private string adviceNoteField;

        /// <remarks/>
        public string recordID
        {
            get
            {
                return this.recordIDField;
            }
            set
            {
                this.recordIDField = value;
            }
        }

        /// <remarks/>
        public string firstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        public string surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        public string companyName
        {
            get
            {
                return this.companyNameField;
            }
            set
            {
                this.companyNameField = value;
            }
        }

        /// <remarks/>
        public string aditionAddress
        {
            get
            {
                return this.aditionAddressField;
            }
            set
            {
                this.aditionAddressField = value;
            }
        }

        /// <remarks/>
        public string subject
        {
            get
            {
                return this.subjectField;
            }
            set
            {
                this.subjectField = value;
            }
        }

        /// <remarks/>
        [DefaultValue(0)]
        public byte ic
        {
            get
            {
                return this.icField;
            }
            set
            {
                this.icField = value;
            }
        }

        /// <remarks/>
        public string dic
        {
            get
            {
                return this.dicField;
            }
            set
            {
                this.dicField = value;
            }
        }

        /// <remarks/>
        public string specification
        {
            get
            {
                return this.specificationField;
            }
            set
            {
                this.specificationField = value;
            }
        }

        /// <remarks/>
        public string street
        {
            get
            {
                return this.streetField;
            }
            set
            {
                this.streetField = value;
            }
        }

        /// <remarks/>
        public string houseNumber
        {
            get
            {
                return this.houseNumberField;
            }
            set
            {
                this.houseNumberField = value;
            }
        }

        /// <remarks/>
        public string sequenceNumber
        {
            get
            {
                return this.sequenceNumberField;
            }
            set
            {
                this.sequenceNumberField = value;
            }
        }

        /// <remarks/>
        public string partCity
        {
            get
            {
                return this.partCityField;
            }
            set
            {
                this.partCityField = value;
            }
        }

        /// <remarks/>
        public string city
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        public string zipCode
        {
            get
            {
                return this.zipCodeField;
            }
            set
            {
                this.zipCodeField = value;
            }
        }

        /// <remarks/>
        public string isoCountry
        {
            get
            {
                return this.isoCountryField;
            }
            set
            {
                this.isoCountryField = value;
            }
        }

        /// <remarks/>
        public string subIsoCountry
        {
            get
            {
                return this.subIsoCountryField;
            }
            set
            {
                this.subIsoCountryField = value;
            }
        }

        /// <remarks/>
        [DefaultValue(0)]
        public byte bank
        {
            get
            {
                return this.bankField;
            }
            set
            {
                this.bankField = value;
            }
        }

        /// <remarks/>
        [DefaultValue(0)]
        public byte prefixAccount
        {
            get
            {
                return this.prefixAccountField;
            }
            set
            {
                this.prefixAccountField = value;
            }
        }

        /// <remarks/>
        [DefaultValue(0)]
        public byte account
        {
            get
            {
                return this.accountField;
            }
            set
            {
                this.accountField = value;
            }
        }

        /// <remarks/>
        public string mobileNumber
        {
            get
            {
                return this.mobileNumberField;
            }
            set
            {
                this.mobileNumberField = value;
            }
        }

        /// <remarks/>
        public string phoneNumber
        {
            get
            {
                return this.phoneNumberField;
            }
            set
            {
                this.phoneNumberField = value;
            }
        }

        /// <remarks/>
        public string emailAddress
        {
            get
            {
                return this.emailAddressField;
            }
            set
            {
                this.emailAddressField = value;
            }
        }

        /// <remarks/>
        public string custCardNum
        {
            get
            {
                return this.custCardNumField;
            }
            set
            {
                this.custCardNumField = value;
            }
        }

        /// <remarks/>
        public string adviceInformation1
        {
            get
            {
                return this.adviceInformation1Field;
            }
            set
            {
                this.adviceInformation1Field = value;
            }
        }

        /// <remarks/>
        public string adviceInformation2
        {
            get
            {
                return this.adviceInformation2Field;
            }
            set
            {
                this.adviceInformation2Field = value;
            }
        }

        /// <remarks/>
        public string adviceInformation3
        {
            get
            {
                return this.adviceInformation3Field;
            }
            set
            {
                this.adviceInformation3Field = value;
            }
        }

        /// <remarks/>
        public string adviceInformation4
        {
            get
            {
                return this.adviceInformation4Field;
            }
            set
            {
                this.adviceInformation4Field = value;
            }
        }

        /// <remarks/>
        public string adviceInformation5
        {
            get
            {
                return this.adviceInformation5Field;
            }
            set
            {
                this.adviceInformation5Field = value;
            }
        }

        /// <remarks/>
        public string adviceInformation6
        {
            get
            {
                return this.adviceInformation6Field;
            }
            set
            {
                this.adviceInformation6Field = value;
            }
        }

        /// <remarks/>
        public string adviceNote
        {
            get
            {
                return this.adviceNoteField;
            }
            set
            {
                this.adviceNoteField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1")]
    public partial class parcelServiceSyncRequestDoPOLSyncParcelDataDoPOLParcelAddressDocument
    {

        private string recordIDField;

        private string firstNameField;

        private string surnameField;

        private string companyNameField;

        private string aditionAddressField;

        private string subjectField;

        private byte icField;

        private string dicField;

        private string specificationField;

        private string streetField;

        private string houseNumberField;

        private string sequenceNumberField;

        private string partCityField;

        private string cityField;

        private string zipCodeField;

        private string isoCountryField;

        private string subIsoCountryField;

        private byte bankField;

        private byte prefixAccountField;

        private byte accountField;

        private string mobileNumberField;

        private string phoneNumberField;

        private string emailAddressField;

        private string custCardNumField;

        private string adviceInformation1Field;

        private string adviceInformation2Field;

        private string adviceInformation3Field;

        private string adviceInformation4Field;

        private string adviceInformation5Field;

        private string adviceInformation6Field;

        private string adviceNoteField;

        /// <remarks/>
        public string recordID
        {
            get
            {
                return this.recordIDField;
            }
            set
            {
                this.recordIDField = value;
            }
        }

        /// <remarks/>
        public string firstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        public string surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        public string companyName
        {
            get
            {
                return this.companyNameField;
            }
            set
            {
                this.companyNameField = value;
            }
        }

        /// <remarks/>
        public string aditionAddress
        {
            get
            {
                return this.aditionAddressField;
            }
            set
            {
                this.aditionAddressField = value;
            }
        }

        /// <remarks/>
        public string subject
        {
            get
            {
                return this.subjectField;
            }
            set
            {
                this.subjectField = value;
            }
        }

        /// <remarks/>
        [DefaultValue(0)]
        public byte ic
        {
            get
            {
                return this.icField;
            }
            set
            {
                this.icField = value;
            }
        }

        /// <remarks/>
        public string dic
        {
            get
            {
                return this.dicField;
            }
            set
            {
                this.dicField = value;
            }
        }

        /// <remarks/>
        public string specification
        {
            get
            {
                return this.specificationField;
            }
            set
            {
                this.specificationField = value;
            }
        }

        /// <remarks/>
        public string street
        {
            get
            {
                return this.streetField;
            }
            set
            {
                this.streetField = value;
            }
        }

        /// <remarks/>
        public string houseNumber
        {
            get
            {
                return this.houseNumberField;
            }
            set
            {
                this.houseNumberField = value;
            }
        }

        /// <remarks/>
        public string sequenceNumber
        {
            get
            {
                return this.sequenceNumberField;
            }
            set
            {
                this.sequenceNumberField = value;
            }
        }

        /// <remarks/>
        public string partCity
        {
            get
            {
                return this.partCityField;
            }
            set
            {
                this.partCityField = value;
            }
        }

        /// <remarks/>
        public string city
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        public string zipCode
        {
            get
            {
                return this.zipCodeField;
            }
            set
            {
                this.zipCodeField = value;
            }
        }

        /// <remarks/>
        public string isoCountry
        {
            get
            {
                return this.isoCountryField;
            }
            set
            {
                this.isoCountryField = value;
            }
        }

        /// <remarks/>
        public string subIsoCountry
        {
            get
            {
                return this.subIsoCountryField;
            }
            set
            {
                this.subIsoCountryField = value;
            }
        }

        /// <remarks/>
        [DefaultValue(0)]
        public byte bank
        {
            get
            {
                return this.bankField;
            }
            set
            {
                this.bankField = value;
            }
        }

        /// <remarks/>
        [DefaultValue(0)]
        public byte prefixAccount
        {
            get
            {
                return this.prefixAccountField;
            }
            set
            {
                this.prefixAccountField = value;
            }
        }

        /// <remarks/>
        [DefaultValue(0)]
        public byte account
        {
            get
            {
                return this.accountField;
            }
            set
            {
                this.accountField = value;
            }
        }

        /// <remarks/>
        public string mobileNumber
        {
            get
            {
                return this.mobileNumberField;
            }
            set
            {
                this.mobileNumberField = value;
            }
        }

        /// <remarks/>
        public string phoneNumber
        {
            get
            {
                return this.phoneNumberField;
            }
            set
            {
                this.phoneNumberField = value;
            }
        }

        /// <remarks/>
        public string emailAddress
        {
            get
            {
                return this.emailAddressField;
            }
            set
            {
                this.emailAddressField = value;
            }
        }

        /// <remarks/>
        public string custCardNum
        {
            get
            {
                return this.custCardNumField;
            }
            set
            {
                this.custCardNumField = value;
            }
        }

        /// <remarks/>
        public string adviceInformation1
        {
            get
            {
                return this.adviceInformation1Field;
            }
            set
            {
                this.adviceInformation1Field = value;
            }
        }

        /// <remarks/>
        public string adviceInformation2
        {
            get
            {
                return this.adviceInformation2Field;
            }
            set
            {
                this.adviceInformation2Field = value;
            }
        }

        /// <remarks/>
        public string adviceInformation3
        {
            get
            {
                return this.adviceInformation3Field;
            }
            set
            {
                this.adviceInformation3Field = value;
            }
        }

        /// <remarks/>
        public string adviceInformation4
        {
            get
            {
                return this.adviceInformation4Field;
            }
            set
            {
                this.adviceInformation4Field = value;
            }
        }

        /// <remarks/>
        public string adviceInformation5
        {
            get
            {
                return this.adviceInformation5Field;
            }
            set
            {
                this.adviceInformation5Field = value;
            }
        }

        /// <remarks/>
        public string adviceInformation6
        {
            get
            {
                return this.adviceInformation6Field;
            }
            set
            {
                this.adviceInformation6Field = value;
            }
        }

        /// <remarks/>
        public string adviceNote
        {
            get
            {
                return this.adviceNoteField;
            }
            set
            {
                this.adviceNoteField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1")]
    public partial class parcelServiceSyncRequestDoPOLSyncParcelDataDoPOLParcelCustomsDeclaration
    {

        private string categoryField;

        private string noteField;

        private string customValCurField;

        private string importerRefNumField;

        private parcelServiceSyncRequestDoPOLSyncParcelDataDoPOLParcelCustomsDeclarationDoPOLParcelCustomsGoods[] doPOLParcelCustomsGoodsField;

        /// <remarks/>
        public string category
        {
            get
            {
                return this.categoryField;
            }
            set
            {
                this.categoryField = value;
            }
        }

        /// <remarks/>
        public string note
        {
            get
            {
                return this.noteField;
            }
            set
            {
                this.noteField = value;
            }
        }

        /// <remarks/>
        public string customValCur
        {
            get
            {
                return this.customValCurField;
            }
            set
            {
                this.customValCurField = value;
            }
        }

        /// <remarks/>
        public string importerRefNum
        {
            get
            {
                return this.importerRefNumField;
            }
            set
            {
                this.importerRefNumField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("doPOLParcelCustomsGoods")]
        public parcelServiceSyncRequestDoPOLSyncParcelDataDoPOLParcelCustomsDeclarationDoPOLParcelCustomsGoods[] doPOLParcelCustomsGoods
        {
            get
            {
                return this.doPOLParcelCustomsGoodsField;
            }
            set
            {
                this.doPOLParcelCustomsGoodsField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1")]
    public partial class parcelServiceSyncRequestDoPOLSyncParcelDataDoPOLParcelCustomsDeclarationDoPOLParcelCustomsGoods
    {

        private byte sequenceField;

        private string customContField;

        private string quantityField;

        private decimal weightField;

        private int customValField;

        private string hsCodeField;

        private string isoField;

        /// <remarks/>
        public byte sequence
        {
            get
            {
                return this.sequenceField;
            }
            set
            {
                this.sequenceField = value;
            }
        }

        /// <remarks/>
        public string customCont
        {
            get
            {
                return this.customContField;
            }
            set
            {
                this.customContField = value;
            }
        }

        /// <remarks/>
        public string quantity
        {
            get
            {
                return this.quantityField;
            }
            set
            {
                this.quantityField = value;
            }
        }

        /// <remarks/>
        public decimal weight
        {
            get
            {
                return this.weightField;
            }
            set
            {
                this.weightField = value;
            }
        }

        /// <remarks/>
        public int customVal
        {
            get
            {
                return this.customValField;
            }
            set
            {
                this.customValField = value;
            }
        }

        /// <remarks/>
        public string hsCode
        {
            get
            {
                return this.hsCodeField;
            }
            set
            {
                this.hsCodeField = value;
            }
        }

        /// <remarks/>
        public string iso
        {
            get
            {
                return this.isoField;
            }
            set
            {
                this.isoField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1")]
    public partial class parcelServiceSyncRequestDoPOLSyncParcelDataDoPOLParcelCustomsDocument
    {

        private string recordIDField;

        private string codeField;

        private string nameField;

        private string idField;

        /// <remarks/>
        public string recordID
        {
            get
            {
                return this.recordIDField;
            }
            set
            {
                this.recordIDField = value;
            }
        }

        /// <remarks/>
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }


}

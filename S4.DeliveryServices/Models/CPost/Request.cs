﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.DeliveryServices.Models.CPost
{

    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://b2b.postaonline.cz/schema/B2BCommon-v1")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "https://b2b.postaonline.cz/schema/B2BCommon-v1", IsNullable = false)]
    public partial class b2bRequest
    {

        private b2bRequestHeader headerField;

        private b2bRequestServiceData serviceDataField;

        /// <remarks/>
        public b2bRequestHeader header
        {
            get
            {
                return this.headerField;
            }
            set
            {
                this.headerField = value;
            }
        }

        /// <remarks/>
        public b2bRequestServiceData serviceData
        {
            get
            {
                return this.serviceDataField;
            }
            set
            {
                this.serviceDataField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://b2b.postaonline.cz/schema/B2BCommon-v1")]
    public partial class b2bRequestHeader
    {

        private byte idExtTransactionField;

        private System.DateTime timeStampField;

        private ulong idContractField;

        /// <remarks/>
        public byte idExtTransaction
        {
            get
            {
                return this.idExtTransactionField;
            }
            set
            {
                this.idExtTransactionField = value;
            }
        }

        /// <remarks/>
        public System.DateTime timeStamp
        {
            get
            {
                return this.timeStampField;
            }
            set
            {
                this.timeStampField = value;
            }
        }

        /// <remarks/>
        public ulong idContract
        {
            get
            {
                return this.idContractField;
            }
            set
            {
                this.idContractField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://b2b.postaonline.cz/schema/B2BCommon-v1")]
    public partial class b2bRequestServiceData
    {

        private getStats getStatsField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1")]
        public getStats getStats
        {
            get
            {
                return this.getStatsField;
            }
            set
            {
                this.getStatsField = value;
            }
        }

        private parcelServiceSyncRequest parcelServiceSyncRequestField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1")]
        public parcelServiceSyncRequest parcelServiceSyncRequest
        {
            get
            {
                return this.parcelServiceSyncRequestField;
            }
            set
            {
                this.parcelServiceSyncRequestField = value;
            }
        }

        private getParcelsPrinting getParcelsPrintingField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1")]
        public getParcelsPrinting getParcelsPrinting
        {
            get
            {
                return this.getParcelsPrintingField;
            }
            set
            {
                this.getParcelsPrintingField = value;
            }
        }

        private getParcelState getParcelStateField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "https://b2b.postaonline.cz/schema/POLServices-v1")]
        public getParcelState getParcelState
        {
            get
            {
                return this.getParcelStateField;
            }
            set
            {
                this.getParcelStateField = value;
            }
        }
    }
}

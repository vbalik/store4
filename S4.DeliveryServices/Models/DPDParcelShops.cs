﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.DeliveryServices.Models
{

    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class parcelshops
    {

        private parcelshopsParcelshop[] parcelshopField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("parcelshop")]
        public parcelshopsParcelshop[] parcelshop
        {
            get
            {
                return this.parcelshopField;
            }
            set
            {
                this.parcelshopField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class parcelshopsParcelshop
    {

        private string idField;

        private string companyField;

        private string streetField;

        private string house_numberField;

        private uint postcodeField;

        private string cityField;

        private uint phoneField;

        private object faxField;

        private string emailField;

        private string homepageField;

        private byte pickup_allowedField;

        private byte return_allowedField;

        private byte express_allowedField;

        private byte cod_allowedField;

        private byte cardpayment_allowedField;

        private decimal latitudeField;

        private decimal longitudeField;

        private parcelshopsParcelshopOpening[] opening_hoursField;

        /// <remarks/>
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        public string company
        {
            get
            {
                return this.companyField;
            }
            set
            {
                this.companyField = value;
            }
        }

        /// <remarks/>
        public string street
        {
            get
            {
                return this.streetField;
            }
            set
            {
                this.streetField = value;
            }
        }

        /// <remarks/>
        public string house_number
        {
            get
            {
                return this.house_numberField;
            }
            set
            {
                this.house_numberField = value;
            }
        }

        /// <remarks/>
        public uint postcode
        {
            get
            {
                return this.postcodeField;
            }
            set
            {
                this.postcodeField = value;
            }
        }

        /// <remarks/>
        public string city
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        public uint phone
        {
            get
            {
                return this.phoneField;
            }
            set
            {
                this.phoneField = value;
            }
        }

        /// <remarks/>
        public object fax
        {
            get
            {
                return this.faxField;
            }
            set
            {
                this.faxField = value;
            }
        }

        /// <remarks/>
        public string email
        {
            get
            {
                return this.emailField;
            }
            set
            {
                this.emailField = value;
            }
        }

        /// <remarks/>
        public string homepage
        {
            get
            {
                return this.homepageField;
            }
            set
            {
                this.homepageField = value;
            }
        }

        /// <remarks/>
        public byte pickup_allowed
        {
            get
            {
                return this.pickup_allowedField;
            }
            set
            {
                this.pickup_allowedField = value;
            }
        }

        /// <remarks/>
        public byte return_allowed
        {
            get
            {
                return this.return_allowedField;
            }
            set
            {
                this.return_allowedField = value;
            }
        }

        /// <remarks/>
        public byte express_allowed
        {
            get
            {
                return this.express_allowedField;
            }
            set
            {
                this.express_allowedField = value;
            }
        }

        /// <remarks/>
        public byte cod_allowed
        {
            get
            {
                return this.cod_allowedField;
            }
            set
            {
                this.cod_allowedField = value;
            }
        }

        /// <remarks/>
        public byte cardpayment_allowed
        {
            get
            {
                return this.cardpayment_allowedField;
            }
            set
            {
                this.cardpayment_allowedField = value;
            }
        }

        /// <remarks/>
        public decimal latitude
        {
            get
            {
                return this.latitudeField;
            }
            set
            {
                this.latitudeField = value;
            }
        }

        /// <remarks/>
        public decimal longitude
        {
            get
            {
                return this.longitudeField;
            }
            set
            {
                this.longitudeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("opening", IsNullable = false)]
        public parcelshopsParcelshopOpening[] opening_hours
        {
            get
            {
                return this.opening_hoursField;
            }
            set
            {
                this.opening_hoursField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class parcelshopsParcelshopOpening
    {

        private parcelshopsParcelshopOpeningDay dayField;

        private string openMorningField;

        private string closeMorningField;

        private string openAfternoonField;

        private string closeAfternoonField;

        /// <remarks/>
        public parcelshopsParcelshopOpeningDay day
        {
            get
            {
                return this.dayField;
            }
            set
            {
                this.dayField = value;
            }
        }

        /// <remarks/>
        public string openMorning
        {
            get
            {
                return this.openMorningField;
            }
            set
            {
                this.openMorningField = value;
            }
        }

        /// <remarks/>
        public string closeMorning
        {
            get
            {
                return this.closeMorningField;
            }
            set
            {
                this.closeMorningField = value;
            }
        }

        /// <remarks/>
        public string openAfternoon
        {
            get
            {
                return this.openAfternoonField;
            }
            set
            {
                this.openAfternoonField = value;
            }
        }

        /// <remarks/>
        public string closeAfternoon
        {
            get
            {
                return this.closeAfternoonField;
            }
            set
            {
                this.closeAfternoonField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class parcelshopsParcelshopOpeningDay
    {

        private byte dayField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte day
        {
            get
            {
                return this.dayField;
            }
            set
            {
                this.dayField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }


}

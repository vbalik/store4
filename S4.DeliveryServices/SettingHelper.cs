﻿using System;
using System.Collections.Generic;
using System.Text;

using S4.Core;

namespace S4.DeliveryServices
{
    internal static class SettingHelper
    {
        public static Models.DeliveryServicesSettings GetSetting()
        {
            //read setting
            var deliveryServicesSettingsJson = Singleton<DAL.SettingsSet>.Instance.GetValue_String(Entities.SettingsDictionary.DELIVERY_SERVICES_SETTING);

            if (string.IsNullOrWhiteSpace(deliveryServicesSettingsJson))
                return new Models.DeliveryServicesSettings();

            return Newtonsoft.Json.JsonConvert.DeserializeObject<Models.DeliveryServicesSettings>(deliveryServicesSettingsJson);
        }
    }
}

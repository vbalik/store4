﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Linq;
using System.ComponentModel.DataAnnotations;

using S4.Entities;
using S4.Entities.XtraData;

namespace S4.DeliveryServices
{
    public abstract class DeliveryServiceBase
    {
        protected static Models.DeliveryServicesSettings Settings = null;
        private const DeliveryDirection.DeliveryCountryEnum _DEFAULT_COUNTRY = DeliveryDirection.DeliveryCountryEnum.CZ;
        private const DeliveryDirection.DeliveryCurrencyEnum _DEFAULT_CURRENCY = DeliveryDirection.DeliveryCurrencyEnum.CZK;
        public const string CONTENT_DIVIDER = "CONTENT_DIVIDER";
        public const string BEGIN_OF_GROUP = "BEGIN_OF_GROUP";
        public const string GROUP_HEADER = "GROUP_HEADER";
        public const string END_OF_GROUP = "END_OF_GROUP";
        public const string ERROR_ITEM = "ERROR_ITEM";

        public DeliveryServiceBase() : this(S4.Core.Data.ConnectionHelper.ConnectionString)
        {
        }

        public DeliveryServiceBase(string connectionString)
        {
            Core.Data.ConnectionHelper.ConnectionString = connectionString;
            ReadConfig();
        }

        public event LogEventHandler LogEvent;
        public event EventHandler<GetLabelEventArgs> GetLabelEvent;
        public event EventHandler<GetLabelEventArgs> GetManifestReportEvent;
        public event GetShipmentStatusEventHandler GetShipmentStatusEvent;

        public short ErrorsLimit { get => Settings.ErrorLimit; }

        public string SavePathRequestResponse { get => Settings.SavePathRequestResponse; }

        protected void SendToLog(IDeliveryService sender, string message, LogLevelEnum logLevel, Exception exception = null)
        {
            LogEvent?.Invoke(sender, message, logLevel, exception);
        }

        protected void InvokeLabelEvent(IDeliveryService sender, string referenceId, byte[] data)
        {
            InvokeReportEvent(sender, GetLabelEvent, referenceId, data);
        }

        protected void InvokeManifestReportEvent(IDeliveryService sender, string referenceId, byte[] data)
        {
            InvokeReportEvent(sender, GetManifestReportEvent, referenceId, data);
        }

        protected void OnGetShipmentStatus(object sender, DeliveryDirection.DeliveryProviderEnum provider, Entities.Models.LabeledValue[] content)
        {
            GetShipmentStatusEvent?.Invoke(sender, provider, content);
        }

        public delegate bool InsertShipmentEventHandler(IDeliveryService sender, int directionID, string shipmentReference, Tuple<int, string>[] parcels, out string error);
        public event InsertShipmentEventHandler InsertShipmentEvent;
        protected bool InsertShipment(IDeliveryService sender, int directionID, string shipmentReference, Tuple<int, string>[] parcels, out string error)
        {
            error = null;
            return InsertShipmentEvent?.Invoke(sender, directionID, shipmentReference, parcels, out error) ?? true;
        }


        public delegate void SetShipmentFailedEventHandler(IDeliveryService sender, int directionID, string text);
        public event SetShipmentFailedEventHandler SetShipmentFailedEvent;
        protected void SetShipmentFailed(IDeliveryService sender, int directionID, string text)
        {
            SetShipmentFailedEvent?.Invoke(sender, directionID, text);
        }

        protected void InsertShipmentError(IDeliveryService sender, int directionID, string error)
        {
            try
            {
                using (var db = Core.Data.ConnectionHelper.GetDB())
                {
                    var deliveryDirection = new DAL.DeliveryDirectionDAL().GetByDirectionID(directionID, db);
                    if (deliveryDirection == null)
                    {
                        deliveryDirection = new DeliveryDirection()
                        {
                            DirectionID = directionID,
                            DeliveryProvider = (int)sender.DeliveryProvider,
                            DeliveryStatus = DeliveryDirection.DD_STATUS_OK
                        };
                    }
                    deliveryDirection.CreateShipmentAttempt = (short)((deliveryDirection.CreateShipmentAttempt ?? 0) + 1);
                    deliveryDirection.CreateShipmentError = error;

                    //insert or update delivery direction
                    if (deliveryDirection.DeliveryDirectionID == 0)
                        deliveryDirection.DeliveryDirectionID = Convert.ToInt32(db.Insert(deliveryDirection));
                    else
                        db.Save(deliveryDirection);

                    if (deliveryDirection.CreateShipmentAttempt >= ErrorsLimit)
                        SetShipmentFailed(sender, directionID, error);
                }
            }
            catch(Exception ex)
            {
                SendToLog(sender, ex.ToString(), LogLevelEnum.Error, ex);
            }
        }

        protected void InsertManifestError(IDeliveryService sender, DeliveryManifest deliveryManifest, string error)
        {
            try
            {
                deliveryManifest.CreateManifestAttempt = (short)((deliveryManifest.CreateManifestAttempt ?? 0) + 1);
                deliveryManifest.CreateManifestError = error;
                if (deliveryManifest.CreateManifestAttempt >= Settings.ErrorLimit)
                    deliveryManifest.ManifestStatus = DeliveryManifest.STATUS_ERROR;
            }
            catch (Exception ex)
            {
                SendToLog(sender, ex.ToString(), LogLevelEnum.Error, ex);
            }
        }

        public delegate void LabelPrintedEventHandler(IDeliveryService sender, int directionID);
        public event LabelPrintedEventHandler LabelPrintedEvent;
        protected void LabelPrinted(IDeliveryService sender, int directionID)
        {
            LabelPrintedEvent?.Invoke(sender, directionID);
        }

        public delegate void ManifestCreatedEventHandler(IDeliveryService sender, int deliveryManifestID, string manifestRefNumber);
        public event ManifestCreatedEventHandler ManifestCreatedEvent;
        protected void ManifestCreated(IDeliveryService sender, int deliveryManifestID, string manifestRefNumber)
        {
            ManifestCreatedEvent?.Invoke(sender, deliveryManifestID, manifestRefNumber);   
        }

        private static void ReadConfig()
        {
            if (Settings != null)
                return;

            Settings = SettingHelper.GetSetting();
        }

        private void InvokeReportEvent(IDeliveryService deliveryService, EventHandler<GetLabelEventArgs> handler, string referenceId, byte[] data)
        {
            handler?.Invoke(this, new GetLabelEventArgs() { ReferenceId = referenceId, Data = data, DeliveryService = deliveryService });
        }
    }
}

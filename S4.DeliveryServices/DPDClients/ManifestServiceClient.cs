﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Threading.Tasks;
using System.Xml;

using DPDManifestService;

namespace S4.DeliveryServices.DPDClients
{
    public class ManifestServiceClient : ClientBase<ManifestServiceImpl>, ManifestServiceImpl
    {
        public ManifestServiceClient() :
            base(new BasicHttpBinding
            {
                SendTimeout = TimeSpan.FromSeconds(180),
                MaxBufferSize = int.MaxValue,
                MaxReceivedMessageSize = int.MaxValue,
                AllowCookies = true,
                ReaderQuotas = XmlDictionaryReaderQuotas.Max
            },
                new EndpointAddress("http://www.mojedpd.cz/IT4EMWebServices/eshop/ManifestServiceImpl"))
        {
        }

        [System.ServiceModel.OperationContractAttribute(Action = "", ReplyAction = "*")]
        [System.ServiceModel.FaultContractAttribute(typeof(DPDManifestService.EShopException), Action = "", Name = "EShopException")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults = true)]
        public System.Threading.Tasks.Task<DPDManifestService.closeManifestResponse1> closeManifestAsync(DPDManifestService.closeManifestRequest request)
        {
            return Channel.closeManifestAsync(request);
        }

        [System.ServiceModel.OperationContractAttribute(Action = "", ReplyAction = "*")]
        [System.ServiceModel.FaultContractAttribute(typeof(DPDManifestService.EShopException), Action = "", Name = "EShopException")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults = true)]
        public System.Threading.Tasks.Task<DPDManifestService.closeManifestWithoutPrintResponse1> closeManifestWithoutPrintAsync(DPDManifestService.closeManifestWithoutPrintRequest request)
        {
            return Channel.closeManifestWithoutPrintAsync(request);
        }

        [System.ServiceModel.OperationContractAttribute(Action = "", ReplyAction = "*")]
        [System.ServiceModel.FaultContractAttribute(typeof(DPDManifestService.EShopException), Action = "", Name = "EShopException")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults = true)]
        public System.Threading.Tasks.Task<DPDManifestService.getAllShipmentsWithoutManifestResponse1> getAllShipmentsWithoutManifestAsync(DPDManifestService.getAllShipmentsWithoutManifestRequest request)
        {
            return Channel.getAllShipmentsWithoutManifestAsync(request);
        }

        [System.ServiceModel.OperationContractAttribute(Action = "", ReplyAction = "*")]
        [System.ServiceModel.FaultContractAttribute(typeof(DPDManifestService.EShopException), Action = "", Name = "EShopException")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults = true)]
        public System.Threading.Tasks.Task<DPDManifestService.getAllShipmentsWithoutManifestByUserNameResponse1> getAllShipmentsWithoutManifestByUserNameAsync(DPDManifestService.getAllShipmentsWithoutManifestByUserNameRequest request)
        {
            return Channel.getAllShipmentsWithoutManifestByUserNameAsync(request);
        }

        [System.ServiceModel.OperationContractAttribute(Action = "", ReplyAction = "*")]
        [System.ServiceModel.FaultContractAttribute(typeof(DPDManifestService.EShopException), Action = "", Name = "EShopException")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults = true)]
        public System.Threading.Tasks.Task<DPDManifestService.reprintManifestResponse1> reprintManifestAsync(DPDManifestService.reprintManifestRequest request)
        {
            return Channel.reprintManifestAsync(request);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using System.Threading.Tasks;

using Newtonsoft.Json;
using S4.DeliveryServices.Models;

namespace S4.DeliveryServices.DPDClients
{
    public class DPDApiClient
    {
        private HttpClient _client;
        private string _url = "https://pickup.dpd.cz/";

        public DPDApiClient()
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri(_url);
        }

        public async Task<Models.DPDParcelShop> GetParcelShop(string parcelShopId)
        {
            if (parcelShopId != null)
            {
                HttpResponseMessage response = await _client.GetAsync($"api/get-parcel-shop-by-id?id={WebUtility.UrlEncode(parcelShopId)}");
                if (response.IsSuccessStatusCode)
                {
                    var resultStr = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<Models.DPDApiResult<Models.DPDParcelShop>>(resultStr);
                    if (result.Status == "ok")
                        return result.Data;
                }
            }

            return null;
        }

        public async Task<string> GetParcelShops()
        {
            HttpResponseMessage response = await _client.GetAsync($"export/xml?country=203");
            if (response.IsSuccessStatusCode)
            {
                var resultStr = await response.Content.ReadAsStringAsync();
                return resultStr;
            }
            return null;
        }
    }
}

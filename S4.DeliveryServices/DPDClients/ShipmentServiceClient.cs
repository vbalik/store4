﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Threading.Tasks;
using System.Xml;

using DPDShipmentService;

namespace S4.DeliveryServices.DPDClients
{
    public class ShipmentServiceClient : ClientBase<ShipmentServiceImpl>, ShipmentServiceImpl
    {
        public ShipmentServiceClient() :
            base(new BasicHttpBinding
            {
                SendTimeout = TimeSpan.FromSeconds(180),
                MaxBufferSize = int.MaxValue,
                MaxReceivedMessageSize = int.MaxValue,
                AllowCookies = true,
                ReaderQuotas = XmlDictionaryReaderQuotas.Max
            },
                new EndpointAddress("http://www.mojedpd.cz/IT4EMWebServices/eshop/ShipmentServiceImpl"))
        {
        }

        [System.ServiceModel.OperationContractAttribute(Action = "", ReplyAction = "*")]
        [System.ServiceModel.FaultContractAttribute(typeof(DPDShipmentService.EShopException), Action = "", Name = "EShopException")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults = true)]
        public System.Threading.Tasks.Task<DPDShipmentService.calculatePriceResponse1> calculatePriceAsync(DPDShipmentService.calculatePriceRequest request)
        {
            return Channel.calculatePriceAsync(request);
        }

        [System.ServiceModel.OperationContractAttribute(Action = "", ReplyAction = "*")]
        [System.ServiceModel.FaultContractAttribute(typeof(DPDShipmentService.EShopException), Action = "", Name = "EShopException")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults = true)]
        public System.Threading.Tasks.Task<DPDShipmentService.createShipmentResponse1> createShipmentAsync(DPDShipmentService.createShipmentRequest request)
        {
            return Channel.createShipmentAsync(request);
        }

        [System.ServiceModel.OperationContractAttribute(Action = "", ReplyAction = "*")]
        [System.ServiceModel.FaultContractAttribute(typeof(DPDShipmentService.EShopException), Action = "", Name = "EShopException")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults = true)]
        public System.Threading.Tasks.Task<DPDShipmentService.createShipmentWLResponse1> createShipmentWLAsync(DPDShipmentService.createShipmentWLRequest request)
        {
            return Channel.createShipmentWLAsync(request);
        }

        [System.ServiceModel.OperationContractAttribute(Action = "", ReplyAction = "*")]
        [System.ServiceModel.FaultContractAttribute(typeof(DPDShipmentService.EShopException), Action = "", Name = "EShopException")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults = true)]
        public System.Threading.Tasks.Task<DPDShipmentService.deleteShipmentResponse1> deleteShipmentAsync(DPDShipmentService.deleteShipmentRequest request)
        {
            return Channel.deleteShipmentAsync(request);
        }

        [System.ServiceModel.OperationContractAttribute(Action = "", ReplyAction = "*")]
        [System.ServiceModel.FaultContractAttribute(typeof(DPDShipmentService.EShopException), Action = "", Name = "EShopException")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults = true)]
        public System.Threading.Tasks.Task<DPDShipmentService.getParcelIdByParcelNumberResponse1> getParcelIdByParcelNumberAsync(DPDShipmentService.getParcelIdByParcelNumberRequest request)
        {
            return Channel.getParcelIdByParcelNumberAsync(request);
        }

        [System.ServiceModel.OperationContractAttribute(Action = "", ReplyAction = "*")]
        [System.ServiceModel.FaultContractAttribute(typeof(DPDShipmentService.EShopException), Action = "", Name = "EShopException")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults = true)]
        public System.Threading.Tasks.Task<DPDShipmentService.getParcelStatusResponse1> getParcelStatusAsync(DPDShipmentService.getParcelStatusRequest request)
        {
            return Channel.getParcelStatusAsync(request);
        }

        [System.ServiceModel.OperationContractAttribute(Action = "", ReplyAction = "*")]
        [System.ServiceModel.FaultContractAttribute(typeof(DPDShipmentService.EShopException), Action = "", Name = "EShopException")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults = true)]
        public System.Threading.Tasks.Task<DPDShipmentService.getShipmentResponse1> getShipmentAsync(DPDShipmentService.getShipmentRequest request)
        {
            return Channel.getShipmentAsync(request);
        }

        [System.ServiceModel.OperationContractAttribute(Action = "", ReplyAction = "*")]
        [System.ServiceModel.FaultContractAttribute(typeof(DPDShipmentService.EShopException), Action = "", Name = "EShopException")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults = true)]
        public System.Threading.Tasks.Task<DPDShipmentService.getShipmentLabelResponse1> getShipmentLabelAsync(DPDShipmentService.getShipmentLabelRequest request)
        {
            return Channel.getShipmentLabelAsync(request);
        }

        [System.ServiceModel.OperationContractAttribute(Action = "", ReplyAction = "*")]
        [System.ServiceModel.FaultContractAttribute(typeof(DPDShipmentService.EShopException), Action = "", Name = "EShopException")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults = true)]
        public System.Threading.Tasks.Task<DPDShipmentService.getShipmentStatusResponse1> getShipmentStatusAsync(DPDShipmentService.getShipmentStatusRequest request)
        {
            return Channel.getShipmentStatusAsync(request);
        }

        [System.ServiceModel.OperationContractAttribute(Action = "", ReplyAction = "*")]
        [System.ServiceModel.FaultContractAttribute(typeof(DPDShipmentService.EShopException), Action = "", Name = "EShopException")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults = true)]
        public System.Threading.Tasks.Task<DPDShipmentService.parcelShopSearchResponse1> parcelShopSearchAsync(DPDShipmentService.parcelShopSearchRequest request)
        {
            return Channel.parcelShopSearchAsync(request);
        }

        [System.ServiceModel.OperationContractAttribute(Action = "", ReplyAction = "*")]
        [System.ServiceModel.FaultContractAttribute(typeof(DPDShipmentService.EShopException), Action = "", Name = "EShopException")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults = true)]
        public System.Threading.Tasks.Task<DPDShipmentService.parcelShopSearchWLResponse1> parcelShopSearchWLAsync(DPDShipmentService.parcelShopSearchWLRequest request)
        {
            return Channel.parcelShopSearchWLAsync(request);
        }

        [System.ServiceModel.OperationContractAttribute(Action = "", ReplyAction = "*")]
        [System.ServiceModel.FaultContractAttribute(typeof(DPDShipmentService.EShopException), Action = "", Name = "EShopException")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults = true)]
        public System.Threading.Tasks.Task<DPDShipmentService.reprintParcelLabelResponse1> reprintParcelLabelAsync(DPDShipmentService.reprintParcelLabelRequest request)
        {
            return Channel.reprintParcelLabelAsync(request);
        }

        [System.ServiceModel.OperationContractAttribute(Action = "", ReplyAction = "*")]
        [System.ServiceModel.FaultContractAttribute(typeof(DPDShipmentService.EShopException), Action = "", Name = "EShopException")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults = true)]
        public System.Threading.Tasks.Task<DPDShipmentService.searchShipmentResponse1> searchShipmentAsync(DPDShipmentService.searchShipmentRequest request)
        {
            return Channel.searchShipmentAsync(request);
        }

        [System.ServiceModel.OperationContractAttribute(Action = "", ReplyAction = "*")]
        [System.ServiceModel.FaultContractAttribute(typeof(DPDShipmentService.EShopException), Action = "", Name = "EShopException")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults = true)]
        public System.Threading.Tasks.Task<DPDShipmentService.updateShipmentResponse1> updateShipmentAsync(DPDShipmentService.updateShipmentRequest request)
        {
            return Channel.updateShipmentAsync(request);
        }
    }
}

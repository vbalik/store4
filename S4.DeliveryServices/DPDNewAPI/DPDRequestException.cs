﻿using System;

namespace S4.DeliveryServices.DPDNewAPI
{
    internal class DPDRequestException : Exception
    {
        public string Response;

        public DPDRequestException(string message, string response) : base(message)
        {
            this.Response = response;
        }
    }
}

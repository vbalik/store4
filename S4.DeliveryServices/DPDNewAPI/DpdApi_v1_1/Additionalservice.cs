﻿using System.Collections.Generic;

namespace S4.DeliveryServices.DPDNewAPI.DpdApi_v1_1
{
    public class AdditionalService
    {
        public Cod cod { get; set; }

        public List<Predict> predicts { get; set; }

        public string pudoId { get; set; }
    }

}
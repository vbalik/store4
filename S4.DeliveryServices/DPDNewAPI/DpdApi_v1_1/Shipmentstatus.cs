﻿namespace S4.DeliveryServices.DPDNewAPI.DpdApi_v1_1
{
    public class Shipmentstatus
    {
        public int transactionId { get; set; }
        public Shipment shipment { get; set; }
    }
}
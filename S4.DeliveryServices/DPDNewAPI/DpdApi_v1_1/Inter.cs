﻿using System.Collections.Generic;

namespace S4.DeliveryServices.DPDNewAPI.DpdApi_v1_1
{
    public class Inter
    {
        public string parcelType { get; set; }
        public string currency { get; set; }
        public string currencyEx { get; set; }
        public string clearanceCleared { get; set; }
        public string highLowValue { get; set; }
        public string preAlertStatus { get; set; }
        public string shipMrn { get; set; }
        public string siName1 { get; set; }
        public string siName2 { get; set; }
        public string siStreet { get; set; }
        public string siPropNum { get; set; }
        public string siCountryCode { get; set; }
        public string siState { get; set; }
        public string siZipCode { get; set; }
        public string siTown { get; set; }
        public string siGpsLat { get; set; }
        public string siGpsLong { get; set; }
        public string siContact { get; set; }
        public string siPhonePrefix { get; set; }
        public string siPhone { get; set; }
        public string siEmail { get; set; }
        public string opCode { get; set; }
        public int numberOfArticle { get; set; }
        public string destCountryReg { get; set; }
        public string siEori { get; set; }
        public string reasonForExport { get; set; }
        public int cAmount { get; set; }
        public int cAmountEx { get; set; }
        public string cTerms { get; set; }
        public string cPaper { get; set; }
        public string cComment { get; set; }
        public string cInvoice { get; set; }
        public string cInvoiceDate { get; set; }
        public string cName1 { get; set; }
        public string cName2 { get; set; }
        public string cStreet { get; set; }
        public string cPropNum { get; set; }
        public string cCountryCode { get; set; }
        public string cState { get; set; }
        public string cZipCode { get; set; }
        public string cTown { get; set; }
        public string cGpsLat { get; set; }
        public string cGpsLong { get; set; }
        public string cContact { get; set; }
        public string cPhonePrefix { get; set; }
        public string cPhone { get; set; }
        public string cFaxPrefix { get; set; }
        public string cFax { get; set; }
        public string cEmail { get; set; }
        public string cGln { get; set; }
        public int cVatNo { get; set; }
        public string cNumber { get; set; }
        public int cEori { get; set; }
        public int sVatNo { get; set; }
        public int sEori { get; set; }
        public int rVatNo { get; set; }
        public int rEori { get; set; }
        public List<Interinvoicelist> interInvoiceList { get; set; }
    }

}
﻿namespace S4.DeliveryServices.DPDNewAPI.DpdApi_v1_1
{
    public class Returnconsolidation
    {
        public string returnComment { get; set; }
        public string returnGln { get; set; }
        public string returnDepot { get; set; }
        public string returnPudoId { get; set; }
        public Returndepotaddress returnDepotAddress { get; set; }
    }

}
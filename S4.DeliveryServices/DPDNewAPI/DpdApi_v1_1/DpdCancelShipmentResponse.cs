﻿using System.Collections.Generic;

namespace S4.DeliveryServices.DPDNewAPI.DpdApi_v1_1
{
    public class DpdCancelShipmentResponse
    {
        public int transactionId { get; set; }
        public List<Resultlist> resultList { get; set; }
    }
}
﻿namespace S4.DeliveryServices.DPDNewAPI.DpdApi_v1_1
{
    public class Routing
    {
        public string buCode { get; set; }
        public string buAlphaString { get; set; }
        public string networkCode { get; set; }
        public string dcountry { get; set; }
        public string barcodeId { get; set; }
        public string serviceText { get; set; }
        public string version { get; set; }
        public string dDepotCountry { get; set; }
        public string dDepot { get; set; }
        public string dDepotStr { get; set; }
        public string sSort { get; set; }
        public string dSort { get; set; }
    }
}

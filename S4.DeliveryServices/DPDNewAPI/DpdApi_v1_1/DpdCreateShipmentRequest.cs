﻿using System.Collections.Generic;

namespace S4.DeliveryServices.DPDNewAPI.DpdApi_v1_1
{
    public class DpdCreateShipmentRequest
    {
        public string buCode { get; set; }
        public string customerId { get; set; }
        public List<Shipment> shipments { get; set; }
    }
}
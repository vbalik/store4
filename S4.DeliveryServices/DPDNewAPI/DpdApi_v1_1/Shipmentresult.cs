﻿using System.Collections.Generic;
namespace S4.DeliveryServices.DPDNewAPI.DpdApi_v1_1
{
    public class ShipmentResult
    {
        public int numOrder { get; set; }
        public Shipment shipment { get; set; }
        //public int shipmentId { get; set; }
        //public List<Parcelresult> parcelResults { get; set; }
        public List<Error> errors { get; set; }
    }
}

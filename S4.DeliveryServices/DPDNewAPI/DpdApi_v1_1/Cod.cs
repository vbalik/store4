﻿namespace S4.DeliveryServices.DPDNewAPI.DpdApi_v1_1
{
    public class Cod
    {
        public double amount { get; set; }
        public string currency { get; set; }
        public string paymentType { get; set; }
        public string reference { get; set; }
        public string split { get; set; }
    }

}
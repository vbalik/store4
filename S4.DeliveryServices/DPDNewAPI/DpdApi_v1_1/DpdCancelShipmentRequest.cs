﻿using System.Collections.Generic;

namespace S4.DeliveryServices.DPDNewAPI.DpdApi_v1_1
{
    public class DpdCancelShipmentRequest
    {
        public string customerId { get; set; }
        public List<int> shipmentIdList { get; set; }
    }
}
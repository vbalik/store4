﻿using System.Collections.Generic;

namespace S4.DeliveryServices.DPDNewAPI.DpdApi_v1_1
{
    public class Additionalproductlist
    {
        public string name { get; set; }
        public List<string> elements { get; set; }
        public List<object> nonopsElements { get; set; }
    }
}

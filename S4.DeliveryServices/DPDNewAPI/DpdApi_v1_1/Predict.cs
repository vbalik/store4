﻿namespace S4.DeliveryServices.DPDNewAPI.DpdApi_v1_1
{
    public class Predict
    {
        public string destination { get; set; }
        public string destinationType { get; set; }
        public string type { get; set; }
    }
}
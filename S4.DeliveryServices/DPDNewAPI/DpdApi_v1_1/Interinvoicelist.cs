﻿namespace S4.DeliveryServices.DPDNewAPI.DpdApi_v1_1
{
    public class Interinvoicelist
    {
        public string rcTarif { get; set; }
        public string scTarif { get; set; }
        public string goodsWebPage { get; set; }
        public int cInvoicePosition { get; set; }
        public int qItems { get; set; }
        public string cContent { get; set; }
        public float cAmountLine { get; set; }
        public string cComment { get; set; }
        public string cOrigin { get; set; }
        public int cNetWeight { get; set; }
        public int cGrossWeight { get; set; }
        public string cProdCode { get; set; }
        public string cProdType { get; set; }
        public string cFabricComposition { get; set; }
    }

}
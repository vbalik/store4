﻿using System.Collections.Generic;

namespace S4.DeliveryServices.DPDNewAPI.DpdApi_v1_1
{
    public class Service
    {
        public AdditionalService additionalService { get; set; }
        public string mainServiceCode { get; set; }
        public List<string> mainServiceElementCodes { get; set; }
    }
}
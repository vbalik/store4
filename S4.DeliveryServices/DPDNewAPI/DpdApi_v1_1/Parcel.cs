﻿namespace S4.DeliveryServices.DPDNewAPI.DpdApi_v1_1
{
    public class Parcel
    {
        public int parcelId { get; set; }
        public string parcelNumber { get; set; }
        public string parcelNumberCckey { get; set; }
        public string barcodeText { get; set; }
        public int dimensionHeight { get; set; }
        public int dimensionLength { get; set; }
        public int dimensionWidth { get; set; }
        public bool limitedQuantity { get; set; }
        public string reference1 { get; set; }
        public string reference2 { get; set; }
        public string reference3 { get; set; }
        public string reference4 { get; set; }
        public double weight { get; set; }
        public string insCurrency { get; set; }
        public string codCurrency { get; set; }
        public string soCode { get; set; }
    }
}
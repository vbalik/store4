﻿namespace S4.DeliveryServices.DPDNewAPI.DpdApi_v1_1
{
    public class Error
    {
        public string errorCode { get; set; }
        public string errorContent { get; set; }
    }
}
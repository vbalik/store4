﻿using System.Collections.Generic;

namespace S4.DeliveryServices.DPDNewAPI.DpdApi_v1_1
{
    public class Shipment
    {
        public int shipmentId { get; set; }
        public int numOrder { get; set; }
        public string senderAddressId { get; set; }
        public Sender sender { get; set; }
        public Receiver receiver { get; set; }
        public Inter inter { get; set; }
        public List<Parcel> parcels { get; set; }
        public Service service { get; set; }
        public string pickupDate { get; set; }
        public string fromTime { get; set; }
        public string toTime { get; set; }
        public string reference1 { get; set; }
        public string reference2 { get; set; }
        public string reference3 { get; set; }
        public string reference4 { get; set; }
        public string saveMode { get; set; }
        public string printFormat { get; set; }
        public string optionalDirectives { get; set; }
        public string labelSize { get; set; }
        public bool extendShipmentData { get; set; }
        public bool printRef1AsBarcode { get; set; }
        public string originalCustomerId { get; set; }
        public Returnconsolidation returnConsolidation { get; set; }
        public int shpParcelCounts { get; set; }
        public float shpWeight { get; set; }
        public string mpsidCckey { get; set; }
        public string customerId { get; set; }
        public string createDate { get; set; }
        public string createTime { get; set; }
        public string printDate { get; set; }
        public string printTime { get; set; }
        public string sendingBuCode { get; set; }
        public string updateDate { get; set; }
        public string updateTime { get; set; }
        public Routing routing { get; set; }
        public string shpReference1 { get; set; }
        public string shpReference2 { get; set; }
        public string shpReference3 { get; set; }
        public string shpReference4 { get; set; }
        public int status { get; set; }
        public string mpsId { get; set; }
        public string sDepot { get; set; }
    }

}
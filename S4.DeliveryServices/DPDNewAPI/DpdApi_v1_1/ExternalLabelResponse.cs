﻿using System.Collections.Generic;

namespace S4.DeliveryServices.DPDNewAPI.DpdApi_v1_1
{
    public class ExternalLabelResponse
    {
        public List<Error> errors { get; set; }
        public string pdfFile { get; set; }
        public int transactionId { get; set; }
    }
}
﻿using System.Collections.Generic;

namespace S4.DeliveryServices.DPDNewAPI.DpdApi_v1_1
{
    public class DpdCreateShipmentResponse
    {
        public int transactionId { get; set; }
        public List<ShipmentResult> shipmentResults { get; set; }
    }
}

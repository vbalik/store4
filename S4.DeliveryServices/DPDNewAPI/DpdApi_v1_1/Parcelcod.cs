﻿namespace S4.DeliveryServices.DPDNewAPI.DpdApi_v1_1
{
    public class Parcelcod
    {
        public float codAmount { get; set; }
        public string currency { get; set; }
        public string paymentType { get; set; }
        public string reference { get; set; }
    }
}

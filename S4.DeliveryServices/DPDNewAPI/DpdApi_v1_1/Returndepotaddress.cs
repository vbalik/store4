﻿namespace S4.DeliveryServices.DPDNewAPI.DpdApi_v1_1
{
    public class Returndepotaddress
    {
        public string name { get; set; }
        public string name2 { get; set; }
        public string companyName { get; set; }
        public string countryCode { get; set; }
        public string zipCode { get; set; }
        public string city { get; set; }
        public string street { get; set; }
        public string houseNo { get; set; }
        public string flatNo { get; set; }
        public string address2 { get; set; }
        public string address3 { get; set; }
        public string department { get; set; }
        public string floor { get; set; }
        public string doorCode { get; set; }
        public string building { get; set; }
        public string contactName { get; set; }
        public string contactPhonePrefix { get; set; }
        public string contactPhone { get; set; }
        public string contactFaxPrefix { get; set; }
        public string contactFax { get; set; }
        public string contactInterphoneName { get; set; }
        public string contactEmail { get; set; }
        public string additionalAddressInfo { get; set; }
        public string longitude { get; set; }
        public string latitude { get; set; }
        public string stateCode { get; set; }
    }

}
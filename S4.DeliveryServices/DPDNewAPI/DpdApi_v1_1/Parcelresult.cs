﻿using System.Collections.Generic;

namespace S4.DeliveryServices.DPDNewAPI.DpdApi_v1_1
{
    public class Parcelresult
    {
        public int parcelId { get; set; }
        public string parcelNumber { get; set; }
    }
}
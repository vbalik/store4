﻿using System.Net.Http.Headers;
using System.Net;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Net.Http;
using System;
using Newtonsoft.Json;
using System.Text;
using S4.DeliveryServices.DPDNewAPI.DpdApi_v1_1;

namespace S4.DeliveryServices.DPDNewAPI
{
    public class DpdShipmentService
    {

        private const string _serverBaseUrl = "https://shipping.dpdgroup.com";
        private const string _shippingResource = "/api/v1.1/shipments";
        private const string _cancelShippingResource = "/api/v1.1/shipments/cancellation";
        private const string _cancelCollectionResource = "/api/v1.1/collections/cancel";
        private const string _labelsResource = "/api/v1.1/labels";

        private string _jwtToken = string.Empty;
        public string JwtToken { get { return string.Empty; } set { _jwtToken = value; } }
        public string ResponseJson { get; private set; } = string.Empty;


        public DpdShipmentService SetJwtToken(string token)
        {
            JwtToken = token;
            return this;
        }


        public async Task<DpdCreateShipmentResponse> CreateDpdShipment(DpdCreateShipmentRequest createShipmentRequest)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _jwtToken);
                client.BaseAddress = new Uri(_serverBaseUrl);

                var req = JsonConvert.SerializeObject(createShipmentRequest, new JsonSerializerSettings() { DefaultValueHandling = DefaultValueHandling.Ignore });
                using (var stringContent = new StringContent(req, Encoding.UTF8, "application/json"))
                {
                    var httpResponse = await client.PostAsync(_shippingResource, stringContent);
                    ResponseJson = await httpResponse.Content.ReadAsStringAsync();
                    if (httpResponse.StatusCode == HttpStatusCode.OK)
                    {
                        var dpdCreateShipmentResponse = JsonConvert.DeserializeObject<DpdCreateShipmentResponse>(ResponseJson);
                        return dpdCreateShipmentResponse;
                    }
                    else
                        throw new DPDRequestException($"Response error {httpResponse.StatusCode}", ResponseJson);
                }
            }
        }


        public async Task<DpdCancelShipmentResponse> CancelDpdShipments(string customerId, List<int> shipmentIds)
        {
            var cancellationRequest = new DpdCancelShipmentRequest() { customerId = customerId, shipmentIdList = shipmentIds };

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _jwtToken);
                client.BaseAddress = new Uri(_serverBaseUrl);

                var req = JsonConvert.SerializeObject(cancellationRequest, new JsonSerializerSettings() { DefaultValueHandling = DefaultValueHandling.Ignore });
                using (var stringContent = new StringContent(req, Encoding.UTF8, "application/json"))
                {
                    var httpResponse = await client.PutAsync(_cancelShippingResource, stringContent);
                    ResponseJson = await httpResponse.Content.ReadAsStringAsync();
                    if (httpResponse.StatusCode == HttpStatusCode.OK)
                    {
                        var dpdCancelShipmentResponse = JsonConvert.DeserializeObject<DpdCancelShipmentResponse>(ResponseJson);
                        return dpdCancelShipmentResponse;
                    }
                    else
                        throw new DPDRequestException($"Response error {httpResponse.StatusCode}", ResponseJson);
                }
            }
        }


        public async Task<ExternalLabelResponse> GetLabelPdf(int shipmentId)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _jwtToken);
                client.BaseAddress = new Uri(_serverBaseUrl);
                var uri = $"{_labelsResource}?shipmentId={shipmentId}&labelSize=A6&printFormat=pdf";
                var httpResponse = await client.GetAsync(uri);
                ResponseJson = await httpResponse.Content.ReadAsStringAsync();
                if (httpResponse.StatusCode == HttpStatusCode.OK)
                {
                    var externalLabelResponse = JsonConvert.DeserializeObject<ExternalLabelResponse>(ResponseJson);
                    return externalLabelResponse;
                }
                else
                    throw new DPDRequestException($"Response error {httpResponse.StatusCode}", ResponseJson);
            }
        }

        public async Task<Shipmentstatus> RequestShipmentStatus(string shipmentReference)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _jwtToken);
                client.BaseAddress = new Uri(_serverBaseUrl);

                var httpResponse = await client.GetAsync($"{_shippingResource}/{shipmentReference}");
                ResponseJson = await httpResponse.Content.ReadAsStringAsync();
                if (httpResponse.StatusCode == HttpStatusCode.OK)
                {
                    var shipmentStatusResponse = JsonConvert.DeserializeObject<Shipmentstatus>(ResponseJson);
                    return shipmentStatusResponse;
                }
                else
                    throw new DPDRequestException($"Response error {httpResponse.StatusCode}", ResponseJson);
                
            }
        }
    }
}

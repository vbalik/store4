﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S4mp.Settings
{
    // singleton current instance info
    public class BaseInfo
    {
        public enum AppStatusEnum
        {
            Unknown,
            Started,
            LoggedOn,

            ConnectionError,

            Locked
        }


        private object _lock = new object();

        private AppStatusEnum _appStatus = AppStatusEnum.Unknown;

        private string _currentWorkerID;
        private bool _terminalHubConnected;

        public AppStatusEnum AppStatus
        {
            get { lock (_lock) { return _appStatus; } }
            set { lock (_lock) { _appStatus = value; } }
        }

        /// <summary>
        /// Gets or sets the current user identifier.
        /// </summary>
        /// <value>
        /// The current user identifier.
        /// </value>
        public string CurrentWorkerID
        {
            get { lock (_lock) { return _currentWorkerID; } }
            set { lock (_lock) { _currentWorkerID = value; } }
        }


        public bool TerminalHubConnected
        {
            get { lock (_lock) { return _terminalHubConnected; } }
            set { lock (_lock) { _terminalHubConnected = value; } }
        }


        public void Clear()
        {
            CurrentWorkerID = null;
            AppStatus = AppStatusEnum.Started;
        }
    }
}

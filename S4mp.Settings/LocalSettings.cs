﻿using System;
using System.IO;
using System.Xml.Linq;
using System.Linq;
using System.Text;
using System.Xml;

using Xamarin;
using Xamarin.Forms;

namespace S4mp.Settings
{
    public class LocalSettings
    {
        private object _lock = new object();
        private string _settingsFilePath = null;
        private XDocument _xml;

        public LocalSettings()
        {
        }


        public string XmlString
        {
            get { return _xml.ToString(); }
        }


        public string this[string key]
        {
            get
            {
                lock (_lock)
                {
                    var values = from n in _xml.Root.Elements()
                                 where n.Name == key
                                 select n.Value;
                    if (values.Any())
                    {
                        return values.First();
                    }
                    return null;
                }
            }
            set
            {
                lock (_lock)
                {
                    var values = from n in _xml.Root.Elements()
                                 where n.Name == key
                                 select n;
                    if (values.Any())
                    {
                        values.First().Value = value;
                    }
                    else
                    {
                        _xml.Root.Add(new XElement(key, value));
                    }
                }
            }
        }


        public void Init(string debugSettingsXml)
        {
            lock (_lock)
            {
                var folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                _settingsFilePath = Path.Combine(folder, "LocalSettings.xml");

                if (debugSettingsXml != null)
                {
                    var xmlDocument = new XmlDocument();
                    xmlDocument.LoadXml(debugSettingsXml);
                    xmlDocument.Save(_settingsFilePath);
                }
                else
                {
                    if (!File.Exists(_settingsFilePath))
                    {
                        var xmlDocument = new XmlDocument();
                        xmlDocument.LoadXml($"<settings><api_url>http://10.0.0.1</api_url><textsize>Medium</textsize></settings>");
                        xmlDocument.Save(_settingsFilePath);
                    }
                }

                _xml = XDocument.Load(_settingsFilePath);
            }
        }


        public void Save()
        {
            lock (_lock)
            {
                File.WriteAllText(_settingsFilePath, XmlString);
            }
        }
    }
}


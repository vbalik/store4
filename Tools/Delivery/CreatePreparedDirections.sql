declare @directionID int;
declare crDirection cursor local for
select top(1) directionID from s4_direction where docStatus = 'DCHE' order by docDate desc;
open crDirection;
fetch next from crDirection into @directionID;
while @@FETCH_STATUS = 0
begin
	insert into s4_direction_xtra(directionID, xtraCode, docPosition, xtraValue) values(@directionID, 'TTYPE', 0, 'Doprava DPD');
	insert into s4_direction_xtra(directionID, xtraCode, docPosition, xtraValue) values(@directionID, 'DI_BOX_CNT', 0, '2');
	insert into s4_direction_xtra(directionID, xtraCode, docPosition, xtraValue) values(@directionID, 'DI_WEIGHT', 1, '2.5');
	insert into s4_direction_xtra(directionID, xtraCode, docPosition, xtraValue) values(@directionID, 'DI_WEIGHT', 2, '3.5');
	insert into s4_direction_xtra(directionID, xtraCode, docPosition, xtraValue) values(@directionID, 'DOD', 0, '1500');
	insert into s4_direction_xtra(directionID, xtraCode, docPosition, xtraValue) values(@directionID, 'PTYPE', 0, 'Platba dob�rkou');
	insert into s4_direction_xtra(directionID, xtraCode, docPosition, xtraValue) values(@directionID, 'DCOUN', 0, 'CZ');
	insert into s4_direction_xtra(directionID, xtraCode, docPosition, xtraValue) values(@directionID, 'DCURR', 0, 'CZK');
	fetch next from crDirection into @directionID;
end;

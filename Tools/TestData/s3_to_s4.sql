INSERT INTO [dbo].[s4_workerList]
	(workerID, workerName, workerClass, workerStatus, workerLogon, workerSecData)
SELECT 
	RTRIM(workerID), workerName, workerClass, workerStatus, workerLogon,  CONVERT(VARCHAR(256), workerSecData, 2)
FROM
	[dbo].[__s3_workerList]



INSERT INTO [dbo].[s4_sectionList]
	(sectID, sectDesc, source_id)
SELECT 
	RTRIM(sectID), sectDesc, source_id
FROM
	[dbo].[__s3_sectionList]



INSERT INTO [dbo].[s4_manufactList]
	(manufID, manufName, manufStatus, manufSettings, source_id, useBatch, useExpiration, sectID)
SELECT 
	RTRIM(REPLACE(REPLACE(manufID, CHAR(13), ''), CHAR(10), '')), manufName, manufStatus, manufSettings, source_id, def_useBatch, def_useExpiration, RTRIM(def_sectID)
FROM
	[dbo].[__s3_manufactList]



SET IDENTITY_INSERT [dbo].[s4_articleList] ON;

INSERT INTO [dbo].[s4_articleList]
	(articleID, articleCode, articleDesc, articleStatus, articleType, source_id, sectID, unitDesc, manufID, useBatch, mixBatch, useExpiration)
SELECT 
	articleID, articleCode, articleDesc, 
	CASE WHEN articleStatus = 0 THEN 'AR_OK' ELSE 'AR_DISABLED' END, 
	articleType, source_id, RTRIM(sectID), unitDesc, RTRIM(REPLACE(REPLACE(manufID, CHAR(13), ''), CHAR(10), '')), useBatch, mixBatch, useExpiration
FROM
	[dbo].[__s3_articleList]

SET IDENTITY_INSERT [dbo].[s4_articleList] OFF;
GO



SET IDENTITY_INSERT [dbo].[s4_articleList_packings] ON;

INSERT INTO [dbo].[s4_articleList_packings]
	(artPackID, articleID, packDesc, packRelation, movablePack, source_id, packStatus, barCodeType, barCode, inclCarrier, packWeight, packVolume)
SELECT 
	artPackID, articleID, packDesc, packRelation, movablePack, source_id, 
	CASE WHEN packStatus = 0 THEN 'AP_OK' ELSE 'AP_DISABLED' END, 
	barCodeType, barCode, inclCarrier, packWeight, packVolume
FROM
	[dbo].[__s3_articleList_packings]

SET IDENTITY_INSERT [dbo].[s4_articleList_packings] OFF;
GO



SET IDENTITY_INSERT [dbo].[s4_partnerList] ON;

INSERT INTO [dbo].[s4_partnerList]
	(partnerID, partnerName, source_id, partnerStatus, partnerDispCheck, partnerAddr_1, partnerAddr_2, partnerCity, partnerPostCode, partnerPhone)
SELECT 
	partnerID, partnerName, source_id, 
	CASE WHEN partnerStatus = 0 THEN 'PA_OK' ELSE 'PA_DISABLED' END, 
	partnerDispCheck, partnerAddr_1, partnerAddr_2, partnerCity, partnerPostCode, partnerPhone
FROM
	[dbo].[__s3_partnerList]
WHERE partnerID > 1

SET IDENTITY_INSERT [dbo].[s4_partnerList] OFF;
GO


SET IDENTITY_INSERT [dbo].[s4_partnerList_addresses] ON;

INSERT INTO [dbo].[s4_partnerList_addresses]
	(partAddrID, partnerID, addrName, source_id, addrLine_1, addrLine_2, addrCity, addrPostCode, addrPhone, addrRemark, deliveryInst)
SELECT 
	partAddrID, partnerID, addrName, source_id, addrLine_1, addrLine_2, addrCity, addrPostCode, addrPhone, addrRemark, deliveryInst
FROM
	[dbo].[__s3_partnerList_addresses]
WHERE partnerID > 1

SET IDENTITY_INSERT [dbo].[s4_partnerList_addresses] OFF;
GO


SET IDENTITY_INSERT [dbo].[s4_direction] ON;

INSERT INTO [dbo].[s4_direction]
	([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[partnerRef],[docRemark],[entryDateTime],[entryUserID])
SELECT 
	[directionID],RTRIM([docStatus]),RTRIM([docNumPrefix]),[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[partnerRef],[docRemark],[entryDateTime],[entryUserID]
FROM
	[dbo].[__s3_direction]

SET IDENTITY_INSERT [dbo].[s4_direction] OFF;
GO


SET IDENTITY_INSERT [dbo].[s4_direction_assign] ON;

INSERT INTO [dbo].[s4_direction_assign]
	(directAssignID, directionID, jobID, workerID, assignParams, entryDateTime, entryUserID)
SELECT 
	directAssignID, directionID, RTRIM(jobID), RTRIM(workerID), RTRIM(assignParams), entryDateTime, entryUserID
FROM
	[dbo].[__s3_direction_assign]

SET IDENTITY_INSERT [dbo].[s4_direction_assign] OFF;
GO


SET IDENTITY_INSERT [dbo].[s4_direction_items] ON;

INSERT INTO [dbo].[s4_direction_items]
	(directionItemID, directionID, docPosition, itemStatus, artPackID, articleCode, unitDesc, quantity, unitPrice, partnerDepart, entryDateTime, entryUserID)
SELECT 
	directionItemID, directionID, docPosition, RTRIM(itemStatus), artPackID, articleCode, unitDesc, quantity, unitPrice, partnerDepart, RTRIM(entryDateTime), entryUserID
FROM
	[dbo].[__s3_direction_items]

SET IDENTITY_INSERT [dbo].[s4_direction_items] OFF;
GO


SET IDENTITY_INSERT [dbo].[s4_direction_xtra] ON;

INSERT INTO [dbo].[s4_direction_xtra]
	(directionExID, directionID, xtraCode, docPosition, xtraValue)
SELECT 
	directionExID, directionID, RTRIM(xtraCode), docPosition, xtraValue
FROM
	[dbo].[__s3_direction_xtra]

SET IDENTITY_INSERT [dbo].[s4_direction_xtra] OFF;
GO



SET IDENTITY_INSERT [dbo].[s4_direction_history] ON;

INSERT INTO [dbo].[s4_direction_history]
	(directionHistID, directionID, docPosition, eventCode, eventData, entryDateTime, entryUserID)
SELECT 
	directionHistID, directionID, docPosition, RTRIM(eventCode), ISNULL(smallData, bigData), entryDateTime, RTRIM(entryUserID)
FROM
	[dbo].[__s3_direction_history]

SET IDENTITY_INSERT [dbo].[s4_direction_history] OFF;
GO


INSERT INTO [dbo].[s4_houseList]
	(houseID, houseDesc, houseMap )
SELECT
	houseID, houseDesc, houseMap 
FROM
	[dbo].[__s3_houseList]

GO


SET IDENTITY_INSERT [dbo].[s4_positionList] ON;

INSERT INTO [dbo].[s4_positionList]
	(positionID, posCode, posDesc, houseID, posCateg, posStatus, posHeight, posX, posY, posZ, mapPos)
SELECT 
	[positionID], [posCode], [posDesc], [houseID], [posCateg], 
	CASE [posStatus] WHEN 0 THEN 'POS_OK' WHEN 255 THEN 'POS_CANCELED' ELSE 'POS_BLOCKED' END, 
	[posHeight], [posX], [posY], [posZ], [mapPos]
FROM
	[dbo].[__s3_positionList]
WHERE [positionID] > 1

SET IDENTITY_INSERT [dbo].[s4_positionList] OFF;
GO


INSERT INTO [dbo].[s4_positionList_sections]
	(sectID, positionID)
SELECT 
	sectID, positionID
FROM
	[dbo].[__s3_positionList_sections]



SET IDENTITY_INSERT [dbo].[s4_stocktaking] ON;

INSERT INTO [dbo].[s4_stocktaking]
	(stotakID, stotakStatus, stotakDesc, beginDate, endDate, entryDateTime, entryUserID)
SELECT 
	stotakID, 
	CASE stotakStatus WHEN 0 THEN 'OPENED' WHEN 10 THEN 'SAVED' ELSE 'CANCELED' END, 
	stotakDesc, beginDate, endDate, entryDateTime, RTRIM(entryUserID)
FROM
	[dbo].[__s3_stocktaking]

SET IDENTITY_INSERT [dbo].[s4_stocktaking] OFF;
GO


SET IDENTITY_INSERT [dbo].[s4_stocktaking_history] ON;

INSERT INTO [dbo].[s4_stocktaking_history]
	(stotakHistID, stotakID, eventCode, positionID, artPackID, carrierNum, quantity, batchNum, expirationDate, entryDateTime, entryUserID)
SELECT 
	stotakHistID, stotakID, eventCode, positionID, artPackID, carrierNum, quantity, batchNum, expirationDate, entryDateTime, RTRIM(entryUserID)
FROM
	[dbo].[__s3_stocktaking_history]
WHERE	artPackID IN (SELECT artPackID FROM [dbo].[s4_articleList_packings])

SET IDENTITY_INSERT [dbo].[s4_stocktaking_history] OFF;
GO


DISABLE TRIGGER [s4_stocktaking_items_delete] ON [dbo].[s4_stocktaking_items];
DISABLE TRIGGER [s4_stocktaking_items_insert_update] ON [dbo].[s4_stocktaking_items]; 
SET IDENTITY_INSERT [dbo].[s4_stocktaking_items] ON;

INSERT INTO [dbo].[s4_stocktaking_items]
	(stotakItemID, stotakID, positionID, artPackID, quantity, carrierNum, batchNum, expirationDate, entryDateTime, entryUserID)
SELECT 
	stotakItemID, stotakID, positionID, artPackID, quantity, carrierNum, batchNum, 
	CASE WHEN expirationDate IS NULL THEN '1900-01-01' ELSE expirationDate END, 
	entryDateTime, RTRIM(entryUserID)
FROM
	[dbo].[__s3_stocktaking_items]
WHERE	artPackID IN (SELECT artPackID FROM [dbo].[s4_articleList_packings])

SET IDENTITY_INSERT [dbo].[s4_stocktaking_items] OFF;
ENABLE TRIGGER [s4_stocktaking_items_delete] ON [dbo].[s4_stocktaking_items];
ENABLE TRIGGER [s4_stocktaking_items_insert_update] ON [dbo].[s4_stocktaking_items]; 
GO


SET IDENTITY_INSERT [dbo].[s4_stocktaking_positions] ON;

INSERT INTO [dbo].[s4_stocktaking_positions]
	(stotakPosID, stotakID, positionID, checkCounter)
SELECT 
	stotakPosID, stotakID, positionID, checkCounter
FROM
	[dbo].[__s3_stocktaking_positions]

SET IDENTITY_INSERT [dbo].[s4_stocktaking_positions] OFF;
GO


SET IDENTITY_INSERT [dbo].[s4_stocktaking_snapshots] ON;

INSERT INTO [dbo].[s4_stocktaking_snapshots]
	(stotakSnapID, stotakID, snapshotClass, positionID, artPackID, carrierNum, quantity, batchNum, expirationDate, unitPrice)
SELECT 
	stotakSnapID, stotakID, snapshotClass, positionID, artPackID, carrierNum, quantity, batchNum, 
	CASE WHEN expirationDate IS NULL THEN '1900-01-01' ELSE expirationDate END,
	unitPrice
FROM
	[dbo].[__s3_stocktaking_snapshots]
WHERE	artPackID IN (SELECT artPackID FROM [dbo].[s4_articleList_packings])

SET IDENTITY_INSERT [dbo].[s4_stocktaking_snapshots] OFF;
GO



SET IDENTITY_INSERT [dbo].[s4_storeMove] ON;

INSERT INTO [dbo].[s4_storeMove]
	(stoMoveID, docStatus, docNumPrefix, docType, docNumber, docDate, parent_directionID, docRemark, entryDateTime, entryUserID)
SELECT 
	stoMoveID, docStatus, docNumPrefix, docType, docNumber, docDate, parent_directionID, docRemark, entryDateTime, RTRIM(entryUserID)
FROM
	[dbo].[__s3_storeMove]

SET IDENTITY_INSERT [dbo].[s4_storeMove] OFF;
GO


SET IDENTITY_INSERT [dbo].[s4_storeMove_history] ON;

INSERT INTO [dbo].[s4_storeMove_history]
	(stoMoveHistID, stoMoveID, docPosition, eventCode, [eventData], entryDateTime, entryUserID)
SELECT 
	stoMoveHistID, stoMoveID, docPosition, eventCode, ISNULL(smallData, bigData), entryDateTime, RTRIM(entryUserID)	
FROM
	[dbo].[__s3_storeMove_history]

SET IDENTITY_INSERT [dbo].[s4_storeMove_history] OFF;
GO



INSERT INTO [dbo].[s4_storeMove_lots]
	(artPackID, batchNum, expirationDate)
SELECT	[artPackID], [batchNum], ISNULL([expirationDate], '1900-01-01')
FROM	[dbo].[__s3_storeMove_items]
WHERE [artPackID] IN (SELECT [artPackID] FROM [dbo].[s4_articleList_packings])
GROUP BY 
	[artPackID], [batchNum], [expirationDate]



SET IDENTITY_INSERT [dbo].[s4_storeMove_items] ON;

INSERT INTO [dbo].[s4_storeMove_items]
	(stoMoveItemID, stoMoveID, docPosition, itemStatus, itemValidity, itemDirection, stoMoveLotID, positionID, carrierNum, quantity, parent_docPosition, brotherID, entryDateTime, entryUserID)
SELECT
	stoMoveItemID, stoMoveID, docPosition, itemStatus, 
	CASE WHEN itemStatus = 'CANC' THEN 255 WHEN itemStatus  IN ('SDIM','SDIS','SEXP','SMOV','SREC','SRPC','SSTI','STOK') THEN 100 ELSE 0 END AS itemValidity,
	itemDirection, stoMoveLotID, positionID, carrierNum, quantity, parent_docPosition, brotherID, entryDateTime, RTRIM(entryUserID)
FROM	[dbo].[__s3_storeMove_items] si JOIN [dbo].[s4_storeMove_lots] lot ON (si.[artPackID] = lot.[artPackID] AND si.[batchNum] = lot.[batchNum] AND ISNULL(si.[expirationDate], '1900-01-01') = lot.[expirationDate] )


SET IDENTITY_INSERT [dbo].[s4_storeMove_items] OFF;
GO



DROP TABLE [dbo].[__s3_articleList]
DROP TABLE [dbo].[__s3_articleList_packings]
DROP TABLE [dbo].[__s3_direction]
DROP TABLE [dbo].[__s3_direction_assign]
DROP TABLE [dbo].[__s3_direction_history]
DROP TABLE [dbo].[__s3_direction_items]
DROP TABLE [dbo].[__s3_direction_xtra]
DROP TABLE [dbo].[__s3_manufactList]
DROP TABLE [dbo].[__s3_sectionList]
DROP TABLE [dbo].[__s3_workerList]

DROP TABLE [dbo].[__s3_positionList_sections]
DROP TABLE [dbo].[__s3_positionList]
DROP TABLE [dbo].[__s3_houseList]

DROP TABLE [dbo].[__s3_stocktaking]
DROP TABLE [dbo].[__s3_stocktaking_history]
DROP TABLE [dbo].[__s3_stocktaking_items]
DROP TABLE [dbo].[__s3_stocktaking_positions]

DROP TABLE [dbo].[__s3_storeMove]
DROP TABLE [dbo].[__s3_storeMove_history]
DROP TABLE [dbo].[__s3_storeMove_items]
GO

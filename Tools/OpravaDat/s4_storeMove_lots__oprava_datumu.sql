ALTER TABLE [dbo].[s4_onStoreSummary_items]  WITH CHECK ADD  CONSTRAINT [FK_s4_onStoreSummary_items_s4_storeMove_lots] FOREIGN KEY([stoMoveLotID])
REFERENCES [dbo].[s4_storeMove_lots] ([stoMoveLotID])
GO
ALTER TABLE [dbo].[s4_onStoreSummary_items] CHECK CONSTRAINT [FK_s4_onStoreSummary_items_s4_storeMove_lots]
GO




SELECT [stoMoveLotID]
      ,[artPackID]
      ,[batchNum]
      ,[expirationDate]
	  ,convert(varchar(8), expirationDate, 108),
	  CONVERT(date, expirationDate)
FROM [dbo].[s4_storeMove_lots]
WHERE convert(varchar(8), expirationDate, 108) <> '00:00:00'
ORDER BY [stoMoveLotID] DESC




SELECT
    [artPackID],
    [batchNum],
	CONVERT(date, expirationDate),
	COUNT(*)
FROM [dbo].[s4_storeMove_lots]
GROUP BY
	[artPackID],
    [batchNum],
	CONVERT(date, expirationDate)
HAVING COUNT(*) > 1
ORDER BY 
	[artPackID],
    [batchNum],
	CONVERT(date, expirationDate)


--DROP TABLE _tmp_lotsToRepair

SELECT 
	lots.*,
	(SELECT MIN(stoMoveLotID) FROM [dbo].[s4_storeMove_lots] l1 WHERE (lots.artPackID = l1.artPackID AND lots.batchNum = l1.batchNum AND CONVERT(date, lots.expirationDate) = CONVERT(date, l1.expirationDate))) AS stoMoveLotID_to
INTO _tmp_lotsToRepair
FROM 
	(
	SELECT
		[artPackID],
		[batchNum],
		CONVERT(date, expirationDate) AS expirationDate,
		COUNT(*) AS cnt
	FROM [dbo].[s4_storeMove_lots]
	GROUP BY
		[artPackID],
		[batchNum],
		CONVERT(date, expirationDate)
	HAVING COUNT(*) > 1
	) list
	JOIN [dbo].[s4_storeMove_lots] lots	
		ON (lots.artPackID = list.artPackID AND lots.batchNum = list.batchNum AND CONVERT(date, lots.expirationDate) = list.expirationDate)
		
ORDER BY
	lots.[artPackID],
    lots.[batchNum],
	lots.expirationDate


SELECT	*
FROM	_tmp_lotsToRepair




--	--139096	0009777066	01.07.2021	4
--SELECT *
--FROM [dbo].[s4_storeMove_lots]
--WHERE	artPackID = 139096 AND batchNum = '0009777066' 


SELECT	*
FROM	[dbo].[s4_storeMove_lots]
WHERE	stoMoveLotID IN (439751, 436740)


-- update
SELECT	*
--UPDATE [dbo].[s4_storeMove_items] SET [dbo].[s4_storeMove_items].stoMoveLotID = rep.stoMoveLotID_to
FROM	[dbo].[s4_storeMove_items] itm
		JOIN _tmp_lotsToRepair rep
		ON itm.stoMoveLotID = rep.stoMoveLotID
WHERE	(rep.stoMoveLotID <> rep.stoMoveLotID_to)
		--AND positionID = 17312
		--AND itm.stoMoveLotID = 439751



DISABLE TRIGGER [s4_storeMove_lots_delete] ON [dbo].[s4_storeMove_lots];
GO  
DISABLE TRIGGER [s4_storeMove_lots_update] ON [dbo].[s4_storeMove_lots];
GO  


--TRUNCATE TABLE [dbo].[s4_onStoreSummary_items]
--DELETE [dbo].[s4_onStoreSummary]

-- delete not used
--SELECT	stoMoveLotID
--FROM	
--		_tmp_lotsToRepair rep
--WHERE	(rep.stoMoveLotID <> rep.stoMoveLotID_to)
--		AND (NOT EXISTS (SELECT * FROM [dbo].[s4_storeMove_items] itm WHERE rep.stoMoveLotID = itm.stoMoveLotID))

SELECT	*
FROM	[dbo].[s4_storeMove_lots]
WHERE 
		NOT EXISTS (SELECT * FROM [dbo].[s4_storeMove_items] itm WHERE [dbo].[s4_storeMove_lots].stoMoveLotID = itm.stoMoveLotID)
		AND
		NOT EXISTS (SELECT * FROM [dbo].[s4_onStoreSummary_items] itm WHERE [dbo].[s4_storeMove_lots].stoMoveLotID = itm.stoMoveLotID)


--DELETE [dbo].[s4_storeMove_lots] 
	WHERE 
	NOT EXISTS (SELECT * FROM [dbo].[s4_storeMove_items] itm WHERE [dbo].[s4_storeMove_lots].stoMoveLotID = itm.stoMoveLotID)
	AND
	NOT EXISTS (SELECT * FROM [dbo].[s4_onStoreSummary_items] itm WHERE [dbo].[s4_storeMove_lots].stoMoveLotID = itm.stoMoveLotID)



-- update lots
SELECT	*
-- UPDATE [dbo].[s4_storeMove_lots] SET expirationDate = CONVERT(date, expirationDate)
FROM	[dbo].[s4_storeMove_lots]
WHERE	CONVERT(date, expirationDate) <> expirationDate


ENABLE TRIGGER [s4_storeMove_lots_delete] ON [dbo].[s4_storeMove_lots];
GO  
ENABLE TRIGGER [s4_storeMove_lots_update] ON [dbo].[s4_storeMove_lots];
GO 





--SELECT	*
----DELETE
--FROM	[dbo].[s4_onStoreSummary_items]
--WHERE	NOT stoMoveLotID IN (SELECT stoMoveLotID FROM [dbo].[s4_storeMove_lots])



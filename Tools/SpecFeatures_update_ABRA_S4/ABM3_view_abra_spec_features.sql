USE [ABM3_Promedica]
GO

/****** Object:  View [dbo].[abra_spec_features]    Script Date: 03.05.2021 12:35:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




ALTER VIEW [dbo].[abra_spec_features]
AS

SELECT 
	id, hidden, code, cooled, cyto, medicines, obsahuje_ochr_kod
FROM
	[ABRA-DB-PPG]..[S01_PPG].[ABMEXCHANGE_SPEC_FEATURES]

GO



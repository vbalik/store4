
  CREATE OR REPLACE FORCE VIEW "S01_PPG"."ABMEXCHANGE_SPEC_FEATURES" ("ID", "HIDDEN", "CODE", "COOLED", "CYTO", "MEDICINES", "OBSAHUJE_OCHR_KOD") AS 
  select sc.id, sc.hidden, sc.code, sc.X_ChlazenyRezim as cooled, case drd1.code when 'LéčivoCYTO' then 'A' else 'N' end as cyto, ud.stringfieldvalue as medicines, case x_fmd when 1 then 'A' else 'N' end as obsahuje_ochr_kod
  from storecards sc 
    left join defrolldata drd1 on drd1.clsid='WNALSCQ0IEC4TFMVIZYUQQCE1G' and drd1.id=sc.X_ProductType_ID
    left join defrolldata drd2 on drd2.clsid='HQWSBIK4QRS41DLBSCUPVLRWM0' and drd2.id=sc.X_ForeignNameID
    left join userdata ud on ud.clsid='HQWSBIK4QRS41DLBSCUPVLRWM0' and ud.fieldcode=2000005 and ud.id=drd2.id
where sc.hidden='N';

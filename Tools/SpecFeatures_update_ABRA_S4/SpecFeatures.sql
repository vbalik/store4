SELECT a.articleCode, a.articleDesc, 
abra.cooled, abra.medicines, abra.cyto, abra.obsahuje_ochr_kod as checkOnReceive,
a.specFeatures, 
(CASE WHEN abra.cooled = 'A' THEN 1 ELSE 0 END + CASE WHEN abra.medicines = 'A' THEN 2 ELSE 0 END + CASE WHEN abra.cyto = 'A' THEN 4 ELSE 0 END + CASE WHEN abra.obsahuje_ochr_kod = 'A' THEN 8 ELSE 0 END) as specFeaturesNew
FROM [192.168.1.14].[ABM3_Promedica].[dbo].[abra_spec_features] abra
LEFT JOIN s4_articleList a on a.source_id = abra.id
where a.articleCode = '0218922'
--where a.specFeatures = 0 AND (abra.cooled = 'A' OR abra.medicines = 'A' OR abra.cyto = 'A' OR abra.obsahuje_ochr_kod = 'A')

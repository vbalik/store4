--begin tran;

UPDATE
    s4
SET
    s4.specFeatures = (CASE WHEN abra.cooled = 'A' THEN 1 ELSE 0 END + CASE WHEN abra.medicines = 'A' THEN 2 ELSE 0 END + CASE WHEN abra.cyto = 'A' THEN 4 ELSE 0 END + CASE WHEN abra.obsahuje_ochr_kod = 'A' THEN 8 ELSE 0 END)
FROM
    [192.168.1.14].[ABM3_Promedica].[dbo].[abra_spec_features] abra
	LEFT JOIN s4_articleList s4 on s4.source_id = abra.id
WHERE
   s4.specFeatures = 0 --AND (abra.cooled = 'A' OR abra.medicines = 'A' OR abra.cyto = 'A' OR abra.obsahuje_ochr_kod = 'A')
   --s4.articleCode = '0218922'

--rollback tran;
--commit tran;
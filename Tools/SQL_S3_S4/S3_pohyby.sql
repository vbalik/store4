select a.articleCode, a.articleDesc, mi.batchNum, CONVERT(varchar,mi.expirationDate, 4) as expirationDate, mi.quantity, CONVERT(varchar,d.docDate, 4) as docDate, d.docInfo, p.partnerName
from [dbo].[s3_storeMove] m
left join [dbo].[s3_storeMove_items] mi on mi.stoMoveID = m.stoMoveID
left join [dbo].[s3_articleList_packings] ap on ap.artPackID = mi.artPackID
left join [dbo].[s3_articleList] a on a.articleID = ap.articleID
left join [s3_direction] d on d.directionID = m.parent_directionID
left join [dbo].[s3_partnerList] p on p.partnerID = d.partnerID
--where a.articleCode in ('7400009A', 'E5MC4007N', 'EMC1402') AND
where a.manufID = 'Deltamed' AND
mi.itemDirection = -1 AND mi.itemStatus = 'SEXP' AND
d.docStatus = 'EXPE' AND d.docDirection = -1 AND YEAR(d.docDate) in (2016,2017)
order by a.articleCode, p.partnerName




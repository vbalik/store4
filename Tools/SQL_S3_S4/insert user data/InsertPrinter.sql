-- add s4_printerType
SET IDENTITY_INSERT [dbo].[s4_printerType] ON;

INSERT INTO [dbo].[s4_printerType] (printerTypeID, printerDesc)
     VALUES (1, 'Laser print')
INSERT INTO [dbo].[s4_printerType] ([printerTypeID], [printerClass], [printerDesc])
     VALUES (4, 1, 'PM43 - uzke')
INSERT INTO [dbo].[s4_printerType] ([printerTypeID], [printerClass], [printerDesc])
     VALUES (5, 2, 'PM43 - siroke')
INSERT INTO [dbo].[s4_printerType] ([printerTypeID], [printerClass], [printerDesc])
     VALUES (6, 1, 'Zebra - uzke')
INSERT INTO [dbo].[s4_printerType] ([printerTypeID], [printerClass], [printerDesc])
     VALUES (7, 2, 'Zebra - siroke')

SET IDENTITY_INSERT [dbo].[s4_printerType] OFF;

--printer location
SET IDENTITY_INSERT [dbo].[s4_printerLocation] ON 
GO

INSERT INTO [dbo].[s4_printerLocation] ([printerLocationID], [printerLocationDesc], [printerPath], [printerSettings], [printerTypeID]) VALUES (1, N'HALA1_HP', N'HALA1_HP', N'{ dispatchPositionCodes: [ "E01", "E02", "E03", "E04", "E05", "E06", "E07" ] }', 1)
GO
INSERT INTO [dbo].[s4_printerLocation] ([printerLocationID], [printerLocationDesc], [printerPath], [printerSettings], [printerTypeID]) VALUES (4, N'HALA2_HP', N'HALA2_HP', N'{ dispatchPositionCodes: [ "E16", "E17", "E18", "E19", "E20", "E21", "E22" ] }', 1)
GO
INSERT INTO [dbo].[s4_printerLocation] ([printerLocationID], [printerLocationDesc], [printerPath], [printerSettings], [printerTypeID]) VALUES (5, N'HALA3_HP', N'HALA3_HP', N'{ dispatchPositionCodes: [ "E08", "E09", "E10", "E11", "E12", "E13", "E14", "E15" ] }', 1)
GO
INSERT INTO [dbo].[s4_printerLocation] ([printerLocationID], [printerLocationDesc], [printerPath], [printerSettings], [printerTypeID]) VALUES (6, N'HALA4_HP', N'HALA4_HP', N'{ dispatchPositionCodes: [ "E23", "E24", "E25" ] }', 1)
GO
INSERT [dbo].[s4_printerLocation] ([printerLocationID], [printerLocationDesc], [printerPath], [printerSettings], [printerTypeID]) VALUES (7, N'X1000 Hala 1', N'HALA1_X1000', NULL, 6)
GO
INSERT [dbo].[s4_printerLocation] ([printerLocationID], [printerLocationDesc], [printerPath], [printerSettings], [printerTypeID]) VALUES (8, N'PM43 Hala 2', N'HALA2_PM43_MALA', NULL, 4)
GO
INSERT [dbo].[s4_printerLocation] ([printerLocationID], [printerLocationDesc], [printerPath], [printerSettings], [printerTypeID]) VALUES (9, N'X1000 NemLog', N'HALA3_X1000', NULL, 6)
GO
INSERT [dbo].[s4_printerLocation] ([printerLocationID], [printerLocationDesc], [printerPath], [printerSettings], [printerTypeID]) VALUES (10, N'PM43 Morava male', N'HALA4_PM43_MALA', NULL, 4)
GO
INSERT [dbo].[s4_printerLocation] ([printerLocationID], [printerLocationDesc], [printerPath], [printerSettings], [printerTypeID]) VALUES (11, N'PM43 Hala3 male', N'HALA3_PM43_MALA', NULL, 4)
GO
INSERT [dbo].[s4_printerLocation] ([printerLocationID], [printerLocationDesc], [printerPath], [printerSettings], [printerTypeID]) VALUES (12, N'G6000 Hala 1', N'HALA1_G6000', NULL, 7)
GO
INSERT [dbo].[s4_printerLocation] ([printerLocationID], [printerLocationDesc], [printerPath], [printerSettings], [printerTypeID]) VALUES (13, N'G6000 Hala 2', N'HALA2_G6000', NULL, 7)
GO
INSERT [dbo].[s4_printerLocation] ([printerLocationID], [printerLocationDesc], [printerPath], [printerSettings], [printerTypeID]) VALUES (14, N'G6000 NemLog', N'HALA3_G6000', NULL, 7)
GO
INSERT [dbo].[s4_printerLocation] ([printerLocationID], [printerLocationDesc], [printerPath], [printerSettings], [printerTypeID]) VALUES (15, N'Lednice', N'\\NB-LEDNICE\PC43T', NULL, 4)
GO
INSERT [dbo].[s4_printerLocation] ([printerLocationID], [printerLocationDesc], [printerPath], [printerSettings], [printerTypeID]) VALUES (16, N'PM43 Morava velke', N'HALA4_PM43_VELKA', NULL, 5)
GO
INSERT [dbo].[s4_printerLocation] ([printerLocationID], [printerLocationDesc], [printerPath], [printerSettings], [printerTypeID]) VALUES (18, N'PM43 MEDTRONIC velke', N'MEDTRONIC_PM43_VELKA', NULL, 5)
GO
INSERT INTO [dbo].[s4_printerLocation] ([printerLocationID], [printerLocationDesc], [printerPath], [printerSettings], [printerTypeID]) VALUES (38, N'* M�stn� *', N'_LOCAL_', NULL, 1)
GO

SET IDENTITY_INSERT [dbo].[s4_printerLocation] OFF
GO

--reports
SET IDENTITY_INSERT [dbo].[s4_reports] ON
INSERT INTO [dbo].[s4_reports] ([reportID], [reportType], [template], [settings], [printerTypeID]) VALUES (1, 'WebPrint', 'x', NULL, 1)
INSERT INTO [dbo].[s4_reports] ([reportID], [reportType], [template], [settings], [printerTypeID]) VALUES (3, 'WebPrint', 'x', NULL, 1)
INSERT INTO [dbo].[s4_reports] ([reportID], [reportType], [template], [settings], [printerTypeID]) VALUES (4, 'WebPrint', 'x', NULL, 1)
INSERT INTO [dbo].[s4_reports] ([reportID], [reportType], [template], [settings], [printerTypeID]) VALUES (5, 'WebPrint', 'x', NULL, 1)
INSERT INTO [dbo].[s4_reports] ([reportID], [reportType], [template], [settings], [printerTypeID]) VALUES (6, 'WebPrint', 'x', NULL, 1)
INSERT INTO [dbo].[s4_reports] ([reportID], [reportType], [template], [settings], [printerTypeID]) VALUES (8, 'WebPrint', 'x', NULL, 1)
INSERT INTO [dbo].[s4_reports] ([reportID], [reportType], [template], [settings], [printerTypeID]) VALUES (9, 'packingLabel', 'x', NULL, 4)
INSERT INTO [dbo].[s4_reports] ([reportID], [reportType], [template], [settings], [printerTypeID]) VALUES (10,'boxLabel', 'x', NULL, 4)
INSERT INTO [dbo].[s4_reports] ([reportID], [reportType], [template], [settings], [printerTypeID]) VALUES (11,'boxLabel', 'x', NULL, 5)
INSERT INTO [dbo].[s4_reports] ([reportID], [reportType], [template], [settings], [printerTypeID]) VALUES (13,'boxLabel', 'x', NULL, 7)
INSERT INTO [dbo].[s4_reports] ([reportID], [reportType], [template], [settings], [printerTypeID]) VALUES (14,'boxLabel', 'x', NULL, 7)
INSERT INTO [dbo].[s4_reports] ([reportID], [reportType], [template], [settings], [printerTypeID]) VALUES (15,'boxLabel', 'x', NULL, 6)
INSERT INTO [dbo].[s4_reports] ([reportID], [reportType], [template], [settings], [printerTypeID]) VALUES (17,'carrLabel', 'x', NULL, 7)
INSERT INTO [dbo].[s4_reports] ([reportID], [reportType], [template], [settings], [printerTypeID]) VALUES (18,'packingLabel', 'x', NULL, 6)
INSERT INTO [dbo].[s4_reports] ([reportID], [reportType], [template], [settings], [printerTypeID]) VALUES (19,'carrLabel', 'x', NULL, 5)
INSERT INTO [dbo].[s4_reports] ([reportID], [reportType], [template], [settings], [printerTypeID]) VALUES (20,'storeMove', 'x', NULL, 1)
INSERT INTO [dbo].[s4_reports] ([reportID], [reportType], [template], [settings], [printerTypeID]) VALUES (21,'onStore', 'x', NULL, 1)
INSERT INTO [dbo].[s4_reports] ([reportID], [reportType], [template], [settings], [printerTypeID]) VALUES (22, 'WebPrint', 'x', NULL, 1)
INSERT INTO [dbo].[s4_reports] ([reportID], [reportType], [template], [settings], [printerTypeID]) VALUES (23, 'WebPrint', 'x', NULL, 1)
INSERT INTO [dbo].[s4_reports] ([reportID], [reportType], [template], [settings], [printerTypeID]) VALUES (24, 'WebPrint', 'x', NULL, 1)
INSERT INTO [dbo].[s4_reports] ([reportID], [reportType], [template], [settings], [printerTypeID]) VALUES (25, 'WebPrint', 'x', NULL, 1)
INSERT INTO [dbo].[s4_reports] ([reportID], [reportType], [template], [settings], [printerTypeID]) VALUES (26, 'WebPrint', 'x', NULL, 1)

INSERT [dbo].[s4_reports] ([reportID], [reportType], [template], [settings], [printerTypeID]) VALUES (2, N'dispatchLabel', N'BF ON
FT "Univers Condensed Bold",46
PP 80,0
PT "{1} / {2}"

FT "Univers Condensed Bold",20
PP 80,150
PT "{0.DocNumPrefix}/{0.DocYear}/{0.DocNumber}/"
PF', NULL, 4)

INSERT [dbo].[s4_reports] ([reportID], [reportType], [template], [settings], [printerTypeID]) VALUES (7, N'positionLabel', N'c0000
KI503
O0220
f220
KW0453
KI7
V0
L
H13
PC
A2
D11
1A6305400200284{0.PosCode}
164400001100053{0.PosCode}
^01
Q0001
E
', NULL, 7)


SET IDENTITY_INSERT [dbo].[s4_reports] OFF
GO

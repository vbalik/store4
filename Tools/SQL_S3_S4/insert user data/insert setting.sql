
INSERT INTO [dbo].[s4_settings]
           ([setFieldID]
           ,[setDataType]
           ,[setValue])
     VALUES
           ('DispatchReportID','System.Int32','1')
GO

INSERT INTO [dbo].[s4_settings]
           ([setFieldID]
           ,[setDataType]
           ,[setValue])
     VALUES
           ('ReceiveReportID','System.Int32','8')
GO

INSERT INTO [dbo].[s4_settings]
           ([setFieldID]
           ,[setDataType]
           ,[setValue])
     VALUES
           ('DispatchPrintLabelReportID','System.Int32','2')
GO

INSERT INTO [dbo].[s4_settings]
           ([setFieldID]
           ,[setDataType]
           ,[setValue])
     VALUES
           ('PositionPrintLabelReportID','System.Int32','7')
GO



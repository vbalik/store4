USE [S4_ArgoMed]
GO


print '[s4_sectionList]'
INSERT INTO [dbo].[s4_sectionList]
([sectID],[sectDesc],[source_id])
SELECT
[sectID] = TRIM([sectID]),[sectDesc],[source_id]
  FROM [192.168.2.201].[Store3_ArgoMed].[dbo].[s3_sectionList] 

GO

print '[s4_manufactList]'
INSERT INTO [dbo].[s4_manufactList]
 ([manufID], [manufName], [manufStatus],
  [manufSettings], [source_id], [useBatch], [useExpiration], [sectID])
SELECT
   [manufID] = TRIM([manufID]),[manufName], manufStatus = CASE [manufStatus] WHEN 0 THEN 'MAN_OK' WHEN 255 THEN 'MAN_LOCKED' END,
   [manufSettings], [source_id], [def_useBatch], [def_useExpiration], [def_sectID] = TRIM([def_sectID])
  FROM [192.168.2.201].[Store3_ArgoMed].[dbo].[s3_manufactList]

GO

print '[s4_articleList]'
SET IDENTITY_INSERT [dbo].[s4_articleList] ON;
INSERT INTO [dbo].[s4_articleList] 
([articleID], [articleCode], [articleDesc], [articleStatus],
 [articleType], [source_id], [sectID], [unitDesc], [manufID], [useBatch], [mixBatch], [useExpiration])
SELECT 
  [articleID], [articleCode], [articleDesc], articleStatus = CASE [articleStatus] WHEN 0 THEN 'AR_OK' WHEN 255 THEN 'AR_DISABLED' END,
   [articleType], [source_id], [sectID] = TRIM([sectID]), [unitDesc], [manufID] = TRIM([manufID]), [useBatch], [mixBatch], [useExpiration]
  FROM [192.168.2.201].[Store3_ArgoMed].[dbo].[s3_articleList]
SET IDENTITY_INSERT [dbo].[s4_articleList] OFF;

GO

print '[s4_articleList_history]'
SET IDENTITY_INSERT [dbo].[s4_articleList_history] ON;
INSERT INTO [dbo].[s4_articleList_history]
([articleHistID],[articleID],[artPackID],[eventCode],
[eventData],[entryDateTime],[entryUserID])
SELECT
 [articleHistID],[articleID],[artPackID],eventCode = CASE eventCode WHEN 'ARUP' THEN 'AR_UPDATE' WHEN 'ARBS' THEN 'AR_BCODE_SET' WHEN 'ARBD' THEN 'AR_BCODE_DEL' WHEN 'ARDI' THEN 'AR_DISABLED' END,
 [bigData],[entryDateTime],[entryUserID]
  FROM [192.168.2.201].[Store3_ArgoMed].[dbo].[s3_articleList_history]
--WHERE [articleID] in (select [articleID] from [dbo].[s4_articleList]) 
SET IDENTITY_INSERT [dbo].[s4_articleList_history] OFF;

GO

print '[s4_articleList_packings]'
SET IDENTITY_INSERT [dbo].[s4_articleList_packings] ON;
INSERT INTO [dbo].[s4_articleList_packings]
([artPackID],[articleID],[packDesc],[packRelation],[movablePack],[source_id],[packStatus],[barCodeType],[barCode],[inclCarrier],[packWeight],[packVolume])
 SELECT
 [artPackID],[articleID],[packDesc],[packRelation],[movablePack],[source_id],packStatus = CASE [packStatus] WHEN 0 THEN 'AP_OK' WHEN 1 THEN 'AP_OK' WHEN 255 THEN 'AP_DISABLED' END,
 [barCodeType],[barCode],[inclCarrier],[packWeight],[packVolume]
 FROM [192.168.2.201].[Store3_ArgoMed].[dbo].[s3_articleList_packings]
 --WHERE [articleID] in (select [articleID] from [dbo].[s4_articleList]) 
SET IDENTITY_INSERT [dbo].[s4_articleList_packings] OFF;

GO

print '[s4_houseList]'
INSERT INTO [dbo].[s4_houseList]
([houseID],[houseDesc],[houseMap])
SELECT 
[houseID] = TRIM([houseID]),[houseDesc],[houseMap]
FROM [192.168.2.201].[Store3_ArgoMed].[dbo].[s3_houseList]

GO

print '[s4_partnerList]'
SET IDENTITY_INSERT [dbo].[s4_partnerList] ON;
INSERT INTO [dbo].[s4_partnerList]
([partnerID],[partnerName],[source_id],[partnerStatus],
[partnerDispCheck],[partnerAddr_1],[partnerAddr_2],[partnerCity],[partnerPostCode],[partnerPhone])
 SELECT
 [partnerID],[partnerName],[source_id],partnerStatus = CASE [partnerStatus] WHEN 0 THEN 'PA_OK' WHEN 255 THEN 'PA_DISABLED' END,
 [partnerDispCheck],[partnerAddr_1],[partnerAddr_2],[partnerCity],[partnerPostCode],[partnerPhone]
FROM [192.168.2.201].[Store3_ArgoMed].[dbo].[s3_partnerList]

INSERT INTO [dbo].[s4_partnerList] ([partnerID], [partnerName], [source_id], [partnerStatus], [partnerDispCheck], [partnerAddr_1], [partnerAddr_2], [partnerCity], [partnerPostCode], [partnerPhone])
     VALUES (0, 'Pr�zdn�', 'Pr�zdn�', 'PA_OK', 123, 'Pr�zdn�', 'Pr�zdn�', 'Pr�zdn�', 'Pr�zdn�', 'Pr�zdn�')

INSERT INTO [dbo].[s4_partnerList] ([partnerID], [partnerName], [source_id], [partnerStatus], [partnerDispCheck], [partnerAddr_1], [partnerAddr_2], [partnerCity], [partnerPostCode], [partnerPhone])
     VALUES (-1, '???', '???', 'PA_OK', 1, '???', '???', '???', '???', '???')

SET IDENTITY_INSERT [dbo].[s4_partnerList] OFF;

GO

print '[s4_partnerList_addresses]'
SET IDENTITY_INSERT [dbo].[s4_partnerList_addresses] ON;
INSERT INTO [dbo].[s4_partnerList_addresses]
([partAddrID],[partnerID],[addrName],[source_id],[addrLine_1],[addrLine_2],[addrCity],[addrPostCode],[addrPhone],[addrRemark],[deliveryInst])
SELECT 
 [partAddrID],[partnerID],[addrName],[source_id],[addrLine_1],[addrLine_2],[addrCity],[addrPostCode],[addrPhone],[addrRemark],[deliveryInst]
 FROM [192.168.2.201].[Store3_ArgoMed].[dbo].[s3_partnerList_addresses]
SET IDENTITY_INSERT [dbo].[s4_partnerList_addresses] OFF;

GO

print '[s4_positionList]'
SET IDENTITY_INSERT [dbo].[s4_positionList] ON;
INSERT INTO [dbo].[s4_positionList]
([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
SELECT 
 [positionID],[posCode],[posDesc],[houseID] = TRIM([houseID]),[posCateg],posStatus = CASE [posStatus] WHEN 0 THEN 'POS_OK' WHEN 210 THEN 'POS_STOCTAKING' ELSE 'POS_BLOCKED' END,
 [posHeight],[posX],[posY],[posZ],[mapPos]
FROM [192.168.2.201].[Store3_ArgoMed].[dbo].[s3_positionList]
SET IDENTITY_INSERT [dbo].[s4_positionList] OFF;

GO

print '[s4_truckList]'
SET IDENTITY_INSERT [dbo].[s4_truckList] ON;
INSERT INTO [dbo].[s4_truckList]
([truckID],[truckRegist],[truckCapacity],[truckComment])
SELECT
 [truckID],[truckRegist],[truckCapacity],[truckComment]
  FROM [192.168.2.201].[Store3_ArgoMed].[dbo].[s3_truckList]
SET IDENTITY_INSERT [dbo].[s4_truckList] OFF;

GO

print '[s4_workerList]'
INSERT INTO [dbo].[s4_workerList]
([workerID],[workerName],[workerClass],[workerStatus],[workerLogon],[workerSecData])
SELECT 
 [workerID] = TRIM([workerID]),[workerName],[workerClass],[workerStatus] = CASE [workerStatus] WHEN 0 THEN 'ONDUTY' WHEN 10 THEN 'RECEIVE' WHEN 11 THEN 'STOMOVE' WHEN 12 THEN 'DISPATCH' WHEN 13 THEN 'DISPCHECK' WHEN 201 THEN 'HOLIDAY' WHEN 255 THEN 'DISABLED' END,
 [workerLogon],CONVERT(VARCHAR(256), workerSecData, 2)
FROM [192.168.2.201].[Store3_ArgoMed].[dbo].[s3_workerList]

GO

print '[s4_workerList_history]'
SET IDENTITY_INSERT [dbo].[s4_workerList_history] ON;
INSERT INTO [dbo].[s4_workerList_history]
([workerHistID],[workerID],[eventCode],[eventData],[entryDateTime],[entryUserID])
SELECT
 [workerHistID],[workerID] = TRIM([workerID]),eventCode = CASE [eventCode] WHEN 0 THEN 'ONDUTY' WHEN 10 THEN 'RECEIVE' WHEN 11 THEN 'STOMOVE' WHEN 12 THEN 'DISPATCH' WHEN 13 THEN 'DISPCHECK' WHEN 201 THEN 'HOLIDAY' WHEN 255 THEN 'DISABLED' END,
 [eventData],[entryDateTime],[entryUserID]
FROM [192.168.2.201].[Store3_ArgoMed].[dbo].[s3_workerList_history]
SET IDENTITY_INSERT[dbo].[s4_workerList_history] OFF;

GO

print '[s4_direction]'
SET IDENTITY_INSERT [dbo].[s4_direction] ON;
INSERT INTO [dbo].[s4_direction]
([directionID],[docStatus], [docNumPrefix], [docDirection], [docYear], [docNumber], [docDate], [partnerID], [partAddrID], [prepare_directionID], [partnerRef], [docRemark], [entryDateTime], [entryUserID])
SELECT 
 [directionID], [docStatus], [docNumPrefix] = TRIM([docNumPrefix]), [docDirection], [docYear] = TRIM([docYear]), [docNumber], [docDate], [partnerID], [partAddrID], [prepare_directionID], [partnerRef], [docRemark], [entryDateTime], [entryUserID]
 FROM [192.168.2.201].[Store3_ArgoMed].[dbo].[s3_direction]
--WHERE [partnerID] in (select [partnerID] from [dbo].[s4_partnerList]) 
SET IDENTITY_INSERT [dbo].[s4_direction] OFF;

GO

print '[s4_direction_assign]'
SET IDENTITY_INSERT [dbo].[s4_direction_assign] ON;
INSERT INTO [dbo].[s4_direction_assign]
([directAssignID],[directionID],[jobID],[workerID],[assignParams],[entryDateTime],[entryUserID])
SELECT 
[directAssignID],[directionID],[jobID] = TRIM([jobID]),[workerID] = TRIM([workerID]),[assignParams],[entryDateTime],[entryUserID]
FROM [192.168.2.201].[Store3_ArgoMed].[dbo].[s3_direction_assign]
--WHERE [directionID] in (select [directionID] from [dbo].[s4_direction])
SET IDENTITY_INSERT [dbo].[s4_direction_assign] OFF;

GO

print '[s4_direction_history]'
SET IDENTITY_INSERT [dbo].[s4_direction_history] ON;
INSERT INTO [dbo].[s4_direction_history]
([directionHistID],[directionID],[docPosition],[eventCode],[eventData],[entryDateTime],[entryUserID])
SELECT 
 [directionHistID],[directionID],[docPosition],[eventCode],eventData = CASE WHEN smalldata IS NULL THEN bigdata ELSE smalldata END,
 [entryDateTime],[entryUserID]
  FROM [192.168.2.201].[Store3_ArgoMed].[dbo].[s3_direction_history]
-- WHERE [directionID] in (select [directionID] from [dbo].[s4_direction])
SET IDENTITY_INSERT [dbo].[s4_direction_history] OFF;

GO

print '[s4_direction_items]'
SET IDENTITY_INSERT [dbo].[s4_direction_items] ON;
INSERT INTO [dbo].[s4_direction_items]
([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[unitDesc],[quantity],[unitPrice],[partnerDepart],[entryDateTime],[entryUserID])
SELECT 
 [directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[unitDesc],[quantity],[unitPrice],[partnerDepart],[entryDateTime],[entryUserID]
FROM [192.168.2.201].[Store3_ArgoMed].[dbo].[s3_direction_items]
--WHERE [directionID] in (select [directionID] from [dbo].[s4_direction])
SET IDENTITY_INSERT [dbo].[s4_direction_items] OFF;

GO

print '[s4_direction_xtra]'
SET IDENTITY_INSERT [dbo].[s4_direction_xtra] ON;
INSERT INTO [dbo].[s4_direction_xtra]
([directionExID],[directionID],[xtraCode],[docPosition],[xtraValue])
SELECT 
 [directionExID],[directionID],[xtraCode],[docPosition],[xtraValue]
FROM [192.168.2.201].[Store3_ArgoMed].[dbo].[s3_direction_xtra]
--WHERE [directionID] in (select [directionID] from [dbo].[s4_direction])
SET IDENTITY_INSERT [dbo].[s4_direction_xtra] OFF;

GO

print '[s4_positionList_sections]'
SET IDENTITY_INSERT [dbo].[s4_positionList_sections] ON;
INSERT INTO [dbo].[s4_positionList_sections]
([posSectID],[sectID],[positionID])
SELECT
 [posSectID],[sectID] = TRIM([sectID]),[positionID]
  FROM [192.168.2.201].[Store3_ArgoMed].[dbo].[s3_positionList_sections]
SET IDENTITY_INSERT [dbo].[s4_positionList_sections] OFF;

GO

print '[s4_prefixList]'
INSERT INTO [dbo].[s4_prefixList]
([docClass],[docNumPrefix],[prefixDesc],[docDirection],[docType],[prefixEnabled],[prefixSection],[prefixSettings])
SELECT
[docClass],[docNumPrefix] = TRIM([docNumPrefix]),[prefixDesc],[docDirection],[docType],[prefixEnabled],[prefixSection] = TRIM([prefixSection]),null
 FROM [192.168.2.201].[Store3_ArgoMed].[dbo].[s3_prefixList]
GO


print '[s4_storeMove_lots]'
INSERT INTO [dbo].[s4_storeMove_lots]
([artPackID],[batchNum],[expirationDate])
SELECT [artPackID],[batchNum] = CASE WHEN [batchNum] = '?' THEN '_NONE_' ELSE [batchNum] END, [expirationDate] = CASE WHEN [expirationDate] IS NULL THEN '1900-01-01' ELSE [expirationDate] END
FROM [192.168.2.201].[Store3_ArgoMed].[dbo].[s3_storeMove_items]
WHERE
		[itemStatus] in ('SDIM','SDIS','SEXP','SMOV','SREC','SRPC','SSTI','STOK') 
		--AND [stoMoveID] in (select [stoMoveID] from [dbo].[s4_storeMove])
GROUP BY [artPackID],[batchNum],[expirationDate]

GO

print '[s4_storeMove]'
SET IDENTITY_INSERT [dbo].[s4_storeMove] ON;
INSERT INTO [dbo].[s4_storeMove]
([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
SELECT 
 [stoMoveID],[docStatus],[docNumPrefix] = TRIM([docNumPrefix]),[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID]
FROM [192.168.2.201].[Store3_ArgoMed].[dbo].[s3_storeMove]
SET IDENTITY_INSERT [dbo].[s4_storeMove] OFF;
  
GO

print '[s4_storeMove_history]'
SET IDENTITY_INSERT [dbo].[s4_storeMove_history] ON;
INSERT INTO [dbo].[s4_storeMove_history]
([stoMoveHistID],[stoMoveID],[docPosition],[eventCode],[eventData],[entryDateTime],[entryUserID])
SELECT 
 [stoMoveHistID],[stoMoveID],[docPosition],eventCode = CASE eventCode WHEN 'ITCH' THEN 'ITEM_CHANGED' ELSE [eventCode] END,
 null,[entryDateTime],[entryUserID]
  FROM [192.168.2.201].[Store3_ArgoMed].[dbo].[s3_storeMove_history]
--WHERE [stoMoveID] in (select [stoMoveID] from [dbo].[s4_storeMove])
SET IDENTITY_INSERT [dbo].[s4_storeMove_history] OFF;

GO

print '[s4_storeMove_xtra]'
SET IDENTITY_INSERT [dbo].[s4_storeMove_xtra] ON;

INSERT INTO [dbo].[s4_storeMove_xtra]
([stoMoveExID],[stoMoveID],[xtraCode],[xtraValue])
SELECT 
[stoMoveExID],[stoMoveID],[xtraCode],[xtraValue]
FROM [192.168.2.201].[Store3_ArgoMed].[dbo].[s3_storeMove_xtra]
SET IDENTITY_INSERT [dbo].[s4_storeMove_xtra] OFF;

GO


print '[s4_storeMove_items]'
SET IDENTITY_INSERT [dbo].[s4_storeMove_items] ON;
INSERT INTO [dbo].[s4_storeMove_items]
([stoMoveItemID],[stoMoveID],[docPosition],
 [itemValidity],
 [itemDirection],[stoMoveLotID],
 [positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID]) 
SELECT 
 i.[stoMoveItemID],i.[stoMoveID],i.[docPosition],
 itemValidity = 100,
 i.[itemDirection], stoMoveLotID = l.[stoMoveLotID],
 i.[positionID],i.[carrierNum],i.[quantity],i.[parent_docPosition],i.[brotherID],i.[entryDateTime],i.[entryUserID]
 FROM [192.168.2.201].[Store3_ArgoMed].[dbo].[s3_storeMove_items] i
INNER JOIN [dbo].[s4_storeMove_lots] l on l.[artPackID] = i.[artPackID] AND
l.[batchNum] COLLATE DATABASE_DEFAULT = CASE WHEN i.[batchNum] = '?' THEN '_NONE_' ELSE i.[batchNum] END COLLATE DATABASE_DEFAULT AND
l.[expirationDate] = CASE WHEN i.[expirationDate] IS NULL THEN '1900-01-01' ELSE i.[expirationDate] END
WHERE
	i.[itemStatus] in ('SDIM','SDIS','SEXP','SMOV','SREC','SRPC','SSTI','STOK') 
	--AND i.[stoMoveID] in (select [stoMoveID] from [dbo].[s4_storeMove])
SET IDENTITY_INSERT [dbo].[s4_storeMove_items] OFF;

GO
 


print '[s4_stocktaking]'

SET IDENTITY_INSERT [dbo].[s4_stocktaking] ON;

INSERT INTO [dbo].[s4_stocktaking]
      ([stotakID],[stotakDesc],[beginDate],[endDate],[entryDateTime],[entryUserID],[stotakStatus])
SELECT [stotakID],[stotakDesc],[beginDate],[endDate],[entryDateTime],TRIM([entryUserID]) AS entryUserID, CASE [stotakStatus] WHEN 0 THEN 'OPENED' WHEN 10 THEN 'SAVED' WHEN 255 THEN 'CANCELED' END AS stotakStatus
FROM [192.168.2.201].[Store3_ArgoMed].[dbo].[s3_stocktaking]

SET IDENTITY_INSERT [dbo].[s4_stocktaking] OFF;
GO


print '[s4_stocktaking_history]'
SET IDENTITY_INSERT [dbo].[s4_stocktaking_history] ON;

INSERT INTO [dbo].[s4_stocktaking_history]
([stotakHistID],[stotakID],[positionID],[artPackID],[carrierNum],[quantity],[batchNum],[expirationDate],[entryDateTime],[entryUserID],[eventCode])
 SELECT
 [stotakHistID],[stotakID],[positionID],[artPackID],[carrierNum],[quantity],[batchNum],[expirationDate],[entryDateTime],TRIM([entryUserID]) AS entryUserID,
 eventCode = CASE [eventCode] WHEN 1 THEN 'SET' WHEN 2 THEN 'CLEAR' WHEN 5 THEN 'OK' WHEN 10 THEN 'SAVED' WHEN 20 THEN 'CHANGED' WHEN 25 THEN 'ADDED' WHEN 30 THEN 'DELETED' WHEN 50 THEN 'COPIED' WHEN 255 THEN 'CANCELED' END
 FROM [192.168.2.201].[Store3_ArgoMed].[dbo].[s3_stocktaking_history]
--WHERE [stotakID] in (select [stotakID] from [dbo].[s4_stocktaking])
SET IDENTITY_INSERT [dbo].[s4_stocktaking_history] OFF;

GO


print '[s4_stocktaking_items]'
GO
DISABLE TRIGGER [dbo].[s4_stocktaking_items_insert_update] ON [dbo].[s4_stocktaking_items];  
GO  

SET IDENTITY_INSERT [dbo].[s4_stocktaking_items] ON;

INSERT INTO [dbo].[s4_stocktaking_items]
([stotakItemID],[stotakID],[positionID],[artPackID],[quantity],[carrierNum],[batchNum],[expirationDate],[entryDateTime],[entryUserID])
SELECT
 [stotakItemID],[stotakID],[positionID],[artPackID],[quantity],[carrierNum],[batchNum],[expirationDate],[entryDateTime],TRIM([entryUserID]) AS entryUserID
  FROM [192.168.2.201].[Store3_ArgoMed].[dbo].[s3_stocktaking_items]
--WHERE [stotakID] in (select [stotakID] from [dbo].[s4_stocktaking])
SET IDENTITY_INSERT [dbo].[s4_stocktaking_items] OFF;
GO

ENABLE TRIGGER [dbo].[s4_stocktaking_items_insert_update] ON [dbo].[s4_stocktaking_items];  
GO  


print '[s4_stocktaking_positions]'
SET IDENTITY_INSERT [dbo].[s4_stocktaking_positions] ON;

INSERT INTO [dbo].[s4_stocktaking_positions]
([stotakPosID],[stotakID],[positionID],[checkCounter])
SELECT
 [stotakPosID],[stotakID],[positionID],[checkCounter]
  FROM [192.168.2.201].[Store3_ArgoMed].[dbo].[s3_stocktaking_positions]
--WHERE [stotakID] in (select [stotakID] from [dbo].[s4_stocktaking])
SET IDENTITY_INSERT [dbo].[s4_stocktaking_positions] OFF;

GO

print '[s4_stocktaking_snapshots]'
SET IDENTITY_INSERT [dbo].[s4_stocktaking_snapshots] ON;

INSERT INTO [dbo].[s4_stocktaking_snapshots]
([stotakSnapID],[stotakID],[snapshotClass],[positionID],[artPackID],[carrierNum],[quantity],[batchNum],[expirationDate],[unitPrice])
SELECT
 [stotakSnapID],[stotakID],[snapshotClass],[positionID],[artPackID],[carrierNum],[quantity],[batchNum],[expirationDate],[unitPrice]
 FROM [192.168.2.201].[Store3_ArgoMed].[dbo].[s3_stocktaking_snapshots]
 --WHERE [stotakID] in (select [stotakID] from [dbo].[s4_stocktaking])
 
SET IDENTITY_INSERT [dbo].[s4_stocktaking_snapshots] OFF;

GO

----------------------------------------
DECLARE @xml XML, @docClass varchar(32), @docNumPrefix varchar(16);  
DECLARE @JSONString varchar(max);
DECLARE @tempXML varchar(max);
  
DECLARE prefix_cursor CURSOR FOR  
select CAST(prefixSettings as XML) AS xml, docClass, docNumPrefix
 from [192.168.2.201].[Store3_ArgoMed].[dbo].[s3_prefixList] where prefixSettings is not null 
  
OPEN prefix_cursor;  
    
FETCH NEXT FROM prefix_cursor  
INTO @xml, @docClass, @docNumPrefix;  
 
WHILE @@FETCH_STATUS = 0  
BEGIN  
  
    SET @tempXML =
	(
		SELECT	
			xmlData.A.value('@sectid', 'VARCHAR(100)') AS [sectID],
			xmlData.A.value('@order', 'int') AS [order]
		FROM	@xml.nodes('/prefixsettings/dispatchsections/dispatchsection') xmlData(A)
		FOR JSON PATH, ROOT('dispatchSections') 
	);

	SET @JSONString =
	(
		SELECT	
			xmlData.A.value('@prefixdispatchdistribution', 'VARCHAR(100)') AS [dispatchDistribution],
			'nic' AS [xxx]
		FROM	@xml.nodes('/prefixsettings') xmlData(A)
		FOR JSON AUTO,  WITHOUT_ARRAY_WRAPPER 
	);
		
	--print @tempXML;
	--print @JSONString;

	set @tempXML = Stuff(@tempXML, 1, 1, '');
	set @tempXML = Stuff(@tempXML, LEN(@tempXML), 1, '');

	set @JSONString = Replace(@JSONString, '"xxx":"nic"', @tempXML);
    
	--print @JSONString;
	print 'JSON VALID = ' + cast(ISJSON(@JSONString) AS CHAR(1)) + ' ----> ' + 'Update s4_prefixList: ' + @docClass + ' / ' +  @docNumPrefix;;
	update [dbo].[s4_prefixList] set [prefixSettings] = @JSONString where [docClass] = @docClass AND [docNumPrefix] = @docNumPrefix;
    
   FETCH NEXT FROM prefix_cursor  
   INTO @xml, @docClass, @docNumPrefix;  
END  
  
CLOSE prefix_cursor;  
DEALLOCATE prefix_cursor;  
GO  

--update icons [s4_manufactList]
update [s4_manufactList] 
SET manufSign = 'fas fa-heartbeat'
where manufID IN ('STRYKER','SMITHNTN','MEDTRONI','MEDTROCO');


--check quantity
print 'check quantity S3-S4'

--DROP TABLE _s3_check
--DROP TABLE _s4_check

SELECT positionID, carrierNum COLLATE Czech_CI_AS as carrierNum, quantity, artPackID, CASE WHEN [batchNum] = '?' THEN '_NONE_' ELSE [batchNum] END COLLATE Czech_CI_AS as batchNum, CASE WHEN [expirationDate] IS NULL THEN '1900-01-01' ELSE [expirationDate] END AS expirationDate
into _s3_check
from [192.168.2.201].[Store3_ArgoMed].[dbo].[s3_vw_onPositionSaved]
go

SELECT positionID, carrierNum COLLATE Czech_CI_AS as carrierNum, quantity, artPackID, batchNum COLLATE Czech_CI_AS as batchNum, expirationDate
into _s4_check
from [dbo].[s4_vw_onStore_Valid]
go

select s4.artPackID, FORMAT(SUM(quantity),'# ### ##0') as quantityS4, (select FORMAT(SUM(quantity),'# ### ##0') as quantity from _s3_check	where artPackID = s4.artPackID) as quantityS3, MAX(ar.articleDesc) as articleDesc
from _s4_check s4
left join [dbo].[s4_articleList_packings] pack on pack.artPackID = s4.artPackID
left join [dbo].[s4_articleList] ar on pack.articleID = ar.articleID
group by s4.artPackID
having SUM(quantity) <>
(
	select SUM(quantity) as quantity 
	from _s3_check
	where artPackID = s4.artPackID
)
order by artPackID
go


SELECT SRC, positionID, carrierNum, quantity, artPackID, batchNum, expirationDate
FROM (
	SELECT '_s3_check-ONLY' AS SRC, T1.*
	FROM (
		  SELECT * FROM _s3_check
		  EXCEPT
		  SELECT * FROM _s4_check
		  ) AS T1
	UNION ALL
	SELECT '_s4_check-ONLY' AS SRC, T2.*
	FROM (
		  SELECT * FROM _s4_check
		  EXCEPT
		  SELECT * FROM _s3_check
		  ) AS T2
) x
ORDER BY
	positionID, carrierNum, quantity
GO
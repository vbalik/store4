﻿using S4.DeliveryServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks.Sources;
using System.Threading.Tasks;

namespace DeliveryServicesTestAppC
{
    internal class CPostGetStats : UnitBase
    {
        private string _commands;
        private DateTime _from;
        private DateTime _to;

        public CPostGetStats(string commands)
        {
            _commands = commands;
        }

        protected override void Run()
        {
            try
            {
                if (string.IsNullOrWhiteSpace(_commands))
                {
                    WriteLine(string.Format("Invalid parameters \"{0}\"", _commands));
                    return;
                }
                
                
                var parts = _commands.Split(" ");
                if (parts.Count() != 2)
                {
                    WriteLine(string.Format("Invalid parameters \"{0}\"", _commands));
                    return;
                }

                _from = S4.Core.Utils.DateUtils.GetDateByMask(parts[0], "yyyyMMdd").Value;
                _to = S4.Core.Utils.DateUtils.GetDateByMask(parts[1], "yyyyMMdd").Value;

                var dsCPOST = new S4.DeliveryServices.DSCPOST();
                dsCPOST.LogEvent += DsCPOST_LogEvent;
                var task = dsCPOST.GetStats(_from, _to);
                task.Wait();
                var result = task.Result;

                var resultStr = new S4.DeliveryServices.Models.SerializerBase<S4.DeliveryServices.Models.CPost.b2bSyncResponse>().Serialize(result);
                WriteLine(resultStr);
            }
            catch (Exception ex)
            {
                WriteLine(ex.ToString(), ConsoleColor.Red);
            }
        }

        private void DsCPOST_LogEvent(S4.DeliveryServices.IDeliveryService sender, string message, S4.DeliveryServices.LogLevelEnum logLevel, Exception ex = null)
        {
            switch (logLevel)
            {
                case S4.DeliveryServices.LogLevelEnum.Error:
                case S4.DeliveryServices.LogLevelEnum.Fatal:
                    WriteLine(message, ConsoleColor.Red);
                    break;
                case S4.DeliveryServices.LogLevelEnum.Warn:
                    WriteLine(message, ConsoleColor.Yellow);
                    break;
                default:
                    WriteLine(message);
                    break;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeliveryServicesTestAppC
{
    internal class DPDGetPshops : UnitBase
    {
        protected override void Run()
        {
            try
            {
                var client = new S4.DeliveryServices.DPDClients.DPDApiClient();
                var task = client.GetParcelShops();
                task.Wait();
                var result = task.Result;
            }
            catch (Exception ex)
            {
                WriteLine(ex.ToString(), ConsoleColor.Red);
            }
        }
    }
}

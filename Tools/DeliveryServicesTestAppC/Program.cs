﻿using System;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;

namespace DeliveryServicesTestAppC
{
    class Program
    {
        public static IConfigurationRoot Settings = null;

        static void Main(string[] args)
        {
            //read setting
            ReadConfig();

            // set connection string for procedures
            S4.Core.Data.ConnectionHelper.ConnectionString = Program.Settings.GetValue<string>("S4_LOCALDB_CN");

            string cmd = string.Empty;
            while (cmd.ToUpper() != "EXIT")
            {
                Console.ResetColor();
                Console.WriteLine();
                Console.Write("DeliveryServicesTestAppC: ");
                cmd = Console.ReadLine();
                ProcessCmd(cmd);
            }
        }

        private static void ProcessCmd(string line)
        {
            var cmd = line.Split(' ')[0];
            var parameters = line.Remove(0, cmd.Length).TrimStart();

            switch (cmd.ToUpper())
            {
                case "EXIT":
                    break;
                case "?":
                case "HELP":
                    Console.WriteLine();
                    WriteHelp();
                    break;
                case "LABEL":
                    Console.WriteLine();
                    GetShipmentLabel(parameters);
                    break;
                case "SENDCPOSTXML":
                    Console.WriteLine();
                    SendCPostXML(parameters);
                    break;
                case "CPOSTGETSTATS":
                    Console.WriteLine();
                    CPostGetStats(parameters);
                    break;
                case "DPDGETPSHOPS":
                    Console.WriteLine();
                    DPDGetPshops();
                    break;

                default: Console.WriteLine("Unknown command"); break;
            }
        }

        private static void GetShipmentLabel(string param)
        {
            new GetShipmentLabel(param).RunUnit();
        }

        private static void SendCPostXML(string param)
        {
            new SendCPostXML(param).RunUnit();
        }

        private static void CPostGetStats(string param)
        {
            new CPostGetStats(param).RunUnit();
        }

        private static void DPDGetPshops()
        {
            new DPDGetPshops().RunUnit();
        }

        private static void ReadConfig()
        {
            //JSON config
            var builderJSON = new ConfigurationBuilder()
                .SetBasePath(Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName))
                .AddJsonFile("hosting.json", optional: false);

            Settings = builderJSON.Build();

        }

        private static void WriteHelp()
        {
            WriteHelpLine("CMD", "PARAMETERS", "DESC", "EXAMPLE");
            Console.WriteLine("--------------------------------------------------------------------------");
            WriteHelpLine("HELP or ?", "", "Displays all commands", "help");
            WriteHelpLine("LABEL", "DIRECTIONID=<id> PRINTER=<printer>", "Gets shipment label and sends it to specified printer", "label directionid=123 printer=\"Print to PDF\"");
            WriteHelpLine("SENDCPOSTXML", "METHOD=<method> XML=<path>", "Sends request to CPOST B2B API", "sendcpostxml method=getStats xml=C:\\test.xml");
            WriteHelpLine("CPOSTGETSTATS", "<yyyyMMdd> <yyyyMMdd>", "GetStats from CPOST B2B API", "cpostgetstats 20200101 20200801");
            WriteHelpLine("DPDGETPSHOPS", "", "Gets all parcel shops from DPD API", "dpdgetpshops");

            WriteHelpLine("EXIT", "", "Ends the application", "exit");
        }

        private static void WriteHelpLine(string cmd, string parameters, string desc, string example)
        {
            int lines = 1;

            var cmdParts = GetParts(cmd, 10);
            if (cmdParts.Length > lines) lines = cmdParts.Length;

            var cmdParameters = GetParts(parameters, 20);
            if (cmdParameters.Length > lines) lines = cmdParameters.Length;

            var cmdDesc = GetParts(desc, 20);
            if (cmdDesc.Length > lines) lines = cmdDesc.Length;

            var cmdExample = GetParts(example, 20);
            if (cmdExample.Length > lines) lines = cmdExample.Length;

            for (int i = 0; i < lines; i++)
            {
                cmd = cmdParts.Length > i ? cmdParts[i] : null;
                parameters = cmdParameters.Length > i ? cmdParameters[i] : null;
                desc = cmdDesc.Length > i ? cmdDesc[i] : null;
                example = cmdExample.Length > i ? cmdExample[i] : null;

                Console.WriteLine(string.Format("{0,-10}|{1,-20}|{2,-20}|{3}", cmd, parameters, desc, example));
            }
        }

        private static string[] GetParts(string text, int maxLength)
        {
            var parts = new List<string>();
            var part = GetPart(text, maxLength);
            while (part != string.Empty)
            {
                parts.Add(part);
                text = text.Remove(0, part.Length).TrimStart();
                part = GetPart(text, maxLength);
            }

            return parts.ToArray();
        }

        private static string GetPart(string text, int maxLength)
        {
            if (text.Length < maxLength) return text;

            var part = text.Substring(0, maxLength);
            var lastSpace = part.LastIndexOf(" ");
            if (lastSpace > 0) part = text.Substring(0, lastSpace);

            return part;
        }
    }
}

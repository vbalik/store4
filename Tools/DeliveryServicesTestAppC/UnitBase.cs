﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace DeliveryServicesTestAppC
{
    internal class UnitBase
    {
        public void WriteLine(string text, ConsoleColor? color = null)
        {
            if (color == null)
            {
                Console.ResetColor();
            }
            else
            {
                Console.ForegroundColor = color.Value;
            }
            Console.WriteLine(text);
        }

        public void RunUnit()
        {
            Run();
        }

        protected virtual void Run()
        {

        }
    }
}

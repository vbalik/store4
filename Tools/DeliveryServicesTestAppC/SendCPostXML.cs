﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Net;
using System.IO;
using System.Security.Cryptography.X509Certificates;

namespace DeliveryServicesTestAppC
{
    internal class SendCPostXML : UnitBase
    {
        private string _baseUrl = "https://b2b.postaonline.cz/services/POLService/v1/";
        private string _commands;
        private string _method;
        private string _requestXML;

        public SendCPostXML(string commands)
        {
            _commands = commands;
        }

        protected override void Run()
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(_commands))
                {
                    var parts = Regex.Split(_commands, " (?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                    foreach (var cmd in parts)
                    {
                        var pam_parts = cmd.Split('=');

                        switch (pam_parts[0].ToUpper())
                        {
                            case "METHOD":
                                _method = pam_parts[1];
                                break;
                            case "XML":
                                _requestXML = pam_parts[1].Replace("\"", "");
                                break;

                            default:
                                WriteLine(string.Format("Unknown parameter \"{0}\"", cmd)); return;
                        }
                    }
                }

                if (string.IsNullOrEmpty(_method))
                {
                    WriteLine("Method is missing!", ConsoleColor.Red);
                    return;
                }

                if (string.IsNullOrEmpty(_requestXML))
                {
                    WriteLine("XML path is missing!", ConsoleColor.Red);
                    return;
                }

                X509Certificate2Collection certificates = new X509Certificate2Collection();
                certificates.Import(@"c:\Firmy\Promedica\B2B_CP_POL_13012020\ArgomedCPOST.p12", "erisha1", X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.PersistKeySet);

                ServicePointManager.ServerCertificateValidationCallback = (a, b, c, d) => true;

                var xml = File.ReadAllText(_requestXML);

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(_baseUrl + _method);
                request.AllowAutoRedirect = true;
                request.ClientCertificates = certificates;
                var bytes = File.ReadAllBytes(_requestXML);
                request.ContentType = "text/xml; encoding='utf-8'";
                request.ContentLength = bytes.Length;
                request.Method = "POST";
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                requestStream.Close();
                HttpWebResponse response;
                response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    Stream responseStream = response.GetResponseStream();
                    string responseStr = new StreamReader(responseStream).ReadToEnd();
                    WriteLine(responseStr);
                }
            }
            catch(Exception ex)
            {
                WriteLine(ex.ToString(), ConsoleColor.Red);
            }
        }
    }
}

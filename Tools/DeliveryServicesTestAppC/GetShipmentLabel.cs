﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using PdfiumPrinter;

namespace DeliveryServicesTestAppC
{
    internal class GetShipmentLabel : UnitBase
    {
        private string _commands;
        private int _directionID;
        private string _printerName;

        public GetShipmentLabel(string commands)
        {
            _commands = commands;
        }

        protected override void Run()
        {
            if (!string.IsNullOrWhiteSpace(_commands))
            {
                var parts = Regex.Split(_commands, " (?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                foreach (var cmd in parts)
                {
                    var pam_parts = cmd.Split('=');

                    Func<Tuple<bool, int>> intTest = () =>
                    {
                        if (!int.TryParse(pam_parts[1], out int result))
                        {
                            WriteLine(string.Format("Invalid parameter value \"{0}\" in {1} argument", pam_parts[1], pam_parts[0]));
                            return new Tuple<bool, int>(false, -1);
                        }
                        return new Tuple<bool, int>(true, result);
                    };

                    switch (pam_parts[0].ToUpper())
                    {
                        case "DIRECTIONID":
                            var dirParam = intTest();
                            if (dirParam.Item1)
                                _directionID = dirParam.Item2;
                            break;
                        case "PRINTER":
                            _printerName = pam_parts[1].Replace("\"", "");
                            break;

                        default:
                            WriteLine(string.Format("Unknown parameter \"{0}\"", cmd)); return;
                    }
                }
            }

            if (_directionID == 0)
            {
                WriteLine("DirectionID is missing!", ConsoleColor.Red);
                return;
            }
            if (string.IsNullOrEmpty(_printerName))
            {
                WriteLine("Printer name is missing!", ConsoleColor.Red);
                return;
            }

            var deliveryService = new S4.DeliveryServices.DeliveryService();
            deliveryService.LogEvent += DeliveryService_LogEvent;
            deliveryService.GetLabelEvent += DeliveryService_GetLabelEvent;
            deliveryService.GetShipmentLabel(_directionID);
        }

        private void DeliveryService_GetLabelEvent(object sender, S4.DeliveryServices.GetLabelEventArgs e)
        {
            var pdfPrinter = new PdfPrinter(_printerName);
            pdfPrinter.Print(new System.IO.MemoryStream(e.Data));
        }

        private void DeliveryService_LogEvent(S4.DeliveryServices.IDeliveryService sender, string message, S4.DeliveryServices.LogLevelEnum logLevel, Exception ex = null)
        {
            Console.WriteLine(message);
        }
    }
}

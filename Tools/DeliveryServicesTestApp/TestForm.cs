﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Extensions.Configuration;
using PdfiumPrinter;

using S4.Entities;
using S4.Entities.Models;
using S4.ProcedureModels.Delivery;
using S4.Procedures.Delivery;

namespace DeliveryServicesTestApp
{
    public partial class TestForm : Form
    {
        public TestForm()
        {
            InitializeComponent();
        }

        private async void btnSendCS_Click(object sender, EventArgs e)
        {
            //DPD
            log.Items.Clear();

            var directionModel = new DirectionModel()
            {
                DirectionID = int.Parse(edDirectionID.Text),
                DocNumPrefix = "DL",
                DocNumber = 12345,
                DocYear = "21"
            };
            directionModel.XtraData.Data.Add(new DirectionXtra() { XtraCode = Direction.XTRA_DISPATCH_BOXES_CNT, XtraValue = edParcelsCount.Text });
            directionModel.XtraData.Data.Add(new DirectionXtra() { XtraCode = Direction.XTRA_INVOICE_NUMBER, XtraValue = "Invoice1" });
            directionModel.XtraData.Data.Add(new DirectionXtra() { XtraCode = Direction.XTRA_TRANSPORTATION_TYPE, XtraValue = "Doprava DPD" });
            directionModel.XtraData.Data.Add(new DirectionXtra() { XtraCode = Direction.XTRA_PAYMENT_TYPE, XtraValue = "Platba platební kartou" });

            var weights = edWeight.Text.Split(',');
            for (int i = 0; i < weights.Length; i++)
                directionModel.XtraData.Data.Add(new DirectionXtra() { XtraCode = Direction.XTRA_DISPATCH_WEIGHT, DocPosition = i + 1, XtraValue = weights[i] });
            var partner = new Partner()
            {
                PartnerName = edName.Text
            };
            var address = new PartnerAddress()
            {
                AddrLine_1 = edAddress1.Text,
                AddrLine_2 = edAddress2.Text,
                AddrCity = edCity.Text,
                AddrRemark = edEmail.Text,
                AddrPhone = edPhone.Text,
                AddrName = edName.Text
            };

            var dpd = new S4.DeliveryServices.DSDPD(Program.Settings.GetValue<string>("S4_LOCALDB_CN"));
            dpd.LogEvent += Dpd_LogEvent;
            dpd.GetLabelEvent += (s, args) =>
            {
                var fileName = $"Label{args.ReferenceId.Replace("/", "_")}.pdf";
                try
                {
                    System.IO.File.WriteAllBytes(fileName, args.Data);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString(), ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            };

            dpd.InsertShipmentEvent += Dpd_InsertShipmentEvent;

            await dpd.CreateShipment(directionModel, partner, address);
        }

        private bool Dpd_InsertShipmentEvent(S4.DeliveryServices.IDeliveryService sender, int directionID, string shipmentReference, Tuple<int, string>[] parcels, out string error)
        {
            error = null;
            try
            {
                var insertShipment = new InsertShipment()
                {
                    DirectionID = directionID,
                    ShipmentReference = shipmentReference,
                    DeliveryProvider = sender.DeliveryProvider,
                    Parcels = parcels
                };

                //var result = new InsertShipmentProc("AAAA1234").DoProcedure(insertShipment);
                //if (!result.Success)
                //{
                //    error = result.ErrorText;
                //    return false;
                //}
            }
            catch (Exception ex)
            {
                error = ex.ToString();
                return false;
            }

            return true;
        }

        private void Dpd_LogEvent(S4.DeliveryServices.IDeliveryService sender, string message, S4.DeliveryServices.LogLevelEnum logLevel, Exception ex = null)
        {
            SendToLog(message, logLevel);
            log.Items.Add(message);
        }

        private async void btnSendDS_Click(object sender, EventArgs e)
        {
            logDel.Items.Clear();

            var dpd = new S4.DeliveryServices.DSDPD(Program.Settings.GetValue<string>("S4_LOCALDB_CN"));
            dpd.LogEvent += Dpd_LogEvent1;

            await dpd.DeleteShipment(edDirectionIDDS.Text);
        }

        private void Dpd_LogEvent1(S4.DeliveryServices.IDeliveryService sender, string message, S4.DeliveryServices.LogLevelEnum logLevel, Exception ex = null)
        {
            SendToLog(message, logLevel, ex);
            logDel.Items.Add(message);
        }

        private async void btnSendL_Click(object sender, EventArgs e)
        {
            logL.Items.Clear();

            var dpd = new S4.DeliveryServices.DSDPD(Program.Settings.GetValue<string>("S4_LOCALDB_CN"));
            dpd.LogEvent += Dpd_LogEvent2;
            dpd.GetLabelEvent += (s, args) =>
            {
                var fileName = $"Label{args.ReferenceId}.pdf";
                try
                {
                    System.IO.File.WriteAllBytes(fileName, args.Data);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString(), ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            };

            dpd.GetLabelEvent += Dpd_GetLabelEvent;
            await dpd.GetLabel(edDirectionIDL.Text);
        }

        private void Dpd_GetLabelEvent(object sender, S4.DeliveryServices.GetLabelEventArgs e)
        {
            var printerName = "Intermec PM43 (203 dpi)";
            var pdfPrinter = new PdfPrinter(printerName);
            pdfPrinter.Print(new System.IO.MemoryStream(e.Data));
        }

        private void Dpd_LogEvent2(S4.DeliveryServices.IDeliveryService sender, string message, S4.DeliveryServices.LogLevelEnum logLevel, Exception ex = null)
        {
            SendToLog(message, logLevel, ex);
            logL.Items.Add(message);
        }

        private async void btnManifest_Click(object sender, EventArgs e)
        {
            log.Items.Clear();

            var dpd = new S4.DeliveryServices.DSDPD(Program.Settings.GetValue<string>("S4_LOCALDB_CN"));
            dpd.LogEvent += Dpd_LogEvent;
            dpd.GetManifestReportEvent += (s, args) =>
            {
                var fileName = $"Manifest{args.ReferenceId}.pdf";
                try
                {
                    System.IO.File.WriteAllBytes(fileName, args.Data);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString(), ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            };

            //await dpd.CreateManifest();
        }

        private async void btnReprintManifest_Click(object sender, EventArgs e)
        {
            logL.Items.Clear();

            var dpd = new S4.DeliveryServices.DSDPD(Program.Settings.GetValue<string>("S4_LOCALDB_CN"));
            dpd.LogEvent += Dpd_LogEvent2;
            dpd.GetManifestReportEvent += (s, args) =>
            {
                var fileName = $"Manifest{args.ReferenceId}.pdf";
                try
                {
                    System.IO.File.WriteAllBytes(fileName, args.Data);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString(), ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            };

            await dpd.PrintManifest(edManifest.Text);
        }

        private void SendToLog(string message, S4.DeliveryServices.LogLevelEnum logLevel, Exception ex = null)
        {
            switch (logLevel)
            {
                case S4.DeliveryServices.LogLevelEnum.Debug:
                    Program.NLogger.Debug(message);
                    break;
                case S4.DeliveryServices.LogLevelEnum.Error:
                    Program.NLogger.Error(message, ex);
                    break;
                case S4.DeliveryServices.LogLevelEnum.Fatal:
                    Program.NLogger.Fatal(message);
                    break;
                case S4.DeliveryServices.LogLevelEnum.Info:
                    Program.NLogger.Info(message);
                    break;
                case S4.DeliveryServices.LogLevelEnum.Trace:
                    Program.NLogger.Trace(message);
                    break;
                case S4.DeliveryServices.LogLevelEnum.Warn:
                    Program.NLogger.Warn(message);
                    break;
            }
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private async void button1_Click(object sender, EventArgs e)
        {
            logL.Items.Clear();

            var dpd = new S4.DeliveryServices.DSDPD(Program.Settings.GetValue<string>("S4_LOCALDB_CN"));
            dpd.LogEvent += Dpd_LogEvent2;
            dpd.GetShipmentStatusEvent += (s, p, args) =>
            {

                var content = args;
                var provider = p;
            };
            dpd.GetShipmentStatusEvent += Dpd_GetShipmentStatusEvent;

            await dpd.GetShipmentStatus(edDirectionIDL.Text);
        }

        private void Dpd_GetShipmentStatusEvent(object sender, DeliveryDirection.DeliveryProviderEnum provider, LabeledValue[] content)
        {
            foreach (var item in content)
            {
                logL.Items.Add($"{item.Key}: {item.Value}");
            }
        }



        private void logL_MouseDoubleClick(object sender, MouseEventArgs e)
        {

            string s = logL.SelectedItem.ToString();
            ClipboardSave(s);

        }

        private void log_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            string s = log.SelectedItem.ToString();
            ClipboardSave(s);
        }

        private void logDel_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            string s = logDel.SelectedItem.ToString();
            ClipboardSave(s);
        }

        private void ClipboardSave(string s)
        {
            Clipboard.SetData(DataFormats.StringFormat, s);
            MessageBox.Show("Clipboard save");
        }

        private async void button2_Click(object sender, EventArgs e)
        {
            //PPL
            log.Items.Clear();

            var directionModel = new DirectionModel()
            {
                DirectionID = int.Parse(edDirectionID.Text),
                DocNumPrefix = "DL",
                DocNumber = 1,
                DocYear = "20"
            };
            directionModel.XtraData.Data.Add(new DirectionXtra() { XtraCode = Direction.XTRA_DISPATCH_BOXES_CNT, XtraValue = edParcelsCount.Text });
            directionModel.XtraData.Data.Add(new DirectionXtra() { XtraCode = Direction.XTRA_INVOICE_NUMBER, XtraValue = "Invoice1" });
            directionModel.XtraData.Data.Add(new DirectionXtra() { XtraCode = Direction.XTRA_TRANSPORTATION_TYPE, XtraValue = "Doprava DPD" });
            directionModel.XtraData.Data.Add(new DirectionXtra() { XtraCode = Direction.XTRA_PAYMENT_TYPE, XtraValue = "Platba platební kartou" });

            var weights = edWeight.Text.Split(',');
            for (int i = 0; i < weights.Length; i++)
                directionModel.XtraData.Data.Add(new DirectionXtra() { XtraCode = Direction.XTRA_DISPATCH_WEIGHT, DocPosition = i + 1, XtraValue = weights[i] });
            var partner = new Partner()
            {
                PartnerName = edName.Text
            };
            var address = new PartnerAddress()
            {
                AddrLine_1 = edAddress1.Text,
                AddrLine_2 = edAddress2.Text,
                AddrCity = edCity.Text,
                AddrRemark = edEmail.Text,
                AddrPhone = edPhone.Text,
                AddrName = edName.Text
            };

            var ppl = new S4.DeliveryServices.DSPPL(Program.Settings.GetValue<string>("S4_LOCALDB_CN"));
            ppl.LogEvent += Ppl_LogEvent;
            ppl.GetLabelEvent += (s, args) =>
            {
                var fileName = $"Label{args.ReferenceId}.pdf";
                try
                {
                    System.IO.File.WriteAllBytes(fileName, args.Data);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString(), ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            };

            ppl.InsertShipmentEvent += Ppl_InsertShipmentEvent;

            await ppl.CreateShipment(directionModel, partner, address);
        }

        private bool Ppl_InsertShipmentEvent(S4.DeliveryServices.IDeliveryService sender, int directionID, string shipmentReference, Tuple<int, string>[] parcels, out string error)
        {
            error = null;
            try
            {
                var insertShipment = new InsertShipment()
                {
                    DirectionID = directionID,
                    ShipmentReference = shipmentReference,
                    DeliveryProvider = sender.DeliveryProvider,
                    Parcels = parcels
                };

                //var result = new InsertShipmentProc("AAAA1234").DoProcedure(insertShipment);
                //if (!result.Success)
                //{
                //    error = result.ErrorText;
                //    return false;
                //}
            }
            catch (Exception ex)
            {
                error = ex.ToString();
                return false;
            }

            return true;
        }

        private void Ppl_LogEvent(S4.DeliveryServices.IDeliveryService sender, string message, S4.DeliveryServices.LogLevelEnum logLevel, Exception exception = null)
        {
            SendToLog(message, logLevel);
            log.Items.Add(message);
        }

        private async void button3_Click(object sender, EventArgs e)
        {
            //CPOST
            log.Items.Clear();

            var directionModel = new DirectionModel()
            {
                DirectionID = int.Parse(edDirectionID.Text),
                DocNumPrefix = "DL",
                DocNumber = 1,
                DocYear = "20"
            };
            directionModel.XtraData.Data.Add(new DirectionXtra() { XtraCode = Direction.XTRA_DISPATCH_BOXES_CNT, XtraValue = edParcelsCount.Text });
            directionModel.XtraData.Data.Add(new DirectionXtra() { XtraCode = Direction.XTRA_INVOICE_NUMBER, XtraValue = "Invoice1" });
            directionModel.XtraData.Data.Add(new DirectionXtra() { XtraCode = Direction.XTRA_TRANSPORTATION_TYPE, XtraValue = "Doprava DPD" });
            directionModel.XtraData.Data.Add(new DirectionXtra() { XtraCode = Direction.XTRA_PAYMENT_TYPE, XtraValue = "Platba platební kartou" });
            directionModel.XtraData.Data.Add(new DirectionXtra() { XtraCode = Direction.XTRA_DELIVERY_SHIPMENTINFO, XtraValue = "Tohle je testovací pokus 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789" });

            var weights = edWeight.Text.Split(',');
            for (int i = 0; i < weights.Length; i++)
                directionModel.XtraData.Data.Add(new DirectionXtra() { XtraCode = Direction.XTRA_DISPATCH_WEIGHT, DocPosition = i + 1, XtraValue = weights[i] });
            var partner = new Partner()
            {
                PartnerName = edName.Text
            };
            var address = new PartnerAddress()
            {
                AddrLine_1 = edAddress1.Text,
                AddrLine_2 = edAddress2.Text,
                AddrCity = edCity.Text,
                AddrRemark = edEmail.Text,
                AddrPhone = edPhone.Text,
                AddrName = edName.Text
            };

            var cpost = new S4.DeliveryServices.DSCPOST(Program.Settings.GetValue<string>("S4_LOCALDB_CN"));
            cpost.LogEvent += Cpost_LogEvent;
            cpost.GetLabelEvent += (s, args) =>
            {
                var fileName = $"Label{args.ReferenceId}.pdf";
                try
                {
                    System.IO.File.WriteAllBytes(fileName, args.Data);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString(), ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            };

            cpost.InsertShipmentEvent += Cpost_InsertShipmentEvent;

            await cpost.CreateShipment(directionModel, partner, address);
        }

        private bool Cpost_InsertShipmentEvent(S4.DeliveryServices.IDeliveryService sender, int directionID, string shipmentReference, Tuple<int, string>[] parcels, out string error)
        {
            error = null;
            try
            {
                var insertShipment = new InsertShipment()
                {
                    DirectionID = directionID,
                    ShipmentReference = shipmentReference,
                    DeliveryProvider = sender.DeliveryProvider,
                    Parcels = parcels
                };

                //var result = new InsertShipmentProc("AAAA1234").DoProcedure(insertShipment);
                //if (!result.Success)
                //{
                //    error = result.ErrorText;
                //    return false;
                //}
            }
            catch (Exception ex)
            {
                error = ex.ToString();
                return false;
            }

            return true;
        }

        private void Cpost_LogEvent(S4.DeliveryServices.IDeliveryService sender, string message, S4.DeliveryServices.LogLevelEnum logLevel, Exception exception = null)
        {
            SendToLog(message, logLevel);
            log.Items.Add(message);
        }

        private async void button4_Click(object sender, EventArgs e)
        {
            logL.Items.Clear();

            var cpost = new S4.DeliveryServices.DSCPOST(Program.Settings.GetValue<string>("S4_LOCALDB_CN"));
            cpost.LogEvent += Cpost_LogEvent1;
            cpost.GetShipmentStatusEvent += (s, p, args) =>
            {

                var content = args;
                var provider = p;
            };
            cpost.GetShipmentStatusEvent += Dpd_GetShipmentStatusEvent;

            await cpost.GetShipmentStatus(edDirectionIDL.Text);
        }

        private void Cpost_LogEvent1(S4.DeliveryServices.IDeliveryService sender, string message, S4.DeliveryServices.LogLevelEnum logLevel, Exception exception = null)
        {
            SendToLog(message, logLevel, null);
            logL.Items.Add(message);
        }


        private async void btnDPD2_Click(object sender, EventArgs e)
        {
            //DPD - new API
            log.Items.Clear();

            var directionModel = new DirectionModel()
            {
                DirectionID = int.Parse(edDirectionID.Text),
                DocNumPrefix = "DL",
                DocNumber = 12345,
                DocYear = "21"
            };
            directionModel.XtraData.Data.Add(new DirectionXtra() { XtraCode = Direction.XTRA_DISPATCH_BOXES_CNT, XtraValue = edParcelsCount.Text });
            directionModel.XtraData.Data.Add(new DirectionXtra() { XtraCode = Direction.XTRA_INVOICE_NUMBER, XtraValue = "Invoice1" });
            
            // normal
            directionModel.XtraData.Data.Add(new DirectionXtra() { XtraCode = Direction.XTRA_TRANSPORTATION_TYPE, XtraValue = "Doprava DPD" });
            directionModel.XtraData.Data.Add(new DirectionXtra() { XtraCode = Direction.XTRA_PAYMENT_TYPE, XtraValue = "Platba platební kartou" });

            // COD
            //directionModel.XtraData.Data.Add(new DirectionXtra() { XtraCode = Direction.XTRA_TRANSPORTATION_TYPE, XtraValue = "Doprava DPD" });
            //directionModel.XtraData.Data.Add(new DirectionXtra() { XtraCode = Direction.XTRA_PAYMENT_TYPE, XtraValue = "Platba dobírkou" });
            //directionModel.XtraData.Data.Add(new DirectionXtra() { XtraCode = Direction.XTRA_CASH_ON_DELIVERY, XtraValue = "123" });

            // pickup
            //directionModel.XtraData.Data.Add(new DirectionXtra() { XtraCode = Direction.XTRA_TRANSPORTATION_TYPE, XtraValue = "Doprava PickUp" });
            //directionModel.XtraData.Data.Add(new DirectionXtra() { XtraCode = Direction.XTRA_PAYMENT_TYPE, XtraValue = "Platba platební kartou" });
            //directionModel.XtraData.Data.Add(new DirectionXtra() { XtraCode = Direction.XTRA_DELIVERY_PARCEL_SHOP, XtraValue = "CZ10084" });


            var weights = edWeight.Text.Split(',');
            for (int i = 0; i < weights.Length; i++)
                directionModel.XtraData.Data.Add(new DirectionXtra() { XtraCode = Direction.XTRA_DISPATCH_WEIGHT, DocPosition = i + 1, XtraValue = weights[i] });
            var partner = new Partner()
            {
                PartnerName = edName.Text
            };
            var address = new PartnerAddress()
            {
                AddrLine_1 = edAddress1.Text,
                AddrLine_2 = edAddress2.Text,
                AddrCity = edCity.Text,
                AddrRemark = edEmail.Text,
                AddrPhone = edPhone.Text,
                AddrName = edName.Text
            };

            var dpdNew = new S4.DeliveryServices.DSDPDNew(Program.Settings.GetValue<string>("S4_LOCALDB_CN"));
            dpdNew.LogEvent += Dpd_LogEvent;
            dpdNew.GetLabelEvent += (s, args) =>
            {
                var fileName = $"Label{args.ReferenceId.Replace("/", "_")}.pdf";
                try
                {
                    System.IO.File.WriteAllBytes(fileName, args.Data);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString(), ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            };

            dpdNew.InsertShipmentEvent += Dpd_InsertShipmentEvent;

            await dpdNew.CreateShipment(directionModel, partner, address);
        }


        private async void btnGetLabelDPD2_Click(object sender, EventArgs e)
        {
            logL.Items.Clear();

            var dpd = new S4.DeliveryServices.DSDPDNew(Program.Settings.GetValue<string>("S4_LOCALDB_CN"));
            dpd.LogEvent += Dpd_LogEvent2;
            dpd.GetLabelEvent += (s, args) =>
            {
                var fileName = $"Label{args.ReferenceId}.pdf";
                try
                {
                    System.IO.File.WriteAllBytes(fileName, args.Data);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString(), ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            };

            dpd.GetLabelEvent += Dpd_GetLabelEvent;
            await dpd.GetLabel(edDirectionIDL.Text);
        }

        private async void btnDeleteDPD2_Click(object sender, EventArgs e)
        {
            logDel.Items.Clear();

            var dpd = new S4.DeliveryServices.DSDPDNew(Program.Settings.GetValue<string>("S4_LOCALDB_CN"));
            dpd.LogEvent += Dpd_LogEvent1;

            await dpd.DeleteShipment(edDirectionIDDS.Text);
        }
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using Microsoft.Extensions.Configuration;
using NLog;
using NLog.Extensions.Logging;
using NLog.Targets;
using Microsoft.Extensions.DependencyInjection;

using S4.Messages;

namespace DeliveryServicesTestApp
{
    static class Program
    {
        public static Logger NLogger = LogManager.GetLogger("DeliveryServicesTestApp");
        public static IConfigurationRoot Settings = null;

        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //read setting
            ReadConfig();
            //Nlog Configuration
            NLog.LogManager.Configuration = new NLogLoggingConfiguration(Settings.GetSection("NLog"));

            // setup ServicesFactory.ServiceProvider
            var services = new ServiceCollection();
            services.AddSingleton<IMessageRouter>(new MessageRouter());

            var serviceProvider = services.BuildServiceProvider();
            S4.Procedures.ServicesFactory.ServiceProvider = serviceProvider;

            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new TestForm());
        }

        private static void ReadConfig()
        {
            //JSON config
            var builderJSON = new ConfigurationBuilder()
                .SetBasePath(Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName))
                .AddJsonFile("Settings.json");

            Settings = builderJSON.Build();
        }
    }
}

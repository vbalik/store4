﻿namespace DeliveryServicesTestApp
{
    partial class TestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            tabPage3 = new System.Windows.Forms.TabPage();
            btnDeleteDPD2 = new System.Windows.Forms.Button();
            btnSendDS = new System.Windows.Forms.Button();
            logDel = new System.Windows.Forms.ListBox();
            lbDirectionIDDS = new System.Windows.Forms.Label();
            edDirectionIDDS = new System.Windows.Forms.TextBox();
            tabPage2 = new System.Windows.Forms.TabPage();
            btnGetLabelDPD2 = new System.Windows.Forms.Button();
            button4 = new System.Windows.Forms.Button();
            button1 = new System.Windows.Forms.Button();
            btnReprintManifest = new System.Windows.Forms.Button();
            edManifest = new System.Windows.Forms.TextBox();
            lbManifest = new System.Windows.Forms.Label();
            btnSendL = new System.Windows.Forms.Button();
            logL = new System.Windows.Forms.ListBox();
            lbDirectionIDL = new System.Windows.Forms.Label();
            edDirectionIDL = new System.Windows.Forms.TextBox();
            tabPage1 = new System.Windows.Forms.TabPage();
            btnDPD2 = new System.Windows.Forms.Button();
            button3 = new System.Windows.Forms.Button();
            button2 = new System.Windows.Forms.Button();
            btnManifest = new System.Windows.Forms.Button();
            lbWeight = new System.Windows.Forms.Label();
            edWeight = new System.Windows.Forms.TextBox();
            lbPhone = new System.Windows.Forms.Label();
            edPhone = new System.Windows.Forms.TextBox();
            lbEmail = new System.Windows.Forms.Label();
            edEmail = new System.Windows.Forms.TextBox();
            btnSendCS = new System.Windows.Forms.Button();
            log = new System.Windows.Forms.ListBox();
            lbParcelsCount = new System.Windows.Forms.Label();
            edParcelsCount = new System.Windows.Forms.TextBox();
            edDirectionID = new System.Windows.Forms.TextBox();
            lbDirectionID = new System.Windows.Forms.Label();
            edCity = new System.Windows.Forms.TextBox();
            lbCity = new System.Windows.Forms.Label();
            edAddress2 = new System.Windows.Forms.TextBox();
            lbAddress2 = new System.Windows.Forms.Label();
            edAddress1 = new System.Windows.Forms.TextBox();
            lbAddress1 = new System.Windows.Forms.Label();
            edName = new System.Windows.Forms.TextBox();
            lbName = new System.Windows.Forms.Label();
            tabControl1 = new System.Windows.Forms.TabControl();
            tabPage3.SuspendLayout();
            tabPage2.SuspendLayout();
            tabPage1.SuspendLayout();
            tabControl1.SuspendLayout();
            SuspendLayout();
            // 
            // tabPage3
            // 
            tabPage3.Controls.Add(btnDeleteDPD2);
            tabPage3.Controls.Add(btnSendDS);
            tabPage3.Controls.Add(logDel);
            tabPage3.Controls.Add(lbDirectionIDDS);
            tabPage3.Controls.Add(edDirectionIDDS);
            tabPage3.Location = new System.Drawing.Point(4, 24);
            tabPage3.Name = "tabPage3";
            tabPage3.Padding = new System.Windows.Forms.Padding(3);
            tabPage3.Size = new System.Drawing.Size(946, 521);
            tabPage3.TabIndex = 2;
            tabPage3.Text = "Delete Shipment";
            tabPage3.UseVisualStyleBackColor = true;
            // 
            // btnDeleteDPD2
            // 
            btnDeleteDPD2.Location = new System.Drawing.Point(540, 38);
            btnDeleteDPD2.Name = "btnDeleteDPD2";
            btnDeleteDPD2.Size = new System.Drawing.Size(100, 23);
            btnDeleteDPD2.TabIndex = 4;
            btnDeleteDPD2.Text = "SEND DPD2";
            btnDeleteDPD2.UseVisualStyleBackColor = true;
            btnDeleteDPD2.Click += btnDeleteDPD2_Click;
            // 
            // btnSendDS
            // 
            btnSendDS.Location = new System.Drawing.Point(540, 9);
            btnSendDS.Name = "btnSendDS";
            btnSendDS.Size = new System.Drawing.Size(100, 23);
            btnSendDS.TabIndex = 3;
            btnSendDS.Text = "SEND DPD";
            btnSendDS.UseVisualStyleBackColor = true;
            btnSendDS.Click += btnSendDS_Click;
            // 
            // logDel
            // 
            logDel.FormattingEnabled = true;
            logDel.ItemHeight = 15;
            logDel.Location = new System.Drawing.Point(10, 83);
            logDel.Name = "logDel";
            logDel.Size = new System.Drawing.Size(928, 424);
            logDel.TabIndex = 2;
            logDel.MouseDoubleClick += logDel_MouseDoubleClick;
            // 
            // lbDirectionIDDS
            // 
            lbDirectionIDDS.AutoSize = true;
            lbDirectionIDDS.Location = new System.Drawing.Point(10, 10);
            lbDirectionIDDS.Name = "lbDirectionIDDS";
            lbDirectionIDDS.Size = new System.Drawing.Size(66, 15);
            lbDirectionIDDS.TabIndex = 1;
            lbDirectionIDDS.Text = "DirectionID";
            // 
            // edDirectionIDDS
            // 
            edDirectionIDDS.Location = new System.Drawing.Point(100, 10);
            edDirectionIDDS.Name = "edDirectionIDDS";
            edDirectionIDDS.Size = new System.Drawing.Size(213, 23);
            edDirectionIDDS.TabIndex = 0;
            // 
            // tabPage2
            // 
            tabPage2.Controls.Add(btnGetLabelDPD2);
            tabPage2.Controls.Add(button4);
            tabPage2.Controls.Add(button1);
            tabPage2.Controls.Add(btnReprintManifest);
            tabPage2.Controls.Add(edManifest);
            tabPage2.Controls.Add(lbManifest);
            tabPage2.Controls.Add(btnSendL);
            tabPage2.Controls.Add(logL);
            tabPage2.Controls.Add(lbDirectionIDL);
            tabPage2.Controls.Add(edDirectionIDL);
            tabPage2.Location = new System.Drawing.Point(4, 24);
            tabPage2.Name = "tabPage2";
            tabPage2.Padding = new System.Windows.Forms.Padding(3);
            tabPage2.Size = new System.Drawing.Size(946, 521);
            tabPage2.TabIndex = 1;
            tabPage2.Text = "Get Label";
            tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnGetLabelDPD2
            // 
            btnGetLabelDPD2.Location = new System.Drawing.Point(300, 90);
            btnGetLabelDPD2.Name = "btnGetLabelDPD2";
            btnGetLabelDPD2.Size = new System.Drawing.Size(109, 23);
            btnGetLabelDPD2.TabIndex = 9;
            btnGetLabelDPD2.Text = "Get Label DPD2";
            btnGetLabelDPD2.UseVisualStyleBackColor = true;
            btnGetLabelDPD2.Click += btnGetLabelDPD2_Click;
            // 
            // button4
            // 
            button4.Location = new System.Drawing.Point(415, 50);
            button4.Name = "button4";
            button4.Size = new System.Drawing.Size(124, 23);
            button4.TabIndex = 8;
            button4.Text = "Get Status CPOST";
            button4.UseVisualStyleBackColor = true;
            button4.Click += button4_Click;
            // 
            // button1
            // 
            button1.Location = new System.Drawing.Point(415, 10);
            button1.Name = "button1";
            button1.Size = new System.Drawing.Size(124, 23);
            button1.TabIndex = 7;
            button1.Text = "Get Status DPD";
            button1.UseVisualStyleBackColor = true;
            button1.Click += button1_Click;
            // 
            // btnReprintManifest
            // 
            btnReprintManifest.Location = new System.Drawing.Point(858, 10);
            btnReprintManifest.Name = "btnReprintManifest";
            btnReprintManifest.Size = new System.Drawing.Size(75, 23);
            btnReprintManifest.TabIndex = 6;
            btnReprintManifest.Text = "PRINT";
            btnReprintManifest.UseVisualStyleBackColor = true;
            btnReprintManifest.Click += btnReprintManifest_Click;
            // 
            // edManifest
            // 
            edManifest.Location = new System.Drawing.Point(630, 10);
            edManifest.Name = "edManifest";
            edManifest.Size = new System.Drawing.Size(211, 23);
            edManifest.TabIndex = 5;
            // 
            // lbManifest
            // 
            lbManifest.AutoSize = true;
            lbManifest.Location = new System.Drawing.Point(570, 10);
            lbManifest.Name = "lbManifest";
            lbManifest.Size = new System.Drawing.Size(53, 15);
            lbManifest.TabIndex = 4;
            lbManifest.Text = "Manifest";
            // 
            // btnSendL
            // 
            btnSendL.Location = new System.Drawing.Point(300, 10);
            btnSendL.Name = "btnSendL";
            btnSendL.Size = new System.Drawing.Size(109, 23);
            btnSendL.TabIndex = 3;
            btnSendL.Text = "Get Label DPD";
            btnSendL.UseVisualStyleBackColor = true;
            btnSendL.Click += btnSendL_Click;
            // 
            // logL
            // 
            logL.FormattingEnabled = true;
            logL.ItemHeight = 15;
            logL.Location = new System.Drawing.Point(9, 156);
            logL.Name = "logL";
            logL.Size = new System.Drawing.Size(929, 349);
            logL.TabIndex = 2;
            logL.MouseDoubleClick += logL_MouseDoubleClick;
            // 
            // lbDirectionIDL
            // 
            lbDirectionIDL.AutoSize = true;
            lbDirectionIDL.Location = new System.Drawing.Point(9, 10);
            lbDirectionIDL.Name = "lbDirectionIDL";
            lbDirectionIDL.Size = new System.Drawing.Size(66, 15);
            lbDirectionIDL.TabIndex = 1;
            lbDirectionIDL.Text = "DirectionID";
            // 
            // edDirectionIDL
            // 
            edDirectionIDL.Location = new System.Drawing.Point(100, 10);
            edDirectionIDL.Name = "edDirectionIDL";
            edDirectionIDL.Size = new System.Drawing.Size(192, 23);
            edDirectionIDL.TabIndex = 0;
            // 
            // tabPage1
            // 
            tabPage1.Controls.Add(btnDPD2);
            tabPage1.Controls.Add(button3);
            tabPage1.Controls.Add(button2);
            tabPage1.Controls.Add(btnManifest);
            tabPage1.Controls.Add(lbWeight);
            tabPage1.Controls.Add(edWeight);
            tabPage1.Controls.Add(lbPhone);
            tabPage1.Controls.Add(edPhone);
            tabPage1.Controls.Add(lbEmail);
            tabPage1.Controls.Add(edEmail);
            tabPage1.Controls.Add(btnSendCS);
            tabPage1.Controls.Add(log);
            tabPage1.Controls.Add(lbParcelsCount);
            tabPage1.Controls.Add(edParcelsCount);
            tabPage1.Controls.Add(edDirectionID);
            tabPage1.Controls.Add(lbDirectionID);
            tabPage1.Controls.Add(edCity);
            tabPage1.Controls.Add(lbCity);
            tabPage1.Controls.Add(edAddress2);
            tabPage1.Controls.Add(lbAddress2);
            tabPage1.Controls.Add(edAddress1);
            tabPage1.Controls.Add(lbAddress1);
            tabPage1.Controls.Add(edName);
            tabPage1.Controls.Add(lbName);
            tabPage1.Location = new System.Drawing.Point(4, 24);
            tabPage1.Name = "tabPage1";
            tabPage1.Padding = new System.Windows.Forms.Padding(3);
            tabPage1.Size = new System.Drawing.Size(946, 521);
            tabPage1.TabIndex = 0;
            tabPage1.Text = "Create Shipment";
            tabPage1.UseVisualStyleBackColor = true;
            // 
            // btnDPD2
            // 
            btnDPD2.Location = new System.Drawing.Point(540, 39);
            btnDPD2.Name = "btnDPD2";
            btnDPD2.Size = new System.Drawing.Size(156, 23);
            btnDPD2.TabIndex = 21;
            btnDPD2.Text = "SEND DPD2";
            btnDPD2.UseVisualStyleBackColor = true;
            btnDPD2.Click += btnDPD2_Click;
            // 
            // button3
            // 
            button3.Location = new System.Drawing.Point(702, 9);
            button3.Name = "button3";
            button3.Size = new System.Drawing.Size(75, 23);
            button3.TabIndex = 20;
            button3.Text = "SEND CP";
            button3.UseVisualStyleBackColor = true;
            button3.Click += button3_Click;
            // 
            // button2
            // 
            button2.Location = new System.Drawing.Point(621, 9);
            button2.Name = "button2";
            button2.Size = new System.Drawing.Size(75, 23);
            button2.TabIndex = 19;
            button2.Text = "SEND PPL";
            button2.UseVisualStyleBackColor = true;
            button2.Click += button2_Click;
            // 
            // btnManifest
            // 
            btnManifest.Location = new System.Drawing.Point(854, 10);
            btnManifest.Name = "btnManifest";
            btnManifest.Size = new System.Drawing.Size(75, 23);
            btnManifest.TabIndex = 18;
            btnManifest.Text = "MANIFEST";
            btnManifest.UseVisualStyleBackColor = true;
            btnManifest.Click += btnManifest_Click;
            // 
            // lbWeight
            // 
            lbWeight.AutoSize = true;
            lbWeight.Location = new System.Drawing.Point(10, 250);
            lbWeight.Name = "lbWeight";
            lbWeight.Size = new System.Drawing.Size(64, 15);
            lbWeight.TabIndex = 5;
            lbWeight.Text = "Hmotnosti";
            // 
            // edWeight
            // 
            edWeight.Location = new System.Drawing.Point(100, 250);
            edWeight.Name = "edWeight";
            edWeight.Size = new System.Drawing.Size(515, 23);
            edWeight.TabIndex = 4;
            edWeight.Text = "1";
            // 
            // lbPhone
            // 
            lbPhone.AutoSize = true;
            lbPhone.Location = new System.Drawing.Point(10, 160);
            lbPhone.Name = "lbPhone";
            lbPhone.Size = new System.Drawing.Size(45, 15);
            lbPhone.TabIndex = 17;
            lbPhone.Text = "Telefon";
            // 
            // edPhone
            // 
            edPhone.Location = new System.Drawing.Point(100, 160);
            edPhone.Name = "edPhone";
            edPhone.Size = new System.Drawing.Size(234, 23);
            edPhone.TabIndex = 16;
            edPhone.Text = "+420 603 464 958";
            // 
            // lbEmail
            // 
            lbEmail.AutoSize = true;
            lbEmail.Location = new System.Drawing.Point(10, 130);
            lbEmail.Name = "lbEmail";
            lbEmail.Size = new System.Drawing.Size(36, 15);
            lbEmail.TabIndex = 15;
            lbEmail.Text = "Email";
            // 
            // edEmail
            // 
            edEmail.Location = new System.Drawing.Point(100, 130);
            edEmail.Name = "edEmail";
            edEmail.Size = new System.Drawing.Size(234, 23);
            edEmail.TabIndex = 14;
            edEmail.Text = "richardho@volnz.cz";
            // 
            // btnSendCS
            // 
            btnSendCS.Location = new System.Drawing.Point(540, 9);
            btnSendCS.Name = "btnSendCS";
            btnSendCS.Size = new System.Drawing.Size(75, 23);
            btnSendCS.TabIndex = 13;
            btnSendCS.Text = "SEND DPD";
            btnSendCS.UseVisualStyleBackColor = true;
            btnSendCS.Click += btnSendCS_Click;
            // 
            // log
            // 
            log.FormattingEnabled = true;
            log.ItemHeight = 15;
            log.Location = new System.Drawing.Point(8, 282);
            log.Name = "log";
            log.Size = new System.Drawing.Size(930, 229);
            log.TabIndex = 12;
            log.MouseDoubleClick += log_MouseDoubleClick;
            // 
            // lbParcelsCount
            // 
            lbParcelsCount.AutoSize = true;
            lbParcelsCount.Location = new System.Drawing.Point(10, 220);
            lbParcelsCount.Name = "lbParcelsCount";
            lbParcelsCount.Size = new System.Drawing.Size(72, 15);
            lbParcelsCount.TabIndex = 11;
            lbParcelsCount.Text = "Počet balíků";
            // 
            // edParcelsCount
            // 
            edParcelsCount.Location = new System.Drawing.Point(100, 220);
            edParcelsCount.Name = "edParcelsCount";
            edParcelsCount.Size = new System.Drawing.Size(100, 23);
            edParcelsCount.TabIndex = 10;
            edParcelsCount.Text = "1";
            // 
            // edDirectionID
            // 
            edDirectionID.Location = new System.Drawing.Point(100, 190);
            edDirectionID.Name = "edDirectionID";
            edDirectionID.Size = new System.Drawing.Size(100, 23);
            edDirectionID.TabIndex = 9;
            edDirectionID.Text = "1298386";
            // 
            // lbDirectionID
            // 
            lbDirectionID.AutoSize = true;
            lbDirectionID.Location = new System.Drawing.Point(10, 190);
            lbDirectionID.Name = "lbDirectionID";
            lbDirectionID.Size = new System.Drawing.Size(66, 15);
            lbDirectionID.TabIndex = 8;
            lbDirectionID.Text = "DirectionID";
            // 
            // edCity
            // 
            edCity.Location = new System.Drawing.Point(100, 100);
            edCity.Name = "edCity";
            edCity.Size = new System.Drawing.Size(234, 23);
            edCity.TabIndex = 7;
            edCity.Text = "Beroun";
            // 
            // lbCity
            // 
            lbCity.AutoSize = true;
            lbCity.Location = new System.Drawing.Point(10, 100);
            lbCity.Name = "lbCity";
            lbCity.Size = new System.Drawing.Size(40, 15);
            lbCity.TabIndex = 6;
            lbCity.Text = "Město";
            // 
            // edAddress2
            // 
            edAddress2.Location = new System.Drawing.Point(100, 70);
            edAddress2.Name = "edAddress2";
            edAddress2.Size = new System.Drawing.Size(234, 23);
            edAddress2.TabIndex = 5;
            edAddress2.Text = "266 01";
            // 
            // lbAddress2
            // 
            lbAddress2.AutoSize = true;
            lbAddress2.Location = new System.Drawing.Point(10, 70);
            lbAddress2.Name = "lbAddress2";
            lbAddress2.Size = new System.Drawing.Size(52, 15);
            lbAddress2.TabIndex = 4;
            lbAddress2.Text = "Adresa 2";
            // 
            // edAddress1
            // 
            edAddress1.Location = new System.Drawing.Point(100, 40);
            edAddress1.Name = "edAddress1";
            edAddress1.Size = new System.Drawing.Size(234, 23);
            edAddress1.TabIndex = 3;
            edAddress1.Text = "Zborovské nábřeží 46";
            // 
            // lbAddress1
            // 
            lbAddress1.AutoSize = true;
            lbAddress1.Location = new System.Drawing.Point(10, 40);
            lbAddress1.Name = "lbAddress1";
            lbAddress1.Size = new System.Drawing.Size(52, 15);
            lbAddress1.TabIndex = 2;
            lbAddress1.Text = "Adresa 1";
            // 
            // edName
            // 
            edName.Location = new System.Drawing.Point(100, 10);
            edName.Name = "edName";
            edName.Size = new System.Drawing.Size(234, 23);
            edName.TabIndex = 1;
            edName.Text = "Richard Horký";
            // 
            // lbName
            // 
            lbName.AutoSize = true;
            lbName.Location = new System.Drawing.Point(10, 10);
            lbName.Name = "lbName";
            lbName.Size = new System.Drawing.Size(42, 15);
            lbName.TabIndex = 0;
            lbName.Text = "Jméno";
            // 
            // tabControl1
            // 
            tabControl1.Controls.Add(tabPage1);
            tabControl1.Controls.Add(tabPage2);
            tabControl1.Controls.Add(tabPage3);
            tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            tabControl1.Location = new System.Drawing.Point(0, 0);
            tabControl1.Name = "tabControl1";
            tabControl1.SelectedIndex = 3;
            tabControl1.Size = new System.Drawing.Size(954, 549);
            tabControl1.TabIndex = 0;
            tabControl1.SelectedIndexChanged += tabControl1_SelectedIndexChanged;
            // 
            // TestForm
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(954, 549);
            Controls.Add(tabControl1);
            Name = "TestForm";
            Text = "TestForm";
            tabPage3.ResumeLayout(false);
            tabPage3.PerformLayout();
            tabPage2.ResumeLayout(false);
            tabPage2.PerformLayout();
            tabPage1.ResumeLayout(false);
            tabPage1.PerformLayout();
            tabControl1.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button btnSendDS;
        private System.Windows.Forms.ListBox logDel;
        private System.Windows.Forms.Label lbDirectionIDDS;
        private System.Windows.Forms.TextBox edDirectionIDDS;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnReprintManifest;
        private System.Windows.Forms.TextBox edManifest;
        private System.Windows.Forms.Label lbManifest;
        private System.Windows.Forms.Button btnSendL;
        private System.Windows.Forms.ListBox logL;
        private System.Windows.Forms.Label lbDirectionIDL;
        private System.Windows.Forms.TextBox edDirectionIDL;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button btnManifest;
        private System.Windows.Forms.Label lbWeight;
        private System.Windows.Forms.TextBox edWeight;
        private System.Windows.Forms.Label lbPhone;
        private System.Windows.Forms.TextBox edPhone;
        private System.Windows.Forms.Label lbEmail;
        private System.Windows.Forms.TextBox edEmail;
        private System.Windows.Forms.Button btnSendCS;
        private System.Windows.Forms.ListBox log;
        private System.Windows.Forms.Label lbParcelsCount;
        private System.Windows.Forms.TextBox edParcelsCount;
        private System.Windows.Forms.TextBox edDirectionID;
        private System.Windows.Forms.Label lbDirectionID;
        private System.Windows.Forms.TextBox edCity;
        private System.Windows.Forms.Label lbCity;
        private System.Windows.Forms.TextBox edAddress2;
        private System.Windows.Forms.Label lbAddress2;
        private System.Windows.Forms.TextBox edAddress1;
        private System.Windows.Forms.Label lbAddress1;
        private System.Windows.Forms.TextBox edName;
        private System.Windows.Forms.Label lbName;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button btnDPD2;
        private System.Windows.Forms.Button btnGetLabelDPD2;
        private System.Windows.Forms.Button btnDeleteDPD2;
    }
}
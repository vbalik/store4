[List description]
[Description]
 Text=Projekt List & Label
 FileVersion=15
 RootTable=
 DataProviderMode=Interface
 Units=SCM-Units
 Metric=2
 TextQuote.Start=1
 TextQuote.End=1
 RepresentationCode.CondStart=171
 RepresentationCode.CondSep=166
 RepresentationCode.CondEnd=187
 RepresentationCode.Tab=247
 RepresentationCode.Ret=182
 RepresentationCode.PhantomSpace=8203
 RepresentationCode.LockNextChar=8288
 RepresentationCode.ExprSep=164
 LCID=1029
 UserVars.ManualSortOrder=1
 Created=7.11.2018, 10:59:40
 CreatedByApp=DOTNET.EXE [2.1.26919.01 @BuiltBy: dlab14-DDVSOWINAGE018 @Branch: release/2.1 @SrcCode: https://github.com/dotnet/core-setup/tree/290303f510986f8f832fd2dc9e313cebe06ec68d. Commit Hash: 290303f510986f8f832fd2dc9e313cebe06ec68d]
 CreatedByDLL=CXLL24.DLL [24.0.2018.29813 (18-10-19 13:46)F]
 CreatedByUser=petrun on NB-PETRUN
 LastModified=31.01.2020, 12:56:29
 LastModifiedByApp=DEMOAPPLICATION24.EXE [24.1.0.0]
 LastModifiedByDLL=CMLL24.DLL [24.1.2019.1611 (19-01-16 11:22)F]
 LastModifiedByUser=petrun on NB-PETRUNNEW
 CreatedByCallFrom=
 [@DatabaseStructure]
  {
  [DatabaseStructures]
   {
   [DatabaseStructure]
	{
	}
   }
  }
[Layout]
 P1.Template=
 xGrid=991
 yGrid=991
 GridShow=FALSE
 GridSnap=FALSE
 EnhancedExpressions=TRUE
 EnhancedOldExpressions=FALSE
 Decimals=2
 RealDataPreviewPageLimit=-2
 DefFont={(0,0,0),12.000000,-16,0,0,0,400,0,0,0,238,4,0,0,0,Arial}
 HelpLines.Horizontal=
 HelpLines.Vertical=
 PDF.MaxOutlineDepth=2
 PDF.MaxOutlineDepth.IDX=3
 DefaultSize.Objecttype1=(0,0,71603,7188)
 DefaultSize.Objecttype2=(0,0,284150,25832)
 DefaultSize.Objecttype6=(0,0,284150,154330)
 ObjectCount=0
[UserSection]
 
[SumVariables]
[Layer]
 Name=Základní vrstva
 Condition=
 WorkspaceColor=(102,102,102)
 Visible=TRUE
 LayerID=0
[Layer]
 Name=První stránka
 Condition=Page() = 1
 WorkspaceColor=(197,207,10)
 Visible=TRUE
 LayerID=1
[Layer]
 Name=Další stránky
 Condition=Page() <> 1
 WorkspaceColor=(161,203,243)
 Visible=TRUE
 LayerID=2
[ProjectTemplates]
 [@ProjectTemplates]
  {
  [ProjectTemplates]
   {
   }
  }
[UserVariables]
[Parameters]
 [@Parameters]
  {
  [Parameters]
   {
   LL.ActiveLayout=Standard Layout
   LL.DesignScheme="COMBITBLUE"
   LL.ProjectDescription=Projekt List & Label
   }
  }
[UsedIdentifiers]
 Variables=DocDate;DocInfo;PartnerAddr_1;PartnerAddr_2;PartnerCity;PartnerName;PartnerRef
 Fields=DispatchReportModel.Col1;DispatchReportModel.Col3;DispatchReportModel.Col4;DispatchReportModel.Col5;DispatchReportModel.Col6;DispatchReportModel.GroupNum;DispatchReportModel.Head
 ChartFields=
[PageLayouts]
 [@PageLayouts]
  {
  [PageLayouts]
   {
   [PageLayout]
	{
	DisplayName=
	Condition=True
	SourceTray=0
	ForcePaperFormat=True
	UsePhysicalPaper=True
	PaperFormat=9
	PaperFormat.cx=296951
	PaperFormat.cy=209982
	PaperFormat.Orientation=2
	Copies=1
	Duplex=0
	Collate=0
	FitPage=False
	ShowInPrintDialog=True
	}
   }
  }
[ExtendedInfo]
 [@ExtendedInfos]
  {
  [UserDefinedDesignScheme]
   {
   [Foreground]
	{
	Color=(188,221,161)
	Color=(154,201,73)
	Color=(135,165,101)
	Color=(235,93,129)
	Color=(75,152,184)
	Color=(91,179,213)
	Color=(171,214,226)
	Color=(153,153,153)
	Color=(102,102,102)
	Color=(204,204,204)
	}
   [Background]
	{
	Color=(251,251,251)
	Color=(204,204,204)
	Color=(77,77,77)
	Color=(51,51,51)
	FillMode=1
	}
   }
  [UIDescriptions]
   {
   }
  }

﻿
--velké šťítky
DECLARE @reportID2 as int;
SET @reportID2 = 28; 

--DELETE FROM [s4_reports] WHERE [reportID] = @reportID2;
IF NOT EXISTS (SELECT 1 FROM [dbo].[s4_reports] WHERE (reportID = @reportID2))
BEGIN
	SET IDENTITY_INSERT [dbo].[s4_reports] ON
    
	INSERT [dbo].[s4_reports] ([reportID], [reportType], [template], [settings], [printerTypeID]) VALUES (@reportID2, N'dispatchLabel', N'BF ON
	FT "Univers Condensed Bold",70
	PP 80,0
	PT "{1} / {2}"

	FT "Univers Condensed Bold",40
	PP 100,200
	PT "{0.DocNumPrefix}/{0.DocYear}/{0.DocNumber}/"
	PF', NULL, 4)

	SET IDENTITY_INSERT [dbo].[s4_reports] OFF
	print 'insert ok reportID = '+ CAST(@reportID2 AS varchar(10));
END

GO


UPDATE [dbo].[s4_reports]
  SET  [template] =  (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Stitky\ZboziPC43.rep', SINGLE_NCLOB ) as Document)
 WHERE [reportID] = 9

UPDATE [dbo].[s4_reports]
  SET  [template] =  (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Stitky\BoxSmallPC43.rep', SINGLE_NCLOB ) as Document), [settings] = '{ "reportName": "Intermec - malý" }'
 WHERE [reportID] = 10

UPDATE [dbo].[s4_reports]
  SET  [template] =  (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Stitky\BoxPC43.rep', SINGLE_NCLOB ) as Document), [settings] = '{ "reportName": "Intermec - velký" }'
 WHERE [reportID] = 11
   
UPDATE [dbo].[s4_reports]
  SET  [template] =  (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Stitky\BoxBig2.rep', SINGLE_NCLOB ) as Document), [settings] = '{ "reportName": "Velký 2" }'
 WHERE [reportID] = 13

UPDATE [dbo].[s4_reports]
  SET  [template] =  (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Stitky\BoxBig.rep', SINGLE_NCLOB ) as Document), [settings] = '{ "reportName": "Velký" }'
 WHERE [reportID] = 14

UPDATE [dbo].[s4_reports]
  SET  [template] =  (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Stitky\BoxSmall.rep', SINGLE_NCLOB ) as Document), [settings] = '{ "reportName": "Malý" }'
 WHERE [reportID] = 15

UPDATE [dbo].[s4_reports]
  SET  [template] =  (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Stitky\PaletaB.rep', SINGLE_NCLOB ) as Document)
 WHERE [reportID] = 17

UPDATE [dbo].[s4_reports]
  SET  [template] =  (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Stitky\Zbozi.rep', SINGLE_NCLOB ) as Document)
 WHERE [reportID] = 18

UPDATE [dbo].[s4_reports]
  SET  [template] =  (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Stitky\Paleta.rep', SINGLE_NCLOB ) as Document)
 WHERE [reportID] = 19
 
 GO
 
 
--Expedice rozvoz Morava
UPDATE [dbo].[s4_reports]
  SET  [template] =  (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Reporty\RozvozExpedice.srt', SINGLE_NCLOB ) as Document)
 WHERE [reportID] = 22

--DodaciList
UPDATE [dbo].[s4_reports]
  SET  [template] =  (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Reporty\DodaciList.srt', SINGLE_NCLOB ) as Document)
 WHERE [reportID] = 1

--VydejovyList
UPDATE [dbo].[s4_reports]
  SET  [template] =  (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Reporty\VydejovyList.srt', SINGLE_NCLOB ) as Document)
 WHERE [reportID] = 8

 --Nebezpečný náklad
--UPDATE [dbo].[s4_reports]
--  SET  [template] =  (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Reporty\NebezpecnyNaklad.srt', SINGLE_NCLOB ) as Document)
-- WHERE [reportID] = 39


--report StavSkladuPodleVyrobce
DECLARE @jsonVariable NVARCHAR(MAX)

SET @jsonVariable = N'{
"sql": "
	SELECT	MAX(art.articleCode) AS articleCode,
			MAX(art.articleDesc) AS articleDesc,
			SUM(onpos.quantity) AS quantity,
			CASE WHEN onpos.batchNum = ''_NONE_'' THEN NULL ELSE onpos.batchNum END as batchNum,
			CASE WHEN onpos.expirationDate = ''01.01.1900'' THEN NULL ELSE onpos.expirationDate END as expirationDate,
			art.unitDesc
	FROM dbo.s4_vw_onStore_Summary_Valid onpos
			JOIN dbo.s4_vw_article_base art ON (onpos.artPackID = art.artPackID)
			JOIN dbo.s4_positionList pos ON (onpos.positionID = pos.positionID)
	WHERE	(manufID = @manufID) 			
			AND (pos.posCateg = 0)
	GROUP BY
		art.articleID, 
		onpos.batchNum, 
		onpos.expirationDate,
		art.useExpiration,
		art.unitDesc
	ORDER BY
		MAX(art.articleCode),
		onpos.batchNum
		",
"sql4Filters": [
	{
		"sql" : "select manufID as ID, manufName AS name from [dbo].[s4_manufactList] where manufStatus = ''MAN_OK'' AND  manufid in (select b.manufID from s4_vw_article_base b join s4_vw_onStore_Summary_Valid v on b.artPackID = v.artPackID join s4_positionList pos on v.positionID = pos.positionID WHERE pos.posCateg = 0) order by name",
		"name" : "Výrobce",
		"id" : "manufID",
		"type" : "sql",
		"width" : "400",
		"mandatory": "true"
	}
],
"typeQuery": "None",
"reportName": "Stav skladu podle výrobce"
}'


UPDATE [dbo].[s4_reports]
  SET  [template] =  (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Reporty\StavSkladuPodleVyrobce.srt', SINGLE_NCLOB ) as Document),
	   [settings] =  @jsonVariable
 WHERE [reportID] = 3

 --report StavSkladuPodleSekce
DECLARE @jsonVariable1 NVARCHAR(MAX)
SET @jsonVariable1 = N'{
"sql": "
SELECT	
		MAX(art.articleCode) AS articleCode,
		MAX(art.articleDesc) AS articleDesc,
		SUM(onpos.quantity) AS quantity,
		CASE WHEN onpos.batchNum = ''_NONE_'' THEN NULL ELSE onpos.batchNum END as batchNum,
		CASE WHEN onpos.expirationDate = ''01.01.1900'' THEN NULL ELSE onpos.expirationDate END as expirationDate,
		manuf.manufName,
		art.unitDesc
	FROM dbo.s4_vw_onStore_Summary_Valid onpos
			JOIN dbo.s4_vw_article_base art ON (onpos.artPackID = art.artPackID)
			JOIN dbo.s4_positionList pos ON (onpos.positionID = pos.positionID)
			LEFT JOIN dbo.s4_manufactList manuf ON manuf.manufID = art.manufID
	WHERE	(LEN(@manufID) = 0 OR art.manufID = @manufID) 
			AND (onpos.positionID IN (SELECT positionID FROM [dbo].[s4_positionList_sections] WHERE sectID = @sectID))
			AND (pos.posCateg = 0)
	GROUP BY
		art.articleID, 
		onpos.batchNum, 
		onpos.expirationDate,
		manuf.manufName,
		art.unitDesc
	ORDER BY
		MAX(art.articleCode),
		onpos.batchNum
		",
"sql4Filters": [
	{
		"sql": "select manufID as ID, manufName AS name from [dbo].[s4_manufactList] where manufStatus = ''MAN_OK'' AND  manufid in (select b.manufID from s4_vw_article_base b join s4_vw_onStore_Summary_Valid v on b.artPackID = v.artPackID join s4_positionList pos on v.positionID = pos.positionID WHERE pos.posCateg = 0) order by name",
		"name" : "Výrobce",
		"id" : "manufID",
		"type" : "sql",
		"width" : "400",
		"mandatory": "false"
	},
	{
		"sql": "select sectID as ID, sectDesc AS name from [dbo].[s4_sectionList] order by name",
		"name" : "Sekce",
		"id" : "sectID",
		"type" : "sql",
		"width" : "400",
		"mandatory": "true"
	}
],
"typeQuery": "None",
"reportName": "Stav skladu podle sekce"
}'


UPDATE [dbo].[s4_reports]
  SET  [template] =  (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Reporty\StavSkladuPodleSekce.srt', SINGLE_NCLOB ) as Document),
	   [settings] =  @jsonVariable1
 WHERE [reportID] = 4

 
 --report prace skladniku
DECLARE @jsonVariable2 NVARCHAR(MAX)
SET @jsonVariable2 = N'{
"sql": "
        DECLARE @base TABLE
        (
        workerID	varchar(16),
        jobID		varchar(16),
        docs		int,
        items		int
        )

        INSERT INTO @base
        SELECT
        workerID,
        ass.jobID,
        COUNT(DISTINCT dr.directionID),
        COUNT(*)
        FROM	dbo.s4_direction dr WITH (NOLOCK) JOIN dbo.s4_direction_items di WITH (NOLOCK) ON (dr.directionID = di.directionID)
        JOIN dbo.s4_direction_assign ass WITH (NOLOCK) ON (di.directionID = ass.directionID)
        WHERE	(docDirection = 1)
        AND (docStatus != ''CANC'')
        AND (docDate_d BETWEEN @from AND @to)
        GROUP BY
        workerID,
        ass.jobID


        INSERT INTO @base
        SELECT
        workerID,
        ass.jobID,
        COUNT(DISTINCT dr.directionID),
        COUNT(*)
        FROM	dbo.s4_direction dr WITH (NOLOCK) JOIN dbo.s4_direction_items di WITH (NOLOCK) ON (dr.directionID = di.directionID)
        JOIN dbo.s4_direction_assign ass WITH (NOLOCK) ON (di.directionID = ass.directionID)
        WHERE	(docDirection = -1)
        AND (docStatus != ''CANC'')
        AND (docDate_d BETWEEN @from AND @to)
        GROUP BY
        workerID,
        ass.jobID


        INSERT INTO @base
        SELECT
        si.entryUserID,
        ''MOVE'',
        COUNT(DISTINCT sm.stoMoveID),
        COUNT(*) / 2
        FROM	dbo.s4_storeMove sm JOIN dbo.s4_storeMove_items si (nolock) ON (sm.stoMoveID = si.stoMoveID)
        WHERE	(docType IN (2, 4, 9))
        AND (docDate_d BETWEEN @from AND @to)
        GROUP BY
        si.entryUserID


        SELECT
        (SELECT workerName FROM dbo.s4_workerList WHERE workerID = ba.workerID) AS Jmeno,

        (SELECT SUM(docs) FROM @base WHERE workerID = ba.workerID AND jobID = ''RECE'') AS Prijem_dokl,
        (SELECT SUM(items) FROM @base WHERE workerID = ba.workerID AND jobID = ''RECE'') AS Prijem_pol,

        (SELECT SUM(docs) FROM @base WHERE workerID = ba.workerID AND jobID = ''STIN'') AS Nasklad_dokl,
        (SELECT SUM(items) FROM @base WHERE workerID = ba.workerID AND jobID = ''STIN'') AS Nasklad_pol,

        (SELECT SUM(docs) FROM @base WHERE workerID = ba.workerID AND jobID = ''DISP'') AS Vychyst_dokl,
        (SELECT SUM(items) FROM @base WHERE workerID = ba.workerID AND jobID = ''DISP'') AS Vychyst_pol,

        (SELECT SUM(docs) FROM @base WHERE workerID = ba.workerID AND jobID = ''DICH'') AS Kontrola_dokl,
        (SELECT SUM(items) FROM @base WHERE workerID = ba.workerID AND jobID = ''DICH'') AS Kontrola_pol,

        (SELECT SUM(docs) FROM @base WHERE workerID = ba.workerID AND jobID = ''MOVE'') AS Preskl_dokl,
        (SELECT SUM(items) FROM @base WHERE workerID = ba.workerID AND jobID = ''MOVE'') AS Preskl_pol
        FROM	@base ba
        GROUP BY
            workerID
        ORDER BY
            (SELECT workerName FROM dbo.s4_workerList WHERE workerID = ba.workerID)
		",
"sql4Filters": [
	{
		"sql": "",
		"name" : "Datum od",
		"id" : "from",
		"type" : "date",
		"class" : "col-sm-2",
		"mandatory": "true"
	},
	{
		"sql": "",
		"name" : "Datum do",
		"id" : "to",
		"type" : "date",
		"class" : "col-sm-2",
		"mandatory": "true"
	}
],
"typeQuery": "None",
"reportName": "Práce skladníků",
"flag": "1024"
}'


UPDATE [dbo].[s4_reports]
  SET  [template] =  (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Reporty\praceSkladniku.srt', SINGLE_NCLOB ) as Document),
	   [settings] =  @jsonVariable2
 WHERE [reportID] = 6
  
 --report expirace
 DECLARE @jsonVariable3 NVARCHAR(MAX)
 SET @jsonVariable3 = N'{
"sql": "
	DECLARE	@expirationDate	smalldatetime;
	SET	@expirationDate = DATEADD(m, CAST(@expiraceMonth AS INT), GETDATE())

	SELECT	onp.positionID, art.articleCode, art.articleDesc, quantity, CASE WHEN batchNum = ''_NONE_'' THEN NULL ELSE batchNum END as batchNum, expirationDate, pos.posCode, m.source_id
	FROM	dbo.s4_vw_onStore_Summary_Valid onp
	JOIN dbo.s4_positionList pos ON (onp.positionID = pos.positionID)
	JOIN dbo.s4_vw_article_base art ON (onp.artPackID = art.artPackID)
	JOIN s4_manufactList m ON (m.manufID = art.manufID)
	JOIN s4_articleList a ON (a.articleID = art.articleID)
	WHERE (expirationDate <= @expirationDate AND expirationDate > ''1900-01-01 00:00:00'') AND (pos.posCateg = 0) AND (LEN(@manufID) = 0 OR art.manufID = @manufID) 
	AND (LEN(@brand) = 0 OR a.brand LIKE ''%'' + @brand + ''%'') 
	AND (LEN(@sectID) = 0 OR (pos.positionID IN (SELECT positionID FROM dbo.s4_positionList_sections WHERE sectID = @sectID)))
	ORDER BY expirationDate, art.articleCode, pos.posCode
		",
"sql4Filters": [
	{
		"sql": "select manufID as ID, manufName AS name from [dbo].[s4_manufactList] where manufStatus = ''MAN_OK'' AND  manufid in (select b.manufID from s4_vw_article_base b join s4_vw_onStore_Summary_Valid v on b.artPackID = v.artPackID join s4_positionList pos on v.positionID = pos.positionID WHERE pos.posCateg = 0) order by name",
		"name" : "Výrobce",
		"id" : "manufID",
		"type" : "sql",
		"width" : "200",
		"mandatory": "false"
	},
	{
		"sql": "",
		"name" : "Značka",
		"id" : "brand",
		"type" : "input",
		"width" : "200",
		"mandatory": "false"
	},
	{
		"sql": "select '''' as ID, ''*všechny sekce*'' AS name UNION select sectID as ID, sectDesc AS name from [dbo].[s4_sectionList] order by name",
		"name" : "Sekce",
		"id" : "sectID",
		"type" : "sql",
		"width" : "300",
		"mandatory": "false"
	},
	{
		"sql": "select 1 as ID, 1 as name UNION select 2 as ID, 2 as name UNION select 3 as ID, 3 as name UNION select 4 as ID, 4 as name UNION select 5 as ID, 5 as name UNION select 6 as ID, 6 as name UNION select 7 as ID, 7 as name UNION select 8 as ID, 8 as name UNION select 9 as ID, 9 as name UNION select 10 as ID, 10 as name UNION select 11 as ID, 11 as name UNION select 12 as ID, 12 as name",
		"name" : "Počet měsíců",
		"id" : "expiraceMonth",
		"type" : "sql",
		"width" : "100",
		"mandatory": "true",
		"datatype": "number"
	}
],
"typeQuery": "None",
"reportName": "Expirace"
}'

UPDATE [dbo].[s4_reports]
  SET  [template] = (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Reporty\Expirace.srt', SINGLE_NCLOB ) as Document),
	   [settings] = @jsonVariable3
 WHERE [reportID] = 5
 
UPDATE [dbo].[s4_reports]
  SET  [template] =  (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Reporty\SkladovyPohyb.srt', SINGLE_NCLOB ) as Document)
 WHERE [reportID] = 20

UPDATE [dbo].[s4_reports]
  SET  [template] =  (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Reporty\NaSklade.srt', SINGLE_NCLOB ) as Document)
 WHERE [reportID] = 21

--report výrobce na skladě
 DECLARE @jsonVariable4 NVARCHAR(MAX)
 SET @jsonVariable4 = N'{
"sql": "
	DECLARE @base TABLE (    articleID int,    positionID int,    quantity numeric(38,4)   )
	INSERT INTO @base (articleID, positionID, quantity)

	SELECT    art.articleID,    onpos.positionID,    SUM(quantity) AS quantity
	FROM dbo.s4_vw_onStore_Summary_Valid onpos
	JOIN dbo.s4_vw_article_base art ON (onpos.artPackID = art.artPackID)
	JOIN dbo.s4_positionList pos ON (onpos.positionID = pos.positionID)
	WHERE (art.manufID = @manufID)    AND (pos.posCateg = 0) 
	AND (LEN(@sectID) = 0 OR (onpos.positionID IN (SELECT positionID FROM dbo.s4_positionList_sections WHERE (sectID = @sectID))))
	GROUP BY    articleID,    onpos.positionID

	SELECT art.articleCode, art.articleDesc, onpos.quantity, onpos.posCnt
	FROM (    SELECT     articleID,     SUM(quantity) AS quantity,     COUNT(positionID) AS posCnt
	FROM @base base
	GROUP BY     articleID    ) onpos
	JOIN dbo.s4_articleList art ON (onpos.articleID = art.articleID)
	ORDER BY    art.articleCode
		",
"sql4Filters": [
	{
		"sql": "select manufID as ID, manufName AS name from [dbo].[s4_manufactList] where manufStatus = ''MAN_OK'' AND  manufid in (select b.manufID from s4_vw_article_base b join s4_vw_onStore_Summary_Valid v on b.artPackID = v.artPackID join s4_positionList pos on v.positionID = pos.positionID WHERE pos.posCateg = 0) order by name",
		"name" : "Výrobce",
		"id" : "manufID",
		"type" : "sql",
		"width" : "400",
		"mandatory": "true"
	},
	{
		"sql": "select sectID as ID, sectDesc AS name from [dbo].[s4_sectionList] order by name",
		"name" : "Sekce",
		"id" : "sectID",
		"type" : "sql",
		"width" : "400",
		"mandatory": "false"
	}
],
"typeQuery": "None",
"reportName": "Výrobce na skladě"
}'


UPDATE [dbo].[s4_reports]
  SET  [template] = (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Reporty\VyrobceNaSklade.srt', SINGLE_NCLOB ) as Document),
	   [settings] = @jsonVariable4
 WHERE [reportID] = 23

 --report obsah pozice
 DECLARE @jsonVariable5 NVARCHAR(MAX)
 SET @jsonVariable5 = N'{
"sql": "
	DECLARE @base TABLE (    articleID int,    posCode varchar(16),    quantity numeric(38,4)   )
	INSERT INTO @base (articleID, posCode, quantity)

	SELECT    art.articleID,    pos.posCode,    SUM(quantity) AS quantity
	FROM dbo.s4_vw_onStore_Summary_Valid onpos
	JOIN dbo.s4_vw_article_base art ON (onpos.artPackID = art.artPackID)
	JOIN dbo.s4_positionList pos ON (onpos.positionID = pos.positionID)
	WHERE (pos.positionID = @positionID) AND (pos.posCateg = 0) 
	GROUP BY    articleID,    pos.posCode

	SELECT art.articleCode, art.articleDesc, onpos.quantity
	FROM (    SELECT     articleID,     SUM(quantity) AS quantity
	FROM @base base
	GROUP BY     articleID    ) onpos
	JOIN dbo.s4_articleList art ON (onpos.articleID = art.articleID)
	ORDER BY    art.articleCode
		",
"sql4Filters": [
	{
		"sql": "",
		"name" : "Kód pozice",
		"id" : "positionID",
		"type" : "positionSeek",
		"width" : "400",
		"mandatory": "true",
		"datatype": "number"
	}
],
"typeQuery": "None",
"reportName": "Obsah pozice"
}'


UPDATE [dbo].[s4_reports]
  SET  [template] = (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Reporty\ObsahPozice.srt', SINGLE_NCLOB ) as Document),
	   [settings] = @jsonVariable5
 WHERE [reportID] = 24

 --report obsah pozice2
 DECLARE @jsonVariable6 NVARCHAR(MAX)
 SET @jsonVariable6 = N'{
"sql": "
		SELECT art.articleCode, MAX(art.articleDesc) as articleDesc, SUM(onpos.quantity) as quantity, CASE WHEN batchNum = ''_NONE_'' THEN NULL ELSE onpos.batchNum END as batchNum, art.unitDesc, 
		CASE WHEN onpos.expirationDate = ''1900-01-01 00:00:00'' THEN NULL ELSE onpos.expirationDate END as expirationDate
		FROM dbo.s4_vw_onStore_Summary_Valid onpos
		JOIN dbo.s4_vw_article_base art ON (onpos.artPackID = art.artPackID)
		JOIN dbo.s4_positionList pos ON (onpos.positionID = pos.positionID)
		WHERE (pos.positionID = @positionID)  AND (pos.posCateg = 0) 
		GROUP BY art.articleCode, art.unitDesc, onpos.batchNum, onpos.expirationDate
		ORDER BY art.articleCode, onpos.batchNum
		",
"sql4Filters": [
	{
		"sql": "",
		"name" : "Kód pozice",
		"id" : "positionID",
		"type" : "positionSeek",
		"width" : "400",
		"mandatory": "true",
		"datatype": "number"
	}
],
"typeQuery": "None",
"reportName": "Obsah pozice (šarže, expirace)"
}'


UPDATE [dbo].[s4_reports]
  SET  [template] = (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Reporty\ObsahPozice2.srt', SINGLE_NCLOB ) as Document),
	   [settings] = @jsonVariable6
 WHERE [reportID] = 25

  --report Prijem nebo Vydej dle Sarze a Karty
 DECLARE @jsonVariable7 NVARCHAR(MAX)
 SET @jsonVariable7 = N'{
"sql": "
	SELECT CASE WHEN lot.batchNum = ''_NONE_'' THEN NULL ELSE lot.batchNum END as batchNum,
	lot.expirationDate, smi.quantity, sm.docDate, sm.docInfo as doklad, d.docInfo as prikaz, p.partnerName, ap.packDesc
	FROM s4_storeMove_items smi (nolock)
	LEFT JOIN s4_storeMove sm on smi.stoMoveID = sm.stoMoveID
	LEFT JOIN s4_direction d on d.directionID = sm.parent_directionID
	LEFT JOIN s4_storeMove_lots lot on lot.stoMoveLotID = smi.stoMoveLotID
	LEFT JOIN s4_articleList_packings ap on ap.artPackID = lot.artPackID
	LEFT JOIN s4_partnerList p on p.partnerID = d.partnerID
	WHERE d.docDirection = @docDirection AND lot.batchNum = @batchNum AND ap.articleID = @articleID AND sm.docDate between @od AND @do
	AND sm.docType = CASE WHEN @docDirection = -1 THEN 5 ELSE 1 END
	AND smi.itemValidity = 100
	GROUP BY lot.batchNum, lot.expirationDate, smi.quantity, sm.docDate, sm.docInfo, d.docInfo, p.partnerName, ap.packDesc
	ORDER BY sm.docDate;
		",
"sql4Filters": [
	{
		"sql": "select -1 as ID, ''Vyskladnění'' as name union select 1 as ID, ''Naskladnění'' as name",
		"name" : "Směr",
		"id" : "docDirection",
		"type" : "sql",
		"width" : "150",
		"mandatory": "true",
		"datatype": "number"
	},
	{
		"sql": "",
		"name" : "Karta",
		"id" : "articleID",
		"type" : "articleSeek",
		"width" : "800",
		"mandatory": "true",
		"datatype": "number"
	},
	{
		"sql": "",
		"name" : "Šarže",
		"id" : "batchNum",
		"type" : "input",
		"width" : "400",
		"mandatory": "true"
	},
	{
		"sql": "",
		"name" : "Datum od",
		"id" : "od",
		"type" : "date",
		"width" : "",
		"mandatory": "true"
	},
	{
		"sql": "",
		"name" : "Datum do",
		"id" : "do",
		"type" : "date",
		"width" : "",
		"mandatory": "true"
	}
],
"typeQuery": "None",
"reportName": "Pohyby dle šarže a karty"
}'

UPDATE [dbo].[s4_reports]
  SET  --[template] = (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Reporty\PrijemVydejDleSarzeKarty.srt', SINGLE_NCLOB ) as Document),
	   [settings] = @jsonVariable7
 WHERE [reportID] = 26

  --report StavSkladuCelkovy
DECLARE @jsonVariable8 NVARCHAR(MAX)
SET @jsonVariable8 = N'{
"sql": "
SELECT	
		MAX(art.articleCode) AS articleCode,
		MAX(art.articleDesc) AS articleDesc,
		SUM(onpos.quantity) AS quantity,
		CASE WHEN onpos.batchNum = ''_NONE_'' THEN NULL ELSE onpos.batchNum END as batchNum,
		CASE WHEN onpos.expirationDate = ''01.01.1900'' THEN NULL ELSE onpos.expirationDate END as expirationDate,
		manuf.manufName
	FROM dbo.s4_vw_onStore_Summary_Valid onpos
			JOIN dbo.s4_vw_article_base art ON (onpos.artPackID = art.artPackID)
			JOIN dbo.s4_positionList pos ON (onpos.positionID = pos.positionID)
			LEFT JOIN dbo.s4_manufactList manuf ON manuf.manufID = art.manufID
	WHERE  (LEN(@batchNum) = 0 OR onpos.batchNum = @batchNum) AND (LEN(@articleID) = 0 OR art.articleID = @articleID) AND (pos.posCateg = 0)
	GROUP BY
		art.articleID, 
		onpos.batchNum, 
		onpos.expirationDate,
		manuf.manufName
	ORDER BY
		MAX(art.articleCode),
		onpos.batchNum
		",
"sql4Filters": [
	{
		"sql": "",
		"name" : "Karta",
		"id" : "articleID",
		"type" : "articleSeek",
		"width" : "800",
		"mandatory": "false",
		"datatype": "number"
	},
	{
		"sql": "",
		"name" : "Šarže",
		"id" : "batchNum",
		"type" : "input",
		"width" : "400",
		"mandatory": "false"
	}
],
"typeQuery": "None",
"reportName": "Celkový stav skladu"
}'


UPDATE [dbo].[s4_reports]
  SET  [template] =  (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Reporty\StavSkladuPodleSekce.srt', SINGLE_NCLOB ) as Document),
	   [settings] =  @jsonVariable8
 WHERE [reportID] = 27

 
--report PohybyKeKartam
DECLARE @jsonVariable9 NVARCHAR(MAX)
SET @jsonVariable9 = N'{
"sql": "
	DECLARE @count as int

	SELECT * into #temp FROM STRING_SPLIT(@articleCode, '';'');
	SELECT @count = (select COUNT(1) FROM #temp where value != '''');

	SELECT * into #temp2 FROM STRING_SPLIT(@sectID, '','');

	SELECT
	--Kod výrobku, název výrobku, šarže, expirace, partner, datum, množství, doklad
	ar.articleCode, ar.articleDesc,
	CASE WHEN lot.batchNum = ''_NONE_'' THEN NULL ELSE lot.batchNum END as batchNum,
	lot.expirationDate,	
	pa.partnerName,
	paa.addrName + '' '' + paa.addrLine_1 + '' '' + paa.addrLine_2 + '' '' + paa.addrCity + '' '' + paa.addrPhone + '' '' + paa.addrRemark + '' '' + paa.deliveryInst as partnerAddress,
	mo.docDate,
	CASE mo.docType WHEN 5 THEN - moi.quantity ELSE moi.quantity END AS quantity,
	dir.docInfo

FROM	dbo.s4_storeMove mo
		JOIN dbo.s4_storeMove_Items moi (nolock) ON mo.stoMoveID = moi.stoMoveID
		JOIN dbo.s4_storeMove_lots lot ON moi.stoMoveLotID = lot.stoMoveLotID
		JOIN dbo.s4_vw_article_Base ar ON (lot.artPackID = ar.artPackID)
		JOIN dbo.s4_direction dir ON (mo.parent_directionID = dir.directionID)
		JOIN dbo.s4_partnerList pa ON (dir.partnerID = pa.partnerID)
		LEFT JOIN dbo.s4_partnerList_addresses paa ON (dir.partAddrID = paa.partAddrID)
WHERE	
	(
		(
			(mo.docType = 5) 
			AND (moi.itemDirection = -1) 
			AND (moi.itemValidity = 100)
			AND (mo.parent_directionID IN (SELECT directionID FROM dbo.s4_direction WHERE docNumPrefix <> ''DLL''))
		)
		OR 
		(
			(mo.docType = 3) 
			AND (moi.itemDirection = 1) 
			AND (moi.itemValidity = 100)
		)
	)
	AND 
	(
		(@count = 0 OR ar.articleCode IN (select value from #temp))
	)
	AND (LEN(@manufID) = 0 OR ar.manufID = @manufID)
	AND (convert(varchar, mo.docDate, 121) BETWEEN @from AND DATEADD(DAY, 1, @to))
	AND ((LEN(@sectID) = 0 OR @sectID IS NULL) OR moi.positionID IN (SELECT positionID FROM dbo.s4_positionList_sections WHERE (sectID IN (SELECT value FROM #temp2 WHERE RTRIM(value) <> '''') ))) 
ORDER BY
	ar.articleCode,
	lot.batchNum,
	mo.docDate

	drop table #temp
	drop table #temp2
		",
"sql4Filters": [
	{
		"sql": "",
		"name" : "Kódy karet (oddělené středníkem)",
		"id" : "articleCode",
		"type" : "input",
		"width" : "1000",
		"mandatory": "false"
	},
	{
		"sql" : "select manufID as ID, manufName AS name from [dbo].[s4_manufactList] where manufStatus = ''MAN_OK'' AND  manufid in (select b.manufID from s4_vw_article_base b join s4_vw_onStore_Summary_Valid v on b.artPackID = v.artPackID join s4_positionList pos on v.positionID = pos.positionID WHERE pos.posCateg = 0) order by name",
		"name" : "Výrobce",
		"id" : "manufID",
		"type" : "sql",
		"width" : "400",
		"mandatory": "false"
	},
	{
		"sql": "",
		"name" : "Datum od",
		"id" : "from",
		"type" : "date",
		"class" : "col-sm-2",
		"mandatory": "true"
	},
	{
		"sql": "",
		"name" : "Datum do",
		"id" : "to",
		"type" : "date",
		"class" : "col-sm-2",
		"mandatory": "true"
	},
	{
		"sql" : "select distinct(sectID) as ID, sectDesc as name from dbo.s4_sectionList order by sectID",
		"name" : "Sekce",
		"id" : "sectID",
		"type" : "sqlMulti",
		"width" : "370",
		"mandatory": "false"
	}
],

"typeQuery": "None",
"reportName": "Pohyby ke kartám"
}'


UPDATE [dbo].[s4_reports]
  SET  --[template] =  (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Reporty\PohybyKeKartam.srt', SINGLE_NCLOB ) as Document),
	   [settings] =  @jsonVariable9
 WHERE [reportID] = 29


--report Využití pozic
DECLARE @jsonVariable10 NVARCHAR(MAX)
SET @jsonVariable10 = N'{
"sql": "

	SELECT r.positionID, r.posCode, r.posDesc, r.houseID, r.posCateg, r.posStatus, r.posHeight, r.posAttributes, r.posX, r.posY, r.posZ, r.mapPos, r.movesCnt, r.onStore, h.houseDesc
	FROM [dbo].[prom_fn_posUseReportBase](@od, @do) as r
	LEFT JOIN s4_houseList as h on h.houseID = r.houseID
	WHERE (LEN(@houseID) = 0 OR r.houseID IN (SELECT value FROM STRING_SPLIT(@houseID, '','') WHERE RTRIM(value) <> '''')) AND (LEN(@posZ) = 0 OR r.posZ IN (SELECT value FROM STRING_SPLIT(@posZ, '','') WHERE RTRIM(value) <> '''')) AND (@onStore = ''1'' AND onstore = onstore OR @onStore  = ''2'' AND onstore = 0 OR @onStore  = ''3'' AND onstore > 0) AND (@move = ''1'' AND movesCnt = movesCnt OR @move  = ''2'' AND movesCnt = 0 OR @move  = ''3'' AND movesCnt > 0)
	ORDER BY r.posCode, r.houseID
		",
"sql4Filters": [
	{
		"sql": "",
		"name" : "Datum od",
		"id" : "od",
		"type" : "date",
		"width" : "",
		"mandatory": "true"
	},
	{
		"sql": "",
		"name" : "Datum do",
		"id" : "do",
		"type" : "date",
		"width" : "",
		"mandatory": "true"
	},
	{
		"sql" : "select distinct(houseID) as ID, houseDesc as name from dbo.s4_houseList order by houseID",
		"name" : "Haly",
		"id" : "houseID",
		"type" : "sqlMulti",
		"width" : "400",
		"mandatory": "false"
	},
	{
		"sql": "select 1 as ID, ''1.patro'' as name union select 2 as ID, ''2.patro'' as name union select 3 as ID, ''3.patro'' as name union select 4 as ID, ''4.patro'' as name union select 5 as ID, ''5.patro'' as name",
		"name" : "Patro",
		"id" : "posZ",
		"type" : "sqlMulti",
		"width" : "400",
		"mandatory": "false",
		"datatype": "number"
	},
	{
		"sql": "select 1 as ID, ''Vše'' as name union select 2 as ID, ''Ano'' as name union select 3 as ID, ''Ne'' as name",
		"name" : "Prázdné pozice",
		"id" : "onStore",
		"type" : "sql",
		"width" : "120",
		"mandatory": "true",
		"datatype": "number"
	},
	{
		"sql": "select 1 as ID, ''Vše'' as name union select 2 as ID, ''Ano'' as name union select 3 as ID, ''Ne'' as name",
		"name" : "Bez pohybu",
		"id" : "move",
		"type" : "sql",
		"width" : "150",
		"mandatory": "true",
		"datatype": "number"
	}
],

"typeQuery": "None",
"reportName": "Využití pozic"
}'


UPDATE [dbo].[s4_reports]
  SET  [template] =  (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Reporty\VyuzitiPozic.srt', SINGLE_NCLOB ) as Document),
	   [settings] =  @jsonVariable10
 WHERE [reportID] = 30

 
--report Stav skladu historický
DECLARE @jsonVariable11 NVARCHAR(MAX)
SET @jsonVariable11 = N'{
"sql": "

SELECT SUM(quantity) as quantity, batchNum, expirationDate, articleCode, articleDesc, manufName, manufID, packDesc
FROM
(
	SELECT SUM(i.quantity * i.itemDirection) as quantity, CASE WHEN lots.batchNum = ''_NONE_'' THEN NULL ELSE lots.batchNum END as batchNum,
	CASE WHEN CONVERT(date, lots.expirationDate) = ''1900-01-01'' THEN NULL ELSE lots.expirationDate END as expirationDate, a.articleCode, a.articleDesc, man.manufName, man.manufID, p.packDesc

	FROM dbo.s4_storeMove_items as i (nolock)
	LEFT JOIN s4_storeMove as m on m.stoMoveID = i.stoMoveID
	LEFT JOIN dbo.s4_positionList pos ON (i.positionID = pos.positionID)

	JOIN dbo.s4_storeMove_lots lots ON (i.stoMoveLotID = lots.stoMoveLotID)
	LEFT JOIN s4_articleList_packings as p on lots.artPackID = p.artPackID
	LEFT JOIN s4_articleList as a on a.articleID = p.articleID
	LEFT JOIN s4_manufactList as man on man.manufID = a.manufID

	WHERE i.itemValidity = 100 AND m.docDate < DATEADD(day, 1, CAST(@date AS DATETIME)) AND
	(LEN(@sectID) = 0 OR i.positionID IN (SELECT positionID FROM [dbo].[s4_positionList_sections] WHERE sectID = @sectID)) AND (pos.posCateg = 0)
	AND (LEN(@articleID) = 0 OR a.articleID = @articleID) AND (LEN(@manufID) = 0 OR man.manufID = @manufID) 
	GROUP BY lots.batchNum, m.docDate, lots.expirationDate, a.articleCode, a.articleDesc, man.manufName, man.manufID, p.packDesc
	HAVING SUM(i.quantity * itemDirection) <> 0

) a
GROUP BY batchNum, expirationDate, articleCode, articleDesc, manufName, manufID, packDesc
HAVING SUM(quantity) <> 0
	",
"sql4Filters": [
	{
		"sql": "",
		"name" : "K datu",
		"id" : "date",
		"type" : "date",
		"width" : "",
		"mandatory": "true"
	},
	{
		"sql": "",
		"name" : "Karta",
		"id" : "articleID",
		"type" : "articleSeek",
		"width" : "400",
		"mandatory": "false",
		"datatype": "number"
	},
	{
		"sql": "select sectID as ID, sectDesc AS name from [dbo].[s4_sectionList] order by name",
		"name" : "Sekce",
		"id" : "sectID",
		"type" : "sql",
		"width" : "300",
		"mandatory": "false"
	},
	{
		"sql": "select manufID as ID, manufName AS name from [dbo].[s4_manufactList] where manufStatus = ''MAN_OK'' AND  manufid in (select b.manufID from s4_vw_article_base b join s4_vw_onStore_Summary_Valid v on b.artPackID = v.artPackID join s4_positionList pos on v.positionID = pos.positionID WHERE pos.posCateg = 0) order by name",
		"name" : "Výrobce",
		"id" : "manufID",
		"type" : "sql",
		"width" : "300",
		"mandatory": "false"
	}
],

"typeQuery": "None",
"reportName": "Historický stavu skladu k datu"
}'


UPDATE [dbo].[s4_reports]
  SET  [template] =  (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Reporty\StavSKladuKDatu.srt', SINGLE_NCLOB ) as Document),
	   [settings] =  @jsonVariable11
 WHERE [reportID] = 31

  
--report exchange, direction bez expedice, nebo bez kontroly
DECLARE @jsonVariable12 NVARCHAR(MAX)
SET @jsonVariable12 = N'{
"sql": "
	select docInfo, docDate, docStatus = CASE docStatus WHEN ''CREC'' THEN ''Příjem - hotovo'' WHEN ''CSTI'' THEN ''Naskladnění - hotovo'' WHEN ''DCHE'' THEN ''Kontrola - hotovo'' WHEN ''LOAD'' THEN ''Nahráno'' WHEN ''PDIM'' THEN ''PDIM'' WHEN ''PDIS'' THEN ''Vychystání - v běhu'' WHEN ''RES_IN_PROC'' THEN ''Rezervace'' WHEN ''RES_NOT_FOUND'' THEN ''Chyba rezervace'' WHEN ''SDIS'' THEN ''Vychystání - hotovo'' WHEN ''WDIM'' THEN ''WDIM'' WHEN ''WCHE'' THEN ''Kontrola - přiřazeno'' WHEN ''SHDEL'' THEN ''Zásilka vytvořena'' ELSE docStatus END
	from s4_direction
	where docStatus not in (''CANC'', ''EXPE'', ''SHDEL'',''NOT_PROC'') AND docDirection = -1 AND docDate < DATEADD(DAY, CAST(@last AS int) * -1, GetDate())
	order by docDate asc
	",
	"typeQuery": "DaysBack",
	"reportName": "Nevyexpedované doklady do "
}'


UPDATE [dbo].[s4_reports]
  SET   [template] =  (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Reporty\ExchangeDirectionOld.srt', SINGLE_NCLOB ) as Document),
		[settings] =  @jsonVariable12
 WHERE [reportID] = 32

  
--report Report seriových čísel

UPDATE [dbo].[s4_reports]
  SET   [template] =  (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Reporty\SeriovaCisla.srt', SINGLE_NCLOB ) as Document),
		[settings] =  null
 WHERE [reportID] = 33


--report exchange, Nemlog - zboží na příjmu (Naskladnění - hotovo), expirace kratší nebo rovno 12 měsíců
DECLARE @jsonVariable13 NVARCHAR(MAX)
SET @jsonVariable13 = N'{
"sql": "
	DECLARE	@expirationDate	smalldatetime;
	SET	@expirationDate = DATEADD(m, 12, GETDATE())
	
	SELECT
		--Kod výrobku, název výrobku, výrobce, šarže, expirace, datum, množství, doklad
		ar.articleCode,
		ar.articleDesc,
		man.manufName,
		CASE WHEN lot.batchNum = ''_NONE_'' THEN NULL ELSE lot.batchNum END as batchNum,
		lot.expirationDate,
		mo.docDate,
		CASE mo.docType WHEN 5 THEN - moi.quantity ELSE moi.quantity END AS quantity,
		dir.docInfo
	FROM	dbo.s4_storeMove mo
			JOIN dbo.s4_storeMove_Items moi (nolock) ON mo.stoMoveID = moi.stoMoveID
			JOIN dbo.s4_storeMove_lots lot (NOLOCK) ON moi.stoMoveLotID = lot.stoMoveLotID
			JOIN dbo.s4_vw_article_Base ar (NOLOCK) ON (lot.artPackID = ar.artPackID)
			JOIN dbo.s4_direction dir (NOLOCK) ON (mo.parent_directionID = dir.directionID)
			JOIN dbo.s4_manufactList (NOLOCK) man ON man.manufID = ar.manufID
	WHERE   dir.docDirection = 1 
			AND dir.docStatus = ''CSTI'' --Naskladnění - hotovo
			AND moi.positionID in (SELECT positionID FROM dbo.s4_positionList_sections WHERE sectID in (''NLOG'', ''CZZ''))
			AND (expirationDate <= @expirationDate AND expirationDate > ''1900-01-01 00:00:00'') 
			AND convert(varchar, dir.docDate, 105) = convert(varchar, getdate(), 105) AND moi.itemValidity = 100
	ORDER BY
		ar.articleCode,
		lot.batchNum,
		mo.docDate
	",
	"typeQuery": "None",
	"reportName": "Naskladněné karty s expirací 12 měsíců a méně"
}'


UPDATE [dbo].[s4_reports]
  SET   --[template] =  (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Reporty\ExpiracePrijemNemLog.srt', SINGLE_NCLOB ) as Document),
		[settings] =  @jsonVariable13
 WHERE [reportID] = 34

 
 --report seznam EANů
 DECLARE @jsonVariable14 NVARCHAR(MAX)
 SET @jsonVariable14 = N'{
"sql": "
	select a.articleCode, LTRIM(a.articleDesc) as articleDesc, p.packDesc, p.barCode, p.movablePack
	from s4_articleList_packings p
	left join s4_articleList a on a.articleID = p.articleID
	where p.barCode is not null and packStatus = ''AP_OK'' AND (LEN(@manufID) = 0 OR a.manufID = @manufID) AND (@movablePack = -1 OR p.movablePack = @movablePack)
	order by LTRIM(a.articleDesc)
		",
"sql4Filters": [
	{
		"sql": "select manufID as ID, manufName AS name from [dbo].[s4_manufactList] where manufStatus = ''MAN_OK'' AND  manufid in (select b.manufID from s4_vw_article_base b join s4_vw_onStore_Summary_Valid v on b.artPackID = v.artPackID join s4_positionList pos on v.positionID = pos.positionID WHERE pos.posCateg = 0) order by name",
		"name" : "Výrobce",
		"id" : "manufID",
		"type" : "sql",
		"width" : "300",
		"mandatory": "false"
	},
	{
		"sql": "select -1 as ID, ''Vše'' as name, 1 as orderCol union select 1 as ID, ''Pouze hlavní kódy'' as name, 2 as orderCol union select 0 as ID, ''Pouze vedlejší kódy'' as name, 3 as orderCol order by orderCol",
		"name" : "Typ balení",
		"id" : "movablePack",
		"type" : "sql",
		"width" : "250",
		"mandatory": "true",
		"datatype": "number"
	}
],
"typeQuery": "None",
"reportName": "Čárové kódy EAN"
}'

UPDATE [dbo].[s4_reports]
  SET  [template] = (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Reporty\EAN.srt', SINGLE_NCLOB ) as Document),
	   [reportType] = 'WebPrint',
	   [settings] = @jsonVariable14
 WHERE [reportID] = 35

 --report exchange, Sklad - DL vyexpedované změnou stavu
DECLARE @jsonVariable15 NVARCHAR(MAX)
SET @jsonVariable15 = N'{
"sql": "
DECLARE	@date	smalldatetime;
SET	@date = DATEADD(hh, -24, GETDATE())

SELECT d.docNumPrefix + ''/'' + docYear + ''/'' + CAST(docNumber AS VARCHAR(50)) as doc, d.docDate, p.partnerName, h2.workerName as userName
FROM [dbo].[s4_direction] d (NOLOCK)
left join [dbo].[s4_partnerList] p (NOLOCK) on p.partnerID = d.partnerID
left join 
	(select MAX(h.directionHistID) as directionHistID, MAX(h.entryUserID) as entryUserID, h.directionID, h.eventCode, h.eventData, w.workerName
	from s4_direction_history h (NOLOCK)
	left join s4_workerList w (NOLOCK) on w.workerID = h.entryUserID
	WHERE h.eventCode = ''STATUS_CHANGED'' AND h.eventData like ''%EXPE''
	group by h.directionID, h.eventCode, h.eventData, w.workerName) as h2 on h2.directionID = d.directionID 
left join s4_messageBus b (NOLOCK) on b.directionID = d.directionID
WHERE d.docDirection = -1 AND d.docStatus = ''EXPE'' AND d.docNumPrefix = ''DL'' AND d.docDate >= @date
AND NOT EXISTS (SELECT 1 FROM s4_direction_history WHERE directionID = d.directionID AND eventCode = ''EXPORT_DONE'')
AND h2.directionHistID IS NOT NULL
AND b.messageBusID IS NULL
group by d.directionID, d.docNumPrefix, docYear, docNumber, d.docDate, p.partnerName, h2.workerName
order by d.directionID desc
	",
	"typeQuery": "None",
	"reportName": "Chybně expedované doklady za 24 hodin"
}'


UPDATE [dbo].[s4_reports]
  SET   --[template] =  (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Reporty\ChybneExpediceSklad.srt', SINGLE_NCLOB ) as Document),
		[settings] =  @jsonVariable15
 WHERE [reportID] = 36

 GO

 --Inventura vzdálená
DECLARE @jsonVariable16 NVARCHAR(MAX)
SET @jsonVariable16 = N'{
"sql": "
	
		SELECT S.*, (quantStTa - quantOnSt) AS quantDiff, P.posCode, P.houseID, A.articleID, A.articleCode, A.articleDesc, A.packDesc FROM
		(
		SELECT positionID, artPackID, SUM(quantOnSt) AS quantOnSt, SUM(quantStTa) AS quantStTa
		FROM
		(SELECT positionID, artPackID, SUM(quantity) AS quantOnSt, 0 AS quantStTa
		FROM dbo.s4_stocktaking_snapshots
		WHERE(stotakID = @stotakID) AND(snapshotClass = @snapshotClass)
		GROUP BY positionID, artPackID
		UNION ALL
		SELECT positionID, artPackID, 0 as quantOnSt, SUM(quantity) AS quantStTa
		FROM dbo.s4_stocktaking_items
		WHERE(stotakID = @stotakID)
		GROUP BY positionID, artPackID) Derived GROUP BY positionID, artPackID
		HAVING ISNULL(SUM(quantOnSt), 0) != ISNULL(SUM(quantStTa), 0)) S
		LEFT OUTER JOIN s4_positionList P ON P.positionID = S.positionID
		LEFT OUTER JOIN s4_vw_article_base A ON A.artPackID = S.artPackID
		ORDER BY A.articleCode
	",
	"typeQuery": "None",
	"reportName": "Vzdálená inventura"
}'


UPDATE [dbo].[s4_reports]
  SET  [template] =  (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Reporty\VzdalenaInventura.srt', SINGLE_NCLOB ) as Document),
	   [settings] =  @jsonVariable16
 WHERE [reportID] = 37

 
 
--report exchange - zboží na příjmu (Naskladnění - hotovo), filtr na kategorie
DECLARE @jsonVariable17 NVARCHAR(MAX)
SET @jsonVariable17 = N'{
"sql": "
SELECT
		--Kod výrobku, název výrobku, výrobce, šarže, expirace, datum, množství, doklad
		ar.articleCode,
		ar.articleDesc,
		man.manufName,
		CASE WHEN lot.batchNum = ''_NONE_'' THEN NULL ELSE lot.batchNum END as batchNum,
		CASE WHEN lot.expirationDate = ''01.01.1900'' THEN NULL ELSE lot.expirationDate END as expirationDate,
		mo.docDate,
		CASE mo.docType WHEN 5 THEN - moi.quantity ELSE moi.quantity END AS quantity,
		dir.docInfo
	FROM	dbo.s4_storeMove mo
			JOIN dbo.s4_storeMove_Items (NOLOCK) moi ON mo.stoMoveID = moi.stoMoveID
			JOIN dbo.s4_storeMove_lots lot (NOLOCK) ON moi.stoMoveLotID = lot.stoMoveLotID
			JOIN dbo.s4_vw_article_Base ar (NOLOCK) ON (lot.artPackID = ar.artPackID)
			JOIN dbo.s4_direction dir (NOLOCK) ON (mo.parent_directionID = dir.directionID)
			JOIN dbo.s4_manufactList man (NOLOCK) ON man.manufID = ar.manufID
			LEFT JOIN [SQL-2019].[ABM3_Promedica].[dbo].[cache_karty] abm3Karty ON abm3Karty.kartaid = ar.source_id
	WHERE   dir.docDirection = 1 AND dir.docNumPrefix = ''PRIJ'' AND mo.docStatus = ''SREC''
			AND dir.docStatus = ''CSTI'' --Naskladnění - hotovo
			AND dir.docDate between DATEADD(DAY, CAST(@last AS int) * -1, GetDate()) AND getdate()
			AND abm3Karty.kategorie5_text IN (''OP - Ansell'',''OP - Halyard sety'',''OP - Efektline'',''OP - FAPOMED'',''OP - Halyard plaste'',''OP - Obuv na sály'') AND moi.itemValidity = 100
	ORDER BY
		ar.articleCode,
		lot.batchNum,
		mo.docDate
	",
	"typeQuery": "DaysBack",
	"reportName": "Naskladněné karty pro kategorii OP od"
}'


UPDATE [dbo].[s4_reports]
  SET   [template] =  (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Reporty\PrijemOP.srt', SINGLE_NCLOB ) as Document),
		[settings] =  @jsonVariable17
 WHERE [reportID] = 38
  
 GO

 --report obsah pozice podle výrobce
 DECLARE @jsonVariable NVARCHAR(MAX)
 DECLARE @reportID as int;
 SET @reportID = 39; 
 SET @jsonVariable = N'{
"sql": "
	SELECT * into #temp FROM STRING_SPLIT(@sectID, '','');

	SELECT CASE @emptyRow WHEN 1 THEN ROW_NUMBER() OVER(ORDER BY articleCode ASC) ELSE 1 END AS RowNum, pos.posCode, art.articleCode, MAX(art.articleDesc) as articleDesc, SUM(onpos.quantity) as quantity, CASE WHEN batchNum = ''_NONE_'' THEN NULL ELSE onpos.batchNum END as batchNum,
	art.unitDesc, CASE WHEN onpos.expirationDate = ''1900-01-01 00:00:00'' THEN NULL ELSE onpos.expirationDate END as expirationDate
	FROM dbo.s4_vw_onStore_Summary_Valid onpos
	JOIN dbo.s4_vw_article_base art ON (onpos.artPackID = art.artPackID)
	JOIN dbo.s4_positionList pos ON (onpos.positionID = pos.positionID)
	WHERE (pos.posCateg = 0) AND art.manufID = @manufID
	AND ((LEN(@sectID) = 0 OR @sectID IS NULL) OR pos.positionID IN (SELECT positionID FROM dbo.s4_positionList_sections WHERE (sectID IN (SELECT value FROM #temp WHERE RTRIM(value) <> '''') ))) 
	GROUP BY art.articleCode, art.unitDesc, onpos.batchNum, onpos.expirationDate, pos.posCode, pos.posDesc
	ORDER BY pos.posCode, art.articleCode, onpos.batchNum

	drop table #temp
		",
"sql4Filters": [
	{
		"sql" : "select manufID as ID, manufName AS name from [dbo].[s4_manufactList] where manufStatus = ''MAN_OK'' AND  manufid in (select b.manufID from s4_vw_article_base b join s4_vw_onStore_Summary_Valid v on b.artPackID = v.artPackID join s4_positionList pos on v.positionID = pos.positionID WHERE pos.posCateg = 0) order by name",
		"name" : "Výrobce",
		"id" : "manufID",
		"type" : "sql",
		"width" : "250",
		"mandatory": "true"
	},
	{
		"sql" : "select distinct(sectID) as ID, sectDesc as name from dbo.s4_sectionList order by sectID",
		"name" : "Sekce",
		"id" : "sectID",
		"type" : "sqlMulti",
		"width" : "600",
		"mandatory": "false"
	},
	{
		"sql": "select 1 as ID, ''Ano'' as name union select 0 as ID, ''Ne'' as name",
		"name" : "Prázdný sudý řádek",
		"id" : "emptyRow",
		"type" : "sql",
		"width" : "150",
		"mandatory": "false",
		"datatype": "number"
	}
],
"typeQuery": "None",
"reportName": "Obsah pozice podle výrobce"
}'



IF NOT EXISTS (SELECT 1 FROM [dbo].[s4_reports] WHERE (reportID = @reportID))
BEGIN
	SET IDENTITY_INSERT [dbo].[s4_reports] ON
    INSERT INTO [dbo].[s4_reports] ([reportID], [reportType], [template], [settings], [printerTypeID]) VALUES (@reportID, 'WebPrint', 'x', NULL, 1);
	SET IDENTITY_INSERT [dbo].[s4_reports] OFF
	print 'insert ok reportID = '+ CAST(@reportID AS varchar(10));
END

UPDATE [dbo].[s4_reports]
  SET  [template] = (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Reporty\ObsahPozice3.srt', SINGLE_NCLOB ) as Document),
	   [settings] = @jsonVariable
 WHERE [reportID] = @reportID

 go;

 --report expirace Nemlog exchange
 DECLARE @jsonVariable NVARCHAR(MAX)
 DECLARE @reportID as int;
 SET @reportID = 40; 
 SET @jsonVariable = N'{
"sql": "
	DECLARE	@expirationDate	smalldatetime;
	SET	@expirationDate = DATEADD(m, CAST(8 AS INT), GETDATE()); --8 month expiration

	SELECT	onp.positionID, art.articleCode, art.articleDesc, quantity, CASE WHEN batchNum = ''_NONE_'' THEN NULL ELSE batchNum END as batchNum, expirationDate, pos.posCode, m.source_id
	FROM	dbo.s4_vw_onStore_Summary_Valid onp
	JOIN dbo.s4_positionList pos ON (onp.positionID = pos.positionID)
	JOIN dbo.s4_vw_article_base art ON (onp.artPackID = art.artPackID)
	JOIN s4_manufactList m ON (m.manufID = art.manufID)
	JOIN s4_articleList a ON (a.articleID = art.articleID)
	WHERE (expirationDate < @expirationDate AND expirationDate > ''1900-01-01 00:00:00'') AND (pos.posCateg = 0) 
	AND (pos.positionID IN (SELECT positionID FROM dbo.s4_positionList_sections WHERE sectID = ''NLOG''))
	ORDER BY expirationDate, art.articleCode, pos.posCode
		",
"typeQuery": "None",
"typeExport": "Xlsx",
"reportName": "Zboží skladem s exspirací kratší 8 měsíců"
}'

IF NOT EXISTS (SELECT 1 FROM [dbo].[s4_reports] WHERE (reportID = @reportID))
BEGIN
	SET IDENTITY_INSERT [dbo].[s4_reports] ON
    INSERT INTO [dbo].[s4_reports] ([reportID], [reportType], [template], [settings], [printerTypeID]) VALUES (@reportID, 'Exchange', '', NULL, 1);
	SET IDENTITY_INSERT [dbo].[s4_reports] OFF
	print 'insert ok reportID = '+ CAST(@reportID AS varchar(10));
END

UPDATE [dbo].[s4_reports]
  SET  [settings] = @jsonVariable
 WHERE [reportID] = @reportID

GO

--report exchange, Nemlog - zboží na příjmu (Naskladnění - hotovo), expirace kratší nebo rovno 18 měsíců, výrobce TELEFLEX
DECLARE @jsonVariable NVARCHAR(MAX)
DECLARE @reportID as int;
SET @reportID = 41; 
SET @jsonVariable = N'{
"sql": "
	DECLARE	@expirationDate	smalldatetime;
	SET	@expirationDate = DATEADD(m, 18, GETDATE())
	
	SELECT
		--Kod výrobku, název výrobku, výrobce, šarže, expirace, datum, množství, doklad
		ar.articleCode,
		ar.articleDesc,
		man.manufName,
		CASE WHEN lot.batchNum = ''_NONE_'' THEN NULL ELSE lot.batchNum END as batchNum,
		lot.expirationDate,
		mo.docDate,
		CASE mo.docType WHEN 5 THEN - moi.quantity ELSE moi.quantity END AS quantity,
		dir.docInfo
	FROM	dbo.s4_storeMove mo
			JOIN dbo.s4_storeMove_Items (NOLOCK) moi ON mo.stoMoveID = moi.stoMoveID
			JOIN dbo.s4_storeMove_lots (NOLOCK) lot ON moi.stoMoveLotID = lot.stoMoveLotID
			JOIN dbo.s4_vw_article_Base (NOLOCK) ar ON (lot.artPackID = ar.artPackID)
			JOIN dbo.s4_direction dir (NOLOCK) ON (mo.parent_directionID = dir.directionID)
			JOIN dbo.s4_manufactList (NOLOCK) man ON man.manufID = ar.manufID
	WHERE   dir.docDirection = 1 
			AND dir.docStatus = ''CSTI'' --Naskladnění - hotovo
			AND moi.positionID in (SELECT positionID FROM dbo.s4_positionList_sections WHERE sectID in (''NLOG'', ''CZZ''))
			AND (expirationDate <= @expirationDate AND expirationDate > ''1900-01-01 00:00:00'') 
			AND convert(varchar, dir.docDate, 105) = convert(varchar, getdate(), 105) AND moi.itemValidity = 100
			AND man.manufID = ''TELEFLEX''
	ORDER BY
		ar.articleCode,
		lot.batchNum,
		mo.docDate
	",
	"typeQuery": "None",
	"reportName": "Naskladněné karty s expirací 18 měsíců a méně, výrobce TELEFLEX"
}'


IF NOT EXISTS (SELECT 1 FROM [dbo].[s4_reports] WHERE (reportID = @reportID))
BEGIN
	SET IDENTITY_INSERT [dbo].[s4_reports] ON
    INSERT INTO [dbo].[s4_reports] ([reportID], [reportType], [template], [settings], [printerTypeID]) VALUES (@reportID, 'Exchange', '', NULL, 1);
	SET IDENTITY_INSERT [dbo].[s4_reports] OFF
	print 'insert ok reportID = '+ CAST(@reportID AS varchar(10));
END

UPDATE [dbo].[s4_reports]
  SET   --[template] =  (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Reporty\ExpiracePrijemNemLog.srt', SINGLE_NCLOB ) as Document),
		[settings] =  @jsonVariable
 WHERE [reportID] = @reportID

 GO

 
 --report expirace Nemlog exchange
 DECLARE @jsonVariable NVARCHAR(MAX)
 DECLARE @reportID as int;
 SET @reportID = 42; 
 SET @jsonVariable = N'{
"sql": "
	DECLARE	@expirationDate	smalldatetime;
	SET	@expirationDate = DATEADD(m, CAST(14 AS INT), GETDATE()); --14 month expiration

	SELECT	onp.positionID, art.articleCode, art.articleDesc, quantity, CASE WHEN batchNum = ''_NONE_'' THEN NULL ELSE batchNum END as batchNum, expirationDate, pos.posCode, m.source_id
	FROM	dbo.s4_vw_onStore_Summary_Valid onp
	JOIN dbo.s4_positionList pos ON (onp.positionID = pos.positionID)
	JOIN dbo.s4_vw_article_base art ON (onp.artPackID = art.artPackID)
	JOIN s4_manufactList m ON (m.manufID = art.manufID)
	JOIN s4_articleList a ON (a.articleID = art.articleID)
	WHERE (expirationDate < @expirationDate AND expirationDate > ''1900-01-01 00:00:00'') AND (pos.posCateg = 0) 
	AND (pos.positionID IN (SELECT positionID FROM dbo.s4_positionList_sections WHERE sectID = ''NLOG''))
	AND m.manufID = ''TELEFLEX''
	ORDER BY expirationDate, art.articleCode, pos.posCode
		",
"typeQuery": "None",
"typeExport": "Xlsx",
"reportName": "Zboží výrobce ''Teleflex'' skladem s exspirací kratší 14 měsíců"
}'

IF NOT EXISTS (SELECT 1 FROM [dbo].[s4_reports] WHERE (reportID = @reportID))
BEGIN
	SET IDENTITY_INSERT [dbo].[s4_reports] ON
    INSERT INTO [dbo].[s4_reports] ([reportID], [reportType], [template], [settings], [printerTypeID]) VALUES (@reportID, 'Exchange', '', NULL, 1);
	SET IDENTITY_INSERT [dbo].[s4_reports] OFF
	print 'insert ok reportID = '+ CAST(@reportID AS varchar(10));
END

UPDATE [dbo].[s4_reports]
  SET  [settings] = @jsonVariable
 WHERE [reportID] = @reportID

GO

--report expirace Nemlog exchange Report FNKV
 DECLARE @jsonVariable NVARCHAR(MAX)
 DECLARE @reportID as int;
 SET @reportID = 43; 
 SET @jsonVariable = N'{
"sql": "
	DECLARE	@expirationDate	smalldatetime;
	SET	@expirationDate = DATEADD(m, CAST(6 AS INT), GETDATE()); --6 month expiration

	SELECT	onp.positionID, art.articleCode, art.articleDesc, quantity, CASE WHEN batchNum = ''_NONE_'' THEN NULL ELSE batchNum END as batchNum, expirationDate, pos.posCode, m.source_id
	FROM	dbo.s4_vw_onStore_Summary_Valid onp
	JOIN dbo.s4_positionList pos ON (onp.positionID = pos.positionID)
	JOIN dbo.s4_vw_article_base art ON (onp.artPackID = art.artPackID)
	JOIN s4_manufactList m ON (m.manufID = art.manufID)
	JOIN s4_articleList a ON (a.articleID = art.articleID)
	WHERE (expirationDate < @expirationDate AND expirationDate > ''1900-01-01 00:00:00'') AND (pos.posCateg = 0) 
	AND (pos.positionID IN (SELECT positionID FROM dbo.s4_positionList_sections WHERE sectID = ''BOSK'') AND pos.houseID = ''HAL2'')
	ORDER BY expirationDate, art.articleCode, pos.posCode
		",
"typeQuery": "None",
"typeExport": "Xlsx",
"reportName": "Zboží skladem s exspirací kratší 6 měsíců, sekce FNKV zásoba, Hala 2"
}'

IF NOT EXISTS (SELECT 1 FROM [dbo].[s4_reports] WHERE (reportID = @reportID))
BEGIN
	SET IDENTITY_INSERT [dbo].[s4_reports] ON
    INSERT INTO [dbo].[s4_reports] ([reportID], [reportType], [template], [settings], [printerTypeID]) VALUES (@reportID, 'Exchange', '', NULL, 1);
	SET IDENTITY_INSERT [dbo].[s4_reports] OFF
	print 'insert ok reportID = '+ CAST(@reportID AS varchar(10));
END

UPDATE [dbo].[s4_reports]
  SET  [settings] = @jsonVariable
 WHERE [reportID] = @reportID

GO

--report expirace Nemlog exchange Report Karviná
 DECLARE @jsonVariable NVARCHAR(MAX)
 DECLARE @reportID as int;
 SET @reportID = 44; 
 SET @jsonVariable = N'{
"sql": "
	DECLARE	@expirationDate	smalldatetime;
	SET	@expirationDate = DATEADD(m, CAST(6 AS INT), GETDATE()); --6 month expiration

	SELECT	onp.positionID, art.articleCode, art.articleDesc, quantity, CASE WHEN batchNum = ''_NONE_'' THEN NULL ELSE batchNum END as batchNum, expirationDate, pos.posCode, m.source_id
	FROM	dbo.s4_vw_onStore_Summary_Valid onp
	JOIN dbo.s4_positionList pos ON (onp.positionID = pos.positionID)
	JOIN dbo.s4_vw_article_base art ON (onp.artPackID = art.artPackID)
	JOIN s4_manufactList m ON (m.manufID = art.manufID)
	JOIN s4_articleList a ON (a.articleID = art.articleID)
	WHERE (expirationDate < @expirationDate AND expirationDate > ''1900-01-01 00:00:00'') AND (pos.posCateg = 0) 
	AND (pos.positionID IN (SELECT positionID FROM dbo.s4_positionList_sections WHERE sectID = ''KROZ''))
	ORDER BY expirationDate, art.articleCode, pos.posCode
		",
"typeQuery": "None",
"typeExport": "Xlsx",
"reportName": "Zboží skladem s exspirací kratší 6 měsíců, sekce Karviná rozbalování"
}'

IF NOT EXISTS (SELECT 1 FROM [dbo].[s4_reports] WHERE (reportID = @reportID))
BEGIN
	SET IDENTITY_INSERT [dbo].[s4_reports] ON
    INSERT INTO [dbo].[s4_reports] ([reportID], [reportType], [template], [settings], [printerTypeID]) VALUES (@reportID, 'Exchange', '', NULL, 1);
	SET IDENTITY_INSERT [dbo].[s4_reports] OFF
	print 'insert ok reportID = '+ CAST(@reportID AS varchar(10));
END

UPDATE [dbo].[s4_reports]
  SET  [settings] = @jsonVariable
 WHERE [reportID] = @reportID

GO

--report expirace Nemlog exchange Report Opava
 DECLARE @jsonVariable NVARCHAR(MAX)
 DECLARE @reportID as int;
 SET @reportID = 45; 
 SET @jsonVariable = N'{
"sql": "
	DECLARE	@expirationDate	smalldatetime;
	SET	@expirationDate = DATEADD(m, CAST(6 AS INT), GETDATE()); --6 month expiration

	SELECT	onp.positionID, art.articleCode, art.articleDesc, quantity, CASE WHEN batchNum = ''_NONE_'' THEN NULL ELSE batchNum END as batchNum, expirationDate, pos.posCode, m.source_id
	FROM	dbo.s4_vw_onStore_Summary_Valid onp
	JOIN dbo.s4_positionList pos ON (onp.positionID = pos.positionID)
	JOIN dbo.s4_vw_article_base art ON (onp.artPackID = art.artPackID)
	JOIN s4_manufactList m ON (m.manufID = art.manufID)
	JOIN s4_articleList a ON (a.articleID = art.articleID)
	WHERE (expirationDate < @expirationDate AND expirationDate > ''1900-01-01 00:00:00'') AND (pos.posCateg = 0) 
	AND (pos.positionID IN (SELECT positionID FROM dbo.s4_positionList_sections WHERE sectID = ''MLBO''))
	ORDER BY expirationDate, art.articleCode, pos.posCode
		",
"typeQuery": "None",
"typeExport": "Xlsx",
"reportName": "Zboží skladem s exspirací kratší 6 měsíců, sekce Opava zásoba"
}'

IF NOT EXISTS (SELECT 1 FROM [dbo].[s4_reports] WHERE (reportID = @reportID))
BEGIN
	SET IDENTITY_INSERT [dbo].[s4_reports] ON
    INSERT INTO [dbo].[s4_reports] ([reportID], [reportType], [template], [settings], [printerTypeID]) VALUES (@reportID, 'Exchange', '', NULL, 1);
	SET IDENTITY_INSERT [dbo].[s4_reports] OFF
	print 'insert ok reportID = '+ CAST(@reportID AS varchar(10));
END

UPDATE [dbo].[s4_reports]
  SET  [settings] = @jsonVariable
 WHERE [reportID] = @reportID

GO


--report exchange - zboží na příjmu (Naskladnění - hotovo), filtr na kategorie
 DECLARE @reportID as int;
 SET @reportID = 46; 

DECLARE @jsonVariable NVARCHAR(MAX)
SET @jsonVariable = N'{
"sql": "
SELECT
		--Kod výrobku, název výrobku, výrobce, šarže, expirace, datum, množství, doklad
		ar.articleCode,
		ar.articleDesc,
		man.manufName,
		CASE WHEN lot.batchNum = ''_NONE_'' THEN NULL ELSE lot.batchNum END as batchNum,
		CASE WHEN lot.expirationDate = ''01.01.1900'' THEN NULL ELSE lot.expirationDate END as expirationDate,
		mo.docDate,
		CASE mo.docType WHEN 5 THEN - moi.quantity ELSE moi.quantity END AS quantity,
		dir.docInfo
	FROM	dbo.s4_storeMove mo
			JOIN dbo.s4_storeMove_Items (NOLOCK) moi ON mo.stoMoveID = moi.stoMoveID
			JOIN dbo.s4_storeMove_lots lot (NOLOCK) ON moi.stoMoveLotID = lot.stoMoveLotID
			JOIN dbo.s4_vw_article_Base ar (NOLOCK) ON (lot.artPackID = ar.artPackID)
			JOIN dbo.s4_direction dir (NOLOCK) ON (mo.parent_directionID = dir.directionID)
			JOIN dbo.s4_manufactList man (NOLOCK) ON man.manufID = ar.manufID
			LEFT JOIN [SQL-2019].[ABM3_Promedica].[dbo].[cache_karty] abm3Karty ON abm3Karty.kartaid = ar.source_id
	WHERE   dir.docDirection = 1 AND dir.docNumPrefix = ''PRIJ'' AND mo.docStatus = ''SREC''
			AND dir.docStatus = ''CSTI'' --Naskladnění - hotovo
			AND dir.docDate between DATEADD(DAY, CAST(@last AS int) * -1, GetDate()) AND getdate()
			AND abm3Karty.kategorie5_text IN (''IM - VR Medical'',''IM - Pinmed'',''IM - Efektline - ostatní'',''IM - Efektline - filtry'',''IM - SINAPI - hrudní drenáž'',''IM - Biometrix'',''IM - AMPri'',''IM - Efektline - kyslíková terapie'',''IM - Avanos Kimvent Turbo-Cleaning 72'',''IM - Efektline - odsávací cévky'',''IM - Convatec'',''IM - Efektline - ventilační okruhy'',''IM - Avanos Kimvent Standard 24'',''IM - Avanos Kimvent Oral Care'') AND moi.itemValidity = 100
	ORDER BY
		ar.articleCode,
		lot.batchNum,
		mo.docDate
	",
	"typeQuery": "DaysBack",
	"reportName": "Naskladněné karty pro kategorii IM od"
}'

IF NOT EXISTS (SELECT 1 FROM [dbo].[s4_reports] WHERE (reportID = @reportID))
BEGIN
	SET IDENTITY_INSERT [dbo].[s4_reports] ON
    INSERT INTO [dbo].[s4_reports] ([reportID], [reportType], [template], [settings], [printerTypeID]) VALUES (@reportID, 'Exchange', '', NULL, 1);
	SET IDENTITY_INSERT [dbo].[s4_reports] OFF
	print 'insert ok reportID = '+ CAST(@reportID AS varchar(10));
END

UPDATE [dbo].[s4_reports]
  SET   --[template] =  (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Reporty\PrijemOP.srt', SINGLE_NCLOB ) as Document),
		[settings] =  @jsonVariable
 WHERE [reportID] = @reportID
  
 GO

 --report - Naskladněné karty podle sekce (Naskladnění - hotovo)
 DECLARE @reportID as int;
 SET @reportID = 47; 

DECLARE @jsonVariable NVARCHAR(MAX)
SET @jsonVariable = N'{
"sql": "
SELECT * into #temp2 FROM STRING_SPLIT(@sectID, '','');

SELECT
		--Kod výrobku, název výrobku, výrobce, šarže, expirace, datum, množství, doklad
		ar.articleCode,
		ar.articleDesc,
		man.manufName,
		CASE WHEN lot.batchNum = ''_NONE_'' THEN NULL ELSE lot.batchNum END as batchNum,
		lot.expirationDate,
		mo.docDate,
		CASE mo.docType WHEN 5 THEN - moi.quantity ELSE moi.quantity END AS quantity,
		dir.docInfo
	FROM	dbo.s4_storeMove mo
			JOIN dbo.s4_storeMove_Items moi (nolock) ON mo.stoMoveID = moi.stoMoveID
			JOIN dbo.s4_storeMove_lots lot (NOLOCK) ON moi.stoMoveLotID = lot.stoMoveLotID
			JOIN dbo.s4_vw_article_Base ar (NOLOCK) ON (lot.artPackID = ar.artPackID)
			JOIN dbo.s4_direction dir (NOLOCK) ON (mo.parent_directionID = dir.directionID)
			JOIN dbo.s4_manufactList (NOLOCK) man ON man.manufID = ar.manufID
	WHERE   dir.docDirection = 1 
			AND dir.docStatus = ''CSTI'' --Naskladnění - hotovo
		    AND (@sectID IS NULL OR moi.positionID IN (SELECT positionID FROM dbo.s4_positionList_sections WHERE (sectID IN (SELECT value FROM #temp2 WHERE RTRIM(value) <> '''') ))) 
			AND dir.docDate between @od AND @do
			AND moi.itemValidity = 100
   ORDER BY
			ar.articleCode,
			lot.batchNum,
			mo.docDate

drop table #temp2
	",
	"sql4Filters": [
	{
		"sql": "",
		"name" : "Datum od",
		"id" : "od",
		"type" : "date",
		"width" : "",
		"mandatory": "true"
	},
	{
		"sql": "",
		"name" : "Datum do",
		"id" : "do",
		"type" : "date",
		"width" : "",
		"mandatory": "true"
	},
	{
		"sql" : "select distinct(sectID) as ID, sectDesc as name from dbo.s4_sectionList order by sectID",
		"name" : "Sekce",
		"id" : "sectID",
		"type" : "sqlMulti",
		"width" : "370",
		"mandatory": "true"
	}
],
	"typeQuery": "None",
	"reportName": "Naskladněné karty podle sekce"
}'

IF NOT EXISTS (SELECT 1 FROM [dbo].[s4_reports] WHERE (reportID = @reportID))
BEGIN
	SET IDENTITY_INSERT [dbo].[s4_reports] ON
    INSERT INTO [dbo].[s4_reports] ([reportID], [reportType], [template], [settings], [printerTypeID]) VALUES (@reportID, 'WebPrint', '', NULL, 1);
	SET IDENTITY_INSERT [dbo].[s4_reports] OFF
	print 'insert ok reportID = '+ CAST(@reportID AS varchar(10));
END

UPDATE [dbo].[s4_reports]
  SET   --[template] =  (select BulkColumn FROM OPENROWSET (BULK N'c:\temp\NaskladneneKartyPodleSekce.srt', SINGLE_NCLOB ) as Document),
		[settings] =  @jsonVariable
 WHERE [reportID] = @reportID
  
 GO

 --Report přepravní doklad
 DECLARE @reportID as int;
 SET @reportID = 49; 

 IF NOT EXISTS (SELECT 1 FROM [dbo].[s4_reports] WHERE (reportID = @reportID))
BEGIN
	SET IDENTITY_INSERT [dbo].[s4_reports] ON
    INSERT INTO [dbo].[s4_reports] ([reportID], [reportType], [template], [settings], [printerTypeID]) VALUES (@reportID, 'WebPrint', '', NULL, 1);
	SET IDENTITY_INSERT [dbo].[s4_reports] OFF
	print 'insert ok reportID = '+ CAST(@reportID AS varchar(10));
END

UPDATE [dbo].[s4_reports]
  SET   [template] =  (select BulkColumn FROM OPENROWSET (BULK N'c:\tmp\PrepravniDoklad.srt', SINGLE_NCLOB ) as Document),
		[settings] =  null
 WHERE [reportID] = @reportID

 GO

  --Archív dokumentů
DECLARE @reportID as int;
SET @reportID = 50;

IF NOT EXISTS (SELECT 1 FROM [dbo].[s4_reports] WHERE (reportID = @reportID))
BEGIN
	SET IDENTITY_INSERT [dbo].[s4_reports] ON
    INSERT INTO [dbo].[s4_reports] ([reportID], [reportType], [template], [settings], [printerTypeID]) VALUES (@reportID, 'WebPrint', '', NULL, 1);
	SET IDENTITY_INSERT [dbo].[s4_reports] OFF
	print 'insert ok reportID = '+ CAST(@reportID AS varchar(10));
END

DECLARE @jsonVariable NVARCHAR(MAX)
SET @jsonVariable = N'{
"sql": "
--kód , název karty, poznámka, znak, stav skladu, pozice
select a.articleCode
     , a.articleDesc
     , a.sign
     , qty.quantity
     , qty.position
     , qty.docRemark
from s4_articleList a

         left join (SELECT SUM(si.[quantity] * si.[itemDirection]) AS [quantity]
                         , lots.artPackID
                         , ap.articleID
                         , mov.docRemark
                         , pos.posCode                             as position
                    FROM [dbo].[s4_storeMove_items] si
                             inner join s4_storeMove_lots lots on si.stoMoveLotID = lots.stoMoveLotID
                             inner join s4_articleList_packings ap ON ap.artPackID = lots.artPackID
                             inner join s4_storeMove mov on mov.stoMoveID = si.stoMoveID
                             left join s4_positionList pos on si.positionID = pos.positionID
                    WHERE [itemValidity] = 100
                    GROUP BY lots.artPackID, ap.articleID, mov.docRemark, pos.posCode
                    HAVING SUM(si.[quantity] * si.[itemDirection]) <> 0) qty ON qty.articleID = a.articleID
where a.sectID = ''ARCHIV''
  and qty.quantity > 0
	",
	"sql4Filters": [],
	"typeQuery": "None",
	"reportName": "Archív dokumentů"
}'

UPDATE [dbo].[s4_reports]
    SET [template] =  (select BulkColumn FROM OPENROWSET (BULK N'c:\temp\ArchivDokumentu.srt', SINGLE_NCLOB ) as Document)
		[settings] =  @jsonVariable
 WHERE [reportID] = @reportID
  
 GO
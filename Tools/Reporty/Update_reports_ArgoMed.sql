--�t�tek pozice" pro tisk�rnu PC43 (ArgoMed)

--DELETE FROM [s4_printerType] WHERE [printerTypeID] = 8;
IF NOT EXISTS (SELECT 1 FROM [dbo].[s4_printerType] WHERE ([printerTypeID] = 8))
BEGIN
SET IDENTITY_INSERT [dbo].[s4_printerType] ON
INSERT INTO [dbo].[s4_printerType] ([printerTypeID], [printerClass], [printerDesc])
     VALUES (8, 2, 'ARGOMED_PM43_VELKA')
SET IDENTITY_INSERT [dbo].[s4_printerType] OFF
END

GO


--DodaciList Start
UPDATE [dbo].[s4_reports]
  SET  [template] =  (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Reporty\DodaciListARG.srt', SINGLE_NCLOB ) as Document)
 WHERE [reportID] = 1

GO
--DodaciList End

DECLARE @reportID3 as int;
SET @reportID3 = 29; 

DELETE FROM [s4_reports] WHERE [reportID] = @reportID3;
IF NOT EXISTS (SELECT 1 FROM [dbo].[s4_reports] WHERE (reportID = @reportID3))
BEGIN
	SET IDENTITY_INSERT [dbo].[s4_reports] ON
    
	INSERT [dbo].[s4_reports] ([reportID], [reportType], [template], [settings], [printerTypeID]) VALUES (@reportID3, N'positionLabel', N'BF ON
PP 120,100
BT "CODE128"
BH 100
BARFONT "Univers Condensed Bold",80
PRBAR "{0.PosCode}"
PF', NULL, 5)

	SET IDENTITY_INSERT [dbo].[s4_reports] OFF
	print 'insert ok reportID = '+ CAST(@reportID3 AS varchar(10));
END

GO

DECLARE @reportID4 as int;
SET @reportID4 = 30; 

DELETE FROM [s4_reports] WHERE [reportID] = @reportID4;
IF NOT EXISTS (SELECT 1 FROM [dbo].[s4_reports] WHERE (reportID = @reportID4))
BEGIN
	SET IDENTITY_INSERT [dbo].[s4_reports] ON
    
	INSERT [dbo].[s4_reports] ([reportID], [reportType], [template], [settings], [printerTypeID]) VALUES (@reportID4, N'boxLabel', N'BF ON
FT "Univers Bold",17
PP 60,460
PT "{3.AddrName}"
PP 60,400
PT "{3.AddrLine_1}"
PP 60,340
PT "{3.AddrLine_2}"
PP 60,280
PT "{3.AddrCity}"
FT "Univers",15
PP 60,190
PT "{3.AddrPhone}"
PP 60,130
PT "{3.AddrRemark}"
PF', '{ "reportName": "Tisk adresy" }', 5)

	SET IDENTITY_INSERT [dbo].[s4_reports] OFF
	print 'insert ok reportID = '+ CAST(@reportID4 AS varchar(10));
END

GO

IF NOT EXISTS (SELECT 1 FROM [dbo].[s4_printerLocation] WHERE ([printerLocationID] = 42))
BEGIN
	SET IDENTITY_INSERT [dbo].[s4_printerLocation] ON
	INSERT [dbo].[s4_printerLocation] ([printerLocationID], [printerLocationDesc], [printerPath], [printerSettings], [printerTypeID])
	VALUES (42, N'ARGOMED PM43 VELKA', N'ARGOMED_PM43_VELKA', NULL, 8)
	SET IDENTITY_INSERT [dbo].[s4_printerLocation] OFF
END


INSERT INTO [dbo].[s4_settings] (setFieldID,setDataType,setValue) VALUES ('position/labelReport/id', 'System.Int32', 29);

GO

INSERT INTO [dbo].[s4_settings] (setFieldID,setDataType,setValue) VALUES ('dispatch/boxReport/id', 'System.Int32', 30);
GO

INSERT INTO [dbo].[s4_settings] (setFieldID,setDataType,setValue) VALUES ('deliverypackages/report/id', 'System.Int32', 39);
GO

UPDATE [dbo].[s4_reports]
  SET  [template] =  (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Stitky\BoxSmallPC43.rep', SINGLE_NCLOB ) as Document), [settings] = '{ "reportName": "Intermec - mal�" }'
 WHERE [reportID] = 10

UPDATE [dbo].[s4_reports]
  SET  [template] =  (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Stitky\BoxPC43.rep', SINGLE_NCLOB ) as Document), [settings] = '{ "reportName": "Intermec - velk�" }'
 WHERE [reportID] = 11
   
UPDATE [dbo].[s4_reports]
  SET  [template] =  (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Stitky\BoxBig2.rep', SINGLE_NCLOB ) as Document), [settings] = '{ "reportName": "Velk� 2" }'
 WHERE [reportID] = 13

UPDATE [dbo].[s4_reports]
  SET  [template] =  (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Stitky\BoxBig.rep', SINGLE_NCLOB ) as Document), [settings] = '{ "reportName": "Velk�" }'
 WHERE [reportID] = 14

UPDATE [dbo].[s4_reports]
  SET  [template] =  (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Stitky\BoxSmall.rep', SINGLE_NCLOB ) as Document), [settings] = '{ "reportName": "Mal�" }'
 WHERE [reportID] = 15

GO

INSERT INTO [dbo].[s4_settings] (setFieldID,setDataType,setValue) VALUES ('delivery/pplDeliveryReport/id', 'System.Int32', 36);

GO

DECLARE @reportID36 as int;
SET @reportID36 = 36;

DELETE FROM [s4_reports] WHERE [reportID] = @reportID36;
IF NOT EXISTS (SELECT 1 FROM [dbo].[s4_reports] WHERE (reportID = @reportID36))
BEGIN
	SET IDENTITY_INSERT [dbo].[s4_reports] ON
    
	INSERT [dbo].[s4_reports] ([reportID], [reportType], [template], [settings], [printerTypeID]) VALUES (@reportID36, N'deliveryLabel_PPL', 
(select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Stitky\DeliveryLabelPM43.rep', SINGLE_NCLOB ) as Document)
, NULL, 6)

	SET IDENTITY_INSERT [dbo].[s4_reports] OFF
	print 'insert ok reportID = '+ CAST(@reportID36 AS varchar(10));
END

GO

--report exchange - zbo�� na p��jmu (Naskladn�n� - hotovo), filtr v�robce nen� PPG
DECLARE @jsonVariable38 NVARCHAR(MAX)
SET @jsonVariable38 = N'{
"sql": "
DECLARE	@expirationDate	smalldatetime;
SET	@expirationDate = DATEADD(m, 12, GETDATE())

SELECT
		--Kod v�robku, n�zev v�robku, v�robce, �ar�e, expirace, datum, mno�stv�, doklad
		ar.articleCode,
		ar.articleDesc,
		man.manufName,
		CASE WHEN lot.batchNum = ''_NONE_'' THEN NULL ELSE lot.batchNum END as batchNum,
		CASE WHEN lot.expirationDate = ''01.01.1900'' THEN NULL ELSE lot.expirationDate END as expirationDate,
		mo.docDate,
		CASE mo.docType WHEN 5 THEN - moi.quantity ELSE moi.quantity END AS quantity,
		dir.docInfo
	FROM	dbo.s4_storeMove mo
			JOIN dbo.s4_storeMove_Items moi ON mo.stoMoveID = moi.stoMoveID
			JOIN dbo.s4_storeMove_lots lot ON moi.stoMoveLotID = lot.stoMoveLotID
			JOIN dbo.s4_vw_article_Base ar ON (lot.artPackID = ar.artPackID)
			JOIN dbo.s4_direction dir ON (mo.parent_directionID = dir.directionID)
			JOIN dbo.s4_manufactList man ON man.manufID = ar.manufID
	WHERE   dir.docDirection = 1 AND dir.docNumPrefix = ''PRIJ'' AND mo.docStatus = ''SREC''
			AND dir.docStatus = ''CSTI'' --Naskladn�n� - hotovo
			AND dir.docDate between DATEADD(DAY, CAST(@last AS int) * -1, GetDate()) AND getdate()
			AND (expirationDate <= @expirationDate AND expirationDate > ''1900-01-01 00:00:00'') 
			/*AND man.manufID != ''PPG''*/ AND moi.itemValidity = 100
	ORDER BY
		ar.articleCode,
		lot.batchNum,
		mo.docDate
	",
	"typeQuery": "DaysBack",
	"reportName": "Naskladn�n� karty s expirac� krat�� ne� 12 m�s�c� od "
}'

DECLARE @reportID as int;
set @reportID = 38;

IF NOT EXISTS (SELECT 1 FROM [dbo].[s4_reports] WHERE (reportID = @reportID))
BEGIN
	SET IDENTITY_INSERT [dbo].[s4_reports] ON
    INSERT INTO [dbo].[s4_reports] ([reportID], [reportType], [template], [settings], [printerTypeID]) VALUES (@reportID, 'Exchange', 'x', NULL, 1);
	SET IDENTITY_INSERT [dbo].[s4_reports] OFF
	print 'insert ok reportID = '+ CAST(@reportID AS varchar(10));
END


UPDATE [dbo].[s4_reports]
  SET   [template] =  (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Reporty\PrijemExpiraceARG.srt', SINGLE_NCLOB ) as Document), 
		[settings] =  @jsonVariable38
 WHERE [reportID] = @reportID
  
GO

--seznam bal��k� pro dopravce 
DECLARE @reportID as int;
set @reportID = 39;

IF NOT EXISTS (SELECT 1 FROM [dbo].[s4_reports] WHERE (reportID = @reportID))
BEGIN
	SET IDENTITY_INSERT [dbo].[s4_reports] ON
    INSERT INTO [dbo].[s4_reports] ([reportID], [reportType], [template], [settings], [printerTypeID]) VALUES (@reportID, 'WebPrint', 'x', NULL, 1);
	SET IDENTITY_INSERT [dbo].[s4_reports] OFF
	print 'insert ok reportID = '+ CAST(@reportID AS varchar(10));
END


UPDATE [dbo].[s4_reports]
  SET   [template] =  (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Reporty\DopravciSeznamBalicku.srt', SINGLE_NCLOB ) as Document)
 WHERE [reportID] = @reportID
  
GO
 
--report PohybyKeKartam
DECLARE @reportID as int;
set @reportID = 40;
DECLARE @jsonVariable NVARCHAR(MAX)
SET @jsonVariable = N'{
"sql": "
	DECLARE @count as int

	SELECT * into #temp FROM STRING_SPLIT(@articleCode, '';'');
	SELECT @count = (select COUNT(1) FROM #temp where value != '''');

	SELECT * into #temp2 FROM STRING_SPLIT(@manufaID, '','');
	SELECT * into #temp3 FROM STRING_SPLIT(@brand, '','');
	SELECT * into #temp4 FROM STRING_SPLIT(@sectID, '','');

	SELECT
	--Kod v�robku, n�zev v�robku, �ar�e, expirace, partner, datum, mno�stv�, doklad
	ar.articleCode, ar.articleDesc,
	CASE WHEN lot.batchNum = ''_NONE_'' THEN NULL ELSE lot.batchNum END as batchNum,
	lot.expirationDate,	mo.docDate,
	ABS(moi.quantity) AS quantity,
	dir.docInfo,
	pa.partnerName,
	paa.addrName + '' '' + paa.addrLine_1 + '' '' + paa.addrLine_2 + '' '' + paa.addrCity + '' '' + paa.addrPhone + '' '' + paa.addrRemark + '' '' + paa.deliveryInst as partnerAddress,
	ar.brand
FROM	dbo.s4_storeMove mo
		JOIN dbo.s4_storeMove_Items moi ON mo.stoMoveID = moi.stoMoveID
		JOIN dbo.s4_storeMove_lots lot ON moi.stoMoveLotID = lot.stoMoveLotID
		JOIN dbo.s4_vw_article_Base ar ON (lot.artPackID = ar.artPackID)
		JOIN dbo.s4_direction dir ON (mo.parent_directionID = dir.directionID)
		JOIN dbo.s4_partnerList pa ON (dir.partnerID = pa.partnerID)
		LEFT JOIN dbo.s4_partnerList_addresses paa ON (dir.partAddrID = paa.partAddrID)
		JOIN dbo.s4_manufactList man ON (man.manufID = ar.manufID)
WHERE	
	(
		(
			(mo.docType = 5) 
			AND (moi.itemDirection = -1) 
			AND (moi.itemValidity = 100)
			AND (mo.parent_directionID IN (SELECT directionID FROM dbo.s4_direction WHERE docNumPrefix <> ''DLL''))
		)
		OR 
		(
			(mo.docType = 3) 
			AND (moi.itemDirection = 1) 
			AND (moi.itemValidity = 100)
		)
	)
	AND 
	(
		(@count = 0 OR ar.articleCode IN (select value from #temp))
	)
	AND (@docDirection = 0 OR dir.docDirection = @docDirection)
	AND ((LEN(@manufaID) = 0 OR @manufaID IS NULL) OR ar.manufID IN (SELECT value FROM #temp2 WHERE RTRIM(value) <> '''') ) 
	AND ((LEN(@brand) = 0 OR @brand IS NULL) OR ar.brand IN (SELECT value FROM #temp3 WHERE RTRIM(value) <> '''') ) 
	AND (convert(varchar, mo.docDate, 121) BETWEEN @from AND DATEADD(DAY, 1, @to))
	AND ((LEN(@sectID) = 0 OR @sectID IS NULL) OR moi.positionID IN (SELECT positionID FROM dbo.s4_positionList_sections WHERE (sectID IN (SELECT value FROM #temp4 WHERE RTRIM(value) <> '''') ))) 
ORDER BY
	ar.articleCode,
	lot.batchNum,
	mo.docDate

	drop table #temp
	drop table #temp2
	drop table #temp3
	drop table #temp4
		",
"sql4Filters": [
	{
		"sql": "",
		"name" : "K�dy karet (odd�len� st�edn�kem)",
		"id" : "articleCode",
		"type" : "input",
		"width" : "850",
		"mandatory": "false"
	},
	{
		"sql": "select -1 as ID, ''vyskladn�n�'' as name UNION select 1 as ID, ''p��jem'' as name",
		"name" : "Pohyb karty",
		"id" : "docDirection",
		"type" : "sql",
		"width" : "150",
		"mandatory": "false"
	},
	{
		"sql" : "select manufID as ID, manufName AS name from [dbo].[s4_manufactList] where manufStatus = ''MAN_OK'' AND  manufid in (select b.manufID from s4_vw_article_base b join s4_vw_onStore_Summary_Valid v on b.artPackID = v.artPackID join s4_positionList pos on v.positionID = pos.positionID WHERE pos.posCateg = 0) order by name",
		"name" : "V�robce",
		"id" : "manufaID",
		"type" : "sqlMulti",
		"width" : "400",
		"mandatory": "false"
	},
	{
		"sql" : "select distinct(brand) as ID, brand as name from dbo.s4_vw_article_Base order by brand",
		"name" : "Zna�ka",
		"id" : "brand",
		"type" : "sqlMulti",
		"width" : "220",
		"mandatory": "false"
	},
	{
		"sql": "",
		"name" : "Datum od",
		"id" : "from",
		"type" : "date",
		"class" : "col-sm-2",
		"mandatory": "true"
	},
	{
		"sql": "",
		"name" : "Datum do",
		"id" : "to",
		"type" : "date",
		"class" : "col-sm-2",
		"mandatory": "true"
	},
	{
		"sql" : "select distinct(sectID) as ID, sectDesc as name from dbo.s4_sectionList order by sectID",
		"name" : "Sekce",
		"id" : "sectID",
		"type" : "sqlMulti",
		"width" : "150",
		"mandatory": "false"
	}
],

"typeQuery": "None",
"reportName": "Pohyby ke kart�m"
}'

IF NOT EXISTS (SELECT 1 FROM [dbo].[s4_reports] WHERE (reportID = @reportID))
BEGIN
	SET IDENTITY_INSERT [dbo].[s4_reports] ON
    INSERT INTO [dbo].[s4_reports] ([reportID], [reportType], [template], [settings], [printerTypeID]) VALUES (@reportID, 'WebPrint', 'x', NULL, 1);
	SET IDENTITY_INSERT [dbo].[s4_reports] OFF
	print 'insert ok reportID = '+ CAST(@reportID AS varchar(10));
END


UPDATE [dbo].[s4_reports]
  SET  [template] =  (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Reporty\PohybyKeKartam_ArgoMed.srt', SINGLE_NCLOB ) as Document),
	   [settings] =  @jsonVariable
 WHERE [reportID] = @reportID
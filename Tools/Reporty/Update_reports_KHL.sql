﻿DECLARE @reportID as int;
SET @reportID = 37; --zde upravit pro novy report WEB PRINT

IF NOT EXISTS (SELECT 1 FROM [dbo].[s4_reports] WHERE (reportID = @reportID))
BEGIN
	SET IDENTITY_INSERT [dbo].[s4_reports] ON
    INSERT INTO [dbo].[s4_reports] ([reportID], [reportType], [template], [settings], [printerTypeID]) VALUES (@reportID, 'Exchange', 'x', NULL, 1);
	SET IDENTITY_INSERT [dbo].[s4_reports] OFF
	print 'insert ok reportID = '+ CAST(@reportID AS varchar(10));
END

GO


 --report expirace
 DECLARE @jsonVariable3 NVARCHAR(MAX)
 SET @jsonVariable3 = N'{
"sql": "
	DECLARE	@expirationDate	smalldatetime;
	SET	@expirationDate = DATEADD(m, CAST(3 AS INT), GETDATE())

	SELECT	onp.positionID, art.articleCode, art.articleDesc, quantity, CASE WHEN batchNum = ''_NONE_'' THEN NULL ELSE batchNum END as batchNum, expirationDate, pos.posCode, m.source_id
	FROM	dbo.s4_vw_onStore_Summary_Valid onp
	JOIN dbo.s4_positionList pos ON (onp.positionID = pos.positionID)
	JOIN dbo.s4_vw_article_base art ON (onp.artPackID = art.artPackID)
	JOIN s4_manufactList m ON (m.manufID = art.manufID)
	WHERE (expirationDate <= @expirationDate AND expirationDate > ''1900-01-01 00:00:00'') AND (pos.posCateg = 0) 
	ORDER BY expirationDate, art.articleCode, pos.posCode
		",
	"typeQuery": "None",
	"reportName": "Naskladněné karty s expirací 3 měsíců a méně"
	}'

UPDATE [dbo].[s4_reports]
  SET  [template] = (select BulkColumn FROM OPENROWSET (BULK N'c:\Projects\store4\Tools\Reporty\Expirace2.srt', SINGLE_NCLOB ) as Document),
	   [settings] = @jsonVariable3
 WHERE [reportID] = 37
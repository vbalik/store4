use S4_KHL;
GO

INSERT INTO [S4_KHL].[dbo].[s4_sectionList] (sectID,sectDesc,source_id)
select sectID,sectDesc,source_id from [S4_PPG].[dbo].[s4_sectionList]
go

INSERT INTO [S4_KHL].[dbo].[s4_manufactList] (manufID,manufName,manufStatus,manufSettings,source_id,useBatch,useExpiration,sectID,manufSign)
SELECT manufID,manufName,manufStatus,manufSettings,source_id,useBatch,useExpiration,sectID,manufSign FROM [S4_PPG].[dbo].[s4_manufactList];
GO

INSERT INTO [S4_KHL].[dbo].[s4_workerList] (workerID,workerName,workerClass,workerStatus,workerLogon,workerSecData)
select workerID,workerName,workerClass,workerStatus,workerLogon,workerSecData from [S4_PPG].[dbo].[s4_workerList]
go

SET IDENTITY_INSERT [S4_KHL].[dbo].[s4_truckList] ON;
GO

INSERT INTO [S4_KHL].[dbo].[s4_truckList] (truckID,truckRegist,truckCapacity,truckComment,truckRegion)
select truckID,truckRegist,truckCapacity,truckComment,truckRegion from [S4_PPG].[dbo].[s4_truckList]
go

SET IDENTITY_INSERT [S4_KHL].[dbo].[s4_truckList] OFF;
GO

INSERT INTO [S4_KHL].[dbo].[s4_settings] (setFieldID,setDataType,setValue)
select setFieldID,setDataType,setValue from [S4_PPG].[dbo].[s4_settings];
go


SET IDENTITY_INSERT [S4_KHL].[dbo].[s4_printerType] ON;
GO

INSERT INTO [S4_KHL].[dbo].[s4_printerType] (printerTypeID,printerClass,printerDesc)
select printerTypeID,printerClass,printerDesc from [S4_PPG].[dbo].[s4_printerType];
go

SET IDENTITY_INSERT [S4_KHL].[dbo].[s4_printerType] OFF;
GO

SET IDENTITY_INSERT [S4_KHL].[dbo].[s4_reports] ON;
GO

INSERT INTO [S4_KHL].[dbo].[s4_reports] (reportID,reportType,template,settings,printerTypeID)
select reportID,reportType,template,settings,printerTypeID from [S4_PPG].[dbo].[s4_reports];
go

SET IDENTITY_INSERT [S4_KHL].[dbo].[s4_reports] OFF;
GO

SET IDENTITY_INSERT [S4_KHL].[dbo].[s4_printerLocation] ON;
GO

INSERT INTO [S4_KHL].[dbo].[s4_printerLocation] (printerLocationID,printerLocationDesc,printerPath,printerSettings,printerTypeID)
select printerLocationID,printerLocationDesc,printerPath,printerSettings,printerTypeID from [S4_PPG].[dbo].[s4_printerLocation];
go

SET IDENTITY_INSERT [S4_KHL].[dbo].[s4_printerLocation] OFF;
GO


insert into [S4_KHL].[dbo].[s4_prefixList] (docClass,docNumPrefix,prefixDesc,docDirection,docType,prefixEnabled,prefixSection)
values ('DI','DLNP','Dodac� List N�chod Prodej - dod�n� do l�k�rny N�chod', -1, 0, 1,'RO');

insert into [S4_KHL].[dbo].[s4_prefixList] (docClass,docNumPrefix,prefixDesc,docDirection,docType,prefixEnabled,prefixSection)
values ('DI','PLHJ','P�epravn� List Holding Ji��n � rozvoz na odd�len�', -1, 0, 1,'RO');

insert into [S4_KHL].[dbo].[s4_prefixList] (docClass,docNumPrefix,prefixDesc,docDirection,docType,prefixEnabled,prefixSection)
values ('DI','PLHN','P�epravn� List Holding N�chod � rozvoz na odd�len�', -1, 0, 1,'RO');

insert into [S4_KHL].[dbo].[s4_prefixList] (docClass,docNumPrefix,prefixDesc,docDirection,docType,prefixEnabled,prefixSection)
values ('DI','PLHR','P�epravn� List Holding Rychnov � rozvoz na odd�len�', -1, 0, 1,'RO');

insert into [S4_KHL].[dbo].[s4_prefixList] (docClass,docNumPrefix,prefixDesc,docDirection,docType,prefixEnabled,prefixSection)
values ('DI','PLHD','P�epravn� List Holding Dv�r � rozvoz na odd�len�', -1, 0, 1,'RO');

insert into [S4_KHL].[dbo].[s4_prefixList] (docClass,docNumPrefix,prefixDesc,docDirection,docType,prefixEnabled,prefixSection)
values ('DI','PLHT','P�epravn� List Holding Trutnov � rozvoz na odd�len�', -1, 0, 1,'RO');



SET IDENTITY_INSERT [S4_KHL].[dbo].[s4_partnerList] ON;
GO

INSERT INTO [S4_KHL].[dbo].[s4_partnerList] (partnerID,partnerName,source_id,partnerStatus,partnerDispCheck,partnerAddr_1,partnerAddr_2,partnerCity,partnerPostCode,partnerPhone)
select partnerID,partnerName,source_id,partnerStatus,partnerDispCheck,partnerAddr_1,partnerAddr_2,partnerCity,partnerPostCode,partnerPhone from [S4_PPG].[dbo].[s4_partnerList];
go

SET IDENTITY_INSERT [S4_KHL].[dbo].[s4_partnerList] OFF;
GO

SET IDENTITY_INSERT [S4_KHL].[dbo].[s4_partnerList_addresses] ON;
GO

INSERT INTO [S4_KHL].[dbo].[s4_partnerList_addresses] (partAddrID,partnerID,addrName,source_id,addrLine_1,addrLine_2,addrCity,addrPostCode,addrPhone,addrRemark,deliveryInst)
select partAddrID,partnerID,addrName,source_id,addrLine_1,addrLine_2,addrCity,addrPostCode,addrPhone,addrRemark,deliveryInst from [S4_PPG].[dbo].[s4_partnerList_addresses];
go

SET IDENTITY_INSERT [S4_KHL].[dbo].[s4_partnerList_addresses] OFF;
GO

INSERT INTO [S4_KHL].[dbo].[s4_houseList] (houseID,houseDesc,houseMap) 
select houseID,houseDesc,houseMap from [S4_PPG].[dbo].[s4_houseList];
go

SET IDENTITY_INSERT [S4_KHL].[dbo].[s4_positionList] ON;
GO

INSERT INTO [S4_KHL].[dbo].[s4_positionList] (positionID,posCode,posDesc,houseID,posCateg,posStatus,posHeight,posAttributes,posX,posY,posZ,mapPos)
select positionID,posCode,posDesc,houseID,posCateg,posStatus,posHeight,posAttributes,posX,posY,posZ,mapPos
from [S4_PPG].[dbo].[s4_positionList]
where [S4_PPG].[dbo].[s4_positionList].[posCode] in ('1E01A','1E01B','1E01C','1E01D','1E02A','1E02B','1E02C','1E02D','1E03A','1E03B','1E03C','1E03D','1E04A','1E04B','1E04C','1E04D','1E05A','1E05B','1E05C','1E05D','1E06A','1E06B','1E06C','1E06D','1E07A','1E07B','1E07C','1E07D','1E08A','1E08B','1E08C','1E08D','1E09A','1E09B','1E09C','1E09D')

INSERT INTO [S4_KHL].[dbo].[s4_positionList] (positionID,posCode,posDesc,houseID,posCateg,posStatus,posHeight,posAttributes,posX,posY,posZ,mapPos)
select positionID,posCode,posDesc,houseID,posCateg,posStatus,posHeight,posAttributes,posX,posY,posZ,mapPos
from [S4_PPG].[dbo].[s4_positionList]
where [S4_PPG].[dbo].[s4_positionList].[posCode] in ('1E10A','1E10B','1E10C','1E10D','1E11A','1E11B','1E11C','1E11D','1E12A','1E12B','1E12C','1E12D','1E13A','1E13B','1E13C','1E13D','1E14A','1E14B','1E14C','1E14D','1E15A','1E15B','1E15C','1E15D')

SET IDENTITY_INSERT [S4_KHL].[dbo].[s4_positionList] ON;
GO

INSERT INTO [dbo].[s4_positionList] (positionID,posCode,posDesc,houseID,posCateg,posStatus,posHeight,posAttributes,posX,posY,posZ,mapPos)
select positionID,posCode,posDesc,houseID,posCateg,posStatus,posHeight,posAttributes,posX,posY,posZ,mapPos
from [S4_PPG].[dbo].[s4_positionList]
where [S4_PPG].[dbo].[s4_positionList].[posCode] in ('1F01A','1F01B','1F01C','1F01D','1F02A','1F02B','1F02C','1F02D','1F03A','1F03B','1F03C','1F03D')

INSERT INTO [dbo].[s4_positionList] (positionID,posCode,posDesc,houseID,posCateg,posStatus,posHeight,posAttributes,posX,posY,posZ,mapPos)
select positionID,posCode,posDesc,houseID,posCateg,posStatus,posHeight,posAttributes,posX,posY,posZ,mapPos
from [S4_PPG].[dbo].[s4_positionList]
where [S4_PPG].[dbo].[s4_positionList].[posCode] in ('1F04A','1F04B','1F04C','1F04D','1F05A','1F05B','1F05C','1F05D','1F06A','1F06B','1F06C','1F06D','1F07A','1F07B','1F07C','1F07D','1F08A','1F08B','1F08C','1F08D','1F09A','1F09B','1F09C','1F09D')

SET IDENTITY_INSERT [S4_KHL].[dbo].[s4_positionList] OFF;
GO







SET IDENTITY_INSERT [S4_KHL].[dbo].[s4_positionList] OFF;
GO



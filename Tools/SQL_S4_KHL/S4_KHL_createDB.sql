/* user s4khl: password: Asd123Eefj_456 pozor nastavena role sysadmin jinak nebyly vidět tabulky uživatelské nevím proč */
USE [master]
GO
/****** Object:  Database [S4_KHL]    Script Date: 18.08.2020 12:34:48 ******/
/*
CREATE DATABASE [S4_KHL]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'S4_KHL', FILENAME = N'D:\Data\S4_KHL.mdf' , SIZE = 17506304KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
--( NAME = N'S4_KHL', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\S4_KHL.mdf' , SIZE = 17506304KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'S4_KHL_log', FILENAME = N'D:\Log\S4_KHL_log.ldf' , SIZE = 21635072KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
--( NAME = N'S4_KHL_log', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\S4_KHL_log.ldf' , SIZE = 21635072KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
*/
ALTER DATABASE [S4_KHL] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [S4_KHL].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [S4_KHL] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [S4_KHL] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [S4_KHL] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [S4_KHL] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [S4_KHL] SET ARITHABORT OFF 
GO
ALTER DATABASE [S4_KHL] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [S4_KHL] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [S4_KHL] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [S4_KHL] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [S4_KHL] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [S4_KHL] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [S4_KHL] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [S4_KHL] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [S4_KHL] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [S4_KHL] SET  DISABLE_BROKER 
GO
ALTER DATABASE [S4_KHL] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [S4_KHL] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [S4_KHL] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [S4_KHL] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [S4_KHL] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [S4_KHL] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [S4_KHL] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [S4_KHL] SET RECOVERY FULL 
GO
ALTER DATABASE [S4_KHL] SET  MULTI_USER 
GO
ALTER DATABASE [S4_KHL] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [S4_KHL] SET DB_CHAINING OFF 
GO
ALTER DATABASE [S4_KHL] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [S4_KHL] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [S4_KHL] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'S4_KHL', N'ON'
GO
ALTER DATABASE [S4_KHL] SET QUERY_STORE = OFF
GO
USE [S4_KHL]
GO

/****** Object:  Default [s4_def_emptyBatchNum]    Script Date: 18.08.2020 12:34:49 ******/
CREATE DEFAULT [dbo].[s4_def_emptyBatchNum] 
AS
'_NONE_';

GO
/****** Object:  Default [s4_def_emptyCarrier]    Script Date: 18.08.2020 12:34:49 ******/
CREATE DEFAULT [dbo].[s4_def_emptyCarrier] 
AS
'_NONE_';

GO
/****** Object:  Default [s4_def_emptyExpirationDate]    Script Date: 18.08.2020 12:34:49 ******/
CREATE DEFAULT [dbo].[s4_def_emptyExpirationDate] 
AS
'1900-01-01';

GO
/****** Object:  Default [s4_def_emptyPartner]    Script Date: 18.08.2020 12:34:49 ******/
CREATE DEFAULT [dbo].[s4_def_emptyPartner] 
AS
-1;

GO
/****** Object:  Default [s4_def_emptyPrice]    Script Date: 18.08.2020 12:34:49 ******/
CREATE DEFAULT [dbo].[s4_def_emptyPrice] 
AS
-1;

GO
/****** Object:  Default [s4_def_emptyUser]    Script Date: 18.08.2020 12:34:49 ******/
CREATE DEFAULT [dbo].[s4_def_emptyUser] 
AS
'?';

GO
/****** Object:  UserDefinedDataType [dbo].[s4_type_batchNum]    Script Date: 18.08.2020 12:34:49 ******/
CREATE TYPE [dbo].[s4_type_batchNum] FROM [varchar](32) NOT NULL
GO
/****** Object:  UserDefinedDataType [dbo].[s4_type_carrierNum]    Script Date: 18.08.2020 12:34:49 ******/
CREATE TYPE [dbo].[s4_type_carrierNum] FROM [varchar](32) NOT NULL
GO
/****** Object:  UserDefinedDataType [dbo].[s4_type_docClass]    Script Date: 18.08.2020 12:34:49 ******/
CREATE TYPE [dbo].[s4_type_docClass] FROM [varchar](32) NOT NULL
GO
/****** Object:  UserDefinedDataType [dbo].[s4_type_docHistoryID]    Script Date: 18.08.2020 12:34:49 ******/
CREATE TYPE [dbo].[s4_type_docHistoryID] FROM [bigint] NOT NULL
GO
/****** Object:  UserDefinedDataType [dbo].[s4_type_docItemID]    Script Date: 18.08.2020 12:34:49 ******/
CREATE TYPE [dbo].[s4_type_docItemID] FROM [int] NOT NULL
GO
/****** Object:  UserDefinedDataType [dbo].[s4_type_documentID]    Script Date: 18.08.2020 12:34:49 ******/
CREATE TYPE [dbo].[s4_type_documentID] FROM [int] NOT NULL
GO
/****** Object:  UserDefinedDataType [dbo].[s4_type_manufID]    Script Date: 18.08.2020 12:34:49 ******/
CREATE TYPE [dbo].[s4_type_manufID] FROM [varchar](16) NOT NULL
GO
/****** Object:  UserDefinedDataType [dbo].[s4_type_sectID]    Script Date: 18.08.2020 12:34:49 ******/
CREATE TYPE [dbo].[s4_type_sectID] FROM [varchar](16) NOT NULL
GO
/****** Object:  UserDefinedDataType [dbo].[s4_type_sourceID]    Script Date: 18.08.2020 12:34:49 ******/
CREATE TYPE [dbo].[s4_type_sourceID] FROM [varchar](32) NOT NULL
GO
/****** Object:  UserDefinedDataType [dbo].[s4_type_status]    Script Date: 18.08.2020 12:34:49 ******/
CREATE TYPE [dbo].[s4_type_status] FROM [varchar](16) NOT NULL
GO
/****** Object:  UserDefinedDataType [dbo].[s4_type_userID]    Script Date: 18.08.2020 12:34:49 ******/
CREATE TYPE [dbo].[s4_type_userID] FROM [varchar](16) NOT NULL
GO
/****** Object:  UserDefinedDataType [dbo].[s4_type_xtraCode]    Script Date: 18.08.2020 12:34:49 ******/
CREATE TYPE [dbo].[s4_type_xtraCode] FROM [varchar](16) NOT NULL
GO
/****** Object:  UserDefinedDataType [dbo].[s4_type_xtraID]    Script Date: 18.08.2020 12:34:49 ******/
CREATE TYPE [dbo].[s4_type_xtraID] FROM [int] NOT NULL
GO
/****** Object:  UserDefinedDataType [dbo].[s4_type_xtraValue]    Script Date: 18.08.2020 12:34:49 ******/
CREATE TYPE [dbo].[s4_type_xtraValue] FROM [nvarchar](max) NOT NULL
GO
/****** Object:  UserDefinedFunction [dbo].[s4_getOnSummaryErrorText]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[s4_getOnSummaryErrorText]
(
)
RETURNS nvarchar(256)
AS
BEGIN
	RETURN 'Store4: Can''t change items of s4_storeMove_items - thay are part of onStoreSummary'
END
GO
/****** Object:  Table [dbo].[s4_partnerList]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_partnerList](
	[partnerID] [int] IDENTITY(1,1) NOT NULL,
	[partnerName] [nvarchar](128) NOT NULL,
	[source_id] [dbo].[s4_type_sourceID] NOT NULL,
	[partnerStatus] [dbo].[s4_type_status] NOT NULL,
	[partnerDispCheck] [tinyint] NOT NULL,
	[partnerAddr_1] [varchar](48) NULL,
	[partnerAddr_2] [varchar](48) NULL,
	[partnerCity] [varchar](48) NULL,
	[partnerPostCode] [varchar](16) NULL,
	[partnerPhone] [varchar](48) NULL,
	[r_Edit] [timestamp] NOT NULL,
	[partnerInfo]  AS ([partnerName]+isnull(', '+[partnerCity],'')),
 CONSTRAINT [PK_s4_partnerList] PRIMARY KEY CLUSTERED 
(
	[partnerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s4_articleList]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_articleList](
	[articleID] [int] IDENTITY(1,1) NOT NULL,
	[articleCode] [nvarchar](64) NOT NULL,
	[articleDesc] [nvarchar](256) NULL,
	[articleStatus] [nvarchar](16) NOT NULL,
	[articleType] [tinyint] NOT NULL,
	[source_id] [dbo].[s4_type_sourceID] NOT NULL,
	[sectID] [dbo].[s4_type_sectID] NULL,
	[unitDesc] [nvarchar](32) NULL,
	[manufID] [dbo].[s4_type_manufID] NOT NULL,
	[useBatch] [bit] NOT NULL,
	[mixBatch] [bit] NOT NULL,
	[useExpiration] [bit] NOT NULL,
	[r_Edit] [timestamp] NOT NULL,
	[articleInfo]  AS (([articleCode]+' - ')+isnull([articleDesc],'')),
	[specFeatures] [int] NOT NULL,
	[userRemark] [nvarchar](max) NULL,
	[useSerialNumber] [bit] NOT NULL,
 CONSTRAINT [PK_s4_articleList] PRIMARY KEY CLUSTERED 
(
	[articleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s4_articleList_packings]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_articleList_packings](
	[artPackID] [int] IDENTITY(1,1) NOT NULL,
	[articleID] [int] NOT NULL,
	[packDesc] [nvarchar](32) NOT NULL,
	[packRelation] [numeric](9, 2) NOT NULL,
	[movablePack] [bit] NOT NULL,
	[source_id] [dbo].[s4_type_sourceID] NOT NULL,
	[packStatus] [nvarchar](16) NOT NULL,
	[barCodeType] [tinyint] NOT NULL,
	[barCode] [varchar](32) NULL,
	[inclCarrier] [bit] NOT NULL,
	[packWeight] [numeric](5, 2) NOT NULL,
	[packVolume] [numeric](5, 2) NOT NULL,
	[r_Edit] [timestamp] NOT NULL,
	[packInfo]  AS ((([packDesc]+' (')+CONVERT([varchar](16),[packRelation]))+')'),
 CONSTRAINT [PK_s4_articleList_packings] PRIMARY KEY CLUSTERED 
(
	[artPackID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[s4_vw_article_base]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[s4_vw_article_base]
	AS 
	SELECT
		ar.articleID, articleCode, articleDesc, articleStatus, articleType, ar.source_id, sectID, unitDesc, manufID, useBatch, mixBatch, useExpiration, articleInfo,
		artPackID, packDesc, packRelation, movablePack, packStatus, barCodeType, barCode, inclCarrier, packWeight, packVolume, packInfo, specFeatures, userRemark, useSerialNumber
	FROM 
		[dbo].[s4_articleList] ar
		JOIN [dbo].[s4_articleList_packings] ap ON (ar.articleID = ap.articleID)
GO
/****** Object:  Table [dbo].[s4_storeMove]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_storeMove](
	[stoMoveID] [dbo].[s4_type_documentID] IDENTITY(1,1) NOT NULL,
	[docStatus] [dbo].[s4_type_status] NOT NULL,
	[docNumPrefix] [varchar](16) NOT NULL,
	[docType] [tinyint] NOT NULL,
	[docNumber] [int] NOT NULL,
	[docDate] [datetime] NOT NULL,
	[parent_directionID] [dbo].[s4_type_documentID] NOT NULL,
	[docRemark] [nvarchar](max) NULL,
	[entryDateTime] [datetime] NOT NULL,
	[entryUserID] [dbo].[s4_type_userID] NOT NULL,
	[r_Edit] [timestamp] NOT NULL,
	[docDate_d]  AS (CONVERT([datetime],CONVERT([int],CONVERT([float],[docDate])))),
	[docDate_p]  AS (datepart(year,[docDate])*(100)+datepart(month,[docDate])),
	[docInfo]  AS ((rtrim([docNumPrefix])+'/')+CONVERT([varchar](16),[docNumber])),
	[docRemark_prev]  AS (CONVERT([varchar](64),[docRemark])+case when datalength([docRemark])>(64) then '...' else '' end),
 CONSTRAINT [PK_s4_storeMove] PRIMARY KEY CLUSTERED 
(
	[stoMoveID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s4_storeMove_items]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_storeMove_items](
	[stoMoveItemID] [dbo].[s4_type_docItemID] IDENTITY(1,1) NOT NULL,
	[stoMoveID] [dbo].[s4_type_documentID] NOT NULL,
	[docPosition] [int] NOT NULL,
	[itemValidity] [tinyint] NOT NULL,
	[itemDirection] [smallint] NOT NULL,
	[stoMoveLotID] [dbo].[s4_type_docItemID] NOT NULL,
	[positionID] [int] NOT NULL,
	[carrierNum] [dbo].[s4_type_carrierNum] NOT NULL,
	[quantity] [numeric](18, 4) NOT NULL,
	[parent_docPosition] [int] NOT NULL,
	[brotherID] [int] NOT NULL,
	[entryDateTime] [datetime] NOT NULL,
	[entryUserID] [dbo].[s4_type_userID] NOT NULL,
	[r_Edit] [timestamp] NOT NULL,
 CONSTRAINT [PK_s4_storeMove_items] PRIMARY KEY CLUSTERED 
(
	[stoMoveItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s4_storeMove_lots]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_storeMove_lots](
	[stoMoveLotID] [dbo].[s4_type_docItemID] IDENTITY(1,1) NOT NULL,
	[artPackID] [int] NOT NULL,
	[batchNum] [dbo].[s4_type_batchNum] NOT NULL,
	[expirationDate] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_s4_storeMove_lots] PRIMARY KEY CLUSTERED 
(
	[stoMoveLotID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s4_direction]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_direction](
	[directionID] [dbo].[s4_type_documentID] IDENTITY(1,1) NOT NULL,
	[docStatus] [dbo].[s4_type_status] NOT NULL,
	[docNumPrefix] [varchar](16) NOT NULL,
	[docDirection] [smallint] NOT NULL,
	[docYear] [char](2) NOT NULL,
	[docNumber] [int] NOT NULL,
	[docDate] [datetime] NOT NULL,
	[partnerID] [int] NOT NULL,
	[partAddrID] [int] NOT NULL,
	[prepare_directionID] [dbo].[s4_type_documentID] NOT NULL,
	[partnerRef] [nvarchar](max) NULL,
	[docRemark] [nvarchar](max) NULL,
	[entryDateTime] [datetime] NOT NULL,
	[entryUserID] [dbo].[s4_type_userID] NOT NULL,
	[docDate_d]  AS (CONVERT([datetime],CONVERT([int],CONVERT([float],[docDate])))),
	[docDate_p]  AS (datepart(year,[docDate])*(100)+datepart(month,[docDate])),
	[docInfo]  AS ((((rtrim([docNumPrefix])+'/')+[docYear])+'/')+CONVERT([varchar](16),[docNumber])),
	[docRemark_prev]  AS (CONVERT([varchar](64),[docRemark])+case when datalength([docRemark])>(64) then '...' else '' end),
	[invoiceID] [int] NULL,
	[r_Edit] [timestamp] NOT NULL,
 CONSTRAINT [PK_s4_direction] PRIMARY KEY CLUSTERED 
(
	[directionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [dbo].[prom_vw_moveReportBase2]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[prom_vw_moveReportBase2]
AS

SELECT
	dir.docDate_p,
	ar.source_id AS art_source_id,
	pa.source_id AS partner_source_id,
	lot.batchNum,
	CASE mo.docType WHEN 5 THEN 1 WHEN 3 THEN -1 END AS moveSign,
	SUM(quantity) AS quantity
FROM	dbo.s4_storeMove mo
		JOIN dbo.s4_storeMove_Items moi ON mo.stoMoveID = moi.stoMoveID
		JOIN dbo.s4_storeMove_lots lot ON moi.stoMoveLotID = lot.stoMoveLotID
		JOIN dbo.s4_vw_article_Base ar ON (lot.artPackID = ar.artPackID)
		JOIN dbo.s4_direction dir ON (mo.parent_directionID = dir.directionID)
		JOIN dbo.s4_partnerList pa ON (dir.partnerID = pa.[partnerID])
WHERE	
	(
		(
			(mo.docType = 5) 
			AND (moi.itemDirection = -1) 
			AND (moi.itemValidity = 100)
			--AND (mo.docStatus != 'CANC')
			AND (mo.parent_directionID IN (SELECT directionID FROM dbo.s4_direction WHERE docNumPrefix <> 'DLL'))
		)
		OR 
		(
			(mo.docType = 3) 
			AND (moi.itemDirection = 1) 
			AND (moi.itemValidity = 100)
			--AND (mo.docStatus != 'CANC') 
			AND (mo.parent_directionID IN (SELECT directionID FROM dbo.s4_direction WHERE docNumPrefix = 'VRAT'))
		)
	)
GROUP BY
	dir.docDate_p,
	ar.source_id,
	pa.source_id,
	lot.batchNum,
	CASE mo.docType WHEN 5 THEN 1 WHEN 3 THEN -1 END

GO
/****** Object:  Table [dbo].[s4_positionList]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_positionList](
	[positionID] [int] IDENTITY(1,1) NOT NULL,
	[posCode] [varchar](16) NOT NULL,
	[posDesc] [nvarchar](128) NULL,
	[houseID] [varchar](16) NOT NULL,
	[posCateg] [tinyint] NOT NULL,
	[posStatus] [varchar](16) NOT NULL,
	[posHeight] [tinyint] NOT NULL,
	[posAttributes] [int] NOT NULL,
	[posX] [int] NOT NULL,
	[posY] [int] NOT NULL,
	[posZ] [int] NOT NULL,
	[mapPos] [varchar](32) NULL,
	[r_Edit] [timestamp] NOT NULL,
 CONSTRAINT [PK_s4_positionList] PRIMARY KEY CLUSTERED 
(
	[positionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_s4_positionList_posXYZ] UNIQUE NONCLUSTERED 
(
	[posX] ASC,
	[posY] ASC,
	[posZ] ASC,
	[houseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s4_onStoreSummary]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_onStoreSummary](
	[onStoreSummID] [dbo].[s4_type_docItemID] IDENTITY(1,1) NOT NULL,
	[onStoreSummStatus] [tinyint] NOT NULL,
	[lastStoMoveItemID] [dbo].[s4_type_docItemID] NOT NULL,
	[entryDateTime] [datetime] NOT NULL,
	[r_Edit] [timestamp] NOT NULL,
 CONSTRAINT [PK_s4_onStoreSummary] PRIMARY KEY CLUSTERED 
(
	[onStoreSummID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s4_onStoreSummary_items]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_onStoreSummary_items](
	[onStoreSummItemID] [dbo].[s4_type_docItemID] IDENTITY(1,1) NOT NULL,
	[onStoreSummID] [dbo].[s4_type_docItemID] NOT NULL,
	[stoMoveLotID] [dbo].[s4_type_docItemID] NOT NULL,
	[positionID] [int] NOT NULL,
	[carrierNum] [dbo].[s4_type_carrierNum] NOT NULL,
	[quantity] [numeric](18, 4) NOT NULL,
	[r_Edit] [timestamp] NOT NULL,
 CONSTRAINT [PK_s4_onStoreSummary_item] PRIMARY KEY CLUSTERED 
(
	[onStoreSummItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[s4_vw_onStore_Summary_Valid]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[s4_vw_onStore_Summary_Valid]
AS 

WITH onStore ([stoMoveLotID], [positionID], [carrierNum], [quantity])
AS 
(
	SELECT
		[stoMoveLotID], [positionID], [carrierNum], SUM([quantity]) AS [quantity]
	FROM
		(
			SELECT
				[stoMoveLotID], 
				[positionID], 
				[carrierNum], 
				[quantity]
			FROM	[dbo].[s4_onStoreSummary_items]
			WHERE	[onStoreSummID] = (SELECT MAX([onStoreSummID]) FROM [dbo].[s4_onStoreSummary] WHERE [onStoreSummStatus] = 100)

			UNION ALL
		
			SELECT
				[stoMoveLotID],
				[positionID],
				[carrierNum],
				SUM([quantity] * [itemDirection]) AS [quantity]
			FROM	[dbo].[s4_storeMove_items]
			WHERE	[itemValidity] = 100
			AND		[stoMoveItemID] > ISNULL((SELECT MAX([lastStoMoveItemID]) FROM [dbo].[s4_onStoreSummary] WHERE [onStoreSummStatus] = 100), 0)
			GROUP BY
				[stoMoveLotID],
				[positionID],
				[carrierNum]
			HAVING
				SUM([quantity] * [itemDirection]) <> 0
		) un
	GROUP BY
		[stoMoveLotID],
		[positionID],
		[carrierNum]
	HAVING 
		SUM([quantity]) <> 0
)

SELECT
	onStore.positionID,
	onStore.carrierNum,
	onStore.quantity,
	lots.stoMoveLotID,
	lots.artPackID,
	lots.batchNum,
	lots.expirationDate
FROM	onStore
		JOIN [dbo].[s4_storeMove_lots] lots ON (onStore.stoMoveLotID = lots.stoMoveLotID)
GO
/****** Object:  UserDefinedFunction [dbo].[prom_fn_posUseReportBase]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   FUNCTION [dbo].[prom_fn_posUseReportBase]
(	
	@from AS date,
	@to AS date
)  
RETURNS TABLE  
    RETURN 

	SELECT
			pos.positionID, pos.posCode, pos.posDesc, pos.houseID, pos.posCateg, pos.posStatus, pos.posHeight, pos.posAttributes, pos.posX, pos.posY, pos.posZ, pos.mapPos,
			ISNULL(movesCnt.cnt, 0) AS movesCnt,
			ISNULL(onStore.quantity, 0) AS onStore
	FROM	dbo.s4_positionList pos
			LEFT JOIN 
				(
				SELECT
						smi.positionID,
						COUNT(*) AS cnt
				FROM	dbo.s4_storeMove sm JOIN dbo.s4_storeMove_items smi ON (sm.stoMoveID = smi.stoMoveID)
				WHERE	(smi.itemValidity = 100)
						AND (sm.docDate_d BETWEEN @from AND @to)
				GROUP BY
						smi.positionID
				) movesCnt ON (pos.positionID = movesCnt.positionID)
			LEFT JOIN 
				(
				SELECT	
						positionID,
						SUM(quantity) AS quantity
				FROM	dbo.s4_vw_onStore_Summary_Valid
				GROUP BY
						positionID
				) onStore ON (pos.positionID = onStore.positionID)
	WHERE
			pos.posCateg = 0
GO
/****** Object:  View [dbo].[s4_vw_onStore_NotValid]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[s4_vw_onStore_NotValid]
AS 
SELECT
	onStore.positionID,
	onStore.carrierNum,
	onStore.quantity,
	lots.stoMoveLotID,
	lots.artPackID,
	lots.batchNum,
	lots.expirationDate
FROM
(
	SELECT
		[stoMoveLotID],
		[positionID],
		[carrierNum],
		SUM([quantity] * [itemDirection]) AS [quantity]
	FROM	[dbo].[s4_storeMove_items]
	WHERE	([itemDirection] = 1 AND [itemValidity] = 100) OR ([itemDirection] = -1 AND [itemValidity] IN (0, 100))
	GROUP BY
		[stoMoveLotID],
		[positionID],
		[carrierNum]
	HAVING
		SUM([quantity] * [itemDirection]) <> 0
) onStore
JOIN [dbo].[s4_storeMove_lots] lots ON (onStore.stoMoveLotID = lots.stoMoveLotID)
GO
/****** Object:  View [dbo].[s4_vw_onStore_Reservation]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[s4_vw_onStore_Reservation]
AS 
SELECT
	onStore.positionID,
	onStore.carrierNum,
	onStore.quantity,
	lots.stoMoveLotID,
	lots.artPackID,
	lots.batchNum,
	lots.expirationDate
FROM
(
	SELECT
		[stoMoveLotID],
		[positionID],
		[carrierNum],
		SUM([quantity] * [itemDirection]) AS [quantity]
	FROM	[dbo].[s4_storeMove_items]
	WHERE	[itemValidity] = 0
	GROUP BY
		[stoMoveLotID],
		[positionID],
		[carrierNum]
	HAVING
		SUM([quantity] * [itemDirection]) <> 0
) onStore
JOIN [dbo].[s4_storeMove_lots] lots ON (onStore.stoMoveLotID = lots.stoMoveLotID)
GO
/****** Object:  View [dbo].[s4_vw_onStore_Valid]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[s4_vw_onStore_Valid]
AS 
SELECT
	onStore.positionID,
	onStore.carrierNum,
	onStore.quantity,
	lots.stoMoveLotID,
	lots.artPackID,
	lots.batchNum,
	lots.expirationDate
FROM
(
	SELECT
		[stoMoveLotID],
		[positionID],
		[carrierNum],
		SUM([quantity] * [itemDirection]) AS [quantity]
	FROM	[dbo].[s4_storeMove_items]
	WHERE	[itemValidity] = 100
	GROUP BY
		[stoMoveLotID],
		[positionID],
		[carrierNum]
	HAVING
		SUM([quantity] * [itemDirection]) <> 0
) onStore
JOIN [dbo].[s4_storeMove_lots] lots ON (onStore.stoMoveLotID = lots.stoMoveLotID)
GO
/****** Object:  Table [dbo].[s4_stocktaking]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_stocktaking](
	[stotakID] [dbo].[s4_type_documentID] IDENTITY(1,1) NOT NULL,
	[stotakStatus] [dbo].[s4_type_status] NOT NULL,
	[stotakDesc] [nvarchar](max) NOT NULL,
	[beginDate] [datetime] NOT NULL,
	[endDate] [datetime] NULL,
	[entryDateTime] [datetime] NOT NULL,
	[entryUserID] [dbo].[s4_type_userID] NOT NULL,
	[r_Edit] [timestamp] NOT NULL,
 CONSTRAINT [PK_s4_stocktaking] PRIMARY KEY CLUSTERED 
(
	[stotakID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s4_stocktaking_items]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_stocktaking_items](
	[stotakItemID] [dbo].[s4_type_docItemID] IDENTITY(1,1) NOT NULL,
	[stotakID] [dbo].[s4_type_documentID] NOT NULL,
	[positionID] [int] NOT NULL,
	[artPackID] [int] NOT NULL,
	[quantity] [numeric](18, 4) NOT NULL,
	[carrierNum] [dbo].[s4_type_carrierNum] NULL,
	[batchNum] [dbo].[s4_type_batchNum] NULL,
	[expirationDate] [smalldatetime] NULL,
	[entryDateTime] [datetime] NOT NULL,
	[entryUserID] [dbo].[s4_type_userID] NOT NULL,
	[r_Edit] [timestamp] NOT NULL,
 CONSTRAINT [PK_s4_stocktaking_items] PRIMARY KEY CLUSTERED 
(
	[stotakItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s4_stocktaking_positions]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_stocktaking_positions](
	[stotakPosID] [dbo].[s4_type_docItemID] IDENTITY(1,1) NOT NULL,
	[stotakID] [dbo].[s4_type_documentID] NOT NULL,
	[positionID] [int] NOT NULL,
	[checkCounter] [int] NOT NULL,
	[r_Edit] [timestamp] NOT NULL,
 CONSTRAINT [PK_s4_stocktaking_positions] PRIMARY KEY CLUSTERED 
(
	[stotakPosID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_s4_stocktaking_positions] UNIQUE NONCLUSTERED 
(
	[stotakID] ASC,
	[positionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[s4_vw_stockTaking]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[s4_vw_stockTaking]
AS
SELECT        dbo.s4_stocktaking.*,
items = (SELECT COUNT(stotakItemID) FROM s4_stocktaking_items WHERE stotakID = s4_stocktaking.stotakID),
checked = (SELECT COUNT(stotakPosID) FROM s4_stocktaking_positions WHERE stotakID = s4_stocktaking.stotakID AND checkCounter > 0),
nonChecked = (SELECT COUNT(stotakPosID) FROM s4_stocktaking_positions WHERE stotakID = s4_stocktaking.stotakID AND checkCounter = 0)
FROM            dbo.s4_stocktaking
GO
/****** Object:  Table [dbo].[s4_stocktaking_history]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_stocktaking_history](
	[stotakHistID] [dbo].[s4_type_docHistoryID] IDENTITY(1,1) NOT NULL,
	[stotakID] [dbo].[s4_type_documentID] NOT NULL,
	[eventCode] [dbo].[s4_type_status] NOT NULL,
	[positionID] [int] NULL,
	[artPackID] [int] NULL,
	[carrierNum] [dbo].[s4_type_carrierNum] NULL,
	[quantity] [numeric](18, 4) NULL,
	[batchNum] [dbo].[s4_type_batchNum] NULL,
	[expirationDate] [smalldatetime] NULL,
	[entryDateTime] [datetime] NOT NULL,
	[entryUserID] [dbo].[s4_type_userID] NOT NULL,
	[r_Edit] [timestamp] NOT NULL,
 CONSTRAINT [PK_s4_stocktaking_history] PRIMARY KEY CLUSTERED 
(
	[stotakHistID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[s4_vw_stockTakingHistory]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[s4_vw_stockTakingHistory]
AS
SELECT H.*, P.posCode, A.articleID, A.articleCode, A.articleDesc FROM s4_stocktaking_history H
LEFT OUTER JOIN s4_positionList P ON P.positionID = H.positionID
LEFT OUTER JOIN s4_vw_article_base A ON A.artPackID = H.artPackID
GO
/****** Object:  View [dbo].[s4_vw_stockTakingItem]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[s4_vw_stockTakingItem]
AS
SELECT I.*, POS.posCode, A.articleCode, A.articleDesc, A.articleID FROM s4_stocktaking_items I
LEFT OUTER JOIN s4_positionList POS ON POS.positionID = I.positionID
LEFT OUTER JOIN s4_articleList_packings P ON P.artPackID = I.artPackID
LEFT OUTER JOIN s4_articleList A ON A.articleID = P.articleID
GO
/****** Object:  View [dbo].[s4_vw_stockTakingPosition]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[s4_vw_stockTakingPosition]
AS
SELECT SP.[stotakPosID]
      ,SP.[stotakID]
      ,SP.[positionID]
      ,SP.[checkCounter]
      ,SP.[r_Edit]
	  ,P.[posCode]
	  ,P.[houseID]
  FROM [dbo].[s4_stocktaking_positions] SP
  LEFT OUTER JOIN [dbo].[s4_positionList] P ON P.positionID = SP.positionID
GO
/****** Object:  Table [dbo].[s4_stocktaking_snapshots]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_stocktaking_snapshots](
	[stotakSnapID] [dbo].[s4_type_docItemID] IDENTITY(1,1) NOT NULL,
	[stotakID] [dbo].[s4_type_documentID] NOT NULL,
	[snapshotClass] [tinyint] NOT NULL,
	[positionID] [int] NOT NULL,
	[artPackID] [int] NOT NULL,
	[carrierNum] [dbo].[s4_type_carrierNum] NULL,
	[quantity] [numeric](18, 4) NOT NULL,
	[batchNum] [dbo].[s4_type_batchNum] NULL,
	[expirationDate] [smalldatetime] NULL,
	[unitPrice] [money] NULL,
	[r_Edit] [timestamp] NOT NULL,
 CONSTRAINT [PK_s4_stocktaking_snapshots] PRIMARY KEY CLUSTERED 
(
	[stotakSnapID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[s4_vw_stockTakingSnapshot]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[s4_vw_stockTakingSnapshot]
AS
SELECT S.*, POS.posCode, A.articleCode, A.articleDesc FROM s4_stocktaking_snapshots S
LEFT OUTER JOIN s4_positionList POS ON POS.positionID = S.positionID
LEFT OUTER JOIN s4_articleList_packings P ON P.artPackID = S.artPackID
LEFT OUTER JOIN s4_articleList A ON A.articleID = P.articleID
GO
/****** Object:  Table [dbo].[s4_articleList_history]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_articleList_history](
	[articleHistID] [dbo].[s4_type_docHistoryID] IDENTITY(1,1) NOT NULL,
	[articleID] [int] NOT NULL,
	[artPackID] [int] NOT NULL,
	[eventCode] [dbo].[s4_type_status] NOT NULL,
	[eventData] [nvarchar](max) NULL,
	[entryDateTime] [datetime] NOT NULL,
	[entryUserID] [dbo].[s4_type_userID] NOT NULL,
	[r_Edit] [timestamp] NOT NULL,
 CONSTRAINT [PK_s4_articleList_history] PRIMARY KEY CLUSTERED 
(
	[articleHistID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s4_clientError]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_clientError](
	[clientErrorID] [int] IDENTITY(1,1) NOT NULL,
	[appVersion] [nvarchar](64) NOT NULL,
	[errorClass] [nvarchar](128) NOT NULL,
	[errorDateTime] [datetime] NOT NULL,
	[errorMessage] [nvarchar](max) NOT NULL,
	[path] [nvarchar](256) NOT NULL,
	[statusData] [nvarchar](max) NULL,
	[requestBody] [nvarchar](max) NULL,
	[r_Edit] [timestamp] NOT NULL,
 CONSTRAINT [PK_s4_clientError] PRIMARY KEY CLUSTERED 
(
	[clientErrorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s4_deliveryDirection]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_deliveryDirection](
	[deliveryDirectionID] [int] IDENTITY(1,1) NOT NULL,
	[directionID] [int] NOT NULL,
	[deliveryProvider] [int] NOT NULL,
	[createShipmentAttempt] [smallint] NULL,
	[createShipmentError] [text] NULL,
	[shipmentRefNumber] [nvarchar](128) NULL,
	[deliveryManifestID] [int] NULL,
	[deliveryCreated] [datetime] NULL,
	[deliveryChanged] [datetime] NULL,
	[r_Edit] [timestamp] NOT NULL,
 CONSTRAINT [PK_S4_deliveryDirection] PRIMARY KEY CLUSTERED 
(
	[deliveryDirectionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s4_deliveryDirection_items]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_deliveryDirection_items](
	[deliveryDirectionID] [int] NOT NULL,
	[parcelPosition] [int] NOT NULL,
	[parcelRefNumber] [nvarchar](128) NULL,
	[r_Edit] [timestamp] NOT NULL,
 CONSTRAINT [PK_s4_deliveryDirection_items] PRIMARY KEY CLUSTERED 
(
	[deliveryDirectionID] ASC,
	[parcelPosition] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s4_deliveryManifest]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_deliveryManifest](
	[deliveryManifestID] [int] IDENTITY(1,1) NOT NULL,
	[manifestStatus] [dbo].[s4_type_status] NOT NULL,
	[deliveryProvider] [int] NOT NULL,
	[createManifestAttempt] [smallint] NULL,
	[createManifestError] [text] NULL,
	[manifestRefNumber] [nvarchar](128) NULL,
	[manifestCreated] [datetime] NULL,
	[manifestChanged] [datetime] NULL,
	[r_Edit] [timestamp] NOT NULL,
 CONSTRAINT [PK_s4_deliveryManifest] PRIMARY KEY CLUSTERED 
(
	[deliveryManifestID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s4_direction_assign]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_direction_assign](
	[directAssignID] [int] IDENTITY(1,1) NOT NULL,
	[directionID] [dbo].[s4_type_documentID] NOT NULL,
	[jobID] [char](4) NOT NULL,
	[workerID] [dbo].[s4_type_userID] NOT NULL,
	[assignParams] [varchar](max) NULL,
	[entryDateTime] [datetime] NOT NULL,
	[entryUserID] [dbo].[s4_type_userID] NOT NULL,
	[r_Edit] [timestamp] NOT NULL,
 CONSTRAINT [PK_s4_direction_assign] PRIMARY KEY CLUSTERED 
(
	[directAssignID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s4_direction_history]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_direction_history](
	[directionHistID] [dbo].[s4_type_docHistoryID] IDENTITY(1,1) NOT NULL,
	[directionID] [dbo].[s4_type_documentID] NOT NULL,
	[docPosition] [int] NOT NULL,
	[eventCode] [dbo].[s4_type_status] NOT NULL,
	[eventData] [nvarchar](max) NULL,
	[entryDateTime] [datetime] NOT NULL,
	[entryUserID] [dbo].[s4_type_userID] NOT NULL,
	[r_Edit] [timestamp] NOT NULL,
 CONSTRAINT [PK_s4_direction_history] PRIMARY KEY CLUSTERED 
(
	[directionHistID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s4_direction_items]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_direction_items](
	[directionItemID] [dbo].[s4_type_docItemID] IDENTITY(1,1) NOT NULL,
	[directionID] [dbo].[s4_type_documentID] NOT NULL,
	[docPosition] [int] NOT NULL,
	[itemStatus] [dbo].[s4_type_status] NOT NULL,
	[artPackID] [int] NOT NULL,
	[articleCode] [varchar](64) NOT NULL,
	[unitDesc] [nvarchar](32) NULL,
	[quantity] [numeric](18, 4) NOT NULL,
	[unitPrice] [money] NOT NULL,
	[partnerDepart] [varchar](max) NULL,
	[entryDateTime] [datetime] NOT NULL,
	[entryUserID] [dbo].[s4_type_userID] NOT NULL,
	[r_Edit] [timestamp] NOT NULL,
	[price]  AS ([unitPrice]*[quantity]),
 CONSTRAINT [PK_s4_direction_items] PRIMARY KEY CLUSTERED 
(
	[directionItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s4_direction_xtra]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_direction_xtra](
	[directionExID] [dbo].[s4_type_xtraID] IDENTITY(1,1) NOT NULL,
	[directionID] [dbo].[s4_type_documentID] NOT NULL,
	[xtraCode] [dbo].[s4_type_xtraCode] NOT NULL,
	[docPosition] [int] NOT NULL,
	[xtraValue] [dbo].[s4_type_xtraValue] NOT NULL,
	[r_Edit] [timestamp] NOT NULL,
 CONSTRAINT [PK_s4_direction_xtra] PRIMARY KEY CLUSTERED 
(
	[directionExID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s4_houseList]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_houseList](
	[houseID] [varchar](16) NOT NULL,
	[houseDesc] [nvarchar](max) NOT NULL,
	[houseMap] [image] NULL,
	[r_Edit] [timestamp] NOT NULL,
 CONSTRAINT [PK_s4_houseList] PRIMARY KEY CLUSTERED 
(
	[houseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s4_internMessages]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_internMessages](
	[intMessID] [int] IDENTITY(1,1) NOT NULL,
	[msgClass] [dbo].[s4_type_docClass] NOT NULL,
	[msgSeverity] [tinyint] NOT NULL,
	[msgHeader] [nvarchar](max) NOT NULL,
	[msgText] [nvarchar](max) NULL,
	[msgDateTime] [datetime] NOT NULL,
	[msgUserID] [varchar](16) NULL,
	[r_Edit] [timestamp] NOT NULL,
 CONSTRAINT [PK_s4_internMessages] PRIMARY KEY CLUSTERED 
(
	[intMessID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s4_invoice]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_invoice](
	[invoiceID] [int] IDENTITY(1,1) NOT NULL,
	[docNumber] [int] NOT NULL,
	[docPrefix] [varchar](16) NOT NULL,
	[docYear] [char](2) NOT NULL,
	[lastDateTime] [datetime] NOT NULL,
	[entryDateTime] [datetime] NOT NULL,
	[r_Edit] [timestamp] NOT NULL,
 CONSTRAINT [PK_s4_invoice] PRIMARY KEY CLUSTERED 
(
	[invoiceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s4_invoice_xtra]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_invoice_xtra](
	[invoiceExID] [int] IDENTITY(1,1) NOT NULL,
	[invoiceID] [int] NOT NULL,
	[xtraCode] [dbo].[s4_type_xtraCode] NOT NULL,
	[xtraValue] [dbo].[s4_type_xtraValue] NOT NULL,
	[r_Edit] [timestamp] NOT NULL,
 CONSTRAINT [PK_s4_invoice_xtra] PRIMARY KEY CLUSTERED 
(
	[invoiceExID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s4_manufactList]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_manufactList](
	[manufID] [dbo].[s4_type_manufID] NOT NULL,
	[manufName] [nvarchar](64) NOT NULL,
	[manufStatus] [dbo].[s4_type_status] NOT NULL,
	[manufSettings] [varchar](max) NULL,
	[source_id] [dbo].[s4_type_sourceID] NULL,
	[useBatch] [bit] NOT NULL,
	[useExpiration] [bit] NOT NULL,
	[sectID] [dbo].[s4_type_sectID] NULL,
	[r_Edit] [timestamp] NOT NULL,
	[manufSign] [nvarchar](32) NULL,
 CONSTRAINT [PK_s4_manufactList] PRIMARY KEY CLUSTERED 
(
	[manufID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s4_messageBus]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_messageBus](
	[messageBusID] [int] IDENTITY(1,1) NOT NULL,
	[messageStatus] [varchar](32) NOT NULL,
	[messageType] [int] NOT NULL,
	[messageData] [nvarchar](max) NULL,
	[tryCount] [int] NOT NULL,
	[nextTryTime] [datetime] NULL,
	[errorDesc] [nvarchar](max) NULL,
	[lastDateTime] [datetime] NULL,
	[entryDateTime] [datetime] NOT NULL,
	[directionID] [dbo].[s4_type_documentID] NULL,
	[r_Edit] [timestamp] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[messageBusID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s4_messageBus_document]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_messageBus_document](
	[messageBusDocumentID] [int] IDENTITY(1,1) NOT NULL,
	[messageBusID] [int] NOT NULL,
	[documentName] [varchar](512) NOT NULL,
	[mimeType] [varchar](256) NOT NULL,
	[document] [varbinary](max) NOT NULL,
	[entryDateTime] [datetime] NOT NULL,
	[r_Edit] [timestamp] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[messageBusDocumentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s4_partnerList_addresses]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_partnerList_addresses](
	[partAddrID] [int] IDENTITY(1,1) NOT NULL,
	[partnerID] [int] NOT NULL,
	[addrName] [nvarchar](256) NOT NULL,
	[source_id] [dbo].[s4_type_sourceID] NOT NULL,
	[addrLine_1] [nvarchar](128) NULL,
	[addrLine_2] [nvarchar](128) NULL,
	[addrCity] [nvarchar](128) NULL,
	[addrPostCode] [nvarchar](32) NULL,
	[addrPhone] [nvarchar](128) NULL,
	[addrRemark] [nvarchar](max) NULL,
	[deliveryInst] [nvarchar](max) NULL,
	[r_Edit] [timestamp] NOT NULL,
	[addrInfo]  AS ([addrName]+isnull(', '+[addrCity],'')),
 CONSTRAINT [PK_s4_partnerList_addresses] PRIMARY KEY CLUSTERED 
(
	[partAddrID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s4_partnerList_history]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_partnerList_history](
	[partnerHistID] [dbo].[s4_type_docHistoryID] IDENTITY(1,1) NOT NULL,
	[partnerID] [int] NOT NULL,
	[partAddrID] [int] NOT NULL,
	[eventCode] [dbo].[s4_type_status] NOT NULL,
	[eventData] [nvarchar](max) NULL,
	[entryDateTime] [datetime] NOT NULL,
	[entryUserID] [dbo].[s4_type_userID] NOT NULL,
	[r_Edit] [timestamp] NOT NULL,
 CONSTRAINT [PK_s4_partnerList_history] PRIMARY KEY CLUSTERED 
(
	[partnerHistID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s4_positionList_history]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_positionList_history](
	[positionHistID] [dbo].[s4_type_docHistoryID] IDENTITY(1,1) NOT NULL,
	[positionID] [int] NOT NULL,
	[eventCode] [dbo].[s4_type_status] NOT NULL,
	[eventData] [nvarchar](max) NULL,
	[entryDateTime] [datetime] NOT NULL,
	[entryUserID] [dbo].[s4_type_userID] NOT NULL,
	[r_Edit] [timestamp] NOT NULL,
 CONSTRAINT [PK_s4_positionList_history] PRIMARY KEY CLUSTERED 
(
	[positionHistID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s4_positionList_sections]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_positionList_sections](
	[posSectID] [int] IDENTITY(1,1) NOT NULL,
	[sectID] [dbo].[s4_type_sectID] NOT NULL,
	[positionID] [int] NOT NULL,
	[r_Edit] [timestamp] NOT NULL,
 CONSTRAINT [PK_s4_positionList_sections] PRIMARY KEY CLUSTERED 
(
	[posSectID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UN_s4_positionList_sections_sectID_positionID] UNIQUE NONCLUSTERED 
(
	[sectID] ASC,
	[positionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s4_prefixList]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_prefixList](
	[docClass] [dbo].[s4_type_docClass] NOT NULL,
	[docNumPrefix] [char](16) NOT NULL,
	[prefixDesc] [varchar](64) NOT NULL,
	[docDirection] [smallint] NOT NULL,
	[docType] [tinyint] NOT NULL,
	[prefixEnabled] [bit] NOT NULL,
	[prefixSection] [dbo].[s4_type_sectID] NULL,
	[prefixSettings] [varchar](max) NULL,
	[r_Edit] [timestamp] NOT NULL,
 CONSTRAINT [PK_s4_prefixList] PRIMARY KEY CLUSTERED 
(
	[docClass] ASC,
	[docNumPrefix] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_s4_prefixList_docNumPrefix] UNIQUE NONCLUSTERED 
(
	[docNumPrefix] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s4_printerLocation]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_printerLocation](
	[printerLocationID] [int] IDENTITY(1,1) NOT NULL,
	[printerLocationDesc] [varchar](128) NULL,
	[printerPath] [varchar](256) NOT NULL,
	[printerSettings] [varchar](max) NULL,
	[printerTypeID] [int] NOT NULL,
	[r_Edit] [timestamp] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[printerLocationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s4_printerType]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_printerType](
	[printerTypeID] [int] IDENTITY(1,1) NOT NULL,
	[printerClass] [tinyint] NOT NULL,
	[printerDesc] [nvarchar](max) NOT NULL,
	[r_Edit] [timestamp] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[printerTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s4_reports]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_reports](
	[reportID] [int] IDENTITY(1,1) NOT NULL,
	[reportType] [varchar](32) NOT NULL,
	[template] [nvarchar](max) NOT NULL,
	[settings] [nvarchar](max) NULL,
	[printerTypeID] [int] NOT NULL,
	[r_Edit] [timestamp] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[reportID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s4_sectionList]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_sectionList](
	[sectID] [dbo].[s4_type_sectID] NOT NULL,
	[sectDesc] [nvarchar](64) NOT NULL,
	[source_id] [varchar](48) NULL,
	[r_Edit] [timestamp] NOT NULL,
 CONSTRAINT [PK_s4_sectionList] PRIMARY KEY CLUSTERED 
(
	[sectID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s4_settings]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_settings](
	[setFieldID] [varchar](32) NOT NULL,
	[setDataType] [varchar](64) NOT NULL,
	[setValue] [nvarchar](max) NOT NULL,
	[r_Edit] [timestamp] NOT NULL,
 CONSTRAINT [PK_s4_settings] PRIMARY KEY CLUSTERED 
(
	[setFieldID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s4_storeMove_history]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_storeMove_history](
	[stoMoveHistID] [dbo].[s4_type_docHistoryID] IDENTITY(1,1) NOT NULL,
	[stoMoveID] [dbo].[s4_type_documentID] NOT NULL,
	[docPosition] [int] NOT NULL,
	[eventCode] [dbo].[s4_type_status] NOT NULL,
	[eventData] [nvarchar](max) NULL,
	[entryDateTime] [datetime] NOT NULL,
	[entryUserID] [dbo].[s4_type_userID] NOT NULL,
	[r_Edit] [timestamp] NOT NULL,
 CONSTRAINT [PK_s4_storeMove_history] PRIMARY KEY CLUSTERED 
(
	[stoMoveHistID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s4_storeMove_serials]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_storeMove_serials](
	[serialID] [int] IDENTITY(1,1) NOT NULL,
	[stoMoveItemID] [dbo].[s4_type_docItemID] NOT NULL,
	[serialNumber] [nvarchar](64) NOT NULL,
	[r_Edit] [timestamp] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[serialID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s4_storeMove_xtra]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_storeMove_xtra](
	[stoMoveExID] [dbo].[s4_type_xtraID] IDENTITY(1,1) NOT NULL,
	[stoMoveID] [dbo].[s4_type_documentID] NOT NULL,
	[xtraCode] [dbo].[s4_type_xtraCode] NOT NULL,
	[xtraValue] [dbo].[s4_type_xtraValue] NOT NULL,
	[r_Edit] [timestamp] NOT NULL,
 CONSTRAINT [PK_s4_storeMove_xtra] PRIMARY KEY CLUSTERED 
(
	[stoMoveExID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s4_truckList]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_truckList](
	[truckID] [int] IDENTITY(1,1) NOT NULL,
	[truckRegist] [varchar](32) NOT NULL,
	[truckCapacity] [decimal](18, 0) NOT NULL,
	[truckComment] [nvarchar](max) NULL,
	[truckRegion] [tinyint] NOT NULL,
	[r_Edit] [timestamp] NOT NULL,
 CONSTRAINT [PK_s4_truckList] PRIMARY KEY CLUSTERED 
(
	[truckID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s4_workerList]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_workerList](
	[workerID] [dbo].[s4_type_userID] NOT NULL,
	[workerName] [nvarchar](256) NOT NULL,
	[workerClass] [int] NOT NULL,
	[workerStatus] [dbo].[s4_type_status] NOT NULL,
	[workerLogon] [varchar](32) NOT NULL,
	[workerSecData] [varchar](256) NULL,
	[r_Edit] [timestamp] NOT NULL,
 CONSTRAINT [PK_s4_workerList] PRIMARY KEY CLUSTERED 
(
	[workerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UN_s4_workerList_workerLogon] UNIQUE NONCLUSTERED 
(
	[workerLogon] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s4_workerList_history]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_workerList_history](
	[workerHistID] [dbo].[s4_type_docHistoryID] IDENTITY(1,1) NOT NULL,
	[workerID] [dbo].[s4_type_userID] NOT NULL,
	[eventCode] [dbo].[s4_type_status] NOT NULL,
	[eventData] [nvarchar](max) NULL,
	[entryDateTime] [datetime] NOT NULL,
	[entryUserID] [dbo].[s4_type_userID] NOT NULL,
	[r_Edit] [timestamp] NOT NULL,
 CONSTRAINT [PK_s4_workerList_history] PRIMARY KEY CLUSTERED 
(
	[workerHistID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s4_workerList_setting]    Script Date: 18.08.2020 12:34:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s4_workerList_setting](
	[workerID] [varchar](16) NOT NULL,
	[settingCode] [varchar](16) NOT NULL,
	[settingValue] [text] NULL,
 CONSTRAINT [PK_s4_workerList_setting] PRIMARY KEY CLUSTERED 
(
	[workerID] ASC,
	[settingCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_s4_articleList_articleCode]    Script Date: 18.08.2020 12:34:49 ******/
CREATE NONCLUSTERED INDEX [IX_s4_articleList_articleCode] ON [dbo].[s4_articleList]
(
	[articleCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_s4_articleList_articleDesc]    Script Date: 18.08.2020 12:34:49 ******/
CREATE NONCLUSTERED INDEX [IX_s4_articleList_articleDesc] ON [dbo].[s4_articleList]
(
	[articleDesc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_s4_articleList_articleStatus]    Script Date: 18.08.2020 12:34:49 ******/
CREATE NONCLUSTERED INDEX [IX_s4_articleList_articleStatus] ON [dbo].[s4_articleList]
(
	[articleStatus] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_s4_articleList_manufID]    Script Date: 18.08.2020 12:34:49 ******/
CREATE NONCLUSTERED INDEX [IX_s4_articleList_manufID] ON [dbo].[s4_articleList]
(
	[manufID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_s4_articleList_source_id]    Script Date: 18.08.2020 12:34:49 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_s4_articleList_source_id] ON [dbo].[s4_articleList]
(
	[source_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO
/****** Object:  Index [IX_s4_articleList_history_articleID]    Script Date: 18.08.2020 12:34:49 ******/
CREATE NONCLUSTERED INDEX [IX_s4_articleList_history_articleID] ON [dbo].[s4_articleList_history]
(
	[articleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_s4_articleList_packings]    Script Date: 18.08.2020 12:34:49 ******/
CREATE NONCLUSTERED INDEX [IX_s4_articleList_packings] ON [dbo].[s4_articleList_packings]
(
	[articleID] ASC,
	[packDesc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_s4_articleList_packings_source_id]    Script Date: 18.08.2020 12:34:49 ******/
CREATE NONCLUSTERED INDEX [IX_s4_articleList_packings_source_id] ON [dbo].[s4_articleList_packings]
(
	[source_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO
/****** Object:  Index [IX_s4_clientError_clientErrorID]    Script Date: 18.08.2020 12:34:49 ******/
CREATE NONCLUSTERED INDEX [IX_s4_clientError_clientErrorID] ON [dbo].[s4_clientError]
(
	[clientErrorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO
/****** Object:  Index [IX_s4_direction_docDirection]    Script Date: 18.08.2020 12:34:49 ******/
CREATE NONCLUSTERED INDEX [IX_s4_direction_docDirection] ON [dbo].[s4_direction]
(
	[docDirection] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_s4_direction_docNumPrefix_docYear_docNumber]    Script Date: 18.08.2020 12:34:49 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_s4_direction_docNumPrefix_docYear_docNumber] ON [dbo].[s4_direction]
(
	[docNumPrefix] ASC,
	[docYear] ASC,
	[docNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_s4_direction_docStatus]    Script Date: 18.08.2020 12:34:49 ******/
CREATE NONCLUSTERED INDEX [IX_s4_direction_docStatus] ON [dbo].[s4_direction]
(
	[docStatus] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_s4_direction_docStatus_docNumPrefix]    Script Date: 18.08.2020 12:34:49 ******/
CREATE NONCLUSTERED INDEX [IX_s4_direction_docStatus_docNumPrefix] ON [dbo].[s4_direction]
(
	[docStatus] ASC,
	[docNumPrefix] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_s4_direction_partnerID]    Script Date: 18.08.2020 12:34:49 ******/
CREATE NONCLUSTERED INDEX [IX_s4_direction_partnerID] ON [dbo].[s4_direction]
(
	[partnerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_s4_direction_assign_directionID]    Script Date: 18.08.2020 12:34:49 ******/
CREATE NONCLUSTERED INDEX [IX_s4_direction_assign_directionID] ON [dbo].[s4_direction_assign]
(
	[directionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_s4_direction_assign_directionID_jobID_workerID]    Script Date: 18.08.2020 12:34:49 ******/
CREATE NONCLUSTERED INDEX [IX_s4_direction_assign_directionID_jobID_workerID] ON [dbo].[s4_direction_assign]
(
	[directionID] ASC,
	[jobID] ASC,
	[workerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_s4_direction_history_position]    Script Date: 18.08.2020 12:34:49 ******/
CREATE NONCLUSTERED INDEX [IX_s4_direction_history_position] ON [dbo].[s4_direction_history]
(
	[directionID] ASC,
	[docPosition] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO
/****** Object:  Index [IX_s4_direction_items]    Script Date: 18.08.2020 12:34:49 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_s4_direction_items] ON [dbo].[s4_direction_items]
(
	[directionID] ASC,
	[docPosition] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_s4_direction_items_directionID]    Script Date: 18.08.2020 12:34:49 ******/
CREATE NONCLUSTERED INDEX [IX_s4_direction_items_directionID] ON [dbo].[s4_direction_items]
(
	[directionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_s4_direction_xtra_directionID_xtraCode]    Script Date: 18.08.2020 12:34:49 ******/
CREATE NONCLUSTERED INDEX [IX_s4_direction_xtra_directionID_xtraCode] ON [dbo].[s4_direction_xtra]
(
	[directionID] ASC,
	[xtraCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_s4_direction_xtra_directionID_xtraCode_docPosition]    Script Date: 18.08.2020 12:34:49 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_s4_direction_xtra_directionID_xtraCode_docPosition] ON [dbo].[s4_direction_xtra]
(
	[directionID] ASC,
	[xtraCode] ASC,
	[docPosition] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_s4_onStoreSummary_items_onStore1]    Script Date: 18.08.2020 12:34:49 ******/
CREATE NONCLUSTERED INDEX [IX_s4_onStoreSummary_items_onStore1] ON [dbo].[s4_onStoreSummary_items]
(
	[onStoreSummID] ASC,
	[stoMoveLotID] ASC,
	[positionID] ASC,
	[carrierNum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_s4_onStoreSummary_items_onStoreSummID]    Script Date: 18.08.2020 12:34:49 ******/
CREATE NONCLUSTERED INDEX [IX_s4_onStoreSummary_items_onStoreSummID] ON [dbo].[s4_onStoreSummary_items]
(
	[onStoreSummID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_s4_partnerList_source_id]    Script Date: 18.08.2020 12:34:49 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_s4_partnerList_source_id] ON [dbo].[s4_partnerList]
(
	[source_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_s4_partnerList_addresses_partnerID]    Script Date: 18.08.2020 12:34:49 ******/
CREATE NONCLUSTERED INDEX [IX_s4_partnerList_addresses_partnerID] ON [dbo].[s4_partnerList_addresses]
(
	[partnerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_s4_partnerList_addresses_source_id]    Script Date: 18.08.2020 12:34:49 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_s4_partnerList_addresses_source_id] ON [dbo].[s4_partnerList_addresses]
(
	[source_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_s4_partnerList_history_partnerID]    Script Date: 18.08.2020 12:34:49 ******/
CREATE NONCLUSTERED INDEX [IX_s4_partnerList_history_partnerID] ON [dbo].[s4_partnerList_history]
(
	[partnerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO
/****** Object:  Index [IX_s4_positionList_posCateg]    Script Date: 18.08.2020 12:34:49 ******/
CREATE NONCLUSTERED INDEX [IX_s4_positionList_posCateg] ON [dbo].[s4_positionList]
(
	[posCateg] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_s4_positionList_posCode]    Script Date: 18.08.2020 12:34:49 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_s4_positionList_posCode] ON [dbo].[s4_positionList]
(
	[posCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_s4_positionList_history_positionID]    Script Date: 18.08.2020 12:34:49 ******/
CREATE NONCLUSTERED INDEX [IX_s4_positionList_history_positionID] ON [dbo].[s4_positionList_history]
(
	[positionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_s4_positionList_sections_sectID]    Script Date: 18.08.2020 12:34:49 ******/
CREATE NONCLUSTERED INDEX [IX_s4_positionList_sections_sectID] ON [dbo].[s4_positionList_sections]
(
	[sectID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_s4_storeMove_docNumPrefix_docNumber]    Script Date: 18.08.2020 12:34:49 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_s4_storeMove_docNumPrefix_docNumber] ON [dbo].[s4_storeMove]
(
	[docNumPrefix] ASC,
	[docNumber] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_s4_storeMove_docType]    Script Date: 18.08.2020 12:34:49 ******/
CREATE NONCLUSTERED INDEX [IX_s4_storeMove_docType] ON [dbo].[s4_storeMove]
(
	[docType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_s4_storeMove_docType_docNumber]    Script Date: 18.08.2020 12:34:49 ******/
CREATE NONCLUSTERED INDEX [IX_s4_storeMove_docType_docNumber] ON [dbo].[s4_storeMove]
(
	[docType] ASC,
	[docNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_s4_storeMove_parent_directionID]    Script Date: 18.08.2020 12:34:49 ******/
CREATE NONCLUSTERED INDEX [IX_s4_storeMove_parent_directionID] ON [dbo].[s4_storeMove]
(
	[parent_directionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_s4_storeMove_history_position]    Script Date: 18.08.2020 12:34:49 ******/
CREATE NONCLUSTERED INDEX [IX_s4_storeMove_history_position] ON [dbo].[s4_storeMove_history]
(
	[stoMoveID] ASC,
	[docPosition] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_s4_storeMove_items_position]    Script Date: 18.08.2020 12:34:49 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_s4_storeMove_items_position] ON [dbo].[s4_storeMove_items]
(
	[stoMoveID] ASC,
	[docPosition] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_s4_storeMove_items_positionID]    Script Date: 18.08.2020 12:34:49 ******/
CREATE NONCLUSTERED INDEX [IX_s4_storeMove_items_positionID] ON [dbo].[s4_storeMove_items]
(
	[stoMoveItemID] ASC,
	[positionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_s4_storeMove_items_stoMoveLotID]    Script Date: 18.08.2020 12:34:49 ******/
CREATE NONCLUSTERED INDEX [IX_s4_storeMove_items_stoMoveLotID] ON [dbo].[s4_storeMove_items]
(
	[stoMoveLotID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [s4_storeMove_items_onPos1]    Script Date: 18.08.2020 12:34:49 ******/
CREATE NONCLUSTERED INDEX [s4_storeMove_items_onPos1] ON [dbo].[s4_storeMove_items]
(
	[positionID] ASC,
	[stoMoveLotID] ASC,
	[carrierNum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [s4_storeMove_items_onPos2]    Script Date: 18.08.2020 12:34:49 ******/
CREATE NONCLUSTERED INDEX [s4_storeMove_items_onPos2] ON [dbo].[s4_storeMove_items]
(
	[itemValidity] ASC,
	[positionID] ASC,
	[stoMoveLotID] ASC,
	[carrierNum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_s4_storeMove_lots_artPackID_batchNum_expirationDate]    Script Date: 18.08.2020 12:34:49 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_s4_storeMove_lots_artPackID_batchNum_expirationDate] ON [dbo].[s4_storeMove_lots]
(
	[artPackID] ASC,
	[batchNum] ASC,
	[expirationDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_s4_storeMove_serials_stoMoveItemID]    Script Date: 18.08.2020 12:34:49 ******/
CREATE NONCLUSTERED INDEX [IX_s4_storeMove_serials_stoMoveItemID] ON [dbo].[s4_storeMove_serials]
(
	[stoMoveItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_s4_storeMove_xtra_stoMoveID_xtraCode]    Script Date: 18.08.2020 12:34:49 ******/
CREATE NONCLUSTERED INDEX [IX_s4_storeMove_xtra_stoMoveID_xtraCode] ON [dbo].[s4_storeMove_xtra]
(
	[stoMoveID] ASC,
	[xtraCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_s4_truckList_truckRegist]    Script Date: 18.08.2020 12:34:49 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_s4_truckList_truckRegist] ON [dbo].[s4_truckList]
(
	[truckRegist] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_s4_workerList_history_workerID]    Script Date: 18.08.2020 12:34:49 ******/
CREATE NONCLUSTERED INDEX [IX_s4_workerList_history_workerID] ON [dbo].[s4_workerList_history]
(
	[workerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_s4_workerList_history_workerID_eventCode]    Script Date: 18.08.2020 12:34:49 ******/
CREATE NONCLUSTERED INDEX [IX_s4_workerList_history_workerID_eventCode] ON [dbo].[s4_workerList_history]
(
	[workerID] ASC,
	[eventCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[s4_articleList] ADD  CONSTRAINT [DF_s4_articleList_articleType]  DEFAULT ((0)) FOR [articleType]
GO
ALTER TABLE [dbo].[s4_articleList] ADD  CONSTRAINT [DF_s4_articleList_useBatch]  DEFAULT ((0)) FOR [useBatch]
GO
ALTER TABLE [dbo].[s4_articleList] ADD  CONSTRAINT [DF_s4_articleList_mixBatch]  DEFAULT ((0)) FOR [mixBatch]
GO
ALTER TABLE [dbo].[s4_articleList] ADD  CONSTRAINT [DF_s4_articleList_useExpiration]  DEFAULT ((0)) FOR [useExpiration]
GO
ALTER TABLE [dbo].[s4_articleList] ADD  DEFAULT ((0)) FOR [specFeatures]
GO
ALTER TABLE [dbo].[s4_articleList] ADD  CONSTRAINT [DF_s4_articleList_useSerialNumber]  DEFAULT ((0)) FOR [useSerialNumber]
GO
ALTER TABLE [dbo].[s4_articleList_history] ADD  CONSTRAINT [DF_s4_articleList_history_artPackID]  DEFAULT ((-1)) FOR [artPackID]
GO
ALTER TABLE [dbo].[s4_articleList_history] ADD  CONSTRAINT [DF_s4_articleList_history_entryDateTime]  DEFAULT (getdate()) FOR [entryDateTime]
GO
ALTER TABLE [dbo].[s4_articleList_packings] ADD  CONSTRAINT [DF_s4_articleList_packings_basicPacking]  DEFAULT ((0)) FOR [movablePack]
GO
ALTER TABLE [dbo].[s4_articleList_packings] ADD  CONSTRAINT [DF_s4_articleList_packings_packStatus]  DEFAULT ((0)) FOR [packStatus]
GO
ALTER TABLE [dbo].[s4_articleList_packings] ADD  CONSTRAINT [DF_s4_articleList_packings_barCodeType]  DEFAULT ((0)) FOR [barCodeType]
GO
ALTER TABLE [dbo].[s4_articleList_packings] ADD  CONSTRAINT [DF_s4_articleList_packings_inclCarrier]  DEFAULT ((0)) FOR [inclCarrier]
GO
ALTER TABLE [dbo].[s4_articleList_packings] ADD  CONSTRAINT [DF_s4_articleList_packings_packWeight]  DEFAULT ((0)) FOR [packWeight]
GO
ALTER TABLE [dbo].[s4_articleList_packings] ADD  CONSTRAINT [DF_s4_articleList_packings_packVolume]  DEFAULT ((0)) FOR [packVolume]
GO
ALTER TABLE [dbo].[s4_direction] ADD  CONSTRAINT [DF_s4_direction_parent_directionID]  DEFAULT ((-1)) FOR [prepare_directionID]
GO
ALTER TABLE [dbo].[s4_direction] ADD  CONSTRAINT [DF_s4_direction_entryDateTime]  DEFAULT (getdate()) FOR [entryDateTime]
GO
ALTER TABLE [dbo].[s4_direction_assign] ADD  CONSTRAINT [DF_s4_direction_assign_entryDateTime]  DEFAULT (getdate()) FOR [entryDateTime]
GO
ALTER TABLE [dbo].[s4_direction_history] ADD  CONSTRAINT [DF_s4_direction_history_entryDateTime]  DEFAULT (getdate()) FOR [entryDateTime]
GO
ALTER TABLE [dbo].[s4_direction_items] ADD  CONSTRAINT [DF_s4_direction_items_entryDateTime]  DEFAULT (getdate()) FOR [entryDateTime]
GO
ALTER TABLE [dbo].[s4_direction_xtra] ADD  CONSTRAINT [DF_s4_direction_xtra_docPosition]  DEFAULT ((0)) FOR [docPosition]
GO
ALTER TABLE [dbo].[s4_internMessages] ADD  CONSTRAINT [DF_s4_internMessages_msgClass]  DEFAULT ('?') FOR [msgClass]
GO
ALTER TABLE [dbo].[s4_internMessages] ADD  CONSTRAINT [DF_s4_internMessages_msgSeverity]  DEFAULT ((0)) FOR [msgSeverity]
GO
ALTER TABLE [dbo].[s4_internMessages] ADD  CONSTRAINT [DF_s4_internMessages_msgDateTime]  DEFAULT (getdate()) FOR [msgDateTime]
GO
ALTER TABLE [dbo].[s4_invoice] ADD  CONSTRAINT [DF_s4_invoice_lastDateTime]  DEFAULT (getdate()) FOR [lastDateTime]
GO
ALTER TABLE [dbo].[s4_invoice] ADD  CONSTRAINT [DF_s4_invoice_entryDateTime]  DEFAULT (getdate()) FOR [entryDateTime]
GO
ALTER TABLE [dbo].[s4_manufactList] ADD  CONSTRAINT [DF_s4_manufactList_manufStatus]  DEFAULT ((0)) FOR [manufStatus]
GO
ALTER TABLE [dbo].[s4_manufactList] ADD  CONSTRAINT [DF_s4_manufactList_useBatch]  DEFAULT ((0)) FOR [useBatch]
GO
ALTER TABLE [dbo].[s4_manufactList] ADD  CONSTRAINT [DF_s4_manufactList_useExpiration]  DEFAULT ((0)) FOR [useExpiration]
GO
ALTER TABLE [dbo].[s4_onStoreSummary] ADD  DEFAULT ((0)) FOR [onStoreSummStatus]
GO
ALTER TABLE [dbo].[s4_onStoreSummary] ADD  CONSTRAINT [DF_s4_onStoreSummary_entryDateTime]  DEFAULT (getdate()) FOR [entryDateTime]
GO
ALTER TABLE [dbo].[s4_partnerList] ADD  CONSTRAINT [DF_s4_partnerList_partnerStatus1]  DEFAULT ((0)) FOR [partnerDispCheck]
GO
ALTER TABLE [dbo].[s4_partnerList_history] ADD  CONSTRAINT [DF_s4_partnerList_history_partAddrID]  DEFAULT ((-1)) FOR [partAddrID]
GO
ALTER TABLE [dbo].[s4_partnerList_history] ADD  CONSTRAINT [DF_s4_partnerList_history_entryDateTime]  DEFAULT (getdate()) FOR [entryDateTime]
GO
ALTER TABLE [dbo].[s4_positionList] ADD  CONSTRAINT [DF_s4_positionList_posCateg]  DEFAULT ((0)) FOR [posCateg]
GO
ALTER TABLE [dbo].[s4_positionList] ADD  CONSTRAINT [DF_s4_positionList_posStatus]  DEFAULT ((0)) FOR [posStatus]
GO
ALTER TABLE [dbo].[s4_positionList] ADD  CONSTRAINT [DF_s4_positionList_posHeight]  DEFAULT ((0)) FOR [posHeight]
GO
ALTER TABLE [dbo].[s4_positionList] ADD  CONSTRAINT [DF_s4_positionList_posAttributes]  DEFAULT ((0)) FOR [posAttributes]
GO
ALTER TABLE [dbo].[s4_positionList] ADD  CONSTRAINT [DF_s4_positionList_posX]  DEFAULT ((0)) FOR [posX]
GO
ALTER TABLE [dbo].[s4_positionList] ADD  CONSTRAINT [DF_s4_positionList_posY]  DEFAULT ((0)) FOR [posY]
GO
ALTER TABLE [dbo].[s4_positionList] ADD  CONSTRAINT [DF_s4_positionList_posZ]  DEFAULT ((0)) FOR [posZ]
GO
ALTER TABLE [dbo].[s4_positionList_history] ADD  CONSTRAINT [DF_s4_positionList_history_entryDateTime]  DEFAULT (getdate()) FOR [entryDateTime]
GO
ALTER TABLE [dbo].[s4_prefixList] ADD  CONSTRAINT [DF_s4_prefixList_docDirection]  DEFAULT ((0)) FOR [docDirection]
GO
ALTER TABLE [dbo].[s4_prefixList] ADD  CONSTRAINT [DF_s4_prefixList_docType]  DEFAULT ((0)) FOR [docType]
GO
ALTER TABLE [dbo].[s4_prefixList] ADD  CONSTRAINT [DF_s4_prefixList_prefixEnabled]  DEFAULT ((1)) FOR [prefixEnabled]
GO
ALTER TABLE [dbo].[s4_printerType] ADD  DEFAULT ((0)) FOR [printerClass]
GO
ALTER TABLE [dbo].[s4_stocktaking] ADD  CONSTRAINT [DF_s4_stocktaking_entryDateTime]  DEFAULT (getdate()) FOR [entryDateTime]
GO
ALTER TABLE [dbo].[s4_stocktaking_history] ADD  CONSTRAINT [DF_s4_stocktaking_history_entryDateTime]  DEFAULT (getdate()) FOR [entryDateTime]
GO
ALTER TABLE [dbo].[s4_stocktaking_items] ADD  CONSTRAINT [DF_s4_stocktaking_items_entryDateTime]  DEFAULT (getdate()) FOR [entryDateTime]
GO
ALTER TABLE [dbo].[s4_stocktaking_positions] ADD  CONSTRAINT [DF_s4_stocktaking_positions_checkCounter]  DEFAULT ((0)) FOR [checkCounter]
GO
ALTER TABLE [dbo].[s4_stocktaking_snapshots] ADD  DEFAULT ((0)) FOR [snapshotClass]
GO
ALTER TABLE [dbo].[s4_storeMove] ADD  CONSTRAINT [DF_s4_storeMove_parent_stoDocID]  DEFAULT ((-1)) FOR [parent_directionID]
GO
ALTER TABLE [dbo].[s4_storeMove] ADD  CONSTRAINT [DF_s4_storeMove_entryDateTime]  DEFAULT (getdate()) FOR [entryDateTime]
GO
ALTER TABLE [dbo].[s4_storeMove_history] ADD  CONSTRAINT [DF_s4_storeMove_history_entryDateTime]  DEFAULT (getdate()) FOR [entryDateTime]
GO
ALTER TABLE [dbo].[s4_storeMove_items] ADD  DEFAULT ((0)) FOR [itemValidity]
GO
ALTER TABLE [dbo].[s4_storeMove_items] ADD  CONSTRAINT [DF_s4_storeMove_items_parent_docPosition]  DEFAULT ((-1)) FOR [parent_docPosition]
GO
ALTER TABLE [dbo].[s4_storeMove_items] ADD  CONSTRAINT [DF_s4_storeMove_items_parent_docPosition1]  DEFAULT ((-1)) FOR [brotherID]
GO
ALTER TABLE [dbo].[s4_storeMove_items] ADD  CONSTRAINT [DF_s4_storeMove_items_entryDateTime]  DEFAULT (getdate()) FOR [entryDateTime]
GO
ALTER TABLE [dbo].[s4_truckList] ADD  CONSTRAINT [DF_s4_truckList_truckCapacity]  DEFAULT ((-1)) FOR [truckCapacity]
GO
ALTER TABLE [dbo].[s4_truckList] ADD  DEFAULT ((0)) FOR [truckRegion]
GO
ALTER TABLE [dbo].[s4_workerList] ADD  CONSTRAINT [DF_s4_workerList_workerClass]  DEFAULT ((0)) FOR [workerClass]
GO
ALTER TABLE [dbo].[s4_workerList_history] ADD  CONSTRAINT [DF_s4_workerList_history_entryDateTime]  DEFAULT (getdate()) FOR [entryDateTime]
GO
ALTER TABLE [dbo].[s4_articleList]  WITH CHECK ADD  CONSTRAINT [FK_s4_articleList_s4_manufactList] FOREIGN KEY([manufID])
REFERENCES [dbo].[s4_manufactList] ([manufID])
GO
ALTER TABLE [dbo].[s4_articleList] CHECK CONSTRAINT [FK_s4_articleList_s4_manufactList]
GO
ALTER TABLE [dbo].[s4_articleList]  WITH CHECK ADD  CONSTRAINT [FK_s4_articleList_s4_sectionList] FOREIGN KEY([sectID])
REFERENCES [dbo].[s4_sectionList] ([sectID])
GO
ALTER TABLE [dbo].[s4_articleList] CHECK CONSTRAINT [FK_s4_articleList_s4_sectionList]
GO
ALTER TABLE [dbo].[s4_articleList_history]  WITH CHECK ADD  CONSTRAINT [FK_s4_articleList_history_s4_articleList] FOREIGN KEY([articleID])
REFERENCES [dbo].[s4_articleList] ([articleID])
GO
ALTER TABLE [dbo].[s4_articleList_history] CHECK CONSTRAINT [FK_s4_articleList_history_s4_articleList]
GO
ALTER TABLE [dbo].[s4_articleList_packings]  WITH CHECK ADD  CONSTRAINT [FK_s4_articleList_packings_s4_articleList] FOREIGN KEY([articleID])
REFERENCES [dbo].[s4_articleList] ([articleID])
GO
ALTER TABLE [dbo].[s4_articleList_packings] CHECK CONSTRAINT [FK_s4_articleList_packings_s4_articleList]
GO
ALTER TABLE [dbo].[s4_deliveryDirection]  WITH CHECK ADD  CONSTRAINT [FK_s4_deliveryDirection_direction] FOREIGN KEY([directionID])
REFERENCES [dbo].[s4_direction] ([directionID])
GO
ALTER TABLE [dbo].[s4_deliveryDirection] CHECK CONSTRAINT [FK_s4_deliveryDirection_direction]
GO
ALTER TABLE [dbo].[s4_deliveryDirection]  WITH CHECK ADD  CONSTRAINT [FK_s4_deliveryDirection_s4_deliveryManifest] FOREIGN KEY([deliveryManifestID])
REFERENCES [dbo].[s4_deliveryManifest] ([deliveryManifestID])
GO
ALTER TABLE [dbo].[s4_deliveryDirection] CHECK CONSTRAINT [FK_s4_deliveryDirection_s4_deliveryManifest]
GO
ALTER TABLE [dbo].[s4_deliveryDirection_items]  WITH CHECK ADD  CONSTRAINT [FK_s4_deliveryDirection_items_deliveryDirection] FOREIGN KEY([deliveryDirectionID])
REFERENCES [dbo].[s4_deliveryDirection] ([deliveryDirectionID])
GO
ALTER TABLE [dbo].[s4_deliveryDirection_items] CHECK CONSTRAINT [FK_s4_deliveryDirection_items_deliveryDirection]
GO
ALTER TABLE [dbo].[s4_direction]  WITH CHECK ADD  CONSTRAINT [FK_s4_partnerList_s4_direction] FOREIGN KEY([partnerID])
REFERENCES [dbo].[s4_partnerList] ([partnerID])
GO
ALTER TABLE [dbo].[s4_direction] CHECK CONSTRAINT [FK_s4_partnerList_s4_direction]
GO
ALTER TABLE [dbo].[s4_direction_assign]  WITH CHECK ADD  CONSTRAINT [FK_s4_direction_assign_s4_direction] FOREIGN KEY([directionID])
REFERENCES [dbo].[s4_direction] ([directionID])
GO
ALTER TABLE [dbo].[s4_direction_assign] CHECK CONSTRAINT [FK_s4_direction_assign_s4_direction]
GO
ALTER TABLE [dbo].[s4_direction_assign]  WITH CHECK ADD  CONSTRAINT [FK_s4_direction_assign_s4_workerList] FOREIGN KEY([workerID])
REFERENCES [dbo].[s4_workerList] ([workerID])
GO
ALTER TABLE [dbo].[s4_direction_assign] CHECK CONSTRAINT [FK_s4_direction_assign_s4_workerList]
GO
ALTER TABLE [dbo].[s4_direction_history]  WITH CHECK ADD  CONSTRAINT [FK_s4_direction_history_s4_direction] FOREIGN KEY([directionID])
REFERENCES [dbo].[s4_direction] ([directionID])
GO
ALTER TABLE [dbo].[s4_direction_history] CHECK CONSTRAINT [FK_s4_direction_history_s4_direction]
GO
ALTER TABLE [dbo].[s4_direction_items]  WITH CHECK ADD  CONSTRAINT [FK_s4_direction_items_s4_articleList_packings] FOREIGN KEY([artPackID])
REFERENCES [dbo].[s4_articleList_packings] ([artPackID])
GO
ALTER TABLE [dbo].[s4_direction_items] CHECK CONSTRAINT [FK_s4_direction_items_s4_articleList_packings]
GO
ALTER TABLE [dbo].[s4_direction_items]  WITH CHECK ADD  CONSTRAINT [FK_s4_direction_items_s4_direction] FOREIGN KEY([directionID])
REFERENCES [dbo].[s4_direction] ([directionID])
GO
ALTER TABLE [dbo].[s4_direction_items] CHECK CONSTRAINT [FK_s4_direction_items_s4_direction]
GO
ALTER TABLE [dbo].[s4_direction_xtra]  WITH CHECK ADD  CONSTRAINT [FK_s4_direction_xtra_s4_direction] FOREIGN KEY([directionID])
REFERENCES [dbo].[s4_direction] ([directionID])
GO
ALTER TABLE [dbo].[s4_direction_xtra] CHECK CONSTRAINT [FK_s4_direction_xtra_s4_direction]
GO
ALTER TABLE [dbo].[s4_invoice_xtra]  WITH CHECK ADD  CONSTRAINT [FK_s4_invoice_xtra_s4_invoice] FOREIGN KEY([invoiceID])
REFERENCES [dbo].[s4_invoice] ([invoiceID])
GO
ALTER TABLE [dbo].[s4_invoice_xtra] CHECK CONSTRAINT [FK_s4_invoice_xtra_s4_invoice]
GO
ALTER TABLE [dbo].[s4_manufactList]  WITH CHECK ADD  CONSTRAINT [FK_s4_manufactList_s4_sectionList] FOREIGN KEY([sectID])
REFERENCES [dbo].[s4_sectionList] ([sectID])
GO
ALTER TABLE [dbo].[s4_manufactList] CHECK CONSTRAINT [FK_s4_manufactList_s4_sectionList]
GO
ALTER TABLE [dbo].[s4_messageBus_document]  WITH CHECK ADD  CONSTRAINT [FK_s4_messageBus_document_s4_messageBus] FOREIGN KEY([messageBusID])
REFERENCES [dbo].[s4_messageBus] ([messageBusID])
GO
ALTER TABLE [dbo].[s4_messageBus_document] CHECK CONSTRAINT [FK_s4_messageBus_document_s4_messageBus]
GO
ALTER TABLE [dbo].[s4_onStoreSummary_items]  WITH CHECK ADD  CONSTRAINT [FK_s4_onStoreSumm_item_s4_onStoreSummary] FOREIGN KEY([onStoreSummID])
REFERENCES [dbo].[s4_onStoreSummary] ([onStoreSummID])
GO
ALTER TABLE [dbo].[s4_onStoreSummary_items] CHECK CONSTRAINT [FK_s4_onStoreSumm_item_s4_onStoreSummary]
GO
ALTER TABLE [dbo].[s4_onStoreSummary_items]  WITH CHECK ADD  CONSTRAINT [FK_s4_onStoreSummary_items_s4_storeMove_lots] FOREIGN KEY([stoMoveLotID])
REFERENCES [dbo].[s4_storeMove_lots] ([stoMoveLotID])
GO
ALTER TABLE [dbo].[s4_onStoreSummary_items] CHECK CONSTRAINT [FK_s4_onStoreSummary_items_s4_storeMove_lots]
GO
ALTER TABLE [dbo].[s4_partnerList_addresses]  WITH CHECK ADD  CONSTRAINT [FK_s4_partnerList_addresses_s4_partnerList] FOREIGN KEY([partnerID])
REFERENCES [dbo].[s4_partnerList] ([partnerID])
GO
ALTER TABLE [dbo].[s4_partnerList_addresses] CHECK CONSTRAINT [FK_s4_partnerList_addresses_s4_partnerList]
GO
ALTER TABLE [dbo].[s4_partnerList_history]  WITH CHECK ADD  CONSTRAINT [FK_s4_partnerList_history_s4_partnerList] FOREIGN KEY([partnerID])
REFERENCES [dbo].[s4_partnerList] ([partnerID])
GO
ALTER TABLE [dbo].[s4_partnerList_history] CHECK CONSTRAINT [FK_s4_partnerList_history_s4_partnerList]
GO
ALTER TABLE [dbo].[s4_positionList]  WITH CHECK ADD  CONSTRAINT [FK_s4_positionList_s4_houseList] FOREIGN KEY([houseID])
REFERENCES [dbo].[s4_houseList] ([houseID])
GO
ALTER TABLE [dbo].[s4_positionList] CHECK CONSTRAINT [FK_s4_positionList_s4_houseList]
GO
ALTER TABLE [dbo].[s4_positionList_history]  WITH CHECK ADD  CONSTRAINT [FK_s4_positionList_history_s4_positionList] FOREIGN KEY([positionID])
REFERENCES [dbo].[s4_positionList] ([positionID])
GO
ALTER TABLE [dbo].[s4_positionList_history] CHECK CONSTRAINT [FK_s4_positionList_history_s4_positionList]
GO
ALTER TABLE [dbo].[s4_positionList_sections]  WITH CHECK ADD  CONSTRAINT [FK_s4_positionList_sections_s4_positionList] FOREIGN KEY([positionID])
REFERENCES [dbo].[s4_positionList] ([positionID])
GO
ALTER TABLE [dbo].[s4_positionList_sections] CHECK CONSTRAINT [FK_s4_positionList_sections_s4_positionList]
GO
ALTER TABLE [dbo].[s4_positionList_sections]  WITH CHECK ADD  CONSTRAINT [FK_s4_positionList_sections_s4_sectionList] FOREIGN KEY([sectID])
REFERENCES [dbo].[s4_sectionList] ([sectID])
GO
ALTER TABLE [dbo].[s4_positionList_sections] CHECK CONSTRAINT [FK_s4_positionList_sections_s4_sectionList]
GO
ALTER TABLE [dbo].[s4_printerLocation]  WITH CHECK ADD  CONSTRAINT [FK_s4_printerLocation_PrinterType] FOREIGN KEY([printerTypeID])
REFERENCES [dbo].[s4_printerType] ([printerTypeID])
GO
ALTER TABLE [dbo].[s4_printerLocation] CHECK CONSTRAINT [FK_s4_printerLocation_PrinterType]
GO
ALTER TABLE [dbo].[s4_reports]  WITH CHECK ADD  CONSTRAINT [FK_s4_reports_PrinterType] FOREIGN KEY([printerTypeID])
REFERENCES [dbo].[s4_printerType] ([printerTypeID])
GO
ALTER TABLE [dbo].[s4_reports] CHECK CONSTRAINT [FK_s4_reports_PrinterType]
GO
ALTER TABLE [dbo].[s4_stocktaking_history]  WITH CHECK ADD  CONSTRAINT [FK_s4_stocktaking_history_s4_articleList_packings] FOREIGN KEY([artPackID])
REFERENCES [dbo].[s4_articleList_packings] ([artPackID])
GO
ALTER TABLE [dbo].[s4_stocktaking_history] CHECK CONSTRAINT [FK_s4_stocktaking_history_s4_articleList_packings]
GO
ALTER TABLE [dbo].[s4_stocktaking_history]  WITH CHECK ADD  CONSTRAINT [FK_s4_stocktaking_history_s4_positionList] FOREIGN KEY([positionID])
REFERENCES [dbo].[s4_positionList] ([positionID])
GO
ALTER TABLE [dbo].[s4_stocktaking_history] CHECK CONSTRAINT [FK_s4_stocktaking_history_s4_positionList]
GO
ALTER TABLE [dbo].[s4_stocktaking_history]  WITH CHECK ADD  CONSTRAINT [FK_s4_stocktaking_history_s4_stocktaking] FOREIGN KEY([stotakID])
REFERENCES [dbo].[s4_stocktaking] ([stotakID])
GO
ALTER TABLE [dbo].[s4_stocktaking_history] CHECK CONSTRAINT [FK_s4_stocktaking_history_s4_stocktaking]
GO
ALTER TABLE [dbo].[s4_stocktaking_items]  WITH CHECK ADD  CONSTRAINT [FK_s4_stocktaking_items_s4_articleList_packings] FOREIGN KEY([artPackID])
REFERENCES [dbo].[s4_articleList_packings] ([artPackID])
GO
ALTER TABLE [dbo].[s4_stocktaking_items] CHECK CONSTRAINT [FK_s4_stocktaking_items_s4_articleList_packings]
GO
ALTER TABLE [dbo].[s4_stocktaking_items]  WITH CHECK ADD  CONSTRAINT [FK_s4_stocktaking_items_s4_positionList] FOREIGN KEY([positionID])
REFERENCES [dbo].[s4_positionList] ([positionID])
GO
ALTER TABLE [dbo].[s4_stocktaking_items] CHECK CONSTRAINT [FK_s4_stocktaking_items_s4_positionList]
GO
ALTER TABLE [dbo].[s4_stocktaking_items]  WITH CHECK ADD  CONSTRAINT [FK_s4_stocktaking_items_s4_stocktaking] FOREIGN KEY([stotakID])
REFERENCES [dbo].[s4_stocktaking] ([stotakID])
GO
ALTER TABLE [dbo].[s4_stocktaking_items] CHECK CONSTRAINT [FK_s4_stocktaking_items_s4_stocktaking]
GO
ALTER TABLE [dbo].[s4_stocktaking_positions]  WITH CHECK ADD  CONSTRAINT [FK_s4_stocktaking_positions_s4_positionList] FOREIGN KEY([positionID])
REFERENCES [dbo].[s4_positionList] ([positionID])
GO
ALTER TABLE [dbo].[s4_stocktaking_positions] CHECK CONSTRAINT [FK_s4_stocktaking_positions_s4_positionList]
GO
ALTER TABLE [dbo].[s4_stocktaking_positions]  WITH CHECK ADD  CONSTRAINT [FK_s4_stocktaking_positions_s4_stocktaking] FOREIGN KEY([stotakID])
REFERENCES [dbo].[s4_stocktaking] ([stotakID])
GO
ALTER TABLE [dbo].[s4_stocktaking_positions] CHECK CONSTRAINT [FK_s4_stocktaking_positions_s4_stocktaking]
GO
ALTER TABLE [dbo].[s4_stocktaking_snapshots]  WITH CHECK ADD  CONSTRAINT [FK_s4_stocktaking_snapshots_s4_articleList_packings] FOREIGN KEY([artPackID])
REFERENCES [dbo].[s4_articleList_packings] ([artPackID])
GO
ALTER TABLE [dbo].[s4_stocktaking_snapshots] CHECK CONSTRAINT [FK_s4_stocktaking_snapshots_s4_articleList_packings]
GO
ALTER TABLE [dbo].[s4_stocktaking_snapshots]  WITH CHECK ADD  CONSTRAINT [FK_s4_stocktaking_snapshots_s4_positionList] FOREIGN KEY([positionID])
REFERENCES [dbo].[s4_positionList] ([positionID])
GO
ALTER TABLE [dbo].[s4_stocktaking_snapshots] CHECK CONSTRAINT [FK_s4_stocktaking_snapshots_s4_positionList]
GO
ALTER TABLE [dbo].[s4_stocktaking_snapshots]  WITH CHECK ADD  CONSTRAINT [FK_s4_stocktaking_snapshots_s4_stocktaking] FOREIGN KEY([stotakID])
REFERENCES [dbo].[s4_stocktaking] ([stotakID])
GO
ALTER TABLE [dbo].[s4_stocktaking_snapshots] CHECK CONSTRAINT [FK_s4_stocktaking_snapshots_s4_stocktaking]
GO
ALTER TABLE [dbo].[s4_storeMove_history]  WITH CHECK ADD  CONSTRAINT [FK_s4_storeMove_history_s4_storeMove] FOREIGN KEY([stoMoveID])
REFERENCES [dbo].[s4_storeMove] ([stoMoveID])
GO
ALTER TABLE [dbo].[s4_storeMove_history] CHECK CONSTRAINT [FK_s4_storeMove_history_s4_storeMove]
GO
ALTER TABLE [dbo].[s4_storeMove_items]  WITH CHECK ADD  CONSTRAINT [FK_s4_storeMove_items_s4_positionList] FOREIGN KEY([positionID])
REFERENCES [dbo].[s4_positionList] ([positionID])
GO
ALTER TABLE [dbo].[s4_storeMove_items] CHECK CONSTRAINT [FK_s4_storeMove_items_s4_positionList]
GO
ALTER TABLE [dbo].[s4_storeMove_items]  WITH CHECK ADD  CONSTRAINT [FK_s4_storeMove_items_s4_storeMove] FOREIGN KEY([stoMoveID])
REFERENCES [dbo].[s4_storeMove] ([stoMoveID])
GO
ALTER TABLE [dbo].[s4_storeMove_items] CHECK CONSTRAINT [FK_s4_storeMove_items_s4_storeMove]
GO
ALTER TABLE [dbo].[s4_storeMove_items]  WITH CHECK ADD  CONSTRAINT [FK_s4_storeMove_items_s4_storeMove_lots] FOREIGN KEY([stoMoveLotID])
REFERENCES [dbo].[s4_storeMove_lots] ([stoMoveLotID])
GO
ALTER TABLE [dbo].[s4_storeMove_items] CHECK CONSTRAINT [FK_s4_storeMove_items_s4_storeMove_lots]
GO
ALTER TABLE [dbo].[s4_storeMove_lots]  WITH CHECK ADD  CONSTRAINT [FK_s4_storeMove_lots_s4_articleList_packings] FOREIGN KEY([artPackID])
REFERENCES [dbo].[s4_articleList_packings] ([artPackID])
GO
ALTER TABLE [dbo].[s4_storeMove_lots] CHECK CONSTRAINT [FK_s4_storeMove_lots_s4_articleList_packings]
GO
ALTER TABLE [dbo].[s4_storeMove_serials]  WITH CHECK ADD  CONSTRAINT [FK_s4_storeMove_serials_s4_storeMove_items] FOREIGN KEY([stoMoveItemID])
REFERENCES [dbo].[s4_storeMove_items] ([stoMoveItemID])
GO
ALTER TABLE [dbo].[s4_storeMove_serials] CHECK CONSTRAINT [FK_s4_storeMove_serials_s4_storeMove_items]
GO
ALTER TABLE [dbo].[s4_storeMove_xtra]  WITH CHECK ADD  CONSTRAINT [FK_s4_storeMove_xtra_s4_storeMove] FOREIGN KEY([stoMoveID])
REFERENCES [dbo].[s4_storeMove] ([stoMoveID])
GO
ALTER TABLE [dbo].[s4_storeMove_xtra] CHECK CONSTRAINT [FK_s4_storeMove_xtra_s4_storeMove]
GO
ALTER TABLE [dbo].[s4_workerList_history]  WITH CHECK ADD  CONSTRAINT [FK_s4_workerList_history_s4_workerList] FOREIGN KEY([workerID])
REFERENCES [dbo].[s4_workerList] ([workerID])
GO
ALTER TABLE [dbo].[s4_workerList_history] CHECK CONSTRAINT [FK_s4_workerList_history_s4_workerList]
GO
ALTER TABLE [dbo].[s4_direction]  WITH CHECK ADD  CONSTRAINT [CK_s4_direction] CHECK  (([docDirection]=(-1) OR [docDirection]=(1) OR [docDirection]=(0)))
GO
ALTER TABLE [dbo].[s4_direction] CHECK CONSTRAINT [CK_s4_direction]
GO
ALTER TABLE [dbo].[s4_direction_items]  WITH CHECK ADD  CONSTRAINT [CK_s4_direction_items] CHECK  (([docPosition]<>(0)))
GO
ALTER TABLE [dbo].[s4_direction_items] CHECK CONSTRAINT [CK_s4_direction_items]
GO
ALTER TABLE [dbo].[s4_prefixList]  WITH CHECK ADD  CONSTRAINT [CK_s4_prefixList] CHECK  (([docDirection]=(-1) OR ([docDirection]=(1) OR [docDirection]=(0))))
GO
ALTER TABLE [dbo].[s4_prefixList] CHECK CONSTRAINT [CK_s4_prefixList]
GO
ALTER TABLE [dbo].[s4_storeMove_items]  WITH CHECK ADD  CONSTRAINT [CK_s4_storeMove_items] CHECK  (([itemDirection]=(-1) OR [itemDirection]=(1)))
GO
ALTER TABLE [dbo].[s4_storeMove_items] CHECK CONSTRAINT [CK_s4_storeMove_items]
GO
ALTER TABLE [dbo].[s4_storeMove_items]  WITH CHECK ADD  CONSTRAINT [CK_s4_storeMove_items_itemValidity] CHECK  (([itemValidity]=(0) OR [itemValidity]=(100) OR [itemValidity]=(255)))
GO
ALTER TABLE [dbo].[s4_storeMove_items] CHECK CONSTRAINT [CK_s4_storeMove_items_itemValidity]
GO
ALTER TABLE [dbo].[s4_storeMove_items]  WITH CHECK ADD  CONSTRAINT [CK_s4_storeMove_items_position] CHECK  (([docPosition]<>(0)))
GO
ALTER TABLE [dbo].[s4_storeMove_items] CHECK CONSTRAINT [CK_s4_storeMove_items_position]
GO
/****** Object:  StoredProcedure [dbo].[s4_makeOnStoreSummary]    Script Date: 18.08.2020 12:34:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[s4_makeOnStoreSummary]
	@lastStoMoveItemID int
AS

DECLARE @onStoreSummID int;


-- check summary with 'new summary' status
IF(EXISTS(SELECT * FROM [dbo].[s4_onStoreSummary] WHERE [onStoreSummStatus] = 0))
THROW 50000, 'Store4: New summary already exists', 1;


-- insert new summary with 'new summary' status
INSERT INTO [dbo].[s4_onStoreSummary] ([onStoreSummStatus] , [lastStoMoveItemID])
VALUES
	(0, @lastStoMoveItemID)
SELECT @onStoreSummID = SCOPE_IDENTITY()


BEGIN TRANSACTION;

BEGIN TRY
	-- cancel not valid s4_storeMove_items
	UPDATE [dbo].[s4_storeMove_items]
	SET
		[itemValidity] = 255 -- MI_VALIDITY_CANCELED
	WHERE
		[itemValidity] = 0 -- MI_VALIDITY_NOT_VALID
		AND stoMoveItemID <= @lastStoMoveItemID
	

	-- fill s4_onStoreSummary_items
	INSERT INTO [dbo].[s4_onStoreSummary_items] ([onStoreSummID], [stoMoveLotID], [positionID], [carrierNum], [quantity])
	SELECT
		@onStoreSummID,
		[stoMoveLotID],
		[positionID],
		[carrierNum],
		SUM([quantity] * [itemDirection]) AS [quantity]
	FROM	[dbo].[s4_storeMove_items]
	WHERE	[itemValidity] = 100
			AND [stoMoveItemID] <= @lastStoMoveItemID
	GROUP BY
		[stoMoveLotID],
		[positionID],
		[carrierNum]
	HAVING
		SUM([quantity] * [itemDirection]) <> 0


	-- update last summary status to 'replaced summary'
	UPDATE [dbo].[s4_onStoreSummary]
	SET
		[onStoreSummStatus] = 255
	WHERE
		[onStoreSummStatus] <> 255
		AND [onStoreSummID] <> @onStoreSummID


	-- update new summary to 'saved summary' status
	UPDATE [dbo].[s4_onStoreSummary]
	SET
		[onStoreSummStatus] = 100
	WHERE
		[onStoreSummID] = @onStoreSummID

	COMMIT TRANSACTION;
END TRY
BEGIN CATCH
    IF @@TRANCOUNT > 0
    BEGIN
        ROLLBACK TRANSACTION;
    END
END CATCH
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 - standard, 1 - carrier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N's4_articleList', @level2type=N'COLUMN',@level2name=N'articleType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 - new summary, 100 - saved summary, 255 - replaced summary' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N's4_onStoreSummary', @level2type=N'COLUMN',@level2name=N'onStoreSummStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Can be 0 - not valid, 100 - valid, 255 - canceled' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N's4_storeMove_items', @level2type=N'COLUMN',@level2name=N'itemValidity'
GO
USE [master]
GO
ALTER DATABASE [S4_KHL] SET  READ_WRITE 
GO

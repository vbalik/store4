﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DeliveryTestAppW
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            // set connection string for procedures
            S4.Core.Data.ConnectionHelper.ConnectionString = Environment.GetEnvironmentVariable("S4_LOCALDB_CN");

            Application.Run(new Form1());
        }
    }
}

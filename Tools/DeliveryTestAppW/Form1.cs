﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PdfiumPrinter;

namespace DeliveryTestAppW
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();

            var deliveryService = new S4.DeliveryServices.DeliveryService();
            deliveryService.LogEvent += DeliveryService_LogEvent;
            deliveryService.GetLabelEvent += (s, args) =>
            {
                var fileName = $"Label{args.ReferenceId}.pdf";
                try
                {
                    System.IO.File.WriteAllBytes(fileName, args.Data);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString(), ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            };

            deliveryService.GetLabelEvent += DeliveryService_GetLabelEvent;
            await deliveryService.GetShipmentLabel(Convert.ToInt32(textBox1.Text));
        }

        private void DeliveryService_GetLabelEvent(object sender, S4.DeliveryServices.GetLabelEventArgs e)
        {
            var printerName = "Intermec PM43 (203 dpi)";
            var pdfPrinter = new PdfPrinter(printerName);
            pdfPrinter.Print(new System.IO.MemoryStream(e.Data));
        }

        private void DeliveryService_LogEvent(S4.DeliveryServices.IDeliveryService sender, string message, S4.DeliveryServices.LogLevelEnum logLevel)
        {
            listBox1.Items.Add(message);
        }
    }
}

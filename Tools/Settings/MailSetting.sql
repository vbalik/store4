DECLARE @jsonMailSetting NVARCHAR(MAX)

SET @jsonMailSetting = N'{
"mailFrom": "store4@promedica-praha.cz",
"mailServer": "smtp.promedica-praha.cz",
"mailUzivatel": "store4",
"mailHeslo": "P1st0le155."
}';


DECLARE @setFieldID NVARCHAR(32);
DECLARE @setDataType NVARCHAR(64);
SET @setDataType = 'System.String';
SET @setFieldID = 'MailSetting';

IF NOT EXISTS (SELECT 1 FROM [dbo].[s4_settings] WHERE (setFieldID = @setFieldID))
BEGIN
    INSERT INTO [dbo].[s4_settings] (setFieldID, setDataType, setValue) VALUES (@setFieldID, @setDataType, @jsonMailSetting);
	print 'insert ok '+ @setFieldID;
END
ELSE
BEGIN
	UPDATE [dbo].[s4_settings] SET setDataType = @setDataType, setValue = @jsonMailSetting WHERE setFieldID = @setFieldID;
	print 'update ok '+ @setFieldID;
END

--select * from [s4_settings]


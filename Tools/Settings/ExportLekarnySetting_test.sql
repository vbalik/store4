DECLARE @jsonExportLekarny NVARCHAR(MAX)

SET @jsonExportLekarny = N'{
    "dodavatelICO": "25099019",
    "dodavatelDIC": "CZ25099019",
    "kontrolniKopie": true,
    "odeslatKontrolniKopie": "petrun@promedica-praha.cz",
    "vysledekUlozit": true,
    "ulozitCesta": "C:\\temp",
    "odeslatZakaznik": false,
    "settingsExportRady": [
      {
        "prefix": "PL",
        "g4source": "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.1.101)(PORT=1521)))(CONNECT_DATA=(SID=abra)(SERVER=DEDICATED)));User Id=S09_FNKV;Password=abra;",
        "ulozitG4": "C:\\temp\\copy" //"\\\\abra-app\\Komunikace"
      },
      {
        "prefix": "CDLD,CDLR,CDLN,CDLJ,CDLT,IDLD,IDLR,IDLN,IDLJ,IDLT,DLP",
        "g4source": "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.1.101)(PORT=1521)))(CONNECT_DATA=(SID=abra)(SERVER=DEDICATED)));User Id=S20_CZZ;Password=abra;",
        "ulozitG4": "C:\\temp\\copy",
      },
      {
        "prefix": "PLB",
        "g4source": "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.1.101)(PORT=1521)))(CONNECT_DATA=(SID=abra)(SERVER=DEDICATED)));User Id=S10_BOLESLAV;Password=abra;",
        "ulozitG4": "C:\\temp\\copy"
      },
      {
        "prefix": "DLM,PRVH",
        "g4source": "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.1.101)(PORT=1521)))(CONNECT_DATA=(SID=abra)(SERVER=DEDICATED)));User Id=S12_MEDPROGRES;Password=abra;",
        "ulozitG4": "C:\\temp\\copy",
		"MSSQLsource": "Server=192.168.1.189;Database=S12;User ID=sa;Password=Masterkey2010"
      },
      {
        "prefix": "MZNV,SZMS,PVL",
        "g4source": "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.1.101)(PORT=1521)))(CONNECT_DATA=(SID=abra)(SERVER=DEDICATED)));User Id=S11_CL;Password=abra;",
        "ulozitG4": "C:\\temp\\copy"
      },
      {
        "prefix": "DLL",
        "g4source": "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.1.101)(PORT=1521)))(CONNECT_DATA=(SID=abra)(SERVER=DEDICATED)));User Id=S04_NL;Password=abra;",
        "ulozitG4": "C:\\temp\\copy",
		"MSSQLsource": "Server=192.168.1.189;Database=S04;User ID=sa;Password=Masterkey2010"
      },
      {
        "prefix": "PLJ,PLP",
        "g4source": "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.1.101)(PORT=1521)))(CONNECT_DATA=(SID=abra)(SERVER=DEDICATED)));User Id=S17_JIHLAVA;Password=abra;",
        "ulozitG4": "C:\\temp\\copy"
      },
      {
        "prefix": "PLS,PLD",
        "g4source": "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.1.101)(PORT=1521)))(CONNECT_DATA=(SID=abra)(SERVER=DEDICATED)));User Id=S07_OPAVA;Password=abra;",
        "ulozitG4": "C:\\temp\\copy"
      },
      {
        "prefix": "PLK,PLH,PLKS,PLHS",
        "g4source": "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.1.101)(PORT=1521)))(CONNECT_DATA=(SID=abra)(SERVER=DEDICATED)));User Id=S22_KaHaN;Password=abra;",
        "ulozitG4": "C:\\temp\\copy",
		"ignore": "true"
      },
      {
        "prefix": "DLPD,DLPJ,DLPT",
        "g4source": "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.1.101)(PORT=1521)))(CONNECT_DATA=(SID=abra)(SERVER=DEDICATED)));User Id=S03_Holding;Password=abra;",
        "ulozitG4": "C:\\temp\\copy"
      },
	  {
        "prefix": "PLBO",
        "g4source": "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.1.101)(PORT=1521)))(CONNECT_DATA=(SID=abra)(SERVER=DEDICATED)));User Id=S24_Boro;Password=abra;",
        "ulozitG4": "C:\\temp\\copy"
      },
      {
        "prefix": "*",
        "g4source": "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.1.101)(PORT=1521)))(CONNECT_DATA=(SID=abra)(SERVER=DEDICATED)));User Id=S01_PPG;Password=abra;",
        "ulozitG4": "C:\\temp\\copy"
      }
    ]
}';


DECLARE @setFieldID NVARCHAR(32);
DECLARE @setDataType NVARCHAR(64);
SET @setDataType = 'System.String';
SET @setFieldID = 'ExportLekarny';

IF NOT EXISTS (SELECT 1 FROM [dbo].[s4_settings] WHERE (setFieldID = @setFieldID))
BEGIN
    INSERT INTO [dbo].[s4_settings] (setFieldID, setDataType, setValue) VALUES (@setFieldID, @setDataType, @jsonExportLekarny);
	print 'insert ok '+ @setFieldID;
END
ELSE
BEGIN
	UPDATE [dbo].[s4_settings] SET setDataType = @setDataType, setValue = @jsonExportLekarny WHERE setFieldID = @setFieldID;
	print 'update ok '+ @setFieldID;
END

--select * from [s4_settings]

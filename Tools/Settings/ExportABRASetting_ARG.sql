DECLARE @jsonExportABRA NVARCHAR(MAX)

SET @jsonExportABRA = N'{
    "settingsExportRady": [
      {
        "prefix": "*",
		"ulozitG4": "\\\\abra-app\\Komunikace\\Sarze_new\\S02"
      }
    ]
}';


DECLARE @setFieldID NVARCHAR(32);
DECLARE @setDataType NVARCHAR(64);
SET @setDataType = 'System.String';

SET @setFieldID = 'ExportABRA';

IF NOT EXISTS (SELECT 1 FROM [dbo].[s4_settings] WHERE (setFieldID = @setFieldID))
BEGIN
    INSERT INTO [dbo].[s4_settings] (setFieldID, setDataType, setValue) VALUES (@setFieldID, @setDataType, @jsonExportABRA);
	print 'insert ok '+ @setFieldID;
END
ELSE
BEGIN
	UPDATE [dbo].[s4_settings] SET setDataType = @setDataType, setValue = @jsonExportABRA WHERE setFieldID = @setFieldID;
	print 'update ok '+ @setFieldID;
END


--select * from [s4_settings]

DECLARE @jsonExportABRA NVARCHAR(MAX)

SET @jsonExportABRA = N'{
    "settingsExportRady": [
	  {
        "prefix": "PLAL",
        "ulozitG4": "\\\\abra-app\\Komunikace\\Sarze_new\\S25" 
      },
      {
        "prefix": "PL",
        "ulozitG4": "\\\\abra-app\\Komunikace\\Sarze_new\\S09" 
      },
      {
        "prefix": "CDLD,CDLR,CDLN,CDLJ,CDLT,IDLD,IDLR,IDLN,IDLJ,IDLT,DLP",
        "ulozitG4": "\\\\abra-app\\Komunikace\\Sarze_new\\S20" 
      },
      {
        "prefix": "PLB",
        "ulozitG4": "\\\\abra-app\\Komunikace\\Sarze_new\\S10"
      },
      {
        "prefix": "DLM,PRVH",
        "ulozitG4": "\\\\abra-app\\Komunikace\\Sarze_new\\S12"
      },
      {
        "prefix": "MZNV,SZMS,PVL",
        "ulozitG4": "\\\\abra-app\\Komunikace\\Sarze_new\\S11"
      },
      {
        "prefix": "DLL",
        "ulozitG4": "\\\\abra-app\\Komunikace\\Sarze_new\\S04"
      },
      {
        "prefix": "PLJ,PLP",
        "ulozitG4": "\\\\abra-app\\Komunikace\\Sarze_new\\S17"
      },
      {
        "prefix": "PLS,PLD,PLSP,PLDP",
        "ulozitG4": "\\\\abra-app\\Komunikace\\Sarze_new\\S07"
      },
      {
        "prefix": "PLK,PLH,PLKS,PLHS",
        "ulozitG4": "\\\\abra-app\\Komunikace\\Sarze_new\\S22"
      },
      {
        "prefix": "DLPD,DLPJ,DLPT",
        "ulozitG4": "\\\\abra-app\\Komunikace\\Sarze_new\\S03"
      },
      {
        "prefix": "PLBO",
        "ulozitG4": "\\\\abra-app\\Komunikace\\Sarze_new\\S24"
      },
      {
        "prefix": "FDL",
        "ulozitG4": "\\\\abra-app\\Komunikace\\Sarze_new\\S31"
      },
      {
        "prefix": "*",
        "ulozitG4": "\\\\abra-app\\Komunikace\\Sarze_new\\S01"
      }
    ]
}';


DECLARE @setFieldID NVARCHAR(32);
DECLARE @setDataType NVARCHAR(64);
SET @setDataType = 'System.String';

SET @setFieldID = 'ExportABRA';

IF NOT EXISTS (SELECT 1 FROM [dbo].[s4_settings] WHERE (setFieldID = @setFieldID))
BEGIN
    INSERT INTO [dbo].[s4_settings] (setFieldID, setDataType, setValue) VALUES (@setFieldID, @setDataType, @jsonExportABRA);
	print 'insert ok '+ @setFieldID;
END
ELSE
BEGIN
	UPDATE [dbo].[s4_settings] SET setDataType = @setDataType, setValue = @jsonExportABRA WHERE setFieldID = @setFieldID;
	print 'update ok '+ @setFieldID;
END


--select * from [s4_settings]

DECLARE @jsonExportLekarny NVARCHAR(MAX)

SET @jsonExportLekarny = N'{
    "dodavatelICO": "25099019",
    "dodavatelDIC": "CZ25099019",
    "kontrolniKopie": true,
    "odeslatKontrolniKopie": "dodacilisty@promedica-praha.cz",
    "vysledekUlozit": true,
    "ulozitCesta": "C:\\S4_KHL\\S4.ExchangeService\\ExportFiles",
    "odeslatZakaznik": false, 
    "settingsExportRady": [
      {
        "prefix": "DLNP,PLHJ,PLHN,PLHR,PLHD,PLHT",
        "g4source": "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.1.101)(PORT=1521)))(CONNECT_DATA=(SID=abra)(SERVER=DEDICATED)));User Id=S23_LKHK;Password=abra;",
		"ulozitG4": "\\\\abra-app\\Komunikace\\Sarze\\S23",
		"MSSQLsource": "Server=192.168.1.189;Database=S23;User ID=sa;Password=Masterkey2010"
      }
    ]
}';


DECLARE @setFieldID NVARCHAR(32);
DECLARE @setDataType NVARCHAR(64);
SET @setDataType = 'System.String';

SET @setFieldID = 'ExportLekarny';

IF NOT EXISTS (SELECT 1 FROM [dbo].[s4_settings] WHERE (setFieldID = @setFieldID))
BEGIN
    INSERT INTO [dbo].[s4_settings] (setFieldID, setDataType, setValue) VALUES (@setFieldID, @setDataType, @jsonExportLekarny);
	print 'insert ok '+ @setFieldID;
END
ELSE
BEGIN
	UPDATE [dbo].[s4_settings] SET setDataType = @setDataType, setValue = @jsonExportLekarny WHERE setFieldID = @setFieldID;
	print 'update ok '+ @setFieldID;
END


--select * from [s4_settings]

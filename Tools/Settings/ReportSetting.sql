
DECLARE @setFieldID as varchar(100);
DECLARE @setValue as varchar(100);

SET @setFieldID = 'dispatch_sn/report/id';
SET @setValue = '33';

IF NOT EXISTS (SELECT 1 FROM [dbo].[s4_settings] WHERE (setFieldID = @setFieldID))
BEGIN
    INSERT INTO [dbo].[s4_settings] (setFieldID, setDataType, setValue) VALUES (@setFieldID, 'System.Int32', @setValue);
	print 'insert ok '+ @setFieldID;
END
ELSE
BEGIN
	UPDATE [dbo].[s4_settings] SET setDataType = 'System.Int32', setValue = @setValue WHERE setFieldID = @setFieldID;
	print 'update ok '+ @setFieldID;
END

SET @setFieldID = 'stotakRemote/report/id';
SET @setValue = '37';
IF NOT EXISTS (SELECT 1 FROM [dbo].[s4_settings] WHERE (setFieldID = @setFieldID))
BEGIN
    INSERT INTO [dbo].[s4_settings] (setFieldID, setDataType, setValue) VALUES (@setFieldID, 'System.Int32', @setValue);
	print 'insert ok '+ @setFieldID;
END
ELSE
BEGIN
	UPDATE [dbo].[s4_settings] SET setDataType = 'System.Int32', setValue = @setValue WHERE setFieldID = @setFieldID;
	print 'update ok '+ @setFieldID;
END

GO
DECLARE @jsonDelivery NVARCHAR(MAX)

SET @jsonDelivery = N'{
  "errorLimit": 3,
  "savePathRequestResponse": "c:\\S4_ArgoMed\\S4.ExchangeService\\ExchangeFiles\\delivery",
  "sendWithZeroWeight": false,
  "providers": [
    {
      "providerName": "DPD",
      "properties": {
        "applicationType": 9,
        "language": "CZ",
        "password": "argOMed875",
        "payerId": "8046506",
        "senderAddressId": "8192736",
        "userName": "ARGOMED",
        "mainServiceCode": 109,
		"trackURL": "https://tracking.dpd.de/parcelstatus?locale=cs_CZ&Tracking=Sledovat&query="
      }
    },
    {
      "providerName": "PPL",
      "properties": {
        "custId": 1720501,
        "password": "AGM1720501",
        "userName": "AGM1720501",
        "routeCodePPL": "",
        "senderName": "ArgoMed a.s.",
        "senderAddress": "K Arconu 70, Ja�lovice-���any",
        "senderZipCode": "251 01",
        "defaultPrinterLocationID": 43,
		"trackURL": "https://www.ppl.cz/vyhledat-zasilku?shipmentId="
      }
    },
    {
      "providerName": "CPOST",
      "properties": {
        "contractID": "249011002",
        "customerID": "M14423",
        "postCode": "10003",
        "locationNumber": "1",
        "language": "cs",
		"trackURL": "https://www.postaonline.cz/cs/trackandtrace/-/zasilka/cislo?parcelNumbers="
      }
    }
  ]
}';


DECLARE @setFieldID NVARCHAR(32);
DECLARE @setDataType NVARCHAR(64);
SET @setDataType = 'System.String';

SET @setFieldID = 'deliveryservicessetting';

IF NOT EXISTS (SELECT 1 FROM [dbo].[s4_settings] WHERE (setFieldID = @setFieldID))
BEGIN
    INSERT INTO [dbo].[s4_settings] (setFieldID, setDataType, setValue) VALUES (@setFieldID, @setDataType, @jsonDelivery);
	print 'insert ok '+ @setFieldID;
END
ELSE
BEGIN
	UPDATE [dbo].[s4_settings] SET setDataType = @setDataType, setValue = @jsonDelivery WHERE setFieldID = @setFieldID;
	print 'update ok '+ @setFieldID;
END


--select * from [s4_settings]



DECLARE @setFieldID as varchar(100);
DECLARE @setValue as varchar(100);
DECLARE @dataType as varchar(100);

SET @setFieldID = 'exportadvantis/partner/ids';
SET @setValue = '135'; --m��e b�t v�ce hodnot odd�len�ch ��rkou '135,136,137'
SET @dataType = 'System.String';

IF NOT EXISTS (SELECT 1 FROM [dbo].[s4_settings] WHERE (setFieldID = @setFieldID))
BEGIN
    INSERT INTO [dbo].[s4_settings] (setFieldID, setDataType, setValue) VALUES (@setFieldID, @dataType, @setValue);
	print 'insert ok '+ @setFieldID;
END
ELSE
BEGIN
	UPDATE [dbo].[s4_settings] SET setDataType = @dataType, setValue = @setValue WHERE setFieldID = @setFieldID;
	print 'update ok '+ @setFieldID;
END

GO


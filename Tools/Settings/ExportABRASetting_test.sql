DECLARE @jsonExportABRA NVARCHAR(MAX)

SET @jsonExportABRA = N'{
    "settingsExportRady": [
      {
        "prefix": "PL",
        "ulozitG4": "C:\\temp\\copy" 
      },
      {
        "prefix": "CDLD,CDLR,CDLN,CDLJ,CDLT,IDLD,IDLR,IDLN,IDLJ,IDLT,DLP",
        "ulozitG4": "C:\\temp\\copy" 
      },
      {
        "prefix": "PLB",
        "ulozitG4": "C:\\temp\\copy"
      },
      {
        "prefix": "DLM,PRVH",
        "ulozitG4": "C:\\temp\\copy"
      },
      {
        "prefix": "MZNV,SZMS,PVL",
        "ulozitG4": "C:\\temp\\copy"
      },
      {
        "prefix": "DLL",
        "ulozitG4": "C:\\temp\\copy"
      },
      {
        "prefix": "PLJ,PLP",
        "ulozitG4": "C:\\temp\\copy"
      },
      {
        "prefix": "PLS,PLD",
        "ulozitG4": "C:\\temp\\copy"
      },
      {
        "prefix": "PLK,PLH,PLKS,PLHS",
        "ulozitG4": "C:\\temp\\copy"
      },
      {
        "prefix": "DLPD,DLPJ,DLPT",
        "ulozitG4": "C:\\temp\\copy"
      },
      {
        "prefix": "PLBO",
        "ulozitG4": "C:\\temp\\copy"
      },
      {
        "prefix": "*",
        "ulozitG4": "C:\\temp\\copy"
      }
    ]
}';


DECLARE @setFieldID NVARCHAR(32);
DECLARE @setDataType NVARCHAR(64);
SET @setDataType = 'System.String';

SET @setFieldID = 'ExportABRA';

IF NOT EXISTS (SELECT 1 FROM [dbo].[s4_settings] WHERE (setFieldID = @setFieldID))
BEGIN
    INSERT INTO [dbo].[s4_settings] (setFieldID, setDataType, setValue) VALUES (@setFieldID, @setDataType, @jsonExportABRA);
	print 'insert ok '+ @setFieldID;
END
ELSE
BEGIN
	UPDATE [dbo].[s4_settings] SET setDataType = @setDataType, setValue = @jsonExportABRA WHERE setFieldID = @setFieldID;
	print 'update ok '+ @setFieldID;
END


--select * from [s4_settings]


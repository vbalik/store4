DECLARE @setVal NVARCHAR(MAX)

--set @setVal = 'Server=192.168.1.189;Database=S02;User ID=sa;Password=Masterkey2010' --ARG
--set @setVal = 'Server=192.168.1.189;Database=S01;User ID=sa;Password=Masterkey2010' --PPG
--set @setVal = 'Server=192.168.1.189;Database=S23;User ID=sa;Password=Masterkey2010' --KHL


DECLARE @setFieldID NVARCHAR(32);
DECLARE @setDataType NVARCHAR(64);
SET @setDataType = 'System.String';

SET @setFieldID = 'abra/connection_string';

IF NOT EXISTS (SELECT 1 FROM [dbo].[s4_settings] WHERE (setFieldID = @setFieldID))
BEGIN
    INSERT INTO [dbo].[s4_settings] (setFieldID, setDataType, setValue) VALUES (@setFieldID, @setDataType, @setVal);
	print 'insert ok '+ @setFieldID;
END
ELSE
BEGIN
	UPDATE [dbo].[s4_settings] SET setDataType = @setDataType, setValue = @setVal WHERE setFieldID = @setFieldID;
	print 'update ok '+ @setFieldID;
END


--select * from [s4_settings]

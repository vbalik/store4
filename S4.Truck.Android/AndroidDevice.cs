﻿using S4.Truck.Core.Common;
using S4Truck.Droid;

[assembly: Xamarin.Forms.Dependency(typeof(AndroidDevice))]
namespace S4Truck.Droid
{
    public class AndroidDevice : IAndroidDevice
    {
        public string GetIdentifier()
        {
            return Android.Provider.Settings.Secure.GetString(Android.App.Application.Context.ContentResolver, Android.Provider.Settings.Secure.AndroidId);
        }
    }
}
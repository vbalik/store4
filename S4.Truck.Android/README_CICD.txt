##################################################
##
##  CI/CD run this PowerShell command for ZEBRA
##
##################################################

# CMD 1

((Get-Content -path S4.Truck.Android.csproj -Raw) -replace '<AndroidManifest>Properties\AndroidManifest.xml</AndroidManifest>','<AndroidManifest>Properties\AndroidManifest_Zebra.xml</AndroidManifest>') | Set-Content -Path S4.Truck.Android.csproj


# CMD 2

((Get-Content -path S4.Truck.Android.csproj -Raw) -replace '<DefineConstants>HONEYWELL</DefineConstants>','<DefineConstants>ZEBRA</DefineConstants>') | Set-Content -Path S4.Truck.Android.csproj


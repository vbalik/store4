﻿using Android.Widget;
using S4.Truck.Droid;

[assembly: Xamarin.Forms.Dependency(typeof(ToastService))]
namespace S4.Truck.Droid
{
    public class ToastService : IToast
    {
        public void Show(string message, bool isFail = true)
        {
            Toast t = Toast.MakeText(Android.App.Application.Context, message, ToastLength.Long);
            t.Show();
        }
    }
}
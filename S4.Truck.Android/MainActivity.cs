﻿using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.OS;
using Android.Content;
using S4Truck.Droid.SyncDataService;
using S4.Truck;
using System.Net;

namespace S4Truck.Droid
{
    [Activity(Label = "S4Truck", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = true, ScreenOrientation = ScreenOrientation.Portrait, ConfigurationChanges = ConfigChanges.ScreenSize)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        static readonly string TAG = typeof(MainActivity).FullName;
        Intent SyncDataServiceIntent;
        private bool _isSyncDataServiceStarted = false;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(savedInstanceState);

            global::Xamarin.Forms.Forms.SetFlags("CollectionView_Experimental");
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);
            LoadApplication(new App());

            // service running
            if (savedInstanceState != null)
                _isSyncDataServiceStarted = savedInstanceState.GetBoolean(Constants.SERVICE_STARTED_KEY, false);

            // create service intent
            SyncDataServiceIntent = new Intent(this, typeof(SyncDataService.SyncDataService));
            SyncDataServiceIntent.SetAction(Constants.ACTION_START_SERVICE);

            // start service
            if (!_isSyncDataServiceStarted)
                StartService(SyncDataServiceIntent);
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        protected override void OnNewIntent(Intent intent)
        {
            if (intent == null) return;

            var bundle = intent.Extras;
            if (bundle != null && bundle.ContainsKey(Constants.SERVICE_STARTED_KEY))
            {
                _isSyncDataServiceStarted = true;
            }
        }

        protected override void OnSaveInstanceState(Bundle outState)
        {
            outState.PutBoolean(Constants.SERVICE_STARTED_KEY, _isSyncDataServiceStarted);
            base.OnSaveInstanceState(outState);
        }
    }
}
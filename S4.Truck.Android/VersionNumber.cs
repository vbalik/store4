﻿using Android.Content.PM;
using S4.Truck.Droid;

[assembly: Xamarin.Forms.Dependency(typeof(VersionNumberService))]
namespace S4.Truck.Droid
{
    public class VersionNumberService : IAppVersionNumber
    {
        PackageInfo _appInfo;
        
        public VersionNumberService()
        {
            var context = Android.App.Application.Context;
            _appInfo = context.PackageManager.GetPackageInfo(context.PackageName, 0);
        }

        public string GetVersionNumber()
        {
            return _appInfo.VersionCode.ToString();
        }
    }
}
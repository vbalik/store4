﻿using System;
using System.Timers;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;

namespace S4Truck.Droid.SyncDataService
{
    [Service]
    public class SyncDataService : Service
    {
        static readonly string TAG = typeof(SyncDataService).FullName;
        static readonly string CHANNEL_ID = "S4TruckServiceChannel";

        bool isStarted;
        Handler handler;
        Action syncDataInit;

        public override void OnCreate()
        {
            base.OnCreate();

            handler = new Handler();

            syncDataInit = new Action(() =>
            {
                var timer = new Timer();
                timer.Interval = 15000;
                timer.Elapsed += Timer_Elapsed;
                timer.Start();

                string msg = "Synchronizuji data";
                Log.Debug(TAG, msg);
                Intent i = new Intent(Constants.NOTIFICATION_BROADCAST_ACTION);
                i.PutExtra(Constants.BROADCAST_MESSAGE_KEY, msg);
                //Android.Support.V4.Content.LocalBroadcastManager.GetInstance(this).SendBroadcast(i);
            });
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                var sync = new S4.Truck.Services.SyncDataService();
                sync.Run().Wait();
            }
            catch (Exception)
            {
                isStarted = false;
            }
        }

        public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId)
        {
            if (intent.Action.Equals(Constants.ACTION_START_SERVICE))
            {
                if (isStarted)
                {
                    Log.Info(TAG, "OnStartCommand: The service is already running.");
                }
                else
                {
                    Log.Info(TAG, "OnStartCommand: The service is starting.");
                    RegisterForegroundService();

                    handler.PostDelayed(syncDataInit, Constants.DELAY_SYNC_DATA_INIT);

                    isStarted = true;
                }
            }
            else if (intent.Action.Equals(Constants.ACTION_STOP_SERVICE))
            {
                Log.Info(TAG, "OnStartCommand: The service is stopping.");
                StopForeground(true);
                StopSelf();
                isStarted = false;
            }
            else if (intent.Action.Equals(Constants.ACTION_RESTART_TIMER))
            {
                Log.Info(TAG, "OnStartCommand: Restarting the timer.");
            }

            // This tells Android not to restart the service if it is killed to reclaim resources.
            return StartCommandResult.Sticky;
        }

        public override IBinder OnBind(Intent intent)
        {
            // Return null because this is a pure started service. A hybrid service would return a binder.
            return null;
        }

        public override void OnDestroy()
        {
            // We need to shut things down.
            Log.Debug(TAG, "The TimeStamper has been disposed.");
            Log.Info(TAG, "OnDestroy: The started service is shutting down.");

            // Stop the handler.
            handler.RemoveCallbacks(syncDataInit);

            // Remove the notification from the status bar.
            var notificationManager = (NotificationManager)GetSystemService(NotificationService);
            notificationManager.Cancel(Constants.SERVICE_RUNNING_NOTIFICATION_ID);

            isStarted = false;
            base.OnDestroy();
        }

        void RegisterForegroundService()
        {
            var channel = new NotificationChannel(CHANNEL_ID, "Channel", NotificationImportance.Default)
            {
                Description = "Foreground Service Channel"
            };

            var notificationManager = (NotificationManager)GetSystemService(NotificationService);
            notificationManager.CreateNotificationChannel(channel);

            var notification = new Notification.Builder(this, CHANNEL_ID)
            .SetContentTitle("S4.Truck služba")
            .SetContentText("Po kliknutí zobrazíte stav")
            .SetSmallIcon(Resource.Drawable.abc_btn_radio_material)
            .SetContentIntent(BuildIntentToShowMainActivity())
            .SetOngoing(true)
            //.AddAction(BuildRestartTimerAction())
            //.AddAction(BuildStopServiceAction())
            .Build();

            // Enlist this instance of the service as a foreground service
            StartForeground(Constants.SERVICE_RUNNING_NOTIFICATION_ID, notification);
        }

        PendingIntent BuildIntentToShowMainActivity()
        {
            var notificationIntent = new Intent(this, typeof(MainActivity));
            notificationIntent.SetAction(Constants.ACTION_MAIN_ACTIVITY);
            notificationIntent.SetFlags(ActivityFlags.SingleTop | ActivityFlags.ClearTask);
            notificationIntent.PutExtra(Constants.SERVICE_STARTED_KEY, true);

            var pendingIntent = PendingIntent.GetActivity(this, 0, notificationIntent, PendingIntentFlags.UpdateCurrent);
            return pendingIntent;
        }
    }
}

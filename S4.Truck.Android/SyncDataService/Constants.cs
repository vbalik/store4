﻿using System;

namespace S4Truck.Droid.SyncDataService
{
    public static class Constants
	{
		public const int DELAY_SYNC_DATA_INIT = 1500; // milliseconds
		public const int DELAY_BETWEEN_LOG_MESSAGES = 5000; // milliseconds
		public const int SERVICE_RUNNING_NOTIFICATION_ID = 10000;
		public const string SERVICE_STARTED_KEY = "has_service_been_started";
		public const string BROADCAST_MESSAGE_KEY = "broadcast_message";
		public const string NOTIFICATION_BROADCAST_ACTION = "S4TruckService.Notification.Action";

		public const string ACTION_START_SERVICE = "S4TruckService.action.START_SERVICE";
		public const string ACTION_STOP_SERVICE = "S4TruckService.action.STOP_SERVICE";
		public const string ACTION_RESTART_TIMER = "S4TruckService.action.RESTART_TIMER";
		public const string ACTION_MAIN_ACTIVITY = "S4TruckService.action.MAIN_ACTIVITY";
	}
}

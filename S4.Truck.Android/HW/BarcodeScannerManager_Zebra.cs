﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using S4.Truck;
using Symbol.XamarinEMDK;
using Symbol.XamarinEMDK.Barcode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

#if ZEBRA
[assembly: Xamarin.Forms.Dependency(typeof(S4Truck.Droid.HW.BarcodeScannerManager_Zebra))]
#endif
namespace S4Truck.Droid.HW
{
    // based on https://techdocs.zebra.com/emdk-for-xamarin/6-0/samples/multibarcode1/
    public class BarcodeScannerManager_Zebra : Java.Lang.Object, EMDKManager.IEMDKListener, S4.Truck.IBarcodeScannerManager
    {
        // Declare a variable to store EMDKManager object
        private EMDKManager emdkManager = null;

        // Declare a variable to store BarcodeManager object
        private BarcodeManager barcodeManager = null;

        // Declare a variable to store Scanner object
        private Scanner scanner = null;

        private bool initReady = false;
        private IList<ScannerInfo> scannerList = null;
        private string barcodeScannerName = "2D Barcode Imager";
        private bool isContinuousMode = true;
        //private bool isHardTrigger = true;
        private string statusString = "";


#region IBarcodeScannerManager

        public bool IsScannerEnabled { get; private set; }

        public event EventHandler<ScanResult> ScanReceived;



        public async Task Init()
        {
            initReady = false;

            // The EMDKManager object will be created and returned in the callback
            EMDKResults results = EMDKManager.GetEMDKManager(Application.Context, this);

            // Check the return status of GetEMDKManager
            if (results.StatusCode != EMDKResults.STATUS_CODE.Success)
            {
                // EMDKManager object initialization failed
                Console.WriteLine("Status: EMDKManager object creation failed.");
            }
            else
            {
                // EMDKManager object initialization succeeded
                Console.WriteLine("Status: EMDKManager object creation succeeded.");
            }

            // wait until OnOpened is done
            int timeoutCnt = 10; // max 500 ms
            do
            {
                await Task.Delay(50);
                timeoutCnt--;
                if (timeoutCnt == 0)
                    throw new TimeoutException();
            } while (!initReady);
        }


        public async Task Shutdown()
        {
            // De-initialize scanner
            DeInitScanner();

            // Clean up the objects created by EMDK manager
            if (barcodeManager != null)
            {
                // Remove connection listener
                barcodeManager = null;
            }

            if (emdkManager != null)
            {
                emdkManager.Release();
                emdkManager = null;
            }

            await Task.CompletedTask;
        }


        public async Task Enable()
        {
            if (scanner == null)
            {
                InitScanner();
            }

            if (scanner != null)
            {
                try
                {
                    scanner.Enable();

                    if (scanner.IsEnabled)
                    {
                        // Submit a new read.
                        scanner.Read();
                    }
                    else
                    {
                        Console.WriteLine("Status: Scanner is not enabled");
                    }
                }
                catch (ScannerException ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
            }

            IsScannerEnabled = true;
            await Task.CompletedTask;
        }


        public async Task Disable()
        {
            IsScannerEnabled = false;

            if (scanner != null)
            {
                try
                {
                    // Cancel the pending read.
                    scanner.CancelRead();

                    scanner.Disable();

                }
                catch (ScannerException ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
            }

            await Task.CompletedTask;
        }

#endregion


#region IEMDKListener

        public void OnOpened(EMDKManager emdkManagerInstance)
        {
            // This callback will be issued when the EMDK is ready to use.
            Console.WriteLine("Status: EMDK open success.");

            this.emdkManager = emdkManagerInstance;

            try
            {
                // Acquire the barcode manager resources
                barcodeManager = (BarcodeManager)emdkManager.GetInstance(EMDKManager.FEATURE_TYPE.Barcode);

                // Enumerate scanner devices
                EnumerateScanners();
            }
            catch (Exception e)
            {
                Console.WriteLine("Status: BarcodeManager object creation failed.");
                Console.WriteLine("Exception:" + e.StackTrace);
            }

            initReady = true;
        }

        public void OnClosed()
        {
            // This callback will be issued when the EMDK closes unexpectedly.

            if (emdkManager != null)
            {
                if (barcodeManager != null)
                {
                    // Remove connection listener
                    barcodeManager = null;
                }

                // Release all the resources
                emdkManager.Release();
                emdkManager = null;
            }

            Console.WriteLine("Status: EMDK closed unexpectedly! Please close and restart the application.");
        }

#endregion


        void scanner_Data(object sender, Scanner.DataEventArgs e)
        {
            ScanDataCollection scanDataCollection = e.P0;

            if ((scanDataCollection != null) && (scanDataCollection.Result == ScannerResults.Success))
            {
                IList<ScanDataCollection.ScanData> scanData = scanDataCollection.GetScanData();

                foreach (ScanDataCollection.ScanData data in scanData)
                {
                    var result = new ScanResult() { TextData = data.Data, CodeType = data.LabelType.ToString() };
                    ScanReceived?.Invoke(sender, result);
                }
            }
        }


        void scanner_Status(object sender, Scanner.StatusEventArgs e)
        {
            StatusData statusData = e.P0;
            StatusData.ScannerStates state = e.P0.State;

            if (state == StatusData.ScannerStates.Idle)
            {
                statusString = "Status: " + statusData.FriendlyName + " is enabled and idle...";
                Console.WriteLine(statusString);

                if (IsScannerEnabled && isContinuousMode)
                {
                    try
                    {
                        // An attempt to use the scanner continuously and rapidly (with a delay < 100 ms between scans) 
                        // may cause the scanner to pause momentarily before resuming the scanning. 
                        // Hence add some delay (>= 100ms) before submitting the next read.
                        try
                        {
                            Thread.Sleep(100);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.StackTrace);
                        }

                        // Submit another read to keep the continuation
                        scanner.Read();
                    }
                    catch (ScannerException ex)
                    {
                        statusString = "Status: " + ex.Message;
                        Console.WriteLine(ex.StackTrace);
                    }
                    catch (NullReferenceException ex)
                    {
                        statusString = "Status: An error has occurred.";
                        Console.WriteLine(ex.StackTrace);
                    }
                }
            }

            else if (state == StatusData.ScannerStates.Waiting)
            {
                statusString = "Status: Scanner is waiting for trigger press...";
            }
            else if (state == StatusData.ScannerStates.Scanning)
            {
                statusString = "Status: Scanning...";
            }
            else if (state == StatusData.ScannerStates.Disabled)
            {
                statusString = "Status: " + statusData.FriendlyName + " is disabled.";
            }
            else if (state == StatusData.ScannerStates.Error)
            {
                statusString = "Status: An error has occurred.";
            }

            Console.WriteLine(statusString);
        }


        private void EnumerateScanners()
        {
            if (barcodeManager != null)
            {
                // Query the supported scanners on the device
                scannerList = barcodeManager.SupportedDevicesInfo;

                if ((scannerList == null) || (scannerList.Count == 0))
                {
                    Console.WriteLine("Status: Failed to get the list of supported scanner devices! Please close and restart the application.");
                }
            }
        }


        private void InitScanner()
        {
            if (scanner == null)
            {
                if ((scannerList != null) && (scannerList.Count > 0))
                {
                    var scannerInfo = scannerList.Single(_ => _.FriendlyName == barcodeScannerName);
                    // Get new scanner device based on the selected index
                    scanner = barcodeManager.GetDevice(scannerInfo);
                }
                else
                {
                    Console.WriteLine("Status: Failed to get the specified scanner device! Please close and restart the application.");
                    return;
                }

                if (scanner != null)
                {
                    // Add data listener
                    scanner.Data += scanner_Data;

                    // Add status listener
                    scanner.Status += scanner_Status;
                   
                    try
                    {
                        // Enable the scanner
                        scanner.Enable();
                    }
                    catch (ScannerException e)
                    {
                        Console.WriteLine(e.StackTrace);
                    }

                    SetScannerParams();
                }
                else
                {
                    Console.WriteLine("Status: Failed to initialize the scanner device.");
                }
            }
        }


        private void DeInitScanner()
        {
            if (scanner != null)
            {
                try
                {
                    // Cancel if there is any pending read
                    scanner.CancelRead();

                    // Disable the scanner 
                    if(scanner.IsEnabled)
                        scanner.Disable();
                }
                catch (ScannerException e)
                {
                    Console.WriteLine("Status: " + e.Message);
                    Console.WriteLine(e.StackTrace);
                }

                // Remove data listener
                scanner.Data -= scanner_Data;

                // Remove status listener
                scanner.Status -= scanner_Status;

                try
                {
                    // Release the scanner
                    scanner.Release();
                }
                catch (ScannerException e)
                {
                    Console.WriteLine("Status: " + e.Message);
                    Console.WriteLine(e.StackTrace);
                }

                scanner = null;
            }
        }

        //private void SetTrigger()
        //{
        //    if (scanner == null)
        //    {
        //        InitScanner();
        //    }

        //    if (scanner != null)
        //    {
        //        if (isHardTrigger)
        //            scanner.TriggerType = Scanner.TriggerTypes.Hard;
        //        else
        //            scanner.TriggerType = Scanner.TriggerTypes.SoftAlways;

        //    }
        //}

        private void SetScannerParams()
        {
            if ((scanner != null) && (scanner.IsEnabled))
            {
                try
                {
                    // Config object should be taken out before changing.
                    ScannerConfig config = scanner.GetConfig();

                    config.DecoderParams.Ean8.Enabled = true;
                    config.DecoderParams.Ean13.Enabled = true;
                    config.DecoderParams.Code39.Enabled = true;
                    config.DecoderParams.Code128.Enabled = true;
                    config.DecoderParams.QrCode.Enabled = true;
                    config.DecoderParams.DataMatrix.Enabled = true;

                    // switch off 'scanned' sound
                    config.ScanParams.DecodeAudioFeedbackMode = ScannerConfig.DecodeAudioFeedbackMode.Disable;
                    config.ScanParams.DecodeAudioFeedbackUri = "";

                    // Should be assigned back to the property to get the changes to the lower layers.
                    scanner.SetConfig(config);
                }
                catch (ScannerException e)
                {
                    Console.WriteLine(e.StackTrace);
                }
            }
        }
    }
}
﻿using Honeywell.AIDC.CrossPlatform;
using S4.Truck;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

#if HONEYWELL
[assembly: Xamarin.Forms.Dependency(typeof(S4Truck.Droid.HW.BarcodeScannerManager_Honeywell))]
#endif
namespace S4Truck.Droid.HW
{
    public class BarcodeScannerManager_Honeywell : IBarcodeScannerManager
    {
        private readonly SynchronizationContext mUIContext = SynchronizationContext.Current;
        private BarcodeReader _barcodeReader;


        public bool IsScannerEnabled { get; private set; }

        public event EventHandler<ScanResult> ScanReceived;

        public async Task Init()
        {
            if (_barcodeReader == null)
                _barcodeReader = await GetBarCodeReader();
            if (_barcodeReader != null && !_barcodeReader.IsReaderOpened)
            {
                var result = await _barcodeReader.OpenAsync();
                if (result.Code == BarcodeReader.Result.Codes.SUCCESS ||
                    result.Code == BarcodeReader.Result.Codes.READER_ALREADY_OPENED)
                {
                    SetScannerAndSymbologySettings();
                }
                else
                {
                    throw new Exception($"OpenAsync failed, Code:{ result.Code } Message:{result.Message}");
                }
            }
        }


        public async Task Shutdown()
        {
            var result = await _barcodeReader.CloseAsync();
            if (result.Code != BarcodeReader.Result.Codes.SUCCESS)
            {
                throw new Exception($"CloseAsync failed, Code:{ result.Code } Message:{result.Message}");
            }
        }


        public async Task Enable()
        {
            var result = await _barcodeReader.EnableAsync(true); // Enables or disables barcode reader
            if (result.Code != BarcodeReader.Result.Codes.SUCCESS)
            {
                throw new Exception($"EnableAsync failed, Code:{ result.Code } Message:{result.Message}");
            }
            IsScannerEnabled = true;
        }


        public async Task Disable()
        {
            if (_barcodeReader != null && _barcodeReader.IsReaderOpened)
            {
                BarcodeReader.Result result = await _barcodeReader.EnableAsync(false); // Enables or disables barcode reader
                if (result.Code != BarcodeReader.Result.Codes.SUCCESS)
                {
                    throw new Exception($"EnableAsync failed, Code:{ result.Code } Message:{result.Message}");
                }
            }
            IsScannerEnabled = false;
        }


        private async Task<BarcodeReader> GetBarCodeReader()
        {
            // Queries the list of readers that are connected to the mobile computer.
            BarcodeReaderInfo readerInfo = (await BarcodeReader.GetConnectedBarcodeReaders()).FirstOrDefault();
            if (readerInfo != null)
            {
                var barcodeReader = new BarcodeReader(readerInfo.ScannerName);
                barcodeReader.BarcodeDataReady += BarcodeReader_BarcodeDataReady;

                return barcodeReader;
            }
            return null;
        }


        private async void SetScannerAndSymbologySettings()
        {
            if (_barcodeReader.IsReaderOpened)
            {
                Dictionary<string, object> settings = new Dictionary<string, object>()
                    {
                        {_barcodeReader.SettingKeys.TriggerScanMode, _barcodeReader.SettingValues.TriggerScanMode_OneShot },
                        {_barcodeReader.SettingKeys.Code128Enabled, true },
                        {_barcodeReader.SettingKeys.Code39Enabled, true },
                        {_barcodeReader.SettingKeys.Ean8Enabled, true },
                        {_barcodeReader.SettingKeys.Ean8CheckDigitTransmitEnabled, true },
                        {_barcodeReader.SettingKeys.Ean13Enabled, true },
                        {_barcodeReader.SettingKeys.Ean13CheckDigitTransmitEnabled, true },
                        {_barcodeReader.SettingKeys.UpcAEnable, true },
                        {_barcodeReader.SettingKeys.UpcACheckDigitTransmitEnabled, true },
                        {_barcodeReader.SettingKeys.UpcATranslateEan13, true },
                        {_barcodeReader.SettingKeys.Interleaved25Enabled, true },
                        {_barcodeReader.SettingKeys.Interleaved25MaximumLength, 100 },
                        {_barcodeReader.SettingKeys.Postal2DMode, _barcodeReader.SettingValues.Postal2DMode_Usps }
                    };

                BarcodeReader.Result result = await _barcodeReader.SetAsync(settings);
                if (result.Code != BarcodeReader.Result.Codes.SUCCESS)
                {
                    throw new Exception($"Symbology settings failed, Code:{ result.Code } Message:{result.Message}");
                }
            }
        }


        private void BarcodeReader_BarcodeDataReady(object sender, BarcodeDataArgs e)
        {
            mUIContext.Post(_ =>
            {
                OnCodeScanned(e);
            }, null);
        }


        private void OnCodeScanned(BarcodeDataArgs e)
        {
            var result = new ScanResult() { TextData = e.Data, CodeType = e.SymbologyName.ToString() };
            ScanReceived?.Invoke(this, result);
        }
    }
}
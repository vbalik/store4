﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//[assembly: Xamarin.Forms.Dependency(typeof(S4Truck.Droid.HW.BarcodeScannerManager_Mock))]
namespace S4Truck.Droid.HW
{
    public class BarcodeScannerManager_Mock : S4.Truck.IBarcodeScannerManager
    {
        public bool IsScannerEnabled { get; private set; }

        public event EventHandler<S4.Truck.ScanResult> ScanReceived;


        public async Task Disable()
        {
            IsScannerEnabled = false;
            await Task.CompletedTask;
        }


        public async Task Enable()
        {
            IsScannerEnabled = true;

            _ = Task.Factory.StartNew(() =>
              {
                  ScanReceived?.Invoke(this, new S4.Truck.ScanResult() { TextData = "DL/21/1234", CodeType = "Code128" });
              });

            await Task.CompletedTask;
        }


        public async Task Init()
        {
            // do nothing
            await Task.CompletedTask;
        }


        public async Task Shutdown()
        {
            // do nothing
            await Task.CompletedTask;
        }
    }
}
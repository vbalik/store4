using System;
using TestHelpers;
using Xunit;

namespace S4.DAL.Tests
{
    [Collection("DALTests")]
    public class PositionDALTests
    {
        protected DatabaseFixture _fixture;


        public PositionDALTests(DatabaseFixture fixture)
        {
            this._fixture = fixture;
            Core.Data.ConnectionHelper.ConnectionString = fixture.ConnectionString;
        }



        [Fact]
        public void GetModel()
        {
            var dal = new PositionDAL();
            var model = dal.GetModel(1);

            Assert.NotNull(model);
            Assert.Equal(1, model.PositionID);
            Assert.NotEmpty(model.PositionSections);
        }


        [Fact]
        public void ScanByPosCode()
        {
            var dal = new PositionDAL();
            var list = dal.ScanByPosCode("123");

            Assert.NotNull(list);
            Assert.NotEmpty(list);
            Assert.Equal(1, list[0].PositionID);
            Assert.NotEmpty(list[0].PositionSections);
        }


        [Fact]
        public void ListByCategory()
        {
            var dal = new PositionDAL();
            var list = dal.ListByCategory(Entities.Position.PositionCategoryEnum.Dispatch);

            Assert.NotNull(list);
            Assert.NotEmpty(list);
        }
    }
}

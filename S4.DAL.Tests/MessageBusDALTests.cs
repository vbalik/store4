using System;
using TestHelpers;
using Xunit;

namespace S4.DAL.Tests
{
    [Collection("DALTests")]
    public class MessageBusDALTests
    {
        protected DatabaseFixture _fixture;


        public MessageBusDALTests(DatabaseFixture fixture)
        {
            this._fixture = fixture;
            Core.Data.ConnectionHelper.ConnectionString = fixture.ConnectionString;
        }

        [Fact]
        public void MessageBusByStatusCount()
        {
            var dal = new MessageBusDAL();
            var result = dal.Last10DaysCount();
            Assert.True(result.Count == 1);
        }
        
        [Fact]
        public void AddDoneDirection2MessageBus()
        {
            var directionID = 0;
            var dal = new MessageBusDAL();

            var result = dal.TryInsertMessageBusStatusNew(directionID, Entities.MessageBus.MessageBusTypeEnum.ExportDirection, "test");
            Assert.True(result == true);
                       
        }     
        
    }
}

using System;
using TestHelpers;
using Xunit;

namespace S4.DAL.Tests
{
    [Collection("DALTests")]
    public class DirectionAssignDALTests
    {
        protected DatabaseFixture _fixture;


        public DirectionAssignDALTests(DatabaseFixture fixture)
        {
            this._fixture = fixture;
            Core.Data.ConnectionHelper.ConnectionString = fixture.ConnectionString;
        }



        [Fact]
        public void ScanAssignments()
        {
            var dal = new DirectionAssignDAL();
            var result = dal.ScanAssignments();

            Assert.NotNull(result);
        }


        [Fact]
        public void ScanAssignments_workerID()
        {
            var dal = new DirectionAssignDAL();
            var result = dal.ScanAssignments("123");

            Assert.NotNull(result);
        }


        [Fact]
        public void Statistics()
        {
            var dal = new DirectionAssignDAL();
            var result = dal.Statistics(100);

            Assert.NotNull(result);
        }
    }
}

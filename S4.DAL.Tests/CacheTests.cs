﻿using S4.DAL.Cache;
using S4.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using TestHelpers;
using Xunit;

namespace S4.DAL.Tests
{
    [Collection("DALTests")]
    public class CacheTests
    {
        protected DatabaseFixture _fixture;

        public CacheTests(DatabaseFixture fixture)
        {
            this._fixture = fixture;
            Core.Data.ConnectionHelper.ConnectionString = fixture.ConnectionString;
        }


        [Fact]
        public void ArticleModelCache()
        {
            var cache = new ArticleModelCache();
            var x = cache.GetItem(100);

            Assert.NotNull(x);
            Assert.NotNull(x.Packings);
        }


        [Fact]
        public void ArticlePackingCache()
        {
            var cache = new ArticlePackingCache();
            var x = cache.GetItem(1001);

            Assert.NotNull(x);
        }


        [Fact]
        public void ManufactCache()
        {
            var cache = new ManufactCache();
            var x = cache.GetItem("ONTX_123");

            Assert.NotNull(x);
        }


        [Fact]
        public void PartnerCache()
        {
            var cache = new PartnerCache();
            var x = cache.GetItem(0);

            Assert.NotNull(x);
        }


        [Fact]
        public void PositionCache()
        {
            var cache = new PositionCache();
            var x = cache.GetItem(1);

            Assert.NotNull(x);
        }


        [Fact]
        public void PrefixCache()
        {
            var cache = new PrefixCache();
            var x = cache.GetItem("DI", "PR");

            Assert.NotNull(x);
        }


        [Fact]
        public void PrinterLocationCache()
        {
            var cache = new PrinterLocationCache();
            var x = cache.GetItem(1);

            Assert.NotNull(x);
        }

        [Fact]
        public void ReportCache()
        {
            var cache = new ReportCache();
            var x = cache.GetItem(1);

            Assert.NotNull(x);
        }

        [Fact]
        public void ReportCache_ByType()
        {
            var cache = new ReportCache_ByType();
            var x = cache.GetItem(2, "packingLabel");

            Assert.NotNull(x);
        }

        [Fact]
        public void SectionModelCache()
        {
            var cache = new SectionModelCache();
            var x = cache.GetItem("SEC");

            Assert.NotNull(x);
        }


        [Fact]
        public void PositionByCategoryModelCache()
        {
            var cache = new PositionByCategoryModelCache();
            var x = cache.GetItem(Position.PositionCategoryEnum.Dispatch);

            Assert.NotNull(x);
        }
    }
}

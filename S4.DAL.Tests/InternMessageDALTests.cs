using System;
using TestHelpers;
using Xunit;

namespace S4.DAL.Tests
{
    [Collection("DALTests")]
    public class InternMessageDALTests
    {
        protected DatabaseFixture _fixture;


        public InternMessageDALTests(DatabaseFixture fixture)
        {
            this._fixture = fixture;
            Core.Data.ConnectionHelper.ConnectionString = fixture.ConnectionString;
        }



        [Fact]
        public void Last20()
        {
            var dal = new InternMessageDAL();
            var result = dal.Last20();

            Assert.NotNull(result);
        }        
    }
}

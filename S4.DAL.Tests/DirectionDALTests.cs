using System;
using TestHelpers;
using Xunit;

namespace S4.DAL.Tests
{
    [Collection("DALTests")]
    public class DirectionDALTests
    {
        protected DatabaseFixture _fixture;
        private const int DIRECTION_ID = 17;

        public DirectionDALTests(DatabaseFixture fixture)
        {
            this._fixture = fixture;
            Core.Data.ConnectionHelper.ConnectionString = fixture.ConnectionString;
        }



        [Fact]
        public void GetDirectionAllData()
        {
            var dal = new DirectionDAL();
            var result = dal.GetDirectionAllData(DIRECTION_ID);

            Assert.NotNull(result);
            Assert.True(result.Assigns.Count == 1);
            Assert.True(result.History.Count > 0);
            Assert.True(result.Items.Count == 1);
            Assert.NotNull(result.XtraData);
        }


        [Fact]
        public void GetDirectionXtraData()
        {
            var dal = new DirectionDAL();
            var result = dal.GetDirectionXtraData(DIRECTION_ID);

            Assert.NotNull(result);
            Assert.NotNull(result.XtraData);
        }


        [Fact]
        public void AddDirectionHistory()
        {
            var h = new Entities.DirectionHistory()
            {
                DirectionID = DIRECTION_ID,
                DocPosition = -1,
                EntryDateTime = DateTime.Now,
                EventCode = "1",
                EntryUserID = "1"
            };

            var dal = new DirectionDAL();
            var result = dal.AddDirectionHistory(h);

            Assert.True(result > 0);
        }


        [Fact]
        public void GetStatus()
        {
            var dal = new DirectionDAL();
            var result = dal.GetStatus(DIRECTION_ID);

            Assert.Equal("DCHE", result);
        }


        [Fact]
        public void GetItemsStatus()
        {
            var dal = new DirectionDAL();
            var result = dal.GetItemsStatus(DIRECTION_ID);

            Assert.NotNull(result);
            Assert.NotEmpty(result);
        }

        [Theory]
        [InlineData("001231231203")]
        [InlineData("91231231231")]
        [InlineData("091231231231")]
        [InlineData("01231231203")]
        [InlineData("026114")]
        [InlineData("251100286")]
        [InlineData("0251100286")]
        public void GetDirectionsByAbraDocID_Success(string docID)
        {
            var result = new DirectionDAL().GetDirectionsByAbraDocID(docID);

            Assert.NotNull(result);
            Assert.NotEmpty(result);
        }
    }
}

using System;
using TestHelpers;
using Xunit;

namespace S4.DAL.Tests
{
    [Collection("DALTests")]
    public class WorkerDALTests
    {
        protected DatabaseFixture _fixture;


        public WorkerDALTests(DatabaseFixture fixture)
        {
            this._fixture = fixture;
            Core.Data.ConnectionHelper.ConnectionString = fixture.ConnectionString;
        }



        [Fact]
        public void GetByLogon()
        {
            var dal = new WorkerDAL();
            var model = dal.GetByLogon("worker2");

            Assert.NotNull(model);
            Assert.Equal("worker2", model.WorkerLogon);
        }


        [Fact]
        public void ListByWorkerClass()
        {
            var dal = new WorkerDAL();
            var list = dal.ListByWorkerClass(Entities.Worker.WorkerClassEnum.CanDispatch);

            Assert.NotNull(list);
            Assert.NotEmpty(list);
        }
    }
}

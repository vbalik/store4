using System;
using TestHelpers;
using Xunit;

namespace S4.DAL.Tests
{
    [Collection("DALTests")]
    public class ArticleDALTests
    {
        protected DatabaseFixture _fixture;


        public ArticleDALTests(DatabaseFixture fixture)
        {
            this._fixture = fixture;
            Core.Data.ConnectionHelper.ConnectionString = fixture.ConnectionString;
        }



        [Fact]
        public void GetModel()
        {
            var dal = new ArticleDAL();
            var model = dal.GetModel(100);

            Assert.NotNull(model);
            Assert.Equal(100, model.ArticleID);
            Assert.NotEmpty(model.Packings);
        }


        [Fact]
        public void ScanByArticleCode()
        {
            var dal = new ArticleDAL();
            var list = dal.ScanByArticleCode("X123", true);

            Assert.NotNull(list);
            Assert.NotEmpty(list);
            Assert.Equal(100, list[0].ArticleID);
            Assert.NotEmpty(list[0].Packings);
        }


        [Fact]
        public void ScanByArticleDesc()
        {
            var dal = new ArticleDAL();
            var list = dal.ScanByArticleDesc("Zbo�� 123", true);

            Assert.NotNull(list);
            Assert.NotEmpty(list);
            Assert.Equal(100, list[0].ArticleID);
            Assert.NotEmpty(list[0].Packings);
        }



        [Fact]
        public void ScanByBarCode()
        {
            var dal = new ArticleDAL();
            var list = dal.ScanByBarCode("BARCODE1", true);

            Assert.NotNull(list);
            Assert.NotEmpty(list);
            Assert.Equal(100, list[0].ArticleID);
            Assert.NotEmpty(list[0].Packings);
        }
    }
}

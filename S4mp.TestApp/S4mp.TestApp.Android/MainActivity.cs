﻿using System;
using System.Collections.Generic;

using Android;
using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace S4mp.Droid
{
    [Activity(Label = "S4mp.", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        internal static MainActivity Instance { get; set; }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(savedInstanceState);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);
            ZXing.Net.Mobile.Forms.Android.Platform.Init();

#if !DATALOGIC && !HONEYWELL
            ZXing.Net.Mobile.Forms.Android.Platform.Init();
            ZXing.Mobile.MobileBarcodeScanner.Initialize(Application);
#endif

            CheckPermissions();

            Instance = this;

            LoadApplication(new App());
        }

        private void CheckPermissions()
        {
            if ((int)Build.VERSION.SdkInt < 23)
            {
                return;
            }

            string[] permissions = new string[]
            {
                Manifest.Permission.Camera,
                Manifest.Permission.Flashlight,
            };
            var permissionsForRequest = new List<string>();

            foreach (var permission in permissions)
            {
                if (CheckSelfPermission(permission) != (int)Permission.Granted)
                {
                    permissionsForRequest.Add(permission);
                }
            }

            if (permissionsForRequest.Count == 0)
                return;

            RequestPermissions(permissionsForRequest.ToArray(), 1);
        }
    }
}
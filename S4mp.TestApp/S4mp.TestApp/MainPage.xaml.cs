﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace S4mp
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private async void btnCScanner_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Pages.BarCodeScanner());
        }

        private async void BtnScanner_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Pages.ScannerPage());
        }
    }
}

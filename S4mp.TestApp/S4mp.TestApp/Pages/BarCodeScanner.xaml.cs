﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace S4mp.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class BarCodeScanner : ContentPage
	{
        private ZXing.Mobile.MobileBarcodeScanner _scanner;
        public BarCodeScanner ()
		{
			InitializeComponent ();
            
            _scanner = new ZXing.Mobile.MobileBarcodeScanner();
        }

        public ZXing.Mobile.MobileBarcodeScanner Scanner { get => _scanner; }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            var result = await _scanner.Scan();

            HandleScanResult(result);
        }

        private void HandleScanResult(ZXing.Result result)
        {
            string msg = "";

            if (result != null && !string.IsNullOrEmpty(result.Text))
                msg = "Found Barcode: " + result.Text;
            else
                msg = "Scanning Canceled!";

            lbResult.Text = msg;
        }

        private void Button_Clicked_1(object sender, EventArgs e)
        {
            _scanner.ToggleTorch();
        }
    }
}
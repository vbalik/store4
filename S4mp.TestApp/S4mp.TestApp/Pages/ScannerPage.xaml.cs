﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using S4mp.Controls;

namespace S4mp.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ScannerPage : ContentPage
	{
        private IScannerResolver _scanner;

        public ScannerPage ()
		{
			InitializeComponent ();

            var dlg = new Dialogs.ScanDialog();
            dlg.Closed += Dlg_Closed;
            Navigation.PushModalAsync(dlg);

            _scanner = DependencyService.Get<IScannerResolver>();
        }

        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);

            if (propertyName.Equals("Renderer", StringComparison.OrdinalIgnoreCase))
            {
                var rr = DependencyService.Get<IRendererResolver>();
                if (!rr.HasRenderer(this))
                    _scanner.Init(false, _scanner_Scanned);
            }
        }

        private void _scanner_Scanned(object sender, Controls.Scanner.BarCodeTypeEnum e)
        {
            DisplayEAN((sender as IScannerResolver).Text);
        }

        private void Dlg_Closed(object sender, Controls.Common.ButtonsBar.ButtonsEnum e)
        {
            if (e == Controls.Common.ButtonsBar.ButtonsEnum.OK)
                DisplayEAN((sender as Dialogs.ScanDialog).Text);

            _scanner = DependencyService.Get<IScannerResolver>();
            _scanner.Init(true, _scanner_Scanned);
        }

        private async void DisplayEAN(string ean)
        {
            await DisplayAlert("EAN", ean, "OK");
        }
    }
}
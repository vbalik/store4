﻿using System;
using System.Collections.Generic;
using System.Text;

using Xamarin.Forms;

namespace S4mp.Dialogs
{
    public class ScanDialog : Controls.Dialogs.EditFormDialogBase
    {
        private Controls.Scanner.ScannerView _scanner;

        public string Text { get; set; }

        protected override bool OnBackButtonPressed()
        {
            OnButtonClicked(Controls.Common.ButtonsBar.ButtonsEnum.Cancel);
            return base.OnBackButtonPressed();
        }

        protected override Controls.Common.ButtonsBar CreateButtonsBar()
        {
            return new Controls.Common.ButtonsBar(Controls.Common.ButtonsBar.ButtonsEnum.Cancel, Controls.Common.ButtonsBar.ButtonsEnum.OK);
        }

        protected override void Init(bool scrolled)
        {
            base.Init(scrolled);

            _scanner = new Controls.Scanner.ScannerView();
            _scanner.ValueScannedCmd = new Command(() => 
            {
                Text = _scanner.Text;
                OnButtonClicked(Controls.Common.ButtonsBar.ButtonsEnum.OK);
            });

            Content = new StackLayout()
            {
                Children =
                {
                    _scanner,
                    ButtonsBar
                }
            };
        }
    }
}

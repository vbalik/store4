﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Essentials;

namespace S4.Truck.Helpers
{
    public class SettingsProxy
    {
        private const string SETUP_DONE = "SetupDone";
        public bool SetupDone
        {
            get => Preferences.Get(SETUP_DONE, false);
            set => Preferences.Set(SETUP_DONE, value);
        }

        private const string USER_ID = "UserID";
        public string UserID
        {
            get => Preferences.Get(USER_ID, null);
            set => Preferences.Set(USER_ID, value);
        }

        private const string TRUCK_REGIST = "TruckRegist";
        public string TruckRegist
        {
            get => Preferences.Get(TRUCK_REGIST, null);
            set => Preferences.Set(TRUCK_REGIST, value);
        }

        private const string ENDPOINT_SERVER = "EndpointServer";
        public string EndpointServer
        {
            get => Preferences.Get(ENDPOINT_SERVER, null);
            set => Preferences.Set(ENDPOINT_SERVER, value);
        }
    }
}

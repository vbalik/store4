﻿using S4.Truck.Core.Common;
using System;
using System.Threading.Tasks;

namespace S4.Truck.Core.Helpers
{
    public static class ViewHelpers
    {
        /// <summary>
        /// Make red border on entry element
        /// </summary>
        public static async void Entry_FrameError(ErrorFrame errorFrame)
        {
            errorFrame.HasError(true);
            await Task.Delay(2000);
            errorFrame.HasError(false);
        }
    }
}

﻿using S4.SharedLib;
using S4.SharedLib.RemoteAction;
using System;
using System.Text.RegularExpressions;

namespace S4.Truck.Core.Helpers
{
    public static class ValidationHelpers
    {
        /// <summary>
        /// Valid DL/20/222; 20/222
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string DocInfoValidation(string value)
        {
            if (value == null) return null;

            var values = value.Split('/');
            if (values.Length != 3) return null;

            var result = true;
            
            // part 1
            if (values[0].Length > 4)
                result = false;
            foreach (var chr in values[0].ToCharArray())
                if (!char.IsLetterOrDigit(chr))
                    result = false;

            // parts 2 & 3
            for (int i = 1; i < values.Length; i++)
            {
                if (!int.TryParse(values[i], out _))
                    result = false;
            }

            return result ? value.ToUpper() : null;
        }


        public static (bool isOK, DocIDSourceEnum docIDSource, string docID, string docInfo) TryParseScan(string textData, string codeType)
        {
            // QRCode variant
            var (isOK, docID, docInfo) = TryParseScan_QRCode(textData, codeType);
            if (isOK)
                return (isOK, DocIDSourceEnum.DirectionID, docID, docInfo);

            // docInfo variant
            (isOK, docID, docInfo) = TryParseScan_DocInfo(textData, codeType);
            if (isOK)
                return (isOK, DocIDSourceEnum.DocInfo, docID, docInfo);

            if(Regex.IsMatch(textData, "^[a-zA-Z0-9]+$"))
                return (true, DocIDSourceEnum.AbraID, textData, textData);

            // no success
            return (false, DocIDSourceEnum.DocInfo, null, null);
        }

        public static (bool isOK, string docID, string docInfo) TryParseScan_DocInfo(string textData, string codeType)
        {
            var parsed = DocInfoValidation(textData);
            if (parsed != null)
                return (true, parsed, parsed);
            else
                return (false, null, null);
        }

        public static (bool isOK, string docID, string docInfo) TryParseScan_QRCode(string textData, string codeType)
        {
            var (parsed, qrCode) = DocQRCode.TryParse(textData);
            if (parsed)
                return (true, qrCode.DocID, qrCode.DocInfo);
            else
                return (false, null, null);
        }
    }
}

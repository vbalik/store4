﻿using System.Threading.Tasks;

namespace S4.Truck.Core.Clients
{
    public interface IBaseClient
    {
        Task<HttpResponse<T>> GetAsync<T>(string url);

        Task<HttpResponse<T>> GetAsync<T>(string url, dynamic id);

        Task<HttpResponse<U>> PostAsync<T, U>(string url, T model);
    }
}

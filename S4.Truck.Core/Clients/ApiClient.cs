﻿using Newtonsoft.Json;
using S4.SharedLib.S4Truck;
using System.Net.Http;
using System.Threading.Tasks;

namespace S4.Truck.Core.Clients
{
    public class ApiClient : BaseClient, IApiClient
    {
        public async Task<HttpResponse<SetupDataModel>> GetSetupDataAsync()
        {
            try
            {
                return await base.GetAsync<SetupDataModel>("/api/external/remoteaction/setupdata");
            }
            catch (JsonException e)
            {
                throw new HttpRequestException("Can not parse response body");
            }

        }
    }
}

﻿using S4.SharedLib.S4Truck;
using System.Threading.Tasks;

namespace S4.Truck.Core.Clients
{
    public interface IApiClient
    {
        void SetClientBaseUrl(string url);
        Task<HttpResponse<SetupDataModel>> GetSetupDataAsync();
    }
}

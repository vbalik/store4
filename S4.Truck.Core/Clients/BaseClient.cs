﻿using Newtonsoft.Json;
using S4.SharedLib.Security;
using S4.Truck.Helpers;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace S4.Truck.Core.Clients
{
    public class BaseClient : IBaseClient
    {
        private HttpClient _client;

        public BaseClient()
        {
            var handler = new HttpClientHandler();

            // allow self-signed certificate
            handler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => true;

            _client = new HttpClient(handler);

            _client.BaseAddress = new Uri($"{(new SettingsProxy().EndpointServer ?? "http://empty")}/");
        }

        bool IsConnected => Connectivity.NetworkAccess == NetworkAccess.Internet;

        public void SetClientBaseUrl(string stringURI) => _client.BaseAddress = new Uri(stringURI);

        public async Task<HttpResponse<T>> GetAsync<T>(string url)
        {
            if (!IsConnected || string.IsNullOrEmpty(url)) return new HttpResponse<T> { StatusCode = HttpStatusCode.ServiceUnavailable };

            _SetDefaultAuthorizationHeaders(string.Empty, DateTime.Now);

            var response = await _client.GetAsync(url);

            return new HttpResponse<T>
            {
                Headers = response.Headers,
                StatusCode = response.StatusCode,
                Data = await Task.Run(() => JsonConvert.DeserializeObject<T>(response.Content.ReadAsStringAsync().Result))
            };
        }

        public async Task<HttpResponse<T>> GetAsync<T>(string url, dynamic id)
        {
            if (!IsConnected || string.IsNullOrEmpty(url)) return new HttpResponse<T> { StatusCode = HttpStatusCode.ServiceUnavailable };

            _SetDefaultAuthorizationHeaders(string.Empty, DateTime.Now);

            var response = await _client.GetAsync($"{url}/{id}");

            return new HttpResponse<T>
            {
                Headers = response.Headers,
                StatusCode = response.StatusCode,
                Data = await Task.Run(() => JsonConvert.DeserializeObject<T>(response.Content.ReadAsStringAsync().Result))
            };
        }

        public async Task<HttpResponse<U>> PostAsync<T, U>(string url, T model)
        {
            if (!IsConnected || string.IsNullOrEmpty(url)) return new HttpResponse<U> { StatusCode = HttpStatusCode.ServiceUnavailable };

            var json = JsonConvert.SerializeObject(model);

            _SetDefaultAuthorizationHeaders(json, DateTime.Now);

            var response = await _client.PostAsync(url, new StringContent(json, Encoding.UTF8, "application/json"));

            return new HttpResponse<U>
            {
                Headers = response.Headers,
                StatusCode = response.StatusCode,
                Data = await Task.Run(() => JsonConvert.DeserializeObject<U>(response.Content.ReadAsStringAsync().Result))
            };
        }

        /// <summary>
        /// Set default authorization headers
        /// </summary>
        private void _SetDefaultAuthorizationHeaders(string apiAuthorizationKey, DateTime dateTime)
        {
            _client.DefaultRequestHeaders.Remove("apiAuthorization");
            _client.DefaultRequestHeaders.Remove("datetime");

            _client.DefaultRequestHeaders.Add("apiAuthorization", RemoteAccessAuthorization.CreateKey(apiAuthorizationKey, dateTime));
            _client.DefaultRequestHeaders.Add("datetime", dateTime.ToString("O"));
        }
    }

    public class HttpResponse<T>
    {
        public HttpResponseHeaders Headers { get; set; }
        public HttpStatusCode StatusCode { get; set; }
        public T Data { get; set; }
    }
}
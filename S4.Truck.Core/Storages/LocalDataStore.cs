﻿using S4.SharedLib.RemoteAction;
using S4.Truck.Core.Entities;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4Truck.Core.Storages
{
    public class LocalDataStore : IDataStore
    {
        static SQLiteAsyncConnection Database;

        public LocalDataStore(string dbPath = null)
        {
            Database = new SQLiteAsyncConnection(dbPath ?? LocalDataStoreConstants.DatabasePath, LocalDataStoreConstants.Flags, true);
        }

        public LocalDataStore()
        {
            Database = new SQLiteAsyncConnection(LocalDataStoreConstants.DatabasePath, LocalDataStoreConstants.Flags, true);
        }

        public static void CheckAndCreateDatabase(string dbPath = null)
        {
            if (!System.IO.File.Exists(dbPath ?? LocalDataStoreConstants.DatabasePath))
            {
                var conn = new SQLiteAsyncConnection(dbPath ?? LocalDataStoreConstants.DatabasePath);
                conn.CreateTableAsync<DirectionAction>().Wait();
                conn.CreateTableAsync<DirectionInProgress>().Wait();
                conn.CreateTableAsync<Truck>().Wait();
            }
            else
            {
                // migrations from ver.7 to 8
                using (var db = new SQLiteConnection(dbPath ?? LocalDataStoreConstants.DatabasePath))
                {
                    var partnerAddressExist = db.GetTableInfo(nameof(DirectionAction)).Where(_=>_.Name == nameof(DirectionAction.PartnerAddress)).Count();

                    if(partnerAddressExist == 0)
                        db.Execute("ALTER TABLE DirectionAction ADD COLUMN PartnerAddress VARCHAR(255)");
                }
            }
        }

        public async Task<bool> AddDirectionActionAsync(DirectionAction item)
        {
            item.DocInfo = item.DocInfo.ToUpperInvariant();
            item.DocID = item.DocID.ToUpperInvariant();

            // check if exists
            var savedItem = await Database.Table<DirectionAction>().FirstOrDefaultAsync(_ => _.ActionType == item.ActionType
                && _.DocIDSource == item.DocIDSource
                && _.DocID == item.DocID
                && _.DocInfo == item.DocInfo
                && !_.IsCanceled);

            if (savedItem != null)
                return false;

            var result = await Database.InsertAsync(item);
            return result > 0;
        }

        public async Task<bool> CancelDirectionActionAsync(DirectionAction item)
        {
            int result = 0;

            var direction = await Database.Table<DirectionAction>().FirstOrDefaultAsync(_ => _.Id == item.Id);

            if (!direction.IsSynced)
                result = await Database.DeleteAsync(item);
            else
            {
                item.IsCanceled = true;
                result = await Database.UpdateAsync(item);
            }

            return result > 0;
        }

        public Task<DirectionAction> GetDirectionActionAsync(int id)
        {
            return Database.Table<DirectionAction>().Where(_ => _.Id == id).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<DirectionAction>> GetDirectionActionsAsync(ActionTypeEnum type)
        {
            return await Database.Table<DirectionAction>()
                .Where(_ => _.ActionType == type)
                .ToListAsync();
        }

        public async Task<bool> UpdateDirectionActionAsync(DirectionAction item)
        {
            var result = await Database.UpdateAsync(item);
            return result > 0;
        }

        public async Task<int> CountDirectionActionsAsync()
        {
            return await Database.Table<DirectionAction>().CountAsync();
        }


        public async Task<IEnumerable<Truck>> GetTruckListAsync()
        {
            return await Database.Table<Truck>()
                .ToListAsync();
        }

        public async Task<bool> SetTruckListAsync(IEnumerable<Truck> list)
        {
            // sync data by TruckID

            // delete
            var listIDs = list.Select(_ => _.TruckID).ToList();
            var cnt = await Database.Table<Truck>().DeleteAsync(_ => !listIDs.Contains(_.TruckID));

            // add
            var dbIDs = (await Database.Table<Truck>().ToListAsync()).Select(_ => _.TruckID).ToList();
            foreach (var truck in list)
                if (!dbIDs.Contains(truck.TruckID))
                {
                    await Database.InsertAsync(truck);
                    cnt++;
                }

            // update
            foreach (var truck in list)
                if (dbIDs.Contains(truck.TruckID))
                {
                    await Database.UpdateAsync(truck);
                    cnt++;
                }

            return cnt > 0;
        }

        public async Task<IEnumerable<DirectionInProgress>> GetInProgressDirectionsAsync()
        {
            return await Database.Table<DirectionInProgress>()
                .ToListAsync();
        }

        public async Task<bool> AddInProgressDirectionAsync(DirectionInProgress direction)
        {
            var result = await Database.InsertAsync(direction);

            return result > 0;
        }

        public async Task<bool> UpdatePartnerDirectionInProgressAsync(string docID, string partnerName, string partnerAddress, bool isSyncedFail, string docInfo)
        {
            var direction = await Database.Table<DirectionInProgress>().FirstOrDefaultAsync(_ => _.DocID == docID);

            if (direction == null) return false;

            direction.PartnerName = partnerName;
            direction.PartnerAddress = partnerAddress;
            direction.IsSyncedFail = isSyncedFail;
            direction.DocInfo = docInfo;

            var result = await Database.UpdateAsync(direction);
            return result > 0;
        }

        public async Task<bool> RemoveInProgressDirectionAsync(string docID)
        {
            var direction = await Database.Table<DirectionInProgress>().FirstOrDefaultAsync(_ => _.DocID == docID);

            if (direction == null) return false;

            var result = await Database.DeleteAsync(direction);

            return result > 0;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using S4.SharedLib.RemoteAction;
using S4.Truck.Core.Entities;

namespace S4Truck.Core.Storages
{
    public class MockDataStore : IDataStore
    {
        private List<DirectionAction> items;
        private List<Truck> trucks;

        public MockDataStore()
        {
            items = new List<DirectionAction>()
            {
                new DirectionAction
                {
                    Id = 1,
                    ActionType = ActionTypeEnum.TruckLoadUp,
                    DocID = "DL/20/2020",
                    DocInfo = "DL/20/2020",
                    UserId = "AAA1234",
                    IsCanceled = false,
                    IsSynced = false,
                    CreatedDate = new DateTime(2021, 2, 5)
                },
                new DirectionAction
                {
                    Id = 2,
                    ActionType = ActionTypeEnum.TruckLoadUp,
                    DocID = "DL/20/2020",
                    DocInfo = "DL/20/2020",
                    UserId = "AAA1234",
                    IsCanceled = false,
                    IsSynced = true,
                    CreatedDate = new DateTime(2021, 2, 5)
                },
                new DirectionAction
                {
                    Id = 3,
                    ActionType = ActionTypeEnum.TruckLoadUp,
                    DocID = "DL/20/2020",
                    DocInfo = "DL/20/2020",
                    UserId = "AAA1234",
                    IsCanceled = false,
                    IsSynced = false,
                    CreatedDate = new DateTime(2021, 2, 1)
                }
            };

            trucks = new List<Truck>()
            {
                new Truck() { TruckID = 2, TruckRegist = "B01 2345" },
                new Truck() { TruckID = 3, TruckRegist = "C01 2345" },
                new Truck() { TruckID = 1, TruckRegist = "A01 2345" },
            };
        }

        public async Task<bool> AddDirectionActionAsync(DirectionAction item)
        {
            // check if exists
            var savedItem = items.SingleOrDefault(_ =>
                _.ActionType == item.ActionType &&
                _.DocIDSource == item.DocIDSource &&
                _.DocID.Equals(item.DocID, StringComparison.OrdinalIgnoreCase) &&
                _.DocInfo.Equals(_.DocInfo, StringComparison.OrdinalIgnoreCase) &&
                !_.IsCanceled);

            if (savedItem != null)
                return await Task.FromResult(false);

            item.Id = items.Count + 1;
            items.Add(item);
            return await Task.FromResult(true);
        }

        public async Task<bool> UpdateDirectionActionAsync(DirectionAction item)
        {
            var oldDirectionAction = items.Where((DirectionAction arg) => arg.Id == item.Id).FirstOrDefault();
            items.Remove(oldDirectionAction);
            items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> CancelDirectionActionAsync(DirectionAction item)
        {
            var oldDirectionAction = items.Where((DirectionAction arg) => arg.Id == item.Id).FirstOrDefault();
            oldDirectionAction.IsCanceled = true;

            return await Task.FromResult(true);
        }

        public async Task<DirectionAction> GetDirectionActionAsync(int id)
        {
            return await Task.FromResult(items.FirstOrDefault(s => s.Id == id));
        }

        public async Task<IEnumerable<DirectionAction>> GetDirectionActionsAsync(ActionTypeEnum type)
        {
            return await Task.FromResult(items.Where(_ => _.ActionType == type).ToList());
        }

        public async Task<int> CountDirectionActionsAsync()
        {
            return await Task.FromResult(items.Count);
        }


        public async Task<bool> SetTruckListAsync(IEnumerable<Truck> list)
        {
            trucks = new List<Truck>(list);
            return await Task.FromResult(true);
        }

        public async Task<IEnumerable<Truck>> GetTruckListAsync()
        {
            return await Task.FromResult(trucks);
        }

        public async Task<IEnumerable<DirectionInProgress>> GetInProgressDirectionsAsync()
        {
            var list = new List<DirectionInProgress>
            {
                new DirectionInProgress
                {
                    Id = 3,
                    DocIDSource = DocIDSourceEnum.DocInfo,
                    DocID = "DL/20/2021",
                    DocInfo = "DL/20/2021",
                    UserId = "AAA1234",
                    CreatedDate = new DateTime(2021, 2, 1)
                }
            };

            return await Task.FromResult(list);
        }

        public Task<bool> AddInProgressDirectionAsync(DirectionInProgress direction)
        {
            return Task.FromResult(true);
        }

        public Task<bool> UpdatePartnerDirectionInProgressAsync(string docID, string partnerName, string partnerAddress, bool isSyncedFail, string docInfo)
        {
            return Task.FromResult(true);
        }

        public Task<bool> RemoveInProgressDirectionAsync(string docID)
        {
            return Task.FromResult(true);
        }
    }
}
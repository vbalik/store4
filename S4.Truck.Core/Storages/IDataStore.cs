﻿using S4.SharedLib.RemoteAction;
using S4.Truck.Core.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace S4Truck.Core.Storages
{
    public interface IDataStore
    {
        Task<bool> AddDirectionActionAsync(DirectionAction item);
        Task<bool> UpdateDirectionActionAsync(DirectionAction item);
        Task<bool> CancelDirectionActionAsync(DirectionAction item);
        Task<DirectionAction> GetDirectionActionAsync(int id);
        Task<IEnumerable<DirectionAction>> GetDirectionActionsAsync(ActionTypeEnum type);
        Task<int> CountDirectionActionsAsync();
        Task<bool> SetTruckListAsync(IEnumerable<Truck> list);
        Task<IEnumerable<Truck>> GetTruckListAsync();

        // in work overview
        Task<IEnumerable<DirectionInProgress>> GetInProgressDirectionsAsync();
        Task<bool> AddInProgressDirectionAsync(DirectionInProgress direction);
        Task<bool> UpdatePartnerDirectionInProgressAsync(string docID, string partnerName, string partnerAddress, bool isSyncedFail, string docInfo);
        Task<bool> RemoveInProgressDirectionAsync(string docID);
    }
}

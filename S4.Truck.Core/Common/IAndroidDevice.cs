﻿namespace S4.Truck.Core.Common
{
    public interface IAndroidDevice
    {
        string GetIdentifier();
    }
}

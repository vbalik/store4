﻿using Xamarin.Forms;

namespace S4.Truck.Core.Common
{
    public class ErrorFrame : Frame
    {
        public ErrorFrame()
        {
            CornerRadius = 2;
            Padding = 0;
            HorizontalOptions = LayoutOptions.FillAndExpand;
            Padding = new Thickness(3, 3, 3, 3); // fixed border size out of visible area
        }

        public void HasError(bool flag = true)
        {
            BorderColor = flag ? Color.Red : Color.Transparent;
        }
    }
}
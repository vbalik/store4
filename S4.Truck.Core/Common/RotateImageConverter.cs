﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace S4.Truck.Core.Common
{
    public class RotateImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //((Image)parameter).RotateTo(360, 5000);

            return 360; // !(bool)value ? 180 : 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

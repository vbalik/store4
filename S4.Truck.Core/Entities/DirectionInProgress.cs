﻿using S4.SharedLib.RemoteAction;
using SQLite;
using System;

namespace S4.Truck.Core.Entities
{
    public class DirectionInProgress
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public DocIDSourceEnum DocIDSource { get; set; }
        public string DocID { get; set; }
        public string DocInfo { get; set; }
        public string UserId { get; set; }
        public string PartnerName { get; set; }
        public string PartnerAddress { get; set; }
        public bool IsSyncedFail { get; set; }
        public DateTime CreatedDate { get; set; } = DateTime.Now;


        public event EventHandler<string> ValueChanged;
    }
}
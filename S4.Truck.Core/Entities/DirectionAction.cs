﻿using S4.SharedLib.RemoteAction;
using S4.Truck.Core.Helpers;
using SQLite;
using System;

namespace S4.Truck.Core.Entities
{
    public class DirectionAction
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public ActionTypeEnum ActionType { get; set; } = ActionTypeEnum.TruckLoadUp;
        public DocIDSourceEnum DocIDSource { get; set; } // can not be private cuz database binding
        public string DocID { get; set; }
        public string UserId { get; set; }
        public string PartnerName { get; set; }
        public string PartnerAddress { get; set; }

        private string _docInfo;
        public string DocInfo
        {
            get => _docInfo;
            set
            {
                _docInfo = value;
                ValueChanged?.Invoke(this, nameof(DocInfo));
                _SetDocIDSource(value);
            }
        }

        private bool _isCanceled = false;
        public bool IsCanceled
        {
            get => _isCanceled;
            set
            {
                _isCanceled = value;
                ValueChanged?.Invoke(this, nameof(IsCanceled));
            }
        }

        private bool _isSynced = false;
        public bool IsSynced
        {
            get => _isSynced;
            set
            {
                _isSynced = value;
                ValueChanged?.Invoke(this, nameof(IsSynced));
            }
        }
        
        private bool _isSyncedFail = false;
        public bool IsSyncedFail
        {
            get => _isSyncedFail;
            set
            {
                _isSyncedFail = value;
                ValueChanged?.Invoke(this, nameof(IsSyncedFail));
            }
        }

        public DateTime CreatedDate { get; set; } = DateTime.Now;
        public DateTime? SyncedDate { get; set; }


        public event EventHandler<string> ValueChanged;

        private void _SetDocIDSource(string docInfo)
        {
            var (isOK, docIDSource, docID, docInfo2) = ValidationHelpers.TryParseScan(docInfo, docInfo);
            DocIDSource = docIDSource;
        }
    }
}
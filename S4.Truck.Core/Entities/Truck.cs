﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Truck.Core.Entities
{
    public class Truck
    {
        [PrimaryKey]
        public int TruckID { get; set; }
        public string TruckRegist { get; set; }
    }
}

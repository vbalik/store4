﻿using System.Collections.ObjectModel;
using Xamarin.Forms;
using S4.Truck.Core.Entities;
using S4.Truck.ViewModels;
using System;
using System.Diagnostics;
using S4Truck.Core.Storages;
using System.Collections.Generic;
using System.Linq;

namespace S4.Truck.Pages.TruckInProgress
{
    public class TruckInProgressViewModel : BaseViewModel
    {
        public IDataStore DataStore { get; set; }
        public IToast Toast { get; set; }

        private ObservableCollection<GroupedDirectionActionModel> _items;
        public ObservableCollection<GroupedDirectionActionModel> Items
        {
            get => _items;
            set
            {
                SetProperty(ref _items, value);
                ItemsEmpty = _items.Count() == 0;
            }
        }

        private bool _itemsEmpty;
        public bool ItemsEmpty
        {
            get => _itemsEmpty;
            set => SetProperty(ref _itemsEmpty, value);
        }


        public Command LoadItemsCommand { get; set; }


        public TruckInProgressViewModel()
        {
            Title = "Unknown";
            Items = new ObservableCollection<GroupedDirectionActionModel>();

            LoadItemsCommand = new Command(ExecuteLoadItemsCommand);
        }

        public AppStatus AppStatus => DependencyService.Get<AppStatus>();

        private async void ExecuteLoadItemsCommand()
        {
            try
            {
                Items.Clear();
                var items = await DataStore.GetInProgressDirectionsAsync();
                GroupItems(items);
            }
            catch (Exception ex)
            {
                //TODO ?
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        private void GroupItems(IEnumerable<DirectionInProgress> list)
        {
            var data = list.GroupBy(
                key => key.CreatedDate.ToString("dd.MM.yy"),
                (key, g) => new
                {
                    Date = key,
                    Items = g.OrderByDescending(_ => _.Id).ToList()
                })
                .OrderByDescending(_ => _.Date)
                .ToList();

            var groups = new ObservableCollection<GroupedDirectionActionModel>();

            foreach (var g in data)
            {
                var group = new GroupedDirectionActionModel { GroupName = g.Date == DateTime.Now.ToString("dd.MM.yy") ? "Dnes" : g.Date, DocCount = g.Items.Count };
                g.Items.ForEach(item => group.Add(new DirectionActionItemViewModel(item)));
                groups.Add(group);
            }

            Items = groups;
        }
    }

    public class GroupedDirectionActionModel : List<DirectionActionItemViewModel>
    {
        public string GroupName { get; set; }
        public int DocCount { get; set; }
        public string DocCountDesc
        {
            get
            {
                switch (DocCount)
                {
                    case 0:
                        return "?";
                    case 1:
                        return "1 doklad";
                    case int n when (n < 5):
                        return $"{DocCount} doklady";
                    default:
                        return $"{DocCount} dokladů";
                }
            }
        }
    }
}
﻿using S4.Truck.Core.Entities;
using S4Truck.Core.Storages;
using System;
using System.ComponentModel;
using Xamarin.Forms;

namespace S4.Truck.Pages.TruckInProgress
{
    [DesignTimeVisible(false)]
    public partial class TruckInProgressPage : ContentPage
    {
        private readonly TruckInProgressViewModel _viewModel;

        public TruckInProgressPage()
        {
            InitializeComponent();

            BindingContext = _viewModel = new TruckInProgressViewModel();
            _viewModel.Title = "K vyřízení";
            _viewModel.DataStore = DependencyService.Get<IDataStore>();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            _viewModel.Toast = DependencyService.Get<IToast>();

            _viewModel.LoadItemsCommand.Execute(null);
        }

        public async void Settings_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SettingsPage());
        }

        /// <summary>
        /// Show menu for selected item
        /// </summary>
        private void OnBindingContextChanged(object sender, EventArgs e)
        {
            base.OnBindingContextChanged();

            if (BindingContext == null)
                return;

            ViewCell viewCell = ((ViewCell) sender);
            var item = viewCell.BindingContext as DirectionActionItemViewModel;
            viewCell.ContextActions.Clear();

            var command = new Command<DirectionActionItemViewModel>(RemoveDirectionItem);

            viewCell.ContextActions.Add(new MenuItem
            {
                Text = "Smazat DL",
                Command = command,
                CommandParameter = item
            });
        }

        private async void RemoveDirectionItem(DirectionActionItemViewModel direction)
        {
            var dlgResult = await DisplayAlert("Upozornění!", "Opravdu chcete smazat DL ze seznamu 'K vyřízení'?", "Ano smazat", "Ne");

            if (!dlgResult) return;

            await _viewModel.DataStore.RemoveInProgressDirectionAsync(direction.Item.DocID);

            _viewModel.LoadItemsCommand.Execute(null);
        }

        private async void InProgressListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var itemMain = e.Item as DirectionActionItemViewModel;
            var errorText = itemMain.Item.IsSyncedFail ? "\n\n\n*** CHYBA! Neexistující DL ***" : string.Empty;
            await DisplayAlert($"Info - {itemMain.DocInfo}", $"Název partnera: \n{itemMain.Item.PartnerName}\n\nAdresa: \n{itemMain.Item.PartnerAddress}{errorText}", "Zavřít");
        }
    }

    public class DirectionActionItemViewModel : INotifyPropertyChanged
    {
        public readonly DirectionInProgress Item;

        public DirectionActionItemViewModel(DirectionInProgress item)
        {
            this.Item = item;
            this.Item.ValueChanged += Item_ValueChanged;
        }

        public string DocInfo { get => Item.DocInfo; }


        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        private void Item_ValueChanged(object sender, string e)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(e));
        }
        #endregion
    }
}
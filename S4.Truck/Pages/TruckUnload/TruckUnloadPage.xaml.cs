﻿using S4.SharedLib.RemoteAction;
using S4.Truck.Core.Entities;
using S4.Truck.ViewModels;
using S4Truck.Core.Storages;
using System;
using System.ComponentModel;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace S4.Truck.Pages.TruckUnload
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class TruckUnloadPage : ContentPage
    {
        private readonly DirectionActionListViewModel _viewModel;

        public TruckUnloadPage()
        {
            InitializeComponent();

            BindingContext = _viewModel = new DirectionActionListViewModel();
            _viewModel.ActionType = ActionTypeEnum.TruckUnload;
            _viewModel.Title = "Předat DL";
            _viewModel.DataStore = DependencyService.Get<IDataStore>();
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();

            _viewModel.Toast = DependencyService.Get<IToast>();

            if (_viewModel.Items.Count == 0)
                _viewModel.LoadItemsCommand.Execute(null);

            // barcode scanner on
            await DependencyService.Get<ScannerBase>().Listen(this, (sr) =>
            {
                var (isOK, docIDSource, docID, docInfo) = Core.Helpers.ValidationHelpers.TryParseScan(sr.TextData, sr.CodeType);
                if (isOK)
                    Task.Run(async () => await _viewModel.AddDirectionAction(docID, docInfo));
                else
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        _viewModel.Toast.Show($"Neplatný kód", true);
                        Helpers.Sounds.Error();
                    });
            });
        }

        protected async override void OnDisappearing()
        {
            // barcode scanner off
            await DependencyService.Get<ScannerBase>().UnListen(this);

            base.OnDisappearing();
        }


        public async void Settings_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SettingsPage());
        }

        /// <summary>
        /// Show menu for selected item
        /// </summary>
        private void OnBindingContextChanged(object sender, EventArgs e)
        {
            base.OnBindingContextChanged();

            if (BindingContext == null)
                return;

            ViewCell viewCell = ((ViewCell)sender);
            var item = viewCell.BindingContext as DirectionActionItemViewModel;
            viewCell.ContextActions.Clear();

            if (!item.IsSynced)
            {
                viewCell.ContextActions.Add(new MenuItem
                {
                    Text = "Zrušit DL",
                    Command = (BindingContext as DirectionActionListViewModel).OnDeleteCommand,
                    CommandParameter = item
                });
            }
        }

        private async void TakeOverListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var itemMain = e.Item as DirectionActionItemViewModel;
            var errorText = itemMain.Item.IsSyncedFail ? "\n\n\n*** CHYBA! Neexistující DL ***" : string.Empty;
            await DisplayAlert($"Info - {itemMain.DocInfo}", $"Název partnera: \n{itemMain.Item.PartnerName}\n\nAdresa: \n{itemMain.Item.PartnerAddress}{errorText}", "Zavřít");
        }
    }
}
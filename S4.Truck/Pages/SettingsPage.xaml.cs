﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace S4.Truck.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SettingsPage : ContentPage
    {
        private SettingsPageViewModel _settingsPageViewModel;
        public SettingsPage()
        {
            InitializeComponent();

            _settingsPageViewModel = new SettingsPageViewModel();
        }

        protected override async void OnAppearing()
        {
            _settingsPageViewModel.Page = this;

            BindingContext = _settingsPageViewModel;

            await _settingsPageViewModel.LoadData();

            // hide endpoint entry
            if (!string.IsNullOrEmpty(_settingsPageViewModel.EndpointServer)) Settings.Root.Remove(endpointSec);

            // hide truck refresh button
            refreshTruckBtn.IsVisible = !string.IsNullOrEmpty(_settingsPageViewModel.EndpointServer);
            testConnectionBtn.IsVisible = !string.IsNullOrEmpty(_settingsPageViewModel.EndpointServer);

            MessagingCenter.Subscribe<SettingsPageViewModel>(this, "SaveSettings", (sender) =>
            {
                // set default main page from model
                Application.Current.MainPage = new AppShell();
            });

            base.OnAppearing();
        }
    }
}
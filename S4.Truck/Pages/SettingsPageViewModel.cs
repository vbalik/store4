﻿using S4.SharedLib.S4Truck;
using S4.Truck.Core.Clients;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace S4.Truck.Pages
{
    internal class SettingsPageViewModel : INotifyPropertyChanged
    {
        private int _dbCnt;
        private bool _userIDChangeEnabled;
        private string _userID;
        private ObservableCollection<string> _truckList = new ObservableCollection<string>();
        private string _truckRegist;
        private bool _syncHttpClientFailed;
        private string _endpointServer;
        private readonly S4Truck.Core.Storages.IDataStore _datastore;
        private IApiClient _client;

        public Page Page { get; set; }
        public Command SaveCommand { get; set; }
        public Command RefreshCommand { get; set; }
        public Command TestConnectionCommand { get; set; }

        public SettingsPageViewModel()
        {
            _datastore = DependencyService.Get<S4Truck.Core.Storages.IDataStore>();
            _client = DependencyService.Get<IApiClient>();

            // save
            SaveCommand = new Command(() =>
            {
                var settProxy = DependencyService.Resolve<Helpers.SettingsProxy>();

                if (!string.IsNullOrWhiteSpace(_userID)
                    && !settProxy.SetupDone
                    && !string.IsNullOrWhiteSpace(_endpointServer)
                    && (_endpointServer ?? string.Empty).StartsWith("http"))
                {
                    settProxy.UserID = _userID;
                    settProxy.SetupDone = true;
                    settProxy.EndpointServer = _endpointServer;

                    _client.SetClientBaseUrl(_endpointServer);

                    DependencyService.Resolve<AppStatus>().SetupDone = true;
                    _userIDChangeEnabled = true;
                }

                settProxy.TruckRegist = _truckRegist;

                if (!string.IsNullOrWhiteSpace(_userID) && !string.IsNullOrWhiteSpace(_endpointServer) && (_endpointServer ?? string.Empty).StartsWith("http"))
                    MessagingCenter.Send<SettingsPageViewModel>(this, "SaveSettings");
            });

            // refresh data
            RefreshCommand = new Command(async () =>
            {
                try
                {
                    var response = await _client.GetSetupDataAsync();

                    if (response.StatusCode != HttpStatusCode.OK)
                        throw new HttpRequestException($"Chyba připojení s kódem {response.StatusCode}");

                    await Page.DisplayAlert("Informace", "SPZ byly staženy", "Ok");

                    // update local truck list
                    await _datastore.SetTruckListAsync(response.Data.TruckInfos.Select(_ => new Core.Entities.Truck
                    {
                        TruckID = _.TruckID,
                        TruckRegist = _.TruckRegist
                    }).ToList());
                }
                catch (HttpRequestException e)
                {
                    await Page.DisplayAlert("Chyba připojení", e.Message, "Ok");
                }
                finally
                {
                    await FillTruckList();
                }
            });

            TestConnectionCommand = new Command(async () =>
            {
                try
                {
                    var response = await _client.GetSetupDataAsync();

                    if (response.StatusCode == HttpStatusCode.OK)
                        await Page.DisplayAlert("Test připojení", "Připojeno", "Ok");
                    else
                        throw new HttpRequestException($"Chyba připojení s kódem {response.StatusCode}");
                }
                catch (HttpRequestException e)
                {
                    await Page.DisplayAlert("Test připojení", e.Message, "Ok");
                }
            });
        }


        internal async Task LoadData()
        {
            await FillTruckList();

            // data from settings
            var settingsProxy = DependencyService.Resolve<Helpers.SettingsProxy>();
            UserIDChangeEnabled = !DependencyService.Resolve<AppStatus>().SetupDone;
            UserID = settingsProxy.UserID;
            TruckRegist = settingsProxy.TruckRegist;
            EndpointServer = settingsProxy.EndpointServer;

            SyncHttpClientFailed = DependencyService.Resolve<AppStatus>().SyncHttpClientFailed;

            // db couter
            var datastore = DependencyService.Get<S4Truck.Core.Storages.IDataStore>();
            _dbCnt = await datastore.CountDirectionActionsAsync();
            OnPropertyChanged(nameof(DBInfoString));
        }

        public bool UserIDChangeEnabled
        {
            get => _userIDChangeEnabled;
            set
            {
                _userIDChangeEnabled = value;
                OnPropertyChanged(nameof(UserIDChangeEnabled));
            }
        }

        public string UserID
        {
            get => _userID;
            set
            {
                _userID = value;
                OnPropertyChanged(nameof(UserID));
            }
        }

        public string TruckRegist
        {
            get => _truckRegist;
            set
            {
                _truckRegist = value;
                OnPropertyChanged(nameof(TruckRegist));
            }
        }

        public ObservableCollection<string> TruckList
        {
            get => _truckList;
            set
            {
                _truckList = value;
                OnPropertyChanged(nameof(TruckList));
            }
        }

        public string VersionString
        {
            get
            {
                var ver = DependencyService.Resolve<IAppVersionNumber>().GetVersionNumber();
                return $"Verze: {ver}";
            }
        }

        public string DBInfoString => $"Počet záznamů v db: {_dbCnt}";

        public bool SyncHttpClientFailed
        {
            get => _syncHttpClientFailed;
            set
            {
                _syncHttpClientFailed = value;
                OnPropertyChanged(nameof(SyncHttpClientFailed));
            }
        }

        public string EndpointServer
        {
            get => _endpointServer;
            set
            {
                _endpointServer = value;
                OnPropertyChanged(nameof(EndpointServer));
            }
        }

        private async Task FillTruckList()
        {
            var datastore = DependencyService.Get<S4Truck.Core.Storages.IDataStore>();
            var trucks = await datastore.GetTruckListAsync();

            TruckList.Clear();

            foreach (var item in trucks.Select(_ => _.TruckRegist).OrderBy(_ => _))
                TruckList.Add(item);
        }


        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

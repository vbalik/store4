﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Xamarin.Forms;
using S4.Truck.Core.Entities;
using S4.Truck.ViewModels;
using System;
using System.Diagnostics;
using S4Truck.Core.Storages;
using S4.Truck.Core.Helpers;
using System.Collections.Generic;
using System.Linq;
using S4.SharedLib.RemoteAction;
using S4.Truck.Services;

namespace S4.Truck.Pages
{
    public class DirectionActionListViewModel : BaseViewModel
    {
        public IDataStore DataStore { get; set; }
        public IToast Toast { get; set; }
        public ActionTypeEnum ActionType { get; set; }


        private ObservableCollection<GroupedDirectionActionModel> _items;
        public ObservableCollection<GroupedDirectionActionModel> Items { get => _items; set => SetProperty(ref _items, value); }


        public Command LoadItemsCommand { get; set; }
        public Command OnDeleteCommand { get; set; }
        public Command OnAddCommand { get; set; }


        public DirectionActionListViewModel()
        {
            Title = "Unknown";
            Items = new ObservableCollection<GroupedDirectionActionModel>();

            LoadItemsCommand = new Command(ExecuteLoadItemsCommand);
            OnAddCommand = new Command<string>(async (docInfo) => await AddCommand(docInfo));
            OnDeleteCommand = new Command<DirectionActionItemViewModel>(DeleteCommand);

            MessagingCenter.Subscribe<SyncDataService>(this, "UpdateActionData", (sender) =>
            {
                LoadItemsCommand.Execute(null);
            });
        }

        public AppStatus AppStatus
        {
            get { return DependencyService.Get<AppStatus>(); }
        }


        private async void ExecuteLoadItemsCommand()
        {
            try
            {
                Items.Clear();
                var items = await DataStore.GetDirectionActionsAsync(ActionType);

                GroupItems(items);
            }
            catch (Exception ex)
            {
                //TODO ?
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        private async Task AddCommand(string docInfo)
        {
            if (string.IsNullOrEmpty(ValidationHelpers.DocInfoValidation(docInfo)))
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Helpers.Sounds.Error();
                });
                return;
            }

            await AddDirectionAction(docInfo, docInfo);
        }

        public async Task<bool> AddDirectionAction(string docID, string docInfo)
        {
            var direction = new DirectionAction
            {
                ActionType = ActionType,
                DocID = docID,
                DocInfo = docInfo,
                UserId = DependencyService.Get<AppStatus>().CurrentUser
            };

            // save action
            var saved = await DataStore.AddDirectionActionAsync(direction);
            bool inProgressResult = false;

            // save to in progress list
            if (ActionType == ActionTypeEnum.TruckLoadUp)
            {
                inProgressResult = await DataStore.AddInProgressDirectionAsync(new DirectionInProgress
                {
                    DocIDSource = direction.DocIDSource,
                    DocID = direction.DocID,
                    DocInfo = direction.DocInfo,
                    UserId = direction.UserId
                });
            }
            else if (ActionType == ActionTypeEnum.TruckUnload)
            {
                // remove from in progress list
                inProgressResult = await DataStore.RemoveInProgressDirectionAsync(direction.DocID);
            }

            if (saved && inProgressResult)
            {
                LoadItemsCommand.Execute(null);

                Device.BeginInvokeOnMainThread(() =>
                {
                    Toast.Show($"Přidán {direction.DocInfo}", false);
                    Helpers.Sounds.OK();
                });
            }
            else
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Toast.Show($"Doklad {direction.DocInfo} už byl přidán", true);
                    Helpers.Sounds.Error();
                });
            }

            return saved;
        }

        private async void DeleteCommand(DirectionActionItemViewModel directionActionVM)
        {
            if ((directionActionVM.IsSynced && !directionActionVM.IsSyncedFail) || directionActionVM.IsCanceled) return;

            var result = await DataStore.CancelDirectionActionAsync(directionActionVM.Item);

            // remove from in progress list
            var inProgressResult = await DataStore.RemoveInProgressDirectionAsync(directionActionVM.Item.DocID);

            if (!result || !inProgressResult)
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Toast.Show($"Chyba aplikace", true);
                    Helpers.Sounds.Error();
                });

                return; // skip
            }

            Device.BeginInvokeOnMainThread(() =>
            {
                Toast.Show($"Zrušen {directionActionVM.DocInfo}", true);
                Helpers.Sounds.OK();
            });

            LoadItemsCommand.Execute(null);
        }

        private void GroupItems(IEnumerable<DirectionAction> list)
        {
            var data = list.OrderByDescending(_ => _.CreatedDate)
                .GroupBy(
                    key => key.CreatedDate.ToString("dd.MM.yy"),
                    (key, g) => new
                    {
                        Date = key,
                        Items = g.OrderByDescending(_ => _.Id).ToList()
                    }
                )
                .ToList();

            var groups = new ObservableCollection<GroupedDirectionActionModel>();

            foreach (var g in data)
            {
                var group = new GroupedDirectionActionModel { GroupName = g.Date == DateTime.Now.ToString("dd.MM.yy") ? "Dnes" : g.Date, DocCount = g.Items.Count };
                g.Items.ForEach(item => group.Add(new DirectionActionItemViewModel(item)));
                groups.Add(group);
            }

            Items = groups;
        }
    }

    public class GroupedDirectionActionModel : List<DirectionActionItemViewModel>
    {
        public string GroupName { get; set; }
        public int DocCount { get; set; }
        public string DocCountDesc
        {
            get
            {
                switch (DocCount)
                {
                    case 0:
                        return "?";
                    case 1:
                        return "1 doklad";
                    case int n when (n < 5):
                        return $"{DocCount} doklady";
                    default:
                        return $"{DocCount} dokladů";
                }
            }
        }
    }
}
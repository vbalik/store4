﻿using S4.Truck.Core.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace S4.Truck.ViewModels
{
    public class DirectionActionItemViewModel : INotifyPropertyChanged
    {
        private readonly DirectionAction item;

        public DirectionActionItemViewModel(DirectionAction item)
        {
            this.item = item;
            this.item.ValueChanged += Item_ValueChanged;
        }



        public string DocInfo { get => item.DocInfo; }

        public bool IsCanceled { get => item.IsCanceled; }
        public bool IsSynced { get => item.IsSynced; }
        public bool IsSyncedFail { get => item.IsSyncedFail; }

        public DirectionAction Item { get => this.item; }



        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        private void Item_ValueChanged(object sender, string e)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(e));
        }
        #endregion
    }
}

﻿namespace S4.Truck
{
    public interface IToast
    {
        void Show(string message, bool isFail);
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace S4.Truck
{
    public class ScanResult
    {
        public string TextData { get; set; }
        public string CodeType { get; set; }
    }
}
﻿using Xamarin.Essentials;
using Xamarin.Forms;
using S4Truck.Core.Storages;
using S4.Truck.Core.Clients;
using S4.Truck.Helpers;
using System.Net;

namespace S4.Truck
{
    public partial class App : Application
    {
        //TODO: Replace with *.azurewebsites.net url after deploying backend to Azure
        //To debug on Android emulators run the web backend against .NET Core not IIS
        //If using other emulators besides stock Google images you may need to adjust the IP address
        public static string AzureBackendUrl =
            DeviceInfo.Platform == DevicePlatform.Android ? "http://10.0.2.2:5000" : "http://localhost:5000";

        public static bool UseMockDataStore = false;

        private ScannerBase _scannerBase;
        private AppStatus _appStatus;
        private SettingsProxy _settingsProxy;

        public App()
        {
            if (UseMockDataStore)
            {
                DependencyService.Register<SettingsProxy>();
                DependencyService.Register<MockDataStore>();
                DependencyService.RegisterSingleton<ApiClient>(new ApiClient());
            }
            else
            {
                DependencyService.Register<SettingsProxy>();
                DependencyService.Register<LocalDataStore>();
                DependencyService.RegisterSingleton<ApiClient>(new ApiClient());
            }

            DependencyService.Register<IToast>();
            DependencyService.Register<IAppVersionNumber>();
        }


        protected override void OnStart()
        {
            // init services
            _scannerBase = new ScannerBase();
            DependencyService.RegisterSingleton<ScannerBase>(_scannerBase);

            _settingsProxy = new SettingsProxy();
            DependencyService.RegisterSingleton<SettingsProxy>(_settingsProxy);

            _appStatus = new AppStatus();
            _appStatus.Start(_settingsProxy.SetupDone);
            DependencyService.RegisterSingleton<AppStatus>(_appStatus);

            // init db
            LocalDataStore.CheckAndCreateDatabase();

            // load main page
            if (_appStatus.SetupDone)
                MainPage = new AppShell();
            else
                MainPage = new Pages.SettingsPage();
        }


        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}

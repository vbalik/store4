﻿namespace S4.Truck
{
    public interface IAppVersionNumber
    {
        string GetVersionNumber();
    }
}
﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace S4.Truck
{
    public partial class AppShell : Xamarin.Forms.Shell
    {
        public AppShell()
        {
            InitializeComponent();
            BindingContext = this;
        }

        public AppStatus AppStatus => DependencyService.Resolve<AppStatus>();
    }
}

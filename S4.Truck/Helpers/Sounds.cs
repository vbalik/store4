﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace S4.Truck.Helpers
{
    static class Sounds
    {
        public static void OK()
        {
            DependencyService.Get<IAudio>().PlayAudioFile("109662__grunz__success.wav");
        }

        public static void Error()
        {
            DependencyService.Get<IAudio>().PlayAudioFile("142608__autistic-lucario__error.wav");
        }

        public static void Synchonised()
        {
            DependencyService.Get<IAudio>().PlayAudioFile("446114__justinvoke__confirm-jingle");
        }
    }
}

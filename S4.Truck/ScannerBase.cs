﻿using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using Xamarin.Forms;


namespace S4.Truck
{
    class ScannerBase
    {
        private bool _started = false;
        private IBarcodeScannerManager _barcodeScannerManager;
        private readonly ConcurrentDictionary<object, Action<ScanResult>> _clients = new ConcurrentDictionary<object, Action<ScanResult>>();

        internal async Task Start()
        {
            _barcodeScannerManager = DependencyService.Get<IBarcodeScannerManager>();
            await _barcodeScannerManager.Init();
            _barcodeScannerManager.ScanReceived += BarcodeScannerManager_ScanReceived;
            _started = true;
        }


        internal async Task End()
        {
            if (_barcodeScannerManager.IsScannerEnabled)
                await _barcodeScannerManager.Disable();
            _barcodeScannerManager.ScanReceived -= BarcodeScannerManager_ScanReceived;
            await _barcodeScannerManager.Shutdown();
            _barcodeScannerManager = null;
        }


        internal async Task Listen(object sender, Action<ScanResult> listenProc)
        {
            if(!_started)
                await Start();

            _clients.TryAdd(sender, listenProc);
            if (!_barcodeScannerManager.IsScannerEnabled)
                await _barcodeScannerManager.Enable();
        }

        internal async Task UnListen(object sender)
        {
            _clients.TryRemove(sender, out _);
            if (_clients.Count == 0 && _barcodeScannerManager.IsScannerEnabled)
                await _barcodeScannerManager.Disable();
        }

        private void BarcodeScannerManager_ScanReceived(object sender, ScanResult e)
        {
            foreach (var proc in _clients.Values)
                proc.Invoke(e);
        }
    }
}

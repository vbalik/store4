﻿using S4.Truck.Core.Helpers;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace S4.Truck.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddItemEntry : ContentView
    {
        public static readonly BindableProperty DocInfoProperty = BindableProperty.Create(nameof(DocInfo), typeof(string), typeof(AddItemEntry), null);
        public string DocInfo
        {
            get { return (string)GetValue(DocInfoProperty); }
            set { SetValue(DocInfoProperty, value); }
        }

        public static readonly BindableProperty AddCommandProperty = BindableProperty.Create(nameof(AddCommand), typeof(Command), typeof(AddItemEntry), null);
        public Command AddCommand
        {
            get { return (Command)GetValue(AddCommandProperty); }
            set { SetValue(AddCommandProperty, value); }
        }

        public AddItemEntry()
        {
            InitializeComponent();
        }

        private void AddItem_Clicked(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(ValidationHelpers.DocInfoValidation(DocInfo)))
            {
                ViewHelpers.Entry_FrameError(docInfoTxtFrame);
                Helpers.Sounds.Error();
                return;
            }

            if (AddCommand != null)
                AddCommand.Execute(DocInfo);

            DocInfo = null;
        }
    }
}
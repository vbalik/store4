﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S4.Truck
{
    public interface IBarcodeScannerManager
    {
        event EventHandler<ScanResult> ScanReceived;
        bool IsScannerEnabled { get; }
        Task Init();
        Task Shutdown();
        Task Enable();
        Task Disable();
    }
}
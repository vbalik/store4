﻿using Newtonsoft.Json;
using S4.SharedLib.RemoteAction;
using S4.SharedLib.Security;
using S4.Truck.Core.Common;
using S4.Truck.Core.Entities;
using S4.Truck.Helpers;
using S4Truck.Core.Storages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace S4.Truck.Services
{
    public class SyncDataService
    {
        static readonly int CANCEL_DELAY_MINUTES = 2;

        static readonly string TAG = typeof(SyncDataService).FullName;

        private HttpClient _httpClient;
        private string ActionUrl(ActionTypeEnum action) => $"{new SettingsProxy().EndpointServer}/api/external/remoteaction/{action}";
        
        private Helpers.SettingsProxy _settingsProxy;
        private string _deviceID;
        private IDataStore _dataStore;
        private AppStatus _appStatus;
        private bool _existingChanges;

        public SyncDataService()
        {
            var handler = new HttpClientHandler();

            // allow self-signed certificate
            handler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => true;

            _httpClient = new HttpClient(handler);
        }

        public async Task Run()
        {
            var device = DependencyService.Get<IAndroidDevice>();
            _deviceID = device != null ? device.GetIdentifier() : "unknown";

            _dataStore = DependencyService.Get<IDataStore>();
            var loadUpActions = await _dataStore.GetDirectionActionsAsync(ActionTypeEnum.TruckLoadUp);
            var unLoadActions = await _dataStore.GetDirectionActionsAsync(ActionTypeEnum.TruckUnload);

            _appStatus = DependencyService.Resolve<AppStatus>();
            _settingsProxy = DependencyService.Resolve<Helpers.SettingsProxy>();


            // skip if user data are not filled yet
            if (string.IsNullOrEmpty(_settingsProxy.UserID) || string.IsNullOrEmpty(_settingsProxy.TruckRegist))
                return;

            // sync data
            await SyncData<RemoteActionModel_TruckLoadUp>(ActionTypeEnum.TruckLoadUp, loadUpActions);
            await SyncData<RemoteActionModel_TruckUnload>(ActionTypeEnum.TruckUnload, unLoadActions);

            if(_existingChanges)
                MessagingCenter.Send<SyncDataService>(this, "UpdateActionData");
        }

        private async Task SyncData<T>(ActionTypeEnum actionType, IEnumerable<DirectionAction> actions) where T : RemoteActionModelBase, new()
        {
            // sync - load up
            foreach (var item in actions.Where(_ => !_.IsCanceled && !_.IsSynced && _.CreatedDate.AddSeconds(CANCEL_DELAY_MINUTES) < DateTime.Now).ToList())
            {
                using (var requestMessage = new HttpRequestMessage(HttpMethod.Post, ActionUrl(actionType)))
                {
                    var date = DateTime.Now;

                    var requestBody = new T
                    {
                        DocIDSource = item.DocIDSource,
                        DirectionID = item.DocIDSource == DocIDSourceEnum.DirectionID ? int.Parse(item.DocID) : default,
                        DocInfo = item.DocInfo,
                        ExpedTruck = _settingsProxy.TruckRegist,
                        UserID = _settingsProxy.UserID,
                        DeviceID = _deviceID,
                        ActionDate = item.CreatedDate,
                    };

                    var requestBodySerialized = JsonConvert.SerializeObject(requestBody);
                    var token = RemoteAccessAuthorization.CreateKey(requestBodySerialized, date);

                    requestMessage.Headers.Add("apiAuthorization", token);
                    requestMessage.Headers.Add("datetime", date.ToString("O", System.Globalization.CultureInfo.InvariantCulture));

                    requestMessage.Content = new StringContent(requestBodySerialized, Encoding.UTF8, "application/json");

                    try
                    {
                        var response = await _httpClient.SendAsync(requestMessage);
                        var responseData = JsonConvert.DeserializeObject<RemoteActionResponseModel>(await response.Content.ReadAsStringAsync());

                        if (response.StatusCode != System.Net.HttpStatusCode.Accepted)
                        {
                            // synced with fail status
                            item.IsSyncedFail = true;
                        }
                        else
                        {
                            item.PartnerName = responseData.PartnerName;
                            item.PartnerAddress = responseData.PartnerAddress;

                            if (item.DocIDSource != DocIDSourceEnum.DocInfo)
                                item.DocInfo = responseData.DocInfo;
                        }

                        _appStatus.SyncHttpClientFailed = false;
                        _existingChanges = true;
                        item.IsSynced = true;
                        item.SyncedDate = DateTime.Now;

                        await _dataStore.UpdateDirectionActionAsync(item);

                        await _dataStore.UpdatePartnerDirectionInProgressAsync(item.DocID, responseData.PartnerName, responseData.PartnerAddress, item.IsSyncedFail, item.DocInfo);

                    }
                    catch (HttpRequestException e)
                    {
                        // update sync status log
                        _appStatus.SyncHttpClientFailed = true;
                    }
                }
            }
        }
    }
}
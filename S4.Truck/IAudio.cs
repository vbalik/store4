﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Truck
{
    public interface IAudio
    {
        void PlayAudioFile(string fileName);
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Xamarin.Essentials;

namespace S4.Truck
{
    public class AppStatus : INotifyPropertyChanged
    {
        private bool _isConnected = false;
        private bool _setupDone = false;
        private string _currentUser = "?";
        private bool _syncHttpClientFailed = false;

        public event PropertyChangedEventHandler PropertyChanged;


        public void Start(bool setupDone)
        {
            _setupDone = setupDone;
            IsConnected = (Connectivity.NetworkAccess == NetworkAccess.Internet);
            Connectivity.ConnectivityChanged += Connectivity_ConnectivityChanged;
        }

        public bool IsConnected
        {
            set
            {
                _isConnected = value;
                OnPropertyChanged(nameof(IsConnected));
            }
            get => _isConnected;
        }

        public bool SetupDone
        {
            set
            {
                _setupDone = value;
                OnPropertyChanged(nameof(SetupDone));
            }
            get => _setupDone;
        }


        public string CurrentUser
        {
            set
            {
                _currentUser = value;
                OnPropertyChanged(nameof(CurrentUser));
            }
            get => _currentUser;
        }

        public bool SyncHttpClientFailed
        {
            set
            {
                _syncHttpClientFailed = value;
                OnPropertyChanged(nameof(SyncHttpClientFailed));
            }
            get => _syncHttpClientFailed;
        }
        

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void Connectivity_ConnectivityChanged(object sender, ConnectivityChangedEventArgs e)
        {
            IsConnected = (e.NetworkAccess == NetworkAccess.Internet);
        }
    }
}

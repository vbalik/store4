﻿using System;
using Xamarin.Forms;
using S4mp.Droid;
using S4mp.Controls;

[assembly: Dependency(typeof(AndroidRendererResolver))]
namespace S4mp.Droid
{
    public class AndroidRendererResolver : IRendererResolver
    {
        public object GetRenderer(VisualElement element)
        {
            return Xamarin.Forms.Platform.Android.Platform.GetRenderer(element);
        }

        public bool HasRenderer(VisualElement element)
        {
            return GetRenderer(element) != null;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Graphics;
using Android.Graphics.Drawables;

using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;

using S4mp.Controls.Common;
using S4mp.Droid.Renderers;

[assembly: ExportRenderer(typeof(ImgButton), typeof(ImgButtonRenderer))]

namespace S4mp.Droid.Renderers
{
	public class ImgButtonRenderer : ButtonRenderer
	{
		private ImgButton _imageButton;
		private ButtonDrawable _normalDrawable, _pressedDrawable;

        public ImgButtonRenderer(Context context) : base(context)
        {

        }

		protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
		{
			base.OnElementChanged(e);

			if (e.NewElement != null)
			{
				_imageButton = e.NewElement as ImgButton;
				_imageButton.RedrawRequest += NewElement_RedrawRequest;
				_imageButton.SizeChanged += _imageButton_SizeChanged;
			}
		}

		private void NewElement_RedrawRequest(object sender, EventArgs e)
		{
			CreateDrawables();
		}

		private void _imageButton_SizeChanged(object sender, EventArgs e)
		{
			CreateDrawables();
		}

		public override void RemoveAllViews()
		{
			_imageButton.RedrawRequest -= NewElement_RedrawRequest;
			_imageButton.SizeChanged -= _imageButton_SizeChanged;

			base.RemoveAllViews();
		}

		private void CreateDrawables()
		{
			var radius = (float)Math.Min(_imageButton.Width, _imageButton.Height) / 10;

			_normalDrawable = new ButtonDrawable(_imageButton, Context);
			_normalDrawable.SetColor(_imageButton.BackgroundColor.ToAndroid());
			_normalDrawable.SetCornerRadius(radius);

			// Create a drawable for the button's pressed state
			_pressedDrawable = new ButtonDrawable(_imageButton, Context);
			var highlight = Context.ObtainStyledAttributes(new int[] { Android.Resource.Attribute.ColorActivatedHighlight }).GetColor(0, Android.Graphics.Color.Gray);
			_pressedDrawable.SetColor(highlight);
			_pressedDrawable.SetCornerRadius(radius);

			// Add the drawables to a state list and assign the state list to the button
			var sld = new StateListDrawable();
			sld.AddState(new int[] { Android.Resource.Attribute.StatePressed }, _pressedDrawable);
			sld.AddState(new int[] { }, _normalDrawable);
			Control.SetBackground(sld);
		}
	}

	internal class ButtonDrawable : GradientDrawable
	{
		private ImgButton _imageButton;
		private Context _context;

		public ButtonDrawable(ImgButton imageButton, Context context)
		{
			_imageButton = imageButton;
			_context = context;
		}

		public override void Draw(Canvas canvas)
		{


			base.Draw(canvas);

			var density = (float)S4mp.Core.Consts.DENSITY_COEF;
			var imgSize = (float)_imageButton.ImgSize * density;
			var height = (float)_imageButton.Height * density;
			var width = (float)_imageButton.Width * density;
			var padding = (float)ImgButton._PADDING * density;

			float beforeText = padding;
			float afterText = padding;

			using (var paint = new Paint() { AntiAlias = true, Dither = true })
			{
				if (_imageButton.ShowImage)
				{
					var imgSource = _imageButton.IsEnabled ? _imageButton.EnabledImage : (_imageButton.DisabledImage ?? _imageButton.EnabledImage);
					var image = new Drawing.DrawingHelper().GetBitmap(imgSource, _context);

					if (image != null)
					{
						float imgLeft = 0;
						float imgTop = (height - imgSize) / 2;

						switch (_imageButton.ImageAlignment)
						{
							case Xamarin.Forms.TextAlignment.Start:
								imgLeft = padding;
								beforeText = imgSize + padding;
								break;
							case Xamarin.Forms.TextAlignment.Center:
								imgLeft = (width - imgSize) / 2;
								break;
							case Xamarin.Forms.TextAlignment.End:
								imgLeft = width - imgSize -padding;
								afterText = imgSize + padding;
								break;
						}

						RectF imgRect = new RectF(imgLeft, imgTop, imgLeft + imgSize, imgTop + imgSize);

						canvas.DrawBitmap(image, null, imgRect, paint);
					}
				}

				if (_imageButton.Caption != null)
				{
					paint.TextSize = (float)_imageButton.FontSize * density;
					paint.Color = _imageButton.IsEnabled ? _imageButton.EnabledTextColor.ToAndroid() : _imageButton.DisabledTextColor.ToAndroid();

					var textRect = new Rect();
					paint.GetTextBounds(_imageButton.Caption, 0, _imageButton.Caption.Length, textRect);
					var textTop = height / 2 - textRect.CenterY();
					float textLeft = 0;
					switch (_imageButton.TextAlignment)
					{
						case Xamarin.Forms.TextAlignment.Start:
							textLeft = beforeText;
							break;
						case Xamarin.Forms.TextAlignment.Center:
							textLeft = beforeText + (width - afterText - beforeText) / 2 - textRect.CenterX();
							break;
						case Xamarin.Forms.TextAlignment.End:
							textLeft = width - afterText - textRect.Width();
							break;
					}

					canvas.DrawText(_imageButton.Caption, 0, _imageButton.Caption.Length, textLeft, textTop, paint);
				}
			}
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using S4mp.Controls.Scanner;
using S4mp.Droid.Renderers;

using Com.Datalogic.Decode;

[assembly: ExportRenderer(typeof(ScannerView), typeof(ScannerViewRenderer))]
namespace S4mp.Droid.Renderers
{
    public class ScannerViewRenderer : ViewRenderer
    {
#if DATALOGIC
        private Scanner.DatalogicReadListener _listener;
#endif
        private ScannerView _scannerView;

        public ScannerViewRenderer(Context context) : base(context)
        {
            
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.View> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null)
            {
                _scannerView = e.NewElement as ScannerView;
                _scannerView.Listen += _scannerView_Listen;
            }
        }

        public void Torch(bool on)
        {
            var camera = Android.Hardware.Camera.Open();

            var p = camera.GetParameters();
            var supportedFlashModes = p.SupportedFlashModes;

            if (supportedFlashModes == null)
                supportedFlashModes = new List<string>();

            var flashMode = string.Empty;

            if (on)
            {
                if (supportedFlashModes.Contains(Android.Hardware.Camera.Parameters.FlashModeTorch))
                    flashMode = Android.Hardware.Camera.Parameters.FlashModeTorch;
                else if (supportedFlashModes.Contains(Android.Hardware.Camera.Parameters.FlashModeOn))
                    flashMode = Android.Hardware.Camera.Parameters.FlashModeOn;
            }
            else
            {
                if (supportedFlashModes.Contains(Android.Hardware.Camera.Parameters.FlashModeOff))
                    flashMode = Android.Hardware.Camera.Parameters.FlashModeOff;
            }

            if (!string.IsNullOrEmpty(flashMode))
            {
                p.FlashMode = flashMode;
                camera.SetParameters(p);
            }
        }

        /// <summary>
        /// setups or cleans scanner listener
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _scannerView_Listen(object sender, bool e)
        {
            if (e)
            {
                //Torch(true);
#if DATALOGIC
                try
                {
                    _listener = new Scanner.DatalogicReadListener();
                    _listener.ScannerResult += _listener_ScannerResult;
                }
                catch (Exception ex)
                {
                    _scannerView.Error = ex.Message;
                }
#else
                _scannerView.GetTestData += (s, args) =>
                {
                    _scannerView.ID = debug_settings.CodeID;
                    _scannerView.Text = debug_settings.CodeText;
                    _scannerView.OnValueScanned(ScannerView.BarCodeTypeEnum.Ean);
                };
#endif
            }
            else
            {
#if DATALOGIC
                _listener.Dispose();
#endif
            }
        }

        private void _listener_ScannerResult(object sender, IDecodeResult e)
        {
            _scannerView.ID = e.BarcodeID.ToString();
            _scannerView.Text = e.Text;
            _scannerView.RawData = e.GetRawData();
            var barCodeType =
                e.BarcodeID == BarcodeID.MicroQr ||
                e.BarcodeID == BarcodeID.Qrcode ||
                e.BarcodeID == BarcodeID.Gs1128 ||
                e.BarcodeID == BarcodeID.Gs114 ||
                e.BarcodeID == BarcodeID.Gs1Exp ||
                e.BarcodeID == BarcodeID.Gs1Limit ||
                e.BarcodeID == BarcodeID.Ean13 ||
                e.BarcodeID == BarcodeID.Ean13Addon2 ||
                e.BarcodeID == BarcodeID.Ean13Addon5 ||
                e.BarcodeID == BarcodeID.Ean13Isbn ||
                e.BarcodeID == BarcodeID.Ean13Issn ||
                e.BarcodeID == BarcodeID.Ean8 ||
                e.BarcodeID == BarcodeID.Ean8Addon2 ||
                e.BarcodeID == BarcodeID.Ean8Addon5 ?
                ScannerView.BarCodeTypeEnum.Ean :
                ScannerView.BarCodeTypeEnum.Undefined;

            _scannerView.OnValueScanned(barCodeType);
        }
    }
}
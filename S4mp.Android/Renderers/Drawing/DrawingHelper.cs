using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;

using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;

namespace S4mp.Droid.Renderers.Drawing
{
    public class DrawingHelper
    {
        public Bitmap GetBitmap(ImageSource source, Context context)
        {
            Bitmap bitmap = null;
            var task = Task.Run(async () => {
                bitmap = await GetBitmapAsync(source, context);
                });
            task.Wait();

            return bitmap;
        }

        public async Task<Bitmap> GetBitmapAsync(ImageSource source, Context context)
        {
            IImageSourceHandler handler;

            if (source is FileImageSource)
            {
                handler = new FileImageSourceHandler();
            }
            else if (source is StreamImageSource)
            {
                handler = new StreamImagesourceHandler(); // sic
            }
            else if (source is UriImageSource)
            {
                handler = new ImageLoaderSourceHandler(); // sic
            }
            else
            {
                return null;
            }

            return await handler.LoadImageAsync(source, context);
        }

        public byte[] GetImgBytes(ImageSource imageSource, Context context)
        {
            using (var ms = new System.IO.MemoryStream())
            {
                using (var bitmap = GetBitmap(imageSource, context))
                {
                    try
                    {
                        bitmap.Compress(Bitmap.CompressFormat.Jpeg, 100, ms);
                    }
                    finally
                    {
                        bitmap.Recycle();
                    }
                }

                return ms.ToArray();
            }
        }

        public byte[] GetBitmapBytes(Bitmap bmp)
        {
            return GetBitmapBytes(bmp, Bitmap.CompressFormat.Jpeg);
        }

        public byte[] GetBitmapBytes(Bitmap bmp, Bitmap.CompressFormat format)
        {
            using (var ms = new System.IO.MemoryStream())
            {
                bmp.Compress(format, 100, ms);

                return ms.ToArray();
            }
        }

        public static PointF[] PointsToFloat(Android.Graphics.Point[] source)
        {
            return source.Select(s => new PointF(s.X, s.Y)).ToArray();
        }

        public static Xamarin.Forms.Point[] PointsToXamarin(PointF[] source)
        {
            return source.Select(s => new Xamarin.Forms.Point(s.X, s.Y)).ToArray();
        }

        public static PointF[] XPointsToPoints(Xamarin.Forms.Point[] source)
        {
            return source.Select(s => new PointF((float)s.X, (float)s.Y)).ToArray();
        }
    }
}
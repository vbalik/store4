﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Xamarin.Forms;

using S4mp.Droid;
using S4mp.Controls.Dialogs;
using S4mp.Core;

[assembly: Dependency(typeof(EntryPopupLoader))]
namespace S4mp.Droid
{
    public class EntryPopupLoader : IEntryPopupLoader
    {
        private AlertDialog dialog;

        public void ShowPopup(EntryPopup popup, Func<string, bool> validateResult)
        {
            //var alert = new AlertDialog.Builder(Forms.Context);

            // Build the dialog.
            var builder = new AlertDialog.Builder(Forms.Context);
            var edit = new EditText(Forms.Context) { Text = popup.Text };
            builder.SetView(edit);
            builder.SetTitle(popup.Title);

            builder.SetPositiveButton(Consts.OK_BUTTON, (EventHandler<DialogClickEventArgs>)null);
            builder.SetNegativeButton(Consts.CANCEL_BUTTON, (EventHandler<DialogClickEventArgs>)null);

            dialog = builder.Create();

            // Show the dialog. This is important to do before accessing the buttons.
            dialog.Show();

            // Get the buttons.
            var btnOK = dialog.GetButton((int)DialogButtonType.Positive);
            var btnCancel = dialog.GetButton((int)DialogButtonType.Negative);

            EventHandler okAction = (sender, args) =>
            {
                if (validateResult == null || validateResult(edit.Text))
                {
                    popup.OnPopupClosed(new EntryPopupClosedArgs
                    {
                        Button = Consts.OK_BUTTON,
                        Text = edit.Text
                    });
                    dialog.Dismiss();
                }
            };

            // Assign our handlers.
            btnOK.Click += okAction;

            btnCancel.Click += (sender, args) =>
            {
                popup.OnPopupClosed(new EntryPopupClosedArgs
                {
                    Button = Consts.CANCEL_BUTTON,
                    Text = edit.Text
                });
                // Dismiss dialog.
                dialog.Dismiss();
            };

            edit.EditorAction += (s, e) =>
            {
                if (e.Event.Action == KeyEventActions.Down && e.Event.KeyCode == Keycode.Enter)
                    okAction(s, e);
            };
        }

        public void Dismiss()
        {
            dialog.Dismiss();
        }
    }
}
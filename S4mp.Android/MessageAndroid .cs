using Android.App;
using Android.Graphics;
using Android.Views;
using Android.Widget;
using S4mp.Droid;
using S4mp.Interfaces;
using System.Drawing;

[assembly: Xamarin.Forms.Dependency(typeof(MessageAndroid))]
namespace S4mp.Droid
{
    public class MessageAndroid : IMessage
    {
        public void LongAlert(string message)
        {
            Toast.MakeText(Application.Context, message, ToastLength.Long).Show();
        }

        public void ShortAlert(string message)
        {
            Toast.MakeText(Application.Context, message, ToastLength.Short).Show();
        }

        public void ErrorAlert(string message)
        {
            var t = Toast.MakeText(Application.Context, "INTERN� CHYBA: \r\n" + message, ToastLength.Long);
            var c = Android.Graphics.Color.Red;
            var cf = new ColorMatrixColorFilter(new float[]
            {
                0,0,0,0,c.R,
                0,0,0,0,c.G,
                0,0,0,0,c.B,
                0,0,0,1,0            
            });
            t.View.Background.SetColorFilter(cf);
            t.SetGravity(GravityFlags.Center | GravityFlags.Center, 0, 0);
            t.Show();
        }
    }
}
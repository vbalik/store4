﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Xamarin.Forms;
using S4mp.Droid;
using S4mp.Core;

[assembly: Dependency(typeof(AndroidSettingsTimeResolver))]
namespace S4mp.Droid
{
    public class AndroidSettingsTimeResolver : ISettingsTimeResolver
    {
        public void OpenSettingsTime()
        {
            MainActivity.Instance.StartActivity(new Intent(Android.Provider.Settings.ActionDateSettings));
        }
    }
}
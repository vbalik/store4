﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net;

using Android;
using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.OS;
using S4.Core;
using S4mp.Core;
using S4mp.Client;

namespace S4mp.Droid
{
    [Activity(Label = "S4mp", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.Portrait)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        internal static MainActivity Instance { get; set; }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);

            // You may use ServicePointManager here
            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;

            AppDomain.CurrentDomain.UnhandledException += CurrentDomainOnUnhandledException;
            TaskScheduler.UnobservedTaskException += TaskSchedulerOnUnobservedTaskException;
            AndroidEnvironment.UnhandledExceptionRaiser += HandleAndroidException;

            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(savedInstanceState);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);
#if !DATALOGIC
            ZXing.Net.Mobile.Forms.Android.Platform.Init();
            ZXing.Mobile.MobileBarcodeScanner.Initialize(Application);
#endif

#if DEBUG
            Singleton<Settings.LocalSettings>.Instance.Init($"<settings><api_url>{S4mp.debug_settings.ApiUrl}</api_url><connect_scanner_web>False</connect_scanner_web><user>{S4mp.debug_settings.User}</user><password>{S4mp.debug_settings.Password}</password><textsize>{S4mp.debug_settings.TextSize}</textsize></settings>");
#else
            Singleton<Settings.LocalSettings>.Instance.Init(null);
#endif
            var verName = this.PackageManager.GetPackageInfo(this.PackageName, 0).VersionName;
            var verCode = this.PackageManager.GetPackageInfo(this.PackageName, 0).VersionCode;
            var appVersion = string.Format("v. {0}, {1}", verName, verCode);
            var osVersion = string.Format("Android {0} {1} SDK{2}", Build.VERSION.Release, Build.VERSION.SdkInt, Build.VERSION.Sdk);

            var mono = string.Format("Mono {0}", Mono.Runtime.GetDisplayName());
            var hwName = string.Format("{0} {1}", Build.Brand, Build.Model);
            var app = new App(hwName, appVersion, osVersion, mono);

            app.SaveSettingDelegate += (sender, e) =>
                Singleton<Settings.LocalSettings>.Instance.Save();

            var disp = WindowManager.DefaultDisplay;
            var met = new Android.Util.DisplayMetrics();
            disp.GetMetrics(met);
            S4mp.Core.Consts.DENSITY_COEF = met.Density;

            CheckPermissions();

            Instance = this;

            LoadApplication(app);
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        private void CheckPermissions()
        {
            if ((int)Build.VERSION.SdkInt < 23)
            {
                return;
            }

            string[] permissions = new string[]
            {
                Manifest.Permission.ReadExternalStorage,
                Manifest.Permission.WriteExternalStorage,
                Manifest.Permission.Camera,
                Manifest.Permission.Flashlight,
                Manifest.Permission.Internet
            };
            var permissionsForRequest = new List<string>();

            foreach (var permission in permissions)
            {
                if (CheckSelfPermission(permission) != (int)Permission.Granted)
                {
                    permissionsForRequest.Add(permission);
                }
            }

            if (permissionsForRequest.Count == 0)
                return;

            RequestPermissions(permissionsForRequest.ToArray(), 1);
        }



        #region Android exception

        private void HandleAndroidException(object sender, RaiseThrowableEventArgs e)
        {
            ProcessException(e.Exception);
            e.Handled = true;
        }

        private void TaskSchedulerOnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e)
        {
            var ex = new Exception("TaskSchedulerOnUnobservedTaskException", e.Exception);
            ProcessException(ex);
        }

        private void CurrentDomainOnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            var ex = new Exception(nameof(CurrentDomainOnUnhandledException), e.ExceptionObject as Exception);
            ProcessException(ex);
        }

        private void ProcessException(Exception ex)
        {
            System.Console.Write(ex.Message);

            /// exclude common errors

            // worker ex
            if (ex is WorkerIsNullException || ex.InnerException is WorkerIsNullException)
                return;

            // OnEditorAction ex
            if (ex is NullReferenceException && ex.StackTrace.Contains("Android.Widget.TextView.IOnEditorActionListener.OnEditorAction"))
                return;

            // on dialog closed
            if ((ex is NullReferenceException && ex.StackTrace.Contains("Dialog_Closed")) || (ex.InnerException is NullReferenceException && ex.InnerException.StackTrace.Contains("Dialog_Close")))
                return;

            if (Singleton<BaseInfo>.Instance.AppStatus != BaseInfo.AppStatusEnum.Unknown && Singleton<BaseInfo>.Instance.AppStatus != BaseInfo.AppStatusEnum.ConnectionError)
                S4mp.App.SendClientError(ex);

            // if bad request 400 do not show client error
            if(!(ex is ApiException_BadRequest))
                S4mp.Dialogs.Dialog.ShowError(ex, App.Current.MainPage);
        }

        #endregion
    }
}
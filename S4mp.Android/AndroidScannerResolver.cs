﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Xamarin.Forms;

using Com.Datalogic.Decode;

using S4mp.Controls;
using S4mp.Droid;
using S4mp.Controls.Scanner;
using S4mp.Core;
using S4mp.Core.EANCode;
using S4mp.Core.Logger;
using S4.Core.Utils;

[assembly: Dependency(typeof(AndroidScannerResolver))]
namespace S4mp.Droid
{
    public class AndroidScannerResolver : IScannerResolver
    {
        private BarcodeID[] EanTypes => new BarcodeID[]
        {
                    BarcodeID.Interleaved25,
                    BarcodeID.Matrix25,
                    BarcodeID.Maxicode,
                    BarcodeID.Micropdf417,
                    BarcodeID.MicroQr,
                    BarcodeID.Msi,
                    BarcodeID.UpceAddon2,
                    BarcodeID.Qrcode,
                    BarcodeID.Trioptic,
                    BarcodeID.Upca,
                    BarcodeID.UpcaAddon2,
                    BarcodeID.Gs1Limit,
                    BarcodeID.Gs1Exp,
                    BarcodeID.Gs114,
                    BarcodeID.UpceAddon5,
                    BarcodeID.Aztec,
                    BarcodeID.Codabar,
                    BarcodeID.Code128,
                    BarcodeID.Code32,
                    BarcodeID.Code39,
                    BarcodeID.Code39Fullascii,
                    BarcodeID.Code93,
                    BarcodeID.Datamatrix,
                    BarcodeID.Discrete25,
                    BarcodeID.Ean13,
                    BarcodeID.Ean13Addon2,
                    BarcodeID.Ean13Addon5,
                    BarcodeID.Ean13Isbn,
                    BarcodeID.Ean13Issn,
                    BarcodeID.Ean8,
                    BarcodeID.Ean8Addon2,
                    BarcodeID.Ean8Addon5,
                    BarcodeID.Gs1128,
                    BarcodeID.UpcaAddon5,
                    BarcodeID.Upce,
        };

        private object _lock = new object();
        private ICustomEventLogger _logger;

#if CSCANNER
        private static ZXing.Mobile.MobileBarcodeScanner _scanner;
#endif
#if DATALOGIC
        private static Scanner.DatalogicReadListener _listener;
#endif
#if HONEYWELL
        private static Controls.Scanner.HoneywellScanner _honeywellScanner;
#endif
        public void Init(bool start, EventHandler<BarCodeTypeEnum> handler)
        {
            _logger = DependencyService.Get<ICustomEventLogger>();

            lock (_lock)
            {
                if (start)
                {
#if CSCANNER
                    try
                    {
                        if (_scanner == null)
                        {
                            _scanner = new ZXing.Mobile.MobileBarcodeScanner();
                            _scanner.UseCustomOverlay = true;
                            var zxingOverlay = LayoutInflater.FromContext(MainActivity.Instance).Inflate(Resource.Layout.ZxingOverlay, null);
                            var flashButton = zxingOverlay.FindViewById<Android.Widget.Button>(Resource.Id.buttonZxingFlash);
                            flashButton.Click += (s, args) => _scanner.ToggleTorch();
                            _scanner.CustomOverlay = zxingOverlay;
                        }
                    }
                    catch (Exception ex)
                    {
                        Error = ex.Message;
                    }
#endif
#if DATALOGIC
                    try
                    {
                        if (_listener == null)
                        {
                            _listener = new Scanner.DatalogicReadListener();
                            _listener.ScannerResult += _listener_ScannerResult;
                        }
                    }
                    catch (Exception ex)
                    {
                        Error = ex.Message;
                    }
#endif
#if HONEYWELL
                    try
                    {
                        _honeywellScanner = new HoneywellScanner();
                        _honeywellScanner.OpenBarcodeReader();
                        _honeywellScanner.CodeScanned += _honeywellScanner_CodeScanned;
                    }
                    catch (Exception ex)
                    {
                        Error = ex.Message;
                    }
#endif

                    // clear all scan events
                    // only one scan event can be attached
                    Scanned = null;

                    if (handler != null)
                        Scanned += handler;
                }
                else
                {
                    if (handler != null)
                        Scanned = null;
                }
            }
        }

        public string ID { get; set; }
        public byte[] RawData { get; set; }
        public string Text { get; set; }

        public EANCode EAN { get; set; }
        public string Error { get; set; }

        private event EventHandler<BarCodeTypeEnum> Scanned;

        public async void Scan()
        {
            await _logger.LogInfo($"{nameof(AndroidScannerResolver)}: Scan started.", "");

#if CSCANNER
            var result = await _scanner.Scan();

            await _logger.LogInfo(string.Format("{0}: Scan result: Test: {1}, RawBytes: {2}, Points: {3}, Metadata: {4}, Format: {5}, Time: {6} ", 
                nameof(AndroidScannerResolver),
                result.Text,
                result.RawBytes,
                result.ResultPoints,
                result.ResultMetadata,
                result.BarcodeFormat,
                result.Timestamp
                ), "");

            HandleScanResult(result);

            void HandleScanResult(ZXing.Result r)
            {
                ID = r.BarcodeFormat.ToString();
                Text = r.Text;
                RawData = r.RawBytes;
                TryToSetEan(BarCodeTypeEnum.Ean);
                Scanned?.Invoke(this, BarCodeTypeEnum.Ean);
            }
#elif DEBUG
            ID = debug_settings.CodeID;
            Text = debug_settings.CodeText;
            TryToSetEan(BarCodeTypeEnum.Ean);
            Scanned?.Invoke(this, BarCodeTypeEnum.Ean);
#endif
        }

        private void _honeywellScanner_CodeScanned(object sender, HoneywellScanner.HoneywellScannedEventArgs e)
        {
            ID = e.ID.ToString();
            Text = e.Text;
            RawData = Encoding.ASCII.GetBytes(e.Text);
            TryToSetEan(e.BarCodeType);
            Scanned?.Invoke(this, e.BarCodeType);
        }

        private void _listener_ScannerResult(object sender, Com.Datalogic.Decode.IDecodeResult e)
        {
            ID = e.BarcodeID.ToString();
            Text = e.Text;
            RawData = e.GetRawData();
            var barCodeType = EanTypes.Contains(e.BarcodeID) ? BarCodeTypeEnum.Ean : BarCodeTypeEnum.Undefined;
            TryToSetEan(barCodeType);
            Scanned?.Invoke(this, barCodeType);
        }

        private async void TryToSetEan(BarCodeTypeEnum barCodeType)
        {
            EAN = null; // clear

            try
            {
                switch (barCodeType)
                {
                    case BarCodeTypeEnum.Ean:
                        EAN = EANCodeFactory.Create(Text);
                        break;
                }
            }
            catch(Exception ex)
            {
                await _logger.LogError($"{nameof(AndroidScannerResolver)}.{nameof(TryToSetEan)}: Failed to set EAN. {ex.Message}", "");
            }

            if (EAN == null)
            {
                Text = Consts.BARCODE_UNKNOWN_VALUE;
                return;
            }

            var barCode = EAN[EANCodePartEnum.ShippingCode]?.Value;
            if (!string.IsNullOrWhiteSpace(barCode))
                Text = barCode;

            await _logger.LogInfo($"{nameof(AndroidScannerResolver)}.{nameof(TryToSetEan)}: Recognized barcode: {Text}", "");
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Com.Datalogic.Decode;

namespace S4mp.Droid.Scanner
{
    public class DatalogicReadListener : Activity, IReadListener, Interfaces.IScanner
    {
        private BarcodeManager decoder = null;

        public DatalogicReadListener()
        {
            if (decoder == null)
            {
                decoder = new BarcodeManager();
                decoder.AddReadListener(this);
            }
        }

        protected override void Dispose(bool disposing)
        {
            decoder.RemoveReadListener(this);

            base.Dispose(disposing);
        }

        public event EventHandler<IDecodeResult> ScannerResult;

        public void OnRead(IDecodeResult p0)
        {
            ScannerResult?.Invoke(this, p0);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Xamarin.Forms;
using S4mp.Droid;
using S4mp.Core;

[assembly: Dependency(typeof(AndroidColorsResolver))]
namespace S4mp.Droid
{
    public class AndroidColorsResolver : IColorsResolver
    {
        public Color GetNamedColor(Colors.NamedColorEnum color)
        {
            var colorAttribure = new Android.Util.TypedValue();
            int attributeId;
            switch (color)
            {
                case Colors.NamedColorEnum.ColorPrimary: attributeId = Resource.Attribute.colorPrimary; break;
                case Colors.NamedColorEnum.TooltipForeground: attributeId = Resource.Attribute.tooltipForegroundColor; break;
                default: throw new NotImplementedException();
            }

            MainActivity.Instance.Theme.ResolveAttribute(attributeId, colorAttribure, true);
            var droidColor = new Android.Graphics.Color(colorAttribure.Data);
            return Color.FromRgb(droidColor.R, droidColor.G, droidColor.B);
        }
    }
}
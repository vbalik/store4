﻿
using projectname.Droid;
using S4mp.Core;

[assembly: Xamarin.Forms.Dependency(typeof(AndroidDevice))]
namespace projectname.Droid
{
    public class AndroidDevice : IDevice
    {
        public string GetIdentifier()
        {
            return Android.Provider.Settings.Secure.GetString(Android.App.Application.Context.ContentResolver, Android.Provider.Settings.Secure.AndroidId);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Com.Datalogic.Decode;

namespace S4mp.Droid.Interfaces
{
    public interface IScanner
    {
        event EventHandler<IDecodeResult> ScannerResult;
    }
}
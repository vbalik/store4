﻿using S4.Core.Cache;
using S4.StoreCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace S4.Picking
{
    class OnStoreCache
    {
        private readonly ICache<Entities.Models.SectionModel, string> _sectionModelCache;
        private readonly IOnStore _onStore;
        private Dictionary<string, List<OnStoreItemFull>> _cache = new Dictionary<string, List<OnStoreItemFull>>();

        public OnStoreCache(ICache<Entities.Models.SectionModel, string> sectionModelCache, IOnStore onStore)
        {
            _sectionModelCache = sectionModelCache;
            _onStore = onStore;
        }


        public void Preload(List<int> articleIDs)
        {
            // get store content for all items
            articleIDs = articleIDs.Distinct().ToList();
            var storeContent = _onStore.ByArticle(articleIDs, ResultValidityEnum.NotValid);
            storeContent = FilterExpired(storeContent);

            // save to cache
            _cache.Clear();
            foreach (var articleID in articleIDs)
                _cache.Add($"{articleID}", storeContent.Where(i => i.ArticleID == articleID).ToList());
        }



        public List<OnStoreItemFull> GetOnStore(int articleID, string secID)
        {
            var key = $"{articleID}";
            List<OnStoreItemFull> storeContent2 = null;
            if (_cache.ContainsKey(key))
                storeContent2 = _cache[key];
            else
            {
                var storeContent = _onStore.ByArticle(articleID, ResultValidityEnum.NotValid, true);

                storeContent = FilterExpired(storeContent);
                _cache.Add(key, storeContent);
                storeContent2 = storeContent;
            }

            // filter content by section ID
            var sect = _sectionModelCache.GetItem(secID);
            if (sect == null)
                throw new Exception($"Section not found; secID: {secID}");            
            storeContent2 = storeContent2.Where(i => sect.PositionIDs.Contains(i.PositionID)).ToList();

            // filter content by PosStatus
            storeContent2 = storeContent2.Where(sc => sc.Position.PosStatus == Entities.Position.POS_STATUS_OK).ToList();

            return storeContent2;
        }


        private List<OnStoreItemFull> FilterExpired(List<OnStoreItemFull> storeContent)
        {
            // filter - remove already expired items
            var limitDate = DateTime.Today;
            return storeContent.Where(i => i.ExpirationDate == null || i.ExpirationDate > limitDate).ToList();
        }
    }
}

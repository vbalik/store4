﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using S4.Core.Extensions;
using S4.StoreCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace S4.Picking
{
    public class PickingHistoryNode
    {
        public PickingHistoryNode()
        {
            RootNode = this;
        }


        [JsonConverter(typeof(StringEnumConverter))]
        public enum HistoryNodeResultEnum
        {
            None,
            OK,
            NotFound
        }


        public class OnStoreSnapshotItem
        {
            [JsonProperty(PropertyName = "positionID")]
            public int PositionID { get; set; }
            [JsonProperty(PropertyName = "posCode")]
            public string PosCode { get; set; }
            [JsonProperty(PropertyName = "stoMoveLotID")]
            public int StoMoveLotID { get; set; }
            [JsonProperty(PropertyName = "carrierNum")]
            public string CarrierNum { get; set; }
            [JsonProperty(PropertyName = "quantity")]
            public decimal Quantity { get; set; }
            [JsonProperty(PropertyName = "batchNum")]
            public string BatchNum { get; set; }
            [JsonProperty(PropertyName = "expirationDate")]
            public DateTime? ExpirationDate { get; set; }
        }

        public class BookingSnapshotItem
        {            
            [JsonProperty(PropertyName = "stoMoveLotID")]
            public int StoMoveLotID { get; set; }
            [JsonProperty(PropertyName = "bookingID")]         
            public int BookingID { get; internal set; }
            [JsonProperty(PropertyName = "partnerID")]
            public int PartnerID { get; internal set; }
            [JsonProperty(PropertyName = "partAddrID")]
            public int PartAddrID { get; internal set; }
            [JsonProperty(PropertyName = "requiredQuantity")]
            public decimal RequiredQuantity { get; internal set; }
        }


        [JsonIgnore]
        public PickingHistoryNode RootNode { get; private set; }

        [JsonProperty(PropertyName = "message")]
        public string Message { get; set; }

        [JsonProperty(PropertyName = "result")]
        [JsonConverter(typeof(StringEnumConverter))]
        public HistoryNodeResultEnum Result { get; set; }

        [JsonProperty(PropertyName = "onStoreSnapshot")]
        public List<OnStoreSnapshotItem> OnStoreSnapshot { get; set; }

        [JsonProperty(PropertyName = "bookingSnapshot")]
        public List<BookingSnapshotItem> BookingSnapshot { get; set; }

        [JsonProperty(PropertyName = "items")]
        public List<PickingHistoryNode> SubItems { get; set; } = new List<PickingHistoryNode>();


        public PickingHistoryNode AddMessage(string message)
        {
            var newNode = new PickingHistoryNode() { RootNode = this.RootNode, Message = message };
            SubItems.Add(newNode);
            return newNode;
        }


        internal void SaveOnStore(List<OnStoreItemFull> onStore)
        {
            // create copy of onStore list
            OnStoreSnapshot = onStore.Select(o => new OnStoreSnapshotItem() {
                PositionID = o.PositionID,
                PosCode = o.Position?.PosCode,
                StoMoveLotID = o.StoMoveLotID,
                CarrierNum = o.CarrierNum,
                Quantity = o.Quantity,
                BatchNum = o.BatchNum,
                ExpirationDate = o.ExpirationDate
            }).ToList();
        }


        internal void SaveBookings(List<DAL.Interfaces.BookingInfo> bookings)
        {
            // create copy of onStore list
            BookingSnapshot = bookings.Select(o => new BookingSnapshotItem()
            {
                BookingID = o.BookingID,
                PartnerID = o.PartnerID,
                PartAddrID = o.PartAddrID,
                StoMoveLotID = o.StoMoveLotID,
                RequiredQuantity = o.RequiredQuantity
            }).ToList();
        }

        public override string ToString()
        {
            return Message;
        }


        // Newtonsoft.Json serializer helper
        public bool ShouldSerializeResult() => Result != HistoryNodeResultEnum.None;
        public bool ShouldSerializeSubItems() => SubItems.Count > 0;
        public bool ShouldSerializeOnStoreSnapshot() => OnStoreSnapshot != null;
        public bool ShouldSerializeBookingSnapshot() => BookingSnapshot != null;
    }
}

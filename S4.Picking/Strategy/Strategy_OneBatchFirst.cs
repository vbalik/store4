﻿using System;
using System.Collections.Generic;
using System.Text;
using S4.Entities;
using S4.Entities.Models;
using S4.StoreCore.Interfaces;

namespace S4.Picking.Strategy
{
    public class Strategy_OneBatchFirst : IPicking
    {
        public decimal TryToPick(ref PickingHistoryNode history, ArticleModel articleModel, ref List<OnStoreItemFull> onStore, decimal destQuantity, int docPosition, ref List<ResultItem> saveItems, DateTime date)
        {
            decimal dispRemainder = 0;

            var oneBatch = new Method.Method_OneBatch();
            dispRemainder = oneBatch.TryToPick(ref history, articleModel, ref onStore, destQuantity, docPosition, ref saveItems, date);

            // do the rest
            if (dispRemainder > 0)
            {
                var byExpiration = new Method.Method_ByExpiration();
                dispRemainder = byExpiration.TryToPick(ref history, articleModel, ref onStore, dispRemainder, docPosition, ref saveItems, date);
            }

            // return disp procedure remainder
            return dispRemainder;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using S4.Entities.Models;
using S4.Picking.Method;
using S4.StoreCore.Interfaces;


namespace S4.Picking.Strategy
{
    public class Strategy_BigOrSmallFirst : IPicking
    {
        public decimal TryToPick(ref PickingHistoryNode history, ArticleModel articleModel, ref List<OnStoreItemFull> onStore, decimal destQuantity, int docPosition, ref List<ResultItem> saveItems, DateTime date)
        {
            decimal dispRemainder = 0;

            decimal bigPackRelation = articleModel.Packings.Where(p => p.PackDesc == "pal").Select(p => p.PackRelation).FirstOrDefault();
			decimal smallPackRelation = articleModel.Packings.Where(p => p.PackDesc == "kar").Select(p => p.PackRelation).FirstOrDefault();


            if (bigPackRelation > 1 && destQuantity >= bigPackRelation)
            {
                // big pack variant
                var bigPack = new Method_BigPack(bigPackRelation);
                dispRemainder = bigPack.TryToPick(ref history, articleModel, ref onStore, destQuantity, docPosition, ref saveItems, date);
            }
            else if (smallPackRelation > 1)
            {
                // small pack variant

                decimal secQuantity = destQuantity / smallPackRelation;
                decimal bigPart = decimal.Floor(secQuantity) * smallPackRelation;
                decimal smallPart = destQuantity - bigPart;

                // if there is small part
                if (smallPart > 0)
                {
                    // do small part
                    var small = new Method_Small(smallPart);
                    dispRemainder = small.TryToPick(ref history, articleModel, ref onStore, destQuantity, docPosition, ref saveItems, date);
                }
                else
                {
                    // only bigPart
                    var smallPack = new Method_SmallPack(smallPackRelation);
                    dispRemainder = smallPack.TryToPick(ref history, articleModel, ref onStore, destQuantity, docPosition, ref saveItems, date);
                }

            }
            else
            {
                // no secondary packs
                var nearest = new Method_Nearest();
                dispRemainder = nearest.TryToPick(ref history, articleModel, ref onStore, destQuantity, docPosition, ref saveItems, date);
            }


            // do rest
            if (dispRemainder > 0)
            {
                var byExpiration = new Method_ByExpiration();
                dispRemainder = byExpiration.TryToPick(ref history, articleModel, ref onStore, dispRemainder, docPosition, ref saveItems, date);
            }

            // return disp procedure remainder
            return dispRemainder;

        }
    }
}

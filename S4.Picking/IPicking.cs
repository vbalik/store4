﻿using S4.Entities;
using S4.Entities.Models;
using S4.StoreCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Picking
{
    interface IPicking
    {
        decimal TryToPick(ref PickingHistoryNode history, ArticleModel articleModel, ref List<OnStoreItemFull> onStore, decimal destQuantity, int docPosition, ref List<ResultItem> saveItems, DateTime date);
    }
}

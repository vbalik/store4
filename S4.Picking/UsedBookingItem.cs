﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Picking
{
    public class UsedBookingItem
    {
        public int BookingID { get; set; }

        public decimal UsedQuantity { get; set; }
    }
}

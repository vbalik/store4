﻿using System;
using S4.Entities;
using S4.Entities.Models;
using S4.Entities.Settings;
using S4.StoreCore.Interfaces;
using System.Collections.Generic;


namespace S4.Picking
{
    public interface IPlanMaker
    {
        bool SaveOnStoreToHistory { get; set; }
        PickingResult CreatePlan(DirectionModel direction, PrefixSettings prefixSettings, DateTime date, string defaultSection = "VYSK");
    }
}

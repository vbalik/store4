﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Picking
{
    public class ResultItem
    {
        public int PositionID { get; set; }
        public int ArtPackID { get; set; }
        public string CarrierNum { get; set; }
        public decimal Quantity { get; set; }
        public string BatchNum { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public int DocPosition { get; set; }
    }
}

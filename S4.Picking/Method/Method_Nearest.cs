﻿using System;
using System.Collections.Generic;
using System.Text;
using S4.Entities.Models;
using S4.StoreCore.Interfaces;

namespace S4.Picking.Method
{
    class Method_Nearest : IPicking
    {
        public decimal TryToPick(ref PickingHistoryNode history, ArticleModel articleModel, ref List<OnStoreItemFull> onStore, decimal destQuantity, int docPosition, ref List<ResultItem> saveItems, DateTime date)
        {
            var methodHistory = history.AddMessage(nameof(Method_Nearest));

            // find nearest packs
            while (destQuantity > 0)
            {
                var foundNearest = onStore.GetNearestItem(destQuantity);
                if (foundNearest.moveQuantity == 0)
                    break;

                methodHistory.AddMessage($"Found nearest item; moveQuantity: {foundNearest.moveQuantity}; position: {foundNearest.foundItem.Position.PosCode}; carrierNum: {foundNearest.foundItem.CarrierNum}; batchNum: {foundNearest.foundItem.BatchNum}; expirationDate: {foundNearest.foundItem.ExpirationDate:yyyy-MM-dd};");

                // add result line
                saveItems.Add(new ResultItem()
                {
                    Quantity = foundNearest.moveQuantity,
                    PositionID = foundNearest.foundItem.PositionID,
                    ArtPackID = foundNearest.foundItem.ArtPackID,
                    CarrierNum = foundNearest.foundItem.CarrierNum,
                    BatchNum = foundNearest.foundItem.BatchNum,
                    ExpirationDate = foundNearest.foundItem.ExpirationDate,
                    DocPosition = docPosition
                });

                // calc remainder
                destQuantity -= foundNearest.moveQuantity;
            }

            // return remainder
            return destQuantity;
        }
    }
}

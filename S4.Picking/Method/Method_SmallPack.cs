﻿using System;
using System.Collections.Generic;
using System.Text;
using S4.Entities.Models;
using S4.StoreCore.Interfaces;

namespace S4.Picking.Method
{ 
    class Method_SmallPack : IPicking
    {
        private decimal _smallPackQuantity;


        public Method_SmallPack(decimal smallPackQuantity)
        {
            _smallPackQuantity = smallPackQuantity;
        }


        public decimal TryToPick(ref PickingHistoryNode history, ArticleModel articleModel, ref List<OnStoreItemFull> onStore, decimal destQuantity, int docPosition, ref List<ResultItem> saveItems, DateTime date)
        {
            var methodHistory = history.AddMessage(nameof(Method_SmallPack));


            // find equal item
            var foundEqual = onStore.GetEqualItem(destQuantity);
            if (foundEqual.moveQuantity == destQuantity)
            {
                methodHistory.AddMessage($"Found equal item; moveQuantity: {foundEqual.moveQuantity}; position: {foundEqual.foundItem.Position.PosCode}; carrierNum: {foundEqual.foundItem.CarrierNum}; batchNum: {foundEqual.foundItem.BatchNum}; expirationDate: {foundEqual.foundItem.ExpirationDate:yyyy-MM-dd};");

                // add result line
                saveItems.Add(new ResultItem()
                {
                    Quantity = foundEqual.moveQuantity,
                    PositionID = foundEqual.foundItem.PositionID,
                    ArtPackID = foundEqual.foundItem.ArtPackID,
                    CarrierNum = foundEqual.foundItem.CarrierNum,
                    BatchNum = foundEqual.foundItem.BatchNum,
                    ExpirationDate = foundEqual.foundItem.ExpirationDate,
                    DocPosition = docPosition
                });

                // return remainder
                return 0;
            }
            else
            {
                // find nearest aliquot packs
                while (destQuantity > 0)
                {
                    var foundNearest = onStore.GetNearestAliquotItem(destQuantity, _smallPackQuantity);
                    if (foundNearest.moveQuantity == 0)
                        break;

                    methodHistory.AddMessage($"Found nearest aliquot item; moveQuantity: {foundNearest.moveQuantity}; position: {foundNearest.foundItem.Position.PosCode}; carrierNum: {foundNearest.foundItem.CarrierNum}; batchNum: {foundNearest.foundItem.BatchNum}; expirationDate: {foundNearest.foundItem.ExpirationDate:yyyy-MM-dd};");

                    // add result line
                    saveItems.Add(new ResultItem()
                    {
                        Quantity = foundNearest.moveQuantity,
                        PositionID = foundNearest.foundItem.PositionID,
                        ArtPackID = foundNearest.foundItem.ArtPackID,
                        CarrierNum = foundNearest.foundItem.CarrierNum,
                        BatchNum = foundNearest.foundItem.BatchNum,
                        ExpirationDate = foundNearest.foundItem.ExpirationDate,
                        DocPosition = docPosition
                    });

                    // calc remainder
                    destQuantity -= foundNearest.moveQuantity;
                }

                // return remainder
                return destQuantity;
            }
        }
    }
}

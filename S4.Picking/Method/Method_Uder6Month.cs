﻿using System;
using System.Collections.Generic;
using System.Linq;
using S4.Entities.Models;
using S4.StoreCore.Interfaces;

namespace S4.Picking.Method
{
    class Method_Uder6Month : IPicking
    {
        public decimal TryToPick(ref PickingHistoryNode history, ArticleModel articleModel, ref List<OnStoreItemFull> onStore, decimal destQuantity, int docPosition, ref List<ResultItem> saveItems, DateTime date)
        {
            var methodHistory = history.AddMessage(nameof(Method_Uder6Month));

            // last expiration date
            var lastExpiration = date.AddMonths(6);
            var onStoreUnder6 = onStore.Where(i => (i.ExpirationDate ?? DateTime.MaxValue) <= lastExpiration).ToList();

            // while not fullfilled
            while (destQuantity > 0)
            {
                // find next available pos
                var findResult = onStoreUnder6.GetNext(destQuantity);
                if (findResult.moveQuantity == 0)
                    break;

                methodHistory.AddMessage($"Found by expiration under 6 months; moveQuantity: {findResult.moveQuantity}; position: {findResult.foundItem.Position.PosCode}; carrierNum: {findResult.foundItem.CarrierNum}; batchNum: {findResult.foundItem.BatchNum}; expirationDate: {findResult.foundItem.ExpirationDate:yyyy-MM-dd};");

                // add result line
                saveItems.Add(new ResultItem()
                {
                    Quantity = findResult.moveQuantity,
                    PositionID = findResult.foundItem.PositionID,
                    ArtPackID = findResult.foundItem.ArtPackID,
                    CarrierNum = findResult.foundItem.CarrierNum,
                    BatchNum = findResult.foundItem.BatchNum,
                    ExpirationDate = findResult.foundItem.ExpirationDate,
                    DocPosition = docPosition
                });

                destQuantity -= findResult.moveQuantity;
            }

            // update history node
            methodHistory.Result = (destQuantity > 0) ? PickingHistoryNode.HistoryNodeResultEnum.NotFound : PickingHistoryNode.HistoryNodeResultEnum.OK;

            // return remainder
            return destQuantity;
        }
    }
}

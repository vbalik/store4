﻿using System;
using System.Collections.Generic;
using System.Text;
using S4.Entities.Models;
using S4.StoreCore.Interfaces;

namespace S4.Picking.Method
{
    public class Method_OneBatch : IPicking
    {
        public decimal TryToPick(ref PickingHistoryNode history, ArticleModel articleModel, ref List<OnStoreItemFull> onStore, decimal destQuantity, int docPosition, ref List<ResultItem> saveItems, DateTime date)
        {
            var methodHistory = history.AddMessage(nameof(Method_OneBatch));

            var checkResult = onStore.CheckOneBatch(destQuantity);
            if (checkResult.foundQuantity > 0)
            {
                // while not direction item filled
                while (destQuantity > 0)
                {
                    var findResult = onStore.GetByBatch(destQuantity, checkResult.foundBatchNum);
                    if (findResult.moveQuantity == 0)
                        break;

                    methodHistory.AddMessage($"Found one batch; moveQuantity: {findResult.moveQuantity}; position: {findResult.foundItem.Position.PosCode}; carrierNum: {findResult.foundItem.CarrierNum}; batchNum: {findResult.foundItem.BatchNum}; expirationDate: {findResult.foundItem.ExpirationDate:yyyy-MM-dd};");
                    
                    // add result line
                    saveItems.Add(new ResultItem()
                    {
                        Quantity = findResult.moveQuantity,
                        PositionID = findResult.foundItem.PositionID,
                        ArtPackID = findResult.foundItem.ArtPackID,
                        CarrierNum = findResult.foundItem.CarrierNum,
                        BatchNum = findResult.foundItem.BatchNum,
                        ExpirationDate = findResult.foundItem.ExpirationDate,
                        DocPosition = docPosition
                    });

                    destQuantity -= findResult.moveQuantity;
                }
            }

            // update history node
            methodHistory.Result = (destQuantity > 0) ? PickingHistoryNode.HistoryNodeResultEnum.NotFound : PickingHistoryNode.HistoryNodeResultEnum.OK;

            // return remainder
            return destQuantity;
        }
    }
}

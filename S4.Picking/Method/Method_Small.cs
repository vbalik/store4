﻿using System;
using System.Collections.Generic;
using System.Text;
using S4.Entities.Models;
using S4.StoreCore.Interfaces;

namespace S4.Picking.Method
{
    class Method_Small : IPicking
    {
        private decimal _smallQuantity;


        public Method_Small(decimal smallQuantity)
        {
            _smallQuantity = smallQuantity;
        }


        public decimal TryToPick(ref PickingHistoryNode history, ArticleModel articleModel, ref List<OnStoreItemFull> onStore, decimal destQuantity, int docPosition, ref List<ResultItem> saveItems, DateTime date)
        {
            var methodHistory = history.AddMessage(nameof(Method_Small));


            // while not direction item filled
            while (destQuantity > 0)
            {
                var foundSmall = onStore.GetSmallerItem(destQuantity, _smallQuantity);
                if (foundSmall.moveQuantity == 0)
                    break;

                methodHistory.AddMessage($"Found small item; moveQuantity: {foundSmall.moveQuantity}; position: {foundSmall.foundItem.Position.PosCode}; carrierNum: {foundSmall.foundItem.CarrierNum}; batchNum: {foundSmall.foundItem.BatchNum}; expirationDate: {foundSmall.foundItem.ExpirationDate:yyyy-MM-dd};");

                // add result line
                saveItems.Add(new ResultItem()
                {
                    Quantity = foundSmall.moveQuantity,
                    PositionID = foundSmall.foundItem.PositionID,
                    ArtPackID = foundSmall.foundItem.ArtPackID,
                    CarrierNum = foundSmall.foundItem.CarrierNum,
                    BatchNum = foundSmall.foundItem.BatchNum,
                    ExpirationDate = foundSmall.foundItem.ExpirationDate,
                    DocPosition = docPosition
                });

                // calc remainder
                destQuantity -= foundSmall.moveQuantity;
            }

            // return remainder
            return destQuantity;
        }
    }
}

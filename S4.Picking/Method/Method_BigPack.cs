﻿using System;
using System.Collections.Generic;
using System.Text;
using S4.Entities.Models;
using S4.StoreCore.Interfaces;

namespace S4.Picking.Method
{
    public class Method_BigPack : IPicking
    {
        private decimal _bigPackQuantity;


        public Method_BigPack(decimal bigPackQuantity)
        {
            _bigPackQuantity = bigPackQuantity;
        }


        public decimal TryToPick(ref PickingHistoryNode history, ArticleModel articleModel, ref List<OnStoreItemFull> onStore, decimal destQuantity, int docPosition, ref List<ResultItem> saveItems, DateTime date)
        {
            var methodHistory = history.AddMessage(nameof(Method_BigPack));


            // find equal item
            var foundEqual = onStore.GetEqualItem(destQuantity);
            if (foundEqual.moveQuantity == destQuantity)
            {
                methodHistory.AddMessage($"Found equal item; moveQuantity: {foundEqual.moveQuantity}; position: {foundEqual.foundItem.Position.PosCode}; carrierNum: {foundEqual.foundItem.CarrierNum}; batchNum: {foundEqual.foundItem.BatchNum}; expirationDate: {foundEqual.foundItem.ExpirationDate:yyyy-MM-dd};");

                // add result line
                saveItems.Add(new ResultItem()
                {
                    Quantity = foundEqual.moveQuantity,
                    PositionID = foundEqual.foundItem.PositionID,
                    ArtPackID = foundEqual.foundItem.ArtPackID,
                    CarrierNum = foundEqual.foundItem.CarrierNum,
                    BatchNum = foundEqual.foundItem.BatchNum,
                    ExpirationDate = foundEqual.foundItem.ExpirationDate,
                    DocPosition = docPosition
                });

                // return remainder
                return 0;
            }
            else
            {

                // find whole carriers
                while (destQuantity > 0 && destQuantity >= _bigPackQuantity)
                {
                    var foundBigPack = onStore.GetEqualItem(_bigPackQuantity);
                    if (foundBigPack.moveQuantity == 0)
                        break;

                    methodHistory.AddMessage($"Found bigPack size item; moveQuantity: {foundBigPack.moveQuantity}; position: {foundBigPack.foundItem.Position.PosCode}; carrierNum: {foundBigPack.foundItem.CarrierNum}; batchNum: {foundBigPack.foundItem.BatchNum}; expirationDate: {foundBigPack.foundItem.ExpirationDate:yyyy-MM-dd};");

                    // add result line
                    saveItems.Add(new ResultItem()
                    {
                        Quantity = foundBigPack.moveQuantity,
                        PositionID = foundBigPack.foundItem.PositionID,
                        ArtPackID = foundBigPack.foundItem.ArtPackID,
                        CarrierNum = foundBigPack.foundItem.CarrierNum,
                        BatchNum = foundBigPack.foundItem.BatchNum,
                        ExpirationDate = foundBigPack.foundItem.ExpirationDate,
                        DocPosition = docPosition
                    });

                    // calc remainder
                    destQuantity -= foundBigPack.moveQuantity;
                }


                // find bigger item for rest
                if (destQuantity > 0)
                {
                    var foundBigger = onStore.GetBiggerItem(destQuantity);
                    if (foundBigger.moveQuantity == destQuantity)
                    {
                        methodHistory.AddMessage($"Found bigger item; moveQuantity: {foundBigger.moveQuantity}; position: {foundBigger.foundItem.Position.PosCode}; carrierNum: {foundBigger.foundItem.CarrierNum}; batchNum: {foundBigger.foundItem.BatchNum}; expirationDate: {foundBigger.foundItem.ExpirationDate:yyyy-MM-dd};");

                        // add result line
                        saveItems.Add(new ResultItem()
                        {
                            Quantity = foundBigger.moveQuantity,
                            PositionID = foundBigger.foundItem.PositionID,
                            ArtPackID = foundBigger.foundItem.ArtPackID,
                            CarrierNum = foundBigger.foundItem.CarrierNum,
                            BatchNum = foundBigger.foundItem.BatchNum,
                            ExpirationDate = foundBigger.foundItem.ExpirationDate,
                            DocPosition = docPosition
                        });

                        // calc remainder
                        destQuantity -= foundBigger.moveQuantity;
                    }
                }

                // return remainder
                return destQuantity;
            }            
        }
    }
}

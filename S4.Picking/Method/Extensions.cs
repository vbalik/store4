﻿using S4.StoreCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace S4.Picking.Method
{
    public static class Extensions
    {
        public static (decimal moveQuantity, OnStoreItemFull foundItem)
            GetNext(this List<OnStoreItemFull> onStore, decimal reqQuantity)
        {
            // filter zero quantity & order by expiration
            var ordered = onStore.Where(o => o.Quantity > 0).OrderBy(o => o.ExpirationDate);

            // find first item 
            var foundItem = ordered.FirstOrDefault();

            if (foundItem != null)
            {
                decimal moveQuantity = Math.Min(reqQuantity, foundItem.Quantity);
                foundItem.Quantity = foundItem.Quantity - moveQuantity;

                return (moveQuantity, foundItem);
            }
            else
                // not found any
                return (0, null);
        }


        public static (decimal moveQuantity, OnStoreItemFull foundItem)
            GetNextWithQuantity(this List<OnStoreItemFull> onStore, decimal reqQuantity)
        {
            // filter zero quantity & order by expiration, then by quantity
            var ordered = onStore.Where(o => o.Quantity > 0).OrderBy(o => o.ExpirationDate).ThenBy(o => o.Quantity);

            // find first item 
            var foundItem = ordered.FirstOrDefault();

            if (foundItem != null)
            {
                decimal moveQuantity = Math.Min(reqQuantity, foundItem.Quantity);
                foundItem.Quantity = foundItem.Quantity - moveQuantity;

                return (moveQuantity, foundItem);
            }
            else
                // not found any
                return (0, null);
        }


        public static (decimal foundQuantity, string foundBatchNum)
             CheckOneBatch(this List<OnStoreItemFull> onStore, decimal reqQuantity)
        {
            // sum by batch
            var sumByBatch = onStore.GroupBy(k => new { k.ExpirationDate, k.BatchNum }, (k, i) => new { k.ExpirationDate, k.BatchNum, Quantity = i.Sum(o => o.Quantity) });

            // sort by quantity
            var ordered = sumByBatch.OrderBy(o => o.ExpirationDate).ThenBy(o => o.Quantity);

            // find first bigger or equal item
            var foundItem = ordered.FirstOrDefault(o => o.Quantity >= reqQuantity);
            if (foundItem != null)
            {
                return (foundItem.Quantity, foundItem.BatchNum);
            }
            else
            {
                // not found any
                return (0, null);
            }
        }


        public static (decimal moveQuantity, OnStoreItemFull foundItem)
            GetByBatch(this List<OnStoreItemFull> onStore, decimal reqQuantity, string batchNumber)
        {

            // filter by batch
            var byBatch = onStore.Where(o => o.BatchNum == batchNumber && o.Quantity > 0);

            // sort by position
            var ordered = byBatch.OrderBy(o => o.PositionID);

            // find first item with proper batchNum
            var foundItem = ordered.FirstOrDefault();

            if (foundItem != null)
            {
                decimal moveQuantity = Math.Min(reqQuantity, foundItem.Quantity);
                foundItem.Quantity = foundItem.Quantity - moveQuantity;

                return (moveQuantity, foundItem);
            }
            else
                // not found any
                return (0, null);
        }



        public static (decimal moveQuantity, OnStoreItemFull foundItem)
            GetEqualItem(this List<OnStoreItemFull> onStore, decimal reqQuantity)
        {
            // filter zero quantity & order by expiration
            var ordered = onStore.Where(o => o.Quantity > 0).OrderBy(o => o.ExpirationDate);

            // find first item with same quantity
            var foundItem = ordered.FirstOrDefault(o => o.Quantity == reqQuantity);

            if (foundItem != null)
            {
                decimal moveQuantity = Math.Min(reqQuantity, foundItem.Quantity);
                foundItem.Quantity = foundItem.Quantity - moveQuantity;

                return (moveQuantity, foundItem);
            }
            else
                // not found any
                return (0, null);
        }


        public static (decimal moveQuantity, OnStoreItemFull foundItem)
            GetNearestItem(this List<OnStoreItemFull> onStore, decimal reqQuantity)
        {

            // filter zero quantity & order by quantity difference
            var ordered = onStore.Where(o => o.Quantity > 0).OrderBy(o => Math.Abs(o.Quantity - reqQuantity));

            // find first item
            var foundItem = ordered.FirstOrDefault();

            if (foundItem != null)
            {
                decimal moveQuantity = Math.Min(reqQuantity, foundItem.Quantity);
                foundItem.Quantity = foundItem.Quantity - moveQuantity;

                return (moveQuantity, foundItem);
            }
            else
                // not found any
                return (0, null);
        }


        public static (decimal moveQuantity, OnStoreItemFull foundItem)
            GetNearestAliquotItem(this List<OnStoreItemFull> onStore, decimal reqQuantity, decimal packQuantity)
        {

            // filter zero quantity & remainder == 0 & order by quantity difference
            var ordered = onStore.Where(o => o.Quantity > 0 && Decimal.Remainder(o.Quantity, packQuantity) == 0).OrderBy(o => Math.Abs(o.Quantity - reqQuantity));

            // find first item
            var foundItem = ordered.FirstOrDefault();

            if (foundItem != null)
            {
                decimal moveQuantity = Math.Min(reqQuantity, foundItem.Quantity);
                foundItem.Quantity = foundItem.Quantity - moveQuantity;

                return (moveQuantity, foundItem);
            }
            else
                // not found any
                return (0, null);
        }


        public static (decimal moveQuantity, OnStoreItemFull foundItem)
            GetSmallerItem(this List<OnStoreItemFull> onStore, decimal reqQuantity, decimal maxQuantity)
        {

            // filter zero quantity & order by quantity
            var ordered = onStore.Where(o => o.Quantity > 0 && o.Quantity <= maxQuantity).OrderBy(o => o.Quantity);

            // find first item
            var foundItem = ordered.FirstOrDefault();

            if (foundItem != null)
            {
                decimal moveQuantity = Math.Min(reqQuantity, foundItem.Quantity);
                foundItem.Quantity = foundItem.Quantity - moveQuantity;

                return (moveQuantity, foundItem);
            }
            else
                // not found any
                return (0, null);
        }


        public static (decimal moveQuantity, OnStoreItemFull foundItem)
            GetBiggerItem(this List<OnStoreItemFull> onStore, decimal reqQuantity)
        {

            // filter zero quantity & order by quantity
            var ordered = onStore.Where(o => o.Quantity > reqQuantity).OrderBy(o => o.Quantity);

            // find first item
            var foundItem = ordered.FirstOrDefault();

            if (foundItem != null)
            {
                decimal moveQuantity = Math.Min(reqQuantity, foundItem.Quantity);
                foundItem.Quantity = foundItem.Quantity - moveQuantity;

                return (moveQuantity, foundItem);
            }
            else
                // not found any
                return (0, null);
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using S4.Core.Cache;
using S4.DAL.Interfaces;
using S4.Entities;
using S4.Entities.Models;
using S4.Entities.Settings;
using S4.StoreCore.Interfaces;

namespace S4.Picking
{
    public class PlanMaker : IPlanMaker
    {
        private readonly ICache<ArticlePacking, int> _articlePackingCache;
        private readonly ICache<ArticleModel, int> _articleModelCache;
        private readonly ICache<Manufact, string> _manufactCache;
        private readonly ICache<SectionModel, string> _sectionModelCache;
        private readonly IOnStore _onStore;
        private readonly IBookings _bookings;

        public PlanMaker(
            ICache<ArticlePacking, int> articlePackingCache,
            ICache<ArticleModel, int> articleModelCache,
            ICache<Manufact, string> manufactCache,
            ICache<SectionModel, string> sectionModelCache,
            IOnStore onStore,
            IBookings bookings)
        {
            _articlePackingCache = articlePackingCache;
            _articleModelCache = articleModelCache;
            _manufactCache = manufactCache;
            _sectionModelCache = sectionModelCache;
            _onStore = onStore;
            _bookings = bookings;
        }


        public bool SaveOnStoreToHistory { get; set; }


        public PickingResult CreatePlan(DirectionModel direction, PrefixSettings prefixSettings, DateTime date, string defaultSection = "VYSK")
        {
            // prepare
            var sectionsInOrder = prefixSettings.DispatchSections.OrderBy(s => s.Order).Select(s => s.SectID).ToList();
            if (!sectionsInOrder.Any())
                sectionsInOrder.Add(defaultSection);
            var historyRoot = new PickingHistoryNode() { Message = $"{prefixSettings.DispatchDistribution} ditribution" };

            switch (prefixSettings.DispatchDistribution)
            {
                case PrefixSettings.DispatchDistributionEnum.OneFromOneSection:
                    return OneFromOneSection(ref historyRoot, direction, sectionsInOrder, date);
                case PrefixSettings.DispatchDistributionEnum.AllFromOneSection:
                    return AllFromOneSection(ref historyRoot, direction, sectionsInOrder, date);
                default:
                    throw new NotImplementedException(prefixSettings.DispatchDistribution.ToString());
            }
        }


        private PickingResult OneFromOneSection(ref PickingHistoryNode history, DirectionModel direction, List<string> sectionsInOrder, DateTime date)
        {
            bool directionIsDone = true;
            var saveItems = new List<ResultItem>();
            var notFound = new List<NotFoundItem>();
            var usedBookings = new List<UsedBookingItem>();
            var onStoreCache = new OnStoreCache(_sectionModelCache, _onStore);

            // preload on store cache
            //var articleIDs = direction.Items.Select(di => _articlePackingCache.GetItem(di.ArtPackID).ArticleID).ToList();
            //onStoreCache.Preload(articleIDs);

            // for each direction item ...
            foreach (var dirItem in direction.Items)
            {
                var dirItemHistory = history.AddMessage($"Item '{dirItem.ArticleCode}' ({dirItem.ArtPackID}); quantity: {dirItem.Quantity}");
                bool dirItemIsDone = false;


                // create sections list
                List<string> sectionsList = null;

                if (!direction.XtraData.IsNull(Direction.XTRA_DISPATCH_DIRECT_SECTION_LIST, dirItem.DocPosition))
                {
                    // use item direct section list
                    var directSectionsList = (string)direction.XtraData[Direction.XTRA_DISPATCH_DIRECT_SECTION_LIST, dirItem.DocPosition];
                    sectionsList = (directSectionsList).Split('|').ToList();
                    dirItemHistory.AddMessage($"DirectSectionsList: {directSectionsList}");
                }
                else
                {
                    // no direct section list - use sections from profile
                    sectionsList = sectionsInOrder.ToList();

                    if (!direction.XtraData.IsNull(Direction.XTRA_DISPATCH_ALTER_SECTION, dirItem.DocPosition))
                    {
                        // add alternative section to list
                        var alternativeSection = (string)direction.XtraData[Direction.XTRA_DISPATCH_ALTER_SECTION, dirItem.DocPosition];
                        sectionsList.Add(alternativeSection);
                        dirItemHistory.AddMessage($"AlternativeSection: {alternativeSection}");
                    }
                }


                // ... try sections in order
                foreach (var secID in sectionsList)
                {
                    var sectionHistory = dirItemHistory.AddMessage($"Section '{secID}'");

                    var isDone = TryPickItems(ref sectionHistory, ref onStoreCache, new List<DirectionItem>() { dirItem }, secID, ref saveItems, ref notFound, ref usedBookings, date, direction.PartnerID, direction.PartAddrID);
                    if (isDone)
                    {
                        dirItemIsDone = true;
                        dirItemHistory.Result = PickingHistoryNode.HistoryNodeResultEnum.OK;
                        break;
                    }
                    else
                    {
                        dirItemHistory.Result = PickingHistoryNode.HistoryNodeResultEnum.NotFound;
                    }
                }

                // if not found in any section
                if (!dirItemIsDone)
                    directionIsDone = false;
            }

            if (directionIsDone)
            {
                history.Result = PickingHistoryNode.HistoryNodeResultEnum.OK;
                return new PickingResult(history.RootNode) { Result = true, ResultItems = saveItems, UsedBookings = usedBookings };
            }
            else
            {
                history.Result = PickingHistoryNode.HistoryNodeResultEnum.NotFound;
                return new PickingResult(history.RootNode) { Result = false, NotFound = notFound };
            }
        }


        private PickingResult AllFromOneSection(ref PickingHistoryNode history, DirectionModel direction, List<string> sectionsInOrder, DateTime date)
        {
            var saveItems = new List<ResultItem>();
            var notFound = new List<NotFoundItem>();
            var usedBookings = new List<UsedBookingItem>();
            var onStoreCache = new OnStoreCache(_sectionModelCache, _onStore);

            // preload on store cache
            //var articleIDs = direction.Items.Select(di => _articlePackingCache.GetItem(di.ArtPackID).ArticleID).ToList();
            //onStoreCache.Preload(articleIDs);

            // try every section - in order
            foreach (var secID in sectionsInOrder)
            {
                var sectionHistory = history.AddMessage($"Section '{secID}'");

                // try to find all items
                var isDone = TryPickItems(ref sectionHistory, ref onStoreCache, direction.Items, secID, ref saveItems, ref notFound, ref usedBookings, date, direction.PartnerID, direction.PartAddrID);
                if (isDone)
                {
                    history.Result = PickingHistoryNode.HistoryNodeResultEnum.OK;
                    return new PickingResult(history.RootNode)
                    {
                        Result = true,
                        ResultItems = saveItems,
                        UsedBookings = usedBookings
                    };
                }
            }

            history.Result = PickingHistoryNode.HistoryNodeResultEnum.NotFound;
            return new PickingResult(history.RootNode) { Result = false, NotFound = notFound };
        }


        private bool TryPickItems(ref PickingHistoryNode history, ref OnStoreCache onStoreCache, List<DirectionItem> directionItems, string secID, ref List<ResultItem> saveItems, ref List<NotFoundItem> notFound, ref List<UsedBookingItem> usedBookings, DateTime date, int partnerID, int partAddrID)
        {
            bool directionIsDone = true;
            foreach (var dirItem in directionItems)
            {
                var remainder = dirItem.Quantity;

                var dirItemHistory = history.AddMessage($"Item '{dirItem.ArticleCode}' ({dirItem.ArtPackID}); quantity: {dirItem.Quantity}");

                // get article
                var articlePacking = _articlePackingCache.GetItem(dirItem.ArtPackID);
                var article = _articleModelCache.GetItem(articlePacking.ArticleID);
                // get manufact
                var manufact = _manufactCache.GetItem(article.ManufID);

                // get on store
                var onStore = onStoreCache.GetOnStore(article.ArticleID, secID);
                if (SaveOnStoreToHistory)
                    dirItemHistory.SaveOnStore(onStore);

                // bookings processing
                var validBookings = _bookings.ListValidBookings(dirItem.ArtPackID);
                if (validBookings.Any())
                    dirItemHistory.SaveBookings(validBookings);

                // try to find booking for this item
                var booking = FindBooking(partnerID, partAddrID, validBookings, onStore);
                while (booking != null && remainder > 0)
                {
                    remainder = TryToPickFromBookings(booking, ref dirItemHistory, ref onStore, remainder, dirItem.DocPosition, ref saveItems, ref usedBookings);

                    if (remainder == 0)
                        break;

                    // try other bookings
                    var restOfBookings = validBookings.RemoveUsedBookings(usedBookings);
                    booking = FindBooking(partnerID, partAddrID, restOfBookings, onStore);
                }

                // continue without bookings
                if (remainder > 0)
                {
                    // remove valid bookings from onStore - they are not available, except for bookings
                    onStore = WithoutBookedLots(onStore, validBookings);

                    var sumQuantity = onStore.Sum(o => o.Quantity);
                    if (sumQuantity < remainder)
                    {
                        directionIsDone = false;
                        dirItemHistory.Result = PickingHistoryNode.HistoryNodeResultEnum.NotFound;
                        notFound.Add(new NotFoundItem() { ArtPackID = dirItem.ArtPackID, SecID = secID, ArticleInfo = article.ArticleInfo, Quantity = dirItem.Quantity });
                        // skip this item
                        continue;
                    }

                    // select strategy
                    var strategyInstance = (IPicking)Activator.CreateInstance(Type.GetType($"S4.Picking.Strategy.Strategy_{manufact.ManufactSettings.DispatchAlgoritm}"));
                    var dirItemStrategyHistory = dirItemHistory.AddMessage($"Strategy {manufact.ManufactSettings.DispatchAlgoritm}");

                    // try to pick
                    remainder = strategyInstance.TryToPick(ref dirItemStrategyHistory, article, ref onStore, remainder, dirItem.DocPosition, ref saveItems, date);
                    if (remainder > 0)
                    {
                        directionIsDone = false;
                    }
                }

                // update history node
                if (remainder > 0)
                {
                    dirItemHistory.Result = PickingHistoryNode.HistoryNodeResultEnum.NotFound;
                    notFound.Add(new NotFoundItem() { ArtPackID = dirItem.ArtPackID, SecID = secID, ArticleInfo = article.ArticleInfo, Quantity = dirItem.Quantity });
                }
                else
                {
                    dirItemHistory.Result = PickingHistoryNode.HistoryNodeResultEnum.OK;
                }

            }

            return directionIsDone;
        }


        private BookingInfo FindBooking(int partnerID, int partAddrID, List<BookingInfo> validBookings, List<OnStoreItemFull> onStore)
        {
            var booking = validBookings.FirstOrDefault(_ => _.PartnerID == partnerID && _.PartAddrID == partAddrID);
            if (booking == null)
                return null;
            var lotSum = onStore.Where(_ => _.StoMoveLotID == booking.StoMoveLotID).Sum(_ => _.Quantity);
            return (lotSum > 0) ? booking : null;
        }


        private decimal TryToPickFromBookings(BookingInfo booking, ref PickingHistoryNode history, ref List<OnStoreItemFull> onStore, decimal destQuantity, int docPosition, ref List<ResultItem> saveItems, ref List<UsedBookingItem> usedBookings)
        {
            history.AddMessage($"Booking exists; bookingID: {booking.BookingID}; requiredQuantity: {booking.RequiredQuantity}");

            decimal usedQuantity = 0;
            while (destQuantity > 0)
            {
                var foundItem = onStore.FirstOrDefault(_ => _.StoMoveLotID == booking.StoMoveLotID && _.Quantity > 0);
                if (foundItem == null)
                    break;

                // add result line
                var moveQuantity = Math.Min(foundItem.Quantity, destQuantity);
                history.AddMessage($"Found item from booking; moveQuantity: {moveQuantity}; position: {foundItem.Position.PosCode}; carrierNum: {foundItem.CarrierNum}; batchNum: {foundItem.BatchNum}; expirationDate: {foundItem.ExpirationDate:yyyy-MM-dd};");

                saveItems.Add(new ResultItem()
                {
                    Quantity = moveQuantity,
                    PositionID = foundItem.PositionID,
                    ArtPackID = foundItem.ArtPackID,
                    CarrierNum = foundItem.CarrierNum,
                    BatchNum = foundItem.BatchNum,
                    ExpirationDate = foundItem.ExpirationDate,
                    DocPosition = docPosition
                });

                foundItem.Quantity -= moveQuantity;

                // calc remainder
                destQuantity -= moveQuantity;

                usedQuantity += moveQuantity;
            }

            usedBookings.Add(new UsedBookingItem() { BookingID = booking.BookingID, UsedQuantity = usedQuantity });
            return destQuantity;
        }


        public static List<OnStoreItemFull> WithoutBookedLots(List<OnStoreItemFull> onStore, List<BookingInfo> validBookings)
        {
            // remove booked items from onStore

            foreach (var booking in validBookings)
            {
                var requiredQuantity = booking.RequiredQuantity;
                while (requiredQuantity > 0)
                {
                    // find lot
                    var lotItem = onStore.FirstOrDefault(_ => _.StoMoveLotID == booking.StoMoveLotID && _.Quantity > 0);
                    if (lotItem != null)
                    {
                        // remove quantity from lot
                        var toRemove = Math.Min(requiredQuantity, lotItem.Quantity);
                        lotItem.Quantity -= toRemove;
                        requiredQuantity -= toRemove;
                    }
                    else
                    {
                        // lot item not found - end process
                        requiredQuantity = 0;
                    }
                }
            }

            // return only usefull onstore items
            return onStore.Where(_ => _.Quantity > 0).ToList();
        }
    }
}

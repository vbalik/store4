﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Picking
{
    public class NotFoundItem
    {
        public int ArtPackID { get; set; }
        public string SecID { get; set; }
        public string ArticleInfo { get; set; }
        public decimal Quantity { get; set; }
    }
}

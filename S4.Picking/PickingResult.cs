﻿using S4.StoreCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Picking
{
    public class PickingResult
    {
        public PickingResult(PickingHistoryNode history)
        {
            History = history;
        }


        public bool Result { get; set; }
        public List<ResultItem> ResultItems { get; set; }
        public PickingHistoryNode History { get; set; }
        public List<NotFoundItem> NotFound { get; set; }
        public List<UsedBookingItem> UsedBookings { get; set; }
    }
}

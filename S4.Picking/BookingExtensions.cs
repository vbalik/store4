﻿using S4.DAL.Interfaces;
using S4.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace S4.Picking
{
    public static class BookingExtensions
    {
        public static List<BookingInfo> RemoveUsedBookings(this List<BookingInfo> bookings, List<UsedBookingItem> usedBookings)
        {
            return bookings
                .Select(_ => new BookingInfo()
                {
                    BookingID = _.BookingID,
                    StoMoveLotID = _.StoMoveLotID,
                    PartnerID = _.PartnerID,
                    PartAddrID = _.PartAddrID,
                    RequiredQuantity = _.RequiredQuantity - usedBookings.Where(u => u.BookingID == _.BookingID).Sum(u => u.UsedQuantity)
                })
                .Where(_ => _.RequiredQuantity > 0)
                .ToList();
        }
    }
}

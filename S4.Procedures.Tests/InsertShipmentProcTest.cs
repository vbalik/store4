﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Delivery;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class InsertShipmentProcTest : ProcTestBase
    {
        private const int _DIRECTION_ID_WRONG = 41;
        private const int _DIRECTION_ID = 42;

        public InsertShipmentProcTest(DatabaseFixture fixture) : base(fixture)
        { }

        [Fact]
        public void InsertShipmentProc_MissingParams()
        {
            var insertShipment = new InsertShipment();
            insertShipment = new Procedures.Delivery.InsertShipmentProc(USER_ID).DoProcedure(insertShipment);
            Assert.False(insertShipment.Success);
            Assert.Equal("Parametry procedury nejsou platné DirectionID IsNull , ShipmentReference IsNull , DeliveryProvider IsNull , Parcels IsNull ", insertShipment.ErrorText);
        }

        [Fact]
        public void InsertShipmentProc_BadStatus()
        {
            var insertShipment = new InsertShipment()
            {
                DeliveryProvider = DeliveryDirection.DeliveryProviderEnum.DPD,
                DirectionID = _DIRECTION_ID_WRONG,
                ShipmentReference = _DIRECTION_ID_WRONG.ToString(),
                Parcels = new Tuple<int, string>[] { new Tuple<int, string>(1, "BAL1"), new Tuple<int, string>(2, "BAL2") }
            };
            insertShipment = new Procedures.Delivery.InsertShipmentProc(USER_ID).DoProcedure(insertShipment);
            Assert.False(insertShipment.Success);
            Assert.Equal("Direction nemá DocStatus DR_STATUS_DISP_CHECK_DONE; DirectionID: 41", insertShipment.ErrorText);
        }

        [Fact]
        public void InsertShipmentProc_Success()
        {
            var insertShipment = new InsertShipment()
            {
                DeliveryProvider = DeliveryDirection.DeliveryProviderEnum.DPD,
                DirectionID = _DIRECTION_ID,
                ShipmentReference = _DIRECTION_ID.ToString(),
                Parcels = new Tuple<int, string>[] { new Tuple<int, string>(1, "BAL1"), new Tuple<int, string>(2, "BAL2") }
            };
            insertShipment = new Procedures.Delivery.InsertShipmentProc(USER_ID).DoProcedure(insertShipment);

            //check success
            Assert.True(insertShipment.Success);
            //check status
            var status = new DAL.DirectionDAL().GetStatus(_DIRECTION_ID);
            Assert.Equal(Direction.DR_STATUS_DELIVERY_SHIPMENT, status);
            //check history
            var history = new DAL.DirectionDAL().GetDirectionHistory(_DIRECTION_ID).Where(i => i.EventCode == Direction.DR_STATUS_DELIVERY_SHIPMENT).FirstOrDefault();
            Assert.NotNull(history);
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //check DeliveryDirection
                var deliveryDirection = new DAL.DeliveryDirectionDAL().GetByDirectionID(_DIRECTION_ID, db);
                Assert.NotNull(deliveryDirection);
                Assert.Equal(_DIRECTION_ID.ToString(), deliveryDirection.ShipmentRefNumber);
                Assert.Equal(1, deliveryDirection.CreateShipmentAttempt ?? 0);
                //check deliveryDirectionItems
                var deliveryDirectionItems = db.Fetch<DeliveryDirectionItem>("select * from s4_deliveryDirection_items where deliveryDirectionID = @0", deliveryDirection.DeliveryDirectionID);
                Assert.Collection(deliveryDirectionItems,
                    item => Assert.Equal(new Tuple<int, string>(1, "BAL1"), new Tuple<int, string>(item.ParcelPosition, item.ParcelRefNumber)),
                    item => Assert.Equal(new Tuple<int, string>(2, "BAL2"), new Tuple<int, string>(item.ParcelPosition, item.ParcelRefNumber)));
            }
        }
    }
}

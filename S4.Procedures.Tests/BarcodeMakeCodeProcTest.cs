﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.BarCode;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class BarcodeMakeCodeProcTest : ProcTestBase
    {
        const int ARTICLE_ID = 103;
        const int ART_PACK_ID = 1004;
        

        public BarcodeMakeCodeProcTest(DatabaseFixture fixture) : base(fixture) 
        { }


        [Fact]
        public void BarcodeMakeCodeProcTest_Success()
        {
            var model = new BarcodeMakeCode()
            {
                ArtPackID = ART_PACK_ID
            };

            ArticlePacking oldPack = null;
            using (var db = GetDB())
            {
                oldPack = db.SingleById<ArticlePacking>(model.ArtPackID);
            }

                var proc = new Barcode.BarcodeMakeCodeProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            using (var db = GetDB())
            {
                var pack = db.SingleById<ArticlePacking>(model.ArtPackID);
                Assert.True(pack.PackStatus == ArticlePacking.AP_STATUS_OK);

                var hist = db.Fetch<ArticleHistory>(new NPoco.Sql().Where("artpackid = @0", model.ArtPackID));
                Assert.True(hist.Count > 0);
                var hist2 = hist.OrderByDescending(h => h.ArticleHistID).First();
                Assert.Equal(ARTICLE_ID, hist2.ArticleID);
                Assert.Equal(Article.AR_EVENT_BARCODE_CREATED, hist2.EventCode.Trim());
                Assert.Equal(res.BarCode, hist2.EventData);
                Assert.Equal(USER_ID, hist2.EntryUserID.Trim());
                Assert.True(hist2.EntryDateTime <= DateTime.Now);

                var msg = db.Fetch<InternMessage>(new NPoco.Sql().Where("msgclass = @0", Article.AR_EVENT_BARCODE_SET));
                Assert.True(msg.Count > 0);
                var msg2 = msg.OrderByDescending(h => h.IntMessID).First();
                Assert.Equal(InternMessage.MessageSeverityEnum.Low, msg2.MsgSeverity);
                Assert.Equal("Vygenerován čárový kód k položce 'X123 - Zboží 123'", msg2.MsgHeader);
                Assert.True(msg2.MsgDateTime < DateTime.Now);

                var msgTextDictionary = Newtonsoft.Json.JsonConvert.DeserializeObject<System.Collections.Generic.Dictionary<string, dynamic>>(msg2.MsgText);
                Assert.Equal(msgTextDictionary["articleID"], 103);
                Assert.Equal(msgTextDictionary["oldBarCode"], oldPack.BarCode);
                Assert.Equal(msgTextDictionary["newBarCode"], pack.BarCode);


            }
        }


        [Fact]
        public void BarcodeMakeCodeProcTest_Error()
        {
            var model = new BarcodeMakeCode()
            {
                ArtPackID = -1
            };

            var proc = new Barcode.BarcodeMakeCodeProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal("Balení nebylo nalezeno; ArtPackID: -1", res.ErrorText);
        }
    }
}

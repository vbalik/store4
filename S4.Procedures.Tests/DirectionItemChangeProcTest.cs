﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Admin;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class DirectionItemChangeProcTest : ProcTestBase
    {
       

        public DirectionItemChangeProcTest(DatabaseFixture fixture) : base(fixture) 
        { }


        [Fact]
        public void DirectionItemChangeProcTest_Dispatch_Success()
        {
            const int DIRECTION_ITEM_ID = 7;
            const int DIRECTION_ID = 18;
            
            var model = new DirectionItemChange()
            {
                DirectionItemID = DIRECTION_ITEM_ID,
                ArticleCode = "X123x",
                ArtPackID = 1002,
                PartnerDepart = "Test",
                Quantity = 10,
                UnitDesc = "ud2"
            };

            DirectionItem storeMoveItemOld = null;
            using (var db = GetDB())
            {
                storeMoveItemOld = db.SingleOrDefault<DirectionItem>("where directionItemID = @0", model.DirectionItemID);
            }

            var proc = new Admin.DirectionItemChangeProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);

            using (var db = GetDB())
            {
                var storeMoveItem = db.SingleOrDefault<DirectionItem>("where directionItemID = @0", model.DirectionItemID);
                Assert.Equal(model.ArticleCode, storeMoveItem.ArticleCode);
                Assert.Equal(model.ArtPackID, storeMoveItem.ArtPackID);
                Assert.Equal(model.PartnerDepart, storeMoveItem.PartnerDepart);
                Assert.Equal(model.Quantity, storeMoveItem.Quantity);
                Assert.Equal(model.UnitDesc, storeMoveItem.UnitDesc);

                var hist = db.Fetch<DirectionHistory>(new NPoco.Sql().Where("directionID = @0", DIRECTION_ID));
                Assert.True(hist.Count > 0);
                var hist2 = hist.OrderByDescending(h => h.DirectionHistID).First();
                Assert.Equal(Direction.DR_EVENT_ITEM_CHANGED, hist2.EventCode.Trim());
                Assert.Equal("{\"changes\":[{\"property\":\"ArtPackID\",\"originValue\":\"1001\",\"currentValue\":\"1002\"},{\"property\":\"ArticleCode\",\"originValue\":\"X123\",\"currentValue\":\"X123x\"},{\"property\":\"Quantity\",\"originValue\":\"1.0000\",\"currentValue\":\"10\"},{\"property\":\"UnitDesc\",\"originValue\":\"\",\"currentValue\":\"ud2\"},{\"property\":\"PartnerDepart\",\"originValue\":\"\",\"currentValue\":\"Test\"}]}", hist2.EventData.Trim());
                Assert.Equal(USER_ID, hist2.EntryUserID.Trim());
                Assert.True(hist2.EntryDateTime <= DateTime.Now);

                var msg = db.Fetch<InternMessage>(new NPoco.Sql().Where("msgclass = @0", InternMessage.DR_ITEM_CHANGED));
                Assert.True(msg.Count > 0);
                var msg2 = msg.OrderByDescending(h => h.IntMessID).First();
                Assert.Equal(InternMessage.MessageSeverityEnum.High, msg2.MsgSeverity);
                Assert.Equal("Změna položky dokladu DL/14/9 (DirectionID: 18)", msg2.MsgHeader);
                Assert.True(msg2.MsgDateTime <= DateTime.Now);

                var propertiesComparer = new Core.PropertiesComparer.PropertiesComparer<DirectionItem>();
                propertiesComparer.DoCompare(storeMoveItemOld, model, nameof(model.ArtPackID), nameof(model.ArticleCode), nameof(model.Quantity), nameof(model.UnitDesc), nameof(model.PartnerDepart));
                                
                Assert.Equal(propertiesComparer.JsonText, msg2.MsgText);
            }
        }

        [Fact]
        public void DirectionItemChangeProcTest_Receive_Success()
        {
            const int DIRECTION_ITEM_ID = 30;
            const int DIRECTION_ID = 31;
            const int STOMOVE_ID = 516;

            var model = new DirectionItemChange()
            {
                DirectionItemID = DIRECTION_ITEM_ID,
                ArticleCode = "X123x",
                ArtPackID = 1002,
                PartnerDepart = "Test",
                Quantity = 10,
                UnitDesc = "ud2"
            };

            DirectionItem storeMoveItemOld = null;
            using (var db = GetDB())
            {
                storeMoveItemOld = db.SingleOrDefault<DirectionItem>("where directionItemID = @0", model.DirectionItemID);
            }

            var proc = new Admin.DirectionItemChangeProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);

            using (var db = GetDB())
            {
                var storeMoveItem = db.SingleOrDefault<DirectionItem>("where directionItemID = @0", model.DirectionItemID);
                Assert.Equal(model.ArticleCode, storeMoveItem.ArticleCode);
                Assert.Equal(model.ArtPackID, storeMoveItem.ArtPackID);
                Assert.Equal(model.PartnerDepart, storeMoveItem.PartnerDepart);
                Assert.Equal(model.Quantity, storeMoveItem.Quantity);
                Assert.Equal(model.UnitDesc, storeMoveItem.UnitDesc);

                var hist = db.Fetch<DirectionHistory>(new NPoco.Sql().Where("directionID = @0", DIRECTION_ID));
                Assert.True(hist.Count > 0);
                var hist2 = hist.OrderByDescending(h => h.DirectionHistID).First();
                Assert.Equal(Direction.DR_EVENT_ITEM_CHANGED, hist2.EventCode.Trim());
                Assert.Equal("{\"changes\":[{\"property\":\"ArtPackID\",\"originValue\":\"1006\",\"currentValue\":\"1002\"},{\"property\":\"ArticleCode\",\"originValue\":\"X123\",\"currentValue\":\"X123x\"},{\"property\":\"Quantity\",\"originValue\":\"1.0000\",\"currentValue\":\"10\"},{\"property\":\"UnitDesc\",\"originValue\":\"\",\"currentValue\":\"ud2\"},{\"property\":\"PartnerDepart\",\"originValue\":\"\",\"currentValue\":\"Test\"}]}", hist2.EventData.Trim());
                Assert.Equal(USER_ID, hist2.EntryUserID.Trim());
                Assert.True(hist2.EntryDateTime <= DateTime.Now);

                var msg = db.Fetch<InternMessage>(new NPoco.Sql().Where("msgclass = @0", InternMessage.DR_ITEM_CHANGED));
                Assert.True(msg.Count > 0);
                var msg2 = msg.OrderByDescending(h => h.IntMessID).First();
                Assert.Equal(InternMessage.MessageSeverityEnum.High, msg2.MsgSeverity);
                Assert.Equal("Změna položky dokladu PRIJ/18/3 (DirectionID: 31)", msg2.MsgHeader);
                Assert.True(msg2.MsgDateTime <= DateTime.Now);

                var moves = db.Fetch<StoreMoveItem>(new NPoco.Sql().Where("[stoMoveID] = @0", STOMOVE_ID));
                Assert.NotEmpty(moves);
                Assert.Equal(model.Quantity, moves[0].Quantity);

                var lot = db.SingleById<StoreMoveLot>(moves[0].StoMoveLotID);
                Assert.Equal(model.ArtPackID, lot.ArtPackID);

                var moveHist = db.Fetch<StoreMoveHistory>(new NPoco.Sql().Where("[stoMoveID] = @0", STOMOVE_ID));
                Assert.NotEmpty(moveHist);
                var moveHist2 = moveHist.OrderByDescending(h => h.StoMoveHistID).First();
                Assert.Equal(StoreMove.MO_EVENT_ITEM_CHANGED, moveHist2.EventCode.Trim());
                Assert.Equal("{\"changes\":[{\"property\":\"StoMoveLotID\",\"originValue\":\"44\",\"currentValue\":\"65\"},{\"property\":\"Quantity\",\"originValue\":\"1.0000\",\"currentValue\":\"10\"}]}", moveHist2.EventData.Trim());
                Assert.Equal(USER_ID, moveHist2.EntryUserID.Trim());
                Assert.True(moveHist2.EntryDateTime <= DateTime.Now);

                var propertiesComparer = new Core.PropertiesComparer.PropertiesComparer<DirectionItem>();
                propertiesComparer.DoCompare(storeMoveItemOld, model, nameof(model.ArtPackID), nameof(model.ArticleCode), nameof(model.Quantity), nameof(model.UnitDesc), nameof(model.PartnerDepart));

                Assert.Equal(propertiesComparer.JsonText, msg2.MsgText);
            }
        }


        [Fact]
        public void DirectionItemChangeProcTest_Error()
        {
            var model = new DirectionItemChange()
            {
                DirectionItemID = -1,
                ArticleCode = "x",
                ArtPackID = -1,
                PartnerDepart = "x",
                Quantity = -1,
                UnitDesc = "x" 
            };

            var proc = new Admin.DirectionItemChangeProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.False(res.Success);
            Assert.Equal("DirectionItem nebylo nalezeno; DirectionItemID: -1", res.ErrorText);
        }
    }
}

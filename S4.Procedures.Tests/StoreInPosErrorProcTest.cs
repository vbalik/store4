﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.StoreIn;

namespace S4.Procedures.Tests
{
    [Collection("StoreInTests2")]
    [Trait("Category", "StoreInTests2")]
    public class StoreInPosErrorProcTest : ProcTestBase
    {
        public StoreInPosErrorProcTest(DatabaseFixture fixture) : base(fixture)
        { }

        [Fact]
        public void StoreInPosErrorProcTest_Success()
        {
            var model = new StoreInPosError()
            {
                PositionID = 17,
                StoMoveID = 506,
                Reason = ProcedureModels.PositionErrorReason.NotUsable
            };

            var proc = new StoreIn.StoreInPosErrorProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var position = db.Fetch<Position>(new NPoco.Sql().Where("positionID = @0", model.PositionID)).FirstOrDefault();
                Assert.Equal(Position.POS_STATUS_BLOCKED, position.PosStatus);
                var moveHist = db.Fetch<StoreMoveHistory>(new NPoco.Sql().Where("stoMoveID = @0", model.StoMoveID));
                Assert.True(moveHist.Count > 0);
                var moveHist2 = moveHist.OrderByDescending(h => h.StoMoveID).First();
                Assert.Equal(StoreMove.MO_EVENT_DISP_POS_ERROR, moveHist2.EventCode);
                Assert.Equal("17|SIPEP|NotUsable", moveHist2.EventData);                
                Assert.Equal(USER_ID, moveHist2.EntryUserID.Trim());
                Assert.True(moveHist2.EntryDateTime < DateTime.Now);
            }
        }

        [Fact]
        public void StoreInPosErrorProcTest_Error()
        {
            var model = new StoreInPosError();

            var proc = new StoreIn.StoreInPosErrorProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal("Parametry procedury nejsou platné StoMoveID IsNull , PositionID IsNull", res.ErrorText.Trim());
        }
    }
}

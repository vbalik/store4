﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Move;
using System.Collections.Generic;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class MoveCheckItemsTest : ProcTestBase
    {
        private const int POSITIONID = 8;

        public MoveCheckItemsTest(DatabaseFixture fixture) : base(fixture)
        { }

        [Fact]
        public void MoveCheckItemsTest_Success_Exists()
        {

            var articlesList = new List<Entities.Helpers.ArticleInfo>();
            var article = new Entities.Helpers.ArticleInfo()
            {
                ArticleCode = "X123",
                ArticleDesc = "Zboží 123",
                ArticleID = 100,
                ArtPackID = 1001,
                BatchNum = "000X",
                CarrierNum = "0",
                ExpirationDate = new DateTime(2018, 1, 1),
                PackDesc = "ks",
                Quantity = 1
            };

            articlesList.Add(article);

            var model = new MoveCheckItems()
            {
                PositionID = POSITIONID,
                Articles = articlesList
            };

            var proc = new Move.MoveCheckItemsProc(USER_ID);
            var res = proc.DoProcedure(model);
            Assert.True(res.Success, res.ErrorText);
            Assert.True(res.ItemExists);
        }


        [Fact]
        public void MoveCheckItemsTest_Success_NotExists()
        {

            var articlesList = new List<Entities.Helpers.ArticleInfo>();
            var article = new Entities.Helpers.ArticleInfo()
            {
                ArticleCode = "X123",
                ArticleDesc = "Zboží 123",
                ArticleID = 100,
                ArtPackID = 1001,
                BatchNum = "XX",
                CarrierNum = "0",
                ExpirationDate = new DateTime(2018, 1, 1),
                PackDesc = "ks",
                Quantity = 1
            };

            articlesList.Add(article);

            var model = new MoveCheckItems()
            {
                PositionID = POSITIONID,
                Articles = articlesList
            };

            var proc = new Move.MoveCheckItemsProc(USER_ID);
            var res = proc.DoProcedure(model);
            Assert.True(res.Success, res.ErrorText);
            Assert.False(res.ItemExists);
        }


        [Fact]
        public void MoveCheckItemsTest_Success_NotExists2()
        {

            var articlesList = new List<Entities.Helpers.ArticleInfo>();
            var article = new Entities.Helpers.ArticleInfo()
            {
                ArticleCode = "X123",
                ArticleDesc = "Zboží 123",
                ArticleID = 100,
                ArtPackID = 1001,
                BatchNum = "000X",
                CarrierNum = "0",
                ExpirationDate = new DateTime(2018, 1, 1),
                PackDesc = "ks",
                Quantity = 1000
            };

            articlesList.Add(article);

            var model = new MoveCheckItems()
            {
                PositionID = POSITIONID,
                Articles = articlesList
            };

            var proc = new Move.MoveCheckItemsProc(USER_ID);
            var res = proc.DoProcedure(model);
            Assert.True(res.Success, res.ErrorText);
            Assert.False(res.ItemExists);
        }


        [Fact]
        public void MoveGetPositionTest_Error()
        {
            var model = new MoveCheckItems()
            {
                PositionID = -1,
                Articles = new List<Entities.Helpers.ArticleInfo>()
            };

            var proc = new Move.MoveCheckItemsProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            Assert.True(res.PositionResult == MoveStatusEnum.NotFound);
        }

    }
}

﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Admin;
using S4.Procedures.Admin;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class RemoveExpBatchProcTest : ProcTestBase
    {
        const int ARTICLE_ID = 115;
        const int DONT_TOUCH_ARTICLE_ID = 118;

        public RemoveExpBatchProcTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void RemoveExpBatchProcTest_Success()
        {
            var model = new RemoveExpBatch()
            {
                ArticleID = ARTICLE_ID
            };

            var proc = new RemoveExpBatchProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success, res.ErrorText);

            var receiveDoneModel = new ProcedureModels.Receive.ReceiveDone()
            {
                StoMoveID = 521
            };
            new Procedures.Receive.ReceiveDoneProc(USER_ID).DoProcedure(receiveDoneModel);

            var dontTouchOnStore = new StoreCore.OnStore_Basic.OnStore().ByArticle(DONT_TOUCH_ARTICLE_ID, StoreCore.Interfaces.ResultValidityEnum.Valid);
            Assert.True(dontTouchOnStore.Count == 1);
            var onStore = new StoreCore.OnStore_Basic.OnStore().ByArticle(ARTICLE_ID, StoreCore.Interfaces.ResultValidityEnum.Valid);
            Assert.True(onStore.Count() == 6);
            foreach (var item in onStore)
            {
                Assert.NotNull(item.BatchNum);
                Assert.NotNull(item.ExpirationDate);
                Assert.Equal(1, item.Quantity);
            }

            res = proc.DoProcedure(model);
            Assert.True(res.Success, res.ErrorText);
            onStore = new StoreCore.OnStore_Basic.OnStore().ByArticle(ARTICLE_ID, StoreCore.Interfaces.ResultValidityEnum.Valid);
            Assert.True(onStore.Count == 2);
            Assert.Null(onStore[0].BatchNum);
            Assert.Null(onStore[0].ExpirationDate);
            Assert.Null(onStore[0].CarrierNum);
            Assert.Equal(3, onStore[0].Quantity);
            Assert.Null(onStore[1].BatchNum);
            Assert.Null(onStore[1].ExpirationDate);
            Assert.NotNull(onStore[1].CarrierNum);
            Assert.Equal(3, onStore[1].Quantity);

            dontTouchOnStore = new StoreCore.OnStore_Basic.OnStore().ByArticle(DONT_TOUCH_ARTICLE_ID, StoreCore.Interfaces.ResultValidityEnum.Valid);
            Assert.True(dontTouchOnStore.Count == 1);
        }
    }
}

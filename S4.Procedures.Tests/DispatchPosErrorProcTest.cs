﻿using S4.Entities;
using S4.ProcedureModels.Dispatch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestHelpers;
using Xunit;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class DispatchPosErrorProcTest : ProcTestBase
    {
        public DispatchPosErrorProcTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void DispatchPosErrorProcTest_Success()
        {
            var model = new DispatchPosError()
            {
                StoMoveID = 512,
                PositionID = 116
            };


            var proc = new Dispatch.DispatchPosErrorProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var position = db.FirstOrDefault<Position>("where positionID = @0", model.PositionID);
                Assert.Equal(Position.POS_STATUS_BLOCKED, position.PosStatus);

                var hist = db.Fetch<StoreMoveHistory>(new NPoco.Sql().Where("stoMoveID = @0", model.StoMoveID));
                Assert.True(hist.Count > 0);
                var hist2 = hist.OrderByDescending(h => h.StoMoveID).First();
                Assert.Equal(StoreMove.MO_EVENT_DISP_POS_ERROR, hist2.EventCode.Trim());
                Assert.Equal(USER_ID, hist2.EntryUserID.Trim());
                Assert.True(hist2.EntryDateTime <= DateTime.Now);
            }
        }


        [Fact]
        public void DispatchPosErrorProcTest_Error()
        {
            var model = new DispatchPosError()
            {
                StoMoveID = 510,
                PositionID = 116
            };

            var proc = new Dispatch.DispatchPosErrorProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal($"Doklad nemá očekávaný status MO_STATUS_NEW_DISPATCH; StoMoveID: {model.StoMoveID}", res.ErrorText);
        }
    }
}

﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using S4.ProcedureModels.Dispatch;
using Xunit;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class DispatchWaitProcTest : ProcTestBase
    {
        const int DIRECTION_ID = 7;
       

        public DispatchWaitProcTest(DatabaseFixture fixture) : base(fixture) 
        { }


        [Fact]
        public void DispatchWaitProcTest_Success()
        {
            var model = new DispatchWait()
            {
                DirectionID = DIRECTION_ID,
                DestPositionID = 123,
                WorkerID = "Test1"
            };

            int directionAssignID = 0;
            using (var db = GetDB())
            {
                var directionAssigns1 = db.Fetch<DirectionAssign>("where directionid = @0", DIRECTION_ID);
                Assert.True(directionAssigns1.Count == 1);
                directionAssignID = directionAssigns1[0].DirectAssignID;
            }

            var proc = new Dispatch.DispatchWaitProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success);
            
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var direction = db.FirstOrDefault<Direction>("where directionid = @0", DIRECTION_ID);
                Assert.True(direction.DocStatus == Direction.DR_STATUS_RES_IN_PROC);

                var directionAssigns2 = db.Fetch<DirectionAssign>("where directionid = @0", DIRECTION_ID);
                Assert.True(directionAssigns2.Count == 1);
                Assert.True(directionAssigns2[0].DirectAssignID != directionAssignID);

                //test xtraData
                var xtraData = new Entities.XtraData.XtraDataProxy<Entities.DirectionXtra>(Direction.XtraDataDict);
                xtraData.Load(db, new NPoco.Sql().Where("DirectionID = @0", DIRECTION_ID));
                Assert.NotEmpty(xtraData.Data);
                Assert.Equal(model.DestPositionID.ToString(), xtraData[Direction.XTRA_DISPATCH_DIRECT_SECTION]);

                var hist = db.Fetch<DirectionHistory>(new NPoco.Sql().Where("directionid = @0", DIRECTION_ID));
                Assert.True(hist.Count > 0);
                var hist2 = hist.OrderByDescending(h => h.DirectionID).First();
                Assert.Equal(DIRECTION_ID, hist2.DirectionID);
                Assert.Equal(Direction.DR_STATUS_DISPATCH_WAIT, hist2.EventCode.Trim());
                Assert.Equal("Test1|123", hist2.EventData);
                Assert.Equal(USER_ID, hist2.EntryUserID.Trim());
                Assert.True(hist2.EntryDateTime < DateTime.Now);
            }
            
        }


        [Fact]
        public void DispatchWaitProcTest_Error()
        {
            var model = new DispatchWait()
            {
                DirectionID = -1,
                DestPositionID = -1,
                WorkerID = "-1"
            };

            var proc = new Dispatch.DispatchWaitProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal("Doklad nebyl nalezen; DirectionID: -1", res.ErrorText);
        }
              
    }
}

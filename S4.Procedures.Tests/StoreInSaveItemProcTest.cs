﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.StoreIn;

namespace S4.Procedures.Tests
{
    [Collection("StoreInTests")]
    [Trait("Category", "StoreInTests")]
    public class StoreInSaveItemProcTest : ProcTestBase
    {
        const int STOMOVE_ID = 507;
        const int FINAL_POSITION_ID = 34;
        const int STOREMOVEITEM_ID = 13;

        public StoreInSaveItemProcTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void StoreInSaveItemProcTest_Success()
        {
            var model = new StoreInSaveItem()
            {
                StoMoveID = STOMOVE_ID,
                FinalCarrierNum = "1",
                FinalPositionID = FINAL_POSITION_ID
            };

            var proc = new StoreIn.StoreInSaveItemProc(USER_ID);
            var res = proc.DoProcedure(model);
            Assert.True(res.Success, res.ErrorText);
            
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var item = db.SingleById<StoreMoveItem>(STOREMOVEITEM_ID);
                Assert.True(item.PositionID == FINAL_POSITION_ID);
            }

        }
                
    }
}

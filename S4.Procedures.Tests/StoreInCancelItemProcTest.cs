﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.StoreIn;

namespace S4.Procedures.Tests
{
    [Collection("StoreInTests")]
    [Trait("Category", "StoreInTests")]
    public class StoreInCancelItemProcTest : ProcTestBase
    {
        const int STOMOVE_ID = 506;

        public StoreInCancelItemProcTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void StoreInCancelItemProcTest_Success()
        {
            var model = new StoreInCancelItem()
            {
                StoMoveID = STOMOVE_ID
            };

            var proc = new StoreIn.StoreInCancelItemProc(USER_ID);
            var res = proc.DoProcedure(model);
            Assert.True(res.Success, res.ErrorText);

            using (var db = GetDB())
            {
                //check status
                var stoMove = db.SingleById<StoreMove>(STOMOVE_ID);
                var stoMoveItem = db.Fetch<StoreMoveItem>("where stoMoveID = @0", STOMOVE_ID).First();
                Assert.True(stoMove.DocStatus == StoreMove.MO_STATUS_CANCEL, "stoMove.DocStatus není MO_STATUS_CANCEL");
                Assert.True(stoMoveItem.ItemValidity == StoreMoveItem.MI_VALIDITY_CANCELED, "StoreMoveItem.ItemValidity není MI_VALIDITY_CANCELED");
            }


        }

        [Fact]
        public void StoreInCancelItemProcTest_Error()
        {
            var model = new StoreInCancelItem()
            {
                StoMoveID = -1
            };

            var proc = new StoreIn.StoreInCancelItemProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
        }

    }
}

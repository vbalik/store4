﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;

namespace S4.Procedures.Tests
{
    [Collection("StockTakingTests")]
    [Trait("Category", "StockTakingTests")]
    public class StockTakingAddPosItemTest : ProcTestBase
    {
        private int STOTAK_ID = 6;

        public StockTakingAddPosItemTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void StockTakingAddPosItemTest_Success()
        {
            var model = new ProcedureModels.StockTaking.StockTakingAddPosItem()
            {
                StotakID = STOTAK_ID,
                PositionID = 3,
                ArtPackID = 1001,
                CarrierNum = "123Y",
                Quantity = 1,
                BatchNum = "123X",
                ExpirationDate = new DateTime(2018, 1, 1)

            };


            var proc = new StockTaking.StockTakingAddPosItemProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            Assert.True(res.StotakItemID > 0);

            using (var db = GetDB())
            {
                var pos = db.First<StockTakingPosition>("where stotakID = @0 AND positionID = @1", model.StotakID, model.PositionID);
                Assert.True(pos != null);
                Assert.True(pos.CheckCounter == 1);

                // item
                var items = db.Fetch<StockTakingItem>("where stotakid = @0", STOTAK_ID);
                Assert.True(items != null);
                Assert.True(items.Count == 1);
                Assert.Equal(model.ArtPackID, items[0].ArtPackID);
                Assert.Equal(model.BatchNum, items[0].BatchNum);
                Assert.Equal(model.CarrierNum, items[0].CarrierNum);
                Assert.Equal(model.ExpirationDate, items[0].ExpirationDate);
                Assert.Equal(model.PositionID, items[0].PositionID);
                Assert.Equal(model.Quantity, items[0].Quantity);
                
                // history
                var hist = db.Fetch<StockTakingHistory>(new NPoco.Sql().Where("stotakID = @0", model.StotakID));
                Assert.True(hist.Count > 0);
                var hist2 = hist.OrderByDescending(h => h.StotakHistID).First();
                Assert.Equal(STOTAK_ID, hist2.StotakID);
                Assert.Equal(StockTakingHistory.STOCKTAKING_EVENT_SET, hist2.EventCode.Trim());
                Assert.Equal(USER_ID, hist2.EntryUserID.Trim());
                Assert.True(hist2.EntryDateTime <= DateTime.Now);
                Assert.Equal(model.ArtPackID, hist2.ArtPackID);
                Assert.Equal(model.BatchNum, hist2.BatchNum);
                Assert.Equal(model.CarrierNum, hist2.CarrierNum);
                Assert.Equal(model.ExpirationDate, hist2.ExpirationDate);
                Assert.Equal(model.PositionID, hist2.PositionID);
                Assert.Equal(model.Quantity, hist2.Quantity);
            }
        }



        [Fact]
        public void StockTakingAddItemTest_Error()
        {
            var model = new ProcedureModels.StockTaking.StockTakingAddPosItem()
            {
                StotakID = -1,
                ArtPackID = -1,
                BatchNum = "x",
                CarrierNum = "x",
                ExpirationDate = DateTime.Now,
                PositionID = -1,
                Quantity = 1
            };

            var proc = new StockTaking.StockTakingAddPosItemProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal("StockTaking nebylo nalezeno; StotakID: -1", res.ErrorText);
        }
    }
}

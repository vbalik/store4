﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Admin;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class StoreMoveCancelProcTest : ProcTestBase
    {
        const int ST_MOVE_ID = 1;


        public StoreMoveCancelProcTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void StoreMoveCancelProcTest_Success()
        {
            var model = new StoreMoveCancel()
            {
                StoMoveID = ST_MOVE_ID,
                CancelReason = "Test"
            };

            var proc = new Admin.StoreMoveCancelProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);

            using (var db = GetDB())
            {
                var storeMove = db.SingleOrDefault<StoreMove>("where stoMoveID = @0", model.StoMoveID);
                Assert.True(storeMove.DocStatus == StoreMove.MO_STATUS_CANCEL, storeMove.DocStatus);

                var hist = db.Fetch<StoreMoveHistory>(new NPoco.Sql().Where("stoMoveID = @0", model.StoMoveID));
                Assert.True(hist.Count > 0);
                var hist2 = hist.OrderByDescending(h => h.StoMoveID).First();
                Assert.Equal(ST_MOVE_ID, hist2.StoMoveID);
                Assert.Equal(StoreMove.MO_STATUS_CANCEL, hist2.EventCode.Trim());
                Assert.Equal(model.CancelReason, hist2.EventData.Trim());
                Assert.Equal(USER_ID, hist2.EntryUserID.Trim());
                Assert.True(hist2.EntryDateTime < DateTime.Now);

                var msg = db.Fetch<InternMessage>(new NPoco.Sql().Where("msgclass = @0", InternMessage.MO_STATUS_CANCEL));
                Assert.True(msg.Count > 0);
                var msg2 = msg.OrderByDescending(h => h.IntMessID).First();
                Assert.Equal(InternMessage.MessageSeverityEnum.Low, msg2.MsgSeverity);
                Assert.Equal("Doklad 'XXX/1' zrušen", msg2.MsgHeader);
                Assert.True(msg2.MsgDateTime < DateTime.Now);

                var msgTextDictionary = Newtonsoft.Json.JsonConvert.DeserializeObject<System.Collections.Generic.Dictionary<string, dynamic>>(msg2.MsgText);
                Assert.Equal(msgTextDictionary["stoMoveID"], ST_MOVE_ID);
                Assert.Equal(msgTextDictionary["cancelReason"], "Test");
            }

        }


        [Fact]
        public void StoreMoveCancelProcTest_Error()
        {
            var model = new StoreMoveCancel()
            {
                StoMoveID = -1,
                CancelReason = null
            };

            var proc = new Admin.StoreMoveCancelProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal("Parametry procedury nejsou platné CancelReason IsNull ", res.ErrorText);
        }


        [Fact]
        public void StoreMoveCancelProcTest_Error2()
        {
            var model = new StoreMoveCancel()
            {
                StoMoveID = -1,
                CancelReason = "Neco"
            };

            var proc = new Admin.StoreMoveCancelProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal("StoreMove nebylo nalezeno; StMoveID: -1", res.ErrorText);
        }
    }
}

﻿using System;
using S4.Entities;
using S4.ProcedureModels.Dispatch;
using System.Collections.Generic;
using TestHelpers;
using Xunit;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests2")]
    [Trait("Category", "ProcedureTests2")]
    public class DispatchSetXtraValuesProcTest : ProcTestBase
    {
        const int DIRECTION_ID = 2;

        public DispatchSetXtraValuesProcTest(DatabaseFixture fixture) : base(fixture)
        { }

        [Fact]
        public void DispatchSetXtraValuesProcTest_Success()
        {
            var model = new DispatchSetXtraValues
            {
                DirectionID = DIRECTION_ID,
                Values = new List<DispatchSetXtraValue>
                {
                    new DispatchSetXtraValue
                    {
                        Key = Direction.XTRA_DISPATCH_WEIGHT,
                        Value = new decimal(2.2)
                    },
                    new DispatchSetXtraValue
                    {
                        Key = Direction.XTRA_DISPATCH_BOXES_CNT,
                        Value = 5
                    }
                }
            };

            var proc = new Dispatch.DispatchSetXtraValuesProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success);
        }
        
        [Fact]
        public void DispatchSetXtraValuesProcTest_SuccessNullable()
        {
            var model = new DispatchSetXtraValues
            {
                DirectionID = DIRECTION_ID,
                Values = new List<DispatchSetXtraValue>
                {
                    new DispatchSetXtraValue
                    {
                        Key = Direction.XTRA_DISPATCH_WEIGHT,
                        Value = null
                    }
                }
            };

            var proc = new Dispatch.DispatchSetXtraValuesProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success);
        }


        [Fact]
        public void DispatchSetXtraValuesProcTest_ErrorBadFieldKey()
        {
            var model = new DispatchSetXtraValues
            {
                DirectionID = DIRECTION_ID,
                Values = new List<DispatchSetXtraValue>
                {
                    new DispatchSetXtraValue
                    {
                        Key = "failField",
                        Value = null
                    }
                }
            };
            
            var proc = new Dispatch.DispatchSetXtraValuesProc(USER_ID);
            Action act = () => proc.DoProcedure(model);
            
            var ex = Assert.Throws<Exception>(act);
            Assert.Contains("XtraData field", ex.Message);
        }
    }
}

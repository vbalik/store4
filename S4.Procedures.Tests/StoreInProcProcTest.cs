﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.StoreIn;

namespace S4.Procedures.Tests
{
    [Collection("StoreInTests")]
    [Trait("Category", "StoreInTests")]
    public class StoreInProcProcTest : ProcTestBase
    {
        const int DIRECTION_ID = 23;
        const int STOREMOVE_ITEM_ID = 11;

        public StoreInProcProcTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void StoreInProcProc_Success()
        {
            var model = new StoreInProc()
            {
                DirectionID = DIRECTION_ID
            };

            var proc = new StoreIn.StoreInProcProc(USER_ID);
            var res = proc.DoProcedure(model);
            Assert.True(res.Success, res.ErrorText);

            using (var db = GetDB())
            {
                //check DocStatus
                var stoMoveItem = db.SingleById<StoreMoveItem>(STOREMOVE_ITEM_ID);
                Assert.True(stoMoveItem.ItemValidity == StoreMoveItem.MI_VALIDITY_VALID, "StoreMoveItem.ItemValidity není MI_VALIDITY_VALID");
            }


        }
                
    }
}

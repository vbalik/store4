﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Dispatch;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class DispatchNextItemProcTest : ProcTestBase
    {
        public DispatchNextItemProcTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void DispatchNextItemProcTest_Success()
        {
            var model = new DispatchNextItem()
            {
                StoMoveID = 510
            };

            var proc = new Dispatch.DispatchNextItemProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            Assert.True(res.WasFound);
            Assert.Equal(113, res.DestPosition.PositionID);
        }

        [Fact]
        public void DispatchNextItemProcTest_NotFound()
        {
            var model = new DispatchNextItem()
            {
                StoMoveID = 3
            };

            var proc = new Dispatch.DispatchNextItemProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            Assert.False(res.WasFound);
        }


        [Fact]
        public void DispatchNextItemProcTest_Error()
        {
            var model = new DispatchNextItem();

            var proc = new Dispatch.DispatchNextItemProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal("Parametry procedury nejsou platné StoMoveID IsNull", res.ErrorText.Trim());
        }
    }
}

﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Receive;

namespace S4.Procedures.Tests
{
    [Collection("ReceiveTests")]
    [Trait("Category", "ReceiveTests")]
    public class ReceiveDoneProcTest : ProcTestBase
    {
        public ReceiveDoneProcTest(DatabaseFixture fixture) : base(fixture)
        { }

        [Fact]
        public void ReceiveDoneProcTest_Success()
        {
            var model = new ReceiveDone()
            {
                StoMoveID = 504
            };

            var proc = new Receive.ReceiveDoneProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var storeMove = db.FirstOrDefault<StoreMove>(new NPoco.Sql().Where("stoMoveID = @0", model.StoMoveID));
                Assert.NotNull(storeMove);
                Assert.Equal(StoreMove.MO_STATUS_SAVED_RECEIVE, storeMove.DocStatus);
                var moveItems = db.Fetch<StoreMoveItem>(new NPoco.Sql().Where("stoMoveID = @0", model.StoMoveID));
                Assert.True(moveItems.Count > 0);
                foreach (var item in moveItems)
                {
                    Assert.Equal(item.ItemValidity, StoreMoveItem.MI_VALIDITY_VALID);
                }
                var moveHist = db.Fetch<StoreMoveHistory>(new NPoco.Sql().Where("stoMoveID = @0", model.StoMoveID));
                Assert.True(moveHist.Count > 0);
                var moveHist2 = moveHist.Where(i => i.DocPosition == 0).FirstOrDefault();
                Assert.NotNull(moveHist2);
                Assert.Equal(StoreMove.MO_STATUS_SAVED_RECEIVE, moveHist2.EventCode.Trim());
                var moveHistItem = moveHist.Where(i => i.DocPosition == 0).FirstOrDefault();
                Assert.NotNull(moveHistItem);
                Assert.Equal(StoreMove.MO_STATUS_SAVED_RECEIVE, moveHistItem.EventCode.Trim());
            }
        }

        [Fact]
        public void ReceiveDoneProcTest_Error()
        {
            var model = new ReceiveCancel();

            var proc = new Receive.ReceiveCancelProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal("Parametry procedury nejsou platné StoMoveID IsNull", res.ErrorText.Trim());
        }
    }
}

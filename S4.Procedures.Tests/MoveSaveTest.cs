﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Move;
using System.Collections.Generic;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class MoveSaveTest : ProcTestBase
    {
        private const int SOURCEPOSITION_ID = 11;
        private const int DESTPOSITION_ID = 12;

        public MoveSaveTest(DatabaseFixture fixture) : base(fixture)
        { }

        [Fact]
        public void MoveSaveTest_Success()
        {
            var articleList = new List<Entities.Helpers.ArticleInfo> {
                new Entities.Helpers.ArticleInfo()
                {
                    ArticleCode = "X123",
                    ArticleDesc = "Zboží 123",
                    ArticleID = 100,
                    ArtPackID = 1001,
                    BatchNum = "1234X",
                    CarrierNum = "0",
                    ExpirationDate = new DateTime(2017, 1, 1),
                    PackDesc = "ks",
                    Quantity = 1
                }
            };

            var model = new MoveSave()
            {
                SourcePositionID = SOURCEPOSITION_ID,
                DestPositionID = DESTPOSITION_ID,
                ChangeCarrier = true,
                DestCarrierNum = "1",
                Articles = articleList,
                Remark = "remark123"
            };

            var proc = new Move.MoveSaveProc(USER_ID);
            var res = proc.DoProcedure(model);
            Assert.True(res.Success, res.ErrorText);
            Assert.True(res.StoMoveID != 0);

            var onStore = (new ServicesFactory()).GetOnStore();
            var onPosSource = onStore.OnPosition(SOURCEPOSITION_ID, StoreCore.Interfaces.ResultValidityEnum.Valid);
            Assert.Empty(onPosSource);
            var onPosDest = onStore.OnPosition(DESTPOSITION_ID, StoreCore.Interfaces.ResultValidityEnum.Valid);
            Assert.Single(onPosDest);
            Assert.Equal(100, onPosDest[0].ArticleID);
            Assert.Equal(1001, onPosDest[0].ArtPackID);
            Assert.Equal(1, onPosDest[0].Quantity);
            Assert.Equal("1", onPosDest[0].CarrierNum);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var storeMove = db.FirstOrDefault<StoreMove>(new NPoco.Sql().Where("stoMoveID = @0", res.StoMoveID));
                Assert.Equal(model.Remark, storeMove.DocRemark);
            }
        }


        [Fact]
        public void MoveSaveTest_Error()
        {
            var model = new MoveSave()
            {
                SourcePositionID = -1,
                DestPositionID = -1,
                ChangeCarrier = true,
                DestCarrierNum = "1",
                Articles = new List<Entities.Helpers.ArticleInfo>()
            };

            var proc = new Move.MoveSaveProc(USER_ID);
            var res = proc.DoProcedure(model);
            Assert.True(!res.Success, res.ErrorText);
            Assert.Equal(MoveStatusEnum.NotFound, res.PositionResultDest);
            Assert.Equal(MoveStatusEnum.NotFound, res.PositionResultSource);
            Assert.Equal("Chyba zdrojové pozice: Nenalezeno; Chyba cílové pozice: Nenalezeno", res.ErrorText);
        }


        [Fact]
        public void MoveSaveTest_BadExpirationDateError()
        {
            // bad expiration date
            var articleList = new List<Entities.Helpers.ArticleInfo> {
                new Entities.Helpers.ArticleInfo()
                {
                    ArticleCode = "X123",
                    ArticleDesc = "Zboží 123",
                    ArticleID = 100,
                    ArtPackID = 1001,
                    BatchNum = "1234X",
                    CarrierNum = "0",
                    ExpirationDate = new DateTime(2018, 1, 1),
                    PackDesc = "ks",
                    Quantity = 1
                }
            };

            var model = new MoveSave()
            {
                SourcePositionID = SOURCEPOSITION_ID,
                DestPositionID = DESTPOSITION_ID,
                ChangeCarrier = true,
                DestCarrierNum = "1",
                Articles = articleList,
                Remark = "remark123"
            };

            var proc = new Move.MoveSaveProc(USER_ID);
            var res = proc.DoProcedure(model);
            Assert.True(!res.Success, res.ErrorText);
            Assert.Equal(MoveStatusEnum.OK, res.PositionResultDest);
            Assert.Equal(MoveStatusEnum.OK, res.PositionResultSource);
            Assert.Contains("Karta neexistuje na zdrojové pozici", res.ErrorText);
        }


        [Fact]
        public void MoveSaveTest_BadBatchNumError()
        {
            // bad batchNum
            var articleList = new List<Entities.Helpers.ArticleInfo> {
                new Entities.Helpers.ArticleInfo()
                {
                    ArticleCode = "X123",
                    ArticleDesc = "Zboží 123",
                    ArticleID = 100,
                    ArtPackID = 1001,
                    BatchNum = "bad",
                    CarrierNum = "0",
                    ExpirationDate = new DateTime(2018, 1, 1),
                    PackDesc = "ks",
                    Quantity = 1
                }
            };

            var model = new MoveSave()
            {
                SourcePositionID = SOURCEPOSITION_ID,
                DestPositionID = DESTPOSITION_ID,
                ChangeCarrier = true,
                DestCarrierNum = "1",
                Articles = articleList,
                Remark = "remark123"
            };

            var proc = new Move.MoveSaveProc(USER_ID);
            var res = proc.DoProcedure(model);
            Assert.True(!res.Success, res.ErrorText);
            Assert.Equal(MoveStatusEnum.OK, res.PositionResultDest);
            Assert.Equal(MoveStatusEnum.OK, res.PositionResultSource);
            Assert.Contains("Karta neexistuje na zdrojové pozici", res.ErrorText);
        }
    }
}

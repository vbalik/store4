﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using S4.ProcedureModels.Dispatch;
using Xunit;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests2")]
    [Trait("Category", "ProcedureTests2")]
    public class DispatchReassignProcTest : ProcTestBase
    {
        const int DIRECTION_ID_SUCCESS = 21;
        const int DIRECTION_ID_NON_SUCCESS = 35;

        public DispatchReassignProcTest(DatabaseFixture fixture) : base(fixture)
        { }

        [Fact]
        public void DispatchReassignProcTest_Success()
        {
            var model = new DispatchReassign()
            {
                DirectionID = DIRECTION_ID_SUCCESS,
                DestPositionID = 123,
                WorkerID = "Test1"
            };

            int directionAssignID = 0;
            using (var db = GetDB())
            {
                var directionAssigns1 = db.Fetch<DirectionAssign>("where directionid = @0", DIRECTION_ID_SUCCESS);
                Assert.True(directionAssigns1.Count == 1);
                directionAssignID = directionAssigns1[0].DirectAssignID;
            }

            var proc = new Dispatch.DispatchReassignProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var directionAssigns2 = db.Fetch<DirectionAssign>("where directionid = @0", DIRECTION_ID_SUCCESS);
                Assert.True(directionAssigns2.Count == 1);
                Assert.True(directionAssigns2[0].DirectAssignID != directionAssignID);

                //test xtraData
                var xtraData = new Entities.XtraData.XtraDataProxy<Entities.DirectionXtra>(Direction.XtraDataDict);
                xtraData.Load(db, new NPoco.Sql().Where("DirectionID = @0", DIRECTION_ID_SUCCESS));
                Assert.NotEmpty(xtraData.Data);
                Assert.Equal(model.DestPositionID, xtraData[Direction.XTRA_DISPATCH_POSITION_ID]);

                var hist = db.Fetch<DirectionHistory>(new NPoco.Sql().Where("directionid = @0", DIRECTION_ID_SUCCESS));
                Assert.True(hist.Count > 0);
                var hist2 = hist.OrderByDescending(h => h.DirectionID).First();
                Assert.Equal(DIRECTION_ID_SUCCESS, hist2.DirectionID);
                Assert.Equal(Direction.DR_STATUS_DISPATCH_WAIT, hist2.EventCode.Trim());
                Assert.Equal("Test1|123", hist2.EventData);
                Assert.Equal(USER_ID, hist2.EntryUserID.Trim());
                Assert.True(hist2.EntryDateTime < DateTime.Now);
            }

        }


        [Fact]
        public void DispatchReassignProcTest_Non_Success()
        {
            var model = new DispatchReassign()
            {
                DirectionID = DIRECTION_ID_NON_SUCCESS,
                DestPositionID = 123,
                WorkerID = "Test1"
            };

            
            var proc = new Dispatch.DispatchReassignProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.False(res.Success);
            Assert.Equal("Změna pozice vyskladnění není možná - na pozici 118 už jsou nějaké pohyby; DirectionID: 35", res.ErrorText);
        }


        [Fact]
        public void DispatchReassignProcTest_Error()
        {
            var model = new DispatchReassign()
            {
                DirectionID = -1,
                DestPositionID = -1,
                WorkerID = "-1"
            };

            var proc = new Dispatch.DispatchReassignProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal("Doklad nebyl nalezen; DirectionID: -1", res.ErrorText);
        }
    }
}

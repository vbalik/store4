﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Move;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class MoveGetPositionTest : ProcTestBase
    {
        private const int POSITIONID = 8;

        public MoveGetPositionTest(DatabaseFixture fixture) : base(fixture)
        { }
        
        [Fact]
        public void MoveGetPositionTest_Success()
        {
            var model = new MoveGetPosition()
            {
                PositionID = POSITIONID
            };

            var proc = new Move.MoveGetPositionProc(USER_ID);
            var res = proc.DoProcedure(model);
            Assert.True(res.Success, res.ErrorText);
            Assert.Equal(MoveStatusEnum.OK, res.PositionResult);
            Assert.True(res.Articles.Count == 1);
            Assert.True(res.Articles[0].ArtPackID == 1001);
        }


        [Fact]
        public void MoveGetPositionTest_NotFound()
        {
            var model = new MoveGetPosition()
            {
                PositionID = -1
            };

            var proc = new Move.MoveGetPositionProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            Assert.Equal(MoveStatusEnum.NotFound, res.PositionResult);
        }

    }
}

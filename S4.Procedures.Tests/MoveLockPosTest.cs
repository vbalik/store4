﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Move;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class MoveLockPosTest : ProcTestBase
    {
        private const int POSITIONID = 7;

        public MoveLockPosTest(DatabaseFixture fixture) : base(fixture)
        { }
        
        [Fact]
        public void MoveLockPosTest_Success()
        {
            var model = new MoveLockPos()
            {
                PositionID = POSITIONID
            };

            var proc = new Move.MoveLockPosProc(USER_ID);
            var res = proc.DoProcedure(model);
            Assert.True(res.Success, res.ErrorText);
            Assert.Equal(MoveStatusEnum.OK, res.PositionResult);
        }


        [Fact]
        public void MoveLockPosTest_Error()
        {
            var model = new MoveLockPos()
            {
                PositionID = -1
            };

            var proc = new Move.MoveLockPosProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            Assert.Equal(MoveStatusEnum.NotFound, res.PositionResult);
        }

    }
}

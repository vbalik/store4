﻿using S4.Entities;
using S4.ProcedureModels.Dispatch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestHelpers;
using Xunit;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests2")]
    [Trait("Category", "ProcedureTests2")]
    public class DispatchSaveItemProcTest : ProcTestBase
    {
        public DispatchSaveItemProcTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void DispatchSaveItemProcTest_Success()
        {
            var model = new DispatchSaveItem()
            {
                StoMoveID = 511,
                StoMoveItemID = 122
            };


            var proc = new Dispatch.DispatchSaveItemProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var storeMoveItem = db.SingleOrDefaultById<StoreMoveItem>( model.StoMoveItemID);
                Assert.Equal(StoreMoveItem.MI_VALIDITY_VALID, storeMoveItem.ItemValidity);

                var storeMove = db.SingleOrDefaultById<StoreMove>(model.StoMoveID);            
                var dirItems = db.Fetch<DirectionItem>(new NPoco.Sql().Where("directionID = @0", storeMove.Parent_directionID));
                Assert.NotEmpty(dirItems);
                Assert.Equal(storeMoveItem.Parent_docPosition, dirItems[0].DocPosition);
                Assert.Equal(DirectionItem.DI_STATUS_DISPATCH_SAVED, dirItems[0].ItemStatus);

                var hist = db.Fetch<StoreMoveHistory>(new NPoco.Sql().Where("stoMoveID = @0", model.StoMoveID));
                Assert.True(hist.Count > 0);
                var hist2 = hist.OrderByDescending(h => h.StoMoveID).First();
                Assert.Equal(StoreMove.MO_EVENT_CONF_DISPATCH, hist2.EventCode.Trim());
                Assert.Equal(USER_ID, hist2.EntryUserID.Trim());
                Assert.True(hist2.EntryDateTime <= DateTime.Now);
            }
        }


        [Fact]
        public void DispatchSaveItemProcTest_Error()
        {
            var model = new DispatchSaveItem()
            {
                StoMoveID = 510,
                StoMoveItemID = 16
            };

            var proc = new Dispatch.DispatchSaveItemProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal($"Doklad nemá očekávaný status MO_STATUS_NEW_DISPATCH; StoMoveID: {model.StoMoveID}", res.ErrorText);
        }
    }
}

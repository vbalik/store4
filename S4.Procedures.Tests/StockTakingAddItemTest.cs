﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;

namespace S4.Procedures.Tests
{
    [Collection("StockTakingTests")]
    [Trait("Category", "StockTakingTests")]
    public class StockTakingAddItemTest : ProcTestBase
    {

        int STOTAK_ID = 3;

        public StockTakingAddItemTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void StockTakingAddItemTest_Success()
        {
            var model = new ProcedureModels.StockTaking.StockTakingAddItem()
            {
                StotakID = STOTAK_ID,
                ArtPackID = 1002,
                BatchNum = "XXX",
                CarrierNum = "YYY",
                ExpirationDate = new DateTime(2020,1,1),
                PositionID = 2,
                Quantity = 10
            };

            using (var db = GetDB())
            {
               
                var proc = new StockTaking.StockTakingAddItemProc(USER_ID);
                var res = proc.DoProcedure(model);

                Assert.True(res.Success, res.ErrorText);

                // get item
                var items = db.Fetch<StockTakingItem>("where stotakid = @0", STOTAK_ID);
                Assert.True(items != null);
                Assert.True(items.Count == 1);
                Assert.True(items[0].ArtPackID == 1002);
                Assert.True(items[0].BatchNum == "XXX");
                Assert.True(items[0].CarrierNum == "YYY");
                Assert.True(items[0].ExpirationDate == new DateTime(2020, 1, 1));
                Assert.True(items[0].PositionID == 2);
                Assert.True(items[0].Quantity == 10);


                //his
                var hist = db.Fetch<StockTakingHistory>(new NPoco.Sql().Where("stotakID = @0", STOTAK_ID));
                Assert.True(hist.Count > 0);
                var hist2 = hist.OrderByDescending(h => h.StotakHistID).First();
                Assert.Equal(STOTAK_ID, hist2.StotakID);
                Assert.Equal(StockTakingHistory.STOCKTAKING_EVENT_ITEM_ADDED, hist2.EventCode.Trim());
                Assert.Equal(USER_ID, hist2.EntryUserID.Trim());
                Assert.True(hist2.EntryDateTime <= DateTime.Now);
                Assert.True(hist2.ArtPackID == 1002);
                Assert.True(hist2.BatchNum == "XXX");
                Assert.True(hist2.CarrierNum == "YYY");
                Assert.True(hist2.ExpirationDate == new DateTime(2020, 1, 1));
                Assert.True(hist2.PositionID == 2);
                Assert.True(hist2.Quantity == 10);
            }
            
        }
                

        [Fact]
        public void StockTakingAddItemTest_Error()
        {
            var model = new ProcedureModels.StockTaking.StockTakingAddItem()
            {
                StotakID = -1,
                ArtPackID = -1,
                BatchNum = "x",
                CarrierNum = "x",
                ExpirationDate = DateTime.Now,
                PositionID = -1,
                Quantity = 1
            };

            var proc = new StockTaking.StockTakingAddItemProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal("StockTaking nebylo nalezeno; StotakID: -1", res.ErrorText);
        }

        [Fact]
        public void StockTakingAddItemTest_BatchNum_Null_Success()
        {
            var model = new ProcedureModels.StockTaking.StockTakingAddItem()
            {
                StotakID = -1,
                ArtPackID = -1,
                BatchNum = null,
                CarrierNum = "x",
                ExpirationDate = DateTime.Now,
                PositionID = -1,
                Quantity = 1
            };

            var proc = new StockTaking.StockTakingAddItemProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal("StockTaking nebylo nalezeno; StotakID: -1", res.ErrorText);
        }


        [Fact]
        public void StockTakingAddItemTest_BatchNumLength_Error()
        {
            var model = new ProcedureModels.StockTaking.StockTakingAddItem()
            {
                StotakID = STOTAK_ID,
                ArtPackID = 1002,
                BatchNum = "123456789012345678901234567890XXXX",
                CarrierNum = "YYY",
                ExpirationDate = new DateTime(2020, 1, 1),
                PositionID = 2,
                Quantity = 10
            };

            var proc = new StockTaking.StockTakingAddItemProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Contains("BatchNum je delší než povolená délka", res.ErrorText);
        }
    }
}

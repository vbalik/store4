﻿using S4.Entities;
using S4.Procedures.Dispatch;
using S4.ProcedureModels.Dispatch;
using System;
using System.Collections.Generic;
using System.Text;
using TestHelpers;
using Xunit;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class DispatchCalcBoxLabelsProcTest : ProcTestBase
    {
        const int DIRECTION_ID = 7;

        public DispatchCalcBoxLabelsProcTest(DatabaseFixture fixture) : base(fixture)
        {
        }


        [Fact]
        public void DispatchCalcBoxLabelsProcTest_Success()
        {
            var model = new DispatchCalcBoxLabels()
            {
                DirectionID = DIRECTION_ID
            };

            var proc = new DispatchCalcBoxLabelsProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            Assert.NotEmpty(res.BoxLabels);
            Assert.Equal("dep0", res.BoxLabels[0].LabelText);
            Assert.NotEmpty(res.Reports);
            Assert.NotEmpty(res.Printers);


        }


        [Fact]
        public void DispatchCalcBoxLabelsProcTest_Error()
        {
            var model = new DispatchCalcBoxLabels()
            {
                DirectionID = -1
            };

            var proc = new DispatchCalcBoxLabelsProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.False(res.Success);
        }
    }
}

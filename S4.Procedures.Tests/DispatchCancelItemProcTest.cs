﻿using S4.Entities;
using S4.ProcedureModels.Dispatch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestHelpers;
using Xunit;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class DispatchCancelItemProcTest : ProcTestBase
    {
        public DispatchCancelItemProcTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void DispatchCancelItemProcTest_Success()
        {
            var model = new DispatchCancelItem()
            {
                StoMoveID = 519,
                StoMoveItemID = 121
            };


            var proc = new Dispatch.DispatchCancelItemProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var storeMoveItem = db.FirstOrDefault<StoreMoveItem>("where stoMoveItemID = @0", model.StoMoveItemID);
                Assert.Equal(StoreMoveItem.MI_VALIDITY_CANCELED, storeMoveItem.ItemValidity);

                var hist = db.Fetch<StoreMoveHistory>(new NPoco.Sql().Where("stoMoveID = @0", model.StoMoveID));
                Assert.True(hist.Count > 0);
                var hist2 = hist.OrderByDescending(h => h.StoMoveID).First();
                Assert.Equal(StoreMove.MO_STATUS_CANCEL, hist2.EventCode.Trim());
                Assert.Equal(USER_ID, hist2.EntryUserID.Trim());
                Assert.True(hist2.EntryDateTime <= DateTime.Now);
            }
        }


        [Fact]
        public void DispatchCancelItemProcTest_Error()
        {
            var model = new DispatchCancelItem()
            {
                StoMoveID = 510,
                StoMoveItemID = 16
            };

            var proc = new Dispatch.DispatchCancelItemProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal($"Doklad nemá očekávaný status MO_STATUS_NEW_DISPATCH; StoMoveID: {model.StoMoveID}", res.ErrorText);
        }
    }
}

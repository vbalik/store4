﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;

namespace S4.Procedures.Tests
{
    [Collection("StockTakingTests")]
    [Trait("Category", "StockTakingTests")]
    public class StockTakingScanPosTest : ProcTestBase
    {

        public StockTakingScanPosTest(DatabaseFixture fixture) : base(fixture) 
        { }


        [Fact]
        public void StockTakingScanPosTest_Success()
        {
            var model = new ProcedureModels.StockTaking.StockTakingScanPos()
            {
                StotakID = 1,
                PositionID = 1
                
            };
            var proc = new StockTaking.StockTakingScanPosProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            Assert.True(res.StockTakingItems.Count > 0);
        }


        [Fact]
        public void StockTakingScanPosTest_BadPosition()
        {
            var model = new ProcedureModels.StockTaking.StockTakingScanPos()
            {
                StotakID = 1,
                PositionID = -1

            };
            var proc = new StockTaking.StockTakingScanPosProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success);
            Assert.True(res.BadPosition);
        }


        [Fact]
        public void StockTakingScanPosTest_Error()
        {
            var model = new ProcedureModels.StockTaking.StockTakingScanPos()
            {
                StotakID = -1,
                PositionID = -1

            };
            var proc = new StockTaking.StockTakingScanPosProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.False(res.Success);
            Assert.Equal("StockTaking nebylo nalezeno; StotakID: -1", res.ErrorText);
        }
    }
}

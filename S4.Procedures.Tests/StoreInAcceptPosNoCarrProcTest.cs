﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.StoreIn;

namespace S4.Procedures.Tests
{
    [Collection("StoreInTests")]
    [Trait("Category", "StoreInTests")]
    public class StoreInAcceptPosNoCarrProcTest : ProcTestBase
    {
        const int DIRECTION_ID = 23;

        public StoreInAcceptPosNoCarrProcTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void StoreInAcceptPosNoCarrProcTest_Success()
        {
            var article = new Entities.Helpers.ArticleInfo()
            {
                ArticleCode = "X123",
                ArticleDesc = "Zboží 123",
                ArticleID = 100,
                ArtPackID = 1001,
                BatchNum = "1234X",
                CarrierNum = "0",
                ExpirationDate = new DateTime(2018, 1, 1),
                PackDesc = "ks",
                Quantity = 1
            };

            var model = new StoreInAcceptPosNoCarr()
            {
                Article = article,
                DirectionID = DIRECTION_ID,
                SrcPositionID = 1,
                DestPositionID = 2
            };

            var proc = new StoreIn.StoreInAcceptPosNoCarrProc(USER_ID);
            var res = proc.DoProcedure(model);
            Assert.True(res.Success, res.ErrorText);

            using (var db = GetDB())
            {
                //check DocStatus
                //var stoMoveItem = db.SingleById<StoreMoveItem>(STOREMOVE_ITEM_ID);
                //Assert.True(stoMoveItem.ItemValidity == StoreMoveItem.MI_VALIDITY_CANCELED, "StoreMoveItem.ItemValidity není MI_VALIDITY_CANCELED");
            }


        }

        [Fact]
        public void StoreInAcceptPosNoCarrProcTest_Error()
        {
            var model = new StoreInAcceptPosNoCarr()
            {
                Article = new Entities.Helpers.ArticleInfo(),
                DirectionID = -1,
                SrcPositionID = 0,
                DestPositionID = 0
            };

            var proc = new StoreIn.StoreInAcceptPosNoCarrProc(USER_ID);
            var res = proc.DoProcedure(model);
            Assert.True(!res.Success);
        }


    }
}

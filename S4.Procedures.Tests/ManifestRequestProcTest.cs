﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Delivery;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class ManifestRequestProcTest : ProcTestBase
    {
        public ManifestRequestProcTest(DatabaseFixture fixture) : base(fixture)
        { }

        [Fact]
        public void ManifestRequestProc_MissingParams()
        {
            var manifestRequest = new ManifestRequest();
            manifestRequest = new Procedures.Delivery.ManifestRequestProc(USER_ID).DoProcedure(manifestRequest);

            Assert.False(manifestRequest.Success);
            Assert.Equal("Parametry procedury nejsou platné DirectionIDs IsNull ", manifestRequest.ErrorText);
        }

        [Fact]
        public void ManifestRequestProc_Success()
        {
            var getDirectionsForManifest = new Procedures.Delivery.GetDirectionsForManifestProc(USER_ID).DoProcedure(
                new GetDirectionsForManifest() { DeliveryProvider = DeliveryDirection.DeliveryProviderEnum.DPD });
            var manifestRequest = new ManifestRequest()
            {
                DeliveryProvider = DeliveryDirection.DeliveryProviderEnum.DPD,
                DirectionIDs = getDirectionsForManifest.Directions
            };
            manifestRequest = new Procedures.Delivery.ManifestRequestProc(USER_ID).DoProcedure(manifestRequest);

            //check success
            Assert.True(manifestRequest.Success);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var deliveryDirections = new DAL.DeliveryDirectionDAL().GetByDirectionIDs(getDirectionsForManifest.Directions.ToArray(), db);
                foreach (var deliveryDirection in deliveryDirections)
                {
                    var manifest = new DAL.DeliveryManifestDAL().Get(deliveryDirection.DeliveryManifestID ?? 0);
                    Assert.NotNull(manifest);
                    Assert.Equal("WAIT", manifest.ManifestStatus);
                }
            }
        }
    }
}

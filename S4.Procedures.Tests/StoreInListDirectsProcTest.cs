﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.StoreIn;

namespace S4.Procedures.Tests
{
    [Collection("StoreInTests")]
    [Trait("Category", "StoreInTests")]
    public class StoreInListDirectsProcTest : ProcTestBase
    {
        const string WORKER_ID = "WRK1";
        
        public StoreInListDirectsProcTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void StoreInListDirectsProcTest_Success()
        {
            var model = new StoreInListDirects()
            {
                WorkerID = WORKER_ID
            };

            var proc = new StoreIn.StoreInListDirectsProc(USER_ID);
            var res = proc.DoProcedure(model);
            Assert.True(res.Success, res.ErrorText);
            Assert.True(res.Directions.Count == 1, "DocumentInfo --> procedura musí vrátit 1 větu");
            Assert.Equal("PRIJ / 16 / 9", res.Directions[0].DocumentName);
            Assert.Equal(100, res.Directions[0].DocumentID);
        }
                
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace TestHelpers
{
    [CollectionDefinition("ProcedureTests")]
    public class DatabaseCollection : ICollectionFixture<DatabaseFixture>
    {
        // This class has no code, and is never created. Its purpose is simply
        // to be the place to apply [CollectionDefinition] and all the
        // ICollectionFixture<> interfaces.
    }

    [CollectionDefinition("ProcedureTests2")]
    public class ProcedureTests2Collection : ICollectionFixture<DatabaseFixture>
    {
    }

    [CollectionDefinition("StockTakingTests")]
    public class StockTakingCollection : ICollectionFixture<DatabaseFixture>
    {
    }

    [CollectionDefinition("StoreInTests")]
    public class StoreInCollection : ICollectionFixture<DatabaseFixture>
    {
    }

    [CollectionDefinition("StoreInTests2")]
    public class StoreIn2Collection : ICollectionFixture<DatabaseFixture>
    {
    }

    [CollectionDefinition("ReceiveTests")]
    public class ReceiveCollection : ICollectionFixture<DatabaseFixture>
    {
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using TestHelpers;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using System.Threading;
using System.Threading.Tasks;
using S4.DeliveryServices;

namespace S4.Procedures.Tests
{
    public class ProcTestBase
    {
        protected const string USER_ID = "USER1";

        protected DatabaseFixture fixture;


        public ProcTestBase(DatabaseFixture fixture)
        {
            this.fixture = fixture;
            Core.Data.ConnectionHelper.ConnectionString = fixture.ConnectionString;


            // setup ServicesFactory.ServiceProvider
            var sc = new ServiceCollection();
            // IBackgroundTaskQueue
            var backgroundTaskQueue = new Mock<Core.Interfaces.IBackgroundTaskQueue>();
            backgroundTaskQueue.Setup(m => m.QueueBackgroundWorkItem(It.IsAny<Func<CancellationToken, Task>>(), It.IsAny<Func<CancellationToken, Task>>())).Returns("123");
            sc.AddSingleton<Core.Interfaces.IBackgroundTaskQueue>(backgroundTaskQueue.Object);
            // IMessageRouter
            var messageRouter = new Mock<S4.Messages.IMessageRouter>();
            messageRouter.Setup(m => m.Publish(It.IsAny<S4.Messages.MessageBase>())).Verifiable();
            sc.AddSingleton<S4.Messages.IMessageRouter>(messageRouter.Object);
            // delivery service
            var deliveryService = new Mock<S4.DeliveryServices.IDeliveryCommon>();
            deliveryService.Setup(m => m.GetDeliveryProviders())
                .Returns(() => { return new Entities.DeliveryDirection.DeliveryProviderEnum[] { Entities.DeliveryDirection.DeliveryProviderEnum.DPD, Entities.DeliveryDirection.DeliveryProviderEnum.PPL }; });
            deliveryService.Setup(m => m.GetShipmentLabel(46))
                .Returns(Task.CompletedTask)
                .Raises(m => m.GetLabelEvent += null, this, new DeliveryServices.GetLabelEventArgs()
                { Data = new byte[] { 4, 6 }, DeliveryService = new DSDPD(), ReferenceId = "41" });
            deliveryService.Setup(m => m.GetManifestReport(Entities.DeliveryDirection.DeliveryProviderEnum.DPD, "123"))
                .Returns(Task.CompletedTask)
                .Raises(m => m.GetManifestReportEvent += null, this, new DeliveryServices.GetLabelEventArgs()
                { Data = new byte[] { 1, 2, 3 }, DeliveryService = new DSDPD(), ReferenceId = "123" });
            deliveryService.Setup(m => m.GetShipmentStatus(46, 0))
                .Returns(Task.CompletedTask)
                .Raises(m => m.GetShipmentStatusEvent += null, this, Entities.DeliveryDirection.DeliveryProviderEnum.DPD, new Entities.Models.LabeledValue[]
                {
                    new Entities.Models.LabeledValue("key1", "label1", "value1"),
                    new Entities.Models.LabeledValue("key2", "label2", "value2")
                });
            sc.AddSingleton<S4.DeliveryServices.IDeliveryCommon>(deliveryService.Object);

            var serviceProvider = sc.BuildServiceProvider();
            S4.Procedures.ServicesFactory.ServiceProvider = serviceProvider;
        }

        protected NPoco.Database GetDB()
        {
            var db = new NPoco.Database(fixture.ConnectionString, NPoco.DatabaseType.SqlServer2012, System.Data.SqlClient.SqlClientFactory.Instance);
            db.Mappers.Add(new Core.Data.NPocoEnumMapper());
            return db;
        }
    }
}

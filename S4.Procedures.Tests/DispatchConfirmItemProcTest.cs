﻿using S4.Entities;
using S4.ProcedureModels.Dispatch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestHelpers;
using Xunit;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class DispatchConfirmItemProcTest : ProcTestBase
    {
        public DispatchConfirmItemProcTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void DispatchConfirmItemProcTest_Success()
        {
            var model = new DispatchConfirmItem()
            {
                StoMoveID = 511,
                StoMoveItemID = 20
            };


            var proc = new Dispatch.DispatchConfirmItemProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var storeMoveItem = db.FirstOrDefault<StoreMoveItem>("where stoMoveItemID = @0", model.StoMoveItemID);
                Assert.Equal(StoreMoveItem.MI_VALIDITY_VALID, storeMoveItem.ItemValidity);

                var hist = db.Fetch<StoreMoveHistory>(new NPoco.Sql().Where("stoMoveID = @0", model.StoMoveID));
                Assert.True(hist.Count > 0);
                var hist2 = hist.OrderByDescending(h => h.StoMoveID).First();
                Assert.Equal(StoreMove.MO_EVENT_ITEM_CHANGED, hist2.EventCode.Trim());
                Assert.Equal(USER_ID, hist2.EntryUserID.Trim());
                Assert.True(hist2.EntryDateTime <= DateTime.Now);
            }
        }


        [Fact]
        public void DispatchConfirmItemProcTest_Error()
        {
            var model = new DispatchConfirmItem()
            {
                StoMoveID = 510,
                StoMoveItemID = 16
            };

            var proc = new Dispatch.DispatchConfirmItemProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal($"Doklad nemá očekávaný status MO_STATUS_NEW_DISPATCH; StoMoveID: {model.StoMoveID}", res.ErrorText);
        }
    }
}

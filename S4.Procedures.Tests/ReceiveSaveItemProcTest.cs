﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Receive;

namespace S4.Procedures.Tests
{
    [Collection("ReceiveTests")]
    [Trait("Category", "ReceiveTests")]
    public class ReceiveSaveItemProcTest : ProcTestBase
    {
        public ReceiveSaveItemProcTest(DatabaseFixture fixture) : base(fixture)
        { }

        [Fact]
        public void ReceiveSaveItemProcTest_Success()
        {
            var model = new ReceiveSaveItem()
            {
                ArtPackID = 1001,
                BatchNum = "BATCH",
                CarrierNum = "CARRIER",
                ExpirationDate = DateTime.Now,
                PositionID = 1,
                Quantity = (decimal)2.5,
                StoMoveID = 1,
                UDICode = "018765432111111"
            };

            var proc = new Receive.ReceiveSaveItemProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var moveItems = db.Fetch<StoreMoveHistory>(new NPoco.Sql().Where("stoMoveID = @0", model.StoMoveID));
                Assert.True(moveItems.Count > 0);
                var lastMoveItem = moveItems.OrderByDescending(i => i.DocPosition).First();
                var moveHist = db.Fetch<StoreMoveHistory>(new NPoco.Sql().Where("stoMoveID = @0", model.StoMoveID));
                Assert.True(moveHist.Count > 0);
                var moveHist2 = moveHist.OrderByDescending(h => h.StoMoveID).First();
                Assert.Equal(lastMoveItem.DocPosition, moveHist2.DocPosition);
                Assert.Equal(StoreMove.MO_EVENT_ITEM_CHANGED, moveHist2.EventCode.Trim());
                Assert.Equal(USER_ID, moveHist2.EntryUserID.Trim());
                Assert.True(moveHist2.EntryDateTime < DateTime.Now);
                
                var move = db.Single<StoreMove>(new NPoco.Sql().Where("stoMoveID = @0", model.StoMoveID));
                var directionItems = db.Fetch<DirectionItem>(new NPoco.Sql().Where("directionID = @0", move.Parent_directionID));
                Assert.NotEmpty(directionItems);
                Assert.Equal(model.ArtPackID, directionItems[0].ArtPackID);
                Assert.Equal(model.Quantity, directionItems[0].Quantity);
                Assert.Equal(1, directionItems[0].DocPosition);
                Assert.Equal(DirectionItem.DI_STATUS_RECEIVE_IN_PROC, directionItems[0].ItemStatus);

                var storeMoveItems = db.Fetch<StoreMoveItem>(new NPoco.Sql().Where("stoMoveID = @0", move.StoMoveID));
                var lot = db.SingleOrDefaultById<StoreMoveLot>(storeMoveItems[0].StoMoveLotID);
                Assert.Equal(model.UDICode, lot.UDICode);
            }
        }

        [Fact]
        public void ReceiveUpdateQuantityItemProcTest_Success()
        {
            var model = new ReceiveSaveItem()
            {
                ArtPackID = 1024,
                BatchNum = "BATCH1024",
                CarrierNum = "",
                ExpirationDate = new DateTime(2022,1,1),
                PositionID = 1,
                Quantity = (decimal)2.5,
                StoMoveID = 531
            };

            var proc = new Receive.ReceiveSaveItemProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success);
            Assert.True(res.RepeatedItem);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var moveItems = db.Fetch<StoreMoveItem>(new NPoco.Sql().Where("stoMoveID = @0", model.StoMoveID));
                Assert.Equal(2, moveItems.Count);
                Assert.Equal(StoreMoveItem.MI_VALIDITY_CANCELED, moveItems.First().ItemValidity);
                Assert.Equal(StoreMoveItem.MI_VALIDITY_VALID, moveItems.Last().ItemValidity);
                
                var moveItem = moveItems.Last();
                var lot = db.Single<StoreMoveLot>(new NPoco.Sql().Where("stoMoveLotID = @0", moveItem.StoMoveLotID));

                Assert.Equal(moveItems.First().Quantity + model.Quantity, moveItem.Quantity);
                Assert.Equal(model.BatchNum, lot.BatchNum);
                Assert.Null(moveItem.CarrierNum);
            }
        }

        [Fact]
        public void ReceiveSaveItemProcTest_Error()
        {
            var model = new ReceiveSaveItem()
            {
                ArtPackID = 1001,
                BatchNum = "BATCH",
                CarrierNum = "CARRIER",
                ExpirationDate = DateTime.Now,
                PositionID = 1,
                Quantity = (decimal) 2.5,
                StoMoveID = -1
            };

            var proc = new Receive.ReceiveSaveItemProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.False(res.Success);
            Assert.Equal("StoreMove nebylo nalezeno; StMoveID: -1", res.ErrorText.Trim());
        }
    }
}

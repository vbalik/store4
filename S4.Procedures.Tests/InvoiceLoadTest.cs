﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Load;
using S4.Entities.XtraData;
using S4.Entities.Helpers.Xml.Dispatch;
using S4.Entities.Helpers;
using S4.Entities.Helpers.Xml.Invoice;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests2")]
    [Trait("Category", "ProcedureTests2")]
    public class InvoiceLoadTest : ProcTestBase
    {

        public InvoiceLoadTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void InvoiceLoadTest_Success()
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                InvoiceS3 invoiceS3 = null;
                var xml = XmlHelper.GetStringFromXMLFile(@"..\..\..\XmlTestData\Invoice1.xml");
                System.Exception exception = null;
                var load = InvoiceS3.Serializer.Deserialize(xml, out invoiceS3, out exception);

                Assert.True(load);
                                
                var invoiceNull = db.FirstOrDefault<Invoice>("where docNumber = @0 AND docPrefix = @1 AND docYear = @2", invoiceS3.DocNumber, invoiceS3.DocPrefix, invoiceS3.DocDate.ToString("yy"));
                Assert.Null(invoiceNull);

                var directionNull = db.FirstOrDefault<Direction>("where docNumber = @0 AND docNumPrefix = @1", invoiceS3.DispatchDocs[0].DocNumber, invoiceS3.DispatchDocs[0].DocPrefix);
                Assert.Null(directionNull);

                var model = new InvoiceLoad()
                {
                    InvoiceS3 = invoiceS3,
                    Xml = xml,
                    Remark = "Invoice1.xml"
                };

                var proc = new Load.InvoiceLoadProc(USER_ID);
                var res = proc.DoProcedure(model);

                Assert.True(res.Success, res.ErrorText);

                var invoiceSaved = db.FirstOrDefault<Invoice>("where docNumber = @0 AND docPrefix = @1 AND docYear = @2", invoiceS3.DocNumber, invoiceS3.DocPrefix, invoiceS3.DocDate.ToString("yy"));
                Assert.NotNull(invoiceSaved);

                var directionSaved = db.FirstOrDefault<Direction>("where docNumber = @0 AND docNumPrefix = @1", invoiceS3.DispatchDocs[0].DocNumber, invoiceS3.DispatchDocs[0].DocPrefix);
                Assert.NotNull(directionSaved);

                var xtraDataSaved = new XtraDataProxy<Entities.InvoiceXtra>(Invoice.XtraDataDict);
                xtraDataSaved.Load(db, new NPoco.Sql().Where("InvoiceID = @0", invoiceSaved.InvoiceID));

                Assert.Equal(invoiceS3.TransportationType, xtraDataSaved[Invoice.XTRA_TRANSPORTATION_TYPE]);
                Assert.Equal(invoiceS3.PaymentType, xtraDataSaved[Invoice.XTRA_PAYMENT_TYPE]);
                Assert.Equal(invoiceS3.PaymentReference, xtraDataSaved[Invoice.XTRA_PAYMENT_REFERENCE]);
                Assert.Equal(invoiceS3.CashOnDelivery, xtraDataSaved[Invoice.XTRA_CASH_ON_DELIVERY]);
                Assert.Equal(invoiceS3.Country, xtraDataSaved[Invoice.XTRA_DELIVERY_COUNTRY]);
                Assert.Equal(invoiceS3.Currency, xtraDataSaved[Invoice.XTRA_DELIVERY_CURRENCY]);
                Assert.Equal(invoiceS3.DocDate, xtraDataSaved[Invoice.XTRA_DOC_DATE]);

                var invoce1 = db.FirstOrDefault<Invoice>("where invoiceID = @0", res.InvoceID);
                Assert.Equal(invoce1.DocNumber, invoiceS3.DocNumber);

                //update all data is same
                model = new InvoiceLoad()
                {
                    InvoiceS3 = invoiceS3,
                    Xml = xml,
                    Remark = "Invoice1.xml"
                };

                var proc1 = new Load.InvoiceLoadProc(USER_ID);
                var res1 = proc1.DoProcedure(model);

                Assert.True(res1.Success, res1.ErrorText);

                //update invoiceID is diffrent
                InvoiceS3 invoiceS3_2 = null;
                xml = XmlHelper.GetStringFromXMLFile(@"..\..\..\XmlTestData\Invoice2.xml");
                load = InvoiceS3.Serializer.Deserialize(xml, out invoiceS3_2, out exception);

                Assert.True(load);
                model = new InvoiceLoad()
                {
                    InvoiceS3 = invoiceS3_2,
                    Xml = xml,
                    Remark = "Invoice2.xml"
                };

                var proc2 = new Load.InvoiceLoadProc(USER_ID);
                var res2 = proc2.DoProcedure(model);

                Assert.False(res2.Success, res2.ErrorText);

                //Items is diffrent
                InvoiceS3 invoiceS3_3 = null;
                xml = XmlHelper.GetStringFromXMLFile(@"..\..\..\XmlTestData\Invoice3.xml");
                exception = null;
                load = InvoiceS3.Serializer.Deserialize(xml, out invoiceS3_3, out exception);

                Assert.True(load);

                model = new InvoiceLoad()
                {
                    InvoiceS3 = invoiceS3_3,
                    Xml = xml,
                    Remark = "Invoice3.xml"
                };

                var proc3 = new Load.InvoiceLoadProc(USER_ID);
                var res3 = proc3.DoProcedure(model);

                Assert.False(res3.Success, res3.ErrorText);
            }
        }




    }
}

﻿using S4.Entities;
using System;
using System.Linq;
using Moq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Delivery;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class GetDeliveryProvidersProcTest : ProcTestBase
    {
        public GetDeliveryProvidersProcTest(DatabaseFixture fixture) : base(fixture)
        { }

        [Fact]
        public void GetDeliveryProviders()
        {
            var getDeliveryProviders = new GetDeliveryProviders();
            getDeliveryProviders = new Procedures.Delivery.GetDeliveryProvidersProc(USER_ID).DoProcedure(getDeliveryProviders);
            var providers = getDeliveryProviders.Providers.Select(i => i.providerCode);
            Assert.Collection(providers,
                item => Assert.Equal(DeliveryDirection.DeliveryProviderEnum.DPD, item),
                item => Assert.Equal(DeliveryDirection.DeliveryProviderEnum.PPL, item));
        }
    }
}

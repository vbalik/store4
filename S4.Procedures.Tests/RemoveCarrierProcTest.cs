﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Admin;
using S4.Procedures.Admin;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class RemoveCarrierProcTest : ProcTestBase
    {
        const int POSITION_ID = 121;

        public RemoveCarrierProcTest(DatabaseFixture fixture) : base(fixture)
        { }

        [Fact]
        public void RemoveExpBatchProcTest_Success()
        {
            var onStore = new StoreCore.OnStore_Basic.OnStore().OnPosition(POSITION_ID, StoreCore.Interfaces.ResultValidityEnum.Reservation);
            Assert.Equal(4, onStore.Count);
            Assert.Collection<StoreCore.Interfaces.OnStoreItemFull>(onStore,
                item => { Assert.Equal(121, item.Article.ArticleID); Assert.Null(item.CarrierNum); Assert.Equal(2, item.Quantity); },
                item => { Assert.Equal(121, item.Article.ArticleID); Assert.Equal("CARR1", item.CarrierNum); Assert.Equal(3, item.Quantity); },
                item => { Assert.Equal(122, item.Article.ArticleID); Assert.Null(item.CarrierNum); Assert.Equal(4, item.Quantity); },
                item => { Assert.Equal(122, item.Article.ArticleID); Assert.Equal("CARR1", item.CarrierNum); Assert.Equal(5, item.Quantity); });

            var model = new RemoveCarrier()
            {
                PositionID = POSITION_ID
            };

            var proc = new RemoveCarrierProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success, res.ErrorText);

            var receiveDoneModel = new ProcedureModels.Receive.ReceiveDone()
            {
                StoMoveID = 525
            };
            new Procedures.Receive.ReceiveDoneProc(USER_ID).DoProcedure(receiveDoneModel);

            res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);

            onStore = new StoreCore.OnStore_Basic.OnStore().OnPosition(POSITION_ID, StoreCore.Interfaces.ResultValidityEnum.Valid);
            Assert.Equal(2, onStore.Count);
            Assert.Collection<StoreCore.Interfaces.OnStoreItemFull>(onStore,
                item => { Assert.Equal(121, item.Article.ArticleID); Assert.Null(item.CarrierNum); Assert.Equal(5, item.Quantity); },
                item => { Assert.Equal(122, item.Article.ArticleID); Assert.Null(item.CarrierNum); Assert.Equal(9, item.Quantity); });
        }
    }
}

﻿using S4.Entities;
using TestHelpers;
using Xunit;

namespace S4.Procedures.Tests
{
    [Collection("StockTakingTests")]
    [Trait("Category", "StockTakingTests")]
    public class StockTakingRemoteItemsFromSnapshotProcTest : ProcTestBase
    {
        public StockTakingRemoteItemsFromSnapshotProcTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void StockTakingCancelTest_Success()
        {
            var model = new ProcedureModels.StockTaking.StockTakingRemoteItemsFromSnapshot()
            {
                StotakID = 7,
                ArtPackID = 1001
            };

            var proc = new StockTaking.StockTakingRemoteItemsFromSnapshotProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            Assert.NotEmpty(res.Items);

            using (var db = GetDB())
            {
                var items = db.Query<StockTakingSnapshot>()
                    .Where(_ => _.StotakID == model.StotakID)
                    .Where(_ => _.ArtPackID == model.ArtPackID)
                    .Where(_ => _.SnapshotClass == StockTakingSnapshot.StockTakingSnapshotClassEnum.Internal)
                    .ToList();
                
                Assert.Equal(items.Count, res.Items.Count);
            }
        }


        [Fact]
        public void StockTakingCancelTest_Error()
        {
            var model = new ProcedureModels.StockTaking.StockTakingRemoteItemsFromSnapshot()
            {
                StotakID = -1,
                ArtPackID = -1
            };

            var proc = new StockTaking.StockTakingRemoteItemsFromSnapshotProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success);
            Assert.Empty(res.Items);
        }
    }
}

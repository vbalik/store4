﻿using System;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Booking;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class BookingListAddItemProcTest : ProcTestBase
    {
        public const int STO_MOVE_LOT_ID = 69;
        public const int PARTNER_ID = 1;
        public const int ADDR_ID = 99;
        public const int REQUIRED_QUANTITY = 1;
        public DateTime BOOKING_TIMEOUT = new DateTime(2055, 1, 1);

        public BookingListAddItemProcTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void BookingAddItem_Success()
        {
            var model = new BookingAddItem()
            {
                StoMoveLotID = STO_MOVE_LOT_ID,
                PartnerID = PARTNER_ID,
                PartAddrID = ADDR_ID,
                RequiredQuantity = REQUIRED_QUANTITY,
                BookingTimeout = BOOKING_TIMEOUT,
            };

            var proc = new Booking.BookingAddItemProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success);


            using (var db = GetDB())
            {
                var booking = db.FirstOrDefault<Entities.Booking>("where StoMoveLotID = @0 AND RequiredQuantity = @1", model.StoMoveLotID, model.RequiredQuantity);
                Assert.NotNull(booking);

                Assert.Equal(model.StoMoveLotID, booking.StoMoveLotID);
                Assert.Equal(model.PartnerID, booking.PartnerID);
                Assert.Equal(model.RequiredQuantity, booking.RequiredQuantity);
                Assert.Equal(model.BookingTimeout, booking.BookingTimeout);
                Assert.Equal(Entities.Booking.VALID, booking.BookingStatus);
            }
        }

        [Fact]
        public void BookingItemExist_Error()
        {
            var model = new BookingAddItem()
            {
                StoMoveLotID = STO_MOVE_LOT_ID,
                PartnerID = PARTNER_ID,
                PartAddrID = ADDR_ID,
                RequiredQuantity = REQUIRED_QUANTITY,
                BookingTimeout = new DateTime(2020, 1, 1),
            };

            var proc = new Booking.BookingAddItemProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.False(res.Success);
            Assert.Contains("Platnost rezervace musí být větší než dnes", res.ErrorText);
        }
    }
}

﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;

namespace S4.Procedures.Tests
{
    [Collection("StockTakingTests")]
    [Trait("Category", "StockTakingTests")]
    public class StockTakingItemsFromSnapshotTest : ProcTestBase
    {

        private int STOTAK_ID = 2;

        public StockTakingItemsFromSnapshotTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void StockTakingItemsFromSnapshotTest_Success()
        {
            var user = "USER2";
            var model = new ProcedureModels.StockTaking.StockTakingItemsFromSnapshot()
            {
                StotakID = STOTAK_ID
            };

            using (var db = GetDB())
            {
                // get items
                var items = db.Fetch<StockTakingItem>("where stotakID = @0", model.StotakID);
                Assert.True(items.Count == 2);
                
                var proc = new StockTaking.StockTakingItemsFromSnapshotProc(user);
                var res = proc.DoProcedure(model);

                Assert.True(res.Success, res.ErrorText);

                var snapshots = db.Fetch<Entities.StockTakingSnapshot>("where stotakID = @0 AND snapshotClass = @1", model.StotakID, (byte)StockTakingSnapshot.StockTakingSnapshotClassEnum.Internal).OrderBy(i => i.StotakSnapID).ToList();
                var items2 = db.Fetch<StockTakingItem>("where stotakID = @0", model.StotakID).OrderBy(i => i.StotakItemID).ToList();
                Assert.True(items2.Count == snapshots.Count);
                for (int pos = 0; pos < snapshots.Count; pos++)
                {
                    Assert.Equal(items2[pos].ArtPackID, snapshots[pos].ArtPackID);
                    Assert.Equal(items2[pos].BatchNum, snapshots[pos].BatchNum);
                    Assert.Equal(items2[pos].CarrierNum, snapshots[pos].CarrierNum);
                    Assert.Equal(items2[pos].ExpirationDate, snapshots[pos].ExpirationDate);
                    Assert.Equal(items2[pos].Quantity, snapshots[pos].Quantity);
                }

                // history
                var hist = db.Fetch<StockTakingHistory>(new NPoco.Sql().Where("stotakID = @0", model.StotakID));
                Assert.True(hist.Count > 0);
                var hist2 = hist.OrderByDescending(h => h.StotakHistID).First();
                Assert.Equal(STOTAK_ID, hist2.StotakID);
                Assert.Equal(StockTakingHistory.STOCKTAKING_EVENT_TAKEN, hist2.EventCode.Trim());
                Assert.Equal(user, hist2.EntryUserID.Trim());
                Assert.True(hist2.EntryDateTime <= DateTime.Now);

                var posList = db.Fetch<StockTakingPosition>("where stotakID = @0 AND checkCounter = 11", model.StotakID);
                Assert.True(posList != null);
                Assert.True(posList.Count == 3);
                
            }
            
        }

        [Fact]
        public void StockTakingItemsFromSnapshotTest_Error()
        {
            var model = new ProcedureModels.StockTaking.StockTakingItemsFromSnapshot()
            {
                StotakID = -1
            };

            var proc = new StockTaking.StockTakingItemsFromSnapshotProc("USER2");
            var res = proc.DoProcedure(model);

            Assert.False(res.Success);
            Assert.Equal("StockTaking nebylo nalezeno; StotakID: -1", res.ErrorText);
        }

        [Fact]
        public void StockTakingItemsFromSnapshotTest_Error2()
        {
            var model = new ProcedureModels.StockTaking.StockTakingItemsFromSnapshot()
            {
                StotakID = -1
            };

            var proc = new StockTaking.StockTakingItemsFromSnapshotProc("USER3");
            var res = proc.DoProcedure(model);

            Assert.False(res.Success);
            Assert.Equal("Nemáte právo na provedení akce", res.ErrorText);
        }
    }
}

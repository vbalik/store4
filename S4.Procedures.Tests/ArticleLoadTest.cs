﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Load;
using S4.Entities.Helpers;
using S4.Entities.Helpers.Xml.Article;
using S4.Procedures.Load.Helper;
using S4.Entities.XtraData;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class ArticleLoadTest : ProcTestBase
    {
        private const string ARTICLESOURCE_ID = "HG8H000101";
        private const string ARTICLESOURCEPACKING_ID = "HYFJ000101";
        
        public ArticleLoadTest(DatabaseFixture fixture) : base(fixture) 
        { }


        [Fact]
        public void ArticleLoadTest_Success()
        {
            ArticleS3 articleS3 = null;
            var xml = XmlHelper.GetStringFromXMLFile(@"..\..\..\XmlTestData\Article.xml");
            System.Exception exception = null;
            var load = ArticleS3.Serializer.Deserialize(xml, out articleS3, out exception);

            Assert.True(load);

            var model = new ArticleLoad()
            {
                ArticleS3 = articleS3,
                Xml = xml,
                Remark = "Article.xml"
            };

            var proc = new Load.ArticleLoadProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            
            using (var db = GetDB())
            {
                var article = db.Fetch<Article>("where source_id = @0", ARTICLESOURCE_ID);
                Assert.True(article.Count == 1);
                //packing
                var articlePack = db.Fetch<ArticlePacking>("where source_id = @0", ARTICLESOURCEPACKING_ID);
                Assert.True(articlePack.Count == 1);
                Assert.True((int)article[0].SpecFeatures == 15);

                // xtra
                var xtraDataSaved = new XtraDataProxy<Entities.ArticleXtra>(Article.XtraDataDict);
                xtraDataSaved.Load(db, new NPoco.Sql().Where("ArticleID = @0", article[0].ArticleID));

                Assert.Equal(articleS3.Adr.UnKod, xtraDataSaved[Article.XTRA_UN_KOD]);
                Assert.Equal(articleS3.Adr.ObalovaSkupina, xtraDataSaved[Article.XTRA_OBALOVA_SKUPINA]);
                Assert.Equal(articleS3.Adr.OhrozujeProstredi, xtraDataSaved[Article.XTRA_OHROZUJE_PROSTREDI]);
                Assert.Equal(articleS3.Adr.BezpecnostiZnacka, xtraDataSaved[Article.XTRA_BEZPECNOSTI_ZNACKA]);
                Assert.Equal(articleS3.Adr.DruhObalu, xtraDataSaved[Article.XTRA_DRUH_OBALU]);
            }

            //check change data
            ArticleS3 articleS3_2 = null;
            exception = null;
            var xml2 = XmlHelper.GetStringFromXMLFile(@"..\..\..\XmlTestData\ArticleChange.xml");
            var load2 = ArticleS3.Serializer.Deserialize(xml2, out articleS3_2, out exception);

            Assert.True(load2);

            var modelChange = new ArticleLoad()
            {
                ArticleS3 = articleS3_2,
                Xml = xml2,
                Remark = "ArticleChange.xml"
            };

            var procChange = new Load.ArticleLoadProc(USER_ID);
            var resChange = proc.DoProcedure(modelChange);

            Assert.True(resChange.Success, resChange.ErrorText);

            using (var db = GetDB())
            {
                var article = db.First<Article>("where source_id = @0", ARTICLESOURCE_ID);
                Assert.True(article.UseBatch == true);
                Assert.True(article.UseExpiration == true);
                Assert.True((int)article.SpecFeatures == 2);
                var test = "Test";
                Assert.Equal(article.ArticleDesc, test);
                //packing
                var articlePack = db.First<ArticlePacking>("where source_id = @0", ARTICLESOURCEPACKING_ID);
                Assert.Equal(articlePack.PackDesc, test);
                Assert.Equal(articlePack.PackDesc, test);

                // xtra
                var xtraDataSaved = new XtraDataProxy<Entities.ArticleXtra>(Article.XtraDataDict);
                xtraDataSaved.Load(db, new NPoco.Sql().Where("ArticleID = @0", article.ArticleID));

                Assert.Equal(articleS3_2.Adr.UnKod, xtraDataSaved[Article.XTRA_UN_KOD]);
                Assert.Equal(articleS3_2.Adr.ObalovaSkupina, xtraDataSaved[Article.XTRA_OBALOVA_SKUPINA]);
                Assert.Equal(articleS3_2.Adr.OhrozujeProstredi, xtraDataSaved[Article.XTRA_OHROZUJE_PROSTREDI]);
                Assert.Equal(articleS3_2.Adr.BezpecnostiZnacka, xtraDataSaved[Article.XTRA_BEZPECNOSTI_ZNACKA]);
                Assert.Equal(articleS3_2.Adr.DruhObalu, xtraDataSaved[Article.XTRA_DRUH_OBALU]);
            }
        }
        
        [Fact]
        public void ArticleLoadTestExtraDataADREmpty_Success()
        {
            ArticleS3 articleS3 = null;
            var xml = XmlHelper.GetStringFromXMLFile(@"..\..\..\XmlTestData\ArticleXtraDataADREmptyValid.xml");
            System.Exception exception = null;
            var load = ArticleS3.Serializer.Deserialize(xml, out articleS3, out exception);

            Assert.True(load);

            var model = new ArticleLoad()
            {
                ArticleS3 = articleS3,
                Xml = xml,
                Remark = "ArticleXtraDataADREmptyValid.xml"
            };

            var proc = new Load.ArticleLoadProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            

        }

        [Fact]
        public void ArticleLoadDisableTest_Success()
        {
            ArticleS3 articleS3 = null;
            var xml = XmlHelper.GetStringFromXMLFile(@"..\..\..\XmlTestData\ArticleDisabled.xml");
            System.Exception exception = null;
            var load = ArticleS3.Serializer.Deserialize(xml, out articleS3, out exception);

            Assert.True(load);
            var model = new ArticleLoad()
            {
                ArticleS3 = articleS3,
                Xml = xml,
                Remark = "ArticleDisabled.xml"
            };

            var proc = new Load.ArticleLoadProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var article = db.SingleById<Article>(109);
                
                Assert.True(article != null);
                Assert.True(article.ArticleStatus == Article.AR_STATUS_DISABLED);
            }



        }

        [Fact]
        public void GetSpecFeatures()
        {
            var helper = new Load.Helper.LoadArticleAndPackings();

            var items = new ArticleItems();
            Assert.True(helper.GetSpecFeatures(items, Article.SpecFeaturesEnum.Any) == Article.SpecFeaturesEnum.Any);

            items = new ArticleItems() { ArticleCooled = true };
            Assert.True(helper.GetSpecFeatures(items, Article.SpecFeaturesEnum.Any) == Article.SpecFeaturesEnum.Cooled);

            items = new ArticleItems() { ArticleCooled = true, ArticleMedicines = true };
            Assert.True((int)helper.GetSpecFeatures(items, Article.SpecFeaturesEnum.Any) == 3);
            Assert.True((int)helper.GetSpecFeatures(items, Article.SpecFeaturesEnum.CheckOnReceive) == 11);

            items = new ArticleItems() { ArticleCooled = true };
            Assert.True((int)helper.GetSpecFeatures(items, (Article.SpecFeaturesEnum)15) == 15); 

            items = new ArticleItems() { ArticleCooled = false };
            Assert.True((int)helper.GetSpecFeatures(items, (Article.SpecFeaturesEnum)15) == 14);

            items = new ArticleItems() { ArticleCooled = true, ArticleCytostatics = true, ArticleMedicines = true, ArticleCheckOnReceive = true };
            Assert.True((int)helper.GetSpecFeatures(items, Article.SpecFeaturesEnum.Any) == 15);

        }



    }
}

﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;

namespace S4.Procedures.Tests
{
    [Collection("StockTakingTests")]
    [Trait("Category", "StockTakingTests")]
    public class StockTakingChangeItemTest : ProcTestBase
    {

        int STOTAK_ITEM_ID = 9;

        public StockTakingChangeItemTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void StockTakingChangeItemTest_Success()
        {
            var model = new ProcedureModels.StockTaking.StockTakingChangeItem()
            {
                StotakItemID = STOTAK_ITEM_ID,
                ArtPackID = 1002,
                BatchNum = "XXX",
                CarrierNum = "YYY",
                ExpirationDate = new DateTime(2020,1,1),
                PositionID = 2,
                Quantity = 10
            };

            using (var db = GetDB())
            {
               
                var proc = new StockTaking.StockTakingChangeItemProc(USER_ID);
                var res = proc.DoProcedure(model);

                Assert.True(res.Success, res.ErrorText);

                // get item
                var item = db.FirstOrDefault<StockTakingItem>("where stotakitemid = @0", STOTAK_ITEM_ID);
                Assert.True(item != null);
                Assert.True(item.StotakItemID == 9);
                Assert.True(item.ArtPackID == 1002);
                Assert.True(item.BatchNum == "XXX");
                Assert.True(item.CarrierNum == "YYY");
                Assert.True(item.ExpirationDate == new DateTime(2020, 1, 1));
                Assert.True(item.PositionID == 2);
                Assert.True(item.Quantity == 10);


                //his
                var hist = db.Fetch<StockTakingHistory>(new NPoco.Sql().Where("stotakID = @0", item.StotakID));
                Assert.True(hist.Count > 0);
                var hist2 = hist.OrderByDescending(h => h.StotakHistID).First();
                Assert.Equal(item.StotakID, hist2.StotakID);
                Assert.Equal(StockTakingHistory.STOCKTAKING_EVENT_ITEM_CHANGED, hist2.EventCode.Trim());
                Assert.Equal(USER_ID, hist2.EntryUserID.Trim());
                Assert.True(hist2.EntryDateTime <= DateTime.Now);
                Assert.Equal(model.ArtPackID, hist2.ArtPackID);
                Assert.Equal(model.BatchNum, hist2.BatchNum);
                Assert.Equal(model.CarrierNum, hist2.CarrierNum);
                Assert.Equal(model.ExpirationDate, hist2.ExpirationDate);
                Assert.Equal(model.PositionID, hist2.PositionID);
                Assert.Equal(model.Quantity, hist2.Quantity);

            }
            
        }

        [Fact]
        public void StockTakingChangeItemTest_Error()
        {
            var model = new ProcedureModels.StockTaking.StockTakingChangeItem()
            {
                StotakItemID = -1,
                ArtPackID = -1,
                BatchNum = "x",
                CarrierNum = "x",
                ExpirationDate = DateTime.Now,
                PositionID = -1,
                Quantity = 1
            };

            var proc = new StockTaking.StockTakingChangeItemProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.False(res.Success);
            Assert.Equal("StockTakingItem nebyl nalezen; StotakItemID: -1", res.ErrorText);
        }


    }
}

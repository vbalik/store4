﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Admin;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class DirectionCancelProcTest : ProcTestBase
    {
        const int DIRECTION_ID = 1;
        const string NOT_SUPERUSER_ID = "Test1";
       

        public DirectionCancelProcTest(DatabaseFixture fixture) : base(fixture) 
        { }


        [Fact]
        public void DirectionCancelProcTest_Success()
        {
            var model = new DirectionCancel()
            {
                DirectionID = DIRECTION_ID,
                CancelReason = "Test"
            };

            var proc = new Admin.DirectionCancelProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success == true, res.ErrorText);
            
            using (var db = GetDB())
            {
                var direction = db.FirstOrDefault<Direction>("where directionid = @0", DIRECTION_ID);
                Assert.Equal(Direction.DR_STATUS_CANCEL, direction.DocStatus);

                var hist = db.Fetch<DirectionHistory>(new NPoco.Sql().Where("directionid = @0", model.DirectionID));
                Assert.True(hist.Count > 0);
                var hist2 = hist.OrderByDescending(h => h.DirectionID).First();
                Assert.Equal(DIRECTION_ID, hist2.DirectionID);
                Assert.Equal(Direction.DR_STATUS_CANCEL, hist2.EventCode.Trim());
                Assert.Equal(model.CancelReason, hist2.EventData);
                Assert.Equal(USER_ID, hist2.EntryUserID.Trim());
                Assert.True(hist2.EntryDateTime < DateTime.Now);

                var msg = db.Fetch<InternMessage>(new NPoco.Sql().Where("msgclass = @0", InternMessage.DR_CANCEL));
                Assert.True(msg.Count > 0);
                var msg2 = msg.OrderByDescending(h => h.IntMessID).First();
                Assert.Equal(InternMessage.MessageSeverityEnum.Low, msg2.MsgSeverity);
                Assert.Equal("Doklad 'DL/04/1' zrušen", msg2.MsgHeader);
                Assert.True(msg2.MsgDateTime < DateTime.Now);

                var msgTextDictionary = Newtonsoft.Json.JsonConvert.DeserializeObject<System.Collections.Generic.Dictionary<string, dynamic>>(msg2.MsgText);
                Assert.Equal(msgTextDictionary["directionID"], 1);
                Assert.Equal(msgTextDictionary["cancelReason"], "Test");

                var moves = db.Fetch<StoreMove>(new NPoco.Sql().Where("parent_directionID = @0", model.DirectionID));
                Assert.All(moves, i => Assert.Equal(StoreMove.MO_STATUS_CANCEL, i.DocStatus));
            }
            
        }


        [Fact]
        public void DirectionCancelProcTest_Error()
        {
            var model = new DirectionCancel()
            {
                DirectionID = -1,
                CancelReason = null
            };

            var proc = new Admin.DirectionCancelProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
        }

        [Fact]
        public void DirectionCancelProcTest_NotSuperUserError()
        {
            var model = new DirectionCancel()
            {
                DirectionID = DIRECTION_ID,
                CancelReason = "I am not creator and not super user"
            };

            var proc = new Admin.DirectionCancelProc(NOT_SUPERUSER_ID);
            var res = proc.DoProcedure(model);

            Assert.False(res.Success);

            Assert.Contains("Uživatel není tvůrce dokladu; DirectionID:", res.ErrorText);
        }
    }
}

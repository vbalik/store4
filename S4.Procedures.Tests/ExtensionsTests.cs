﻿using S4.Entities;
using S4.ProcedureModels.Extensions;
using System;
using System.Linq;
using TestHelpers;
using Xunit;

namespace S4.Procedures.Tests
{
    public class ExtensionsTests
    {

        [Fact]
        public void DirectionExtensions_DocInfo()
        {
            var dir = new Direction() { DocNumPrefix = "DL", DocNumber = 1234 };
            Assert.Equal("DL /  / 1234", dir.DocInfo());
        }        
    }
}

﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.Procedures.Admin;
using S4.ProcedureModels.Admin;
using System.Collections.Generic;

namespace S4.Procedures.Tests
{
    [Collection("ReceiveTests")]
    [Trait("Category", "ReceiveTests")]
    public class DirectionReceiveChangeProcTest : ProcTestBase
    {
        public DirectionReceiveChangeProcTest(DatabaseFixture fixture) : base(fixture)
        { }

        [Fact]
        public void DirectionReceiveChangeProcTest_Success()
        {
            var stoMoveID = 528;

            var model = new DirectionItemChange2()
            {
                ArtPackID = 1023,
                DirectionID = 59,
                DirectionItemID = 54,
                ArticleCode = "RECCHANGE125",
                DirectionStoMoveItems = new List<DirectionStoMoveItem>
                {
                    new DirectionStoMoveItem
                    {
                        DocPosition    = 1,
                        Quantity       = 10,
                        BatchNum       = "TestBatch",
                        ExpirationDate = new DateTime(2025, 5, 5),
                        CarrierNum     = "12345678901234567890",
                        StoMoveID      = 528,
                        StoMoveItemID  = 153,
                        StoMoveLotID   = 63
                    }
                }
            };

            var proc = new DirectionItemChange2Proc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // s4_storeMove 1x
                var moves = db.Fetch<StoreMove>(new NPoco.Sql().Where("stoMoveID = @0", stoMoveID));
                Assert.Single(moves);
                Assert.Equal(StoreMove.MO_DOCTYPE_RECEIVE, moves.First().DocType);
                Assert.Equal(StoreMove.MO_STATUS_SAVED_RECEIVE, moves.First().DocStatus);

                // s4_storeMove_items 3x
                var storeMoveItems = db.Fetch<StoreMoveItem>(new NPoco.Sql().Where("stoMoveID = @0", stoMoveID).OrderBy("stoMoveItemID asc"));
                Assert.Equal(3, storeMoveItems.Count);
                // old move
                Assert.Equal(StoreMoveItem.MI_VALIDITY_CANCELED, storeMoveItems[0].ItemValidity);
                Assert.Equal(model.DirectionStoMoveItems[0].StoMoveLotID, storeMoveItems[0].StoMoveLotID);
                Assert.NotEqual(model.DirectionStoMoveItems[0].CarrierNum, storeMoveItems[0].CarrierNum);
                Assert.NotEqual(model.DirectionStoMoveItems[0].Quantity, storeMoveItems[0].Quantity);
                Assert.Equal(1, storeMoveItems[0].DocPosition);
                // new move
                Assert.Equal(StoreMoveItem.MI_VALIDITY_VALID, storeMoveItems.Last().ItemValidity);
                Assert.NotEqual(model.DirectionStoMoveItems[0].StoMoveLotID, storeMoveItems.Last().StoMoveLotID);
                Assert.Equal(model.DirectionStoMoveItems[0].CarrierNum, storeMoveItems.Last().CarrierNum);
                Assert.Equal(model.DirectionStoMoveItems[0].Quantity, storeMoveItems.Last().Quantity);
                Assert.Equal(3, storeMoveItems.Last().DocPosition);


                // s4_storeMove_lots from s4_storeMove_items
                var lot = db.Single<StoreMoveLot>(new NPoco.Sql().Where("stoMoveLotID = @0", storeMoveItems[2].StoMoveLotID));
                Assert.NotNull(lot);
                Assert.Equal(model.ArtPackID, lot.ArtPackID);
                Assert.Equal(model.DirectionStoMoveItems.First().BatchNum, lot.BatchNum);
                Assert.Equal(model.DirectionStoMoveItems.First().ExpirationDate, lot.ExpirationDate);

                // s4_storemove_history 1x
                var moveHistory = db.Fetch<StoreMoveHistory>(new NPoco.Sql().Where("stoMoveID = @0", stoMoveID));
                Assert.Single(moveHistory);
                Assert.Equal(StoreMove.MO_EVENT_ITEM_CHANGED, moveHistory.First().EventCode);

                // s4_direction_items
                var items = db.Fetch<DirectionItem>(new NPoco.Sql().Where("directionID = @0", model.DirectionID));
                Assert.Equal(2, items.Count);
                Assert.Equal(model.ArtPackID, items.First().ArtPackID);
                Assert.Equal(model.ArticleCode, items.First().ArticleCode);
                Assert.Equal(model.DirectionStoMoveItems.First().Quantity, items.First().Quantity);

                // s4_direction_history 1x
                var directionHistory = db.Fetch<DirectionHistory>(new NPoco.Sql().Where("directionID = @0", model.DirectionID));
                Assert.Single(directionHistory);
                Assert.Equal(Direction.DR_EVENT_ITEM_CHANGED, directionHistory.First().EventCode);
            }
        }

        [Fact]
        public void DirectionReceiveChangeProcTest_StoreInMoveError()
        {
            var model = new DirectionItemChange2()
            {
                ArtPackID = 1023,
                DirectionID = 59,
                DirectionItemID = 55,
                ArticleCode = "RECCHANGE125",
                DirectionStoMoveItems = new List<DirectionStoMoveItem>
                {
                    new DirectionStoMoveItem
                    {
                        DocPosition    = 2,
                        Quantity       = 10,
                        BatchNum       = "TestBatch",
                        ExpirationDate = new DateTime(2025, 5, 5),
                        CarrierNum     = "12345678901234567890",
                        StoMoveID      = 529,
                        StoMoveItemID  = 154,
                        StoMoveLotID   = 63
                    }
                }
            };

            var proc = new DirectionItemChange2Proc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal("Položku nelze změnit - byla již naskladněna", res.ErrorText.Trim());
        }

        [Fact]
        public void DirectionReceiveChangeProcTest_WrongStoMoveItemID()
        {
            var model = new DirectionItemChange2()
            {
                ArtPackID = 1023,
                DirectionID = 59,
                DirectionItemID = 54,
                ArticleCode = "RECCHANGE125",
                DirectionStoMoveItems = new List<DirectionStoMoveItem>
                {
                    new DirectionStoMoveItem
                    {
                        DocPosition    = 1,
                        Quantity       = 10,
                        BatchNum       = "TestBatch",
                        ExpirationDate = new DateTime(2025, 5, 5),
                        CarrierNum     = "12345678901234567890",
                        StoMoveID      = 528,
                        StoMoveItemID  = 99999,
                        StoMoveLotID   = 63
                    }
                }
            };

            var proc = new DirectionItemChange2Proc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal("Položku nelze změnit - pohyby nenalezeny", res.ErrorText.Trim());
        }

        [Fact]
        public void DirectionReceiveChangeProcTest_DirectionItemError()
        {
            var model = new DirectionItemChange2()
            {
                ArtPackID = 1023,
                DirectionID = 59,
                DirectionItemID = 999,
                ArticleCode = "RECCHANGE125",
                DirectionStoMoveItems = new List<DirectionStoMoveItem>()
            };

            var proc = new DirectionItemChange2Proc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Contains("DirectionItem nebylo nalezeno; DirectionItemID", res.ErrorText.Trim());
        }
    }
}

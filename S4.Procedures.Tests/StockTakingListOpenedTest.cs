﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;

namespace S4.Procedures.Tests
{
    [Collection("StockTakingTests")]
    [Trait("Category", "StockTakingTests")]
    public class StockTakingListOpenedTest : ProcTestBase
    {

        public StockTakingListOpenedTest(DatabaseFixture fixture) : base(fixture) 
        { }


        [Fact]
        public void StockTakingListOpenedTest_Success()
        {
            var model = new ProcedureModels.StockTaking.StockTakingListOpened();
            var proc = new StockTaking.StockTakingListOpenedProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            Assert.True(res.StockTakingList.Count > 0);
        }        
    }
}

﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.StoreIn;

namespace S4.Procedures.Tests
{
    [Collection("StoreInTests2")]
    [Trait("Category", "StoreInTests2")]
    public class StoreInCheckCarrierProcTest : ProcTestBase
    {
        public StoreInCheckCarrierProcTest(DatabaseFixture fixture) : base(fixture)
        { }

        [Fact]
        public void StoreInCheckCarrierProcTest_Success()
        {
            var model = new StoreInCheckCarrier()
            {
                CarrierNum = "A12345",
                SrcPositionID = 33,
                DirectionID = 24
            };

            var proc = new StoreIn.StoreInCheckCarrierProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            Assert.True(res.ArticleList.Count() > 0);
        }

        [Fact]
        public void StoreInCheckCarrierProcTest_Error()
        {
            var model = new StoreInCheckCarrier();

            var proc = new StoreIn.StoreInCheckCarrierProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal("Parametry procedury nejsou platné DirectionID IsNull , CarrierNum IsNull , SrcPositionID IsNull", res.ErrorText.Trim());
        }
    }
}

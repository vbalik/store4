﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.StoreIn;

namespace S4.Procedures.Tests
{
    [Collection("StoreInTests")]
    [Trait("Category", "StoreInTests")]
    public class StoreInCheckPosTest : ProcTestBase
    {
        public StoreInCheckPosTest(DatabaseFixture fixture) : base(fixture)
        { }

        [Fact]
        public void StoreInCheckPosTest_Success()
        {
            var model = new StoreInCheckPos()
            {
                PositionID = 17,
                ArtPackID = 1011
            };

            var proc = new StoreIn.StoreInCheckPosProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            Assert.Equal(StoreInCheckPos.PosEnabled.Yes, res.Enabled);
        }

        [Fact]
        public void StoreInCheckPosTest_NoStoin()
        {
            var model = new StoreInCheckPos()
            {
                PositionID = 13,
                ArtPackID = 1011
            };

            var proc = new StoreIn.StoreInCheckPosProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success);
            Assert.Equal("Nejde o skladovou pozici.", res.ResReason.Trim());
        }

        [Fact]
        public void StoreInCheckPosTest_Error()
        {
            var model = new StoreInCheckPos();

            var proc = new StoreIn.StoreInCheckPosProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal("Parametry procedury nejsou platné PositionID IsNull , ArtPackID IsNull", res.ErrorText.Trim());
        }
    }
}

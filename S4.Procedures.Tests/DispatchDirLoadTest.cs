﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Load;
using S4.Entities.XtraData;
using S4.Entities.Helpers.Xml.Dispatch;
using S4.Entities.Helpers;
using S4.DAL;
using S4.Entities.Models;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests2")]
    [Trait("Category", "ProcedureTests2")]
    public class DispatchDirLoadTest : ProcTestBase
    {

        private const string CUSTOMERID = "EL10000101";
        private const string ADDRESSID = "S530000101";

        private const string ARTICLEPACKINGID1 = "I08F000101";
        private const string ARTICLEPACKINGID2 = "4L60000101";

        public DispatchDirLoadTest(DatabaseFixture fixture) : base(fixture) 
        { }


        [Fact]
        public void DispatchDirLoadTest_Success()
        {
            DispatchS3 dispatchS3 = null;
            var xml = XmlHelper.GetStringFromXMLFile(@"..\..\..\XmlTestData\Dispatch1.xml");
            System.Exception exception = null;
            var load = DispatchS3.Serializer.Deserialize(xml, out dispatchS3, out exception);

            Assert.True(load);

            var model = new DispatchDirLoad()
            {
                DispatchS3 = dispatchS3,
                Xml = xml
            };

            using (var db = GetDB())
            {
                var partner1 = db.FirstOrDefault<Partner>("where source_id = @0", CUSTOMERID);
                var address1 = db.FirstOrDefault<PartnerAddress>("where source_id = @0", ADDRESSID);

                Assert.True(partner1 == null);
                Assert.True(address1 == null);

            }
            
            var proc = new Load.DispatchDirLoadProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            Assert.True(res.Result == DispatchDirLoad.ResultEnum.Saved);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var directionID = db.FirstOrDefault<Direction>("where docNumPrefix = 'DL' AND docYear = 18 AND docNumber = '92900'").DirectionID;

                var partner2 = db.FirstOrDefault<Partner>("where source_id = @0", CUSTOMERID);
                var address2 = db.FirstOrDefault<PartnerAddress>("where source_id = @0", ADDRESSID);

                Assert.True(partner2 != null);
                Assert.True(address2 != null);

                var artPack = db.Fetch<ArticlePacking>("where source_id in (@0, @1)", ARTICLEPACKINGID1, ARTICLEPACKINGID2);
                Assert.True(artPack.Count == 2);

                var dirItems = db.Fetch<DirectionItem>("where artPackID in (@0, @1)", artPack[0].ArtPackID, artPack[1].ArtPackID);
                Assert.True(dirItems.Count == 2);

                var xtraData = new XtraDataProxy<Entities.DirectionXtra>(Direction.XtraDataDict);
                xtraData.Load(db, new NPoco.Sql().Where("DirectionID = @0", directionID));

                var x = xtraData[Direction.XTRA_DISPATCH_DIRECT_SECTION_LIST, 1];
                Assert.True(x.Equals("VYSK|VYS2"));

                bool x1 = (bool)xtraData[Direction.XTRA_ARTICLE_COOLED, 1];
                Assert.True(x1 == true);

                bool x2 = (bool)xtraData[Direction.XTRA_ARTICLE_MEDICINES, 1];
                Assert.True(x2 == true);

                string x3 = (string)xtraData[Direction.XTRA_MANUF_ICON, 1];
                Assert.True(x3 == "fab fa-apple");

                string x4 = (string)xtraData[Direction.XTRA_MANUF_ICON, 2];
                Assert.True(x4 == "fab fa-apple");

                var alt = xtraData[Direction.XTRA_DISPATCH_ALTER_SECTION, 1];
                Assert.True(alt.Equals("Test1"));

                var cod = (decimal)xtraData[Direction.XTRA_CASH_ON_DELIVERY];
                Assert.Equal(3969, cod);

                var country = xtraData[Direction.XTRA_DELIVERY_COUNTRY];
                Assert.Equal("CZ", country);

                var currency = xtraData[Direction.XTRA_DELIVERY_CURRENCY];
                Assert.Equal("CZK", currency);

                var parcelShop = xtraData[Direction.XTRA_DELIVERY_PARCEL_SHOP];
                Assert.Equal("PSHOP", parcelShop);

                var shipmentinformation = xtraData[Direction.XTRA_DELIVERY_SHIPMENTINFO];
                Assert.Equal("Dorucit do vratnice", shipmentinformation);

                var docBarcode = xtraData[Direction.XTRA_ABRA_DOC_ID];
                Assert.Equal("1234567890", docBarcode);

                string riskClass0 = (string)xtraData[Direction.XTRA_DELIVERY_RISK_CLASS, 1];
                Assert.Equal("I", riskClass0);

                string riskClass1 = (string)xtraData[Direction.XTRA_DELIVERY_RISK_CLASS, 2];
                Assert.True(string.IsNullOrWhiteSpace(riskClass1));

                string udidi0 = (string)xtraData[Direction.XTRA_DELIVERY_UDIDI, 1];
                Assert.Equal("4029577000018U", udidi0);

                string udidi1 = (string)xtraData[Direction.XTRA_DELIVERY_UDIDI, 2];
                Assert.True(string.IsNullOrWhiteSpace(udidi1));

                string receivingPerson = (string) xtraData[Direction.XTRA_DELIVERY_RECEIVING_PERSON];
                Assert.Equal("přijímací osoba X", receivingPerson);

                bool mandatoryDirPrint = (bool) xtraData[Direction.XTRA_MANDATORY_DIRECTION_PRINT];
                Assert.True(mandatoryDirPrint);

                //Invoice
                var invoice = db.FirstOrDefault<Invoice>("where [docStatus] = @0 AND [docNumber] = 1485 AND [docPrefix] = 'FV' AND [docYear] = 18", Invoice.INV_STATUS_LOAD);

                if (invoice == null)
                {
                    Assert.False(invoice == null);
                }
                else
                {
                    var invoiceHis = db.FirstOrDefault<InvoiceHistory>("where[invoiceID] = @0", invoice.InvoiceID);
                    var invoiceXtra = db.FirstOrDefault<InvoiceXtra>("where[invoiceID] = @0", invoice.InvoiceID);
                    Assert.False(invoiceHis == null);
                    Assert.False(invoiceXtra == null);
                }

                // item ADR xtra data
                var firstArticleItem = dispatchS3.Items[0];
                var xtraArticlePacking = db.FirstOrDefault<ArticlePacking>("where [source_id] = @0", firstArticleItem.ArticlePackingID);

                var xtraDataSaved = new XtraDataProxy<ArticleXtra>(Article.XtraDataDict);
                xtraDataSaved.Load(db, new NPoco.Sql().Where("ArticleID = @0", xtraArticlePacking.ArticleID));

                Assert.Equal(firstArticleItem.Extradata.Adr.UnKod, xtraDataSaved[Article.XTRA_UN_KOD]);
                Assert.Equal(firstArticleItem.Extradata.Adr.ObalovaSkupina, xtraDataSaved[Article.XTRA_OBALOVA_SKUPINA]);
                Assert.Equal(firstArticleItem.Extradata.Adr.OhrozujeProstredi, xtraDataSaved[Article.XTRA_OHROZUJE_PROSTREDI]);
                Assert.Equal(firstArticleItem.Extradata.Adr.BezpecnostiZnacka, xtraDataSaved[Article.XTRA_BEZPECNOSTI_ZNACKA]);
                Assert.Equal(firstArticleItem.Extradata.Adr.DruhObalu, xtraDataSaved[Article.XTRA_DRUH_OBALU]);

                var booking = db.FirstOrDefault<Entities.Booking>("where bookingStatus = @0 AND stoMoveLotID = 74 AND partnerID = @1 AND usedInDirectionID = @2", Entities.Booking.VALID, partner2.PartnerID, directionID);
                Assert.True(booking != null);
            }

            var res2 = proc.DoProcedure(model);
            Assert.True(res2.Result == DispatchDirLoad.ResultEnum.AlreadyLoaded);

        }

        [Fact]
        public void DispatchDirLoadTest_BookingError1()
        {
            DispatchS3 dispatchS3 = null;
            var xml = XmlHelper.GetStringFromXMLFile(@"..\..\..\XmlTestData\Dispatch2.xml");
            System.Exception exception = null;
            var load = DispatchS3.Serializer.Deserialize(xml, out dispatchS3, out exception);

            Assert.True(load);

            var model = new DispatchDirLoad()
            {
                DispatchS3 = dispatchS3,
                Xml = xml
            };

            var proc = new Load.DispatchDirLoadProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.False(res.Success, res.ErrorText);
            Assert.True(res.Result == DispatchDirLoad.ResultEnum.Error);
            Assert.True(res.ErrorText == "Nenalezena šarže NEEXISTUJE, rezervace nemůže být provedena!");
        }


    }
}

﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.BarCode;
using System.Reflection;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class BarcodeConfirmProcTest : ProcTestBase
    {
        const int ARTICLE_ID = 102;
        const int ART_PACK_ID = 1003;
        
        public BarcodeConfirmProcTest(DatabaseFixture fixture) : base(fixture) 
        { }


        [Fact]
        public void BarcodeConfirmProcTest_Success()
        {
            var model = new BarcodeConfirm()
            {
                ArtPackID = ART_PACK_ID,
            };

            var proc = new Barcode.BarcodeConfirmProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            using (var db = GetDB())
            {
                var pack = db.SingleById<ArticlePacking>(model.ArtPackID);
                Assert.True(pack.PackStatus == ArticlePacking.AP_STATUS_OK);

                var hist = db.Fetch<ArticleHistory>(new NPoco.Sql().Where("artpackid = @0", model.ArtPackID));
                Assert.True(hist.Count > 0);
                var hist2 = hist.OrderByDescending(h => h.ArticleHistID).First();
                Assert.Equal(ARTICLE_ID, hist2.ArticleID);
                Assert.Equal(Article.AR_EVENT_BARCODE_CONFIRMED, hist2.EventCode.Trim());
                Assert.Null(hist2.EventData);
                Assert.Equal(USER_ID, hist2.EntryUserID.Trim());
                Assert.True(hist2.EntryDateTime <= DateTime.Now);

                var msg = db.Fetch<InternMessage>(new NPoco.Sql().Where("msgclass = @0", Article.AR_EVENT_UPDATE));
                Assert.True(msg.Count > 0);
                var msg2 = msg.OrderByDescending(h => h.IntMessID).First();
                Assert.Equal(InternMessage.MessageSeverityEnum.Low, msg2.MsgSeverity);
                Assert.Equal($"Potvrzení čárového kódu u položky 'X789:Zboží 789'", msg2.MsgHeader);
                Assert.True(msg2.MsgDateTime <= DateTime.Now);
                
                var msgTextDictionary = Newtonsoft.Json.JsonConvert.DeserializeObject<System.Collections.Generic.Dictionary<string, dynamic>>(msg2.MsgText);
                Assert.Equal(msgTextDictionary["artPackID"], 1003);

             
                

            }
        }


        [Fact]
        public void BarcodeSetProcTest_Error()
        {
            var model = new BarcodeConfirm()
            {
                ArtPackID = -1,
            };

            var proc = new Barcode.BarcodeConfirmProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal("Balení nebylo nalezeno; ArtPackID: -1", res.ErrorText);
        }
    }
}

﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using S4.ProcedureModels.Dispatch;
using Xunit;
using S4.Core;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class DispatchUserAssignmentProcTest : ProcTestBase
    {

        public DispatchUserAssignmentProcTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Theory]
        [InlineData("DL/21/64")] //Is direction - directionID = 65;
        [InlineData("FV/21/1")] //Is invoice - directionID = 66;  
        public void DispatchUserAssignmentProcTest_Success(string docInfo)
        {
            var directionID = 65;

            if (docInfo.Equals("FV/21/1"))
                directionID = 66;

            var model = new DispatchUserAssignment()
            {
                DocInfo = docInfo,
                WorkerID = "Test1"
            };

            int directionAssignID = 0;
            using (var db = GetDB())
            {
                var directionAssigns1 = db.Fetch<DirectionAssign>("where directionid = @0", directionID);
                Assert.True(directionAssigns1.Count == 1);
                directionAssignID = directionAssigns1[0].DirectAssignID;
            }

            var proc = new Dispatch.DispatchUserAssignmentProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var direction = db.FirstOrDefault<Direction>("where directionid = @0", directionID);
                Assert.True(direction.DocStatus == Direction.DR_STATUS_RES_IN_PROC);

                var directionAssigns2 = db.Fetch<DirectionAssign>("where directionid = @0", directionID);
                Assert.True(directionAssigns2.Count == 1);
                Assert.True(directionAssigns2[0].DirectAssignID != directionAssignID);

                //test xtraData
                var xtraData = new Entities.XtraData.XtraDataProxy<Entities.DirectionXtra>(Direction.XtraDataDict);
                xtraData.Load(db, new NPoco.Sql().Where("DirectionID = @0", directionID));
                Assert.NotEmpty(xtraData.Data);

                var destPositionID = Singleton<DAL.SettingsSet>.Instance.GetValue_Int32(SettingsDictionary.DISPATCH_DEFAULT_POSITION_ID);

                Assert.Equal(destPositionID.ToString(), xtraData[Direction.XTRA_DISPATCH_DIRECT_SECTION]);

                var hist = db.Fetch<DirectionHistory>(new NPoco.Sql().Where("directionid = @0", directionID));
                Assert.True(hist.Count > 0);
                var hist2 = hist.OrderByDescending(h => h.DirectionID).First();
                Assert.Equal(directionID, hist2.DirectionID);
                Assert.Equal(Direction.DR_STATUS_DISPATCH_WAIT, hist2.EventCode.Trim());
                Assert.Equal("Test1|123", hist2.EventData);
                Assert.Equal(USER_ID, hist2.EntryUserID.Trim());
                Assert.True(hist2.EntryDateTime < DateTime.Now);
            }
        }


        [Fact]
        public void DispatchUserAssignmentProcTest_NoSuccess_1()
        {
            var model = new DispatchUserAssignment()
            {
                DocInfo = "DL/21/66", //Is direction - directionID = 67; 
                WorkerID = "Test1"
            };

            var proc = new Dispatch.DispatchUserAssignmentProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal($"Doklad nemá očekávaný status DR_STATUS_LOAD nebo DR_STATUS_DISPATCH_WAIT nebo DR_STATUS_RES_NOT_FOUND; DirectionID: 67", res.ErrorText);

        }


        [Fact]
        public void DispatchUserAssignmentProcTest_Error()
        {
            var model = new DispatchUserAssignment()
            {
                DocInfo = "-1",
                WorkerID = "-1"
            };

            var proc = new Dispatch.DispatchUserAssignmentProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal("Nebyly nalezeny žádné doklady ke zpracování!", res.ErrorText);
        }

    }
}

﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Load;
using S4.Entities.Helpers;
using S4.Entities.Helpers.Xml.Receive;

namespace S4.Procedures.Tests
{
    [Collection("ReceiveTests")]
    [Trait("Category", "ReceiveTests")]
    public class ReceiveDirLoadTest : ProcTestBase
    {
        private const string packingSource_id1 = "TBZI000101";
        private const string packingSource_id2 = "YAQF000101";
        private const string packingSource_id3 = "L9X2000101";
        
        public ReceiveDirLoadTest(DatabaseFixture fixture) : base(fixture) 
        { }


        [Fact]
        public void ReceiveDirLoadTest_Success()
        {
            Entities.Helpers.Xml.Receive.ReceiveS3 receiveS3 = null;
            var xml = XmlHelper.GetStringFromXMLFile(@"..\..\..\XmlTestData\Receive.xml");
            System.Exception exception = null;
            var load = ReceiveS3.Serializer.Deserialize(xml, out receiveS3, out exception);

            Assert.True(load);
            var model = new ReceiveDirLoad()
            {
                ReceiveS3 = receiveS3,
                Xml = xml,
                Remark = "Receive.xml"
        };

            var proc = new Load.ReceiveDirLoadProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            Assert.True(res.Result != ReceiveDirLoad.ResultEnum.AlreadyLoaded, res.ErrorText);

            using (var db = GetDB())
            {
                var newPackingList = db.Fetch<ArticlePacking>("where source_id in (@0, @1, @2)", packingSource_id1, packingSource_id2, packingSource_id3);
                Assert.True(newPackingList.Count == 3);
            }
            
            res = proc.DoProcedure(model);
            Assert.True(res.Result == ReceiveDirLoad.ResultEnum.AlreadyLoaded, res.ErrorText);


        }


        
              
    }
}

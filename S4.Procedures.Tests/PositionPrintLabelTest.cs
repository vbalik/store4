﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Pos;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class PositionPrintLabelTest : ProcTestBase
    {
        const int POSITION_ID = 1;


        public PositionPrintLabelTest(DatabaseFixture fixture) : base(fixture) 
        { }


        [Fact]
        public void PositionPrintLabelTest_Success()
        {
            var model = new PositionPrintLabel()
            {
                PositionID = POSITION_ID,
                PrinterLocationID = 2,
                ReportID = 4
            };
            var proc = new Pos.PositionPrintLabelProc(USER_ID) { IsTesting = true };
            var res = proc.DoProcedure(model);
            Assert.True(res.Success);
            Assert.Equal($"PRINT label {POSITION_ID}", proc.RenderResult);
        }


        [Fact]
        public void PositionPrintLabelTest_Error()
        {
            var model = new PositionPrintLabel()
            {
                PositionID = -1
            };

            var proc = new Pos.PositionPrintLabelProc(USER_ID) { IsTesting = true };
            var res = proc.DoProcedure(model);

            Assert.False(res.Success);
        }

    }
}

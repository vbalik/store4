﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Delivery;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class GetDirectionsForManifestProcTest : ProcTestBase
    {
        public GetDirectionsForManifestProcTest(DatabaseFixture fixture) : base(fixture)
        { }

        [Fact]
        public void GetDirectionsForManifestProc_Error()
        {
            var getDirectionsForManifest = new GetDirectionsForManifest();
            getDirectionsForManifest = new Procedures.Delivery.GetDirectionsForManifestProc(USER_ID).DoProcedure(getDirectionsForManifest);
            Assert.False(getDirectionsForManifest.Success);
            Assert.Equal("Parametry procedury nejsou platné DeliveryProvider IsNull ", getDirectionsForManifest.ErrorText);
        }

        [Fact]
        public void GetDirectionsForManifestProc_Success()
        {
            var getDirectionsForManifest = new GetDirectionsForManifest() { DeliveryProvider = DeliveryDirection.DeliveryProviderEnum.DPD };
            getDirectionsForManifest = new Procedures.Delivery.GetDirectionsForManifestProc(USER_ID).DoProcedure(getDirectionsForManifest);
            Assert.True(getDirectionsForManifest.Success);
            Assert.Collection(getDirectionsForManifest.Directions, item => Assert.Equal(41, item));
        }
    }
}

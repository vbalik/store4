﻿using System;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Booking;
using System.Linq;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class BookingListAvailableLotsProcTest : ProcTestBase
    {
        public BookingListAvailableLotsProcTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void GetAvailableListNoReservation_Success()
        {
            const int ART_PACK_ID = 1027;
            const string BATCH = "BATCH71";
            var date = new DateTime(2022, 1, 1);

            var model = new BookingListAvailableLots()
            {
                ArtPackID = ART_PACK_ID
            };

            var proc = new Booking.BookingListAvailableLotsProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            Assert.NotNull(res.StoreMoveLots);

            Assert.Single(res.StoreMoveLots);

            var item = res.StoreMoveLots.Single(_ => _.BatchNum == BATCH);

            Assert.Equal(ART_PACK_ID, item.ArtPackID);
            Assert.Equal(BATCH, item.BatchNum);
            Assert.True(item.ExpirationDate > date);

            Assert.Equal(1, item.Quantity);
        }

        [Fact]
        public void GetAvailableListFilterReservations_Success()
        {
            const int ART_PACK_ID = 1028;
            const string BATCH = "BATCH72";
            var date = new DateTime(2022, 1, 1);

            var model = new BookingListAvailableLots()
            {
                ArtPackID = ART_PACK_ID
            };

            var proc = new Booking.BookingListAvailableLotsProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            Assert.NotNull(res.StoreMoveLots);

            Assert.Empty(res.StoreMoveLots);            
        }
    }
}

﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Admin;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class DirectionDeleteProcTest : ProcTestBase
    {
        const int DIRECTION_ID = 2;


        public DirectionDeleteProcTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void DirectionDeleteProcTest_Success()
        {
            var model = new DirectionDelete()
            {
                DirectionID = DIRECTION_ID
            };

            var proc = new Admin.DirectionDeleteProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);

            using (var db = GetDB())
            {
                var direction = db.FirstOrDefault<Direction>("where directionid = @0", DIRECTION_ID);
                Assert.True(direction == null);

                var msg = db.Fetch<InternMessage>(new NPoco.Sql().Where("msgclass = @0", InternMessage.DR_DELETE));
                Assert.True(msg.Count > 0);
                var msg2 = msg.OrderByDescending(h => h.IntMessID).First();
                Assert.Equal(InternMessage.MessageSeverityEnum.Low, msg2.MsgSeverity);
                Assert.Equal("Doklad smazán 'DL/04/2'", msg2.MsgHeader);
                Assert.True(msg2.MsgDateTime < DateTime.Now);

                var msgTextDictionary = Newtonsoft.Json.JsonConvert.DeserializeObject<System.Collections.Generic.Dictionary<string, dynamic>>(msg2.MsgText);
                Assert.Equal(msgTextDictionary["directionID"], 2);
            }

        }


        [Fact]
        public void DirectionDeleteProcTest_Error()
        {
            var model = new DirectionDelete()
            {
                DirectionID = -1
            };

            var proc = new Admin.DirectionDeleteProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
        }
    }
}

﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Logistics;
using S4.Procedures.Logistics;
using System.Collections.Generic;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests2")]
    [Trait("Category", "ProcedureTests2")]
    public class LogisticsImportReceiveProcTest : ProcTestBase
    {
        public LogisticsImportReceiveProcTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void LogisticsImportReceiveProcTest_Success()
        {
            var model = new LogisticsImportReceive()
            {
                Items = new List<LogisticsImportReceive.LogisticsImportReceiveItem>() { 
                    new LogisticsImportReceive.LogisticsImportReceiveItem() { ArtPackID = 1011, Quantity = 1 },
                    new LogisticsImportReceive.LogisticsImportReceiveItem() { ArtPackID = 1012, Quantity = 10 },
                },
                PartnerRef = "aaaa-44444-bbbb-8888",
                ReceivePositionID = 114,
                DestPositionID = 117,
                WorkerID = "WRK1"
            };

            var proc = new LogisticsImportReceiveProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // direction
                var directionIn = db.FirstOrDefault<Direction>("where partnerRef = @0", model.PartnerRef);
                Assert.NotNull(directionIn);
                Assert.True(directionIn.DocStatus == Direction.DR_STATUS_STOIN_COMPLET);

                var histIn = db.Fetch<DirectionHistory>(new NPoco.Sql().Where("directionid = @0", directionIn.DirectionID));
                Assert.Single(histIn);
                Assert.Equal(Direction.DR_EVENT_IMPORT_RECEIVE, histIn[0].EventCode);
                Assert.Equal(model.PartnerRef, histIn[0].EventData);

                var directionInItems = db.Fetch<DirectionItem>("where directionid = @0", directionIn.DirectionID);
                Assert.Equal(model.Items.Count, directionInItems.Count);
                var items2 = directionInItems.Select(i => new LogisticsImportReceive.LogisticsImportReceiveItem() { ArtPackID = i.ArtPackID, Quantity = i.Quantity }).ToList();
                TestItems(model.Items, items2);

                var directionAssigns = db.Fetch<DirectionAssign>("where directionid = @0 AND jobID = @1", directionIn.DirectionID, DirectionAssign.JOB_ID_STORE_IN);
                Assert.NotNull(directionAssigns);


                // store move - receive
                var stMoveReceive = db.FirstOrDefault<StoreMove>("where parent_directionID = @0 AND docType = @1", directionIn.DirectionID, StoreMove.MO_DOCTYPE_RECEIVE);
                Assert.NotNull(stMoveReceive);
                var stMoveReceiveHist = db.FirstOrDefault<StoreMoveHistory>(new NPoco.Sql().Where("stoMoveID = @0", stMoveReceive.StoMoveID));
                Assert.NotNull(stMoveReceiveHist);
                Assert.Equal(StoreMove.MO_STATUS_SAVED_RECEIVE, stMoveReceiveHist.EventCode);

                TestItems(db, model.Items, stMoveReceive);

                // store move - stoin
                var stMoveStoIn = db.FirstOrDefault<StoreMove>("where parent_directionID = @0 AND docType = @1", directionIn.DirectionID, StoreMove.MO_DOCTYPE_STORE_IN);
                Assert.NotNull(stMoveStoIn);
                var stMoveStoInHist = db.FirstOrDefault<StoreMoveHistory>(new NPoco.Sql().Where("stoMoveID = @0", stMoveStoIn.StoMoveID));
                Assert.NotNull(stMoveStoInHist);
                Assert.Equal(StoreMove.MO_STATUS_NEW_STOIN, stMoveStoInHist.EventCode);

                TestItems(db, model.Items, stMoveStoIn);

                // test onStore result
                var onPosition = new StoreCore.OnStore_Basic.OnStore().OnPosition(model.DestPositionID, StoreCore.Interfaces.ResultValidityEnum.Valid);
                var items3 = onPosition.Select(i => new LogisticsImportReceive.LogisticsImportReceiveItem() { ArtPackID = i.ArtPackID, Quantity = i.Quantity }).ToList();
                TestItems(model.Items, items3);
            }
        }

        private void TestItems(NPoco.IDatabase db, List<LogisticsImportReceive.LogisticsImportReceiveItem> items, StoreMove stMove)
        {
            var stMoveItems = db.Fetch<StoreMoveItem>(new NPoco.Sql().Where("stoMoveID = @0", stMove.StoMoveID));
            var items2 = items.Select(i => new LogisticsImportReceive.LogisticsImportReceiveItem() { ArtPackID = i.ArtPackID, Quantity = i.Quantity }).ToList();
            TestItems(items, items2);
        }

        private void TestItems(List<LogisticsImportReceive.LogisticsImportReceiveItem> items, List<LogisticsImportReceive.LogisticsImportReceiveItem> items2)
        {
            Assert.Empty(items.Except(items2, new ItemComparer()));
            Assert.Empty(items2.Except(items, new ItemComparer()));
        }

        private class ItemComparer : IEqualityComparer<LogisticsImportReceive.LogisticsImportReceiveItem>
        {
            public bool Equals(LogisticsImportReceive.LogisticsImportReceiveItem x, LogisticsImportReceive.LogisticsImportReceiveItem y)
            {
                return (x.ArtPackID == y.ArtPackID && x.Quantity == y.Quantity);
            }

            public int GetHashCode(LogisticsImportReceive.LogisticsImportReceiveItem obj)
            {
                return obj.ArtPackID.GetHashCode() + obj.Quantity.GetHashCode();
            }
        }
    }
}

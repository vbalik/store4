﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.Procedures.Delivery;
using S4.ProcedureModels.Delivery;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class DirectionSetShipmentFailedStatusProcTest : ProcTestBase
    {
        private static int DIRECTION_ID = 57;
        private static string TEXT = "error";

        public DirectionSetShipmentFailedStatusProcTest(DatabaseFixture fixture) : base(fixture) 
        { }


        [Fact]
        public void DirectionSetShipmentFailedStatusProcTest_Success()
        {
            var model = new DirectionSetShipmentFailedStatus
            {
                DirectionID = DIRECTION_ID,
                Text = TEXT
            };

            var res = new DirectionSetShipmentFailedStatusProc(USER_ID).DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);

            using (var db = GetDB())
            {
                var direction = db.Fetch<Direction>(new NPoco.Sql().Where("directionID = @0", DIRECTION_ID));
                Assert.Equal(Direction.DR_STATUS_DELIVERY_SHIPMENT_FAILED, direction.First().DocStatus);

                var hist = db.Fetch<DirectionHistory>(new NPoco.Sql().Where("directionID = @0", DIRECTION_ID));
                Assert.True(hist.Count > 0);
                Assert.Equal(hist.First().EventCode, Direction.DR_STATUS_DELIVERY_SHIPMENT_FAILED);
                Assert.Equal(hist.First().EventData, TEXT);

                var internMessages = db.Fetch<InternMessage>(new NPoco.Sql().Where("msgClass = @0 AND msgHeader LIKE @1", InternMessage.DR_STATUS_DELIVERY_SHIPMENT_FAILED, $"%{direction.First().DocNumber}%"));
                Assert.True(internMessages.Count > 0);
                Assert.Contains("Chyba zásilku nelze odeslat", internMessages.First().MsgHeader);
            }
        }

        [Fact]
        public void DirectionSetShipmentFailedStatusProcTest_Error()
        {
            var model = new DirectionSetShipmentFailedStatus
            {
                DirectionID = 0,
                Text = string.Empty
            };

            var res = new DirectionSetShipmentFailedStatusProc(USER_ID).DoProcedure(model);

            Assert.False(res.Success);
            Assert.Contains("Parametry procedury nejsou platné", res.ErrorText);
        }
    }
}

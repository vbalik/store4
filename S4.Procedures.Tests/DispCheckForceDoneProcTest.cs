﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.DispCheck;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class DispCheckForceDoneProcTest : ProcTestBase
    {
        const int DIRECTION_ID = 15;


        public DispCheckForceDoneProcTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void DispCheckForceDoneProcTest_Success()
        {
            
            var model = new DispCheckForceDone()
            {
                DirectionID = DIRECTION_ID
            };

            var proc = new DispCheck.DispCheckForceDoneProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var direction = db.FirstOrDefault<Direction>("where directionid = @0", DIRECTION_ID);
                Assert.True(direction.DocStatus == Direction.DR_STATUS_DISP_CHECK_DONE);

                var hist = db.Fetch<DirectionHistory>(new NPoco.Sql().Where("directionid = @0", DIRECTION_ID));
                Assert.True(hist.Count > 0);
                var hist2 = hist.OrderByDescending(h => h.DirectionID).First();
                Assert.Equal(DIRECTION_ID, hist2.DirectionID);
                Assert.Equal(Direction.DR_STATUS_DISP_CHECK_DONE, hist2.EventCode.Trim());
                Assert.Equal("Forced", hist2.EventData.Trim());
                Assert.Equal(USER_ID, hist2.EntryUserID.Trim());
                Assert.True(hist2.EntryDateTime <= DateTime.Now);

                //messageBus
                var messageBus = db.Fetch<MessageBus>(new NPoco.Sql().Where("DirectionID = @0", DIRECTION_ID));
                Assert.Collection(messageBus, 
                        (mb) => Assert.Equal(MessageBus.MessageBusTypeEnum.ExportDirection, mb.MessageType),
                        (mb) => Assert.Equal(MessageBus.MessageBusTypeEnum.ExportABRA, messageBus[1].MessageType)
                        );
            }
        }


        [Fact]
        public void DispCheckForceDoneProcTest_Error()
        {
            var model = new DispCheckForceDone()
            {
                DirectionID = -1
            };

            var proc = new DispCheck.DispCheckForceDoneProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal("Doklad nebyl nalezen; DirectionID: -1", res.ErrorText);
        }
    }
}

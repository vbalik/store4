﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Logistics;
using System.Collections.Generic;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class LogisticsDispatchToPositionProcTest : ProcTestBase
    {
        public LogisticsDispatchToPositionProcTest(DatabaseFixture fixture) : base(fixture)
        { }

        private class TestItem
        {
            public int ArtPackID { get; set; }
            public decimal Quantity { get; set; }
        }


        [Fact]
        public void LogisticsDispatchToPositionProcTest_Success()
        {
            var model = new LogisticsDispatchToPosition()
            {
                DirectionID = 27,
                ReceivePositionID = 114,
                DestPositionID = 117,
                WorkerID = "WRK1"
            };

            var proc = new Logistics.LogisticsDispatchToPositionProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // output side
                var direction = db.FirstOrDefault<Direction>("where directionid = @0", model.DirectionID);
                Assert.True(direction.DocStatus == Direction.DR_STATUS_DISP_EXPED);

                var directionItems = db.Fetch<DirectionItem>("where directionid = @0", model.DirectionID);
                var testItems = directionItems.Select(i => new TestItem() { ArtPackID = i.ArtPackID, Quantity = i.Quantity }).ToList();

                var hist = db.Fetch<DirectionHistory>(new NPoco.Sql().Where("directionid = @0", model.DirectionID));
                Assert.True(hist.Count > 0);

                // input side
                var directionIn = db.FirstOrDefault<Direction>("where partnerRef = @0", $"{direction.DocNumPrefix} / {direction.DocYear} / {direction.DocNumber}");
                Assert.NotNull(directionIn);
                Assert.True(directionIn.DocStatus == Direction.DR_STATUS_STOIN_COMPLET);

                var histIn = db.Fetch<DirectionHistory>(new NPoco.Sql().Where("directionid = @0", directionIn.DirectionID));
                Assert.Single(histIn);
                Assert.Equal(Direction.DR_EVENT_DISP_TO_POSITION, histIn[0].EventCode);

                var directionInItems = db.Fetch<DirectionItem>("where directionid = @0", directionIn.DirectionID);
                Assert.Equal(testItems.Count, directionInItems.Count);
                var items2 = directionInItems.Select(i => new TestItem() { ArtPackID = i.ArtPackID, Quantity = i.Quantity }).ToList();
                TestItems(testItems, items2);

                var directionAssigns = db.Fetch<DirectionAssign>("where directionid = @0 AND jobID = @1", directionIn.DirectionID, DirectionAssign.JOB_ID_STORE_IN);
                Assert.NotNull(directionAssigns);


                // store move - receive
                var stMoveReceive = db.FirstOrDefault<StoreMove>("where parent_directionID = @0 AND docType = @1", directionIn.DirectionID, StoreMove.MO_DOCTYPE_RECEIVE);
                Assert.NotNull(stMoveReceive);
                var stMoveReceiveHist = db.FirstOrDefault<StoreMoveHistory>(new NPoco.Sql().Where("stoMoveID = @0", stMoveReceive.StoMoveID));
                Assert.NotNull(stMoveReceiveHist);
                Assert.Equal(StoreMove.MO_STATUS_SAVED_RECEIVE, stMoveReceiveHist.EventCode);

                TestItems(db, testItems, stMoveReceive);

                // store move - stoin
                var stMoveStoIn = db.FirstOrDefault<StoreMove>("where parent_directionID = @0 AND docType = @1", directionIn.DirectionID, StoreMove.MO_DOCTYPE_STORE_IN);
                Assert.NotNull(stMoveStoIn);
                var stMoveStoInHist = db.FirstOrDefault<StoreMoveHistory>(new NPoco.Sql().Where("stoMoveID = @0", stMoveStoIn.StoMoveID));
                Assert.NotNull(stMoveStoInHist);
                Assert.Equal(StoreMove.MO_STATUS_NEW_STOIN, stMoveStoInHist.EventCode);

                TestItems(db, testItems, stMoveStoIn);

                // test onStore result
                var onPosition = new StoreCore.OnStore_Basic.OnStore().OnPosition(model.DestPositionID, StoreCore.Interfaces.ResultValidityEnum.Valid);
                var items3 = onPosition.Select(i => new TestItem() { ArtPackID = i.ArtPackID, Quantity = i.Quantity }).ToList();
                TestItems(testItems, items3);
            }
        }

        private void TestItems(NPoco.IDatabase db, List<TestItem> items, StoreMove stMove)
        {
            var stMoveItems = db.Fetch<StoreMoveItem>(new NPoco.Sql().Where("stoMoveID = @0", stMove.StoMoveID));
            var items2 = items.Select(i => new TestItem() { ArtPackID = i.ArtPackID, Quantity = i.Quantity }).ToList();
            TestItems(items, items2);
        }

        private void TestItems(List<TestItem> items, List<TestItem> items2)
        {
            Assert.Empty(items.Except(items2, new ItemComparer()));
            Assert.Empty(items2.Except(items, new ItemComparer()));
        }

        private class ItemComparer : IEqualityComparer<TestItem>
        {
            public bool Equals(TestItem x, TestItem y)
            {
                return (x.ArtPackID == y.ArtPackID && x.Quantity == y.Quantity);
            }

            public int GetHashCode(TestItem obj)
            {
                return obj.ArtPackID.GetHashCode() + obj.Quantity.GetHashCode();
            }
        }

        [Fact]
        public void LogisticsDispatchToPositionProcTest_Error()
        {
            var model = new LogisticsDispatchToPosition()
            {
                DirectionID = 18,
                ReceivePositionID = 113,
                DestPositionID = 1,
                WorkerID = "WRK1"
            };

            var proc = new Logistics.LogisticsDispatchToPositionProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal("Doklad nemá nastavenu expediční pozici; DirectionID: 18", res.ErrorText);
        }
    }
}

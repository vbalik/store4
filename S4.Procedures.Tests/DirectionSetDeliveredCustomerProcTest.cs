﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.Procedures.Admin;
using S4.ProcedureModels.Admin;
using System.Collections.Generic;
using S4.ProcedureModels.Exped;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class DirectionSetDeliveredCustomerProcTest : ProcTestBase
    {
        const int DIRECTION_ID = 64;

        public DirectionSetDeliveredCustomerProcTest(DatabaseFixture fixture) : base(fixture)
        { }

        [Fact]
        public void DirectionSetDeliveredCustomerProcTest_Success()
        {
            var dateTimeDeliveredCustomer = DateTime.Now;
            var model = new DirectionSetDeliveredCustomer()
            {
                DirectionID = DIRECTION_ID,
                DateTimeDeliveredCustomer = dateTimeDeliveredCustomer
            };

            var proc = new S4.Procedures.Exped.DirectionSetDeliveredCustomerProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success == true, res.ErrorText);

            using (NPoco.Database db = Core.Data.ConnectionHelper.GetDB())
            {
                var dh = db.Fetch<DirectionHistory>(new NPoco.Sql().Where("directionID = @0", DIRECTION_ID)).Last();
                Assert.True(dh != null);
                Assert.Equal(Direction.DR_EVENT_DELIVERED, dh.EventCode);
                               
            }


        }

        [Fact]
        public void DirectionSetDeliveredCustomerProcTest_NotSuccess()
        {
            var dateTimeDeliveredCustomer = DateTime.Now;
            var model = new DirectionSetDeliveredCustomer()
            {
                DirectionID = -1,
                DateTimeDeliveredCustomer = dateTimeDeliveredCustomer
            };

            var proc = new S4.Procedures.Exped.DirectionSetDeliveredCustomerProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success == false);
            Assert.Equal($"Doklad nebyl nalezen; DirectionID: {model.DirectionID}", res.ErrorText);


        }
    }
}

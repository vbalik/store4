﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.DispCheck;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class DispCheckGetItemsProcTest : ProcTestBase
    {
        const int DIRECTION_ID = 13;


        public DispCheckGetItemsProcTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void DispCheckGetItemsProcTest_Success()
        {
            var model = new DispCheckGetItems()
            {
                DirectionID = DIRECTION_ID
            };

            var proc = new DispCheck.DispCheckGetItemsProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            Assert.Single(res.ItemInfoList);
            
            // ItemInfoList
            Assert.Equal("X123", res.ItemInfoList[0].ArticleCode);
            Assert.Equal("Zboží 1234", res.ItemInfoList[0].ArticleDesc);
            Assert.Single(res.ItemInfoList[0].ArticlePackingList);
            Assert.Equal("ks", res.ItemInfoList[0].ArticlePackingList[0].PackDesc);
            Assert.Equal(1, res.ItemInfoList[0].ArticlePackingList[0].PackRelation);

            // CheckDetails
            Assert.Collection(res.CheckDetails,
                (i) =>
                {
                    Assert.Equal(1001, i.ArtPackID);
                    Assert.Equal("5679X", i.BatchNum);
                    Assert.Equal(new DateTime(2019, 3, 1), i.ExpirationDate);
                    Assert.Equal(1, i.Quantity);
                },
                (i) =>
                {
                    Assert.Equal(1002, i.ArtPackID);
                    Assert.Null(i.BatchNum);
                    Assert.Null(i.ExpirationDate);
                    Assert.Equal(1, i.Quantity);
                }
                );
        }


        [Fact]
        public void DispCheckGetItemsProcTest_Error()
        {
            var model = new DispCheckGetItems()
            {
                DirectionID = -1
            };

            var proc = new DispCheck.DispCheckGetItemsProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal("Doklad nebyl nalezen; DirectionID: -1", res.ErrorText);
        }
    }
}

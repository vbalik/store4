﻿using S4.Entities;
using S4.ProcedureModels.Pos;
using System;
using System.Linq;
using TestHelpers;
using Xunit;


namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class PositionChangeStatusProcTest : ProcTestBase
    {
        const int POSITION_ID = 120;
       

        public PositionChangeStatusProcTest(DatabaseFixture fixture) : base(fixture) 
        { }


        [Fact]
        public void PositionChangeStatusProcTest_Success()
        {
            var model = new PositionSetStatus()
            {
                PositionID = POSITION_ID,
                Status = Position.POS_STATUS_BLOCKED
            };

            var proc = new S4.Procedures.Pos.PositionSetStatusProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            
            using (var db = GetDB())
            {
                var position = db.FirstOrDefault<Position>("where positionid = @0", POSITION_ID);
                Assert.True(position.PosStatus == Position.POS_STATUS_BLOCKED);
                var positionHis = db.FirstOrDefault<PositionHistory>("where positionid = @0", POSITION_ID);
                Assert.True(positionHis != null);
            }
            
        }


        [Fact]
        public void PositionResetStatusProcTest_Error()
        {
            var model = new PositionSetStatus()
            {
                PositionID = -1,
                Status = Position.POS_STATUS_OK
            };

            var proc = new S4.Procedures.Pos.PositionSetStatusProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.False(res.Success);
            Assert.Equal($"Pozice nebyla nalezena; PositionID: {model.PositionID}", res.ErrorText);
        }
    }
}

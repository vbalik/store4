﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.DispCheck;
using S4.Procedures.DispCheck;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class DispCheckListDirectionsProcTest : ProcTestBase
    {
        const string WORKER_ID = "WRK1";


        public DispCheckListDirectionsProcTest(DatabaseFixture fixture) : base(fixture) 
        { }


        [Fact]
        public void DispCheckListDirectionsProcTest_Success()
        {
            var model = new DispCheckListDirections()
            {
                WorkerID = WORKER_ID
            };

            var proc = new DispCheck.DispCheckListDirectionsProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);

        }
    }
}

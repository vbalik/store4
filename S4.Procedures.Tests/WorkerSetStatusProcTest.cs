﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Admin;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class WorkerSetStatusProcTest : ProcTestBase
    {
        const string WORKER_ID = "Test1";
       

        public WorkerSetStatusProcTest(DatabaseFixture fixture) : base(fixture) 
        { }


        [Fact]
        public void WorkerSetStatusProcTest_Success()
        {
            var model = new WorkerSetStatus()
            {
                WorkerID = WORKER_ID,
                NewStatus = Worker.WK_STATUS_DISABLED
            };

            var proc = new Admin.WorkerSetStatusProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            
            using (var db = GetDB())
            {
                var worker = db.FirstOrDefault<Worker>("where workerid = @0", WORKER_ID);
                Assert.True(worker.WorkerStatus == model.NewStatus);

                var hist = db.Fetch<WorkerHistory>(new NPoco.Sql().Where("workerid = @0", WORKER_ID));
                Assert.True(hist.Count > 0);
                var hist2 = hist.OrderByDescending(h => h.WorkerID).First();
                Assert.Equal(WORKER_ID, hist2.WorkerID);
                Assert.Equal(model.NewStatus.ToString(), hist2.EventCode.Trim());
                Assert.Equal($"ONDUTY|{model.NewStatus}", hist2.EventData.Trim());
                Assert.Equal(USER_ID, hist2.EntryUserID.Trim());
                Assert.True(hist2.EntryDateTime <= DateTime.Now);
            }
            
        }


        [Fact]
        public void WorkerSetStatusProcTest_Error()
        {
            var model = new WorkerSetStatus()
            {
                WorkerID = "-1",
                NewStatus = Worker.WK_STATUS_DISABLED
            };

            var proc = new Admin.WorkerSetStatusProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal("Worker nebyl nalezen; WorkerID: -1", res.ErrorText);
        }
    }
}

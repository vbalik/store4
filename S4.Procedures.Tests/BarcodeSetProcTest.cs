﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.BarCode;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class BarcodeSetProcTest : ProcTestBase
    {
        const int ARTICLE_ID = 100;
        const int ART_PACK_ID = 1001;
        const string BAR_CODE = "BarCode123456";


        public BarcodeSetProcTest(DatabaseFixture fixture) : base(fixture) 
        { }


        [Fact]
        public void BarcodeSetProcTest_Success()
        {
            var model = new BarcodeSet()
            {
                ArtPackID = ART_PACK_ID,
                BarCode = BAR_CODE
            };

            ArticlePacking oldPack = null;
            using (var db = GetDB())
            {
                oldPack = db.SingleById<ArticlePacking>(model.ArtPackID);
            }

            var proc = new Barcode.BarcodeSetProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            using (var db = GetDB())
            {
                var pack = db.SingleById<ArticlePacking>(model.ArtPackID);
                Assert.Equal(pack.BarCode, model.BarCode);
                Assert.True(pack.PackStatus == ArticlePacking.AP_STATUS_OK);

                var hist = db.Fetch<ArticleHistory>(new NPoco.Sql().Where("artpackid = @0", model.ArtPackID));
                Assert.True(hist.Count > 0);
                var hist2 = hist.OrderByDescending(h => h.ArticleHistID).First();
                Assert.Equal(ARTICLE_ID, hist2.ArticleID);
                Assert.Equal(Article.AR_EVENT_BARCODE_SET, hist2.EventCode.Trim());
                Assert.Equal("BARCODE1|" + BAR_CODE, hist2.EventData);
                Assert.Equal(USER_ID, hist2.EntryUserID.Trim());
                Assert.True(hist2.EntryDateTime < DateTime.Now);


                var msg = db.Fetch<InternMessage>(new NPoco.Sql().Where("msgclass = @0", Article.AR_EVENT_BARCODE_SET));
                Assert.True(msg.Count > 0);
                var msg2 = msg.OrderByDescending(h => h.IntMessID).First();
                Assert.Equal(InternMessage.MessageSeverityEnum.Low, msg2.MsgSeverity);
                Assert.Equal("Změna čárového kódu u položky 'X123 - Zboží 123'", msg2.MsgHeader);
                Assert.True(msg2.MsgDateTime < DateTime.Now);

                var msgTextDictionary = Newtonsoft.Json.JsonConvert.DeserializeObject<System.Collections.Generic.Dictionary<string, dynamic>>(msg2.MsgText);
                Assert.Equal(msgTextDictionary["articleID"], 100);
                Assert.Equal(msgTextDictionary["artPackID"], 1001);
                Assert.Equal(msgTextDictionary["oldBarCode"], oldPack.BarCode);
                Assert.Equal(msgTextDictionary["newBarCode"], model.BarCode);
            }
        }


        [Fact]
        public void BarcodeSetProcTest_TooLong()
        {
            var model = new BarcodeSet()
            {
                ArtPackID = -1,
                BarCode = "0123456789012345678901234567890123456789"
            };

            var proc = new Barcode.BarcodeSetProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.False(res.Success);
            Assert.Equal("Čárový kód je moc dlouhý - 40", res.ErrorText);
        }


        [Fact]
        public void BarcodeSetProcTest_Error()
        {
            var model = new BarcodeSet()
            {
                ArtPackID = -1,
                BarCode = BAR_CODE
            };

            var proc = new Barcode.BarcodeSetProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
        }


        [Fact]
        public void BarcodeSetProcTest_BarcodePacking_Add()
        {
            const string BAR_CODE2 = "barCodeAdd123456789";

            var model = new BarcodeSet()
            {
                ArtPackID = ART_PACK_ID,
                BarCode = BAR_CODE2,
                AnotherBarCodeCreation = true
            };

            var proc = new Barcode.BarcodeSetProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success);

            using (var db = GetDB())
            {
                var barcode = db.Query<ArticlePackingBarCode>().Where(_ => _.BarCode == BAR_CODE2).SingleOrDefault();
                Assert.NotNull(barcode);
                Assert.Equal(BAR_CODE2, barcode.BarCode);
            }
        }


        [Fact]
        public void BarcodeSetProcTest_BarcodePacking_Update()
        {
            const int ART_PACK_ID = 1024;
            const string BAR_CODE2 = "5555";
            const int BAR_CODE_ID = 1;

            var model = new BarcodeSet()
            {
                ArtPackID = ART_PACK_ID,
                BarCode = BAR_CODE2,
                ArtPackBarCodeID = BAR_CODE_ID
            };

            var proc = new Barcode.BarcodeSetProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success);

            using (var db = GetDB())
            {
                var barcode = db.SingleById<ArticlePackingBarCode>(BAR_CODE_ID);
                Assert.Equal(BAR_CODE2, barcode.BarCode);
            }
        }

        [Fact]
        public void BarcodeSetProcTest_BarcodePacking_Duplicate()
        {
            const string BAR_CODE = "BarCode123456789";

            var model = new BarcodeSet()
            {
                ArtPackID = ART_PACK_ID,
                BarCode = BAR_CODE,
                AnotherBarCodeCreation = true
            };

            var proc = new Barcode.BarcodeSetProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success);
         
            var proc2 = new Barcode.BarcodeSetProc(USER_ID);
            var res2 = proc2.DoProcedure(model);

            Assert.False(res2.Success);

            Assert.Contains("Čárový kód je již přiřazen", res2.ErrorText);
        }
    }
}

﻿using NPoco;
using S4.Entities;
using S4.Entities.Models;
using S4.ProcedureModels.Dispatch;
using TestHelpers;
using Xunit;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class DispatchManualBeginProcTest : ProcTestBase
    {
        public DispatchManualBeginProcTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void DispatchManualBeginProcTest_Success()
        {
            var model = new DispatchManualBegin()
            {
                DirectionID = 50,
                PositionID = 111
            };

            var proc = new Dispatch.DispatchManualBeginProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var direction = db.SingleOrDefaultById<DirectionModel>(model.DirectionID);
                 
                var sql = new Sql().Where("directionID = @0", model.DirectionID);
                direction.XtraData.Load(db, sql);

                Assert.Equal(Direction.DR_STATUS_DISPATCH_MANUAL, direction.DocStatus);
                Assert.NotNull(direction.XtraData[Direction.XTRA_DISPATCH_POSITION_ID]);
                Assert.Equal(111, direction.XtraData[Direction.XTRA_DISPATCH_POSITION_ID]);
            }
        }


        [Fact]
        public void DispatchManualBeginProcTest_Error()
        {
            var model = new DispatchManualBegin()
            {
                DirectionID = 39,
                PositionID = 1
            };

            var proc = new Dispatch.DispatchManualBeginProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.False(res.Success);
            Assert.Equal($"Doklad nemá očekávaný status DR_STATUS_LOAD; DirectionID: {model.DirectionID}", res.ErrorText);
        }
    }
}

﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.BarCode;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class BarcodeRePrintCarrierLabelProcTest : ProcTestBase
    {
        public BarcodeRePrintCarrierLabelProcTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void BarcodeRePrintCarrierLabelProcTest_Success()
        {
            var model = new BarcodeRePrintCarrierLabel()
            {
                CarrierNum = "ABCDEF",
                PrinterLocationID = 2
            };
            var proc = new Barcode.BarcodeRePrintCarrierLabelProc(USER_ID) { IsTesting = true };
            var res = proc.DoProcedure(model);
            Assert.True(res.Success);
            Assert.Equal("PRINT ABCDEF", proc.RenderResult);
        }

        [Fact]
        public void BarcodeRePrintCarrierLabelProcTest_Error()
        {
            var model = new BarcodeRePrintCarrierLabel()
            {
                CarrierNum = "ABCDEF"
            };

            var proc = new Barcode.BarcodeRePrintCarrierLabelProc(USER_ID) { IsTesting = true };
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
        }
    }
}

﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Delivery;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class PrintLabelProcTest : ProcTestBase
    {
        private const int _DIRECTION_ID_WRONG = 40;
        private const int _DIRECTION_ID = 46;

        public PrintLabelProcTest(DatabaseFixture fixture) : base(fixture)
        { }

        [Fact]
        public void PrintLabelProc_MissingParams()
        {
            var printLabel = new PrintLabel();
            printLabel = new Procedures.Delivery.PrintLabelProc(USER_ID).DoProcedure(printLabel);

            Assert.False(printLabel.Success);
            Assert.Equal("Parametry procedury nejsou platné DirectionID IsNull ", printLabel.ErrorText);
        }

        [Fact]
        public void PrintLabelProc_BadStatus()
        {
            var printLabel = new PrintLabel() { DirectionID = _DIRECTION_ID_WRONG };
            printLabel = new Procedures.Delivery.PrintLabelProc(USER_ID).DoProcedure(printLabel);

            Assert.False(printLabel.Success);
            Assert.Equal("Direction nemá DocStatus DR_STATUS_DELIVERY_SHIPMENT nebo DR_STATUS_DELIVERY_PRINT; DirectionID: 40", printLabel.ErrorText);
        }

        [Fact]
        public void PrintLabelProc_Success()
        {
            var printLabel = new PrintLabel() { DirectionID = _DIRECTION_ID };
            printLabel = new Procedures.Delivery.PrintLabelProc(USER_ID).DoProcedure(printLabel);

            //check success
            Assert.True(printLabel.Success);
            //check data
            Assert.Equal(new byte[] { 4, 6 }, printLabel.FileData);
        }
    }
}

﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Admin;
using S4.Procedures.Admin;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class RemoveExpirationProcTest : ProcTestBase
    {
        const int ARTICLE_ID = 117;

        public RemoveExpirationProcTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void RemoveExpirationProcTest_Success()
        {
            var model = new RemoveExpBatch()
            {
                ArticleID = ARTICLE_ID
            };

            var proc = new RemoveExpirationProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success, res.ErrorText);

            var receiveDoneModel = new ProcedureModels.Receive.ReceiveDone()
            {
                StoMoveID = 523
            };
            new Procedures.Receive.ReceiveDoneProc(USER_ID).DoProcedure(receiveDoneModel);

            var onStore = new StoreCore.OnStore_Basic.OnStore().ByArticle(ARTICLE_ID, StoreCore.Interfaces.ResultValidityEnum.Valid);
            Assert.True(onStore.Count == 3);
            foreach (var item in onStore)
            {
                Assert.NotNull(item.BatchNum);
                Assert.NotNull(item.ExpirationDate);
                Assert.Equal(1, item.Quantity);
            }

            res = proc.DoProcedure(model);
            Assert.True(res.Success, res.ErrorText);
            onStore = new StoreCore.OnStore_Basic.OnStore().ByArticle(ARTICLE_ID, StoreCore.Interfaces.ResultValidityEnum.Valid);
            Assert.True(onStore.Count == 2);
            Assert.NotNull(onStore[0].BatchNum);
            Assert.Null(onStore[0].ExpirationDate);
            Assert.Equal(1, onStore[0].Quantity);
            Assert.NotNull(onStore[1].BatchNum);
            Assert.Null(onStore[1].ExpirationDate);
            Assert.Equal(2, onStore[1].Quantity);
        }
    }
}

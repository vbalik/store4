﻿using S4.Entities;
using S4.ProcedureModels.Dispatch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestHelpers;
using Xunit;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class DispatchItemInstructProcTest : ProcTestBase
    {
        public DispatchItemInstructProcTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void DispatchItemInstructProcTest_Success()
        {
            var model = new DispatchItemInstruct()
            {
                StoMoveID = 511,
                StoMoveItemID = 123
            };


            var proc = new Dispatch.DispatchItemInstructProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success);
            Assert.Equal(115, res.DestPosition.PositionID);
            Assert.Equal(1001, res.Item.ArtPackID);
        }


        [Fact]
        public void DispatchItemInstructProcTest_Error()
        {
            var model = new DispatchItemInstruct()
            {
                StoMoveID = 510,
                StoMoveItemID = 16
            };

            var proc = new Dispatch.DispatchItemInstructProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal($"Doklad nemá očekávaný status MO_STATUS_NEW_DISPATCH; StoMoveID: {model.StoMoveID}", res.ErrorText);
        }
    }
}

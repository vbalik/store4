﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.DispCheck;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class DispCheckProcTest : ProcTestBase
    {
        const int DIRECTION_ID = 12;


        public DispCheckProcTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void DispCheckProcTest_Success()
        {
            var model = new DispCheckProc()
            {
                DirectionID = DIRECTION_ID
            };
            
            var proc = new DispCheck.DispCheckProcProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            Assert.True(res.CheckMethod == Direction.DispCheckMethodsEnum.EnterQuantity);

            using (var db = GetDB())
            {
                var hist = db.Fetch<DirectionHistory>(new NPoco.Sql().Where("directionid = @0", DIRECTION_ID));
                Assert.True(hist.Count > 0);
                var hist2 = hist.OrderByDescending(h => h.DirectionID).First();
                Assert.Equal(DIRECTION_ID, hist2.DirectionID);
                Assert.Equal(Direction.DR_STATUS_DISP_CHECK_PROC, hist2.EventCode.Trim());
                Assert.Equal("EnterQuantity", hist2.EventData.Trim());
                Assert.Equal(USER_ID, hist2.EntryUserID.Trim());
                Assert.True(hist2.EntryDateTime <= DateTime.Now);
            }

        }


        [Fact]
        public void DispCheckProcTest_Error()
        {
            var model = new DispCheckProc()
            {
                DirectionID = -1,
            };

            var proc = new DispCheck.DispCheckProcProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal("Doklad nebyl nalezen; DirectionID: -1", res.ErrorText);
        }

    }
}

﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Load;
using S4.Entities.Helpers;
using S4.Entities.Helpers.Xml.Article;
using S4.Procedures.Load.Helper;
using S4.Entities.Helpers.Xml.Cancellation;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class CancellationLoadTest : ProcTestBase
    {
        private const int DIRECTION_ID1 = 60;
        private const int DIRECTION_ID2 = 61;

        public CancellationLoadTest(DatabaseFixture fixture) : base(fixture)
        { }

        [Fact]
        public void CancellationLoadTest_NoSuccess_BadParams()
        {
            var model = new CancellationLoad()
            {
                CancellationS3 = null,
                Xml = "",
                Remark = ""
            };

            var proc = new Load.CancellationLoadProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.False(res.Success);

        }

        [Fact]
        public void CancellationLoadTest_NoSuccess_NoFound()
        {
            CancellationS3 cancellationS3 = new CancellationS3();
            cancellationS3.DocDate = new DateTime(1970, 1, 1);
            cancellationS3.DocPrefix= "XX";
            cancellationS3.DocNumber = -1;

            var model = new CancellationLoad()
            {
                CancellationS3 = cancellationS3,
                Xml = "xxx",
                Remark = "Cancellation.xml"
            };

            var proc = new Load.CancellationLoadProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.False(res.Success);
            var error = "Cancellation.xml (XX/70/-1) Doklad nebyl nalezen!";
            Assert.Equal(res.ErrorText, error);

        }

        [Fact]
        public void CancellationLoadTest_NoSuccess_BadStatus()
        {
            CancellationS3 cancellationS3 = new CancellationS3();
            cancellationS3.DocDate = new DateTime(2021, 1, 1);
            cancellationS3.DocPrefix = "DL";
            cancellationS3.DocNumber = 1;

            var model = new CancellationLoad()
            {
                CancellationS3 = cancellationS3,
                Xml = "xxx",
                Remark = "Cancellation.xml"
            };

            var proc = new Load.CancellationLoadProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.False(res.Success);
            var error = "Cancellation.xml (DL/21/1) Doklad nelze stornovat protože je ve stavu *Export dat*";
            Assert.Equal(res.ErrorText, error);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var his = db.Fetch<DirectionHistory>("where directionID = @0", DIRECTION_ID1).LastOrDefault();
                Assert.Equal(his.EventCode, Direction.DR_EVENT_UNSUCCESSFUL_CANCEL);
            }

        }

        [Fact]
        public void CancellationLoadTest_Success()
        {
            CancellationS3 cancellationS3 = null;
            var xml = XmlHelper.GetStringFromXMLFile(@"..\..\..\XmlTestData\Cancellation.xml");
            System.Exception exception = null;
            var load = CancellationS3.Serializer.Deserialize(xml, out cancellationS3, out exception);

            Assert.True(load);

            var model = new CancellationLoad()
            {
                CancellationS3 = cancellationS3,
                Xml = xml,
                Remark = "Cancellation.xml"
            };

            var proc = new Load.CancellationLoadProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var his = db.Fetch<DirectionHistory>("where directionID = @0", DIRECTION_ID2).LastOrDefault();
                Assert.Equal(his.EventCode, Direction.DR_STATUS_CANCEL);
                var dir = db.Fetch<Direction>("where directionID = @0", DIRECTION_ID2).FirstOrDefault();
                Assert.Equal(dir.DocStatus, Direction.DR_STATUS_CANCEL);
            }
        }

    }
}

﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.StoreIn;

namespace S4.Procedures.Tests
{
    [Collection("StoreInTests")]
    [Trait("Category", "StoreInTests")]
    public class StoreInWaitProcTest : ProcTestBase
    {
        const string WORKER_ID = "WRK1";
        const int DIRECTION_ID = 22;


        public StoreInWaitProcTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void StoreInWaitProcTest_Success()
        {
            var model = new StoreInWait()
            {
                DirectionID = DIRECTION_ID,
                WorkerID = WORKER_ID
            };

            var proc = new StoreIn.StoreInWaitProc(USER_ID);
            var res = proc.DoProcedure(model);
            Assert.True(res.Success, res.ErrorText);

            using (var db = GetDB())
            {
                //check DocStatus
                var direction = db.FirstOrDefault<Direction>("where directionid = @0", DIRECTION_ID);
                Assert.True(direction.DocStatus == Direction.DR_STATUS_STOIN_WAIT, "Direction.DocStatus není DR_STATUS_STOIN_WAIT");

                //check remove old asignments
                var directionAssigns = db.Fetch<DirectionAssign>("where directionid = @0", DIRECTION_ID);
                Assert.True(directionAssigns.Count == 1, "DirectionAssigns nebyl nalezen");

                //check history
                var hist = db.Fetch<DirectionHistory>(new NPoco.Sql().Where("directionid = @0", DIRECTION_ID));
                Assert.True(hist.Count > 0);
                var hist2 = hist.OrderByDescending(h => h.DirectionID).First();
                Assert.Equal(DIRECTION_ID, hist2.DirectionID);
                Assert.Equal(Direction.DR_STATUS_STOIN_WAIT, hist2.EventCode.Trim());
                Assert.Equal(USER_ID, hist2.EntryUserID.Trim());
                Assert.True(hist2.EntryDateTime < DateTime.Now);
            }
        }

        [Fact]
        public void StoreInWaitProcTest_Error()
        {
            var model = new StoreInWait()
            {
                DirectionID = -1,
                WorkerID = WORKER_ID
            };

            var proc = new StoreIn.StoreInWaitProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
        }
    }
}

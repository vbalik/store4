﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;

namespace S4.Procedures.Tests
{
    [Collection("StockTakingTests")]
    [Trait("Category", "StockTakingTests")]
    public class StockTakingDeleteItemTest : ProcTestBase
    {
        int STOTAK_ITEM_ID = 10;

        public StockTakingDeleteItemTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void StockTakingDeleteItemTest_Success()
        {
            var model = new ProcedureModels.StockTaking.StockTakingDeleteItem()
            {
                StotakItemID = STOTAK_ITEM_ID
            };

            using (var db = GetDB())
            {
                // get item
                var item = db.FirstOrDefault<StockTakingItem>("where stotakitemid = @0", STOTAK_ITEM_ID);
                Assert.True(item != null);

                var proc = new StockTaking.StockTakingDeleteItemProc(USER_ID);
                var res = proc.DoProcedure(model);

                Assert.True(res.Success, res.ErrorText);

                // get item
                var item2 = db.FirstOrDefault<StockTakingItem>("where stotakitemid = @0", STOTAK_ITEM_ID);
                Assert.True(item2 == null);

                //his
                var hist = db.Fetch<StockTakingHistory>(new NPoco.Sql().Where("stotakID = @0", item.StotakID));
                Assert.True(hist.Count > 0);
                var hist2 = hist.OrderByDescending(h => h.StotakHistID).First();
                Assert.Equal(item.StotakID, hist2.StotakID);
                Assert.Equal(StockTakingHistory.STOCKTAKING_EVENT_ITEM_DELETED, hist2.EventCode.Trim());
                Assert.Equal(USER_ID, hist2.EntryUserID.Trim());
                Assert.True(hist2.EntryDateTime < DateTime.Now);
            }
            
        }


        [Fact]
        public void StockTakingDeleteItemTest_Error()
        {
            var model = new ProcedureModels.StockTaking.StockTakingDeleteItem()
            {
                StotakItemID = -1
            };

            var proc = new StockTaking.StockTakingDeleteItemProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal("StockTakingItem nebyl nalezen; StotakItemID: -1", res.ErrorText);
        }
    }
}

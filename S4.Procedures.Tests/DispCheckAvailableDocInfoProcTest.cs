﻿using TestHelpers;
using Xunit;
using S4.ProcedureModels.DispCheck;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class DispCheckAvailableDocInfoProcTest : ProcTestBase
    {
        public DispCheckAvailableDocInfoProcTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Theory]
        [InlineData("21")]
        [InlineData("521")]
        [InlineData("20/521")]
        [InlineData("DL/20/521")]
        [InlineData("dl/20/521")]
        public void AvailableDocInfoDirectionsTest_Success(string docInfo)
        {
            var model = new DispCheckAvailableDirectionsDocInfo()
            {
                DocInfo = docInfo,
                DocYear = 20
            };

            var proc = new DispCheck.DispCheckAvailableDirectionsDocInfoProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            Assert.Single(res.Directions);
        }
    }
}

﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Delivery;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class PrintManifestProcTest : ProcTestBase
    {
        public PrintManifestProcTest(DatabaseFixture fixture) : base(fixture)
        { }

        [Fact]
        public void PrintManifestProc_MissingParams()
        {
            var printManifest = new PrintManifest();
            printManifest = new Procedures.Delivery.PrintManifestProc(USER_ID).DoProcedure(printManifest);

            Assert.False(printManifest.Success);
            Assert.Equal("Parametry procedury nejsou platné ManifestRefNumber IsNull ", printManifest.ErrorText);
        }

        [Fact]
        public void PrintManifestProc_Success()
        {
            var printManifest = new PrintManifest() { DeliveryProvider = DeliveryDirection.DeliveryProviderEnum.DPD, ManifestRefNumber = "123" };
            printManifest = new Procedures.Delivery.PrintManifestProc(USER_ID).DoProcedure(printManifest);

            //check success
            Assert.True(printManifest.Success);
            //check data
            Assert.Equal(new byte[] { 1, 2, 3 }, printManifest.FileData);
        }
    }
}

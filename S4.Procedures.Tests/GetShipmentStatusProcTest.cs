﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Delivery;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class GetShipmentStatusProcTest : ProcTestBase
    {
        private const int _DIRECTION_ID_WRONG = 1;
        private const int _DIRECTION_ID = 46;

        public GetShipmentStatusProcTest(DatabaseFixture fixture) : base(fixture)
        { }

        [Fact]
        public void GetShipmentStatusProc_MissingParams()
        {
            var getShipmentStatus = new GetShipmentStatus();
            getShipmentStatus = new Procedures.Delivery.GetShipmentStatusProc(USER_ID).DoProcedure(getShipmentStatus);
            Assert.False(getShipmentStatus.Success);
            Assert.Equal("Parametry procedury nejsou platné DirectionID IsNull ", getShipmentStatus.ErrorText);
        }

        [Fact]
        public void GetShipmentStatusProc_BadStatus()
        {
            var getShipmentStatus = new GetShipmentStatus()
            {
                DirectionID = _DIRECTION_ID_WRONG,
                IncludingCanceledShipment = false
            };
            getShipmentStatus = new Procedures.Delivery.GetShipmentStatusProc(USER_ID).DoProcedure(getShipmentStatus);
            Assert.False(getShipmentStatus.Success);
            Assert.Equal("Direction nemá DocDirection DOC_DIRECTION_OUT; DirectionID: 1", getShipmentStatus.ErrorText);
        }

        [Fact]
        public void GetShipmentStatusProc_Success()
        {
            var getShipmentStatus = new GetShipmentStatus()
            {
                DirectionID = _DIRECTION_ID,
                IncludingCanceledShipment = false
            };
            getShipmentStatus = new Procedures.Delivery.GetShipmentStatusProc(USER_ID).DoProcedure(getShipmentStatus);

            //check success
            Assert.True(getShipmentStatus.Success);
            Assert.Equal(DeliveryDirection.DeliveryProviderEnum.DPD, getShipmentStatus.Provider);
            Assert.Collection(getShipmentStatus.Content,
                item => { Assert.Equal("key1", item.Key); Assert.Equal("label1", item.Label); Assert.Equal("value1", item.Value); },
                item => { Assert.Equal("key2", item.Key); Assert.Equal("label2", item.Label); Assert.Equal("value2", item.Value); });
        }
    }
}
﻿using S4.Entities;
using S4.ProcedureModels.Dispatch;
using System;
using TestHelpers;
using Xunit;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class DispatchManualSaveItemProcTest : ProcTestBase
    {
        public DispatchManualSaveItemProcTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void DispatchManualSaveItemProcTest_Success()
        {
            var model = new DispatchManualSaveItem()
            {
                DirectionID = 48,
                PositionID = 123,
                ArtPackID = 1021,
                Quantity = 1,
                ParentDocPosition = 1,
                BatchNum = "DISMANSAVE",
                ExpirationDate = new DateTime(2020, 1, 1)
            };

            var proc = new Dispatch.DispatchManualSaveItemProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var storeMove = db.SingleOrDefaultById<StoreMove>(res.StoMoveID);            
                var dirItems = db.Fetch<DirectionItem>(new NPoco.Sql().Where("directionID = @0", storeMove.Parent_directionID));
                Assert.NotEmpty(dirItems);
                
                Assert.Equal(StoreMove.MO_STATUS_SAVED_DISPATCH, storeMove.DocStatus);

                Assert.Equal(DirectionItem.DI_STATUS_DISPATCH_SAVED, dirItems[model.ParentDocPosition-1].ItemStatus);

                var storeMoveItem = db.FirstOrDefault<StoreMoveItem>(new NPoco.Sql().Where("stoMoveID = @0", res.StoMoveID));
                Assert.Equal(StoreMoveItem.MI_VALIDITY_VALID, storeMoveItem.ItemValidity);
                Assert.Equal(storeMoveItem.Parent_docPosition, dirItems[model.ParentDocPosition-1].DocPosition);
            }
        }

        [Fact]
        public void DispatchManualSaveItemProcTest_NoBatch_Success()
        {
            var model = new DispatchManualSaveItem()
            {
                DirectionID = 48,
                PositionID = 123,
                ArtPackID = 1022,
                Quantity = 1,
                ParentDocPosition = 2
            };

            var proc = new Dispatch.DispatchManualSaveItemProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var storeMove = db.SingleOrDefaultById<StoreMove>(res.StoMoveID);
                var dirItems = db.Fetch<DirectionItem>(new NPoco.Sql().Where("directionID = @0", storeMove.Parent_directionID));
                Assert.NotEmpty(dirItems);

                Assert.Equal(StoreMove.MO_STATUS_SAVED_DISPATCH, storeMove.DocStatus);

                Assert.Equal(DirectionItem.DI_STATUS_DISPATCH_SAVED, dirItems[model.ParentDocPosition-1].ItemStatus);

                var storeMoveItem = db.FirstOrDefault<StoreMoveItem>(new NPoco.Sql().Where("stoMoveID = @0", res.StoMoveID));
                Assert.Equal(StoreMoveItem.MI_VALIDITY_VALID, storeMoveItem.ItemValidity);
                Assert.Equal(storeMoveItem.Parent_docPosition, dirItems[model.ParentDocPosition-1].DocPosition);
            }
        }


        [Fact]
        public void DispatchManualSaveItemProcTest_Error()
        {
            var model = new DispatchManualSaveItem()
            {
                DirectionID = 39,
                PositionID = 120,
                ArtPackID = 1017,
                Quantity = 1,
                ParentDocPosition = 1
            };

            var proc = new Dispatch.DispatchManualSaveItemProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.False(res.Success);
            Assert.Equal($"Doklad nemá očekávaný status DR_STATUS_DISPATCH_MANUAL; DirectionID: {model.DirectionID}", res.ErrorText);
        }

        [Fact]
        public void DispatchManualSaveItemProcTest_WrongBatch_Error()
        {
            var model = new DispatchManualSaveItem()
            {
                DirectionID = 48,
                PositionID = 123,
                ArtPackID = 1021,
                Quantity = 1,
                ParentDocPosition = 1,
                BatchNum = "WRONG",
                ExpirationDate = new DateTime(2020, 1, 1)
            };

            var proc = new Dispatch.DispatchManualSaveItemProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.False(res.Success);
            Assert.Contains($"Položka nenalezena", res.ErrorText);
        }

        [Fact]
        public void DispatchManualSaveItemProcTest_WrongExpiration_Error()
        {
            var model = new DispatchManualSaveItem()
            {
                DirectionID = 48,
                PositionID = 123,
                ArtPackID = 1021,
                Quantity = 1,
                ParentDocPosition = 1,
                BatchNum = "DISMANSAVE",
                ExpirationDate = new DateTime(2050, 1, 1)
            };

            var proc = new Dispatch.DispatchManualSaveItemProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.False(res.Success);
            Assert.Contains($"Položka nenalezena", res.ErrorText);
        }
    }
}

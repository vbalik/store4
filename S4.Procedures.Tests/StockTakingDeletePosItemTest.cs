﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;

namespace S4.Procedures.Tests
{
    [Collection("StockTakingTests")]
    [Trait("Category", "StockTakingTests")]
    public class StockTakingDeletePosItemTest : ProcTestBase
    {
        private int STOTAK_ID = 1;

        public StockTakingDeletePosItemTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void StockTakingDeletePosItemTest_Success()
        {
            var model = new ProcedureModels.StockTaking.StockTakingDeletePosItem()
            {
                StotakID = STOTAK_ID,
                PositionID = 1,
                StotakItemID = 2
            };

            using (var db = GetDB())
            {
                var item = db.SingleOrDefaultById<StockTakingItem>(model.StotakItemID);
                Assert.True(item != null);

                var proc = new StockTaking.StockTakingDeletePosItemProc(USER_ID);
                var res = proc.DoProcedure(model);

                Assert.True(res.Success, res.ErrorText);

                var item2 = db.SingleOrDefaultById<StockTakingItem>(model.StotakItemID);
                Assert.True(item2 == null);

                var pos = db.First<StockTakingPosition>("where stotakID = @0 AND positionID = @1", model.StotakID, model.PositionID);
                Assert.True(pos != null);
                Assert.True(pos.CheckCounter == 1);

                //his
                var hist = db.Fetch<StockTakingHistory>(new NPoco.Sql().Where("stotakID = @0", model.StotakID));
                Assert.True(hist.Count > 0);
                var hist2 = hist.OrderByDescending(h => h.StotakHistID).First();
                Assert.Equal(STOTAK_ID, hist2.StotakID);
                Assert.Equal(StockTakingHistory.STOCKTAKING_EVENT_CLEAR, hist2.EventCode.Trim());
                Assert.Equal(USER_ID, hist2.EntryUserID.Trim());
                Assert.True(hist2.EntryDateTime < DateTime.Now);

            }
            
        }


        [Fact]
        public void StockTakingDeletePosItemTest_Error()
        {
            var model = new ProcedureModels.StockTaking.StockTakingDeletePosItem()
            {
                StotakID = -1,
                PositionID = 1,
                StotakItemID = 2
            };

            var proc = new StockTaking.StockTakingDeletePosItemProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.False(res.Success);
            Assert.Equal("StockTaking nebylo nalezeno; StotakID: -1", res.ErrorText);
        }


        [Fact]
        public void StockTakingDeletePosItemTest_ItemNotFound()
        {
            var model = new ProcedureModels.StockTaking.StockTakingDeletePosItem()
            {
                StotakID = STOTAK_ID,
                PositionID = 1,
                StotakItemID = -2
            };

            var proc = new StockTaking.StockTakingDeletePosItemProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.False(res.Success);
            Assert.Equal("StockTakingItem nebyl nalezen; StotakItemID: -2", res.ErrorText);
        }


        [Fact]
        public void StockTakingDeletePosItemTest_BadPosition()
        {
            var model = new ProcedureModels.StockTaking.StockTakingDeletePosItem()
            {
                StotakID = STOTAK_ID,
                PositionID = -1,
                StotakItemID = 2
            };

            var proc = new StockTaking.StockTakingDeletePosItemProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success);
            Assert.True(res.BadPosition);
        }
    }
}

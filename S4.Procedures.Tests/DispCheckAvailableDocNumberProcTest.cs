﻿using TestHelpers;
using Xunit;
using S4.ProcedureModels.DispCheck;
using System.Linq;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class DispCheckAvailableDocNumberProcTest : ProcTestBase
    {
        const string ABRA_DOC_ID = "1234567890";
        const string ABRA_DOC_ID_BAD_STATUS = "1234567891";
        const int DIRECTION_ID = 51;


        public DispCheckAvailableDocNumberProcTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void AvailableAbraDirectionsTest_Success()
        {
            var model = new DispCheckAvailableDirectionsDocNumber()
            {
                DocumentID = ABRA_DOC_ID
            };

            var proc = new DispCheck.DispCheckAvailableDirectionsDocNumberProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            Assert.Single(res.Directions);
            Assert.Equal(DIRECTION_ID, res.Directions.First().DocumentID);
        }

        [Fact]
        public void AvailableAbraDirectionsTest_BadDocStatus()
        {
            var model = new DispCheckAvailableDirectionsDocNumber()
            {
                DocumentID = ABRA_DOC_ID_BAD_STATUS
            };

            var proc = new DispCheck.DispCheckAvailableDirectionsDocNumberProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success);
            Assert.Empty(res.Directions);
        }

        [Fact]
        public void AvailableAbraDirectionsTest_NotFound()
        {
            var model = new DispCheckAvailableDirectionsDocNumber()
            {
                DocumentID = "000"
            };

            var proc = new DispCheck.DispCheckAvailableDirectionsDocNumberProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success);
            Assert.Empty(res.Directions);
        }

        [Fact]
        public void AvailableInvoiceNumberDirectionsTest_Success()
        {
            const string INUM = "INUM1234";

            var model = new DispCheckAvailableDirectionsDocNumber()
            {
                DocumentID = INUM
            };

            var proc = new DispCheck.DispCheckAvailableDirectionsDocNumberProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success);
            Assert.Single(res.Directions);
            Assert.Equal(DIRECTION_ID, res.Directions.First().DocumentID);
        }

        [Fact]
        public void AvailableInvoiceNumberDirectionsTest_SuccessMulti()
        {
            const string INUM = "INUM1234MULTI";

            var model = new DispCheckAvailableDirectionsDocNumber()
            {
                DocumentID = INUM
            };

            var proc = new DispCheck.DispCheckAvailableDirectionsDocNumberProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success);
            Assert.Equal(2, res.Directions.Count);
        }

        [Theory]
        [InlineData("DL/20/51")]
        [InlineData("dl/20/51")]
        [InlineData("20/51")]
        public void AvailableDocNumberDirectionsTest_Success(string docInfo)
        {
            var model = new DispCheckAvailableDirectionsDocNumber()
            {
                DocumentID = docInfo
            };

            var proc = new DispCheck.DispCheckAvailableDirectionsDocNumberProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            Assert.Single(res.Directions);
            Assert.Equal(DIRECTION_ID, res.Directions.First().DocumentID);
        }

    }
}

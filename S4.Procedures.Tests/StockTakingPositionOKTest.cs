﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;

namespace S4.Procedures.Tests
{
    [Collection("StockTakingTests")]
    [Trait("Category", "StockTakingTests")]
    public class StockTakingPositionOKTest : ProcTestBase
    {

        private int STOTAK_ID = 1;

        public StockTakingPositionOKTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void StockTakingPositionOKTest_Success()
        {
            var model = new ProcedureModels.StockTaking.StockTakingPositionOK()
            {
                StotakID = STOTAK_ID,
                PositionID = 4
            };

            using (var db = GetDB())
            {
                // get items
                var items = db.Fetch<StockTakingItem>("where stotakID = @0 AND positionID = @1", model.StotakID, model.PositionID);
                Assert.True(items.Count > 0);
                
                var proc = new StockTaking.StockTakingPositionOKProc(USER_ID);
                var res = proc.DoProcedure(model);

                Assert.True(res.Success, res.ErrorText);
                
                var pos = db.First<StockTakingPosition>("where stotakID = @0 AND positionID = @1", model.StotakID, model.PositionID);
                Assert.True(pos != null);
                Assert.True(pos.CheckCounter == 1);

                // get snapshot
                var snapshots = db.Fetch<Entities.StockTakingSnapshot>("where stotakID = @0 AND snapshotClass = @1 AND positionID = @2", model.StotakID, (byte)StockTakingSnapshot.StockTakingSnapshotClassEnum.Internal, model.PositionID);
                var items2 = db.Fetch<StockTakingItem>("where stotakID = @0 AND positionID = @1", model.StotakID, model.PositionID);
                Assert.True(items2.Count == snapshots.Count);
                Assert.Equal(items2[0].ArtPackID, snapshots[0].ArtPackID);
                Assert.Equal(items2[0].BatchNum, snapshots[0].BatchNum);
                Assert.Equal(items2[0].CarrierNum, snapshots[0].CarrierNum);
                Assert.Equal(items2[0].ExpirationDate, snapshots[0].ExpirationDate);
                Assert.Equal(items2[0].Quantity, snapshots[0].Quantity);

                // history
                var hist = db.Fetch<StockTakingHistory>(new NPoco.Sql().Where("stotakID = @0", model.StotakID));
                Assert.True(hist.Count > 0);
                var hist2 = hist.OrderByDescending(h => h.StotakHistID).First();
                Assert.Equal(STOTAK_ID, hist2.StotakID);
                Assert.Equal(StockTakingHistory.STOCKTAKING_EVENT_POSITION_IS_OK, hist2.EventCode.Trim());
                Assert.Equal(USER_ID, hist2.EntryUserID.Trim());
                Assert.True(hist2.EntryDateTime <= DateTime.Now);

            }
            
        }

        [Fact]
        public void StockTakingPositionOKTest_Error()
        {
            var model = new ProcedureModels.StockTaking.StockTakingClearPos()
            {
                StotakID = -1,
                PositionID = 2
            };

            var proc = new StockTaking.StockTakingClearPosProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.False(res.Success);
            Assert.Equal("StockTaking nebylo nalezeno; StotakID: -1", res.ErrorText);
        }


    }
}

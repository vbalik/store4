﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.BarCode;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class BarcodeDeleteProcTest : ProcTestBase
    {
        const int ARTICLE_ID = 101;
        const int ART_PACK_ID = 1002;
        const string BAR_CODE = "XX12345";

        public BarcodeDeleteProcTest(DatabaseFixture fixture) : base(fixture) 
        { }


        [Fact]
        public void BarcodeDeleteProcTest_Success()
        {
            var model = new BarcodeDelete()
            {
                ArtPackID = ART_PACK_ID
            };

            var proc = new Barcode.BarcodeDeleteProc(USER_ID);
            var res = proc.DoProcedure(model);
            Assert.True(res.Success, res.ErrorText);

            using (var db = GetDB())
            {
                var pack = db.SingleById<ArticlePacking>(model.ArtPackID);
                Assert.True(pack.BarCode == null, "BarCode is not empty");
                Assert.True(pack.PackStatus == ArticlePacking.AP_STATUS_OK, "PackStatus is not AP_STATUS_OK");

                var hist = db.Fetch<ArticleHistory>(new NPoco.Sql().Where("artpackid = @0", model.ArtPackID));
                Assert.True(hist.Count > 0);
                var hist2 = hist.OrderByDescending(h => h.ArticleHistID).First();
                Assert.Equal(ARTICLE_ID, hist2.ArticleID);
                Assert.Equal(Article.AR_EVENT_BARCODE_DEL, hist2.EventCode.Trim());
                Assert.Equal(BAR_CODE, hist2.EventData);
                Assert.Equal(USER_ID, hist2.EntryUserID.Trim());
                Assert.True(hist2.EntryDateTime <= DateTime.Now);


                var msg = db.Fetch<InternMessage>(new NPoco.Sql().Where("msgclass = @0", Article.AR_EVENT_BARCODE_DEL));
                Assert.True(msg.Count > 0);
                var msg2 = msg.OrderByDescending(h => h.IntMessID).First();
                Assert.Equal(InternMessage.MessageSeverityEnum.Low, msg2.MsgSeverity);
                Assert.Equal($"Smazání čárového kódu 'XX12345' u položky 'X456:Zboží 456'", msg2.MsgHeader);
                Assert.True(msg2.MsgDateTime <= DateTime.Now);

                var msgTextDictionary = Newtonsoft.Json.JsonConvert.DeserializeObject<System.Collections.Generic.Dictionary<string, dynamic>>(msg2.MsgText);
                Assert.Equal(msgTextDictionary["artPackID"], 1002);
            }
        }


        [Fact]
        public void BarcodeDeleteProcTest_Error()
        {
            var model = new BarcodeDelete()
            {
                ArtPackID = -1,
            };

            var proc = new Barcode.BarcodeDeleteProc(USER_ID);
            var res = proc.DoProcedure(model);
            Assert.True(!res.Success);
        }





        [Fact]
        public void BarcodeDeleteProcTest_BarcodePacking_Delete()
        {
            const int ART_PACK_ID = 1024;
            const int BAR_CODE_ID = 2;

            var model = new BarcodeDelete()
            {
                ArtPackID = ART_PACK_ID,
                ArtPackBarCodeID = BAR_CODE_ID
            };

            var proc = new Barcode.BarcodeDeleteProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success);


            using (var db = GetDB())
            {
                var barcode = db.SingleOrDefaultById<ArticlePackingBarCode>(BAR_CODE_ID);
                Assert.Null(barcode);
            }
        }
    }
}

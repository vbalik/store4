﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.DispCheck;
using System.Collections.Generic;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests2")]
    [Trait("Category", "ProcedureTests2")]

    public class DispCheckDoneProcTest : ProcTestBase
    {
        public DispCheckDoneProcTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void DispCheckDoneProcTest_Success()
        {
            const int DIRECTION_ID = 14;
            const int DIRECTION_ITEM_ID = 46;
            const int STOMOVEITEM_ID = 145;
            const string SERIAL_NUMBER1 = "AAA";
            const string SERIAL_NUMBER2 = "BBB";


            var model = new DispCheckDone()
            {
                DirectionID = DIRECTION_ID,
                SerialNumbers = new Dictionary<int, List<DispCheckDone.SerialNumberItem>>()
                {
                    {
                        DIRECTION_ITEM_ID,
                        new List<DispCheckDone.SerialNumberItem>()
                        {
                            new DispCheckDone.SerialNumberItem() { SerialNumber = SERIAL_NUMBER1 },
                            new DispCheckDone.SerialNumberItem() { SerialNumber = SERIAL_NUMBER2 }
                        }
                    }
                }
            };

            var proc = new DispCheck.DispCheckDoneProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var direction = db.FirstOrDefault<Direction>("where directionid = @0", DIRECTION_ID);
                Assert.True(direction.DocStatus == Direction.DR_STATUS_DISP_CHECK_DONE);

                var storedSerials = db.Fetch<StoreMoveSerials>(new NPoco.Sql().Where($"{nameof(StoreMoveSerials.StoMoveItemID)} = @0", STOMOVEITEM_ID));
                Assert.Collection(storedSerials, item => Assert.Equal(SERIAL_NUMBER1, item.SerialNumber), item => Assert.Equal(SERIAL_NUMBER2, item.SerialNumber));

                var hist = db.Fetch<DirectionHistory>(new NPoco.Sql().Where("directionid = @0", DIRECTION_ID));
                Assert.True(hist.Count > 0);
                var hist2 = hist.OrderByDescending(h => h.DirectionID).First();
                Assert.Equal(DIRECTION_ID, hist2.DirectionID);
                Assert.Equal(Direction.DR_STATUS_DISP_CHECK_DONE, hist2.EventCode.Trim());
                Assert.Equal(USER_ID, hist2.EntryUserID.Trim());
                Assert.True(hist2.EntryDateTime < DateTime.Now);

                //messageBus
                var messageBus = db.Fetch<MessageBus>(new NPoco.Sql().Where("DirectionID = @0", DIRECTION_ID));
                Assert.Equal(2, messageBus.Count);
                Assert.Equal(MessageBus.MessageBusTypeEnum.ExportDirection, messageBus[0].MessageType);
                Assert.Equal(MessageBus.MessageBusTypeEnum.ExportABRA, messageBus[1].MessageType);
            }
        }


        [Fact]
        public void DispCheckDoneProcTest_InvalidSerialsError()
        {
            const int DIRECTION_ID = 14;
            const int DIRECTION_ITEM_ID = 46;
            const string SERIAL_NUMBER1 = "AAA";

            var model = new DispCheckDone()
            {
                DirectionID = DIRECTION_ID,
                SerialNumbers = new Dictionary<int, List<DispCheckDone.SerialNumberItem>>()
                {
                    {
                        DIRECTION_ITEM_ID,
                        new List<DispCheckDone.SerialNumberItem>()
                        {
                            new DispCheckDone.SerialNumberItem() { SerialNumber = SERIAL_NUMBER1 },
                            new DispCheckDone.SerialNumberItem() { SerialNumber = SERIAL_NUMBER1 }
                        }
                    }
                }
            };

            var proc = new DispCheck.DispCheckDoneProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.False(res.Success);
            Assert.Equal("Množství sériových čísel (1) neodpovídá množství (2) položky \"USESERS - Use Serial Numbers\" v dokladu!", res.ErrorText);
        }


        [Fact]
        public void DispCheckDoneProcTest_Error()
        {
            var model = new DispCheckDone()
            {
                DirectionID = -1
            };

            var proc = new DispCheck.DispCheckDoneProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal("Doklad nebyl nalezen; DirectionID: -1", res.ErrorText);
        }
    }
}

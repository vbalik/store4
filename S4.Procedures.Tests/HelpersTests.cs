﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using S4.Procedures.Move.Helper;
using S4.Entities.Helpers;

namespace S4.Procedures.Tests
{
    public class HelpersTests
    {
        [Fact]
        public void ContainsAll()
        {
            var b = new List<ArticleInfo>()
            {
                new ArticleInfo() { ArtPackID = 1, BatchNum = "b", ExpirationDate = new DateTime(2012, 10, 1), CarrierNum = "c", Quantity = 100 }
            };

            var s1 = new List<ArticleInfo>()
            {
                new ArticleInfo() { ArtPackID = 1, BatchNum = "b", ExpirationDate = new DateTime(2012, 10, 1), CarrierNum = "c", Quantity = 10 }
            };

            var s2 = new List<ArticleInfo>()
            {
                new ArticleInfo() { ArtPackID = 1, BatchNum = "b", ExpirationDate = new DateTime(2012, 10, 1), CarrierNum = "c", Quantity = 101 }
            };

            var s3 = new List<ArticleInfo>()
            {
                new ArticleInfo() { ArtPackID = 1, BatchNum = "b", ExpirationDate = new DateTime(2012, 10, 1), CarrierNum = "c", Quantity = 10 },
                new ArticleInfo() { ArtPackID = 2, BatchNum = "b", ExpirationDate = new DateTime(2012, 10, 1), CarrierNum = "c", Quantity = 10 }
            };

            var s4 = new List<ArticleInfo>()
            {
                new ArticleInfo() { ArtPackID = 1, BatchNum = "B", ExpirationDate = new DateTime(2012, 10, 1), CarrierNum = "C", Quantity = 10 }
            };

            Assert.True(b.ContainsAll(s1).result);

            var r1 = b.ContainsAll(s2);
            Assert.False(r1.result);
            Assert.Equal(1, r1.articleInfo.ArtPackID);

            var r2 = b.ContainsAll(s3);
            Assert.False(r2.result);
            Assert.Equal(2, r2.articleInfo.ArtPackID);

            Assert.True(b.ContainsAll(s4).result);
        }


        [Fact]
        public void ContainsAll_SameItems()
        {
            var b = new List<ArticleInfo>()
            {
                new ArticleInfo() { ArtPackID = 1, BatchNum = "b", ExpirationDate = new DateTime(2012, 10, 1), CarrierNum = "c", Quantity = 1 },
                new ArticleInfo() { ArtPackID = 1, BatchNum = "b", ExpirationDate = new DateTime(2012, 10, 1), CarrierNum = "c", Quantity = 2 }
            };

            var s1 = new List<ArticleInfo>()
            {
                new ArticleInfo() { ArtPackID = 1, BatchNum = "b", ExpirationDate = new DateTime(2012, 10, 1), CarrierNum = "c", Quantity = 3 }
            };

            Assert.True(b.ContainsAll(s1).result);
        }


        [Fact]
        public void ContainsAll_Nulls()
        {
            var b = new List<ArticleInfo>()
            {
                new ArticleInfo() { ArtPackID = 1, BatchNum = null, ExpirationDate = new DateTime(2012, 10, 1), CarrierNum = null, Quantity = 3 },
            };

            var s1 = new List<ArticleInfo>()
            {
                new ArticleInfo() { ArtPackID = 1, BatchNum = null, ExpirationDate = new DateTime(2012, 10, 1), CarrierNum = null, Quantity = 3 }
            };

            Assert.True(b.ContainsAll(s1).result);
        }


        [Fact]
        public void IsSameArticleInfo()
        {
            Assert.True(
                (new ArticleInfo()
                {
                    ArtPackID = 1,
                    BatchNum = "b",
                    ExpirationDate = new DateTime(2012, 10, 1),
                    CarrierNum = "c"
                })
                .IsSameArticleInfo(new ArticleInfo()
                {
                    ArtPackID = 1,
                    BatchNum = "B",
                    ExpirationDate = new DateTime(2012, 10, 1),
                    CarrierNum = "C"
                }, true));

            Assert.False(
                (new ArticleInfo()
                {
                    ArtPackID = 1,
                    BatchNum = "b",
                    ExpirationDate = new DateTime(2012, 10, 1),
                    CarrierNum = "c"
                })
                .IsSameArticleInfo(new ArticleInfo()
                {
                    ArtPackID = 2,
                    BatchNum = "b",
                    ExpirationDate = new DateTime(2012, 10, 1),
                    CarrierNum = "c"
                }, true));

            Assert.False(
                (new ArticleInfo()
                {
                    ArtPackID = 1,
                    BatchNum = "b",
                    ExpirationDate = new DateTime(2012, 10, 1),
                    CarrierNum = "c"
                })
                .IsSameArticleInfo(new ArticleInfo()
                {
                    ArtPackID = 1,
                    BatchNum = "x",
                    ExpirationDate = new DateTime(2012, 10, 1),
                    CarrierNum = "c"
                }, true));


            Assert.False(
                (new ArticleInfo()
                {
                    ArtPackID = 1,
                    BatchNum = "b",
                    ExpirationDate = new DateTime(2012, 10, 1),
                    CarrierNum = "c"
                })
                .IsSameArticleInfo(new ArticleInfo()
                {
                    ArtPackID = 1,
                    BatchNum = "b",
                    ExpirationDate = new DateTime(2012, 11, 1),
                    CarrierNum = "c"
                }, true));

            Assert.False(
                (new ArticleInfo()
                {
                    ArtPackID = 1,
                    BatchNum = "b",
                    ExpirationDate = new DateTime(2012, 10, 1),
                    CarrierNum = "c"
                })
                .IsSameArticleInfo(new ArticleInfo()
                {
                    ArtPackID = 1,
                    BatchNum = "b",
                    ExpirationDate = new DateTime(2012, 10, 1),
                    CarrierNum = "x"
                }, true));
        }


        [Fact]
        public void ExpirationEqual()
        {
            Assert.False(((DateTime?)DateTime.Now).ExpirationEqual((DateTime?)null));
            Assert.True(((DateTime?)null).ExpirationEqual((DateTime?)null));
            Assert.True(((DateTime?)new DateTime(2012, 10, 10)).ExpirationEqual((DateTime?)new DateTime(2012, 10, 10)));
            Assert.False(((DateTime?)new DateTime(2012, 10, 1)).ExpirationEqual((DateTime?)new DateTime(2012, 11, 1)));
        }
    }
}

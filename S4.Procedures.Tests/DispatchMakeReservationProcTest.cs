﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Dispatch;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class DispatchMakeReservationProcTest : ProcTestBase
    {
        const int NOT_FOUND_DIRECTION_ID = 8;
        const int FOUND_DIRECTION_ID = 19;
       

        public DispatchMakeReservationProcTest(DatabaseFixture fixture) : base(fixture) 
        { }


        [Fact]
        public void DispatchMakeReservationProcTest_Success_NotFound()
        {
            var model = new DispatchMakeReservation()
            {
                DirectionID = NOT_FOUND_DIRECTION_ID
            };
            var proc = new Dispatch.DispatchMakeReservationProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            Assert.False(res.IsPrepared);
            Assert.Equal("X123 - Zboží 123 - 1.0000", res.NotFoundInfo);

            using (var db = GetDB())
            {
                var hist = db.Fetch<DirectionHistory>(new NPoco.Sql().Where("directionid = @0", NOT_FOUND_DIRECTION_ID));
                Assert.True(hist.Count > 0);
                var hist2 = hist.OrderByDescending(h => h.DirectionID).First();
                Assert.Equal(NOT_FOUND_DIRECTION_ID, hist2.DirectionID);
                Assert.Equal(Direction.DR_EVENT_RESERVATION_NOT_FOUND, hist2.EventCode.Trim());
                Assert.NotNull(hist2.EventData);
                Assert.Equal(USER_ID, hist2.EntryUserID.Trim());
                Assert.True(hist2.EntryDateTime <= DateTime.Now);

                var msg = db.Fetch<InternMessage>(new NPoco.Sql().Where("msgclass = @0", Direction.DR_EVENT_RESERVATION_NOT_FOUND));
                Assert.True(msg.Count > 0);
                var msg2 = msg.OrderByDescending(h => h.IntMessID).First();
                Assert.Equal(InternMessage.MessageSeverityEnum.Error, msg2.MsgSeverity);
                Assert.Equal("Doklad není možné vyskladnit 'DL/04/8'", msg2.MsgHeader);
                Assert.True(msg2.MsgDateTime <= DateTime.Now);

                var msgTextDictionary = Newtonsoft.Json.JsonConvert.DeserializeObject<System.Collections.Generic.Dictionary<string, dynamic>>(msg2.MsgText);
                Assert.Equal(msgTextDictionary["directionID"], NOT_FOUND_DIRECTION_ID);

            }
        }


        [Fact]
        public void DispatchMakeReservationProcTest_Success_Found()
        {
            var model = new DispatchMakeReservation()
            {
                DirectionID = FOUND_DIRECTION_ID
            };
            var proc = new Dispatch.DispatchMakeReservationProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            Assert.True(res.IsPrepared);
            Assert.True(res.UseRemarks);

            using (var db = GetDB())
            {            
                var direction = db.FirstOrDefault<Direction>("where directionid = @0", FOUND_DIRECTION_ID);
                Assert.True(direction.DocStatus == Direction.DR_STATUS_DISPATCH_PROC);

                var hist = db.Fetch<DirectionHistory>(new NPoco.Sql().Where("directionid = @0", FOUND_DIRECTION_ID));
                Assert.True(hist.Count > 0);
                var hist2 = hist.OrderByDescending(h => h.DirectionID).First();
                Assert.Equal(FOUND_DIRECTION_ID, hist2.DirectionID);
                Assert.Equal(Direction.DR_EVENT_RESERVATION_DONE, hist2.EventCode.Trim());
                Assert.NotNull(hist2.EventData);
                Assert.Equal(USER_ID, hist2.EntryUserID.Trim());
                Assert.True(hist2.EntryDateTime <= DateTime.Now);

                var xtra = db.Fetch<DirectionXtra>(new NPoco.Sql().Where("directionid = @0", FOUND_DIRECTION_ID));
                var xtra2 = xtra.Where(x => x.XtraCode == Direction.XTRA_DISPATCH_STORE_MOVE_ID).SingleOrDefault();
                Assert.NotNull(xtra2);
                Assert.Equal(res.StoMoveID.ToString(), xtra2.XtraValue);

                var move = db.FirstOrDefault<StoreMove>(new NPoco.Sql().Where("stoMoveID = @0", res.StoMoveID));
                Assert.NotNull(move);
                Assert.Equal(FOUND_DIRECTION_ID, move.Parent_directionID);
                Assert.Equal(StoreMove.MO_DOCTYPE_DISPATCH, move.DocType);
                Assert.Equal(StoreMove.MO_STATUS_NEW_DISPATCH, move.DocStatus);
                Assert.Equal(StoreMove.MO_PREFIX_DISPATCH, move.DocNumPrefix);

                var moveItems = db.Fetch<StoreMoveItem>(new NPoco.Sql().Where("stoMoveID = @0", res.StoMoveID)).OrderBy(i => i.ItemDirection).ToList();
                Assert.NotNull(moveItems);
                Assert.Equal(2, moveItems.Count);
                Assert.Equal(0, moveItems.Sum(i => i.Quantity * i.ItemDirection));

                Assert.Equal(-1, moveItems[0].ItemDirection);                
                Assert.Equal(1, moveItems[0].PositionID);                
                Assert.Equal(1, moveItems[1].ItemDirection);
                Assert.Equal(16, moveItems[1].PositionID);

                Assert.All(moveItems, item =>
                {
                    Assert.Equal(1, item.Parent_docPosition);
                    Assert.Equal(1, item.Quantity);
                    Assert.Equal(StoreMoveItem.MI_VALIDITY_NOT_VALID, item.ItemValidity);
                });
            }
        }
    }
}

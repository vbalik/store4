﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Admin;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class DirectionSetStatusProcTest : ProcTestBase
    {
        const int DIRECTION_ID = 3;
       

        public DirectionSetStatusProcTest(DatabaseFixture fixture) : base(fixture) 
        { }


        [Fact]
        public void DirectionSetStatusProcTest_Success()
        {
            var model = new DirectionSetStatus()
            {
                DirectionID = DIRECTION_ID,
                Status = Direction.DR_STATUS_RECEIVE_COMPLET
            };

            Direction oldDirection = null;
            using (var db = GetDB())
            {
                oldDirection = db.FirstOrDefault<Direction>("where directionid = @0", DIRECTION_ID);
            }

                var proc = new Admin.DirectionSetStatusProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            
            using (var db = GetDB())
            {
                var direction = db.FirstOrDefault<Direction>("where directionid = @0", DIRECTION_ID);
                Assert.True(direction.DocStatus == model.Status, direction.DocStatus);

                var hist = db.Fetch<DirectionHistory>(new NPoco.Sql().Where("directionid = @0", model.DirectionID));
                Assert.True(hist.Count > 0);
                var hist2 = hist.OrderByDescending(h => h.DirectionID).First();
                Assert.Equal(DIRECTION_ID, hist2.DirectionID);
                Assert.Equal(Direction.DR_EVENT_STATUS_CHANGED, hist2.EventCode.Trim());
                Assert.Equal("ACT:CREC", hist2.EventData.Trim());
                Assert.Equal(USER_ID, hist2.EntryUserID.Trim());
                Assert.True(hist2.EntryDateTime < DateTime.Now);

                var msg = db.Fetch<InternMessage>(new NPoco.Sql().Where("msgclass = @0", InternMessage.DR_STATUS_CHANGED));
                Assert.True(msg.Count > 0);
                var msg2 = msg.OrderByDescending(h => h.IntMessID).First();
                Assert.Equal(InternMessage.MessageSeverityEnum.Low, msg2.MsgSeverity);
                Assert.Equal("Změna stavu dokladu 'DL/04/3'", msg2.MsgHeader);
                Assert.True(msg2.MsgDateTime < DateTime.Now);
                                
                var msgTextDictionary = Newtonsoft.Json.JsonConvert.DeserializeObject<System.Collections.Generic.Dictionary<string, dynamic>>(msg2.MsgText);
                Assert.Equal(msgTextDictionary["directionID"], 3);
                Assert.Equal(msgTextDictionary["oldStatus"], oldDirection.DocStatus);
                Assert.Equal(msgTextDictionary["newStatus"], model.Status);

            }

        }


        [Fact]
        public void DirectionSetStatusProcTest_Error()
        {
            var model = new DirectionSetStatus()
            {
                DirectionID = -1
            };

            var proc = new Admin.DirectionSetStatusProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
        }
    }
}

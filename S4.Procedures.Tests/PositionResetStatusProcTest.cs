﻿using S4.Entities;
using S4.ProcedureModels.Admin;
using System;
using System.Linq;
using TestHelpers;
using Xunit;


namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class PositionResetStatusProcTest : ProcTestBase
    {
        const int POSITION_ID = 2;


        public PositionResetStatusProcTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void PositionResetStatusProcTest_Success()
        {
            var model = new PositionResetStatus()
            {
                PositionID = POSITION_ID
            };

            Position positionOld = null;
            using (var db = GetDB())
            {
                positionOld = db.FirstOrDefault<Position>("where positionid = @0", POSITION_ID);
            }

            var proc = new Admin.PositionResetStatusProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);

            using (var db = GetDB())
            {
                var position = db.FirstOrDefault<Position>("where positionid = @0", POSITION_ID);
                Assert.True(position.PosStatus == Position.POS_STATUS_OK);

                var msg = db.Fetch<InternMessage>(new NPoco.Sql().Where("msgclass = @0", Position.POS_STATUS_OK));
                Assert.True(msg.Count > 0);
                var msg2 = msg.OrderByDescending(h => h.IntMessID).First();
                Assert.Equal(InternMessage.MessageSeverityEnum.Low, msg2.MsgSeverity);
                Assert.Equal("Pozice odemčena '456:'", msg2.MsgHeader);
                Assert.True(msg2.MsgDateTime <= DateTime.Now);

                var msgTextDictionary = Newtonsoft.Json.JsonConvert.DeserializeObject<System.Collections.Generic.Dictionary<string, dynamic>>(msg2.MsgText);
                Assert.Equal(msgTextDictionary["positionID"], POSITION_ID);
                Assert.Equal(msgTextDictionary["oldStatus"], positionOld.PosStatus);
                Assert.Equal(msgTextDictionary["newStatus"], Position.POS_STATUS_OK);
            }

        }


        [Fact]
        public void PositionResetStatusProcTest_Error()
        {
            var model = new PositionResetStatus()
            {
                PositionID = -1
            };

            var proc = new Admin.PositionResetStatusProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.False(res.Success);
            Assert.Equal("Pozice nebyla nalezena; PositionID: -1", res.ErrorText);
        }
    }
}

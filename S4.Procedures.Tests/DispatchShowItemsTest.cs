﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Dispatch;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class DispatchShowItemsTest : ProcTestBase
    {
        const int STOMOVE_ID = 513;


        public DispatchShowItemsTest(DatabaseFixture fixture) : base(fixture) 
        { }


        [Fact]
        public void DispatchShowItemsTest_Success()
        {
            var model = new DispatchShowItems()
            {
                StoMoveID = STOMOVE_ID
            };
            var proc = new Dispatch.DispatchShowItemsProc(USER_ID) { IsTesting = true };
            var res = proc.DoProcedure(model);
            Assert.True(res.Success);
            Assert.True(res.Items.Count == 1);
        }


        [Fact]
        public void DispatchShowItemsTest_Error()
        {
            var model = new DispatchShowItems()
            {
                StoMoveID = -1
            };

            var proc = new Dispatch.DispatchShowItemsProc(USER_ID) { IsTesting = true };
            var res = proc.DoProcedure(model);

            Assert.False(res.Success);
        }

    }
}

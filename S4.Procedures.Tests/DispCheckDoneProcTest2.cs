﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.DispCheck;
using System.Collections.Generic;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]

    public class DispCheckDoneProcTest2 : ProcTestBase
    {
        public DispCheckDoneProcTest2(DatabaseFixture fixture) : base(fixture)
        { }



        [Fact]
        public void DispCheckDoneProcTest_NoSerialsError()
        {
            const int DIRECTION_ID = 14;

            var model = new DispCheckDone()
            {
                DirectionID = DIRECTION_ID
            };

            var proc = new DispCheck.DispCheckDoneProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.False(res.Success);
            Assert.Equal("U položky \"USESERS - Use Serial Numbers\" nejsou vyplněna sériová čísla!", res.ErrorText);
        }


        [Fact]
        public void DispCheckDoneProcTest_SuccessMixedBatches()
        {
            const int DIRECTION_ID = 1400;
            const int DIRECTION_ITEM_ID = 4600;
            const int STOMOVEITEM_ID1 = 14600;
            const int STOMOVEITEM_ID2 = 14601;
            const string SERIAL_NUMBER1 = "AAA";
            const string SERIAL_NUMBER2 = "BBB";
            const string SERIAL_NUMBER3 = "CCC";


            var model = new DispCheckDone()
            {
                DirectionID = DIRECTION_ID,
                SerialNumbers = new Dictionary<int, List<DispCheckDone.SerialNumberItem>>()
                {
                    {
                        DIRECTION_ITEM_ID,
                        new List<DispCheckDone.SerialNumberItem>()
                        {
                            new DispCheckDone.SerialNumberItem() { SerialNumber = SERIAL_NUMBER1, BatchNum = "BATCH1", ExpirationDate = new DateTime(2020, 1, 1) },
                            new DispCheckDone.SerialNumberItem() { SerialNumber = SERIAL_NUMBER2, BatchNum = "BATCH2", ExpirationDate = new DateTime(2020, 2, 2) },
                            new DispCheckDone.SerialNumberItem() { SerialNumber = SERIAL_NUMBER3, BatchNum = "BATCH2", ExpirationDate = new DateTime(2020, 2, 2) },
                        }
                    }
                }
            };

            var proc = new DispCheck.DispCheckDoneProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var direction = db.FirstOrDefault<Direction>("where directionid = @0", DIRECTION_ID);
                Assert.True(direction.DocStatus == Direction.DR_STATUS_DISP_CHECK_DONE);

                var storedSerials1 = db.Fetch<StoreMoveSerials>(new NPoco.Sql().Where($"{nameof(StoreMoveSerials.StoMoveItemID)} = @0", STOMOVEITEM_ID1));
                Assert.Collection(storedSerials1, item => Assert.Equal(SERIAL_NUMBER1, item.SerialNumber));
                var storedSerials2 = db.Fetch<StoreMoveSerials>(new NPoco.Sql().Where($"{nameof(StoreMoveSerials.StoMoveItemID)} = @0", STOMOVEITEM_ID2)).OrderBy(_ => _.SerialNumber);
                Assert.Collection(storedSerials2, item => Assert.Equal(SERIAL_NUMBER2, item.SerialNumber), item => Assert.Equal(SERIAL_NUMBER3, item.SerialNumber));

                var hist = db.Fetch<DirectionHistory>(new NPoco.Sql().Where("directionid = @0", DIRECTION_ID));
                Assert.True(hist.Count > 0);
                var hist2 = hist.OrderByDescending(h => h.DirectionID).First();
                Assert.Equal(DIRECTION_ID, hist2.DirectionID);
                Assert.Equal(Direction.DR_STATUS_DISP_CHECK_DONE, hist2.EventCode.Trim());
                Assert.Equal(USER_ID, hist2.EntryUserID.Trim());
                Assert.True(hist2.EntryDateTime < DateTime.Now);

                //messageBus
                var messageBus = db.Fetch<MessageBus>(new NPoco.Sql().Where("DirectionID = @0", DIRECTION_ID));
                Assert.Equal(2, messageBus.Count);
                Assert.Equal(MessageBus.MessageBusTypeEnum.ExportDirection, messageBus[0].MessageType);
                Assert.Equal(MessageBus.MessageBusTypeEnum.ExportABRA, messageBus[1].MessageType);
            }
        }

        [Fact]
        public void DispCheckDoneProcTest_SerialNumbersBadCount()
        {
            const int DIRECTION_ID = 1401;
            const int DIRECTION_ITEM_ID = 4601;


            var model = new DispCheckDone()
            {
                DirectionID = DIRECTION_ID,
                SerialNumbers = new Dictionary<int, List<DispCheckDone.SerialNumberItem>>()
                {
                    {
                        DIRECTION_ITEM_ID,
                        new List<DispCheckDone.SerialNumberItem>()
                        {
                            new DispCheckDone.SerialNumberItem() { SerialNumber = "AAA", BatchNum = "BATCH1", ExpirationDate = new DateTime(2020, 1, 1) },
                            new DispCheckDone.SerialNumberItem() { SerialNumber = "BBB", BatchNum = "BATCH2", ExpirationDate = new DateTime(2020, 2, 2) },
                            new DispCheckDone.SerialNumberItem() { SerialNumber = "CCC", BatchNum = "BATCH2", ExpirationDate = new DateTime(2020, 2, 2) },
                        }
                    }
                }
            };

            var proc = new DispCheck.DispCheckDoneProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.False(res.Success);
            Assert.Contains("Množství sériových čísel", res.ErrorText);
        }

        [Fact]
        public void DispCheckDoneProcTest_SerialNumbers()
        {
            const int DIRECTION_ID = 1401;
            const int DIRECTION_ITEM_ID = 4601;
            const int STOMOVEITEM_ID1 = 14602;
            const int STOMOVEITEM_ID2 = 14603;

            const string SERIAL_NUMBER1 = "AAA";
            const string SERIAL_NUMBER2 = "BBB";
            const string SERIAL_NUMBER3 = "CCC";
            const string SERIAL_NUMBER4 = "DDD";


            var model = new DispCheckDone()
            {
                DirectionID = DIRECTION_ID,
                SerialNumbers = new Dictionary<int, List<DispCheckDone.SerialNumberItem>>()
                {
                    {
                        DIRECTION_ITEM_ID,
                        new List<DispCheckDone.SerialNumberItem>()
                        {
                            new DispCheckDone.SerialNumberItem() { SerialNumber = SERIAL_NUMBER1, BatchNum = "BATCH1", ExpirationDate = new DateTime(2022, 2, 2) },
                            new DispCheckDone.SerialNumberItem() { SerialNumber = SERIAL_NUMBER3, BatchNum = "BATCH1", ExpirationDate = new DateTime(2022, 2, 2) },
                            new DispCheckDone.SerialNumberItem() { SerialNumber = SERIAL_NUMBER4, BatchNum = "BATCH1", ExpirationDate = new DateTime(2022, 2, 2) },
                            new DispCheckDone.SerialNumberItem() { SerialNumber = SERIAL_NUMBER2, BatchNum = "BATCH2", ExpirationDate = new DateTime(2022, 2, 2) },
                        }
                    }
                }
            };

            var proc = new DispCheck.DispCheckDoneProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var direction = db.FirstOrDefault<Direction>("where directionid = @0", DIRECTION_ID);
                Assert.True(direction.DocStatus == Direction.DR_STATUS_DISP_CHECK_DONE);

                var storedSerials1 = db.Fetch<StoreMoveSerials>(new NPoco.Sql().Where($"{nameof(StoreMoveSerials.StoMoveItemID)} = @0", STOMOVEITEM_ID1));
                Assert.Equal(3, storedSerials1.Count);
                var storedSerials2 = db.Fetch<StoreMoveSerials>(new NPoco.Sql().Where($"{nameof(StoreMoveSerials.StoMoveItemID)} = @0", STOMOVEITEM_ID2)).OrderBy(_ => _.SerialNumber).ToList();
                Assert.Single(storedSerials2);

                var hist = db.Fetch<DirectionHistory>(new NPoco.Sql().Where("directionid = @0", DIRECTION_ID));
                Assert.True(hist.Count > 0);
                var hist2 = hist.OrderByDescending(h => h.DirectionID).First();
                Assert.Equal(DIRECTION_ID, hist2.DirectionID);
                Assert.Equal(Direction.DR_STATUS_DISP_CHECK_DONE, hist2.EventCode.Trim());
                Assert.Equal(USER_ID, hist2.EntryUserID.Trim());
                Assert.True(hist2.EntryDateTime < DateTime.Now);

                ////messageBus
                var messageBus = db.Fetch<MessageBus>(new NPoco.Sql().Where("DirectionID = @0", DIRECTION_ID));
                Assert.Equal(2, messageBus.Count);
                Assert.Equal(MessageBus.MessageBusTypeEnum.ExportDirection, messageBus[0].MessageType);
                Assert.Equal(MessageBus.MessageBusTypeEnum.ExportABRA, messageBus[1].MessageType);
            }
        }
    }
}

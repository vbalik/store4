﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;

namespace S4.Procedures.Tests
{
    [Collection("StockTakingTests")]
    [Trait("Category", "StockTakingTests")]
    public class StockTakingSaveTest : ProcTestBase
    {
        private int STOTAK_ID = 4;

        public StockTakingSaveTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void StockTakingSaveTest_Success()
        {
            var model = new ProcedureModels.StockTaking.StockTakingSave()
            {
                StotakID = STOTAK_ID
            };

            using (var db = GetDB())
            {
                var proc = new StockTaking.StockTakingSaveProc(USER_ID);
                var res = proc.DoProcedure(model);
                Assert.True(res.Success, res.ErrorText);

                var stockTaking = db.SingleOrDefaultById<Entities.StockTaking>(STOTAK_ID);
                Assert.True(stockTaking.StotakStatus == Entities.StockTaking.STOCKTAKING_STATUS_SAVED);
                
                // history
                var hist = db.Fetch<StockTakingHistory>(new NPoco.Sql().Where("stotakID = @0", model.StotakID));
                Assert.True(hist.Count > 0);
                var hist2 = hist.OrderByDescending(h => h.StotakHistID).First();
                Assert.Equal(STOTAK_ID, hist2.StotakID);
                Assert.Equal(StockTakingHistory.STOCKTAKING_EVENT_SAVED, hist2.EventCode.Trim());
                Assert.Equal(USER_ID, hist2.EntryUserID.Trim());
                Assert.True(hist2.EntryDateTime < DateTime.Now);

                var stockTakingPositions = db.Fetch<Entities.StockTakingPosition>("where stotakID = @0", model.StotakID);
                foreach (var pos in stockTakingPositions)
                {
                    var position = db.SingleById<Entities.Position>(pos.PositionID);
                    Assert.True(position.PosStatus == Position.POS_STATUS_OK);
                }
            }
            
        }


        [Fact]
        public void StockTakingDeletePosItemTest_Error()
        {
            var model = new ProcedureModels.StockTaking.StockTakingSave()
            {
                StotakID = -1
            };

            var proc = new StockTaking.StockTakingSaveProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.False(res.Success);
            Assert.Equal("StockTaking nebylo nalezeno; StotakID: -1", res.ErrorText);
        }
    }
}

﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Admin;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class StoreMoveItemChangeProcTest : ProcTestBase
    {
        const int ST_MOVE_ID = 3;
        const int ST_MOVE_ITEM_ID = 1;
       

        public StoreMoveItemChangeProcTest(DatabaseFixture fixture) : base(fixture) 
        { }


        [Fact]
        public void StoreMoveItemChangeProcTest_Success()
        {
            var model = new StoreMoveItemChange()
            {
                StoMoveItemID = 1,
                CarrierNum = "1",
                Quantity = 1,
                ArtPackID = 1001
            };

            StoreMoveItem storeMoveItemOld = null;
            using (var db = GetDB())
            {
                storeMoveItemOld = db.SingleOrDefault<StoreMoveItem>("where stoMoveItemID = @0", model.StoMoveItemID);
            }

            var proc = new Admin.StoreMoveItemChangeProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            
            using (var db = GetDB())
            {
                var storeMoveItem = db.SingleOrDefault<StoreMoveItem>("where stoMoveItemID = @0", model.StoMoveItemID);
                Assert.True(storeMoveItem.Quantity == model.Quantity);
                Assert.True(storeMoveItem.CarrierNum == model.CarrierNum);

                var hist = db.Fetch<StoreMoveHistory>(new NPoco.Sql().Where("stoMoveID = @0", ST_MOVE_ID));
                Assert.True(hist.Count > 0);
                var hist2 = hist.OrderByDescending(h => h.StoMoveID).First();
                Assert.Equal(StoreMove.MO_EVENT_ITEM_CHANGED, hist2.EventCode.Trim());
                Assert.Equal("{\"changes\":[{\"property\":\"Quantity\",\"originValue\":\"1.0000\",\"currentValue\":\"1\"},{\"property\":\"CarrierNum\",\"originValue\":\"0\",\"currentValue\":\"1\"}]}", hist2.EventData.Trim());
                Assert.Equal(USER_ID, hist2.EntryUserID.Trim());
                Assert.True(hist2.EntryDateTime <= DateTime.Now);

                var msg = db.Fetch<InternMessage>(new NPoco.Sql().Where("msgclass = @0", InternMessage.MO_ITEM_CHANGED));
                Assert.True(msg.Count > 0);
                var msg2 = msg.OrderByDescending(h => h.IntMessID).First();
                Assert.Equal(InternMessage.MessageSeverityEnum.High, msg2.MsgSeverity);
                Assert.Equal("Změna položky dokladu XXX2/1 (StoMoveID: 3)", msg2.MsgHeader);
                Assert.True(msg2.MsgDateTime <= DateTime.Now);

                var propertiesComparer = new Core.PropertiesComparer.PropertiesComparer<StoreMoveItem>();
                propertiesComparer.DoCompare<StoreMoveItemChange>(storeMoveItemOld, model, nameof(model.Quantity), nameof(model.CarrierNum));

                Assert.Equal(propertiesComparer.JsonText, msg2.MsgText);
            }            
        }


        [Fact]
        public void StoreMoveItemChangeProcTest_Error()
        {
            var model = new StoreMoveItemChange()
            {
                StoMoveItemID = -1,
                CarrierNum = "-1",
                Quantity = -1,
                ArtPackID = 1001
            };

            var proc = new Admin.StoreMoveItemChangeProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.False(res.Success);
            Assert.Equal("StoreMoveItem nebylo nalezeno; StoMoveItemID: -1", res.ErrorText);
        }
    }
}

﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Admin;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class DirectionSetDispSectionProcTest : ProcTestBase
    {
        const int DIRECTION_ID = 4;
       

        public DirectionSetDispSectionProcTest(DatabaseFixture fixture) : base(fixture) 
        { }


        [Fact]
        public void DirectionSetDispSectionProcTest_Success()
        {
            var model = new DirectionSetDispSection()
            {
                DirectionID = DIRECTION_ID,
                ChangeReason = "Test",
                SectID = "Test SectID 1234"
            };

            var proc = new Admin.DirectionSetDispSectionProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            
            using (var db = GetDB())
            {
                var direction = db.FirstOrDefault<Direction>("where directionid = @0", DIRECTION_ID);
                Assert.True(direction.DocStatus == DirectionItem.DI_STATUS_LOAD, direction.DocStatus);

                var hist = db.Fetch<DirectionHistory>(new NPoco.Sql().Where("directionid = @0", model.DirectionID));
                Assert.True(hist.Count > 0);
                var hist2 = hist.OrderByDescending(h => h.DirectionID).First();
                Assert.Equal(DIRECTION_ID, hist2.DirectionID);
                Assert.Equal(Direction.DR_EVENT_DISP_SECT_CHANGE, hist2.EventCode.Trim());
                Assert.Equal($"Direction Set Disp Section, DirectionID: '{direction.DirectionID}'", hist2.EventData.Trim());
                Assert.Equal(USER_ID, hist2.EntryUserID.Trim());
                Assert.True(hist2.EntryDateTime < DateTime.Now);

                //test xtraData
                var xtraData = new Entities.XtraData.XtraDataProxy<Entities.DirectionXtra>(Direction.XtraDataDict);
                xtraData.Load(db, new NPoco.Sql().Where("DirectionID = @0", DIRECTION_ID));
                Assert.NotEmpty(xtraData.Data);
                Assert.Equal(model.SectID, xtraData[Direction.XTRA_DISPATCH_DIRECT_SECTION]);
            }
            
        }


        [Fact]
        public void DirectionSetDispSectionProcTest_Error()
        {
            var model = new DirectionSetDispSection()
            {
                DirectionID = -1,
                ChangeReason = "X",
                SectID = "X"
            };

            var proc = new Admin.DirectionSetDispSectionProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
        }
    }
}

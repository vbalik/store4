﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Exped;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class ExpedDoneTest : ProcTestBase
    {
        const int DIRECTION_ID_DCHE = 10;
        const int DIRECTION_ID_MADEL = 1001;
        const int DIRECTION_DONOTPROCESS = 1002;

        public ExpedDoneTest(DatabaseFixture fixture) : base(fixture) 
        { }

        [Theory]
        [InlineData(DIRECTION_ID_DCHE, 13)]
        [InlineData(DIRECTION_ID_MADEL, 1301)]
        public void ExpedDoneTest_Success(int directionID, int positionID)
        {
            var model = new ExpedDone()
            {
                DirectionID = directionID,
                ExpedTruck = "Exped"
            };

            var proc = new Exped.ExpedDoneProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            Assert.True(res.StoMoveID != 0);

            using (var db = GetDB())
            {
                // status
                var direction = db.FirstOrDefault<Direction>("where directionid = @0", directionID);
                Assert.Equal(Direction.DR_STATUS_DISP_EXPED, direction.DocStatus);

                // history
                var hist = db.Fetch<DirectionHistory>(new NPoco.Sql().Where("directionid = @0", directionID));
                Assert.True(hist.Count > 0);
                var hist2 = hist.OrderByDescending(h => h.DirectionID).First();
                Assert.Equal(directionID, hist2.DirectionID);
                Assert.Equal(Direction.DR_STATUS_DISP_EXPED, hist2.EventCode.Trim());
                Assert.Equal(model.ExpedTruck, hist2.EventData.Trim());
                Assert.Equal(USER_ID, hist2.EntryUserID.Trim());
                Assert.True(hist2.EntryDateTime < DateTime.Now);

                // test xtraData
                var xtraData = new Entities.XtraData.XtraDataProxy<Entities.DirectionXtra>(Direction.XtraDataDict);
                xtraData.Load(db, new NPoco.Sql().Where("DirectionID = @0", directionID));
                Assert.NotEmpty(xtraData.Data);
                Assert.Equal(model.ExpedTruck, xtraData[Direction.XTRA_EXPED_TRUCK]);


                // move
                var move = db.FirstOrDefault<StoreMove>(new NPoco.Sql().Where("stoMoveID = @0", res.StoMoveID));
                Assert.NotNull(move);
                Assert.Equal(directionID, move.Parent_directionID);
                Assert.Equal(StoreMove.MO_DOCTYPE_EXPED, move.DocType);
                Assert.Equal(StoreMove.MO_STATUS_SAVED_EXPED, move.DocStatus);
                Assert.Equal(StoreMove.MO_PREFIX_EXPED, move.DocNumPrefix);

                var moveItems = db.Fetch<StoreMoveItem>(new NPoco.Sql().Where("stoMoveID = @0", res.StoMoveID)).OrderBy(i => i.ItemDirection).ToList();
                Assert.NotNull(moveItems);
                Assert.Single(moveItems);

                Assert.Equal(-1, moveItems[0].ItemDirection);
                Assert.Equal(positionID, moveItems[0].PositionID);
                Assert.Equal(-1, moveItems[0].Parent_docPosition);
                Assert.Equal(1, moveItems[0].Quantity);
                Assert.Equal(StoreMoveItem.MI_VALIDITY_VALID, moveItems[0].ItemValidity);



                // store out
                var onStore = (new ServicesFactory()).GetOnStore();
                var status = onStore.OnPosition(positionID, StoreCore.Interfaces.ResultValidityEnum.Valid);
                Assert.Empty(status);

                //messageBus
                var messageBus = db.Fetch<MessageBus>(new NPoco.Sql().Where("DirectionID = @0", directionID));
                Assert.Equal(2, messageBus.Count);
                Assert.Equal(MessageBus.MessageBusTypeEnum.ExportDirection, messageBus[0].MessageType);
                Assert.Equal(MessageBus.MessageBusTypeEnum.ExportABRA, messageBus[1].MessageType);
            }
        }


        [Fact]
        public void ExpedDoneTest_Error()
        {
            var model = new ExpedDone()
            {
               DirectionID = -1
            };

            var proc = new Exped.ExpedDoneProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.False(res.Success);
            Assert.Equal("Doklad nebyl nalezen; DirectionID: -1", res.ErrorText);
        }

        [Theory]
        [InlineData(DIRECTION_DONOTPROCESS, 0)]
        public void ExpedDoneDoNotProcessTest_Success(int directionID, int positionID)
        {
            var model = new ExpedDone()
            {
                DirectionID = directionID,
                ExpedTruck = "Exped"
            };

            var proc = new Exped.ExpedDoneProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            Assert.True(res.StoMoveID == 0);

            
        }

    }
}

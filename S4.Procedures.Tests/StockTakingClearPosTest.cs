﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;

namespace S4.Procedures.Tests
{
    [Collection("StockTakingTests")]
    [Trait("Category", "StockTakingTests")]
    public class StockTakingClearPosTest : ProcTestBase
    {
        private int STOTAK_ID = 1;

        public StockTakingClearPosTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void StockTakingClearPosTest_Success()
        {
            var model = new ProcedureModels.StockTaking.StockTakingClearPos()
            {
                StotakID = STOTAK_ID,
                PositionID = 2
            };

            using (var db = GetDB())
            {
                // get items
                var items = db.Fetch<StockTakingItem>("where stotakID = @0 AND positionID = @1", model.StotakID, model.PositionID);
                Assert.True(items.Count > 0);
                
                var proc = new StockTaking.StockTakingClearPosProc(USER_ID);
                var res = proc.DoProcedure(model);

                Assert.True(res.Success, res.ErrorText);

                var items2 = db.Fetch<StockTakingItem>("where stotakID = @0 AND positionID = @1", model.StotakID, model.PositionID);
                Assert.True(items2.Count == 0);

                var pos = db.First<StockTakingPosition>("where stotakID = @0 AND positionID = @1", model.StotakID, model.PositionID);
                Assert.True(pos != null);
                Assert.True(pos.CheckCounter == 1);

                //his
                var hist = db.Fetch<StockTakingHistory>(new NPoco.Sql().Where("stotakID = @0", model.StotakID));
                Assert.True(hist.Count > 0);
                var hist2 = hist.OrderByDescending(h => h.StotakHistID).First();
                Assert.Equal(STOTAK_ID, hist2.StotakID);
                Assert.Equal(StockTakingHistory.STOCKTAKING_EVENT_CLEAR, hist2.EventCode.Trim());
                Assert.Equal(USER_ID, hist2.EntryUserID.Trim());
                Assert.True(hist2.EntryDateTime < DateTime.Now);
            }            
        }


        [Fact]
        public void StockTakingDeletePosItemTest_BadPosition()
        {
            var model = new ProcedureModels.StockTaking.StockTakingClearPos()
            {
                StotakID = STOTAK_ID,
                PositionID = -1
            };

            var proc = new StockTaking.StockTakingClearPosProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success);
            Assert.True(res.BadPosition);
        }


        [Fact]
        public void StockTakingDeletePosItemTest_Error()
        {
            var model = new ProcedureModels.StockTaking.StockTakingClearPos()
            {
                StotakID = -1,
                PositionID = 2
            };

            var proc = new StockTaking.StockTakingClearPosProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal("StockTaking nebylo nalezeno; StotakID: -1", res.ErrorText);
        }
    }
}

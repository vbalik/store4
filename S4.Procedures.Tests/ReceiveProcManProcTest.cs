﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Receive;

namespace S4.Procedures.Tests
{
    [Collection("ReceiveTests")]
    [Trait("Category", "ReceiveTests")]
    public class ReceiveProcManProcTest : ProcTestBase
    {
        public ReceiveProcManProcTest(DatabaseFixture fixture) : base(fixture)
        { }

        [Fact]
        public void ReceiveProcManProcTest_Success()
        {
            var model = new ReceiveProcMan()
            {
                WorkerID = "Test1"
            };

            var proc = new Receive.ReceiveProcManProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var direction = db.FirstOrDefault<Direction>("where directionid = @0", res.DirectionID);
                Assert.NotNull(direction);
                Assert.True(direction.DocStatus == Direction.DR_STATUS_RECEIVE_PROC);

                var directionAssigns = db.Fetch<DirectionAssign>("where directionid = @0 AND jobID = @1", res.DirectionID, DirectionAssign.JOB_ID_RECEIVE);
                Assert.True(directionAssigns.Count == 1);

                var moveHist = db.Fetch<StoreMoveHistory>(new NPoco.Sql().Where("stoMoveID = @0", res.StoMoveID));
                Assert.True(moveHist.Count > 0);
                var moveHist2 = moveHist.OrderByDescending(h => h.StoMoveID).First();
                Assert.Equal(res.StoMoveID, moveHist2.StoMoveID);
                Assert.Equal(StoreMove.MO_STATUS_NEW_RECEIVE, moveHist2.EventCode.Trim());
                Assert.Equal(USER_ID, moveHist2.EntryUserID.Trim());
                Assert.True(moveHist2.EntryDateTime < DateTime.Now);

                Assert.NotNull(res.Items);
            }


            // add items
            var saveProc = new Receive.ReceiveSaveItemProc(USER_ID);
            var saveRes = saveProc.DoProcedure(new ReceiveSaveItem() { StoMoveID = res.StoMoveID, ArtPackID = 1012, BatchNum = "zzz", CarrierNum = "ccccc", ExpirationDate = null, Quantity = 1, PositionID = 23 });


            var proc2 = new Receive.ReceiveProcManProc(USER_ID);
            var res2 = proc2.DoProcedure(model);

            Assert.NotNull(res2.Items);
            Assert.NotEmpty(res2.Items);
            Assert.Equal(1, res2.Items[0].DocPosition);
            Assert.NotEqual(0, res2.Items[0].StoMoveID);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var direction = db.FirstOrDefault<Direction>("where directionid = @0", res.DirectionID);
                direction.XtraData.Load(db, new NPoco.Sql().Where("DirectionID = @0", direction.DirectionID));
                var cooled = direction.XtraData[Direction.XTRA_ARTICLE_COOLED, 1];
                Assert.NotNull(cooled);
                Assert.True((bool)cooled);
            }
        }

        [Fact]
        public void ReceiveProcManProcTest_Error()
        {
            var model = new ReceiveProcMan();

            var proc = new Receive.ReceiveProcManProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal("Parametry procedury nejsou platné WorkerID IsNull", res.ErrorText.Trim());
        }
    }
}

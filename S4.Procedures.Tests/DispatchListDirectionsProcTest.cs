﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Dispatch;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class DispatchListDirectionsProcTest : ProcTestBase
    {
        const string WORKER_ID = "WRK1";


        public DispatchListDirectionsProcTest(DatabaseFixture fixture) : base(fixture) 
        { }


        [Fact]
        public void ListDirectionsProc_Success()
        {
            var model = new DispatchListDirections()
            {
                WorkerID = WORKER_ID
            };

            var proc = new Dispatch.DispatchListDirectionsProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            Assert.Equal(3, res.Directions.Count());
            Assert.Equal(5, res.Directions[0].DocumentID);
            Assert.Equal(6, res.Directions[1].DocumentID);
        }
    }
}

﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;

namespace S4.Procedures.Tests
{
    [Collection("StockTakingTests")]
    [Trait("Category", "StockTakingTests")]
    public class StockTakingBeginArticleTest : ProcTestBase
    {

        public StockTakingBeginArticleTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void StockTakingBeginArticleTest_Success()
        {
            var model = new ProcedureModels.StockTaking.StockTakingBeginArticle()
            {
                ArticleID = 100
            };
            var proc = new StockTaking.StockTakingBeginArticleProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);

            using (var db = GetDB())
            {
                var stotak = db.Single<Entities.StockTaking>("where stotakID = @0", res.StotakID);
                Assert.NotNull(stotak);
                Assert.Equal(USER_ID, stotak.EntryUserID);
                Assert.Equal("Inventura karty 'X123 : Zboží 123'", stotak.StotakDesc);
                Assert.Equal(Entities.StockTaking.STOCKTAKING_STATUS_OPENED, stotak.StotakStatus);
                Assert.True(stotak.BeginDate <= DateTime.Now);
                Assert.Null(stotak.EndDate);


                var positions = db.Fetch<StockTakingPosition>("where stotakID = @0", res.StotakID);
                Assert.NotNull(positions);
                Assert.NotEmpty(positions);
                Assert.All(positions, i => 
                {
                    var pos = db.Single<Entities.Position>("where positionID = @0", i.PositionID);
                    Assert.Equal(Position.POS_STATUS_STOCKTAKING, pos.PosStatus);
                });
            }
        }


        [Fact]
        public void StockTakingBeginArticleTest_Error()
        {
            var model = new ProcedureModels.StockTaking.StockTakingBeginArticle()
            {
                ArticleID = 200
            };
            var proc = new StockTaking.StockTakingBeginArticleProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.False(res.Success, res.ErrorText);
            Assert.Equal("Na pozici 'OnStore21' jsou neuložené pohyby.", res.ErrorText);
        }
    }
}

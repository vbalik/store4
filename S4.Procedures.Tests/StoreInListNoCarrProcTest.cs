﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.StoreIn;

namespace S4.Procedures.Tests
{
    [Collection("StoreInTests")]
    [Trait("Category", "StoreInTests")]
    public class StoreInListNoCarrProcTest : ProcTestBase
    {
        public StoreInListNoCarrProcTest(DatabaseFixture fixture) : base(fixture)
        { }

        [Fact]
        public void StoreInListNoCarrProcTest_Success()
        {
            var model = new StoreInListNoCarr()
            {
                DirectionID = 16,
                SrcPositionID = 27
            };

            var proc = new StoreIn.StoreInListNoCarrProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            Assert.NotNull(res.ArticleList);
            Assert.True(res.ArticleList.Count > 0);
        }

        [Fact]
        public void StoreInListNoCarrProcTest_Error()
        {
            var model = new StoreInListNoCarr();

            var proc = new StoreIn.StoreInListNoCarrProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal("Parametry procedury nejsou platné SrcPositionID IsNull , DirectionID IsNull", res.ErrorText.Trim());
        }
    }
}

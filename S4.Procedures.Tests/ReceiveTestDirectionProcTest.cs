﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Receive;

namespace S4.Procedures.Tests
{
    [Collection("ReceiveTests")]
    [Trait("Category", "ReceiveTests")]
    public class ReceiveTestDirectionProcTest : ProcTestBase
    {
        public ReceiveTestDirectionProcTest(DatabaseFixture fixture) : base(fixture)
        { }

        [Fact]
        public void ReceiveTestDirectionProcTest_Success()
        {
            var model = new ReceiveTestDirection()
            {
                DirectionID = 26
            };

            var proc = new Receive.ReceiveTestDirectionProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var direction = db.FirstOrDefault<Direction>(new NPoco.Sql().Where("directionID = @0", model.DirectionID));
                Assert.Equal(Direction.DR_STATUS_RECEIVE_COMPLET, direction.DocStatus);

                var directionItems = db.Fetch<DirectionItem>(new NPoco.Sql().Where("directionID = @0", model.DirectionID));
                Assert.NotEmpty(directionItems);
                Assert.All(directionItems, i => Assert.Equal(DirectionItem.DI_STATUS_RECEIVE_COMPLET, i.ItemStatus));
            }
        }

        [Fact]
        public void ReceiveTestDirectionProcTest_Error()
        {
            var model = new ReceiveTestDirection()
            {
                DirectionID = int.MaxValue
            };

            var proc = new Receive.ReceiveTestDirectionProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal($"Direction nebylo nalezeno; DirectionID: {model.DirectionID}", res.ErrorText.Trim());
        }
    }
}

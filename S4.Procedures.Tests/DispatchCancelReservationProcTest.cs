﻿using S4.Entities;
using S4.ProcedureModels.Dispatch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestHelpers;
using Xunit;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class DispatchCancelReservationProcTest : ProcTestBase
    {
        const int DIRECTION_ID = 21;


        public DispatchCancelReservationProcTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void DispatchCancelReservationProcTest_Success()
        {
            var model = new DispatchCancelReservation()
            {
                DirectionID = DIRECTION_ID,
            };


            var proc = new Dispatch.DispatchCancelReservationProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var direction = db.FirstOrDefault<Direction>("where directionid = @0", DIRECTION_ID);
                Assert.Equal(Direction.DR_STATUS_DISPATCH_WAIT, direction.DocStatus);

                var hist = db.Fetch<DirectionHistory>(new NPoco.Sql().Where("directionid = @0", DIRECTION_ID));
                Assert.True(hist.Count > 0);
                var hist2 = hist.OrderByDescending(h => h.DirectionID).First();
                Assert.Equal(DIRECTION_ID, hist2.DirectionID);
                Assert.Equal(Direction.DR_EVENT_RESERVATION_CANCELED, hist2.EventCode.Trim());
                Assert.Equal(USER_ID, hist2.EntryUserID.Trim());
                Assert.True(hist2.EntryDateTime <= DateTime.Now);
            }
        }


        [Fact]
        public void DispatchCancelReservationProcTest_Error()
        {
            var model = new DispatchWait()
            {
                DirectionID = -1,
                DestPositionID = -1,
                WorkerID = "-1"
            };

            var proc = new Dispatch.DispatchWaitProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal("Doklad nebyl nalezen; DirectionID: -1", res.ErrorText);
        }
    }
}

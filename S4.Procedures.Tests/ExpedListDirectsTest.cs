﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Dispatch;
using S4.ProcedureModels.Exped;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class ExpedListDirectsTest : ProcTestBase
    {
              
        public ExpedListDirectsTest(DatabaseFixture fixture) : base(fixture) 
        { }


        [Fact]
        public void ExpedListDirectsTest_Success()
        {
            var model = new ExpedListDirects();
            var proc = new Exped.ExpedListDirectsProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            Assert.True(res.Directions.Count > 0, res.ErrorText);

        }


        
              
    }
}

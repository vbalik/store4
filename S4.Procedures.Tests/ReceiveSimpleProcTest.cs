﻿using NPoco;
using S4.Entities;
using S4.ProcedureModels.Receive;
using System;
using TestHelpers;
using Xunit;

namespace S4.Procedures.Tests
{
    [Collection("ReceiveTests")]
    [Trait("Category", "ReceiveTests")]
    public class ReceiveSimpleProcTest : ProcTestBase
    {
        public ReceiveSimpleProcTest(DatabaseFixture fixture) : base(fixture)
        { }

        [Fact]
        public void ReceiveSimpleProcTest_Success()
        {
            //Arrange
            const int ARTICLEID_FROM_POSTDEPLOY_DATA = 127;
            const int SPECIAL_CATEGORY_POSITION_ID = 125;

            var procModel = new ReceiveSimple()
            {
                ArticleID = ARTICLEID_FROM_POSTDEPLOY_DATA,
                Quantity = 4,
                ExpirationDate = "05.05.2024",
                PositionID = SPECIAL_CATEGORY_POSITION_ID,
                DocumentRemark = "Testing only",
            };

            var proc = new Receive.ReceiveSimpleProc(USER_ID);

            //Act
            var response = proc.DoProcedure(procModel);

            int storeMoveIDFromDb = 0;
            using (var db = GetDB())
            {
                var moves = db.Fetch<StoreMove>(new NPoco.Sql().Where("stoMoveID = @0", response.StoreMoveID));
                storeMoveIDFromDb = moves[0].StoMoveID;
            }

            //Assert
            Assert.NotNull(response);
            Assert.True(response.Success, response.ErrorText);
            Assert.True(response.StoreMoveID > 1);
            Assert.Equal(response.StoreMoveID, storeMoveIDFromDb);
        }

        [Fact]
        public void ReceiveSimpleProcTest_Fail()
        {
            //Arrange
            const int ARTICLEID_FROM_POSTDEPLOY_DATA = 127;
            const int NON_SPECIAL_CATEGORY_POSITION_ID = 126;
            const string EXPECTED_EXCEPTION_MESSAGE = "Position is not Receive position";
            var proc = new Receive.ReceiveSimpleProc(USER_ID);
            var procModel = new ReceiveSimple()
            {
                ArticleID = ARTICLEID_FROM_POSTDEPLOY_DATA,
                Quantity = 4,
                ExpirationDate = "05.05.2024",
                PositionID = NON_SPECIAL_CATEGORY_POSITION_ID,
                DocumentRemark = "Testing only",
            };

            //Act
            var ex = Record.Exception(() => {
                proc.DoProcedure(procModel);
            });
            
            //Assert
            Assert.NotNull(ex);
            Assert.IsType<ArgumentException>(ex);
            Assert.Equal(EXPECTED_EXCEPTION_MESSAGE, ex.Message);
        }

        [Fact]
        public void ReceiveSimpleProcTest_IncompleteInputData_Fail()
        {
            //Arrange
            const int ARTICLEID_FROM_POSTDEPLOY_DATA = 127;
            const int SPECIAL_CATEGORY_POSITION_ID = 125;
            var proc = new Receive.ReceiveSimpleProc(USER_ID);
            var procModelWithMissingValues = new ReceiveSimple()
            {
                PositionID = SPECIAL_CATEGORY_POSITION_ID,
                DocumentRemark = "Testing only",
            };

            //Act
            var response = proc.DoProcedure(procModelWithMissingValues);

            //Assert
            Assert.NotNull(response);
            Assert.False(response.Success, response.ErrorText);
            Assert.Contains("Parametry procedury nejsou platné", response.ErrorText);
        }

        [Fact]
        public void ReceiveSimpleProcTest_NegativeIDsInData_Fail()
        {
            //Arrange
            const int ARTICLEID_FROM_POSTDEPLOY_DATA = 127;
            const int SPECIAL_CATEGORY_POSITION_ID = 125;
            const int NEGATIVE_QUANTITY = -5;
            var proc = new Receive.ReceiveSimpleProc(USER_ID);
            var procModel = new ReceiveSimple()
            {
                ArticleID = ARTICLEID_FROM_POSTDEPLOY_DATA,
                Quantity = NEGATIVE_QUANTITY,
                ExpirationDate = "05.05.2024",
                PositionID = SPECIAL_CATEGORY_POSITION_ID,
                DocumentRemark = "Testing only",
            };

            //Act
            var response = proc.DoProcedure(procModel);

            //Assert
            Assert.NotNull(response);
            Assert.False(response.Success, response.ErrorText);
            Assert.Contains("Mnozstvi nesmi byt mensi nez 1", response.ErrorText);
        }
    }
}

﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Dispatch;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class DispatchPrintLabelTest : ProcTestBase
    {
        
        public DispatchPrintLabelTest(DatabaseFixture fixture) : base(fixture) 
        { }


        [Fact]
        public void DispatchPrintLabelSMallTest_Success()
        {
            var model = new DispatchPrintLabel()
            {
                DirectionID = 1,
                Count = 1,
                PrinterLocationID = 2,
                TypePrint = TypePrint.MALE_STITKY
            };
            var proc = new Dispatch.DispatchPrintLabelProc(USER_ID) { IsTesting = true };
            var res = proc.DoProcedure(model);
            Assert.True(res.Success);
            Assert.Equal("PRINT S4.Entities.Models.DirectionModel", proc.RenderResult);

        }

        [Fact]
        public void DispatchPrintLabelBigTest_Success()
        {
            var model = new DispatchPrintLabel()
            {
                DirectionID = 1,
                Count = 1,
                PrinterLocationID = 2,
                TypePrint = TypePrint.VELKE_STITKY
            };
            var proc = new Dispatch.DispatchPrintLabelProc(USER_ID) { IsTesting = true };
            var res = proc.DoProcedure(model);
            Assert.True(res.Success);
            Assert.Equal("PRINT S4.Entities.Models.DirectionModel", proc.RenderResult);

        }
    }
}

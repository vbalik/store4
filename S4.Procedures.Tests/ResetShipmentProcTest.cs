﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Delivery;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class ResetShipmentProcTest : ProcTestBase
    {
        private const int _DIRECTION_ID = 58;

        public ResetShipmentProcTest(DatabaseFixture fixture) : base(fixture)
        { }

        [Fact]
        public void ResetShipmentProc_MissingParams()
        {
            var resetShipment = new ResetShipment();
            resetShipment = new Procedures.Delivery.ResetShipmentProc(USER_ID) { IsTesting = true }.DoProcedure(resetShipment);
            Assert.False(resetShipment.Success);
        }

        [Fact]
        public void ResetShipmentProc_Success()
        {
            var resetShipment = new ResetShipment()
            {
                DirectionID = _DIRECTION_ID,
                BoxesCnt = 2,
                Weight = 2.5m
            };

            var result = new Procedures.Delivery.ResetShipmentProc(USER_ID) { IsTesting = true }.DoProcedure(resetShipment);

            //check success
            Assert.True(result.Success);
            //check status
            var status = new DAL.DirectionDAL().GetStatus(_DIRECTION_ID);
            Assert.Equal(Direction.DR_STATUS_DISP_CHECK_DONE, status);
            //check history
            var history = new DAL.DirectionDAL().GetDirectionHistory(_DIRECTION_ID).Where(i => i.EventCode == Direction.DR_STATUS_DISP_CHECK_DONE).FirstOrDefault();
            Assert.NotNull(history);
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //check DeliveryDirection
                var deliveryDirections = new DAL.DeliveryDirectionDAL().GetAllRowsByDirectionID(_DIRECTION_ID, db);
                var deliveryDirection = deliveryDirections.First();

                Assert.NotNull(deliveryDirection);
                Assert.Equal(_DIRECTION_ID.ToString(), deliveryDirection.ShipmentRefNumber);
                Assert.Equal(DeliveryDirection.DD_STATUS_CANCELED, deliveryDirection.DeliveryStatus);

                //check BoxesCnt and Weight
                var direction = new DAL.DirectionDAL().GetDirectionAllData(_DIRECTION_ID);
                Assert.NotNull(direction);
                Assert.Equal(_DIRECTION_ID.ToString(), deliveryDirection.ShipmentRefNumber);

                Assert.Equal(direction.XtraData[Direction.XTRA_DISPATCH_WEIGHT], resetShipment.Weight);
                Assert.Equal(direction.XtraData[Direction.XTRA_DISPATCH_BOXES_CNT], resetShipment.BoxesCnt);
                               
                var result1 = new Procedures.Delivery.ResetShipmentProc(USER_ID) { IsTesting = true }.DoProcedure(resetShipment);

                //check success
                Assert.False(result1.Success);
                Assert.Equal("Doklad již čeká na vytvoření požadavku na dopravu.", result1.ErrorText);

               
            }
        }
    }
}

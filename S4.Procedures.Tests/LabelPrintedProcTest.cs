﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Delivery;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests2")]
    [Trait("Category", "ProcedureTests2")]
    public class LabelPrintedProcTest : ProcTestBase
    {
        private const int _DIRECTION_ID_WRONG = 42;
        private const int _DIRECTION_ID = 43;

        public LabelPrintedProcTest(DatabaseFixture fixture) : base(fixture)
        { }

        [Fact]
        public void LabelPrintedProc_MissingParams()
        {
            var labelPrinted = new LabelPrinted();
            labelPrinted = new Procedures.Delivery.LabelPrintedProc(USER_ID).DoProcedure(labelPrinted);
            Assert.False(labelPrinted.Success);
            Assert.Equal("Parametry procedury nejsou platné DirectionID IsNull ", labelPrinted.ErrorText);
        }

        [Fact]
        public void LabelPrintedProc_BadStatus()
        {
            var labelPrinted = new LabelPrinted()
            {
                DirectionID = _DIRECTION_ID_WRONG
            };
            labelPrinted = new Procedures.Delivery.LabelPrintedProc(USER_ID).DoProcedure(labelPrinted);
            Assert.False(labelPrinted.Success);
            Assert.Equal("Direction nemá DocStatus DR_STATUS_DELIVERY_SHIPMENT; DirectionID: 42", labelPrinted.ErrorText);
        }

        [Fact]
        public void LabelPrintedProc_Success()
        {
            var labelPrinted = new LabelPrinted()
            {
                DirectionID = _DIRECTION_ID
            };
            labelPrinted = new Procedures.Delivery.LabelPrintedProc(USER_ID).DoProcedure(labelPrinted);

            //check success
            Assert.True(labelPrinted.Success);
            //check status
            var status = new DAL.DirectionDAL().GetStatus(_DIRECTION_ID);
            Assert.Equal(Direction.DR_STATUS_DELIVERY_PRINT, status);
            //check history
            var history = new DAL.DirectionDAL().GetDirectionHistory(_DIRECTION_ID).Where(i => i.EventCode == Direction.DR_STATUS_DELIVERY_PRINT).FirstOrDefault();
            Assert.NotNull(history);

            //test reprint
            labelPrinted = new LabelPrinted()
            {
                DirectionID = _DIRECTION_ID
            };
            labelPrinted = new Procedures.Delivery.LabelPrintedProc(USER_ID).DoProcedure(labelPrinted);
            //check success
            Assert.True(labelPrinted.Success);
        }
    }
}

﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Admin;
using S4.ProcedureModels.MessageBus;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class MessageBusSetStatusProcTest : ProcTestBase
    {
        const int ID = 1;
       

        public MessageBusSetStatusProcTest(DatabaseFixture fixture) : base(fixture) 
        { }


        [Fact]
        public void MessageBusSetStatusProcTest_Success()
        {
            var model = new MessageBusSetStatusToNew()
            {
               MessageBusID = ID
            };

            var proc = new S4.Procedures.MessagesBus.MessageBusSetStatusToNewProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            
            using (var db = GetDB())
            {
                var messageBus = db.SingleById<Entities.MessageBus>(ID);
                Assert.True(messageBus.MessageStatus == S4.Entities.MessageBus.NEW);
            }
            
        }
     
    }
}

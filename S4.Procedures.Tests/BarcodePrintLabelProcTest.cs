﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.BarCode;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class BarcodePrintLabelProcTest : ProcTestBase
    {

        public BarcodePrintLabelProcTest(DatabaseFixture fixture) : base(fixture) 
        { }


        [Fact]
        public void BarcodePrintLabelProc_Success()
        {
            var model = new BarcodePrintLabel()
            {
                ArtPackID = 1001,
                PrinterLocationID = 2
            };
            var proc = new Barcode.BarcodePrintLabelProc(USER_ID) { IsTesting = true };
            var res = proc.DoProcedure(model);
            Assert.True(res.Success);
            Assert.Equal("PRINT BARCODE1 ks Zbozi 123", proc.RenderResult);
        }


        [Fact]
        public void BarcodePrintLabelProc_Error()
        {
            var model = new BarcodePrintLabel()
            {
                ArtPackID = -1
            };

            var proc = new Barcode.BarcodePrintLabelProc(USER_ID) { IsTesting = true };
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
        }

    }
}

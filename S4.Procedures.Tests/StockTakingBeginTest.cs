﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;

namespace S4.Procedures.Tests
{
    [Collection("StockTakingTests")]
    [Trait("Category", "StockTakingTests")]
    public class StockTakingBeginTest : ProcTestBase
    {

        public StockTakingBeginTest(DatabaseFixture fixture) : base(fixture) 
        { }


        [Fact]
        public void StockTakingBeginTest_Success()
        {
            var model = new ProcedureModels.StockTaking.StockTakingBegin()
            {
                PositionIDs = new int[] { 14 },
                StotakDesc = "Test"
            };
            var proc = new StockTaking.StockTakingBeginProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);

            using (var db = GetDB())
            {
                var stotak = db.Single<Entities.StockTaking>("where stotakID = @0", res.StotakID);
                Assert.NotNull(stotak);
                Assert.Equal(USER_ID, stotak.EntryUserID);
                Assert.Equal("Test", stotak.StotakDesc);
                Assert.Equal(Entities.StockTaking.STOCKTAKING_STATUS_OPENED, stotak.StotakStatus);
                Assert.True(stotak.BeginDate <= DateTime.Now);
                Assert.Null(stotak.EndDate);


                var positions = db.Fetch<StockTakingPosition>("where stotakID = @0", res.StotakID);
                Assert.NotNull(positions);
                Assert.NotEmpty(positions);
                Assert.All(positions, i =>
                {
                    var pos = db.Single<Entities.Position>("where positionID = @0", i.PositionID);
                    Assert.Equal(Position.POS_STATUS_STOCKTAKING, pos.PosStatus);
                });
            }
        }


        [Fact]
        public void StockTakingBeginTest_Error()
        {
            var model = new ProcedureModels.StockTaking.StockTakingBegin()
            {
                PositionIDs = new int[] { 15 },
                StotakDesc = "Test"
            };
            var proc = new StockTaking.StockTakingBeginProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.False(res.Success);
            Assert.Equal("Na pozici 'POS_STOCKTAKING2' probíhá inventura.", res.ErrorText);
        }
    }
}

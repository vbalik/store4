﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Logistics;
using System.Collections.Generic;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class LogisticDispStoInProcTest : ProcTestBase
    {
        public LogisticDispStoInProcTest(DatabaseFixture fixture) : base(fixture)
        { }

        private class TestItem
        {
            public int ArtPackID { get; set; }
            public decimal Quantity { get; set; }
        }


        [Fact]
        public void LogisticDispatchStoInProcTest_Success()
        {
            var model = new LogisticsDispToStoIn()
            {
                DirectionID = 25,
                ReceivePositionID = 114,
                WorkerID = "WRK1"
            };

            var proc = new Logistics.LogisticsDispatchToStoInProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //output side
                var direction = db.FirstOrDefault<Direction>("where directionid = @0", model.DirectionID);
                Assert.True(direction.DocStatus == Direction.DR_STATUS_DISP_EXPED);

                var directionItems = db.Fetch<DirectionItem>("where directionid = @0", model.DirectionID);
                var testItems = directionItems.Select(i => new TestItem() { ArtPackID = i.ArtPackID, Quantity = i.Quantity }).ToList();

                var hist = db.Fetch<DirectionHistory>(new NPoco.Sql().Where("directionid = @0", model.DirectionID));
                Assert.True(hist.Count > 0);

                // input side
                var directionIn = db.FirstOrDefault<Direction>("where partnerRef = @0", $"{direction.DocNumPrefix} / {direction.DocYear} / {direction.DocNumber}");
                Assert.NotNull(directionIn);
                Assert.Equal(Direction.DR_STATUS_RECEIVE_COMPLET, directionIn.DocStatus);

                var histIn = db.Fetch<DirectionHistory>(new NPoco.Sql().Where("directionid = @0", directionIn.DirectionID));
                Assert.Single(histIn);
                Assert.Equal(Direction.DR_EVENT_DISP_TO_STOIN, histIn[0].EventCode);

                var directionInItems = db.Fetch<DirectionItem>("where directionid = @0", directionIn.DirectionID);
                Assert.Equal(testItems.Count, directionInItems.Count);
                var items2 = directionInItems.Select(i => new TestItem() { ArtPackID = i.ArtPackID, Quantity = i.Quantity }).ToList();
                TestItems(testItems, items2);

                var directionAssigns = db.Fetch<DirectionAssign>("where directionid = @0 AND jobID = @1", directionIn.DirectionID, DirectionAssign.JOB_ID_STORE_IN);
                Assert.NotNull(directionAssigns);


                // store move - receive
                var stMoveReceive = db.FirstOrDefault<StoreMove>("where parent_directionID = @0 AND docType = @1", directionIn.DirectionID, StoreMove.MO_DOCTYPE_RECEIVE);
                Assert.NotNull(stMoveReceive);
                var stMoveReceiveHist = db.FirstOrDefault<StoreMoveHistory>(new NPoco.Sql().Where("stoMoveID = @0", stMoveReceive.StoMoveID));
                Assert.NotNull(stMoveReceiveHist);
                Assert.Equal(StoreMove.MO_STATUS_SAVED_RECEIVE, stMoveReceiveHist.EventCode);

                TestItems(db, testItems, stMoveReceive);

            }
        }

        private void TestItems(NPoco.IDatabase db, List<TestItem> items, StoreMove stMove)
        {
            var stMoveItems = db.Fetch<StoreMoveItem>(new NPoco.Sql().Where("stoMoveID = @0", stMove.StoMoveID));
            var items2 = items.Select(i => new TestItem() { ArtPackID = i.ArtPackID, Quantity = i.Quantity }).ToList();
            TestItems(items, items2);
        }

        private void TestItems(List<TestItem> items, List<TestItem> items2)
        {
            Assert.Empty(items.Except(items2, new ItemComparer()));
            Assert.Empty(items2.Except(items, new ItemComparer()));
        }

        private class ItemComparer : IEqualityComparer<TestItem>
        {
            public bool Equals(TestItem x, TestItem y)
            {
                return (x.ArtPackID == y.ArtPackID && x.Quantity == y.Quantity);
            }

            public int GetHashCode(TestItem obj)
            {
                return obj.ArtPackID.GetHashCode() + obj.Quantity.GetHashCode();
            }
        }


        [Fact]
        public void LogisticDispatchStoInProcTest_Error01()
        {
            var model = new LogisticsDispToStoIn()
            {
                DirectionID = 24,
                ReceivePositionID = 113,
                WorkerID = "WRK1"
            };

            var proc = new Logistics.LogisticsDispatchToStoInProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal("Doklad nemá DocDirection DOC_DIRECTION_OUT; DirectionID: 24", res.ErrorText);
        }

        [Fact]
        public void LogisticDispatchStoInProcTest_Error02()
        {
            var model = new LogisticsDispToStoIn()
            {
                DirectionID = 18,
                ReceivePositionID = 113,
                WorkerID = "WRK1"
            };

            var proc = new Logistics.LogisticsDispatchToStoInProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal("Doklad nemá nastavenu expediční pozici; DirectionID: 18", res.ErrorText);
        }
    }
}

﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.BarCode;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class BarcodePrintCarrierLabelsProcTest : ProcTestBase
    {
        public BarcodePrintCarrierLabelsProcTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void BarcodePrintCarrierLabelsProcTest_Success()
        {
            var model = new BarcodePrintCarrierLabels()
            {
                LabelsQuant = 1,
                PrinterLocationID = 2
            };
            var proc = new Barcode.BarcodePrintCarrierLabelsProc(USER_ID) { IsTesting = true };
            var res = proc.DoProcedure(model);
            Assert.True(res.Success);
        }



        [Fact]
        public void BarcodePrintCarrierLabelsProcTest_Error()
        {
            var model = new BarcodePrintCarrierLabels()
            {
                LabelsQuant = 10
            };

            var proc = new Barcode.BarcodePrintCarrierLabelsProc(USER_ID) { IsTesting = true };
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
        }
    }
}

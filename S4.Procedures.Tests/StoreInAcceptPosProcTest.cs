﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.StoreIn;

namespace S4.Procedures.Tests
{
    [Collection("StoreInTests")]
    [Trait("Category", "StoreInTests")]
    public class StoreInAcceptPosProcTest : ProcTestBase
    {
        public StoreInAcceptPosProcTest(DatabaseFixture fixture) : base(fixture)
        { }

        [Fact]
        public void StoreInAcceptPosProcTest_Success()
        {
            var model = new StoreInAcceptPos()
            {
                CarrierNum = "A12345",
                SrcPositionID = 33,
                DestPositionID = 122,
                DirectionID = 24
            };

            var proc = new StoreIn.StoreInAcceptPosProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            Assert.True(res.StoMoveID > 0);
            using (var db = GetDB())
            {
                var storeMove = db.SingleOrDefault<StoreMove>("where stoMoveID = @0", res.StoMoveID);
                Assert.NotNull(storeMove);
                Assert.Equal(StoreMove.MO_STATUS_NEW_STOIN, storeMove.DocStatus);
                var items = db.Fetch<StoreMoveItem>("where stoMoveID = @0", res.StoMoveID);
                Assert.True(items.Count > 0);
                var hist = db.Fetch<StoreMoveHistory>("where stoMoveID = @0", res.StoMoveID);
                Assert.True(hist.Count > 0);
                var histItem = hist.OrderByDescending(h => h.StoMoveHistID).First();
                Assert.Equal(StoreMove.MO_STATUS_NEW_STOIN, histItem.EventCode);
                Assert.True(histItem.EntryDateTime < DateTime.Now);
            }


            // check
            var checkModel = new StoreInCheckCarrier()
            {
                CarrierNum = "A12345",
                SrcPositionID = 33,
                DirectionID = 24
            };

            var checkProc = new StoreIn.StoreInCheckCarrierProc(USER_ID);
            var checkRes = checkProc.DoProcedure(checkModel);
            Assert.False(checkRes.IsCarrierFound);

            // second usage - not successful
            var model2 = new StoreInAcceptPos()
            {
                CarrierNum = model.CarrierNum,
                SrcPositionID = model.SrcPositionID,
                DestPositionID = model.DestPositionID,
                DirectionID = model.DirectionID
            };
            var res2 = proc.DoProcedure(model2);

            Assert.False(res2.Success);
            Assert.Equal("Paleta k naskladnění nebyla nalezena; carrierNum: A12345", res2.ErrorText);
        }


        [Fact]
        public void StoreInAcceptPosProcTest_Error()
        {
            var model = new StoreInAcceptPos()
            {
                CarrierNum = "A12345",
                SrcPositionID = 33,
                DestPositionID = 33,
                DirectionID = 7
            };

            var proc = new StoreIn.StoreInAcceptPosProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal("Doklad nemá DocDirection DOC_DIRECTION_IN; DirectionID: 7", res.ErrorText.Trim());
        }
    }
}

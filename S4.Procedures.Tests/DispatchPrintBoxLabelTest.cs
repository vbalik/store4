﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Dispatch;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class DispatchPrintBoxLabelTest : ProcTestBase
    {
        const int DIRECTION_ID = 1;


        public DispatchPrintBoxLabelTest(DatabaseFixture fixture) : base(fixture) 
        { }


        [Fact]
        public void DispatchPrintBoxLabelTest_Success()
        {
            var model = new DispatchPrintBoxLabel()
            {
                LabelText = "Label",
                LabelsQuant = 1,
                DirectionID = DIRECTION_ID,
                DocPosition = 1,
                PrinterLocationID = 2,
                ReportID = 3
            };
            var proc = new Dispatch.DispatchPrintBoxLabelProc(USER_ID) { IsTesting = true };
            var res = proc.DoProcedure(model);
            Assert.True(res.Success);
            Assert.Equal($"PRINT Label {DIRECTION_ID} {model.DocPosition}", proc.RenderResult);
        }


        [Fact]
        public void DispatchPrintBoxLabelTest_Error()
        {
            var model = new DispatchPrintBoxLabel()
            {
                DirectionID = -1
            };

            var proc = new Dispatch.DispatchPrintBoxLabelProc(USER_ID) { IsTesting = true };
            var res = proc.DoProcedure(model);

            Assert.False(res.Success);
        }

    }
}

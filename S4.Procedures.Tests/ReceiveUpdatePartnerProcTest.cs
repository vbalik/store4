﻿using S4.Entities;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Receive;

namespace S4.Procedures.Tests
{
    [Collection("ReceiveTests")]
    [Trait("Category", "ReceiveTests")]
    public class ReceiveChangePartnerProcTest : ProcTestBase
    {
        public ReceiveChangePartnerProcTest(DatabaseFixture fixture) : base(fixture)
        { }

        [Fact]
        public void ReceiveChangePartnerProcTest_Success()
        {
            var model = new ReceiveChangePartner()
            {
                DirectionID = 54,
                PartnerID = 2,
                PartnerRef = "test ref",
                DocRemark = "doc remark"
            };

            var proc = new Receive.ReceiveChangePartnerProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var direction = db.FirstOrDefault<Direction>(new NPoco.Sql().Where("directionID = @0", model.DirectionID));
                Assert.NotNull(direction);
                Assert.Equal(Direction.DR_STATUS_RECEIVE_PROC, direction.DocStatus);
                Assert.Equal(model.PartnerID, direction.PartnerID);
                Assert.Equal(model.PartnerRef, direction.PartnerRef);
                Assert.Equal(model.DocRemark, direction.DocRemark);
            }
        }

        [Fact]
        public void ReceiveChangePartnerProcTest_ErrorBadDocStatus()
        {
            var model = new ReceiveChangePartner()
            {
                DirectionID = 55,
                PartnerID = 2
            };

            var proc = new Receive.ReceiveChangePartnerProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Contains("Doklad nemá očekávaný status DR_STATUS_RECEIVE_PROC", res.ErrorText.Trim());
        }

        [Fact]
        public void ReceiveChangePartnerProcTest_EmptyPartner()
        {
            var model = new ReceiveChangePartner()
            {
                DirectionID = 56,
                PartnerID = null
            };

            var proc = new Receive.ReceiveChangePartnerProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var direction = db.FirstOrDefault<Direction>(new NPoco.Sql().Where("directionID = @0", model.DirectionID));
                Assert.NotNull(direction);
                Assert.Equal(Direction.DR_STATUS_RECEIVE_PROC, direction.DocStatus);
                Assert.Equal(direction.PartnerID, 0);
            }
        }
    }
}
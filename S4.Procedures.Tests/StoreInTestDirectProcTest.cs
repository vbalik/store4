﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.StoreIn;

namespace S4.Procedures.Tests
{
    [Collection("StoreInTests2")]
    [Trait("Category", "StoreInTests2")]
    public class StoreInTestDirectProcTest : ProcTestBase
    {
        public StoreInTestDirectProcTest(DatabaseFixture fixture) : base(fixture)
        { }

        [Fact]
        public void StoreInTestDirectProcTest_Success()
        {
            var model = new StoreInTestDirect()
            {
                DirectionID = 23
            };

            var proc = new StoreIn.StoreInTestDirectProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var direction = db.SingleOrDefault<Direction>("where directionID = @0", model.DirectionID);
                Assert.NotNull(direction);
                Assert.Equal(Direction.DR_STATUS_STOIN_PROC, direction.DocStatus);
            }
        }

        [Fact]
        public void StoreInTestDirectProcTest_Error()
        {
            var model = new StoreInTestDirect()
            {
                DirectionID = int.MaxValue
            };

            var proc = new StoreIn.StoreInTestDirectProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal($"Direction nebylo nalezeno; DirectionID: {model.DirectionID}", res.ErrorText.Trim());
        }
    }
}

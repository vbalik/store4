﻿using System;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Booking;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class BookingListDeleteItemProcTest : ProcTestBase
    {
        public const int BOOKING_ID = 1;

        public BookingListDeleteItemProcTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void BookingDeleteItem_Success()
        {
            var model = new BookingDeleteItem()
            {
                BookingID = BOOKING_ID,
            };

            var proc = new Booking.BookingDeleteItemProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success);


            using (var db = GetDB())
            {
                var booking = db.FirstOrDefault<Entities.Booking>("where BookingID = @0", BOOKING_ID);
                Assert.Equal(Entities.Booking.DELETED, booking.BookingStatus);
                Assert.NotNull(booking.DeletedDateTime);
                Assert.NotNull(booking.DeletedByEntryUserID);
                Assert.Equal(USER_ID, booking.DeletedByEntryUserID);
            }
        }


        [Fact]
        public void BookingDeleteItem_Error()
        {
            var model = new BookingDeleteItem()
            {
                BookingID = -1,
            };

            var proc = new Booking.BookingDeleteItemProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.False(res.Success);
            Assert.Contains("Rezervace nenalezena", res.ErrorText);
        }
    }
}

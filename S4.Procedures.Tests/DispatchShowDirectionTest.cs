﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Dispatch;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class DispatchShowDirectionTest : ProcTestBase
    {
        const int DIRECTION_ID = 7;
       

        public DispatchShowDirectionTest(DatabaseFixture fixture) : base(fixture) 
        { }


        [Fact]
        public void DispatchShowDirectionTest_Success()
        {
            var model = new DispatchShowDirection()
            {
                DirectionID = DIRECTION_ID
            };
            var proc = new Dispatch.DispatchShowDirectionProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            Assert.Single(res.ItemInfoList);
        }
              
    }
}

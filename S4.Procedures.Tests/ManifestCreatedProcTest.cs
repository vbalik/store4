﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Delivery;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests2")]
    [Trait("Category", "ProcedureTests2")]
    public class ManifestCreatedProcTest : ProcTestBase
    {
        private const int _DELIVERY_MANIFEST_ID_WRONG = 2;
        private const int _DELIVERY_MANIFEST_ID = 1;

        public ManifestCreatedProcTest(DatabaseFixture fixture) : base(fixture)
        { }

        [Fact]
        public void ManifestCreatedProc_MissingParams()
        {
            var manifestCreated = new ManifestCreated();
            manifestCreated = new Procedures.Delivery.ManifestCreatedProc(USER_ID).DoProcedure(manifestCreated);

            Assert.False(manifestCreated.Success);
            Assert.Equal("Parametry procedury nejsou platné DeliveryManifestID IsNull , ManifestRefNumber IsNull ", manifestCreated.ErrorText);
        }

        [Fact]
        public void ManifestCreatedProc_BadStatus()
        {
            var manifestCreated = new ManifestCreated()
            {
                DeliveryManifestID = _DELIVERY_MANIFEST_ID_WRONG,
                DeliveryProvider = DeliveryDirection.DeliveryProviderEnum.DPD,
                ManifestRefNumber = _DELIVERY_MANIFEST_ID_WRONG.ToString()
            };
            manifestCreated = new Procedures.Delivery.ManifestCreatedProc(USER_ID).DoProcedure(manifestCreated);

            Assert.False(manifestCreated.Success);
            Assert.Equal("Direction nemá DocStatus DR_STATUS_DELIVERY_PRINT; DirectionID: 42", manifestCreated.ErrorText);
        }

        [Fact]
        public void ManifestCreatedProc_Success()
        {
            var manifestCreated = new ManifestCreated()
            {
                DeliveryManifestID = _DELIVERY_MANIFEST_ID,
                DeliveryProvider = DeliveryDirection.DeliveryProviderEnum.DPD,
                ManifestRefNumber = _DELIVERY_MANIFEST_ID.ToString()
            };
            manifestCreated = new Procedures.Delivery.ManifestCreatedProc(USER_ID).DoProcedure(manifestCreated);

            //check success
            Assert.True(manifestCreated.Success);
            //test manifest status
            var manifest = new DAL.DeliveryManifestDAL().Get(_DELIVERY_MANIFEST_ID);
            Assert.Equal("DONE", manifest.ManifestStatus);
            Assert.Equal(1, manifest.CreateManifestAttempt ?? 0);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var deliveryDirections = new DAL.DeliveryDirectionDAL().GetByManifestID(_DELIVERY_MANIFEST_ID, db);
                foreach (var deliveryDirection in deliveryDirections)
                {
                    //check status
                    var status = new DAL.DirectionDAL().GetStatus(deliveryDirection.DirectionID);
                    Assert.Equal(Direction.DR_STATUS_DELIVERY_MANIFEST, status);
                    //check history
                    var history = new DAL.DirectionDAL().GetDirectionHistory(deliveryDirection.DirectionID).Where(i => i.EventCode == Direction.DR_STATUS_DELIVERY_MANIFEST).FirstOrDefault();
                    Assert.NotNull(history);
                }
            }
        }
    }
}

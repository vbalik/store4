﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Admin;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class StoreMoveSetStatusProcTest : ProcTestBase
    {
        const int ST_MOVE_ID = 2;


        public StoreMoveSetStatusProcTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void StoreMoveSetStatusProcTest_Success()
        {
            var model = new StoreMoveSetStatus()
            {
                StoMoveID = ST_MOVE_ID,
                ChangeReason = "Test",
                Status = StoreMove.MO_STATUS_NEW_DIR_MOVE
            };

            StoreMove storeMoveOld = null;
            using (var db = GetDB())
            {
                storeMoveOld = db.SingleOrDefault<StoreMove>("where stoMoveID = @0", model.StoMoveID);
            }

            var proc = new Admin.StoreMoveSetStatusProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);

            using (var db = GetDB())
            {
                var storeMove = db.SingleOrDefault<StoreMove>("where stoMoveID = @0", model.StoMoveID);
                Assert.True(storeMove.DocStatus == model.Status, storeMove.DocStatus);

                var hist = db.Fetch<StoreMoveHistory>(new NPoco.Sql().Where("stoMoveID = @0", model.StoMoveID));
                Assert.True(hist.Count > 0);
                var hist2 = hist.OrderByDescending(h => h.StoMoveID).First();
                Assert.Equal(ST_MOVE_ID, hist2.StoMoveID);
                Assert.Equal(StoreMove.MO_EVENT_STATUS_CHANGED, hist2.EventCode.Trim());
                Assert.Equal("MOVE|NDIM", hist2.EventData.Trim());
                Assert.Equal(USER_ID, hist2.EntryUserID.Trim());
                Assert.True(hist2.EntryDateTime <= DateTime.Now);

                var msg = db.Fetch<InternMessage>(new NPoco.Sql().Where("msgclass = @0", InternMessage.MO_STATUS_CHANGED));
                Assert.True(msg.Count > 0);
                var msg2 = msg.OrderByDescending(h => h.IntMessID).First();
                Assert.Equal(InternMessage.MessageSeverityEnum.Low, msg2.MsgSeverity);
                Assert.Equal("Změna stavu 'XXX1/1'", msg2.MsgHeader);
                Assert.True(msg2.MsgDateTime <= DateTime.Now);
                                
                var msgTextDictionary = Newtonsoft.Json.JsonConvert.DeserializeObject<System.Collections.Generic.Dictionary<string, dynamic>>(msg2.MsgText);
                Assert.Equal(msgTextDictionary["stoMoveID"], ST_MOVE_ID);
                Assert.Equal(msgTextDictionary["oldStatus"], storeMoveOld.DocStatus);
                Assert.Equal(msgTextDictionary["newStatus"], model.Status);
            }

        }


        [Fact]
        public void StoreMoveSetStatusProcTest_Error()
        {
            var model = new StoreMoveSetStatus()
            {
                StoMoveID = -1,
                ChangeReason = "Test"
            };

            var proc = new Admin.StoreMoveSetStatusProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal("Parametry procedury nejsou platné Status IsNull ", res.ErrorText);
        }


        [Fact]
        public void StoreMoveSetStatusProcTest_Error2()
        {
            var model = new StoreMoveSetStatus()
            {
                StoMoveID = -1,
                ChangeReason = "Test",
                Status = "XXX"
            };

            var proc = new Admin.StoreMoveSetStatusProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal("StoreMove nebylo nalezeno; StMoveID: -1", res.ErrorText);
        }
    }
}

﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.BarCode;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class BarcodeListPrintersProcTest : ProcTestBase
    {

        public BarcodeListPrintersProcTest(DatabaseFixture fixture) : base(fixture) 
        { }


        [Fact]
        public void BarcodeListPrintersProcTest_Success()
        {
            var model = new BarcodeListPrinters()
            {
                PrinterClass = PrinterType.PrinterClassEnum.PrinterClassA4
            };
            var proc = new Barcode.BarcodeListPrintersProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            Assert.True(res.PrintersList.Count > 0);

        }

        
    }
}

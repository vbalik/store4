﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Info;
using S4.Procedures.Info;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class InfoByArticleProcTest : ProcTestBase
    {

        public InfoByArticleProcTest(DatabaseFixture fixture) : base(fixture) 
        { }


        [Fact]
        public void InfoByArticleProcTest_Success()
        {
            var model = new InfoByArticle()
            {
                ArticleID = 203
            };
            var proc = new InfoByArticleProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            Assert.True(res.OnStoreItemFull.Count > 0);

        }

        [Fact]
        public void InfoByArticleProcTest_Error()
        {
            var model = new InfoByArticle()
            {
                ArticleID = -1
            };
            var proc = new InfoByArticleProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            Assert.True(res.OnStoreItemFull.Count == 0);

        }


    }
}

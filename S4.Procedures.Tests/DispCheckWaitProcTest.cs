﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.DispCheck;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class DispCheckWaitProcTest : ProcTestBase
    {
        const int DIRECTION_ID = 11;


        public DispCheckWaitProcTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void DispCheckWaitProcTest_Success()
        {
            var model = new DispCheckWait()
            {
                DirectionID = DIRECTION_ID,
                WorkerID = "Test1",
                CheckMethod = Direction.DispCheckMethodsEnum.NotSet
            };

            int directionAssignID = 0;
            using (var db = GetDB())
            {
                var directionAssigns1 = db.Fetch<DirectionAssign>("where directionid = @0", DIRECTION_ID);
                Assert.True(directionAssigns1.Count == 1);
                directionAssignID = directionAssigns1[0].DirectAssignID;
            }

            var proc = new DispCheck.DispCheckWaitProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var direction = db.FirstOrDefault<Direction>("where directionid = @0", DIRECTION_ID);
                Assert.True(direction.DocStatus == Direction.DR_STATUS_DISP_CHECK_WAIT);

                var directionAssigns2 = db.Fetch<DirectionAssign>("where directionid = @0 AND jobID = @1", DIRECTION_ID, DirectionAssign.JOB_ID_DISPATCH_CHECK);
                Assert.True(directionAssigns2.Count == 1);
                Assert.True(directionAssigns2[0].DirectAssignID != directionAssignID);

                //test xtraData
                var xtraData = new Entities.XtraData.XtraDataProxy<Entities.DirectionXtra>(Direction.XtraDataDict);
                xtraData.Load(db, new NPoco.Sql().Where("DirectionID = @0", DIRECTION_ID));
                Assert.NotEmpty(xtraData.Data);
                Assert.Equal(123, xtraData[Direction.XTRA_DISP_CHECK_METHOD]);

                var hist = db.Fetch<DirectionHistory>(new NPoco.Sql().Where("directionid = @0", DIRECTION_ID));
                Assert.True(hist.Count > 0);
                var hist2 = hist.OrderByDescending(h => h.DirectionID).First();
                Assert.Equal(DIRECTION_ID, hist2.DirectionID);
                Assert.Equal(Direction.DR_STATUS_DISP_CHECK_WAIT, hist2.EventCode.Trim());
                Assert.Equal($"Test1|123", hist2.EventData.Trim());
                Assert.Equal(USER_ID, hist2.EntryUserID.Trim());
                Assert.True(hist2.EntryDateTime < DateTime.Now);
            }

        }


        [Fact]
        public void DispCheckWaitProcTest_Error()
        {
            var model = new DispCheckWait()
            {
                DirectionID = -1,
                WorkerID = "-1"
            };

            var proc = new DispCheck.DispCheckWaitProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal("Doklad nebyl nalezen; DirectionID: -1", res.ErrorText);
        }

    }
}

﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.StoreIn;

namespace S4.Procedures.Tests
{
    [Collection("StoreInTests")]
    [Trait("Category", "StoreInTests")]
    public class StoreInGetPosNoCarrProcTest : ProcTestBase
    {
        public StoreInGetPosNoCarrProcTest(DatabaseFixture fixture) : base(fixture)
        { }

        [Fact]
        public void StoreInGetPosNoCarrProcTest_Success()
        {
            var model = new StoreInGetPosNoCarr()
            {
                ArtPackID = 1012
            };

            var proc = new StoreIn.StoreInGetPosNoCarrProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);
            Assert.True(res.IsPositionFound);
            Assert.NotNull(res.DestPosition);
        }

        [Fact]
        public void StoreInGetPosNoCarrProcTest_Error()
        {
            var model = new StoreInGetPosNoCarr()
            {
                ArtPackID = 1002
            };

            var proc = new StoreIn.StoreInGetPosNoCarrProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal("Zboží nemá nastavenou sekci!", res.ErrorText.Trim());
        }
    }
}

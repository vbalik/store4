﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.DispCheck;

namespace S4.Procedures.Tests
{
    [Collection("ProcedureTests")]
    public class DispCheckSkipProcTest : ProcTestBase
    {
        const int DIRECTION_ID = 39;


        public DispCheckSkipProcTest(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void DispCheckSkipProcTest_Success()
        {          
            var model = new DispCheckSkip()
            {
                DirectionID = DIRECTION_ID,
                Reason = "xxx"
            };

            var proc = new DispCheck.DispCheckSkipProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var direction = db.FirstOrDefault<Direction>("where directionid = @0", DIRECTION_ID);
                Assert.True(direction.DocStatus == Direction.DR_STATUS_DISP_CHECK_DONE);

                var hist = db.Fetch<DirectionHistory>(new NPoco.Sql().Where("directionid = @0", DIRECTION_ID));
                Assert.True(hist.Count > 0);
                var hist2 = hist.OrderByDescending(h => h.DirectionID).First();
                Assert.Equal(DIRECTION_ID, hist2.DirectionID);
                Assert.Equal(Direction.DR_EVENT_CHECK_SKIPPED, hist2.EventCode.Trim());
                Assert.Equal(model.Reason, hist2.EventData);
                Assert.Equal(USER_ID, hist2.EntryUserID.Trim());
                Assert.True(hist2.EntryDateTime < DateTime.Now);
            }
        }


        [Fact]
        public void DispCheckSkipProcTest_Error()
        {
            var model = new DispCheckSkip()
            {
                DirectionID = -1
            };

            var proc = new DispCheck.DispCheckSkipProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal("Doklad nebyl nalezen; DirectionID: -1", res.ErrorText);
        }
    }
}

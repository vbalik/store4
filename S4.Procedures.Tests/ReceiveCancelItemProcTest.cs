﻿using S4.Entities;
using System;
using System.Linq;
using TestHelpers;
using Xunit;
using S4.ProcedureModels.Receive;

namespace S4.Procedures.Tests
{
    [Collection("ReceiveTests")]
    [Trait("Category", "ReceiveTests")]
    public class ReceiveCancelItemProcTest : ProcTestBase
    {
        public ReceiveCancelItemProcTest(DatabaseFixture fixture) : base(fixture)
        { }

        [Fact]
        public void ReceiveCancelItemProcTest_Success()
        {
            var model = new ReceiveCancelItem()
            {
                StoMoveID = 503,
                DocPosition = 1
            };

            var proc = new Receive.ReceiveCancelItemProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(res.Success, res.ErrorText);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var storeMove = db.FirstOrDefault<StoreMove>(new NPoco.Sql().Where("stoMoveID = @0", model.StoMoveID));
                Assert.NotNull(storeMove);
                var moveItem = db.FirstOrDefault<StoreMoveItem>(new NPoco.Sql().Where("stoMoveID = @0 AND docPosition = @1", model.StoMoveID, model.DocPosition));
                Assert.NotNull(moveItem);
                Assert.Equal(StoreMoveItem.MI_VALIDITY_CANCELED, moveItem.ItemValidity);
                var moveHist = db.Fetch<StoreMoveHistory>(new NPoco.Sql().Where("stoMoveID = @0", model.StoMoveID));
                Assert.True(moveHist.Count > 0);
                var moveHist2 = moveHist.OrderByDescending(h => h.StoMoveID).First();
                Assert.Equal(model.DocPosition, moveHist2.DocPosition);
                Assert.Equal(StoreMove.MO_STATUS_CANCEL, moveHist2.EventCode.Trim());

                var directionItems = db.Fetch<DirectionItem>(new NPoco.Sql().Where("directionID = @0", storeMove.Parent_directionID));
                Assert.NotEmpty(directionItems);
                Assert.All(
                    directionItems.Where(di => di.DocPosition == moveItem.Parent_docPosition), 
                    i => Assert.Equal(DirectionItem.DI_STATUS_CANCEL, i.ItemStatus));

            }
        }

        [Fact]
        public void ReceiveCancelItemProcTest_Error()
        {
            var model = new ReceiveCancelItem()
            {
                StoMoveID = 503,
                DocPosition = 2
            };

            var proc = new Receive.ReceiveCancelItemProc(USER_ID);
            var res = proc.DoProcedure(model);

            Assert.True(!res.Success);
            Assert.Equal("Pozice v dokladu nebyla nalezena; DocPosition: 2", res.ErrorText.Trim());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace S4.SharedLib.Security
{
    public class RemoteAccessAuthorization
    {
        private const string SECRET_SAUCE = "en4KZkEwuEHnJd74";

        public static string CreateKey(string text, DateTime dateTime)
        {
            var hash1 = DoHash(text);
            var withSecret = $"{SECRET_SAUCE}_{hash1}_{dateTime:O}";
            return DoHash(withSecret);


            string DoHash(string val)
            {
                using (SHA256 sha256Hash = SHA256.Create())
                {
                    byte[] data = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(val));
                    return ByteArrayToString(data);
                }
            }

            string ByteArrayToString(byte[] ba)
            {
                return BitConverter.ToString(ba).Replace("-", "");
            }
        }


        public static bool CheckKey(string text, DateTime dateTime, string key)
        {
            var key2 = CreateKey(text, dateTime);
            return String.Equals(key2, key, StringComparison.InvariantCultureIgnoreCase);
        }
    }
}

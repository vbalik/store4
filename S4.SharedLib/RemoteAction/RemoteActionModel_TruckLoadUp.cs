﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.SharedLib.RemoteAction
{
    public class RemoteActionModel_TruckLoadUp : RemoteActionModelBase
    {
        [Required]
        public override DocIDSourceEnum DocIDSource { get; set; }
        public override int DirectionID { get; set; }
        public override string DocInfo { get; set; }
        [Required]
        public override string ExpedTruck { get; set; }
    }
}

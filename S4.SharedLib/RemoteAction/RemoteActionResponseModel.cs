﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.SharedLib.RemoteAction
{
    public class RemoteActionResponseModel
    {
        public bool Success { get; set; }
        public string DocInfo { get; set; }
        public string PartnerName { get; set; }
        public string PartnerAddress { get; set; }
    }
}

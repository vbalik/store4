﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.SharedLib.RemoteAction
{
    public abstract class RemoteActionModelBase
    {
        [Required]
        public string UserID { get; set; }
        [Required]
        public string DeviceID { get; set; }
        [Required]
        public DateTime ActionDate { get; set; }

        [Required]
        public virtual DocIDSourceEnum DocIDSource { get; set; }
        public virtual int DirectionID { get; set; }
        public virtual string DocInfo { get; set; }
        [Required]
        public virtual string ExpedTruck { get; set; }

        public virtual DateTime? DateTimeDeliveredCustomer { get; set; }
    }
}

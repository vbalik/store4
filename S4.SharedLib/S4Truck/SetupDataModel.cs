﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.SharedLib.S4Truck
{
    public class SetupDataModel
    {
        public class TruckInfo
        {
            public int TruckID { get; set; }
            public string TruckRegist { get; set; }
        }


        public string ServerVersion { get; set; }
        public DateTime ServerTime { get; set; }
        public List<TruckInfo> TruckInfos { get; set; }

    }
}

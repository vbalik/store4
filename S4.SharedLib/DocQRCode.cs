﻿using System;
using System.Text.RegularExpressions;


namespace S4.SharedLib
{
    public class DocQRCode
    {
        // DOCINFO*TYPE:DL*ID:123*INFO:DL/12/34

        private const string HEADER_MARK = "DOCINFO";


        public enum DocTypeEnum 
        { 
            Empty,
            DL,
            FA
        }


        public DocTypeEnum DocType { get; set; }
        public string DocID { get; set; }
        public string DocInfo { get; set; }


        public string Formated
        {
            get
            {
                return $"{HEADER_MARK}*TYPE:{DocType}*ID:{DocID}*INFO:{DocInfo}";
            }
        } 


        public static (bool parsed, DocQRCode qrCode) TryParse(string value)
        {
            if (String.IsNullOrWhiteSpace(value))
                return (false, null);
            if (!value.StartsWith(HEADER_MARK))
                return (false, null);

            var matches = Regex.Matches(value, @"(\*(\w+)\:([^*]+))");
            if(matches.Count == 0)
                return (false, null);

            var res = new DocQRCode();
            foreach (Match match in matches)
            {
                switch (match.Groups[2].Value)
                {
                    case "TYPE":
                        DocTypeEnum dt;
                        if (!Enum.TryParse<DocTypeEnum>(match.Groups[3].Value, out dt))
                            return (false, null);
                        else
                            res.DocType = dt;
                        break;

                    case "ID":
                        res.DocID = match.Groups[3].Value;
                        break;
                    
                    case "INFO":
                        res.DocInfo = match.Groups[3].Value;
                        break;

                    default:
                        break;
                }
            }


            return (true, res);
        }
    }
}

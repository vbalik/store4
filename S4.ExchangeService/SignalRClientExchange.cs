﻿
using Microsoft.AspNetCore.SignalR.Client;
using Quartz;
using S4.ExchangeService.Helper;
using System;
using System.Collections.Generic;
using System.Linq;


namespace S4.ExchangeService
{
    public class SignalRClientExchange : SignalRClientBase<Core.Interfaces.Hubs.IExchangeHub>
    {
        private readonly List<Config.ExchangeServiceSettings> _settings = null;
        readonly IScheduler _scheduler = null;

        public SignalRClientExchange(string urlEndpoint, List<Config.ExchangeServiceSettings> settings, IScheduler scheduler) : base(urlEndpoint)
        {
            _settings = settings;
            _scheduler = scheduler;            
        }


        protected override void Init(HubConnection connection)
        {
            connection.On<Procedures.Messages.ExchangeMessage>(nameof(Core.Interfaces.Hubs.IExchangeHub.UpdateValue), (msg) =>
            {
                foreach (var jobName in msg.JobNames)
                {
                    Program.NLogger.Debug($"Try run job from S4: {jobName}");
                    RunJob(jobName);
                }
            });
        }


        private void RunJob(string jobName)
        {
            var jobDef = (from s in _settings where s.JobName.Equals(jobName) select s).FirstOrDefault();
            if (jobDef == null)
                throw new Exception($"Job run name {jobName} is not found in S4.ExchangeSettings.json!");

            // do schedule job
            var identity = jobDef.JobName;
            Type jobType = Type.GetType(jobDef.JobClass);
            IJobDetail job = JobBuilder.Create(jobType).WithIdentity(identity, null).Build();

            //check
            var isJobWaiting = Core.Singleton<WaitingJobs>.Instance.IsJobWaiting(job.Key.Name);

            if (isJobWaiting)
                Program.NLogger.Debug($"Call from Server S4 Job {jobDef.JobName} JobKey {job.Key.Name} is waiting! The job cannot be started");
            else
            {
                Core.Singleton<WaitingJobs>.Instance.AddWaitingJob(job.Key.Name);
                _scheduler.TriggerJob(job.Key);
                Program.NLogger.Debug($"Call from Server S4. Job {jobDef.JobName} will start in a moment....");
            }
        }
    }
}

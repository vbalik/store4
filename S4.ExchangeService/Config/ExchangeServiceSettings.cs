﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace S4.ExchangeService.Config
{

    public class ExchangeServiceSettings
    {
        public string JobName { get; set; }
        public bool IsEnabled { get; set; }
        public bool? IgnoreServiceTime { get; set; }
        public string JobClass { get; set; }
        public string CronExpression { get; set; }
       
        public Properties Properties { get; set; }

    }
}

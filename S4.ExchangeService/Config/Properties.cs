﻿using S4.Entities.Helpers;
using S4.ExchangeService.Jobs.AnomalyWatcher.Config;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.ExchangeService.Config
{
    public class Properties
    {
        //Export
        public int[] TryMinutes { get; set; }
        public int TryMinutesForWaitingComplete { get; set; }

        //ReadXML
        public string PathSource { get; set; }
        public string PathError { get; set; }
        public string PathArchive { get; set; }
        public string SearchPattern { get; set; }
        public int WaitSecond { get; set; }
                        
        //Report
        public int ReportID { get; set; }
        public string ReportName { get; set; }
        public string MailTo { get; set; }
        public string MailSubject { get; set; }
        public string MailBody { get; set; }
        public string SQLWhere { get; set; }
        public int QueryLastPeriod { get; set; }
        public int QueryDaysBack { get; set; }
        public bool SendEmptyReport { get; set; }

        //Mail
        public SettingMail SettingMail { get; set; }

        //ExportBarCode
        public List<string> WSPath { get; set; } = new List<string>();

        //ExportABRA
        public string PathS4 { get; set; }

        //Delivery
        public string ManifestPrinter { get; set; }
        public string LabelsPrinter { get; set; }
        
        //CleanMessageBus
        public int DaysBackDone { get; set; }
        public int DaysBackError { get; set; }

        //SendAlert
        public string[] CountRowsLimit { get; set; }
        public string[] ToBodyRows { get; set; }
        public string[] SqlQuery { get; set; }
        public int CountRowsLimitMinimum { get; set; }
        public int CountRowsLimitMaximum { get; set; }

        //Heartbeat
        public string ServerName { get; set; }
        public string DutyServerURL { get; set; }

        //Export NemStore
        public int SupplierId { get; set; }
        public string BaseURL { get; set; }

        //CheckRunningJobs
        public double DelayCoef { get; set; }
        public int DelayMinutesMax { get; set; }

        //AnomalyWatcher
        public List<Jobs.AnomalyWatcher.Config.Properties> Checkers { get; set; } = new List<Jobs.AnomalyWatcher.Config.Properties>();

        public MailSettings AnomalyWatcherMail { get; set; }

    }
}

﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.ExchangeService.Helper
{
    public static class ServiceTimeHelper
    {
        public static (TimeSpan?, TimeSpan?) GetServiceTime()
        {
            var from = Program.Settings.GetValue<string>("serviceTimeFrom");
            var to = Program.Settings.GetValue<string>("serviceTimeTo");

            TimeSpan timeFrom;
            TimeSpan timeTo;
            if (!TimeSpan.TryParse(from, out timeFrom))
            {
                return (null, null);
            }

            if (!TimeSpan.TryParse(to, out timeTo))
            {
                return (null, null);
            }

            return (timeFrom, timeTo);
        }
      
        public static bool IsServiceTime(TimeSpan? serviceTimeFrom, TimeSpan? serviceTimeTo, DateTime dateTime)
        {
            if (!serviceTimeFrom.HasValue || !serviceTimeTo.HasValue)
                return false;

            TimeSpan timeSpan;
            if (!TimeSpan.TryParse(dateTime.ToString("HH:mm"), out timeSpan))
            {
                return false;
            }

            var result = (timeSpan.Ticks >= serviceTimeFrom.Value.Ticks && timeSpan.Ticks < serviceTimeTo.Value.Ticks);
            return result;
        }

        public static bool IsServiceTime(TimeSpan? serviceTimeFrom, TimeSpan? serviceTimeTo)
        {
            return IsServiceTime(serviceTimeFrom, serviceTimeTo, DateTime.Now);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace S4.ExchangeService.Helper
{
    public static class EANHelper
    {
        public static string EANrepare(string data)
        {
            data = S4.Core.Utils.StrUtils.ReplaceNonAlphaNumericCharacter(data);

            if (data.Length > 30)
                data = data.Substring(0, 30);

            return data;
        }
    }
}

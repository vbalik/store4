﻿using Microsoft.Extensions.Configuration;
using S4.ExchangeService.Config;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace S4.ExchangeService.Helper
{
    public class ExchangeHelper
    {

        public void CheckFilePath(ref string filePath)
        {
            var count = 1;
            var fileName = Path.GetFileNameWithoutExtension(filePath);
            var extension = Path.GetExtension(filePath);
            var directoryName = Path.GetDirectoryName(filePath);
            while (File.Exists(filePath))
            {
                var changedFileName = $"{fileName} ({count++})";
                filePath = $"{directoryName}\\{changedFileName}{extension}";
            }
                       
        }

        public ExchangeServiceSettings ReadSetting(string jobType, string jobName)
        {
            var exchangeServiceSettings = new List<Config.ExchangeServiceSettings>();
            Program.Settings.GetSection("Jobs").Bind(exchangeServiceSettings);
            var setting = (from e in exchangeServiceSettings where e.JobClass.Equals(jobType) && e.JobName.Equals(jobName) select e).FirstOrDefault();
            return setting;
        }

        public List<ExchangeServiceSettings> ReadAllSetting(bool enable = true)
        {
            var exchangeServiceSettings = new List<Config.ExchangeServiceSettings>();
            Program.Settings.GetSection("Jobs").Bind(exchangeServiceSettings);
            var setting = (from e in exchangeServiceSettings where e.IsEnabled == enable select e).ToList();
            return setting;
        }

        public void TryMoveFile(string sourceFileName, string destFileName, bool overwrite, int numberOfRetries = 5)
        {
            int delayOnRetry = 1000;
            IOException lastIOException = null;

            for (int i = 1; i <= numberOfRetries; ++i)
            {
                try
                {
                    System.IO.File.Move(sourceFileName, destFileName, overwrite);
                    lastIOException = null;
                    break;
                }
                catch (IOException ex) when (i <= numberOfRetries)
                {
                    lastIOException = ex;
                    System.Threading.Thread.Sleep(delayOnRetry);
                }
            }

            if (lastIOException != null)
                throw lastIOException;
        }

    }
}

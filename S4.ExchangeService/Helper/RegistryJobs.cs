﻿using NLog.Extensions.Logging;
using NPoco.RowMappers;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Text;
using Quartz;
using Quartz.Util;
using S4.ExchangeService.Helper.Model;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace S4.ExchangeService.Helper
{
    public class RegistryJobs
    {
        private readonly object _lock = new object();
        public ConcurrentDictionary<string, RegistryJob> RegistryJobsDictionary = new ConcurrentDictionary<string, RegistryJob>();

        public RegistryJobs()
        {

        }

        public void StartJob(IJobExecutionContext context)
        {
            lock (_lock)
            {
                var jobName = context.JobDetail.Key.Name;
                DateTime? nextRunDate;
                double intervalSeconds = 0;

                if (context.NextFireTimeUtc.HasValue)
                { 
                    nextRunDate = context.NextFireTimeUtc.Value.DateTime.ToLocalTime();
                    var fireTime = context.FireTimeUtc.DateTime.ToLocalTime();
                    intervalSeconds = (nextRunDate - fireTime).Value.TotalSeconds;
                }
                else
                    return;


                var value = new RegistryJob { NextRunDate = nextRunDate, RunDate = DateTime.Now, RunIntervalSeconds = intervalSeconds };
                
                if (RegistryJobsDictionary.TryGetValue(jobName, out RegistryJob oldValue))
                {
                    if (RegistryJobsDictionary.TryUpdate(jobName, value, oldValue))
                        Program.NLogger.Debug($"RegistryJobs - update job '{jobName}', run date: {value.RunDate}, next run date {value.NextRunDate}");
                    else
                        Program.NLogger.Debug($"RegistryJobs - failed to update job '{jobName}'");

                    return;
                }
                else
                {
                    if (RegistryJobsDictionary.TryAdd(jobName, value))
                        Program.NLogger.Debug($"RegistryJobs - add new job '{jobName}', run date: {value.RunDate}, next run date {value.NextRunDate}");
                    else
                        Program.NLogger.Debug($"RegistryJobs - failed to add new job '{jobName}'");

                    return;
                }
            }
        }

        public void RemoveJob(string jobName)
        {
            if (RegistryJobsDictionary.TryGetValue(jobName, out _))
            {
                if (RegistryJobsDictionary.TryRemove(jobName, out _))
                    Program.NLogger.Debug($"RegistryJobs - remove job '{jobName}'");
                else
                    Program.NLogger.Debug($"RegistryJobs - failed to remove job '{jobName}'");

                return;
            }
            else
            {
                Program.NLogger.Debug($"RegistryJobs - job '{jobName}' not found");
                return;
            }
        }

        public List<KeyValuePair<string, RegistryJob>> GetDelayedJobs(double delayCoef, int delayMinutesMax, string exceptJobName = "")
        {
            lock (_lock)
            {
                var dtNow = DateTime.Now;
                List <KeyValuePair<string, RegistryJob>> resultList = new List<KeyValuePair<string, RegistryJob>>();

                foreach (var item in RegistryJobsDictionary.Where(_ => !_.Key.Equals(exceptJobName)))
                {
                    var min = (item.Value.RunIntervalSeconds * delayCoef) / 60;
                    min = min > delayMinutesMax ? delayMinutesMax : min;

                    if (item.Value.NextRunDate.Value.AddMinutes(min) < dtNow)
                        resultList.Add(item);

                }

                return resultList;
            }
        }
        
    }
}

﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;

namespace S4.ExchangeService.Helper
{
    public class WaitingJobs
    {
        private readonly object _lock = new object();
        private ConcurrentDictionary<string, DateTime> _waitingJobs = new ConcurrentDictionary<string, DateTime>();

        public WaitingJobs()
        {

        }

        public bool AddWaitingJob(string jobName)
        {
            lock (_lock)
            {
                if (!_waitingJobs.ContainsKey(jobName))
                {
                    _waitingJobs.TryAdd(jobName, DateTime.Now);
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public void RemoveWaitingJob(string jobName)
        {
            lock (_lock)
            {
                if (_waitingJobs.ContainsKey(jobName))
                    _waitingJobs.TryRemove(jobName, out _);
            }
        }

        public bool IsJobWaiting(string jobName)
        {
            lock (_lock)
            {
                if (_waitingJobs.ContainsKey(jobName))
                    return true;
                else
                    return false;
            }

        }
    }
}

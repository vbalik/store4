﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace S4.ExchangeService.Helper
{
    public class ErrorLogHelper
    {
        public void ReportProcException(Exception exc, string jobClass, string jobName, NLog.Logger nLogger, string errorMessage = "")
        {
            string errorClass = "";
            if (exc == null)
                errorClass = "User Error";
            else
            {
                errorClass = exc.GetType().Name;
                var message = !string.IsNullOrWhiteSpace(errorMessage) ? $"; {errorMessage}" : "";
                errorMessage = $"{exc.ToString()}{message}";
            }

            var appVersion = Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion;
            var error = new Entities.ExchangeError()
            {
                AppVersion = appVersion.ToString(),
                ErrorClass = errorClass,
                ErrorDateTime = DateTime.Now,
                ErrorMessage = errorMessage,
                JobClass = jobClass,
                JobName = jobName
            };

            // write to log
            nLogger.Error(exc, $"Error {errorMessage}");

            try
            {
                // write error to DB
                (new DAL.ExchangeErrorDAL()).Insert(error);
            }
            catch (Exception exc1)
            {
                nLogger.Error(exc1, "Nepodařilo se zalogovat chybu do DB!");
            }
        }
    }
}

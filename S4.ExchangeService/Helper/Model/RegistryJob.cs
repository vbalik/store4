﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.ExchangeService.Helper.Model
{
    public class RegistryJob
    {
        public DateTime RunDate { get;set;}
        public DateTime? NextRunDate { get; set; }
        public double RunIntervalSeconds { get; set; }

    }
}

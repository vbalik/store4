﻿using Microsoft.AspNetCore.SignalR.Client;
using S4.ExchangeService.Helper;
using System;


namespace S4.ExchangeService
{
    public abstract class SignalRClientBase<T>
    {
        private HubConnection connection;

        private readonly string _urlEndPoint = null;


        protected SignalRClientBase(string urlEndpoint)
        {
            if (string.IsNullOrEmpty(urlEndpoint))
                throw new ArgumentNullException(nameof(urlEndpoint));

            _urlEndPoint = urlEndpoint;


            try
            {
                connection = new HubConnectionBuilder()
                    .WithUrl(_urlEndPoint)
                    .WithAutomaticReconnect()
                    .Build();

                // start the connection
                var task = connection.StartAsync();
                // wait for the connection to complete
                task.Wait();

                Init(connection);

                //log
                Program.NLogger.Debug("Hub connection is OK");                
            }
            catch (Exception e)
            {
                //log error
                var writeError = new ErrorLogHelper();
                writeError.ReportProcException(e, "", "", Program.NLogger, "Hub connection error");
            }
        }


        protected abstract void Init(HubConnection connection);
    }
}

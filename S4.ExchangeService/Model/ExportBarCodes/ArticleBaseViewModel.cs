﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.ExchangeService.Model.ExportBarCodes
{
    public class ArticleBaseViewModel
    {
        public string Source_id { get; set; }
        public string BarCode { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Quartz;
using S4.ExchangeService.Helper;

namespace S4.ExchangeService
{
    class GlobalJobListener : Quartz.IJobListener
    {
        public string Name
        {
            get { return "GlobalJobListener"; }
        }


        public void JobToBeExecuted(Quartz.IJobExecutionContext context)
        {

        }


        public void JobWasExecuted(Quartz.IJobExecutionContext context, Quartz.JobExecutionException jobException)
        {
            
        }


        public void JobExecutionVetoed(Quartz.IJobExecutionContext context)
        {

        }

        public Task JobToBeExecuted(IJobExecutionContext context, CancellationToken cancellationToken = default)
        {
            Core.Singleton<RegistryJobs>.Instance.StartJob(context);
            Program.NLogger.Info($"Job to be executed. Job Key: {context.JobDetail.Key.Name}");
                        
            return Task.CompletedTask;
        }

        public Task JobExecutionVetoed(IJobExecutionContext context, CancellationToken cancellationToken = default)
        {
            return Task.CompletedTask;
        }

        public Task JobWasExecuted(IJobExecutionContext context, JobExecutionException jobException, CancellationToken cancellationToken = default)
        {
            Core.Singleton<WaitingJobs>.Instance.RemoveWaitingJob(context.JobDetail.Key.Name);
            Program.NLogger.Info($"Job was executed. Job Key: {context.JobDetail.Key.Name}");
                       
            return Task.CompletedTask;
        }
    }
}

﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using NLog;
using NLog.Extensions.Logging;
using NLog.Targets;
using NPoco;
using S4.ExchangeService.Helper;
using S4.Messages;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

//https://www.stevejgordon.co.uk/running-net-core-generic-host-applications-as-a-windows-service
//1. Publish
//2. sc create S4.Exchange binPath= "c:\Projects\store4\S4.ExchangeService\bin\Release\netcoreapp2.1\win7-x64\publish\S4.ExchangeService.exe"
//3. sc start S4.Exchange
//4. sc stop S4.Exchange
//5. sc delete S4.Exchange


namespace S4.ExchangeService
{
    class Program
    {
        public const string S4_JOB = "S4.Exchange.Job";
        public static NLog.Logger NLogger = NLog.LogManager.GetLogger("S4.Exchange");
        // settings
        public static IConfigurationRoot Settings = null;
        public static string JobNameToRun = "";

        // run from hand -------> "--jobRun [jobName]" ------> example: "--jobRun ExportDirection"

        private static async Task Main(string[] args)
        {
            //read setting
            ReadConfig();

            //Nlog Configuration
            NLog.LogManager.Configuration = new NLogLoggingConfiguration(Settings.GetSection("NLog"));

            // log
            NLogger.Info("Start up...");
            var appVersion = Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion;
            System.Version ver = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
            NLogger.Info(String.Format("App ver: {0}", appVersion));
            NLogger.Info(String.Format("OS ver: {0}", System.Environment.OSVersion));
            NLogger.Info(String.Format(".NET ver: {0}", System.Environment.Version));

            //Check jobs name
            CheckJobs();

            bool isService;
            var jobRun = args.Contains("--jobRun");

            if (jobRun)
            {
                isService = false;
                if (args.Count() < 2)
                    throw new Exception("Job run name is empty!");

                JobNameToRun = args[1];
            }
            else
                isService = !(Debugger.IsAttached || args.Contains("--console"));

            var builder = new HostBuilder()
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddHostedService<MainService>();
                    services.AddSingleton<IMessageRouter, MessageRouter>();
                    services.AddSingleton<S4.DeliveryServices.IDeliveryCommon, S4.DeliveryServices.DeliveryService>();
                    //services.AddLogging()
                });

            if (isService)
            {
                await builder.RunAsServiceAsync();
            }
            else
            {
                await builder.RunConsoleAsync();
            }
        }

        private static void ReadConfig()
        {
            //JSON config
            var builderJSON = new ConfigurationBuilder()
                .SetBasePath(Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName))
                .AddEnvironmentVariables()
                .AddJsonFile("hosting.json", optional: true)
                .AddJsonFile("S4.ExchangeSettings.json");

            Settings = builderJSON.Build();

        }

        private static void CheckJobs()
        {
            var exchangeServiceSettings = new List<Config.ExchangeServiceSettings>();
            Program.Settings.GetSection("Jobs").Bind(exchangeServiceSettings);
            var settings = (from _ in exchangeServiceSettings where _.IsEnabled select _).GroupBy(_ => _.JobName).Select(grp => grp.First());

            foreach (var setting in settings)
            {
                var s = (from es in exchangeServiceSettings where es.JobName.Equals(setting.JobName) select es).ToList();

                if (s.Count > 1)
                {
                    //log error
                    var writeError = new ErrorLogHelper();
                    writeError.ReportProcException(null, "", "", NLogger, $"Job name: {setting.JobName} is found more than once!");
                }

            }
        }

    }


}

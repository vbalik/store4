﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualBasic;
using NPoco.RowMappers;
using Quartz;
using S4.Entities;
using S4.ExchangeService.Helper;
using S4.SharedLib.RemoteAction;

namespace S4.ExchangeService
{
    class GlobalTriggerListener : Quartz.ITriggerListener
    {
        private TimeSpan? _serviceTimeFrom;
        private TimeSpan? _serviceTimeTo;

        public GlobalTriggerListener()
        {
            var serviceTime = ServiceTimeHelper.GetServiceTime();
            _serviceTimeFrom = serviceTime.Item1;
            _serviceTimeTo = serviceTime.Item2;
            Program.NLogger.Info($"Service time is from: {_serviceTimeFrom} to: {_serviceTimeTo}");
        }

        public string Name
        {
            get { return "GlobalTriggerListener"; }
        }

        public Task TriggerComplete(ITrigger trigger, IJobExecutionContext context, SchedulerInstruction triggerInstructionCode, CancellationToken cancellationToken = default)
        {
            return Task.CompletedTask;
        }

        public Task TriggerFired(ITrigger trigger, IJobExecutionContext context, CancellationToken cancellationToken = default)
        {
            return Task.CompletedTask;
        }

        public Task TriggerMisfired(ITrigger trigger, CancellationToken cancellationToken = default)
        {
            return Task.CompletedTask;
        }

        public Task<bool> VetoJobExecution(ITrigger trigger, IJobExecutionContext context, CancellationToken cancellationToken = default)
        {
            return Task.FromResult(IsServiceTime(context));
        }

        private bool IsServiceTime(IJobExecutionContext context)
        {
            var jobName = context.JobDetail.Key.Name;
            var jobType = context.JobDetail.JobType.FullName;

            var helper = new ExchangeHelper();
            var setting = helper.ReadSetting(jobType, jobName);

            if (setting != null && setting.IgnoreServiceTime.HasValue && setting.IgnoreServiceTime.Value == true)
                return false;

            var result = ServiceTimeHelper.IsServiceTime(_serviceTimeFrom, _serviceTimeTo);

            if (result)
                Program.NLogger.Info($"Job: '{jobName}' not run, service time: {_serviceTimeFrom.Value} - {_serviceTimeTo.Value}");

            return result;
        }

    }
}

﻿using Microsoft.Extensions.Configuration;
using S4.Entities.Helpers;
using S4.Entities.Helpers.Xml.Article;
using S4.Entities.Helpers.Xml.Cancellation;
using S4.Entities.Helpers.Xml.Dispatch;
using S4.Entities.Helpers.Xml.Invoice;
using S4.Entities.Helpers.Xml.Receive;
using S4.ExchangeService.Config;
using S4.ExchangeService.Helper;
using S4.ProcedureModels.Load;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace S4.ExchangeService.Jobs.ReadXML
{
    [Quartz.DisallowConcurrentExecution]
    class ReadXML : Quartz.IJob
    {
        private string _jobType = "";
        private string _jobName = "";
        private ExchangeServiceSettings _setting = null;
        private List<string> _files = null;
        private string _userID;
        private NLog.Logger NLogger = null;
        private string _remark;

        #region Quartz.IJob

        public Task Execute(Quartz.IJobExecutionContext context)
        {
            try
            {
                _jobType = context.JobDetail.JobType.ToString();
                _jobName = context.JobDetail.Key.Name;

                NLogger = NLog.LogManager.GetLogger($"{Program.S4_JOB}.{_jobName}");
                NLogger.Info($"Start job {_jobName}");

                //read setting
                ReadSetting();

                //read all files
                ReadAllFiles();

                //wait
                System.Threading.Thread.Sleep(_setting.Properties.WaitSecond * 1000);

                //do Work
                DoWork();


            }
            catch (Exception exc)
            {
                var writeError = new ErrorLogHelper();
                writeError.ReportProcException(exc, _jobType, _jobName, NLogger);
            }

            NLogger.Info($"End job {_jobName}");
            return Task.CompletedTask;
        }

        #endregion

        #region private

        private void ReadSetting()
        {
            //userID
            _userID = Program.Settings.GetValue<string>("userID").ToString();

            var helper = new ExchangeHelper();
            _setting = helper.ReadSetting(_jobType, _jobName);

            if (_setting == null)
                throw new Exception($"{nameof(ReadXML)} setting not found!");
        }

        private void ReadAllFiles()
        {
            _files = Directory.EnumerateFiles(_setting.Properties.PathSource, "*.xml").ToList();
        }

        private void DoWork()
        {
            NLogger.Info($"Start read files. Files count: {_files.Count}");
            int processingTime = 0;
            foreach (var file in _files)
            {
                _remark = $"Soubor: {Path.GetFileName(file)}";
                DateTime created = File.GetLastWriteTime(file);
                NLogger.Info($"Try parse: {Path.GetFileName(file)} Created: {created}");

                try
                {
                    // load xml
                    var xml = XmlHelper.GetStringFromXMLFile(file);
                    System.Exception exceptionArticleS3 = null;
                    System.Exception exceptionDispatchS3 = null;
                    System.Exception exceptionReceiveS3 = null;
                    System.Exception exceptionInvoiceS3 = null;
                    System.Exception exceptionCancellationS3 = null;
                    bool load = false;

                    //try ArticleS3
                    ArticleS3 articleS3 = null;
                    load = ArticleS3.Serializer.Deserialize(xml, out articleS3, out exceptionArticleS3);

                    if (load)
                    {
                        NLogger.Info($"{Path.GetFileName(file)} - parse as Article");
                        DoProcArticleS3(file, articleS3, xml);
                        processingTime += ProcessingTime(DateTime.Now, created);
                        continue;
                    }

                    //try DispatchS3 //
                    DispatchS3 dispatchS3 = null;
                    load = DispatchS3.Serializer.Deserialize(xml, out dispatchS3, out exceptionDispatchS3);

                    if (load)
                    {
                        NLogger.Info($"{Path.GetFileName(file)} - parse as Dispatch");
                        DoProcDispatchS3(file, dispatchS3, xml);
                        processingTime += ProcessingTime(DateTime.Now, created);
                        continue;
                    }

                    //try ReceiveS3 //
                    ReceiveS3 receiveS3 = null;
                    load = ReceiveS3.Serializer.Deserialize(xml, out receiveS3, out exceptionReceiveS3);

                    if (load)
                    {
                        NLogger.Info($"{Path.GetFileName(file)} - parse as Receive");
                        DoProcReceiveS3(file, receiveS3, xml);
                        processingTime += ProcessingTime(DateTime.Now, created);
                        continue;
                    }

                    //try InvoiceS3 //
                    InvoiceS3 invoiceS3 = null;
                    load = InvoiceS3.Serializer.Deserialize(xml, out invoiceS3, out exceptionInvoiceS3);

                    if (load)
                    {
                        NLogger.Info($"{Path.GetFileName(file)} - parse as Invoice");
                        DoProcInvoiceS3(file, invoiceS3, xml);
                        processingTime += ProcessingTime(DateTime.Now, created);
                        continue;
                    }

                    //try CancellationS3 //
                    CancellationS3 cancellationS3 = null;
                    load = CancellationS3.Serializer.Deserialize(xml, out cancellationS3, out exceptionCancellationS3);

                    if (load)
                    {
                        NLogger.Info($"{Path.GetFileName(file)} - parse as Cancellation");
                        DoProcCancellationS3(file, cancellationS3, xml);
                        processingTime += ProcessingTime(DateTime.Now, created);
                        continue;
                    }

                    //Error parse
                    var errorParse = $"ArticleS3: {exceptionArticleS3.Message} - {exceptionArticleS3.InnerException.Message}, DispatchS3 {exceptionDispatchS3.Message} - {exceptionDispatchS3.InnerException.Message}, ReceiveS3 {exceptionReceiveS3.Message} - {exceptionReceiveS3.InnerException.Message}, InvoiceS3 {exceptionInvoiceS3.Message} - {exceptionInvoiceS3.InnerException.Message}, CancellationS3 {exceptionCancellationS3.Message} - {exceptionCancellationS3.InnerException.Message}";
                    DoError(file, $"Error parse file : {errorParse}");
                    processingTime += ProcessingTime(DateTime.Now, created);
                }
                catch (Exception ex)
                {
                    //log error
                    var writeError = new ErrorLogHelper();
                    writeError.ReportProcException(ex, _jobType, _jobName, NLogger, $"Error {ex.Message}");

                    DoError(file, ex.Message);
                    processingTime += ProcessingTime(DateTime.Now, created);
                }

            }

            if (_files.Count > 0)
            {
                var span = new TimeSpan(0, 0, 0, 0, processingTime / _files.Count);
                NLogger.Info($"End read files. Average processing time: {span.Hours.ToString("00")}:{span.Minutes.ToString("00")}:{span.Seconds.ToString("00")}");
            }
            else
                NLogger.Info($"End read files.");

        }

        private void DoError(string filePath, string errorMsg)
        {
            var fileName = Path.GetFileName(filePath);
            var error = $"{errorMsg} {fileName}";
            NLogger.Info($"{Path.GetFileName(filePath)} - error: {error}");

            // To move a file to a new location:
            var path = $"{_setting.Properties.PathError}\\{fileName}";
            var helper = new Helper.ExchangeHelper();
            helper.CheckFilePath(ref path);
            helper.TryMoveFile(filePath, path, true);
        }

        private void DoProcessed(string filePath, string message = "")
        {
            var fileName = Path.GetFileName(filePath);
            NLogger.Info($"File: {fileName} processed");

            if (!string.IsNullOrWhiteSpace(message))
                NLogger.Info(message);

            // To move a file to a new location:
            //create new dir
            var newDir = $@"{_setting.Properties.PathArchive}{DateTime.Now:yyyy}\{DateTime.Now:MMdd}";
            if (!Directory.Exists(newDir))
                Directory.CreateDirectory(newDir);

            var path = $"{newDir}\\{fileName}";
            var helper = new Helper.ExchangeHelper();
            helper.CheckFilePath(ref path);
            helper.TryMoveFile(filePath, path, true);
        }

        private void DoProcArticleS3(string filePath, ArticleS3 articleS3, string xml)
        {
            var model = new ArticleLoad()
            {
                ArticleS3 = articleS3,
                Xml = xml,
                Remark = _remark
            };

            var proc = new Procedures.Load.ArticleLoadProc(_userID);
            var res = proc.DoProcedure(model);
                      
            if (!res.Success)
                DoError(filePath, res.ErrorText);
            else
                DoProcessed(filePath);
        }

        private void DoProcDispatchS3(string filePath, DispatchS3 dispatchS3, string xml)
        {
            var model = new DispatchDirLoad()
            {
                DispatchS3 = dispatchS3,
                Xml = xml,
                Remark = _remark
            };

            var proc = new Procedures.Load.DispatchDirLoadProc(_userID);
            var res = proc.DoProcedure(model);
                      
            if (!res.Success)
                DoError(filePath, res.ErrorText);
            else
                DoProcessed(filePath, res.MessageText);

            if (res.InvoiceID != null)
                NLogger.Info($"Insert or update Invoice. InvoiceID: {res.InvoiceID}");

        }

        private void DoProcReceiveS3(string filePath, ReceiveS3 receiveS3, string xml)
        {
            var model = new ReceiveDirLoad()
            {
                ReceiveS3 = receiveS3,
                Xml = xml,
                Remark = _remark

            };

            var proc = new Procedures.Load.ReceiveDirLoadProc(_userID);
            var res = proc.DoProcedure(model);

            if (!res.Success)
                DoError(filePath, res.ErrorText);
            else
                DoProcessed(filePath);
        }

        private void DoProcInvoiceS3(string filePath, InvoiceS3 invoiceS3, string xml)
        {
            var model = new InvoiceLoad()
            {
                InvoiceS3 = invoiceS3,
                Xml = xml,
                Remark = _remark
            };

            var proc = new Procedures.Load.InvoiceLoadProc(_userID);
            var res = proc.DoProcedure(model);

            if (!string.IsNullOrEmpty(res.WarningText))
                NLogger.Warn(res.WarningText);


            if (!res.Success)
                DoError(filePath, res.ErrorText);
            else
            {
                string message;
                if (!string.IsNullOrWhiteSpace(res.ErrorText))
                    message = $"{res.MessageText} InvoiceID: {res.InvoceID} ErrorMessage: {res.ErrorText}";
                else
                    message = $"{res.MessageText} InvoiceID: {res.InvoceID}";

                DoProcessed(filePath, message);
            }
        }

        private void DoProcCancellationS3(string filePath, CancellationS3 cancellationS3, string xml)
        {
            var model = new CancellationLoad()
            {
                CancellationS3 = cancellationS3,
                Xml = xml,
                Remark = _remark
            };

            var proc = new Procedures.Load.CancellationLoadProc(_userID);
            var res = proc.DoProcedure(model);

            if (!res.Success)
                DoError(filePath, res.ErrorText);
            else
                DoProcessed(filePath);
        }

        private int ProcessingTime(DateTime processed, DateTime created)
        {
            var span = (processed - created);
            NLogger.Info($"Processing time {span.Hours.ToString("00")}:{span.Minutes.ToString("00")}:{span.Seconds.ToString("00")} ");
            return (int)span.TotalMilliseconds;
        }

        #endregion


    }
}

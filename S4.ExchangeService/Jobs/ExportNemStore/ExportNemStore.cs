﻿using CsvHelper;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using NPoco;
using RestSharp;
using S4.Core;
using S4.DAL;
using S4.Entities;
using S4.Entities.Helpers;
using S4.Entities.Helpers.Xml.Article;
using S4.Entities.Helpers.Xml.Dispatch;
using S4.Entities.Helpers.Xml.Receive;
using S4.Entities.Models;
using S4.ExchangeService.Config;
using S4.ExchangeService.Helper;
using S4.ExchangeService.Jobs.ExportNemStore.Models;
using S4.Export;
using S4.Export.Settings;
using S4.ProcedureModels.Load;
using S4.Procedures;
using S4.StoreCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace S4.ExchangeService.Jobs.ExportNemStore
{
    [Quartz.DisallowConcurrentExecution]
    class ExportNemStore : MessageBusBase, Quartz.IJob
    {

        #region Quartz.IJob

        public Task Execute(Quartz.IJobExecutionContext context)
        {
            try
            {
                _jobType = context.JobDetail.JobType.ToString();
                _jobName = context.JobDetail.Key.Name;

                NLogger = NLog.LogManager.GetLogger($"{Program.S4_JOB}.{_jobName}");
                NLogger.Info($"Start job {_jobName}");

                ReadSetting();
                DoWork(MessageBus.MessageBusTypeEnum.ExportNemStore);

            }
            catch (Exception exc)
            {
                var writeError = new ErrorLogHelper();
                writeError.ReportProcException(exc, _jobType, _jobName, NLogger);
            }

            NLogger.Info($"End job {_jobName}");
            return Task.CompletedTask;
        }

        #endregion


        #region public
        public override bool DoMakeExport(SettingsExport settingsExport, DirectionModel direction, MessageBus messageBus, ref List<int> doneDirs)
        {
            try
            {
                List<MoveResult> moveResult = new List<MoveResult>();
                using (var db = Core.Data.ConnectionHelper.GetDB())
                {
                    var save = Singleton<ServicesFactory>.Instance.GetSave();
                    var dispatchPositionsModel = Singleton<DAL.Cache.PositionByCategoryModelCache>.Instance.GetItem(Entities.Position.PositionCategoryEnum.Dispatch);
                    moveResult = save.MoveResult_ByDirection(db, direction.DirectionID, StoreMove.MO_DOCTYPE_DISPATCH, (mi) => dispatchPositionsModel.PositionIDs.Contains(mi.PositionID));
                }

                var deliveryNoteItemModelList = new List<DeliveryNoteItemModel>();
                foreach (var item in direction.Items)
                {
                    var packing = Singleton<DAL.Cache.ArticlePackingCache>.Instance.GetItem(item.ArtPackID);
                    var article = Singleton<DAL.Cache.ArticleModelCache>.Instance.GetItem(packing.ArticleID);

                    //batchList - expirace, šarže 
                    var mr = (from m in moveResult where m.DocPosition == item.DocPosition select m).GroupBy(m => new { m.ArtPackID, m.ExpirationDate, m.BatchNum }, (k, i) => new { k, Sum = i.Sum(x => x.Quantity) });

                    var batchList = new List<DeliveryNoteBatchModel>();
                    foreach (var mrItem in mr)
                        batchList.Add(new DeliveryNoteBatchModel { BatchNumber = mrItem.k.BatchNum, ExpirationDate = mrItem.k.ExpirationDate, Quantity = mrItem.Sum });

                    //EANList vsechny packingy z karty + s4_articleList_packing_barcodes
                    var EANList = new List<DeliveryNoteEANModel>();
                    foreach (var pack in article.Packings)
                    {
                        EANList.Add(new DeliveryNoteEANModel { EAN = pack.BarCode, EANPackingQuantity = pack.PackRelation });
                    }

                    var deliveryNoteItemModel = new DeliveryNoteItemModel
                    {
                        ArticleCode = item.ArticleCode,
                        ArticleDesc = article.ArticleDesc,
                        ArticlePackingId = packing.ArtPackID.ToString(),
                        BasicPackingQuantity = packing.PackRelation,
                        Quantity = item.Quantity,
                        Position = item.DocPosition,
                        BatchList = batchList,
                        EANList = EANList
                    };


                    deliveryNoteItemModelList.Add(deliveryNoteItemModel);
                }

                var deliveryNoteModel = new DeliveryNoteModel
                {
                    SupplierId = _setting.Properties.SupplierId,
                    DocInfo = direction.DocInfo,
                    DocPrefix = direction.DocNumPrefix,
                    DocNumber = direction.DocNumber,
                    DocDate = direction.DocDate,
                    UserEmail = direction.DocRemark,
                    Items = deliveryNoteItemModelList
                };

                //Send to API
                var error = "";
                var eventData = "";
                bool errorFinal = false;
                var body = Newtonsoft.Json.JsonConvert.SerializeObject(deliveryNoteModel);
                var resultSendAPI = Send2API(body, out error, out errorFinal);
                if (resultSendAPI == false)
                {
                    eventData = $"DirectionID: {direction.DirectionID} nebyl exportován do NemStore - chyba: {error}; data: {body}";
                    WriteError(messageBus, $"{error} ;data: {body}", errorFinal);

                    //log error
                    var message = $"directionID: {messageBus.DirectionID}";
                    AddDirectionHistory(direction.DirectionID, error, MessageBus.MessageBusTypeEnum.ExportNemStore);
                    NLogger.Error($"DirectionID: {messageBus.DirectionID} - {eventData}");
                    return false;
                }

                eventData = $"DirectionID: {direction.DirectionID} byl exportován do NemStore; data: {body}";
                WriteDone(messageBus);
                AddDirectionHistory(direction.DirectionID, eventData, MessageBus.MessageBusTypeEnum.ExportNemStore);
                NLogger.Info($"DirectionID: {messageBus.DirectionID} - {eventData}");

                return true;
            }
            catch (Exception exc)
            {
                var eventData = $"DirectionID: {direction.DirectionID} nebyl exportován do NemStore - chyba: {exc.Message}";
                WriteError(messageBus, exc.Message);

                //log error
                var message = $"directionID: {messageBus.DirectionID}";
                var writeError = new ErrorLogHelper();
                writeError.ReportProcException(exc, _jobType, _jobName, NLogger, message);

                AddDirectionHistory(direction.DirectionID, exc.Message, MessageBus.MessageBusTypeEnum.ExportNemStore);
                return false;
            }
        }

        #endregion

        #region private

        private bool Send2API(string body, out string errorMsg, out bool errorFinal)
        {
            errorMsg = "";
            errorFinal = false;
            
            //Call API
            var client = new RestClient(_setting.Properties.BaseURL);
            var request = new RestRequest(Method.POST);

            // add HTTP Parameters
            request.Parameters.Clear();
            request.RequestFormat = DataFormat.Json;
            request.AddParameter("application/json; charset=utf-8", body, ParameterType.RequestBody);

            var response = client.Execute(request);
            DeliveryNoteUploadResponseModel result = JsonConvert.DeserializeObject<DeliveryNoteUploadResponseModel>(response.Content);

            if (response.StatusCode != HttpStatusCode.OK)
            {
                if (response.StatusCode == HttpStatusCode.Conflict)
                    errorFinal = true;

                errorMsg = $"StatusCode: {response.StatusCode} ErrorDesc: {result.ErrorDesc}";
                return false;
            }

            if (result.Result == DeliveryNoteUploadResponseModel.ResultEnum.Success)
                return true;
            else
            {
                errorMsg = result.ErrorDesc;
                return false;
            }
        }

        #endregion


    }



}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.ExchangeService.Jobs.ExportNemStore.Models
{
    public class DeliveryNoteEANModel
    {
        public string EAN { get; set; }
        public decimal? EANPackingQuantity { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.ExchangeService.Jobs.ExportNemStore.Models
{
    public class DeliveryNoteModel
    {

        public int SupplierId { get; set; }         
        public string DocInfo { get; set; }
        public string DocPrefix { get; set; }
        public int DocNumber { get; set; }
        public DateTime DocDate { get; set; }
        public string DocRemark { get; set; }
        public string UserEmail { get; set; }

        public List<DeliveryNoteItemModel> Items { get; set; }
               
    }
}

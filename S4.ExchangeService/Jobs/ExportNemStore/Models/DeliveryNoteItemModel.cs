﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.ExchangeService.Jobs.ExportNemStore.Models
{
    public class DeliveryNoteItemModel
    {
        public int Position { get; set; }

        public string ArticleCode { get; set; }
        public string ArticleDesc { get; set; }
        public string ArticlePackingId { get; set; }
        public decimal? BasicPackingQuantity { get; set; }
        public List<DeliveryNoteEANModel> EANList { get; set; }

        public decimal Quantity { get; set; }
        public List<DeliveryNoteBatchModel> BatchList { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.ExchangeService.Jobs.ExportNemStore.Models
{
    public class DeliveryNoteUploadResponseModel
    {
        public enum ResultEnum
        {
            Success,
            BadData,
            InternalError,
            Conflict
        }

        public ResultEnum Result { get; set; }
        public string ErrorDesc { get; set; }
    }
}

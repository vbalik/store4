﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.ExchangeService.Jobs.ExportNemStore.Models
{
    public class DeliveryNoteBatchModel
    {
        public string BatchNumber { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public decimal Quantity { get; set; }
    }
}

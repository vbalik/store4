﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using NPoco;
using S4.Core;
using S4.Core.Utils;
using S4.DAL;
using S4.Entities;
using S4.Entities.Helpers;
using S4.Entities.Helpers.Xml.Article;
using S4.Entities.Helpers.Xml.Dispatch;
using S4.Entities.Helpers.Xml.Receive;
using S4.Entities.Models;
using S4.ExchangeService.Config;
using S4.ExchangeService.Helper;
using S4.Export;
using S4.Export.Settings;
using S4.ProcedureModels.Load;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace S4.ExchangeService.Jobs.SendMail
{
    [Quartz.DisallowConcurrentExecution]
    class SendMail : MessageBusBase, Quartz.IJob
    {

        #region Quartz.IJob

        public Task Execute(Quartz.IJobExecutionContext context)
        {
            try
            {
                _jobType = context.JobDetail.JobType.ToString();
                _jobName = context.JobDetail.Key.Name;

                NLogger = NLog.LogManager.GetLogger($"{Program.S4_JOB}.{_jobName}");
                NLogger.Info($"Start job {_jobName}");

                ReadSetting();
                DoWork(MessageBus.MessageBusTypeEnum.SendMail);

            }
            catch (Exception exc)
            {
                var writeError = new ErrorLogHelper();
                writeError.ReportProcException(exc, _jobType, _jobName, NLogger);
            }

            NLogger.Info($"End job {_jobName}");
            return Task.CompletedTask;
        }

        #endregion

        #region public 
        public override bool DoMakeExport(SettingsExport settingsExport, DirectionModel direction, MessageBus messageBus, ref List<int> doneDirs)
        {
            var dal = new MessageBusDocumentDAL();
            var documents = dal.GetDocumentsByMessageBusID(messageBus.MessageBusID);
            SendEmailInfo mailData = JsonConvert.DeserializeObject<SendEmailInfo>(messageBus.MessageData);

            var message = new MailMessage();
            message.From = new MailAddress(mailData.MessageFrom);
            message.Subject = mailData.MessageSubject;
            message.Body = mailData.MessageBody;

            var mail = new Export.MailHelper.Mail();
            var addAddressIsOK = mail.AddAddress(message.To, mailData.MessageTo, out string addAddressMsg);

            if (!addAddressIsOK)
                NLogger.Info(addAddressMsg);

            var pdfDocumentLength = 0;
            var msList = new List<MemoryStream>();
            foreach (var document in documents)
            {
                var ms = new MemoryStream(document.Document);
                msList.Add(ms);
                message.Attachments.Add(new Attachment(ms, document.DocumentName));
                pdfDocumentLength += document.Document.Length;
            }

            // send
            if (message.To.Count > 0)
            {
                // send
                var mailSetting = new Export.MailHelper.MailSetting();
                mailSetting.Message = message;
                mailSetting.Server = settingsExport.SettingMail.MailServer;
                mailSetting.User = settingsExport.SettingMail.MailUzivatel;
                mailSetting.Password = settingsExport.SettingMail.MailHeslo;
                                
                NLogger.Info($"Try send mail....");

                if (mail.SendMailViaSMTP(mailSetting, out string sendError))
                {
                    ClearMemoryStream(msList);
                    NLogger.Info($"Mail has been sent. Subject: {message.Subject}; to: {GetRecipients(message)} length: {pdfDocumentLength} b");
                    WriteDone(messageBus);
                    return true;
                }
                else
                {
                    ClearMemoryStream(msList);

                    //log error
                    var writeError = new ErrorLogHelper();
                    writeError.ReportProcException(null, _jobType, _jobName, NLogger, $"Error! Mail not sent! Error: {sendError}");

                    WriteError(messageBus, sendError);
                    return false;
                }
            }
            else
            {
                NLogger.Info($"No send mail, no recipients....");
            }

            return true;

        }
        #endregion

        #region private

        private void ClearMemoryStream(List<MemoryStream> msList)
        {
            foreach (var ms in msList)
                ms.Dispose();
        }

        private string GetRecipients(MailMessage message)
        {
            var rec = new StringBuilder();
            foreach (var item in message.To)
            {
                rec.Append($"{item.Address}, ");
            }

            foreach (var item in message.CC)
            {
                rec.Append($"{item.Address}, ");
            }

            return rec.ToString().Substring(0, rec.ToString().Length - 2);

        }

        #endregion


    }

}

﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using NPoco;
using S4.Core;
using S4.Core.Utils;
using S4.DAL;
using S4.Entities;
using S4.Entities.Helpers;
using S4.Entities.Helpers.Xml.Article;
using S4.Entities.Helpers.Xml.Dispatch;
using S4.Entities.Helpers.Xml.Receive;
using S4.Entities.Models;
using S4.ExchangeService.Config;
using S4.ExchangeService.Helper;
using S4.Export;
using S4.Export.Settings;
using S4.ProcedureModels.Load;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace S4.ExchangeService.Jobs.Heartbeat
{
    [Quartz.DisallowConcurrentExecution]
    class Heartbeat : Quartz.IJob
    {
        private NLog.Logger NLogger = null;
        private ExchangeServiceSettings _setting = null;
        private string _jobName = "";
        private string _jobType = "";

        #region Quartz.IJob

        public async Task Execute(Quartz.IJobExecutionContext context)
        {
            try
            {
                _jobType = context.JobDetail.JobType.ToString();
                _jobName = context.JobDetail.Key.Name;

                NLogger = NLog.LogManager.GetLogger($"{Program.S4_JOB}.{_jobName}");
                NLogger.Info($"Start job {_jobName}");

                ReadSetting(_jobType, _jobName);
                await DoHeartbeat();

            }
            catch (Exception exc)
            {
                var writeError = new ErrorLogHelper();
                writeError.ReportProcException(exc, _jobType, _jobName, NLogger);
            }

            NLogger.Info($"End job {_jobName}");
        }

        #endregion

        #region private
        private async Task DoHeartbeat()
        {
            using (var client = new HttpClient(new HttpClientHandler()))
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.Timeout = TimeSpan.FromSeconds(15);
                
                var data = new
                {
                    _setting.Properties.ServerName,
                };

                var formatter = new JsonMediaTypeFormatter
                {
                    SerializerSettings = { TypeNameHandling = TypeNameHandling.Objects }
                };

                var response = await client.PostAsync($"{_setting.Properties.DutyServerURL}{_setting.Properties.ServerName}", data, formatter);

                if (!response.IsSuccessStatusCode)
                {
                    //log error
                    var writeError = new ErrorLogHelper();
                    writeError.ReportProcException(null, _jobType, _jobName, NLogger, $"PostAsync {_setting.Properties.DutyServerURL}{_setting.Properties.ServerName} - Error");
                }

            }
        }


        private void ReadSetting(string jobType, string jobName)
        {
            var helper = new ExchangeHelper();
            _setting = helper.ReadSetting(jobType, jobName);

            if (_setting == null)
                throw new Exception($"{jobName} setting not found!");
        }
        #endregion


    }

}

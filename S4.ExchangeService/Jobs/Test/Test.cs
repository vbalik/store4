﻿using Microsoft.Extensions.Configuration;
using S4.ExchangeService.Helper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace S4.ExchangeService.Jobs.Test
{
    [Quartz.DisallowConcurrentExecution]
    class Test : Quartz.IJob
    {


        #region Quartz.IJob
        private static NLog.Logger NLogger = NLog.LogManager.GetLogger("S4.Exchange.Job.Test");

        public Task Execute(Quartz.IJobExecutionContext context)
        {
            var path = $"c:\\temp\\{context.JobDetail.Key}_{Guid.NewGuid()}.txt";
            var jobName = nameof(Test);

            NLogger.Info($"Start job {jobName}");

            using (var sw = File.CreateText(path))
            {
                for (int i = 0; i < 10; i++)
                {
                    var txt = $"Test WriteLine {DateTime.Now}";
                    sw.WriteLine(txt);
                    NLogger.Info($"{txt} to file: {path}");
                    //Thread.Sleep(5000);
                }
            }

            NLogger.Info($"End job: {jobName}");

            return Task.CompletedTask;

                                  
        }

        #endregion


    }
}

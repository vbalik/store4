﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Linq;
using PdfiumPrinter;

using S4.ExchangeService.Config;
using S4.ProcedureModels.Delivery;
using S4.Procedures.Delivery;
using S4.Entities;
using S4.Procedures;
using System.Drawing.Printing;
using S4.DeliveryServices;
using S4.ExchangeService.Helper;

namespace S4.ExchangeService.Jobs.Delivery
{
    [Quartz.DisallowConcurrentExecution]
    class Delivery : Quartz.IJob
    {
        private string _jobType = "";
        private string _jobName = "";

        private static ExchangeServiceSettings _setting = null;
        private static string _userID;
        private NLog.Logger NLogger = null;

        #region Quartz.IJob

        public Task Execute(Quartz.IJobExecutionContext context)
        {
            try
            {
                _jobType = context.JobDetail.JobType.ToString();
                _jobName = context.JobDetail.Key.Name;

                NLogger = NLog.LogManager.GetLogger($"{Program.S4_JOB}.{_jobName}");
                NLogger.Info($"Start job {_jobName}");

                ReadSetting();
                DoWork();
            }
            catch (DeliveryException dexc)
            {
                //only write to log
                NLogger.Warn(dexc, $"Error {dexc.Message}");
            }
            catch (Exception exc)
            {
                var writeError = new ErrorLogHelper();
                writeError.ReportProcException(exc, _jobType, _jobName, NLogger);
            }

            NLogger.Info($"End job {_jobName}");
            return Task.CompletedTask;
        }

        #endregion

        #region private

        private void ReadSetting()
        {
            //userID
            _userID = Program.Settings.GetValue<string>("userID").ToString();

            var exchangeServiceSettings = new List<Config.ExchangeServiceSettings>();
            Program.Settings.GetSection("Jobs").Bind(exchangeServiceSettings);
            _setting = (from e in exchangeServiceSettings where e.JobClass.Equals(_jobType) && e.JobName.Equals(_jobName) select e).FirstOrDefault();

            if (_setting == null)
                throw new Exception("Delivery setting not found!");
        }

        private async void DoWork()
        {
            var directionDelivery = new S4.DeliveryServices.DeliveryService(Core.Data.ConnectionHelper.ConnectionString);
            directionDelivery.LogEvent += SendToLog;
            directionDelivery.GetLabelEvent += (s, args) =>
            {
                bool landscape = false;
                PaperSize ps;

                switch (args.DeliveryService.DeliveryProvider)
                {
                    case Entities.DeliveryDirection.DeliveryProviderEnum.PPL:
                        var defaultPrinterLocationID = new ServicesFactory()
                            .GetDeliveryService()
                            .GetDefaultPrinterLocationID(DeliveryDirection.DeliveryProviderEnum.PPL.ToString());

                        var model = new PrintLabel
                        {
                            DirectionID = int.Parse(args.ReferenceId),
                            PrinterLocationID = defaultPrinterLocationID
                        };

                        var print = new PrintLabelProc(_userID);
                        print.DoProcedure(model);
                        break;
                    case DeliveryDirection.DeliveryProviderEnum.DPD:
                    case DeliveryDirection.DeliveryProviderEnum.DPDNew:
                        ps = new PaperSize();
                        ps.Height = 492;
                        ps.Width = 417;

                        SendToPrinter(args.Data, _setting.Properties.LabelsPrinter, landscape, ps);
                        break;
                    case DeliveryDirection.DeliveryProviderEnum.CPOST:
                        ps = new PaperSize();
                        ps.Height = 492;
                        ps.Width = 417;

                        SendToPrinter(args.Data, _setting.Properties.LabelsPrinter, landscape, ps);
                        break;
                    default:
                        SendToPrinter(args.Data, _setting.Properties.LabelsPrinter, landscape, null);
                        break;
                }
            };
            directionDelivery.InsertShipmentEvent += DirectionDelivery_InsertShipmentEvent;
            directionDelivery.LabelPrintedEvent += DirectionDelivery_LabelPrintedEvent;
            directionDelivery.ManifestCreatedEvent += DirectionDelivery_ManifestCreatedEvent;
            directionDelivery.GetManifestReportEvent += (s, args) =>
            {
                bool landscape = false;
                switch (args.DeliveryService.DeliveryProvider)
                {
                    case Entities.DeliveryDirection.DeliveryProviderEnum.DPD: landscape = true; break;
                }
                SendToPrinter(args.Data, _setting.Properties.ManifestPrinter, landscape, null);
            };
            directionDelivery.SetShipmentFailedEvent += DirectionDelivery_SetShipmentFailedEvent;
            await directionDelivery.ScanDirectionsForShipment();
            await directionDelivery.CreateWaitingManifests();
        }

        private void DirectionDelivery_SetShipmentFailedEvent(DeliveryServices.IDeliveryService sender, int directionID, string text)
        {
            var model = new DirectionSetShipmentFailedStatus()
            {
                DirectionID = directionID,
                Text = text
            };

            var result = new DirectionSetShipmentFailedStatusProc(_userID).DoProcedure(model);
            if (!result.Success)
            {
                SendToLog(sender, result.ErrorText, DeliveryServices.LogLevelEnum.Error);
            }
        }

        private void DirectionDelivery_ManifestCreatedEvent(DeliveryServices.IDeliveryService sender, int deliveryManifestID, string manifestRefNumber)
        {
            var model = new ManifestCreated()
            {
                DeliveryManifestID = deliveryManifestID,
                ManifestRefNumber = manifestRefNumber
            };

            var result = new ManifestCreatedProc(_userID).DoProcedure(model);
            if (!result.Success)
            {
                SendToLog(sender, result.ErrorText, DeliveryServices.LogLevelEnum.Error);
            }
        }

        private void DirectionDelivery_LabelPrintedEvent(DeliveryServices.IDeliveryService sender, int directionID)
        {
            //check DOCSTATUS
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var docStatus = db.SingleOrDefaultById<Direction>(directionID).DocStatus;
                if (docStatus != Direction.DR_STATUS_DELIVERY_SHIPMENT)
                {
                    SendToLog(sender, $"Doklad nemá DocStatus DR_STATUS_DELIVERY_SHIPMENT; DirectionID: {directionID}", DeliveryServices.LogLevelEnum.Warn);
                    return;
                }
                else
                {
                    var result = new LabelPrintedProc(_userID).DoProcedure(new ProcedureModels.Delivery.LabelPrinted() { DirectionID = directionID });
                    if (!result.Success)
                    {
                        SendToLog(sender, result.ErrorText, DeliveryServices.LogLevelEnum.Error);
                    }
                }
            }

        }

        private bool DirectionDelivery_InsertShipmentEvent(DeliveryServices.IDeliveryService sender, int directionID, string shipmentReference, Tuple<int, string>[] parcels, out string error)
        {
            error = null;
            try
            {
                var insertShipment = new InsertShipment()
                {
                    DirectionID = directionID,
                    ShipmentReference = shipmentReference,
                    DeliveryProvider = sender.DeliveryProvider,
                    Parcels = parcels
                };

                var result = new InsertShipmentProc(_userID).DoProcedure(insertShipment);
                if (!result.Success)
                {
                    error = result.ErrorText;
                    return false;
                }
            }
            catch (Exception ex)
            {
                error = ex.ToString();
                return false;
            }

            return true;
        }

        private void SendToLog(DeliveryServices.IDeliveryService sender, string message, S4.DeliveryServices.LogLevelEnum logLevel, Exception exception = null)
        {
            var writeError = new ErrorLogHelper();
            message = $"{(sender == null ? "ALL" : sender.DeliveryProvider.ToString())}>{message}";
            switch (logLevel)
            {
                case S4.DeliveryServices.LogLevelEnum.Debug:
                    NLogger.Debug(message);
                    break;
                case S4.DeliveryServices.LogLevelEnum.Error:
                    writeError.ReportProcException(exception, _jobType, _jobName, NLogger, message);
                    break;
                case S4.DeliveryServices.LogLevelEnum.Fatal:
                    writeError.ReportProcException(exception, _jobType, _jobName, NLogger, message);
                    NLogger.Fatal(message);
                    break;
                case S4.DeliveryServices.LogLevelEnum.Info:
                    NLogger.Info(message);
                    break;
                case S4.DeliveryServices.LogLevelEnum.Trace:
                    NLogger.Trace(message);
                    break;
                case S4.DeliveryServices.LogLevelEnum.Warn:
                    NLogger.Warn(message);
                    break;
            }
        }

        private void SendToPrinter(byte[] data, string printerName, bool landscape, PaperSize paperSizeSettings)
        {
            var pdfPrinter = new PdfPrinter(printerName);

            if (paperSizeSettings != null)
            {
                pdfPrinter.PageSettings.PaperSize = paperSizeSettings;
            }

            pdfPrinter.Print(new System.IO.MemoryStream(data));
        }

        #endregion
    }
}

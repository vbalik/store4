﻿using Microsoft.Extensions.Configuration;
using S4.Entities.Helpers;
using S4.Entities.Helpers.Xml.Article;
using S4.Entities.Helpers.Xml.Dispatch;
using S4.Entities.Helpers.Xml.Receive;
using S4.ExchangeService.Config;
using S4.ProcedureModels.Load;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using S4.Report;
using System.Net.Mail;
using System.Net;
using Newtonsoft.Json.Linq;
using S4.Core;
using NPoco;
using S4.Entities;
using System.Text;
using S4.ProcedureModels.Extensions;
using S4.ExchangeService.Helper;

namespace S4.ExchangeService.Jobs.SendAlert
{
    [Quartz.DisallowConcurrentExecution]
    class SendAlert : Quartz.IJob
    {
        private string _jobType = "";
        private string _jobName = "";
        private ExchangeServiceSettings _setting = null;
        private NLog.Logger NLogger = null;

        #region Quartz.IJob

        public Task Execute(Quartz.IJobExecutionContext context)
        {
            try
            {
                _jobType = context.JobDetail.JobType.ToString();
                _jobName = context.JobDetail.Key.Name;

                NLogger = NLog.LogManager.GetLogger($"{Program.S4_JOB}.{_jobName}");
                NLogger.Info($"Start job {_jobName}");

                //read setting
                ReadSetting();

                //do Work
                DoWork();


            }
            catch (Exception exc)
            {
                var writeError = new ErrorLogHelper();
                writeError.ReportProcException(exc, _jobType, _jobName, NLogger);
            }

            NLogger.Info($"End job {_jobName}");
            return Task.CompletedTask;
        }

        #endregion

        #region private

        private void ReadSetting()
        {
            var helper = new ExchangeHelper();
            _setting = helper.ReadSetting(_jobType, _jobName);

            if (_setting == null)
                throw new Exception($"{nameof(SendAlert)} setting not found!");

            //read setting mail
            _setting.Properties.SettingMail = S4.Export.Settings.Helper.GetSettingMail();
        }

        private void DoWork()
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //SQL
                var sql = new NPoco.Sql();
                for (int i = 0; i < _setting.Properties.SqlQuery.Length; i++)
                    sql.Append(_setting.Properties.SqlQuery[i]);

                var rows = db.Fetch<dynamic>(sql);

                //SQL read rows count
                var sqlRowsLimitCount = new NPoco.Sql();
                for (int i = 0; i < _setting.Properties.CountRowsLimit.Length; i++)
                    sqlRowsLimitCount.Append(_setting.Properties.CountRowsLimit[i]);

                var alertLimit = db.First<decimal>(sqlRowsLimitCount);
                var alertLimitMin = _setting.Properties.CountRowsLimitMinimum;
                var alertLimitMax = _setting.Properties.CountRowsLimitMaximum;

                if (alertLimitMin > 0)
                    alertLimit = alertLimit <= alertLimitMin ? alertLimitMin : alertLimit;

                if (alertLimitMax > 0)
                    alertLimit = alertLimit >= alertLimitMax ? alertLimitMax : alertLimit;

                NLogger.Info($"Rows found count: {rows.Count}; Alert limit: {alertLimit};");

                if (rows.Count >= alertLimit)
                {
                    var tableRows = new StringBuilder();

                    //header
                    var line = new StringBuilder();
                    line.AppendLine("<tr>");
                    foreach (var item in rows[0])
                    {
                        line.AppendLine($"<th><small>{item.Key}</small></th>");
                    }
                    line.AppendLine("</tr>");
                    tableRows.AppendLine(line.ToString());

                    //rows
                    foreach (var row in rows)
                    {
                        line = new StringBuilder();
                        line.AppendLine("<tr>");
                        foreach (var item in row)
                        {
                            line.AppendLine($"<td><small><pre>{item.Value}</pre></small></td>");
                        }
                        line.AppendLine("</tr>");
                        tableRows.AppendLine(line.ToString());
                    }

                    //DB name
                    var i1 = db.ConnectionString.IndexOf("Database=");
                    var i2 = db.ConnectionString.IndexOf(";", i1);
                    var databaseName = db.ConnectionString.Substring(i1, i2 - i1);

                    var toBodyRows = new StringBuilder();
                    for (int i = 0; i < _setting.Properties.ToBodyRows.Length; i++)
                        toBodyRows.AppendLine(_setting.Properties.ToBodyRows[i]);

                    var body = new StringBuilder();
                    body.AppendLine("<!DOCTYPE html>");
                    body.AppendLine("<html>");
                    body.AppendLine("<head>");
                    body.AppendLine("<style>table, th, td {  border:1px solid black;}</style>");
                    body.AppendLine("</head>");

                    if (toBodyRows.Length > 0)
                        body.AppendLine($"<h2>{toBodyRows}</h2>");

                    body.AppendLine($"<b>DB: {databaseName}</b>");
                    body.AppendLine("<br>");
                    body.AppendLine($"<b>Počet vět: {rows.Count}</b>");
                    body.AppendLine("<br>");
                    
                    body.AppendLine("<table>");
                    body.AppendLine(tableRows.ToString());
                    body.AppendLine("</table>");

                    //table
                    var bodyTXT = body.ToString();
                    NLogger.Info($"Počet vět: {rows.Count}");
                    SendMail(bodyTXT, databaseName);
                }
            }

        }

        private void SendMail(string body, string databaseName)
        {
            if (string.IsNullOrWhiteSpace(_setting.Properties.MailTo) || string.IsNullOrWhiteSpace(_setting.Properties.SettingMail.MailServer))
            {
                NLogger.Info($"Mail not send");
                return;
            }

            var message = new MailMessage();
            message.From = new MailAddress(_setting.Properties.SettingMail.MailFrom);
            message.Subject = $"{_setting.Properties.MailSubject} {databaseName}";
            message.IsBodyHtml = true;
            message.Body = body;

            //add recipient
            var mail = new Export.MailHelper.Mail();
            var addAddressIsOK = mail.AddAddress(message.To, _setting.Properties.MailTo, out string addAddressMsg);

            if (!addAddressIsOK)
                NLogger.Info(addAddressMsg);

            // send
            if ((message.CC.Count > 0 || message.To.Count > 0))
            {
                // send
                var mailSetting = new Export.MailHelper.MailSetting();
                mailSetting.Message = message;
                mailSetting.Server = _setting.Properties.SettingMail.MailServer;
                mailSetting.User = _setting.Properties.SettingMail.MailUzivatel;
                mailSetting.Password = _setting.Properties.SettingMail.MailHeslo;

                NLogger.Info($"Try send mail....");

                if (mail.SendMailViaSMTP(mailSetting, out string sendError))
                {
                    NLogger.Info($"Mail has been sent. Subject: {message.Subject}; to: {_setting.Properties.MailTo}");
                    return;
                }
                else
                {
                    //error
                    NLogger.Info($"Error! Mail not sent!");
                    NLogger.Info(sendError);
                    return;
                }
            }
            else
            {
                NLogger.Info($"No send mail, no recipients....");
            }


        }


        #endregion


    }
}

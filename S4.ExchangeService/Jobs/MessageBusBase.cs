﻿using Microsoft.Extensions.Configuration;
using S4.DAL;
using S4.Entities;
using S4.Entities.Models;
using S4.ExchangeService.Config;
using S4.ExchangeService.Helper;
using S4.Export.Settings;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace S4.ExchangeService.Jobs
{
    public abstract class MessageBusBase
    {
        protected string _jobName = "";
        protected string _jobType = "";
        protected ExchangeServiceSettings _setting = null;
        protected string _userID;
        protected NLog.Logger NLogger = null;

        private string _messageStatus = "";

        public abstract bool DoMakeExport(SettingsExport SettingsExport, DirectionModel direction, MessageBus messageBus, ref List<int> doneDirs);


        protected void ReadSetting()
        {
            _userID = Program.Settings.GetValue<string>("userID").ToString();

            var helper = new ExchangeHelper();
            _setting = helper.ReadSetting(_jobType, _jobName);

            if (_setting == null)
                throw new Exception($"{_jobName} setting not found!");
        }

        protected void DoWork(MessageBus.MessageBusTypeEnum type)
        {
            var isDirection = type == MessageBus.MessageBusTypeEnum.ExportABRA || type == MessageBus.MessageBusTypeEnum.ExportDirection || type == MessageBus.MessageBusTypeEnum.ExportNemStore  ? true : false;
            var dal = new MessageBusDAL();
            var rows = dal.GetDataNoProcessedByType(type, type == MessageBus.MessageBusTypeEnum.ExportDirection ? true : false);
            //read setting
            SettingsExport settingsExport = null; 

            if (type != MessageBus.MessageBusTypeEnum.ExportNemStore)
                settingsExport = S4.Export.Settings.Helper.GetSettingsExport();

            int exportCount = 0;
            NLogger.Info($"Rows count: {rows.Count}");
            var doneDirs = new List<int>();

            foreach (var row in rows)
            {
                try
                {
                    if (type == MessageBus.MessageBusTypeEnum.ExportDirection)
                    {
                        //check if is Direction ready (kontrola kvůli sběrným fakturám)
                        var done = (from d in doneDirs where d == row.DirectionID select true).FirstOrDefault();
                        if (done)
                        {
                            NLogger.Info($"DirectionID: {row.DirectionID} is done, continue to next row....");
                            continue;
                        }
                    }

                    //find direction
                    DirectionModel direction = null;
                    if (isDirection)
                    {
                        direction = (new DAL.DirectionDAL()).GetDirectionAllData(row.DirectionID);

                        if (direction == null)
                        {
                            NLogger.Info($"DirectionID: {row.DirectionID} not found!");
                            WriteError(row, $"DirectionID: {row.DirectionID} nebyl nalezen!", true); //error final
                            continue;
                        }

                        NLogger.Info($"DirectionID: {row.DirectionID}  docInfo: {direction.DocInfo} - Start processing");
                    }
                    else
                        NLogger.Info($"MessageBusID: {row.MessageBusID} - Start processing");

                    var doneDirsRef = new List<int>();
                    if (DoMakeExport(settingsExport, direction, row, ref doneDirsRef))
                        exportCount++;

                    if (type == MessageBus.MessageBusTypeEnum.ExportDirection && doneDirsRef.Count > 0)
                        doneDirs.AddRange(doneDirsRef);

                    if (isDirection)
                        NLogger.Info($"DirectionID: {row.DirectionID} docInfo: {direction.DocInfo} - End processing....");
                    else
                        NLogger.Info($"MessageBusID: {row.MessageBusID} - End processing");

                }
                catch (Exception ex)
                {
                    if (isDirection)
                    {
                        NLogger.Info($"Chyba při zpracování exportu dokladu directionID: {row.DirectionID}");
                                                
                        //log error
                        var writeError = new ErrorLogHelper();
                        writeError.ReportProcException(ex, _jobType, _jobName, NLogger, $"directionID: {row.DirectionID}");

                        NLogger.Info($"DirectionID: {row.DirectionID} - End processing....");
                    }
                    else
                    {
                        NLogger.Info($"Chyba při zpracování věty messageBusID: {row.MessageBusID}");
                                                
                        //log error
                        var writeError = new ErrorLogHelper();
                        writeError.ReportProcException(ex, _jobType, _jobName, NLogger, $"messageBusID: {row.MessageBusID}");

                        NLogger.Info($"MessageBusID: {row.MessageBusID} - End processing....");
                    }
                }
            }

            if (isDirection)
                NLogger.Info($"End job. Exported directions: {exportCount} Errors: {rows.Count - exportCount}");
            else
                NLogger.Info($"End job. Rows messageBus: {exportCount} Errors: {rows.Count - exportCount}");


        }

        protected void WriteError(MessageBus messageBus, string error, bool errorFinal = false)
        {
            if ((errorFinal) || (messageBus.MessageStatus == MessageBus.WAITING_FOR_REPEAT && messageBus.TryCount >= _setting.Properties.TryMinutes.Length))
            {
                //to error state
                messageBus.MessageStatus = MessageBus.ERROR;
                _messageStatus = MessageBus.ERROR;
            }
            else
            {
                messageBus.TryCount++;
                int addMin;
                if (messageBus.MessageStatus != MessageBus.WAITING_FOR_COMPLETE)
                    addMin = messageBus.TryCount - 1 > _setting.Properties.TryMinutes.Count() ? _setting.Properties.TryMinutes[_setting.Properties.TryMinutes.Count() - 1] : _setting.Properties.TryMinutes[messageBus.TryCount - 1];
                else
                    addMin = _setting.Properties.TryMinutesForWaitingComplete;

                messageBus.NextTryTime = DateTime.Now.AddMinutes(addMin);
                messageBus.MessageStatus = messageBus.MessageStatus == MessageBus.WAITING_FOR_COMPLETE ? MessageBus.WAITING_FOR_COMPLETE : MessageBus.WAITING_FOR_REPEAT;
                _messageStatus = messageBus.MessageStatus;
            }

            messageBus.LastDateTime = DateTime.Now;
            messageBus.ErrorDesc = $"{error} ({DateTime.Now})";

            //update
            var dal = new MessageBusDAL();
            dal.Update(messageBus);

            if (messageBus.DirectionID > 0)
            {
                NLogger.Info($"Error --> MessageBusID: {messageBus.MessageBusID}, DirectionID: {messageBus.DirectionID} - {error}");
                NLogger.Info($"Chyba při zpracování exportu dokladu directionID: {messageBus.DirectionID}");
            }
            else
            {
                NLogger.Info($"Error --> MessageBusID: {messageBus.MessageBusID} - {error}");
            }
        }

        protected void WriteDone(MessageBus messageBus)
        {
            messageBus.MessageStatus = MessageBus.DONE;
            messageBus.LastDateTime = DateTime.Now;
            _messageStatus = MessageBus.DONE;

            //update
            var dal = new MessageBusDAL();
            dal.Update(messageBus);

            if (messageBus.DirectionID > 0)
                NLogger.Info($"OK --> MessageBusID: {messageBus.MessageBusID}, DirectionID: {messageBus.DirectionID}");
            else
                NLogger.Info($"OK --> MessageBusID: {messageBus.MessageBusID}");
        }

        protected void WriteNoComplete(MessageBus messageBus, string error)
        {
            messageBus.MessageStatus = MessageBus.NOCOMPLETE;
            messageBus.LastDateTime = DateTime.Now;
            messageBus.ErrorDesc = $"{error} ({DateTime.Now})";
            _messageStatus = MessageBus.NOCOMPLETE;

            //update
            var dal = new MessageBusDAL();
            dal.Update(messageBus);

            if (messageBus.DirectionID > 0)
                NLogger.Info($"OK --> MessageBusID: {messageBus.MessageBusID}, DirectionID: {messageBus.DirectionID}");
            else
                NLogger.Info($"OK --> MessageBusID: {messageBus.MessageBusID}");
        }

        protected void AddDirectionHistory(int directionID, string eventData, MessageBus.MessageBusTypeEnum type)
        {
            if (_messageStatus == MessageBus.DONE || _messageStatus == MessageBus.ERROR || _messageStatus == MessageBus.NOCOMPLETE) // write history
            {
                //Add history
                var history = new DirectionHistory();
                history.DirectionID = directionID;
                history.EventCode = type == MessageBus.MessageBusTypeEnum.ExportABRA ? Direction.DR_EVENT_EXPORT_ABRA : Direction.DR_EVENT_EXPORT_DONE;
                history.DirectionHistID = 0;
                history.EntryDateTime = DateTime.Now;
                history.EntryUserID = _userID;
                history.EventData = eventData.Substring(eventData.Length - 1, 1).Equals("|") ? eventData.Substring(0, eventData.Length - 1) : eventData;

                new DAL.DirectionDAL().AddDirectionHistory(history);
                NLogger.Info($"DirectionID: {directionID} add to history data: {history.EventData}");
            }
        }
    }
}

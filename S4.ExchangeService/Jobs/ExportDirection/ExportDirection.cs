﻿using Microsoft.Extensions.Configuration;
using NPoco;
using S4.Core;
using S4.Core.Utils;
using S4.DAL;
using S4.Entities;
using S4.Entities.Helpers;
using S4.Entities.Helpers.Xml.Article;
using S4.Entities.Helpers.Xml.Dispatch;
using S4.Entities.Helpers.Xml.Receive;
using S4.Entities.Models;
using S4.ExchangeService.Config;
using S4.ExchangeService.Helper;
using S4.Export;
using S4.Export.Settings;
using S4.ProcedureModels.Load;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;
using static S4.Export.Export;

namespace S4.ExchangeService.Jobs.ExportDirection
{
    [Quartz.DisallowConcurrentExecution]
    class ExportDirection : MessageBusBase, Quartz.IJob
    {
        private SettingsExport _settingsExport = null;

        #region Quartz.IJob

        public Task Execute(Quartz.IJobExecutionContext context)
        {
            try
            {
                _jobType = context.JobDetail.JobType.ToString();
                _jobName = context.JobDetail.Key.Name;

                NLogger = NLog.LogManager.GetLogger($"{Program.S4_JOB}.{_jobName}");
                NLogger.Info($"Start job {_jobName}");

                ReadSetting();
                DoWork(MessageBus.MessageBusTypeEnum.ExportDirection);

            }
            catch (Exception exc)
            {
                var writeError = new ErrorLogHelper();
                writeError.ReportProcException(exc, _jobType, _jobName, NLogger);
            }

            NLogger.Info($"End job {_jobName}");
            return Task.CompletedTask;
        }

        #endregion

        #region public 
        public override bool DoMakeExport(SettingsExport settingsExport, DirectionModel direction, MessageBus messageBus, ref List<int> doneDirs)
        {
            var eventData = "";
            _settingsExport = settingsExport;
            var error = "";
            var history = (from h in direction.History where h.EventCode == Direction.DR_STATUS_LOAD select h).FirstOrDefault();
            if (history == null)
            {
                error = $"DirectionID: {direction.DirectionID} docInfo: {direction.DocInfo} - there is no history";
                NLogger.Info(error);
                WriteNoComplete(messageBus, error);
                AddDirectionHistory(direction.DirectionID, error, MessageBus.MessageBusTypeEnum.ExportDirection);
                return false;
            }

            DispatchS3 dispatchS3 = null;
            var load = DispatchS3.Serializer.Deserialize(history.EventData, out dispatchS3);

            if (load != true)
            {
                error = $"DirectionID: {direction.DirectionID} docInfo: {direction.DocInfo} - XML is not valid DispatchS3";
                NLogger.Info(error);
                WriteNoComplete(messageBus, error);
                AddDirectionHistory(direction.DirectionID, error, MessageBus.MessageBusTypeEnum.ExportDirection);
                return false;
            }

            //is valid
            if (!IsExportValid(dispatchS3, out string errorOut))
            {
                error = $"Chyba při kontrole XML: {errorOut}";
                NLogger.Info(error);
                WriteNoComplete(messageBus, error);
                AddDirectionHistory(direction.DirectionID, error, MessageBus.MessageBusTypeEnum.ExportDirection);
                return false;
            }

            NLogger.Info($"Exporting DirectionID: {direction.DirectionID} docInfo: {direction.DocInfo}");

            if (IsIgnore(direction.DocNumPrefix))
            {
                error = $"DirectionID: {direction.DirectionID} prefix: {direction.DocNumPrefix} je ignorován";
                NLogger.Info(error);
                WriteNoComplete(messageBus, error);
                AddDirectionHistory(direction.DirectionID, error, MessageBus.MessageBusTypeEnum.ExportDirection);
                return false;
            }

            Encoding encoding = Encoding.Default;
            var ret = S4.Export.Export.MakeExport(settingsExport, direction, dispatchS3, NLogger, out encoding);

            if (ret.ResultEnum != S4.Export.Export.ResultEnum.Sended)
            {
                NLogger.Info($"DirectionID: {direction.DirectionID} - chyba: '{ret.Message}'");
                switch (ret.ResultEnum)
                {
                    case Export.Export.ResultEnum.NotNeeded:
                        eventData = "Not needed";
                        WriteDone(messageBus);
                        return true;
                    case Export.Export.ResultEnum.NotReady:
                        messageBus.MessageStatus = MessageBus.WAITING_FOR_COMPLETE;
                        WriteError(messageBus, ret.Message);
                        AddDirectionHistory(direction.DirectionID, ret.Message, MessageBus.MessageBusTypeEnum.ExportDirection);
                        return false;
                    case Export.Export.ResultEnum.ErrorFinal:
                        WriteError(messageBus, ret.Message, true); //error final
                        AddDirectionHistory(direction.DirectionID, ret.Message, MessageBus.MessageBusTypeEnum.ExportDirection);
                        return false;
                    case Export.Export.ResultEnum.Error:
                        WriteError(messageBus, ret.Message);
                        AddDirectionHistory(direction.DirectionID, ret.Message, MessageBus.MessageBusTypeEnum.ExportDirection);
                        return false;
                }

                return false;
            }

            var mailIsSendError = false;

            try
            {
                ExportResult[] abraExpResults = null;
                if (S4.Export.Export.AbraSend(dispatchS3))
                    //tohle jde ve formatu LEKIS5 do Abry!
                    abraExpResults = S4.Export.Export.MakeExport(settingsExport, direction, dispatchS3, ExportFormat.LEKIS5, out encoding).ExpResults;

                eventData = SendResult(direction, dispatchS3, ret.ExpResults, abraExpResults, encoding, ref mailIsSendError);
            }
            catch (IOException ioExc)
            {
                //messageBus.MessageStatus = MessageBus.WAITING_FOR_REPEAT;
                WriteError(messageBus, ioExc.Message);
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //format
            Enum.TryParse(dispatchS3.Customer.Extradata.FormatDiskety, out S4.Export.Export.ExportFormat format);
            if (S4.Export.Export.AbraSend(dispatchS3))
            {
                format = S4.Export.Export.ExportFormat.PDK18;
            }
                

            var eventData2History = $"Formát: {format}|{eventData}";

            var succes = false;
            if (!mailIsSendError)
                succes = true;

            //sberna faktura
            if (ret.DoneDirs.Count > 1)
            {
                var dirDAL = new DirectionDAL();
                var result = new StringBuilder();
                foreach (var dirID in ret.DoneDirs)
                {
                    var d = dirDAL.GetDirectionAllData(dirID);
                    result.Append($"{d.DocInfo};");
                }

                eventData2History = $"Sběrná faktura. Odeslán doklad {direction.DocInfo} s řádky z dokladů {result.ToString()} {eventData2History}";
            }

            var dal = new MessageBusDAL();
            foreach (var doneDir in ret.DoneDirs)
                AddDirectionHistory(doneDir, eventData2History, MessageBus.MessageBusTypeEnum.ExportDirection);


            if (mailIsSendError)
                WriteError(messageBus, eventData2History);
            else
                WriteDone(messageBus);

            doneDirs = ret.DoneDirs;
            return succes;

        }
        #endregion

        #region private
        private bool IsExportValid(DispatchS3 dispatchS3, out string error)
        {
            error = "";
            if (dispatchS3.Customer.Extradata == null || string.IsNullOrWhiteSpace(dispatchS3.Customer.Extradata.FormatDiskety))
            {
                error = "XML neobsahuje formát diskety";
                return false;
            }

            if (S4.Export.Export.AbraSend(dispatchS3))
                return true;

            Enum.TryParse(dispatchS3.Customer.Extradata.FormatDiskety, out S4.Export.Export.ExportFormat formatEnum);
            if (formatEnum == S4.Export.Export.ExportFormat.None)
            {
                error = "XML obsahuje formát diskety = None (0)";
                return false;
            }

            return true;
        }

        private string SendResult(Direction direction, DispatchS3 dispatchS3, ExportResult[] expResults, ExportResult[] abraExpResults, Encoding encoding, ref bool mailIsSendError)
        {
            mailIsSendError = false;
            NLogger.Info("SendResult start ...");

            StringBuilder eventData = new StringBuilder();
            SettingsExportRady configRada = S4.Export.Settings.Helper.GetRada(_settingsExport, direction.DocNumPrefix, out bool dummy);

            // get format
            S4.Export.Export.ExportFormat format;
            bool odesilaAbra = S4.Export.Export.AbraSend(dispatchS3);
            if (odesilaAbra)
                format = S4.Export.Export.ExportFormat.PDK18;
            else
                Enum.TryParse(dispatchS3.Customer.Extradata.FormatDiskety, out format);

            NLogger.Info($"format: {format.ToString()} bude zkopírováno: {configRada.UlozitG4}");

            if (odesilaAbra)
            {
                //zde udelat export v LEKIS5 a ulozit do Abry
                var ed = SaveFile2Abra(abraExpResults, configRada, direction, encoding);
                eventData.Append(ed);
            }

            if (bool.TryParse(dispatchS3.Customer.Extradata.PoslatMailem, out bool sendMail))
            {
                if (sendMail && !string.IsNullOrWhiteSpace(dispatchS3.Customer.Extradata.MailovaAdresa))
                {
                    var txt = SendViaSMTP(direction, format, dispatchS3, expResults, encoding, ref mailIsSendError);
                    eventData.Append(txt);
                }
            }

            NLogger.Info("SendResult end");
            return eventData.ToString();
        }

        private string SaveFile2Abra(ExportResult[] expResults, SettingsExportRady configRada, Direction direction, Encoding encoding)
        {
            StringBuilder eventData = new StringBuilder();
            if (_settingsExport.SettingExportLekarny.VysledekUlozit && !string.IsNullOrWhiteSpace(_settingsExport.SettingExportLekarny.UlozitCesta))
            {
                string storePath = _settingsExport.SettingExportLekarny.UlozitCesta;
                NLogger.Info("storePath; {0}", storePath);

                if (!Directory.Exists(storePath))
                    Directory.CreateDirectory(storePath);

                foreach (ExportResult result in expResults)
                {
                    // Ssss_nnnnnn_yyyy.DL5  (CDLZ_000286_2014.DL5)
                    string fileName = string.Format("{0}_{1:000000}_20{2}.DL5", direction.DocNumPrefix.Trim(), direction.DocNumber, direction.DocYear);
                    string tmpPath = Path.Combine(Path.GetTempPath(), fileName);
                    using (var sw = new StreamWriter(tmpPath, false, encoding))
                    {
                        sw.Write(result.fileContent);
                        sw.Flush();
                        sw.Close();
                    }

                    string destPath = Path.Combine(storePath, fileName);
                    var ulozitG4 = configRada.UlozitG4;
                    
                    if (!string.IsNullOrWhiteSpace(ulozitG4))
                    {
                        //create if not exists
                        if (!Directory.Exists(ulozitG4))
                            Directory.CreateDirectory(ulozitG4);

                        var fileName2Copy = $"{ulozitG4}\\{fileName}";
                        new ExchangeHelper().CheckFilePath(ref fileName2Copy);

                        File.Copy(tmpPath, fileName2Copy);
                        NLogger.Info($"File {tmpPath} copy to {fileName2Copy}");
                        eventData.Append($"Zkopírováno do {fileName2Copy}|");
                    }

                    MoveFile(tmpPath, destPath);

                    NLogger.Info("File saved (g4); {0}", destPath);
                    eventData.Append($"Soubor uložen do {destPath}|");
                }
            }
            return eventData.ToString();
        }

        private string SendViaSMTP(Direction direction, S4.Export.Export.ExportFormat format, DispatchS3 dispatchS3, ExportResult[] expResults, Encoding encoding, ref bool mailIsSendError)
        {
            mailIsSendError = false;
            // get info
            bool sendToCust = bool.Parse(dispatchS3.Customer.Extradata.PoslatMailem);
            string custMailAddr = dispatchS3.Customer.Extradata.MailovaAdresa;
            string odeslatHlavicka = "Promedica Praha - Soubory k dodacímu listu {0}";
            string odeslatText = "Promedica Praha - Soubory s daty k dodacímu listu {0}; formát {1}\r\n";

            var docInfo = $"{direction.DocNumPrefix}/{direction.DocYear}/{direction.DocNumber}";
            var message = new MailMessage();
            message.From = new MailAddress(_settingsExport.SettingMail.MailFrom);
            message.Subject = string.Format(odeslatHlavicka, docInfo, format);
            message.Body = string.Format(odeslatText, docInfo, format);

            foreach (ExportResult result in expResults)
            {
                byte[] bytes = encoding.GetBytes(result.fileContent);
                //File.WriteAllBytes(@"c:\\temp\\file", bytes);
                Attachment fileAtt = new Attachment(new MemoryStream(bytes), result.fileName);
                fileAtt.TransferEncoding = TransferEncoding.Base64;
                // Add the file attachment to this e-mail message.
                message.Attachments.Add(fileAtt);
            }

            // set recipients
            string logRecip = string.Empty;
            if (sendToCust && !string.IsNullOrEmpty(custMailAddr))
            {
                if (_settingsExport.SettingExportLekarny.OdeslatZakaznik)
                {
                    // send directly to customer
                    _addAddress(message.To, custMailAddr);
                    //newMail.To.Add(custMailAddr);
                    logRecip += custMailAddr + ";";
                }

                // check copy
                if (_settingsExport.SettingExportLekarny.KontrolniKopie)
                {
                    _addAddress(message.CC, _settingsExport.SettingExportLekarny.OdeslatKontrolniKopie);
                    //newMail.CC.Add(config.odeslatKomu);
                    logRecip += _settingsExport.SettingExportLekarny.OdeslatKontrolniKopie + ";";
                }
            }
            else
            {
                // send internally
                _addAddress(message.To, dispatchS3.Customer.Extradata.MailovaAdresa);
                //newMail.To.Add(config.odeslatKomu);
                logRecip += dispatchS3.Customer.Extradata.MailovaAdresa + ";";
            }


            if (!string.IsNullOrEmpty(_settingsExport.SettingMail.MailServer) && (message.CC.Count > 0 || message.To.Count > 0))
            {
                // send
                var mailSetting = new Export.MailHelper.MailSetting();
                mailSetting.Message = message;
                mailSetting.Server = _settingsExport.SettingMail.MailServer;
                mailSetting.User = _settingsExport.SettingMail.MailUzivatel;
                mailSetting.Password = _settingsExport.SettingMail.MailHeslo;

                var mail = new Export.MailHelper.Mail();
                NLogger.Info($"DirectionID: {direction.DirectionID} sending mail....");

                if (mail.SendMailViaSMTP(mailSetting, out string sendError))
                {
                    mailIsSendError = false;
                    NLogger.Info($"Mail sent OK; subject: {message.Subject}; to: {logRecip}");
                    return $"Mail odeslán {logRecip}|";
                }
                else
                {
                    //error
                    mailIsSendError = true;
                    NLogger.Info($"Mail not sent ERROR: {sendError}");
                    return $"{sendError}|";
                }
            }

            return "";
        }

        private static void _addAddress(MailAddressCollection addressColl, string mailToAdd)
        {
            string[] tmp = mailToAdd.Split(';');
            foreach (var adr in tmp)
            {
                if (!string.IsNullOrWhiteSpace(adr))
                    addressColl.Add(adr);
            }

        }

        private static void MoveFile(string tmpPath, string destPath)
        {
            const int MAX_TRY = 5;
                       
            int tryCnt = 0;
            Exception lastExc = null;
            while (tryCnt < MAX_TRY)
            {
                try
                {
                    if (File.Exists(destPath))
                        File.Delete(destPath);

                    File.Move(tmpPath, destPath);
                    // ok, return
                    return;
                }
                catch (System.IO.IOException exc)
                {
                    lastExc = exc;
                }
                tryCnt++;

                // wait for one second
                System.Threading.Thread.Sleep(100);
            }

            throw new Exception("Chyba uložení do G4", lastExc);
        }

        private bool IsIgnore(string docNumPrefix)
        {
            SettingsExportRady configRada = S4.Export.Settings.Helper.GetRada(_settingsExport, docNumPrefix, out bool dummy);
            return configRada.Ignore;
        }

        #endregion


    }

}

﻿using Microsoft.Extensions.Configuration;
using S4.Entities.Helpers;
using S4.Entities.Helpers.Xml.Article;
using S4.Entities.Helpers.Xml.Dispatch;
using S4.Entities.Helpers.Xml.Receive;
using S4.ExchangeService.Config;
using S4.ProcedureModels.Load;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using S4.Report;
using System.Net.Mail;
using System.Net;
using Newtonsoft.Json.Linq;
using S4.Core;
using NPoco;
using S4.Entities;
using S4.ExchangeService.Helper;

namespace S4.ExchangeService.Jobs.CleanMessageBus
{
    [Quartz.DisallowConcurrentExecution]
    class CleanMessageBus : Quartz.IJob
    {
        private string _jobType = "";
        private string _jobName = "";
        private ExchangeServiceSettings _setting = null;
        private NLog.Logger NLogger = null;

        #region Quartz.IJob

        public Task Execute(Quartz.IJobExecutionContext context)
        {
            try
            {
                _jobType = context.JobDetail.JobType.ToString();
                _jobName = context.JobDetail.Key.Name;

                NLogger = NLog.LogManager.GetLogger($"{Program.S4_JOB}.{_jobName}");
                NLogger.Info($"Start job {_jobName}");

                //read setting
                ReadSetting();

                //do Work
                DoWork();


            }
            catch (Exception exc)
            {
                var writeError = new ErrorLogHelper();
                writeError.ReportProcException(exc, _jobType, _jobName, NLogger);
            }

            NLogger.Info($"End job {_jobName}");
            return Task.CompletedTask;
        }

        #endregion

        #region private

        private void ReadSetting()
        {
            var helper = new ExchangeHelper();
            _setting = helper.ReadSetting(_jobType, _jobName);

            if (_setting == null)
                throw new Exception($"{nameof(CleanMessageBus)} setting not found!");

        }

        private void DoWork()
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                using (var tr = db.GetTransaction())
                {
                    var where = $"(messageStatus = 'DONE' AND entryDateTime < DATEADD(d, -{_setting.Properties.DaysBackDone}, GETDATE())) OR (messageStatus in ('NOCOMPLETE', 'ERROR', 'WAITING_FOR_COMPLETE') AND entryDateTime < DATEADD(d, -{_setting.Properties.DaysBackError}, GETDATE()))";

                    //delete s4_messageBus_document
                    var execute = $"delete from s4_messageBus_document where messageBusID in (select messageBusID from s4_messageBus where {where})";
                    var rowsDelDocument = db.Execute(execute);
                    NLogger.Info($"Number of rows deleted (MessageBusDocument): {rowsDelDocument}");

                    //delete MessageBus
                    var sql2 = new Sql();
                    sql2.Where(where);

                    var delCount2 = db.Delete<MessageBus>(sql2);
                    NLogger.Info($"Number of rows deleted (MessageBus): {delCount2}");

                    tr.Complete();
                }
            }

        }

        #endregion


    }
}

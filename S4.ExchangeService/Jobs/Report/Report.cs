﻿using Microsoft.Extensions.Configuration;
using S4.Entities.Helpers;
using S4.Entities.Helpers.Xml.Article;
using S4.Entities.Helpers.Xml.Dispatch;
using S4.Entities.Helpers.Xml.Receive;
using S4.ExchangeService.Config;
using S4.ProcedureModels.Load;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using S4.Report;
using System.Net.Mail;
using System.Net;
using Newtonsoft.Json.Linq;
using S4.Core;
using S4.ExchangeService.Helper;
using S4.Core.Utils;
using S4.Report.Helper;

namespace S4.ExchangeService.Jobs.Report
{
    [Quartz.DisallowConcurrentExecution]
    class Report : Quartz.IJob
    {
        private string _jobType = "";
        private string _jobName = "";
        private ExchangeServiceSettings _setting = null;
        private string _userID;
        private NLog.Logger NLogger = null;

        #region Quartz.IJob

        public Task Execute(Quartz.IJobExecutionContext context)
        {
            try
            {
                _jobType = context.JobDetail.JobType.ToString();
                _jobName = context.JobDetail.Key.Name;

                NLogger = NLog.LogManager.GetLogger($"{Program.S4_JOB}.{_jobName}");
                NLogger.Info($"Start job {_jobName}");

                //read setting
                ReadSetting();

                //do Work
                DoWork();


            }
            catch (Exception exc)
            {
                var writeError = new ErrorLogHelper();
                writeError.ReportProcException(exc, _jobType, _jobName, NLogger);
            }

            NLogger.Info($"End job {_jobName}");
            return Task.CompletedTask;
        }

        #endregion

        #region private

        private void ReadSetting()
        {
            //userID
            _userID = Program.Settings.GetValue<string>("userID").ToString();

            var helper = new ExchangeHelper();
            _setting = helper.ReadSetting(_jobType, _jobName);

            if (_setting == null)
                throw new Exception($"{nameof(Report)} setting not found!");

            //read setting mail
            _setting.Properties.SettingMail = S4.Export.Settings.Helper.GetSettingMail();
        }

        private void DoWork()
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //report from db
                var report = db.SingleOrDefaultById<Entities.Reports>(_setting.Properties.ReportID);

                if (report == null)
                    throw new Exception($"Report ID: {_setting.Properties.ReportID} is not found");

                var sql = report.Settings.SQL;
                var parameters = new List<Tuple<string, object>>();
                var header = "";
                switch (report.Settings.TypeQuery)
                {
                    case Entities.Settings.ReportSettings.TypeQueryEnum.None:
                        break;
                    case Entities.Settings.ReportSettings.TypeQueryEnum.LastPeriod:
                        parameters.Add(new Tuple<string, object>("@lastPeriod", _setting.Properties.QueryLastPeriod.ToString()));
                        break;
                    case Entities.Settings.ReportSettings.TypeQueryEnum.DaysBack:
                        parameters.Add(new Tuple<string, object>("@last", _setting.Properties.QueryDaysBack.ToString()));
                        header = DateTime.Now.AddDays(-_setting.Properties.QueryDaysBack).ToShortDateString();
                        break;
                    default:
                        break;
                }

                sql += $" {_setting.Properties.SQLWhere}";

                var rowsCount = 0;
                byte[] fileToExport = null;
                if (report.Settings.TypeExport == Entities.Settings.ReportSettings.TypeExportEnum.Xlsx)
                {
                    var reportHelper = new ReportHelper();
                    var dataTable = reportHelper.SQL2DataTable(sql);
                    rowsCount = dataTable.Rows.Count;

                    if (rowsCount > 0)
                        fileToExport = ToExcel.Helper.DataTableToExcel(dataTable);
                }
                else if (report.Settings.TypeExport == Entities.Settings.ReportSettings.TypeExportEnum.Pdf)
                {
                    //variables
                    var variables = new List<Tuple<string, object, Type>>();
                    variables.Add(new Tuple<string, object, Type>("Header", $"{report.Settings.ReportName} {header}", typeof(string)));

                    Print print = new Print();
                    print.Variables = variables;
                    print.ReportDefinition = report.Template;
                    //print.DesignMode = true; //Design
                    //print.ReportDefFilePath = @"c:\Projects\store4\Tools\Reporty\ExchangeDirectionOld.srt"; //Design
                    //print.DoPrint("Adobe PDF", sql, parameters); //Design

                    fileToExport = print.DoPrint2PDF(sql, report.Settings.ReportName, parameters, out rowsCount);

                    if (print.IsError)
                        throw new Exception(print.ErrorText);
                }

                //send mail
                if (rowsCount > 0 || _setting.Properties.SendEmptyReport)
                {
                    var toBody = $"Počet vět: {rowsCount}";
                    SaveMail(fileToExport, toBody, report.Settings.TypeExport);
                }
                else
                    NLogger.Info($"Mail not send, no data!");
            }

        }

        private void SaveMail(byte[] pdfDocument, string toBody, Entities.Settings.ReportSettings.TypeExportEnum typeExport)
        {
            if (string.IsNullOrWhiteSpace(_setting.Properties.MailTo))
            {
                NLogger.Info($"Mail not save, mailTo is empty");
                return;
            }

            var documentName = pdfDocument != null ? $"{_setting.Properties.ReportName.Replace(" ", "_")}.{typeExport.ToString()}" : "";
            var dn = pdfDocument != null ? new string[] { documentName } : new string[] { };
            var emailData = new SendEmailInfo(_setting.Properties.SettingMail.MailFrom, _setting.Properties.MailTo, _setting.Properties.MailSubject, $"{_setting.Properties.MailBody} {toBody}", dn);

            //save messageBus
            var messageBusDAL = new DAL.MessageBusDAL();
            var documents = new List<byte[]>();

            if (pdfDocument != null)
                documents.Add(pdfDocument);

            var messageBusID = messageBusDAL.InsertMessageBusDocumentStatusNew(emailData, documents);

            if (pdfDocument != null)
                NLogger.Info($"Mail save. MessageBusID {messageBusID} Document length: {pdfDocument.Length} b");
            else
                NLogger.Info($"Mail save. MessageBusID {messageBusID}");

        }




        #endregion


    }
}

﻿using combit.ListLabel24.DataProviders;
using OfficeOpenXml.FormulaParsing.Excel.Functions.RefAndLookup;
using OfficeOpenXml.FormulaParsing.Utilities;
using Quartz;
using S4.ExchangeService.Config;
using S4.ExchangeService.Helper;
using System;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace S4.ExchangeService.Jobs.CheckRunningJobs
{
    [Quartz.DisallowConcurrentExecution]
    class CheckRunningJobs : Quartz.IJob
    {
        private NLog.Logger NLogger = null;
        private ExchangeServiceSettings _setting = null;
        private string _jobName = "";
        private string _jobType = "";

        #region Quartz.IJob
                     

        public Task Execute(Quartz.IJobExecutionContext context)
        {
            try
            {
                _jobType = context.JobDetail.JobType.ToString();
                _jobName = context.JobDetail.Key.Name;

                NLogger = NLog.LogManager.GetLogger($"{Program.S4_JOB}.{_jobName}");
                NLogger.Info($"Start job {_jobName}");

                ReadSetting(_jobType, _jobName);
                DoCheck();

            }
            catch (Exception exc)
            {
                var writeError = new ErrorLogHelper();
                writeError.ReportProcException(exc, _jobType, _jobName, NLogger);
            }

            NLogger.Info($"End job {_jobName}");
            return Task.CompletedTask;
        }

        #endregion

        #region private
        private void DoCheck()
        {
            var registryJobs = Core.Singleton<RegistryJobs>.Instance.GetDelayedJobs(_setting.Properties.DelayCoef, _setting.Properties.DelayMinutesMax, _jobName);

            var rows = new StringBuilder();
            var sendMail = false;
            var serviceTime = ServiceTimeHelper.GetServiceTime();
            var serviceTimeFrom = serviceTime.Item1;
            var serviceTimeTo = serviceTime.Item2;

            foreach (var job in registryJobs)
            {
                var isServiceTime = false;
                if (job.Value.NextRunDate.HasValue)
                    isServiceTime = ServiceTimeHelper.IsServiceTime(serviceTimeFrom, serviceTimeTo, job.Value.NextRunDate.Value);

                if (isServiceTime)
                {
                    NLogger.Debug($"Job: {job.Key} is delay, because is service time last run {job.Value.RunDate}, next run date: {job.Value.NextRunDate}, now is {DateTime.Now}");
                    continue;
                }

                //prepare body
                rows.AppendLine($"Job: {job.Key} - poslední spuštění {job.Value.RunDate}, měl být spuštěn {job.Value.NextRunDate}.");

                //Remove job from registry
                Core.Singleton<RegistryJobs>.Instance.RemoveJob(job.Key);

                //Log
                NLogger.Warn($"Job: {job.Key} is delay, last run {job.Value.RunDate}, next run date: {job.Value.NextRunDate}, now is {DateTime.Now}");
                sendMail = true;
            }

            if (sendMail)
            {
                //send mail
                SendMail(rows.ToString());
            }
            
        }

        private void ReadSetting(string jobType, string jobName)
        {
            var helper = new ExchangeHelper();
            _setting = helper.ReadSetting(jobType, jobName);

            if (_setting == null)
                throw new Exception($"{jobName} setting not found!");

            //read setting mail
            _setting.Properties.SettingMail = S4.Export.Settings.Helper.GetSettingMail();
        }
        #endregion

        private void SendMail(string body)
        {
            if (string.IsNullOrWhiteSpace(_setting.Properties.MailTo) || string.IsNullOrWhiteSpace(_setting.Properties.SettingMail.MailServer))
            {
                NLogger.Info($"Mail not send");
                return;
            }

            var message = new MailMessage();
            message.From = new MailAddress(_setting.Properties.SettingMail.MailFrom);
            message.Subject = $"{_setting.Properties.MailSubject}";
            message.IsBodyHtml = false;
            message.Body = body;

            //add recipient
            var mail = new Export.MailHelper.Mail();
            var addAddressIsOK = mail.AddAddress(message.To, _setting.Properties.MailTo, out string addAddressMsg);

            if (!addAddressIsOK)
                NLogger.Info(addAddressMsg);

            // send
            if ((message.CC.Count > 0 || message.To.Count > 0))
            {
                // send
                var mailSetting = new Export.MailHelper.MailSetting();
                mailSetting.Message = message;
                mailSetting.Server = _setting.Properties.SettingMail.MailServer;
                mailSetting.User = _setting.Properties.SettingMail.MailUzivatel;
                mailSetting.Password = _setting.Properties.SettingMail.MailHeslo;

                NLogger.Info($"Try send mail....");

                if (mail.SendMailViaSMTP(mailSetting, out string sendError))
                {
                    NLogger.Info($"Mail has been sent. Subject: {message.Subject}; to: {_setting.Properties.MailTo}");
                    return;
                }
                else
                {
                    //error
                    NLogger.Info($"Error! Mail not sent!");
                    NLogger.Info(sendError);
                    return;
                }
            }
            else
            {
                NLogger.Info($"No send mail, no recipients....");
            }


        }
    }

}

﻿using CsvHelper;
using Microsoft.Extensions.Configuration;
using NPoco;
using S4.Core;
using S4.DAL;
using S4.Entities;
using S4.Entities.Helpers;
using S4.Entities.Helpers.Xml.Article;
using S4.Entities.Helpers.Xml.Dispatch;
using S4.Entities.Helpers.Xml.Receive;
using S4.Entities.Models;
using S4.ExchangeService.Config;
using S4.ExchangeService.Helper;
using S4.Export;
using S4.Export.Settings;
using S4.ProcedureModels.Load;
using S4.Procedures;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace S4.ExchangeService.Jobs.ExportABRA
{
    [Quartz.DisallowConcurrentExecution]
    class ExportABRA : MessageBusBase, Quartz.IJob
    {        

        #region Quartz.IJob

        public Task Execute(Quartz.IJobExecutionContext context)
        {
            try
            {
                _jobType = context.JobDetail.JobType.ToString();
                _jobName = context.JobDetail.Key.Name;

                NLogger = NLog.LogManager.GetLogger($"{Program.S4_JOB}.{_jobName}");
                NLogger.Info($"Start job {_jobName}");

                ReadSetting();
                DoWork(MessageBus.MessageBusTypeEnum.ExportABRA);

            }
            catch (Exception exc)
            {
                var writeError = new ErrorLogHelper();
                writeError.ReportProcException(exc, _jobType, _jobName, NLogger);
            }

            NLogger.Info($"End job {_jobName}");
            return Task.CompletedTask;
        }

        #endregion


        #region public
        public override bool DoMakeExport(SettingsExport settingsExport, DirectionModel direction, MessageBus messageBus, ref List<int> doneDirs)
        {
            var eventData = "";
            SettingsExportRady configRada = S4.Export.Settings.Helper.GetRadaABRAExport(settingsExport, direction.DocNumPrefix);

            // prepare data for StoreOut
            var dispatchPositionsModel = Singleton<DAL.Cache.PositionByCategoryModelCache>.Instance.GetItem(Entities.Position.PositionCategoryEnum.Dispatch);

            var save = Singleton<ServicesFactory>.Instance.GetSave();
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var moves = save.MoveResult_ByDirection(db, direction.DirectionID, StoreMove.MO_DOCTYPE_DISPATCH, (mi) => dispatchPositionsModel.PositionIDs.Contains(mi.PositionID));

                if (moves.Count < 1)
                {
                    var error = $"DirectionID: {direction.DirectionID} Nebyly nalezeny žádné pohyby";
                    NLogger.Info(error);
                    eventData = error;
                    WriteError(messageBus, error);
                    return false;
                }

                //Create file
                var fileName = string.Format("{0}_{1:000000}_20{2}.CSV", direction.DocNumPrefix.Trim(), direction.DocNumber, direction.DocYear);
                var year = DateTime.Now.ToString("yyyy");
                var date = DateTime.Now.ToString("ddMM");
                
                var file = new List<RowCSV>();
                var firstCol = $"{direction.DocNumPrefix.Trim()}-{direction.DocNumber}/{direction.DocYear}";

                //history XML (ProvideRow_ID)
                DispatchS3 dispatchS3 = null;
                var load = false;
                if (direction.History != null)
                {
                    var history = (from h in direction.History where h.EventCode == Direction.DR_STATUS_LOAD select h).FirstOrDefault();
                    if (history != null)
                    {
                        load = DispatchS3.Serializer.Deserialize(history.EventData, out dispatchS3);
                    }
                }

                //read items from history XML data
                foreach (var item in dispatchS3.Items.OrderBy(_ => _.Position))
                {
                    var movesList = (from m in moves where m.DocPosition == item.Position + 1 select m).ToList();

                    if (movesList.Count == 0)
                        NLogger.Warn($"Export Abra. Nebyl nalezen žádný pohyb k item Position: {item.Position}; item ArticlePackingID {item.ArticlePackingID}");

                    foreach (var move in movesList)
                    {
                        var packing = Singleton<DAL.Cache.ArticlePackingCache>.Instance.GetItem(move.ArtPackID);
                        var article = Singleton<DAL.Cache.ArticleModelCache>.Instance.GetItem(packing.ArticleID);

                        var s4ID = (direction.DirectionID * 1000) + move.DocPosition;
                        var batchNum = string.IsNullOrWhiteSpace(move.BatchNum) ? "" : move.BatchNum;
                        NumberFormatInfo nfi = new NumberFormatInfo();
                        nfi.NumberDecimalSeparator = ".";
                        var quantity = move.Quantity.ToString(nfi);
                        var barCode = string.IsNullOrWhiteSpace(packing.BarCode) ? "" : packing.BarCode;

                        var data = new RowCSV
                        {
                            Batch = RegexReplace(batchNum),
                            Code = RegexReplace(article.ArticleCode),
                            Document = firstCol,
                            EAN = EANHelper.EANrepare(barCode),
                            Exspiration = move.ExpirationDate.HasValue ? move.ExpirationDate.Value.Date.ToShortDateString() : "",
                            PackingID = RegexReplace(packing.Source_id),
                            Quantity = quantity,
                            ProvideRow_ID = item.Extradata == null ? "" : item.Extradata.ProvideRowID,
                            S4_ID = s4ID
                        };

                        file.Add(data);
                    }

                }

                var path = $@"{_setting.Properties.PathS4}\{year}\{date}";
                var pathABRA = configRada.UlozitG4;

                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);
                
                var filePath = $@"{path}\{fileName}";
                var filePathABRA = $"{pathABRA}\\{fileName}";

                var helper = new Helper.ExchangeHelper();
                helper.CheckFilePath(ref filePath);
                using (var writer = new StreamWriter(filePath))
                {
                    using (var csv = new CsvWriter(writer, CultureInfo.CurrentCulture))
                    {
                        csv.WriteRecords(file);
                    }
                }

                //copy to ABRA
                try
                {
                    if (!Directory.Exists(pathABRA))
                        Directory.CreateDirectory(pathABRA);

                    helper.CheckFilePath(ref filePathABRA);
                    System.IO.File.Copy(filePath, filePathABRA, true);
                }
                catch (IOException exc)
                {
                    eventData = $"{firstCol} nebyl exportován - chyba: {exc.Message}";
                    WriteError(messageBus, exc.Message);

                    //log error
                    var message = $"directionID: {messageBus.DirectionID}";
                    var writeError = new ErrorLogHelper();
                    writeError.ReportProcException(exc, _jobType, _jobName, NLogger, message);

                    AddDirectionHistory(direction.DirectionID, exc.Message, MessageBus.MessageBusTypeEnum.ExportABRA);
                    return false;
                }
                catch (Exception exc2)
                {
                    throw exc2;
                }

                eventData = $"{firstCol} byl exportován - vytvořen nový soubor {filePath}, zkopírován do systému ABRA {filePathABRA}";
                
            }

            WriteDone(messageBus);
            AddDirectionHistory(direction.DirectionID, eventData, MessageBus.MessageBusTypeEnum.ExportABRA);
            NLogger.Info($"DirectionID: {messageBus.DirectionID} - {eventData}");
            return true;
        }
        #endregion

        #region private
       
        private string RegexReplace(string data)
        {
            return S4.Core.Utils.StrUtils.ReplaceNonCSVCharacter(data);
           
        }
        #endregion


    }

    internal class RowCSV
    {
        public string Document { get; set; }
        public string PackingID { get; set; }
        public string Code { get; set; }
        public string EAN { get; set; }
        public string Batch { get; set; }
        public string Exspiration { get; set; }
        public string Quantity { get; set; }
        public string ProvideRow_ID { get; set; }
        public long S4_ID { get; set; }
    }

}

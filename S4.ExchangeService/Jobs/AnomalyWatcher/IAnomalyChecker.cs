﻿using S4.ExchangeService.Config;

namespace S4.ExchangeService.Jobs.AnomalyWatcher
{
    internal interface IAnomalyChecker
    {
        CheckerResult DoCheck(NPoco.Database database, Config.Properties settings, NLog.Logger logger);
    }
}

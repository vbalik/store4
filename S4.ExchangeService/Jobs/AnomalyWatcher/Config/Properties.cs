﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.ExchangeService.Jobs.AnomalyWatcher.Config
{
    public class Properties
    {
        public string Name { get; set; }
        public bool IsEnabled { get; set; }
        public int HourLimitOlderThan { get; set; }
        
    }

    public class MailSettings
    {
        public string MailTo { get; set; }
        public string MailSubject { get; set; }
        public string MailBody { get; set; }
    }
}

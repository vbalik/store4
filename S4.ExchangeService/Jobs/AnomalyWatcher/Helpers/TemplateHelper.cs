﻿using Scriban;
using System;
using System.IO;

namespace S4.ExchangeService.Jobs.AnomalyWatcher.Helpers
{
    /// <summary>
    /// Scriban lightweight templating engine
    /// https://github.com/scriban/scriban/blob/master/doc/language.md
    /// 
    /// NOTICE
    /// 
    /// By default, Properties and methods of.NET objects are automatically exposed with lowercase and _ names.
    /// It means that a property like MyMethodIsNice will be exposed as my_method_is_nice. This is the default convention,
    /// originally to match the behavior of liquid templates. 
    /// </summary>
    public class TemplateHelper
    {
        public const string TemplatePath = "AnomalyWatcher";

        public static string Parse(string path, object data)
        {
            path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, TemplatePath, path);
            if (string.IsNullOrWhiteSpace(path) || !File.Exists(path))
                throw new ArgumentException(nameof(path));

            var templateBody = File.ReadAllText(path);

            var template = Template.Parse(templateBody);
            var result = template.Render(data);

            return result;
        }
    }
}

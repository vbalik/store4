﻿using NPoco;
using S4.Entities;
using S4.ExchangeService.Jobs.AnomalyWatcher.Checkers.LongWaitForExpeditionDirectionsChecker;
using S4.ExchangeService.Jobs.AnomalyWatcher.Helpers;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace S4.ExchangeService.Jobs.AnomalyWatcher.Checkers.LongWaitForProcessDirectionsChecker
{
    /// <summary>
    /// Doklady, které čekají na zpracování víc nez x hodin
    /// </summary>
    internal class LongWaitForProcessDirectionsChecker : IAnomalyChecker
    {
        public CheckerResult DoCheck(Database db, Config.Properties settings, NLog.Logger logger)
        {
            var hourBackLimit = settings.HourLimitOlderThan;
            var limitDate = GetDateTimeBack(hourBackLimit);


            var sql = new Sql(@"select d.directionID, d.docInfo, pl.partnerName, di.itemsCount, d.docDate as entryDateTime
from s4_direction d
    inner join (select ite.directionID, count(*) as itemsCount from s4_direction_items ite group by ite.directionID)
         di on d.directionID = di.directionID
    left join s4_partnerList pl on d.partnerID = pl.partnerID

where d.docDirection = @1
  and d.docStatus IN (@2)
  and d.docDate < @0",
            limitDate,
            Direction.DOC_DIRECTION_OUT,
            new List<string>() { Direction.DR_STATUS_LOAD });

            var stopWatch = new Stopwatch();
            stopWatch.Start();

            // load data
            var directions = db.Fetch<DirectionDto>(sql);

            stopWatch.Stop();
            var ts = stopWatch.Elapsed;
            var elapsedTime = string.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds);

            logger.Info($"Job {nameof(LongWaitForProcessDirectionsChecker)} - loaded data in {elapsedTime} with {directions.Count} records");

            var result = new CheckerResult
            {
                TextResult = GetResultText(directions, hourBackLimit).ToString(),
            };

            return result;
        }

        private string GetResultText(List<DirectionDto> data, int hourBackLimit)
        {
            var templateData = new
            {
                Data = data,
                Count = data.Count,
                HourBackLimit = hourBackLimit,
            };

            var result = TemplateHelper.Parse($"./{nameof(LongWaitForProcessDirectionsChecker)}/DirectionsTemplate.html", templateData);

            return result;
        }

        private DateTime GetDateTimeBack(int hourBackLimit)
        {
            DateTime currentDateTime = DateTime.Now;
            DateTime dateTimeBack = currentDateTime.AddHours(-hourBackLimit);

            var remainHour = Math.Abs(dateTimeBack.Hour - hourBackLimit);
            var remainMinute = dateTimeBack.Minute;

            // Zkontrolujeme, zda tento čas spadá na víkend (sobota nebo neděle)
            if (dateTimeBack.DayOfWeek == DayOfWeek.Saturday || dateTimeBack.DayOfWeek == DayOfWeek.Sunday)
            {
                int moveDaysBack = (int) dateTimeBack.DayOfWeek - (int) DayOfWeek.Friday;
                if (moveDaysBack <= 0)
                {
                    moveDaysBack += 7; // pricteme do plusu
                }

                dateTimeBack = dateTimeBack.AddDays(-moveDaysBack).Date.AddHours(24 - remainHour).AddMinutes(remainMinute);
            }

            return dateTimeBack;
        }
    }
}

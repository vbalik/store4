﻿using S4.Entities;
using System;
using System.Collections.Generic;
using NPoco;
using S4.ExchangeService.Jobs.AnomalyWatcher.Helpers;
using System.Diagnostics;

namespace S4.ExchangeService.Jobs.AnomalyWatcher.Checkers.LongWaitForExpeditionDirectionsChecker
{
    /// <summary>
    /// Neexpedované doklady vice nez x hodin
    /// </summary>
    internal class LongWaitForExpeditionDirectionsChecker : IAnomalyChecker
    {
        public CheckerResult DoCheck(Database db, Config.Properties settings, NLog.Logger logger)
        {
            var hourBackLimit = settings.HourLimitOlderThan;
            var limitDate = DateTime.Today.AddHours(-hourBackLimit);

            var sql = new Sql(@"select d.directionID, d.docInfo, pl.partnerName, di.itemsCount, his.entryDateTime
from s4_direction d
         inner join (select his.directionID,
                            MAX(his.entryDateTime) as entryDateTime
                     FROM s4_direction_history his
                     group by his.directionID) his on his.directionID = d.directionID

         inner join (select ite.directionID, count(*) as itemsCount from s4_direction_items ite group by ite.directionID)
             di on d.directionID = di.directionID
         left join s4_partnerList pl on d.partnerID = pl.partnerID

where d.docDirection = @1
  and d.docStatus IN (@2)
  and his.entryDateTime < @0",
            limitDate,
            Direction.DOC_DIRECTION_OUT,
            new List<string>() { Direction.DR_STATUS_DISP_CHECK_DONE, Direction.DR_STATUS_DISPATCH_SAVED, Direction.DR_STATUS_DISPATCH_MANUAL });

            var stopWatch = new Stopwatch();
            stopWatch.Start();

            // load data
            var directions = db.Fetch<DirectionDto>(sql);

            stopWatch.Stop();
            var ts = stopWatch.Elapsed;
            var elapsedTime = string.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds);

            logger.Info($"Job {nameof(LongWaitForExpeditionDirectionsChecker)} - loaded data in {elapsedTime} with {directions.Count} records");

            return new CheckerResult
            {
                TextResult = GetResultText(directions, hourBackLimit).ToString(),
            };
        }

        private string GetResultText(List<DirectionDto> data, int hourBackLimit)
        {
            var templateData = new
            {
                Data = data,
                Count = data.Count,
                HourBackLimit = hourBackLimit,
            };

            var result = TemplateHelper.Parse($"./{nameof(LongWaitForExpeditionDirectionsChecker)}/DirectionsTemplate.html", templateData);

            return result;
        }
    }

    class DirectionDto
    {
        [Column("directionID")]
        public string DirectionID { get; set; }
        [Column("docInfo")]
        public string DocInfo { get; set; }
        [Column("partnerName")]
        public string PartnerName { get; set; }
        [Column("itemsCount")]
        public int ItemsCount { get; set; }
        [Column("entryDateTime")]
        public DateTime? EntryDateTime { get; set; }
    }
}

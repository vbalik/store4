﻿using S4.ExchangeService.Config;
using System;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Text;
using SystemList = System.Collections.Generic;
using S4.ExchangeService.Helper;
using S4.ExchangeService.Jobs.AnomalyWatcher.Checkers.LongWaitForProcessDirectionsChecker;
using S4.ExchangeService.Jobs.AnomalyWatcher.Checkers.LongWaitForExpeditionDirectionsChecker;

namespace S4.ExchangeService.Jobs.AnomalyWatcher
{
    [Quartz.DisallowConcurrentExecution]
    class AnomalyWatcher : Quartz.IJob
    {
        private string _jobType = "";
        private string _jobName = "";
        private ExchangeServiceSettings _setting = null;
        private NLog.Logger NLogger = null;

        #region Quartz.IJob

        public Task Execute(Quartz.IJobExecutionContext context)
        {
            try
            {
                _jobType = context.JobDetail.JobType.ToString();
                _jobName = context.JobDetail.Key.Name;

                NLogger = NLog.LogManager.GetLogger($"{Program.S4_JOB}.{_jobName}");
                NLogger.Info($"Start job {_jobName}");

                //read setting
                ReadSetting();

                //register checkers
                var checkers = new SystemList.List<IAnomalyChecker>
                {
                    new LongWaitForExpeditionDirectionsChecker(),
                    new LongWaitForProcessDirectionsChecker()
                };

                //do Work
                DoWork(checkers);

            }
            catch (Exception exc)
            {
                var writeError = new ErrorLogHelper();
                writeError.ReportProcException(exc, _jobType, _jobName, NLogger);
            }

            NLogger.Info($"End job {_jobName}");
            return Task.CompletedTask;
        }

        #endregion

        #region private

        private void ReadSetting()
        {
            var helper = new ExchangeHelper();
            _setting = helper.ReadSetting(_jobType, _jobName);

            if (_setting == null)
                throw new Exception($"{nameof(AnomalyWatcher)} setting not found!");

            //read setting mail
            _setting.Properties.SettingMail = S4.Export.Settings.Helper.GetSettingMail();
        }

        private void DoWork(SystemList.List<IAnomalyChecker> checkers)
        {
            var textResultBuilder = new StringBuilder();
            
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                foreach (var checker in checkers)
                {
                    var setting = _setting.Properties.Checkers.Find(x => x.Name == checker.GetType().Name);
                    if (setting == null || !setting.IsEnabled)
                    {
                        NLogger.Info($"Job {_jobName} | checker {checker.GetType().Name} has been skipped");
                        continue;
                    }

                    NLogger.Info($"Job {_jobName} | checker {checker.GetType().Name} being started");

                    var result = checker.DoCheck(db, setting, NLogger);
                    textResultBuilder.Append(result.TextResult);
                }
            }

            SendMail(textResultBuilder.ToString());
        }


        private void SendMail(string body)
        {
            if (string.IsNullOrWhiteSpace(_setting.Properties.AnomalyWatcherMail.MailTo) || string.IsNullOrWhiteSpace(_setting.Properties.SettingMail.MailServer))
            {
                NLogger.Info($"Mail not send");
                return;
            }

            var sett = S4.Export.Settings.Helper.GetSettingsExport();

            var message = new MailMessage();
            message.From = new MailAddress(sett.SettingMail.MailFrom);
            message.Subject = $"{_setting.Properties.AnomalyWatcherMail.MailSubject}";
            message.IsBodyHtml = true;
            message.Body = _setting.Properties.AnomalyWatcherMail.MailBody + "<br><br>" + body;

            //add recipient
            var mail = new Export.MailHelper.Mail();
            var addAddressIsOK = mail.AddAddress(message.To, _setting.Properties.AnomalyWatcherMail.MailTo, out string addAddressMsg);

            if (!addAddressIsOK)
                NLogger.Info(addAddressMsg);

            // send
            if ((message.CC.Count > 0 || message.To.Count > 0))
            {
                // send
                var mailSetting = new Export.MailHelper.MailSetting();
                mailSetting.Message = message;
                mailSetting.Server = sett.SettingMail.MailServer;
                mailSetting.User = sett.SettingMail.MailUzivatel;
                mailSetting.Password = sett.SettingMail.MailHeslo;

                NLogger.Info($"Try send mail....");

                if (mail.SendMailViaSMTP(mailSetting, out string sendError))
                {
                    NLogger.Info($"Mail has been sent. Subject: {message.Subject}; to: {_setting.Properties.AnomalyWatcherMail.MailTo}");
                    return;
                }
                else
                {
                    //error
                    NLogger.Info($"Error! Mail not sent!");
                    NLogger.Info(sendError);
                    return;
                }
            }
            else
            {
                NLogger.Info($"No send mail, no recipients....");
            }


        }


        #endregion


    }
}

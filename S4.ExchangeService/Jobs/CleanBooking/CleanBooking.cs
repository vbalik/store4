﻿using Microsoft.Extensions.Configuration;
using S4.Entities.Helpers;
using S4.Entities.Helpers.Xml.Article;
using S4.Entities.Helpers.Xml.Dispatch;
using S4.Entities.Helpers.Xml.Receive;
using S4.ExchangeService.Config;
using S4.ProcedureModels.Load;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using S4.Report;
using System.Net.Mail;
using System.Net;
using Newtonsoft.Json.Linq;
using S4.Core;
using NPoco;
using S4.Entities;
using S4.ExchangeService.Helper;

namespace S4.ExchangeService.Jobs.CleanBooking
{
    [Quartz.DisallowConcurrentExecution]
    class CleanBooking : Quartz.IJob
    {
        private string _jobType = "";
        private string _jobName = "";
        private ExchangeServiceSettings _setting = null;
        private NLog.Logger NLogger = null;

        #region Quartz.IJob

        public Task Execute(Quartz.IJobExecutionContext context)
        {
            try
            {
                _jobType = context.JobDetail.JobType.ToString();
                _jobName = context.JobDetail.Key.Name;

                NLogger = NLog.LogManager.GetLogger($"{Program.S4_JOB}.{_jobName}");
                NLogger.Info($"Start job {_jobName}");

                //read setting
                ReadSetting();

                //do Work
                DoWork();


            }
            catch (Exception exc)
            {
                var writeError = new ErrorLogHelper();
                writeError.ReportProcException(exc, _jobType, _jobName, NLogger);
            }

            NLogger.Info($"End job {_jobName}");
            return Task.CompletedTask;
        }

        #endregion

        #region private

        private void ReadSetting()
        {
            var helper = new ExchangeHelper();
            _setting = helper.ReadSetting(_jobType, _jobName);

            if (_setting == null)
                throw new Exception($"{nameof(CleanMessageBus)} setting not found!");

        }

        private void DoWork()
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var userID = Program.Settings.GetValue<string>("userID").ToString();
                //update booking
                var execute = $"UPDATE s4_bookings SET bookingStatus = 'DELETED', deletedByEntryUserID = '{userID}', deletedDateTime = GETDATE() WHERE bookingTimeout < GETDATE() AND bookingStatus = 'VALID'";
                var rows = db.Execute(execute);
                NLogger.Info($"Number of rows updated (s4_bookings): {rows}");
            }

        }

        #endregion


    }
}

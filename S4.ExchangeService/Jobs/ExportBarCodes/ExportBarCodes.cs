﻿using Microsoft.Extensions.Configuration;
using S4.Entities.Helpers;
using S4.Entities.Helpers.Xml.Article;
using S4.Entities.Helpers.Xml.Dispatch;
using S4.Entities.Helpers.Xml.Receive;
using S4.ExchangeService.Config;
using S4.ProcedureModels.Load;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using S4.Report;
using System.Net.Mail;
using System.Net;
using Newtonsoft.Json.Linq;
using S4.Entities.Views;
using NPoco;
using System.Xml;
using System.Text;
using RestSharp;
using S4.ExchangeService.Model.ExportBarCodes;
using S4.ExchangeService.Helper;

namespace S4.ExchangeService.Jobs.ExportBarCodes
{
    [Quartz.DisallowConcurrentExecution]
    class ExportBarCodes : Quartz.IJob
    {
        private string _jobType = "";
        private string _jobName = "";
        private ExchangeServiceSettings _setting = null;
        private string _userID;
        private const int BATCH_SIZE = 100;
        private int rowsOK = 0;
        private NLog.Logger NLogger = null;

        #region Quartz.IJob

        public Task Execute(Quartz.IJobExecutionContext context)
        {
            try
            {
                _jobType = context.JobDetail.JobType.ToString();
                _jobName = context.JobDetail.Key.Name;

                NLogger = NLog.LogManager.GetLogger($"{Program.S4_JOB}.{_jobName}");
                NLogger.Info($"Start job {_jobName}");

                //read setting
                ReadSetting();

                //do Work
                DoWork();


            }
            catch (Exception exc)
            {
                var writeError = new ErrorLogHelper();
                writeError.ReportProcException(exc, _jobType, _jobName, NLogger);
            }

            NLogger.Info($"End job {_jobName}");
            return Task.CompletedTask;
        }

        #endregion

        #region private

        private void ReadSetting()
        {
            //userID
            _userID = Program.Settings.GetValue<string>("userID").ToString();

            var helper = new ExchangeHelper();
            _setting = helper.ReadSetting(_jobType, _jobName);

            if (_setting == null)
                throw new Exception($"{nameof(ExportBarCodes)} setting not found!");
        }

        private void DoWork()
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // scan items
                var sql = new Sql();
                sql.Append("SELECT p.source_id, p.barCode");
                sql.From("s4_articleList_packings p");
                sql.LeftJoin("s4_articleList a on a.articleID = p.articleID");
                sql.Where("a.articleStatus = 'AR_OK' AND barCode IS NOT NULL");
                sql.OrderBy("source_id");
                var sourceItems = db.Fetch<ArticleBaseViewModel>(sql);

                sourceItems.ForEach(_ => _.BarCode = EANHelper.EANrepare(_.BarCode));

                // call ws
                int cnt = 0;
                while (cnt < sourceItems.Count)
                {
                    int from = cnt;
                    int to = Math.Min(from + BATCH_SIZE - 1, sourceItems.Count - 1);

                    foreach (var wsPath in _setting.Properties.WSPath)
                        DoUpdate_WS2(wsPath, sourceItems, from, to);

                    cnt += BATCH_SIZE;
                }

                NLogger.Info($"END Scan Export; scanned: {sourceItems.Count}; sentCnt: {rowsOK}");
            }

        }


        private void DoUpdate_WS2(string wsPath, List<ArticleBaseViewModel> data, int from, int to)
        {
            //http://192.168.1.204/S99_REST/RESTB/SETEAN2STORECARDUNITS

            NLogger.Info($"DoUpdate_WS2; {wsPath}; from: {from}; to: {to}");

            // prepare xml
            var updateData = PrepareWS2Xml(data, from, to);


            var client = new RestClient(wsPath);
            var request = new RestRequest("SETEAN2STORECARDUNITS", Method.POST);
            request.AddParameter("text/xml", updateData, ParameterType.RequestBody);

            // execute the request
            RestResponse response = client.Execute(request);
            if (response.ErrorException != null)
            {
                //log error
                NLogger.Warn($"{response.ErrorException.ToString()} {"Update error"}");
            }
            else
            {
                var content = response.Content; // raw content as string

                if (content.StartsWith("<B>!! Chyba !! </B>"))
                {
                    //log error
                    NLogger.Warn($"Update error: {content}");
                }
                else
                {
                    try
                    {
                        ShowResponse(wsPath, content);
                    }
                    catch (Exception exc)
                    {
                        var tempFile = System.IO.Path.GetTempFileName();
                        System.IO.File.WriteAllText(tempFile, content);
                        //log error                        
                        NLogger.Warn($"Response parsing error - {tempFile} - {exc.ToString()}");
                    }
                }
            }

        }

        private object PrepareWS2Xml(List<ArticleBaseViewModel> data, int from, int to)
        {
            //            return @"<request>
            //            <rows>
            //            <row unit_id=""CZ1E000101""  ean=""EY8415#9981615#2024-06"" state=""NEW""/>
            //            </rows>
            //            </request>
            //             ";

            MemoryStream memoryStream = new MemoryStream();
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings()
            {
                Encoding = Encoding.UTF8,
                Indent = true,
                IndentChars = "  "
            };

            var writer = XmlWriter.Create(memoryStream, xmlWriterSettings);

            // header
            writer.WriteStartElement("request");
            writer.WriteStartElement("rows");

            // line
            for (int pos = from; pos <= to; pos++)
            {
                var row = data[pos];

                //verify BarCode
                var isValidBarCode = true;
                List<char> charlist = new List<char>();
                charlist.AddRange(row.BarCode);
                foreach (var item in charlist)
                {
                    if (!XmlConvert.IsXmlChar(item))
                    {
                        isValidBarCode = false;
                        break;
                    }
                }

                if (isValidBarCode)
                {
                    writer.WriteStartElement("row");
                    writer.WriteAttributeString("unit_id", row.Source_id);
                    writer.WriteAttributeString("ean", row.BarCode);
                    writer.WriteAttributeString("state", "NEW");
                    writer.WriteEndElement(); //row
                }
                else
                {
                    //log error
                    NLogger.Warn($"BarCode {row.BarCode} is invalid. SourceID: {row.Source_id}");
                }
            }

            // footer
            writer.WriteEndElement(); //rows
            writer.WriteEndElement(); //request

            writer.Flush();
            var result = Encoding.UTF8.GetString(memoryStream.ToArray());
            return result.Substring(1); // remove BOM characters
        }

        private void ShowResponse(string wsPath, string content)
        {
            /*
            <response>
              <rows>
              <row unit_id="1000000101"  ean="123456" state="NEW" result="OK" ></row>
              <row unit_id="1000000101"  ean="1234567" state="NEW" result ="KO" desc="popis" ></row>
              <row unit_id="1000000101"  ean="1234568" state="NEW" result ="KO" desc="popis chyby" ></row>
              </rows>
            </response>
            */


            var xml = new XmlDocument();
            xml.LoadXml(content);

            var rows = xml.SelectNodes("response/rows/row");
            foreach (XmlNode row in rows)
            {
                var result = row.Attributes["result"].Value;
                if (result == "OK")
                {
                    NLogger.Info($"Update OK; unit_id: {row.Attributes["unit_id"].Value}; ean: {row.Attributes["ean"].Value}");
                    rowsOK++;
                }
                else
                {
                    var attrDesc = row.Attributes["desc"];
                    var attrDesc2 = (attrDesc == null) ? null : attrDesc.Value;
                    //log error
                    NLogger.Warn($"Update error; unit_id: {row.Attributes["unit_id"].Value}; ean: {row.Attributes["ean"].Value}; desc: {attrDesc2}");
                }
            }

        }

        private static string[] ReadPaths(System.Xml.XmlNode configXml)
        {
            var result = new List<string>();
            var pathNodes = configXml.SelectNodes("ws_paths/ws_path");

            foreach (XmlNode node in pathNodes)
                result.Add(node.InnerText);

            return result.ToArray();
        }

        #endregion
    }
}

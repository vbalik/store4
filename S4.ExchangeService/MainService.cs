﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Quartz;
using Quartz.Impl;
using S4.ExchangeService.Config;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using NPoco;
using Quartz.Impl.AdoJobStore;
using Quartz.Plugin.Interrupt;

namespace S4.ExchangeService
{
    public class MainService : IHostedService, IDisposable
    {

        // scheduler
        private ISchedulerFactory _schedFact = null;
        private IScheduler _scheduler = null;
        private IJobListener _globalJobListener = null;
        private ITriggerListener _globalTriggerListener = null;

        internal const string DATAMAP_JOB_DEF = "jobDef";
        private readonly IServiceProvider _service;

        // settings
        private List<Config.ExchangeServiceSettings> _settings = null;

        public MainService(IServiceProvider service)
        {
            _service = service;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            try
            {
                //read setting
                ReadSettings();

                // set connection string for procedures
                Core.Data.ConnectionHelper.ConnectionString = Program.Settings.GetValue<string>("S4_LOCALDB_CN");

                //check connect DB is ready
                CheckConnect2DB();

                // set service provider in procedures
                S4.Procedures.ServicesFactory.ServiceProvider = _service;

                //create scheduler
                //construct a scheduler factory
                _schedFact = new StdSchedulerFactory();
                // get a scheduler
                _scheduler = _schedFact.GetScheduler().GetAwaiter().GetResult();
                _globalJobListener = new GlobalJobListener();
                _globalTriggerListener = new GlobalTriggerListener();
                _scheduler.ListenerManager.AddJobListener(_globalJobListener);
                _scheduler.ListenerManager.AddTriggerListener(_globalTriggerListener);

                //SignalR S4-Exchange
                var serverPath = Program.Settings.GetValue<string>("serverPath", "");
                if (!string.IsNullOrWhiteSpace(serverPath))
                    await Task.Run(() => new SignalRClientExchange(serverPath, _settings, _scheduler));

                // schedule jobs
                if (string.IsNullOrEmpty(Program.JobNameToRun))
                    await ScheduleJobs();
                else
                    ScheduleJobFromHand();

                // start scheduler
                await _scheduler.Start();
                
                return;
            }
            catch (Exception exc)
            {
                Program.NLogger.Fatal(exc, "Startup error");
                throw;
            }

        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public void Dispose()
        {

        }

        #region private

        private void ReadSettings()
        {
            var exchangeServiceSettings = new List<Config.ExchangeServiceSettings>();
            Program.Settings.GetSection("Jobs").Bind(exchangeServiceSettings);
            _settings = exchangeServiceSettings;
        }

        private void ScheduleJobFromHand()
        {
            var jobDef = (from s in _settings where s.JobName.Equals(Program.JobNameToRun) select s).FirstOrDefault();
            if (jobDef == null)
                throw new Exception($"Job run name: {Program.JobNameToRun} is not found in S4.ExchangeSettings.json!");

            // do schedule job
            Type jobType = Type.GetType(jobDef.JobClass);
            IJobDetail job = JobBuilder.Create(jobType).WithIdentity(jobDef.JobName, null).Build();
            job.JobDataMap.Put(DATAMAP_JOB_DEF, jobDef);

            var trigger = TriggerBuilder.Create()
                        .WithIdentity(jobDef.JobName, null)
                        .StartNow()
                        .Build();

            _scheduler.ScheduleJob(job, trigger);
            Program.NLogger.Info($"Job from hand {jobDef.JobName} will start in a moment....");

        }

        private async Task ScheduleJobs()
        {
            foreach (var jobDef in _settings)
            {
                if (jobDef.IsEnabled)
                {
                    // do schedule job
                    Type jobType = Type.GetType(jobDef.JobClass);

                    if (jobType == null)
                    {
                        Program.NLogger.Error($"Job {jobDef.JobName} does not have a valid job class {jobDef.JobClass}! Job will not start!");
                        continue;
                    }

                    IJobDetail job = JobBuilder.Create(jobType)
                        .WithIdentity(jobDef.JobName, null)
                        .Build();

                    job.JobDataMap.Put(DATAMAP_JOB_DEF, jobDef);


                    try
                    {
                        var trigger = GetTrigger(jobDef);
                        await _scheduler.ScheduleJob(job, trigger);
                        Program.NLogger.Info($"Job {jobDef.JobName}; CronExpressionString {((Quartz.Impl.Triggers.CronTriggerImpl) trigger).CronExpressionString}");
                    }
                    catch (FormatException ex)
                    {
                        Program.NLogger.Error(ex, $"Job {jobDef.JobName} has wrong configuration and will not start!");
                    }
                    catch (Exception ex)
                    {
                        Program.NLogger.Error(ex, $"Job {jobDef.JobName} can not create trigger and will not start!");
                    }

                    // wait for a while
                    System.Threading.Thread.Sleep(1000);
                                        
                }
            }

        }

        private ITrigger GetTrigger(ExchangeServiceSettings jobDef)
        {
            //example CronExpression
            //https://www.freeformatter.com/cron-expression-generator-quartz.html
            //https://www.quartz-scheduler.net/documentation/quartz-3.x/tutorial/crontrigger.html

            ITrigger trigger = TriggerBuilder.Create()
                        .WithIdentity(jobDef.JobName, null)
                        .WithSchedule(Quartz.CronScheduleBuilder.CronSchedule(jobDef.CronExpression))
                        .StartAt(DateTime.Now)
                        .Build();

            return trigger;
        }

        private void CheckConnect2DB()
        {
            try
            {
                using (var db = Core.Data.ConnectionHelper.GetDB())
                {
                    Program.NLogger.Info($"Checking connect to {Core.Data.ConnectionHelper.ConnectionString} .....");
                    var sql = new Sql();
                    sql.Select("date = GETDATE()");
                    var d = db.FirstOrDefault<DateTime>(sql);
                    Program.NLogger.Info("DB connect is OK");
                }
            }
            catch (Exception ex)
            {
                Program.NLogger.Info("The database connection does not work");
                throw ex;
            }

        }

        #endregion
    }
}

﻿DECLARE @stoMoveItemID_max int, @makeID int

IF OBJECT_ID('dbo._test_onStoreSummary_1', 'U') IS NOT NULL 
  DROP TABLE dbo._test_onStoreSummary_1; 

IF OBJECT_ID('dbo._test_onStoreSummary_2', 'U') IS NOT NULL 
  DROP TABLE dbo._test_onStoreSummary_2; 

IF OBJECT_ID('dbo._test_onStoreSummary_base', 'U') IS NOT NULL 
  DROP TABLE dbo._test_onStoreSummary_base; 

DELETE FROM [s4_onStoreSummary_items]
DELETE FROM [s4_onStoreSummary]

SELECT @stoMoveItemID_max = MAX([stoMoveItemID]) FROM s4_storeMove_items
-- 30152557


-- run 1
SET @makeID = @stoMoveItemID_max / 2
EXEC s4_makeOnStoreSummary @makeID

SELECT * INTO _test_onStoreSummary_1
FROM [dbo].[s4_vw_onStore_Summary_Valid]
WHERE	quantity <> 0


-- run 2
SET @makeID = @stoMoveItemID_max - 10000
EXEC s4_makeOnStoreSummary @makeID

SELECT * INTO _test_onStoreSummary_2
FROM [dbo].[s4_vw_onStore_Summary_Valid]
WHERE	quantity <> 0


-- make base
SELECT * INTO _test_onStoreSummary_base
FROM [dbo].[s4_vw_onStore_Valid]
WHERE	quantity <> 0


SELECT SRC, [positionID], [stoMoveLotID], [carrierNum], [quantity], [artPackID], [batchNum], [expirationDate]
FROM (
	SELECT 'TABLE1-ONLY' AS SRC, T1.*
	FROM (
		  SELECT * FROM _test_onStoreSummary_1
		  EXCEPT
		  SELECT * FROM _test_onStoreSummary_base
		  ) AS T1
	UNION ALL
	SELECT 'TABLE2-ONLY' AS SRC, T2.*
	FROM (
		  SELECT * FROM _test_onStoreSummary_base
		  EXCEPT
		  SELECT * FROM _test_onStoreSummary_1
		  ) AS T2
) x
ORDER BY
	positionID, stoMoveLotID, carrierNum, quantity


SELECT SRC, [positionID], [stoMoveLotID], [carrierNum], [quantity], [artPackID], [batchNum], [expirationDate]
FROM (
	SELECT 'TABLE1-ONLY' AS SRC, T1.*
	FROM (
		  SELECT * FROM _test_onStoreSummary_2
		  EXCEPT
		  SELECT * FROM _test_onStoreSummary_base
		  ) AS T1
	UNION ALL
	SELECT 'TABLE2-ONLY' AS SRC, T2.*
	FROM (
		  SELECT * FROM _test_onStoreSummary_base
		  EXCEPT
		  SELECT * FROM _test_onStoreSummary_2
		  ) AS T2
) x
ORDER BY
	positionID, stoMoveLotID, carrierNum, quantity


SELECT SRC, [positionID], [stoMoveLotID], [carrierNum], [quantity], [artPackID], [batchNum], [expirationDate]
FROM (
	SELECT 'TABLE1-ONLY' AS SRC, T1.*
	FROM (
		  SELECT * FROM _test_onStoreSummary_1
		  EXCEPT
		  SELECT * FROM _test_onStoreSummary_2
		  ) AS T1
	UNION ALL
	SELECT 'TABLE2-ONLY' AS SRC, T2.*
	FROM (
		  SELECT * FROM _test_onStoreSummary_2
		  EXCEPT
		  SELECT * FROM _test_onStoreSummary_1
		  ) AS T2
) x
ORDER BY
	positionID, stoMoveLotID, carrierNum, quantity

﻿CREATE DEFAULT [dbo].[s4_def_emptyCarrier]
    AS '_NONE_';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[s4_def_emptyCarrier]', @objname = N'[dbo].[s4_stocktaking_snapshots].[carrierNum]';

GO
EXECUTE sp_bindefault @defname = N'[dbo].[s4_def_emptyCarrier]', @objname = N'[dbo].[s4_stocktaking_items].[carrierNum]';

GO
EXECUTE sp_bindefault @defname = N'[dbo].[s4_def_emptyCarrier]', @objname = N'[dbo].[s4_storeMove_items].[carrierNum]';

﻿CREATE DEFAULT [dbo].[s4_def_emptyPrice]
    AS -1;


GO
EXECUTE sp_bindefault @defname = N'[dbo].[s4_def_emptyPrice]', @objname = N'[dbo].[s4_direction_items].[unitPrice]';


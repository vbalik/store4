﻿CREATE DEFAULT [dbo].[s4_def_emptyExpirationDate]
    AS '1900-01-01';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[s4_def_emptyExpirationDate]', @objname = N'[dbo].[s4_stocktaking_snapshots].[expirationDate]';

GO
EXECUTE sp_bindefault @defname = N'[dbo].[s4_def_emptyExpirationDate]', @objname = N'[dbo].[s4_stocktaking_items].[expirationDate]';

GO
EXECUTE sp_bindefault @defname = N'[dbo].[s4_def_emptyExpirationDate]', @objname = N'[dbo].[s4_storeMove_lots].[expirationDate]';

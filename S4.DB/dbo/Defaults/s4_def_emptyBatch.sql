﻿CREATE DEFAULT [dbo].[s4_def_emptyBatchNum]
    AS '_NONE_';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[s4_def_emptyBatchNum]', @objname = N'[dbo].[s4_storeMove_lots].[batchNum]';

GO
EXECUTE sp_bindefault @defname = N'[dbo].[s4_def_emptyBatchNum]', @objname = N'[dbo].[s4_stocktaking_items].[batchNum]';

GO
EXECUTE sp_bindefault @defname = N'[dbo].[s4_def_emptyBatchNum]', @objname = N'[dbo].[s4_stocktaking_snapshots].[batchNum]';


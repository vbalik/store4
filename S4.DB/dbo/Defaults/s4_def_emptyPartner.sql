﻿CREATE DEFAULT [dbo].[s4_def_emptyPartner]
    AS -1;


GO
EXECUTE sp_bindefault @defname = N'[dbo].[s4_def_emptyPartner]', @objname = N'[dbo].[s4_direction].[partnerID]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[s4_def_emptyPartner]', @objname = N'[dbo].[s4_direction].[partAddrID]';


﻿CREATE PROCEDURE [dbo].[s4_makeOnStoreSummary]
	@lastStoMoveItemID int
AS

DECLARE @onStoreSummID int;


-- check summary with 'new summary' status
IF(EXISTS(SELECT * FROM [dbo].[s4_onStoreSummary] WHERE [onStoreSummStatus] = 0))
THROW 50000, 'Store4: New summary already exists', 1;


-- insert new summary with 'new summary' status
INSERT INTO [dbo].[s4_onStoreSummary] ([onStoreSummStatus] , [lastStoMoveItemID])
VALUES
	(0, @lastStoMoveItemID)
SELECT @onStoreSummID = SCOPE_IDENTITY()


BEGIN TRANSACTION;

BEGIN TRY
	-- cancel not valid s4_storeMove_items
	UPDATE [dbo].[s4_storeMove_items]
	SET
		[itemValidity] = 255 -- MI_VALIDITY_CANCELED
	WHERE
		[itemValidity] = 0 -- MI_VALIDITY_NOT_VALID
		AND stoMoveItemID <= @lastStoMoveItemID
	

	-- fill s4_onStoreSummary_items
	INSERT INTO [dbo].[s4_onStoreSummary_items] ([onStoreSummID], [stoMoveLotID], [positionID], [carrierNum], [quantity])
	SELECT
		@onStoreSummID,
		[stoMoveLotID],
		[positionID],
		[carrierNum],
		SUM([quantity] * [itemDirection]) AS [quantity]
	FROM	[dbo].[s4_storeMove_items]
	WHERE	[itemValidity] = 100
			AND [stoMoveItemID] <= @lastStoMoveItemID
	GROUP BY
		[stoMoveLotID],
		[positionID],
		[carrierNum]
	HAVING
		SUM([quantity] * [itemDirection]) <> 0


	-- update last summary status to 'replaced summary'
	UPDATE [dbo].[s4_onStoreSummary]
	SET
		[onStoreSummStatus] = 255
	WHERE
		[onStoreSummStatus] <> 255
		AND [onStoreSummID] <> @onStoreSummID


	-- update new summary to 'saved summary' status
	UPDATE [dbo].[s4_onStoreSummary]
	SET
		[onStoreSummStatus] = 100
	WHERE
		[onStoreSummID] = @onStoreSummID

	COMMIT TRANSACTION;
END TRY
BEGIN CATCH
    IF @@TRANCOUNT > 0
    BEGIN
        ROLLBACK TRANSACTION;
    END
END CATCH
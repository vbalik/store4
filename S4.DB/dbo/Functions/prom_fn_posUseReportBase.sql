﻿CREATE   FUNCTION prom_fn_posUseReportBase
(	
	@from AS date,
	@to AS date
)  
RETURNS TABLE  
    RETURN 

	SELECT
			pos.positionID, pos.posCode, pos.posDesc, pos.houseID, pos.posCateg, pos.posStatus, pos.posHeight, pos.posAttributes, pos.posX, pos.posY, pos.posZ, pos.mapPos,
			ISNULL(movesCnt.cnt, 0) AS movesCnt,
			ISNULL(onStore.quantity, 0) AS onStore
	FROM	dbo.s4_positionList pos
			LEFT JOIN 
				(
				SELECT
						smi.positionID,
						COUNT(*) AS cnt
				FROM	dbo.s4_storeMove sm JOIN dbo.s4_storeMove_items smi ON (sm.stoMoveID = smi.stoMoveID)
				WHERE	(smi.itemValidity = 100)
						AND (sm.docDate_d BETWEEN @from AND @to)
				GROUP BY
						smi.positionID
				) movesCnt ON (pos.positionID = movesCnt.positionID)
			LEFT JOIN 
				(
				SELECT	
						positionID,
						SUM(quantity) AS quantity
				FROM	dbo.s4_vw_onStore_Summary_Valid
				GROUP BY
						positionID
				) onStore ON (pos.positionID = onStore.positionID)
	WHERE
			pos.posCateg = 0

GO
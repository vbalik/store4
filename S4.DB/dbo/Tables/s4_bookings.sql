﻿CREATE TABLE [dbo].[s4_bookings]
(
    [bookingID] INT NOT NULL IDENTITY (1, 1), 
    [bookingStatus] VARCHAR(32) NOT NULL, 
    [stoMoveLotID] INT NOT NULL,
    [partnerID] INT NOT NULL,
    [partAddrID] INT NOT NULL DEFAULT 0,
    [requiredQuantity] NUMERIC (18, 4)  NOT NULL,
    [bookingTimeout] DATETIME NOT NULL,
    [usedInDirectionID] INT,
    [parentBookingID] INT,
    [deletedByEntryUserID]     [dbo].[s4_type_userID] NULL,
    [deletedDateTime]   DATETIME,
    [entryDateTime]   DATETIME                    CONSTRAINT [DF_s4_bookings_items_entryDateTime] DEFAULT (getdate()) NOT NULL,
    [entryUserID]     [dbo].[s4_type_userID]      NOT NULL,
    [r_Edit]          ROWVERSION                  NOT NULL, 
    CONSTRAINT [PK_s4_bookings] PRIMARY KEY CLUSTERED ([bookingID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_s4_storeMove_lots_s4_bookings] FOREIGN KEY ([stoMoveLotID]) REFERENCES [dbo].[s4_storeMove_lots]([stoMoveLotID]),
    CONSTRAINT [FK_s4_partnerList_s4_bookings] FOREIGN KEY ([partnerID]) REFERENCES [dbo].[s4_partnerList] ([partnerID]),
    CONSTRAINT [FK_s4_partnerList_addresses_s4_bookings] FOREIGN KEY ([partAddrID]) REFERENCES [dbo].[s4_partnerList_addresses] ([partAddrID])
)

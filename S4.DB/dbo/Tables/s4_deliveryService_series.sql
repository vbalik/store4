﻿CREATE TABLE [dbo].[s4_deliveryService_series] (
    [deliveryServiceSeriesID]   INT          IDENTITY (1, 1) NOT NULL,
    [deliveryPartnerName]   NVARCHAR(MAX)   NOT NULL,
    [productType]           NVARCHAR(MAX)   NULL,
    [status] [dbo].[s4_type_status]         NOT NULL,
    [currentNumber]         BIGINT          NOT NULL DEFAULT 0,
    [seriesFrom]            BIGINT          NOT NULL DEFAULT 0,
    [seriesTo]              BIGINT          NOT NULL DEFAULT 0,
    [seriesCreated]         DATETIME        NULL,
    [seriesChanged]         DATETIME        NULL,
    [r_Edit]                ROWVERSION      NOT NULL,
    CONSTRAINT [PK_s4_delivery_service_series] PRIMARY KEY CLUSTERED ([deliveryServiceSeriesID] ASC)
);


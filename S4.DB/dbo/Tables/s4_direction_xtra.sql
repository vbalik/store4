﻿CREATE TABLE [dbo].[s4_direction_xtra] (
    [directionExID] [dbo].[s4_type_xtraID]     IDENTITY (1, 1) NOT NULL,
    [directionID]   [dbo].[s4_type_documentID] NOT NULL,
    [xtraCode]      [dbo].[s4_type_xtraCode]   NOT NULL,
    [docPosition]   INT                        CONSTRAINT [DF_s4_direction_xtra_docPosition] DEFAULT (0) NOT NULL,
    [xtraValue]     [dbo].[s4_type_xtraValue]  NOT NULL,
    [r_Edit]        ROWVERSION                 NOT NULL,
    [xtraValue_checksum]  AS (checksum([xtraValue])) PERSISTED,
    CONSTRAINT [PK_s4_direction_xtra] PRIMARY KEY CLUSTERED ([directionExID] ASC),
    CONSTRAINT [FK_s4_direction_xtra_s4_direction] FOREIGN KEY ([directionID]) REFERENCES [dbo].[s4_direction] ([directionID])
);


GO
CREATE NONCLUSTERED INDEX [IX_s4_direction_xtra_directionID_xtraCode]
    ON [dbo].[s4_direction_xtra]([directionID] ASC, [xtraCode] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_s4_direction_xtra_directionID_xtraCode_docPosition]
    ON [dbo].[s4_direction_xtra]([directionID] ASC, [xtraCode] ASC, [docPosition] ASC);

GO
CREATE NONCLUSTERED INDEX [IX_s4_direction_xtra_xtraValue_checksum] 
    ON [dbo].[s4_direction_xtra] ([xtraValue_checksum] ASC)
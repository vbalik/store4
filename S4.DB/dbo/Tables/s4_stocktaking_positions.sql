﻿CREATE TABLE [dbo].[s4_stocktaking_positions] (
    [stotakPosID]  [dbo].[s4_type_docItemID]  IDENTITY (1, 1) NOT NULL,
    [stotakID]     [dbo].[s4_type_documentID] NOT NULL,
    [positionID]   INT                        NOT NULL,
    [checkCounter] INT                        CONSTRAINT [DF_s4_stocktaking_positions_checkCounter] DEFAULT ((0)) NOT NULL,
    [r_Edit]       ROWVERSION                 NOT NULL,
    CONSTRAINT [PK_s4_stocktaking_positions] PRIMARY KEY CLUSTERED ([stotakPosID] ASC),
    CONSTRAINT [FK_s4_stocktaking_positions_s4_stocktaking] FOREIGN KEY ([stotakID]) REFERENCES [dbo].[s4_stocktaking] ([stotakID]),
    CONSTRAINT [FK_s4_stocktaking_positions_s4_positionList] FOREIGN KEY ([positionID]) REFERENCES [dbo].[s4_positionList] ([positionID]),
    CONSTRAINT [IX_s4_stocktaking_positions] UNIQUE NONCLUSTERED ([stotakID] ASC, [positionID] ASC)
);


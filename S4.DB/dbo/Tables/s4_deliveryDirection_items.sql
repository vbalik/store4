﻿CREATE TABLE [dbo].[s4_deliveryDirection_items]
(
	[deliveryDirectionID] INT NOT NULL, 
    [parcelPosition] INT NOT NULL,
    [parcelRefNumber] NVARCHAR(128) NULL,
    [r_Edit]           ROWVERSION   NOT NULL, 
    CONSTRAINT [FK_s4_deliveryDirection_items_deliveryDirection] FOREIGN KEY ([deliveryDirectionID]) REFERENCES [s4_deliveryDirection]([deliveryDirectionID]), 
    CONSTRAINT [PK_s4_deliveryDirection_items] PRIMARY KEY ([deliveryDirectionID], [parcelPosition]) 
)

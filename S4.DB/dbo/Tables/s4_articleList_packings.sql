﻿CREATE TABLE [dbo].[s4_articleList_packings] (
    [artPackID]    INT            IDENTITY (1, 1) NOT NULL,
    [articleID]    INT            NOT NULL,
    [packDesc]     NVARCHAR (32)   NOT NULL,
    [packRelation] NUMERIC (9, 2) NOT NULL,
    [movablePack]  BIT            CONSTRAINT [DF_s4_articleList_packings_basicPacking] DEFAULT (0) NOT NULL,
    [source_id]    [dbo].[s4_type_sourceID]   NOT NULL,
    [packStatus]   NVARCHAR(16)        CONSTRAINT [DF_s4_articleList_packings_packStatus] DEFAULT (0) NOT NULL,
    [barCodeType]  TINYINT        CONSTRAINT [DF_s4_articleList_packings_barCodeType] DEFAULT (0) NOT NULL,
    [barCode]      VARCHAR (32)   NULL,
    [inclCarrier]  BIT            CONSTRAINT [DF_s4_articleList_packings_inclCarrier] DEFAULT (0) NOT NULL,
    [packWeight]   NUMERIC (5, 2) CONSTRAINT [DF_s4_articleList_packings_packWeight] DEFAULT (0) NOT NULL,
    [packVolume]   NUMERIC (5, 2) CONSTRAINT [DF_s4_articleList_packings_packVolume] DEFAULT (0) NOT NULL,
    [r_Edit]       ROWVERSION     NOT NULL,
    [packInfo]     AS             ([packDesc] + ' (' + convert(varchar(16),[packRelation]) + ')'),
    CONSTRAINT [PK_s4_articleList_packings] PRIMARY KEY CLUSTERED ([artPackID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_s4_articleList_packings_s4_articleList] FOREIGN KEY ([articleID]) REFERENCES [dbo].[s4_articleList] ([articleID])
);


GO
CREATE NONCLUSTERED INDEX [IX_s4_articleList_packings]
    ON [dbo].[s4_articleList_packings]([articleID] ASC, [packDesc] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_s4_articleList_packings_source_id]
    ON [dbo].[s4_articleList_packings]([source_id] ASC) WITH (FILLFACTOR = 90);


﻿CREATE TABLE [dbo].[s4_onStoreSummary_items]
(
	[onStoreSummItemID]		[dbo].[s4_type_docItemID]  IDENTITY (1, 1) NOT NULL,
	[onStoreSummID]			[dbo].[s4_type_docItemID] NOT NULL,
	[stoMoveLotID]       [dbo].[s4_type_docItemID]  NOT NULL,
    [positionID]         INT                        NOT NULL,
	[carrierNum]         [dbo].[s4_type_carrierNum] NOT NULL,    
	[quantity]           NUMERIC (18, 4)            NOT NULL,
	[r_Edit]             ROWVERSION                 NOT NULL, 
    CONSTRAINT [FK_s4_onStoreSumm_item_s4_onStoreSummary] FOREIGN KEY ([onStoreSummID]) REFERENCES [s4_onStoreSummary]([onStoreSummID]), 
	CONSTRAINT [FK_s4_onStoreSummary_items_s4_storeMove_lots] FOREIGN KEY([stoMoveLotID]) REFERENCES [dbo].[s4_storeMove_lots] ([stoMoveLotID]),
    CONSTRAINT [PK_s4_onStoreSummary_item] PRIMARY KEY ([onStoreSummItemID])
)

GO

CREATE INDEX [IX_s4_onStoreSummary_items_onStore1] ON [dbo].[s4_onStoreSummary_items] (
	[onStoreSummID] ASC,
	[stoMoveLotID] ASC,
	[positionID] ASC,
	[carrierNum] ASC)
GO

CREATE INDEX [IX_s4_onStoreSummary_items_onStoreSummID] ON [dbo].[s4_onStoreSummary_items] ([onStoreSummID])

﻿CREATE TABLE [dbo].[s4_article_xtra] (
    [articleExID] [dbo].[s4_type_xtraID]     IDENTITY (1, 1) NOT NULL,
    [articleID]   [dbo].[s4_type_documentID] NOT NULL,
    [xtraCode]      [dbo].[s4_type_xtraCode]   NOT NULL,
    [docPosition]   INT                        CONSTRAINT [DF_s4_article_xtra_docPosition] DEFAULT (0) NOT NULL,
    [xtraValue]     [dbo].[s4_type_xtraValue]  NOT NULL,
    [r_Edit]        ROWVERSION                 NOT NULL,
    [xtraValue_checksum]  AS (checksum([xtraValue])) PERSISTED,
    CONSTRAINT [PK_s4_article_xtra] PRIMARY KEY CLUSTERED ([articleExID] ASC),
    CONSTRAINT [FK_s4_article_xtra_s4_article] FOREIGN KEY ([articleID]) REFERENCES [dbo].[s4_articleList] ([articleID])
);


GO
CREATE NONCLUSTERED INDEX [IX_s4_article_xtra_articleID_xtraCode]
    ON [dbo].[s4_article_xtra]([articleID] ASC, [xtraCode] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_s4_article_xtra_articleID_xtraCode_docPosition]
    ON [dbo].[s4_article_xtra]([articleID] ASC, [xtraCode] ASC, [docPosition] ASC);

GO
CREATE NONCLUSTERED INDEX [IX_s4_article_xtra_xtraValue_checksum] 
    ON [dbo].[s4_article_xtra] ([xtraValue_checksum] ASC)
﻿CREATE TABLE [dbo].[s4_storeMove_items] (
    [stoMoveItemID]      [dbo].[s4_type_docItemID]  IDENTITY (1, 1) NOT NULL,
    [stoMoveID]          [dbo].[s4_type_documentID] NOT NULL,
    [docPosition]        INT                        NOT NULL,
    [itemValidity]       TINYINT                    DEFAULT ((0)) NOT NULL,
    [itemDirection]      SMALLINT                   NOT NULL,
    [stoMoveLotID]       [dbo].[s4_type_docItemID]  NOT NULL,
    [positionID]         INT                        NOT NULL,
    [carrierNum]         [dbo].[s4_type_carrierNum] NOT NULL,
    [quantity]           NUMERIC (18, 4)            NOT NULL,
    [parent_docPosition] INT                        CONSTRAINT [DF_s4_storeMove_items_parent_docPosition] DEFAULT ((-1)) NOT NULL,
    [brotherID]          INT                        CONSTRAINT [DF_s4_storeMove_items_parent_docPosition1] DEFAULT ((-1)) NOT NULL,
    [entryDateTime]      DATETIME                   CONSTRAINT [DF_s4_storeMove_items_entryDateTime] DEFAULT (getdate()) NOT NULL,
    [entryUserID]        [dbo].[s4_type_userID]     NOT NULL,
    [r_Edit]             ROWVERSION                 NOT NULL,
    CONSTRAINT [PK_s4_storeMove_items] PRIMARY KEY CLUSTERED ([stoMoveItemID] ASC),
    CONSTRAINT [FK_s4_storeMove_items_s4_positionList] FOREIGN KEY ([positionID]) REFERENCES [dbo].[s4_positionList] ([positionID]),
    CONSTRAINT [FK_s4_storeMove_items_s4_storeMove] FOREIGN KEY ([stoMoveID]) REFERENCES [dbo].[s4_storeMove] ([stoMoveID]),
    CONSTRAINT [FK_s4_storeMove_items_s4_storeMove_lots] FOREIGN KEY ([stoMoveLotID]) REFERENCES [dbo].[s4_storeMove_lots] ([stoMoveLotID]),
    CONSTRAINT [CK_s4_storeMove_items] CHECK ([itemDirection]=(-1) OR [itemDirection]=(1)),
    CONSTRAINT [CK_s4_storeMove_items_itemValidity] CHECK ([itemValidity]=(0) OR [itemValidity]=(100) OR [itemValidity]=(255)),
    CONSTRAINT [CK_s4_storeMove_items_position] CHECK ([docPosition]<>(0))
);




GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_s4_storeMove_items_position]
    ON [dbo].[s4_storeMove_items]([stoMoveID] ASC, [docPosition] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_s4_storeMove_items_positionID]
    ON [dbo].[s4_storeMove_items]([stoMoveItemID] ASC, [positionID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_s4_storeMove_items_stoMoveLotID]
    ON [dbo].[s4_storeMove_items]([stoMoveLotID] ASC);


GO
CREATE NONCLUSTERED INDEX [s4_storeMove_items_onPos1]
    ON [dbo].[s4_storeMove_items]([positionID] ASC, [stoMoveLotID] ASC, [carrierNum] ASC);


GO
CREATE NONCLUSTERED INDEX [s4_storeMove_items_onPos2]
    ON [dbo].[s4_storeMove_items]([itemValidity] ASC, [positionID] ASC, [stoMoveLotID] ASC, [carrierNum] ASC);


GO
--CREATE STATISTICS [hind_1392724014_6A_2A_4A]
--    ON [dbo].[s4_storeMove_items]([positionID], [stoMoveID], [itemStatus], [stoMoveItemID]);


--GO
--CREATE STATISTICS [hind_1392724014_2A_4A_6A]
--    ON [dbo].[s4_storeMove_items]([stoMoveID], [itemStatus], [positionID], [stoMoveItemID]);


--GO
--CREATE STATISTICS [hind_1392724014_4A_7A_6A_8A_10A_11A_5A_9A]
--    ON [dbo].[s4_storeMove_items]([itemStatus], [artPackID], [positionID], [carrierNum], [batchNum], [expirationDate], [itemDirection], [quantity], [stoMoveItemID]);


--GO
--CREATE STATISTICS [hind_1392724014_4A_7A_8A_10A_11A_5A_6A_9A]
--    ON [dbo].[s4_storeMove_items]([itemStatus], [artPackID], [carrierNum], [batchNum], [expirationDate], [itemDirection], [positionID], [quantity], [stoMoveItemID]);


EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Can be 0 - not valid, 100 - valid, 255 - canceled',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N's4_storeMove_items',
    @level2type = N'COLUMN',
    @level2name = N'itemValidity'
GO


CREATE TRIGGER [dbo].[s4_storeMove_items_update_delete]
    ON [dbo].[s4_storeMove_items]
    FOR DELETE, UPDATE
    AS
    BEGIN
        SET NoCount ON

		-- check if it is part of summary
		DECLARE @lastStoMoveItemID int;
		SELECT @lastStoMoveItemID = MAX([lastStoMoveItemID]) FROM [dbo].[s4_onStoreSummary] WHERE [onStoreSummStatus] = 100

		IF((SELECT MAX(stoMoveItemID) FROM deleted) <= @lastStoMoveItemID)
		BEGIN
			RAISERROR ('Store4: Can''t change items of s4_storeMove_items - thay are part of onStoreSummary', 16, 1)
			ROLLBACK TRANSACTION
		END
    END
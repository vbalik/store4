﻿CREATE TABLE [dbo].[s4_internMessages] (
    [intMessID]      INT           IDENTITY (1, 1) NOT NULL,
    [msgClass]       [dbo].[s4_type_docClass]      CONSTRAINT [DF_s4_internMessages_msgClass] DEFAULT ('?') NOT NULL,
    [msgSeverity]    TINYINT       CONSTRAINT [DF_s4_internMessages_msgSeverity] DEFAULT (0) NOT NULL,
    [msgHeader]      NVARCHAR (MAX) NOT NULL,
    [msgText]        NVARCHAR (MAX) NULL,
    [msgDateTime]    DATETIME      CONSTRAINT [DF_s4_internMessages_msgDateTime] DEFAULT (getdate()) NOT NULL,
    [msgUserID]      VARCHAR(16)      NULL ,
    [r_Edit]         ROWVERSION    NOT NULL,
    CONSTRAINT [PK_s4_internMessages] PRIMARY KEY CLUSTERED ([intMessID] ASC)
);


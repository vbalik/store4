﻿CREATE TABLE [dbo].[s4_workerList_history] (
    [workerHistID]  [dbo].[s4_type_docHistoryID] IDENTITY (1, 1) NOT NULL,
    [workerID]      [dbo].[s4_type_userID]       NOT NULL,
    [eventCode]     [dbo].[s4_type_status]       NOT NULL,
    [eventData]     NVARCHAR (MAX)                NULL,
    [entryDateTime] DATETIME                     CONSTRAINT [DF_s4_workerList_history_entryDateTime] DEFAULT (getdate()) NOT NULL,
    [entryUserID]   [dbo].[s4_type_userID]       NOT NULL,
    [r_Edit]        ROWVERSION                   NOT NULL,
    CONSTRAINT [PK_s4_workerList_history] PRIMARY KEY CLUSTERED ([workerHistID] ASC),
    CONSTRAINT [FK_s4_workerList_history_s4_workerList] FOREIGN KEY ([workerID]) REFERENCES [dbo].[s4_workerList] ([workerID])
);


GO
CREATE NONCLUSTERED INDEX [IX_s4_workerList_history_workerID]
    ON [dbo].[s4_workerList_history]([workerID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_s4_workerList_history_workerID_eventCode]
    ON [dbo].[s4_workerList_history]([workerID] ASC, [eventCode] ASC);


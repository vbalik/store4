﻿CREATE TABLE [dbo].[s4_settings] (
    [setFieldID]  VARCHAR (32)   NOT NULL,
    [setDataType] VARCHAR (64)   NOT NULL,
    [setValue]    NVARCHAR (MAX) NOT NULL,
    [r_Edit]      ROWVERSION     NOT NULL, 
    CONSTRAINT [PK_s4_settings] PRIMARY KEY ([setFieldID])
);


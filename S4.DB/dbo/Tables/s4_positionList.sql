﻿CREATE TABLE [dbo].[s4_positionList] (
    [positionID]   INT          IDENTITY (1, 1) NOT NULL,
    [posCode]      VARCHAR (16)  NOT NULL,
    [posDesc]      NVARCHAR (128) NULL,
    [houseID]      VARCHAR(16)     NOT NULL,
    [posCateg]     TINYINT      CONSTRAINT [DF_s4_positionList_posCateg] DEFAULT (0) NOT NULL,
    [posStatus]    VARCHAR(16)      CONSTRAINT [DF_s4_positionList_posStatus] DEFAULT (0) NOT NULL,
    [posHeight]    TINYINT      CONSTRAINT [DF_s4_positionList_posHeight] DEFAULT (0) NOT NULL,
	[posAttributes] INT			CONSTRAINT [DF_s4_positionList_posAttributes] DEFAULT (0) NOT NULL,
    [posX]         INT          CONSTRAINT [DF_s4_positionList_posX] DEFAULT (0) NOT NULL,
    [posY]         INT          CONSTRAINT [DF_s4_positionList_posY] DEFAULT (0) NOT NULL,
    [posZ]         INT          CONSTRAINT [DF_s4_positionList_posZ] DEFAULT (0) NOT NULL,
    [mapPos]       VARCHAR (32) NULL,
    [r_Edit]       ROWVERSION   NOT NULL,
    CONSTRAINT [PK_s4_positionList] PRIMARY KEY CLUSTERED ([positionID] ASC),
    CONSTRAINT [FK_s4_positionList_s4_houseList] FOREIGN KEY ([houseID]) REFERENCES [dbo].[s4_houseList] ([houseID]),
    CONSTRAINT [IX_s4_positionList_posXYZ] UNIQUE NONCLUSTERED ([posX] ASC, [posY] ASC, [posZ] ASC, [houseID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_s4_positionList_posCode]
    ON [dbo].[s4_positionList]([posCode] ASC);


GO

CREATE INDEX [IX_s4_positionList_posCateg] ON [dbo].[s4_positionList] ([posCateg])

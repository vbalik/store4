﻿CREATE TABLE [dbo].[s4_partnerList_history] (
    [partnerHistID] [dbo].[s4_type_docHistoryID] IDENTITY (1, 1) NOT NULL,
    [partnerID]     INT                          NOT NULL,
    [partAddrID]     INT                          CONSTRAINT [DF_s4_partnerList_history_partAddrID] DEFAULT ((-1)) NOT NULL,
    [eventCode]     [dbo].[s4_type_status]       NOT NULL,
    [eventData]     NVARCHAR (MAX)                NULL,
    [entryDateTime] DATETIME                     CONSTRAINT [DF_s4_partnerList_history_entryDateTime] DEFAULT (getdate()) NOT NULL,
    [entryUserID]   [dbo].[s4_type_userID]       NOT NULL,
    [r_Edit]        ROWVERSION                   NOT NULL,
    CONSTRAINT [PK_s4_partnerList_history] PRIMARY KEY CLUSTERED ([partnerHistID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_s4_partnerList_history_s4_partnerList] FOREIGN KEY ([partnerID]) REFERENCES [dbo].[s4_partnerList] ([partnerID])
);


GO
CREATE NONCLUSTERED INDEX [IX_s4_partnerList_history_partnerID]
    ON [dbo].[s4_partnerList_history]([partnerID] ASC) WITH (FILLFACTOR = 90);


﻿CREATE TABLE [dbo].[s4_invoice_history] (
    [invoiceHistID] [dbo].[s4_type_docHistoryID] IDENTITY (1, 1) NOT NULL,
    [invoiceID]     [dbo].[s4_type_documentID]   NOT NULL,
    [eventCode]       [dbo].[s4_type_status]       NOT NULL,
    [eventData]     NVARCHAR (MAX)                NULL,
    [entryDateTime]   DATETIME                     CONSTRAINT [DF_s4_invoice_history_entryDateTime] DEFAULT (getdate()) NOT NULL,
    [entryUserID]     [dbo].[s4_type_userID]       NOT NULL,
    [r_Edit]          ROWVERSION                   NOT NULL,
    CONSTRAINT [PK_s4_invoice_history] PRIMARY KEY CLUSTERED ([invoiceHistID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_s4_invoice_history_s4_direction] FOREIGN KEY ([invoiceID]) REFERENCES [dbo].[s4_invoice] ([invoiceID])
);


GO



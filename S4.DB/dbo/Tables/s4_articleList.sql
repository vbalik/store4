﻿CREATE TABLE [dbo].[s4_articleList] (
    [articleID]       INT                      IDENTITY (1, 1) NOT NULL,
    [articleCode]     NVARCHAR (64)            NOT NULL,
    [articleDesc]     NVARCHAR (256)           NULL,
    [articleStatus]   NVARCHAR (16)            NOT NULL,
    [articleType]     TINYINT                  CONSTRAINT [DF_s4_articleList_articleType] DEFAULT ((0)) NOT NULL,
    [source_id]       [dbo].[s4_type_sourceID] NOT NULL,
    [sectID]          [dbo].[s4_type_sectID]   NULL,
    [unitDesc]        NVARCHAR (32)            NULL,
    [manufID]         [dbo].[s4_type_manufID]  NOT NULL,
    [useBatch]        BIT                      CONSTRAINT [DF_s4_articleList_useBatch] DEFAULT ((0)) NOT NULL,
    [mixBatch]        BIT                      CONSTRAINT [DF_s4_articleList_mixBatch] DEFAULT ((0)) NOT NULL,
    [useExpiration]   BIT                      CONSTRAINT [DF_s4_articleList_useExpiration] DEFAULT ((0)) NOT NULL,
    [articleInfo]     AS                       (([articleCode]+' - ')+isnull([articleDesc],'')),
    [specFeatures]    INT                      DEFAULT ((0)) NOT NULL,
    [userRemark]      NVARCHAR (MAX)           NULL,
    [useSerialNumber] BIT                      CONSTRAINT [DF_s4_articleList_useSerialNumber] DEFAULT ((0)) NOT NULL,
    [brand]           nvarchar(64)             NULL,  
    [r_Edit]          ROWVERSION               NOT NULL,
    [sign] NVARCHAR(64) NULL, 
    CONSTRAINT [PK_s4_articleList] PRIMARY KEY CLUSTERED ([articleID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_s4_articleList_s4_manufactList] FOREIGN KEY ([manufID]) REFERENCES [dbo].[s4_manufactList] ([manufID]),
    CONSTRAINT [FK_s4_articleList_s4_sectionList] FOREIGN KEY ([sectID]) REFERENCES [dbo].[s4_sectionList] ([sectID])
);




GO
CREATE NONCLUSTERED INDEX [IX_s4_articleList_articleCode]
    ON [dbo].[s4_articleList]([articleCode] ASC) WITH (FILLFACTOR = 90);



GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_s4_articleList_source_id]
    ON [dbo].[s4_articleList]([source_id] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_s4_articleList_manufID]
    ON [dbo].[s4_articleList]([manufID] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'0 - standard, 1 - carrier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N's4_articleList', @level2type = N'COLUMN', @level2name = N'articleType';



GO

CREATE INDEX [IX_s4_articleList_articleStatus] ON [dbo].[s4_articleList] ([articleStatus])
GO

CREATE INDEX [IX_s4_articleList_articleDesc] ON [dbo].[s4_articleList] ([articleDesc])

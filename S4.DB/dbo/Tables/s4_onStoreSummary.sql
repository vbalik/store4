﻿CREATE TABLE [dbo].[s4_onStoreSummary]
(
	[onStoreSummID]      [dbo].[s4_type_docItemID]  IDENTITY (1, 1) NOT NULL,
	[onStoreSummStatus]   TINYINT					NOT NULL DEFAULT 0,
	[lastStoMoveItemID]		[dbo].[s4_type_docItemID] NOT NULL,
	[entryDateTime]      DATETIME                   CONSTRAINT [DF_s4_onStoreSummary_entryDateTime] DEFAULT (getdate()) NOT NULL,
    [r_Edit]             ROWVERSION                 NOT NULL, 
    CONSTRAINT [PK_s4_onStoreSummary] PRIMARY KEY ([onStoreSummID])
)

GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'0 - new summary, 100 - saved summary, 255 - replaced summary',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N's4_onStoreSummary',
    @level2type = N'COLUMN',
    @level2name = N'onStoreSummStatus'
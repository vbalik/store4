﻿CREATE TABLE [dbo].[s4_direction] (
    [directionID]         [dbo].[s4_type_documentID]  IDENTITY (1, 1) NOT NULL,
    [docStatus]           [dbo].[s4_type_status]      NOT NULL,
    [docNumPrefix]        VARCHAR(16)                    NOT NULL,
    [docDirection]        SMALLINT                    NOT NULL,
    [docYear]             CHAR (2)                    NOT NULL,
    [docNumber]           INT                         NOT NULL,
    [docDate]             DATETIME                    NOT NULL,
    [partnerID]           INT                         NOT NULL,
    [partAddrID]          INT                         NOT NULL,
    [prepare_directionID] [dbo].[s4_type_documentID]  CONSTRAINT [DF_s4_direction_parent_directionID] DEFAULT ((-1)) NOT NULL,
    [partnerRef]          NVARCHAR(MAX)                NULL,
    [docRemark]           NVARCHAR(MAX)                NULL,
    [entryDateTime]       DATETIME                    CONSTRAINT [DF_s4_direction_entryDateTime] DEFAULT (getdate()) NOT NULL,
    [entryUserID]         [dbo].[s4_type_userID]      NOT NULL,
    [docDate_d]           AS                          (convert(datetime,convert(int,convert(float,[docDate])))),
    [docDate_p]           AS                          (datepart(year,[docDate]) * 100 + datepart(month,[docDate])),
    [docInfo]             AS                          (rtrim([docNumPrefix]) + '/' + [docYear] + '/' + convert(varchar(16),[docNumber])),
    [docRemark_prev]      AS                          (convert(varchar(64),[docRemark]) + case when (datalength([docRemark]) > 64) then '...' else '' end),
    [invoiceID]           INT                         NULL,  
    [doNotProcess]        BIT                         NOT NULL DEFAULT 0, 
    [r_Edit]              ROWVERSION                  NOT NULL,
    CONSTRAINT [PK_s4_direction] PRIMARY KEY CLUSTERED ([directionID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CK_s4_direction] CHECK ([docDirection] = (-1) or [docDirection] = 1 or [docDirection] = 0),
	CONSTRAINT [FK_s4_partnerList_s4_direction] FOREIGN KEY ([partnerID]) REFERENCES [dbo].[s4_partnerList] ([partnerID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_s4_direction_docNumPrefix_docYear_docNumber]
    ON [dbo].[s4_direction]([docNumPrefix] ASC, [docYear] ASC, [docNumber] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_s4_direction_docStatus_docNumPrefix]
    ON [dbo].[s4_direction]([docStatus] ASC, [docNumPrefix] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_s4_direction_partnerID]
    ON [dbo].[s4_direction]([partnerID] ASC);


GO

CREATE INDEX [IX_s4_direction_docStatus] ON [dbo].[s4_direction] ([docStatus])

GO

CREATE INDEX [IX_s4_direction_docDirection] ON [dbo].[s4_direction] ([docDirection])


GO

CREATE INDEX [IX_s4_direction_docDirection_docStatus] ON [dbo].[s4_direction] ([docDirection], [docStatus])

GO
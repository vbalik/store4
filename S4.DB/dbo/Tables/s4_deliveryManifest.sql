﻿CREATE TABLE [dbo].[s4_deliveryManifest] (
    [deliveryManifestID]    INT                    IDENTITY (1, 1) NOT NULL,
    [manifestStatus]        [dbo].[s4_type_status] NOT NULL,
    [deliveryProvider]      INT                    NOT NULL,
    [createManifestAttempt] SMALLINT               NULL,
    [createManifestError]   TEXT                   NULL,
    [manifestRefNumber]     NVARCHAR (128)         NULL,
    [manifestCreated]       DATETIME               NULL,
    [manifestChanged]       DATETIME               NULL,
    [r_Edit]                ROWVERSION             NOT NULL,
    CONSTRAINT [PK_s4_deliveryManifest] PRIMARY KEY CLUSTERED ([deliveryManifestID] ASC)
);


﻿CREATE TABLE [dbo].[s4_sectionList] (
    [sectID]    [dbo].[s4_type_sectID] NOT NULL,
    [sectDesc]  NVARCHAR (64) NOT NULL,
    [source_id] VARCHAR (48) NULL,
    [r_Edit]    ROWVERSION   NOT NULL,
    CONSTRAINT [PK_s4_sectionList] PRIMARY KEY CLUSTERED ([sectID] ASC)
);


﻿CREATE TABLE [dbo].[s4_printerLocation]
(
	[printerLocationID] INT NOT NULL PRIMARY KEY IDENTITY(1, 1), 
    [printerLocationDesc] VARCHAR(128), 
	[printerPath] VARCHAR(256) NOT NULL,
	[printerSettings] VARCHAR(MAX), 
	[printerTypeID] INT NOT NULL, 
    [r_Edit] ROWVERSION NOT NULL, 
    CONSTRAINT [FK_s4_printerLocation_PrinterType] FOREIGN KEY ([printerTypeID]) REFERENCES [s4_printerType]([printerTypeID])
)

﻿CREATE TABLE [dbo].[s4_storeMove_xtra] (
    [stoMoveExID] [dbo].[s4_type_xtraID]     IDENTITY (1, 1) NOT NULL,
    [stoMoveID]   [dbo].[s4_type_documentID] NOT NULL,
    [xtraCode]    [dbo].[s4_type_xtraCode]   NOT NULL,
    [xtraValue]   [dbo].[s4_type_xtraValue]  NOT NULL,
    [r_Edit]      ROWVERSION                 NOT NULL,
    CONSTRAINT [PK_s4_storeMove_xtra] PRIMARY KEY CLUSTERED ([stoMoveExID] ASC),
    CONSTRAINT [FK_s4_storeMove_xtra_s4_storeMove] FOREIGN KEY ([stoMoveID]) REFERENCES [dbo].[s4_storeMove] ([stoMoveID])
);


GO
CREATE NONCLUSTERED INDEX [IX_s4_storeMove_xtra_stoMoveID_xtraCode]
    ON [dbo].[s4_storeMove_xtra]([stoMoveID] ASC, [xtraCode] ASC);


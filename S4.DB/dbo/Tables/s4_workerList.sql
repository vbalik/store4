﻿CREATE TABLE [dbo].[s4_workerList] (
    [workerID]      [dbo].[s4_type_userID]     NOT NULL,
    [workerName]    NVARCHAR (256) NOT NULL,
    [workerClass]   INT      CONSTRAINT [DF_s4_workerList_workerClass] DEFAULT (0) NOT NULL,
    [workerStatus]  [dbo].[s4_type_status]       NOT NULL,
    [workerLogon]   VARCHAR (32) NOT NULL,
    [workerSecData] VARCHAR (256) NULL,
    [r_Edit]        ROWVERSION   NOT NULL,
    CONSTRAINT [PK_s4_workerList] PRIMARY KEY CLUSTERED ([workerID] ASC),
	CONSTRAINT [UN_s4_workerList_workerLogon] UNIQUE NONCLUSTERED ([workerLogon])
);


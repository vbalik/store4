﻿CREATE TABLE [dbo].[s4_deliveryDirection] (
    [deliveryDirectionID]   INT            IDENTITY (1, 1) NOT NULL,
    [directionID]           INT            NOT NULL,
    [deliveryStatus]        NVARCHAR (16) CONSTRAINT [DF_s4_s4_deliveryDirection] DEFAULT 'DD_OK' NOT NULL,
    [deliveryProvider]      INT            NOT NULL,
    [createShipmentAttempt] SMALLINT       NULL,
    [createShipmentError]   TEXT           NULL,
    [shipmentRefNumber]     NVARCHAR (128) NULL,
    [deliveryManifestID]    INT            NULL,
    [deliveryCreated]       DATETIME       NULL,
    [deliveryChanged]       DATETIME       NULL,
    [r_Edit]                ROWVERSION     NOT NULL,
    CONSTRAINT [PK_S4_deliveryDirection] PRIMARY KEY CLUSTERED ([deliveryDirectionID] ASC),
    CONSTRAINT [FK_s4_deliveryDirection_direction] FOREIGN KEY ([directionID]) REFERENCES [dbo].[s4_direction] ([directionID]),
    CONSTRAINT [FK_s4_deliveryDirection_s4_deliveryManifest] FOREIGN KEY ([deliveryManifestID]) REFERENCES [dbo].[s4_deliveryManifest] ([deliveryManifestID])
);



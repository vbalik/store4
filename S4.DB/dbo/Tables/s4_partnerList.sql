﻿CREATE TABLE [dbo].[s4_partnerList] (
    [partnerID]        INT          IDENTITY (1, 1) NOT NULL,
    [partnerName]      NVARCHAR (128) NOT NULL,
    [source_id]        [dbo].[s4_type_sourceID] NOT NULL,
	[partnerStatus]    [dbo].[s4_type_status]      NOT NULL,
    [partnerDispCheck] TINYINT      CONSTRAINT [DF_s4_partnerList_partnerStatus1] DEFAULT (0) NOT NULL,
    [partnerAddr_1]    VARCHAR (256) NULL,
    [partnerAddr_2]    VARCHAR (256) NULL,
    [partnerCity]      VARCHAR (48) NULL,
    [partnerPostCode]  VARCHAR (16) NULL,
    [partnerPhone]     VARCHAR (48) NULL,
    [partnerOrgIdentNumber]     VARCHAR (16) NULL,
    [r_Edit]           ROWVERSION   NOT NULL,
    [partnerInfo]      AS           ([partnerName] + isnull((', ' + [partnerCity]),'')),
    CONSTRAINT [PK_s4_partnerList] PRIMARY KEY CLUSTERED ([partnerID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_s4_partnerList_source_id]
    ON [dbo].[s4_partnerList]([source_id] ASC);


﻿CREATE TABLE [dbo].[s4_messageBus]
(
	[messageBusID] INT NOT NULL PRIMARY KEY IDENTITY, 
    [messageStatus] VARCHAR(32) NOT NULL, 
    [messageType] INT NOT NULL, 
    [messageData] NVARCHAR(MAX) NULL, 
    [tryCount] INT NOT NULL, 
    [nextTryTime] DATETIME NULL, 
    [errorDesc] NVARCHAR(MAX) NULL, 
    [lastDateTime] DATETIME NULL, 
    [entryDateTime] DATETIME NOT NULL, 
    [directionID] [dbo].[s4_type_documentID] NULL,
    [r_Edit] ROWVERSION NOT NULL
);



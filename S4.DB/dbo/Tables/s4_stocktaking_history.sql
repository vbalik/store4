﻿CREATE TABLE [dbo].[s4_stocktaking_history] (
    [stotakHistID]   [dbo].[s4_type_docHistoryID] IDENTITY (1, 1) NOT NULL,
    [stotakID]       [dbo].[s4_type_documentID]   NOT NULL,
    [eventCode]      [dbo].[s4_type_status]       NOT NULL,
    [positionID]     INT                          NULL,
    [artPackID]      INT                          NULL,
    [carrierNum]     [dbo].[s4_type_carrierNum]   NULL,
    [quantity]       NUMERIC (18, 4)              NULL,
    [batchNum]       [dbo].[s4_type_batchNum] NULL,
    [expirationDate] SMALLDATETIME                NULL,
    [entryDateTime]  DATETIME                     CONSTRAINT [DF_s4_stocktaking_history_entryDateTime] DEFAULT (getdate()) NOT NULL,
    [entryUserID]    [dbo].[s4_type_userID]       NOT NULL,
    [r_Edit]         ROWVERSION                   NOT NULL,
    CONSTRAINT [PK_s4_stocktaking_history] PRIMARY KEY CLUSTERED ([stotakHistID] ASC),
    CONSTRAINT [FK_s4_stocktaking_history_s4_articleList_packings] FOREIGN KEY ([artPackID]) REFERENCES [dbo].[s4_articleList_packings] ([artPackID]),
    CONSTRAINT [FK_s4_stocktaking_history_s4_positionList] FOREIGN KEY ([positionID]) REFERENCES [dbo].[s4_positionList] ([positionID]),
    CONSTRAINT [FK_s4_stocktaking_history_s4_stocktaking] FOREIGN KEY ([stotakID]) REFERENCES [dbo].[s4_stocktaking] ([stotakID])
);

GO
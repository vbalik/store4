﻿CREATE TABLE [dbo].[s4_printerType]
(
	[printerTypeID] INT NOT NULL PRIMARY KEY IDENTITY(1, 1), 
	[printerClass] TINYINT NOT NULL DEFAULT(0),
    [printerDesc] NVARCHAR(MAX) NOT NULL, 
    [r_Edit] ROWVERSION NOT NULL 
)

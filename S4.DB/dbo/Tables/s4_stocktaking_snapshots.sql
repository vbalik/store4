﻿CREATE TABLE [dbo].[s4_stocktaking_snapshots] (
    [stotakSnapID]   [dbo].[s4_type_docItemID]  IDENTITY (1, 1) NOT NULL,
    [stotakID]       [dbo].[s4_type_documentID] NOT NULL,
    [snapshotClass]  TINYINT                    NOT NULL DEFAULT 0,
    [positionID]     INT                        NOT NULL,
    [artPackID]      INT                        NOT NULL,
    [carrierNum]     [dbo].[s4_type_carrierNum] NULL,
    [quantity]       NUMERIC (18, 4)            NOT NULL,
    [batchNum]       [dbo].[s4_type_batchNum]   NULL,
    [expirationDate] SMALLDATETIME              NULL,
    [unitPrice]      MONEY                      NULL,
    [r_Edit]         ROWVERSION                 NOT NULL,
    CONSTRAINT [PK_s4_stocktaking_snapshots] PRIMARY KEY CLUSTERED ([stotakSnapID] ASC),
    CONSTRAINT [FK_s4_stocktaking_snapshots_s4_articleList_packings] FOREIGN KEY ([artPackID]) REFERENCES [dbo].[s4_articleList_packings] ([artPackID]),
    CONSTRAINT [FK_s4_stocktaking_snapshots_s4_positionList] FOREIGN KEY ([positionID]) REFERENCES [dbo].[s4_positionList] ([positionID]),
    CONSTRAINT [FK_s4_stocktaking_snapshots_s4_stocktaking] FOREIGN KEY ([stotakID]) REFERENCES [dbo].[s4_stocktaking] ([stotakID])
);


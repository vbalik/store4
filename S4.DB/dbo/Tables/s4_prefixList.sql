﻿CREATE TABLE [dbo].[s4_prefixList] (
    [docClass]       [dbo].[s4_type_docClass] NOT NULL,
    [docNumPrefix]   CHAR (16)       NOT NULL,
    [prefixDesc]     VARCHAR (64)   NOT NULL,
    [docDirection]   SMALLINT       CONSTRAINT [DF_s4_prefixList_docDirection] DEFAULT (0) NOT NULL,
    [docType]        TINYINT        CONSTRAINT [DF_s4_prefixList_docType] DEFAULT (0) NOT NULL,
    [prefixEnabled]  BIT            CONSTRAINT [DF_s4_prefixList_prefixEnabled] DEFAULT (1) NOT NULL,
    [prefixSection]  [dbo].[s4_type_sectID] NULL,
    [prefixSettings] VARCHAR (MAX) NULL,
    [doNotProcess]   BIT NOT NULL DEFAULT 0,
    [r_Edit]         ROWVERSION     NOT NULL,
    CONSTRAINT [PK_s4_prefixList] PRIMARY KEY CLUSTERED ([docClass] ASC, [docNumPrefix] ASC),
    CONSTRAINT [CK_s4_prefixList] CHECK ([docDirection] = (-1) or ([docDirection] = 1 or [docDirection] = 0)),
    CONSTRAINT [IX_s4_prefixList_docNumPrefix] UNIQUE NONCLUSTERED ([docNumPrefix] ASC)
);


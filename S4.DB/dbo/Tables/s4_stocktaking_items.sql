﻿CREATE TABLE [dbo].[s4_stocktaking_items] (
    [stotakItemID]   [dbo].[s4_type_docItemID]  IDENTITY (1, 1) NOT NULL,
    [stotakID]       [dbo].[s4_type_documentID] NOT NULL,
    [positionID]     INT                        NOT NULL,
    [artPackID]      INT                        NOT NULL,
    [quantity]       NUMERIC (18, 4)            NOT NULL,
	[carrierNum]     [dbo].[s4_type_carrierNum] NULL,
    [batchNum]       [dbo].[s4_type_batchNum]   NULL,
    [expirationDate] SMALLDATETIME              NULL,	
    [entryDateTime]  DATETIME                   CONSTRAINT [DF_s4_stocktaking_items_entryDateTime] DEFAULT (getdate()) NOT NULL,
    [entryUserID]    [dbo].[s4_type_userID]     NOT NULL,
    [r_Edit]         ROWVERSION                 NOT NULL,
    CONSTRAINT [PK_s4_stocktaking_items] PRIMARY KEY CLUSTERED ([stotakItemID] ASC),
    CONSTRAINT [FK_s4_stocktaking_items_s4_articleList_packings] FOREIGN KEY ([artPackID]) REFERENCES [dbo].[s4_articleList_packings] ([artPackID]),
    CONSTRAINT [FK_s4_stocktaking_items_s4_positionList] FOREIGN KEY ([positionID]) REFERENCES [dbo].[s4_positionList] ([positionID]),
    CONSTRAINT [FK_s4_stocktaking_items_s4_stocktaking] FOREIGN KEY ([stotakID]) REFERENCES [dbo].[s4_stocktaking] ([stotakID])
);


GO



CREATE TRIGGER s4_stocktaking_items_insert_update
ON dbo.s4_stocktaking_items
FOR INSERT, UPDATE
AS

IF(EXISTS (
	SELECT	*
	FROM	inserted ins JOIN dbo.s4_stocktaking stk ON (ins.stotakID = stk.stotakID)
	WHERE	(stk.stotakStatus != 'OPENED')
))
BEGIN
   RAISERROR ('Store4: Can''t change items of locked stocktaking', 16, 1)
   ROLLBACK TRANSACTION
END


GO



CREATE TRIGGER s4_stocktaking_items_delete
ON dbo.s4_stocktaking_items
FOR DELETE
AS

IF(EXISTS (
	SELECT	*
	FROM	deleted del JOIN dbo.s4_stocktaking stk ON (del.stotakID = stk.stotakID)
	WHERE	(stk.stotakStatus != 'OPENED')
))
BEGIN
   RAISERROR ('Store4: Can''t change items of locked stocktaking', 16, 1)
   ROLLBACK TRANSACTION
END


﻿CREATE TABLE [dbo].[s4_workerList_setting](
	[workerID] [varchar](16) NOT NULL,
	[settingCode] [varchar](16) NOT NULL,
	[settingValue] [text] NULL,
 CONSTRAINT [PK_s4_workerList_setting] PRIMARY KEY CLUSTERED 
(
	[workerID] ASC,
	[settingCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

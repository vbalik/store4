﻿CREATE TABLE [dbo].[s4_articleList_packing_barcodes] (
    [artPackBarCodeID]   INT IDENTITY (1, 1) NOT NULL,
    [articleID]     INT NOT NULL,
    [artPackID]     INT NOT NULL,
    [barCode]       VARCHAR(32) NULL,
    [r_Edit]        ROWVERSION NOT NULL,
    CONSTRAINT [PK_s4_articleList_packing_barcodes] PRIMARY KEY CLUSTERED ([artPackBarCodeID] ASC),
    CONSTRAINT [FK_s4_articleList_packing_barcodes_s4_articleList_packings] FOREIGN KEY ([artPackID]) REFERENCES [dbo].[s4_articleList_packings] ([artPackID])
);

GO
CREATE NONCLUSTERED INDEX [IX_s4_articleList_packing_barcodes]
    ON [dbo].[s4_articleList_packing_barcodes]([barCode] ASC) WITH (FILLFACTOR = 90);

GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_UQ_s4_articleList_packing_barcodes]
    ON [dbo].[s4_articleList_packing_barcodes]([barCode] ASC);
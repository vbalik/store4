﻿CREATE TABLE [dbo].[s4_messageBus_document]
(
	[messageBusDocumentID] INT NOT NULL PRIMARY KEY IDENTITY,
    [messageBusID] INT NOT NULL,
    [documentName] VARCHAR(512) NOT NULL,
    [mimeType] VARCHAR(256) NOT NULL,
    [document] VARBINARY(MAX) NOT NULL, 
    [entryDateTime] DATETIME NOT NULL, 
    [r_Edit] ROWVERSION NOT NULL, 
    CONSTRAINT [FK_s4_messageBus_document_s4_messageBus] FOREIGN KEY ([messageBusID]) REFERENCES [s4_messageBus](messageBusID)
)

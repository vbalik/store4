﻿CREATE TABLE [dbo].[s4_positionList_history]
(
	[positionHistID] [dbo].[s4_type_docHistoryID] IDENTITY (1, 1) NOT NULL,
    [positionID] INT NOT NULL, 
    [eventCode] [dbo].[s4_type_status] NOT NULL, 
    [eventData] NVARCHAR(MAX) NULL, 
    [entryDateTime] DATETIME CONSTRAINT [DF_s4_positionList_history_entryDateTime] DEFAULT (getdate()) NOT NULL,
    [entryUserID] [dbo].[s4_type_userID] NOT NULL, 
    [r_Edit] ROWVERSION NOT NULL, 
    CONSTRAINT [FK_s4_positionList_history_s4_positionList] FOREIGN KEY ([positionID]) REFERENCES [dbo].[s4_positionList] ([positionID]), 
    CONSTRAINT [PK_s4_positionList_history] PRIMARY KEY ([positionHistID])
)

GO
CREATE NONCLUSTERED INDEX [IX_s4_positionList_history_positionID]
    ON [dbo].[s4_positionList_history]([positionID] ASC) WITH (FILLFACTOR = 90);

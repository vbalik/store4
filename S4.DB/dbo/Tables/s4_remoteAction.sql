﻿CREATE TABLE [dbo].[s4_remoteAction]
(
	[raID] INT NOT NULL PRIMARY KEY IDENTITY,
    [runStatus] [dbo].[s4_type_status] NOT NULL,
	[actionType] VARCHAR(32) NOT NULL,
	[actionParams] NVARCHAR(MAX) NOT NULL,
	[sourceUserID] [dbo].[s4_type_userID] NOT NULL,
	[sourceDeviceID] VARCHAR(32) NOT NULL,
	[actionDate] DATETIME NOT NULL,
	[received] DATETIME NOT NULL,
	[completed] DATETIME NULL,
	[errorDesc] NVARCHAR(MAX) NULL,
	[r_Edit] ROWVERSION NOT NULL
)

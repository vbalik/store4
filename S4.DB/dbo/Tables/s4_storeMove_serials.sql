﻿CREATE TABLE [dbo].[s4_storeMove_serials]
(
	[serialID] INT NOT NULL PRIMARY KEY IDENTITY, 
    [stoMoveItemID] [dbo].[s4_type_docItemID] NOT NULL, 
    [serialNumber] NVARCHAR(64) NOT NULL, 
    [r_Edit] ROWVERSION NOT NULL, 
    CONSTRAINT [FK_s4_storeMove_serials_s4_storeMove_items] FOREIGN KEY ([stoMoveItemID]) REFERENCES [s4_storeMove_items]([stoMoveItemID])
)

GO

CREATE INDEX [IX_s4_storeMove_serials_stoMoveItemID] ON [dbo].[s4_storeMove_serials] ([stoMoveItemID])

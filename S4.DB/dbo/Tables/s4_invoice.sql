﻿CREATE TABLE [dbo].[s4_invoice]
(
	[invoiceID] INT IDENTITY (1, 1) NOT NULL,
	[docStatus]           [dbo].[s4_type_status]      NOT NULL,
	[docNumber] INT NOT NULL,
	[docPrefix] VARCHAR(16) NOT NULL,
	[docYear]   CHAR(2) NOT NULL,
	[lastDateTime] DATETIME CONSTRAINT [DF_s4_invoice_lastDateTime] DEFAULT (getdate()) NOT NULL,
    [entryDateTime] DATETIME CONSTRAINT [DF_s4_invoice_entryDateTime] DEFAULT (getdate()) NOT NULL,
    [r_Edit] ROWVERSION NOT NULL, 
    CONSTRAINT [PK_s4_invoice] PRIMARY KEY CLUSTERED ([invoiceID] ASC) WITH (FILLFACTOR = 90)
)

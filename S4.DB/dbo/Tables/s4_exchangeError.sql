﻿CREATE TABLE [dbo].[s4_exchangeError]
(
	[exchangeErrorID] INT IDENTITY NOT NULL, 
    [appVersion] NVARCHAR(64) NOT NULL, 
    [errorClass] NVARCHAR(128) NOT NULL, 
    [errorDateTime] DATETIME NOT NULL, 
    [errorMessage] NVARCHAR(MAX) NOT NULL, 
    [jobClass] NVARCHAR(128) NULL, 
    [jobName] NVARCHAR(128) NULL, 
    [r_Edit] ROWVERSION NOT NULL,
    CONSTRAINT [PK_s4_exchangeError] PRIMARY KEY CLUSTERED ([exchangeErrorID] ASC) WITH (FILLFACTOR = 90)
)

GO
CREATE NONCLUSTERED INDEX [IX_s4_exchangeError_exchangeErrorID]
    ON [dbo].[s4_exchangeError]([exchangeErrorID] ASC) WITH (FILLFACTOR = 90);
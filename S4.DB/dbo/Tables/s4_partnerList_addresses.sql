﻿CREATE TABLE [dbo].[s4_partnerList_addresses] (
    [partAddrID]   INT           IDENTITY (1, 1) NOT NULL,
    [partnerID]    INT           NOT NULL,
    [addrName]     NVARCHAR (256)  NOT NULL,
    [source_id]    [dbo].[s4_type_sourceID]  NOT NULL,
    [addrLine_1]   NVARCHAR (256)  NULL,
    [addrLine_2]   NVARCHAR (256)  NULL,
    [addrCity]     NVARCHAR (128)  NULL,
    [addrPostCode] NVARCHAR (32)  NULL,
    [addrPhone]    NVARCHAR (128)  NULL,
    [addrRemark]   NVARCHAR (MAX) NULL,
    [deliveryInst] NVARCHAR (256) NULL,
    [r_Edit]       ROWVERSION    NOT NULL,
    [addrInfo]     AS            ([addrName] + isnull((', ' + [addrCity]),'')),
    [addrInfo2]     AS            ([addrName] + isnull((', ' + [deliveryInst]),'')),
    CONSTRAINT [PK_s4_partnerList_addresses] PRIMARY KEY CLUSTERED ([partAddrID] ASC),
    CONSTRAINT [FK_s4_partnerList_addresses_s4_partnerList] FOREIGN KEY ([partnerID]) REFERENCES [dbo].[s4_partnerList] ([partnerID])
);


GO
CREATE NONCLUSTERED INDEX [IX_s4_partnerList_addresses_partnerID]
    ON [dbo].[s4_partnerList_addresses]([partnerID] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_s4_partnerList_addresses_source_id]
    ON [dbo].[s4_partnerList_addresses]([source_id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_s4_partnerList_addresses_deliveryInst]
    ON [dbo].[s4_partnerList_addresses]([deliveryInst] ASC);
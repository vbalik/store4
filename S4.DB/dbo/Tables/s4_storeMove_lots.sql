﻿CREATE TABLE [dbo].[s4_storeMove_lots]
(
	[stoMoveLotID]      [dbo].[s4_type_docItemID]  IDENTITY (1, 1) NOT NULL,	
    [artPackID]          INT                        NOT NULL,
    [batchNum]           [dbo].[s4_type_batchNum]   NOT NULL,
    [expirationDate]     SMALLDATETIME              NOT NULL,
    [udiCode]            VARCHAR(256)               NULL, 
    CONSTRAINT [PK_s4_storeMove_lots] PRIMARY KEY CLUSTERED ([stoMoveLotID] ASC),
	CONSTRAINT [FK_s4_storeMove_lots_s4_articleList_packings] FOREIGN KEY ([artPackID]) REFERENCES [dbo].[s4_articleList_packings] ([artPackID]),
);
GO


CREATE TRIGGER [dbo].s4_storeMove_lots_update
    ON [dbo].[s4_storeMove_lots]
    FOR UPDATE
    AS
    BEGIN
		RAISERROR ('Store4: Can''t change items of s4_storeMove_lots', 16, 1)
		ROLLBACK TRANSACTION
    END
GO

CREATE TRIGGER [dbo].s4_storeMove_lots_delete
    ON [dbo].[s4_storeMove_lots]
    FOR DELETE
    AS
    BEGIN
		RAISERROR ('Store4: Can''t delete items of s4_storeMove_lots', 16, 1)
		ROLLBACK TRANSACTION
    END
GO

CREATE UNIQUE INDEX [IX_s4_storeMove_lots_artPackID_batchNum_expirationDate] ON [dbo].[s4_storeMove_lots] ([artPackID], [batchNum], [expirationDate])

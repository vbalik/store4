﻿CREATE TABLE [dbo].[s4_clientError]
(
	[clientErrorID] INT IDENTITY (1, 1) NOT NULL,
    [appVersion] NVARCHAR(64) NOT NULL, 
    [errorClass] NVARCHAR(128) NOT NULL, 
    [errorDateTime] DATETIME NOT NULL, 
    [errorMessage] NVARCHAR(MAX) NOT NULL, 
	[path] NVARCHAR(256) NOT NULL, 
    [statusData] NVARCHAR(MAX) NULL,
	[requestBody] NVARCHAR(MAX) NULL,
	[r_Edit]         ROWVERSION                 NOT NULL,
	CONSTRAINT [PK_s4_clientError] PRIMARY KEY CLUSTERED ([clientErrorID] ASC) WITH (FILLFACTOR = 90)
);

GO
CREATE NONCLUSTERED INDEX [IX_s4_clientError_clientErrorID]
    ON [dbo].[s4_clientError]([clientErrorID] ASC) WITH (FILLFACTOR = 90);

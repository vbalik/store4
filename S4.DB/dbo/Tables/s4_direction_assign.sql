﻿CREATE TABLE [dbo].[s4_direction_assign] (
    [directAssignID] INT                        IDENTITY (1, 1) NOT NULL,
    [directionID]    [dbo].[s4_type_documentID] NOT NULL,
    [jobID]          CHAR (4)                   NOT NULL,
    [workerID]       [dbo].[s4_type_userID]     NOT NULL,
    [assignParams]   VARCHAR (MAX)               NULL,
    [entryDateTime]  DATETIME                   CONSTRAINT [DF_s4_direction_assign_entryDateTime] DEFAULT (getdate()) NOT NULL,
    [entryUserID]    [dbo].[s4_type_userID]     NOT NULL,
    [r_Edit]         ROWVERSION                 NOT NULL,
    CONSTRAINT [PK_s4_direction_assign] PRIMARY KEY CLUSTERED ([directAssignID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_s4_direction_assign_s4_direction] FOREIGN KEY ([directionID]) REFERENCES [dbo].[s4_direction] ([directionID]),
    CONSTRAINT [FK_s4_direction_assign_s4_workerList] FOREIGN KEY ([workerID]) REFERENCES [dbo].[s4_workerList] ([workerID])
);


GO
CREATE NONCLUSTERED INDEX [IX_s4_direction_assign_directionID]
    ON [dbo].[s4_direction_assign]([directionID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_s4_direction_assign_directionID_jobID_workerID]
    ON [dbo].[s4_direction_assign]([directionID] ASC, [jobID] ASC, [workerID] ASC);


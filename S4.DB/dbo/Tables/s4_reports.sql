﻿CREATE TABLE [dbo].[s4_reports]
(
	[reportID] INT NOT NULL PRIMARY KEY IDENTITY(1, 1), 
    [reportType] VARCHAR(32) NOT NULL, 
	[template] NVARCHAR(MAX) NOT NULL,
    [settings] NVARCHAR(MAX) NULL, 
    [printerTypeID] INT NOT NULL, 
    [r_Edit] ROWVERSION NOT NULL, 
    CONSTRAINT [FK_s4_reports_PrinterType] FOREIGN KEY ([printerTypeID]) REFERENCES [dbo].[s4_printerType]([printerTypeID])
)

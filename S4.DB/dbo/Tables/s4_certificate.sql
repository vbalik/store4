﻿CREATE TABLE [dbo].[s4_certificate]
(
	[certificateID] INT IDENTITY (1, 1) NOT NULL, 
    [certificateType] VARCHAR(50) NOT NULL, 
    [expirationDateFrom] DATETIME NOT NULL, 
    [expirationDateTo] DATETIME NOT NULL, 
    [certificateBinary] VARBINARY(MAX) NOT NULL, 
    [certificatePassword] VARCHAR(100) NULL, 
    [r_Edit] ROWVERSION NOT NULL
)

﻿CREATE TABLE [dbo].[s4_truckList] (
    [truckID]       INT          IDENTITY (1, 1) NOT NULL,
    [truckRegist]   VARCHAR (32) NOT NULL,
    [truckCapacity] DECIMAL (18) CONSTRAINT [DF_s4_truckList_truckCapacity] DEFAULT ((-1)) NOT NULL,
    [truckComment]  NVARCHAR(MAX)         NULL,
	[truckRegion]     TINYINT        NOT NULL DEFAULT 0,
    [r_Edit]        ROWVERSION   NOT NULL,
    CONSTRAINT [PK_s4_truckList] PRIMARY KEY CLUSTERED ([truckID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_s4_truckList_truckRegist]
    ON [dbo].[s4_truckList]([truckRegist] ASC);


﻿CREATE TABLE [dbo].[s4_stocktaking] (
    [stotakID]      [dbo].[s4_type_documentID]  IDENTITY (1, 1) NOT NULL,
    [stotakStatus]  [dbo].[s4_type_status]      NOT NULL,
    [stotakDesc]    NVARCHAR (MAX)              NOT NULL,
    [beginDate]     DATETIME                    NOT NULL,
    [endDate]       DATETIME                    NULL,
    [entryDateTime] DATETIME                    CONSTRAINT [DF_s4_stocktaking_entryDateTime] DEFAULT (getdate()) NOT NULL,
    [entryUserID]   [dbo].[s4_type_userID]      NOT NULL,
    [r_Edit]        ROWVERSION                  NOT NULL,
    CONSTRAINT [PK_s4_stocktaking] PRIMARY KEY CLUSTERED ([stotakID] ASC)
);


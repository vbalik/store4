﻿CREATE TABLE [dbo].[s4_direction_items] (
    [directionItemID] [dbo].[s4_type_docItemID]   IDENTITY (1, 1) NOT NULL,
    [directionID]     [dbo].[s4_type_documentID]  NOT NULL,
    [docPosition]     INT                         NOT NULL,
    [itemStatus]      [dbo].[s4_type_status]      NOT NULL,
    [artPackID]       INT                         NOT NULL,
    [articleCode]     VARCHAR (64)                NOT NULL,
    [unitDesc]        NVARCHAR(32)                NULL,
    [quantity]        NUMERIC (18, 4)             NOT NULL,
    [unitPrice]       MONEY                       NOT NULL,
    [partnerDepart]   VARCHAR (MAX)                NULL,
    [entryDateTime]   DATETIME                    CONSTRAINT [DF_s4_direction_items_entryDateTime] DEFAULT (getdate()) NOT NULL,
    [entryUserID]     [dbo].[s4_type_userID]      NOT NULL,
    [r_Edit]          ROWVERSION                  NOT NULL,
    [price]           AS                          ([unitPrice] * [quantity]),
    CONSTRAINT [PK_s4_direction_items] PRIMARY KEY CLUSTERED ([directionItemID] ASC),
    CONSTRAINT [FK_s4_direction_items_s4_articleList_packings] FOREIGN KEY ([artPackID]) REFERENCES [dbo].[s4_articleList_packings] ([artPackID]),
    CONSTRAINT [FK_s4_direction_items_s4_direction] FOREIGN KEY ([directionID]) REFERENCES [dbo].[s4_direction] ([directionID]),
    CONSTRAINT [CK_s4_direction_items] CHECK ([docPosition] <> 0)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_s4_direction_items]
    ON [dbo].[s4_direction_items]([directionID] ASC, [docPosition] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_s4_direction_items_directionID]
    ON [dbo].[s4_direction_items]([directionID] ASC);


﻿CREATE TABLE [dbo].[s4_direction_history] (
    [directionHistID] [dbo].[s4_type_docHistoryID] IDENTITY (1, 1) NOT NULL,
    [directionID]     [dbo].[s4_type_documentID]   NOT NULL,
    [docPosition]     INT                          NOT NULL,
    [eventCode]       [dbo].[s4_type_status]       NOT NULL,
    [eventData]     NVARCHAR (MAX)                NULL,
    [entryDateTime]   DATETIME                     CONSTRAINT [DF_s4_direction_history_entryDateTime] DEFAULT (getdate()) NOT NULL,
    [entryUserID]     [dbo].[s4_type_userID]       NOT NULL,
    [r_Edit]          ROWVERSION                   NOT NULL,
    CONSTRAINT [PK_s4_direction_history] PRIMARY KEY CLUSTERED ([directionHistID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_s4_direction_history_s4_direction] FOREIGN KEY ([directionID]) REFERENCES [dbo].[s4_direction] ([directionID])
);


GO
CREATE NONCLUSTERED INDEX [IX_s4_direction_history_position]
    ON [dbo].[s4_direction_history]([directionID] ASC, [docPosition] ASC) WITH (FILLFACTOR = 90);


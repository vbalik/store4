﻿CREATE TABLE [dbo].[s4_articleList_history] (
    [articleHistID] [dbo].[s4_type_docHistoryID] IDENTITY (1, 1) NOT NULL,
    [articleID]     INT                          NOT NULL,
    [artPackID]     INT                          CONSTRAINT [DF_s4_articleList_history_artPackID] DEFAULT ((-1)) NOT NULL,
    [eventCode]     [dbo].[s4_type_status]       NOT NULL,
    [eventData]     NVARCHAR (MAX)                NULL,
    [entryDateTime] DATETIME                     CONSTRAINT [DF_s4_articleList_history_entryDateTime] DEFAULT (getdate()) NOT NULL,
    [entryUserID]   [dbo].[s4_type_userID]       NOT NULL,
    [r_Edit]        ROWVERSION                   NOT NULL,
    CONSTRAINT [PK_s4_articleList_history] PRIMARY KEY CLUSTERED ([articleHistID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_s4_articleList_history_s4_articleList] FOREIGN KEY ([articleID]) REFERENCES [dbo].[s4_articleList] ([articleID])
);


GO
CREATE NONCLUSTERED INDEX [IX_s4_articleList_history_articleID]
    ON [dbo].[s4_articleList_history]([articleID] ASC) WITH (FILLFACTOR = 90);


﻿CREATE TABLE [dbo].[s4_invoice_xtra]
(
	[invoiceExID] INT IDENTITY (1, 1) NOT NULL,
	[invoiceID]   INT NOT NULL,
	[xtraCode]    [dbo].[s4_type_xtraCode]   NOT NULL,
	[xtraValue]   [dbo].[s4_type_xtraValue]  NOT NULL,
	[r_Edit]      ROWVERSION                 NOT NULL,
	[xtraValue_checksum]  AS (checksum([xtraValue])) PERSISTED,
    CONSTRAINT [PK_s4_invoice_xtra] PRIMARY KEY CLUSTERED ([invoiceExID] ASC),
    CONSTRAINT [FK_s4_invoice_xtra_s4_invoice] FOREIGN KEY ([invoiceID]) REFERENCES [dbo].[s4_invoice] ([invoiceID])
);

GO
CREATE NONCLUSTERED INDEX [IX_s4_invoice_xtra_invoiceID_xtraCode]
    ON [dbo].[s4_invoice_xtra]([invoiceID] ASC, [xtraCode] ASC);

GO
CREATE NONCLUSTERED INDEX [IX_s4_invoice_xtra_xtraValue_checksum] 
    ON [dbo].[s4_invoice_xtra] ([xtraValue_checksum] ASC)
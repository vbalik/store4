﻿CREATE TABLE [dbo].[s4_manufactList] (
    [manufID]           [dbo].[s4_type_manufID]       NOT NULL,
    [manufName]         NVARCHAR (64)   NOT NULL,
    [manufStatus]       [dbo].[s4_type_status]        CONSTRAINT [DF_s4_manufactList_manufStatus] DEFAULT ((0)) NOT NULL,
    [manufSettings]     VARCHAR (MAX) NULL,
    [source_id]         [dbo].[s4_type_sourceID]   NULL,
    [useBatch]      BIT            CONSTRAINT [DF_s4_manufactList_useBatch] DEFAULT ((0)) NOT NULL,
    [useExpiration] BIT            CONSTRAINT [DF_s4_manufactList_useExpiration] DEFAULT ((0)) NOT NULL,    
    [sectID]        [dbo].[s4_type_sectID] NULL,
    [r_Edit]            ROWVERSION     NOT NULL,
    [manufSign] NVARCHAR(32) NULL, 
    CONSTRAINT [PK_s4_manufactList] PRIMARY KEY CLUSTERED ([manufID] ASC),
    CONSTRAINT [FK_s4_manufactList_s4_sectionList] FOREIGN KEY ([sectID]) REFERENCES [dbo].[s4_sectionList] ([sectID])
);


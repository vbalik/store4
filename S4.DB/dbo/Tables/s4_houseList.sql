﻿CREATE TABLE [dbo].[s4_houseList] (
    [houseID]   VARCHAR(16)     NOT NULL ,
    [houseDesc] NVARCHAR(MAX) NOT NULL,
    [houseMap]  IMAGE        NULL,
    [r_Edit]    ROWVERSION   NOT NULL,
    CONSTRAINT [PK_s4_houseList] PRIMARY KEY CLUSTERED ([houseID] ASC)
);


﻿CREATE TABLE [dbo].[s4_storeMove] (
    [stoMoveID]          [dbo].[s4_type_documentID] IDENTITY (1, 1) NOT NULL,
    [docStatus]          [dbo].[s4_type_status]     NOT NULL,
    [docNumPrefix]       VARCHAR(16)                  NOT NULL,
    [docType]            TINYINT                    NOT NULL,
    [docNumber]          INT                        NOT NULL,
    [docDate]            DATETIME                   NOT NULL,
    [parent_directionID] [dbo].[s4_type_documentID] CONSTRAINT [DF_s4_storeMove_parent_stoDocID] DEFAULT ((-1)) NOT NULL,
    [docRemark]          NVARCHAR(MAX) NULL,
    [entryDateTime]      DATETIME                   CONSTRAINT [DF_s4_storeMove_entryDateTime] DEFAULT (getdate()) NOT NULL,
    [entryUserID]        [dbo].[s4_type_userID]     NOT NULL,
    [r_Edit]             ROWVERSION                 NOT NULL,
    [docDate_d]          AS                         (convert(datetime,convert(int,convert(float,[docDate])))),
    [docDate_p]          AS                         (datepart(year,[docDate]) * 100 + datepart(month,[docDate])),
    [docInfo]            AS                         (rtrim([docNumPrefix]) + '/' + convert(varchar(16),[docNumber])),
    [docRemark_prev]     AS                         (convert(varchar(64),[docRemark]) + case when (datalength([docRemark]) > 64) then '...' else '' end),
    CONSTRAINT [PK_s4_storeMove] PRIMARY KEY CLUSTERED ([stoMoveID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_s4_storeMove_docNumPrefix_docNumber]
    ON [dbo].[s4_storeMove]([docNumPrefix] ASC, [docNumber] DESC);


GO
CREATE NONCLUSTERED INDEX [IX_s4_storeMove_docType_docNumber]
    ON [dbo].[s4_storeMove]([docType] ASC, [docNumber] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_s4_storeMove_parent_directionID]
    ON [dbo].[s4_storeMove]([parent_directionID] ASC);


GO

CREATE INDEX [IX_s4_storeMove_docType] ON [dbo].[s4_storeMove] ([docType])

GO

CREATE INDEX [IX_s4_storeMove_docNumPrefix]
    ON [dbo].[s4_storeMove]([docNumPrefix] ASC);

GO
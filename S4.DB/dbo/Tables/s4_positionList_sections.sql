﻿CREATE TABLE [dbo].[s4_positionList_sections] (
    [posSectID]  INT        IDENTITY (1, 1) NOT NULL,
    [sectID]     [dbo].[s4_type_sectID]   NOT NULL,
    [positionID] INT        NOT NULL,
    [r_Edit]     ROWVERSION NOT NULL,
    CONSTRAINT [PK_s4_positionList_sections] PRIMARY KEY CLUSTERED ([posSectID] ASC),
    CONSTRAINT [FK_s4_positionList_sections_s4_positionList] FOREIGN KEY ([positionID]) REFERENCES [dbo].[s4_positionList] ([positionID]),
    CONSTRAINT [FK_s4_positionList_sections_s4_sectionList] FOREIGN KEY ([sectID]) REFERENCES [dbo].[s4_sectionList] ([sectID]),
	CONSTRAINT [UN_s4_positionList_sections_sectID_positionID] UNIQUE NONCLUSTERED ([sectID], [positionID])
);
GO

CREATE INDEX [IX_s4_positionList_sections_sectID] ON [dbo].[s4_positionList_sections] ([sectID])

﻿CREATE TABLE [dbo].[s4_storeMove_history] (
    [stoMoveHistID] [dbo].[s4_type_docHistoryID] IDENTITY (1, 1) NOT NULL,
    [stoMoveID]     [dbo].[s4_type_documentID]   NOT NULL,
    [docPosition]   INT                          NOT NULL,
	[eventCode]       [dbo].[s4_type_status]       NOT NULL,
	[eventData]     NVARCHAR (MAX)                NULL,
    [entryDateTime] DATETIME                     CONSTRAINT [DF_s4_storeMove_history_entryDateTime] DEFAULT (getdate()) NOT NULL,
    [entryUserID]   [dbo].[s4_type_userID]       NOT NULL,
    [r_Edit]        ROWVERSION                   NOT NULL,
    CONSTRAINT [PK_s4_storeMove_history] PRIMARY KEY CLUSTERED ([stoMoveHistID] ASC),
    CONSTRAINT [FK_s4_storeMove_history_s4_storeMove] FOREIGN KEY ([stoMoveID]) REFERENCES [dbo].[s4_storeMove] ([stoMoveID])
);


GO
CREATE NONCLUSTERED INDEX [IX_s4_storeMove_history_position]
    ON [dbo].[s4_storeMove_history]([stoMoveID] ASC, [docPosition] ASC);


﻿CREATE VIEW [dbo].[s4_vw_article_base]
	AS 
	SELECT
		ar.articleID, articleCode, articleDesc, articleStatus, articleType, ar.source_id, sectID, unitDesc, manufID, useBatch, mixBatch, useExpiration, articleInfo,
		artPackID, packDesc, packRelation, movablePack, packStatus, barCodeType, barCode, inclCarrier, packWeight, packVolume, packInfo, specFeatures, userRemark, useSerialNumber, ar.brand, ar.sign
	FROM 
		[dbo].[s4_articleList] ar
		JOIN [dbo].[s4_articleList_packings] ap ON (ar.articleID = ap.articleID)

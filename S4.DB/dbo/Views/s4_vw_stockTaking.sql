﻿CREATE VIEW [dbo].[s4_vw_stockTaking]
AS
SELECT        dbo.s4_stocktaking.*,
items = (SELECT COUNT(stotakItemID) FROM s4_stocktaking_items WHERE stotakID = s4_stocktaking.stotakID),
checked = (SELECT COUNT(stotakPosID) FROM s4_stocktaking_positions WHERE stotakID = s4_stocktaking.stotakID AND checkCounter > 0),
nonChecked = (SELECT COUNT(stotakPosID) FROM s4_stocktaking_positions WHERE stotakID = s4_stocktaking.stotakID AND checkCounter = 0)
FROM            dbo.s4_stocktaking

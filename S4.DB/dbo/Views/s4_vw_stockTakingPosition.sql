﻿CREATE VIEW s4_vw_stockTakingPosition
AS
SELECT SP.[stotakPosID]
      ,SP.[stotakID]
      ,SP.[positionID]
      ,SP.[checkCounter]
      ,SP.[r_Edit]
	  ,P.[posCode]
	  ,P.[houseID]
  FROM [dbo].[s4_stocktaking_positions] SP
  LEFT OUTER JOIN [dbo].[s4_positionList] P ON P.positionID = SP.positionID

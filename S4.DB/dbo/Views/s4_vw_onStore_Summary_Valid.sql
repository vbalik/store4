﻿CREATE VIEW [dbo].[s4_vw_onStore_Summary_Valid]
AS 

WITH onStore ([stoMoveLotID], [positionID], [carrierNum], [quantity])
AS 
(
	SELECT
		[stoMoveLotID], [positionID], [carrierNum], SUM([quantity]) AS [quantity]
	FROM
		(
			SELECT
				[stoMoveLotID], 
				[positionID], 
				[carrierNum], 
				[quantity]
			FROM	[dbo].[s4_onStoreSummary_items]
			WHERE	[onStoreSummID] = (SELECT MAX([onStoreSummID]) FROM [dbo].[s4_onStoreSummary] WHERE [onStoreSummStatus] = 100)

			UNION ALL
		
			SELECT
				[stoMoveLotID],
				[positionID],
				[carrierNum],
				SUM([quantity] * [itemDirection]) AS [quantity]
			FROM	[dbo].[s4_storeMove_items]
			WHERE	[itemValidity] = 100
			AND		[stoMoveItemID] > ISNULL((SELECT MAX([lastStoMoveItemID]) FROM [dbo].[s4_onStoreSummary] WHERE [onStoreSummStatus] = 100), 0)
			GROUP BY
				[stoMoveLotID],
				[positionID],
				[carrierNum]
			HAVING
				SUM([quantity] * [itemDirection]) <> 0
		) un
	GROUP BY
		[stoMoveLotID],
		[positionID],
		[carrierNum]
	HAVING 
		SUM([quantity]) <> 0
)

SELECT
	onStore.positionID,
	onStore.carrierNum,
	onStore.quantity,
	lots.stoMoveLotID,
	lots.artPackID,
	lots.batchNum,
	lots.expirationDate
FROM	onStore
		JOIN [dbo].[s4_storeMove_lots] lots ON (onStore.stoMoveLotID = lots.stoMoveLotID)

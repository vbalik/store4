﻿CREATE VIEW s4_vw_stockTakingSnapshot
AS
SELECT S.*, POS.posCode, A.articleCode, A.articleDesc FROM s4_stocktaking_snapshots S
LEFT OUTER JOIN s4_positionList POS ON POS.positionID = S.positionID
LEFT OUTER JOIN s4_articleList_packings P ON P.artPackID = S.artPackID
LEFT OUTER JOIN s4_articleList A ON A.articleID = P.articleID

﻿CREATE VIEW [dbo].[prom_vw_moveReportBase2]
AS

SELECT
	dir.docDate_p,
	ar.source_id AS art_source_id,
	pa.source_id AS partner_source_id,
	lot.batchNum,
	CASE mo.docType WHEN 5 THEN 1 WHEN 3 THEN -1 END AS moveSign,
	SUM(quantity) AS quantity
FROM	dbo.s4_storeMove mo
		JOIN dbo.s4_storeMove_Items moi ON mo.stoMoveID = moi.stoMoveID
		JOIN dbo.s4_storeMove_lots lot ON moi.stoMoveLotID = lot.stoMoveLotID
		JOIN dbo.s4_vw_article_Base ar ON (lot.artPackID = ar.artPackID)
		JOIN dbo.s4_direction dir ON (mo.parent_directionID = dir.directionID)
		JOIN dbo.s4_partnerList pa ON (dir.partnerID = pa.[partnerID])
WHERE	
	(
		(
			(mo.docType = 5) 
			AND (moi.itemDirection = -1) 
			AND (moi.itemValidity = 100)
			--AND (mo.docStatus != 'CANC')
			AND (mo.parent_directionID IN (SELECT directionID FROM dbo.s4_direction WHERE docNumPrefix <> 'DLL'))
		)
		OR 
		(
			(mo.docType = 3) 
			AND (moi.itemDirection = 1) 
			AND (moi.itemValidity = 100)
			--AND (mo.docStatus != 'CANC') 
			AND (mo.parent_directionID IN (SELECT directionID FROM dbo.s4_direction WHERE docNumPrefix = 'VRAT'))
		)
	)
GROUP BY
	dir.docDate_p,
	ar.source_id,
	pa.source_id,
	lot.batchNum,
	CASE mo.docType WHEN 5 THEN 1 WHEN 3 THEN -1 END

GO



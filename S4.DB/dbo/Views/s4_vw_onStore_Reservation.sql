﻿CREATE VIEW [dbo].[s4_vw_onStore_Reservation]
AS 
SELECT
	onStore.positionID,
	onStore.carrierNum,
	onStore.quantity,
	lots.stoMoveLotID,
	lots.artPackID,
	lots.batchNum,
	lots.expirationDate
FROM
(
	SELECT
		[stoMoveLotID],
		[positionID],
		[carrierNum],
		SUM([quantity] * [itemDirection]) AS [quantity]
	FROM	[dbo].[s4_storeMove_items]
	WHERE	[itemValidity] = 0
	GROUP BY
		[stoMoveLotID],
		[positionID],
		[carrierNum]
	HAVING
		SUM([quantity] * [itemDirection]) <> 0
) onStore
JOIN [dbo].[s4_storeMove_lots] lots ON (onStore.stoMoveLotID = lots.stoMoveLotID)

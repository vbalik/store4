﻿CREATE VIEW s4_vw_stockTakingHistory
AS
SELECT H.*, P.posCode, A.articleID, A.articleCode, A.articleDesc FROM s4_stocktaking_history H
LEFT OUTER JOIN s4_positionList P ON P.positionID = H.positionID
LEFT OUTER JOIN s4_vw_article_base A ON A.artPackID = H.artPackID

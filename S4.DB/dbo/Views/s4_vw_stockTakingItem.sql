﻿CREATE VIEW s4_vw_stockTakingItem
AS
SELECT I.*, POS.posCode, A.articleCode, A.articleDesc, A.articleID FROM s4_stocktaking_items I
LEFT OUTER JOIN s4_positionList POS ON POS.positionID = I.positionID
LEFT OUTER JOIN s4_articleList_packings P ON P.artPackID = I.artPackID
LEFT OUTER JOIN s4_articleList A ON A.articleID = P.articleID

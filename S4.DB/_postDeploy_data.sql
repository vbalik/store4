﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/


--Tyto Inserty musí jít do ostré DB START
--Reference Data for s4_partnerList
SET IDENTITY_INSERT [s4_partnerList] ON;
INSERT INTO [dbo].[s4_partnerList] ([partnerID], [partnerName], [source_id], [partnerStatus], [partnerDispCheck], [partnerAddr_1], [partnerAddr_2], [partnerCity], [partnerPostCode], [partnerPhone])
     VALUES (0, 'Prázdný', 'Prázdný', 'PA_OK', 123, 'Prázdný', 'Prázdný', 'Prázdný', 'Prázdný', 'Prázdný')
SET IDENTITY_INSERT [s4_partnerList] OFF;
--Tyto Inserty musí jít do ostré DB END


--Reference Data for s4_truckList
SET IDENTITY_INSERT [s4_truckList] ON;
MERGE INTO [s4_truckList] AS Target
USING (VALUES
(1,	'1AZ9824',	16,	'M.B.1223, Morava'),
(2,	'4A89654',	14,	'M.B.815, Morava'),
(3,	'ALC3862',	15,	'M.B.1828, Morava'),
(4,	'6A74016',	14,	'M.B.815, Morava'),
(5,	'2A25350',	16,	'M.B.1828, Čechy'),
(6,	'5A15994',	16,	'M.B.1223, Čechy')
)
AS Source (truckID, truckRegist, truckCapacity, truckComment)
ON Target.truckID = Source.truckID
--update matched rows
WHEN MATCHED THEN
UPDATE SET truckRegist = Source.truckRegist, truckCapacity = Source.truckCapacity, truckComment = Source.truckComment
--insert new rows
WHEN NOT MATCHED BY TARGET THEN
INSERT (truckID, truckRegist, truckCapacity, truckComment)
VALUES (truckID, truckRegist, truckCapacity, truckComment)
--delete rows that are in the target but not the source
WHEN NOT MATCHED BY SOURCE THEN
DELETE
;
SET IDENTITY_INSERT [s4_truckList] OFF;

SET IDENTITY_INSERT [s4_partnerList] ON;
INSERT INTO [dbo].[s4_partnerList] ([partnerID], [partnerName], [source_id], [partnerStatus], [partnerDispCheck], [partnerAddr_1], [partnerAddr_2], [partnerCity], [partnerPostCode], [partnerPhone])
     VALUES (1, 'Partner name1', 'xxx', 'PA_OK', 123, 'Adresa1', 'Adresa2', 'Praha', '11150', '123')
--ReceiveChangePartnerProcTest_Success
INSERT INTO [dbo].[s4_partnerList] ([partnerID], [partnerName], [source_id], [partnerStatus], [partnerDispCheck], [partnerAddr_1], [partnerAddr_2], [partnerCity], [partnerPostCode], [partnerPhone])
     VALUES (2, 'Partner receive test', 'rec1', 'PA_OK', 123, 'Adresa1', 'Adresa2', 'Praha', '11150', '123')

SET IDENTITY_INSERT [s4_partnerList] OFF;

--Reference Data for s4_manufactList
INSERT INTO [s4_manufactList] ([manufID],[manufName],[manufStatus],[manufSettings],[source_id],[useBatch],[useExpiration],[sectID],[manufSign])
VALUES ('ONTX_123', 'Ontex', 'MAN_OK', NULL, NULL, 0, 0, NULL, 'fab fa-apple');

INSERT INTO [s4_manufactList] ([manufID],[manufName],[manufStatus],[manufSettings],[source_id],[useBatch],[useExpiration],[sectID])
VALUES ('SMIT_123', 'Smith&Nephew', 'MAN_OK', NULL, 'Smith&Nephew', 0, 0, NULL);

INSERT INTO [s4_manufactList] ([manufID],[manufName],[manufStatus],[manufSettings],[source_id],[useBatch],[useExpiration],[sectID])
VALUES ('ALFA_123', 'Alfa Vita', 'MAN_OK', NULL, 'Alfa Vita', 0, 0, NULL);

--Reference Data for [s4_workerList]
INSERT INTO [dbo].[s4_workerList] ([workerID],[workerName],[workerClass],[workerStatus],[workerLogon],[workerSecData])
VALUES ('Test1', 'Worker1', 255, 'ONDUTY', 'Test', 'Test');
INSERT INTO [dbo].[s4_workerList] ([workerID],[workerName],[workerClass],[workerStatus],[workerLogon],[workerSecData])
VALUES ('WRK1', 'Worker 2', 255, 'ONDUTY', 'worker2', 'Test');
INSERT INTO [dbo].[s4_workerList] ([workerID],[workerName],[workerClass],[workerStatus],[workerLogon],[workerSecData])
VALUES ('WRK_OnDutyTest', 'WRK_OnDutyTest', 255, 'HOLIDAY', 'WRK_OnDutyTest', 'Test');
INSERT INTO [dbo].[s4_workerList] ([workerID],[workerName],[workerClass],[workerStatus],[workerLogon],[workerSecData])
VALUES ('WRK_OrderTest', 'WRK_SetOutOfOrderTest', 255, 'ONDUTY', 'WRK_SetOutOfOrderTest', 'Test');
INSERT INTO [dbo].[s4_workerList] ([workerID],[workerName],[workerClass],[workerStatus],[workerLogon],[workerSecData])
VALUES ('WRK_DisabledTest', 'WRK_SetDisabledTest', 255, 'ONDUTY', 'WRK_SetDisabledTest', 'Test');
INSERT INTO [dbo].[s4_workerList] ([workerID],[workerName],[workerClass],[workerStatus],[workerLogon],[workerSecData])
VALUES ('USER1', 'USER1', 1023, 'DISPATCH', 'a', 'A49A5CAB5E68D7E0167F4C7D790AF7835ED7E49E000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000');
INSERT INTO [dbo].[s4_workerList] ([workerID],[workerName],[workerClass],[workerStatus],[workerLogon],[workerSecData])
VALUES ('USER2', 'USER2', 2048, 'DISPATCH', 'a1', 'a2');
INSERT INTO [dbo].[s4_workerList] ([workerID],[workerName],[workerClass],[workerStatus],[workerLogon],[workerSecData])
VALUES ('USER3', 'USER3', 1023, 'DISPATCH', 'a3', 'a3');

INSERT INTO [dbo].[s4_sectionList] ([sectID], [sectDesc], [source_id]) 
VALUES (N'SEC', N'Section', NULL);
INSERT INTO [dbo].[s4_sectionList] ([sectID], [sectDesc], [source_id]) 
VALUES (N'VYSK', N'Vyskladneni', NULL);
-- test data for OnStore
INSERT INTO [dbo].[s4_sectionList] ([sectID], [sectDesc], [source_id]) 
VALUES (N'SEC1', N'Section1', NULL);
----ArticleLoadTest
INSERT INTO [dbo].[s4_sectionList] ([sectID], [sectDesc], [source_id]) 
VALUES (N'mat', N'MAT', NULL);

--ArticleLoadTest
INSERT INTO [dbo].[s4_manufactList] ([manufID],[manufName],[manufStatus],[manufSettings],[source_id],[useBatch],[useExpiration],[sectID],[manufSign])
VALUES ('HARTMANN', 'Hartmann', 'MAN_OK', NULL, 'Hartmann', 0, 0, 'MAT', NULL)
--ReceiveDirLoadTest
INSERT INTO [dbo].[s4_manufactList] ([manufID],[manufName],[manufStatus],[manufSettings],[source_id],[useBatch],[useExpiration],[sectID],[manufSign])
VALUES ('DIALAB', 'Dialab', 'MAN_OK', NULL, 'Dialab', 0, 0, 'MAT', NULL)

--Reference Data for s4_articleList
SET IDENTITY_INSERT [s4_articleList] ON;
INSERT INTO [s4_articleList] ([articleID],[articleCode],[articleDesc],[articleStatus],[articleType],[source_id],[sectID],[unitDesc],[manufID],[useBatch],[mixBatch],[useExpiration],[specFeatures])
VALUES (100, 'X123', 'Zboží 123', 'AR_OK', 0, '0123456789', NULL, NULL, 'ONTX_123', 1, 1, 1, 2);

INSERT INTO [s4_articleList] ([articleID],[articleCode],[articleDesc],[articleStatus],[articleType],[source_id],[sectID],[unitDesc],[manufID],[useBatch],[mixBatch],[useExpiration])
VALUES (101, 'X456', 'Zboží 456', 'AR_OK', 0, 'abcd1', NULL, NULL, 'ONTX_123', 1, 1, 1);

INSERT INTO [s4_articleList] ([articleID],[articleCode],[articleDesc],[articleStatus],[articleType],[source_id],[sectID],[unitDesc],[manufID],[useBatch],[mixBatch],[useExpiration])
VALUES (102, 'X789', 'Zboží 789', 'AR_OK', 0, 'abcd2', NULL, NULL, 'ONTX_123', 1, 1, 1);

INSERT INTO [s4_articleList] ([articleID],[articleCode],[articleDesc],[articleStatus],[articleType],[source_id],[sectID],[unitDesc],[manufID],[useBatch],[mixBatch],[useExpiration])
VALUES (103, 'X123', 'Zboží 123', 'AR_OK', 0, 'abcd3', NULL, NULL, 'ONTX_123', 1, 1, 1);

INSERT INTO [s4_articleList] ([articleID],[articleCode],[articleDesc],[articleStatus],[articleType],[source_id],[sectID],[unitDesc],[manufID],[useBatch],[mixBatch],[useExpiration])
VALUES (104, 'X444', 'Zboží 1234', 'AR_OK', 0, 'abcd4', NULL, NULL, 'ONTX_123', 1, 1, 1);

INSERT INTO [s4_articleList] ([articleID],[articleCode],[articleDesc],[articleStatus],[articleType],[source_id],[sectID],[unitDesc],[manufID],[useBatch],[mixBatch],[useExpiration])
VALUES (105, 'X445', 'Zboží 12345', 'AR_OK', 0, 'abcd5', NULL, NULL, 'ONTX_123', 1, 1, 1);

INSERT INTO [s4_articleList] ([articleID],[articleCode],[articleDesc],[articleStatus],[articleType],[source_id],[sectID],[unitDesc],[manufID],[useBatch],[mixBatch],[useExpiration])
VALUES (106, 'X123-1', 'Zboží X123-1', 'AR_OK', 0, 'Y200000101x', NULL, NULL, 'ONTX_123', 1, 1, 1);

INSERT INTO [s4_articleList] ([articleID],[articleCode],[articleDesc],[articleStatus],[articleType],[source_id],[sectID],[unitDesc],[manufID],[useBatch],[mixBatch],[useExpiration])
VALUES (107, 'X123-2', 'Zboží X123-2', 'AR_OK', 0, 'F8IB000101x', NULL, NULL, 'ONTX_123', 1, 1, 1);

INSERT INTO [s4_articleList] ([articleID],[articleCode],[articleDesc],[articleStatus],[articleType],[source_id],[sectID],[unitDesc],[manufID],[useBatch],[mixBatch],[useExpiration])
VALUES (108, 'X123-3', 'Zboží X123-3', 'AR_OK', 0, 'CVZA000101x', NULL, NULL, 'ONTX_123', 1, 1, 1);

INSERT INTO [s4_articleList] ([articleID],[articleCode],[articleDesc],[articleStatus],[articleType],[source_id],[sectID],[unitDesc],[manufID],[useBatch],[mixBatch],[useExpiration])
VALUES (109, 'X123-4', 'Zboží X123-4', 'AR_OK', 0, 'ENB5000102x', NULL, NULL, 'ONTX_123', 1, 1, 1);

INSERT INTO [s4_articleList] ([articleID],[articleCode],[articleDesc],[articleStatus],[articleType],[source_id],[sectID],[unitDesc],[manufID],[useBatch],[mixBatch],[useExpiration])
VALUES (110, 'X123-5', 'Zboží X123-5', 'AR_OK', 0, 'XX', NULL, NULL, 'ONTX_123', 1, 1, 1);

INSERT INTO [s4_articleList] ([articleID],[articleCode],[articleDesc],[articleStatus],[articleType],[source_id],[sectID],[unitDesc],[manufID],[useBatch],[mixBatch],[useExpiration])
VALUES (111, 'X123-6', 'Zboží X123-6', 'AR_OK', 0, 'ENB5000101', NULL, NULL, 'ONTX_123', 1, 1, 1);

INSERT INTO [s4_articleList] ([articleID],[articleCode],[articleDesc],[articleStatus],[articleType],[source_id],[sectID],[unitDesc],[manufID],[useBatch],[mixBatch],[useExpiration],[specFeatures])
VALUES (112, 'X123-7', 'Zboží X123-7', 'AR_OK', 0, 'ENB5000102', 'SEC1', NULL, 'ONTX_123', 1, 1, 1, 1);

--ReceiveDirLoadTest
INSERT INTO [s4_articleList] ([articleID],[articleCode],[articleDesc],[articleStatus],[articleType],[source_id],[sectID],[unitDesc],[manufID],[useBatch],[mixBatch],[useExpiration])
VALUES (113, '437199JIHNEM', 'Kontejner na kontaminovaný odpad 30,0l', 'AR_OK', 0, 'TNTG000101', 'MAT', 'ks', 'Dialab', 1, 1, 1);

INSERT INTO [s4_articleList] ([articleID],[articleCode],[articleDesc],[articleStatus],[articleType],[source_id],[sectID],[unitDesc],[manufID],[useBatch],[mixBatch],[useExpiration])
VALUES (114, '450332', 'Kontejner na infekční odpad 1,8 l, hranatý', 'AR_OK', 0, 'SK62000101', 'MAT', 'ks', 'Dialab', 1, 1, 1);

--RemoveExpBatchProc
INSERT INTO [s4_articleList] ([articleID],[articleCode],[articleDesc],[articleStatus],[articleType],[source_id],[sectID],[unitDesc],[manufID],[useBatch],[mixBatch],[useExpiration])
VALUES (115, 'REBP', 'REBP test', 'AR_OK', 0, 'REBP_S', 'SEC1', 'ks', 'Dialab', 1, 1, 1);

INSERT INTO [s4_articleList] ([articleID],[articleCode],[articleDesc],[articleStatus],[articleType],[source_id],[sectID],[unitDesc],[manufID],[useBatch],[mixBatch],[useExpiration])
VALUES (116, 'REBP1', 'REBP test1', 'AR_OK', 0, 'REBP_S1', 'SEC1', 'ks', 'Dialab', 1, 1, 1);

INSERT INTO [s4_articleList] ([articleID],[articleCode],[articleDesc],[articleStatus],[articleType],[source_id],[sectID],[unitDesc],[manufID],[useBatch],[mixBatch],[useExpiration])
VALUES (117, 'REBP2', 'REBP test2', 'AR_OK', 0, 'REBP_S2', 'SEC1', 'ks', 'Dialab', 1, 1, 1);

INSERT INTO [s4_articleList] ([articleID],[articleCode],[articleDesc],[articleStatus],[articleType],[source_id],[sectID],[unitDesc],[manufID],[useBatch],[mixBatch],[useExpiration])
VALUES (118, 'DONTTOUCH', 'DONTTOUCH test', 'AR_OK', 0, 'SOURCE', 'SEC1', 'ks', 'Dialab', 1, 1, 1);

--dispcheckdone useserials
INSERT INTO [s4_articleList] ([articleID],[articleCode],[articleDesc],[articleStatus],[articleType],[source_id],[sectID],[unitDesc],[manufID],[useBatch],[mixBatch],[useExpiration],[useSerialNumber])
VALUES (119, 'USESERS', 'Use Serial Numbers', 'AR_OK', 0, 'SERS', 'SEC1', 'ks', 'Dialab', 1, 1, 1, 1);

INSERT INTO [s4_articleList] ([articleID],[articleCode],[articleDesc],[articleStatus],[articleType],[source_id],[sectID],[unitDesc],[manufID],[useBatch],[mixBatch],[useExpiration],[useSerialNumber])
VALUES (120, 'NUSESERS', 'Dont Use Serial Numbers', 'AR_OK', 0, 'NSERS', 'SEC1', 'ks', 'Dialab', 1, 1, 1, 0);

--removecarrierproc test
INSERT INTO [s4_articleList] ([articleID],[articleCode],[articleDesc],[articleStatus],[articleType],[source_id],[sectID],[unitDesc],[manufID],[useBatch],[mixBatch],[useExpiration],[useSerialNumber])
VALUES (121, 'AR121', 'Article 121', 'AR_OK', 0, 'SO1', 'SEC1', 'ks', 'Dialab', 1, 1, 1, 0);

INSERT INTO [s4_articleList] ([articleID],[articleCode],[articleDesc],[articleStatus],[articleType],[source_id],[sectID],[unitDesc],[manufID],[useBatch],[mixBatch],[useExpiration],[useSerialNumber])
VALUES (122, 'AR122', 'Article 122', 'AR_OK', 0, 'SO2', 'SEC1', 'ks', 'Dialab', 1, 1, 1, 0);

--DispatchManualSaveItemProcTest_Success
INSERT INTO [s4_articleList] ([articleID],[articleCode],[articleDesc],[articleStatus],[articleType],[source_id],[sectID],[unitDesc],[manufID],[useBatch],[mixBatch],[useExpiration])
VALUES (123, 'DISMANSAVE', 'DISMANSAVE test', 'AR_OK', 0, 'DISMANSAVE_SOURCE', 'SEC1', 'ks', 'Dialab', 1, 1, 1);
INSERT INTO [s4_articleList] ([articleID],[articleCode],[articleDesc],[articleStatus],[articleType],[source_id],[sectID],[unitDesc],[manufID],[useBatch],[mixBatch],[useExpiration])
VALUES (124, 'DISMANSAVE2', 'DISMANSAVE test2', 'AR_OK', 0, 'DISMANSAVE_SOURCE2', 'SEC1', 'ks', 'Dialab', 0, 0, 0);

INSERT INTO [s4_articleList] ([articleID],[articleCode],[articleDesc],[articleStatus],[articleType],[source_id],[sectID],[unitDesc],[manufID],[useBatch],[mixBatch],[useExpiration])
VALUES (125, 'RECCHANGE125', 'RECCHANGE_Article 125', 'AR_OK', 0, 'RECCHANGE_SOURCE', 'SEC1', 'ks', 'Dialab', 0, 0, 0);

INSERT INTO [s4_articleList] ([articleID],[articleCode],[articleDesc],[articleStatus],[articleType],[source_id],[sectID],[unitDesc],[manufID],[useBatch],[mixBatch],[useExpiration])
VALUES (126, 'ADDBARCODE126', 'ADDBARCODE 126', 'AR_OK', 0, 'ADDBARCODE_SOURCE', 'SEC1', 'ks', 'Dialab', 0, 0, 0);

--ReceiveSimpleProcTest
INSERT INTO [s4_articleList] ([articleID],[articleCode],[articleDesc],[articleStatus],[articleType],[source_id],[sectID],[unitDesc],[manufID],[useBatch],[mixBatch],[useExpiration])
VALUES (127, 'ADDBARCODE126', 'Article for ReceiveSimpleProcTest', 'AR_OK', 0, 'RECEIVESIMPLE_SOURCE', 'SEC1', 'ks', 'Dialab', 0, 0, 0);

SET IDENTITY_INSERT [s4_articleList] OFF;

--Reference Data for s4_articleList_packings
SET IDENTITY_INSERT [s4_articleList_packings] ON;
INSERT INTO [s4_articleList_packings] ([artPackID],[articleID],[packDesc],[packRelation],[movablePack],[source_id],[packStatus],[barCodeType],[barCode],[inclCarrier],[packWeight],[packVolume])
VALUES (1001, 100, 'ks', 1, 1, '0123456789', 'AP_OK', 0, 'BARCODE1', 0, 0, 0);

INSERT INTO [s4_articleList_packings] ([artPackID],[articleID],[packDesc],[packRelation],[movablePack],[source_id],[packStatus],[barCodeType],[barCode],[inclCarrier],[packWeight],[packVolume])
VALUES (1002, 101, 'ks', 1, 1, 'abcd1', 'AP_OK', 1, 'XX12345', 0, 0, 0);

INSERT INTO [s4_articleList_packings] ([artPackID],[articleID],[packDesc],[packRelation],[movablePack],[source_id],[packStatus],[barCodeType],[barCode],[inclCarrier],[packWeight],[packVolume])
VALUES (1003, 102, 'ks', 1, 1, 'abcd2', 'AP_OK', 0, NULL, 0, 0, 0);

INSERT INTO [s4_articleList_packings] ([artPackID],[articleID],[packDesc],[packRelation],[movablePack],[source_id],[packStatus],[barCodeType],[barCode],[inclCarrier],[packWeight],[packVolume])
VALUES (1004, 103, 'ks', 1, 1, 'abcd3', 'AP_OK', 0, NULL, 0, 0, 0);

INSERT INTO [s4_articleList_packings] ([artPackID],[articleID],[packDesc],[packRelation],[movablePack],[source_id],[packStatus],[barCodeType],[barCode],[inclCarrier],[packWeight],[packVolume])
VALUES (1005, 104, 'ks', 1, 1, 'abcd4', 'AP_OK', 0, NULL, 0, 0, 0);

INSERT INTO [s4_articleList_packings] ([artPackID],[articleID],[packDesc],[packRelation],[movablePack],[source_id],[packStatus],[barCodeType],[barCode],[inclCarrier],[packWeight],[packVolume])
VALUES (1006, 105, 'ks', 1, 1, 'abcd5', 'AP_OK', 0, NULL, 0, 0, 0);

INSERT INTO [s4_articleList_packings] ([artPackID],[articleID],[packDesc],[packRelation],[movablePack],[source_id],[packStatus],[barCodeType],[barCode],[inclCarrier],[packWeight],[packVolume])
VALUES (1007, 106, 'ks', 1, 1, 'R500000101', 'AP_OK', 0, NULL, 0, 0, 0);

INSERT INTO [s4_articleList_packings] ([artPackID],[articleID],[packDesc],[packRelation],[movablePack],[source_id],[packStatus],[barCodeType],[barCode],[inclCarrier],[packWeight],[packVolume])
VALUES (1008, 107, 'ks', 1, 1, 'FD4D000101', 'AP_OK', 0, NULL, 0, 0, 0);

INSERT INTO [s4_articleList_packings] ([artPackID],[articleID],[packDesc],[packRelation],[movablePack],[source_id],[packStatus],[barCodeType],[barCode],[inclCarrier],[packWeight],[packVolume])
VALUES (1009, 108, 'ks', 1, 1, 'EMLC000101', 'AP_OK', 0, NULL, 0, 0, 0);

INSERT INTO [s4_articleList_packings] ([artPackID],[articleID],[packDesc],[packRelation],[movablePack],[source_id],[packStatus],[barCodeType],[barCode],[inclCarrier],[packWeight],[packVolume])
VALUES (1010, 109, 'ks', 1, 1, 'I08F000101', 'AP_OK', 0, NULL, 0, 0, 0);

INSERT INTO [s4_articleList_packings] ([artPackID],[articleID],[packDesc],[packRelation],[movablePack],[source_id],[packStatus],[barCodeType],[barCode],[inclCarrier],[packWeight],[packVolume])
VALUES (1011, 109, 'ks', 1, 1, '4L60000101', 'AP_OK', 0, NULL, 0, 0, 0);

INSERT INTO [s4_articleList_packings] ([artPackID],[articleID],[packDesc],[packRelation],[movablePack],[source_id],[packStatus],[barCodeType],[barCode],[inclCarrier],[packWeight],[packVolume])
VALUES (1012, 112, 'ks', 1, 1, '4A60000101', 'AP_OK', 0, NULL, 0, 0, 0);

--RemoveExpBatchProc
INSERT INTO [s4_articleList_packings] ([artPackID],[articleID],[packDesc],[packRelation],[movablePack],[source_id],[packStatus],[barCodeType],[barCode],[inclCarrier],[packWeight],[packVolume])
VALUES (1013, 115, 'ks', 1, 1, 'REBP_S', 'AP_OK', 0, NULL, 0, 0, 0);

INSERT INTO [s4_articleList_packings] ([artPackID],[articleID],[packDesc],[packRelation],[movablePack],[source_id],[packStatus],[barCodeType],[barCode],[inclCarrier],[packWeight],[packVolume])
VALUES (1014, 116, 'ks', 1, 1, 'REBP_S1', 'AP_OK', 0, NULL, 0, 0, 0);

INSERT INTO [s4_articleList_packings] ([artPackID],[articleID],[packDesc],[packRelation],[movablePack],[source_id],[packStatus],[barCodeType],[barCode],[inclCarrier],[packWeight],[packVolume])
VALUES (1015, 117, 'ks', 1, 1, 'REBP_S2', 'AP_OK', 0, NULL, 0, 0, 0);

INSERT INTO [s4_articleList_packings] ([artPackID],[articleID],[packDesc],[packRelation],[movablePack],[source_id],[packStatus],[barCodeType],[barCode],[inclCarrier],[packWeight],[packVolume])
VALUES (1016, 118, 'ks', 1, 1, 'SOURCE', 'AP_OK', 0, NULL, 0, 0, 0);

--dispcheckdone useserials
INSERT INTO [s4_articleList_packings] ([artPackID],[articleID],[packDesc],[packRelation],[movablePack],[source_id],[packStatus],[barCodeType],[barCode],[inclCarrier],[packWeight],[packVolume])
VALUES (1017, 119, 'ks', 1, 1, 'SERS', 'AP_OK', 0, NULL, 0, 0, 0);

INSERT INTO [s4_articleList_packings] ([artPackID],[articleID],[packDesc],[packRelation],[movablePack],[source_id],[packStatus],[barCodeType],[barCode],[inclCarrier],[packWeight],[packVolume])
VALUES (1018, 120, 'ks', 1, 1, 'NSERS', 'AP_OK', 0, NULL, 0, 0, 0);

--removecarrierproc test
INSERT INTO [s4_articleList_packings] ([artPackID],[articleID],[packDesc],[packRelation],[movablePack],[source_id],[packStatus],[barCodeType],[barCode],[inclCarrier],[packWeight],[packVolume])
VALUES (1019, 121, 'ks', 1, 1, 'SO1', 'AP_OK', 0, NULL, 0, 0, 0);

INSERT INTO [s4_articleList_packings] ([artPackID],[articleID],[packDesc],[packRelation],[movablePack],[source_id],[packStatus],[barCodeType],[barCode],[inclCarrier],[packWeight],[packVolume])
VALUES (1020, 122, 'ks', 1, 1, 'SO2', 'AP_OK', 0, NULL, 0, 0, 0);

--DispatchManualSaveItemProcTest_Success
INSERT INTO [s4_articleList_packings] ([artPackID],[articleID],[packDesc],[packRelation],[movablePack],[source_id],[packStatus],[barCodeType],[barCode],[inclCarrier],[packWeight],[packVolume])
VALUES (1021, 123, 'ks', 1, 1, 'DISMANSAVE_SOURCE', 'AP_OK', 0, NULL, 0, 0, 0);
INSERT INTO [s4_articleList_packings] ([artPackID],[articleID],[packDesc],[packRelation],[movablePack],[source_id],[packStatus],[barCodeType],[barCode],[inclCarrier],[packWeight],[packVolume])
VALUES (1022, 124, 'ks', 1, 1, 'DISMANSAVE_SOURCE2', 'AP_OK', 0, NULL, 0, 0, 0);

--DirectionReceiveChangeTest
INSERT INTO [s4_articleList_packings] ([artPackID],[articleID],[packDesc],[packRelation],[movablePack],[source_id],[packStatus],[barCodeType],[barCode],[inclCarrier],[packWeight],[packVolume])
VALUES (1023, 125, 'ks', 1, 1, 'RECCHANGE_SOURCE', 'AP_OK', 0, NULL, 0, 0, 0);


INSERT INTO [s4_articleList_packings] ([artPackID],[articleID],[packDesc],[packRelation],[movablePack],[source_id],[packStatus],[barCodeType],[barCode],[inclCarrier],[packWeight],[packVolume])
VALUES (1024, 126, 'ks', 1, 1, 'ADDBARCODE_SOURCE', 'AP_OK', 0, '333', 0, 0, 0);

INSERT INTO [s4_articleList_packings] ([artPackID],[articleID],[packDesc],[packRelation],[movablePack],[source_id],[packStatus],[barCodeType],[barCode],[inclCarrier],[packWeight],[packVolume])
VALUES (1025, 126, 'ks', 1, 1, 'ADDBARCODE_SOURCE2', 'AP_OK', 0, '333', 0, 0, 0);

--Bookings
INSERT INTO [s4_articleList_packings] ([artPackID],[articleID],[packDesc],[packRelation],[movablePack],[source_id],[packStatus],[barCodeType],[barCode],[inclCarrier],[packWeight],[packVolume])
VALUES (1026, 100, 'ks', 1, 1, 'SOURCE1026', 'AP_OK', 0, 'BARCODE1026', 0, 0, 0);

INSERT INTO [s4_articleList_packings] ([artPackID],[articleID],[packDesc],[packRelation],[movablePack],[source_id],[packStatus],[barCodeType],[barCode],[inclCarrier],[packWeight],[packVolume])
VALUES (1027, 100, 'ks', 1, 1, 'SOURCE1027', 'AP_OK', 0, 'BARCODE1027', 0, 0, 0);

INSERT INTO [s4_articleList_packings] ([artPackID],[articleID],[packDesc],[packRelation],[movablePack],[source_id],[packStatus],[barCodeType],[barCode],[inclCarrier],[packWeight],[packVolume])
VALUES (1028, 100, 'ks', 1, 1, 'AVAIL_PACK', 'AP_OK', 0, 'BARCODE1028', 0, 0, 0);

--ReceiveSimpleProcTest
INSERT INTO [s4_articleList_packings] ([artPackID],[articleID],[packDesc],[packRelation],[movablePack],[source_id],[packStatus],[barCodeType],[barCode],[inclCarrier],[packWeight],[packVolume])
VALUES (1029, 127, 'ks', 1, 1, 'SOURCE1029', 'AP_OK', 0, 'BARCODE1028', 0, 0, 0);

SET IDENTITY_INSERT [s4_articleList_packings] OFF;

--Reference Data for s4_direction
SET IDENTITY_INSERT [s4_direction] ON;
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (1, 'ACT', 'DL',1,'04',1,GETDATE(), 1, 1, -1, GETDATE());

INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (2, 'CANC', 'DL',1,'04',2,GETDATE(), 1, 1, -1, GETDATE());

INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (3, 'ACT', 'DL',1,'04',3,GETDATE(), 1, 1, -1, GETDATE());

INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (4, 'LOAD', 'DL',1,'04',4,GETDATE(), 1, 1, -1, GETDATE());

INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (5, 'PDIS', 'DL',1,'04',5,GETDATE(), 1, 1, -1, GETDATE());

INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (6, 'WDIS', 'DL',1,'04',6,GETDATE(), 1, 1, -1, GETDATE());

INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (7, 'LOAD', 'DL',-1,'04',7,GETDATE(), 1, 1, -1, GETDATE());

-- DispatchProcProcTest_Success_NotFound
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (8, 'RES_IN_PROC', 'DL',-1,'04',8 ,GETDATE() , 1, 1, -1, GETDATE());

INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (9, 'DCHE', 'DL',-1,'05', 8, GETDATE(), 1, 1, -1, GETDATE());

-- ExpedDoneTest_Success
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (10, 'DCHE', 'DL',-1,'06', 8, GETDATE(), 1, 1, -1, GETDATE());
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (1001, 'MADEL', 'DL',-1,'06', 801, GETDATE(), 1, 1, -1, GETDATE());

INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (11, 'SDIS', 'DL',-1, '07', 9, GETDATE(), 1, 1, -1, GETDATE());

INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (12, 'WCHE', 'DL',-1, '08', 9, GETDATE(), 1, 1, -1, GETDATE());

-- DispCheckGetItemsProcTest_Success
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (13, 'PCHE', 'DL',-1, '09', 9, GETDATE(), 1, 1, -1, GETDATE());

-- DispCheckDoneProcTest_Success
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (14, 'PCHE', 'DL',-1, '10', 9, GETDATE(), 1, 1, -1, GETDATE());

-- DispCheckDoneProcTest_SuccessMixedBatches
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (1400, 'PCHE', 'DL',-1, '10', 900, GETDATE(), 1, 1, -1, GETDATE());

INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (1401, 'PCHE', 'DL',-1, '10', 901, GETDATE(), 1, 1, -1, GETDATE());


INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (15, 'PCHE', 'DL',-1, '11', 9, GETDATE(), 1, 1, -1, GETDATE());

INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (16, 'DCHE', 'DL',-1, '12', 9, GETDATE(), 1, 1, -1, GETDATE());

INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (17, 'DCHE', 'DL',-1, '13', 9, GETDATE(), 1, 1, -1, GETDATE());

--DirectionItemChangeProcTest_Dispatch_Success
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (18, 'DCHE', 'DL',-1, '14', 9, GETDATE(), 1, 1, -1, GETDATE());
--DispatchProcProcTest_Success_Found
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (19, 'RES_IN_PROC', 'DL',-1, '11', 10, GETDATE(), 1, 1, -1, GETDATE());
--DispatchCancelReservationProcTest_Success
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (21, 'PDIS', 'DL',-1, '14', 10, GETDATE(), 1, 1, -1, GETDATE());

INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (22, 'CREC', 'DL', 1, '14', 11, GETDATE(), 1, 1, -1, GETDATE());

INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (23, 'PSTI', 'DL', 1, '14', 12, GETDATE(), 1, 1, -1, GETDATE());
--StoreInAcceptPosProcTest
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (24, 'WSTI', 'DL', 1, '14', 13, GETDATE(), 1, 1, -1, GETDATE());
--LogisticsDispToStoInProcTest
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (25, 'DCHE', 'DL', -1, '18', 14, GETDATE(), 1, 1, -1, GETDATE());
--ReceiveCancelItemProcTest
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (26, 'PREC', 'DL', -1, '18', 15, GETDATE(), 1, 1, -1, GETDATE());
--LogisticsDispatchToPositionProcTest
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (27, 'DCHE', 'DL', -1, '18', 16, GETDATE(), 1, 1, -1, GETDATE());
-- DispatchRepeatReservationProcTest_Success
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (28, 'PDIS', 'DL',-1,'18',17,GETDATE(), 1, 1, -1, GETDATE());
-- ReceiveSaveItemProcTest_Success
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (29, 'PREC', 'PRIJ',1,'18',1,GETDATE(), 1, 1, -1, GETDATE());
-- ReceiveCancelItemProcTest_Success
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (30, 'PREC', 'PRIJ',1,'18',2,GETDATE(), 1, 1, -1, GETDATE());
-- DirectionItemChangeProcTest_Receive_Success
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (31, 'PREC', 'PRIJ',1,'18',3,GETDATE(), 1, 1, -1, GETDATE());
-- DispatchPrintTest,ReceivePrintByDirectionProcTest
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (32, 'ACT', 'DL',1,'19',1,GETDATE(), 1, 1, -1, GETDATE());
-- DispatchSaveItemProcTest_Success
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (33, 'DCHE', 'DL', -1, '19', 19, GETDATE(), 1, 1, -1, GETDATE());
-- export tests
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (34, 'DCHE', 'DL', -1, '20', 19, '09-10-2019', 1, 1, -1, '09-10-2019');
-- DispatchReassignProcTest_Non_Success
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (35, 'PDIS', 'DL',-1, '21', 10, GETDATE(), 1, 1, -1, GETDATE());

-- RemoveExpBatchProc
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (36, 'PDIS', 'DL',1, '19', 21, GETDATE(), 1, 1, -1, GETDATE());

INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (37, 'PDIS', 'DL',1, '19', 22, GETDATE(), 1, 1, -1, GETDATE());

INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (38, 'PDIS', 'DL',1, '19', 23, GETDATE(), 1, 1, -1, GETDATE());

-- DispCheckSkipProcTest_Success
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (39, 'SDIS', 'DL', -1, '19', 24, GETDATE(), 1, 1, -1, GETDATE());

--removecarrierproc test
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (40, 'SDIS', 'DL', -1, '19', 25, GETDATE(), 1, 1, -1, GETDATE());

--GetDirectionsForManifestProcTest
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (41, 'PRDEL', 'DL', -1, '20', 26, GETDATE(), 1, 1, -1, GETDATE());
--InsertShipmentProcTest
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (42, 'DCHE', 'DL', -1, '20', 27, GETDATE(), 1, 1, -1, GETDATE());
--LabelPrintedProcTest
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (43, 'SHDEL', 'DL', -1, '20', 28, GETDATE(), 1, 1, -1, GETDATE());
--ManifestCreatedProcTest
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (44, 'PRDEL', 'DL', -1, '20', 29, GETDATE(), 1, 1, -1, GETDATE());

INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (45, 'PRDEL', 'DL', -1, '20', 30, GETDATE(), 1, 1, -1, GETDATE());

--PrintLabelProcTest
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (46, 'SHDEL', 'DL', -1, '20', 31, GETDATE(), 1, 1, -1, GETDATE());

--DispatchManualSaveItemProcTest_Error
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (47, 'PDIS', 'DL',-1, '20', 32, GETDATE(), 1, 1, -1, GETDATE());
--DispatchManualSaveItemProcTest_Success
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (48, 'MDIS', 'DL',-1, '20', 33, GETDATE(), 1, 1, -1, GETDATE());
--DispatchManualBeginProcTest_Error
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (49, 'PDIS', 'DL',-1, '20', 24, GETDATE(), 1, 1, -1, GETDATE());
--DispatchManualBeginProcTest_Success
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (50, 'LOAD', 'DL',-1, '20', 25, GETDATE(), 1, 1, -1, GETDATE());
--DispCheckAvailableAbraProcTest_Success
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (51, 'SDIS', 'DL',-1, '20', 51, GETDATE(), 1, 1, -1, GETDATE());
--DispCheckAvailableAbraDirectionsTest_Success
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (52, 'SDIS', 'DL',-1, '20', 521, GETDATE(), 1, 1, -1, GETDATE());
--DispCheckAvailableAbraDirectionsTest_BadDocStatus
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (53, 'LOAD', 'DL',-1, '20', 522, GETDATE(), 1, 1, -1, GETDATE());
--ReceiveChangePartnerProcTest_Success
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (54, 'PREC', 'DL',-1, '20', 523, GETDATE(), 2, 2, -1, GETDATE());
--ReceiveChangePartnerProcTest_ErrorBadDocStatus
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (55, 'CREC', 'DL',-1, '20', 524, GETDATE(), 2, 2, -1, GETDATE());
--ReceiveChangePartnerProcTest_EmptyPartner
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (56, 'PREC', 'DL',-1, '20', 525, GETDATE(), 2, 2, -1, GETDATE());
--DirectionSetShipmentFailedStatusProc
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (57, 'LOAD', 'DL',-1, '20', 526, GETDATE(), 1, 1, -1, GETDATE());
--ResetShipmentProcTest
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (58, 'DCHE', 'DL', -1, '20', 527, GETDATE(), 1, 1, -1, GETDATE());
--DirectionReceiveChangeProcTest_Success
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (59, 'PRIJ', 'DL',-1, '20', 528, GETDATE(), 1, 1, -1, GETDATE());
--CancellationLoadTest_NoSuccess_NoStatus
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (60, 'EXPORT_DONE', 'DL',-1, '21', 1, GETDATE(), 1, 1, -1, GETDATE());
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (61, 'LOAD', 'DL',-1, '21', 2, GETDATE(), 1, 1, -1, GETDATE());
--DispCheckAvailableAbraProcTest_SuccessMulti
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (62, 'SDIS', 'DL',-1, '21', 62, GETDATE(), 1, 1, -1, GETDATE());
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (63, 'SDIS', 'DL',-1, '21', 63, GETDATE(), 1, 1, -1, GETDATE());
--DirectionSetDeliveredCustomerProcTest
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (64, 'LOAD', 'DL',-1, '21', 3, GETDATE(), 1, 1, -1, GETDATE());
--DispatchUserAssignmentProcTest_Success
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (65, 'LOAD', 'DL',-1, '21', 64, GETDATE(), 1, 1, -1, GETDATE());
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime], [invoiceID])
VALUES (66, 'LOAD', 'DL',-1, '21', 65, GETDATE(), 1, 1, -1, GETDATE(), 1);
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime], [invoiceID])
VALUES (67, 'SDIS', 'DL',-1, '21', 66, GETDATE(), 1, 1, -1, GETDATE(), 1);

INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime], [invoiceID])
VALUES (68, 'SDIS', 'DL',-1, '21', 67, GETDATE(), 1, 1, -1, GETDATE(), 1);
--GetDirectionsByAbraDocID_Success
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime], [invoiceID])
VALUES (69, 'EXPE', 'DL',-1, '21', 69, GETDATE(), 1, 1, -1, GETDATE(), 1);
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime], [invoiceID])
VALUES (70, 'EXPE', 'DL',-1, '21', 70, GETDATE(), 1, 1, -1, GETDATE(), 1);
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime], [invoiceID])
VALUES (71, 'EXPE', 'DL',-1, '21', 71, GETDATE(), 1, 1, -1, GETDATE(), 1);
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime], [invoiceID])
VALUES (72, 'EXPE', 'DL',-1, '21', 72, GETDATE(), 1, 1, -1, GETDATE(), 1);
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime], [invoiceID])
VALUES (73, 'EXPE', 'DL',-1, '21', 73, GETDATE(), 1, 1, -1, GETDATE(), 1);

--DispatchPrintSerialNumberProcTest_Success
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime], [invoiceID])
VALUES (74, 'EXPE', 'DL',-1, '24', 74, GETDATE(), 1, 1, -1, GETDATE(), 1);


----ExpedDoneDoNotProcessTest_Success
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime], [invoiceID], [doNotProcess])
VALUES (1002, 'NOT_PROC', 'DL',-1, '21', 74, GETDATE(), 1, 1, -1, GETDATE(), 1, 1);

SET IDENTITY_INSERT [s4_direction] OFF;

--[s4_direction_history]
SET IDENTITY_INSERT [s4_direction_history] ON;
INSERT INTO [dbo].[s4_direction_history]([directionHistID], [directionID],[docPosition],[eventCode],[eventData],[entryDateTime],[entryUserID])
VALUES (1, 17, 0, 'LOAD' , NULL, GETDATE(), 'S3IMPO')

--DispatchManualSaveItemProcTest_Error
INSERT INTO [dbo].[s4_direction_history]([directionHistID], [directionID],[docPosition],[eventCode],[eventData],[entryDateTime],[entryUserID])
VALUES (2, 47, 0, 'LOAD' , NULL, GETDATE(), 'S3IMPO')

SET IDENTITY_INSERT [s4_direction_history] OFF;

--Reference Data for s4_directionItems
SET IDENTITY_INSERT [s4_direction_items] ON;
INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID])
VALUES (1, 1, 1, 'LOAD', 1001, 'X123',1,1,GETDATE(),1);
INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID])
VALUES (2, 2, 1, 'LOAD', 1001, 'X123',1,1,GETDATE(),1);

-- DispCheckGetItemsProcTest_Success
INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID])
VALUES (3, 13, 1, 'LOAD', 1005, 'X123',1,1,GETDATE(),1);

INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID], [partnerDepart])
VALUES (4, 7, 1, 'LOAD', 1006, 'X123',1,1,GETDATE(),1, 'dep0');
INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID])
VALUES (5, 16, 1, 'LOAD', 1001, 'X123',1,1,GETDATE(),1);
INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID])
VALUES (6, 17, 1, 'LOAD', 1001, 'X123',1,1,GETDATE(),1);
INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID])
VALUES (7, 18, 1, 'LOAD', 1001, 'X123',1,1,GETDATE(),1);
-- DispatchProcProcTest_Success_NotFound
INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID],[partnerDepart])
VALUES (8, 8, 1, 'LOAD', 1001, 'X123',1,1,GETDATE(),1,'DEP1');
INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID],[partnerDepart])
VALUES (9, 8, 2, 'LOAD', 1001, 'X123',2,1,GETDATE(),1,'DEP2');
-- DispatchProcProcTest_Success_Found
INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID],[partnerDepart])
VALUES (10, 19, 1, 'LOAD', 1001, 'X123',1,1,GETDATE(),1,'DEP1');
-- StoreInAcceptPosNoCarrProcTest_Success
INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID],[partnerDepart])
VALUES (11, 23, 1, 'LOAD', 1001, 'X123',1,1,GETDATE(),1,'DEP1');
-- StoreInAcceptPosProcTest_Success
INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID],[partnerDepart])
VALUES (12, 24, 1, 'LOAD', 1001, 'X123',1,1,GETDATE(),1,'DEP1');
--LogisticsDispToStoInProcTest
INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID],[partnerDepart])
VALUES (13, 25, 1, 'LOAD', 1001, 'X123',1,1,GETDATE(),1,'DEP1');
--LogisticsDispatchToPositionProcTest
INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID],[partnerDepart])
VALUES (14, 27, 1, 'LOAD', 1001, 'X123',1,1,GETDATE(),1,'DEP1');
-- ReceiveTestDirectionProcTest_Success
INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID])
VALUES (15, 26, 1, 'PREC', 1006, 'X123',1,1,GETDATE(),1);
-- DispatchRepeatReservationProcTest_Success
INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID])
VALUES (28, 28, 1, 'LOAD', 1006, 'X123',1,1,GETDATE(),1);
-- ReceiveCancelItemProcTest_Success
INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID])
VALUES (29, 30, 1, 'PREC', 1006, 'X123',1,1,GETDATE(),1);
-- DirectionItemChangeProcTest_Receive_Success
INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID])
VALUES (30, 31, 1, 'PREC', 1006, 'X123',1,1,GETDATE(),1);
--DispatchPrintTest
INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID])
VALUES (31, 32, 1, 'PREC', 1006, 'X123',1,1,GETDATE(),1);
--DispatchSaveItemProcTest_Success
INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID],[partnerDepart])
VALUES (32, 33, 1, 'LOAD', 1001, 'X123',1,1,GETDATE(),1,'DEP1');

--RemoveExpBatchProc
INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID])
VALUES (33, 36, 1, 'CSTI', 1013, 'REBP',1,1,GETDATE(),1);
INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID])
VALUES (34, 36, 2, 'CSTI', 1013, 'REBP',1,1,GETDATE(),1);
INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID])
VALUES (35, 36, 3, 'CSTI', 1013, 'REBP',1,1,GETDATE(),1);
INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID])
VALUES (36, 36, 4, 'CSTI', 1013, 'REBP',1,1,GETDATE(),1);
INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID])
VALUES (37, 36, 5, 'CSTI', 1013, 'REBP',1,1,GETDATE(),1);
INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID])
VALUES (38, 36, 6, 'CSTI', 1013, 'REBP',1,1,GETDATE(),1);

INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID])
VALUES (39, 37, 1, 'CSTI', 1014, 'REBP1',1,1,GETDATE(),1);
INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID])
VALUES (40, 37, 2, 'CSTI', 1014, 'REBP1',1,1,GETDATE(),1);
INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID])
VALUES (41, 37, 3, 'CSTI', 1014, 'REBP1',1,1,GETDATE(),1);

INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID])
VALUES (42, 38, 1, 'CSTI', 1015, 'REBP2',1,1,GETDATE(),1);
INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID])
VALUES (43, 38, 2, 'CSTI', 1015, 'REBP2',1,1,GETDATE(),1);
INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID])
VALUES (44, 38, 3, 'CSTI', 1015, 'REBP2',1,1,GETDATE(),1);

INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID])
VALUES (45, 36, 7, 'CSTI', 1016, 'DONTTOUCH',1,1,GETDATE(),1);

-- DispCheckDoneProcTest_Success
INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID])
VALUES (46, 14, 1, 'CSTI', 1017, 'USESERS',2,1,GETDATE(),1);
INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID])
VALUES (47, 14, 2, 'CSTI', 1018, 'NUSESERS',2,1,GETDATE(),1);

-- DispCheckDoneProcTest_SuccessMixedBatches
INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID])
VALUES (4600, 1400, 1, 'CSTI', 1017, 'USESERS',3,1,GETDATE(),1);

-- DispCheckDoneProcTest_SuccessMixedBatches
INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID])
VALUES (4601, 1401, 1, 'CSTI', 1017, 'USESERS',3,1,GETDATE(),1);
INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID])
VALUES (4602, 1401, 2, 'CSTI', 1017, 'USESERS',1,1,GETDATE(),1);

--removecarrierproc test
INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID])
VALUES (48, 40, 1, 'CSTI', 1019, 'AR121',2,1,GETDATE(),1);

INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID])
VALUES (49, 40, 2, 'CSTI', 1019, 'AR121',3,1,GETDATE(),1);

INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID])
VALUES (50, 40, 3, 'CSTI', 1020, 'AR122',4,1,GETDATE(),1);

INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID])
VALUES (51, 40, 4, 'CSTI', 1020, 'AR122',5,1,GETDATE(),1);

--DispatchManualSaveItemProcTest_Success
INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID])
VALUES (52, 48, 1, 'CSTI', 1021, 'DISMANSAVE',1,1,GETDATE(),1);
INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID])
VALUES (53, 48, 2, 'CSTI', 1022, 'DISMANSAVE',1,1,GETDATE(),1);

--DirectionReceiveChangeProcTest_Success
INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID])
VALUES (54, 59, 1, 'CREC', 1023, 'SREC528',1,1,GETDATE(),1);
--DirectionReceiveChangeProcTest_StoreInMoveError
INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID])
VALUES (55, 59, 2, 'PREC', 1023, 'SREC528',1,1,GETDATE(),1);

INSERT INTO [s4_direction_items] ([directionItemID],[directionID],[docPosition],[itemStatus],[artPackID],[articleCode],[quantity],[unitPrice],[entryDateTime],[entryUserID])
VALUES (56, 68, 1, 'PREC', 1024, 'SREC56',1,1,GETDATE(),1);

SET IDENTITY_INSERT [s4_direction_items] OFF;

--Reference Data for s4_direction_assign
INSERT INTO [dbo].[s4_direction_assign] ([directionID],[jobID],[workerID],[assignParams],[entryDateTime],[entryUserID])
VALUES (2, 'DISP', 'WRK1', NULL, GETDATE(),'Test1')
INSERT INTO [dbo].[s4_direction_assign] ([directionID],[jobID],[workerID],[assignParams],[entryDateTime],[entryUserID])
VALUES (5, 'DISP', 'WRK1', NULL, GETDATE(),'Test1')
INSERT INTO [dbo].[s4_direction_assign] ([directionID],[jobID],[workerID],[assignParams],[entryDateTime],[entryUserID])
VALUES (6, 'DISP', 'WRK1', NULL, GETDATE(),'Test1')
INSERT INTO [dbo].[s4_direction_assign] ([directionID],[jobID],[workerID],[assignParams],[entryDateTime],[entryUserID])
VALUES (7, 'DISP', 'WRK1', NULL, GETDATE(),'Test1')
INSERT INTO [dbo].[s4_direction_assign] ([directionID],[jobID],[workerID],[assignParams],[entryDateTime],[entryUserID])
VALUES (11, 'DISP', 'WRK1', NULL, GETDATE(),'Test1')
INSERT INTO [dbo].[s4_direction_assign] ([directionID],[jobID],[workerID],[assignParams],[entryDateTime],[entryUserID])
VALUES (17, 'DISP', 'WRK1', NULL, GETDATE(),'Test1')
INSERT INTO [dbo].[s4_direction_assign] ([directionID],[jobID],[workerID],[assignParams],[entryDateTime],[entryUserID])
VALUES (21, 'DISP', 'WRK1', NULL, GETDATE(),'Test1')
INSERT INTO [dbo].[s4_direction_assign] ([directionID],[jobID],[workerID],[assignParams],[entryDateTime],[entryUserID])
VALUES (22, 'STIN', 'WRK1', NULL, GETDATE(),'Test1')
INSERT INTO [dbo].[s4_direction_assign] ([directionID],[jobID],[workerID],[assignParams],[entryDateTime],[entryUserID])
VALUES (65, 'DISP', 'WRK1', NULL, GETDATE(),'Test1')
INSERT INTO [dbo].[s4_direction_assign] ([directionID],[jobID],[workerID],[assignParams],[entryDateTime],[entryUserID])
VALUES (66, 'DISP', 'WRK1', NULL, GETDATE(),'Test1')

--Reference Data for s4_direction_xtra
INSERT INTO [dbo].[s4_direction_xtra] ([directionid], [xtracode], [docposition], [xtravalue])
VALUES (1, 'EXPT', 0, 'ABA-1213')
INSERT INTO [dbo].[s4_direction_xtra] ([directionid], [xtracode], [docposition], [xtravalue])
VALUES (1, 'DIPO', 1, '123')
INSERT INTO [dbo].[s4_direction_xtra] ([directionid], [xtracode], [docposition], [xtravalue])
VALUES (5, 'STO_MOVE_ID', 0, '123')
INSERT INTO [dbo].[s4_direction_xtra] ([directionid], [xtracode], [docposition], [xtravalue])
VALUES (6, 'STO_MOVE_ID', 0, '123')
INSERT INTO [dbo].[s4_direction_xtra] ([directionid], [xtracode], [docposition], [xtravalue])
VALUES (7, 'DIDS', 0, '123')
INSERT INTO [dbo].[s4_direction_xtra] ([directionid], [xtracode], [docposition], [xtravalue])
VALUES (8, 'DIPO', 0, '123')
INSERT INTO [dbo].[s4_direction_xtra] ([directionid], [xtracode], [docposition], [xtravalue])
VALUES (10, 'DIPO', 0, '13')
INSERT INTO [dbo].[s4_direction_xtra] ([directionid], [xtracode], [docposition], [xtravalue])
VALUES (1001, 'DIPO', 0, '1301')
INSERT INTO [dbo].[s4_direction_xtra] ([directionid], [xtracode], [docposition], [xtravalue])
VALUES (16, 'DIPO', 0, '10')
INSERT INTO [dbo].[s4_direction_xtra] ([directionid], [xtracode], [docposition], [xtravalue])
VALUES (17, 'DIPO', 0, '11')
INSERT INTO [dbo].[s4_direction_xtra] ([directionid], [xtracode], [docposition], [xtravalue])
VALUES (21, 'STO_MOVE_ID', 0, '123')
-- DispatchProcProcTest_Success_Found
INSERT INTO [dbo].[s4_direction_xtra] ([directionid], [xtracode], [docposition], [xtravalue])
VALUES (19, 'DIPO', 0, '16')
INSERT INTO [dbo].[s4_direction_xtra] ([directionid], [xtracode], [docposition], [xtravalue])
VALUES (19, 'DID', 1, 'SEC')
--LogisticsDispToStoInProcTest
INSERT INTO [dbo].[s4_direction_xtra] ([directionid], [xtracode], [docposition], [xtravalue])
VALUES (25, 'DIPO', 0, '113')
--DispatchShowItems2Test_Success
INSERT INTO [dbo].[s4_direction_xtra] ([directionid], [xtracode], [docposition], [xtravalue])
VALUES (25, 'STO_MOVE_ID', 0, '513')
--LogisticsDispatchToPositionProcTest
INSERT INTO [dbo].[s4_direction_xtra] ([directionid], [xtracode], [docposition], [xtravalue])
VALUES (27, 'DIPO', 0, '113')
-- DispatchRepeatReservationProcTest_Success
INSERT INTO [dbo].[s4_direction_xtra] ([directionid], [xtracode], [docposition], [xtravalue])
VALUES (28, 'DIDS', 0, '123')
INSERT INTO [dbo].[s4_direction_xtra] ([directionid], [xtracode], [docposition], [xtravalue])
VALUES (28, 'DIPO', 0, '113')
--DispatchPrintTest_Success
INSERT INTO [dbo].[s4_direction_xtra] ([directionid], [xtracode], [docposition], [xtravalue])
VALUES (32, 'DIPO', 0, '113')
--DispatchSaveItemProcTest_Success
INSERT INTO [dbo].[s4_direction_xtra] ([directionid], [xtracode], [docposition], [xtravalue])
VALUES (33, 'DIPO', 0, '115')
--DispatchReassignProcTest_Success
INSERT INTO [dbo].[s4_direction_xtra] ([directionid], [xtracode], [docposition], [xtravalue])
VALUES (21, 'DIPO', 0, '115')
-- DispatchReassignProcTest_Non_Success
INSERT INTO [dbo].[s4_direction_xtra] ([directionid], [xtracode], [docposition], [xtravalue])
VALUES (35, 'DIPO', 0, '118')
-- PrintLabelProc_Success
INSERT INTO [dbo].[s4_direction_xtra] ([directionid], [xtracode], [docposition], [xtravalue])
VALUES (46, 'TTYPE', 0, 'Doprava DPD')
--DispatchManualSaveItemProcTest_Success
INSERT INTO [dbo].[s4_direction_xtra] ([directionid], [xtracode], [docposition], [xtravalue])
VALUES (48, 'DIPO', 0, '123')
--DispCheckAvailableAbraDirectionsTest_Success
INSERT INTO [dbo].[s4_direction_xtra] ([directionid], [xtracode], [docposition], [xtravalue])
VALUES (51, 'ABRADOCID', 0, '1234567890')
--DispCheckAvailableAbraDirectionsTest_BadDocStatus
INSERT INTO [dbo].[s4_direction_xtra] ([directionid], [xtracode], [docposition], [xtravalue])
VALUES (53, 'ABRADOCID', 0, '1234567891')
--XtraProxyDBTests_DirectionXtra_Save
INSERT INTO [dbo].[s4_direction_xtra] ([directionid], [xtracode], [docposition], [xtravalue])
VALUES (54, 'DIPO', 0, '1234567891')
--ResetShipmentProcTest
INSERT INTO [dbo].[s4_direction_xtra] ([directionid], [xtracode], [docposition], [xtravalue])
VALUES (58, 'TTYPE', 0, 'Doprava PPL')
-- AvailableInvoiceNumberDirectionsTest_Success
INSERT INTO [dbo].[s4_direction_xtra] ([directionid], [xtracode], [docposition], [xtravalue])
VALUES (51, 'INUM', 0, 'INUM1234')
-- AvailableInvoiceNumberDirectionsTest_SuccessMulti
INSERT INTO [dbo].[s4_direction_xtra] ([directionid], [xtracode], [docposition], [xtravalue])
VALUES (62, 'INUM', 0, 'INUM1234MULTI')
INSERT INTO [dbo].[s4_direction_xtra] ([directionid], [xtracode], [docposition], [xtravalue])
VALUES (63, 'INUM', 0, 'INUM1234MULTI')
-- DispatchUserAssignmentProcTest
INSERT INTO [dbo].[s4_direction_xtra] ([directionid], [xtracode], [docposition], [xtravalue])
VALUES (65, 'DIDS', 0, '123')
INSERT INTO [dbo].[s4_direction_xtra] ([directionid], [xtracode], [docposition], [xtravalue])
VALUES (65, 'DIPO', 0, '987654321')
INSERT INTO [dbo].[s4_direction_xtra] ([directionid], [xtracode], [docposition], [xtravalue])
VALUES (66, 'DIDS', 0, '123')
INSERT INTO [dbo].[s4_direction_xtra] ([directionid], [xtracode], [docposition], [xtravalue])
VALUES (66, 'ABRADOCID', 0, 'FV/21/1')
--GetDirectionsByAbraDocID_Success
INSERT INTO [dbo].[s4_direction_xtra] ([directionid], [xtracode], [docposition], [xtravalue])
VALUES (69, 'ABRADOCID', 0, '001231231203')
INSERT INTO [dbo].[s4_direction_xtra] ([directionid], [xtracode], [docposition], [xtravalue])
VALUES (70, 'ABRADOCID', 0, '01231231203')
INSERT INTO [dbo].[s4_direction_xtra] ([directionid], [xtracode], [docposition], [xtravalue])
VALUES (71, 'ABRADOCID', 0, '026114')
INSERT INTO [dbo].[s4_direction_xtra] ([directionid], [xtracode], [docposition], [xtravalue])
VALUES (72, 'ABRADOCID', 0, '251100286')
INSERT INTO [dbo].[s4_direction_xtra] ([directionid], [xtracode], [docposition], [xtravalue])
VALUES (73, 'ABRADOCID', 0, '091231231231')

 --Reference Data for s4_houseList
INSERT INTO [dbo].[s4_houseList] ([houseID],[houseDesc],[houseMap])
VALUES ('1', 'Test', null)

--Reference Data for s4_positionList
SET IDENTITY_INSERT [s4_positionList] ON;
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES     (1, '123', NULL, '1', 0,'POS_OK',0,0,0,0,null);
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES     (2, '456', NULL, '1', 32,'POS_STOCTAKING',1,1,1,1,null);
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES     (3, '789', NULL, '1', 32,'POS_STOCTAKING',2,2,2,2,null);
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES     (4, '012', NULL, '1', 32,'POS_STOCTAKING',3,3,3,3,null);
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES     (5, '345', NULL, '1', 32,'POS_STOCTAKING',4,4,3,3,null);
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES     (6, '678', NULL, '1', 32,'POS_STOCTAKING',4,5,3,3,null);
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES     (7, '901', NULL, '1', 32, 'POS_OK', 4, 6, 3, 3, null);
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES     (8, '921', NULL, '1', 0,'POS_OK',5, 1, 2, 3, null);
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES     (9, '931', NULL, '1', 0,'POS_OK',5, 3, 2, 2, null);
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES     (10, '932', NULL, '1', 0,'POS_OK',6, 4, 2, 2, null);
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES     (11, '933', NULL, '1', 0,'POS_OK',7, 5, 1, 2, null);
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES     (12, '934', NULL, '1', 0,'POS_OK',8, 6, 2, 3, null);
-- ExpedDoneTest_Success
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES     (13, 'POS_EXP', NULL, '1', 2,'POS_OK',8, 6, 2, 4, null);
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES     (1301, 'POS_EXP01', NULL, '1', 2,'POS_OK',8, 7, 2, 4, null);
-- StockTakingBeginTest_Success
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES     (14, 'POS_STOCKTAKING1', NULL, '1', 2,'POS_OK',8, 6, 2, 5, null);
-- StockTakingBeginTest_Error
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES     (15, 'POS_STOCKTAKING2', NULL, '1', 2,'POS_STOCTAKING',8, 6, 2, 6, null);
-- DispatchProcProcTest_Success_Found
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES     (16, 'EXP1', NULL, '1', 2,'POS_OK',8, 6, 2, 7, null);

-- StoreInPosErrorProcTest
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES (17, 'SIPEP', NULL, '1', 0,'POS_OK',8, 6, 2, 8, null);

INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES (21, 'OnStore21', 'OnStore21', '1', 0,'POS_OK',0,-200,-200,-200,null);
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES (22, 'OnStore22', 'OnStore22', '1', 32,'POS_OK',0,-200,-200,-201,null);
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES (23, 'REC', 'Receive pos', '1', 1,'POS_OK',0,-200,-200,-202,null);
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES (24, 'DISP', 'Dispatch pos', '1', 2,'POS_OK',0,-200,-200,-203,null);
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES (25, 'MOVE1', 'Move from pos', '1', 2,'POS_OK',0,-200,-200,-204,null);
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES (26, 'MOVE2', 'Move to pos', '1', 2,'POS_OK',0,-200,-200,-205,null);
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES (27, 'EMPTYDATA', 'Empty data', '1', 2,'POS_OK',0,-200,-200,-206,null);
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES (28, 'RESET', 'Reset pos', '1', 2,'POS_STOCTAKING',0,-200,-200,-207,null);
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES (29, 'TOCANC', 'To cancel', '1', 2,'POS_STOCTAKING',0,-200,-200,-208,null);
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES (30, 'TOCANCIT', 'To cancel item', '1', 2,'POS_STOCTAKING',0,-200,-200,-209,null);
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES (31, 'TOVALID', 'Make item valid', '1', 2,'POS_STOCTAKING',0,-200,-200,-210,null);
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES (32, 'TOCANCDIR', 'To cancel by direction', '1', 2,'POS_STOCTAKING',0,-200,-200,-211,null);
--StoreInSaveItemProc Test
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES (33, 'MOVE3', 'To change1', '1', 2,'POS_OK',0,-200,-200,-212,null);
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES (34, 'MOVE34', 'To change2', '1', 2,'POS_OK',0,-200,-200,-213,null);
--
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES (112, 'FREE', 'Scan free pos', '1', 0,'POS_OK',0, 10,10,10,null);
-- DispatchListDirectionsProcTest
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES (113, 'LOG', 'Logistic pos', '1', 2,'POS_OK',0, 11,11,11,null);
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES (114, 'LOG_REC', 'Logistic pos', '1', 1,'POS_OK',0, 12,12,12,null);
-- unsaved moves pos
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES (115, 'UNSAVED', 'Unsaved pos', '1', 2,'POS_OK',0, 13,13,13,null);
-- DispatchPosErrorProcTest
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES (116, 'ERROR', 'Error pos', '1', 2,'POS_OK',0, 14,14,14,null);
--LogisticsDispatchToPositionProcTest
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES (117, 'LOG_POS', 'LogisticsDispatchToPositionProcTest', '1', 0,'POS_OK',0, 15,15,15,null);
-- DispatchReassignProcTest_Non_Success
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES     (118, '118', NULL, '1', 0,'POS_OK',8, 16, 16, 16, null);
--RemoveExpBatchProc
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES     (119, '119', NULL, '1', 0,'POS_OK',8, 17, 17, 17, null);
--PositionChangeStatusProcTest
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES     (120, '120', NULL, '1', 0,'POS_OK',8, 18, 18, 18, null);
--removecarrierproc test
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES     (121, '121', NULL, '1', 0,'POS_OK',8, 19, 19, 19, null);
-- StoreInAcceptPosProcTest_Success
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES     (122, '122', NULL, '1', 0,'POS_OK',8, 20, 20, 20, null);
--DispatchManualSaveItemProcTest_Success
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES (123, '123POS', NULL, '1', 2,'POS_OK',8, 123, 123, 123, null);
--DirectionReceiveChangeProcTest_Success
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES (124, '124POS', NULL, '1', 0,'POS_OK',8, 124, 124, 124, null);
--ReceiveSimpleProcTest_Success
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES (125, '125POS', NULL, '1', 3,'POS_OK',8, 125, 125, 125, null);
--ReceiveSimpleProcTest_Fail
INSERT INTO [dbo].[s4_positionList]([positionID],[posCode],[posDesc],[houseID],[posCateg],[posStatus],[posHeight],[posX],[posY],[posZ],[mapPos])
VALUES (126, '126POS', NULL, '1', 0,'POS_OK',8, 126, 126, 126, null);

SET IDENTITY_INSERT [s4_positionList] OFF;


INSERT INTO [dbo].[s4_positionList_sections] ([sectID], [positionID]) 
VALUES (N'SEC', 1);
INSERT INTO [dbo].[s4_positionList_sections] ([sectID], [positionID]) 
VALUES (N'SEC1', 21);
INSERT INTO [dbo].[s4_positionList_sections] ([sectID], [positionID]) 
VALUES (N'SEC1', 112);


--Reference Data for s4_storeMove
SET IDENTITY_INSERT [s4_storeMove] ON;
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (1,'NREC','XXX',1,1,GETDATE(),29,NULL,GETDATE(),1);
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (2,'MOVE','XXX1',1,1,GETDATE(),-1,NULL,GETDATE(),1);
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (3,'NREC','XXX2',1,1,GETDATE(),-1,NULL,GETDATE(),1);
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (4,'NREC','XXX3',1,1,GETDATE(),16,NULL,GETDATE(),1);
-- DirectionCancelProcTest_Success
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (500,'NREC','XXX3',1,2,GETDATE(),1,NULL,GETDATE(),1);
-- ExpedDoneTest_Success
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (501,'NREC','XXX3',5,3,GETDATE(),10,NULL,GETDATE(),1);
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (50101,'NREC','XXX3',5,301,GETDATE(),1001,NULL,GETDATE(),1);
-- ReceiveCancelTest
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (502,'NREC','XXX4',5,4,GETDATE(),10,NULL,GETDATE(),1);
-- ReceiveCancelItemTest
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (503,'NREC','XXX4',5,5,GETDATE(),30,NULL,GETDATE(),1);
-- ReceiveDoneTest
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (504,'NREC','XXX4',5,6,GETDATE(),10,NULL,GETDATE(),1);
--StoreInProcProc Test
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (505,'NREC','XXX4',5,7,GETDATE(),23,NULL,GETDATE(),1);
--StoreInCancelItem Test
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (506,'NSTI','XXX4',5,8,GETDATE(),23,NULL,GETDATE(),1);
--StoreInSaveItemProc Test
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (507,'NSTI','XXX4',5,9,GETDATE(),24,NULL,GETDATE(),1);
--StoreInAcceptPosProcTest
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (508,'NSTI','XXX4',5,10,GETDATE(),24,NULL,GETDATE(),1);
--LogisticDispStoInProcTest
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (509,'NSTI','XXX4',5,11,GETDATE(),25,NULL,GETDATE(),1);
--DispatchNextItemProc
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (510,'NSTI','XXX4',5,12,GETDATE(),25,NULL,GETDATE(),1);
--DispatchConfirmItemProcTest
--DispatchSaveItemProcTest_Success
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (511,'NDIS','XXX4',5,13,GETDATE(),33,NULL,GETDATE(),1);
-- DispatchPosErrorProcTest
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (512,'NDIS','XXX4',5,14,GETDATE(),25,NULL,GETDATE(),1);
--DispatchShowItemsTest
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (513,'NDIS','XXX5',5,11,GETDATE(),25,NULL,GETDATE(),1);
--ReceiveCancelItemProcTest
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (514,'NDIS','XXX5',1,15,GETDATE(),26,NULL,GETDATE(),1);
--LogisticsDispatchToPositionProcTest
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (515,'NSTI','XXX4',5,16,GETDATE(),27,NULL,GETDATE(),1);
-- DirectionItemChangeProcTest_Receive_Success
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (516,'NSTI','XXX4',1,17,GETDATE(),31,NULL,GETDATE(),1);
-- Save_Basic_ChangeMoveItems
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (517,'NSTI','XXX4',1,18,GETDATE(),31,NULL,GETDATE(),1);
--DispatchPrintTest, MovePrintProcTest
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (518,'NSTI','XXX5',5,18,GETDATE(),32,NULL,GETDATE(),1);
--DispatchCancelItemProcTest_Success
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (519,'NDIS','XXX4',5,19,GETDATE(),33,NULL,GETDATE(),1);
-- DispatchReassignProcTest_Non_Success
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (520,'NDIS','XXX4',5,20,GETDATE(),35,NULL,GETDATE(),1);

--RemoveExpBatchProc
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (521,'NREC','ERBP',5,21,GETDATE(),36,NULL,GETDATE(),1);

INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (522,'NREC','ERBP',5,22,GETDATE(),37,NULL,GETDATE(),1);

INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (523,'NREC','ERBP',5,23,GETDATE(),38,NULL,GETDATE(),1);

-- DispCheckDoneProcTest_Success
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (524,'NDIS','ERBP',5,24,GETDATE(),14,NULL,GETDATE(),1);

-- DispCheckDoneProcTest_SuccessMixedBatches
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (5240,'NDIS','ERBP',5,240,GETDATE(),1400,NULL,GETDATE(),1);

-- DispCheckDoneProcTest_SuccessMixedBatches
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (5241,'NDIS','ERBP',5,241,GETDATE(),1401,NULL,GETDATE(),1);
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (5242,'NDIS','ERBP',5,242,GETDATE(),1401,NULL,GETDATE(),1);

--removecarrierproc test
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (525,'NREC','ERBP',5,25,GETDATE(),40,NULL,GETDATE(),1);

--DispatchManualSaveItemProcTest_Success
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (526,'NDIS','XXX1',5,24,GETDATE(),48,NULL,GETDATE(),1);
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (527,'NDIS','XXX12',5,24,GETDATE(),48,NULL,GETDATE(),1);

--DirectionReceiveChangeProcTest_Success
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (528,'SREC','SREC528',1,26,GETDATE(),59,NULL,GETDATE(),1);

--DirectionReceiveChangeProcTest_StoreInMoveError
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (529,'NSTI','MSTI',3,26,GETDATE(),59,NULL,GETDATE(),1);

-- DispCheckGetItemsProcTest_Success
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (530,'NDIS','XXX12',5,25,GETDATE(),13,NULL,GETDATE(),1);

INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (531,'NREC','RECSI1',5,27,GETDATE(),68,NULL,GETDATE(),1);

--Bookings
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (532,'NREC','BOOKINGFREEQTY',5,27,GETDATE(),68,NULL,GETDATE(),1);

INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (533,'NREC','AVAILBOOKING',5,27,GETDATE(),68,NULL,GETDATE(),1);

INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (534,'NREC','AVAILBOOKING2',5,27,GETDATE(),68,NULL,GETDATE(),1);

--DispatchPrintSerialNumberProcTest_Success
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (535,'NREC','SN',5,27,GETDATE(),74,NULL,GETDATE(),1);


SET IDENTITY_INSERT [s4_storeMove] OFF;

--Reference Data for s4_storeMove_lots
SET IDENTITY_INSERT [s4_storeMove_lots] ON;
INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (1, 1001, '000X', GETDATE())
INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (2, 1001, '123X', '01-01-2018')
INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (3, 1001, '1234X', '01-01-2017')
INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (40, 1001, '2345X', '01-01-2019')
INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (41, 1001, '3456X', '02-01-2019')
INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (42, 1001, '4567X', '03-01-2019')
INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (43, 1001, '5678X', '03-01-2019')
INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (44, 1001, '5679X', '03-01-2019')
--export tests
INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (45, 1001, '5680X', '03-01-2019')
INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (46, 1002, '5681X', '04-01-2019')
--RemoveExpBatchProc
INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (47, 1013, 'REBP_B', '01-01-2020')
INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (48, 1013, 'REBP_B01', '01-01-2021')
INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (49, 1013, 'REBP_B02', '01-01-2022')

INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (50, 1014, 'REBP_B1', '01-01-2020')
INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (51, 1014, 'REBP_B11', '01-01-2021')
INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (52, 1014, 'REBP_B12', '01-01-2021')

INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (53, 1015, 'REBP_B2', '01-01-2020')
INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (54, 1015, 'REBP_B21', '01-01-2021')
INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (55, 1015, 'REBP_B21', '01-01-2022')

INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (56, 1016, 'BATCH', '01-01-2020')

-- DispCheckDoneProcTest_Success + DispCheckDoneProcTest_SuccessMixedBatches
INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (57, 1017, '_NONE_', '01-01-1900')

-- DispCheckDoneProcTest_SuccessMixedBatches
INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (580, 1017, 'BATCH1', '01-01-2020')
INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (581, 1017, 'BATCH2', '02-02-2020')

-- DispCheckDoneProcTest_SerialNumbers
INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (582, 1017, 'BATCH1', '02-02-2022')
INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (583, 1017, 'BATCH2', '02-02-2022')

--removecarrierproc test
INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (59, 1019, 'BATCH', '01-01-2020')

INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (60, 1020, 'BATCH', '01-01-2020')

--DispatchManualSaveItemProcTest_Success
INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (61, 1021, 'DISMANSAVE', '01-01-2020')
INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (62, 1022, '', '')

--DirectionReceiveChangeProcTest_Success
INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (63, 1023, 'test', '02-22-2022')

-- Save_Basic_ChangeMoveItems
INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (64, 1012, '5679X', '03-01-2019')

-- DirectionItemChangeProcTest_Receive_Success
INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (65, 1002, '5679X', '03-01-2019')

-- DispCheckGetItemsProcTest_Success
INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (66, 1002, '_NONE_', '01-01-1900')

INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (67, 1024, 'BATCH1024', '01-01-2022')

-- Booking
INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (68, 1026, 'BATCH68', '01-01-2029');

INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (69, 1026, 'BATCH69', '01-01-2029');

INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (70, 1027, 'BATCH70', '01-01-2029');

INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (71, 1027, 'BATCH71', '12-12-2030');

-- BookingListAvailableLotsProc

INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (72, 1028, 'BATCH72', '12-12-2030');

INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (73, 1028, 'BATCH73', '12-12-2028');

--DispatchDirLoadTest
INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (74, 1010, 'BATCH74', '12-12-2029');

SET IDENTITY_INSERT [s4_storeMove_lots] OFF;

--Reference Data for s4_storeMove_items
SET IDENTITY_INSERT [s4_storeMove_items] ON;
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (1, 3, 1,  1, 1, 1, '0', 1, -1, -1, GETDATE(), 0, 100);
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (2, 3, 2,  1, 1, 8, '0', 1, -1, -1, GETDATE(), 0, 100);
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID])
VALUES (3, 3, 3,  1, 1, 9, '0', 1, -1, -1, GETDATE(), 0);
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID])
VALUES (4, 4, 3,  1, 3, 10, '0', 1, -1, -1, GETDATE(), 0);
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID])
VALUES (5, 4, 4,  1, 3, 10, '0', 1, -1, -1, GETDATE(), 0);
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (6, 4, 5,  1, 3, 11, '0', 1, -1, -1, GETDATE(), 0, 100);
-- ExpedDoneTest
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (7, 501, 1, 1, 3, 13, '_NONE_', 1, -1, -1, GETDATE(), 0, 100);
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (701, 50101, 1, 1, 3, 1301, '_NONE_', 1, -1, -1, GETDATE(), 0, 100);
-- ReceiveCancelTest
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (8, 502, 1, 1, 40, 1, '_NONE_', 1, -1, -1, GETDATE(), 0, 100);
-- ReceiveCancelItemTest
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (9, 503, 1, 1, 41, 1, '_NONE_', 1, 1, -1, GETDATE(), 0, 100);
-- ReceiveDoneTest
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (10, 504, 1, 1, 42, 1, '_NONE_', 1, -1, -1, GETDATE(), 0, 100);
--StoreInProcProc Test
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (11, 505, 1, 1, 42, 1, '_NONE_', 1, -1, -1, GETDATE(), 0, 100);
--StoreInCancelItem Test
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (12, 506, 1, 1, 42, 1, '_NONE_', 1, -1, -1, GETDATE(), 0, 100);
--StoreInSaveItemProc Test
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (13, 507, 1, 1, 42, 33, '_NONE_', 1, -1, -1, GETDATE(), 0, 100);
--StoreInAcceptPosProcTest
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (14, 508, 1, 1, 43, 33, 'A12345', 1, -1, -1, GETDATE(), 0, 100);
--LogisticDispStoInProcTest
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (15, 509, 1, 1, 44, 113, '_NONE_', 1, -1, -1, GETDATE(), 0, 100);
--DispatchNextItemProc
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (16, 510, 1, -1, 44, 33, '_NONE_', 1, -1, 18, GETDATE(), 0, 100);
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (17, 510, 2, -1, 44, 33, '_NONE_', 1, -1, 19, GETDATE(), 0, 100);
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (18, 510, 3, 1, 44, 113, '_NONE_', 1, -1, 16, GETDATE(), 0, 100);
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (19, 510, 4, 1, 44, 113, '_NONE_', 1, -1, 17, GETDATE(), 0, 100);
--DispatchConfirmItemProcTest
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (20, 511, 1, 1, 44, 115, '_NONE_', 1, -1, -1, GETDATE(), 0, 0);
--DispatchCancelItemProcTest_Success
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (121, 519, 2, 1, 44, 115, '_NONE_', 1, -1, -1, GETDATE(), 0, 0);
--DispatchSaveItemProcTest_Success
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (122, 511, 3, 1, 44, 115, '_NONE_', 1, 1, -1, GETDATE(), 0, 0);
--DispatchItemInstructProcTest
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (123, 511, 4, 1, 44, 115, '_NONE_', 1, -1, 124, GETDATE(), 0, 0);
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (124, 511, 5, 1, 44, 115, '_NONE_', 1, -1, 123, GETDATE(), 0, 0);
--DispatchShowItemsTest
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (125, 513, 1, -1, 43, 34, 'A12345', 1, -1, -1, GETDATE(), 0, 0);
--ReceiveCancelItemProcTest
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (126, 514, 1, 1, 43, 1, 'A12345', 1, -1, -1, GETDATE(), 0, 100);
--LogisticsDispatchToPositionProcTest
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (127, 515, 1, 1, 44, 113, '_NONE_', 1, -1, -1, GETDATE(), 0, 100);
-- DirectionItemChangeProcTest_Receive_Success
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (128, 516, 1, 1, 44, 113, '_NONE_', 1, 1, -1, GETDATE(), 0, 100);
-- Save_Basic_ChangeMoveItems
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (129, 517, 1, 1, 44, 113, '_NONE_', 5, 1, -1, GETDATE(), 0, 100);
--DispatchPrintTest
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (130, 518, 1, 1, 44, 113, '_NONE_', 1, 1, -1, GETDATE(), 0, 100);
-- DispatchReassignProcTest_Non_Success
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (131, 520, 1, 1, 44, 118, '_NONE_', 1, 1, -1, GETDATE(), 0, 100);

--RemoveExpBatchProc
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (132, 521, 1, 1, 47, 119, '_NONE_', 1, 1, -1, GETDATE(), 0, 0);
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (133, 521, 2, 1, 48, 119, '_NONE_', 1, 1, -1, GETDATE(), 0, 0);
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (134, 521, 3, 1, 49, 119, '_NONE_', 1, 1, -1, GETDATE(), 0, 0);
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (135, 521, 4, 1, 47, 119, '123', 1, 1, -1, GETDATE(), 0, 0);
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (136, 521, 5, 1, 48, 119, '123', 1, 1, -1, GETDATE(), 0, 0);
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (137, 521, 6, 1, 49, 119, '123', 1, 1, -1, GETDATE(), 0, 0);

INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (138, 522, 1, 1, 50, 119, '_NONE_', 1, 1, -1, GETDATE(), 0, 0);
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (139, 522, 2, 1, 51, 119, '_NONE_', 1, 1, -1, GETDATE(), 0, 0);
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (140, 522, 3, 1, 52, 119, '_NONE_', 1, 1, -1, GETDATE(), 0, 0);

INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (141, 523, 1, 1, 53, 119, '_NONE_', 1, 1, -1, GETDATE(), 0, 0);
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (142, 523, 2, 1, 54, 119, '_NONE_', 1, 1, -1, GETDATE(), 0, 0);
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (143, 523, 3, 1, 55, 119, '_NONE_', 1, 1, -1, GETDATE(), 0, 0);

INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (144, 521, 7, 1, 56, 119, '_NONE_', 1, 1, -1, GETDATE(), 0, 0);

-- DispCheckDoneProcTest_Success
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (145, 524, 1, -1, 57, 119, '_NONE_', 2, 1, -1, GETDATE(), 0, 100);

-- DispCheckDoneProcTest_SuccessMixedBatches
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (14600, 5240, 1, -1, 580, 119, '_NONE_', 1, 1, -1, GETDATE(), 0, 100);
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (14601, 5240, 2, -1, 581, 119, '_NONE_', 2, 1, -1, GETDATE(), 0, 100);

-- DispCheckDoneProcTest_SerialNumbers
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (14602, 5241, 1, -1, 582, 119, '_NONE_', 3, 1, -1, GETDATE(), 0, 100);
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (14603, 5242, 2, -1, 583, 119, '_NONE_', 1, 1, -1, GETDATE(), 0, 100);

--removecarrierproc test
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (147, 525, 1, 1, 59, 121, '_NONE_', 2, 1, -1, GETDATE(), 0, 0);

INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (148, 525, 2, 1, 59, 121, 'CARR1', 3, 2, -1, GETDATE(), 0, 0);

INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (149, 525, 3, 1, 60, 121, '_NONE_', 4, 3, -1, GETDATE(), 0, 0);

INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (150, 525, 4, 1, 60, 121, 'CARR1', 5, 4, -1, GETDATE(), 0, 0);

--DispatchManualSaveItemProcTest_Success
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (151, 526, 1, 1, 61, 123, '_NONE_', 1, 1, -1, GETDATE(), 0, 100);
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (152, 527, 2, 1, 62, 123, '_NONE_', 1, 2, -1, GETDATE(), 0, 100);

--DirectionReceiveChangeProcTest_Success
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (153, 528, 1, 1, 63, 1, '_NONE_', 5, 1, -1, GETDATE(), 0, 100);
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (154, 528, 2, 1, 63, 1, '_NONE_', 1, 2, -1, GETDATE(), 0, 100);
--DirectionReceiveChangeProcTest_StoreInMoveError
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (155, 529, 1, -1, 63, 1, '_NONE_', 5, 2, -1, GETDATE(), 0, 100);
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (156, 529, 2, 1, 63, 124, '_NONE_', 5, 2, -1, GETDATE(), 0, 100);
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (157, 518, 2, 1, 44, 114, '_NONE_', 1, 1, -1, GETDATE(), 0, 100);

-- DispCheckGetItemsProcTest_Success
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (158, 530, 1, -1, 44, 114, '_NONE_', 1, 1, -1, GETDATE(), 0, 100);
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (159, 530, 2, -1, 66, 114, '_NONE_', 1, 1, -1, GETDATE(), 0, 100);

INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (160, 531, 1, -1, 67, 1, '_NONE_', 1, 1, -1, GETDATE(), 0, 100);

--Bookings
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (161, 532, 1, 1, 71, 1, '_NONE_', 1, 1, -1, GETDATE(), 0, 100);

INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (162, 533, 1, 1, 72, 1, '_NONE_', 3, 1, -1, GETDATE(), 0, 100);

INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (163, 534, 1, 1, 73, 1, '_NONE_', 1, 1, -1, GETDATE(), 0, 100);

--DispatchPrintSerialNumberProcTest_Success
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID],[itemValidity])
VALUES (164, 535, 1, 1, 73, 1, '_NONE_', 1, 1, -1, GETDATE(), 0, 100);


SET IDENTITY_INSERT [s4_storeMove_items] OFF;


--Reference Data for s4_printerType
SET IDENTITY_INSERT [s4_printerType] ON;
INSERT INTO [dbo].[s4_printerType]([printerTypeID],[printerDesc]) VALUES (1, 'Laser');
INSERT INTO [dbo].[s4_printerType]([printerTypeID],[printerDesc]) VALUES (2, 'Stitky');
SET IDENTITY_INSERT [s4_printerType] OFF;

--Reference Data for s4_printerLocation
SET IDENTITY_INSERT [s4_printerLocation] ON;
INSERT INTO [dbo].[s4_printerLocation]([printerLocationID],[printerPath],[printerTypeID]) VALUES (1, 'Laser 1', 1);
INSERT INTO [dbo].[s4_printerLocation]([printerLocationID],[printerPath],[printerTypeID]) VALUES (2, 'Stitky 1', 2);
SET IDENTITY_INSERT [s4_printerLocation] OFF;

--Reference Data for s4_reports
SET IDENTITY_INSERT [s4_reports] ON;
INSERT INTO [dbo].[s4_reports]([reportID],[reportType],[template],[printerTypeID]) VALUES (1, 'packingLabel', 'PRINT {0} {1} {2}', 2);
INSERT INTO [dbo].[s4_reports]([reportID],[reportType],[template],[printerTypeID]) VALUES (2, 'carrLabel', 'PRINT {0}', 2);
INSERT INTO [dbo].[s4_reports]([reportID],[reportType],[template],[printerTypeID]) VALUES (3, 'boxLabel', 'PRINT {0} {1.DirectionID} {2.DocPosition}', 2);
INSERT INTO [dbo].[s4_reports]([reportID],[reportType],[template],[printerTypeID]) VALUES (4, 'boxLabel', 'PRINT label {0.PositionID}', 2);
INSERT INTO [dbo].[s4_reports]([reportID],[reportType],[template],[printerTypeID]) VALUES (28, 'carrLabel', 'PRINT {0}', 2);
SET IDENTITY_INSERT [s4_reports] OFF;

--set report definition
--DispatchPrintTest - 
SET IDENTITY_INSERT [s4_reports] ON;
INSERT INTO [dbo].[s4_reports]([reportID],[reportType],[template],[printerTypeID]) VALUES (5, 'WebPrint', 'xx', 1);
SET IDENTITY_INSERT [s4_reports] OFF;
UPDATE [dbo].[s4_reports]
  SET  [template] =  (select BulkColumn FROM OPENROWSET (BULK N'$(ProjectDir)\Tools\Reporty\DodaciList.srt', SINGLE_NCLOB ) as Document)
 WHERE [reportID] = 5
--OnStorePrintProcTest - 
SET IDENTITY_INSERT [s4_reports] ON;
INSERT INTO [dbo].[s4_reports]([reportID],[reportType],[template],[printerTypeID]) VALUES (6, 'WebPrint', 'xx', 1);
SET IDENTITY_INSERT [s4_reports] OFF;
UPDATE [dbo].[s4_reports]
  SET  [template] =  (select BulkColumn FROM OPENROWSET (BULK N'$(ProjectDir)\Tools\Reporty\NaSklade.srt', SINGLE_NCLOB ) as Document)
 WHERE [reportID] = 6
 --MovePrintProcTest - 
SET IDENTITY_INSERT [s4_reports] ON;
INSERT INTO [dbo].[s4_reports]([reportID],[reportType],[template],[printerTypeID]) VALUES (7, 'WebPrint', 'xx', 1);
SET IDENTITY_INSERT [s4_reports] OFF;
UPDATE [dbo].[s4_reports]
  SET  [template] =  (select BulkColumn FROM OPENROWSET (BULK N'$(ProjectDir)\Tools\Reporty\SkladovyPohyb.srt', SINGLE_NCLOB ) as Document)
 WHERE [reportID] = 7
 --ReceivePrintByDirectionProcTest - 
SET IDENTITY_INSERT [s4_reports] ON;
INSERT INTO [dbo].[s4_reports]([reportID],[reportType],[template],[printerTypeID]) VALUES (8, 'WebPrint', 'xx', 1);
SET IDENTITY_INSERT [s4_reports] OFF;
UPDATE [dbo].[s4_reports]
  SET  [template] =  (select BulkColumn FROM OPENROWSET (BULK N'$(ProjectDir)\Tools\Reporty\VydejovyList.srt', SINGLE_NCLOB ) as Document)
 WHERE [reportID] = 8
 --DispatchPrintSerialNumberProcTest
SET IDENTITY_INSERT [s4_reports] ON;
INSERT INTO [dbo].[s4_reports]([reportID],[reportType],[template],[printerTypeID]) VALUES (9, 'WebPrint', 'xx', 1);
SET IDENTITY_INSERT [s4_reports] OFF;
UPDATE [dbo].[s4_reports]
  SET  [template] =  (select BulkColumn FROM OPENROWSET (BULK N'$(ProjectDir)\Tools\Reporty\SeriovaCisla.srt', SINGLE_NCLOB ) as Document)
 WHERE [reportID] = 9
  --PrintShippingDocumentADRProcTestData.txt
SET IDENTITY_INSERT [s4_reports] ON;
INSERT INTO [dbo].[s4_reports]([reportID],[reportType],[template],[printerTypeID]) VALUES (10, 'WebPrint', 'xx', 1);
SET IDENTITY_INSERT [s4_reports] OFF;
UPDATE [dbo].[s4_reports]
  SET  [template] =  (select BulkColumn FROM OPENROWSET (BULK N'$(ProjectDir)\Tools\Reporty\PrepravniDoklad.srt', SINGLE_NCLOB ) as Document)
 WHERE [reportID] = 10

--Reference Data for s4_stocktaking
SET IDENTITY_INSERT [s4_stocktaking] ON;
INSERT INTO [dbo].[s4_stocktaking]([stotakID], [stotakStatus], [stotakDesc], [beginDate], [endDate], [entryDateTime], [entryUserID]) 
VALUES (1, 'OPENED', 'Test', GETDATE(), GETDATE(), GETDATE(), 1);
INSERT INTO [dbo].[s4_stocktaking]([stotakID], [stotakStatus], [stotakDesc], [beginDate], [endDate], [entryDateTime], [entryUserID]) 
VALUES (2, 'OPENED', 'Test2', GETDATE(), GETDATE(), GETDATE(), 1);
INSERT INTO [dbo].[s4_stocktaking]([stotakID], [stotakStatus], [stotakDesc], [beginDate], [endDate], [entryDateTime], [entryUserID]) 
VALUES (3, 'OPENED', 'Test3', GETDATE(), GETDATE(), GETDATE(), 1);
INSERT INTO [dbo].[s4_stocktaking]([stotakID], [stotakStatus], [stotakDesc], [beginDate], [endDate], [entryDateTime], [entryUserID]) 
VALUES (4, 'OPENED', 'Test4', GETDATE(), GETDATE(), GETDATE(), 1);
INSERT INTO [dbo].[s4_stocktaking]([stotakID], [stotakStatus], [stotakDesc], [beginDate], [endDate], [entryDateTime], [entryUserID]) 
VALUES (5, 'OPENED', 'Test5', GETDATE(), GETDATE(), GETDATE(), 1);
INSERT INTO [dbo].[s4_stocktaking]([stotakID], [stotakStatus], [stotakDesc], [beginDate], [endDate], [entryDateTime], [entryUserID]) 
VALUES (6, 'OPENED', 'Test6', GETDATE(), GETDATE(), GETDATE(), 1);
INSERT INTO [dbo].[s4_stocktaking]([stotakID], [stotakStatus], [stotakDesc], [beginDate], [endDate], [entryDateTime], [entryUserID]) 
VALUES (7, 'OPENED', 'Test7', GETDATE(), GETDATE(), GETDATE(), 1);
SET IDENTITY_INSERT [s4_stocktaking] OFF;

--Reference Data for s4_stocktaking_items
SET IDENTITY_INSERT [s4_stocktaking_items] ON;
INSERT INTO [dbo].[s4_stocktaking_items] (stotakItemID, [stotakID] ,[positionID] ,[artPackID] ,[quantity] ,[carrierNum] ,[batchNum] ,[expirationDate] ,[entryDateTime] ,[entryUserID]) 
VALUES (1 ,1 ,1 ,1001 ,1 ,'123Y' ,'123X' ,'01-01-2018' ,GETDATE() ,1)
INSERT INTO [dbo].[s4_stocktaking_items] (stotakItemID, [stotakID] ,[positionID] ,[artPackID] ,[quantity] ,[carrierNum] ,[batchNum] ,[expirationDate] ,[entryDateTime] ,[entryUserID]) VALUES (2 ,1 ,1 ,1001 ,1 ,'123Y' ,'123X' ,'01-01-2018' ,GETDATE() ,1)
INSERT INTO [dbo].[s4_stocktaking_items] (stotakItemID, [stotakID] ,[positionID] ,[artPackID] ,[quantity] ,[carrierNum] ,[batchNum] ,[expirationDate] ,[entryDateTime] ,[entryUserID]) VALUES (3 ,1 ,2 ,1001 ,1 ,'123Y' ,'123X' ,'01-01-2018' ,GETDATE() ,1)
INSERT INTO [dbo].[s4_stocktaking_items] (stotakItemID, [stotakID] ,[positionID] ,[artPackID] ,[quantity] ,[carrierNum] ,[batchNum] ,[expirationDate] ,[entryDateTime] ,[entryUserID]) VALUES (4 ,1 ,2 ,1001 ,1 ,'123Y' ,'123X' ,'01-01-2018' ,GETDATE() ,1)
INSERT INTO [dbo].[s4_stocktaking_items] (stotakItemID, [stotakID] ,[positionID] ,[artPackID] ,[quantity] ,[carrierNum] ,[batchNum] ,[expirationDate] ,[entryDateTime] ,[entryUserID]) VALUES (5 ,1 ,4 ,1001 ,1 ,'123Y' ,'123X' ,'01-01-2018' ,GETDATE() ,1)
INSERT INTO [dbo].[s4_stocktaking_items] (stotakItemID, [stotakID] ,[positionID] ,[artPackID] ,[quantity] ,[carrierNum] ,[batchNum] ,[expirationDate] ,[entryDateTime] ,[entryUserID]) VALUES (6 ,1 ,4 ,1001 ,1 ,'123Y' ,'123X' ,'01-01-2018' ,GETDATE() ,1)
INSERT INTO [dbo].[s4_stocktaking_items] (stotakItemID, [stotakID] ,[positionID] ,[artPackID] ,[quantity] ,[carrierNum] ,[batchNum] ,[expirationDate] ,[entryDateTime] ,[entryUserID]) VALUES (7 ,2 ,1 ,1001 ,1 ,'123Y' ,'123X' ,'01-01-2018' ,GETDATE() ,1)
INSERT INTO [dbo].[s4_stocktaking_items] (stotakItemID, [stotakID] ,[positionID] ,[artPackID] ,[quantity] ,[carrierNum] ,[batchNum] ,[expirationDate] ,[entryDateTime] ,[entryUserID]) VALUES (8 ,2 ,1 ,1001 ,1 ,'123Y' ,'123X' ,'01-01-2018' ,GETDATE() ,1)
INSERT INTO [dbo].[s4_stocktaking_items] (stotakItemID, [stotakID] ,[positionID] ,[artPackID] ,[quantity] ,[carrierNum] ,[batchNum] ,[expirationDate] ,[entryDateTime] ,[entryUserID]) VALUES (9 ,1 ,1 ,1001 ,1 ,'123Y' ,'123X' ,'01-01-2018' ,GETDATE() ,1)
INSERT INTO [dbo].[s4_stocktaking_items] (stotakItemID, [stotakID] ,[positionID] ,[artPackID] ,[quantity] ,[carrierNum] ,[batchNum] ,[expirationDate] ,[entryDateTime] ,[entryUserID]) VALUES (10 ,1 ,1 ,1001 ,1 ,'123Y' ,'123X' ,'01-01-2018' ,GETDATE() ,1)
INSERT INTO [dbo].[s4_stocktaking_items] (stotakItemID, [stotakID] ,[positionID] ,[artPackID] ,[quantity] ,[carrierNum] ,[batchNum] ,[expirationDate] ,[entryDateTime] ,[entryUserID]) VALUES (11 ,4 ,5 ,1001 ,1 ,'123Y' ,'123X' ,'01-01-2018' ,GETDATE() ,1)
INSERT INTO [dbo].[s4_stocktaking_items] (stotakItemID, [stotakID] ,[positionID] ,[artPackID] ,[quantity] ,[carrierNum] ,[batchNum] ,[expirationDate] ,[entryDateTime] ,[entryUserID]) VALUES (12 ,4 ,6 ,1001 ,1 ,'123Y' ,'123X' ,'01-01-2018' ,GETDATE() ,1)
SET IDENTITY_INSERT [s4_stocktaking_items] OFF;

--Reference Data for s4_stocktaking_positions
SET IDENTITY_INSERT [s4_stocktaking_positions] ON;
INSERT INTO [dbo].[s4_stocktaking_positions] (stotakPosID ,[stotakID] ,[positionID] ,[checkCounter]) VALUES (1 ,1 ,1 ,0)
INSERT INTO [dbo].[s4_stocktaking_positions] (stotakPosID ,[stotakID] ,[positionID] ,[checkCounter]) VALUES (2 ,1 ,2 ,0)
INSERT INTO [dbo].[s4_stocktaking_positions] (stotakPosID ,[stotakID] ,[positionID] ,[checkCounter]) VALUES (3 ,1 ,3 ,0)
INSERT INTO [dbo].[s4_stocktaking_positions] (stotakPosID ,[stotakID] ,[positionID] ,[checkCounter]) VALUES (4 ,1 ,4 ,0)
INSERT INTO [dbo].[s4_stocktaking_positions] (stotakPosID ,[stotakID] ,[positionID] ,[checkCounter]) VALUES (5 ,2 ,1 ,10)
INSERT INTO [dbo].[s4_stocktaking_positions] (stotakPosID ,[stotakID] ,[positionID] ,[checkCounter]) VALUES (6 ,2 ,2 ,10)
INSERT INTO [dbo].[s4_stocktaking_positions] (stotakPosID ,[stotakID] ,[positionID] ,[checkCounter]) VALUES (7 ,2 ,3 ,10)
INSERT INTO [dbo].[s4_stocktaking_positions] (stotakPosID ,[stotakID] ,[positionID] ,[checkCounter]) VALUES (8 ,4 ,5 ,0)
INSERT INTO [dbo].[s4_stocktaking_positions] (stotakPosID ,[stotakID] ,[positionID] ,[checkCounter]) VALUES (9 ,4 ,6 ,0)
INSERT INTO [dbo].[s4_stocktaking_positions] (stotakPosID ,[stotakID] ,[positionID] ,[checkCounter]) VALUES (10 ,6 ,3 ,0)
SET IDENTITY_INSERT [s4_stocktaking_positions] OFF;

--Reference Data for s4_stocktaking_snapshots
SET IDENTITY_INSERT [s4_stocktaking_snapshots] ON;
INSERT INTO [dbo].[s4_stocktaking_snapshots] ([stotakSnapID], [stotakID], [snapshotClass], [positionID], [artPackID], [carrierNum], [quantity], [batchNum], [expirationDate], [unitPrice])
VALUES (1 ,1 ,0 ,4 ,1001 ,'XX1' ,1 ,'YY' ,'01-01-2019' ,1)
INSERT INTO [dbo].[s4_stocktaking_snapshots] ([stotakSnapID], [stotakID], [snapshotClass], [positionID], [artPackID], [carrierNum], [quantity], [batchNum], [expirationDate], [unitPrice])
VALUES (2 ,2 ,0 ,4 ,1001 ,'XX2' ,1 ,'YY' ,'01-01-2019' ,1)
INSERT INTO [dbo].[s4_stocktaking_snapshots] ([stotakSnapID], [stotakID], [snapshotClass], [positionID], [artPackID], [carrierNum], [quantity], [batchNum], [expirationDate], [unitPrice])
VALUES (3 ,7 ,0 ,1 ,1001 ,'12345X' ,1 ,'123X' ,'01-01-2024' ,2)
INSERT INTO [dbo].[s4_stocktaking_snapshots] ([stotakSnapID], [stotakID], [snapshotClass], [positionID], [artPackID], [carrierNum], [quantity], [batchNum], [expirationDate], [unitPrice])
VALUES (4 ,7 ,0 ,1 ,1001 ,'12345X' ,1 ,'12345YY' ,'01-01-2025' ,1)
SET IDENTITY_INSERT [s4_stocktaking_snapshots] OFF;

--Reference Data for s4_prefixList
INSERT INTO [dbo].[s4_prefixList] ([docClass] ,[docNumPrefix] ,[prefixDesc] ,[docDirection] ,[docType] ,[prefixEnabled] ,[prefixSection] ,[prefixSettings])
VALUES ('DI', 'PR', 'Příjemka', 1, 0, 1, 'SEC1', NULL)
INSERT INTO [dbo].[s4_prefixList] ([docClass] ,[docNumPrefix] ,[prefixDesc] ,[docDirection] ,[docType] ,[prefixEnabled] ,[prefixSection] ,[prefixSettings])
VALUES ('DI', 'DL', 'DL', -1, 0, 1, 'SEC1', NULL)

--Reference Data for s4_settings
INSERT INTO [dbo].[s4_settings] ([setFieldID], [setDataType], [setValue])
VALUES ('dispCheckMethodDefault', 'System.Int32', '1')

INSERT INTO [dbo].[s4_settings] ([setFieldID], [setDataType], [setValue])
VALUES ('directPrefixManReceive', 'System.String', 'MREC')

INSERT INTO [dbo].[s4_settings] ([setFieldID], [setDataType], [setValue])
VALUES ('directPrefixLogReceive', 'System.String', 'PVCS')

INSERT INTO [dbo].[s4_settings] ([setFieldID], [setDataType], [setValue])
VALUES ('dispatch/default/position/id','System.Int32','123')


INSERT INTO [s4_manufactList] ([manufID],[manufName],[manufStatus],[manufSettings],[source_id],[useBatch],[useExpiration],[sectID])
VALUES ('OnStoreMan', 'OnStoreMan', 0, NULL, NULL, 0, 0, NULL);
INSERT INTO [s4_manufactList] ([manufID],[manufName],[manufStatus],[manufSettings],[source_id],[useBatch],[useExpiration],[sectID])
VALUES ('OnStoreMan2', 'OnStoreMan2', 0, NULL, NULL, 0, 0, NULL);

SET IDENTITY_INSERT [s4_articleList] ON;
INSERT INTO [s4_articleList] ([articleID],[articleCode],[articleDesc],[articleStatus],[articleType],[source_id],[sectID],[unitDesc],[manufID],[useBatch],[mixBatch],[useExpiration])
VALUES (200, 'OnStore123', 'Zboží OnStore 123', 'AR_OK', 0, 'OnStore123', NULL, NULL, 'OnStoreMan', 1, 1, 1);
INSERT INTO [s4_articleList] ([articleID],[articleCode],[articleDesc],[articleStatus],[articleType],[source_id],[sectID],[unitDesc],[manufID],[useBatch],[mixBatch],[useExpiration])
VALUES (201, 'OnStore456', 'Zboží OnStore 456', 'AR_OK', 0, 'OnStore456', NULL, NULL, 'OnStoreMan', 1, 1, 1);
INSERT INTO [s4_articleList] ([articleID],[articleCode],[articleDesc],[articleStatus],[articleType],[source_id],[sectID],[unitDesc],[manufID],[useBatch],[mixBatch],[useExpiration])
VALUES (202, 'OnStore789', 'Zboží OnStore 789', 'AR_OK', 0, 'OnStore789', NULL, NULL, 'OnStoreMan', 1, 1, 1);
INSERT INTO [s4_articleList] ([articleID],[articleCode],[articleDesc],[articleStatus],[articleType],[source_id],[sectID],[unitDesc],[manufID],[useBatch],[mixBatch],[useExpiration])
VALUES (203, 'OnStoreABC', 'Zboží OnStore ABC', 'AR_OK', 0, 'OnStoreABC', NULL, NULL, 'OnStoreMan2', 1, 1, 1);
SET IDENTITY_INSERT [s4_articleList] OFF;

SET IDENTITY_INSERT [s4_articleList_packings] ON;
INSERT INTO [s4_articleList_packings] ([artPackID],[articleID],[packDesc],[packRelation],[movablePack],[source_id],[packStatus],[barCodeType],[barCode],[inclCarrier],[packWeight],[packVolume])
VALUES (994, 200, 'ks', 1, 1, 'OnStore123', 'AP_OK', 0, null, 0, 0, 0);
INSERT INTO [s4_articleList_packings] ([artPackID],[articleID],[packDesc],[packRelation],[movablePack],[source_id],[packStatus],[barCodeType],[barCode],[inclCarrier],[packWeight],[packVolume])
VALUES (995, 201, 'ks', 1, 1, 'OnStore456', 'AP_OK', 1, 'XX12345', 0, 0, 0);
INSERT INTO [s4_articleList_packings] ([artPackID],[articleID],[packDesc],[packRelation],[movablePack],[source_id],[packStatus],[barCodeType],[barCode],[inclCarrier],[packWeight],[packVolume])
VALUES (996, 202, 'ks', 1, 1, 'OnStore789', 'AP_OK', 0, NULL, 0, 0, 0);
INSERT INTO [s4_articleList_packings] ([artPackID],[articleID],[packDesc],[packRelation],[movablePack],[source_id],[packStatus],[barCodeType],[barCode],[inclCarrier],[packWeight],[packVolume])
VALUES (997, 203, 'ks', 1, 1, 'OnStoreABC', 'AP_OK', 0, NULL, 0, 0, 0);
SET IDENTITY_INSERT [s4_articleList_packings] OFF;

SET IDENTITY_INSERT [s4_storeMove_lots] ON;
INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (4, 994, '?', '1900-01-01')
INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (5, 995, 'OnStoreBatch1', '2018-01-01')
INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (6, 996, 'OnStoreBatch2', '2017-01-01')
INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (7, 997, 'BATCH1', '1900-01-01')
INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (8, 997, '_NONE_', '1900-01-01')
INSERT INTO [dbo].[s4_storeMove_lots] ([stoMoveLotID],[artPackID],[batchNum],[expirationDate])
VALUES (112, 1012, '_NONE_', '1900-01-01')
SET IDENTITY_INSERT [s4_storeMove_lots] OFF;

SET IDENTITY_INSERT [s4_direction] ON;
-- Save_Basic_CancelByDirection
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (20, 'DCHE', 'DL', -1, '16', 9, GETDATE(), 1, 1, -1, GETDATE());
SET IDENTITY_INSERT [s4_direction] OFF;

SET IDENTITY_INSERT [s4_storeMove] ON;
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (5,'NREC','OnStorePrefix',1,1,GETDATE(),-1,NULL,GETDATE(),'USER');
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (6,'MOVE','OnStorePrefix',1,2,GETDATE(),-1,NULL,GETDATE(),'USER');
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (7,'MOVE','OnStorePrefix',1,3,GETDATE(),-1,NULL,GETDATE(),'USER');
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (8,'MOVE','OnStorePrefix',1,4,GETDATE(),16,NULL,GETDATE(),'USER');
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (9,'MOVE','OnStorePrefix',1,5,GETDATE(),16,NULL,GETDATE(),'USER');
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (10,'MOVE','OnStorePrefix',1,6,GETDATE(),16,NULL,GETDATE(),'USER');
-- canceled move & items
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (11,'CANC','OnStorePrefix',1,7,GETDATE(),-1,'Move canceled',GETDATE(),'USER');
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (12,'MOVE','OnStorePrefix',1,8,GETDATE(),-1,'Items canceled',GETDATE(),'USER');
-- Save_Basic_CancelMove
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (13,'MOVE','OnStorePrefix',1,9,GETDATE(),-1,'To cancel',GETDATE(),'USER');
-- Save_Basic_CancelMoveItem
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (14,'MOVE','OnStorePrefix',1,10,GETDATE(),-1,'To cancel item',GETDATE(),'USER');
-- Save_Basic_ValidateMoveItem
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (15,'MOVE','OnStorePrefix',1,11,GETDATE(),-1,'To cancel item',GETDATE(),'USER');
-- Save_Basic_CancelByDirection
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (16,'MOVE','OnStorePrefix',1,12,GETDATE(),20,'To cancel item',GETDATE(),'USER');
--
INSERT INTO [s4_storeMove] ([stoMoveID],[docStatus],[docNumPrefix],[docType],[docNumber],[docDate],[parent_directionID],[docRemark],[entryDateTime],[entryUserID])
VALUES (112,'NREC','OnStorePrefix',1,13,GETDATE(),-1,NULL,GETDATE(),'USER');
SET IDENTITY_INSERT [s4_storeMove] OFF;

SET IDENTITY_INSERT [s4_storeMove_items] ON;
-- receive
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemValidity],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID])
VALUES (21, 5, 1, 100, 1, 4, 21, '_NONE_', 1, -1, -1, GETDATE(), 'USER');
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemValidity],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID])
VALUES (22, 5, 2, 100, 1, 5, 21, '18062908230392527211', 10, -1, -1, GETDATE(), 'USER');
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemValidity],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID])
VALUES (23, 5, 3, 100, 1, 6, 21, '18062908230392527211', 100, -1, -1, GETDATE(), 'USER');
-- move
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemValidity],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID])
VALUES (24, 6, 1, 100, -1, 5, 21, '18062908230392527211', 4, -1, -1, GETDATE(), 'USER');
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemValidity],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID])
VALUES (25, 6, 2, 100, 1, 5, 22, '18062908230392527211', 4, -1, -1, GETDATE(), 'USER');
-- notValid move
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemValidity],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID])
VALUES (26, 7, 1, 0, -1, 6, 21, '18062908230392527211', 5, -1, -1, GETDATE(), 'USER');
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemValidity],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID])
VALUES (27, 7, 2, 0, 1, 6, 22, '18062908230392527211', 5, -1, -1, GETDATE(), 'USER');
-- Save_Basic_StoreOut
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemValidity],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID])
VALUES (28, 8, 1, 100, 1, 7, 24, 'CARRIER1', 33, -1, -1, GETDATE(), 'USER');
-- Save_Basic_Move
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemValidity],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID])
VALUES (29, 9, 1, 100, 1, 7, 25, 'CARRIER1', 50, -1, -1, GETDATE(), 'USER');
-- OnStore_Basic_ByArticle_emptyDataProxy
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemValidity],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID])
VALUES (30, 9, 2, 100, 1, 8, 27, '_NONE_', 5, -1, -1, GETDATE(), 'USER');
-- OnStore_Basic_ByArticle_emptyDataProxy
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemValidity],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID])
VALUES (31, 10, 1, 100, 1, 8, 28, '_NONE_', 5, -1, -1, GETDATE(), 'USER');
-- canceled move & items
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemValidity],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID])
VALUES (32, 12, 1, 255, 1, 8, 28, '_NONE_', 5, -1, -1, GETDATE(), 'USER');
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemValidity],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID])
VALUES (33, 12, 2, 100, 1, 8, 28, '_NONE_', 5, -1, -999, GETDATE(), 'USER');
-- Save_Basic_CancelMove
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemValidity],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID])
VALUES (34, 13, 1, 100, 1, 8, 29, '_NONE_', 5, -1, -1, GETDATE(), 'USER');
-- Save_Basic_CancelMoveItem
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemValidity],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID])
VALUES (35, 14, 1, 100, 1, 8, 30, '_NONE_', 5, -1, 36, GETDATE(), 'USER');
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemValidity],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID])
VALUES (36, 14, 2, 100, 1, 8, 30, '_NONE_', 10, -1, 35, GETDATE(), 'USER');
-- Save_Basic_ValidateMoveItem
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemValidity],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID])
VALUES (37, 15, 1, 0, 1, 8, 31, '_NONE_', 5, -1, 38, GETDATE(), 'USER');
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemValidity],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID])
VALUES (38, 15, 2, 0, 1, 8, 31, '_NONE_', 10, -1, 37, GETDATE(), 'USER');
-- Save_Basic_CancelByDirection
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemValidity],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID])
VALUES (39, 16, 2, 100, 1, 8, 32, '_NONE_', 10, -1, 37, GETDATE(), 'USER');
--
INSERT INTO [dbo].[s4_storeMove_items] ([stoMoveItemID],[stoMoveID],[docPosition],[itemValidity],[itemDirection],[stoMoveLotID],[positionID],[carrierNum],[quantity],[parent_docPosition],[brotherID],[entryDateTime],[entryUserID])
VALUES (112, 112, 3, 100, 1, 112, 112, '_NONE_', 0, -1, -1, GETDATE(), 'USER');
SET IDENTITY_INSERT [s4_storeMove_items] OFF;


-- StoreInListDirectsProcTest_Success
SET IDENTITY_INSERT [s4_direction] ON;
INSERT INTO [s4_direction] ([directionID],[docStatus],[docNumPrefix],[docDirection],[docYear],[docNumber],[docDate],[partnerID],[partAddrID],[prepare_directionID],[entryDateTime])
VALUES (100, 'WSTI', 'PRIJ', 1, '16', 9, GETDATE(), 1, 1, -1, GETDATE());
SET IDENTITY_INSERT [s4_direction] OFF;

INSERT INTO [dbo].[s4_direction_assign] ([directionID],[jobID],[workerID],[assignParams],[entryDateTime],[entryUserID])
VALUES (100, 'STIN', 'WRK1', NULL, GETDATE(),'Test1')

--MessageBusSetStatusProcTest
SET IDENTITY_INSERT [s4_messageBus] ON;
INSERT INTO [dbo].[s4_messageBus] ([messageBusID],[messageStatus],[messageType],[messageData],[tryCount],[nextTryTime],[errorDesc],[lastDateTime],[entryDateTime])
     VALUES (1, 'ERROR', 0, '{"userID":"MASIN"}', 1, '2020-04-05 10:15:00', 'Chyba při odesílání mailu', GETDATE(), GETDATE())
SET IDENTITY_INSERT [s4_messageBus] OFF;


SET IDENTITY_INSERT [s4_deliveryManifest] ON;

--ManifestCreatedProcTest
INSERT INTO [dbo].[s4_deliveryManifest]([deliveryManifestID],[manifestStatus],[deliveryProvider],[createManifestAttempt],[createManifestError],[manifestRefNumber],[manifestCreated],[manifestChanged])
VALUES(1, 'WAIT', 2, NULL, NULL, 1, GETDATE(), NULL);

INSERT INTO [dbo].[s4_deliveryManifest]([deliveryManifestID],[manifestStatus],[deliveryProvider],[createManifestAttempt],[createManifestError],[manifestRefNumber],[manifestCreated],[manifestChanged])
VALUES(2, 'WAIT', 2, NULL, NULL, 2, GETDATE(), NULL);

SET IDENTITY_INSERT [s4_deliveryManifest] OFF;


SET IDENTITY_INSERT [s4_deliveryDirection] ON;

--GetDirectionsForManifestProcTest
INSERT INTO [dbo].[s4_deliveryDirection] ([deliveryDirectionID],[directionID],[deliveryStatus],[deliveryProvider],[createShipmentAttempt],[createShipmentError],[shipmentRefNumber],[deliveryManifestID],[deliveryCreated],[deliveryChanged])
VALUES (1, 41, 'DD_OK', 2, 0, NULL, 41, NULL, GETDATE(), NULL);

--ManifestCreatedProcTest
INSERT INTO [dbo].[s4_deliveryDirection] ([deliveryDirectionID],[directionID],[deliveryStatus],[deliveryProvider],[createShipmentAttempt],[createShipmentError],[shipmentRefNumber],[deliveryManifestID],[deliveryCreated],[deliveryChanged])
VALUES (2, 44, 'DD_OK', 2, 1, NULL, 44, 1, GETDATE(), GETDATE());

INSERT INTO [dbo].[s4_deliveryDirection] ([deliveryDirectionID],[directionID],[deliveryStatus],[deliveryProvider],[createShipmentAttempt],[createShipmentError],[shipmentRefNumber],[deliveryManifestID],[deliveryCreated],[deliveryChanged])
VALUES (3, 45, 'DD_OK', 2, 1, NULL, 45, 1, GETDATE(), GETDATE());

INSERT INTO [dbo].[s4_deliveryDirection] ([deliveryDirectionID],[directionID],[deliveryStatus],[deliveryProvider],[createShipmentAttempt],[createShipmentError],[shipmentRefNumber],[deliveryManifestID],[deliveryCreated],[deliveryChanged])
VALUES (4, 42, 'DD_OK', 2, NULL, NULL, 42, 2, GETDATE(), GETDATE());

INSERT INTO [dbo].[s4_deliveryDirection] ([deliveryDirectionID],[directionID],[deliveryStatus],[deliveryProvider],[createShipmentAttempt],[createShipmentError],[shipmentRefNumber],[deliveryManifestID],[deliveryCreated],[deliveryChanged])
VALUES (5, 43, 'DD_OK', 2, 1, NULL, 43, 2, GETDATE(), GETDATE());

--ResetShipmentProcTest
INSERT INTO [dbo].[s4_deliveryDirection] ([deliveryDirectionID],[directionID],[deliveryStatus],[deliveryProvider],[createShipmentAttempt],[createShipmentError],[shipmentRefNumber],[deliveryManifestID],[deliveryCreated],[deliveryChanged])
VALUES (6, 58, 'DD_OK', 2, 1, NULL, 58, 2, GETDATE(), GETDATE());

--DirectionDeleteProcTest_Success
INSERT INTO [dbo].[s4_deliveryDirection] ([deliveryDirectionID],[directionID],[deliveryStatus],[deliveryProvider],[createShipmentAttempt],[createShipmentError],[shipmentRefNumber],[deliveryManifestID],[deliveryCreated],[deliveryChanged])
VALUES (7, 2, 'DD_CANCELED', 2, 1, NULL, 58, 2, GETDATE(), GETDATE());

INSERT INTO [dbo].[s4_deliveryDirection] ([deliveryDirectionID],[directionID],[deliveryStatus],[deliveryProvider],[createShipmentAttempt],[createShipmentError],[shipmentRefNumber],[deliveryManifestID],[deliveryCreated],[deliveryChanged])
VALUES (8, 2, 'DD_OK', 2, 1, NULL, 58, 2, GETDATE(), GETDATE());

SET IDENTITY_INSERT [s4_deliveryDirection] OFF;

--s4_deliveryDirection_items
INSERT INTO [dbo].[s4_deliveryDirection_items] (deliveryDirectionID, parcelPosition, parcelRefNumber) VALUES (7, 1, 1);
INSERT INTO [dbo].[s4_deliveryDirection_items] (deliveryDirectionID, parcelPosition, parcelRefNumber) VALUES (8, 1, 1);


SET IDENTITY_INSERT [s4_articleList_packing_barcodes] ON;

--Test_Update
INSERT INTO [dbo].[s4_articleList_packing_barcodes] ([artPackBarCodeID],[articleID],[artPackID],[barCode])
VALUES (1, 126, 1024, '11111');
INSERT INTO [dbo].[s4_articleList_packing_barcodes] ([artPackBarCodeID],[articleID],[artPackID],[barCode])
VALUES (2, 126, 1024, '22222');

SET IDENTITY_INSERT [s4_articleList_packing_barcodes] OFF;

SET IDENTITY_INSERT [s4_invoice] ON;

--DispatchUserAssignmentProcTest_Success
INSERT INTO [dbo].[s4_invoice] ([invoiceID], [docStatus],[docNumber],[docPrefix],[docYear],[lastDateTime],[entryDateTime])
VALUES (1, 'LOAD', 1, 'FV', 21, GETDATE(), GETDATE());
    
SET IDENTITY_INSERT [s4_invoice] OFF;




--Bookings
SET IDENTITY_INSERT [s4_partnerList_addresses] ON;
INSERT INTO [dbo].[s4_partnerList_addresses] ([partAddrID], [partnerID], [addrName], [source_id])
     VALUES (99, 0, 'Prázdný', 'xx')
SET IDENTITY_INSERT [s4_partnerList_addresses] OFF;


SET IDENTITY_INSERT [s4_bookings] ON;

INSERT INTO [dbo].[s4_bookings] ([bookingID], [bookingStatus], [stoMoveLotID], [partnerID], [partAddrID], [requiredQuantity], [bookingTimeout], [usedInDirectionID], [deletedByEntryUserID], [deletedDateTime], [entryDateTime], [entryUserID])
VALUES (1, 'VALID', 68, 0, 99, 2, '01-01-2029', 0, NULL, NULL, GETDATE(), 1);

INSERT INTO [dbo].[s4_bookings] ([bookingID], [bookingStatus], [stoMoveLotID], [partnerID], [partAddrID], [requiredQuantity], [bookingTimeout], [usedInDirectionID], [deletedByEntryUserID], [deletedDateTime], [entryDateTime], [entryUserID])
VALUES (2, 'VALID', 70, 0, 99, 2, '01-01-2029', 0, NULL, NULL, GETDATE(), 1);

INSERT INTO [dbo].[s4_bookings] ([bookingID], [bookingStatus], [stoMoveLotID], [partnerID], [partAddrID], [requiredQuantity], [bookingTimeout], [usedInDirectionID], [deletedByEntryUserID], [deletedDateTime], [entryDateTime], [entryUserID])
VALUES (3, 'VALID', 72, 0, 99, 1, '01-01-2029', 0, NULL, NULL, GETDATE(), 1);

INSERT INTO [dbo].[s4_bookings] ([bookingID], [bookingStatus], [stoMoveLotID], [partnerID], [partAddrID], [requiredQuantity], [bookingTimeout], [usedInDirectionID], [deletedByEntryUserID], [deletedDateTime], [entryDateTime], [entryUserID])
VALUES (4, 'VALID', 72, 0, 99, 1, '01-01-2025', 0, NULL, NULL, GETDATE(), 1);

INSERT INTO [dbo].[s4_bookings] ([bookingID], [bookingStatus], [stoMoveLotID], [partnerID], [partAddrID], [requiredQuantity], [bookingTimeout], [usedInDirectionID], [deletedByEntryUserID], [deletedDateTime], [entryDateTime], [entryUserID])
VALUES (5, 'VALID', 72, 0, 99, 1, '01-01-2020', 0, NULL, NULL, GETDATE(), 1);

INSERT INTO [dbo].[s4_bookings] ([bookingID], [bookingStatus], [stoMoveLotID], [partnerID], [partAddrID], [requiredQuantity], [bookingTimeout], [usedInDirectionID], [deletedByEntryUserID], [deletedDateTime], [entryDateTime], [entryUserID])
VALUES (6, 'VALID', 73, 0, 99, 1, '01-01-2030', 0, NULL, NULL, GETDATE(), 1);

SET IDENTITY_INSERT [s4_bookings] OFF;

--s4_storemove_serials
SET IDENTITY_INSERT s4_storemove_serials ON;

--DispatchPrintSerialNumberProcTest_Success
INSERT INTO [dbo].[s4_storeMove_serials] (serialID, stoMoveItemID, serialNumber) VALUES (1, 164, '123456789')

SET IDENTITY_INSERT s4_storemove_serials OFF;

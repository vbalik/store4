﻿using S4.Core;
using S4.Entities;
using S4.Entities.Helpers;
using S4.ProcedureModels.Move;
using S4.Procedures.Move.Helper;
using S4.StoreCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace S4.Procedures.Move
{
    public class MoveSaveProc : ProceduresBase<MoveSave>
    {

        public MoveSaveProc(string userID) : base(userID)
        {
        }

        protected override MoveSave DoIt(MoveSave procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new MoveSave() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            var result = new MoveSave
            {
                // check positions
                PositionResultSource = Helper.MoveHelper.CheckPositionAvailable(procModel.SourcePositionID),
                PositionResultDest = Helper.MoveHelper.CheckPositionAvailable(procModel.DestPositionID)
            };

            if (result.PositionResultSource != MoveStatusEnum.OK || result.PositionResultDest != MoveStatusEnum.OK)
            {
                result.Success = false;
                var errText = new List<string>();
                if(result.PositionResultSource != MoveStatusEnum.OK) 
                    errText.Add($"Chyba zdrojové pozice: {ToText(result.PositionResultSource)}");
                if(result.PositionResultDest != MoveStatusEnum.OK)
                    errText.Add($"Chyba cílové pozice: {ToText(result.PositionResultDest)}");
                result.ErrorText = String.Join("; ", errText.ToArray());
                return result;
            }


            // get source position content
            var onPosition = Helper.MoveHelper.GetArticles(procModel.SourcePositionID);

            // first step - fill article.expirationDate from on position
            foreach (var article in procModel.Articles)
            {
                var pos = (from p in onPosition where p.ArtPackID == article.ArtPackID && CompareBatchNum(article, p) && CompareExpirationDate(article, p) select p).FirstOrDefault();
                // if article not found in onPosition, break the procedure
                if (pos == null)
                {
                    result.Success = false;
                    result.ErrorText = $"Karta neexistuje na zdrojové pozici; ArticleCode: {article.ArticleCode}";
                    return result;
                }
            }

            // second step - check if onPosition contains all items from procModel.Articles
            var containsAll = onPosition.ContainsAll(procModel.Articles);
            if (!containsAll.result)
            {
                result.Success = false;
                result.ErrorText = $"Karta neexistuje na zdrojové pozici; ArticleCode: {containsAll.articleInfo.ArticleCode}";
                return result;
            }

            // make saveItemList
            var saveItemList = procModel.Articles.Select(item => new MoveItem()
            {
                ArtPackID = item.ArtPackID,
                BatchNum = item.BatchNum,
                CarrierNum = item.CarrierNum,
                ExpirationDate = item.ExpirationDate,
                Quantity = item.Quantity,

                SourcePositionID = procModel.SourcePositionID,
                DestinationPositionID = procModel.DestPositionID
            }).ToList();

            // save
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                using (var tr = db.GetTransaction())
                {
                    var save = Singleton<ServicesFactory>.Instance.GetSave();
                    result.StoMoveID = save.Move(db, _userID, StoreMove.MO_PREFIX_MOVE, StoreMove.MO_DOCTYPE_MOVE, 
                        saveItemList, true, 
                        (sm) =>
                        {
                            if(!String.IsNullOrWhiteSpace(procModel.Remark))
                                sm.DocRemark = procModel.Remark;
                        }, 
                        (si, smi) =>
                        {                           
                            // if ChangeCarrier, set income CarrierNum to procModel.DestCarrierNum
                            if (procModel.ChangeCarrier && smi.ItemDirection == StoreMoveItem.MI_DIRECTION_IN)
                                smi.CarrierNum = procModel.DestCarrierNum;
                        },
                        out string msg);
                    if (result.StoMoveID == -1)
                        return new MoveSave() { ErrorText = msg };

                    tr.Complete();
                }
            }

            result.Success = true;
            return result;
        }


        private static bool CompareBatchNum(ArticleInfo a1, ArticleInfo a2)
        {
            if (a1.BatchNum == null && a2.BatchNum == null)
                return true;
            else if (a1.BatchNum == null || a2.BatchNum == null)
                return false;
            else
                return a1.BatchNum.ToLower().Equals(a2.BatchNum.ToLower());
        }

        private static bool CompareExpirationDate(ArticleInfo a1, ArticleInfo a2)
        {
            if (a1.ExpirationDate == null && a2.ExpirationDate == null)
                return true;
            else if (a1.ExpirationDate == null || a2.ExpirationDate == null)
                return false;
            else
                return a1.ExpirationDate?.ToString("dd.M.yy") == a2.ExpirationDate?.ToString("dd.M.yy");
        }


        private object ToText(MoveStatusEnum positionResult)
        {
            switch (positionResult)
            {
                case MoveStatusEnum.OK:
                    return "OK";
                case MoveStatusEnum.Locked:
                    return "Uzamčeno";
                case MoveStatusEnum.NotSaved:
                    return "Neuloženo";
                case MoveStatusEnum.NotFound:
                    return "Nenalezeno";
                default:
                    throw new NotImplementedException(positionResult.ToString());
            }
        }
    }
}

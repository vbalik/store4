﻿using NPoco;
using S4.Core;
using S4.Entities;
using S4.Procedures.DBHelpers;
using S4.ProcedureModels.Move;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Procedures.Move.Helper
{
    public static class MoveHelper
    {
        public static MoveStatusEnum CheckPositionAvailable(int positionID)
        {

            var pos = Singleton<DAL.Cache.PositionCache>.Instance.GetItem(positionID);
            if (pos == null)
                return MoveStatusEnum.NotFound;

            if (pos.PosStatus == Position.POS_STATUS_STOCKTAKING)
                return MoveStatusEnum.Locked;

            var onStore = Singleton<ServicesFactory>.Instance.GetOnStore();
            var result = onStore.CheckNotValidMoves(new List<int>() { positionID });

            if (result.Count > 0)
                return MoveStatusEnum.NotSaved;
            else
                return MoveStatusEnum.OK;                       
        }


        public static List<Entities.Helpers.ArticleInfo> GetArticles(int positionID)
        {
            var onStore = Singleton<ServicesFactory>.Instance.GetOnStore();
            var result = onStore.OnPosition(positionID, StoreCore.Interfaces.ResultValidityEnum.Valid);

            var articleInfoList = new List<Entities.Helpers.ArticleInfo>();
            foreach (var item in result)
            {
                var articleInfo = new Entities.Helpers.ArticleInfo()
                {
                    ArticleCode = item.Article.ArticleCode,
                    ArticleDesc = item.Article.ArticleDesc,
                    ArticleID = item.Article.ArticleID,
                    ArtPackID = item.ArtPackID,
                    BatchNum = item.BatchNum,
                    CarrierNum = item.CarrierNum,
                    ExpirationDate = item.ExpirationDate,
                    PackDesc = item.ArticlePacking.PackDesc,
                    Quantity = item.Quantity
                };
                articleInfoList.Add(articleInfo);
            }

            return articleInfoList;
        }

    }

}

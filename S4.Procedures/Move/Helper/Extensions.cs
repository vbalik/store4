﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace S4.Procedures.Move.Helper
{
    public static class Extensions
    {
        public static (bool result, Entities.Helpers.ArticleInfo articleInfo) ContainsAll(this List<Entities.Helpers.ArticleInfo> bigList, List<Entities.Helpers.ArticleInfo> smallList)
        {
            var bigListSum = bigList.Summary();
            var smallListSum = smallList.Summary();

            foreach (var posSmall in smallListSum)
            {
                // find same art in big
                bool found = false;
                foreach (var posBig in bigListSum)
                {
                    if (posSmall.IsSameArticleInfo(posBig, true))
                    {
                        if (posSmall.Quantity > posBig.Quantity)
                            // not included
                            return (false, posSmall);
                        else
                            posBig.Quantity -= posSmall.Quantity;

                        found = true;
                        break;
                    }
                }
                if (!found)
                    // item not found
                    return (false, posSmall);
            }

            return (true, null);
        }


        public static List<Entities.Helpers.ArticleInfo> Summary(this List<Entities.Helpers.ArticleInfo> list)
        {
            return list.GroupBy(
                k => new { k.ArtPackID, BatchNum = k.BatchNum?.ToUpperInvariant(), k.ExpirationDate, CarrierNum = k.CarrierNum?.ToUpperInvariant() },
                (k, i) => new Entities.Helpers.ArticleInfo()
                {
                    ArtPackID = k.ArtPackID,
                    BatchNum = k.BatchNum,
                    ExpirationDate = k.ExpirationDate,
                    CarrierNum = k.CarrierNum,
                    Quantity = i.Sum(o => o.Quantity)
                }
                ).ToList();
        }


        public static bool IsSameArticleInfo(this Entities.Helpers.ArticleInfo info1, Entities.Helpers.ArticleInfo info2, bool checkExpiration)
        {
            if (info1.ArtPackID != info2.ArtPackID)
                return false;

            if (String.Compare(info1.BatchNum, info2.BatchNum, true) != 0)
                return false;

            if (checkExpiration)
                if (!info1.ExpirationDate.ExpirationEqual(info2.ExpirationDate))
                    return false;

            if (String.Compare(info1.CarrierNum, info2.CarrierNum, true) != 0)
                return false;

            return true;
        }


        public static bool ExpirationEqual(this DateTime? expirationDate1, DateTime? expirationDate2)
        {
            if (expirationDate1.HasValue && expirationDate2.HasValue)
                return (expirationDate1.Value.Year == expirationDate2.Value.Year && expirationDate1.Value.Month == expirationDate2.Value.Month && expirationDate1.Value.Day == expirationDate2.Value.Day);
            else
                return !(expirationDate1.HasValue ^ expirationDate2.HasValue);
        }
    }
}

﻿using S4.ProcedureModels.Move;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Procedures.Move
{
    public class MoveGetPositionProc : ProceduresBase<MoveGetPosition>
    {

        public MoveGetPositionProc(string userID) : base(userID)
        {
        }

        protected override MoveGetPosition DoIt(MoveGetPosition procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new MoveGetPosition() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            var result = new MoveGetPosition()
            {
                PositionResult = Helper.MoveHelper.CheckPositionAvailable(procModel.PositionID)
            };

            if (result.PositionResult == MoveStatusEnum.OK)
                result.Articles = Helper.MoveHelper.GetArticles(procModel.PositionID);

            result.Success = true;
            return result;
        }

    }
}

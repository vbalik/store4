﻿using NPoco;
using S4.Entities;
using S4.ProcedureModels.Move;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Procedures.Move
{
    public class MoveLockPosProc : ProceduresBase<MoveLockPos>
    {
        public MoveLockPosProc(string userID) : base(userID)
        {
        }

        protected override MoveLockPos DoIt(MoveLockPos procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new MoveLockPos() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            // check availability
            var result = new MoveLockPos()
            {
                PositionResult = Helper.MoveHelper.CheckPositionAvailable(procModel.PositionID),
                Success = true
            };

            return result;
        }       
    }
}

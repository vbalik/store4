﻿using System;
using System.Collections.Generic;
using System.Text;
using NPoco;
using S4.Core;
using S4.Entities;
using S4.Entities.Models;
using S4.ProcedureModels.Move;
using S4.StoreCore.Interfaces;


namespace S4.Procedures.Move
{
    public class MovePrintProc : ProceduresBase<MovePrint>
    {
        public MovePrintProc(string userID) : base(userID)
        {
        }


        protected override MovePrint DoIt(MovePrint procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new MovePrint() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                if (procModel.ReportID == 0)
                {
                    procModel.ReportID = Singleton<DAL.SettingsSet>.Instance.GetValue_Int32(Entities.SettingsDictionary.MOVE_REPORT_ID);
                }

                // get storeMove
                var stMove = (new DAL.StoreMoveDAL()).GetStoreMoveAllData(procModel.StoMoveID);
                if (stMove == null)
                    return new MovePrint() { Success = false, ErrorText = $"Doklad nebyl nalezen; StoMoveID: {procModel.StoMoveID}" };

                // prepare data
                foreach (var item in stMove.Items)
                {
                    item.Position = Singleton<DAL.Cache.PositionCache>.Instance.GetItem(item.PositionID);
                    item.StoreMoveLot = Singleton<DAL.Cache.StoreMoveLotCache>.Instance.GetItem(item.StoMoveLotID);
                    item.ArticlePacking = Singleton<DAL.Cache.ArticlePackingCache>.Instance.GetItem(item.StoreMoveLot.ArtPackID);
                    item.Article = Singleton<DAL.Cache.ArticleModelCache>.Instance.GetItem(item.ArticlePacking.ArticleID);
                }

                // get report
                var report = db.SingleOrDefault<Reports>("where reportID = @0", procModel.ReportID);
                if (report == null)
                    return new MovePrint() { Success = false, ErrorText = $"Report nebyl nalezen; reportID: {procModel.ReportID}" };

                // get printer location
                var printerlocation = db.SingleOrDefault<PrinterLocation>("where printerLocationID = @0", procModel.PrinterLocationID);
                if (printerlocation == null)
                    return new MovePrint() { Success = false, ErrorText = $"PrinterLocation nebyl nalezen; printerLocationID: {procModel.PrinterLocationID}" };

                // variables
                var variables = new List<Tuple<string, object, Type>>();
                variables.Add(new Tuple<string, object, Type>("DocInfo", $"{stMove.DocNumPrefix}/{stMove.DocNumber}", typeof(string)));
                variables.Add(new Tuple<string, object, Type>("DocDate", stMove.DocDate, typeof(DateTime)));
                variables.Add(new Tuple<string, object, Type>("EntryUserID", stMove.EntryUserID, typeof(string)));

                //do print
                Report.Print print = new Report.Print();
                print.Variables = variables;
                print.ReportDefinition = report.Template; //šablona z DB
                //print.DesignMode = true; //pouze pro úpravy šablony
                //print.ReportDefFilePath = @"c:\Projects\store4\Tools\Reporty\SkladovyPohyb.srt"; //pouze pro úpravy šablony

                byte[] fileData = null;
                if (IsTesting)
                {
                    var storeMoveModelList = new List<StoreMoveModel>();
                    storeMoveModelList.Add(stMove);
                    fileData = print.DoPrint2Text(storeMoveModelList);
                }
                else
                {
                    if (procModel.LocalPrint)
                    {
                        var storeMoveModelList = new List<StoreMoveModel>();
                        storeMoveModelList.Add(stMove);
                        fileData = print.DoPrint2PDF(storeMoveModelList);
                    }
                    else
                        print.DoPrint(printerlocation.PrinterPath, stMove);
                }

                if (print.IsError)
                    return new MovePrint() { Success = false, ErrorText = print.ErrorText };
                else
                    return new MovePrint() { Success = true, LocalPrint = procModel.LocalPrint, FileData = fileData };
            }
        }
    }

}

﻿using S4.ProcedureModels.Move;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using S4.Procedures.Move.Helper;

namespace S4.Procedures.Move
{
    public class MoveCheckItemsProc : ProceduresBase<MoveCheckItems>
    {

        public MoveCheckItemsProc(string userID) : base(userID)
        {
        }

        protected override MoveCheckItems DoIt(MoveCheckItems procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new MoveCheckItems() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };


            var result = new MoveCheckItems
            {
                PositionResult = Helper.MoveHelper.CheckPositionAvailable(procModel.PositionID)
            };


            if (result.PositionResult == MoveStatusEnum.OK)
            {
                // get pos content
                var onPosition = Helper.MoveHelper.GetArticles(procModel.PositionID);

                // first step - fill article.expirationDate from on position

                result.ItemExists = true;
                foreach (var article in procModel.Articles)
                {
                    var pos = onPosition.Where(p => p.ArtPackID == article.ArtPackID && String.Equals(p.BatchNum, article.BatchNum, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
                    // if article not found in onPosition, break the procedure
                    if (pos == null)
                    {
                        result.ItemExists = false;
                        result.ErrorText = $"Karta neexistuje na zdrojové pozici; ArticleCode: {article.ArticleCode}";
                        break;
                    }
                    else
                    {
                        // found - fill expiration date
                        article.ExpirationDate = pos.ExpirationDate;
                    }
                }

                // second step - check if onPosition contains all items from procModel.Articles

                if (result.ItemExists)
                {
                    var containsAll = onPosition.ContainsAll(procModel.Articles);
                    if (!containsAll.result)
                    {
                        result.ItemExists = false;
                        result.ErrorText = $"Karta neexistuje na zdrojové pozici; ArticleCode: {containsAll.articleInfo.ArticleCode}";
                    }
                }

            }


            result.Success = true;
            return result;
        }

    }
}

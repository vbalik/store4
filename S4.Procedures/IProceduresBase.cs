﻿using S4.Procedures.Messages;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Procedures
{
    public interface IProceduresBase
    {
        string TaskID { get; set; }
        int LastProgressValue { get; set; }
        string LastHeader { get; set; }
        void SendMessageToClient(string header, string message, int progressValue = -1, NotifyType color = NotifyType.Info);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using NLog;
using S4.Core.Extensions;
using S4.ProcedureModels;
using S4.Procedures.Messages;

namespace S4.Procedures
{
    public abstract class ProceduresBase<T> : IProceduresBase where T : S4.ProcedureModels.ProceduresModel
    {
        private static Logger _logger = LogManager.GetLogger("ProceduresBase");

        protected string _userID = null;
        protected long _transactionNum;
        private string _transName = null;

        // reports testing
        public bool IsTesting { get; set; }
        public string RenderResult { get; set; }

        // unique id of procedure run
        public string TaskID { get; set; } = Guid.NewGuid().ToString();

        //for exception Procedures.RunAsyncAttribute
        public int LastProgressValue { get; set; }
        public string LastHeader { get; set; }

        protected ProceduresBase(string userID)
        {
            _userID = userID;

            // get unique transaction number - for loging
            _transactionNum = TransCounter.NextNumber();

            // get transaction name
            _transName = this.GetType().Name;
        }


        #region running

        public T DoProcedure(T parameterObject)
        {
            // log start
            _logger.Debug("{0}; {1} start; userID: {2}", _transactionNum, _transName, _userID);

            // log in params
            TraceParams(parameterObject, typeof(ParameterInAttribute));

            // do procedure
            DateTime startTime = DateTime.Now;
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            T result = DoIt(parameterObject);
            stopWatch.Stop();

            // do log
            LogResult(result);

            // log out params
            TraceParams(result, typeof(ParameterOutAttribute));

            // send message to procedure log
            var messageRouter = Core.Singleton<ServicesFactory>.Instance.GetMessageRouter();
            var procedureLogItem = new Messages.ProcedureLogItem()
            {
                TransactionNum = _transactionNum,
                ProcedureName = _transName,
                StartTime = startTime,
                ProcedureSuccess = result.Success,
                RunTime = stopWatch.ElapsedMilliseconds
            };
            messageRouter.Publish(procedureLogItem);

            // if ChangesDirectionStatusAttribute exists, send DirectionStatusUpdated message
            if (this.GetType().IsDefined(typeof(ChangesDirectionStatusAttribute)))
            {
                // get directionID value
                var directionID = (int)parameterObject.GetValueByName("DirectionID");
                messageRouter.Publish(new Messages.DirectionStatusUpdated() { DirectionID = directionID });
            }

            // if ChangesDirectionAssignmentAttribute exists, send DirectionAssigmentUpdated message
            if (this.GetType().IsDefined(typeof(ChangesDirectionAssignmentAttribute)) && result.Success)
            {
                // get directionID value
                var directionID = (int)parameterObject.GetValueByName("DirectionID");
                var workerID = (string)parameterObject.GetValueByName("WorkerID");
                messageRouter.Publish(new Messages.DirectionAssigmentUpdated() { DirectionID = directionID, WorkerID = workerID });
            }

            // log end
            _logger.Debug("{0}; {1} end", _transactionNum, _transName);

            return result;
        }


        public void TraceParams(T parameterObject, Type attrType)
        {
            // log in params
            if (_logger.IsTraceEnabled)
                TraceParamsInternal(parameterObject, attrType);
        }


        public void TraceParams(string message)
        {
            // log in params
            if (_logger.IsTraceEnabled)
                _logger.Trace("{0}; {1} - {2}", _transactionNum, typeof(T).Name, message);
        }


        protected abstract T DoIt(T parameterObject);


        public virtual void InCaseOfError(T parameterObject)
        {
            // do nothing - this should be overriden
            _logger.Warn("Empty InCaseOfError procedure called");
        }


        private void LogResult(T parameterObject)
        {
            if (parameterObject is string)
            {
                _logger.Info("{0}; {1} done - {2}", _transactionNum, _transName, parameterObject);
            }
            else
            {
                object result = GetResult(parameterObject);
                object errorText = GetErrorText(parameterObject);

                if (errorText == null)
                    _logger.Info("{0}; {1} done - {2}", _transactionNum, _transName, result);
                else
                    _logger.Info("{0}; {1} done - {2} ({3})", _transactionNum, _transName, result, errorText);
            }
        }


        private object GetResult(T parameterObject)
        {
            var props = from p in typeof(T).GetProperties()
                        let attrs = p.GetCustomAttributes(typeof(ParameterOutAttribute), true)
                        where (attrs.Length != 0) && (((ParameterOutAttribute)attrs[0]).IsResult)
                        select p;

            var prop = props.FirstOrDefault();
            if (prop == null)
                return null;
            else
                return prop.GetValue(parameterObject);
        }


        private object GetErrorText(T parameterObject)
        {
            var props = from p in typeof(T).GetProperties()
                        let attrs = p.GetCustomAttributes(typeof(ParameterOutAttribute), true)
                        where (attrs.Length != 0) && (((ParameterOutAttribute)attrs[0]).IsErrorText)
                        select p;

            var prop = props.FirstOrDefault();
            if (prop == null)
                return null;
            else
                return prop.GetValue(parameterObject);
        }


        private void TraceParamsInternal(T parameterObject, Type attrType)
        {
            if (parameterObject is string)
            {
                _logger.Trace("{0}; {1} - {2}", _transactionNum, attrType.Name, parameterObject);
            }
            else
            {
                var props = from p in typeof(T).GetProperties()
                            let attrs = p.GetCustomAttributes(attrType, true)
                            where attrs.Length != 0
                            select p;

                var sb = new StringBuilder();
                foreach (var prop in props)
                    sb.AppendFormat("{0} : {1}; ", prop.Name, prop.GetValue(parameterObject));

                _logger.Trace("{0}; {1} - {2}", _transactionNum, attrType.Name, sb.ToString());
            }
        }

        #endregion



        #region sql commands loging

        //TODO
        //// log all sql commands - if TRACE is on
        //public override void OnExecutingCommand(System.Data.IDbCommand cmd)
        //{
        //    base.OnExecutingCommand(cmd);

        //    if (_logger.IsTraceEnabled)
        //        _logger.Trace("{0}; Sql: '{1}'; {2}", _transactionNum, cmd.CommandText, ParamsToText(cmd.Parameters));
        //}


        private string ParamsToText(System.Data.IDataParameterCollection dataParameterCollection)
        {
            StringBuilder sb = new StringBuilder();
            foreach (System.Data.IDataParameter param in dataParameterCollection)
                sb.AppendFormat("'{0}',", param.Value.ToString());
            return sb.ToString();
        }

        #endregion



        #region log helpers - protected

        protected void Debug(string format, params object[] args)
        {
            string text = String.Format(format, args);
            _logger.Debug("{0}; {1}", _transactionNum, text);
        }


        protected void Info(string format, params object[] args)
        {
            string text = String.Format(format, args);
            _logger.Info("{0}; {1}", _transactionNum, text);
        }


        protected void Warn(string format, params object[] args)
        {
            string text = String.Format(format, args);
            _logger.Warn("{0}; {1}", _transactionNum, text);
        }

        protected void Error(string format, Exception exception, params object[] args)
        {
            string text = String.Format(format, args);
            _logger.Error(exception, $"{_transactionNum}; {text}");
        }

        #endregion



        #region SignalR Message

        public void SendMessageToClient(string header, string message, int progressValue = -1, NotifyType color = NotifyType.Info)
        {
            LastProgressValue = progressValue;
            LastHeader = header;
            // Color = success (green), info (blue), warning (orange), danger (red)
            var messageRouter = Core.Singleton<ServicesFactory>.Instance.GetMessageRouter();
            messageRouter.Publish(new S4.Procedures.Messages.ProgressMessage() { User = _userID, Header = header, Message = message, NotifyType = color, TaskID = TaskID, ProgressValue = progressValue, TransName = _transName });
        }

        protected void SendMessageToExchange(params string[] jobNames)
        {
            var messageRouter = Core.Singleton<ServicesFactory>.Instance.GetMessageRouter();
            messageRouter.Publish(new S4.Procedures.Messages.ExchangeMessage() { JobNames = jobNames });
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using S4.Core;
using S4.Core.Data;
using S4.Core.PropertiesComparer;
using S4.Entities;
using S4.ProcedureModels.Admin;


namespace S4.Procedures.Admin
{
    public class StoreMoveItemChangeProc : ProceduresBase<StoreMoveItemChange>
    {
        public StoreMoveItemChangeProc(string userID) : base(userID)
        {
        }


        protected override StoreMoveItemChange DoIt(StoreMoveItemChange procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new StoreMoveItemChange() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //check
                var storeMoveItem = db.SingleOrDefault<StoreMoveItem>("where stoMoveItemID = @0", procModel.StoMoveItemID);
                if (storeMoveItem == null)
                    return new StoreMoveItemChange() { Success = false, ErrorText = $"StoreMoveItem nebylo nalezeno; StoMoveItemID: {procModel.StoMoveItemID}" };
                var originStoMoveLotID = storeMoveItem.StoMoveLotID;

                //find brother if is sibling
                StoreMoveItem brotherMoveItem = null;
                if (storeMoveItem.BrotherID != StoreMoveItem.NO_BROTHER_ID)
                {
                    brotherMoveItem = db.SingleOrDefault<StoreMoveItem>("where stoMoveItemID = @0", storeMoveItem.BrotherID);
                    if (brotherMoveItem == null)
                        return new StoreMoveItemChange() { Success = false, ErrorText = $"Bratrské StoreMoveItem nebylo nalezeno; StoMoveItemID: {storeMoveItem.BrotherID}" };
                }

                var storeMove = db.SingleOrDefault<StoreMove>("where stoMoveID = @0", storeMoveItem.StoMoveID);


                // get values for history
                var propertiesComparer = new PropertiesComparer<StoreMoveItem>();
                propertiesComparer.DoCompare<StoreMoveItemChange>(storeMoveItem, procModel, nameof(procModel.Quantity), nameof(procModel.CarrierNum));

                // make info
                var message = new InternMessage()
                {
                    MsgClass = InternMessage.MO_ITEM_CHANGED,
                    MsgSeverity = InternMessage.MessageSeverityEnum.High,
                    MsgHeader = $"Změna položky dokladu {storeMove.DocNumPrefix}/{storeMove.DocNumber} (StoMoveID: {storeMove.StoMoveID})",
                    MsgDateTime = DateTime.Now,
                    MsgUserID = _userID,
                    MsgText = propertiesComparer.JsonText
                };


                var history = new StoreMoveHistory()
                {
                    StoMoveID = storeMoveItem.StoMoveID,
                    DocPosition = storeMoveItem.DocPosition,
                    EventCode = StoreMove.MO_EVENT_ITEM_CHANGED,
                    EventData = propertiesComparer.JsonText,
                    EntryUserID = _userID,
                    EntryDateTime = DateTime.Now
                };

                // change values
                var proxy = new EmptyDataProxy();
                proxy.ToSave(procModel.CarrierNum, StoreMoveItem.EMPTY_CARRIERNUM, value => procModel.CarrierNum = value);
                proxy.ToSave(procModel.BatchNum, StoreMoveItem.EMPTY_BATCHNUM, value => procModel.BatchNum = value);
                proxy.ToSave(procModel.ExpirationDate, StoreMoveItem.EMPTY_EXPIRATION, value => procModel.ExpirationDate = value);

                storeMoveItem.Quantity = procModel.Quantity;
                storeMoveItem._carrierNum = procModel.CarrierNum;
                if (brotherMoveItem != null)
                    brotherMoveItem.Quantity = procModel.Quantity;

                // save
                using (var tr = db.GetTransaction())
                {
                    // create or get lot
                    var storeMoveLot = new StoreCore.Save_Basic.Save()
                        .FindOrCreateLot(db,
                        new StoreCore.Interfaces.SaveItem()
                        {
                            ArtPackID = procModel.ArtPackID,
                            BatchNum = procModel.BatchNum,
                            ExpirationDate = procModel.ExpirationDate
                        });

                    // update item
                    storeMoveItem.StoMoveLotID = storeMoveLot.StoMoveLotID;
                    db.Update(storeMoveItem);
                    // update brother
                    if (brotherMoveItem != null)
                    {
                        brotherMoveItem.StoMoveLotID = storeMoveLot.StoMoveLotID;
                        db.Update(brotherMoveItem);
                    }

                    // save info
                    db.Insert(message);

                    // save history
                    db.Insert(history);

                    // invalidate storage - there is a need for recalculating
                    var save = Singleton<ServicesFactory>.Instance.GetSave();
                    save.InvalidateStorageLot(db, _userID, originStoMoveLotID);
                    if (originStoMoveLotID != storeMoveItem.StoMoveLotID)
                        save.InvalidateStorageLot(db, _userID, storeMoveItem.StoMoveLotID);

                    // do save
                    tr.Complete();
                }

                // log
                base.Info($"StoreMoveItemChangeProc; StoMoveItemID: {procModel.StoMoveItemID}");

                return new StoreMoveItemChange() { Success = true };
            }
        }
    }
}

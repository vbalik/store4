﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Procedures.Admin
{
    public class RemoveBatchProc : RemoveExpBatchProc
    {
        public RemoveBatchProc(string userID) : base(userID)
        {
            _batchOnly = true;
        }
    }
}

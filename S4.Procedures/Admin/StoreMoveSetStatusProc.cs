﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using S4.Core.Utils;
using S4.Entities;
using S4.ProcedureModels.Admin;


namespace S4.Procedures.Admin
{
    public class StoreMoveSetStatusProc : ProceduresBase<StoreMoveSetStatus> 
    {
        public StoreMoveSetStatusProc(string userID) : base(userID)
        {
        }


        protected override StoreMoveSetStatus DoIt(StoreMoveSetStatus procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new StoreMoveSetStatus() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //check
                var storeMove = db.SingleOrDefault<StoreMove>("where stoMoveID = @0", procModel.StoMoveID);
                if (storeMove == null)
                    return new StoreMoveSetStatus() { Success = false, ErrorText = $"StoreMove nebylo nalezeno; StMoveID: {procModel.StoMoveID}" };

                // make info
                var message = new InternMessage()
                {
                    MsgClass = InternMessage.MO_STATUS_CHANGED,
                    MsgSeverity = InternMessage.MessageSeverityEnum.Low,
                    MsgHeader = $"Změna stavu '{storeMove.DocNumPrefix}/{storeMove.DocNumber}'",
                    MsgDateTime = DateTime.Now,
                    MsgUserID = _userID,
                    MsgText = JsonUtils.ToJsonString("stoMoveID", storeMove.StoMoveID, "oldStatus", storeMove.DocStatus, "newStatus", procModel.Status)
                };

                // make history
                var history = new StoreMoveHistory()
                {
                    StoMoveID = procModel.StoMoveID,
                    DocPosition = 0,
                    EventCode = StoreMove.MO_EVENT_STATUS_CHANGED,
                    EventData = $"{storeMove.DocStatus}|{procModel.Status}",
                    EntryUserID = _userID,
                    EntryDateTime = DateTime.Now
                };

                // save
                using (var tr = db.GetTransaction())
                {
                    // save 

                    storeMove.DocStatus = procModel.Status;
                    db.Update(storeMove);

                    // save info
                    db.Insert(message);

                    //save history
                    db.Insert(history);

                    // do save
                    tr.Complete();
                }

                // log
                base.Info($"StoreMoveSetStatusProc; StoMoveID: {procModel.StoMoveID}; Status: {procModel.Status}");

                return new StoreMoveSetStatus() { Success = true }; ;
            }
        }
    }
}

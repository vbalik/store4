﻿using System;
using System.Collections.Generic;
using System.Text;

using S4.Entities;
using S4.ProcedureModels.Admin;


namespace S4.Procedures.Admin
{
    public class WorkerSetStatusProc : ProceduresBase<WorkerSetStatus>
    {
        public WorkerSetStatusProc(string userID) : base(userID)
        {
        }


        protected override WorkerSetStatus DoIt(WorkerSetStatus procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new WorkerSetStatus() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //check
                var worker = db.SingleOrDefault<Worker>("where workerid = @0", procModel.WorkerID);
                if (worker == null)
                    return new WorkerSetStatus() { Success = false, ErrorText = $"Worker nebyl nalezen; WorkerID: {procModel.WorkerID}" };

                // make history
                var history = new WorkerHistory()
                {
                    WorkerID = procModel.WorkerID,
                    EventCode = procModel.NewStatus.ToString(),
                    EventData = $"{worker.WorkerStatus}|{procModel.NewStatus}",
                    EntryUserID = _userID,
                    EntryDateTime = DateTime.Now
                };

                // save
                using (var tr = db.GetTransaction())
                {
                    // save 
                    worker.WorkerStatus = procModel.NewStatus;
                    db.Update(worker);

                    // history
                    db.Insert(history);

                    // do save
                    tr.Complete();
                }

                // log
                base.Info($"WorkerSetStatusProc; WorkerID: {procModel.WorkerID}");

                return new WorkerSetStatus() { Success = true };
            }
        }
    }
}

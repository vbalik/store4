﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using S4.Core.Utils;
using S4.Entities;
using S4.ProcedureModels.Admin;


namespace S4.Procedures.Admin
{
    public class StoreMoveCancelProc : ProceduresBase<StoreMoveCancel>
    {
        public StoreMoveCancelProc(string userID) : base(userID)
        {
        }


        protected override StoreMoveCancel DoIt(StoreMoveCancel procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new StoreMoveCancel() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //check
                var storeMove = db.SingleOrDefault<StoreMove>("where stoMoveID = @0", procModel.StoMoveID);
                if (storeMove == null)
                    return new StoreMoveCancel() { Success = false, ErrorText = $"StoreMove nebylo nalezeno; StMoveID: {procModel.StoMoveID}" };

                // make info
                var message = new InternMessage()
                {
                    MsgClass = InternMessage.MO_STATUS_CANCEL,
                    MsgSeverity = InternMessage.MessageSeverityEnum.Low,
                    MsgHeader = $"Doklad '{storeMove.DocNumPrefix}/{storeMove.DocNumber}' zrušen",
                    MsgDateTime = DateTime.Now,
                    MsgUserID = _userID,
                    MsgText = JsonUtils.ToJsonString("stoMoveID", storeMove.StoMoveID, "cancelReason", procModel.CancelReason)
                };

                // make history
                var history = new StoreMoveHistory()
                {
                    StoMoveID = procModel.StoMoveID,
                    DocPosition = 0,
                    EventCode = StoreMove.MO_STATUS_CANCEL,
                    EventData = procModel.CancelReason,
                    EntryUserID = _userID,
                    EntryDateTime = DateTime.Now
                };


                using (var tr = db.GetTransaction())
                {
                    // save info
                    db.Insert(message);

                    //save history
                    db.Insert(history);

                    // save
                    var save = (new ServicesFactory()).GetSave();
                    if (!save.CancelMove(db, _userID, procModel.StoMoveID, out string msg))
                        return new StoreMoveCancel() { ErrorText = msg };

                    // do save
                    tr.Complete();
                }

                // log
                base.Info($"StoreMoveCancelProc; StoMoveID: {procModel.StoMoveID}");

                return new StoreMoveCancel() { Success = true }; ;
            }
        }
    }
}

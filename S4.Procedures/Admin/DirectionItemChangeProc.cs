﻿using System;
using System.Collections.Generic;
using System.Text;
using NPoco;
using S4.Entities;
using S4.ProcedureModels.Admin;
using System.Linq;
using S4.Core.PropertiesComparer;
using Newtonsoft.Json;

namespace S4.Procedures.Admin
{
    public class DirectionItemChangeProc : ProceduresBase<DirectionItemChange>
    {


        public DirectionItemChangeProc(string userID) : base(userID)
        {
        }


        protected override DirectionItemChange DoIt(DirectionItemChange procModel)
        {

            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new DirectionItemChange() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // load direction item
                var directionItem = db.Query<DirectionItem>().SingleOrDefault(d => d.DirectionItemID == procModel.DirectionItemID);
                if (directionItem == null)
                    return new DirectionItemChange() { Success = false, ErrorText = $"DirectionItem nebylo nalezeno; DirectionItemID: {procModel.DirectionItemID}" };

                // load direction
                var direction = db.Query<Direction>().SingleOrDefault(d => d.DirectionID == directionItem.DirectionID);
                if (direction == null)
                    return new DirectionItemChange() { Success = false, ErrorText = $"Doklad nebyl nalezen; DirectionID: {directionItem.DirectionID}" };

                switch (direction.DocDirection)
                {
                    case Direction.DOC_DIRECTION_IN:
                        return ChangeItemReceive(db, procModel, directionItem, direction);
                    case Direction.DOC_DIRECTION_OUT:
                        return ChangeItemDispatch(db, procModel, directionItem, direction);
                    default:
                        throw new NotImplementedException(direction.DocDirection.ToString());
                }
            }
        }


        private DirectionItemChange ChangeItemReceive(Database db, DirectionItemChange procModel, DirectionItem directionItem, Direction direction)
        {
            // do tests
            if (!(directionItem.ItemStatus == DirectionItem.DI_STATUS_RECEIVE_IN_PROC || directionItem.ItemStatus == DirectionItem.DI_STATUS_RECEIVE_COMPLET))
                return new DirectionItemChange() { Success = false, ErrorText = $"Položka {directionItem.DirectionItemID} nemá správný status" };

            // get moves
            var moves = db.Query<StoreMove>().Where(x => x.Parent_directionID == directionItem.DirectionID).ToList();
            var IDs = moves.Select(m => m.StoMoveID).ToList();
            var moveItems = db.Query<StoreMoveItem>().Where(x => IDs.Contains(x.StoMoveID) && x.Parent_docPosition == directionItem.DocPosition && x.ItemValidity != StoreMoveItem.MI_VALIDITY_CANCELED).ToList();
            if (!moveItems.Any())
                return new DirectionItemChange() { Success = false, ErrorText = $"Položku nelze změnit - nebyly nalezeny skladové pohyby" };

            // check for stoin moves
            var stoInMoves = moves.Where(mo => mo.DocType == StoreMove.MO_DOCTYPE_STORE_IN).Select(mo => mo.StoMoveID);
            var stoInMoveItems = moveItems.Where(mi => stoInMoves.Contains(mi.StoMoveID));
            if (stoInMoveItems.Any())
                return new DirectionItemChange() { Success = false, ErrorText = $"Položku nelze změnit - byla již naskladněna" };

            // get values for history
            var propertiesComparer = new PropertiesComparer<DirectionItem>();
            propertiesComparer.DoCompare(directionItem, procModel, nameof(procModel.ArtPackID), nameof(procModel.ArticleCode), nameof(procModel.Quantity), nameof(procModel.UnitDesc), nameof(procModel.PartnerDepart));

            // change values
            directionItem.ArtPackID = procModel.ArtPackID;
            directionItem.ArticleCode = procModel.ArticleCode;
            directionItem.Quantity = procModel.Quantity;
            directionItem.UnitDesc = procModel.UnitDesc;
            directionItem.PartnerDepart = procModel.PartnerDepart;

            // make info
            var message = new InternMessage()
            {
                MsgClass = InternMessage.DR_ITEM_CHANGED,
                MsgSeverity = InternMessage.MessageSeverityEnum.High,
                MsgHeader = $"Změna položky dokladu {direction.DocNumPrefix}/{direction.DocYear}/{direction.DocNumber} (DirectionID: {direction.DirectionID})",
                MsgDateTime = DateTime.Now,
                MsgUserID = _userID,
                MsgText = propertiesComparer.JsonText
            };

            // make history
            var history = new DirectionHistory()
            {
                DirectionID = directionItem.DirectionID,
                DocPosition = directionItem.DocPosition,
                EventCode = Direction.DR_EVENT_ITEM_CHANGED,
                EventData = propertiesComparer.JsonText,
                EntryUserID = _userID,
                EntryDateTime = DateTime.Now
            };


            // save
            using (var tr = db.GetTransaction())
            {
                var save = (new ServicesFactory()).GetSave();

                // update item
                db.Update(directionItem);

                // change store moves
                if (!save.ChangeMoveItems(db, _userID, moveItems, procModel.ArtPackID, procModel.Quantity, out string msg))
                    return new DirectionItemChange() { ErrorText = msg };
                
                // save message
                db.Insert(message);

                // save history
                db.Insert(history);

                // do save
                tr.Complete();
            }

            // log
            base.Info($"DirectionItemChangeProc; DirectionItemID: {procModel.DirectionItemID}");

            return new DirectionItemChange() { Success = true };
        }


        private DirectionItemChange ChangeItemDispatch(Database db, DirectionItemChange procModel, DirectionItem directionItem, Direction direction)
        {
            // do tests
            if (!(directionItem.ItemStatus == DirectionItem.DI_STATUS_LOAD))
                return new DirectionItemChange() { Success = false, ErrorText = $"Položka {directionItem.DirectionItemID} nemá správný status" };

            // get moves
            var moves = db.Query<StoreMove>().Where(x => x.Parent_directionID == directionItem.DirectionID).ToList();
            var IDs = moves.Select(m => m.StoMoveID).ToList();
            var moveItems = db.Query<StoreMoveItem>().Where(x => IDs.Contains(x.StoMoveID) && x.Parent_docPosition == directionItem.DocPosition && x.ItemValidity != StoreMoveItem.MI_VALIDITY_CANCELED).ToList();
            if (moveItems.Any())
                return new DirectionItemChange() { Success = false, ErrorText = $"Položku nelze změnit - existují skladové pohyby" };

            // get values for history
            var propertiesComparer = new PropertiesComparer<DirectionItem>();
            propertiesComparer.DoCompare(directionItem, procModel, nameof(procModel.ArtPackID), nameof(procModel.ArticleCode), nameof(procModel.Quantity), nameof(procModel.UnitDesc), nameof(procModel.PartnerDepart));

            // change values
            directionItem.ArtPackID = procModel.ArtPackID;
            directionItem.ArticleCode = procModel.ArticleCode;
            directionItem.Quantity = procModel.Quantity;
            directionItem.UnitDesc = procModel.UnitDesc;
            directionItem.PartnerDepart = procModel.PartnerDepart;

            // make info
            var message = new InternMessage()
            {
                MsgClass = InternMessage.DR_ITEM_CHANGED,
                MsgSeverity = InternMessage.MessageSeverityEnum.High,
                MsgHeader = $"Změna položky dokladu {direction.DocNumPrefix}/{direction.DocYear}/{direction.DocNumber} (DirectionID: {direction.DirectionID})",
                MsgDateTime = DateTime.Now,
                MsgUserID = _userID,
                MsgText = propertiesComparer.JsonText
            };

            // make history
            var history = new DirectionHistory()
            {
                DirectionID = directionItem.DirectionID,
                DocPosition = directionItem.DocPosition,
                EventCode = Direction.DR_EVENT_ITEM_CHANGED,
                EventData = propertiesComparer.JsonText,
                EntryUserID = _userID,
                EntryDateTime = DateTime.Now
            };


            // save
            using (var tr = db.GetTransaction())
            {
                var save = (new ServicesFactory()).GetSave();

                // update item
                db.Update(directionItem);

                // save message
                db.Insert(message);

                // save history
                db.Insert(history);

                // do save
                tr.Complete();
            }

            // log
            base.Info($"DirectionItemChangeProc; DirectionItemID: {procModel.DirectionItemID}");

            return new DirectionItemChange() { Success = true };
        }

    }
}

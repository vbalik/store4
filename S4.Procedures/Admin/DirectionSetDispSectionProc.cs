﻿using System;
using System.Collections.Generic;
using System.Text;

using S4.Entities;
using S4.Entities.XtraData;
using S4.ProcedureModels.Admin;


namespace S4.Procedures.Admin
{
    public class DirectionSetDispSectionProc : ProceduresBase<DirectionSetDispSection>
    {
        public DirectionSetDispSectionProc(string userID) : base(userID)
        {
        }


        protected override DirectionSetDispSection DoIt(DirectionSetDispSection procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new DirectionSetDispSection() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //load direction
                var direction = db.SingleOrDefault<Direction>("where directionid = @0", procModel.DirectionID);
                if (direction == null)
                    return new DirectionSetDispSection() { Success = false, ErrorText = $"Doklad nebyl nalezen; DirectionID: {procModel.DirectionID}" };

                // do tests
                if (!(direction.DocStatus == Direction.DR_STATUS_LOAD || direction.DocStatus == Direction.DR_STATUS_DISPATCH_WAIT))
                    return new DirectionSetDispSection() { Success = false, ErrorText = $"Direction {direction.DirectionID} nemá správný status" };

                // set dispatch section 
                var xtraData = new XtraDataProxy<Entities.DirectionXtra>(Direction.XtraDataDict);
                xtraData[Direction.XTRA_DISPATCH_DIRECT_SECTION] = procModel.SectID;
                
                // make history
                var history = new DirectionHistory()
                {
                    DirectionID = procModel.DirectionID,
                    DocPosition = 0,
                    EventCode = Direction.DR_EVENT_DISP_SECT_CHANGE,
                    EventData = $"Direction Set Disp Section, DirectionID: '{direction.DirectionID}'",
                    EntryUserID = _userID,
                    EntryDateTime = DateTime.Now
                };

                // save
                using (var tr = db.GetTransaction())
                {
                    // save 
                    db.Update(direction);

                    //his
                    db.Insert(history);

                    //xtraData Save
                    xtraData.Save(db, direction.DirectionID);

                    // do save
                    tr.Complete();
                }

                // log
                base.Info($"DirectionSetStatusProc; DirectionID: {procModel.DirectionID}");

                var result = new DirectionSetDispSection();
                result.Success = true;
                return result;
            }



        }
    }
}

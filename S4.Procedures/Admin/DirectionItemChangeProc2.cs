﻿using System;
using System.Collections.Generic;
using NPoco;
using S4.Entities;
using S4.ProcedureModels.Admin;
using System.Linq;
using S4.Core.PropertiesComparer;

namespace S4.Procedures.Admin
{
    public class DirectionItemChange2Proc : ProceduresBase<DirectionItemChange2>
    {
        private readonly string _userID;

        public DirectionItemChange2Proc(string userID) : base(userID)
        {
            _userID = userID;
        }


        protected override DirectionItemChange2 DoIt(DirectionItemChange2 procModel)
        {

            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new DirectionItemChange2() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // load direction
                var direction = db.Query<Direction>().SingleOrDefault(d => d.DirectionID == procModel.DirectionID);
                if (direction == null)
                    return new DirectionItemChange2() { Success = false, ErrorText = $"Doklad nebyl nalezen; DirectionID: {procModel.DirectionID}" };

                // load direction items
                var directionItem = db.Query<DirectionItem>()
                    .Where(_ => _.DirectionID == procModel.DirectionID)
                    .Where(_ => _.DirectionItemID == procModel.DirectionItemID)
                    .SingleOrDefault();
                
                if (directionItem == null)
                    return new DirectionItemChange2() { Success = false, ErrorText = $"DirectionItem nebylo nalezeno; DirectionItemID: {procModel.DirectionID}" };

                if (!(directionItem.ItemStatus == DirectionItem.DI_STATUS_RECEIVE_IN_PROC || directionItem.ItemStatus == DirectionItem.DI_STATUS_RECEIVE_COMPLET))
                    return new DirectionItemChange2() { Success = false, ErrorText = $"Položka {directionItem.DirectionItemID} nemá správný status" };


                return ChangeItemReceive(db, procModel, directionItem, direction);
            }
        }

        private DirectionItemChange2 ChangeItemReceive(Database db, DirectionItemChange2 procModel, DirectionItem directionItem, Direction direction)
        {
            // get moves from direction
            var moves = db.Query<StoreMove>()
                .Where(_ => _.Parent_directionID == procModel.DirectionID)
                .Where(mo => mo.DocType == StoreMove.MO_DOCTYPE_RECEIVE || mo.DocType == StoreMove.MO_DOCTYPE_STORE_IN)
                .ToList();
            var IDs = moves.Select(m => m.StoMoveID).ToList();

            // get move items
            var moveItems = db.Query<StoreMoveItem>().Where(_ => IDs.Contains(_.StoMoveID) 
                && _.ItemValidity != StoreMoveItem.MI_VALIDITY_CANCELED
                && _.Parent_docPosition == directionItem.DocPosition).ToList();
            if (!moveItems.Any())
                return new DirectionItemChange2() { Success = false, ErrorText = $"Položku nelze změnit - nebyly nalezeny skladové pohyby" };

            // compare model move items to move items
            var moveItemsModelIDs = procModel.DirectionStoMoveItems.Select(_ => _.StoMoveItemID).ToList();
            var moveItemsCnt = moveItems.Where(_ => moveItemsModelIDs.Contains(_.StoMoveItemID)).ToList().Count();
            if(moveItemsModelIDs.Count < 1 || moveItemsModelIDs.Count != moveItemsCnt)
                return new DirectionItemChange2() { Success = false, ErrorText = $"Položku nelze změnit - pohyby nenalezeny" };

            // get rec items
            var recMoves = moves.Where(mo => mo.DocType == StoreMove.MO_DOCTYPE_RECEIVE).Select(mo => mo.StoMoveID);
            var recMoveItems = moveItems.Where(mi => recMoves.Contains(mi.StoMoveID)).ToList();

            // get stoin moves
            var stoInMoves = moves.Where(mo => mo.DocType == StoreMove.MO_DOCTYPE_STORE_IN).Select(mo => mo.StoMoveID);
            var stoInMoveItems = moveItems.Where(mi => stoInMoves.Contains(mi.StoMoveID)).ToList();
            if (stoInMoveItems.Any())
                return new DirectionItemChange2() { Success = false, ErrorText = $"Položku nelze změnit - byla již naskladněna" };


            // TODO on store - check if sto in moves exists
            //foreach (var stoInMoveItem in stoInMoveItems.Where(_ => _.ItemDirection == StoreMoveItem.MI_DIRECTION_IN).ToList())
            //{
            //    var onStore = Singleton<ServicesFactory>.Instance.GetOnStore();
            //    var onPosition = onStore.OnPosition(stoInMoveItem.PositionID, ResultValidityEnum.Valid);
            //    var itemOnStore = onPosition.Where(_ => _.ArtPackID == procModel.ArtPackID)
            //        .Where(_ => _.CarrierNum == stoInMoveItem.CarrierNum)
            //        .Where(_ => _.StoMoveLotID == stoInMoveItem.StoMoveLotID)
            //        .SingleOrDefault();
            //
            //    if(stoInMoveItem.Quantity != itemOnStore.Quantity)
            //        return new DirectionItemChange2() { Success = false, ErrorText = $"Položku nelze změnit - byla již vyskladněna" };
            //}

            // do change
            return DoChange(db, moves, recMoveItems, stoInMoveItems, directionItem, procModel);
        }

        private DirectionItemChange2 DoChange(Database db, 
            List<StoreMove> moves, 
            List<StoreMoveItem> recMoveItems, 
            List<StoreMoveItem> stoInMoveItems,
            DirectionItem directionItem,
            DirectionItemChange2 procModel)
        {
            using (var tr = db.GetTransaction())
            {
                var save = new ServicesFactory().GetSave();

                foreach (var item in recMoveItems)
                {
                    // get values
                    var itemModel = procModel.DirectionStoMoveItems.SingleOrDefault(_ => _.StoMoveItemID == item.StoMoveItemID);
                    if (itemModel == null) continue;

                    // update values
                    if(!save.ChangeMoveItem2(db, _userID, item, procModel.ArtPackID, itemModel.Quantity, itemModel.BatchNum, itemModel.ExpirationDate, itemModel.CarrierNum, out var msg))
                        return new DirectionItemChange2() { ErrorText = msg };
                }

                // TODO on store - update sto in moves
                //foreach (var stoInMoveItem in stoInMoveItems)
                //{
                //    save.ChangeMoveItem2(db, _userID, stoInMoveItem, procModel.ArtPackID, stoInMoveItem.Quantity, stoInMoveItem.BatchNum, stoInMoveItem.ExpirationDate, stoInMoveItem.CarrierNum, out var msg);
                //}

                // new quantity from new move items
                var IDs = moves.Select(_ => _.StoMoveID).ToList();

                // get rec items
                var recMoves = moves.Where(mo => mo.DocType == StoreMove.MO_DOCTYPE_RECEIVE).Select(mo => mo.StoMoveID);

                // get move items
                recMoveItems = db.Query<StoreMoveItem>().Where(_ => recMoves.Contains(_.StoMoveID)
                    && _.ItemValidity != StoreMoveItem.MI_VALIDITY_CANCELED
                    && _.Parent_docPosition == directionItem.DocPosition).ToList();

                var directionItemQuantity = recMoveItems.Sum(_ => _.Quantity);

                // compare directionItem changes
                var propertiesComparer = new PropertiesComparer<DirectionItem>();
                propertiesComparer.DoCompare(directionItem, new
                {
                    Quantity = directionItemQuantity,
                    ArtPackID = procModel.ArtPackID,
                    ArticleCode = procModel.ArticleCode
                }, nameof(procModel.ArtPackID), nameof(procModel.ArticleCode), nameof(directionItem.Quantity));

                // set values
                directionItem.Quantity = directionItemQuantity;
                directionItem.ArtPackID = procModel.ArtPackID;
                directionItem.ArticleCode = procModel.ArticleCode;

                // update item
                db.Update(directionItem);

                // make history
                var history = new DirectionHistory()
                {
                    DirectionID = directionItem.DirectionID,
                    DocPosition = directionItem.DocPosition,
                    EventCode = Direction.DR_EVENT_ITEM_CHANGED,
                    EventData = propertiesComparer.JsonText,
                    EntryUserID = _userID,
                    EntryDateTime = DateTime.Now
                };

                // save history
                db.Insert(history);

                tr.Complete();
            }

            return new DirectionItemChange2() { Success = true };
        }
    }
}

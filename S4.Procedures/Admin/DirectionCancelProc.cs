﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using S4.Core.Utils;
using S4.Entities;
using S4.ProcedureModels;
using S4.ProcedureModels.Admin;


namespace S4.Procedures.Admin
{
    [ChangesDirectionStatus]
    public class DirectionCancelProc : ProceduresBase<DirectionCancel>
    {
        public DirectionCancelProc(string userID) : base(userID)
        {
        }


        protected override DirectionCancel DoIt(DirectionCancel procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new DirectionCancel() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            var worker = (new DAL.WorkerDAL()).Get(_userID);
            var isSuperUser = worker.WorkerClass.HasFlag(Entities.Worker.WorkerClassEnum.IsSuperUser);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //check
                var direction = (new DAL.DirectionDAL()).GetDirectionAllData(procModel.DirectionID);
                if (direction == null)
                    return new DirectionCancel() { Success = false, ErrorText = $"Doklad nebyl nalezen; DirectionID: {procModel.DirectionID}" };

                if (!isSuperUser && direction.EntryUserID != _userID)
                    return new DirectionCancel() { Success = false, ErrorText = $"Uživatel není tvůrce dokladu; DirectionID: {procModel.DirectionID}" };

                // make info
                var message = new InternMessage()
                {
                    MsgClass = InternMessage.DR_CANCEL,
                    MsgSeverity = InternMessage.MessageSeverityEnum.Low,
                    MsgHeader = $"Doklad '{direction.DocNumPrefix}/{direction.DocYear}/{direction.DocNumber}' zrušen",
                    MsgDateTime = DateTime.Now,
                    MsgUserID = _userID,
                    MsgText = JsonUtils.ToJsonString("directionID", procModel.DirectionID, "cancelReason", procModel.CancelReason)
                };

                // make history
                var history = new DirectionHistory()
                {
                    DirectionID = procModel.DirectionID,
                    DocPosition = 0,
                    EventCode = Direction.DR_STATUS_CANCEL,
                    EventData = procModel.CancelReason,
                    EntryUserID = _userID,
                    EntryDateTime = DateTime.Now
                };

                using (var tr = db.GetTransaction())
                {
                    // save 
                    direction.DocStatus = Direction.DR_STATUS_CANCEL;
                    db.Update(direction);

                    // save info
                    db.Insert(message);

                    //save history
                    db.Insert(history);

                    // cancel connected moves
                    var storeSave = (new ServicesFactory()).GetSave();
                    if (!storeSave.CancelByDirection(db, _userID, direction.DirectionID, false, out string msg))
                        return new DirectionCancel() { ErrorText = msg };

                    // do save
                    tr.Complete();
                }

                // log
                Info($"DirectionCancelProc; DirectionID: {procModel.DirectionID}");

                return new DirectionCancel() { Success = true };
            }
        }
    }
}

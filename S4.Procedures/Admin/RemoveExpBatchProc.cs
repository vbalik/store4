﻿using System;
using System.Collections.Generic;
using System.Text;
using S4.Entities;
using S4.ProcedureModels;
using S4.ProcedureModels.Admin;
using System.Linq;

namespace S4.Procedures.Admin
{
    public class RemoveExpBatchProc : ProceduresBase<RemoveExpBatch>
    {
        protected bool _expOnly;//remove expiration only
        protected bool _batchOnly;//romove batch only

        public RemoveExpBatchProc(string userID) : base(userID)
        {
        }

        protected override RemoveExpBatch DoIt(RemoveExpBatch procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new RemoveExpBatch() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            var notValidMoves = Core.Singleton<ServicesFactory>.Instance.GetOnStore(false).CheckNotValidMovesByArticle(procModel.ArticleID);
            if (notValidMoves.Count > 0)
                return new RemoveExpBatch() { ErrorText = "Skladová karta je použita v neuzavřených pohybech" };

            var onStore = Core.Singleton<ServicesFactory>.Instance.GetOnStore(true).ByArticle(procModel.ArticleID, StoreCore.Interfaces.ResultValidityEnum.Valid);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                using (var transaction = db.GetTransaction())
                {
                    //get and save old on stores, differenced by new setting and wth sum of quantity
                    var onStores = new List<StoreCore.Interfaces.OnStoreItemFull>();
                    foreach (var onStoreItem in onStore)
                    {
                        //remove batch or expiration info
                        if (!_batchOnly)
                            onStoreItem.ExpirationDate = StoreMoveItem.EMPTY_EXPIRATION;
                        if (!_expOnly)
                            onStoreItem.BatchNum = StoreMoveItem.EMPTY_BATCHNUM;

                        var item = onStores
                            .Where(o => o.ArtPackID == onStoreItem.ArticleID &&
                            o.BatchNum == onStoreItem.BatchNum &&
                            o.ExpirationDate == onStoreItem.ExpirationDate &&
                            o.CarrierNum == onStoreItem.CarrierNum && 
                            o.PositionID == onStoreItem.PositionID)
                            .FirstOrDefault();
                        if (item == null)
                            item = onStoreItem;
                        else
                            item.Quantity += onStoreItem.Quantity;

                        onStores.Add(item);
                    }

                    //make inventory for each position
                    foreach (var positionID in onStores.Select(i => i.PositionID).Distinct())
                    {
                        var saveItems = onStores
                            .Where(i => i.PositionID == positionID)
                            .Select(i => new StoreCore.Interfaces.SaveItem()
                            {
                                ArtPackID = i.ArtPackID,
                                BatchNum = i.BatchNum,
                                CarrierNum = i.CarrierNum,
                                ExpirationDate = i.ExpirationDate,
                                Quantity = i.Quantity
                            })
                            .ToList();

                        var position = db.Query<Position>().Single(p => p.PositionID == positionID);
                        var originStatus = position.PosStatus; 
                        new StockTaking.Helper.StockTakingHelper().SetPositionStatus(db, positionID, Position.POS_STATUS_STOCKTAKING);
                        var res = new StoreCore.Save_Basic.Save().ResetPosition(db, _userID, positionID, StoreMove.MO_PREFIX_ARTCHANGE, saveItems, null, out string msg, true);
                        new StockTaking.Helper.StockTakingHelper().SetPositionStatus(db, positionID, originStatus);

                        if (res == -1)
                        {
                            return new RemoveExpBatch() { ErrorText = $"Proces skončil s chybou: {msg}" };
                        }
                    }

                    transaction.Complete();
                }
            }

            return new RemoveExpBatch() { Success = true };
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Procedures.Admin
{
    public class RemoveExpirationProc : RemoveExpBatchProc
    {
        public RemoveExpirationProc(string userID) : base(userID)
        {
            _expOnly = true;
        }
    }
}

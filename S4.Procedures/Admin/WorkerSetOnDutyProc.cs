﻿using S4.ProcedureModels.Admin;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Procedures.Admin
{
    public class WorkerSetOnDutyProc : ProceduresBase<WorkerSetOnDuty>
    {
        public WorkerSetOnDutyProc(string userID) : base(userID)
        {
        }

        protected override WorkerSetOnDuty DoIt(WorkerSetOnDuty procModel)
        {
            var result = new WorkerSetStatusProc(_userID).DoProcedure(procModel);
            return new WorkerSetOnDuty()
            {
                ErrorText = result.ErrorText,
                NewStatus = result.NewStatus,
                Success = result.Success,
                WorkerID = result.WorkerID
            };
        }
    }
}

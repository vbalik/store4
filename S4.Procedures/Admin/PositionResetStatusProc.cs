﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using S4.Core.Utils;
using S4.Entities;
using S4.ProcedureModels.Admin;


namespace S4.Procedures.Admin
{
    public class PositionResetStatusProc : ProceduresBase<PositionResetStatus>
    {
        public PositionResetStatusProc(string userID) : base(userID)
        {
        }


        protected override PositionResetStatus DoIt(PositionResetStatus procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new PositionResetStatus() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };
            
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //check
                var position = db.SingleOrDefault<Position>("where positionid = @0", procModel.PositionID);
                if (position == null)
                    return new PositionResetStatus() { Success = false, ErrorText = $"Pozice nebyla nalezena; PositionID: {procModel.PositionID}" };

                if (position.PosStatus == Position.POS_STATUS_OK)
                    return new PositionResetStatus() { Success = false, ErrorText = $"Pozice je již odemknuta; PositionID: {procModel.PositionID}" };

                // make info
                var message = new InternMessage()
                {
                    MsgClass = InternMessage.POS_STATUS_OK,
                    MsgSeverity = InternMessage.MessageSeverityEnum.Low,
                    MsgHeader = $"Pozice odemčena '{position.PosCode}:{position.PosDesc}'",
                    MsgDateTime = DateTime.Now,
                    MsgUserID = _userID,
                    MsgText = JsonUtils.ToJsonString("positionID", position.PositionID, "oldStatus", position.PosStatus, "newStatus", Position.POS_STATUS_OK)
                };
                                
                // save
                using (var tr = db.GetTransaction())
                {
                    // save 
                    position.PosStatus = Position.POS_STATUS_OK;
                    db.Update(position);
                                       
                    // save info
                    db.Insert(message);

                    // do save
                    tr.Complete();
                }

                // log
                Info($"PositionResetStatusProc; PositionID: {procModel.PositionID}");

                return new PositionResetStatus() { Success = true };
            }
        }
    }
}

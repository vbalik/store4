﻿using System;
using System.Collections.Generic;
using System.Text;
using S4.Core.Utils;
using S4.Entities;
using S4.ProcedureModels.Admin;

namespace S4.Procedures.Admin
{
    public class DirectionDeleteProc : ProceduresBase<DirectionDelete>
    {
        public DirectionDeleteProc(string userID) : base(userID)
        {
        }


        protected override DirectionDelete DoIt(DirectionDelete procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new DirectionDelete() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //check
                var direction = (new DAL.DirectionDAL()).GetDirectionAllData(procModel.DirectionID);
                if (direction == null)
                    return new DirectionDelete() { Success = false, ErrorText = $"Doklad nebyl nalezen; DirectionID: {procModel.DirectionID}" };

                // do tests
                if (direction.DocStatus != Direction.DR_STATUS_CANCEL)
                    return new DirectionDelete() { Success = false, ErrorText = $"Direction nemá status DR_STATUS_CANCEL; DirectionID: {procModel.DirectionID}" };

                // get Delivery data
                var deliveryDirectionsAllData = (new DAL.DeliveryDirectionDAL()).GetDeliveryDirectionsAllData(procModel.DirectionID, db);

                // make info
                var message = new InternMessage()
                {
                    MsgClass = InternMessage.DR_DELETE,
                    MsgSeverity = InternMessage.MessageSeverityEnum.Low,
                    MsgHeader = $"Doklad smazán '{direction.DocNumPrefix}/{direction.DocYear}/{direction.DocNumber}'",
                    MsgDateTime = DateTime.Now,
                    MsgUserID = _userID,
                    MsgText = JsonUtils.ToJsonString("directionID", procModel.DirectionID)
                };

                // delete
                using (var tr = db.GetTransaction())
                {
                    // delete FK
                    var sql = new NPoco.Sql().Where("directionID = @0", direction.DirectionID);
                    db.Delete<DirectionXtra>(sql);
                    db.Delete<DirectionItem>(sql);
                    db.Delete<DirectionAssign>(sql);
                    db.Delete<DirectionHistory>(sql);

                    // delete DeliveryDirectionItems
                    foreach (var row in deliveryDirectionsAllData)
                    {
                        foreach (var item in row.Items)
                        {
                            var sqlItem = new NPoco.Sql().Where("deliveryDirectionID = @0", item.DeliveryDirectionID);
                            db.Delete<DeliveryDirectionItem>(sqlItem);
                        }
                        // delete DeliveryDirection
                        db.Delete<DeliveryDirection>(row.DeliveryDirectionID);
                    }
                    
                    // delete 
                    db.Delete(direction);

                    // save info
                    db.Insert(message);

                    // do save
                    tr.Complete();
                }

                // log
                base.Info($"DirectionDeleteProc; DirectionID: {procModel.DirectionID}");

                return new DirectionDelete() { Success = true }; ;
            }
        }
    }
}

﻿using S4.ProcedureModels.Admin;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Procedures.Admin
{
    public class WorkerSetOutOfOrderProc : ProceduresBase<WorkerSetOutOfOrder>
    {
        public WorkerSetOutOfOrderProc(string userID) : base(userID)
        {
        }

        protected override WorkerSetOutOfOrder DoIt(WorkerSetOutOfOrder procModel)
        {
            var result = new WorkerSetStatusProc(_userID).DoProcedure(procModel);
            return new WorkerSetOutOfOrder()
            {
                ErrorText = result.ErrorText,
                NewStatus = result.NewStatus,
                Success = result.Success,
                WorkerID = result.WorkerID
            };
        }
    }
}

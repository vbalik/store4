﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using S4.Core.Utils;
using S4.Entities;
using S4.ProcedureModels.Admin;


namespace S4.Procedures.Admin
{
    [ChangesDirectionStatus]
    public class DirectionSetStatusProc : ProceduresBase<DirectionSetStatus>
    {
        public DirectionSetStatusProc(string userID) : base(userID)
        {
        }


        protected override DirectionSetStatus DoIt(DirectionSetStatus procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new DirectionSetStatus() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //check
                var direction = (new DAL.DirectionDAL()).GetDirectionAllData(procModel.DirectionID);
                if (direction == null)
                    return new DirectionSetStatus() { Success = false, ErrorText = $"Doklad nebyl nalezen; DirectionID: {procModel.DirectionID}" };

                // make info
                var message = new InternMessage()
                {
                    MsgClass = InternMessage.DR_STATUS_CHANGED,
                    MsgSeverity = InternMessage.MessageSeverityEnum.Low,
                    MsgHeader = $"Změna stavu dokladu '{direction.DocNumPrefix}/{direction.DocYear}/{direction.DocNumber}'",
                    MsgDateTime = DateTime.Now,
                    MsgUserID = _userID,
                    MsgText = JsonUtils.ToJsonString("directionID", procModel.DirectionID, "oldStatus", direction.DocStatus, "newStatus", procModel.Status)
                };

                // make history
                var history = new DirectionHistory()
                {
                    DirectionID = procModel.DirectionID,
                    DocPosition = 0,
                    EventCode = Direction.DR_EVENT_STATUS_CHANGED,
                    EventData = $"{direction.DocStatus}:{procModel.Status}",
                    EntryUserID = _userID,
                    EntryDateTime = DateTime.Now
                };

                // save
                using (var tr = db.GetTransaction())
                {
                    // save 
                    direction.DocStatus = procModel.Status;
                    db.Update(direction);

                    //his
                    db.Insert(history);

                    // save info
                    db.Insert(message);

                    // do save
                    tr.Complete();
                }

                // log
                base.Info($"DirectionSetStatusProc; DirectionID: {procModel.DirectionID}");

                return new DirectionSetStatus() { Success = true };
            }
        }
    }
}

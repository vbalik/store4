﻿using S4.ProcedureModels.Admin;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Procedures.Admin
{
    public class WorkerSetDisabledProc : ProceduresBase<WorkerSetDisabled>
    {
        public WorkerSetDisabledProc(string userID) : base(userID)
        {
        }

        protected override WorkerSetDisabled DoIt(WorkerSetDisabled procModel)
        {
            var result = new WorkerSetStatusProc(_userID).DoProcedure(procModel);
            return new WorkerSetDisabled()
            {
                ErrorText = result.ErrorText,
                NewStatus = result.NewStatus,
                Success = result.Success,
                WorkerID = result.WorkerID
            };
        }
    }
}

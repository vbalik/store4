﻿using System;
using System.Collections.Generic;
using System.Text;
using S4.Entities;
using S4.ProcedureModels;
using S4.ProcedureModels.Admin;
using System.Linq;

namespace S4.Procedures.Admin
{
    public class RemoveCarrierProc : ProceduresBase<RemoveCarrier>
    {
        public RemoveCarrierProc(string userID) : base(userID)
        {
        }

        protected override RemoveCarrier DoIt(RemoveCarrier procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new RemoveCarrier() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            var notValidMoves = Core.Singleton<ServicesFactory>.Instance.GetOnStore(false).CheckNotValidMoves(new List<int>() { procModel.PositionID });
            if (notValidMoves.Count > 0)
                return new RemoveCarrier() { ErrorText = "Na pozici jsou neuzavřené pohyby!" };

            var onStore = Core.Singleton<ServicesFactory>.Instance.GetOnStore(true).OnPosition(procModel.PositionID, StoreCore.Interfaces.ResultValidityEnum.Valid);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                using (var transaction = db.GetTransaction())
                {
                    //get and save old on stores, differenced by new setting and wth sum of quantity
                    var onStores = new List<StoreCore.Interfaces.OnStoreItemFull>();
                    foreach (var onStoreItem in onStore)
                    {
                        var item = onStores
                            .Where(o => o.ArtPackID == onStoreItem.ArticleID &&
                            o.BatchNum == onStoreItem.BatchNum &&
                            o.ExpirationDate == onStoreItem.ExpirationDate)
                            .FirstOrDefault();
                        if (item == null)
                            item = onStoreItem;
                        else
                            item.Quantity += onStoreItem.Quantity;

                        onStores.Add(item);
                    }

                    //make stocktaking
                    var saveItems = onStores
                        .Select(i => new StoreCore.Interfaces.SaveItem()
                        {
                            ArtPackID = i.ArtPackID,
                            BatchNum = i.BatchNum,
                            CarrierNum = StoreMoveItem.EMPTY_CARRIERNUM,
                            ExpirationDate = i.ExpirationDate,
                            Quantity = i.Quantity
                        })
                        .ToList();

                    var position = db.Query<Position>().Single(p => p.PositionID == procModel.PositionID);
                    var originStatus = position.PosStatus;
                    new StockTaking.Helper.StockTakingHelper().SetPositionStatus(db, procModel.PositionID, Position.POS_STATUS_STOCKTAKING);
                    var res = new StoreCore.Save_Basic.Save().ResetPosition(db, _userID, procModel.PositionID, StoreMove.MO_PREFIX_CARIER_REMOVED, saveItems, null, out string msg, true);
                    new StockTaking.Helper.StockTakingHelper().SetPositionStatus(db, procModel.PositionID, originStatus);

                    if (res == -1)
                    {
                        return new RemoveCarrier() { ErrorText = $"Proces skončil s chybou: {msg}" };
                    }

                    transaction.Complete();
                }
            }

            return new RemoveCarrier() { Success = true };
        }
    }
}

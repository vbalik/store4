﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace S4.Procedures
{
    class TransCounter
    {
        protected static long _numCounter = 0;


        internal static long NextNumber()
        {
            return Interlocked.Increment(ref _numCounter);
        }
    }
}

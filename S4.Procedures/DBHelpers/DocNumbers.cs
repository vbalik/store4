﻿using NPoco;
using S4.Core;
using S4.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Procedures.DBHelpers
{
    internal static class DocNumbers
    {
        public static int GetNextDocNumber(IDatabase db, StoreMove storeMove)
        {
            return Singleton<DAL.DocNumbers.NextNumberBase>.Instance.NextNumber_StoreMove.GetNextDocNumber(db, storeMove.DocNumPrefix);
        }


        public static int GetNextDocNumber(IDatabase db, Direction direction)
        {
            return Singleton<DAL.DocNumbers.NextNumberBase>.Instance.NextNumber_Direction.GetNextDocNumber(db, direction.DocNumPrefix);
        }
    }
}

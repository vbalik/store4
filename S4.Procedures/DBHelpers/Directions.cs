﻿using NPoco;
using S4.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace S4.Procedures.DBHelpers
{
    internal class Directions
    {
        public List<Direction> ScanByAssigment(NPoco.Database db, List<string> statuses, string jobID, string workerID)
        {
            var sql = new Sql();
            sql.From("dbo.s4_direction WITH (NOLOCK)");
            sql.Where("s4_direction.docStatus IN (@0)", statuses);
            sql.Where("(EXISTS (SELECT * FROM dbo.s4_direction_assign asi WHERE (asi.directionID = dbo.s4_direction.directionID) AND (asi.jobID = @0) AND (asi.workerID = @1)))", jobID, workerID);
            sql.OrderBy("directionID");
            var dirs = db.Fetch<Direction>(sql);

            return dirs;
        }


        internal static int FindParentDocPosition(int artPackID, List<DirectionItem> items)
        {
            var item = items.Where(i => i.ArtPackID == artPackID).FirstOrDefault();
            return (item == null) ? 0 : item.DocPosition;
        }
    }
}

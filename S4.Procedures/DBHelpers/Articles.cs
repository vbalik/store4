﻿using S4.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Procedures.DBHelpers
{
    class Articles
    {
        public static void FillArticleData(Entities.Helpers.ItemInfo item)
        {
            var packing = Singleton<DAL.Cache.ArticlePackingCache>.Instance.GetItem(item.ArtPackID);
            var article = Singleton<DAL.Cache.ArticleModelCache>.Instance.GetItem(packing.ArticleID);
            item.ArticleID = article.ArticleID;
            item.PackDesc = packing.PackDesc;
            item.ArticleCode = article.ArticleCode;
            item.ArticleDesc = article.ArticleDesc;
            item.UseBatch = article.UseBatch;
            item.UseExpiration = article.UseExpiration;
            item.UserRemark = article.UserRemark;
            item.SpecFeaturesInt = article.SpecFeaturesInt;
        }


        public static string GetArticleCode(int artPackID)
        {
            var packing = Singleton<DAL.Cache.ArticlePackingCache>.Instance.GetItem(artPackID);
            if (packing == null)
                return "???";
            else
            {
                var article = Singleton<DAL.Cache.ArticleModelCache>.Instance.GetItem(packing.ArticleID);
                return (article == null) ? "???" : article.ArticleCode;
            }
        }
    }
}

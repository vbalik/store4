﻿using NPoco;
using S4.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Procedures.DBHelpers
{
    internal class StoreMoves
    {
        public List<StoreMove> ScanByParentDirID(NPoco.Database db, List<string> statuses, int directionID)
        {
            var sql = new Sql();
            sql.Where("docStatus IN (@0)", statuses);
            sql.Where("parent_directionID = @0", directionID);
            sql.OrderBy("stoMoveID");
            var moves = db.Fetch<StoreMove>(sql);

            return moves;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Procedures.Messages
{
    public class DirectionStatusUpdated : S4.Messages.MessageBase
    {
        public int DirectionID { get; set; }
    }
}

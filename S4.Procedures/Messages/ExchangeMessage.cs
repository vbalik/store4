﻿namespace S4.Procedures.Messages
{
    public class ExchangeMessage : S4.Messages.MessageBase
    {
        public string[] JobNames { get; set; }
    }

}

﻿namespace S4.Procedures.Messages
{
    public class DirectionItemUpdated : S4.Messages.MessageBase
    {
        public int DirectionID { get; set; }
    }
}

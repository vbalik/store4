﻿namespace S4.Procedures.Messages
{
    public class ProgressMessage : S4.Messages.MessageBase
    {
        public string User { get; set; }
        public string Header { get; set; }
        public string Message { get; set; }
        public NotifyType NotifyType { get; set; }
        public string TaskID { get; set; }
        public int ProgressValue { get; set; }
        public string TransName { get; set; }
    }

    public enum NotifyType
    {
        Success,
        Info,
        Warning,
        Danger
    }
}

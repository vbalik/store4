﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Procedures.Messages
{
    public class ProcedureLogItem : S4.Messages.MessageBase
    {
        public long TransactionNum { get; set; }
        public string ProcedureName { get; set; }
        public DateTime StartTime { get; set; }
        public bool ProcedureSuccess { get; set; }
        public long RunTime { get; set; }


        public override string ToString()
        {
            return $"{TransactionNum}\t{ProcedureName}\t{StartTime:yyyy-MM-dd HH:mm:ss}\t{ProcedureSuccess}\t{RunTime}\r\n";
        }

    }
}

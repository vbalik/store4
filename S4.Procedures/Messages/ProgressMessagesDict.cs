﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace S4.Procedures.Messages
{
    public class ProgressMessagesDict
    {
        public Dictionary<string, ProgressMessage> Messages { get; private set; } = new Dictionary<string, ProgressMessage>();
        private readonly object _lock = new object();
        private const int MINUTES_TO_CLEAN = 3;

        public void AddOrUpdateMessage(string taskID, ProgressMessage msgItem)
        {
            lock (_lock)
            {
                if (Messages.TryGetValue(taskID, out _))
                    Messages[taskID] = msgItem;
                else
                    Messages.Add(taskID, msgItem);
            }
        }

        public void RemoveOldMessagess()
        {
            lock (_lock)
            {
                var toRemove = (from v in Messages
                                where DateTime.Now > v.Value.MessageCreated.AddMinutes(MINUTES_TO_CLEAN) || v.Value.ProgressValue == 100 ||
                                v.Value.NotifyType == NotifyType.Danger || v.Value.NotifyType == NotifyType.Success
                                select v).ToList();
                
                foreach (var remove in toRemove)
                    Messages.Remove(remove.Key);

            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using NPoco;
using S4.Core;
using S4.Entities;
using S4.ProcedureModels.Dispatch;
using S4.Report.Model.Dispatch;
using S4.StoreCore.Interfaces;
using System.Linq;
using S4.Entities.Helpers.Xml.Dispatch;
using S4.DAL;
using QRCoder;
using System.Drawing;
using S4.Core.EAN;
using System.Drawing.Drawing2D;
using S4.Entities.Helpers.Xml.Article;
using S4.Procedures.Load.Helper;
using S4.Core.ADR;
using S4.DAL.Cache;
using S4.ProcedureModels.Info;

namespace S4.Procedures.Info
{
    public class PrintShippingDocumentADRProc : ProceduresBase<PrintShippingDocumentADR>
    {
        public PrintShippingDocumentADRProc(string userID) : base(userID)
        {
        }


        protected override PrintShippingDocumentADR DoIt(PrintShippingDocumentADR procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new PrintShippingDocumentADR() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                if (procModel.ReportID == 0)
                    procModel.ReportID = Singleton<SettingsSet>.Instance.GetValue_Int32(SettingsDictionary.DISPATCH_PD_ADR_REPORT_ID);

                // get direction
                var direction = new DirectionDAL().GetDirectionAllData(procModel.DirectionID);
                if (direction == null)
                    return new PrintShippingDocumentADR() { Success = false, ErrorText = $"Doklad nebyl nalezen; DirectionID: {procModel.DirectionID}" };

                var cachePartner = new PartnerCache();
                var partner = cachePartner.GetItem(direction.PartnerID);
                var partnerInfo = new StringBuilder();
                var ohrozujeProstrediInfo = new StringBuilder();

                if (partner != null)
                {
                    partnerInfo.AppendLine(partner.PartnerName);
                    partnerInfo.AppendLine(partner.PartnerAddr_1);
                    partnerInfo.AppendLine($"{partner.PartnerCity} {partner.PartnerAddr_2}");
                    partnerInfo.AppendLine(partner.PartnerPhone);
                }

                //make data to report
                var toReport = new List<DocumentADRModel>();
                foreach (var item in direction.Items)
                {
                    var articlePacking = Singleton<ArticlePackingCache>.Instance.GetItem(item.ArtPackID);
                    var article = Singleton<ArticleModelCache>.Instance.GetItem(articlePacking.ArticleID);

                    // xtra
                    article.XtraData.Load(db, new Sql().Where("articleID = @0", article.ArticleID));

                    if (article.XtraData != null)
                    {
                        var UNKod = !article.XtraData.IsNull(Article.XTRA_UN_KOD) ? article.XtraData[Article.XTRA_UN_KOD].ToString() : string.Empty;
                        var nazev = !article.XtraData.IsNull(Article.XTRA_OFICIALNI_NAZEV) ? article.XtraData[Article.XTRA_OFICIALNI_NAZEV].ToString() : string.Empty;
                        var znacka = !article.XtraData.IsNull(Article.XTRA_BEZPECNOSTI_ZNACKA) ? article.XtraData[Article.XTRA_BEZPECNOSTI_ZNACKA].ToString() : string.Empty;
                        var obalSkupina = !article.XtraData.IsNull(Article.XTRA_OBALOVA_SKUPINA) ? article.XtraData[Article.XTRA_OBALOVA_SKUPINA].ToString() : string.Empty;
                        var kodTunelu = !article.XtraData.IsNull(Article.XTRA_KOD_TUNELU) ? article.XtraData[Article.XTRA_KOD_TUNELU].ToString() : string.Empty;
                        var druhObalu = !article.XtraData.IsNull(Article.XTRA_DRUH_OBALU) ? article.XtraData[Article.XTRA_DRUH_OBALU].ToString() : string.Empty;

                        var ohrozujeProstredi = false;
                        if (!article.XtraData.IsNull(Article.XTRA_OHROZUJE_PROSTREDI))
                            bool.TryParse(article.XtraData[Article.XTRA_OHROZUJE_PROSTREDI].ToString(), out ohrozujeProstredi);
                                                
                        int? prepravKategorie = null;
                        if (!article.XtraData.IsNull(Article.XTRA_PREPRAVNI_KATEGORIE) && int.TryParse(article.XtraData[Article.XTRA_PREPRAVNI_KATEGORIE].ToString(), out int pk))
                            prepravKategorie = pk;

                        var mnozstvi = Math.Abs(item.Quantity);

                        int? pocetBodu = null;
                        if (!article.XtraData.IsNull(Article.XTRA_OBJEM_KOEF) && prepravKategorie.HasValue)
                        {
                            if (float.TryParse(article.XtraData[Article.XTRA_OBJEM_KOEF].ToString(), out float objemKoef))
                            {
                                pocetBodu = ADR.SafetyPointCalculate(prepravKategorie.Value, (int)mnozstvi, objemKoef);
                            }
                        }

                        if (!string.IsNullOrEmpty(UNKod) || !string.IsNullOrEmpty(nazev))
                        {
                            if (ohrozujeProstredi)
                            {
                                ohrozujeProstrediInfo.AppendLine(UNKod);
                            }

                            var row = new DocumentADRModel
                            {
                                UNKod = UNKod,
                                Nazev = nazev,
                                Znacka = znacka,
                                ObalSkupina = obalSkupina,
                                KodTunelu = kodTunelu,
                                DruhObalu = druhObalu,
                                Mnozstvi = mnozstvi,
                                PrepravKategorie = prepravKategorie,
                                PocetBodu = pocetBodu,
                            };

                            toReport.Add(row);
                        }
                    }
                }

                //P R I N T
                //get report
                var report = db.SingleOrDefault<Reports>("where reportID = @0", procModel.ReportID);
                if (report == null)
                    return new PrintShippingDocumentADR() { Success = false, ErrorText = $"Report nebyl nalezen; reportID: {procModel.ReportID}" };

                var printerLocation = Singleton<PrinterLocationCache>.Instance.GetItem(procModel.PrinterLocationID);

                if (printerLocation == null)
                    return new PrintShippingDocumentADR() { Success = false, ErrorText = $"PrinterLocation nebyl nalezen; printerLocationID: {procModel.PrinterLocationID}" };

                //variables
                var variables = new List<Tuple<string, object, Type>>
                {
                    new Tuple<string, object, Type>("DocInfo", $"{direction.DocNumPrefix}/{direction.DocYear}/{direction.DocNumber}", typeof(string)),
                    new Tuple<string, object, Type>("DocDate", direction.DocDate, typeof(DateTime)),
                    new Tuple<string, object, Type>("Partner", partnerInfo.ToString(), typeof(string)),
                    new Tuple<string, object, Type>("OhrozujeProstrediInfo", ohrozujeProstrediInfo.ToString(), typeof(string))
                };

                //do print
                Report.Print print = new Report.Print();
                print.Variables = variables;
                print.ReportDefinition = report.Template; //šablona z DB
                //print.DesignMode = true; //pouze pro úpravy šablony
                //print.ReportDefFilePath = @"c:\Projects\store4\Tools\Reporty\PrepravniDoklad.srt"; //pouze pro úpravy šablony

                byte[] fileData = null;
                if (IsTesting)
                {
                    fileData = print.DoPrint2Text(toReport);
                }
                else
                {
                    if (procModel.LocalPrint)
                        fileData = print.DoPrint2PDF(toReport);
                    else
                        print.DoPrint(printerLocation.PrinterPath, toReport);
                }

                if (print.IsError)
                    return new PrintShippingDocumentADR() { Success = false, ErrorText = print.ErrorText };
                else
                    return new PrintShippingDocumentADR() { Success = true, LocalPrint = procModel.LocalPrint, FileData = fileData };
            }


        }

        private Bitmap GetQrImage(DispatchSNReportModel row)
        {
            // barcode should be 14 characters; if it is 13, add '0' at begining
            // other problems are handled in GetEANCode function
            var barCode = row.BarCode.Length == 13 ? "0" + row.BarCode : row.BarCode;

            var ean = new EAN
            {
                Parts = new List<EANPart> {
                        { new EANPart { EANCode = EANCodeEnum.ShippingContainerCode, Value = barCode } },
                        { new EANPart { EANCode = EANCodeEnum.SerialNumber, Value = row.SerialNumber } }}
            };

            if (!string.IsNullOrWhiteSpace(row.BatchNum))
                ean.Parts.Add(new EANPart { EANCode = EANCodeEnum.BatchNumber, Value = row.BatchNum });

            if (row.ExpirationDate.HasValue)
                ean.Parts.Add(new EANPart { EANCode = EANCodeEnum.ExpirationDate, Value = row.ExpirationDate.Value.ToString("yyMMdd") });

            if (ean.GetEANCode(out string eanCode, out string _))
            {
                //QR Code using
                using (QRCodeGenerator qrGenerator = new QRCodeGenerator())
                {
                    QRCodeData qrCodeData = qrGenerator.CreateQrCode(eanCode, QRCodeGenerator.ECCLevel.L);
                    QRCode qrCode = new QRCode(qrCodeData);
                    Bitmap qrCodeImage = qrCode.GetGraphic(20);
                    return qrCodeImage;
                }
            }
            else
            {
                using (Font font = new Font("Arial", 36, FontStyle.Bold, GraphicsUnit.Point))
                {
                    //error 
                    var bmp = new Bitmap(1, 1);
                    bmp.SetPixel(0, 0, Color.WhiteSmoke);
                    bmp = new Bitmap(bmp, 200, 200);

                    //write text to bitmap
                    RectangleF rectf = new RectangleF(0, 0, 200, 200);
                    Graphics g = Graphics.FromImage(bmp);

                    g.SmoothingMode = SmoothingMode.AntiAlias;
                    g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    g.PixelOffsetMode = PixelOffsetMode.HighQuality;

                    StringFormat stringFormat = new StringFormat();
                    stringFormat.Alignment = StringAlignment.Center;
                    stringFormat.LineAlignment = StringAlignment.Center;

                    g.DrawString("?", font, Brushes.Red, rectf, stringFormat);
                    g.Flush();

                    return bmp;
                }
            }
        }
    }


}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPoco;
using S4.Core;
using S4.Entities;
using S4.ProcedureModels.Info;
using S4.StoreCore.Interfaces;

namespace S4.Procedures.Info
{
    public class OnStorePrintProc : ProceduresBase<OnStorePrint>
    {
        public OnStorePrintProc(string userID) : base(userID)
        {
        }


        protected override OnStorePrint DoIt(OnStorePrint procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new OnStorePrint() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                if (procModel.ReportID == 0)
                {
                    procModel.ReportID = Singleton<DAL.SettingsSet>.Instance.GetValue_Int32(Entities.SettingsDictionary.ON_STORE_REPORT_ID);
                }

                // get data
                var onStore = Singleton<ServicesFactory>.Instance.GetOnStore();
                List<OnStoreItemFull> storeItems = null;
                string headerText = null;

                switch (procModel.OnStorePrintSource)
                {
                    case OnStorePrint.OnStorePrintSourceEnum.OnStorePrintSourcePosition:
                        storeItems = onStore.OnPosition(procModel.SourceID, ResultValidityEnum.Valid);
                        // sort
                        storeItems = storeItems
                            .OrderBy(i => i.Article.ArticleCode)
                            .ThenBy(i => i.BatchNum)
                            .ToList();
                        // header
                        var position = Singleton<DAL.Cache.PositionCache>.Instance.GetItem(procModel.SourceID);
                        headerText = $"Pozice {position.PosCode}";
                        break;

                    case OnStorePrint.OnStorePrintSourceEnum.OnStorePrintSourceArticle:
                        storeItems = onStore.ByArticle(procModel.SourceID, ResultValidityEnum.Valid);
                        // filter - only store positions & sort
                        storeItems = storeItems
                            .Where(i => i.Position.PosCateg == Position.PositionCategoryEnum.Store)
                            .OrderBy(i => i.Position.PosCode)
                            .ThenBy(i => i.BatchNum)
                            .ToList();
                        // header
                        var article = Singleton<DAL.Cache.ArticleModelCache>.Instance.GetItem(procModel.SourceID);
                        headerText = $"Karta {article.ArticleCode} - {article.ArticleDesc}";
                        break;

                    default:
                        throw new NotImplementedException(procModel.OnStorePrintSource.ToString());
                }


                // get report
                var report = db.SingleOrDefault<Reports>("where reportID = @0", procModel.ReportID);
                if (report == null)
                    return new OnStorePrint() { Success = false, ErrorText = $"Report nebyl nalezen; reportID: {procModel.ReportID}" };

                // get printer location
                var printerlocation = db.SingleOrDefault<PrinterLocation>("where printerLocationID = @0", procModel.PrinterLocationID);
                if (printerlocation == null)
                    return new OnStorePrint() { Success = false, ErrorText = $"PrinterLocation nebyl nalezen; printerLocationID: {procModel.PrinterLocationID}" };

                // variables
                var variables = new List<Tuple<string, object, Type>>();
                variables.Add(new Tuple<string, object, Type>("OnStorePrintSource", procModel.OnStorePrintSource.ToString(), typeof(string)));
                variables.Add(new Tuple<string, object, Type>("Header", headerText, typeof(string)));
                variables.Add(new Tuple<string, object, Type>("PrintDateTime", DateTime.Now, typeof(DateTime)));

                //do print
                Report.Print print = new Report.Print();
                print.Variables = variables;
                print.ReportDefinition = report.Template; //šablona z DB
                //print.DesignMode = true; //pouze pro úpravy šablony
                //print.ReportDefFilePath = @"c:\Projects\store4\Tools\Reporty\NaSklade.srt"; //pouze pro úpravy šablony

                byte[] fileData = null;
                if (IsTesting)
                {
                    fileData = print.DoPrint2Text(storeItems);
                }
                else
                {
                    if (procModel.LocalPrint)
                        fileData = print.DoPrint2PDF(storeItems);
                    else
                        print.DoPrint(printerlocation.PrinterPath, storeItems);
                }

                if (print.IsError)
                    return new OnStorePrint() { Success = false, ErrorText = print.ErrorText };
                else
                    return new OnStorePrint() { Success = true, LocalPrint = procModel.LocalPrint, FileData = fileData };
            }
        }
    }

}

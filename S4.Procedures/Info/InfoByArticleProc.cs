﻿using NPoco;
using S4.Core;
using S4.Entities;
using S4.Procedures.DBHelpers;
using S4.ProcedureModels.Info;
using S4.StoreCore.Interfaces;
using S4.StoreCore.OnStore_Basic;
using System;
using System.Collections.Generic;
using System.Linq;

namespace S4.Procedures.Info
{
    public class InfoByArticleProc : ProceduresBase<InfoByArticle>
    {
        public InfoByArticleProc(string userID) : base(userID)
        {
        }

        protected override InfoByArticle DoIt(InfoByArticle procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new InfoByArticle() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var onStore = Singleton<ServicesFactory>.Instance.GetOnStore();
                procModel.OnStoreItemFull = onStore.ByArticle(procModel.ArticleID, ResultValidityEnum.Valid);
            }

            procModel.Success = true;
            return procModel;
        }

    }
}

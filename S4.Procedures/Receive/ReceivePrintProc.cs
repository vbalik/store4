﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using NPoco;
using S4.Core;
using S4.Core.Data;
using S4.Entities;
using S4.Entities.Models;
using S4.ProcedureModels.Receive;
using S4.Report.Model.Dispatch;
using S4.StoreCore.Interfaces;

namespace S4.Procedures.Receive
{
    public class ReceivePrintProc : ProceduresBase<ReceivePrint>
    {
        public ReceivePrintProc(string userID) : base(userID)
        {
        }


        protected override ReceivePrint DoIt(ReceivePrint procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new ReceivePrint() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                if (procModel.ReportID == 0)
                    procModel.ReportID = Singleton<DAL.SettingsSet>.Instance.GetValue_Int32(SettingsDictionary.RECEIVE_REPORT_ID);

                if (procModel.PrinterLocationID == 0)
                    procModel.PrinterLocationID = Singleton<DAL.SettingsSet>.Instance.GetValue_Int32(SettingsDictionary.RECEIVE_REPORT_DEFAULT_PRINTER_ID);

                //get storeMove
                var storeMove = (new DAL.StoreMoveDAL()).GetStoreMoveAllData(procModel.StoMoveID);
                if (storeMove == null)
                    return new ReceivePrint() { Success = false, ErrorText = $"StoreMove nebylo nalezeno; StMoveID: {procModel.StoMoveID}" };

                //get direction
                var direction = (new DAL.DirectionDAL()).Get(storeMove.Parent_directionID);

                //get move
                var moveResult = from mr in MakeItems(storeMove.Items.Where(x => x.ItemValidity == StoreMoveItem.MI_VALIDITY_VALID).ToList())
                                 group mr by mr.ArtPackID into r
                                 select new { ArtPackID = r.Key, Other = r.ToList() };

                //make data to report
                var toReport = new List<ReceiveReportModel>();
                var groupNum = 0;
                foreach (var move in moveResult)
                {
                    var articlePacking = Singleton<DAL.Cache.ArticlePackingCache>.Instance.GetItem(move.ArtPackID);
                    var articleModel = Singleton<DAL.Cache.ArticleModelCache>.Instance.GetItem(articlePacking.ArticleID);

                    //first row
                    var row1 = new ReceiveReportModel();
                    row1.GroupNum = groupNum;
                    row1.Header = true;
                    row1.Code = articleModel.ArticleCode;
                    row1.Name = articleModel.ArticleDesc;
                    row1.Quantity = move.Other.Sum(x => x.Quantity);
                    row1.Unit = articlePacking.PackDesc;
                    toReport.Add(row1);

                    //second row
                    var rows = from o in move.Other
                               group o by o.BatchNum into g
                               select new { BatchNum = g.Key, Other = g.ToList() };

                    foreach (var row in rows.OrderBy(x => x.BatchNum))
                    {
                        var row2 = new ReceiveReportModel();
                        row2.GroupNum = groupNum;
                        row2.Header = false;

                        row2.Name = row.BatchNum;
                        row2.Expiration = !row.Other.First().ExpirationDate.HasValue ? "" : row.Other.First().ExpirationDate.Value.Date.ToString("dd.MM.yy");
                        row2.Quantity = row.Other.Sum(x => x.Quantity);
                        toReport.Add(row2);
                    }
                    groupNum++;
                }

                //P R I N T
                //get report
                var report = db.SingleOrDefault<Reports>("where reportID = @0", procModel.ReportID);
                if (report == null)
                    return new ReceivePrint() { Success = false, ErrorText = $"Report nebyl nalezen; reportID: {procModel.ReportID}" };

                //get printer location
                var printerlocation = db.SingleOrDefault<PrinterLocation>("where printerLocationID = @0", procModel.PrinterLocationID);
                if (printerlocation == null)
                    return new ReceivePrint() { Success = false, ErrorText = $"PrinterLocation nebyl nalezen; printerLocationID: {procModel.PrinterLocationID}" };

                //variables
                var variables = new List<Tuple<string, object, Type>>();
                variables.Add(new Tuple<string, object, Type>("DocInfo", $"{direction.DocNumPrefix}/{direction.DocYear}/{direction.DocNumber}", typeof(string)));
                variables.Add(new Tuple<string, object, Type>("DocDate", direction.DocDate, typeof(DateTime)));
                variables.Add(new Tuple<string, object, Type>("Version", Assembly.GetEntryAssembly().GetCustomAttribute<System.Reflection.AssemblyInformationalVersionAttribute>().InformationalVersion, typeof(string)));

                //do print
                Report.Print print = new Report.Print();
                print.Variables = variables;
                print.ReportDefinition = report.Template; //šablona z DB
                //print.DesignMode = true; //pouze pro úpravy šablony
                //print.ReportDefFilePath = @"c:\Projects\store4\Tools\Reporty\VydejovyList.srt"; //pouze pro úpravy šablony
                byte[] fileData = null;
                if (IsTesting)
                {
                    fileData = print.DoPrint2Text(toReport);
                }
                else
                {
                    if (procModel.LocalPrint)
                        fileData = print.DoPrint2PDF(toReport);
                    else
                        print.DoPrint(printerlocation.PrinterPath, toReport);
                }

                if (print.IsError)
                    return new ReceivePrint() { Success = false, ErrorText = print.ErrorText };
                else
                    return new ReceivePrint() { Success = true, LocalPrint = procModel.LocalPrint, FileData = fileData };
            }
        }

        #region Helper

        private List<MoveResultPrint> MakeItems(List<StoreMoveItemModel> moveItems)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // make summary
                var result = moveItems
                    .GroupBy(
                        k => new { k.Parent_docPosition, k.StoMoveLotID, k.CarrierNum },
                        (k, e) => new MoveResultPrint() { DocPosition = k.Parent_docPosition, StoMoveLotID = k.StoMoveLotID, CarrierNum = k.CarrierNum, Quantity = e.Sum(i => i.Quantity * i.ItemDirection) })
                    .ToList();

                // find lots
                result.ForEach(item =>
                {
                    var lot = db.Query<StoreMoveLot>().Where(l => l.StoMoveLotID == item.StoMoveLotID).Single();
                    var articlePacking = Singleton<DAL.Cache.ArticlePackingCache>.Instance.GetItem(lot.ArtPackID);
                    var articleModel = Singleton<DAL.Cache.ArticleModelCache>.Instance.GetItem(articlePacking.ArticleID);
                    item.ArtPackID = lot.ArtPackID;
                    item.BatchNum = lot.BatchNum;
                    item.ExpirationDate = lot.ExpirationDate;
                    item.ArticlePacking = articlePacking;
                    item.ArticleModel = articleModel;
                    item.ArticleID = articleModel.ArticleID;
                });

                RepairEmptyFields_Show(result);
                return result.OrderBy(x => x.ArticleModel.ArticleCode).ToList();
            }
        }

        private void RepairEmptyFields_Show<T>(List<T> result) where T : OnStoreItem
        {
            var proxy = new EmptyDataProxy();
            result.ForEach(i =>
            {
                proxy.ToShow(i.CarrierNum, StoreMoveItem.EMPTY_CARRIERNUM, value => i.CarrierNum = value);
                proxy.ToShow(i.BatchNum, StoreMoveItem.EMPTY_BATCHNUM, value => i.BatchNum = value);
                proxy.ToShow(i.ExpirationDate, StoreMoveItem.EMPTY_EXPIRATION, value => i.ExpirationDate = value);
            });
        }

        #endregion
    }

    #region helper class

    public class MoveResultPrint : MoveResult
    {
        public ArticlePacking ArticlePacking { get; set; }
        public ArticleModel ArticleModel { get; set; }
    }

    #endregion
}

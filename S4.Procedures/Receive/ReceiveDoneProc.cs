﻿using System;
using System.Collections.Generic;
using System.Text;
using S4.Core;
using S4.Entities;
using S4.ProcedureModels.Receive;

namespace S4.Procedures.Receive
{
    public class ReceiveDoneProc : ProceduresBase<ReceiveDone>
    {
        public ReceiveDoneProc(string userID) : base(userID)
        {
        }


        protected override ReceiveDone DoIt(ReceiveDone procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new ReceiveDone() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // check
                var storeMove = db.SingleOrDefault<StoreMove>("where stoMoveID = @0", procModel.StoMoveID);
                if (storeMove == null)
                    return new ReceiveDone() { Success = false, ErrorText = $"StoreMove nebylo nalezeno; StMoveID: {procModel.StoMoveID}" };

                if (storeMove.DocStatus != StoreMove.MO_STATUS_NEW_RECEIVE)
                    return new ReceiveDone() { Success = false, ErrorText = $"Doklad nemá očekávaný status MO_STATUS_NEW_RECEIVE; StoMoveID: {procModel.StoMoveID}" };

                // set status
                storeMove.DocStatus = StoreMove.MO_STATUS_SAVED_RECEIVE;
                var history = new StoreMoveHistory()
                {
                    EntryDateTime = DateTime.Now,
                    EntryUserID = _userID,
                    EventCode = StoreMove.MO_STATUS_SAVED_RECEIVE,
                    StoMoveID = storeMove.StoMoveID
                };

                using (var tr = db.GetTransaction())
                {
                    var save = Singleton<ServicesFactory>.Instance.GetSave();

                    // set items saved
                    var storeMoveItems = db.Fetch<StoreMoveItem>("where stoMoveID = @0 and itemValidity != @1", storeMove.StoMoveID, StoreMoveItem.MI_VALIDITY_CANCELED);
                    foreach (var item in storeMoveItems)
                    {
                        // update item
                        item.ItemValidity = StoreMoveItem.MI_VALIDITY_VALID;
                        db.Update(item);

                        // invalidate store status
                        save.InvalidateStorageLot(db, _userID, item.StoMoveLotID);
                    }
                    
                    // update move
                    db.Update(storeMove);

                    // insert move hist
                    db.Insert(history);
                    
                    // commit
                    tr.Complete();
                }

                // log
                base.Info($"ReceiveDoneProc; StoMoveID: {procModel.StoMoveID};");

                return new ReceiveDone() { Success = true };
            }
        }
    }
}

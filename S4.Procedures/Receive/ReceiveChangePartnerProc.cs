﻿using System;
using System.Collections.Generic;
using System.Text;
using S4.DAL.Cache;
using S4.Entities;
using S4.ProcedureModels.Receive;

namespace S4.Procedures.Receive
{
    public class ReceiveChangePartnerProc : ProceduresBase<ReceiveChangePartner>
    {
        public ReceiveChangePartnerProc(string userID) : base(userID)
        {
        }


        protected override ReceiveChangePartner DoIt(ReceiveChangePartner procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new ReceiveChangePartner() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            var direction = new DAL.DirectionDAL().Get(procModel.DirectionID);
            if(direction == null)
                return new ReceiveChangePartner() { Success = false, ErrorText = $"Doklad nenalezen; DirectionID: {procModel.DirectionID}" };

            if (direction.DocStatus != Direction.DR_STATUS_RECEIVE_PROC)
                return new ReceiveChangePartner() { Success = false, ErrorText = $"Doklad nemá očekávaný status DR_STATUS_RECEIVE_PROC; DirectionID: {procModel.DirectionID}" };

            int partnerID = Partner.DEFAULT_PARTNER_ID;
            if (procModel.PartnerID != null)
                partnerID = new PartnerCache().GetItem((int) procModel.PartnerID).PartnerID;

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                direction.PartnerID = partnerID;
                direction.PartnerRef = procModel.PartnerRef;
                direction.DocRemark = procModel.DocRemark;

                db.Update(direction);
            }

            // log
            base.Info($"ReceiveChangePartnerProc; DirectionID: {procModel.DirectionID}");

           return new ReceiveChangePartner() { Success = true };
        }
    }
}

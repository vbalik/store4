﻿using S4.Core;
using S4.Entities;
using S4.ProcedureModels.Receive;
using S4.StoreCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace S4.Procedures.Receive
{
    public class ReceiveSimpleProc : ProceduresBase<ReceiveSimple>
    {
        public ReceiveSimpleProc(string userID) : base(userID)
        {
        }

        protected override ReceiveSimple DoIt(ReceiveSimple procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new ReceiveSimple() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            int storeMoveID = 0;

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // check if quantity is not negative
                if (procModel.Quantity < 1)
                    return new ReceiveSimple() { Success = false, ErrorText = $"Mnozstvi nesmi byt mensi nez 1." };

                //check if position exists and article exists
                var article = Core.Singleton<DAL.Cache.ArticleModelCache>.Instance.GetItem(procModel.ArticleID);
                if (article.ArticleStatus == Article.AR_STATUS_DISABLED)
                    return new ReceiveSimple() { Success = false, ErrorText = $"Karta má stav zrušeno; ArtID: {article.ArticleID}" };

                var position = Core.Singleton<DAL.Cache.PositionCache>.Instance.GetItem((int)procModel.PositionID);
                if (position is null || position.PosStatus == Position.POS_STATUS_BLOCKED)
                    return new ReceiveSimple() { Success = false, ErrorText = $"Pozice má stav blokovano; PosID: {procModel.PositionID}" };

                var saveItemList = new List<SaveItem>()
                {
                    new SaveItem()
                    {
                        ArtPackID = article.Packings.Where(p => p.MovablePack).Single().ArtPackID,
                        Quantity = procModel.Quantity,
                        ExpirationDate = DateTime.ParseExact(procModel.ExpirationDate, "dd.MM.yyyy", CultureInfo.InvariantCulture),
                        Xtra_PositionID = procModel.PositionID
                    }
                };

                // do receive (StoreIn)
                var save = Singleton<ServicesFactory>.Instance.GetSave();
                var savedID = save.StoreIn(
                    db, _userID, procModel.PositionID, StoreMove.MO_PREFIX_RECEIVE, saveItemList, true,
                        (storeMove) =>
                        {
                            storeMove.DocRemark = procModel.DocumentRemark;
                        },
                        null,
                        out string message);
                if (savedID == -1)
                    return new ReceiveSimple() { ErrorText = message };
                else
                    storeMoveID = savedID;
            }

            return new ReceiveSimple() { Success = true, StoreMoveID = storeMoveID };
        }
    }
}

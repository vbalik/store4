﻿using System;
using System.Collections.Generic;
using System.Text;

using S4.Entities;
using S4.ProcedureModels.Receive;

namespace S4.Procedures.Receive
{
    [ChangesDirectionStatus]
    public class ReceiveCancelProc : ProceduresBase<ReceiveCancel>
    {
        public ReceiveCancelProc(string userID) : base(userID)
        {
        }


        protected override ReceiveCancel DoIt(ReceiveCancel procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new ReceiveCancel() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //check
                var storeMove = db.SingleOrDefault<StoreMove>("where stoMoveID = @0", procModel.StoMoveID);
                if (storeMove == null)
                    return new ReceiveCancel() { Success = false, ErrorText = $"StoreMove nebylo nalezeno; StMoveID: {procModel.StoMoveID}" };

                if (storeMove.DocStatus != StoreMove.MO_STATUS_NEW_RECEIVE)
                    return new ReceiveCancel() { Success = false, ErrorText = $"Doklad nemá očekávaný status MO_STATUS_NEW_RECEIVE; StoMoveID: {procModel.StoMoveID}" };

                var storeMoveHistory = new StoreMoveHistory()
                {
                    EntryDateTime = DateTime.Now,
                    EntryUserID = _userID,
                    EventCode = StoreMove.MO_STATUS_CANCEL,
                    StoMoveID = storeMove.StoMoveID
                };

                using (var tr = db.GetTransaction())
                {
                    //do cancellation
                    if (!new StoreCore.Save_Basic.Save().CancelMove(db, _userID, procModel.StoMoveID, out string msg))
                        return new ReceiveCancel() { ErrorText = msg };
                    //history
                    db.Insert(storeMoveHistory);
                    //commit
                    tr.Complete();
                }

                // log
                base.Info($"ReceiveCancelProc; StoMoveID: {procModel.StoMoveID}");

                return new ReceiveCancel() { Success = true };
            }
        }
    }
}

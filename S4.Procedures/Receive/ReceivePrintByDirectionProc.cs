﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using S4.Core;
using S4.Entities;
using S4.ProcedureModels.Receive;
using S4.Report.Model.Dispatch;
using S4.StoreCore.Interfaces;

namespace S4.Procedures.Receive
{
    public class ReceivePrintByDirectionProc : ProceduresBase<ReceivePrintByDirection>
    {
        public ReceivePrintByDirectionProc(string userID) : base(userID)
        {
        }


        protected override ReceivePrintByDirection DoIt(ReceivePrintByDirection procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new ReceivePrintByDirection() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                if (procModel.ReportID == 0)
                {
                    procModel.ReportID = Singleton<DAL.SettingsSet>.Instance.GetValue_Int32(SettingsDictionary.RECEIVE_REPORT_ID);
                }

                if(procModel.Count == 0)
                {
                    procModel.Count = 1;
                }

                //get direction
                var direction = (new DAL.DirectionDAL()).GetDirectionAllData(procModel.DirectionID);
                if (direction == null)
                    return new ReceivePrintByDirection() { Success = false, ErrorText = $"Direction nebylo nalezeno; DirectionID: {procModel.DirectionID}" };

                var save = Singleton<ServicesFactory>.Instance.GetSave();
                // get destination positionID                
                var receivePositionID = Singleton<DAL.SettingsSet>.Instance.GetValue_Int32(SettingsDictionary.RECEIVE_POSITION_ID);
                var moveResult = save.MoveResult_ByDirection(db, direction.DirectionID, StoreMove.MO_DOCTYPE_RECEIVE, (mi) => mi.PositionID == receivePositionID);

                //make data to report
                var toReport = new List<ReceiveReportModel>();
                var groupNum = 0;
                var rowsDirectionItems = from c in direction.Items
                                         where c.ItemStatus != DirectionItem.DI_STATUS_CANCEL
                                         orderby c.ArticleCode
                                         group c by c.ArtPackID into g
                                         select new { ArtPackID = g.Key, Other = g.ToList() };

                foreach (var directionItem in rowsDirectionItems)
                {

                    var articlePacking = Singleton<DAL.Cache.ArticlePackingCache>.Instance.GetItem(directionItem.ArtPackID);
                    var articleModel = Singleton<DAL.Cache.ArticleModelCache>.Instance.GetItem(articlePacking.ArticleID);
                    //first row
                    var row1 = new ReceiveReportModel();
                    row1.Code = directionItem.Other.FirstOrDefault().ArticleCode;
                    row1.Name = articleModel.ArticleDesc;
                    row1.Quantity = directionItem.Other.Sum(x => x.Quantity);
                    row1.Unit = articlePacking.PackDesc;
                    row1.GroupNum = groupNum;
                    row1.Header = true;
                    toReport.Add(row1);

                    //expirace, šarže 
                    var mr = (from m in moveResult where m.ArtPackID == directionItem.ArtPackID select m).GroupBy(m => new { m.ArtPackID, m.ExpirationDate, m.BatchNum }, (k, i) => new { k, Sum = i.Sum(x => x.Quantity) });
                    foreach (var mrItem in mr.OrderBy(x => x.k.BatchNum))
                    {
                        var row2 = new ReceiveReportModel();
                        row2.Header = false;
                        row2.GroupNum = groupNum;
                        row2.Name = mrItem == null || string.IsNullOrWhiteSpace(mrItem.k.BatchNum) ? "" : mrItem.k.BatchNum.ToString();
                        row2.Expiration = mrItem == null || !mrItem.k.ExpirationDate.HasValue ? "" : mrItem.k.ExpirationDate.Value.Date.ToString("dd.MM.yy");
                        if (!string.IsNullOrWhiteSpace(row2.Expiration) || !string.IsNullOrWhiteSpace(row2.Name))
                        {
                            row2.Quantity = mrItem.Sum;
                            toReport.Add(row2);
                        }
                    }
                    groupNum++;

                }

                //P R I N T
                //get report
                var report = db.SingleOrDefault<Reports>("where reportID = @0", procModel.ReportID);
                if (report == null)
                    return new ReceivePrintByDirection() { Success = false, ErrorText = $"Report nebyl nalezen; reportID: {procModel.ReportID}" };

                //get printer location
                var printerlocation = db.SingleOrDefault<PrinterLocation>("where printerLocationID = @0", procModel.PrinterLocationID);
                if (printerlocation == null)
                    return new ReceivePrintByDirection() { Success = false, ErrorText = $"PrinterLocation nebyl nalezen; printerLocationID: {procModel.PrinterLocationID}" };

                //variables
                var variables = new List<Tuple<string, object, Type>>();
                variables.Add(new Tuple<string, object, Type>("DocInfo", $"{direction.DocNumPrefix}/{direction.DocYear}/{direction.DocNumber}", typeof(string)));
                variables.Add(new Tuple<string, object, Type>("DocDate", direction.DocDate, typeof(DateTime)));
                variables.Add(new Tuple<string, object, Type>("Version", Assembly.GetEntryAssembly().GetCustomAttribute<System.Reflection.AssemblyInformationalVersionAttribute>().InformationalVersion, typeof(string)));

                //do print
                Report.Print print = new Report.Print();
                print.Variables = variables;
                print.ReportDefinition = report.Template;
                //print.DesignMode = true; //pouze pro úpravy šablony
                //print.ReportDefFilePath = @"c:\Projects\store4\Tools\Reporty\VydejovyList.srt"; //pouze pro úpravy šablony
                byte[] fileData = null;
                if (IsTesting)
                {
                    fileData = print.DoPrint2Text(toReport);
                }
                else
                {
                    if (procModel.LocalPrint)
                    {
                        fileData = print.DoPrint2PDF(toReport);
                    }
                    else
                    {
                        for(var i=0; i<procModel.Count; i++)
                        {
                            print.DoPrint(printerlocation.PrinterPath, toReport);
                        }
                    }
                }

                if (print.IsError)
                    return new ReceivePrintByDirection() { Success = false, ErrorText = print.ErrorText };
                else
                    return new ReceivePrintByDirection() { Success = true, LocalPrint = procModel.LocalPrint, FileData = fileData };
            }
        }
    }
}

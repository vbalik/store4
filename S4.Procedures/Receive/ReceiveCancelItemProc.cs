﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using S4.Core;
using S4.Entities;
using S4.ProcedureModels.Receive;

namespace S4.Procedures.Receive
{
    public class ReceiveCancelItemProc : ProceduresBase<ReceiveCancelItem>
    {
        public ReceiveCancelItemProc(string userID) : base(userID)
        {
        }


        protected override ReceiveCancelItem DoIt(ReceiveCancelItem procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new ReceiveCancelItem() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //check
                var storeMove = db.SingleOrDefault<StoreMove>("where stoMoveID = @0", procModel.StoMoveID);
                if (storeMove == null)
                    return new ReceiveCancelItem() { Success = false, ErrorText = $"StoreMove nebylo nalezeno; StMoveID: {procModel.StoMoveID}" };

                if (storeMove.DocStatus != StoreMove.MO_STATUS_NEW_RECEIVE)
                    return new ReceiveCancelItem() { Success = false, ErrorText = $"Doklad nemá očekávaný status MO_STATUS_NEW_RECEIVE; StoMoveID: {procModel.StoMoveID}" };

                var storeMoveItem = db.SingleOrDefault<StoreMoveItem>("where stoMoveID = @0 and docPosition = @1", procModel.StoMoveID, procModel.DocPosition);
                if (storeMoveItem == null)
                    return new ReceiveCancelItem() { Success = false, ErrorText = $"Pozice v dokladu nebyla nalezena; DocPosition: {procModel.DocPosition}" };

                var direction = (new DAL.DirectionDAL()).GetDirectionAllData(storeMove.Parent_directionID);
                if (direction == null)
                    return new ReceiveCancelItem() { Success = false, ErrorText = $"Nebyl nalezen nadřízený doklad; Parent_directionID: {storeMove.Parent_directionID}" };


                var storeMoveHistory = new StoreMoveHistory()
                {
                    DocPosition = procModel.DocPosition,
                    EntryDateTime = DateTime.Now,
                    EntryUserID = _userID,
                    EventCode = StoreMove.MO_STATUS_CANCEL,
                    StoMoveID = storeMove.StoMoveID
                };

                using (var tr = db.GetTransaction())
                {
                    // do cancellation
                    var save = Singleton<ServicesFactory>.Instance.GetSave();
                    if (!save.CancelMoveItem(db, _userID, procModel.StoMoveID, storeMoveItem.StoMoveItemID, out string msg))
                        return new ReceiveCancelItem() { ErrorText = msg };

                    // insert hist
                    db.Insert(storeMoveHistory);

                    // cancel direction item
                    var dirItem = direction.Items.Where(i => i.DocPosition == storeMoveItem.Parent_docPosition).SingleOrDefault();
                    if(dirItem != null)
                    {
                        dirItem.ItemStatus = DirectionItem.DI_STATUS_CANCEL;
                        db.Update(dirItem);
                    }
                    else
                        base.Warn($"ReceiveCancelItem - canceled item without parent dirItem; DirectionID: {direction.DirectionID}; Parent_docPosition: {storeMoveItem.Parent_docPosition}");

                    // commit
                    tr.Complete();
                }

                // log
                base.Info($"ReceiveCancelItemProc; StoMoveID: {procModel.StoMoveID}; StoMoveItemID: {storeMoveItem.StoMoveItemID}");

                return new ReceiveCancelItem() { Success = true };
            }
        }
    }
}

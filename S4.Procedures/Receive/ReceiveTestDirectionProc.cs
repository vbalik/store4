﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using S4.Core;
using S4.Entities;
using S4.Entities.Models;
using S4.ProcedureModels.Receive;

namespace S4.Procedures.Receive
{
    [ChangesDirectionStatus]
    public class ReceiveTestDirectionProc : ProceduresBase<ReceiveTestDirection>
    {
        public ReceiveTestDirectionProc(string userID) : base(userID)
        {
        }


        protected override ReceiveTestDirection DoIt(ReceiveTestDirection procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new ReceiveTestDirection() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // check
                var direction = (new DAL.DirectionDAL()).GetDirectionAllData(procModel.DirectionID);
                if (direction == null)
                    return new ReceiveTestDirection() { Success = false, ErrorText = $"Direction nebylo nalezeno; DirectionID: {procModel.DirectionID}" };

                if (direction.DocStatus != Direction.DR_STATUS_RECEIVE_PROC)
                    return new ReceiveTestDirection() { Success = false, ErrorText = $"Doklad nemá očekávaný status DR_STATUS_RECEIVE_PROC; DirectionID: {procModel.DirectionID}" };

                // set status
                direction.DocStatus = Direction.DR_STATUS_RECEIVE_COMPLET;
                var now = DateTime.Now;

                using (var tr = db.GetTransaction())
                {
                    // update direction
                    db.Update(direction);

                    // update items status
                    direction
                        .Items
                        .Where(i => i.ItemStatus != DirectionItem.DI_STATUS_CANCEL)
                        .ToList()
                        .ForEach(i =>
                        {
                            i.ItemStatus = DirectionItem.DI_STATUS_RECEIVE_COMPLET;
                            db.Update(i);
                        });

                    // commit
                    tr.Complete();
                }

                // log
                base.Info($"ReceiveTestDirectionProc; DirectionID: {procModel.DirectionID};");

                return new ReceiveTestDirection() { Success = true };
            }
        }
    }
}

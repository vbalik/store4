﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using S4.Core;
using S4.Entities;
using S4.ProcedureModels.Receive;
using S4.ProcedureModels.Extensions;
using S4.DAL;

namespace S4.Procedures.Receive
{
    public class ReceiveProcManProc : ProceduresBase<ReceiveProcMan>
    {        
        public ReceiveProcManProc(string userID) : base(userID)
        {
        }


        protected override ReceiveProcMan DoIt(ReceiveProcMan procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new ReceiveProcMan() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            var docNumPrefix = procModel.DocNumPrefix ?? Singleton<DAL.SettingsSet>.Instance.GetValue_String(Entities.SettingsDictionary.RECEIVE_DOC_NUM_PREFIX);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                Direction direction = null;
                StoreMove storeMove = null;
                List<Entities.Helpers.ItemInfo> items = new List<Entities.Helpers.ItemInfo>();

                // 1st step - scan open recive direction (assignet to this user)
                var dirs = (new DBHelpers.Directions()).ScanByAssigment(db, new List<string> { Direction.DR_STATUS_RECEIVE_PROC }, DirectionAssign.JOB_ID_RECEIVE, procModel.WorkerID);
                // filtering by procModel.DocNumPrefix
                dirs = dirs.Where(d => d.DocNumPrefix == docNumPrefix).ToList();
                if (dirs.Any())
                {
                    // return last direction
                    direction = dirs.OrderBy(d => d.DirectionID).Last();

                    // get connected store move
                    var moves = (new DBHelpers.StoreMoves()).ScanByParentDirID(db, new List<string> { StoreMove.MO_STATUS_NEW_RECEIVE }, direction.DirectionID);
                    if (moves.Any())
                    {
                        storeMove = moves.Last();

                        // create list of items
                        var moveItems = db.Fetch<StoreMoveItem>(new NPoco.Sql().Where("stoMoveID = @0", storeMove.StoMoveID));

                        items = moveItems
                            .Where(i => i.ItemValidity != StoreMoveItem.MI_VALIDITY_CANCELED)
                            .Select(i => CreateItemInfo(i))
                            .ToList();
                    }
                    else
                    {
                        // strange condition
                        base.Warn($"ReceiveProcMan - found open direction, without open StoreMove; DirectionID: {direction.DirectionID}");
                    }
                }

                // 2nd step - direction && store move not found - lets create new set
                if(direction == null || storeMove == null)
                {                    
                    // create new direction

                    var now = DateTime.Now;

                    // make empty direction
                    direction = new Direction()
                    {
                        EntryUserID = _userID,
                        DocDirection = Direction.DOC_DIRECTION_IN,
                        DocStatus = Direction.DR_STATUS_RECEIVE_PROC,
                        DocNumPrefix = docNumPrefix,
                        DocDate = now,
                        DocYear = now.ToString("yy"),
                        EntryDateTime = now
                    };

                    // assign worker
                    var directionAssign = new DirectionAssign()
                    {
                        JobID = DirectionAssign.JOB_ID_RECEIVE,
                        WorkerID = procModel.WorkerID,
                        EntryUserID = _userID,
                        EntryDateTime = DateTime.Now,
                    };

                    // make storeMove
                    storeMove = new StoreMove()
                    {
                        DocDate = now,
                        DocStatus = StoreMove.MO_STATUS_NEW_RECEIVE,
                        DocNumPrefix = StoreMove.MO_PREFIX_RECEIVE,
                        DocType = StoreMove.MO_DOCTYPE_RECEIVE,
                        EntryUserID = _userID,
                        EntryDateTime = now
                    };

                    // make storemove history
                    var storeMoveHistory = new StoreMoveHistory()
                    {
                        EntryDateTime = now,
                        EntryUserID = _userID,
                        EventCode = StoreMove.MO_STATUS_NEW_RECEIVE
                    };

                    using (var tr = db.GetTransaction())
                    {
                        // insert direction
                        direction.DocNumber = DBHelpers.DocNumbers.GetNextDocNumber(db, direction);
                        db.Insert(direction);

                        // insert assign
                        directionAssign.DirectionID = direction.DirectionID;
                        db.Insert(directionAssign);

                        // insert storemove
                        storeMove.DocNumber = DBHelpers.DocNumbers.GetNextDocNumber(db, storeMove);
                        storeMove.Parent_directionID = direction.DirectionID;
                        db.Insert(storeMove);

                        // insert history
                        storeMoveHistory.StoMoveID = storeMove.StoMoveID;
                        db.Insert(storeMoveHistory);

                        // do save
                        tr.Complete();
                    }
                }

                var partner = new PartnerDAL().Get(direction.PartnerID);

                // log
                base.Info($"ReceiveProcManProc; DirectionID: {direction.DirectionID}; StoMoveID: {storeMove.StoMoveID}");

                return new ReceiveProcMan()
                { 
                    Success = true,
                    DirectionID = direction.DirectionID,
                    DocInfo = direction.DocInfo(),
                    StoMoveID = storeMove.StoMoveID,
                    Partner = partner,
                    PartnerRef = direction.PartnerRef,
                    DocRemark = direction.DocRemark,
                    Items = items
                };
            }
        }


        private Entities.Helpers.ItemInfo CreateItemInfo(StoreMoveItem i)
        {
            var lot = Singleton<DAL.Cache.StoreMoveLotCache>.Instance.GetItem(i.StoMoveLotID);
            var packing = Singleton<DAL.Cache.ArticlePackingCache>.Instance.GetItem(lot.ArtPackID);
            var article = Singleton<DAL.Cache.ArticleModelCache>.Instance.GetItem(packing.ArticleID);

            return new Entities.Helpers.ItemInfo()
            {
                ArtPackID = lot.ArtPackID,
                ArticleID = packing.ArticleID,
                ArticleCode = article.ArticleCode,
                ArticleDesc = article.ArticleDesc,
                BatchNum = lot.BatchNum,
                ExpirationDate = lot.ExpirationDate,
                UseBatch = article.UseBatch,
                UseExpiration = article.UseExpiration,
                Quantity = i.Quantity,
                PackDesc = packing.PackDesc,
                CarrierNum = i.CarrierNum,
                DocPosition = i.DocPosition,
                StoMoveID = i.StoMoveID,
                SpecFeaturesInt = article.SpecFeaturesInt
            };
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using S4.Core;
using S4.DAL;
using S4.DAL.Cache;
using S4.Entities;
using S4.ProcedureModels.Receive;

namespace S4.Procedures.Receive
{
    public class ReceiveSaveItemProc : ProceduresBase<ReceiveSaveItem>
    {
        public ReceiveSaveItemProc(string userID) : base(userID)
        {
        }

        protected override ReceiveSaveItem DoIt(ReceiveSaveItem procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new ReceiveSaveItem() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // check
                var storeMove = db.SingleOrDefault<StoreMove>("where stoMoveID = @0", procModel.StoMoveID);
                if (storeMove == null)
                    return new ReceiveSaveItem() { Success = false, ErrorText = $"StoreMove nebylo nalezeno; StMoveID: {procModel.StoMoveID}" };

                if (storeMove.DocStatus != StoreMove.MO_STATUS_NEW_RECEIVE)
                    return new ReceiveSaveItem() { Success = false, ErrorText = $"Doklad nemá očekávaný status MO_STATUS_NEW_RECEIVE; StoMoveID: {procModel.StoMoveID}" };

                var direction = new DAL.DirectionDAL().GetDirectionAllData(storeMove.Parent_directionID);
                if (direction == null)
                    return new ReceiveSaveItem() { Success = false, ErrorText = $"Nebyl nalezen nadřízený doklad; Parent_directionID: {storeMove.Parent_directionID}" };

                var artPack = Core.Singleton<DAL.Cache.ArticlePackingCache>.Instance.GetItem(procModel.ArtPackID);
                if (artPack == null)
                    return new ReceiveSaveItem() { Success = false, ErrorText = $"Balení nenalezeno; ArtPackID: {procModel.ArtPackID}" };

                var article = Core.Singleton<DAL.Cache.ArticleModelCache>.Instance.GetItem(artPack.ArticleID);
                if (article.ArticleStatus == Article.AR_STATUS_DISABLED)
                    return new ReceiveSaveItem() { Success = false, ErrorText = $"Karta má stav zrušeno; ArtID: {article.ArticleID}" };

                if (procModel.PositionID == null || procModel.PositionID == 0)
                    procModel.PositionID = Singleton<DAL.SettingsSet>.Instance.GetValue_Int32(SettingsDictionary.RECEIVE_POSITION_ID);

                var position = Core.Singleton<DAL.Cache.PositionCache>.Instance.GetItem((int)procModel.PositionID);

                if (position == null)
                    return new ReceiveSaveItem() { Success = false, ErrorText = $"Pozice nenalezena; PositionID: {procModel.PositionID}" };

                // set default values
                var now = DateTime.Now;
                var proxy = new Core.Data.EmptyDataProxy();
                proxy.ToSave(string.IsNullOrEmpty(procModel.CarrierNum) ? null : procModel.CarrierNum, StoreMoveItem.EMPTY_CARRIERNUM, value => procModel.CarrierNum = value);
                proxy.ToSave(string.IsNullOrEmpty(procModel.BatchNum) ? null : procModel.BatchNum, StoreMoveItem.EMPTY_BATCHNUM, value => procModel.BatchNum = value);
                proxy.ToSave(procModel.ExpirationDate, StoreMoveItem.EMPTY_EXPIRATION, value => procModel.ExpirationDate = value);

                // test item duplicity and update if was item found
                // return
                var storeMoveItems = db.Fetch<StoreMoveItem>("where stoMoveID = @0", procModel.StoMoveID);
                var maxDocPosition = storeMoveItems.Any() ? storeMoveItems.Max(_ => _.DocPosition) : 0;

                foreach (var item in storeMoveItems.Where(_ => _.ItemValidity == StoreMoveItem.MI_VALIDITY_VALID).ToList())
                {
                    var lot = Singleton<StoreMoveLotCache>.Instance.GetItem(item.StoMoveLotID);

                    if (lot.ArtPackID == procModel.ArtPackID
                        && item.CarrierNum == (procModel.CarrierNum == StoreMoveItem.EMPTY_CARRIERNUM ? null : procModel.CarrierNum)    // create null due terminal incoming data inconsistency
                        && lot.BatchNum == (procModel.BatchNum == StoreMoveItem.EMPTY_BATCHNUM ? null : procModel.BatchNum)             // create null due terminal incoming data inconsistency
                        && lot.ExpirationDate == procModel.ExpirationDate)
                    {
                        var directionItem = direction.Items.FirstOrDefault(_ => _.ArtPackID == procModel.ArtPackID && _.ItemStatus == DirectionItem.DI_STATUS_RECEIVE_IN_PROC && _.DocPosition == item.Parent_docPosition);
                        if (directionItem == null)
                        {
                            base.Error($"DirectionItem is null. StoMoveID {item.StoMoveID}, StoMoveItemID {item.StoMoveItemID}, Lot {item.StoMoveLotID}", new ArgumentNullException());
                            return new ReceiveSaveItem() { Success = false, StoMoveItemID = item.StoMoveItemID, RepeatedItem = true };
                        }

                        using (var tr = db.GetTransaction())
                        {
                            directionItem.Quantity += procModel.Quantity;

                            // create history
                            var updateStoreMoveHistory = new StoreMoveHistory
                            {
                                DocPosition = item.DocPosition,
                                EntryDateTime = item.EntryDateTime,
                                EntryUserID = item.EntryUserID,
                                EventCode = StoreMove.MO_EVENT_ITEM_CHANGED,
                                StoMoveID = storeMove.StoMoveID
                            };

                            db.Insert(updateStoreMoveHistory);

                            if (!new ServicesFactory().GetSave().ChangeMoveItem2(db, _userID, item, lot.ArtPackID, directionItem.Quantity, lot.BatchNum, lot.ExpirationDate, item.CarrierNum, out var msg))
                                throw new Exception($"Update quantity for storeMoveItem {item.StoMoveItemID} failed");

                            db.Update(directionItem);

                            tr.Complete();

                            return new ReceiveSaveItem() { Success = true, StoMoveItemID = item.StoMoveItemID, RepeatedItem = true };
                        }
                    }

                    maxDocPosition = Math.Max(maxDocPosition, item.DocPosition);
                }

                // or continue and create item
                var storeMoveItem = new StoreMoveItem()
                {
                    CarrierNum = procModel.CarrierNum,
                    DocPosition = maxDocPosition + 1,
                    EntryDateTime = now,
                    EntryUserID = _userID,
                    ItemDirection = StoreMoveItem.MI_DIRECTION_IN,
                    PositionID = (int)procModel.PositionID,
                    ItemValidity = StoreMoveItem.MI_VALIDITY_VALID,
                    Quantity = procModel.Quantity,
                    StoMoveID = storeMove.StoMoveID,
                };

                // create history
                var storeMoveHistory = new StoreMoveHistory()
                {
                    DocPosition = storeMoveItem.DocPosition,
                    EntryDateTime = storeMoveItem.EntryDateTime,
                    EntryUserID = storeMoveItem.EntryUserID,
                    EventCode = StoreMove.MO_EVENT_ITEM_CHANGED,
                    StoMoveID = storeMove.StoMoveID
                };

                // add direction item
                var dirItem = new DirectionItem()
                {
                    ArtPackID = procModel.ArtPackID,
                    ArticleCode = article.ArticleCode,
                    Quantity = procModel.Quantity,

                    DirectionID = direction.DirectionID,
                    DocPosition = direction.Items.Select(i => i.DocPosition).DefaultIfEmpty(0).Max() + 1,
                    ItemStatus = DirectionItem.DI_STATUS_RECEIVE_IN_PROC,
                    EntryUserID = _userID,
                    EntryDateTime = now
                };

                // XtraData
                if (article.SpecFeatures.HasFlag(Article.SpecFeaturesEnum.Cooled))
                    direction.XtraData[Direction.XTRA_ARTICLE_COOLED, dirItem.DocPosition] = true;
                if (article.SpecFeatures.HasFlag(Article.SpecFeaturesEnum.Cytostatics))
                    direction.XtraData[Direction.XTRA_ARTICLE_CYTOSTATICS, dirItem.DocPosition] = true;
                if (article.SpecFeatures.HasFlag(Article.SpecFeaturesEnum.Medicines))
                    direction.XtraData[Direction.XTRA_ARTICLE_MEDICINES, dirItem.DocPosition] = true;
                if (article.SpecFeatures.HasFlag(Article.SpecFeaturesEnum.CheckOnReceive))
                    direction.XtraData[Direction.XTRA_ARTICLE_CHECK_ON_RECEIVE, dirItem.DocPosition] = true;

                using (var tr = db.GetTransaction())
                {
                    // save direction item
                    db.Insert(dirItem);

                    // create or get lot
                    var storeMoveLot = new StoreCore.Save_Basic.Save()
                        .FindOrCreateLot(db,
                        new StoreCore.Interfaces.SaveItem()
                        {
                            ArtPackID = procModel.ArtPackID,
                            BatchNum = procModel.BatchNum,
                            ExpirationDate = procModel.ExpirationDate
                        },
                        procModel.UDICode);

                    // insert storeMoveItem
                    storeMoveItem.StoMoveLotID = storeMoveLot.StoMoveLotID;
                    storeMoveItem.Parent_docPosition = dirItem.DocPosition;
                    db.Insert(storeMoveItem);

                    // insert history
                    db.Insert(storeMoveHistory);

                    // invalidate store status
                    var save = Singleton<ServicesFactory>.Instance.GetSave();
                    save.InvalidateStorageLot(db, _userID, storeMoveLot.StoMoveLotID);

                    //save extraData
                    direction.XtraData.Save(db, direction.DirectionID);

                    // do save
                    tr.Complete();
                }

                // log
                base.Info($"ReceiveSaveItemProc; StoMoveItemID: {storeMoveItem.StoMoveItemID}");

                return new ReceiveSaveItem() { Success = true, StoMoveItemID = storeMoveItem.StoMoveItemID };
            }
        }
    }
}

﻿using S4.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace S4.Procedures
{
    public static class BackgroundTaskQueueExtension
    {
        public static string QueueProcedure<T>(this IBackgroundTaskQueue queue, ProceduresBase<T> procedure, T model) where T : S4.ProcedureModels.ProceduresModel
        {
            var taskID = queue.QueueBackgroundWorkItem(
                async token => await Task.Run(() => procedure.DoProcedure(model)),
                async token => await Task.Run(() => procedure.InCaseOfError(model))
                );
            return taskID;
        }
    }
}

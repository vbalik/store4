﻿using combit.ListLabel24;
using iText.StyledXmlParser.Jsoup.Parser;
using NPoco;
using Org.BouncyCastle.Asn1.Tsp;
using S4.Core;
using S4.Core.ADR;
using S4.DAL;
using S4.Entities;
using S4.Entities.Helpers.Xml.Article;
using S4.Entities.Helpers.Xml.Dispatch;
using S4.Entities.Models;
using S4.ProcedureModels.Booking;
using S4.ProcedureModels.Load;
using S4.Procedures.Booking;
using S4.Procedures.DBHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static iText.StyledXmlParser.Jsoup.Select.Evaluator;

namespace S4.Procedures.Load.Helper
{
    public class DispatchLoadHelper
    {
        private string _userID;
        private List<RequestBooking> _requestBookings = new List<RequestBooking>();
        public DispatchDirLoad DispatchLoad(Database db, DispatchS3 dispatchS3, string docInfo, string xml, string userID, int invoiceID = 0)
        {
            var messageText = string.Empty;
            _userID = userID;
            var result = new DispatchDirLoad();
            var direction = new Direction();
            var directionItemList = new List<DirectionItem>();
            var docNumPrefix = dispatchS3.DocPrefix;
            var adrUpdateArticles = new List<Article>();
            var usingArticles = new List<Tuple<Article, int>>();
            _requestBookings = new List<RequestBooking>();

            //check prefix
            var prefix = Singleton<DAL.Cache.PrefixCache>.Instance.GetItem(Entities.Direction.DOC_CLASS, docNumPrefix);
            if (prefix == null)
            {
                var msg = $"{docInfo} Dokument prefix {docNumPrefix} není nadefinován!";
                db.Save(SetInternMessage(msg, xml));
                return new DispatchDirLoad() { Result = DispatchDirLoad.ResultEnum.Error, Success = false, ErrorText = msg };
            }

            direction.DocNumPrefix = docNumPrefix;
            direction.DocNumber = dispatchS3.DocNumber;
            direction.DocDate = dispatchS3.DocDate;
            direction.DocYear = dispatchS3.DocDate.ToString("yy");
            direction.PartnerRef = dispatchS3.DocReference;
            direction.DocRemark = dispatchS3.DocRemark;

            //set status 
            if (prefix.DoNotProcess)
            {
                direction.DocStatus = Direction.DR_STATUS_NOT_PROCESSED;
                direction.DoNotProcess = true;
            }
            else
            {
                direction.DoNotProcess = false;
                direction.DocStatus = dispatchS3.Items.Count > 0 ? Direction.DR_STATUS_LOAD : Direction.DR_STATUS_DISP_CHECK_DONE;
            }

            direction.EntryDateTime = DateTime.Now;
            direction.EntryUserID = _userID;

            // Partner
            var partner = SetPartner(dispatchS3.Customer, db);
            direction.PartnerID = partner.PartnerID;

            //Address
            var address = db.FirstOrDefault<PartnerAddress>("where source_id = @0", dispatchS3.DeliveryAddress.AddressID);
            if (address == null)
            {
                address = new PartnerAddress();
            }

            address.AddrCity = dispatchS3.DeliveryAddress.City;
            address.AddrLine_1 = dispatchS3.DeliveryAddress.Adress1;
            address.AddrLine_2 = dispatchS3.DeliveryAddress.Adress2;
            address.AddrName = dispatchS3.DeliveryAddress.Name;
            address.AddrPhone = dispatchS3.DeliveryAddress.Phone;
            address.AddrRemark = dispatchS3.DeliveryAddress.Remark;
            address.DeliveryInst = dispatchS3.DeliveryAddress.Deliveryinstructions;
            address.Source_id = dispatchS3.DeliveryAddress.AddressID;
            direction.PartAddrID = address.PartAddrID;

            if (!string.IsNullOrWhiteSpace(dispatchS3.DeliveryAddress.Remark))
                direction.DocRemark = dispatchS3.DeliveryAddress.Remark;

            //Items
            int itemCounter = 1;
            var errorSB = new StringBuilder();
            var isError = false;

            if (direction.DocStatus != Direction.DR_STATUS_NOT_PROCESSED)
            {
                foreach (var item in dispatchS3.Items)
                {
                    // find current packing
                    var sql = new Sql();
                    sql.Select("p.artPackID");
                    sql.From("s4_articleList_packings p");
                    sql.LeftJoin("s4_articleList a on a.articleID = p.articleID");
                    sql.Where("p.source_id = @0 AND a.articleStatus != 'AR_DISABLED' AND p.packStatus != 'AP_DISABLED'", item.ArticlePackingID);

                    var artPackIDs = db.Fetch<int>(sql);

                    if (artPackIDs.Count > 1)
                    {
                        var msg = $"Balení karty source_id {item.ArticlePackingID} má více než jednu kartu! ";
                        isError = true;
                        errorSB.Append(msg);
                    }

                    if (artPackIDs.Count == 0)
                    {
                        //check article is disable
                        sql = new Sql();
                        sql.Select("a.articleCode");
                        sql.From("s4_articleList_packings p");
                        sql.LeftJoin("s4_articleList a on a.articleID = p.articleID");
                        sql.Where("p.source_id = @0 AND a.articleStatus = 'AP_DISABLED'", item.ArticlePackingID);
                        var articleCode = db.FirstOrDefault<string>(sql);

                        var msg = "";
                        if (!string.IsNullOrWhiteSpace(articleCode))
                            msg = $"Kód karty {articleCode} má nastaven stav na ZRUŠENO! ";
                        else
                            msg = $"Balení karty source_id {item.ArticlePackingID} neexistuje ";

                        isError = true;
                        errorSB.Append(msg);
                    }

                    if (artPackIDs.Count > 1 || artPackIDs.Count == 0)
                        continue;

                    var artPackID = artPackIDs[0];
                    var packing = db.FirstOrDefault<ArticlePacking>("where artPackID = @0", artPackID);

                    if (!string.IsNullOrWhiteSpace(item.RequiredBatchNumber))
                    {
                        var stoMoveLotID = GetLot(artPackID, item.RequiredBatchNumber, db);

                        if (stoMoveLotID == 0)
                        {
                            var msg = $"Nenalezena šarže {item.RequiredBatchNumber}, rezervace nemůže být provedena!";
                            isError = true;
                            errorSB.Append(msg);
                        }
                        else
                        {
                            //add Request Booking
                            _requestBookings.Add(
                                new RequestBooking
                                {
                                    Quantity = item.Quantity,
                                    StoMoveLotID = stoMoveLotID
                                });
                        }
                    }

                    //find Article
                    var article = db.SingleById<Article>(packing.ArticleID);
                    var articlePackings = db.Fetch<ArticlePacking>("where articleID = @0", packing.ArticleID);

                    // reset to movable packing
                    decimal relation = 1;
                    if (!packing.MovablePack)
                    {
                        relation = packing.PackRelation;
                        packing = (from a in articlePackings where a.MovablePack == true select a).FirstOrDefault();
                    }

                    // create new item
                    DirectionItem newDirectionItem = new DirectionItem();
                    newDirectionItem.DocPosition = itemCounter;
                    newDirectionItem.ArtPackID = packing.ArtPackID;
                    newDirectionItem.ArticleCode = article.ArticleCode;
                    newDirectionItem.UnitDesc = packing.PackDesc;

                    //from XML
                    newDirectionItem.Quantity = item.Quantity * relation;
                    newDirectionItem.PartnerDepart = item.Department;

                    //for ADR
                    usingArticles.Add(new Tuple<Article, int>(article, item.Quantity));

                    // <direct_source_section>
                    if (item.DirectSourceSection != null)
                    {
                        foreach (var directSourceSection in item.DirectSourceSection)
                        {
                            if (string.IsNullOrWhiteSpace(directSourceSection.Value))
                            {
                                var msg = "direct_source_section hodnota *value* je prázdná ";
                                isError = true;
                                errorSB.Append(msg);
                                continue;
                            }

                            if (directSourceSection.Number < 1 || directSourceSection.Number > 9)
                            {
                                var msg = $"direct_source_section hodnota *number* musí mít hodnotu 1-9. Hodnota je {directSourceSection.Number} ";
                                isError = true;
                                errorSB.Append(msg);
                                continue;
                            }
                        }

                        var val = (from d in item.DirectSourceSection orderby d.Number select d.Value);
                        if (val != null)
                            direction.XtraData[Direction.XTRA_DISPATCH_DIRECT_SECTION_LIST, newDirectionItem.DocPosition] = string.Join("|", val.ToArray());
                    }

                    // <alter_source_section>
                    if (item.AlterSourceSection != null)
                        direction.XtraData[Direction.XTRA_DISPATCH_ALTER_SECTION, newDirectionItem.DocPosition] = item.AlterSourceSection;

                    // extra data
                    if (item.Extradata != null)
                    {
                        if (GetBoolValue(item.Extradata.ArticleCooled))
                            direction.XtraData[Direction.XTRA_ARTICLE_COOLED, newDirectionItem.DocPosition] = true;
                        if (GetBoolValue(item.Extradata.ArticleMedicines))
                            direction.XtraData[Direction.XTRA_ARTICLE_MEDICINES, newDirectionItem.DocPosition] = true;

                        //check if manufacturer has special sign associated
                        var manuf = Singleton<DAL.Cache.ManufactCache>.Instance.GetItem(article.ManufID);
                        if (!string.IsNullOrWhiteSpace(manuf.ManufSign))
                            direction.XtraData[Direction.XTRA_MANUF_ICON, newDirectionItem.DocPosition] = manuf.ManufSign;

                        // RiskClass
                        if (!string.IsNullOrWhiteSpace(item.Extradata.RiskClass))
                            direction.XtraData[Direction.XTRA_DELIVERY_RISK_CLASS, newDirectionItem.DocPosition] = item.Extradata.RiskClass;

                        // Udidi
                        if (!string.IsNullOrWhiteSpace(item.Extradata.Udidi))
                            direction.XtraData[Direction.XTRA_DELIVERY_UDIDI, newDirectionItem.DocPosition] = item.Extradata.Udidi;

                        // update ADR xtra data article
                        if (item.Extradata.Adr != null)
                        {
                            article.XtraData.Load(db, new Sql().Where("articleID = @0", article.ArticleID));
                            LoadXtraDataHelper.LoadAdrXtraData(item.Extradata.Adr, article, article.ArticleCode);
                            adrUpdateArticles.Add(article);
                        }
                    }

                    // add item
                    newDirectionItem.ItemStatus = DirectionItem.DI_STATUS_LOAD;
                    newDirectionItem.EntryDateTime = DateTime.Now;
                    newDirectionItem.EntryUserID = _userID;
                    directionItemList.Add(newDirectionItem);
                    itemCounter++;

                } //end foreach
            }

            if (isError)
            {
                var error = $"{docInfo} Chyba: {errorSB.ToString()}";
                db.Save(SetInternMessage(error, xml));
                return new DispatchDirLoad() { Result = DispatchDirLoad.ResultEnum.Error, Success = false, ErrorText = errorSB.ToString() };
            }

            // set XTRA_DISP_CHECK_METHOD value from prefix 
            var dispatchCheckMethod = prefix.PrefixSettings.DispatchCheckMethod;
            if (dispatchCheckMethod != 255)
                direction.XtraData[Direction.XTRA_DISP_CHECK_METHOD] = dispatchCheckMethod;

            //set Invoice Number
            if (!string.IsNullOrWhiteSpace(dispatchS3.InvoiceNumber))
                direction.XtraData[Direction.XTRA_INVOICE_NUMBER] = dispatchS3.InvoiceNumber;

            //set Payment Reference
            if (!string.IsNullOrWhiteSpace(dispatchS3.PaymentReference))
                direction.XtraData[Direction.XTRA_PAYMENT_REFERENCE] = dispatchS3.PaymentReference;

            //set Payment Type
            if (!string.IsNullOrWhiteSpace(dispatchS3.PaymentType))
                direction.XtraData[Direction.XTRA_PAYMENT_TYPE] = dispatchS3.PaymentType;

            //set Transportation Type
            if (!string.IsNullOrWhiteSpace(dispatchS3.TransportationType))
                direction.XtraData[Direction.XTRA_TRANSPORTATION_TYPE] = dispatchS3.TransportationType;

            //set CashOnDelivery
            if (dispatchS3.CashOnDelivery != 0)
                direction.XtraData[Direction.XTRA_CASH_ON_DELIVERY] = dispatchS3.CashOnDelivery;

            //set delivery country
            if (!string.IsNullOrWhiteSpace(dispatchS3.Country))
                direction.XtraData[Direction.XTRA_DELIVERY_COUNTRY] = dispatchS3.Country;

            //set delivery currency
            if (!string.IsNullOrWhiteSpace(dispatchS3.Currency))
                direction.XtraData[Direction.XTRA_DELIVERY_CURRENCY] = dispatchS3.Currency;

            //set parcel shop code
            if (!string.IsNullOrWhiteSpace(dispatchS3.ParcelShopCode))
                direction.XtraData[Direction.XTRA_DELIVERY_PARCEL_SHOP] = dispatchS3.ParcelShopCode;

            //set shipmentinformation
            if (!string.IsNullOrWhiteSpace(dispatchS3.Shipmentinformation))
                direction.XtraData[Direction.XTRA_DELIVERY_SHIPMENTINFO] = dispatchS3.Shipmentinformation;

            //set abra doc barcode id
            if (!string.IsNullOrWhiteSpace(dispatchS3.DocBarcode))
                direction.XtraData[Direction.XTRA_ABRA_DOC_ID] = dispatchS3.DocBarcode;

            //set extnumber
            if (!string.IsNullOrWhiteSpace(dispatchS3.ExtNumber))
                direction.XtraData[Direction.XTRA_EXTNUMBER] = dispatchS3.ExtNumber;

            //set receiving person for delivery
            if (!string.IsNullOrWhiteSpace(dispatchS3.DeliveryAddress.ReceivingPerson))
                direction.XtraData[Direction.XTRA_DELIVERY_RECEIVING_PERSON] = dispatchS3.DeliveryAddress.ReceivingPerson;

            //set mandatory direction print
            if (!string.IsNullOrWhiteSpace(dispatchS3.MandatoryDirectionPrint))
                direction.XtraData[Direction.XTRA_MANDATORY_DIRECTION_PRINT] = bool.Parse(dispatchS3.MandatoryDirectionPrint);

            //poslat souhrnně
            if (dispatchS3.Customer != null && dispatchS3.Customer.Extradata != null && !string.IsNullOrWhiteSpace(dispatchS3.Customer.Extradata.PoslatDisketuSouhrnne))
            {
                if (bool.TryParse(dispatchS3.Customer.Extradata.PoslatDisketuSouhrnne, out bool poslatSouhrnne))
                    direction.XtraData[Direction.XTRA_SEND_DISKET_SUMMARY] = poslatSouhrnne;
            }

            //check  "Platba dobírkou" nebo "Platba dobírkou kartou" a dobírka = 0
            var checkPaymantTypeMsg = "";
            if (!Check.CheckPaymantType(dispatchS3.CashOnDelivery, dispatchS3.PaymentType, out checkPaymantTypeMsg))
            {
                var msg = $"{docInfo} {checkPaymantTypeMsg}";
                db.Save(SetInternMessage(msg, xml));
                return new DispatchDirLoad() { Result = DispatchDirLoad.ResultEnum.Error, Success = false, ErrorText = msg };
            }

            //try create or update Invoice start
            var saveInvoice = false;
            Invoice invoice = null;
            if (dispatchS3.InvoiceNumber != null || invoiceID > 0)
            {
                var posFV = invoiceID == 0 ? dispatchS3.InvoiceNumber.IndexOf("-") : 0;
                var posYear = invoiceID == 0 ? dispatchS3.InvoiceNumber.IndexOf("/") : 0;

                if ((posFV > -1 && posYear > -1) || invoiceID > 0)
                {
                    if (invoiceID > 0)
                    {
                        //The invoice has been created in InvoiceLoadProc
                        invoice = db.FirstOrDefault<Invoice>("where invoiceID = @0", invoiceID);
                    }

                    var docPrefix = invoice == null ? dispatchS3.InvoiceNumber.Substring(0, posFV) : invoice.DocPrefix;
                    var docNumber = invoice == null ? dispatchS3.InvoiceNumber.Substring(posFV + 1, posYear - 3) : invoice.DocNumber.ToString();

                    if (int.TryParse(docNumber, out int docNumberInt) || invoiceID > 0)
                    {
                        //check if exists
                        if (invoiceID == 0)
                        {
                            invoice = db.FirstOrDefault<Invoice>("where docNumber = @0 AND docPrefix = @1 AND docYear = @2", docNumber, docPrefix, dispatchS3.DocDate.ToString("yy"));

                            if (invoice == null)
                            {
                                invoice = new Invoice()
                                {
                                    DocNumber = docNumberInt,
                                    DocPrefix = docPrefix,
                                    DocYear = dispatchS3.DocDate.ToString("yy"),
                                    LastDateTime = DateTime.Now,
                                    EntryDateTime = DateTime.Now,
                                    DocStatus = Invoice.INV_STATUS_LOAD
                                };
                            }
                            else
                            {
                                invoice.DocStatus = Invoice.INV_STATUS_RELOAD;
                                invoice.LastDateTime = DateTime.Now;
                                invoice.XtraData.Load(db, new NPoco.Sql().Where("invoiceID = @0", invoice.InvoiceID));
                            }
                        }
                        //xtra
                        invoice.XtraData.Load(db, new NPoco.Sql().Where("invoiceID = @0", invoice.InvoiceID));
                        invoice.XtraData[Invoice.XTRA_TRANSPORTATION_TYPE] = dispatchS3.TransportationType;
                        invoice.XtraData[Invoice.XTRA_PAYMENT_TYPE] = dispatchS3.PaymentType;
                        invoice.XtraData[Invoice.XTRA_PAYMENT_REFERENCE] = dispatchS3.PaymentReference;
                        invoice.XtraData[Invoice.XTRA_CASH_ON_DELIVERY] = dispatchS3.CashOnDelivery;
                        invoice.XtraData[Invoice.XTRA_DELIVERY_COUNTRY] = dispatchS3.Country;
                        invoice.XtraData[Invoice.XTRA_DELIVERY_CURRENCY] = dispatchS3.Currency;
                        invoice.XtraData[Invoice.XTRA_DOC_DATE] = dispatchS3.DocDate;

                        saveInvoice = true;
                    }
                }
            }
            //try create or update Invoice end

            // check prefix & number
            var dir = db.FirstOrDefault<Direction>("where docNumPrefix = @0 AND docYear = @1 AND docNumber = @2", direction.DocNumPrefix, direction.DocYear, direction.DocNumber);
            if (dir != null)
            {
                //update extraData
                var dal = new DirectionDAL();
                var directionUpdate = dal.GetDirectionAllData(dir.DirectionID);

                if (directionUpdate == null)
                {
                    result.Result = DispatchDirLoad.ResultEnum.NoItems;
                    result.ErrorText = $"Direction docNumPrefix: {direction.DocNumPrefix} docYear: {direction.DocYear} docNumber: {direction.DocNumber} not found!";
                    return result;
                }

                var change = false;
                var xtraChange = "";
                //set extnumber
                if (!string.IsNullOrWhiteSpace(dispatchS3.ExtNumber) && (!directionUpdate.XtraData.IsNull(Direction.XTRA_EXTNUMBER) && !directionUpdate.XtraData[Direction.XTRA_EXTNUMBER].Equals(dispatchS3.ExtNumber)) || ((directionUpdate.XtraData.IsNull(Direction.XTRA_EXTNUMBER) && !string.IsNullOrWhiteSpace(dispatchS3.ExtNumber))))
                {
                    change = true;
                    var old = directionUpdate.XtraData.IsNull(Direction.XTRA_EXTNUMBER) ? "empty" : directionUpdate.XtraData[Direction.XTRA_EXTNUMBER];
                    xtraChange += $"Extnumber: {old} --> {dispatchS3.ExtNumber}|";
                    directionUpdate.XtraData[Direction.XTRA_EXTNUMBER] = dispatchS3.ExtNumber;
                }

                //set Abra doc barcode id
                if (!string.IsNullOrWhiteSpace(dispatchS3.DocBarcode) && (!directionUpdate.XtraData.IsNull(Direction.XTRA_ABRA_DOC_ID) && !directionUpdate.XtraData[Direction.XTRA_ABRA_DOC_ID].Equals(dispatchS3.DocBarcode)) || ((directionUpdate.XtraData.IsNull(Direction.XTRA_ABRA_DOC_ID) && !string.IsNullOrWhiteSpace(dispatchS3.DocBarcode))))
                {
                    change = true;
                    var old = directionUpdate.XtraData.IsNull(Direction.XTRA_ABRA_DOC_ID) ? "empty" : directionUpdate.XtraData[Direction.XTRA_ABRA_DOC_ID];
                    xtraChange += $"DocBarcode: {old} --> {dispatchS3.DocBarcode}|";
                    directionUpdate.XtraData[Direction.XTRA_ABRA_DOC_ID] = dispatchS3.DocBarcode;
                }

                //set Invoice Number 
                if (!string.IsNullOrWhiteSpace(dispatchS3.InvoiceNumber) && (!directionUpdate.XtraData.IsNull(Direction.XTRA_INVOICE_NUMBER) && !directionUpdate.XtraData[Direction.XTRA_INVOICE_NUMBER].Equals(dispatchS3.InvoiceNumber)) || ((directionUpdate.XtraData.IsNull(Direction.XTRA_INVOICE_NUMBER) && !string.IsNullOrWhiteSpace(dispatchS3.InvoiceNumber))))
                {
                    change = true;
                    var old = directionUpdate.XtraData.IsNull(Direction.XTRA_INVOICE_NUMBER) ? "empty" : directionUpdate.XtraData[Direction.XTRA_INVOICE_NUMBER];
                    xtraChange += $"InvoiceNumber: {old} --> {dispatchS3.InvoiceNumber}|";
                    directionUpdate.XtraData[Direction.XTRA_INVOICE_NUMBER] = dispatchS3.InvoiceNumber;
                }

                //set Payment Reference 
                if (!string.IsNullOrWhiteSpace(dispatchS3.PaymentReference) && (!directionUpdate.XtraData.IsNull(Direction.XTRA_PAYMENT_REFERENCE) && !directionUpdate.XtraData[Direction.XTRA_PAYMENT_REFERENCE].Equals(dispatchS3.PaymentReference)) || ((directionUpdate.XtraData.IsNull(Direction.XTRA_PAYMENT_REFERENCE) && !string.IsNullOrWhiteSpace(dispatchS3.PaymentReference))))
                {
                    change = true;
                    var old = directionUpdate.XtraData.IsNull(Direction.XTRA_PAYMENT_REFERENCE) ? "empty" : directionUpdate.XtraData[Direction.XTRA_PAYMENT_REFERENCE];
                    xtraChange += $"PaymentReference: {old} --> {dispatchS3.PaymentReference}|";
                    directionUpdate.XtraData[Direction.XTRA_PAYMENT_REFERENCE] = dispatchS3.PaymentReference;
                }

                //set Payment Type
                if (!string.IsNullOrWhiteSpace(dispatchS3.PaymentType) && (!directionUpdate.XtraData.IsNull(Direction.XTRA_PAYMENT_TYPE) && !directionUpdate.XtraData[Direction.XTRA_PAYMENT_TYPE].Equals(dispatchS3.PaymentType)) || (directionUpdate.XtraData.IsNull(Direction.XTRA_PAYMENT_TYPE) && !string.IsNullOrWhiteSpace(dispatchS3.PaymentType)))
                {
                    change = true;
                    var old = directionUpdate.XtraData.IsNull(Direction.XTRA_PAYMENT_TYPE) ? "empty" : directionUpdate.XtraData[Direction.XTRA_PAYMENT_TYPE];
                    xtraChange += $"PaymentType: {old} --> {dispatchS3.PaymentType}|";
                    directionUpdate.XtraData[Direction.XTRA_PAYMENT_TYPE] = dispatchS3.PaymentType;
                }

                //set Transportation Type
                if (!string.IsNullOrWhiteSpace(dispatchS3.TransportationType) && (!directionUpdate.XtraData.IsNull(Direction.XTRA_TRANSPORTATION_TYPE) && !directionUpdate.XtraData[Direction.XTRA_TRANSPORTATION_TYPE].Equals(dispatchS3.TransportationType)) || (directionUpdate.XtraData.IsNull(Direction.XTRA_TRANSPORTATION_TYPE) && !string.IsNullOrWhiteSpace(dispatchS3.TransportationType)))
                {
                    change = true;
                    var old = directionUpdate.XtraData.IsNull(Direction.XTRA_TRANSPORTATION_TYPE) ? "empty" : directionUpdate.XtraData[Direction.XTRA_TRANSPORTATION_TYPE];
                    xtraChange += $"TransportationType: {old} --> {dispatchS3.TransportationType}|";
                    directionUpdate.XtraData[Direction.XTRA_TRANSPORTATION_TYPE] = dispatchS3.TransportationType;
                }

                //set CashOnDelivery
                var existingCashOnDelivery = directionUpdate.XtraData.IsNull(Direction.XTRA_CASH_ON_DELIVERY) ? 0 : (decimal)directionUpdate.XtraData[Direction.XTRA_CASH_ON_DELIVERY];
                if (dispatchS3.CashOnDelivery != existingCashOnDelivery)
                {
                    change = true;
                    var old = directionUpdate.XtraData.IsNull(Direction.XTRA_CASH_ON_DELIVERY) ? "empty" : directionUpdate.XtraData[Direction.XTRA_CASH_ON_DELIVERY];
                    xtraChange += $"CashOnDelivery: {old} --> {dispatchS3.CashOnDelivery}|";
                    directionUpdate.XtraData[Direction.XTRA_CASH_ON_DELIVERY] = dispatchS3.CashOnDelivery;
                }

                //set delivery country
                if (!string.IsNullOrWhiteSpace(dispatchS3.Country) && (!directionUpdate.XtraData.IsNull(Direction.XTRA_DELIVERY_COUNTRY) && !directionUpdate.XtraData[Direction.XTRA_DELIVERY_COUNTRY].Equals(dispatchS3.Country)) || (directionUpdate.XtraData.IsNull(Direction.XTRA_DELIVERY_COUNTRY) && !string.IsNullOrWhiteSpace(dispatchS3.Country)))
                {
                    change = true;
                    var old = directionUpdate.XtraData.IsNull(Direction.XTRA_DELIVERY_COUNTRY) ? "empty" : directionUpdate.XtraData[Direction.XTRA_DELIVERY_COUNTRY];
                    xtraChange += $"Country: {old} --> {dispatchS3.Country}|";
                    directionUpdate.XtraData[Direction.XTRA_DELIVERY_COUNTRY] = dispatchS3.Country;
                }

                //set delivery currency
                if (!string.IsNullOrWhiteSpace(dispatchS3.Currency) && (!directionUpdate.XtraData.IsNull(Direction.XTRA_DELIVERY_CURRENCY) && !directionUpdate.XtraData[Direction.XTRA_DELIVERY_CURRENCY].Equals(dispatchS3.Currency)) || (directionUpdate.XtraData.IsNull(Direction.XTRA_DELIVERY_CURRENCY) && !string.IsNullOrWhiteSpace(dispatchS3.Currency)))
                {
                    change = true;
                    var old = directionUpdate.XtraData.IsNull(Direction.XTRA_DELIVERY_CURRENCY) ? "empty" : directionUpdate.XtraData[Direction.XTRA_DELIVERY_CURRENCY];
                    xtraChange += $"Currency: {old} --> {dispatchS3.Currency}|";
                    directionUpdate.XtraData[Direction.XTRA_DELIVERY_CURRENCY] = dispatchS3.Currency;
                }

                //set parcel shop code
                if (!string.IsNullOrWhiteSpace(dispatchS3.ParcelShopCode) && (!directionUpdate.XtraData.IsNull(Direction.XTRA_DELIVERY_PARCEL_SHOP) && !directionUpdate.XtraData[Direction.XTRA_DELIVERY_PARCEL_SHOP].Equals(dispatchS3.ParcelShopCode)) || (directionUpdate.XtraData.IsNull(Direction.XTRA_DELIVERY_PARCEL_SHOP) && !string.IsNullOrWhiteSpace(dispatchS3.ParcelShopCode)))
                {
                    change = true;
                    var old = directionUpdate.XtraData.IsNull(Direction.XTRA_DELIVERY_PARCEL_SHOP) ? "empty" : directionUpdate.XtraData[Direction.XTRA_DELIVERY_PARCEL_SHOP];
                    xtraChange += $"ParcelShopCode: {old} --> {dispatchS3.ParcelShopCode}|";
                    directionUpdate.XtraData[Direction.XTRA_DELIVERY_PARCEL_SHOP] = dispatchS3.ParcelShopCode;
                }

                //set shipmentinfo
                if (!string.IsNullOrWhiteSpace(dispatchS3.Shipmentinformation) && (!directionUpdate.XtraData.IsNull(Direction.XTRA_DELIVERY_SHIPMENTINFO) && !directionUpdate.XtraData[Direction.XTRA_DELIVERY_SHIPMENTINFO].Equals(dispatchS3.Shipmentinformation)) || (directionUpdate.XtraData.IsNull(Direction.XTRA_DELIVERY_SHIPMENTINFO) && !string.IsNullOrWhiteSpace(dispatchS3.Shipmentinformation)))
                {
                    change = true;
                    var old = directionUpdate.XtraData.IsNull(Direction.XTRA_DELIVERY_SHIPMENTINFO) ? "empty" : directionUpdate.XtraData[Direction.XTRA_DELIVERY_SHIPMENTINFO];
                    xtraChange += $"Shipmentinformation: {old} --> {dispatchS3.Shipmentinformation}|";
                    directionUpdate.XtraData[Direction.XTRA_DELIVERY_SHIPMENTINFO] = dispatchS3.Shipmentinformation;
                }

                xtraChange = xtraChange.EndsWith("|") ? xtraChange.Substring(0, xtraChange.Length - 1) : xtraChange;

                //Address change?
                var addressChange = "";
                var isChangeAddress = IsChangeAddress(db, dispatchS3, ref addressChange);

                if (!change && !(isChangeAddress.Item1 || isChangeAddress.Item2))
                {
                    //add bookings
                    var errorBookingMsg = "";
                    if (!SetBookings(direction.PartnerID, direction.PartAddrID, directionUpdate.DirectionID, db, out errorBookingMsg))
                    {
                        errorBookingMsg = $"chyba rezervace: {errorBookingMsg}";
                    }

                    result.Result = DispatchDirLoad.ResultEnum.AlreadyLoaded;
                    result.ErrorText = $"{docInfo} Soubor byl již nahrán {errorBookingMsg}";
                    db.Save(SetInternMessage(result.ErrorText, xml));
                    return result;
                }

                if ((change || isChangeAddress.Item1 || isChangeAddress.Item2) && directionUpdate.DocStatus == Direction.DR_STATUS_DISP_EXPED)
                {
                    result.Result = DispatchDirLoad.ResultEnum.AlreadyLoaded;
                    result.ErrorText = $"{docInfo} Pokus o změnu položek '{xtraChange}' '{addressChange}' v dokladu který byl již expedován!";
                    db.Save(SetInternMessage(result.ErrorText, xml));
                    return result;
                }

                // continue with xtraData update and address update
                // make history
                var changeMsg = $"{xtraChange}|{addressChange}";
                changeMsg = changeMsg.EndsWith("|") ? changeMsg.Substring(0, changeMsg.Length - 1) : changeMsg;
                changeMsg = changeMsg.StartsWith("|") ? changeMsg.Substring(1) : changeMsg;

                if (!string.IsNullOrWhiteSpace(xtraChange))
                    xtraChange = $"XTRAData change: {xtraChange}; ";

                if (!string.IsNullOrWhiteSpace(addressChange))
                    addressChange = $"Address change: {addressChange}; ";

                messageText = $"{xtraChange}{addressChange}";

                var historyFieldChange = new DirectionHistory()
                {
                    DirectionID = directionUpdate.DirectionID,
                    DocPosition = 0,
                    EventCode = Direction.DR_EVENT_FIELD_CHANGE,
                    EventData = changeMsg,
                    EntryUserID = _userID,
                    EntryDateTime = DateTime.Now
                };

                //save new XML to history
                var historyNewXML = new DirectionHistory()
                {
                    DirectionID = directionUpdate.DirectionID,
                    DocPosition = 0,
                    EventCode = Direction.DR_EVENT_RELOAD,
                    EventData = xml,
                    EntryUserID = _userID,
                    EntryDateTime = DateTime.Now
                };

                using (var tr = db.GetTransaction())
                {
                    //save extraData
                    directionUpdate.XtraData.Save(db, directionUpdate.DirectionID);
                    db.Insert(historyFieldChange);
                    db.Insert(historyNewXML);

                    if (saveInvoice)
                        result.InvoiceID = SaveInvoice(directionUpdate, invoice, db);

                    if (isChangeAddress.Item1)
                    {
                        Core.Utils.EntitiesUtils.RepairMaxLengthModelValues(partner, "source_id");
                        db.Save(partner);
                    }

                    if (isChangeAddress.Item2)
                    {
                        //set address.PartnerID
                        address.PartnerID = partner.PartnerID;
                        Core.Utils.EntitiesUtils.RepairMaxLengthModelValues(address, "source_id");
                        db.Save(address);

                        //update direction PartAddrID
                        directionUpdate.PartAddrID = address.PartAddrID;
                        db.Save(directionUpdate);
                    }

                    result.Result = DispatchDirLoad.ResultEnum.Saved;
                    result.Success = true;
                    tr.Complete();

                    //add booking
                    var errorBookingMsg = "";
                    if (!SetBookings(directionUpdate.PartnerID, directionUpdate.PartAddrID, directionUpdate.DirectionID, db, out errorBookingMsg))
                    {
                        messageText = $"{messageText}, chyba rezervace: {errorBookingMsg}";
                    }

                    result.MessageText = messageText;
                    return result;
                }

            }

            //save
            using (var tr = db.GetTransaction())
            {
                if (partner.PartnerID == 0)
                {
                    //insert Partner
                    Core.Utils.EntitiesUtils.RepairMaxLengthModelValues(partner, "source_id");
                    db.Insert(partner);
                    direction.PartnerID = partner.PartnerID;
                    address.PartnerID = partner.PartnerID;
                }
                else
                {
                    //save Partner
                    Core.Utils.EntitiesUtils.RepairMaxLengthModelValues(partner, "source_id");
                    db.Save(partner);
                }

                //set address.PartnerID
                address.PartnerID = partner.PartnerID;

                //save Address
                if (direction.PartAddrID == 0)
                {
                    Core.Utils.EntitiesUtils.RepairMaxLengthModelValues(address, "source_id");
                    var partAddrID = db.Insert(address);
                    direction.PartAddrID = int.Parse(partAddrID.ToString());
                }
                else
                {
                    Core.Utils.EntitiesUtils.RepairMaxLengthModelValues(address, "source_id");
                    db.Save(address);
                }

                //save Direction
                direction.DocDirection = -1;
                db.Insert(direction);

                //save DirectionItems
                foreach (var directionItem in directionItemList)
                {
                    directionItem.DirectionID = direction.DirectionID;
                    db.Insert(directionItem);
                }

                // make history
                var history = new DirectionHistory()
                {
                    DirectionID = direction.DirectionID,
                    DocPosition = 0,
                    EventCode = Direction.DR_STATUS_LOAD,
                    EventData = xml,
                    EntryUserID = _userID,
                    EntryDateTime = DateTime.Now
                };

                db.Insert(history);

                //no items history
                if (dispatchS3.Items.Count < 1)
                {
                    var noItemsHistory = new DirectionHistory()
                    {
                        DirectionID = history.DirectionID,
                        DocPosition = 0,
                        EventCode = Direction.DR_EVENT_NOITEM,
                        EntryUserID = _userID,
                        EntryDateTime = DateTime.Now
                    };

                    db.Insert(noItemsHistory);
                }

                // save info on error
                if (result.Result == DispatchDirLoad.ResultEnum.AlreadyLoaded)
                {
                    var info = $"{docInfo} Výdejový doklad se nepodařilo nahrát *Doklad je již nahrán*";
                    db.Save(SetInternMessage(info, ""));
                }

                if (saveInvoice)
                {
                    //save Invoice
                    result.InvoiceID = SaveInvoice(direction, invoice, db);
                }

                // save ADR xtra data article
                if (adrUpdateArticles.Any())
                {
                    adrUpdateArticles.ForEach(article => article.XtraData.Save(db, article.ArticleID));
                }

                //ADR SUM to direction xtra START
                List<Tuple<int, int, float>> ADRitems = new List<Tuple<int, int, float>>();
                foreach (var usingArticle in usingArticles)
                {
                    usingArticle.Item1.XtraData.Load(db, new Sql().Where("articleID = @0", usingArticle.Item1.ArticleID));
                    if (!usingArticle.Item1.XtraData.IsNull(Article.XTRA_PREPRAVNI_KATEGORIE) && !usingArticle.Item1.XtraData.IsNull(Article.XTRA_OBJEM_KOEF))
                    {
                        if (int.TryParse(usingArticle.Item1.XtraData[Article.XTRA_PREPRAVNI_KATEGORIE].ToString(), out int prepravKat) && float.TryParse(usingArticle.Item1.XtraData[Article.XTRA_OBJEM_KOEF].ToString(), out float objemKoef))
                            ADRitems.Add(new Tuple<int, int, float>(prepravKat, usingArticle.Item2, objemKoef));
                    }
                }

                if (ADRitems.Count > 0)
                {
                    var value = ADR.SafetyPointCalculateBulk(ADRitems);
                    direction.XtraData[Direction.XTRA_ARTICLE_ADR_SUM] = value;
                }
                //ADR SUM to direction xtra END

                //save direction extraData
                direction.XtraData.Save(db, direction.DirectionID);

                result.Result = DispatchDirLoad.ResultEnum.Saved;
                result.Success = true;
                tr.Complete();

                //add bookings
                var errorBookingMsg = "";
                if (!SetBookings(direction.PartnerID, direction.PartAddrID, direction.DirectionID, db, out errorBookingMsg))
                {
                    result.MessageText = $"Chyba rezervace: {errorBookingMsg}";
                }

                return result;
            }

        }

        public InternMessage SetInternMessage(string msgHeader, string xml)
        {
            return new InternMessage()
            {
                MsgClass = InternMessage.IMPORT_XML,
                MsgDateTime = DateTime.Now,
                MsgHeader = msgHeader,
                MsgSeverity = InternMessage.MessageSeverityEnum.High,
                MsgText = xml,
                MsgUserID = _userID
            };
        }

        public Partner SetPartner(DispatchCustomerS3 customer, Database db)
        {
            var partner = db.FirstOrDefault<Partner>("where source_id = @0", customer.CustomerID);
            if (partner == null)
            {
                partner = new Partner();
                // set partner defaults
                partner.PartnerDispCheck = Singleton<DAL.SettingsSet>.Instance.GetValue_Int32(SettingsDictionary.DISP_CHECK_METHOD_DEFAULT);
            }
            
            partner.PartnerAddr_1 = customer.Adress1;
            partner.PartnerAddr_2 = customer.Adress2;
            partner.PartnerCity = customer.City;
            partner.PartnerName = customer.Name;
            partner.PartnerPhone = customer.Phone;
            partner.PartnerOrgIdentNumber = string.IsNullOrWhiteSpace(customer.OrgIdentNumber) ? null : customer.OrgIdentNumber;
            partner.Source_id = customer.CustomerID;
            partner.PartnerStatus = Partner.PA_STATUS_OK;

            return partner;
        }

        private bool GetBoolValue(string val)
        {
            if (val == null)
                return false;

            if (val.ToLower().Equals("ano"))
                return true;
            else
                return false;
        }

        private int SaveInvoice(Direction direction, Invoice invoice, Database db)
        {
            //save Invoice
            db.Save(invoice);

            //save history
            var invoiceHistory = new InvoiceHistory();
            invoiceHistory.InvoiceID = invoice.InvoiceID;
            invoiceHistory.EntryDateTime = DateTime.Now;
            invoiceHistory.EntryUserID = _userID;
            invoiceHistory.EventCode = invoice.DocStatus;
            db.Save(invoiceHistory);

            //save xtra
            invoice.XtraData.Save(db, invoice.InvoiceID);

            //update Direction
            direction.InvoiceID = invoice.InvoiceID;
            db.Save(direction);
            return invoice.InvoiceID;
        }

        private Tuple<bool, bool> IsChangeAddress(Database db, DispatchS3 dispatchS3, ref string addressChange)
        {
            var partner = db.FirstOrDefault<Partner>("where source_id = @0", dispatchS3.Customer.CustomerID);
            var address = db.FirstOrDefault<PartnerAddress>("where source_id = @0", dispatchS3.DeliveryAddress.AddressID);

            var changePartner = false;
            var changePartnerAddress = false;
            addressChange = "";

            //Partner
            if (partner != null)
            {
                if ((partner.PartnerAddr_1 != null && dispatchS3.Customer.Adress1 != null) && !partner.PartnerAddr_1.Equals(dispatchS3.Customer.Adress1))
                {
                    changePartner = true;
                    addressChange += $"partner.PartnerAddr_1: {partner.PartnerAddr_1} --> {dispatchS3.Customer.Adress1}|";
                }

                if ((partner.PartnerAddr_2 != null && dispatchS3.Customer.Adress2 != null) && !partner.PartnerAddr_2.Equals(dispatchS3.Customer.Adress2))
                {
                    changePartner = true;
                    addressChange += $"partner.PartnerAddr_2: {partner.PartnerAddr_2} --> {dispatchS3.Customer.Adress2}|";
                }

                if ((partner.PartnerCity != null && dispatchS3.Customer.City != null) && !partner.PartnerCity.Equals(dispatchS3.Customer.City))
                {
                    changePartner = true;
                    addressChange += $"partner.PartnerCity: {partner.PartnerCity} --> {dispatchS3.Customer.City}|";
                }

                if ((partner.PartnerName != null && dispatchS3.Customer.Name != null) && !partner.PartnerName.Equals(dispatchS3.Customer.Name))
                {
                    changePartner = true;
                    addressChange += $"partner.PartnerName: {partner.PartnerName} --> {dispatchS3.Customer.Name}|";
                }

                if ((partner.PartnerPhone != null && dispatchS3.Customer.Phone != null) && !partner.PartnerPhone.Equals(dispatchS3.Customer.Phone))
                {
                    changePartner = true;
                    addressChange += $"partner.PartnerPhone: {partner.PartnerPhone} --> {dispatchS3.Customer.Phone}|";
                }

                if ((partner.PartnerOrgIdentNumber != null && dispatchS3.Customer.OrgIdentNumber != null) && !partner.PartnerOrgIdentNumber.Equals(dispatchS3.Customer.OrgIdentNumber))
                {
                    changePartner = true;
                    addressChange += $"partner.PartnerOrgIdentNumber: {partner.PartnerOrgIdentNumber} --> {dispatchS3.Customer.OrgIdentNumber}|";
                }

                if ((partner.Source_id != null && dispatchS3.Customer.CustomerID != null) && !partner.Source_id.Equals(dispatchS3.Customer.CustomerID))
                {
                    changePartner = true;
                    addressChange += $"partner.Source_id: {partner.Source_id} --> {dispatchS3.Customer.CustomerID}|";
                }
            }

            //Address
            if (address != null)
            {
                if ((address.AddrCity != null && dispatchS3.DeliveryAddress.City != null) && !address.AddrCity.Equals(dispatchS3.DeliveryAddress.City))
                {
                    changePartnerAddress = true;
                    addressChange += $"address.AddrCity: {address.AddrCity} --> {dispatchS3.DeliveryAddress.City}|";
                }

                if ((address.AddrLine_1 != null && dispatchS3.DeliveryAddress.Adress1 != null) && !address.AddrLine_1.Equals(dispatchS3.DeliveryAddress.Adress1))
                {
                    changePartnerAddress = true;
                    addressChange += $"address.AddrLine_1: {address.AddrLine_1} --> {dispatchS3.DeliveryAddress.Adress1}|";
                }

                if ((address.AddrLine_2 != null && dispatchS3.DeliveryAddress.Adress2 != null) && !address.AddrLine_2.Equals(dispatchS3.DeliveryAddress.Adress2))
                {
                    changePartnerAddress = true;
                    addressChange += $"address.AddrLine_2: {address.AddrLine_2} --> {dispatchS3.DeliveryAddress.Adress2}|";
                }

                if ((address.AddrName != null && dispatchS3.DeliveryAddress.Name != null) && !address.AddrName.Equals(dispatchS3.DeliveryAddress.Name))
                {
                    changePartnerAddress = true;
                    addressChange += $"address.AddrName: {address.AddrName} --> {dispatchS3.DeliveryAddress.Name}|";
                }

                if ((address.AddrPhone != null && dispatchS3.DeliveryAddress.Phone != null) && !address.AddrPhone.Equals(dispatchS3.DeliveryAddress.Phone))
                {
                    changePartnerAddress = true;
                    addressChange += $"address.AddrPhone: {address.AddrPhone} --> {dispatchS3.DeliveryAddress.Phone}|";
                }

                if ((address.AddrRemark != null && dispatchS3.DeliveryAddress.Remark != null) && !address.AddrRemark.Equals(dispatchS3.DeliveryAddress.Remark))
                {
                    changePartnerAddress = true;
                    addressChange += $"address.AddrRemark: {address.AddrRemark} --> {dispatchS3.DeliveryAddress.Remark}|";
                }

                if ((address.DeliveryInst != null && dispatchS3.DeliveryAddress.Deliveryinstructions != null) && !address.DeliveryInst.Equals(dispatchS3.DeliveryAddress.Deliveryinstructions))
                {
                    changePartnerAddress = true;
                    addressChange += $"address.DeliveryInst: {address.DeliveryInst} --> {dispatchS3.DeliveryAddress.Deliveryinstructions}|";
                }

                if ((address.Source_id != null && dispatchS3.DeliveryAddress.AddressID != null) && !address.Source_id.Equals(dispatchS3.DeliveryAddress.AddressID))
                {
                    changePartnerAddress = true;
                    addressChange += $"address.Source_id: {address.Source_id} --> {dispatchS3.DeliveryAddress.AddressID}|";
                }
            }
            else
            {
                //new adress
                changePartnerAddress = true;
                addressChange += $"insert new adress source_id: {dispatchS3.DeliveryAddress.AddressID}";
            }

            if (!string.IsNullOrWhiteSpace(addressChange))
                addressChange = addressChange.EndsWith("|") ? addressChange.Substring(0, addressChange.Length - 1) : addressChange;

            return new Tuple<bool, bool>(changePartner, changePartnerAddress);
        }

        #region booking

        private bool SetBookings(int partnerID, int partAddrID, int directionID, Database db, out string errorMsg)
        {
            errorMsg = "";
            foreach (var requestBooking in _requestBookings)
            {
                if (!SetBooking(requestBooking, partnerID, partAddrID, directionID, db, out errorMsg))
                    return false;
            }

            return true;
        }

        private bool SetBooking(RequestBooking requestBooking, int partnerID, int partAddrID, int directionID, Database db, out string errorMsg)
        {
            errorMsg = "";
            Entities.Booking booking = null;
            var sql = new Sql();
            sql.Where("partnerID = @0 AND stoMoveLotID = @1 AND usedInDirectionID = @2 AND bookingStatus = @3", partnerID, requestBooking.StoMoveLotID, directionID, Entities.Booking.VALID);
            booking = db.FirstOrDefault<Entities.Booking>(sql);

            if (booking == null)
            {
                //try add new booking
                var res = AddBooking(partnerID, partAddrID, requestBooking.Quantity, requestBooking.StoMoveLotID);

                if (!res.Success)
                {
                    errorMsg = res.ErrorText;
                    return false;
                }
                else
                    booking = db.SingleById<Entities.Booking>(res.BookingID);
            }
            else
            {
                if (booking.RequiredQuantity != requestBooking.Quantity || booking.PartnerID != partnerID || booking.StoMoveLotID != requestBooking.StoMoveLotID)
                {
                    //delete and add
                    var model = new BookingDeleteItem
                    {
                        BookingID = booking.BookingID,
                    };

                    var proc = new Procedures.Booking.BookingDeleteItemProc(_userID);
                    var res1 = proc.DoProcedure(model);

                    if (!res1.Success)
                    {
                        errorMsg = res1.ErrorText;
                        return false;
                    }

                    var res2 = AddBooking(partnerID, partAddrID, requestBooking.Quantity, requestBooking.StoMoveLotID);

                    if (!res2.Success)
                    {
                        errorMsg = res2.ErrorText;
                        return false;
                    }
                    else
                        booking = db.SingleById<Entities.Booking>(res2.BookingID);

                }
            }

            if (booking.UsedInDirectionID == null)
            {
                booking.UsedInDirectionID = directionID;
                db.Save(booking);
            }

            return true;
        }

        private int GetLot(int artPackID, string requiredBatchNumber, Database db)
        {
            var sql = new Sql();
            sql.Select("stoMoveLotID");
            sql.From("s4_articleList_packings p");
            sql.InnerJoin("s4_storeMove_lots l on l.artPackID = p.artPackID");
            sql.Where("l.artPackID = @0 AND l.batchNum = @1", artPackID, requiredBatchNumber);
            sql.OrderBy("expirationDate");
            return db.FirstOrDefault<int>(sql);
        }

        private BookingAddItem AddBooking(int partnerID, int partAddrID, int quantity, int stoMoveLotID)
        {
            var bookingTimeout = Singleton<DAL.SettingsSet>.Instance.GetValue_Int32(SettingsDictionary.BOOKING_TIMEOUT_HOURS);
            var model = new BookingAddItem()
            {
                BookingTimeout = DateTime.Now.AddHours(bookingTimeout),
                PartnerID = partnerID,
                PartAddrID = partAddrID,
                RequiredQuantity = quantity,
                StoMoveLotID = stoMoveLotID,
            };

            var proc = new Procedures.Booking.BookingAddItemProc(_userID);
            var res = proc.DoProcedure(model);

            return res;
        }

        #endregion 
    }
}

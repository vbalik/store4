﻿using S4.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Procedures.Load.Helper
{
    public class RequestBooking
    {
        public int StoMoveLotID { get; set; }
        public int Quantity { get; set; }
    }
}

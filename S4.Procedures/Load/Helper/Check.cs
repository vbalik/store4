﻿using S4.Entities;

namespace S4.Procedures.Load.Helper
{
    public static class Check
    {
        public static bool CheckPaymantType(decimal cashOnDelivery, string paymentType, out string error)
        {
            error = "";

            if (string.IsNullOrEmpty(paymentType))
                return true;

            //check  "Platba dobírkou" nebo "Platba dobírkou kartou" a dobírka = 0
            if (cashOnDelivery == 0)
            {
                var type = new DeliveryDirection().PTYPEToDeliveryPayment(paymentType);
                if (type == DeliveryDirection.DeliveryPaymentEnum.CHOD || type == DeliveryDirection.DeliveryPaymentEnum.CAOD)
                {
                    error = $"Pro druh platby '{paymentType}' nemůže být částka nastavena na hodnotu 0";
                    return false;
                }
            }
            return true;
        }
        
    }

}

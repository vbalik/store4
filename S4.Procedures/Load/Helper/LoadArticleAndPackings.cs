﻿using NPoco;
using S4.Entities;
using S4.Entities.Helpers.Xml.Article;
using S4.Entities.Helpers.Xml.Receive;
using System;
using System.Collections.Generic;
using System.Text;
using static S4.Entities.Article;

namespace S4.Procedures.Load.Helper
{
    public class ArticleItems
    {
        public string ArticleManuf { get; set; }
        public string ArticleCode { get; set; }
        public string ArticleName { get; set; }
        public string ArticleID { get; set; }
        public string ArticleUnit { get; set; }
        public bool ArticleUseBatch { get; set; }
        public bool ArticleUseExpiration { get; set; }
        public string ArticleBrandname { get; set; }
        public List<ArticlePacking> ArticlePacking { get; set; }
        public bool? ArticleCooled { get; set; }
        public bool? ArticleMedicines { get; set; }
        public bool? ArticleCytostatics { get; set; }
        public bool? ArticleCheckOnReceive { get; set; }
    }

    public class LoadArticleAndPackings
    {
        private enum TypeLoad : byte
        {
            RECEIVEITEMSS3 = 0,
            ARTICLES3 = 1
        }

        TypeLoad _typeLoad = TypeLoad.ARTICLES3;

        public bool LoadArticleAndPackings_ReceiveItemsS3(ReceiveItemsS3 receiveItemsS3, Database db, ref Article article, ref List<ArticlePacking> articlePackingsList, ref string error)
        {
            var items = new ArticleItems();
            items.ArticleManuf = receiveItemsS3.Article.ArticleManuf;
            items.ArticleCode = receiveItemsS3.Article.ArticleCode;
            items.ArticleName = receiveItemsS3.Article.ArticleName;
            items.ArticleID = receiveItemsS3.Article.ArticleID;
            items.ArticleUnit = receiveItemsS3.Article.ArticleUnit;
            items.ArticleUseBatch = receiveItemsS3.Article.ArticleUseBatch;
            items.ArticleUseExpiration = receiveItemsS3.Article.ArticleUseExpiration;
            items.ArticleCooled = receiveItemsS3.Article.ArticleCooled;
            items.ArticleMedicines = receiveItemsS3.Article.ArticleMedicines;
            items.ArticleCytostatics = receiveItemsS3.Article.ArticleCytostatics;
            items.ArticleCheckOnReceive = receiveItemsS3.Article.ArticleCheckOnReceive;
            items.ArticleBrandname = receiveItemsS3.Article.ArticleBrandname;

            items.ArticlePacking = new List<ArticlePacking>();
            foreach (var item in receiveItemsS3.Article.ArticlePacking)
            {
                items.ArticlePacking.Add(new ArticlePacking() { Source_id = item.ArticlePackingID, PackDesc = item.PackingName, PackRelation = item.PackingRelationDec });
            }

            _typeLoad = TypeLoad.RECEIVEITEMSS3;
            return DoWork(items, db, ref article, ref articlePackingsList, ref error, article.SpecFeatures);
        }

        public bool LoadArticleAndPackings_ArticleS3(ArticleS3 articleS3, Database db, ref Article article, ref List<ArticlePacking> articlePackingsList, ref string error, SpecFeaturesEnum specFeaturesDB)
        {
            var items = new ArticleItems();
            items.ArticleManuf = articleS3.ArticleManuf;
            items.ArticleCode = articleS3.ArticleCode;
            items.ArticleName = articleS3.ArticleName;
            items.ArticleID = articleS3.ArticleID;
            items.ArticleUnit = articleS3.ArticleUnit;
            items.ArticleUseBatch = articleS3.ArticleUseBatch;
            items.ArticleUseExpiration = articleS3.ArticleUseExpiration;
            items.ArticleCooled = articleS3.ArticleCooled;
            items.ArticleMedicines = articleS3.ArticleMedicines;
            items.ArticleCytostatics = articleS3.ArticleCytostatics;
            items.ArticleCheckOnReceive = articleS3.ArticleCheckOnReceive;
            items.ArticleBrandname = articleS3.ArticleBrandname;

            // load xtra data
            if (articleS3.Adr != null)
            {
                LoadXtraDataHelper.LoadAdrXtraData(articleS3.Adr, article, articleS3.ArticleCode);
            }


            items.ArticlePacking = new List<ArticlePacking>();
            foreach (var item in articleS3.ArticlePacking)
            {
                items.ArticlePacking.Add(new ArticlePacking() { Source_id = item.ArticlePackingID, PackDesc = item.PackingName, PackRelation = item.PackingRelationDec });
            }

            _typeLoad = TypeLoad.ARTICLES3;
            return DoWork(items, db, ref article, ref articlePackingsList, ref error, specFeaturesDB);
        }

        private bool DoWork(ArticleItems items, Database db, ref Article article, ref List<ArticlePacking> articlePackingsList, ref string error, SpecFeaturesEnum specFeaturesDB)
        {
            error = "";
            var errorSB = new StringBuilder();
            var isError = false;

            //find manuf
            var man = db.FirstOrDefault<Manufact>("where source_id = @0", items.ArticleManuf);

            if (man == null)
            {
                var manuf = string.IsNullOrEmpty(items.ArticleManuf) ? "<prázdný>" : items.ArticleManuf;
                error = $"Výrobce '{manuf}' nebyl nalezen";
                return false;
            }

            // load article xml
            article.ArticleCode = items.ArticleCode;
            article.ArticleDesc = items.ArticleName;
            article.ManufID = man.ManufID;
            article.Source_id = items.ArticleID;
            article.UnitDesc = items.ArticleUnit;
            article.Brand = items.ArticleBrandname;
            article.SpecFeatures = GetSpecFeatures(items, specFeaturesDB);

            if (article.ArticleID == 0)
            {
                //only new Article!!!
                article.UseBatch = items.ArticleUseBatch;
                article.UseExpiration = items.ArticleUseExpiration;
            }

            // get packings
            foreach (var xmlPacking in items.ArticlePacking)
            {
                ArticlePacking newPacking = null;

                // get artPackID
                var packingSourceID = xmlPacking.Source_id;
                newPacking = db.FirstOrDefault<ArticlePacking>("where source_id = @0", packingSourceID);

                if (newPacking == null)
                    newPacking = new ArticlePacking();
                else
                {
                    if (_typeLoad == TypeLoad.RECEIVEITEMSS3)
                    {
                        if (newPacking.ArticleID != article.ArticleID)
                        {
                            errorSB.Append($"ArticlePacking {packingSourceID} již patří k jiné kartě id: {newPacking.ArticleID} ");
                            isError = true;
                        }
                    }
                    else
                    {
                        //get ArticleID
                        var a = db.FirstOrDefault<Article>("where source_id = @0", article.Source_id);
                        if (a == null)
                        {
                            errorSB.Append($"Zboží source_id = {article.Source_id} nenalezeno v DB ");
                            isError = true;
                            continue;
                        }

                        article.ArticleID = a.ArticleID;
                        if (newPacking.ArticleID != a.ArticleID)
                        {
                            errorSB.Append($"ArticlePacking packingSourceID : {packingSourceID} již patří k jiné kartě id: {newPacking.ArticleID} ");
                            isError = true;
                            continue;
                        }

                    }
                }

                // load packing xml
                newPacking.ArticleID = article.ArticleID;
                newPacking.PackDesc = xmlPacking.PackDesc;
                newPacking.PackRelation = xmlPacking.PackRelation;
                newPacking.Source_id = xmlPacking.Source_id;

                // repair movable
                if (string.Compare(newPacking.PackDesc, article.UnitDesc, true) == 0)
                {
                    // is movable

                    // if new article - set movable
                    if (article.ArticleID == 0)
                        newPacking.MovablePack = true;

                    // reset packRelation
                    if (newPacking.PackRelation != 1)
                        newPacking.PackRelation = 1;
                }

                // add packing to article
                articlePackingsList.Add(newPacking);
            }

            if (!isError)
                return true;
            else
            {
                error = errorSB.ToString();
                return false;
            }
        }

        public SpecFeaturesEnum GetSpecFeatures(ArticleItems items, SpecFeaturesEnum specFeaturesDB)
        {
            //todo testy
            var checkOnReceive = specFeaturesDB.HasFlag(SpecFeaturesEnum.CheckOnReceive);
            var cooled = specFeaturesDB.HasFlag(SpecFeaturesEnum.Cooled);
            var cytostatics = specFeaturesDB.HasFlag(SpecFeaturesEnum.Cytostatics);
            var medicines = specFeaturesDB.HasFlag(SpecFeaturesEnum.Medicines);

            if (items.ArticleCooled.HasValue)
                cooled = items.ArticleCooled.Value;

            if (items.ArticleMedicines.HasValue)
                medicines = items.ArticleMedicines.Value;

            if (items.ArticleCytostatics.HasValue)
                cytostatics = items.ArticleCytostatics.Value;

            if (items.ArticleCheckOnReceive.HasValue)
                checkOnReceive = items.ArticleCheckOnReceive.Value;

            var specFeatures = 0;
            if (cooled)
                specFeatures += (byte)SpecFeaturesEnum.Cooled;

            if (medicines)
                specFeatures += (byte)SpecFeaturesEnum.Medicines;

            if (cytostatics)
                specFeatures += (byte)SpecFeaturesEnum.Cytostatics;

            if (checkOnReceive)
                specFeatures += (byte)SpecFeaturesEnum.CheckOnReceive;

            return (SpecFeaturesEnum)specFeatures;

        }
    }
}

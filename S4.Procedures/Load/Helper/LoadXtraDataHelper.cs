﻿using iText.Layout;
using NLog;
using NLog.Fluent;
using S4.Core.ADR;
using S4.Entities;
using S4.Entities.Helpers.Xml;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace S4.Procedures.Load.Helper
{
    public static class LoadXtraDataHelper
    {
        private static NLog.Logger _nLogger = NLog.LogManager.GetLogger("S4.LoadXtraDataHelper");

        public static void LoadAdrXtraData(IAdrXtraDataS3 adrS3, Article article, string articleCode)
        {
            article.XtraData[Article.XTRA_UN_KOD] = adrS3.UnKod;
            article.XtraData[Article.XTRA_OFICIALNI_NAZEV] = adrS3.OficialniNazev;
            article.XtraData[Article.XTRA_BEZPECNOSTI_ZNACKA] = adrS3.BezpecnostiZnacka;
            SetObalovaSkupina(article, adrS3, articleCode);
            article.XtraData[Article.XTRA_KOD_TUNELU] = adrS3.KodTunelu;
            article.XtraData[Article.XTRA_DRUH_OBALU] = adrS3.DruhObalu;
            SetPrepravniKategorie(article, adrS3, articleCode);
            article.XtraData[Article.XTRA_OHROZUJE_PROSTREDI] = bool.TryParse(adrS3.OhrozujeProstredi?.ToString(), out bool opResult) && opResult;
                        
            if (float.TryParse(adrS3.ObjemovyKoeficient, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out float ok))
                article.XtraData[Article.XTRA_OBJEM_KOEF] = ok;
                      
            article.XtraData[Article.XTRA_MERNA_JEDNOTKA] = adrS3.MernaJednotka;

        }

        private static void SetPrepravniKategorie(Article article, IAdrXtraDataS3 adrS3, string articleCode)
        {
            var pk = adrS3.PrepravniKategorie;
            string[] values = { "1", "2", "3", "4" };
            if (values.Contains(pk))
                article.XtraData[Article.XTRA_PREPRAVNI_KATEGORIE] = pk;
            else
            {
                if (!string.IsNullOrEmpty(pk))
                    _nLogger.Warn($"articleCode: {articleCode} --> PREPRAVNI_KATEG - not allowed value '{pk}'");
            }

            return;
        }

        private static void SetObalovaSkupina(Article article, IAdrXtraDataS3 adrS3, string articleCode)
        {
            var os = adrS3.ObalovaSkupina;
            string[] values = { "", "I", "II", "III" };
            if (values.Contains(os))
                article.XtraData[Article.XTRA_OBALOVA_SKUPINA] = os;
            else
                _nLogger.Warn($"articleCode: {articleCode} --> OBALOVA_SKUPINA - not allowed value '{os}'");

            return;
        }
    }
}

﻿using S4.Entities;
using S4.Entities.Helpers;
using S4.Entities.Helpers.Xml.Dispatch;
using S4.Entities.XtraData;
using S4.ProcedureModels.Load;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using S4.Core;
using System.IO;
using NPoco;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using S4.DAL;
using S4.Procedures.Load.Helper;

namespace S4.Procedures.Load
{
    public class DispatchDirLoadProc : ProceduresBase<DispatchDirLoad>
    {
        public DispatchDirLoadProc(string userID) : base(userID)
        {
        }

        protected override DispatchDirLoad DoIt(DispatchDirLoad procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new DispatchDirLoad() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            // load xml
            var dispatchS3 = procModel.DispatchS3;

            //doc info
            var remark = string.IsNullOrEmpty(procModel.Remark) ? "" : $"{procModel.Remark} ";
            var docInfo = $"{remark}({dispatchS3.DocPrefix}/{dispatchS3.DocDate.ToString("yy")}/{dispatchS3.DocNumber})";

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var dispatchLoadHelper = new DispatchLoadHelper();
                var warning = string.Empty;
                var result = dispatchLoadHelper.DispatchLoad(db, dispatchS3, docInfo, procModel.Xml, _userID);
                return result;
            }
        }

       

    }

    
}

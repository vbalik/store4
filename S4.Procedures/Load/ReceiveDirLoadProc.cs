﻿using NPoco;
using S4.Core.PropertiesComparer;
using S4.Entities;
using S4.Entities.Helpers;
using S4.Entities.Helpers.Xml.Receive;
using S4.Entities.XtraData;
using S4.ProcedureModels.Load;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace S4.Procedures.Load
{
    public class ReceiveDirLoadProc : ProceduresBase<ReceiveDirLoad>
    {
        public const int PARTNER_ID_DUMMY = 0;

        public ReceiveDirLoadProc(string userID) : base(userID)
        {
        }

        protected override ReceiveDirLoad DoIt(ReceiveDirLoad procModel)
        {
            procModel.Result = ReceiveDirLoad.ResultEnum.None;
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new ReceiveDirLoad() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            // load xml
            var receive = procModel.ReceiveS3;

            //doc info
            var remark = string.IsNullOrEmpty(procModel.Remark) ? "" : $"{procModel.Remark} ";
            var docInfo = $"{remark}({procModel.ReceiveS3.DocPrefix}/{procModel.ReceiveS3.DocNumber})";

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {

                var result = new ReceiveDirLoad();
                // new direction doc
                var directionItemList = new List<DirectionItem>();
                var direction = new Direction();
                var docNumPrefix = receive.DocPrefix;

                var prefix = db.Fetch<Prefix>("where docClass = @0 AND docNumPrefix = @1", Direction.DOC_CLASS, docNumPrefix);

                if (prefix == null || prefix.Count == 0)
                {
                    var msg = $"{docInfo} Dokument prefix {docNumPrefix} není nadefinován!";
                    var i = SetInternMessage(msg, procModel.Xml);
                    using (var tr = db.GetTransaction())
                    {
                        db.Save(i);
                        tr.Complete();
                    }
                    return new ReceiveDirLoad() { Success = false, ErrorText = msg };
                }


                direction.DocNumPrefix = docNumPrefix;
                direction.DocNumber = (int)receive.DocNumber;
                direction.DocDate = receive.DocDate;
                direction.DocYear = receive.DocDate.ToString("yy");
                direction.PartnerRef = receive.DocReference;
                direction.DocRemark = receive.DocRemark;

                //set status 
                direction.DocStatus = Direction.DR_STATUS_LOAD;
                direction.EntryDateTime = DateTime.Now;
                direction.EntryUserID = _userID;
                direction.PartnerID = PARTNER_ID_DUMMY;

                // items
                var itemCounter = 0;
                List<Tuple<Article, List<ArticlePacking>>> articlePackingsList = new List<Tuple<Article, List<ArticlePacking>>>();
                foreach (var item in receive.Items)
                {
                    // create new item
                    var newDirectionItem = new DirectionItem();
                    newDirectionItem.DocPosition = itemCounter + 1;

                    // get article
                    Article article = null;
                    article = db.FirstOrDefault<Article>("where source_id = @0", item.Article.ArticleID);

                    if (article != null)
                        newDirectionItem.ArticleCode = article.ArticleCode;
                    else
                    {
                        article = new Article { ArticleStatus = Article.AR_STATUS_OK };
                    }
                        

                    var error = "";
                    var articlePackings = new List<ArticlePacking>();

                    var helper = new Helper.LoadArticleAndPackings();
                    if (!helper.LoadArticleAndPackings_ReceiveItemsS3(item, db, ref article, ref articlePackings, ref error))
                    {
                        var errorInfo = item.Article != null ? $"{docInfo} Kód karty: {item.Article.ArticleCode} Chyba: {error}" : $"{docInfo} {error}";
                        var i = SetInternMessage(error, procModel.Xml);
                        using (var tr = db.GetTransaction())
                        {
                            db.Save(i);
                            tr.Complete();
                        }
                        return new ReceiveDirLoad() { Success = false, ErrorText = error };
                    }

                    articlePackingsList.Add(new Tuple<Article, List<ArticlePacking>>(article, articlePackings));
                    // find current packing
                    var articlePacking = db.FirstOrDefault<ArticlePacking>("where source_id = @0", item.ArticlePackingID);

                    newDirectionItem.Quantity = item.Quantity;
                    if ((articlePacking != null) && !articlePacking.MovablePack && articlePacking.PackRelation != 1)
                        newDirectionItem.Quantity = newDirectionItem.Quantity * articlePacking.PackRelation;

                    //set status item
                    newDirectionItem.ItemStatus = DirectionItem.DI_STATUS_LOAD;
                    newDirectionItem.EntryDateTime = DateTime.Now;
                    newDirectionItem.EntryUserID = _userID;


                    if (article != null && articlePacking != null)
                    {
                        newDirectionItem.ArtPackID = articlePacking.ArtPackID;
                        directionItemList.Add(newDirectionItem);
                        itemCounter++;
                    }

                }

                if (DBCheckNumberExists(db, direction))
                    result.Result = ReceiveDirLoad.ResultEnum.AlreadyLoaded;
                else
                {
                    using (var tr = db.GetTransaction())
                    {
                        //save direction
                        direction.DocDirection = 1;
                        var directionID = db.Insert(direction);
                        var propertiesComparer = new PropertiesComparer<Article>();

                        foreach (var directionItem in directionItemList)
                        {
                            directionItem.DirectionID = int.Parse(directionID.ToString());
                            db.Insert(directionItem);
                        }

                        //save article and packings
                        foreach (var item in articlePackingsList)
                        {
                            var articleHistory = new ArticleHistory()
                            {
                                ArticleID = 0,
                                ArtPackID = 0,
                                EventCode = Article.AR_EVENT_UPDATE,
                                EventData = null,
                                EntryUserID = _userID,
                                EntryDateTime = DateTime.Now
                            };


                            var article = item.Item1;
                            var articlePackings = item.Item2;

                            int articleID = article.ArticleID;
                            if (articleID == 0)
                            {
                                Core.Utils.EntitiesUtils.RepairMaxLengthModelValues(article, "source_id");
                                var id = db.Insert(article);
                                articleID = int.Parse(id.ToString());
                                Info($"Insert Article: articleCode {article.ArticleCode}");
                                // make history
                                articleHistory.ArticleID = articleID;
                                articleHistory.EventCode = Article.AR_STATUS_OK;
                            }
                            else
                            {
                                var articleOrigin = db.SingleById<Article>(article.ArticleID);
                                Core.Utils.EntitiesUtils.RepairMaxLengthModelValues(article, "source_id");
                                db.Save(article);
                                Info($"Update Article: articleCode {article.ArticleCode}");

                                propertiesComparer.DoCompare(articleOrigin, article, nameof(article.ArticleCode), nameof(article.ArticleDesc),
                               nameof(article.ManufID), nameof(article.Source_id), nameof(article.Source_id), nameof(article.UnitDesc), nameof(article.SpecFeatures));

                                // make history
                                articleHistory.ArticleID = articleID;
                                articleHistory.EventData = propertiesComparer.JsonText;
                            }

                            // insert history
                            db.Insert(articleHistory);

                            foreach (var articlePacking in articlePackings)
                            {
                                articlePacking.ArticleID = articleID;
                                articlePacking.PackStatus = ArticlePacking.AP_STATUS_OK;
                                Core.Utils.EntitiesUtils.RepairMaxLengthModelValues(articlePacking, "source_id");
                                db.Save(articlePacking);
                            }
                        }

                        // make history
                        var history = new DirectionHistory()
                        {
                            DirectionID = int.Parse(directionID.ToString()),
                            DocPosition = 0,
                            EventCode = Direction.DR_STATUS_LOAD,
                            EventData = procModel.Xml,
                            EntryUserID = _userID,
                            EntryDateTime = DateTime.Now
                        };

                        db.Insert(history);

                        result.Result = ReceiveDirLoad.ResultEnum.Saved;
                        tr.Complete();
                    }
                }


                // save info on error
                if (result.Result == ReceiveDirLoad.ResultEnum.AlreadyLoaded)
                {
                    var info = $"{docInfo} Příjmový doklad se nepodařilo nahrát *Doklad je již nahrán*";
                    result.ErrorText = info;
                    using (var tr = db.GetTransaction())
                    {
                        var i = SetInternMessage(info, "");
                        db.Save(i);
                        tr.Complete();
                    }
                    return result;
                }

                result.Success = true;
                return result;


            }
        }

        private bool DBCheckNumberExists(Database db, Direction direction)
        {
            var sql = new Sql();
            sql.Select("count(1)");
            sql.From("s4_direction");
            sql.Where("docNumPrefix = @0 AND docYear = @1 AND docNumber = @2", direction.DocNumPrefix, direction.DocYear, direction.DocNumber);
            var count = db.First<int>(sql);
            return count > 0 ? true : false;
        }

        private InternMessage SetInternMessage(string msgHeader, string xml)
        {
            return new InternMessage()
            {
                MsgClass = InternMessage.IMPORT_XML,
                MsgDateTime = DateTime.Now,
                MsgHeader = msgHeader,
                MsgSeverity = InternMessage.MessageSeverityEnum.High,
                MsgText = xml,
                MsgUserID = null //call from exchange
            };
        }

    }
}

﻿using NPoco;
using S4.Core.PropertiesComparer;
using S4.Entities;
using S4.Entities.Helpers;
using S4.Entities.Helpers.Xml.Article;
using S4.Entities.XtraData;
using S4.ProcedureModels.Load;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace S4.Procedures.Load
{
    public class CancellationLoadProc : ProceduresBase<CancellationLoad>
    {

        public CancellationLoadProc(string userID) : base(userID)
        {
        }

        protected override CancellationLoad DoIt(CancellationLoad procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new CancellationLoad() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            var cancellationS3 = procModel.CancellationS3;
            //doc info
            var remark = string.IsNullOrEmpty(procModel.Remark) ? "" : $"{procModel.Remark} ";
            var docInfo = $"{remark}({cancellationS3.DocPrefix}/{cancellationS3.DocDate:yy}/{cancellationS3.DocNumber})";

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var direction = db.FirstOrDefault<Direction>("where docNumber = @0 AND docNumPrefix = @1 AND docYear = @2", cancellationS3.DocNumber, cancellationS3.DocPrefix, cancellationS3.DocDate.ToString("yy"));

                if (direction == null)
                {
                    var msg = $"{docInfo} Doklad nebyl nalezen!";
                    var i = SetInternMessage(msg, procModel.Xml);
                    db.Save(i);

                    return new CancellationLoad() { Success = false, ErrorText = msg };
                }

                //check status
                if (direction.DocStatus != Direction.DR_STATUS_LOAD)
                {
                    using (var tr = db.GetTransaction())
                    {
                        var status = (from s in Direction.Statuses.Events where s.Code.Equals(direction.DocStatus) select s.Name).FirstOrDefault();
                        var statusName = status != null ? status : direction.DocStatus;
                        var msg = $"{docInfo} Doklad nelze stornovat protože je ve stavu *{statusName}*";
                        var i = SetInternMessage(msg, procModel.Xml);

                        // make history
                        var historyUnsuccessful = new DirectionHistory()
                        {
                            DirectionID = direction.DirectionID,
                            DocPosition = 0,
                            EventCode = Direction.DR_EVENT_UNSUCCESSFUL_CANCEL,
                            EventData = msg,
                            EntryUserID = _userID,
                            EntryDateTime = DateTime.Now
                        };

                        db.Save(historyUnsuccessful);
                        db.Save(i);

                        tr.Complete();

                        // log
                        Info($"CancellationLoadProc; DirectionID: {direction.DirectionID}, unsuccessful cancel - {msg}");

                        return new CancellationLoad() { Success = false, ErrorText = msg };
                    }
                }

                //status
                direction.DocStatus = Direction.DR_STATUS_CANCEL;

                // make history
                var history = new DirectionHistory()
                {
                    DirectionID = direction.DirectionID,
                    DocPosition = 0,
                    EventCode = Direction.DR_STATUS_CANCEL,
                    EventData = $"Doklad stornován z IS ABRA. {remark}",
                    EntryUserID = _userID,
                    EntryDateTime = DateTime.Now
                };


                using (var tr = db.GetTransaction())
                {
                    db.Save(history);
                    db.Save(direction);

                    tr.Complete();

                    // log
                    Info($"CancellationLoadProc; DirectionID: {direction.DirectionID}, set cancel is successful");
                }

                return new CancellationLoad() { Success = true, ErrorText = "" };
            }
        }

        private InternMessage SetInternMessage(string msgHeader, string xml)
        {
            return new InternMessage()
            {
                MsgClass = InternMessage.IMPORT_XML,
                MsgDateTime = DateTime.Now,
                MsgHeader = msgHeader,
                MsgSeverity = InternMessage.MessageSeverityEnum.High,
                MsgText = xml,
                MsgUserID = null //call from exchange
            };
        }


    }
}

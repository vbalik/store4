﻿using NPoco;
using S4.Core.PropertiesComparer;
using S4.Entities;
using S4.Entities.Helpers;
using S4.Entities.Helpers.Xml.Article;
using S4.Entities.XtraData;
using S4.ProcedureModels.Load;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace S4.Procedures.Load
{
    public class ArticleLoadProc : ProceduresBase<ArticleLoad>
    {
        private const string DISABLE = "disabled";

        public ArticleLoadProc(string userID) : base(userID)
        {
        }

        protected override ArticleLoad DoIt(ArticleLoad procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new ArticleLoad() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            var articleS3 = procModel.ArticleS3;

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var result = new ArticleLoad();
                var articlePackingsList = new List<ArticlePacking>();
                // look for article
                var articleSourceID = articleS3.ArticleID;
                // load or create article
                Article article = null;
                article = db.FirstOrDefault<Article>("where source_id = @0", articleSourceID);

                if (article == null)
                    article = new Article();

                var articleStatus = articleS3.ArticleStatus;
                bool disableOrder = false;

                if (articleStatus != null)
                    disableOrder = articleStatus.ToLower().Equals(DISABLE) ? true : false;

                // make history
                var historySave = false;
                var articleHistory = new ArticleHistory()
                {
                    ArticleID = 0,
                    ArtPackID = 0,
                    EventCode = null,
                    EventData = null,
                    EntryUserID = _userID,
                    EntryDateTime = DateTime.Now
                };

                var articleXML = new Article();
                // get values for history
                var propertiesComparer = new PropertiesComparer<Article>();
                var remark = string.IsNullOrEmpty(procModel.Remark) ? "" : $"{procModel.Remark} ";

                if (disableOrder)
                {
                    if (article.ArticleID != 0)
                    {
                        article.ArticleStatus = Article.AR_STATUS_DISABLED;
                        // to history
                        articleHistory.EventCode = Article.AR_STATUS_DISABLED;
                        historySave = true;
                    }
                    else
                    {
                        var msg = $"{remark}Kód karty: {articleS3.ArticleCode} Chyba: Karta sourceID {articleSourceID} nebyla nalezena!";
                        var i = SetInternMessage(msg, procModel.Xml);
                        using (var tr = db.GetTransaction())
                        {
                            db.Save(i);
                            tr.Complete();
                        }
                        return new ArticleLoad() { Success = false, ErrorText = msg };
                    }
                }
                else
                {
                    var error = "";
                    var helper = new Helper.LoadArticleAndPackings();
                    if (!helper.LoadArticleAndPackings_ArticleS3(articleS3, db, ref articleXML, ref articlePackingsList, ref error, article.SpecFeatures))
                    {
                        var i = SetInternMessage($"{remark}Kód karty: {articleS3.ArticleCode} Chyba: {error}", procModel.Xml);
                        using (var tr = db.GetTransaction())
                        {
                            db.Save(i);
                            tr.Complete();
                        }
                        return new ArticleLoad() { Success = false, ErrorText = error };
                    }
                    else
                    {
                        if (article.ArticleID > 0)
                        {
                            propertiesComparer.DoCompare(article, articleXML, nameof(articleXML.ArticleCode), nameof(articleXML.ArticleDesc),
                               nameof(articleXML.ManufID), nameof(articleXML.Source_id), nameof(articleXML.Source_id), nameof(articleXML.UnitDesc), nameof(articleXML.SpecFeatures), nameof(articleXML.Brand));

                            if (propertiesComparer.PropertyValuesCompare.Items.Count > 0)
                                historySave = true;
                        }
                        else
                        {
                            //only new Article!!!
                            article.MixBatch = articleXML.MixBatch;
                            article.UseBatch = articleXML.UseBatch;
                            article.UseExpiration = articleXML.UseExpiration;
                        }

                        article.ArticleCode = articleXML.ArticleCode;
                        article.ArticleDesc = articleXML.ArticleDesc;
                        article.ManufID = articleXML.ManufID;
                        article.Source_id = articleSourceID;
                        article.UnitDesc = articleXML.UnitDesc;
                        article.SpecFeatures = articleXML.SpecFeatures;
                        article.Brand = articleXML.Brand;

                        // xtra
                        article.XtraData.Load(db, new Sql().Where("articleID = @0", article.ArticleID));
                        foreach (var xtra in articleXML.XtraData.Data)
                        {
                            var xtraType = Article.XtraDataDict[xtra.XtraCode].XtraType;
                            article.XtraData[xtra.XtraCode] = Convert.ChangeType(xtra.GetValue(), xtraType);
                        }
                    }

                    // to history
                    articleHistory.EventCode = Article.AR_EVENT_UPDATE;
                }

                // save
                using (var tr = db.GetTransaction())
                {
                    if (article.ArticleID == 0)
                    {
                        article.ArticleStatus = Article.AR_STATUS_OK;
                        Core.Utils.EntitiesUtils.RepairMaxLengthModelValues(article, "source_id");
                        db.Insert(article);
                        Info($"Insert Article: articleCode {article.ArticleCode}");
                    }
                    else
                    {
                        Core.Utils.EntitiesUtils.RepairMaxLengthModelValues(article, "source_id");
                        db.Save(article);
                        Info($"Update Article: articleCode {article.ArticleCode}");
                    }

                    //save packing
                    foreach (var articlePacking in articlePackingsList)
                    {
                        articlePacking.ArticleID = article.ArticleID;
                        articlePacking.PackStatus = ArticlePacking.AP_STATUS_OK;
                        Core.Utils.EntitiesUtils.RepairMaxLengthModelValues(articlePacking, "source_id");
                        db.Save(articlePacking);
                    }

                    //propertiesComparer
                    if (historySave)
                    {
                        articleHistory.ArticleID = article.ArticleID;
                        articleHistory.EventData = propertiesComparer.JsonText;
                        db.Insert(articleHistory);
                    }
                        
                    //save xtra
                    article.XtraData.Save(db, article.ArticleID);

                    tr.Complete();
                    result.Saved = true;
                }

                result.Success = true;
                return result;


            }
        }

        private InternMessage SetInternMessage(string msgHeader, string xml)
        {
            return new InternMessage()
            {
                MsgClass = InternMessage.IMPORT_XML,
                MsgDateTime = DateTime.Now,
                MsgHeader = msgHeader,
                MsgSeverity = InternMessage.MessageSeverityEnum.High,
                MsgText = xml,
                MsgUserID = null //call from exchange
            };
        }


    }
}

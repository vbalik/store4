﻿using S4.Entities;
using S4.Entities.Helpers;
using S4.Entities.Helpers.Xml.Dispatch;
using S4.Entities.XtraData;
using S4.ProcedureModels.Load;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using S4.Core;
using System.IO;
using NPoco;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using S4.DAL;
using S4.Entities.Helpers.Xml.Invoice;
using S4.Core.Extensions;
using S4.Procedures.Load.Helper;

namespace S4.Procedures.Load
{
    public class InvoiceLoadProc : ProceduresBase<InvoiceLoad>
    {
        public InvoiceLoadProc(string userID) : base(userID)
        {
        }

        protected override InvoiceLoad DoIt(InvoiceLoad procModel)
        {
            var messageText = "";
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new InvoiceLoad() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //check if exists
                var invoice = db.FirstOrDefault<Invoice>("where docNumber = @0 AND docPrefix = @1 AND docYear = @2", procModel.InvoiceS3.DocNumber, procModel.InvoiceS3.DocPrefix, procModel.InvoiceS3.DocDate.ToString("yy"));

                if (invoice == null)
                {
                    messageText = $"Insert Invoice - docNumber: {procModel.InvoiceS3.DocNumber} docPrefix: {procModel.InvoiceS3.DocPrefix} docYear: {procModel.InvoiceS3.DocDate.ToString("yy")}";
                    invoice = new Invoice()
                    {
                        DocNumber = procModel.InvoiceS3.DocNumber,
                        DocPrefix = procModel.InvoiceS3.DocPrefix,
                        DocYear = procModel.InvoiceS3.DocDate.ToString("yy"),
                        LastDateTime = DateTime.Now,
                        EntryDateTime = DateTime.Now,
                        DocStatus = Invoice.INV_STATUS_LOAD
                    };

                    //xtra
                    invoice.XtraData[Invoice.XTRA_TRANSPORTATION_TYPE] = procModel.InvoiceS3.TransportationType;
                    invoice.XtraData[Invoice.XTRA_PAYMENT_TYPE] = procModel.InvoiceS3.PaymentType;
                    invoice.XtraData[Invoice.XTRA_PAYMENT_REFERENCE] = procModel.InvoiceS3.PaymentReference;
                    invoice.XtraData[Invoice.XTRA_CASH_ON_DELIVERY] = procModel.InvoiceS3.CashOnDelivery;
                    invoice.XtraData[Invoice.XTRA_DELIVERY_COUNTRY] = procModel.InvoiceS3.Country;
                    invoice.XtraData[Invoice.XTRA_DELIVERY_CURRENCY] = procModel.InvoiceS3.Currency;
                    invoice.XtraData[Invoice.XTRA_DOC_DATE] = procModel.InvoiceS3.DocDate;
                }
                else
                {
                    invoice.DocStatus = Invoice.INV_STATUS_RELOAD;
                    invoice.LastDateTime = DateTime.Now;
                    invoice.XtraData.Load(db, new NPoco.Sql().Where("invoiceID = @0", invoice.InvoiceID));
                    messageText = $"Update Invoice - docNumber: {procModel.InvoiceS3.DocNumber} docPrefix: {procModel.InvoiceS3.DocPrefix} docYear: {procModel.InvoiceS3.DocDate.ToString("yy")}";

                    //xtra
                    invoice.XtraData[Invoice.XTRA_TRANSPORTATION_TYPE] = procModel.InvoiceS3.TransportationType;
                    invoice.XtraData[Invoice.XTRA_PAYMENT_TYPE] = procModel.InvoiceS3.PaymentType;
                    invoice.XtraData[Invoice.XTRA_PAYMENT_REFERENCE] = procModel.InvoiceS3.PaymentReference;
                    invoice.XtraData[Invoice.XTRA_CASH_ON_DELIVERY] = procModel.InvoiceS3.CashOnDelivery;
                    invoice.XtraData[Invoice.XTRA_DELIVERY_COUNTRY] = procModel.InvoiceS3.Country;
                    invoice.XtraData[Invoice.XTRA_DELIVERY_CURRENCY] = procModel.InvoiceS3.Currency;
                    invoice.XtraData[Invoice.XTRA_DOC_DATE] = procModel.InvoiceS3.DocDate;
                }
                
                //check  "Platba dobírkou" nebo "Platba dobírkou kartou" a dobírka = 0
                var checkPaymantTypeMsg = "";
                if (!Check.CheckPaymantType(procModel.InvoiceS3.CashOnDelivery, procModel.InvoiceS3.PaymentType, out checkPaymantTypeMsg))
                {
                    return new InvoiceLoad() { Success = false, ErrorText = checkPaymantTypeMsg };
                }

                //save
                using (var tr = db.GetTransaction())
                {
                    //save invoice
                    db.Save(invoice);

                    //save history
                    var history = new InvoiceHistory();
                    history.InvoiceID = invoice.InvoiceID;
                    history.EntryDateTime = DateTime.Now;
                    history.EntryUserID = _userID;
                    history.EventData = procModel.Xml;
                    history.EventCode = invoice.DocStatus;
                    db.Save(history);

                    //save xtra
                    invoice.XtraData.Save(db, invoice.InvoiceID);

                    var errorSB = new StringBuilder();
                    var messageTextSB = new StringBuilder();
                    var warningTextSB = new StringBuilder();
                    foreach (var dispatch in procModel.InvoiceS3.DispatchDocs)
                    {
                        var errorText = "";
                        var warningText = "";
                        var result = DispatchLoad(db, procModel.Remark, dispatch, invoice, out errorText, out warningText);

                        if (result == ResultLoad.ERROR)
                            errorSB.Append($"{errorText};");
                        else if (result == ResultLoad.ROLLBACK)
                            return new InvoiceLoad() { Success = false, ErrorText = errorText };
                        else if (result == ResultLoad.OK)
                            messageTextSB.Append($"{dispatch.DocPrefix}/{dispatch.DocNumber};");

                        if (!string.IsNullOrEmpty(warningText))
                            warningTextSB.Append(warningText);
                    }

                    tr.Complete();
                    var messageTextDir = string.IsNullOrWhiteSpace(messageTextSB.ToString()) ? "" : $"Založeny doklady {messageTextSB.ToString()}";
                    return new InvoiceLoad() { Success = true, ErrorText = errorSB.ToString(), InvoceID = invoice.InvoiceID, MessageText = $"{messageText}. {messageTextDir}", WarningText = warningTextSB.ToString() };
                }
            }

        }


        #region Load Dispatch

        private ResultLoad DispatchLoad(Database db, string remark, DispatchS3 dispatchS3, Invoice invoice, out string errorText, out string warningText)
        {
            
            errorText = "";
            warningText = "";
            var rem = string.IsNullOrEmpty(remark) ? "" : $"{remark} ";
            var directionName = $"{rem}Doklad {dispatchS3.DocPrefix}/{dispatchS3.DocDate.ToString("yy")}/{dispatchS3.DocNumber}";
            var xml = DispatchS3.Serializer.Serialize(dispatchS3);

            //doc info
            var docInfo = $"Faktura: ({invoice.DocPrefix}/{invoice.DocYear}/{invoice.DocNumber}) Doklad: ({directionName})";

            // check prefix & number
            var directionExist = db.FirstOrDefault<Direction>("where docNumPrefix = @0 AND docYear = @1 AND docNumber = @2", dispatchS3.DocPrefix, dispatchS3.DocDate.ToString("yy"), dispatchS3.DocNumber);

            if (directionExist != null)
            {
                //check invoiceID
                if (directionExist.InvoiceID != invoice.InvoiceID)
                {
                    errorText = $"{docInfo} DirectionID: {directionExist.DirectionID} ({directionExist.DocNumPrefix}/{directionExist.DocNumber}) patří k jiné faktuře InvoiceID: {directionExist.InvoiceID}  ({directionExist.DocNumPrefix}/{directionExist.DocNumber})";
                    SaveInternMessage(errorText, xml);
                    return ResultLoad.ROLLBACK;
                }

                //compare direction items
                if (!CompareDirectionItems(directionExist.DirectionID, dispatchS3.Items, ref errorText))
                {
                    errorText = $"{docInfo} Položky dokladu jsou rozdílné! Chyba: {errorText}";
                    SaveInternMessage(errorText, xml);
                    return ResultLoad.ROLLBACK;
                }
            }

            var dispatchLoadHelper = new DispatchLoadHelper();
            var result = dispatchLoadHelper.DispatchLoad(db, dispatchS3, docInfo, xml, _userID, invoice.InvoiceID);

            if (result.Success)
            {
                errorText = "";
                return ResultLoad.OK;
            }
            else
            {
                errorText = result.ErrorText;
                return ResultLoad.ERROR;
            }
                       
        }

        private void SaveInternMessage(string errorText, string xml)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var dispatchLoadHelper = new DispatchLoadHelper();
                db.Save(dispatchLoadHelper.SetInternMessage(errorText, xml));
            }
        }
    
        private bool CompareDirectionItems(int directionID, List<DispatchItemS3> items, ref string errorText)
        {
            errorText = "";

            var directionModels = new DAL.DirectionDAL().GetDirectionAllData(directionID);
            var dataHis = (from h in directionModels.History where h.EventCode.Equals(Direction.DR_STATUS_LOAD) && h.DocPosition == 0 orderby h.EntryDateTime select h.EventData).LastOrDefault();

            if (dataHis == null)
            {
                errorText = $"DirectionID: {directionID} - Historie je prázdná!";
                return false;
            }

            DispatchS3 dispatchS3His = null;
            System.Exception exception = null;
            if (!DispatchS3.Serializer.Deserialize(dataHis, out dispatchS3His, out exception))
            {
                errorText = $"DirectionID: {directionID} - chyba při pokusu deserializovat historii!";
                return false;
            }

            foreach (var item in items)
            {
                var itemHis = (from h in dispatchS3His.Items where h.ArticlePackingID.Equals(item.ArticlePackingID) select h).FirstOrDefault();

                if (itemHis == null)
                {
                    errorText = $"DirectionID: {directionID} - nenalezena položka ArticlePackingID: {item.ArticlePackingID}";
                    return false;
                }

                if (!itemHis.PublicPropertiesEquals<DispatchItemS3>(item, "Extradata"))
                {
                    errorText = $"DirectionID: {directionID} - položka ArticlePackingID: {item.ArticlePackingID} je rozdílná!";
                    return false;
                }

                if (!itemHis.Extradata.PublicPropertiesEquals<DispatchItemExtradataS3>(item.Extradata))
                {
                    errorText = $"DirectionID: {directionID} - položka ArticlePackingID: {item.ArticlePackingID} (Extradata) jsou rozdílné!";
                    return false;
                }
            }

            return true;
            
        }

        private enum ResultLoad
        {
            OK,
            ERROR,
            ROLLBACK
        }

        #endregion

    }

}

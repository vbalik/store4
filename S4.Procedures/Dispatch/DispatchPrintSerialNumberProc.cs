﻿using System;
using System.Collections.Generic;
using System.Text;
using NPoco;
using S4.Core;
using S4.Entities;
using S4.ProcedureModels.Dispatch;
using S4.Report.Model.Dispatch;
using S4.StoreCore.Interfaces;
using System.Linq;
using S4.Entities.Helpers.Xml.Dispatch;
using S4.DAL;
using QRCoder;
using System.Drawing;
using S4.Core.EAN;
using System.Drawing.Drawing2D;

namespace S4.Procedures.Dispatch
{
    public class DispatchPrintSerialNumberProc : ProceduresBase<DispatchPrintSerialNumber>
    {
        public DispatchPrintSerialNumberProc(string userID) : base(userID)
        {
        }


        protected override DispatchPrintSerialNumber DoIt(DispatchPrintSerialNumber procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new DispatchPrintSerialNumber() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                if (procModel.ReportID == 0)
                    procModel.ReportID = Singleton<DAL.SettingsSet>.Instance.GetValue_Int32(Entities.SettingsDictionary.DISPATCH_SN_REPORT_ID);

                // get direction
                var direction = (new DAL.DirectionDAL()).GetDirectionAllData(procModel.DirectionID);
                if (direction == null)
                    return new DispatchPrintSerialNumber() { Success = false, ErrorText = $"Doklad nebyl nalezen; DirectionID: {procModel.DirectionID}" };

                var save = Singleton<ServicesFactory>.Instance.GetSave();
                // get destination positionID
                List<MoveResult> moveResult = new List<MoveResult>();

                var dispatchPositionsModel = Singleton<DAL.Cache.PositionByCategoryModelCache>.Instance.GetItem(Entities.Position.PositionCategoryEnum.Dispatch);
                moveResult = save.MoveResult_ByDirection(db, direction.DirectionID, StoreMove.MO_DOCTYPE_DISPATCH, 
                                (mi) => !dispatchPositionsModel.PositionIDs.Contains(mi.PositionID));
                
                //make data to report
                var toReport = new List<DispatchSNReportModel>();
                foreach (var move in moveResult)
                {
                    var articlePacking = Singleton<DAL.Cache.ArticlePackingCache>.Instance.GetItem(move.ArtPackID);
                    var article = Singleton<DAL.Cache.ArticleModelCache>.Instance.GetItem(articlePacking.ArticleID);

                    //expirace, šarže 
                    var batchNum = move.BatchNum == null ? "" : move.BatchNum.ToString();
                    var serialNumbers = (new DAL.StoreMoveSerialsDAL()).GetStoreMoveSerialsByStoMoveItemID(move.StoMoveItemID);

                    var row = new DispatchSNReportModel();
                    row.ArticleDesc = article.ArticleDesc;
                    row.BarCode = articlePacking.BarCode;
                    row.ExpirationDate = move.ExpirationDate;
                    row.BatchNum = batchNum;

                    //if (serialNumbers.Count == 0)
                    //{
                    //    row.SerialNumber = "";
                    //    row.Code2D = GetQrImage(row);
                    //    toReport.Add(row);
                    //}
                    //else
                    if (serialNumbers.Count > 0)
                    {
                        foreach (var serialNumber in serialNumbers)
                        {
                            var row2 = new DispatchSNReportModel();
                            row2.ArticleDesc = article.ArticleDesc;
                            row2.BarCode = articlePacking.BarCode;
                            row2.ExpirationDate = move.ExpirationDate;
                            row2.BatchNum = batchNum;
                            row2.SerialNumber = serialNumber.SerialNumber;
                            row2.Code2D = GetQrImage(row2);
                            toReport.Add(row2);
                        }
                    }
                }
                                
                //P R I N T
                //get report
                var report = db.SingleOrDefault<Reports>("where reportID = @0", procModel.ReportID);
                if (report == null)
                    return new DispatchPrintSerialNumber() { Success = false, ErrorText = $"Report nebyl nalezen; reportID: {procModel.ReportID}" };

                var printerLocation = Singleton<DAL.Cache.PrinterLocationCache>.Instance.GetItem(procModel.PrinterLocationID);

                if (printerLocation == null)
                    return new DispatchPrintSerialNumber() { Success = false, ErrorText = $"PrinterLocation nebyl nalezen; printerLocationID: {procModel.PrinterLocationID}" };

                //variables
                var variables = new List<Tuple<string, object, Type>>();
                variables.Add(new Tuple<string, object, Type>("DocInfo", $"{direction.DocNumPrefix}/{direction.DocYear}/{direction.DocNumber}", typeof(string)));
                variables.Add(new Tuple<string, object, Type>("DocDate", direction.DocDate, typeof(DateTime)));
                variables.Add(new Tuple<string, object, Type>("RowsCount", toReport.Count, typeof(int)));
                
                //do print
                Report.Print print = new Report.Print();
                print.Variables = variables;
                print.ReportDefinition = report.Template; //šablona z DB
                //print.DesignMode = true; //pouze pro úpravy šablony
                //print.ReportDefFilePath = @"c:\Projects\store4\Tools\Reporty\SeriovaCisla.srt"; //pouze pro úpravy šablony

                byte[] fileData = null;
                if (IsTesting)
                {
                    fileData = print.DoPrint2Text(toReport);
                }
                else
                {
                    if (procModel.LocalPrint)
                        fileData = print.DoPrint2PDF(toReport);
                    else
                        print.DoPrint(printerLocation.PrinterPath, toReport);
                }

                if (print.IsError)
                    return new DispatchPrintSerialNumber() { Success = false, ErrorText = print.ErrorText };
                else
                    return new DispatchPrintSerialNumber() { Success = true, LocalPrint = procModel.LocalPrint, FileData = fileData };
            }


        }

        private Bitmap GetQrImage(DispatchSNReportModel row)
        {
            // barcode should be 14 characters; if it is 13, add '0' at begining
            // other problems are handled in GetEANCode function
            var barCode = (row.BarCode.Length == 13) ? "0" + row.BarCode : row.BarCode;

            var ean = new EAN
            {
                Parts = new List<EANPart> {
                        { new EANPart { EANCode = EANCodeEnum.ShippingContainerCode, Value = barCode } },
                        { new EANPart { EANCode = EANCodeEnum.SerialNumber, Value = row.SerialNumber } }}
            };

            if (!string.IsNullOrWhiteSpace(row.BatchNum))
                ean.Parts.Add(new EANPart { EANCode = EANCodeEnum.BatchNumber, Value = row.BatchNum });

            if (row.ExpirationDate.HasValue)
                ean.Parts.Add(new EANPart { EANCode = EANCodeEnum.ExpirationDate, Value = row.ExpirationDate.Value.ToString("yyMMdd") });

            if (ean.GetEANCode(out string eanCode, out string _))
            {
                //QR Code using
                using (QRCodeGenerator qrGenerator = new QRCodeGenerator())
                {
                    QRCodeData qrCodeData = qrGenerator.CreateQrCode(eanCode, QRCodeGenerator.ECCLevel.L);
                    QRCode qrCode = new QRCode(qrCodeData);
                    Bitmap qrCodeImage = qrCode.GetGraphic(20);
                    return qrCodeImage;
                }
            }
            else
            {
                using (Font font = new Font("Arial", 36, FontStyle.Bold, GraphicsUnit.Point))
                {
                    //error 
                    var bmp = new Bitmap(1, 1);
                    bmp.SetPixel(0, 0, Color.WhiteSmoke);
                    bmp = new Bitmap(bmp, 200, 200);

                    //write text to bitmap
                    RectangleF rectf = new RectangleF(0, 0, 200, 200);
                    Graphics g = Graphics.FromImage(bmp);

                    g.SmoothingMode = SmoothingMode.AntiAlias;
                    g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    g.PixelOffsetMode = PixelOffsetMode.HighQuality;

                    StringFormat stringFormat = new StringFormat();
                    stringFormat.Alignment = StringAlignment.Center;
                    stringFormat.LineAlignment = StringAlignment.Center;

                    g.DrawString("?", font, Brushes.Red, rectf, stringFormat);
                    g.Flush();

                    return bmp;
                }
            }
        }
    }
       

}

﻿using S4.Core;
using S4.Core.Data;
using S4.Entities;
using S4.Entities.Models;
using S4.ProcedureModels.Dispatch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace S4.Procedures.Dispatch
{
    public class DispatchShowItemsProc : ProceduresBase<DispatchShowItems>
    {
        public DispatchShowItemsProc(string userID) : base(userID)
        {
        }


        protected override DispatchShowItems DoIt(DispatchShowItems procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new DispatchShowItems() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {

                // get storeMove
                var stMove = (new DAL.StoreMoveDAL()).GetStoreMoveAllData(procModel.StoMoveID);
                if (stMove == null)
                    return new DispatchShowItems() { Success = false, ErrorText = $"StoreMove nebylo nalezeno; StoMoveID: {procModel.StoMoveID}" };

                // get parent direction
                var direction = (new DAL.DirectionDAL()).GetDirectionAllData(stMove.Parent_directionID);
                if (direction == null)
                    return new DispatchShowItems() { Success = false, ErrorText = $"Direction nebylo nalezeno; DirectionID: {stMove.Parent_directionID}" };

                // set dispatch section 
                if (direction.XtraData.IsNull(Direction.XTRA_DISPATCH_POSITION_ID, 0))
                    return new DispatchShowItems() { Success = false, ErrorText = "Direction objekt nemá XTRA_DISPATCH_POSITION_ID hodnotu" };

                var result = new DispatchShowItems();

                // do tests
                if (stMove.DocStatus != StoreMove.MO_STATUS_NEW_DISPATCH)
                {
                    // stMove has changed status
                    result.Items = new List<Entities.Helpers.ItemInfo>();
                    result.DoneItems = new List<Entities.Helpers.ItemInfo>();
                }
                else
                {
                    // create list of items to do
                    result.Items = stMove.Items
                        .Where(i => i.ItemDirection == StoreMoveItem.MI_DIRECTION_OUT && i.ItemValidity == StoreMoveItem.MI_VALIDITY_NOT_VALID)
                        .Select(i => new Entities.Helpers.ItemInfo()
                        {
                            StoMoveLotID = i.StoMoveLotID,
                            Quantity = i.Quantity,
                            CarrierNum = i.CarrierNum,
                            XtraString = Singleton<DAL.Cache.PositionCache>.Instance.GetItem(i.PositionID).PosCode,
                            XtraInt = i.StoMoveItemID,
                            StoMoveItemID = i.StoMoveItemID,
                            Remark = GetRemark(i.Parent_docPosition, direction),
                            SpecialDelivery = !direction.XtraData.IsNull(Direction.XTRA_ARTICLE_MEDICINES, i.Parent_docPosition),
                            DocPosition = i.DocPosition

                        }).ToList();

                    // fill in other data
                    FillOtherData(result.Items);

                    // create list of valid items
                    result.DoneItems = stMove.Items
                        .Where(i => i.ItemDirection == StoreMoveItem.MI_DIRECTION_OUT && i.ItemValidity == StoreMoveItem.MI_VALIDITY_VALID)
                        .Select(i => new Entities.Helpers.ItemInfo()
                        {
                            StoMoveLotID = i.StoMoveLotID,
                            Quantity = i.Quantity,
                            CarrierNum = i.CarrierNum,
                            XtraString = Singleton<DAL.Cache.PositionCache>.Instance.GetItem(i.PositionID).PosCode,
                            XtraInt = i.StoMoveItemID,
                            StoMoveItemID = i.StoMoveItemID,
                            Remark = GetRemark(i.Parent_docPosition, direction),
                            SpecialDelivery = !direction.XtraData.IsNull(Direction.XTRA_ARTICLE_MEDICINES, i.Parent_docPosition),
                            DocPosition = i.DocPosition

                        }).ToList();

                    // fill in other data
                    FillOtherData(result.DoneItems);

                    if(result.DoneItems.Count > 0)
                    {
                        var destPositionID = (int) direction.XtraData[Direction.XTRA_DISPATCH_POSITION_ID];
                        result.DestPosition = Singleton<DAL.Cache.PositionCache>.Instance.GetItem(destPositionID);
                    }
                }

                // do use remarks ?
                result.UseRemarks = result.Items.Where(i => i.Remark != null).Any();

                // sort article list
                result.Items = result.Items
                    .OrderBy(i => i.SpecialDelivery)
                    .ThenBy(i => i.Remark ?? String.Empty)
                    .ThenBy(i => i.ArticleCode)
                    .ThenBy(i => i.WholeCarrier)
                    .ThenByDescending(i => i.Quantity)
                    .ToList();

                result.Success = true;
                return result;
            }
        }


        private string GetRemark(int parent_docPosition, DirectionModel direction)
        {
            if (parent_docPosition == StoreMoveItem.NO_PARENT_DOC_POSITION)
                return null;
            else
            {
                var dirItem = direction.Items.Where(di => di.DocPosition == parent_docPosition).SingleOrDefault();
                if (dirItem == null)
                    throw new Exception($"Parent direction item not found; parent_docPosition: {parent_docPosition}");
                else
                    return dirItem.PartnerDepart;
            }
        }

        private void FillOtherData(List<Entities.Helpers.ItemInfo> items)
        {
            var proxy = new EmptyDataProxy();
            foreach (var item in items)
            {
                var lot = Singleton<DAL.Cache.StoreMoveLotCache>.Instance.GetItem(item.StoMoveLotID);
                item.ArtPackID = lot.ArtPackID;
                item.BatchNum = lot.BatchNum;
                item.ExpirationDate = lot.ExpirationDate;

                proxy.ToShow(item.CarrierNum, StoreMoveItem.EMPTY_CARRIERNUM, value => item.CarrierNum = value);
                proxy.ToShow(lot.BatchNum, StoreMoveItem.EMPTY_BATCHNUM, value => item.BatchNum = value);
                proxy.ToShow(lot.ExpirationDate, StoreMoveItem.EMPTY_EXPIRATION, value => item.ExpirationDate = value);

                // article data
                DBHelpers.Articles.FillArticleData(item);

                // WholeCarrier - deprecated
                item.WholeCarrier = false;
                // WholeCarrier = TransHelpers.IsWholeCarr(moveItem, conn, null),
                item.XtraBool = item.WholeCarrier;
            }
        }
    }
}

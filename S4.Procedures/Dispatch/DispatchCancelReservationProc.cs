﻿using S4.Core;
using S4.Entities;
using S4.ProcedureModels.Dispatch;
using System;
using System.Collections.Generic;
using System.Text;


namespace S4.Procedures.Dispatch
{
    [ChangesDirectionStatus]
    public class DispatchCancelReservationProc : ProceduresBase<DispatchCancelReservation>
    {
        public DispatchCancelReservationProc(string userID) : base(userID)
        {
        }


        protected override DispatchCancelReservation DoIt(DispatchCancelReservation procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new DispatchCancelReservation() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //check
                var direction = (new DAL.DirectionDAL()).GetDirectionAllData(procModel.DirectionID);
                if (direction == null)
                    return new DispatchCancelReservation() { Success = false, ErrorText = $"Doklad nebyl nalezen; DirectionID: {procModel.DirectionID}" };

                // do tests
                if (direction.DocDirection != Direction.DOC_DIRECTION_OUT)
                    return new DispatchCancelReservation() { Success = false, ErrorText = $"Doklad nemá DocDirection DOC_DIRECTION_OUT; DirectionID: {procModel.DirectionID}" };

                if (direction.DocStatus != Direction.DR_STATUS_DISPATCH_PROC)
                    return new DispatchCancelReservation() { Success = false, ErrorText = $"Doklad nemá očekávaný status DR_STATUS_DISPATCH_PROC; DirectionID: {procModel.DirectionID}" };


                // make history
                var history = new DirectionHistory()
                {
                    DirectionID = procModel.DirectionID,
                    DocPosition = 0,
                    EventCode = Direction.DR_EVENT_RESERVATION_CANCELED,
                    EntryUserID = _userID,
                    EntryDateTime = DateTime.Now
                };


                using (var tr = db.GetTransaction())
                {
                    var save = Singleton<ServicesFactory>.Instance.GetSave();
                    var bookings = Singleton<ServicesFactory>.Instance.GetBookings();

                    // cancel all unsaved moves owned by direction  
                    if (!save.CancelByDirection(db, _userID, procModel.DirectionID, true, out string msg))
                        return new DispatchCancelReservation() { ErrorText = msg };

                    // cancel used bookings
                    bookings.BookingUseCanceled(db, procModel.DirectionID);

                    // change status
                    direction.DocStatus = Direction.DR_STATUS_DISPATCH_WAIT;
                    db.Save(direction);

                    //add history
                    db.Insert(history);

                    // do save
                    tr.Complete();
                }
            }

            return new DispatchCancelReservation() { Success = true };
        }
    }
}

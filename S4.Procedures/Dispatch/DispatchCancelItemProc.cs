﻿using S4.Core;
using S4.Entities;
using S4.ProcedureModels.Dispatch;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace S4.Procedures.Dispatch
{
    public class DispatchCancelItemProc : ProceduresBase<DispatchCancelItem>
    {
        public DispatchCancelItemProc(string userID) : base(userID)
        {
        }


        protected override DispatchCancelItem DoIt(DispatchCancelItem procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new DispatchCancelItem() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            // checks
            var storeMove = new DAL.StoreMoveDAL().GetStoreMoveAllData(procModel.StoMoveID);
            if (storeMove == null)
                return new DispatchCancelItem() { Success = false, ErrorText = $"StoreMove nebylo nalezeno; StoMoveID: {procModel.StoMoveID}" };

            if (storeMove.DocStatus != StoreMove.MO_STATUS_NEW_DISPATCH)
                return new DispatchCancelItem() { Success = false, ErrorText = $"Doklad nemá očekávaný status MO_STATUS_NEW_DISPATCH; StoMoveID: {procModel.StoMoveID}" };

            // get items
            var selected_item = storeMove.Items.Where(i => i.StoMoveItemID == procModel.StoMoveItemID).First();

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //cancel item
                using (var tr = db.GetTransaction())
                {
                    if (!new StoreCore.Save_Basic.Save().CancelMoveItem(db, _userID, procModel.StoMoveID, procModel.StoMoveItemID, out string msg))
                        return new DispatchCancelItem() { ErrorText = msg };

                    //save history
                    var hist = new StoreMoveHistory()
                    {
                        DocPosition = selected_item.DocPosition,
                        EntryDateTime = DateTime.Now,
                        EntryUserID = _userID,
                        EventCode = StoreMove.MO_STATUS_CANCEL,
                        StoMoveID = procModel.StoMoveID
                    };
                    db.Insert(hist);

                    tr.Complete();
                }
            }

            // log
            base.Info($"DispatchCancelItem; StoMoveItemID: {procModel.StoMoveItemID}");

            return new DispatchCancelItem() { Success = true };
        }
    }
}

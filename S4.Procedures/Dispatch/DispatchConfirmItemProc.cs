﻿using S4.Core;
using S4.Entities;
using S4.ProcedureModels.Dispatch;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace S4.Procedures.Dispatch
{
    public class DispatchConfirmItemProc : ProceduresBase<DispatchConfirmItem>
    {
        public DispatchConfirmItemProc(string userID) : base(userID)
        {
        }


        protected override DispatchConfirmItem DoIt(DispatchConfirmItem procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new DispatchConfirmItem() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            var storeMove = new DAL.StoreMoveDAL().GetStoreMoveAllData(procModel.StoMoveID);
            if (storeMove.DocStatus != StoreMove.MO_STATUS_NEW_DISPATCH)
                return new DispatchConfirmItem() { Success = false, ErrorText = $"Doklad nemá očekávaný status MO_STATUS_NEW_DISPATCH; StoMoveID: {procModel.StoMoveID}" };

            // get items
            var selected_item = storeMove.Items.Where(i => i.StoMoveItemID == procModel.StoMoveItemID).First();

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //validate move item 
                using (var tr = db.GetTransaction())
                {
                    if (!new StoreCore.Save_Basic.Save().ValidateMoveItem(db, _userID, procModel.StoMoveID, procModel.StoMoveItemID, null, out string msg))
                        return new DispatchConfirmItem() { ErrorText = msg };

                    //save history
                    var hist = new StoreMoveHistory()
                    {
                        DocPosition = selected_item.DocPosition,
                        EntryDateTime = DateTime.Now,
                        EntryUserID = _userID,
                        EventCode = StoreMove.MO_EVENT_ITEM_CHANGED,
                        StoMoveID = procModel.StoMoveID
                    };
                    db.Insert(hist);

                    tr.Complete();
                }
            }

            // log
            base.Info($"DispatchConfirmItem; StoMoveItemID: {procModel.StoMoveItemID}");

            return new DispatchConfirmItem() { Success = true };
        }
    }
}

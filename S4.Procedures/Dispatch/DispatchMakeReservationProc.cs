﻿using System;
using System.Collections.Generic;
using NPoco;
using S4.Entities;
using S4.ProcedureModels.Dispatch;
using System.Linq;
using S4.Core;
using S4.StoreCore.Interfaces;
using Newtonsoft.Json;
using S4.Picking;
using S4.Entities.Models;
using S4.Core.Utils;

namespace S4.Procedures.Dispatch
{
    [RunInQueue]
    [ChangesDirectionStatus]
    public class DispatchMakeReservationProc : ProceduresBase<DispatchMakeReservation>
    {
        public DispatchMakeReservationProc(string userID) : base(userID)
        {
        }


        protected override DispatchMakeReservation DoIt(DispatchMakeReservation procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new DispatchMakeReservation() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //check
                var direction = (new DAL.DirectionDAL()).GetDirectionAllData(procModel.DirectionID);
                if (direction == null)
                    return new DispatchMakeReservation() { Success = false, ErrorText = $"Doklad nebyl nalezen; DirectionID: {procModel.DirectionID}" };

                // do tests
                if (direction.DocDirection != Direction.DOC_DIRECTION_OUT)
                    return new DispatchMakeReservation() { Success = false, ErrorText = $"Doklad nemá DocDirection DOC_DIRECTION_OUT; DirectionID: {procModel.DirectionID}" };

                if (direction.DocStatus != Direction.DR_STATUS_RES_IN_PROC)
                    return new DispatchMakeReservation() { Success = false, ErrorText = $"Doklad nemá očekávaný status DR_STATUS_RES_IN_PROC; DirectionID: {procModel.DirectionID}" };

                // set dispatch section 
                if (direction.XtraData.IsNull(Direction.XTRA_DISPATCH_POSITION_ID, 0))
                    return new DispatchMakeReservation() { Success = false, ErrorText = "Direction objekt nemá XTRA_DISPATCH_POSITION_ID hodnotu" };

                var procResult = new DispatchMakeReservation();

                var onStore = Singleton<ServicesFactory>.Instance.GetOnStore();
                var save = Singleton<ServicesFactory>.Instance.GetSave();
                var bookings = Singleton<ServicesFactory>.Instance.GetBookings();


                // cancel all unsaved moves owned by direction                    
                if (!save.CancelByDirection(db, _userID, procModel.DirectionID, true, out string msg))
                    return new DispatchMakeReservation() { ErrorText = msg };

                using (var tr = db.GetTransaction())
                {
                    // get destination positionID
                    var destPositionID = (int)direction.XtraData[Direction.XTRA_DISPATCH_POSITION_ID];

                    // get current fullfilment of direction & make new items - "what is realy needed"
                    direction.Items = UpdateDirectionItems(save, db, direction);

                    // run planMaker
                    var prefix = Singleton<DAL.Cache.PrefixCache>.Instance.GetItem(Entities.Direction.DOC_CLASS, direction.DocNumPrefix);
                    var prefixSettings = (prefix != null) ? prefix.PrefixSettings : Entities.Direction.DefaultPrefixSettings;
                    var planMaker = new Picking.PlanMaker(Singleton<DAL.Cache.ArticlePackingCache>.Instance, Singleton<DAL.Cache.ArticleModelCache>.Instance, Singleton<DAL.Cache.ManufactCache>.Instance, Singleton<DAL.Cache.SectionModelCache>.Instance, onStore, bookings);
                    var planResult = planMaker.CreatePlan(direction, prefixSettings, DateTime.Today, prefix.PrefixSection);
                    if (planResult.Result)
                    {
                        // success

                        procResult.IsPrepared = true;

                        // update status
                        direction.DocStatus = Direction.DR_STATUS_DISPATCH_PROC;

                        // save moves
                        var saveItems = planResult.ResultItems.Select(i => new MoveItem()
                        {
                            ArtPackID = i.ArtPackID,
                            CarrierNum = i.CarrierNum,
                            Quantity = i.Quantity,
                            BatchNum = i.BatchNum,
                            ExpirationDate = i.ExpirationDate,
                            Parent_docPosition = i.DocPosition,

                            SourcePositionID = i.PositionID,
                            DestinationPositionID = destPositionID
                        }).ToList();

                        var stoMoveID = save.Move(db, _userID, StoreMove.MO_PREFIX_DISPATCH, StoreMove.MO_DOCTYPE_DISPATCH, saveItems, false, sm =>
                        {
                            sm.DocStatus = StoreMove.MO_STATUS_NEW_DISPATCH;
                            sm.DocType = StoreMove.MO_DOCTYPE_DISPATCH;
                            sm.Parent_directionID = direction.DirectionID;
                        },
                        null,
                        out string message);
                        if (stoMoveID == -1)
                            return new DispatchMakeReservation() { ErrorText = msg };

                        procResult.StoMoveID = stoMoveID;
                        direction.XtraData[Direction.XTRA_DISPATCH_STORE_MOVE_ID] = stoMoveID;

                        // update bookings
                        foreach (var booking in planResult.UsedBookings)
                            bookings.BookingUsed(db, booking.BookingID, booking.UsedQuantity, direction.DirectionID);

                        // check useRemarks
                        procResult.UseRemarks = direction.Items.Exists(i => !String.IsNullOrWhiteSpace(i.PartnerDepart));

                    }
                    else
                    {
                        // not found

                        procResult.IsPrepared = false;
                        procResult.NotFoundInfo = MakeNotFoundInfo(planResult);

                        // update status
                        direction.DocStatus = Direction.DR_STATUS_RES_NOT_FOUND;

                        // make info
                        var message = new InternMessage()
                        {
                            MsgClass = InternMessage.DR_RESERVATION_NOT_FOUND,
                            MsgSeverity = InternMessage.MessageSeverityEnum.Error,
                            MsgHeader = $"Doklad není možné vyskladnit '{direction.DocNumPrefix}/{direction.DocYear}/{direction.DocNumber}'",
                            MsgDateTime = DateTime.Now,
                            MsgUserID = _userID,
                            MsgText = JsonUtils.ToJsonString("directionID", procModel.DirectionID)
                        };
                        db.Insert(message);

                        // pars.moveRemarks = CreateMoveRemarks(pars.notFound);
                    }


                    // get and save dispatch log
                    var historyJson = JsonConvert.SerializeObject(planResult.History, Formatting.Indented);
                    procModel.DispatchLogText = historyJson;

                    // make history
                    var history = new DirectionHistory()
                    {
                        DirectionID = procModel.DirectionID,
                        DocPosition = 0,
                        EventCode = (procResult.IsPrepared) ? Direction.DR_EVENT_RESERVATION_DONE : Direction.DR_EVENT_RESERVATION_NOT_FOUND,
                        EventData = historyJson,
                        EntryUserID = _userID,
                        EntryDateTime = DateTime.Now
                    };

                    // save history
                    db.Insert(history);

                    // update direction
                    db.Update(direction);
                    direction.XtraData.Save(db, direction.DirectionID);

                    tr.Complete();
                }


                // send DirectionAssigmentUpdated message
                var directionAssign = db.Query<DirectionAssign>().Where(_ => _.DirectionID == procModel.DirectionID && _.JobID == DirectionAssign.JOB_ID_DISPATCH).FirstOrDefault();
                if (directionAssign != null)
                {
                    var messageRouter = Core.Singleton<ServicesFactory>.Instance.GetMessageRouter();
                    messageRouter.Publish(new Messages.DirectionAssigmentUpdated() { DirectionID = procModel.DirectionID, WorkerID = directionAssign.WorkerID });
                }

                procResult.Success = true;
                return procResult;
            }
        }



        public override void InCaseOfError(DispatchMakeReservation procModel)
        {
            // reset error
            using (var db = Core.Data.ConnectionHelper.GetDB())
            using (var tr = db.GetTransaction())
            {
                var direction = db.SingleOrDefaultById<DirectionModel>(procModel.DirectionID);

                // update status
                direction.DocStatus = Direction.DR_STATUS_RES_NOT_FOUND;

                // make history
                var history = new DirectionHistory()
                {
                    DirectionID = procModel.DirectionID,
                    DocPosition = 0,
                    EventCode = Direction.DR_EVENT_RESERVATION_ERROR,
                    EntryUserID = _userID,
                    EntryDateTime = DateTime.Now
                };

                // save history
                db.Insert(history);

                // update direction
                db.Update(direction);

                tr.Complete();
            }

            var messageRouter = Core.Singleton<ServicesFactory>.Instance.GetMessageRouter();
            messageRouter.Publish(new Messages.DirectionStatusUpdated() { DirectionID = procModel.DirectionID });
        }


        private List<DirectionItem> UpdateDirectionItems(ISave save, IDatabase db, DirectionModel direction)
        {
            var dispatchPositionsModel = Singleton<DAL.Cache.PositionByCategoryModelCache>.Instance.GetItem(Entities.Position.PositionCategoryEnum.Dispatch);
            var moveResult = save.MoveResult_ByDirection(db, direction.DirectionID, StoreMove.MO_DOCTYPE_DISPATCH, (mi) => dispatchPositionsModel.PositionIDs.Contains(mi.PositionID));

            // calc sums
            var currentlyDoneSum = moveResult
                .GroupBy(k => k.DocPosition, (k, e) => new { DocPosition = k, Quantity = e.Sum(i => i.Quantity) });

            // update directions
            direction.Items.ForEach(di => {
                di.Quantity = di.Quantity - currentlyDoneSum.Where(sum => sum.DocPosition == di.DocPosition).Select(sum => sum.Quantity).SingleOrDefault();
            });

            return direction.Items.Where(di => di.Quantity > 0).ToList();
        }


        private string MakeNotFoundInfo(PickingResult planResult)
        {
            // group by artPackID
            var grouped = planResult.NotFound.GroupBy(k => k.ArtPackID).Select((k, i) => k.First());
            return String.Join("; ", grouped.Select(i => FormattableString.Invariant($"{i.ArticleInfo} - {i.Quantity}")).ToArray());
        }
    }
}

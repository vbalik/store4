﻿using System;
using System.Collections.Generic;
using System.Linq;
using S4.Core;
using S4.DAL;
using S4.Entities;
using S4.Entities.Helpers;
using S4.ProcedureModels.Dispatch;

namespace S4.Procedures.Dispatch
{
    public class DispatchAvailableDirectionsDocNumberProc : ProceduresBase<DispatchAvailableDirectionsDocNumber>
    {
        public DispatchAvailableDirectionsDocNumberProc(string userID) : base(userID)
        {
        }


        protected override DispatchAvailableDirectionsDocNumber DoIt(DispatchAvailableDirectionsDocNumber procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new DispatchAvailableDirectionsDocNumber() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            var documentInfos = new List<DocumentInfo>();

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // 1) find by abraDocID
                var abraDocList = new DirectionDAL().GetDirectionsByAbraDocID(procModel.DocumentID);

                documentInfos.AddRange(abraDocList.Where(_ => _.DocDirection == Direction.DOC_DIRECTION_OUT
                    && _.DocStatus == Direction.DR_STATUS_LOAD
                    || _.DocStatus == Direction.DR_STATUS_DISPATCH_WAIT)
                        .Select(_ => new DocumentInfo
                        {
                            DocumentID = _.DirectionID,
                            DocumentName = _.DocInfo,
                            DocumentDetail = Singleton<DAL.Cache.PartnerCache>.Instance.GetItem(_.PartnerID)?.PartnerName
                        })
                        .ToList());

                if (documentInfos.Any())
                    return new DispatchAvailableDirectionsDocNumber { Success = true, Directions = documentInfos };


                // 2) find by docInfo
                if (procModel.DocumentID.Contains("/"))
                {
                    var docInfoList = new DirectionDAL().FindDirectionsByDocInfo(procModel.DocumentID, null);

                    documentInfos.AddRange(docInfoList.Where(_ => _.DocDirection == Direction.DOC_DIRECTION_OUT
                        && _.DocStatus == Direction.DR_STATUS_LOAD
                        || _.DocStatus == Direction.DR_STATUS_DISPATCH_WAIT)
                            .Select(_ => new DocumentInfo
                            {
                                DocumentID = _.DirectionID,
                                DocumentName = _.DocInfo,
                                DocumentDetail = Singleton<DAL.Cache.PartnerCache>.Instance.GetItem(_.PartnerID)?.PartnerName
                            })
                            .ToList());

                    if (documentInfos.Any())
                        return new DispatchAvailableDirectionsDocNumber { Success = true, Directions = documentInfos };
                }

                // 3) find by invoice number
                var invoiceList = new DirectionDAL().GetDirectionByInvoiceNumber(procModel.DocumentID);

                documentInfos.AddRange(invoiceList.Where(_ => _.DocDirection == Direction.DOC_DIRECTION_OUT
                    && _.DocStatus == Direction.DR_STATUS_LOAD
                    || _.DocStatus == Direction.DR_STATUS_DISPATCH_WAIT)
                        .Select(_ => new DocumentInfo
                        {
                            DocumentID = _.DirectionID,
                            DocumentName = _.DocInfo,
                            DocumentDetail = Singleton<DAL.Cache.PartnerCache>.Instance.GetItem(_.PartnerID)?.PartnerName
                        })
                        .ToList());

                return new DispatchAvailableDirectionsDocNumber { Success = true, Directions = documentInfos };
            }
        }
    }
}

﻿using S4.Core;
using S4.Core.Data;
using S4.Entities;
using S4.ProcedureModels.Dispatch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace S4.Procedures.Dispatch
{
    public class DispatchItemInstructProc : ProceduresBase<DispatchItemInstruct>
    {

        public DispatchItemInstructProc(string userID) : base(userID)
        {
        }


        protected override DispatchItemInstruct DoIt(DispatchItemInstruct procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new DispatchItemInstruct() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {

                // get storeMove
                var stMove = (new DAL.StoreMoveDAL()).GetStoreMoveAllData(procModel.StoMoveID);
                if (stMove == null)
                    return new DispatchItemInstruct() { Success = false, ErrorText = $"StoreMove nebylo nalezeno; StoMoveID: {procModel.StoMoveID}" };


                // do tests
                if (stMove.DocStatus != StoreMove.MO_STATUS_NEW_DISPATCH)
                {
                    return new DispatchItemInstruct() { Success = false, ErrorText = $"Doklad nemá očekávaný status MO_STATUS_NEW_DISPATCH; StoMoveID: {procModel.StoMoveID}" };
                }
                else
                {
                    // get data
                    var selected_item = stMove.Items.Single(i => i.StoMoveItemID == procModel.StoMoveItemID);
                    var brother_item = stMove.Items.Single(i => i.StoMoveItemID == selected_item.BrotherID);

                    // return data
                    var result = new DispatchItemInstruct();

                    // positions
                    result.ScrPosition = Singleton<DAL.Cache.PositionCache>.Instance.GetItem(selected_item.PositionID);
                    result.DestPosition = Singleton<DAL.Cache.PositionCache>.Instance.GetItem(brother_item.PositionID);

                    // setup result
                    var proxy = new EmptyDataProxy();
                    result.Item = new Entities.Helpers.ItemInfo();
                    result.Item.Quantity = selected_item.Quantity;
                    result.Item.CarrierNum = selected_item.CarrierNum;
                    proxy.ToShow(selected_item.CarrierNum, StoreMoveItem.EMPTY_CARRIERNUM, value => result.Item.CarrierNum = value);

                    // get lot
                    var lot = Singleton<DAL.Cache.StoreMoveLotCache>.Instance.GetItem(selected_item.StoMoveLotID);
                    result.Item.ArtPackID = lot.ArtPackID;
                    result.Item.BatchNum = lot.BatchNum;
                    result.Item.ExpirationDate = lot.ExpirationDate;
                    proxy.ToShow(lot.BatchNum, StoreMoveItem.EMPTY_BATCHNUM, value => result.Item.BatchNum = value);
                    proxy.ToShow(lot.ExpirationDate, StoreMoveItem.EMPTY_EXPIRATION, value => result.Item.ExpirationDate = value);

                    // article
                    DBHelpers.Articles.FillArticleData(result.Item);

                    result.Success = true;
                    return result;
                }
            }
        }
    }
}

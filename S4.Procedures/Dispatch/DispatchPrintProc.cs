﻿using System;
using System.Collections.Generic;
using System.Text;
using NPoco;
using S4.Core;
using S4.Entities;
using S4.ProcedureModels.Dispatch;
using S4.Report.Model.Dispatch;
using S4.StoreCore.Interfaces;
using System.Linq;
using S4.Entities.Helpers.Xml.Dispatch;
using S4.DAL;
using System.Drawing;
using QRCoder;
using S4.SharedLib;
using S4.Core.ADR;
using S4.DAL.Cache;

namespace S4.Procedures.Dispatch
{
    public class DispatchPrintProc : ProceduresBase<DispatchPrint>
    {
        public DispatchPrintProc(string userID) : base(userID)
        {
        }


        protected override DispatchPrint DoIt(DispatchPrint procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new DispatchPrint() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                if (procModel.ReportID == 0)
                    procModel.ReportID = Singleton<DAL.SettingsSet>.Instance.GetValue_Int32(Entities.SettingsDictionary.DISPATCH_REPORT_ID);

                // get direction
                var direction = (new DAL.DirectionDAL()).GetDirectionAllData(procModel.DirectionID);
                if (direction == null)
                    return new DispatchPrint() { Success = false, ErrorText = $"Doklad nebyl nalezen; DirectionID: {procModel.DirectionID}" };

                var save = Singleton<ServicesFactory>.Instance.GetSave();
                // get destination positionID
                List<MoveResult> moveResult = new List<MoveResult>();

                var dispatchPositionsModel = Singleton<DAL.Cache.PositionByCategoryModelCache>.Instance.GetItem(Entities.Position.PositionCategoryEnum.Dispatch);
                moveResult = save.MoveResult_ByDirection(db, direction.DirectionID, StoreMove.MO_DOCTYPE_DISPATCH, (mi) => dispatchPositionsModel.PositionIDs.Contains(mi.PositionID));

                //make data to report
                var directionItemsGroupByPartner = from d in direction.Items
                                                   group d by d.PartnerDepart into g
                                                   select new { PartnerDepart = g.Key, DirectionItems = g.ToList() };

                var toReport = new List<DispatchReportModel>();
                var groupNum = 0;
                foreach (var directionItemGroupByPartner in directionItemsGroupByPartner)
                {
                    var row1 = new DispatchReportModel();
                    row1.GroupNum = groupNum;
                    row1.Col1 = directionItemGroupByPartner.PartnerDepart;
                    row1.Head = true;
                    toReport.Add(row1);

                    foreach (var item in directionItemGroupByPartner.DirectionItems.OrderBy(x => x.ArticleCode))
                    {
                        var articlePacking = Singleton<DAL.Cache.ArticlePackingCache>.Instance.GetItem(item.ArtPackID);
                        var article = Singleton<DAL.Cache.ArticleModelCache>.Instance.GetItem(articlePacking.ArticleID);
                        var row2 = new DispatchReportModel();
                        row2.GroupNum = groupNum;
                        var space = item.ArticleCode.Trim().Length > 25 ? 1 : 25 - item.ArticleCode.Trim().Length;
                        row2.Col1 = $"{item.ArticleCode.Trim()}{new string(' ', space)}{article.ArticleDesc.Trim().Substring(0, article.ArticleDesc.Trim().Length <= 40 ? article.ArticleDesc.Trim().Length : 40)}";
                        row2.Col3 = string.Format("{0:0}", item.Quantity);
                        row2.Col4 = articlePacking.PackDesc;
                        row2.Col5 = (string)direction.XtraData[Direction.XTRA_DELIVERY_RISK_CLASS, item.DocPosition];
                        row2.Col6 = (string)direction.XtraData[Direction.XTRA_DELIVERY_UDIDI, item.DocPosition];
                        toReport.Add(row2);

                        //expirace, šarže 
                        var mr = (from m in moveResult where m.DocPosition == item.DocPosition select m).GroupBy(m => new { m.ArtPackID, m.ExpirationDate, m.BatchNum }, (k, i) => new { k, Sum = i.Sum(x => x.Quantity) });
                        foreach (var mrItem in mr)
                        {
                            var row3 = new DispatchReportModel();
                            row3.GroupNum = groupNum;
                            var exp = mrItem == null || !mrItem.k.ExpirationDate.HasValue ? "" : mrItem.k.ExpirationDate.Value.Date.ToString("dd.MM.yy");
                            var batchNum = mrItem == null || mrItem.k.BatchNum == null ? "" : mrItem.k.BatchNum.ToString();
                            row3.Col1 = $"{new string(' ', 5)}{exp}{new string(' ', 15)}{batchNum}";
                            if (!string.IsNullOrWhiteSpace(exp) || !string.IsNullOrWhiteSpace(batchNum))
                            {
                                row3.Col3 = string.Format("{0:0}", mrItem.Sum);
                                toReport.Add(row3);
                            }
                        }
                    }
                    groupNum++;
                }

                // partner & position
                var partner = Singleton<DAL.Cache.PartnerCache>.Instance.GetItem(direction.PartnerID);
                if (direction.XtraData.IsNull(Direction.XTRA_DISPATCH_POSITION_ID, 0))
                    return new DispatchPrint() { Success = false, ErrorText = $"XTRA_DISPATCH_POSITION_ID nebylo nalezeno; DirectionID: {procModel.DirectionID}" };
                int destPositionID = (int)direction.XtraData[Direction.XTRA_DISPATCH_POSITION_ID];
                var position = Singleton<DAL.Cache.PositionCache>.Instance.GetItem(destPositionID);

                //P R I N T
                //get report
                var report = db.SingleOrDefault<Reports>("where reportID = @0", procModel.ReportID);
                if (report == null)
                    return new DispatchPrint() { Success = false, ErrorText = $"Report nebyl nalezen; reportID: {procModel.ReportID}" };

                // get printer location
                PrinterLocation printerLocation = null;
                if (procModel.PrinterByPosition)
                {
                    // choose printer - by PrinterSettings.DispatchPositionCodes
                    var printerType = Singleton<DAL.SettingsSet>.Instance.GetValue_Int32(Entities.SettingsDictionary.PRINTER_TYPE_LASERJET);
                    var printers = db.Query<PrinterLocation>().Where(pl => pl.PrinterTypeID == printerType).ToList();

                    if (!procModel.LocalPrint)
                    {
                        printerLocation = printers.Where(pl => pl.PrinterSettings.DispatchPositionCodes.Contains(position.PosCode)).FirstOrDefault();
                        if (printerLocation == null)
                        {
                            var defaultPrinter = Singleton<DAL.SettingsSet>.Instance.GetValue_Int32(Entities.SettingsDictionary.DISPATCH_REPORT_DEFAULT_PRINTER_ID);
                            printerLocation = Singleton<DAL.Cache.PrinterLocationCache>.Instance.GetItem(defaultPrinter);
                        }
                    }
                }
                else
                    printerLocation = Singleton<DAL.Cache.PrinterLocationCache>.Instance.GetItem(procModel.PrinterLocationID);

                if (printerLocation == null && !procModel.LocalPrint)
                    return new DispatchPrint() { Success = false, ErrorText = $"PrinterLocation nebyl nalezen; printerLocationID: {procModel.PrinterLocationID}" };

                //variables
                var variables = new List<Tuple<string, object, Type>>
                {
                    new Tuple<string, object, Type>("DocInfo", $"{direction.DocNumPrefix}/{direction.DocYear}/{direction.DocNumber}", typeof(string)),
                    new Tuple<string, object, Type>("DocDate", direction.DocDate, typeof(DateTime)),
                    new Tuple<string, object, Type>("PartnerName", partner.PartnerName, typeof(string)),
                    new Tuple<string, object, Type>("PartnerAddr_1", partner.PartnerAddr_1, typeof(string)),
                    new Tuple<string, object, Type>("PartnerCity", partner.PartnerCity, typeof(string)),
                    new Tuple<string, object, Type>("PartnerAddr_2", partner.PartnerAddr_2, typeof(string)),
                    new Tuple<string, object, Type>("PartnerPostCode", partner.PartnerPostCode, typeof(string)),
                    new Tuple<string, object, Type>("PartnerPostPhone", partner.PartnerPhone, typeof(string)),
                    new Tuple<string, object, Type>("PartnerRef", direction.PartnerRef, typeof(string)),
                    new Tuple<string, object, Type>("QRCodeDLNum", GetQRCode(direction), typeof(Image))
                };

                //Invoice Number
                var invoceNum = "";
                if (!direction.XtraData.IsNull(Direction.XTRA_INVOICE_NUMBER, 0))
                    invoceNum = (string)direction.XtraData[Direction.XTRA_INVOICE_NUMBER];

                //Payment Type
                var paymentType = "";
                if (!direction.XtraData.IsNull(Direction.XTRA_PAYMENT_TYPE, 0))
                    paymentType = (string)direction.XtraData[Direction.XTRA_PAYMENT_TYPE];

                //Transportation Type
                var transportationType = "";
                if (!direction.XtraData.IsNull(Direction.XTRA_TRANSPORTATION_TYPE, 0))
                    transportationType = (string)direction.XtraData[Direction.XTRA_TRANSPORTATION_TYPE];

                //Box count
                var boxCount = 0;
                if (!direction.XtraData.IsNull(Direction.XTRA_DISPATCH_BOXES_CNT, 0))
                    boxCount = (int)direction.XtraData[Direction.XTRA_DISPATCH_BOXES_CNT];

                //Extnumber
                var extnumber = "";
                if (!direction.XtraData.IsNull(Direction.XTRA_EXTNUMBER, 0))
                    extnumber = (string)direction.XtraData[Direction.XTRA_EXTNUMBER];

                //Variables
                variables.Add(new Tuple<string, object, Type>("InvoiceNumber", invoceNum == null ? "" : invoceNum, typeof(string)));
                variables.Add(new Tuple<string, object, Type>("PaymentType", paymentType == null ? "" : paymentType, typeof(string)));
                variables.Add(new Tuple<string, object, Type>("TransportationType", transportationType == null ? "" : transportationType, typeof(string)));
                variables.Add(new Tuple<string, object, Type>("BoxCount", boxCount.ToString().Equals("0") ? "" : boxCount.ToString(), typeof(string)));
                variables.Add(new Tuple<string, object, Type>("ExtNumber", extnumber == null ? "" : extnumber, typeof(string)));

                //zkontroloval, vystavil
                var dche_UserID = (from d in direction.History
                                   where (d.EventCode == Direction.DR_STATUS_DISP_CHECK_DONE)
                                   select d).OrderBy(u => u.EntryDateTime).Select(u => u.EntryUserID).LastOrDefault();

                var dispSaved_UserID = (from d in direction.History
                                        where (d.EventCode == Direction.DR_STATUS_DISPATCH_SAVED)
                                        select d).OrderBy(u => u.EntryDateTime).Select(u => u.EntryUserID).LastOrDefault();

                var dal = new WorkerDAL();
                var dche_User = "";
                var dispSaved_User = "";
                if (!string.IsNullOrWhiteSpace(dche_UserID))
                    dche_User = dal.Get(dche_UserID).WorkerName;

                if (!string.IsNullOrWhiteSpace(dispSaved_UserID))
                    dispSaved_User = dal.Get(dispSaved_UserID).WorkerName;

                variables.Add(new Tuple<string, object, Type>("DcheUser", dche_User, typeof(string)));
                variables.Add(new Tuple<string, object, Type>("DispSavedUser", dispSaved_User, typeof(string)));

                //delivery address
                var history = (from d in direction.History where d.EventCode == Direction.DR_STATUS_LOAD select d).OrderBy(d => d.EntryDateTime).LastOrDefault();

                if (history != null)
                {
                    DispatchS3 di = null;
                    var load = DispatchS3.Serializer.Deserialize(history.EventData, out di);

                    if (load && di.DeliveryAddress != null)
                    {
                        variables.Add(new Tuple<string, object, Type>("DeliveryAddressAdress1", di.DeliveryAddress.Adress1 == null ? "" : di.DeliveryAddress.Adress1, typeof(string)));
                        variables.Add(new Tuple<string, object, Type>("DeliveryAddressAdress2", di.DeliveryAddress.Adress2 == null ? "" : di.DeliveryAddress.Adress2, typeof(string)));
                        variables.Add(new Tuple<string, object, Type>("DeliveryAddressCity", di.DeliveryAddress.City == null ? "" : di.DeliveryAddress.City, typeof(string)));
                        variables.Add(new Tuple<string, object, Type>("DeliveryAddressDeliveryInstructions", di.DeliveryAddress.Deliveryinstructions == null ? "" : di.DeliveryAddress.Deliveryinstructions, typeof(string)));
                        variables.Add(new Tuple<string, object, Type>("DeliveryAddressName", di.DeliveryAddress.Name == null ? "" : di.DeliveryAddress.Name, typeof(string)));
                        variables.Add(new Tuple<string, object, Type>("DeliveryAddressPhone", di.DeliveryAddress.Phone == null ? "" : di.DeliveryAddress.Phone, typeof(string)));
                        variables.Add(new Tuple<string, object, Type>("DeliveryAddressRemark", di.DeliveryAddress.Remark == null ? "" : di.DeliveryAddress.Remark, typeof(string)));
                    }
                }

                //Přepravní doklad ADR
                var toADRReport = new List<DocumentADRModel>();
              
                foreach (var item in direction.Items)
                {
                    var articlePacking = Singleton<ArticlePackingCache>.Instance.GetItem(item.ArtPackID);
                    var article = Singleton<ArticleModelCache>.Instance.GetItem(articlePacking.ArticleID);

                    // xtra
                    article.XtraData.Load(db, new Sql().Where("articleID = @0", article.ArticleID));

                    if (article.XtraData != null)
                    {
                        var UNKod = !article.XtraData.IsNull(Article.XTRA_UN_KOD) ? article.XtraData[Article.XTRA_UN_KOD].ToString() : string.Empty;
                        var nazev = !article.XtraData.IsNull(Article.XTRA_OFICIALNI_NAZEV) ? article.XtraData[Article.XTRA_OFICIALNI_NAZEV].ToString() : string.Empty;
                        var znacka = !article.XtraData.IsNull(Article.XTRA_BEZPECNOSTI_ZNACKA) ? article.XtraData[Article.XTRA_BEZPECNOSTI_ZNACKA].ToString() : string.Empty;
                        var obalSkupina = !article.XtraData.IsNull(Article.XTRA_OBALOVA_SKUPINA) ? article.XtraData[Article.XTRA_OBALOVA_SKUPINA].ToString() : string.Empty;
                        var kodTunelu = !article.XtraData.IsNull(Article.XTRA_KOD_TUNELU) ? article.XtraData[Article.XTRA_KOD_TUNELU].ToString() : string.Empty;
                        var druhObalu = !article.XtraData.IsNull(Article.XTRA_DRUH_OBALU) ? article.XtraData[Article.XTRA_DRUH_OBALU].ToString() : string.Empty;

                        var ohrozujeProstredi = false;
                        if (!article.XtraData.IsNull(Article.XTRA_OHROZUJE_PROSTREDI))
                            bool.TryParse(article.XtraData[Article.XTRA_OHROZUJE_PROSTREDI].ToString(), out ohrozujeProstredi);

                        int? prepravKategorie = null;
                        if (!article.XtraData.IsNull(Article.XTRA_PREPRAVNI_KATEGORIE) && int.TryParse(article.XtraData[Article.XTRA_PREPRAVNI_KATEGORIE].ToString(), out int pk))
                            prepravKategorie = pk;

                        var mnozstvi = Math.Abs(item.Quantity);

                        int? pocetBodu = null;
                        if (!article.XtraData.IsNull(Article.XTRA_OBJEM_KOEF) && prepravKategorie.HasValue)
                        {
                            if (float.TryParse(article.XtraData[Article.XTRA_OBJEM_KOEF].ToString(), out float objemKoef))
                            {
                                pocetBodu = ADR.SafetyPointCalculate(prepravKategorie.Value, (int)mnozstvi, objemKoef);
                            }
                        }

                        if (!string.IsNullOrEmpty(UNKod) || !string.IsNullOrEmpty(nazev))
                        {
                            var row = new DocumentADRModel
                            {
                                UNKod = UNKod,
                                Nazev = nazev,
                                Znacka = znacka,
                                ObalSkupina = obalSkupina,
                                KodTunelu = kodTunelu,
                                DruhObalu = druhObalu,
                                Mnozstvi = mnozstvi,
                                PrepravKategorie = prepravKategorie,
                                PocetBodu = pocetBodu,
                                NazevKarty = article.ArticleDesc,
                                OhrozujeZivProstredi = ohrozujeProstredi ? "Ano" : "Ne",
                            };

                            toADRReport.Add(row);
                        }
                    }
                };
                                
                //do print
                Report.Print print = new Report.Print();
                print.Variables = variables;
                print.ReportDefinition = report.Template; //šablona z DB
                //print.DesignMode = true; //pouze pro úpravy šablony
                //print.ReportDefFilePath = @"c:\temp\DodaciList.srt"; //pouze pro úpravy šablony

                byte[] fileData = null;
                if (IsTesting)
                {
                    fileData = print.DoPrint2Text(toReport);
                }
                else
                {
                    if (procModel.LocalPrint)
                        fileData = print.DoPrint2PDF(toReport, toADRReport);
                    else
                        print.DoPrint(printerLocation.PrinterPath, toReport, toADRReport);
                }

                if (print.IsError)
                    return new DispatchPrint() { Success = false, ErrorText = print.ErrorText };
                else
                    return new DispatchPrint() { Success = true, LocalPrint = procModel.LocalPrint, FileData = fileData };
            }


        }

        private Image GetQRCode(Direction direction)
        {
            var qr = new DocQRCode()
            {
                DocType = DocQRCode.DocTypeEnum.DL,
                DocID = direction.DirectionID.ToString(),
                DocInfo = $"{direction.DocNumPrefix}/{direction.DocYear}/{direction.DocNumber}"
            };

            //QR Code using
            using (QRCodeGenerator qrGenerator = new QRCodeGenerator())
            {
                QRCodeData qrCodeData = qrGenerator.CreateQrCode(qr.Formated, QRCodeGenerator.ECCLevel.H);
                QRCode qrCode = new QRCode(qrCodeData);
                Bitmap qrCodeImage = qrCode.GetGraphic(100, Color.Black, Color.White, false);
                return qrCodeImage;
            }
        }
    }
    
}

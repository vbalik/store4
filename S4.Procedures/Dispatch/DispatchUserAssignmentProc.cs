﻿using S4.Core;
using S4.Entities;
using S4.Entities.Models;
using S4.ProcedureModels.Dispatch;
using System;
using System.Collections.Generic;
using System.Linq;

namespace S4.Procedures.Dispatch
{
    /// DEPRECATED 28.12 v1.46
    public class DispatchUserAssignmentProc : ProceduresBase<DispatchUserAssignment>
    {
        public DispatchUserAssignmentProc(string userID) : base(userID)
        {
        }

        protected override DispatchUserAssignment DoIt(DispatchUserAssignment procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new DispatchUserAssignment { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var directionsList = new List<DirectionModel>();

                // try Direction
                directionsList = (new DAL.DirectionDAL()).FindDirectionsByDocInfo(procModel.DocInfo, null);

                if (directionsList.Count == 0)
                {
                    // try invoice
                    directionsList = (new DAL.DirectionDAL()).GetDirectionsByAbraDocID(procModel.DocInfo)
                        .Where(i => i.DocStatus.Equals(Direction.DR_STATUS_LOAD) || i.DocStatus.Equals(Direction.DR_STATUS_DISPATCH_WAIT) || i.DocStatus.Equals(Direction.DR_STATUS_RES_NOT_FOUND)).ToList();
                }
                
                if (directionsList.Count < 1)
                    return new DispatchUserAssignment { Success = false, ErrorText = "Nebyly nalezeny žádné doklady ke zpracování!" };

                // set messageRouter
                var messageRouter = Core.Singleton<ServicesFactory>.Instance.GetMessageRouter();

                //Read DestPositionID
                var destPositionID = Singleton<DAL.SettingsSet>.Instance.GetValue_Int32(SettingsDictionary.DISPATCH_DEFAULT_POSITION_ID);

                // trans
                using (var tr = db.GetTransaction())
                {
                    foreach (var direction in directionsList)
                    {
                        var directionAllData = (new DAL.DirectionDAL()).GetDirectionAllData(direction.DirectionID);
                        // do tests
                        if (directionAllData.DocDirection != Direction.DOC_DIRECTION_OUT)
                            return new DispatchUserAssignment { Success = false, ErrorText = $"Doklad nemá DocDirection DOC_DIRECTION_OUT; DirectionID: {directionAllData.DirectionID}" };

                        if (directionAllData.DocStatus != Direction.DR_STATUS_LOAD && directionAllData.DocStatus != Direction.DR_STATUS_DISPATCH_WAIT && directionAllData.DocStatus != Direction.DR_STATUS_RES_NOT_FOUND)
                            return new DispatchUserAssignment { Success = false, ErrorText = $"Doklad nemá očekávaný status DR_STATUS_LOAD nebo DR_STATUS_DISPATCH_WAIT nebo DR_STATUS_RES_NOT_FOUND; DirectionID: {directionAllData.DirectionID}" };

                        // change status
                        directionAllData.DocStatus = Direction.DR_STATUS_RES_IN_PROC;

                        // check XTRA_DISPATCH_POSITION_ID change
                        if (!directionAllData.XtraData.IsNull(Direction.XTRA_DISPATCH_POSITION_ID))
                        {
                            int originalDestPositionID = (int)directionAllData.XtraData[Direction.XTRA_DISPATCH_POSITION_ID];
                            if (originalDestPositionID != destPositionID)
                            {
                                var save = Singleton<ServicesFactory>.Instance.GetSave();
                                var moveResult = save.MoveResult_ByDirection(db, directionAllData.DirectionID, StoreMove.MO_DOCTYPE_DISPATCH, (mi) => mi.PositionID == originalDestPositionID);

                                if (moveResult.Any())
                                {
                                    var pos = Singleton<DAL.Cache.PositionCache>.Instance.GetItem(originalDestPositionID);
                                    return new DispatchUserAssignment() { Success = false, ErrorText = $"Změna pozice vyskladnění není možná - na pozici {pos.PosCode} už jsou nějaké pohyby; DirectionID: {directionAllData.DirectionID}" };
                                }
                            }
                        }

                        // set position
                        directionAllData.XtraData[Direction.XTRA_DISPATCH_POSITION_ID] = destPositionID;

                        // assign worker
                        var directionAssign = new DirectionAssign()
                        {
                            JobID = DirectionAssign.JOB_ID_DISPATCH,
                            WorkerID = procModel.WorkerID,
                            EntryUserID = _userID,
                            EntryDateTime = DateTime.Now,
                            DirectionID = directionAllData.DirectionID
                        };

                        // make history
                        var history = new DirectionHistory()
                        {
                            DirectionID = directionAllData.DirectionID,
                            DocPosition = 0,
                            EventCode = Direction.DR_STATUS_DISPATCH_WAIT,
                            EventData = $"{procModel.WorkerID}|{destPositionID}",
                            EntryUserID = _userID,
                            EntryDateTime = DateTime.Now
                        };

                        // save 
                        db.Update(directionAllData);

                        //remove old asignments
                        db.Delete<DirectionAssign>("where directionid = @0 AND jobID = @1", directionAllData.DirectionID, DirectionAssign.JOB_ID_DISPATCH);

                        //add directionAssign
                        db.Save(directionAssign);

                        //add history
                        db.Insert(history);

                        //xtraData Save
                        directionAllData.XtraData.Save(db, directionAllData.DirectionID);

                        // do save
                        tr.Complete();

                        // log
                        Info($"DispatchUserAssignmentProc; DirectionID: {directionAllData.DirectionID}");
                                                
                    }

                    foreach (var direction in directionsList)
                    {
                        // init DispatchMakeReservationProc - add it to queue
                        var backgroundTaskQueue = (new ServicesFactory()).GetBackgroundTaskQueue();
                        var proc = new DispatchMakeReservationProc(_userID);
                        backgroundTaskQueue.QueueProcedure<DispatchMakeReservation>(proc, new DispatchMakeReservation() { DirectionID = direction.DirectionID });

                        // Call messageRouter
                        messageRouter.Publish(new Messages.DirectionStatusUpdated() { DirectionID = direction.DirectionID });
                    }
                }

                return new DispatchUserAssignment { Success = true };
            }

        }
    }
}

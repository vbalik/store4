﻿using System;
using System.Collections.Generic;
using System.Text;
using NPoco;
using S4.Core;
using S4.Entities;
using S4.ProcedureModels.Dispatch;
using S4.Report.Model.Dispatch;
using S4.StoreCore.Interfaces;
using System.Linq;
using combit.ListLabel24;

namespace S4.Procedures.Dispatch
{
    public class DispatchPrintLabelProc : ProceduresBase<DispatchPrintLabel>
    {
        const int MAX_QUANTITY = 100;

        public DispatchPrintLabelProc(string userID) : base(userID)
        {
        }


        protected override DispatchPrintLabel DoIt(DispatchPrintLabel procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new DispatchPrintLabel() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                if (procModel.ReportID == 0)
                {
                    if (procModel.TypePrint == TypePrint.MALE_STITKY)
                        procModel.ReportID = Singleton<DAL.SettingsSet>.Instance.GetValue_Int32(Entities.SettingsDictionary.DISPATCH_LABEL_REPORT_SMALL_ID);
                    else
                        procModel.ReportID = Singleton<DAL.SettingsSet>.Instance.GetValue_Int32(Entities.SettingsDictionary.DISPATCH_LABEL_REPORT_BIG_ID);
                }

                if (procModel.Count > MAX_QUANTITY)
                    return new DispatchPrintLabel() { Success = false, ErrorText = $"Požadované množství překročilo maximální množství ({MAX_QUANTITY})" };

                // get direction
                var direction = (new DAL.DirectionDAL()).GetDirectionAllData(procModel.DirectionID);
                if (direction == null)
                    return new DispatchPrintLabel() { Success = false, ErrorText = $"Doklad nebyl nalezen; DirectionID: {procModel.DirectionID}" };


                // get definition
                var printerLocation = Singleton<DAL.Cache.PrinterLocationCache>.Instance.GetItem(procModel.PrinterLocationID);
                if (printerLocation == null)
                    return new DispatchPrintLabel() { Success = false, ErrorText = $"Nenalezena tiskárna. (PrinterLocationID: {procModel.PrinterLocationID})" };

                var reportDef = Singleton<DAL.Cache.ReportCache>.Instance.GetItem(procModel.ReportID);
                if (reportDef == null)
                    return new DispatchPrintLabel() { Success = false, ErrorText = $"Nenalezen report. (ReportID: {procModel.ReportID}" };

                // print
                var report = new Report.BarCodeReport(reportDef.Template);

                if (IsTesting)
                {
                    var toPrint = report.Render(direction, 1, 1);
                    RenderResult = toPrint;
                }
                else
                {
                    for (int i = 1; i <= procModel.Count; i++)
                    {
                        var toPrint = report.Render(direction, i, procModel.Count);
                        Report.Printers.PrintDirect.PrintRAW(printerLocation.PrinterPath, "Label", toPrint);
                    }
                }

                return new DispatchPrintLabel() { Success = true };
            }


        }
    }

}

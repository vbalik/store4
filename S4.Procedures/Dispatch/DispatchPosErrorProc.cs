﻿using S4.Core;
using S4.Entities;
using S4.ProcedureModels.Dispatch;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Newtonsoft.Json;
using S4.Core.Utils;

namespace S4.Procedures.Dispatch
{
    public class DispatchPosErrorProc : ProceduresBase<DispatchPosError>
    {
        public DispatchPosErrorProc(string userID) : base(userID)
        {
        }


        protected override DispatchPosError DoIt(DispatchPosError procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new DispatchPosError() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            // checks
            var storeMove = new DAL.StoreMoveDAL().GetStoreMoveAllData(procModel.StoMoveID);
            if (storeMove == null)
                return new DispatchPosError() { Success = false, ErrorText = $"StoreMove nebylo nalezeno; StoMoveID: {procModel.StoMoveID}" };

            if (storeMove.DocStatus != StoreMove.MO_STATUS_NEW_DISPATCH)
                return new DispatchPosError() { Success = false, ErrorText = $"Doklad nemá očekávaný status MO_STATUS_NEW_DISPATCH; StoMoveID: {procModel.StoMoveID}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var position = Core.Singleton<DAL.Cache.PositionCache>.Instance.GetItem(procModel.PositionID);
                position.PosStatus = Position.POS_STATUS_BLOCKED;

                // make info
                var message = new InternMessage()
                {
                    MsgClass = InternMessage.POS_STATUS_BLOCKED,
                    MsgSeverity = InternMessage.MessageSeverityEnum.Low,
                    MsgHeader = $"Pozice PosCode: {position.PosCode} byla uzamčena",
                    MsgText = JsonUtils.ToJsonString("positionID", procModel.PositionID, "reason", procModel.Reason),
                    MsgDateTime = DateTime.Now,
                    MsgUserID = _userID
                };

                // create storemove history
                var storeMoveHistory = new StoreMoveHistory()
                {
                    StoMoveID = procModel.StoMoveID,
                    EntryDateTime = DateTime.Now,
                    EntryUserID = _userID,
                    EventCode = StoreMove.MO_EVENT_DISP_POS_ERROR,
                    EventData = $"{position.PositionID}|{position.PosCode}|{procModel.Reason}"
                };

                // create direction history
                var directionHistory = new DirectionHistory()
                {
                    DirectionID = storeMove.Parent_directionID,
                    EntryDateTime = DateTime.Now,
                    EntryUserID = _userID,
                    EventCode = Direction.DR_EVENT_POSITION_ERROR,
                    EventData = $"{position.PositionID}|{position.PosCode}|{procModel.Reason}"
                };

                using (var tr = db.GetTransaction())
                {
                    // save 
                    db.Update(position);

                    // save info
                    db.Insert(message);

                    // insert history
                    db.Insert(storeMoveHistory);
                    db.Insert(directionHistory);
                    // do save
                    tr.Complete();
                }

                // log
                base.Info($"DispatchPosError; PositionID: {position.PositionID}");

                return new DispatchPosError() { Success = true };
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using S4.Core;
using S4.Entities;
using S4.ProcedureModels.Dispatch;

namespace S4.Procedures.Dispatch
{
    [ChangesDirectionStatus]
    [ChangesDirectionAssignment]
    public class DispatchReassignProc : ProceduresBase<DispatchReassign>
    {
        public DispatchReassignProc(string userID) : base(userID)
        {
        }

        protected override DispatchReassign DoIt(DispatchReassign procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new DispatchReassign() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            var destPositionChanged = false;
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // check
                var direction = (new DAL.DirectionDAL()).GetDirectionAllData(procModel.DirectionID);
                if (direction == null)
                    return new DispatchReassign() { Success = false, ErrorText = $"Doklad nebyl nalezen; DirectionID: {procModel.DirectionID}" };

                // do tests
                if (direction.DocDirection != Direction.DOC_DIRECTION_OUT)
                    return new DispatchReassign() { Success = false, ErrorText = $"Doklad nemá DocDirection DOC_DIRECTION_OUT; DirectionID: {procModel.DirectionID}" };

                if (direction.DocStatus != Direction.DR_STATUS_DISPATCH_PROC)
                    return new DispatchReassign() { Success = false, ErrorText = $"Doklad nemá očekávaný status DR_STATUS_RES_IN_PROC; DirectionID: {procModel.DirectionID}" };


                // check XTRA_DISPATCH_POSITION_ID change                
                int originalDestPositionID = (int)direction.XtraData[Direction.XTRA_DISPATCH_POSITION_ID];
                if (originalDestPositionID != procModel.DestPositionID)
                {
                    var save = Singleton<ServicesFactory>.Instance.GetSave();
                    var moveResult = save.MoveResult_ByDirection(db, direction.DirectionID, StoreMove.MO_DOCTYPE_DISPATCH, (mi) => mi.PositionID == originalDestPositionID);

                    if (moveResult.Any())
                    {
                        var pos = Singleton<DAL.Cache.PositionCache>.Instance.GetItem(originalDestPositionID);
                        return new DispatchReassign() { Success = false, ErrorText = $"Změna pozice vyskladnění není možná - na pozici {pos.PosCode} už jsou nějaké pohyby; DirectionID: {procModel.DirectionID}" };
                    }

                    destPositionChanged = true;
                }
                               
                // set position
                direction.XtraData[Direction.XTRA_DISPATCH_POSITION_ID] = procModel.DestPositionID;

                // assign worker
                var directionAssign = new DirectionAssign()
                {
                    JobID = DirectionAssign.JOB_ID_DISPATCH,
                    WorkerID = procModel.WorkerID,
                    EntryUserID = _userID,
                    EntryDateTime = DateTime.Now,
                    DirectionID = direction.DirectionID
                };

                // make history
                var history = new DirectionHistory()
                {
                    DirectionID = procModel.DirectionID,
                    DocPosition = 0,
                    EventCode = Direction.DR_STATUS_DISPATCH_WAIT,
                    EventData = $"{procModel.WorkerID}|{procModel.DestPositionID}",
                    EntryUserID = _userID,
                    EntryDateTime = DateTime.Now
                };

                // save
                using (var tr = db.GetTransaction())
                {
                    //remove old asignments
                    db.Delete<DirectionAssign>("where directionid = @0 AND jobID = @1", direction.DirectionID, DirectionAssign.JOB_ID_DISPATCH);

                    //add directionAssign
                    db.Save(directionAssign);

                    //add history
                    db.Insert(history);

                    //xtraData Save
                    direction.XtraData.Save(db, direction.DirectionID);

                    if(destPositionChanged)
                    {
                        direction.DocStatus = Direction.DR_STATUS_RES_IN_PROC;
                        db.Save(direction);
                    }

                    // do save
                    tr.Complete();
                }


                // log
                Info($"DispatchReassignProc; DirectionID: {procModel.DirectionID}");
            }



            // if dispatch pos changed - init DispatchMakeReservationProc - add it to queue
            if (destPositionChanged)
            {
                var backgroundTaskQueue = (new ServicesFactory()).GetBackgroundTaskQueue();
                var proc = new DispatchMakeReservationProc(_userID);
                backgroundTaskQueue.QueueProcedure<DispatchMakeReservation>(proc, new DispatchMakeReservation() { DirectionID = procModel.DirectionID });
            }


            return new DispatchReassign() { Success = true };
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using S4.Core;
using S4.Core.Data;
using S4.DAL;
using S4.Entities;
using S4.ProcedureModels.Dispatch;
using S4.StoreCore.Interfaces;
using S4.StoreCore.OnStore_Basic;

namespace S4.Procedures.Dispatch
{
    public class DispatchManualSaveItemProc : ProceduresBase<DispatchManualSaveItem>
    {
        public DispatchManualSaveItemProc(string userID) : base(userID)
        {
        }


        protected override DispatchManualSaveItem DoIt(DispatchManualSaveItem procModel)
        {
            var check = new FieldsCheck();
            if (!check.DoCheck(procModel))
                return new DispatchManualSaveItem()
                {
                    Success = false,
                    ErrorText = $"Parametry procedury nejsou platné {Formating.ParseError(check.Errors)}"
                };

            using (var db = ConnectionHelper.GetDB())
            {
                //check
                var direction = (new DirectionDAL()).GetDirectionAllData(procModel.DirectionID);
                if (direction == null)
                    return new DispatchManualSaveItem()
                        {Success = false, ErrorText = $"Doklad nebyl nalezen; DirectionID: {procModel.DirectionID}"};

                // do tests
                if (direction.DocDirection != Direction.DOC_DIRECTION_OUT)
                    return new DispatchManualSaveItem()
                    {
                        Success = false,
                        ErrorText = $"Doklad nemá DocDirection DOC_DIRECTION_OUT; DirectionID: {procModel.DirectionID}"
                    };

                if (direction.DocStatus != Direction.DR_STATUS_DISPATCH_MANUAL)
                    return new DispatchManualSaveItem()
                    {
                        Success = false,
                        ErrorText = $"Doklad nemá očekávaný status DR_STATUS_DISPATCH_MANUAL; DirectionID: {procModel.DirectionID}"
                    };

                // set dispatch section 
                if (direction.XtraData.IsNull(Direction.XTRA_DISPATCH_POSITION_ID, 0))
                    return new DispatchManualSaveItem()
                        {Success = false, ErrorText = "Direction objekt nemá XTRA_DISPATCH_POSITION_ID hodnotu"};

                var procResult = new DispatchManualSaveItem();

                var save = Singleton<ServicesFactory>.Instance.GetSave();
                var onPositionItems =
                    new OnStore().OnPosition(new List<int> {procModel.PositionID},
                        ResultValidityEnum.Valid);

                // filter item by ArtPackID
                var onPositionItemsFiltered = onPositionItems.Where(item => item.ArtPackID == procModel.ArtPackID).ToList();
                var onPositionItemsQuery = onPositionItemsFiltered.AsQueryable();
                var firstItem = onPositionItemsFiltered.FirstOrDefault();

                if (!string.IsNullOrEmpty(procModel.CarrierNum))
                    onPositionItemsQuery = onPositionItemsQuery.Where(item => item.CarrierNum == procModel.CarrierNum);

                if (firstItem.Article.UseBatch)
                    onPositionItemsQuery = onPositionItemsQuery.Where(item => item.BatchNum == procModel.BatchNum);

                if (firstItem.Article.UseExpiration)
                    onPositionItemsQuery = onPositionItemsQuery
                        .Where(item => item.ExpirationDate.GetValueOrDefault().ToString("yyyy-MM-dd") == (procModel.ExpirationDate ?? DateTime.Now).ToString("yyyy-MM-dd"));

                
                OnStoreItemFull articleOnStore = null;

                try
                {
                    articleOnStore = onPositionItemsQuery.SingleOrDefault();

                    if (articleOnStore == null)
                        throw new ArgumentNullException();
                }
                catch (InvalidOperationException)
                {
                    return new DispatchManualSaveItem() { Success = false, ErrorText = "Nalezeno více položek" };
                }
                catch (ArgumentNullException)
                {
                    return new DispatchManualSaveItem() { Success = false, ErrorText = "Položka nenalezena" };
                }

                // cancel all unsaved moves owned by direction                    
                if (!save.CancelByDirection(db, _userID, procModel.DirectionID, true, out var msg))
                    return new DispatchManualSaveItem() {ErrorText = msg};

                var updateDirectionItem = false;
                
                using (var tr = db.GetTransaction())
                {
                    var saveItems = new List<MoveItem>
                    {
                        new MoveItem()
                        {
                            ArtPackID = articleOnStore.ArtPackID,
                            CarrierNum = articleOnStore.CarrierNum,
                            Quantity = procModel.Quantity,
                            BatchNum = articleOnStore.BatchNum,
                            ExpirationDate = articleOnStore.ExpirationDate,
                            Parent_docPosition = procModel.ParentDocPosition,

                            SourcePositionID = articleOnStore.PositionID,
                            DestinationPositionID = (int) direction.XtraData[Direction.XTRA_DISPATCH_POSITION_ID]
                        }
                    };

                    // save moves
                    var stoMoveID = save.Move(db, _userID, StoreMove.MO_PREFIX_DISPATCH, StoreMove.MO_DOCTYPE_DISPATCH,
                        saveItems, true, sm =>
                        {
                            sm.DocStatus = StoreMove.MO_STATUS_SAVED_DISPATCH;
                            sm.DocType = StoreMove.MO_DOCTYPE_DISPATCH;
                            sm.Parent_directionID = direction.DirectionID;
                        },
                        null,
                        out string message);
                    if (stoMoveID == -1)
                        return new DispatchManualSaveItem() {ErrorText = msg};

                    procResult.StoMoveID = stoMoveID;
                    direction.XtraData[Direction.XTRA_DISPATCH_STORE_MOVE_ID] = stoMoveID;

                    // update direction
                    db.Update(direction);
                    direction.XtraData.Save(db, direction.DirectionID);

                    //save history
                    var hist = new StoreMoveHistory()
                    {
                        DocPosition = procModel.ParentDocPosition,
                        EntryDateTime = DateTime.Now,
                        EntryUserID = _userID,
                        EventCode = StoreMove.MO_EVENT_CONF_DISPATCH,
                        StoMoveID = stoMoveID
                    };
                    db.Insert(hist);
                    
                    // direction item
                    var directionItem = direction.Items.Single(i => i.DocPosition == procModel.ParentDocPosition);
                    
                    // determine if after this save will all items on same docPosition valid
                    var dispatchPositionsModel = Singleton<DAL.Cache.PositionByCategoryModelCache>.Instance.GetItem(Entities.Position.PositionCategoryEnum.Dispatch);
                    var stMoves = save.MoveResult_ByDirection(db, direction.DirectionID, StoreMove.MO_DOCTYPE_DISPATCH, (mi) => dispatchPositionsModel.PositionIDs.Contains(mi.PositionID));
                    var movesSum = stMoves
                        .Where(k => k.DocPosition == directionItem.DocPosition)
                        .Sum(k => k.Quantity);
                    
                    updateDirectionItem = (movesSum == directionItem.Quantity);
                    if (updateDirectionItem)
                    {
                        // save direction item
                        directionItem.ItemStatus = DirectionItem.DI_STATUS_DISPATCH_SAVED;
                        db.Save(directionItem);

                        //make history
                        var dirHistory = new DirectionHistory()
                        {
                            DirectionID = directionItem.DirectionID,
                            DocPosition = directionItem.DocPosition,
                            EntryDateTime = DateTime.Now,
                            EntryUserID = _userID,
                            EventCode = Direction.DR_EVENT_ITEM_DISPATCHED
                        };
                        db.Insert(dirHistory);
                    }
                    
                    tr.Complete();
                }

                procResult.Success = true;
                return procResult;
            }
        }
    }
}
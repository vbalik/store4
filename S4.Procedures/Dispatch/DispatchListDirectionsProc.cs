﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using S4.Core;
using S4.Entities;
using S4.ProcedureModels.Extensions;
using S4.ProcedureModels.Dispatch;


namespace S4.Procedures.Dispatch
{
    public class DispatchListDirectionsProc : ProceduresBase<DispatchListDirections>
    {
        public DispatchListDirectionsProc(string userID) : base(userID)
        {
        }


        protected override DispatchListDirections DoIt(DispatchListDirections procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new DispatchListDirections() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // get directions
                var dirs = (new DBHelpers.Directions()).ScanByAssigment(db, new List<string> { Direction.DR_STATUS_DISPATCH_WAIT, Direction.DR_STATUS_DISPATCH_PROC }, DirectionAssign.JOB_ID_DISPATCH, procModel.WorkerID );

                // to result
                var docs = dirs.Select(i => new Entities.Helpers.DocumentInfo()
                {
                    StoMoveID = _GetStoMoveID(i.DirectionID),
                    DocumentID = i.DirectionID,
                    DocumentName = i.DocInfo(),
                    DocumentDetail = Singleton<DAL.Cache.PartnerCache>.Instance.GetItem(i.PartnerID)?.PartnerName
                }).ToList();

                return new DispatchListDirections()
                {
                    Directions = docs,
                    Success = true
                };
            }
        }

        private int _GetStoMoveID(int directionID)
        {
            // just a dummy implementation
            var direction = (new DAL.DirectionDAL()).GetDirectionAllData(directionID);

            if (direction.XtraData.IsNull(Direction.XTRA_DISPATCH_STORE_MOVE_ID, 0))
                throw new Exception($"Dispatch_Proc - XTRA_DISPATCH_STORE_MOVE_ID není nastaveno; directionID: {directionID}");

            return (int)direction.XtraData[Direction.XTRA_DISPATCH_STORE_MOVE_ID];
        }
    }
}
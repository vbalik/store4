﻿using S4.Entities;
using S4.ProcedureModels.Dispatch;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Procedures.Dispatch
{
    [ChangesDirectionStatus]
    public class DispatchRepeatReservationProc : ProceduresBase<DispatchRepeatReservation>
    {
        public DispatchRepeatReservationProc(string userID) : base(userID)
        {
        }


        protected override DispatchRepeatReservation DoIt(DispatchRepeatReservation procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new DispatchRepeatReservation() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //check
                var direction = (new DAL.DirectionDAL()).GetDirectionAllData(procModel.DirectionID);
                if (direction == null)
                    return new DispatchRepeatReservation() { Success = false, ErrorText = $"Doklad nebyl nalezen; DirectionID: {procModel.DirectionID}" };

                // do tests
                if (direction.DocDirection != Direction.DOC_DIRECTION_OUT)
                    return new DispatchRepeatReservation() { Success = false, ErrorText = $"Doklad nemá DocDirection DOC_DIRECTION_OUT; DirectionID: {procModel.DirectionID}" };

                if (direction.DocStatus != Direction.DR_STATUS_DISPATCH_PROC)
                    return new DispatchRepeatReservation() { Success = false, ErrorText = $"Doklad nemá očekávaný status DR_STATUS_DISPATCH_PROC; DirectionID: {procModel.DirectionID}" };

                if(direction.XtraData.IsNull(Direction.XTRA_DISPATCH_POSITION_ID, 0))
                    return new DispatchRepeatReservation() { Success = false, ErrorText = $"Doklad nemá nastaveno XTRA_DISPATCH_POSITION_ID; DirectionID: {procModel.DirectionID}" };


                // change status
                direction.DocStatus = Direction.DR_STATUS_RES_IN_PROC;

                // make history
                var history = new DirectionHistory()
                {
                    DirectionID = procModel.DirectionID,
                    DocPosition = 0,
                    EventCode = Direction.DR_EVENT_DISP_REPEAT,
                    EntryUserID = _userID,
                    EntryDateTime = DateTime.Now
                };


                // save
                using (var tr = db.GetTransaction())
                {
                    // save 
                    db.Update(direction);

                    //add history
                    db.Insert(history);

                    // do save
                    tr.Complete();
                }


                // log
                Info($"DispatchRepeatReservation; DirectionID: {procModel.DirectionID}");


                // init DispatchMakeReservationProc - add it to queue
                var backgroundTaskQueue = (new ServicesFactory()).GetBackgroundTaskQueue();
                var proc = new DispatchMakeReservationProc(_userID);
                backgroundTaskQueue.QueueProcedure<DispatchMakeReservation>(proc, new DispatchMakeReservation() { DirectionID = procModel.DirectionID });


                return new DispatchRepeatReservation() { Success = true };
            }
        }
    }
}

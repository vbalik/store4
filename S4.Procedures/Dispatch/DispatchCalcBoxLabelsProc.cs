﻿using S4.Core;
using S4.Entities;
using S4.ProcedureModels.Dispatch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace S4.Procedures.Dispatch
{
    public class DispatchCalcBoxLabelsProc : ProceduresBase<DispatchCalcBoxLabels>
    {
        const int MAX_QUANTITY = 100;
        const string PRINT_PACKING = "kar";


        public DispatchCalcBoxLabelsProc(string userID) : base(userID)
        {
        }


        protected override DispatchCalcBoxLabels DoIt(DispatchCalcBoxLabels procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new DispatchCalcBoxLabels() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //check
                var direction = (new DAL.DirectionDAL()).GetDirectionAllData(procModel.DirectionID);
                if (direction == null)
                    return new DispatchCalcBoxLabels() { Success = false, ErrorText = $"Doklad nebyl nalezen; DirectionID: {procModel.DirectionID}" };

                // do tests
                if (direction.DocDirection != Direction.DOC_DIRECTION_OUT)
                    return new DispatchCalcBoxLabels() { Success = false, ErrorText = $"Doklad nemá DocDirection DOC_DIRECTION_OUT; DirectionID: {procModel.DirectionID}" };


                // make box quantity estination
                var result = new List<DispatchCalcBoxLabels.BoxLabel>();

                foreach (var dirItem in direction.Items)
                {
                    var newInfo = new DispatchCalcBoxLabels.BoxLabel
                    {
                        DirectionID = direction.DirectionID,
                        DocPosition = dirItem.DocPosition
                    };


                    // set label text
                    if (!String.IsNullOrWhiteSpace(dirItem.PartnerDepart))
                        newInfo.LabelText = dirItem.PartnerDepart;
                    else
                    {
                        if (direction.PartAddrID > 0)
                        {
                            var partnerAddress = Singleton<DAL.Cache.PartnerAddressCache>.Instance.GetItem(direction.PartAddrID);
                            newInfo.LabelText = partnerAddress?.DeliveryInst;
                        }
                        else
                        {
                            newInfo.LabelText = "?";
                        }
                    }

                    // set print quantity
                    var packing = Singleton<DAL.Cache.ArticlePackingCache>.Instance.GetItem(dirItem.ArtPackID);                    
                    if (String.Equals(packing.PackDesc, PRINT_PACKING, StringComparison.InvariantCultureIgnoreCase))
                    {
                        newInfo.LabelQuantity = Math.Min((int)dirItem.Quantity, MAX_QUANTITY);
                    }
                    else
                    {
                        // get sec packing
                        var article = Singleton<DAL.Cache.ArticleModelCache>.Instance.GetItem(packing.ArticleID);
                        var secPacking = article.Packings.Where(p => String.Equals(p.PackDesc, PRINT_PACKING, StringComparison.InvariantCultureIgnoreCase)).SingleOrDefault();

                        // if exists, use it to calculate
                        if (secPacking != null && secPacking.PackRelation != 0)
                        {
                            decimal boxQ = dirItem.Quantity / secPacking.PackRelation;
                            newInfo.LabelQuantity = Math.Min((int)Math.Ceiling(boxQ), MAX_QUANTITY);
                        }
                        else
                        {
                            // no way to count boxes number
                            newInfo.LabelQuantity = 1;
                        }
                    }
                    result.Add(newInfo);
                }


                // compress label text values
                result = result.GroupBy(k => k.LabelText, (k, i) => new DispatchCalcBoxLabels.BoxLabel() {
                    LabelText = k,
                    LabelQuantity = i.Sum(q => q.LabelQuantity),
                    DirectionID = i.Max(d => d.DirectionID),
                    DocPosition = i.Min(d => d.DocPosition),
                }).ToList();


                // reports & printers
                var reports = db.Query<Entities.Reports>().Where(r => r.ReportType == Entities.Reports.REPORT_TYPE_BOX_LABEL).ToList();
                var printerTypes = reports.Select(r => r.PrinterTypeID).ToList();
                var printers = db.Query<Entities.PrinterLocation>().Where(p => printerTypes.Contains(p.PrinterTypeID)).ToList();

                return new DispatchCalcBoxLabels() {
                    Success = true,
                    BoxLabels = result,
                    Reports = reports,
                    Printers = printers
                };
            }
        }
    }
}

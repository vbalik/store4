﻿using S4.Core;
using S4.ProcedureModels.Dispatch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace S4.Procedures.Dispatch
{
    public class DispatchPrintBoxLabelProc : ProceduresBase<DispatchPrintBoxLabel>
    {
        const int MAX_QUANTITY = 100;


        public DispatchPrintBoxLabelProc(string userID) : base(userID)
        {
        }


        protected override DispatchPrintBoxLabel DoIt(DispatchPrintBoxLabel procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new DispatchPrintBoxLabel() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            if (procModel.LabelsQuant > MAX_QUANTITY)
                return new DispatchPrintBoxLabel() { Success = false, ErrorText = $"Požadované množství překročilo maximální množství ({MAX_QUANTITY})" };

            if (procModel.ReportID == 0)
            {
                procModel.ReportID = Singleton<DAL.SettingsSet>.Instance.GetValue_Int32(Entities.SettingsDictionary.DISPATCH_BOX_REPORT_ID);
            }

            // get direction
            var direction = (new DAL.DirectionDAL()).GetDirectionAllData(procModel.DirectionID);
            if (direction == null)
                return new DispatchPrintBoxLabel() { Success = false, ErrorText = $"Doklad nebyl nalezen; DirectionID: {procModel.DirectionID}" };

            var dirItem = direction.Items.Where(i => i.DocPosition == procModel.DocPosition).SingleOrDefault();
            if (dirItem == null)
                return new DispatchPrintBoxLabel() { Success = false, ErrorText = $"Položka nebyla nalezena; DirectionID: {procModel.DirectionID}; DocPosition: {procModel.DocPosition}" };

            // get definition
            var printerLocation = Singleton<DAL.Cache.PrinterLocationCache>.Instance.GetItem(procModel.PrinterLocationID);
            if (printerLocation == null)
                return new DispatchPrintBoxLabel() { Success = false, ErrorText = $"Nenalezena tiskárna. (PrinterLocationID: {procModel.PrinterLocationID})" };

            var reportDef = Singleton<DAL.Cache.ReportCache>.Instance.GetItem(procModel.ReportID);
            if (reportDef == null)
                return new DispatchPrintBoxLabel() { Success = false, ErrorText = $"Nenalezen report. (ReportID: {procModel.ReportID}" };

            // get other data
            direction.Partner = Singleton<DAL.Cache.PartnerCache>.Instance.GetItem(direction.PartnerID);
            direction.PartnerAddress = Singleton<DAL.Cache.PartnerAddressCache>.Instance.GetItem(direction.PartAddrID);

            // print
            var report = new Report.BarCodeReport(reportDef.Template);
            var toPrint = report.Render(S4.Core.Utils.StrUtils.ReplaceUnPrintableCharacter, procModel.LabelText ?? String.Empty, direction, dirItem, direction.PartnerAddress);

            if (IsTesting)
                RenderResult = toPrint;
            else
            {
                for (int i = 0; i < procModel.LabelsQuant; i++)
                    Report.Printers.PrintDirect.PrintRAW(printerLocation.PrinterPath, "BoxLabel", toPrint);
            }
            
            return new DispatchPrintBoxLabel() { Success = true };
        }
    }
}

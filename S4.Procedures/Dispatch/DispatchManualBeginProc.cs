﻿using S4.Core.Data;
using S4.DAL;
using S4.Entities;
using S4.ProcedureModels.Dispatch;

namespace S4.Procedures.Dispatch
{
    public class DispatchManualBeginProc : ProceduresBase<DispatchManualBegin>
    {
        public DispatchManualBeginProc(string userID) : base(userID)
        {
        }

        protected override DispatchManualBegin DoIt(DispatchManualBegin procModel)
        {
            var check = new FieldsCheck();
            if (!check.DoCheck(procModel))
                return new DispatchManualBegin()
                {
                    Success = false,
                    ErrorText = $"Parametry procedury nejsou platné {Formating.ParseError(check.Errors)}"
                };

            var procResult = new DispatchManualBegin();

            using (var db = ConnectionHelper.GetDB())
            {
                //check
                var direction = (new DirectionDAL()).GetDirectionAllData(procModel.DirectionID);
                if (direction == null)
                    return new DispatchManualBegin()
                        {Success = false, ErrorText = $"Doklad nebyl nalezen; DirectionID: {procModel.DirectionID}"};

                // do tests
                if (direction.DocDirection != Direction.DOC_DIRECTION_OUT)
                    return new DispatchManualBegin()
                    {
                        Success = false,
                        ErrorText = $"Doklad nemá DocDirection DOC_DIRECTION_OUT; DirectionID: {procModel.DirectionID}"
                    };

                if (direction.DocStatus != Direction.DR_STATUS_LOAD)
                    return new DispatchManualBegin()
                    {
                        Success = false,
                        ErrorText = $"Doklad nemá očekávaný status DR_STATUS_LOAD; DirectionID: {procModel.DirectionID}"
                    };

                using (var tr = db.GetTransaction())
                {
                    // set status
                    direction.DocStatus = Direction.DR_STATUS_DISPATCH_MANUAL;

                    // set position
                    direction.XtraData[Direction.XTRA_DISPATCH_POSITION_ID] = procModel.PositionID;

                    // update direction
                    db.Update(direction);
                    direction.XtraData.Save(db, direction.DirectionID);

                    tr.Complete();
                }
            }

            procResult.Success = true;
            return procResult;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using S4.Core;
using S4.Entities;
using S4.ProcedureModels.Dispatch;


namespace S4.Procedures.Dispatch
{
    public class DispatchShowDirectionProc : ProceduresBase<DispatchShowDirection>
    {
        public DispatchShowDirectionProc(string userID) : base(userID)
        {
        }


        protected override DispatchShowDirection DoIt(DispatchShowDirection procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new DispatchShowDirection() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //get direction
                var directionItem = db.Fetch<DirectionItem>("where directionID = @0", procModel.DirectionID);

                // convert to ItemInfo
                var result = new DispatchShowDirection();
                result.ItemInfoList = new List<Entities.Helpers.ItemInfo>();
                foreach (var item in directionItem)
                {
                    // get article details
                    var articlePacking = Singleton<DAL.Cache.ArticlePackingCache>.Instance.GetItem(item.ArtPackID);
                    var articleModel = Singleton<DAL.Cache.ArticleModelCache>.Instance.GetItem(articlePacking.ArticleID);

                    var itemInfo = new Entities.Helpers.ItemInfo()
                    {
                        ArtPackID = item.ArtPackID,
                        Quantity = item.Quantity,
                        PackDesc = articlePacking?.PackDesc,
                        ArticleCode = item.ArticleCode,
                        ArticleDesc = articleModel.ArticleDesc,
                        UseBatch = articleModel.UseBatch,
                        UseExpiration = articleModel.UseExpiration
                    };
                    result.ItemInfoList.Add(itemInfo);
                }

                result.Success = true;
                return result;
            }
        }
    }
}

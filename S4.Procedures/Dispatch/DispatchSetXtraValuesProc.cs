﻿using System;
using S4.Entities;
using S4.Entities.XtraData;
using S4.ProcedureModels.Dispatch;


namespace S4.Procedures.Dispatch
{
    public class DispatchSetXtraValuesProc : ProceduresBase<DispatchSetXtraValues>
    {
        public DispatchSetXtraValuesProc(string userID) : base(userID)
        {
        }


        protected override DispatchSetXtraValues DoIt(DispatchSetXtraValues procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new DispatchSetXtraValues()
                {
                    Success = false,
                    ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}"
                };

            if (procModel.Values.Count == 0)
                return new DispatchSetXtraValues() { Success = false, ErrorText = $"Hodnoty jsou prazdné" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                using (var tr = db.GetTransaction())
                {
                    //get direction
                    var direction = (new DAL.DirectionDAL()).GetDirectionAllData(procModel.DirectionID);

                    foreach (var val in procModel.Values)
                    {
                        if (val.Value == null || (val.Value is string && string.IsNullOrEmpty(val.Value)))
                        {
                            direction.XtraData[val.Key] = null;
                        }
                        else
                        {
                            var xtraType = Direction.XtraDataDict[val.Key].XtraType;
                            direction.XtraData[val.Key] = Convert.ChangeType(val.Value, xtraType);
                        }
                    }

                    // save direction
                    db.Save(direction);

                    // save xtraData
                    direction.XtraData.Save(db, direction.DirectionID);

                    tr.Complete();

                    return new DispatchSetXtraValues() { Success = true };
                }
            }
        }
    }
}
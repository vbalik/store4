﻿using S4.Core;
using S4.Entities;
using S4.ProcedureModels.Dispatch;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace S4.Procedures.Dispatch
{
    public class DispatchSaveItemProc : ProceduresBase<DispatchSaveItem>
    {
        public DispatchSaveItemProc(string userID) : base(userID)
        {
        }


        protected override DispatchSaveItem DoIt(DispatchSaveItem procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new DispatchSaveItem() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            // checks
            var storeMove = new DAL.StoreMoveDAL().GetStoreMoveAllData(procModel.StoMoveID);
            if (storeMove == null)
                return new DispatchSaveItem() { Success = false, ErrorText = $"StoreMove nebylo nalezeno; StoMoveID: {procModel.StoMoveID}" };

            if (storeMove.DocStatus != StoreMove.MO_STATUS_NEW_DISPATCH)
                return new DispatchSaveItem() { Success = false, ErrorText = $"Doklad nemá očekávaný status MO_STATUS_NEW_DISPATCH; StoMoveID: {procModel.StoMoveID}" };

            var selected_item = storeMove.Items.Where(i => i.StoMoveItemID == procModel.StoMoveItemID).SingleOrDefault();
            if (selected_item == null)
                return new DispatchSaveItem() { Success = false, ErrorText = $"StoreMoveItem nebylo nalezeno; StoMoveItemID: {procModel.StoMoveItemID}" };

            var direction = new DAL.DirectionDAL().GetDirectionAllData(storeMove.Parent_directionID);
            if (direction == null)
                return new DispatchSaveItem() { Success = false, ErrorText = $"Direction nebylo nalezeno; Parent_directionID: {storeMove.Parent_directionID}" };

            var directionItem = direction.Items.Where(i => i.DocPosition == selected_item.Parent_docPosition).SingleOrDefault();
            if (directionItem == null)
                return new DispatchSaveItem() { Success = false, ErrorText = $"DirectionItem nebylo nalezeno; Parent_docPosition: {selected_item.Parent_docPosition}" };


            bool updateDirectionItem = false;

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // update
                using (var tr = db.GetTransaction())
                {
                    var save = Singleton<ServicesFactory>.Instance.GetSave();

                    //save storemoveitem
                    if (!save.ValidateMoveItem(db, _userID, procModel.StoMoveID, procModel.StoMoveItemID, procModel.DestCarrierNum, out string msg))
                        return new DispatchSaveItem() { ErrorText = msg };

                    //save history
                    var hist = new StoreMoveHistory()
                    {
                        DocPosition = selected_item.DocPosition,
                        EntryDateTime = DateTime.Now,
                        EntryUserID = _userID,
                        EventCode = StoreMove.MO_EVENT_CONF_DISPATCH,
                        StoMoveID = procModel.StoMoveID
                    };
                    db.Insert(hist);

                    // determine if after this save will all items on same docPosition valid
                    var dispatchPositionsModel = Singleton<DAL.Cache.PositionByCategoryModelCache>.Instance.GetItem(Entities.Position.PositionCategoryEnum.Dispatch);
                    var stMoves = save.MoveResult_ByDirection(db, storeMove.Parent_directionID, StoreMove.MO_DOCTYPE_DISPATCH, (mi) => dispatchPositionsModel.PositionIDs.Contains(mi.PositionID));
                    var movesSum = stMoves
                        .Where(k => k.DocPosition == selected_item.Parent_docPosition)
                        .Sum(k => k.Quantity);

                    updateDirectionItem = (movesSum == directionItem.Quantity);
                    if (updateDirectionItem)
                    {
                        // save direction item
                        directionItem.ItemStatus = DirectionItem.DI_STATUS_DISPATCH_SAVED;
                        db.Save(directionItem);

                        //make history
                        var dirHistory = new DirectionHistory()
                        {
                            DirectionID = directionItem.DirectionID,
                            DocPosition = directionItem.DocPosition,
                            EntryDateTime = DateTime.Now,
                            EntryUserID = _userID,
                            EventCode = Direction.DR_EVENT_ITEM_DISPATCHED
                        };
                        db.Insert(dirHistory);
                    }

                    tr.Complete();
                }
            }

            // log
            base.Info($"DispatchSaveItem; StoMoveItemID: {procModel.StoMoveItemID}");

            // send 'Messages.DirectionItemUpdated' message
            if (updateDirectionItem)
            {
                var messageRouter = Core.Singleton<ServicesFactory>.Instance.GetMessageRouter();
                messageRouter.Publish(new Messages.DirectionItemUpdated() { DirectionID = direction.DirectionID });
            }

            return new DispatchSaveItem() { Success = true };
        }
    }
}

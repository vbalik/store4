﻿using S4.Core;
using S4.Entities;
using S4.ProcedureModels.Dispatch;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace S4.Procedures.Dispatch
{
    public class DispatchNextItemProc : ProceduresBase<DispatchNextItem>
    {
        public DispatchNextItemProc(string userID) : base(userID)
        {
        }


        protected override DispatchNextItem DoIt(DispatchNextItem procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new DispatchNextItem() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            var storeMove = new DAL.StoreMoveDAL().GetStoreMoveAllData(procModel.StoMoveID);
            if (storeMove.DocStatus == StoreMove.MO_STATUS_NEW_DISPATCH)
                return new DispatchNextItem() { Success = true, WasFound = false };


            var items = storeMove.Items.Where(i => i.ItemDirection == StoreMoveItem.MI_DIRECTION_OUT);
            if (items.Count() == 0)
            {
                return new DispatchNextItem() { Success = true, WasFound = false };
            }

            DispatchNextItem res = null;
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var storeMoveItem = storeMove.Items.Where(i => i.ItemDirection == StoreMoveItem.MI_DIRECTION_OUT)
                    .Select(i => new { StoreMoveItem = i, StoreMoveLot = db.SingleById<StoreMoveLot>(i.StoMoveLotID) })
                    .Select(i => new
                    {
                        i.StoreMoveItem,
                        ArticlePacking = Singleton<DAL.Cache.ArticlePackingCache>.Instance.GetItem(i.StoreMoveLot.ArtPackID),
                        Position = Singleton<DAL.Cache.PositionCache>.Instance.GetItem(i.StoreMoveItem.PositionID),
                    })
                    .OrderBy(i => i.ArticlePacking.InclCarrier)
                    .ThenBy(i => i.ArticlePacking.ArtPackID)
                    .ThenBy(i => i.Position.PosX)
                    .ThenBy(i => i.Position.PosY)
                    .First();

                res = new DispatchNextItem()
                {
                    Success = true,
                    WasFound = true,
                    StoMoveItemID = storeMoveItem.StoreMoveItem.StoMoveItemID,
                    ScrPosition = storeMoveItem.Position,
                    DestPosition = Singleton<DAL.Cache.PositionCache>.Instance.GetItem(storeMove.Items.Where(i => i.StoMoveItemID == storeMoveItem.StoreMoveItem.BrotherID).First().PositionID),
                    Article = new Entities.Helpers.ItemInfo() { ArtPackID = storeMoveItem.ArticlePacking.ArtPackID }
                };
                DBHelpers.Articles.FillArticleData(res.Article);
            }

            return res;
        }
    }
}

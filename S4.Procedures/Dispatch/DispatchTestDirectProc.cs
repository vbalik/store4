﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using S4.Core;
using S4.Entities;
using S4.ProcedureModels.Dispatch;

namespace S4.Procedures.Dispatch
{
    [ChangesDirectionStatus]
    public class DispatchTestDirectProc : ProceduresBase<DispatchTestDirect>
    {
        public DispatchTestDirectProc(string userID) : base(userID)
        {
        }

        protected override DispatchTestDirect DoIt(DispatchTestDirect procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new DispatchTestDirect() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // check
                var direction = (new DAL.DirectionDAL()).GetDirectionAllData(procModel.DirectionID);
                if (direction == null)
                    return new DispatchTestDirect() { Success = false, ErrorText = $"Direction nebylo nalezeno; DirectionID: {procModel.DirectionID}" };

                if (direction.DocStatus != Direction.DR_STATUS_DISPATCH_PROC && direction.DocStatus != Direction.DR_STATUS_DISPATCH_MANUAL)
                    return new DispatchTestDirect()
                    {
                        Success = false,
                        ErrorText = $"Příkaz nemá očekávaný status DR_STATUS_DISPATCH_PROC, DR_STATUS_DISPATCH_MANUAL; DirectionID: {procModel.DirectionID}"
                    };


                // get summary of store docs by article
                var dirSum = direction.Items
                    .Where(i => i.ItemStatus != DirectionItem.DI_STATUS_CANCEL)
                    .GroupBy(k => k.ArtPackID, (ArtPackID, items) => new { ArtPackID, Quantity = items.Sum(i => i.Quantity) })
                    .ToList();

                // get summary of dispatch store moves by article
                var save = Singleton<ServicesFactory>.Instance.GetSave();
                var dispatchPositionsModel = Singleton<DAL.Cache.PositionByCategoryModelCache>.Instance.GetItem(Entities.Position.PositionCategoryEnum.Dispatch);
                var stMoves = save.MoveResult_ByDirection(db, procModel.DirectionID, StoreMove.MO_DOCTYPE_DISPATCH, (mi) => dispatchPositionsModel.PositionIDs.Contains(mi.PositionID));
                var movesSum = stMoves
                    .GroupBy(k => k.ArtPackID, (ArtPackID, items) => new { ArtPackID, Quantity = items.Sum(i => i.Quantity) })
                    .ToList();

                // compare
                bool noDifferences = dirSum.Count == movesSum.Count && !dirSum.Except(movesSum).Any();


                if (noDifferences)
                {
                    // set status
                    direction.DocStatus = Direction.DR_STATUS_DISPATCH_SAVED;

                    var history = new DirectionHistory()
                    {
                        DirectionID = direction.DirectionID,
                        EntryDateTime = DateTime.Now,
                        EntryUserID = _userID,
                        EventCode = Direction.DR_STATUS_DISPATCH_SAVED
                    };

                    using (var tr = db.GetTransaction())
                    {
                        //save direction
                        db.Update(direction);
                        direction.Items.ForEach(di => db.Update(di));

                        //insert history
                        db.Insert(history);

                        //trans complete
                        tr.Complete();
                    }
                }

                // is mandatory direction print [argomed modification]
                var isMandatoryDirectionPrint = (bool?) direction.XtraData[Direction.XTRA_MANDATORY_DIRECTION_PRINT];

                // log
                base.Info($"DispatchTestDirectProc; DirectionID: {direction.DirectionID}");

                return new DispatchTestDirect() { Success = true, IsCompleted = noDifferences, IsMandatoryDirectionPrint = isMandatoryDirectionPrint ?? false };
            }
        }
    }
}

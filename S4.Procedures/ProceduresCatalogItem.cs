﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Procedures
{
    public class ProceduresCatalogItem
    {
        public string Name { get; set; }
        public Type ParameterType { get; set; }
        public Type ProcedureType { get; set; }
    }
}

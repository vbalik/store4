﻿using S4.Core;
using S4.ProcedureModels.Booking;
using S4.StoreCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace S4.Procedures.Booking
{
    public class BookingFromReceiveProc : ProceduresBase<BookingFromReceive>
    {
        private const int DEFAULT_TIMEOUT_DAYS = 365;

        public BookingFromReceiveProc(string userID) : base(userID)
        {
        }


        protected override BookingFromReceive DoIt(BookingFromReceive procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new BookingFromReceive() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            if (procModel.BookingTimeout == null)
                // default timeout
                procModel.BookingTimeout = DateTime.Today.AddDays(DEFAULT_TIMEOUT_DAYS);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // check
                var direction = (new DAL.DirectionDAL()).Get(procModel.DirectionID);
                if (direction == null)
                    return new BookingFromReceive() { Success = false, ErrorText = $"Direction nebylo nalezeno; DirectionID: {procModel.DirectionID}" };

                if (direction.DocDirection != Entities.Direction.DOC_DIRECTION_IN)
                    return new BookingFromReceive() { Success = false, ErrorText = $"Doklad nemá očekávané DocDirection = DOC_DIRECTION_IN; DirectionID: {procModel.DirectionID}" };

                if (direction.DocStatus != Entities.Direction.DR_STATUS_STOIN_COMPLET)
                    return new BookingFromReceive() { Success = false, ErrorText = $"Doklad nemá očekávaný status DR_STATUS_STOIN_COMPLET; DirectionID: {procModel.DirectionID}" };

                var partner = Singleton<DAL.Cache.PartnerCache>.Instance.GetItem(procModel.PartnerID);
                if (partner == null)
                    return new BookingFromReceive() { Success = false, ErrorText = $"Partner nebyl nalezen; PartnerID: {procModel.PartnerID}" };

                // get receive content
                var save = Singleton<ServicesFactory>.Instance.GetSave();
                var moveResult = save.MoveResult_ByDirection(db, procModel.DirectionID, Entities.StoreMove.MO_DOCTYPE_RECEIVE, (mi) => true);

                var grouped = moveResult.GroupBy(_ => _.StoMoveLotID, (k, items) => new MoveResult()
                {
                    StoMoveLotID = k,
                    Quantity = items.Sum(i => i.Quantity)
                }).ToList();

                using (var tr = db.GetTransaction())
                {
                    foreach (var item in grouped)
                    {
                        var newItem = new Entities.Booking
                        {
                            StoMoveLotID = item.StoMoveLotID,
                            PartnerID = procModel.PartnerID,
                            PartAddrID = procModel.PartAddrID,
                            RequiredQuantity = item.Quantity,
                            BookingTimeout = procModel.BookingTimeout.Value,
                            BookingStatus = Entities.Booking.VALID,
                            EntryDateTime = DateTime.Now,
                            EntryUserID = _userID
                        };

                        db.Insert(newItem);
                    }

                    var hist = new Entities.DirectionHistory() 
                    { 
                        DirectionID = direction.DirectionID, 
                        EventCode = Entities.Direction.DR_EVENT_TO_BOOKINGS,
                        EventData = $"{partner.PartnerName} (id: {partner.PartnerID})",
                        EntryDateTime = DateTime.Now, 
                        EntryUserID = _userID 
                    };
                    db.Insert(hist);

                    // do save
                    tr.Complete();
                }

                return new BookingFromReceive()
                {
                    Success = true
                };
            }

        }
    }
}

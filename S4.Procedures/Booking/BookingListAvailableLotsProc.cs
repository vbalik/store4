﻿using S4.Core;
using S4.Entities;
using S4.ProcedureModels.Booking;
using S4.StoreCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace S4.Procedures.Booking
{
    public class BookingListAvailableLotsProc : ProceduresBase<BookingListAvailableLots>
    {

        public BookingListAvailableLotsProc(string userID) : base(userID)
        {
        }


        protected override BookingListAvailableLots DoIt(BookingListAvailableLots procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new BookingListAvailableLots() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var articlePacking = Singleton<DAL.Cache.ArticlePackingCache>.Instance.GetItem(procModel.ArtPackID);
                var onStoreItems = Singleton<ServicesFactory>.Instance.GetOnStore().ByArticle(articlePacking.ArticleID, ResultValidityEnum.Valid, true)
                    .Where(_ => _.ArtPackID == procModel.ArtPackID)
                    .ToList();

                // get on store
                var onStoreItemsGrouped = onStoreItems.GroupBy(
                    _ => _.StoMoveLotID,
                    (k, grp) => new OnStoreItemFull()
                    {
                        StoMoveLotID = k,
                        ArtPackID = grp.First().ArtPackID,
                        BatchNum = grp.First().BatchNum,
                        ExpirationDate = grp.First().ExpirationDate,
                        Quantity = grp.Sum(q => q.Quantity)
                    }).ToList();

                // get bookings
                var storeMoveLotIds = onStoreItemsGrouped.Select(_ => _.StoMoveLotID).ToList();
                var existingBookings = db.Query<Entities.Booking>()
                    .Where(_ => storeMoveLotIds.Contains(_.StoMoveLotID))
                    .Where(_ => _.BookingTimeout >= DateTime.Now)
                    .Where(_ => _.BookingStatus == Entities.Booking.VALID)
                    .ToList();

                var existingBookingsLotIds = existingBookings.Select(_ => _.StoMoveLotID).ToList();

                // create models - onlu "free" lots
                var storeMoveLotModels = onStoreItemsGrouped
                    .Where(_ => !existingBookingsLotIds.Contains(_.StoMoveLotID))
                    .Select(_ => new StoreMoveLotModel()
                    {
                        StoMoveLotID = _.StoMoveLotID,
                        ArtPackID = _.ArtPackID,
                        _batchNum = _.BatchNum,
                        _expirationDate = _.ExpirationDate ?? StoreMoveItem.EMPTY_EXPIRATION,
                        Quantity = _.Quantity,
                    }).ToList();

                return new BookingListAvailableLots()
                {
                    Success = true,
                    StoreMoveLots = storeMoveLotModels
                };
            }
        }
    }
}
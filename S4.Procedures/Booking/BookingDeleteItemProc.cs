﻿using S4.Entities;
using S4.ProcedureModels.Booking;
using System;

namespace S4.Procedures.Booking
{
    public class BookingDeleteItemProc : ProceduresBase<BookingDeleteItem>
    {

        public BookingDeleteItemProc(string userID) : base(userID)
        {
        }


        protected override BookingDeleteItem DoIt(BookingDeleteItem procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new BookingDeleteItem() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var booking = db.Query<Entities.Booking>()
                    .Where(_ => _.BookingID == procModel.BookingID)
                    .SingleOrDefault();

                if (booking == null)
                    return new BookingDeleteItem() { Success = false, ErrorText = $"Rezervace nenalezena" };

                booking.BookingStatus = Entities.Booking.DELETED;
                booking.DeletedByEntryUserID = _userID;
                booking.DeletedDateTime = DateTime.Now;

                db.Update(booking);

                return new BookingDeleteItem()
                {
                    Success = true
                };
            }

        }

    }

}

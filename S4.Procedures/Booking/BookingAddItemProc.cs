﻿using S4.Entities;
using S4.ProcedureModels.Booking;
using System;

namespace S4.Procedures.Booking
{
    public class BookingAddItemProc : ProceduresBase<BookingAddItem>
    {

        public BookingAddItemProc(string userID) : base(userID)
        {
        }


        protected override BookingAddItem DoIt(BookingAddItem procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new BookingAddItem() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };            

            if (procModel.BookingTimeout < DateTime.Now)
                return new BookingAddItem() { Success = false, ErrorText = $"Platnost rezervace musí být větší než dnes" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var storeMoveLot = db.Query<StoreMoveLot>()
                    .Where(_ => _.StoMoveLotID == procModel.StoMoveLotID)
                    .SingleOrDefault();

                if (storeMoveLot == null)
                    return new BookingAddItem() { Success = false, ErrorText = $"StoMoveLotID {procModel.StoMoveLotID} neexistuje" };

                var newItem = new Entities.Booking
                {
                    StoMoveLotID = procModel.StoMoveLotID,
                    PartnerID = procModel.PartnerID,
                    PartAddrID = procModel.PartAddrID,
                    RequiredQuantity = procModel.RequiredQuantity,
                    BookingTimeout = procModel.BookingTimeout,
                    BookingStatus = Entities.Booking.VALID,
                    EntryDateTime = DateTime.Now,
                    EntryUserID = _userID
                };

                db.Insert(newItem);

                return new BookingAddItem()
                {
                    Success = true,
                    BookingID = newItem.BookingID,
                };
            }

        }

    }

}

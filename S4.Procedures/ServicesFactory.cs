﻿using System;
using S4.Core.Interfaces;
using S4.DAL.Interfaces;
using S4.StoreCore.Interfaces;

namespace S4.Procedures
{
    public class ServicesFactory
    {
        public static System.IServiceProvider ServiceProvider = null;


        public IOnStore GetOnStore(bool useSummaryView = false)
        {
            if(useSummaryView)
                return new S4.StoreCore.OnStore_Summary.OnStore_Summary();
            else
                return new S4.StoreCore.OnStore_Basic.OnStore();
        }


        public ISave GetSave()
        {
            return new S4.StoreCore.Save_Basic.Save();
        }


        public IBookings GetBookings()
        {
            return new S4.DAL.BookingDAL();
        }


        public IBackgroundTaskQueue GetBackgroundTaskQueue()
        {
            return GetService<IBackgroundTaskQueue>();
        }

        public S4.Messages.IMessageRouter GetMessageRouter()
        {
            return GetService<S4.Messages.IMessageRouter>();
        }

        public S4.DeliveryServices.IDeliveryCommon GetDeliveryService()
        {
            return GetService<S4.DeliveryServices.IDeliveryCommon>();
        }


        private T GetService<T>()
        {
            if (ServiceProvider == null)
                throw new Exception("ServiceProvider is not set");

            T service = (T)ServiceProvider.GetService(typeof(T));
            if(service == null)
                throw new Exception($"Service {typeof(T)} is not available");

            return service;
        }
    }
}

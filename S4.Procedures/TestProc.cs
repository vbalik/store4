﻿using NPoco;
using S4.Core.PropertiesComparer;
using S4.Core.Utils;
using S4.Entities;
using S4.Entities.Helpers;
using S4.Entities.Helpers.Xml.Article;
using S4.Entities.XtraData;
using S4.ProcedureModels;
using S4.Procedures.Messages;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace S4.Procedures
{
    [RunAsync]
    public class TestProc : ProceduresBase<Test>
    {

        public TestProc(string userID) : base(userID)
        {
        }

        protected override Test DoIt(Test procModel)
        {
            if (procModel.Exchange)
            {
                SendMessageToExchange("Test");
                return new Test { Success = true };
            }
                

            var header = $"Testovací procedura {TaskID}";
            var collection = new List<int>();

            for (int i = 0; i < 14589; i++)
                collection.Add(i);

            var processed = 1;
            var progressUtils = new ProgressUtils();
            SendMessageToClient(header, $"Příprava ({collection.Count})", 0);
            foreach (var item in collection)
            {
                var step = progressUtils.GetProgressValue(processed, collection.Count, 10);
                if (step > 0)
                {
                    SendMessageToClient(header, $"({step}%) - {_userID} - {DateTime.Now.ToLongTimeString()}", step);
                }
                System.Threading.Thread.Sleep(1);
                processed++;
            }



            SendMessageToClient(header, "A to je konec", 100, NotifyType.Success);

            return new Test { Success = true };



        }



    }
}

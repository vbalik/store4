﻿using System;
using System.Collections.Generic;
using System.Text;
using NPoco;
using S4.Core;
using S4.Entities;
using S4.Report.Model.Dispatch;
using S4.StoreCore.Interfaces;
using System.Linq;
using combit.ListLabel24;
using S4.ProcedureModels.Pos;

namespace S4.Procedures.Pos
{
    public class PositionPrintLabelProc : ProceduresBase<PositionPrintLabel>
    {
        public PositionPrintLabelProc(string userID) : base(userID)
        {
        }

        protected override PositionPrintLabel DoIt(PositionPrintLabel procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new PositionPrintLabel() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                if (procModel.ReportID == 0)
                {
                    procModel.ReportID = Singleton<DAL.SettingsSet>.Instance.GetValue_Int32(Entities.SettingsDictionary.POSITION_LABEL_REPORT_ID);
                }

                // get position
                var position = (new DAL.PositionDAL()).Get(procModel.PositionID);
                if (position == null)
                    return new PositionPrintLabel() { Success = false, ErrorText = $"Pozice nebyla nalezena; PositionID: {procModel.PositionID}" };

                // get definition
                var printerLocation = Singleton<DAL.Cache.PrinterLocationCache>.Instance.GetItem(procModel.PrinterLocationID);
                if (printerLocation == null)
                    return new PositionPrintLabel() { Success = false, ErrorText = $"Nenalezena tiskárna. (PrinterLocationID: {procModel.PrinterLocationID})" };

                var reportDef = Singleton<DAL.Cache.ReportCache>.Instance.GetItem(procModel.ReportID);
                if (reportDef == null)
                    return new PositionPrintLabel() { Success = false, ErrorText = $"Nenalezen report. (ReportID: {procModel.ReportID}" };

                // print
                var report = new Report.BarCodeReport(reportDef.Template);
                var toPrint = report.Render(position);

                if (IsTesting)
                    RenderResult = toPrint;
                else
                    Report.Printers.PrintDirect.PrintRAW(printerLocation.PrinterPath, "PrintPositionLabel", toPrint);

                return new PositionPrintLabel() { Success = true };
            }


        }
    }

}

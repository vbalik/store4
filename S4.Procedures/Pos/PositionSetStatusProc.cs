﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using S4.Core.Utils;
using S4.Entities;
using S4.ProcedureModels.Pos;


namespace S4.Procedures.Pos
{
    [ChangesPositionStatusAttribute]
    public class PositionSetStatusProc : ProceduresBase<PositionSetStatus>
    {
        public PositionSetStatusProc(string userID) : base(userID)
        {
        }


        protected override PositionSetStatus DoIt(PositionSetStatus procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new PositionSetStatus() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //check
                var position = (new DAL.PositionDAL()).Get(procModel.PositionID);
                if (position == null)
                    return new PositionSetStatus() { Success = false, ErrorText = $"Pozice nebyla nalezena; PositionID: {procModel.PositionID}" };

                // make info
                var message = new InternMessage()
                {
                    MsgClass = InternMessage.POS_STATUS_CHANGE,
                    MsgSeverity = InternMessage.MessageSeverityEnum.Low,
                    MsgHeader = $"Změna stavu pozice: '{position.PosDesc}'",
                    MsgDateTime = DateTime.Now,
                    MsgUserID = _userID,
                    MsgText = JsonUtils.ToJsonString("positionID", procModel.PositionID, "oldStatus", position.PosStatus, "newStatus", procModel.Status)
                };

                // make history
                var history = new PositionHistory()
                {
                    PositionID = procModel.PositionID,
                    EventCode = Position.POS_EVENT_STATUS_CHANGED,
                    EventData = $"{position.PosStatus}:{procModel.Status}",
                    EntryUserID = _userID,
                    EntryDateTime = DateTime.Now
                };

                // save
                using (var tr = db.GetTransaction())
                {
                    // save 
                    position.PosStatus = procModel.Status;
                    db.Update(position);

                    //his
                    db.Insert(history);

                    // save info
                    db.Insert(message);

                    // do save
                    tr.Complete();
                }

                // log
                base.Info($"PositionSetStatusProc; PositionID: {procModel.PositionID}");

                return new PositionSetStatus() { Success = true };
            }
        }
    }
}

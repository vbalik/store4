﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPoco;
using S4.Core;
using S4.Entities;
using S4.ProcedureModels;
using S4.ProcedureModels.StoreIn;

namespace S4.Procedures.StoreIn
{
    public class StoreInSaveItemProc : ProceduresBase<StoreInSaveItem>
    {

        public StoreInSaveItemProc(string userID) : base(userID)
        {
        }


        protected override StoreInSaveItem DoIt(StoreInSaveItem procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new StoreInSaveItem() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // check
                var stMove = (new DAL.StoreMoveDAL()).GetStoreMoveAllData(procModel.StoMoveID);
                if (stMove == null)
                    return new StoreInSaveItem() { Success = false, ErrorText = $"StoreMove nebyl nalezen; StoMoveID: {procModel.StoMoveID}" };

                // do tests
                if (stMove.DocStatus != StoreMove.MO_STATUS_NEW_STOIN)
                    return new StoreInSaveItem() { Success = false, ErrorText = $"StoreMove nemá DocStatus MO_STATUS_NEW_STOIN; StoMoveID: {procModel.StoMoveID}" };

                using (var tr = db.GetTransaction())
                {
                    var save = Singleton<ServicesFactory>.Instance.GetSave();
                    var now = DateTime.Now;

                    // change items - final position
                    var itemsIn = (from i in stMove.Items where i.ItemDirection == StoreMoveItem.MI_DIRECTION_IN select i).ToList();
                    foreach (var item in itemsIn)
                    {
                        var doSave = false;

                        // check destination position
                        if (item.PositionID != procModel.FinalPositionID)
                        {
                            var pos1 = Singleton<DAL.Cache.PositionCache>.Instance.GetItem(item.PositionID);
                            var pos2 = Singleton<DAL.Cache.PositionCache>.Instance.GetItem(procModel.FinalPositionID);
                            if (pos1 != null && pos2 != null)
                            {
                                var changeDesc = $"{pos1.PosCode}|{pos2.PosCode}";
                                item.PositionID = procModel.FinalPositionID;

                                var storeMoveHistory = new StoreMoveHistory()
                                {
                                    EntryDateTime = now,
                                    EntryUserID = _userID,
                                    EventCode = StoreMove.MO_EVENT_STIN_POS_CHANGED,
                                    EventData = changeDesc,
                                    StoMoveID = item.StoMoveID
                                };

                                // save storeMoveHistory
                                db.Insert(storeMoveHistory);

                                doSave = true;
                            }
                        }

                        // set carrier num, if neccessary
                        if (procModel.FinalCarrierNum != null)
                        {
                            if (string.Compare(item.CarrierNum, procModel.FinalCarrierNum, true) != 0)
                            {
                                item.CarrierNum = procModel.FinalCarrierNum;
                                doSave = true;
                            }
                        }

                        // save, if needed
                        if (doSave)
                        {
                            // save item
                            db.Save(item);

                            // invalidate storage - there is a need for recalculating
                            save.InvalidateStorageLot(db, _userID, item.StoMoveLotID);
                        }
                    }

                    // if all items are valid ...
                    if (!stMove.Items.Where(i => i.ItemValidity == StoreMoveItem.MI_VALIDITY_NOT_VALID).Any())
                    {
                        // ... confirm storeMove
                        stMove.DocStatus = StoreMove.MO_STATUS_SAVED_STOIN;
                        db.Update(stMove);
                    }

                    // trans complete
                    tr.Complete();
                }

                return new StoreInSaveItem() { Success = true };
            }

        }

    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPoco;
using S4.Core;
using S4.Entities;
using S4.ProcedureModels.Extensions;
using S4.ProcedureModels;
using S4.ProcedureModels.StoreIn;

namespace S4.Procedures.StoreIn
{
    public class StoreInListDirectsProc : ProceduresBase<StoreInListDirects>
    {

        public StoreInListDirectsProc(string userID) : base(userID)
        {
        }


        protected override StoreInListDirects DoIt(StoreInListDirects procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new StoreInListDirects() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // get directions
                var dirs = (new DBHelpers.Directions()).ScanByAssigment(db, new List<string> { Direction.DR_STATUS_STOIN_WAIT, Direction.DR_STATUS_STOIN_PROC }, DirectionAssign.JOB_ID_STORE_IN, procModel.WorkerID);

                // to result
                var docs = dirs.Select(i => new Entities.Helpers.DocumentInfo()
                {
                    DocumentID = i.DirectionID,
                    DocumentName = i.DocInfo(),
                    DocumentDetail = Singleton<DAL.Cache.PartnerCache>.Instance.GetItem(i.PartnerID)?.PartnerName
                }).ToList();

                return new StoreInListDirects()
                {
                    Directions = docs,
                    Success = true
                };
            }

        }

    }

}

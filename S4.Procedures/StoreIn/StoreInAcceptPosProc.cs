﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPoco;
using S4.Core;
using S4.Entities;
using S4.ProcedureModels.Extensions;
using S4.ProcedureModels;
using S4.ProcedureModels.StoreIn;
using S4.StoreCore.Interfaces;

namespace S4.Procedures.StoreIn
{
    [CheckParamsDuplicity]
    public class StoreInAcceptPosProc : ProceduresBase<StoreInAcceptPos>
    {
        public StoreInAcceptPosProc(string userID) : base(userID)
        {
        }


        protected override StoreInAcceptPos DoIt(StoreInAcceptPos procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new StoreInAcceptPos() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            if (procModel.SrcPositionID == null || procModel.SrcPositionID == 0)
                procModel.SrcPositionID = Singleton<DAL.SettingsSet>.Instance.GetValue_Int32(SettingsDictionary.RECEIVE_POSITION_ID);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var direction = (new DAL.DirectionDAL()).GetDirectionAllData(procModel.DirectionID);
                if (direction == null)
                    return new StoreInAcceptPos() { Success = false, ErrorText = $"Doklad nebyl nalezen; DirectionID: {procModel.DirectionID}" };

                // do tests
                if (direction.DocDirection != Direction.DOC_DIRECTION_IN)
                    return new StoreInAcceptPos() { Success = false, ErrorText = $"Doklad nemá DocDirection DOC_DIRECTION_IN; DirectionID: {procModel.DirectionID}" };

                if (direction.DocStatus != Direction.DR_STATUS_STOIN_PROC && direction.DocStatus != Direction.DR_STATUS_STOIN_WAIT)
                    return new StoreInAcceptPos() { Success = false, ErrorText = $"Doklad nemá DocStatus DR_STATUS_STOIN_PROC, nebo DR_STATUS_STOIN_WAIT; DirectionID: {procModel.DirectionID}" };

                var onStore = Singleton<ServicesFactory>.Instance.GetOnStore();
                var carrItems = onStore.ScanSMPositionCarrierDirection((int) procModel.SrcPositionID, procModel.CarrierNum, procModel.DirectionID, true);
                if(carrItems.Count == 0)
                    return new StoreInAcceptPos() { Success = false, ErrorText = $"Paleta k naskladnění nebyla nalezena; carrierNum: {procModel.CarrierNum}" };

                // make saveItemList
                var storeMoveItems = carrItems
                    .Select(i => new MoveItem()
                    {
                        ArtPackID = i.ArtPackID,
                        BatchNum = i.BatchNum,
                        CarrierNum = procModel.CarrierNum,
                        ExpirationDate = i.ExpirationDate,
                        Quantity = i.Quantity,

                        SourcePositionID = (int) procModel.SrcPositionID,
                        DestinationPositionID = procModel.DestPositionID,

                        // find Parent_docPosition
                        Parent_docPosition = direction
                            .Items
                            .Where(di => di.ItemStatus != DirectionItem.DI_STATUS_CANCEL && di.ArtPackID == i.ArtPackID)
                            .FirstOrDefault()?.DocPosition ?? StoreMoveItem.NO_PARENT_DOC_POSITION
                    })
                    .ToList();

                int stoMoveID;
                using (var tr = db.GetTransaction())
                {
                    // make storeMove
                    stoMoveID = new StoreCore.Save_Basic.Save().Move(db, _userID, StoreMove.MO_PREFIX_STORE_IN, StoreMove.MO_DOCTYPE_STORE_IN, storeMoveItems, true, sm =>
                    {
                        sm.DocStatus = StoreMove.MO_STATUS_NEW_STOIN;
                        sm.Parent_directionID = direction.DirectionID;
                    },
                    null,
                    out string msg);
                    if (stoMoveID == -1)
                        return new StoreInAcceptPos() { ErrorText = msg };

                    // create history
                    var hist = new StoreMoveHistory()
                    {
                        EntryDateTime = DateTime.Now,
                        EntryUserID = _userID,
                        EventCode = StoreMove.MO_STATUS_NEW_STOIN,
                        StoMoveID = stoMoveID
                    };
                    db.Insert(hist);

                    // commit changes
                    tr.Complete();
                }

                // log
                base.Info($"StoreInAcceptPosProc; StoMoveID: {stoMoveID}");
                return new StoreInAcceptPos() { Success = true, StoMoveID = stoMoveID };
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPoco;
using S4.Entities;
using S4.ProcedureModels;
using S4.ProcedureModels.StoreIn;

namespace S4.Procedures.StoreIn
{
    [ChangesDirectionStatus]
    public class StoreInProcProc : ProceduresBase<StoreInProc>
    {

        public StoreInProcProc(string userID) : base(userID)
        {
        }


        protected override StoreInProc DoIt(StoreInProc procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new StoreInProc() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //load
                var direction = (new DAL.DirectionDAL()).GetDirectionAllData(procModel.DirectionID);
                if (direction == null)
                    return new StoreInProc() { Success = false, ErrorText = $"Doklad nebyl nalezen; DirectionID: {procModel.DirectionID}" };

                // do tests
                if (direction.DocDirection != Direction.DOC_DIRECTION_IN)
                    return new StoreInProc() { Success = false, ErrorText = $"Doklad nemá DocDirection DOC_DIRECTION_IN; DirectionID: {procModel.DirectionID}" };

                if (direction.DocStatus != Direction.DR_STATUS_STOIN_PROC && direction.DocStatus != Direction.DR_STATUS_STOIN_WAIT)
                    return new StoreInProc() { Success = false, ErrorText = $"Doklad nemá DocStatus DR_STATUS_STOIN_PROC, nebo DR_STATUS_STOIN_WAIT; DirectionID: {procModel.DirectionID}" };

                
                using (var tr = db.GetTransaction())
                {

                    // change direction status
                    DirectionHistory history = null;
                    if (direction.DocStatus != Direction.DR_STATUS_STOIN_PROC)
                    { 
                        direction.DocStatus = Direction.DR_STATUS_STOIN_PROC;
                        db.Save(direction);
                    }
                    else
                    {
                        // make history
                        history = new DirectionHistory()
                        {
                            DirectionID = procModel.DirectionID,
                            DocPosition = 0,
                            EventCode = Direction.DR_STATUS_STOIN_PROC,
                            EntryUserID = _userID,
                            EntryDateTime = DateTime.Now
                        };
                        db.Save(history);
                    }

                    // cancel unsaved stMoves
                    var storeSave = (new ServicesFactory()).GetSave();
                    if (!storeSave.CancelByDirection(db, _userID, direction.DirectionID, true, out string msg))
                        return new StoreInProc() { ErrorText = msg };

                    //trans complete
                    tr.Complete();
                }

                return new StoreInProc() { Success = true };
            }

        }

    }

}

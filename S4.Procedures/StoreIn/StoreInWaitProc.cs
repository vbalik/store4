﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPoco;
using S4.Entities;
using S4.ProcedureModels;
using S4.ProcedureModels.StoreIn;

namespace S4.Procedures.StoreIn
{
    [ChangesDirectionStatus]
    [ChangesDirectionAssignment]
    public class StoreInWaitProc : ProceduresBase<StoreInWait>
    {

        public StoreInWaitProc(string userID) : base(userID)
        {
        }


        protected override StoreInWait DoIt(StoreInWait procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new StoreInWait() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //load
                var direction = (new DAL.DirectionDAL()).GetDirectionAllData(procModel.DirectionID);
                if (direction == null)
                    return new StoreInWait() { Success = false, ErrorText = $"Doklad nebyl nalezen; DirectionID: {procModel.DirectionID}" };

                // do tests
                if (direction.DocDirection != Direction.DOC_DIRECTION_IN)
                    return new StoreInWait() { Success = false, ErrorText = $"Doklad nemá DocDirection DOC_DIRECTION_IN; DirectionID: {procModel.DirectionID}" };

                if (direction.DocStatus != Direction.DR_STATUS_RECEIVE_COMPLET && direction.DocStatus != Direction.DR_STATUS_STOIN_WAIT)
                    return new StoreInWait() { Success = false, ErrorText = $"Doklad nemá DocStatus DR_STATUS_RECEIVE_COMPLET, nebo DR_STATUS_STOIN_WAIT; DirectionID: {procModel.DirectionID}" };

                // change status
                direction.DocStatus = Direction.DR_STATUS_STOIN_WAIT;

                // assign worker
                var directionAssign = new DirectionAssign()
                {
                    JobID = DirectionAssign.JOB_ID_STORE_IN,
                    WorkerID = procModel.WorkerID,
                    EntryUserID = _userID,
                    EntryDateTime = DateTime.Now,
                    DirectionID = direction.DirectionID
                };

                // make history
                var history = new DirectionHistory()
                {
                    DirectionID = procModel.DirectionID,
                    DocPosition = 0,
                    EventCode = Direction.DR_STATUS_STOIN_WAIT,
                    EntryUserID = _userID,
                    EntryDateTime = DateTime.Now
                };

                using (var tr = db.GetTransaction())
                {
                    // remove old asignments
                    db.Delete<DirectionAssign>("where directionid = @0 AND jobID = @1", direction.DirectionID, DirectionAssign.JOB_ID_STORE_IN);

                    //save assign worker
                    db.Save(directionAssign);

                    //save direction
                    db.Save(direction);

                    //save history
                    db.Save(history);

                    //trans complete
                    tr.Complete();
                }
                 
                return new StoreInWait() { Success = true };
            }

        }

    }

}

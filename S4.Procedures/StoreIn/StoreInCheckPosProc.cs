﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using S4.Entities;
using S4.ProcedureModels.StoreIn;

namespace S4.Procedures.StoreIn
{
    public class StoreInCheckPosProc : ProceduresBase<StoreInCheckPos>
    {
        public StoreInCheckPosProc(string userID) : base(userID)
        {
        }

        protected override StoreInCheckPos DoIt(StoreInCheckPos procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new StoreInCheckPos() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var position = Core.Singleton<DAL.Cache.PositionCache>.Instance.GetItem(procModel.PositionID);

                if (position.PosCateg != 0)
                {
                    return new StoreInCheckPos() { Enabled = StoreInCheckPos.PosEnabled.No, ResReason = "Nejde o skladovou pozici.", Success = true };
                }

                if (position.PosStatus != Position.POS_STATUS_OK)
                {
                    return new StoreInCheckPos() { Enabled = StoreInCheckPos.PosEnabled.No, ResReason = "Pozice je uzamčena.", Success = true };
                }

                var articlesOnPos = new StoreCore.OnStore_Basic.OnStore().OnPosition(procModel.PositionID, StoreCore.Interfaces.ResultValidityEnum.Valid);

                var result = new StoreInCheckPos() { Success = true, Enabled = StoreInCheckPos.PosEnabled.Yes };

                if (articlesOnPos.Any())
                {
                    if(articlesOnPos.Where(i => i.ArtPackID != procModel.ArtPackID).Any())
                    {
                        result.Enabled = StoreInCheckPos.PosEnabled.Warn;
                        result.ResReason = "Na pozici je jiné zboží.";
                    }
                    else
                    {
                        result.Enabled = StoreInCheckPos.PosEnabled.Warn;
                        result.ResReason = "Pozice se používá.";
                    }
                }

                return result;
            }
        }
    }
}

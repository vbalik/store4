﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using S4.Core.Utils;
using S4.Entities;
using S4.ProcedureModels.StoreIn;

namespace S4.Procedures.StoreIn
{
    public class StoreInPosErrorProc : ProceduresBase<StoreInPosError>
    {
        public StoreInPosErrorProc(string userID) : base(userID)
        {
        }

        protected override StoreInPosError DoIt(StoreInPosError procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new StoreInPosError() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //check
                var storeMove = db.SingleOrDefault<StoreMove>("where stoMoveID = @0", procModel.StoMoveID);
                if (storeMove == null)
                    return new StoreInPosError() { Success = false, ErrorText = $"StoreMove nebylo nalezeno; StMoveID: {procModel.StoMoveID}" };

                if (storeMove.DocStatus != StoreMove.MO_STATUS_NEW_STOIN)
                    return new StoreInPosError() { Success = false, ErrorText = $"Doklad nemá očekávaný status MO_STATUS_NEW_STOIN; StoMoveID: {procModel.StoMoveID}" };

                var position = Core.Singleton<DAL.Cache.PositionCache>.Instance.GetItem(procModel.PositionID);

                // make info
                var message = new InternMessage()
                {
                    MsgClass = InternMessage.POS_STATUS_BLOCKED,
                    MsgSeverity = InternMessage.MessageSeverityEnum.Low,
                    MsgHeader = $"Pozice '{position.PosCode}' byla uzamčena",
                    MsgText = JsonUtils.ToJsonString("positionID", position.PositionID, "reason", procModel.Reason),
                    MsgDateTime = DateTime.Now,
                    MsgUserID = _userID
                };

                // create storemove history
                var storeMoveHistory = new StoreMoveHistory()
                {
                    StoMoveID = procModel.StoMoveID,
                    EntryDateTime = DateTime.Now,
                    EntryUserID = _userID,
                    EventCode = StoreMove.MO_EVENT_DISP_POS_ERROR,
                    EventData = $"{position.PositionID}|{position.PosCode}|{procModel.Reason}"
                };

                // create direction history
                var directionHistory = new DirectionHistory()
                {
                    DirectionID = storeMove.Parent_directionID,
                    EntryDateTime = DateTime.Now,
                    EntryUserID = _userID,
                    EventCode = Direction.DR_EVENT_POSITION_ERROR,
                    EventData = $"{position.PositionID}|{position.PosCode}|{procModel.Reason}"
                };

                using (var tr = db.GetTransaction())
                {
                    // save 
                    position.PosStatus = Position.POS_STATUS_BLOCKED;
                    db.Update(position);

                    // save info
                    db.Insert(message);

                    // insert history
                    db.Insert(storeMoveHistory);
                    db.Insert(directionHistory);

                    // do save
                    tr.Complete();
                }

                // log
                base.Info($"StoreInPosErrorProc; PositionID: {position.PositionID}");

                return new StoreInPosError() { Success = true };
            }
        }
    }
}

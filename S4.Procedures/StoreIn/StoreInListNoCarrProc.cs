﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using S4.Entities;
using S4.ProcedureModels.StoreIn;
using S4.Core.Data;

namespace S4.Procedures.StoreIn
{
    public class StoreInListNoCarrProc : ProceduresBase<StoreInListNoCarr>
    {
        public StoreInListNoCarrProc(string userID) : base(userID)
        {
        }

        protected override StoreInListNoCarr DoIt(StoreInListNoCarr procModel)
        {
            //check required params
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new StoreInListNoCarr() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            //load pos content
            var posContent = new StoreCore.OnStore_Basic.OnStore().ScanSMPositionCarrierDirection(procModel.SrcPositionID, StoreMoveItem.EMPTY_CARRIERNUM, procModel.DirectionID, true);
            if (posContent.Count == 0)
            {
                return new StoreInListNoCarr() { ArticleList = new List<Entities.Helpers.ItemInfo> { }, Success = true };
            }

            //fill items list
            var proxy = new EmptyDataProxy();

            var itemInfoList = new List<Entities.Helpers.ItemInfo>();

            posContent.ForEach(i =>
            {
                proxy.ToShow(i.BatchNum, StoreMoveItem.EMPTY_BATCHNUM, value => i.BatchNum = value);
                proxy.ToShow(i.ExpirationDate, StoreMoveItem.EMPTY_EXPIRATION, value => i.ExpirationDate = value);

                itemInfoList.Add(new Entities.Helpers.ItemInfo()
                {
                    ArtPackID = i.ArtPackID,
                    BatchNum = i.BatchNum,
                    ExpirationDate = i.ExpirationDate,
                    Quantity = i.Quantity
                });
            });

            itemInfoList.ForEach(DBHelpers.Articles.FillArticleData);

            return new StoreInListNoCarr() { Success = true, ArticleList = itemInfoList };
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using S4.Core;
using S4.Entities;
using S4.ProcedureModels.StoreIn;

namespace S4.Procedures.StoreIn
{
    [ChangesDirectionStatus]
    public class StoreInTestDirectProc : ProceduresBase<StoreInTestDirect>
    {
        public StoreInTestDirectProc(string userID) : base(userID)
        {
        }

        protected override StoreInTestDirect DoIt(StoreInTestDirect procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new StoreInTestDirect() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // check
                var direction = (new DAL.DirectionDAL()).GetDirectionAllData(procModel.DirectionID);
                if (direction == null)
                    return new StoreInTestDirect() { Success = false, ErrorText = $"Direction nebylo nalezeno; DirectionID: {procModel.DirectionID}" };

                if (direction.DocStatus != Direction.DR_STATUS_STOIN_PROC)
                    return new StoreInTestDirect() { Success = false, ErrorText = $"Příkaz nemá očekávaný status DR_STATUS_STOIN_PROC; DirectionID: {procModel.DirectionID}" };


                // get summary of store docs by article
                var dirSum = direction.Items
                    .Where(i => i.ItemStatus != DirectionItem.DI_STATUS_CANCEL)
                    .GroupBy(k => k.ArtPackID, (ArtPackID, items) => new { ArtPackID, Quantity = items.Sum(i => i.Quantity) })
                    .ToList();

                // get summary of STOREIN store moves by article
                var save = Singleton<ServicesFactory>.Instance.GetSave();
                var receivePositionID = Singleton<DAL.SettingsSet>.Instance.GetValue_Int32(SettingsDictionary.RECEIVE_POSITION_ID);
                var stMoves = save.MoveResult_ByDirection(db, procModel.DirectionID, StoreMove.MO_DOCTYPE_STORE_IN, (mi) => mi.PositionID == receivePositionID);
                var movesSum = stMoves
                    .GroupBy(k => k.ArtPackID, (ArtPackID, items) => new { ArtPackID, Quantity = -items.Sum(i => i.Quantity) })
                    .ToList();

                // compare
                bool noDifferences = dirSum.Count == movesSum.Count && !dirSum.Except(movesSum).Any();


                if (noDifferences)
                {
                    // set status
                    direction.DocStatus = Direction.DR_STATUS_STOIN_COMPLET;
                    direction.Items.ForEach(di =>
                    {
                        if (di.ItemStatus != DirectionItem.DI_STATUS_CANCEL)
                            di.ItemStatus = DirectionItem.DI_STATUS_STOIN_COMPLET;
                    });

                    var history = new DirectionHistory()
                    {
                        DirectionID = direction.DirectionID,
                        EntryDateTime = DateTime.Now,
                        EntryUserID = _userID,
                        EventCode = Direction.DR_STATUS_STOIN_COMPLET
                    };

                    using (var tr = db.GetTransaction())
                    {
                        //save direction
                        db.Update(direction);
                        direction.Items.ForEach(di => db.Update(di));

                        //insert history
                        db.Insert(history);

                        //trans complete
                        tr.Complete();
                    }
                }

                // log
                base.Info($"StoreInTestDirectProc; DirectionID: {direction.DirectionID}");

                return new StoreInTestDirect() { Success = true, IsCompleted = noDifferences };
            }
        }
    }
}

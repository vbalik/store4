﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using S4.Core.Utils;
using S4.Entities;
using S4.ProcedureModels.StoreIn;

namespace S4.Procedures.StoreIn
{
    public class StoreInGetPosNoCarrProc : ProceduresBase<StoreInGetPosNoCarr>
    {
        public StoreInGetPosNoCarrProc(string userID) : base(userID)
        {
        }

        protected override StoreInGetPosNoCarr DoIt(StoreInGetPosNoCarr procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new StoreInGetPosNoCarr() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var articlePack = Core.Singleton<DAL.Cache.ArticlePackingCache>.Instance.GetItem(procModel.ArtPackID);
                var article = Core.Singleton<DAL.Cache.ArticleModelCache>.Instance.GetItem(articlePack.ArticleID);
                if (string.IsNullOrEmpty(article.SectID))
                {
                    return new StoreInGetPosNoCarr() { ErrorText = "Zboží nemá nastavenou sekci!" };
                }

                int destPositionID = new DAL.PositionDAL().ScanFreePosition(article.SectID, 0);
                if (destPositionID == -1)
                {
                    // make info
                    var message = new InternMessage()
                    {
                        MsgClass = InternMessage.POS_NOT_FOUND,
                        MsgSeverity = InternMessage.MessageSeverityEnum.Low,
                        MsgHeader = $"Nebyla nalezena volná pozice",
                        MsgDateTime = DateTime.Now,
                        MsgUserID = _userID,
                        MsgText = JsonUtils.ToJsonString("articleCode", article.ArticleCode, "articleDesc", article.ArticleDesc, "sectID", article.SectID)
                    };

                    return new StoreInGetPosNoCarr() { Success = true, IsPositionFound = false };
                }

                return new StoreInGetPosNoCarr() { Success = true, IsPositionFound = true, DestPosition = db.Single<Position>("where positionID = @0", destPositionID) };
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPoco;
using S4.Core;
using S4.Entities;
using S4.ProcedureModels.Extensions;
using S4.ProcedureModels;
using S4.ProcedureModels.StoreIn;
using S4.StoreCore.Interfaces;

namespace S4.Procedures.StoreIn
{
    public class StoreInAcceptPosNoCarrProc : ProceduresBase<StoreInAcceptPosNoCarr>
    {

        public StoreInAcceptPosNoCarrProc(string userID) : base(userID)
        {
        }


        protected override StoreInAcceptPosNoCarr DoIt(StoreInAcceptPosNoCarr procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new StoreInAcceptPosNoCarr() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            if (procModel.SrcPositionID == null || procModel.SrcPositionID == 0)
                procModel.SrcPositionID = Singleton<DAL.SettingsSet>.Instance.GetValue_Int32(SettingsDictionary.RECEIVE_POSITION_ID);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var direction = (new DAL.DirectionDAL()).GetDirectionAllData(procModel.DirectionID);
                if (direction == null)
                    return new StoreInAcceptPosNoCarr() { Success = false, ErrorText = $"Doklad nebyl nalezen; DirectionID: {procModel.DirectionID}" };
                
                // do tests
                if (direction.DocDirection != Direction.DOC_DIRECTION_IN)
                    return new StoreInAcceptPosNoCarr() { Success = false, ErrorText = $"Doklad nemá DocDirection DOC_DIRECTION_IN; DirectionID: {procModel.DirectionID}" };

                if (direction.DocStatus != Direction.DR_STATUS_STOIN_PROC && direction.DocStatus != Direction.DR_STATUS_STOIN_WAIT)
                    return new StoreInAcceptPosNoCarr() { Success = false, ErrorText = $"Doklad nemá DocStatus DR_STATUS_STOIN_PROC, nebo DR_STATUS_STOIN_WAIT; DirectionID: {procModel.DirectionID}" };

                var now = DateTime.Now;

                //make storemove history
                var storeMoveHistory = new StoreMoveHistory()
                {
                    EntryDateTime = now,
                    EntryUserID = _userID,
                    EventCode = StoreMove.MO_STATUS_NEW_STOIN
                };

                // make saveItemList
                // find Parent_docPosition
                int parentDocPosition = StoreMoveItem.NO_PARENT_DOC_POSITION;
                var parentItems = direction.Items.Where(di => di.ItemStatus != DirectionItem.DI_STATUS_CANCEL && di.ArtPackID == procModel.Article.ArtPackID && di.Quantity == procModel.Article.Quantity).ToList();
                if (parentItems.Any())
                    parentDocPosition = parentItems[0].DocPosition;
                else
                {
                    parentItems = direction.Items.Where(di => di.ItemStatus != DirectionItem.DI_STATUS_CANCEL && di.ArtPackID == procModel.Article.ArtPackID).ToList();
                    if (parentItems.Any())
                        parentDocPosition = parentItems[0].DocPosition;
                }

                var saveItem = new MoveItem()
                {
                    ArtPackID = procModel.Article.ArtPackID,
                    BatchNum = procModel.Article.BatchNum,
                    CarrierNum = procModel.Article.CarrierNum,
                    ExpirationDate = procModel.Article.ExpirationDate,
                    Quantity = procModel.Article.Quantity,

                    SourcePositionID = (int) procModel.SrcPositionID,
                    DestinationPositionID = procModel.DestPositionID,

                    Parent_docPosition = parentDocPosition
                };

                var saveItemList = new List<MoveItem>();
                saveItemList.Add(saveItem);
                              
                var stoMoveID = 0;
                using (var tr = db.GetTransaction())
                {
                    var save = Singleton<ServicesFactory>.Instance.GetSave();
                    stoMoveID = save.Move(db, _userID, StoreMove.MO_PREFIX_STORE_IN, StoreMove.MO_DOCTYPE_STORE_IN, saveItemList, true, sm =>
                    {
                        sm.DocStatus = StoreMove.MO_STATUS_NEW_STOIN;
                        sm.Parent_directionID = direction.DirectionID;
                    },
                    null,
                    out string msg);
                    if (stoMoveID == -1)
                        return new StoreInAcceptPosNoCarr() { ErrorText = msg };

                    //save history
                    storeMoveHistory.StoMoveID = stoMoveID;
                    db.Save(storeMoveHistory);

                    tr.Complete();
                }



                return new StoreInAcceptPosNoCarr()
                {
                    StoMoveID = stoMoveID,
                    Success = true
                };
            }

        }

    }

}

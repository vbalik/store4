﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using S4.Entities;
using S4.ProcedureModels.StoreIn;
using S4.Core;

namespace S4.Procedures.StoreIn
{
    public class StoreInCheckCarrierProc : ProceduresBase<StoreInCheckCarrier>
    {
        public StoreInCheckCarrierProc(string userID) : base(userID)
        {
        }

        protected override StoreInCheckCarrier DoIt(StoreInCheckCarrier procModel)
        {
            // check required params
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new StoreInCheckCarrier() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            //l oad
            var direction = (new DAL.DirectionDAL()).Get(procModel.DirectionID);
            if (direction == null)
                return new StoreInCheckCarrier() { Success = false, ErrorText = $"Doklad nebyl nalezen; DirectionID: {procModel.DirectionID}" };

            // do tests
            if (direction.DocDirection != Direction.DOC_DIRECTION_IN)
                return new StoreInCheckCarrier() { Success = false, ErrorText = $"Doklad nemá DocDirection DOC_DIRECTION_IN; DirectionID: {procModel.DirectionID}" };

            if (direction.DocStatus != Direction.DR_STATUS_STOIN_PROC && direction.DocStatus != Direction.DR_STATUS_STOIN_WAIT)
                return new StoreInCheckCarrier() { Success = false, ErrorText = $"Doklad nemá DocStatus DR_STATUS_STOIN_PROC, nebo DR_STATUS_STOIN_WAIT; DirectionID: {procModel.DirectionID}" };


            // load pos content
            var onStore = Singleton<ServicesFactory>.Instance.GetOnStore();
            var posContent = onStore.ScanSMPositionDirection(procModel.SrcPositionID, procModel.DirectionID, true);
            // filter by positive quantity - to prevent repeated operations https://promedica.atlassian.net/browse/STO4-632
            posContent = posContent.Where(_ => _.Quantity > 0).ToList();

            // fill items list
            var itemInfoList = posContent
                .Where(i => i.CarrierNum == procModel.CarrierNum)
                .Select(i => new Entities.Helpers.ItemInfo()
                {
                    ArtPackID = i.ArtPackID,
                    BatchNum = i.BatchNum,
                    ExpirationDate = i.ExpirationDate,
                    Quantity = i.Quantity
                })
                .ToList();
            itemInfoList.ForEach(DBHelpers.Articles.FillArticleData);

            return new StoreInCheckCarrier() { Success = true, IsCarrierFound = (itemInfoList.Count > 0), ArticleList = itemInfoList };
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPoco;
using S4.Entities;
using S4.ProcedureModels;
using S4.ProcedureModels.StoreIn;

namespace S4.Procedures.StoreIn
{
    public class StoreInCancelItemProc : ProceduresBase<StoreInCancelItem>
    {

        public StoreInCancelItemProc(string userID) : base(userID)
        {
        }


        protected override StoreInCancelItem DoIt(StoreInCancelItem procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new StoreInCancelItem() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //load
                var stMove = (new DAL.StoreMoveDAL()).GetStoreMoveAllData(procModel.StoMoveID);
                if (stMove == null)
                    return new StoreInCancelItem() { Success = false, ErrorText = $"StoreMove nebyl nalezen; StoMoveID: {procModel.StoMoveID}" };

                // do tests
                if (stMove.DocStatus != StoreMove.MO_STATUS_NEW_STOIN)
                    return new StoreInCancelItem() { Success = false, ErrorText = $"StoreMove nemá DocStatus MO_STATUS_NEW_STOIN; StoMoveID: {procModel.StoMoveID}" };

                // cancel stMove items
                foreach (var item in stMove.Items)
                    item.ItemValidity = StoreMoveItem.MI_VALIDITY_CANCELED;

                // cancel storeMove
                stMove.DocStatus = StoreMove.MO_STATUS_CANCEL;
                
                using (var tr = db.GetTransaction())
                {
                    //save stMove
                    db.Save(stMove);

                    //save items
                    foreach (var item in stMove.Items)
                        db.Save(item);

                    //trans complete
                    tr.Complete();
                }

                // log
                base.Info($"StoreInCancelItemProc; StoMoveID: {stMove.StoMoveID}");
                return new StoreInCancelItem() { Success = true };
            }

        }

    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using S4.DAL;
using S4.Entities;
using S4.Entities.Helpers;
using S4.ProcedureModels.DispCheck;

namespace S4.Procedures.DispCheck
{
    /// <summary>
    /// Find dispatch check directions available by abraDocID, docInfo, invoiceNumber" in docStatus DR_STATUS_DISPATCH_SAVED
    /// 
    /// 
    /// </summary>
    public class DispCheckAvailableDirectionsDocNumberProc : ProceduresBase<DispCheckAvailableDirectionsDocNumber>
    {
        public DispCheckAvailableDirectionsDocNumberProc(string userID) : base(userID)
        {
        }


        protected override DispCheckAvailableDirectionsDocNumber DoIt(DispCheckAvailableDirectionsDocNumber procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new DispCheckAvailableDirectionsDocNumber() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };
                       

            var documentInfos = new List<DocumentInfo>();

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // 1) find by abraDocID
                var abraDocList = new DirectionDAL().GetDirectionsByAbraDocID(procModel.DocumentID);

                documentInfos.AddRange(abraDocList.Where(_ => _.DocDirection == Direction.DOC_DIRECTION_OUT && _.DocStatus == Direction.DR_STATUS_DISPATCH_SAVED)
                    .Select(_ => new DocumentInfo
                    {
                        DocumentID = _.DirectionID,
                        DocumentName = _.DocInfo
                    })
                    .ToList());

                if (documentInfos.Any())
                    return new DispCheckAvailableDirectionsDocNumber { Success = true, Directions = documentInfos };


                // 2) find by docInfo
                var docInfoList = new DirectionDAL().FindDirectionsByDocInfo(procModel.DocumentID, null);

                documentInfos.AddRange(docInfoList.Where(x => x.DocDirection == Direction.DOC_DIRECTION_OUT && x.DocStatus == Direction.DR_STATUS_DISPATCH_SAVED)
                    .Select(_ => new DocumentInfo
                    {
                        DocumentID = _.DirectionID,
                        DocumentName = _.DocInfo
                    })
                    .ToList());

                if (documentInfos.Any())
                    return new DispCheckAvailableDirectionsDocNumber { Success = true, Directions = documentInfos };

                // 3) find by invoice number
                var invoiceList = new DirectionDAL().GetDirectionByInvoiceNumber(procModel.DocumentID);

                documentInfos.AddRange(invoiceList.Where(x => x.DocDirection == Direction.DOC_DIRECTION_OUT && x.DocStatus == Direction.DR_STATUS_DISPATCH_SAVED)
                    .Select(_ => new DocumentInfo
                    {
                        DocumentID = _.DirectionID,
                        DocumentName = _.DocInfo
                    })
                    .ToList());

                return new DispCheckAvailableDirectionsDocNumber { Success = true, Directions = documentInfos };
            }
        }
    }
}

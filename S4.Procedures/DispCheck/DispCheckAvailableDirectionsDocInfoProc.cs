﻿using System;
using System.Linq;
using S4.Entities;
using S4.Entities.Helpers;
using S4.ProcedureModels.DispCheck;

namespace S4.Procedures.DispCheck
{
    /// DEPRECATED 28.12 v1.46
    public class DispCheckAvailableDirectionsDocInfoProc : ProceduresBase<DispCheckAvailableDirectionsDocInfo>
    {
        public DispCheckAvailableDirectionsDocInfoProc(string userID) : base(userID)
        {
        }


        protected override DispCheckAvailableDirectionsDocInfo DoIt(DispCheckAvailableDirectionsDocInfo procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new DispCheckAvailableDirectionsDocInfo() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var list = new DAL.DirectionDAL().FindDirectionsByDocInfo(procModel.DocInfo, procModel.DocYear);

                // filter by status
                var directionsResult = list.Where(_ => _.DocDirection == Direction.DOC_DIRECTION_OUT && _.DocStatus == Direction.DR_STATUS_DISPATCH_SAVED)
                    .Select(_ => new DocumentInfo {
                        DocumentID = _.DirectionID,
                        DocumentName = _.DocInfo
                    })
                    .ToList();
                
                return new DispCheckAvailableDirectionsDocInfo() {
                    Success = true,
                    Directions = directionsResult
                };
            }
        }
    }
}

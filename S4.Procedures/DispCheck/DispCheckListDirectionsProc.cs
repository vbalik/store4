﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using S4.Core;
using S4.Entities;
using S4.ProcedureModels.Extensions;
using S4.ProcedureModels.DispCheck;


namespace S4.Procedures.DispCheck
{
    public class DispCheckListDirectionsProc : ProceduresBase<DispCheckListDirections>
    {
        public DispCheckListDirectionsProc(string userID) : base(userID)
        {
        }


        protected override DispCheckListDirections DoIt(DispCheckListDirections procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new DispCheckListDirections() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // get directions
                var dirs = (new DBHelpers.Directions()).ScanByAssigment(db, new List<string> { Direction.DR_STATUS_DISP_CHECK_WAIT, Direction.DR_STATUS_DISP_CHECK_PROC }, DirectionAssign.JOB_ID_DISPATCH_CHECK, procModel.WorkerID);

                // to result
                var docs = dirs.Select(i => new Entities.Helpers.DocumentInfo()
                {
                    DocumentID = i.DirectionID,
                    DocumentName = i.DocInfo(),
                    DocumentDetail = Singleton<DAL.Cache.PartnerCache>.Instance.GetItem(i.PartnerID)?.PartnerName,
                    DispatchPosition = _GetDispatchPosition(i)
                }).ToList();

                return new DispCheckListDirections() { Directions = docs, Success = true };
            }
        }

        private Position _GetDispatchPosition(Direction direction)
        {
            var xtraData = new DAL.DirectionDAL().GetDirectionXtraData(direction.DirectionID);
            var positionID = xtraData.XtraData[Direction.XTRA_DISPATCH_POSITION_ID];

            if(positionID != null)
                return Singleton<DAL.Cache.PositionCache>.Instance.GetItem((int) positionID);

            return null;
        }
    }
}

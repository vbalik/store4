﻿using S4.Entities;
using S4.ProcedureModels.DispCheck;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Procedures.DispCheck
{
    [ChangesDirectionStatus]
    public class DispCheckSkipProc : ProceduresBase<DispCheckSkip>
    {
        public DispCheckSkipProc(string userID) : base(userID)
        {
        }


        protected override DispCheckSkip DoIt(DispCheckSkip procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new DispCheckSkip() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // check
                var direction = (new DAL.DirectionDAL()).GetDirectionAllData(procModel.DirectionID);
                if (direction == null)
                    return new DispCheckSkip() { Success = false, ErrorText = $"Doklad nebyl nalezen; DirectionID: {procModel.DirectionID}" };

                // do tests
                if (direction.DocDirection != Direction.DOC_DIRECTION_OUT)
                    return new DispCheckSkip() { Success = false, ErrorText = $"Doklad nemá DocDirection DOC_DIRECTION_OUT; DirectionID: {procModel.DirectionID}" };

                if (direction.DocStatus != Direction.DR_STATUS_DISPATCH_SAVED)
                    return new DispCheckSkip() { Success = false, ErrorText = $"Doklad nemá DocStatus DR_STATUS_DISPATCH_SAVED; DirectionID: {procModel.DirectionID}" };


                // change status
                direction.DocStatus = Direction.DR_STATUS_DISP_CHECK_DONE;

                // make history
                var history = new DirectionHistory()
                {
                    DirectionID = procModel.DirectionID,
                    DocPosition = 0,
                    EventCode = Direction.DR_EVENT_CHECK_SKIPPED,
                    EventData = procModel.Reason,
                    EntryUserID = _userID,
                    EntryDateTime = DateTime.Now
                };

                // save
                using (var tr = db.GetTransaction())
                {
                    // save 
                    db.Update(direction);

                    // save history
                    db.Insert(history);

                    // do save
                    tr.Complete();
                }

                return new DispCheckSkip() { Success = true };
            }
        }
    }
}

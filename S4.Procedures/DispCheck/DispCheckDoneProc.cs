﻿using S4.Entities;
using S4.Entities.XtraData;
using S4.ProcedureModels.DispCheck;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using S4.Core;
using S4.Core.Utils;
using S4.Entities.Models;

namespace S4.Procedures.DispCheck
{
    [ChangesDirectionStatus]
    public class DispCheckDoneProc : ProceduresBase<DispCheckDone>
    {
        public DispCheckDoneProc(string userID) : base(userID)
        {
        }

        protected override DispCheckDone DoIt(DispCheckDone procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new DispCheckDone() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // check
                var direction = (new DAL.DirectionDAL()).GetDirectionAllData(procModel.DirectionID);
                if (direction == null)
                    return new DispCheckDone() { Success = false, ErrorText = $"Doklad nebyl nalezen; DirectionID: {procModel.DirectionID}" };

                // do tests
                if (direction.DocDirection != Direction.DOC_DIRECTION_OUT)
                    return new DispCheckDone() { Success = false, ErrorText = $"Doklad nemá DocDirection DOC_DIRECTION_OUT; DirectionID: {procModel.DirectionID}" };

                if (direction.DocStatus != Direction.DR_STATUS_DISP_CHECK_PROC)
                    return new DispCheckDone() { Success = false, ErrorText = $"Doklad nemá DocStatus DR_STATUS_DISP_CHECK_PROC; DirectionID: {procModel.DirectionID}" };

                // do tests - items

                var storeMoves = new DAL.StoreMoveDAL().GetStoreMoveAllDataByDirection(procModel.DirectionID);
                var serialsToSave = new List<StoreMoveSerials>();


                var directionItemsGrp = direction.Items.GroupBy(_ => _.ArtPackID, (key, g) =>
                {
                    var item = g.First();
                    item.Quantity = g.Sum(_ => _.Quantity);
                    return item;
                }).ToList();


                foreach (var item in directionItemsGrp)
                {
                    var articlePacking = Singleton<DAL.Cache.ArticlePackingCache>.Instance.GetItem(item.ArtPackID);
                    if (articlePacking == null)
                        return new DispCheckDone() { ErrorText = $"Balení nenalezeno; ArtPackID: {item.ArtPackID}" };
                    var article = Singleton<DAL.Cache.ArticleModelCache>.Instance.GetItem(articlePacking.ArticleID);
                    if (article == null)
                        return new DispCheckDone() { ErrorText = $"Karta nenalezena; ArticleID: {articlePacking.ArticleID}" };
                    
                    // check serial numbers
                    if (article.UseSerialNumber)
                    {
                        if (procModel.SerialNumbers == null || !procModel.SerialNumbers.ContainsKey(item.DirectionItemID))
                            return new DispCheckDone() { ErrorText = $"U položky \"{article.ArticleInfo}\" nejsou vyplněna sériová čísla!" };

                        var snList = procModel.SerialNumbers[item.DirectionItemID];
                        var snCount = snList.Select(_ => _.SerialNumber).Distinct().Count();
                        if (snCount != item.Quantity)
                            return new DispCheckDone() { ErrorText = $"Množství sériových čísel ({snCount}) neodpovídá množství ({item.Quantity:0}) položky \"{article.ArticleInfo}\" v dokladu!" };

                        // group SN from client by BatchNum and ExpirationDate
                        var groupedSN = snList
                            .GroupBy(k => new { k.BatchNum, k.ExpirationDate }, (k, i) => new { k.BatchNum, k.ExpirationDate, SerialNumbers = i.Select(_ => _.SerialNumber) });

                        foreach (var snGroup in groupedSN)
                        {
                            // check & add serial numbers
                            var storeMoveItem = FindStoreMoveForSN(db, storeMoves, item, snGroup.BatchNum, snGroup.ExpirationDate);
                            if (storeMoveItem == null)
                                return new DispCheckDone() { ErrorText = $"Pohyb pro uložení seriových čísel nebyl nalezen; DirectionItemID: {item.DirectionItemID}" };

                            // prepare for saving serial numbers
                            foreach (var sn in snGroup.SerialNumbers)
                                serialsToSave.Add(new StoreMoveSerials() { StoMoveItemID = storeMoveItem.StoMoveItemID, SerialNumber = sn });
                        }
                    }
                }

                // change status
                direction.DocStatus = Direction.DR_STATUS_DISP_CHECK_DONE;
                
                // make history
                var history = new DirectionHistory()
                {
                    DirectionID = procModel.DirectionID,
                    DocPosition = 0,
                    EventCode = Direction.DR_STATUS_DISP_CHECK_DONE,
                    EntryUserID = _userID,
                    EntryDateTime = DateTime.Now
                };

               
                // save
                using (var tr = db.GetTransaction())
                {
                    // delete existing serials
                    var stoMoveItemIDs = serialsToSave.Select(i => i.StoMoveItemID).Distinct().ToList();
                    db.Execute("DELETE FROM s4_storeMove_serials WHERE stoMoveItemID IN (@0)", stoMoveItemIDs);

                    // fill serials
                    foreach (var item in serialsToSave)
                        db.Insert(item);

                    // save 
                    db.Update(direction);

                    // save history
                    db.Insert(history);

                    // do save
                    tr.Complete();
                }

                // messageBus - to run exports
                var messageBusData = JsonUtils.ToJsonString("directionID", direction.DirectionID, "procName", "DispCheckDoneProc", "userID", _userID);
                var messageBusDAL = new DAL.MessageBusDAL();
                messageBusDAL.TryInsertMessageBusStatusNew(direction.DirectionID, MessageBus.MessageBusTypeEnum.ExportDirection, messageBusData, db);
                messageBusDAL.TryInsertMessageBusStatusNew(direction.DirectionID, MessageBus.MessageBusTypeEnum.ExportABRA, messageBusData, db);

                return new DispCheckDone() { Success = true };
            }
        }


        private StoreMoveItemModel FindStoreMoveForSN(NPoco.Database db, List<StoreMoveModel> storeMoves, DirectionItem directionItem, string batchNum, DateTime? expirationDate)
        {
            // get lot
            var lot = new StoreCore.Save_Basic.Save()
                .FindOrCreateLot(db,
                    new StoreCore.Interfaces.SaveItem()
                    {
                        ArtPackID = directionItem.ArtPackID,
                        BatchNum = batchNum,
                        ExpirationDate = expirationDate
                    });

            // find move item
            var moveItems = storeMoves
                .Where(_ => _.DocType == StoreMove.MO_DOCTYPE_DISPATCH)
                .SelectMany(_ => _.Items)
                .Where(_ =>
                    _.Parent_docPosition == directionItem.DocPosition
                    && _.ItemDirection == StoreMoveItem.MI_DIRECTION_OUT
                    && _.ItemValidity == StoreMoveItem.MI_VALIDITY_VALID
                    && _.StoMoveLotID == lot.StoMoveLotID);

            return moveItems.FirstOrDefault();
        }
    }
}

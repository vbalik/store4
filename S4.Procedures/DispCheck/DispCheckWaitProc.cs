﻿using S4.Entities;
using S4.Entities.XtraData;
using S4.ProcedureModels.DispCheck;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Procedures.DispCheck
{
    [ChangesDirectionStatus]
    [ChangesDirectionAssignment]
    public class DispCheckWaitProc : ProceduresBase<DispCheckWait>
    {
        public DispCheckWaitProc(string userID) : base(userID)
        {
        }

        protected override DispCheckWait DoIt(DispCheckWait procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new DispCheckWait() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // check
                var direction = (new DAL.DirectionDAL()).GetDirectionAllData(procModel.DirectionID);
                if (direction == null)
                    return new DispCheckWait() { Success = false, ErrorText = $"Doklad nebyl nalezen; DirectionID: {procModel.DirectionID}" };

                // do tests
                if (direction.DocDirection != Direction.DOC_DIRECTION_OUT)
                    return new DispCheckWait() { Success = false, ErrorText = $"Doklad nemá DocDirection DOC_DIRECTION_OUT; DirectionID: {procModel.DirectionID}" };

                if (direction.DocStatus != Direction.DR_STATUS_DISPATCH_SAVED && direction.DocStatus != Direction.DR_STATUS_DISP_CHECK_WAIT)
                    return new DispCheckWait() { Success = false, ErrorText = $"Doklad nemá DocStatus DR_STATUS_DISPATCH_SAVED nebo DR_STATUS_DISP_CHECK_WAIT; DirectionID: {procModel.DirectionID}" };

                // change status
                direction.DocStatus = Direction.DR_STATUS_DISP_CHECK_WAIT;

                // assign worker
                var directionAssign = new DirectionAssign()
                {
                    JobID = DirectionAssign.JOB_ID_DISPATCH_CHECK,
                    WorkerID = procModel.WorkerID,
                    EntryUserID = _userID,
                    EntryDateTime = DateTime.Now,
                    DirectionID = direction.DirectionID
                };

                // set check method
                if (procModel.CheckMethod == Direction.DispCheckMethodsEnum.NotSet)
                {
                    if (direction.XtraData[Direction.XTRA_DISP_CHECK_METHOD] == null)
                    {
                        var partner = db.SingleById<Partner>(direction.PartnerID);
                        direction.XtraData[Direction.XTRA_DISP_CHECK_METHOD] = partner.PartnerDispCheck;
                    }
                }
                else
                {
                    direction.XtraData[Direction.XTRA_DISP_CHECK_METHOD] = (int)procModel.CheckMethod;
                }

                // make history
                var history = new DirectionHistory()
                {
                    DirectionID = procModel.DirectionID,
                    DocPosition = 0,
                    EventCode = Direction.DR_STATUS_DISP_CHECK_WAIT,
                    EventData = $"{procModel.WorkerID}|{direction.XtraData[Direction.XTRA_DISP_CHECK_METHOD]}",
                    EntryUserID = _userID,
                    EntryDateTime = DateTime.Now
                };


                // save
                using (var tr = db.GetTransaction())
                {
                    // save 
                    db.Update(direction);

                    // save history
                    db.Insert(history);

                    //remove old asignments
                    db.Delete<DirectionAssign>("where directionid = @0 AND jobID = @1", direction.DirectionID, DirectionAssign.JOB_ID_DISPATCH_CHECK);

                    //add directionAssign
                    db.Save(directionAssign);

                    //xtraData Save
                    direction.XtraData.Save(db, direction.DirectionID);

                    // do save
                    tr.Complete();
                }

                // log
                base.Info($"DispatchWaitProc; DirectionID: {procModel.DirectionID}");

                return new DispCheckWait() { Success = true };

            }
        }

    }
}

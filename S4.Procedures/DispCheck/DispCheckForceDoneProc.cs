﻿using S4.Core.Utils;
using S4.Entities;
using S4.Entities.XtraData;
using S4.ProcedureModels.DispCheck;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Procedures.DispCheck
{
    [ChangesDirectionStatus]
    public class DispCheckForceDoneProc : ProceduresBase<DispCheckForceDone>
    {
        public DispCheckForceDoneProc(string userID) : base(userID)
        {
        }

        protected override DispCheckForceDone DoIt(DispCheckForceDone procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new DispCheckForceDone() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // check
                var direction = (new DAL.DirectionDAL()).GetDirectionAllData(procModel.DirectionID);
                if (direction == null)
                    return new DispCheckForceDone() { Success = false, ErrorText = $"Doklad nebyl nalezen; DirectionID: {procModel.DirectionID}" };

                // do tests
                if (direction.DocDirection != Direction.DOC_DIRECTION_OUT)
                    return new DispCheckForceDone() { Success = false, ErrorText = $"Doklad nemá DocDirection DOC_DIRECTION_OUT; DirectionID: {procModel.DirectionID}" };


                // change status
                direction.DocStatus = Direction.DR_STATUS_DISP_CHECK_DONE;

                // make history
                var history = new DirectionHistory()
                {
                    DirectionID = procModel.DirectionID,
                    DocPosition = 0,
                    EventCode = Direction.DR_STATUS_DISP_CHECK_DONE,
                    EventData = $"Forced",
                    EntryUserID = _userID,
                    EntryDateTime = DateTime.Now
                };

                // save
                using (var tr = db.GetTransaction())
                {
                    // save 
                    db.Update(direction);

                    // save history
                    db.Insert(history);

                    // do save
                    tr.Complete();
                }

                // messageBus - to run exports
                var messageBusData = JsonUtils.ToJsonString("directionID", direction.DirectionID, "procName", "DispCheckForceDoneProc", "userID", _userID);
                var messageBusDAL = new DAL.MessageBusDAL();
                messageBusDAL.TryInsertMessageBusStatusNew(direction.DirectionID, MessageBus.MessageBusTypeEnum.ExportDirection, messageBusData, db);
                messageBusDAL.TryInsertMessageBusStatusNew(direction.DirectionID, MessageBus.MessageBusTypeEnum.ExportABRA, messageBusData, db);

                return new DispCheckForceDone() { Success = true };
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using NPoco;
using S4.Entities;
using S4.Entities.XtraData;
using System.Linq;
using S4.ProcedureModels.DispCheck;
using static S4.Entities.Direction;


namespace S4.Procedures.DispCheck
{
    [ChangesDirectionStatus]
    public class DispCheckProcProc : ProceduresBase<DispCheckProc>
    {
        private const DispCheckMethodsEnum DISP_CHECK_METHOD_DEFAULT = DispCheckMethodsEnum.EnterQuantity;



        public DispCheckProcProc(string userID) : base(userID)
        {
        }
        
        protected override DispCheckProc DoIt(DispCheckProc procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new DispCheckProc() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // check
                var direction = (new DAL.DirectionDAL()).GetDirectionAllData(procModel.DirectionID);
                if (direction == null)
                    return new DispCheckProc() { Success = false, ErrorText = $"Doklad nebyl nalezen; DirectionID: {procModel.DirectionID}" };

                // do tests
                if (direction.DocDirection != Direction.DOC_DIRECTION_OUT)
                    return new DispCheckProc() { Success = false, ErrorText = $"Doklad nemá DocDirection DOC_DIRECTION_OUT; DirectionID: {procModel.DirectionID}" };

                if (direction.DocStatus != Direction.DR_STATUS_DISP_CHECK_WAIT && direction.DocStatus != Direction.DR_STATUS_DISP_CHECK_PROC)
                    return new DispCheckProc() { Success = false, ErrorText = $"Doklad nemá DocStatus DR_STATUS_DISP_CHECK_WAIT nebo DR_STATUS_DISP_CHECK_PROC; DirectionID: {procModel.DirectionID}" };

                // get check method
                DispCheckMethodsEnum checkMethod;
                if (direction.XtraData.IsNull(Direction.XTRA_DISP_CHECK_METHOD, 0))
                    checkMethod = DISP_CHECK_METHOD_DEFAULT;
                else
                    checkMethod = (DispCheckMethodsEnum)(int)direction.XtraData[Direction.XTRA_DISP_CHECK_METHOD];

                // change direction status
                DirectionHistory history = null;
                if (direction.DocStatus != Direction.DR_STATUS_DISP_CHECK_PROC)
                { 
                    direction.DocStatus = Direction.DR_STATUS_DISP_CHECK_PROC;

                    // make history
                    history = new DirectionHistory()
                    {
                        DirectionID = procModel.DirectionID,
                        DocPosition = 0,
                        EventCode = Direction.DR_STATUS_DISP_CHECK_PROC,
                        EventData = $"{procModel.CheckMethod}",
                        EntryUserID = _userID,
                        EntryDateTime = DateTime.Now
                    };
                }


                // save
                using (var tr = db.GetTransaction())
                {
                    db.Save(direction);

                    if (history != null)
                        db.Insert(history);

                    tr.Complete();
                }

                return new DispCheckProc() { Success = true, CheckMethod = checkMethod };
            }
        }
    }

}

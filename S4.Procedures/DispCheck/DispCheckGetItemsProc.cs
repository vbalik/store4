﻿using System;
using System.Collections.Generic;
using System.Text;
using NPoco;
using S4.Entities;
using S4.Entities.XtraData;
using System.Linq;
using S4.ProcedureModels.DispCheck;
using S4.Core;

namespace S4.Procedures.DispCheck
{
    public class DispCheckGetItemsProc : ProceduresBase<DispCheckGetItems>
    {

        public DispCheckGetItemsProc(string userID) : base(userID)
        {
        }

        protected override DispCheckGetItems DoIt(DispCheckGetItems procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new DispCheckGetItems() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // check
                var direction = (new DAL.DirectionDAL()).GetDirectionAllData(procModel.DirectionID);
                if (direction == null)
                    return new DispCheckGetItems() { Success = false, ErrorText = $"Doklad nebyl nalezen; DirectionID: {procModel.DirectionID}" };

                // do tests
                if (direction.DocDirection != Direction.DOC_DIRECTION_OUT)
                    return new DispCheckGetItems() { Success = false, ErrorText = $"Doklad nemá DocDirection DOC_DIRECTION_OUT; DirectionID: {procModel.DirectionID}" };

                if (direction.DocStatus != Direction.DR_STATUS_DISP_CHECK_PROC)
                    return new DispCheckGetItems() { Success = false, ErrorText = $"Doklad nemá DocStatus DR_STATUS_DISP_CHECK_PROC; DirectionID: {procModel.DirectionID}" };

                // make items list
                var result = new DispCheckGetItems
                {
                    ItemInfoList = new List<Entities.Helpers.ItemInfo>()
                };

                foreach (var item in direction.Items)
                {
                    var articlePacking = Singleton<DAL.Cache.ArticlePackingCache>.Instance.GetItem(item.ArtPackID);
                    var articleModel = Singleton<DAL.Cache.ArticleModelCache>.Instance.GetItem(articlePacking.ArticleID);
                    var itemInfo = new Entities.Helpers.ItemInfo()
                    {
                        DirectionItemID = item.DirectionItemID,
                        ArtPackID = item.ArtPackID,
                        Quantity = item.Quantity,
                        PackDesc = articlePacking.PackDesc,
                        ArticleCode = item.ArticleCode,
                        ArticleDesc = articleModel.ArticleDesc,
                        UseBatch = articleModel.UseBatch,
                        UseExpiration = articleModel.UseExpiration,
                        UseSerialNumber = articleModel.UseSerialNumber
                    };
                    
                    itemInfo.ArticlePackingList = articleModel.Packings;
                    
                    if (!string.IsNullOrWhiteSpace(item.PartnerDepart))
                    {
                        procModel.UseDepartments = true;
                        itemInfo.Remark = item.PartnerDepart;
                    }

                    result.ItemInfoList.Add(itemInfo);
                }

                // make CheckDetails list
                var storeMoves = new DAL.StoreMoveDAL().GetStoreMoveAllDataByDirection(procModel.DirectionID);
                var moveItems = storeMoves
                    .Where(_ => _.DocType == StoreMove.MO_DOCTYPE_DISPATCH)
                    .SelectMany(_ => _.Items)
                    .Where(_ =>
                        _.ItemDirection == StoreMoveItem.MI_DIRECTION_OUT
                        && _.ItemValidity == StoreMoveItem.MI_VALIDITY_VALID);

                result.CheckDetails = moveItems
                    .GroupBy(k => k.StoMoveLotID, (k, i) => new { Lot = Singleton<DAL.Cache.StoreMoveLotCache>.Instance.GetItem(k), Quantity = i.Sum(_ => _.Quantity) })
                    .Select(_ => new DispCheckGetItems.CheckDetail() 
                    {
                        ArtPackID = _.Lot.ArtPackID,
                        BatchNum = _.Lot.BatchNum,
                        ExpirationDate = _.Lot.ExpirationDate,
                        Quantity = _.Quantity
                    })
                    .ToList();
                

                result.Success = true;
                return result;
            }
        }
    }

}

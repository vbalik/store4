﻿using S4.Procedures.Info;
using S4.Procedures.MessagesBus;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Procedures
{
    public class ProceduresCatalog
    {
        private Dictionary<string, ProceduresCatalogItem> _catalog = new Dictionary<string, ProceduresCatalogItem>(StringComparer.OrdinalIgnoreCase);

        public ProceduresCatalog(Action<ProceduresCatalog> initFunction)
        {
            initFunction(this);
        }


        internal ProceduresCatalogItem AddProcedure(ProceduresCatalogItem proc)
        {
            _catalog.Add(proc.Name, proc);
            return proc;
        }

        internal ProceduresCatalogItem AddProcedure(Type procedureType, Type parameterType)
        {
            var name = procedureType.Name;
            var proc = new ProceduresCatalogItem() { Name = name.Substring(0, name.Length - 4), ProcedureType = procedureType, ParameterType = parameterType };
            return AddProcedure(proc);
        }

        internal ProceduresCatalogItem AddProcedure<TProc, TParam>() where TProc : ProceduresBase<TParam> where TParam : S4.ProcedureModels.ProceduresModel
        {
            var name = typeof(TProc).Name;
            var proc = new ProceduresCatalogItem() { Name = name.Substring(0, name.Length - 4), ProcedureType = typeof(TProc), ParameterType = typeof(TParam) };
            return AddProcedure(proc);
        }
        
        public ProceduresCatalogItem GetProcedure(string procedureName)
        {
            ProceduresCatalogItem res;
            _catalog.TryGetValue(procedureName, out res);
            return res;
        }


        public static ProceduresCatalog Current { get; } = new ProceduresCatalog(c =>
        {
            // info 
            c.AddProcedure<Info.InfoByArticleProc, S4.ProcedureModels.Info.InfoByArticle>();
            c.AddProcedure<Info.OnStorePrintProc, S4.ProcedureModels.Info.OnStorePrint>();
            c.AddProcedure<Info.PrintShippingDocumentADRProc, S4.ProcedureModels.Info.PrintShippingDocumentADR>();

            // admin
            c.AddProcedure<Admin.DirectionSetStatusProc, S4.ProcedureModels.Admin.DirectionSetStatus>();
            c.AddProcedure<Admin.DirectionDeleteProc, S4.ProcedureModels.Admin.DirectionDelete>();
            c.AddProcedure<Admin.DirectionCancelProc, S4.ProcedureModels.Admin.DirectionCancel>();
            c.AddProcedure<Admin.DirectionItemChangeProc, S4.ProcedureModels.Admin.DirectionItemChange>();
            c.AddProcedure<Admin.DirectionItemChange2Proc, S4.ProcedureModels.Admin.DirectionItemChange2>();
            c.AddProcedure<Admin.StoreMoveCancelProc, S4.ProcedureModels.Admin.StoreMoveCancel>();
            c.AddProcedure<Admin.StoreMoveSetStatusProc, S4.ProcedureModels.Admin.StoreMoveSetStatus>();
            c.AddProcedure<Admin.StoreMoveItemChangeProc, S4.ProcedureModels.Admin.StoreMoveItemChange>();
            c.AddProcedure<Admin.PositionResetStatusProc, S4.ProcedureModels.Admin.PositionResetStatus>();
            c.AddProcedure<Admin.RemoveExpBatchProc, S4.ProcedureModels.Admin.RemoveExpBatch>();
            c.AddProcedure<Admin.RemoveBatchProc, S4.ProcedureModels.Admin.RemoveExpBatch>();
            c.AddProcedure<Admin.RemoveExpirationProc, S4.ProcedureModels.Admin.RemoveExpBatch>();
            c.AddProcedure<Admin.RemoveCarrierProc, S4.ProcedureModels.Admin.RemoveCarrier>();

            // move
            c.AddProcedure<Move.MoveSaveProc, S4.ProcedureModels.Move.MoveSave>();
            c.AddProcedure<Move.MovePrintProc, S4.ProcedureModels.Move.MovePrint>();
            c.AddProcedure<Move.MoveGetPositionProc, S4.ProcedureModels.Move.MoveGetPosition>();
            c.AddProcedure<Move.MoveLockPosProc, S4.ProcedureModels.Move.MoveLockPos>();

            // stocktaking
            c.AddProcedure<StockTaking.StockTakingAddItemProc, S4.ProcedureModels.StockTaking.StockTakingAddItem>();
            c.AddProcedure<StockTaking.StockTakingAddPosItemProc, S4.ProcedureModels.StockTaking.StockTakingAddPosItem>();
            c.AddProcedure<StockTaking.StockTakingChangeItemProc, S4.ProcedureModels.StockTaking.StockTakingChangeItem>();
            c.AddProcedure<StockTaking.StockTakingDeleteItemProc, S4.ProcedureModels.StockTaking.StockTakingDeleteItem>();
            c.AddProcedure<StockTaking.StockTakingDeletePosItemProc, S4.ProcedureModels.StockTaking.StockTakingDeletePosItem>();
            c.AddProcedure<StockTaking.StockTakingItemsFromSnapshotProc, S4.ProcedureModels.StockTaking.StockTakingItemsFromSnapshot>();
            c.AddProcedure<StockTaking.StockTakingLoadABRAStatusProc, S4.ProcedureModels.StockTaking.StockTakingLoadABRAStatus>();
            c.AddProcedure<StockTaking.StockTakingSaveProc, S4.ProcedureModels.StockTaking.StockTakingSave>();
            c.AddProcedure<StockTaking.StockTakingCancelProc, S4.ProcedureModels.StockTaking.StockTakingCancel>();
            c.AddProcedure<StockTaking.StockTakingListOpenedProc, S4.ProcedureModels.StockTaking.StockTakingListOpened>();
            c.AddProcedure<StockTaking.StockTakingBeginProc, S4.ProcedureModels.StockTaking.StockTakingBegin>();
            c.AddProcedure<StockTaking.StockTakingScanPosProc, S4.ProcedureModels.StockTaking.StockTakingScanPos>();
            c.AddProcedure<StockTaking.StockTakingPositionOKProc, S4.ProcedureModels.StockTaking.StockTakingPositionOK>();
            c.AddProcedure<StockTaking.StockTakingRemoteListOpenedProc, S4.ProcedureModels.StockTaking.StockTakingRemoteListOpened>();
            c.AddProcedure<StockTaking.StockTakingRemoteScanPosProc, S4.ProcedureModels.StockTaking.StockTakingRemoteScanPos>();
            c.AddProcedure<StockTaking.StockTakingRemotePrintProc, S4.ProcedureModels.StockTaking.StockTakingRemotePrint>();
            c.AddProcedure<StockTaking.StockTakingRemoteItemsFromSnapshotProc, S4.ProcedureModels.StockTaking.StockTakingRemoteItemsFromSnapshot>();

            // dispatch
            c.AddProcedure<Dispatch.DispatchWaitProc, S4.ProcedureModels.Dispatch.DispatchWait>();
            c.AddProcedure<Dispatch.DispatchMakeReservationProc, S4.ProcedureModels.Dispatch.DispatchMakeReservation>();
            c.AddProcedure<Dispatch.DispatchCancelReservationProc, S4.ProcedureModels.Dispatch.DispatchCancelReservation>();
            c.AddProcedure<Dispatch.DispatchPrintProc, S4.ProcedureModels.Dispatch.DispatchPrint>();
            c.AddProcedure<Dispatch.DispatchPrintLabelProc, S4.ProcedureModels.Dispatch.DispatchPrintLabel>();
            c.AddProcedure<Dispatch.DispatchPrintSerialNumberProc, S4.ProcedureModels.Dispatch.DispatchPrintSerialNumber>();
            c.AddProcedure<Dispatch.DispatchTestDirectProc, S4.ProcedureModels.Dispatch.DispatchTestDirect>();
            c.AddProcedure<Dispatch.DispatchRepeatReservationProc, S4.ProcedureModels.Dispatch.DispatchRepeatReservation>();
            c.AddProcedure<Dispatch.DispatchCalcBoxLabelsProc, S4.ProcedureModels.Dispatch.DispatchCalcBoxLabels>();
            c.AddProcedure<Dispatch.DispatchReassignProc, S4.ProcedureModels.Dispatch.DispatchReassign>();
            c.AddProcedure<Dispatch.DispatchListDirectionsProc, S4.ProcedureModels.Dispatch.DispatchListDirections>();
            c.AddProcedure<Dispatch.DispatchShowItemsProc, S4.ProcedureModels.Dispatch.DispatchShowItems>();
            c.AddProcedure<Dispatch.DispatchItemInstructProc, S4.ProcedureModels.Dispatch.DispatchItemInstruct>();
            c.AddProcedure<Dispatch.DispatchPosErrorProc, S4.ProcedureModels.Dispatch.DispatchPosError>();
            c.AddProcedure<Dispatch.DispatchConfirmItemProc, S4.ProcedureModels.Dispatch.DispatchConfirmItem>();
            c.AddProcedure<Dispatch.DispatchSaveItemProc, S4.ProcedureModels.Dispatch.DispatchSaveItem>();
            c.AddProcedure<Dispatch.DispatchPrintBoxLabelProc, S4.ProcedureModels.Dispatch.DispatchPrintBoxLabel>();
            c.AddProcedure<Dispatch.DispatchSetXtraValuesProc, S4.ProcedureModels.Dispatch.DispatchSetXtraValues>();
            c.AddProcedure<Dispatch.DispatchManualSaveItemProc, S4.ProcedureModels.Dispatch.DispatchManualSaveItem>();
            c.AddProcedure<Dispatch.DispatchManualBeginProc, S4.ProcedureModels.Dispatch.DispatchManualBegin>();
            c.AddProcedure<Dispatch.DispatchAvailableDirectionsDocNumberProc, S4.ProcedureModels.Dispatch.DispatchAvailableDirectionsDocNumber>();
            c.AddProcedure<Dispatch.DispatchUserAssignmentProc, S4.ProcedureModels.Dispatch.DispatchUserAssignment>();
            
            // logistics
            c.AddProcedure<Logistics.LogisticsDispatchToStoInProc, S4.ProcedureModels.Logistics.LogisticsDispToStoIn>();
            c.AddProcedure<Logistics.LogisticsDispatchToPositionProc, S4.ProcedureModels.Logistics.LogisticsDispatchToPosition>();
            c.AddProcedure<Logistics.LogisticsImportReceiveProc, S4.ProcedureModels.Logistics.LogisticsImportReceive>();

            // dispatch check
            c.AddProcedure<DispCheck.DispCheckWaitProc, S4.ProcedureModels.DispCheck.DispCheckWait>();
            c.AddProcedure<DispCheck.DispCheckProcProc, S4.ProcedureModels.DispCheck.DispCheckProc>();
            c.AddProcedure<DispCheck.DispCheckListDirectionsProc, S4.ProcedureModels.DispCheck.DispCheckListDirections>();
            c.AddProcedure<DispCheck.DispCheckGetItemsProc, S4.ProcedureModels.DispCheck.DispCheckGetItems>();
            c.AddProcedure<DispCheck.DispCheckDoneProc, S4.ProcedureModels.DispCheck.DispCheckDone>();
            c.AddProcedure<DispCheck.DispCheckDone2Proc, S4.ProcedureModels.DispCheck.DispCheckDone2>();
            c.AddProcedure<DispCheck.DispCheckSkipProc, S4.ProcedureModels.DispCheck.DispCheckSkip>();
            c.AddProcedure<DispCheck.DispCheckAvailableDirectionsDocInfoProc, S4.ProcedureModels.DispCheck.DispCheckAvailableDirectionsDocInfo>();
            c.AddProcedure<DispCheck.DispCheckAvailableDirectionsDocNumberProc, S4.ProcedureModels.DispCheck.DispCheckAvailableDirectionsDocNumber>();

            // exped
            c.AddProcedure<Exped.ExpedListDirectsProc, S4.ProcedureModels.Exped.ExpedListDirects>();
            c.AddProcedure<Exped.ExpedDoneProc, S4.ProcedureModels.Exped.ExpedDone>();

            // worker
            c.AddProcedure<Admin.WorkerSetDisabledProc, S4.ProcedureModels.Admin.WorkerSetDisabled>();
            c.AddProcedure<Admin.WorkerSetOnDutyProc, S4.ProcedureModels.Admin.WorkerSetOnDuty>();
            c.AddProcedure<Admin.WorkerSetOutOfOrderProc, S4.ProcedureModels.Admin.WorkerSetOutOfOrder>();
            c.AddProcedure<Admin.WorkerSetStatusProc, S4.ProcedureModels.Admin.WorkerSetStatus>();

            // receive
            c.AddProcedure<Receive.ReceiveProcManProc, S4.ProcedureModels.Receive.ReceiveProcMan>();
            c.AddProcedure<Receive.ReceiveSaveItemProc, S4.ProcedureModels.Receive.ReceiveSaveItem>();
            c.AddProcedure<Receive.ReceiveCancelItemProc, S4.ProcedureModels.Receive.ReceiveCancelItem>();
            c.AddProcedure<Receive.ReceiveDoneProc, S4.ProcedureModels.Receive.ReceiveDone>();
            c.AddProcedure<Receive.ReceiveSimpleProc, S4.ProcedureModels.Receive.ReceiveSimple>();
            c.AddProcedure<Receive.ReceivePrintProc, S4.ProcedureModels.Receive.ReceivePrint>();
            c.AddProcedure<Receive.ReceivePrintByDirectionProc, S4.ProcedureModels.Receive.ReceivePrintByDirection>();
            c.AddProcedure<Receive.ReceiveTestDirectionProc, S4.ProcedureModels.Receive.ReceiveTestDirection>();
            c.AddProcedure<Receive.ReceiveChangePartnerProc, S4.ProcedureModels.Receive.ReceiveChangePartner>();

            // store in
            c.AddProcedure<StoreIn.StoreInWaitProc, S4.ProcedureModels.StoreIn.StoreInWait>();
            c.AddProcedure<StoreIn.StoreInListDirectsProc, S4.ProcedureModels.StoreIn.StoreInListDirects>();
            c.AddProcedure<StoreIn.StoreInProcProc, S4.ProcedureModels.StoreIn.StoreInProc>();
            c.AddProcedure<StoreIn.StoreInListNoCarrProc, S4.ProcedureModels.StoreIn.StoreInListNoCarr>();
            c.AddProcedure<StoreIn.StoreInCheckCarrierProc, S4.ProcedureModels.StoreIn.StoreInCheckCarrier>();
            c.AddProcedure<StoreIn.StoreInCheckPosProc, S4.ProcedureModels.StoreIn.StoreInCheckPos>();
            c.AddProcedure<StoreIn.StoreInSaveItemProc, S4.ProcedureModels.StoreIn.StoreInSaveItem>();
            c.AddProcedure<StoreIn.StoreInTestDirectProc, S4.ProcedureModels.StoreIn.StoreInTestDirect>();
            c.AddProcedure<StoreIn.StoreInAcceptPosProc, S4.ProcedureModels.StoreIn.StoreInAcceptPos>();
            c.AddProcedure<StoreIn.StoreInAcceptPosNoCarrProc, S4.ProcedureModels.StoreIn.StoreInAcceptPosNoCarr>();

            // position
            c.AddProcedure<Pos.PositionPrintLabelProc, S4.ProcedureModels.Pos.PositionPrintLabel>();
            c.AddProcedure<Pos.PositionSetStatusProc, S4.ProcedureModels.Pos.PositionSetStatus>();

            // barcode
            c.AddProcedure<Barcode.BarcodeSetProc, S4.ProcedureModels.BarCode.BarcodeSet>();
            c.AddProcedure<Barcode.BarcodeDeleteProc, S4.ProcedureModels.BarCode.BarcodeDelete>();
            c.AddProcedure<Barcode.BarcodeMakeCodeProc, S4.ProcedureModels.BarCode.BarcodeMakeCode>();
            c.AddProcedure<Barcode.BarcodeListPrintersProc, S4.ProcedureModels.BarCode.BarcodeListPrinters>();
            c.AddProcedure<Barcode.BarcodePrintLabelProc, S4.ProcedureModels.BarCode.BarcodePrintLabel>();
            c.AddProcedure<Barcode.BarcodePrintCarrierLabelsProc, S4.ProcedureModels.BarCode.BarcodePrintCarrierLabels>();

            // delivery
            c.AddProcedure<Delivery.PrintLabelProc, S4.ProcedureModels.Delivery.PrintLabel>();
            c.AddProcedure<Delivery.PrintManifestProc, S4.ProcedureModels.Delivery.PrintManifest>();
            c.AddProcedure<Delivery.GetDeliveryProvidersProc, S4.ProcedureModels.Delivery.GetDeliveryProviders>();
            c.AddProcedure<Delivery.ManifestRequestProc, S4.ProcedureModels.Delivery.ManifestRequest>();
            c.AddProcedure<Delivery.GetShipmentStatusProc, S4.ProcedureModels.Delivery.GetShipmentStatus>();
            c.AddProcedure<Delivery.DirectionSetShipmentFailedStatusProc, S4.ProcedureModels.Delivery.DirectionSetShipmentFailedStatus>();
            c.AddProcedure<Delivery.ResetShipmentProc, S4.ProcedureModels.Delivery.ResetShipment>();
            c.AddProcedure<Delivery.GetDirectionsForWeighProc, S4.ProcedureModels.Delivery.GetDirectionsForWeigh>();

            //test
            c.AddProcedure<TestProc, ProcedureModels.Test>();

            //messageBus
            c.AddProcedure<MessageBusSetStatusToNewProc, S4.ProcedureModels.MessageBus.MessageBusSetStatusToNew>();

            // booking
            c.AddProcedure<Booking.BookingAddItemProc, S4.ProcedureModels.Booking.BookingAddItem>();
            c.AddProcedure<Booking.BookingDeleteItemProc, S4.ProcedureModels.Booking.BookingDeleteItem>();
            c.AddProcedure<Booking.BookingListAvailableLotsProc, S4.ProcedureModels.Booking.BookingListAvailableLots>();
            c.AddProcedure<Booking.BookingFromReceiveProc, S4.ProcedureModels.Booking.BookingFromReceive>();
        });
    }
}

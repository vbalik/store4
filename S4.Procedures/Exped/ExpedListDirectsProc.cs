﻿using System;
using System.Collections.Generic;
using System.Text;
using NPoco;
using S4.Entities;
using S4.ProcedureModels.Dispatch;
using S4.ProcedureModels.Exped;
using System.Linq;
using S4.ProcedureModels.Extensions;
using S4.Core;

namespace S4.Procedures.Exped
{
    public class ExpedListDirectsProc : ProceduresBase<ExpedListDirects>
    {
        public ExpedListDirectsProc(string userID) : base(userID)
        {
        }


        protected override ExpedListDirects DoIt(ExpedListDirects procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new ExpedListDirects() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // get directions
                var dirs = db.Query<Direction>().Where(d => (d.DocStatus == Direction.DR_STATUS_DISP_CHECK_DONE || d.DocStatus == Direction.DR_STATUS_DELIVERY_MANIFEST) && d.DocDirection == Direction.DOC_DIRECTION_OUT).ToList();

                // get xtra data
                var xtra = db.Query<DirectionXtra>().Where(x => x.XtraCode == Direction.XTRA_DISPATCH_POSITION_ID && dirs.Select(d => d.DirectionID).Contains(x.DirectionID)).ToList();
                dirs.ForEach(d =>
                {
                    var xtraItem = xtra.Where(x => x.DirectionID == d.DirectionID).SingleOrDefault();
                    if (xtraItem != null)
                        d.XtraData.Data.Add(xtraItem);
                });

                // to result
                var docs = dirs.Select(i => new Entities.Helpers.DocumentInfo()
                {
                    DocumentID = i.DirectionID,
                    DocumentName = i.DocInfo(),
                    DocumentDetail = Singleton<DAL.Cache.PartnerCache>.Instance.GetItem(i.PartnerID)?.PartnerName,
                    DocumentDetail2 = GetExpedPositionCode(db, i)
                })
                .OrderBy(i => i.DocumentDetail2)
                .ThenBy(i => i.DocumentName)
                .ToList();

                return new ExpedListDirects
                {
                    Directions = docs,
                    Success = true
                };
            }
        }


        private string GetExpedPositionCode(Database db, Direction direction)
        {
            var positionID = (int?)direction.XtraData[Direction.XTRA_DISPATCH_POSITION_ID];
            if (positionID.HasValue)
                return Singleton<DAL.Cache.PositionCache>.Instance.GetItem(positionID.Value)?.PosCode;
            else
                return "???";
        }
    }

}

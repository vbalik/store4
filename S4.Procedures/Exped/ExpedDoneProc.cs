﻿using System;
using System.Collections.Generic;
using System.Text;
using NPoco;
using S4.Entities;
using S4.ProcedureModels.Dispatch;
using S4.ProcedureModels.Exped;
using System.Linq;
using S4.ProcedureModels.Extensions;
using S4.Entities.XtraData;
using S4.Procedures.DBHelpers;
using S4.StoreCore.Interfaces;
using S4.Core;
using S4.DAL;
using S4.Core.Utils;

namespace S4.Procedures.Exped
{
    [ChangesDirectionStatus]
    public class ExpedDoneProc : ProceduresBase<ExpedDone>
    {
        public ExpedDoneProc(string userID) : base(userID)
        {
        }


        protected override ExpedDone DoIt(ExpedDone procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new ExpedDone() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //check
                var direction = (new DAL.DirectionDAL()).GetDirectionAllData(procModel.DirectionID);
                if (direction == null)
                    return new ExpedDone() { Success = false, ErrorText = $"Doklad nebyl nalezen; DirectionID: {procModel.DirectionID}" };

                // do tests
                if (direction.DocDirection != Direction.DOC_DIRECTION_OUT)
                    return new ExpedDone() { Success = false, ErrorText = $"Direction nemá DocDirection DOC_DIRECTION_OUT; DirectionID: {procModel.DirectionID}" };

                //doklad co se neprovádí zpracování
                if (direction.DoNotProcess)
                    return new ExpedDone() { Success = true, StoMoveID = 0 };

                if (direction.DocStatus != Direction.DR_STATUS_DISP_CHECK_DONE &&
                    direction.DocStatus != Direction.DR_STATUS_DELIVERY_PRINT &&
                    direction.DocStatus != Direction.DR_STATUS_DELIVERY_SHIPMENT_FAILED &&
                    direction.DocStatus != Direction.DR_STATUS_DELIVERY_MANIFEST)
                    return new ExpedDone() { Success = false, ErrorText = $"Direction nemá DocStatus DR_STATUS_DISP_CHECK_DONE nebo DR_STATUS_DELIVERY_PRINT nebo DR_STATUS_DELIVERY_MANIFEST; DirectionID: {procModel.DirectionID}" };

                if (direction.XtraData.IsNull(Direction.XTRA_DISPATCH_POSITION_ID))
                    return new ExpedDone() { Success = false, ErrorText = $"Direction nemá nastavenou expediční pozici; DirectionID: {procModel.DirectionID}" };

                // change status
                direction.DocStatus = Direction.DR_STATUS_DISP_EXPED;

                // make history
                var history = new DirectionHistory()
                {
                    DirectionID = procModel.DirectionID,
                    DocPosition = 0,
                    EventCode = Direction.DR_STATUS_DISP_EXPED,
                    EventData = procModel.ExpedTruck,
                    EntryUserID = _userID,
                    EntryDateTime = DateTime.Now
                };

                // set exped truck
                direction.XtraData[Direction.XTRA_EXPED_TRUCK] = procModel.ExpedTruck;

                // prepare data for StoreOut
                int expedPositionID = (int)direction.XtraData[Direction.XTRA_DISPATCH_POSITION_ID];
                var dispatchPositionsModel = Singleton<DAL.Cache.PositionByCategoryModelCache>.Instance.GetItem(Entities.Position.PositionCategoryEnum.Dispatch);
                var save = Singleton<ServicesFactory>.Instance.GetSave();
                var toStoreOut = save.MoveResult_ByDirection(db, direction.DirectionID, StoreMove.MO_DOCTYPE_DISPATCH, (mi) => dispatchPositionsModel.PositionIDs.Contains(mi.PositionID));
                var saveItemList = toStoreOut.Select(item => new SaveItem()
                {
                    ArtPackID = item.ArtPackID,
                    BatchNum = item.BatchNum,
                    CarrierNum = item.CarrierNum,
                    ExpirationDate = item.ExpirationDate,
                    Quantity = item.Quantity,
                    Parent_docPosition = item.DocPosition,
                    Xtra_PositionID = item.PositionID
                }).ToList();

                // prepare messageBus data and create DAL
                var messageBusData = JsonUtils.ToJsonString("directionID", direction.DirectionID, "procName", "ExpedDoneProc", "userID", _userID);
                var messageBusDAL = new DAL.MessageBusDAL();

                // save
                int stoMoveID = 0;
                using (var tr = db.GetTransaction())
                {
                    // save direction
                    db.Save(direction);

                    // save xtraData
                    direction.XtraData.Save(db, direction.DirectionID);

                    // save history
                    db.Insert(history);

                    // clean up expedition position
                    if (saveItemList.Count > 0)
                    {
                        stoMoveID = save.StoreOut(db, _userID, expedPositionID, StoreMove.MO_PREFIX_EXPED, saveItemList, true,
                        (storeMove) =>
                        {
                            storeMove.Parent_directionID = direction.DirectionID;
                        },
                        (saveItem, storeMoveItem) =>
                        {
                            // rewriting storeMoveItem.PositionID - dispatch moves can be from different dispatch positions
                            storeMoveItem.PositionID = saveItem.Xtra_PositionID;
                        },
                        out string msg);
                        if (stoMoveID == -1)
                            return new ExpedDone() { ErrorText = msg };
                    }
                    else
                    {
                        base.Warn($"ExpedDone - seznam položek pro StoreOut je prázdný; directionID: {direction.DirectionID}");
                    }

                    // insert messageBus
                    messageBusDAL.TryInsertMessageBusStatusNew(direction.DirectionID, MessageBus.MessageBusTypeEnum.ExportDirection, messageBusData, db);
                    messageBusDAL.TryInsertMessageBusStatusNew(direction.DirectionID, MessageBus.MessageBusTypeEnum.ExportABRA, messageBusData, db);

                    //only Advantis
                    var partnerIDs = Singleton<DAL.SettingsSet>.Instance.GetValue_String(Entities.SettingsDictionary.EXPORT_ADVANTIS_DEFAULT_PARTNER_IDs);
                    var findPartner = false;
                    if (!string.IsNullOrWhiteSpace(partnerIDs))
                    {
                        foreach (var id in partnerIDs.Split(','))
                        {
                            if (direction.PartnerID.ToString().Equals(id))
                            {
                                findPartner = true;
                                break;
                            }
                        }

                        if (findPartner)
                            messageBusDAL.TryInsertMessageBusStatusNew(direction.DirectionID, MessageBus.MessageBusTypeEnum.ExportNemStore, messageBusData, db);
                    }

                    tr.Complete();
                }


                // log
                base.Info($"ExpedDone; DirectionID: {procModel.DirectionID}; ExpedTruck: {procModel.ExpedTruck}");

                // call exchange
                var exchangeJob1 = "ExportABRA";
                var exchangeJob2 = "ExportDirection";
                Info($"Call Exchange jobs: {exchangeJob1}, {exchangeJob2}");
                SendMessageToExchange(exchangeJob1, exchangeJob2);

                return new ExpedDone() { Success = true, StoMoveID = stoMoveID };
            }
        }
    }

}

﻿using System;
using S4.Core.Utils;
using S4.Entities;
using S4.ProcedureModels.Exped;


namespace S4.Procedures.Exped
{
    [ChangesDirectionStatus]
    public class DirectionSetDeliveredCustomerProc : ProceduresBase<DirectionSetDeliveredCustomer>
    {
        public DirectionSetDeliveredCustomerProc(string userID) : base(userID)
        {
        }


        protected override DirectionSetDeliveredCustomer DoIt(DirectionSetDeliveredCustomer procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new DirectionSetDeliveredCustomer() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            // set direction status
            
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //check
                var direction = (new DAL.DirectionDAL()).GetDirectionAllData(procModel.DirectionID);
                if (direction == null)
                    return new DirectionSetDeliveredCustomer() { Success = false, ErrorText = $"Doklad nebyl nalezen; DirectionID: {procModel.DirectionID}" };

                // make history
                var history = new DirectionHistory()
                {
                    DirectionID = procModel.DirectionID,
                    DocPosition = 0,
                    EventCode = Direction.DR_EVENT_DELIVERED,
                    EntryUserID = _userID,
                    EventData = JsonUtils.ToJsonString("Date and time of delivery to the customer", procModel.DateTimeDeliveredCustomer),
                    EntryDateTime = DateTime.Now
                };

                // save
                using (var tr = db.GetTransaction())
                {
                    //his
                    db.Insert(history);

                    // do save
                    tr.Complete();
                }

                // log
                base.Info($"DirectionSetDeliveredCustomerProc; DirectionID: {procModel.DirectionID}");

                return new DirectionSetDeliveredCustomer() { Success = true };
            }
        }
    }
}

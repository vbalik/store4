﻿using S4.Core;
using S4.Entities;
using S4.Entities.Models;
using S4.ProcedureModels.Logistics;
using S4.StoreCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace S4.Procedures.Logistics
{
    public class LogisticsImportReceiveProc : ProceduresBase<LogisticsImportReceive>
    {
        public LogisticsImportReceiveProc(string userID) : base(userID)
        {
        }


        protected override LogisticsImportReceive DoIt(LogisticsImportReceive procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new LogisticsImportReceive() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            // if ReceivePositionID not set, get default
            if (procModel.ReceivePositionID == 0)
                procModel.ReceivePositionID = Singleton<DAL.SettingsSet>.Instance.GetValue_Int32(SettingsDictionary.RECEIVE_POSITION_ID);


            var now = DateTime.Now;
            int directionID, stoMoveStoInID, stoMoveReceiveID;

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                using (var tr = db.GetTransaction())
                {
                    var save = Singleton<ServicesFactory>.Instance.GetSave();
                    
                    // IN part

                    // create new receive direction
                    var prefix = Singleton<DAL.SettingsSet>.Instance.GetValue_String(Entities.SettingsDictionary.RECEIVE_DOC_NUM_PREFIX);
                    var directionIn = new DirectionModel()
                    {
                        DocDate = now,
                        DocDirection = 1,
                        DocNumPrefix = prefix,
                        DocStatus = Direction.DR_STATUS_STOIN_COMPLET,
                        DocYear = now.ToString("yy"),
                        EntryDateTime = now,
                        EntryUserID = _userID,
                        PartnerRef = procModel.PartnerRef
                    };

                    // create direction in items - from grouped saveItemList
                    var docPosition = 1;
                    directionIn.Items = procModel.Items
                        .GroupBy(k => k.ArtPackID, (ArtPackID, items) => new DirectionItem()
                        {
                            DocPosition = docPosition++,
                            ItemStatus = DirectionItem.DI_STATUS_RECEIVE_COMPLET,
                            ArtPackID = ArtPackID,
                            ArticleCode = DBHelpers.Articles.GetArticleCode(ArtPackID),
                            Quantity = items.Sum(i => i.Quantity),
                            EntryUserID = _userID,
                            EntryDateTime = now
                        }).ToList();

                    // save
                    directionIn.DocNumber = DBHelpers.DocNumbers.GetNextDocNumber(db, directionIn);
                    db.Insert(directionIn);
                    directionIn.Items.ForEach(di =>
                    {
                        di.DirectionID = directionIn.DirectionID;
                        db.Insert(di);
                    });
                    directionID = directionIn.DirectionID;

                    // and assign storein worker
                    DirectionAssign directionAssign = null;
                    if (!string.IsNullOrEmpty(procModel.WorkerID))
                    {
                        directionAssign = new DirectionAssign()
                        {
                            JobID = DirectionAssign.JOB_ID_STORE_IN,
                            WorkerID = procModel.WorkerID,
                            EntryUserID = _userID,
                            EntryDateTime = now,
                            DirectionID = directionIn.DirectionID
                        };
                        db.Insert(directionAssign);
                    }

                    // add info to history
                    var dirInHistory = new DirectionHistory()
                    {
                        DirectionID = directionIn.DirectionID,
                        DocPosition = 0,
                        EventCode = Direction.DR_EVENT_IMPORT_RECEIVE,
                        EventData = procModel.PartnerRef,
                        EntryUserID = _userID,
                        EntryDateTime = now
                    };
                    db.Insert(dirInHistory);


                    List<SaveItem> saveItemList = procModel.Items
                        .GroupBy(k => k.ArtPackID, (ArtPackID, items) => new SaveItem()
                        {
                            ArtPackID = ArtPackID,
                            Quantity = items.Sum(i => i.Quantity)
                            // ignore other fields - BatchNum, ExpirationDate, CarrierNum
                        }).ToList();


                    // do receive (StoreIn)
                    stoMoveReceiveID = save.StoreIn(
                        db, _userID, procModel.ReceivePositionID, StoreMove.MO_PREFIX_RECEIVE, saveItemList.ToList(), true, (storeMove) =>
                        {
                            storeMove.Parent_directionID = directionIn.DirectionID;
                        },
                        (saveItem, storeMoveItem) =>
                        {
                            storeMoveItem.Parent_docPosition = DBHelpers.Directions.FindParentDocPosition(saveItem.ArtPackID, directionIn.Items);
                        },
                        out string message);
                    if (stoMoveReceiveID == -1)
                        return new LogisticsImportReceive() { ErrorText = message };

                    // create store in history
                    var storeMoveHistReceive = new StoreMoveHistory()
                    {
                        EntryDateTime = DateTime.Now,
                        EntryUserID = _userID,
                        EventCode = StoreMove.MO_STATUS_SAVED_RECEIVE,
                        StoMoveID = stoMoveReceiveID
                    };
                    db.Insert(storeMoveHistReceive);


                    // do store in
                    var storeMoveItems = saveItemList
                    .Select(i => new MoveItem()
                    {
                        ArtPackID = i.ArtPackID,
                        BatchNum = i.BatchNum,
                        CarrierNum = i.CarrierNum,
                        ExpirationDate = i.ExpirationDate,
                        Quantity = i.Quantity,

                        SourcePositionID = procModel.ReceivePositionID,
                        DestinationPositionID = procModel.DestPositionID,

                        Parent_docPosition = i.Parent_docPosition
                    })
                    .ToList();

                    // make storeMove
                    stoMoveStoInID = new StoreCore.Save_Basic.Save().Move(db, _userID, StoreMove.MO_PREFIX_STORE_IN, StoreMove.MO_DOCTYPE_STORE_IN, storeMoveItems, true, sm =>
                    {
                        sm.DocStatus = StoreMove.MO_STATUS_NEW_STOIN;
                        sm.Parent_directionID = directionIn.DirectionID;
                    },
                    null,
                    out string erMsg);
                    if (stoMoveStoInID == -1)
                        return new LogisticsImportReceive() { ErrorText = erMsg };

                    // create history
                    var stoMoveHistStoIn = new StoreMoveHistory()
                    {
                        EntryDateTime = DateTime.Now,
                        EntryUserID = _userID,
                        EventCode = StoreMove.MO_STATUS_NEW_STOIN,
                        StoMoveID = stoMoveStoInID
                    };
                    db.Insert(stoMoveHistStoIn);

                    tr.Complete();
                }
            }

            // log
            base.Info($"LogisticsImportReceiveProc; stoMoveReceiveID: {stoMoveReceiveID}; stoMoveStoInID: {stoMoveStoInID}");

            return new LogisticsImportReceive() { Success = true, DirectionID = directionID, StoMoveID = stoMoveStoInID };
        }
    }
}

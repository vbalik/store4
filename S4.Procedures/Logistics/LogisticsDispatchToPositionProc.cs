﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPoco;
using S4.Core;
using S4.Entities;
using S4.Entities.Models;
using S4.ProcedureModels.Extensions;
using S4.ProcedureModels;
using S4.ProcedureModels.Logistics;
using S4.StoreCore.Interfaces;


namespace S4.Procedures.Logistics
{
    [ChangesDirectionAssignment]
    public class LogisticsDispatchToPositionProc : ProceduresBase<LogisticsDispatchToPosition>
    {
        public LogisticsDispatchToPositionProc(string userID) : base(userID)
        {
        }


        protected override LogisticsDispatchToPosition DoIt(LogisticsDispatchToPosition procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new LogisticsDispatchToPosition() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            var directionDisp = (new DAL.DirectionDAL()).GetDirectionAllData(procModel.DirectionID);
            if (directionDisp == null)
                return new LogisticsDispatchToPosition() { Success = false, ErrorText = $"Doklad nebyl nalezen; DirectionID: {procModel.DirectionID}" };

            // do tests
            if (directionDisp.DocDirection != Direction.DOC_DIRECTION_OUT)
                return new LogisticsDispatchToPosition() { Success = false, ErrorText = $"Doklad nemá DocDirection DOC_DIRECTION_OUT; DirectionID: {procModel.DirectionID}" };

            if (directionDisp.DocStatus != Direction.DR_STATUS_DISP_CHECK_DONE)
                return new LogisticsDispatchToPosition() { Success = false, ErrorText = $"Doklad nemá DocStatus DR_STATUS_DISP_CHECK_DONE; DirectionID: {procModel.DirectionID}" };

            if (directionDisp.XtraData.IsNull(Direction.XTRA_DISPATCH_POSITION_ID, 0))
                return new LogisticsDispatchToPosition() { Success = false, ErrorText = $"Doklad nemá nastavenu expediční pozici; DirectionID: {procModel.DirectionID}" };

            // if ReceivePositionID not set, get default
            if (procModel.ReceivePositionID == 0)
                procModel.ReceivePositionID = Singleton<DAL.SettingsSet>.Instance.GetValue_Int32(SettingsDictionary.RECEIVE_POSITION_ID);


            DirectionModel directionIn = new DirectionModel();
            var now = DateTime.Now;

            int stoMoveStoInID;
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                using (var tr = db.GetTransaction())
                {
                    // OUT part

                    // prepare data for StoreOut
                    int expedPositionID = (int)directionDisp.XtraData[Direction.XTRA_DISPATCH_POSITION_ID];
                    var dispatchPositionsModel = Singleton<DAL.Cache.PositionByCategoryModelCache>.Instance.GetItem(Entities.Position.PositionCategoryEnum.Dispatch);
                    var save = Singleton<ServicesFactory>.Instance.GetSave();
                    var toStoreOut = save.MoveResult_ByDirection(db, directionDisp.DirectionID, StoreMove.MO_DOCTYPE_DISPATCH, (mi) => dispatchPositionsModel.PositionIDs.Contains(mi.PositionID));
                    var saveItemList = toStoreOut.Select(item => new SaveItem()
                    {
                        ArtPackID = item.ArtPackID,
                        BatchNum = item.BatchNum,
                        CarrierNum = item.CarrierNum,
                        ExpirationDate = item.ExpirationDate,
                        Quantity = item.Quantity,
                        Parent_docPosition = item.DocPosition,
                        Xtra_PositionID = item.PositionID
                    }).ToList();

                    // do expedition (StoreOut)
                    var stoMoveOutID = save.StoreOut(
                        db, _userID, expedPositionID, StoreMove.MO_PREFIX_EXPED, saveItemList, true,
                        (storeMove) =>
                        {
                            storeMove.Parent_directionID = directionDisp.DirectionID;
                        },
                        (saveItem, storeMoveItem) =>
                        {
                            // rewriting storeMoveItem.PositionID - dispatch moves can be from different dispatch positions
                            storeMoveItem.PositionID = saveItem.Xtra_PositionID;
                        },
                        out string msg);
                    if (stoMoveOutID == -1)
                        return new LogisticsDispatchToPosition() { ErrorText = msg };

                    // create store out history
                    var storeMoveHistOut = new StoreMoveHistory()
                    {
                        EntryDateTime = DateTime.Now,
                        EntryUserID = _userID,
                        EventCode = StoreMove.MO_STATUS_SAVED_EXPED,
                        StoMoveID = stoMoveOutID
                    };
                    db.Insert(storeMoveHistOut);


                    // IN part

                    // create new receive direction
                    //  with status MO_STATUS_SAVED_RECEIVE
                    var prefix = Singleton<DAL.SettingsSet>.Instance.GetValue_String(Entities.SettingsDictionary.RECEIVE_DOC_NUM_PREFIX);
                    directionIn = new DirectionModel()
                    {
                        DocDate = now,
                        DocDirection = 1,
                        DocNumPrefix = prefix,
                        DocStatus = Direction.DR_STATUS_STOIN_COMPLET,
                        DocYear = now.ToString("yy"),
                        EntryDateTime = now,
                        EntryUserID = _userID,
                        PartAddrID = directionDisp.PartAddrID,
                        PartnerID = directionDisp.PartnerID,
                        PartnerRef = directionDisp.DocInfo()
                    };

                    // create direction in items - from grouped saveItemList
                    var docPosition = 1;
                    directionIn.Items = saveItemList
                        .GroupBy(k => k.ArtPackID, (ArtPackID, items) => new DirectionItem()
                        {
                            DocPosition = docPosition++,
                            ItemStatus = DirectionItem.DI_STATUS_RECEIVE_COMPLET,
                            ArtPackID = ArtPackID,
                            ArticleCode = DBHelpers.Articles.GetArticleCode(ArtPackID),
                            Quantity = items.Sum(i => i.Quantity),
                            EntryUserID = _userID,
                            EntryDateTime = now
                        }).ToList();

                    // save
                    directionIn.DocNumber = DBHelpers.DocNumbers.GetNextDocNumber(db, directionIn);
                    db.Insert(directionIn);
                    directionIn.Items.ForEach(di =>
                    {
                        di.DirectionID = directionIn.DirectionID;
                        db.Insert(di);
                    });

                    // and assign storein worker
                    DirectionAssign directionAssign = null;
                    if (!string.IsNullOrEmpty(procModel.WorkerID))
                    {
                        directionAssign = new DirectionAssign()
                        {
                            JobID = DirectionAssign.JOB_ID_STORE_IN,
                            WorkerID = procModel.WorkerID,
                            EntryUserID = _userID,
                            EntryDateTime = now,
                            DirectionID = directionIn.DirectionID
                        };
                        db.Insert(directionAssign);
                    }

                    // add info to history
                    var dirInHistory = new DirectionHistory()
                    {
                        DirectionID = directionIn.DirectionID,
                        DocPosition = 0,
                        EventCode = Direction.DR_EVENT_DISP_TO_POSITION,
                        EventData = $"{directionDisp.DirectionID}:{directionDisp.DocInfo()}",
                        EntryUserID = _userID,
                        EntryDateTime = now
                    };
                    db.Insert(dirInHistory);


                    // do receive 
                    var stoMoveReceiveID = save.StoreIn(
                        db, _userID, procModel.ReceivePositionID, StoreMove.MO_PREFIX_RECEIVE, saveItemList.ToList(), true, (storeMove) =>
                        {
                            storeMove.Parent_directionID = directionIn.DirectionID;
                        },
                        (saveItem, storeMoveItem) =>
                        {
                            storeMoveItem.Parent_docPosition = DBHelpers.Directions.FindParentDocPosition(saveItem.ArtPackID, directionIn.Items);
                        },
                        out string message);
                    if (stoMoveReceiveID == -1)
                        return new LogisticsDispatchToPosition() { ErrorText = message };

                    // create store in history
                    var storeMoveReceiveHist = new StoreMoveHistory()
                    {
                        EntryDateTime = DateTime.Now,
                        EntryUserID = _userID,
                        EventCode = StoreMove.MO_STATUS_SAVED_RECEIVE,
                        StoMoveID = stoMoveReceiveID
                    };
                    db.Insert(storeMoveReceiveHist);


                    // update disp direction

                    // set disp direction status to DR_STATUS_DISP_EXPED
                    directionDisp.DocStatus = Direction.DR_STATUS_DISP_EXPED;
                    db.Update(directionDisp);

                    // add info to history
                    var dirDispHistory = new DirectionHistory()
                    {
                        DirectionID = directionDisp.DirectionID,
                        DocPosition = 0,
                        EventCode = Direction.DR_EVENT_DISP_TO_STOIN,
                        EventData = $"{directionIn.DirectionID}:{directionIn.DocInfo()}",
                        EntryUserID = _userID,
                        EntryDateTime = now
                    };
                    db.Insert(dirDispHistory);


                    // do store in
                    var storeMoveItems = saveItemList
                    .Select(i => new MoveItem()
                    {
                        ArtPackID = i.ArtPackID,
                        BatchNum = i.BatchNum,
                        CarrierNum = i.CarrierNum,
                        ExpirationDate = i.ExpirationDate,
                        Quantity = i.Quantity,

                        SourcePositionID = procModel.ReceivePositionID,
                        DestinationPositionID = procModel.DestPositionID,

                        Parent_docPosition = i.Parent_docPosition
                    })
                    .ToList();

                    // make storeMove
                    stoMoveStoInID = new StoreCore.Save_Basic.Save().Move(db, _userID, StoreMove.MO_PREFIX_STORE_IN, StoreMove.MO_DOCTYPE_STORE_IN, storeMoveItems, true, sm =>
                    {
                        sm.DocStatus = StoreMove.MO_STATUS_NEW_STOIN;
                        sm.Parent_directionID = directionIn.DirectionID;
                    },
                    null,
                    out string erMsg);
                    if (stoMoveStoInID == -1)
                        return new LogisticsDispatchToPosition() { ErrorText = erMsg };

                    // create history
                    var storeMoveStoInHist = new StoreMoveHistory()
                    {
                        EntryDateTime = DateTime.Now,
                        EntryUserID = _userID,
                        EventCode = StoreMove.MO_STATUS_NEW_STOIN,
                        StoMoveID = stoMoveStoInID
                    };
                    db.Insert(storeMoveStoInHist);

                    tr.Complete();
                }
            }

            // log
            base.Info($"LogisticsDispatchToPositionProc; stoMoveStoInID: {stoMoveStoInID}");
            return new LogisticsDispatchToPosition() { Success = true, StoMoveID = stoMoveStoInID };
        }
    }
}

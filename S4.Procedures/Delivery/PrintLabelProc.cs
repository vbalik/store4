﻿using System;
using S4.Entities;
using S4.ProcedureModels.Delivery;
using System.Linq;
using S4.Core;
using S4.DeliveryServices;
using S4.Entities.Models;
using System.Collections.Generic;

namespace S4.Procedures.Delivery
{
    public class PrintLabelProc : ProceduresBase<PrintLabel>
    {
        const int MAX_PRINT_QUANTITY = 10;

        public PrintLabelProc(string userID) : base(userID)
        {
        }

        protected override PrintLabel DoIt(PrintLabel procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new PrintLabel() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //check
                var direction = new DAL.DirectionDAL().GetDirectionXtraData(procModel.DirectionID);
                if (direction == null)
                    return new PrintLabel() { Success = false, ErrorText = $"Doklad nebyl nalezen; DirectionID: {procModel.DirectionID}" };

                // get direction with xtra data
                if (direction.DocDirection != Direction.DOC_DIRECTION_OUT)
                    return new PrintLabel() { Success = false, ErrorText = $"Direction nemá DocDirection DOC_DIRECTION_OUT; DirectionID: {procModel.DirectionID}" };

                if (direction.DocStatus != Direction.DR_STATUS_DELIVERY_SHIPMENT && direction.DocStatus != Direction.DR_STATUS_DELIVERY_PRINT)
                    return new PrintLabel() { Success = false, ErrorText = $"Direction nemá DocStatus DR_STATUS_DELIVERY_SHIPMENT nebo DR_STATUS_DELIVERY_PRINT; DirectionID: {procModel.DirectionID}" };

                // choose shipment label or raw label print depend on provider
                var deliveryProvider = new DeliveryDirection().TTYPEToDeliveryProvider(direction.XtraData[Direction.XTRA_TRANSPORTATION_TYPE].ToString());
                if (deliveryProvider == DeliveryDirection.DeliveryProviderEnum.PPL)
                {
                    return _RawLabelPrint(procModel, direction);
                }
                else
                {
                    return _ShipmentLabelPrint(procModel, direction);
                }
            }
        }

        private PrintLabel _ShipmentLabelPrint(PrintLabel procModel, DirectionModel direction)
        {
            var result = new PrintLabel();
            var deliveryService = new ServicesFactory().GetDeliveryService();
            deliveryService.LogEvent += (s, m, l, e) =>
            {
                if (l == LogLevelEnum.Error)
                {
                    var exceptionMsg = "";
                    if (e != null)
                        exceptionMsg = $" {e.Message}";

                    var deliveryProvider = "";
                    if (s != null)
                        deliveryProvider = s.DeliveryProvider.ToString();

                    result.ErrorText = $"Tisk štítků {deliveryProvider} se nezdařil: {m}{exceptionMsg}";

                    if (e != null)
                        base.Error(result.ErrorText, e);
                }
            };
            deliveryService.GetLabelEvent += (s, args) =>
            {
                result.Success = true;
                result.FileData = args.Data;
                result.LocalPrint = procModel.LocalPrint;
            };

            var task = deliveryService.GetShipmentLabel(procModel.DirectionID);
            task.Wait();

            if (!result.Success)
                return result;

            // log
            base.Info($"PrintLabel; DirectionID: {procModel.DirectionID}");

            if (direction.DocStatus == Direction.DR_STATUS_DELIVERY_SHIPMENT)
            {
                var backgroundTaskQueue = (new ServicesFactory()).GetBackgroundTaskQueue();
                var proc = new LabelPrintedProc(_userID);
                backgroundTaskQueue.QueueProcedure<LabelPrinted>(proc, new LabelPrinted() { DirectionID = procModel.DirectionID });
            }

            return result;
        }

        private PrintLabel _RawLabelPrint(PrintLabel procModel, DirectionModel direction)
        {
            if (procModel.LabelsQuant > MAX_PRINT_QUANTITY)
                return new PrintLabel() { Success = false, ErrorText = $"Požadované množství překročilo maximální množství ({MAX_PRINT_QUANTITY})" };

            if (procModel.LabelsQuant == null || procModel.LabelsQuant < 1)
                procModel.LabelsQuant = 1;

            var ReportID = Singleton<DAL.SettingsSet>.Instance.GetValue_Int32(Entities.SettingsDictionary.DELIVERY_PRINT_S4_RAW_LABEL_REPORT_PPL_ID);

            // get delivery direction for pack number
            DeliveryDirectionModel deliveryDirectionModel = null;
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                deliveryDirectionModel = new DAL.DeliveryDirectionDAL().GetDeliveryDirectionAllData(procModel.DirectionID, db);
            }

            // get definition
            var printerLocation = Singleton<DAL.Cache.PrinterLocationCache>.Instance.GetItem(procModel.PrinterLocationID ?? 0);
            if (printerLocation == null)
                return new PrintLabel() { Success = false, ErrorText = $"Nenalezena tiskárna. (PrinterLocationID: {procModel.PrinterLocationID})" };

            var reportDef = Singleton<DAL.Cache.ReportCache>.Instance.GetItem(ReportID);
            if (reportDef == null)
                return new PrintLabel() { Success = false, ErrorText = $"Nenalezen report. (ReportID: {ReportID}" };

            // get other data
            direction.Partner = Singleton<DAL.Cache.PartnerCache>.Instance.GetItem(direction.PartnerID);
            direction.PartnerAddress = Singleton<DAL.Cache.PartnerAddressCache>.Instance.GetItem(direction.PartAddrID);

            // print
            var report = new Report.BarCodeReport(reportDef.Template);

            if (IsTesting)
            {
                var toPrint = report.Render(direction, direction.Items[0], DeliveryProviderDataResolver.GetRecipient(direction), DeliveryProviderDataResolver.ResolveData(direction, deliveryDirectionModel.Items.First(), 1, deliveryDirectionModel.Items.Count()));
                RenderResult = toPrint;
            }
            else
            {
                if (deliveryDirectionModel == null || deliveryDirectionModel.Items == null)
                    return new PrintLabel() { Success = false, ErrorText = "Hodnota 'Items' nemůže být prázdná!" };

                var dirItemCnt = 1;
                foreach (var dirItem in deliveryDirectionModel.Items)
                {
                    for (int i = 0; i < procModel.LabelsQuant; i++)
                    {
                        var toPrint = report.Render(direction, dirItem, DeliveryProviderDataResolver.GetRecipient(direction), DeliveryProviderDataResolver.ResolveData(direction, dirItem, dirItemCnt, deliveryDirectionModel.Items.Count()));
                        Report.Printers.PrintDirect.PrintRAW(printerLocation.PrinterPath, "DeliveryLabel", toPrint);
                    }

                    dirItemCnt++;
                }
            }

            return new PrintLabel() { Success = true };
        }
    }

    /// <summary>
    /// Get specific data for current delivery provider
    /// </summary>
    internal class DeliveryProviderDataResolver
    {
        /// <summary>
        /// Get specific data
        /// </summary>
        public static object ResolveData(DirectionModel directionModel, DeliveryDirectionItem deliveryDirectionItem, int packOrderNum, int packCount)
        {
            var deliveryProvider = new DeliveryDirection().TTYPEToDeliveryProvider(directionModel.XtraData[Direction.XTRA_TRANSPORTATION_TYPE].ToString());
            switch (deliveryProvider)
            {
                case DeliveryDirection.DeliveryProviderEnum.PPL:
                    return PPLdata(directionModel, deliveryDirectionItem, packOrderNum, packCount);
                default:
                    throw new Exception($"Data for delivery provider {deliveryProvider} not found.");
            }
        }

        /// <summary>
        /// Get PPL provider data
        /// </summary>
        private static object PPLdata(DirectionModel directionModel, DeliveryDirectionItem deliveryDirectionItem, int packOrderNum, int packCount)
        {
            // set cod
            var cod = "";
            decimal? codPrice = null;
            var codCurrency = "";
            var paymentType = DataParserHelper.GetPaymentType(directionModel);
            if (paymentType == DeliveryDirection.DeliveryPaymentEnum.CAOD || paymentType == DeliveryDirection.DeliveryPaymentEnum.CHOD)
            {
                cod = "COD/DOB:";
                codPrice = packOrderNum == 1 ? DataParserHelper.GetCashOnDelivery(directionModel) : 0;
                codCurrency = DataParserHelper.GetCurrency(directionModel).ToString();
            }

            // set weight
            var weight = DataParserHelper.GetWeight(directionModel, packOrderNum);

            // set country code
            var countryCode = "";
            if (DataParserHelper.GetCountry(directionModel).ToString() == DeliveryDirection.DeliveryCountryEnum.SK.ToString())
                countryCode = DeliveryDirection.DeliveryCountryEnum.SK.ToString();

            // set services
            var service = "";
            if (DataParserHelper.GetTransportationType(directionModel) == TransportationType.VecerniDoruceni)
            {
                service = "VECER";
            }
            if (!string.IsNullOrEmpty(countryCode) && !string.IsNullOrEmpty(cod))
            {
                service = "COD";
            }

            // get product name by ico
            var productName = !string.IsNullOrEmpty(directionModel.Partner.PartnerOrgIdentNumber)
                ? "BUSINESS"
                : "PRIVATE";

            (string name, string address, string zipCode) senderAddress = new ServicesFactory()
                .GetDeliveryService()
                .GetSenderAddress(DeliveryDirection.DeliveryProviderEnum.PPL.ToString());

            // routing square
            var routingSquare = !DataParserHelper.GetRoutingSquare(directionModel)
                ? ""
                : @"PX138,142,6";

            // delivery instructions
            // add receiving person
            var deliveryInstList = _SplitTextHelper(DataParserHelper.GetReceivingPerson(directionModel, directionModel?.PartnerAddress?.AddrPhone, true) + " " + directionModel?.PartnerAddress?.DeliveryInst, 25);
            var deliveryInstRow = deliveryInstList.Count > 0 ? deliveryInstList[0] : "";
            var deliveryInstRow2 = deliveryInstList.Count > 1 ? $"PP452,707:PT \"{deliveryInstList[1]}\"" : "";
            
            var data = new
            {
                Note = DataParserHelper.GetShipInfo(directionModel),
                CustomerReferenceNumber = $"{directionModel.DocNumPrefix}/{directionModel.DocYear}/{directionModel.DocNumber}",
                ProductName = productName,

                CountryCode = countryCode,
                ZipCode = DataParserHelper.GetRouteCode(directionModel)?.ToString(),
                Weight = weight != null ? $"{weight} kg" : null,
                TodayDate = DateTime.Now.ToString("d.M.yyyy"),

                PackNumber = deliveryDirectionItem.ParcelRefNumber,
                MainEanCode = deliveryDirectionItem.ParcelRefNumber + "-" + DataParserHelper.GetRouteCode(directionModel),
                Service = service,

                PackCount = packCount,
                PackOrderNum = packOrderNum,

                Cod = cod,
                CodPrice = $"{codPrice:0,00} {codCurrency}",
                CodService = !string.IsNullOrEmpty(cod) ? "- COD" : string.Empty,

                RouteCode = DataParserHelper.GetRouteCode(directionModel)?.ToString(),
                DepoCode = DataParserHelper.GetDepoCode(directionModel)?.ToString(),

                SenderName = senderAddress.name,
                SenderAddress = senderAddress.address,
                SenderZipCode = senderAddress.zipCode,

                RoutingSquare = routingSquare,

                DeliveryInstRow = deliveryInstRow,
                DeliveryInstRow2 = deliveryInstRow2
            };

            return data;
        }

        /// <summary>
        /// Get recipient data
        /// </summary>
        public static object GetRecipient(DirectionModel directionModel)
        {
            var country = DeliveryDirection.DeliveryCountryEnum.CZ;
            if (!directionModel.XtraData.IsNull(Direction.XTRA_DELIVERY_COUNTRY))
                country = (DeliveryDirection.DeliveryCountryEnum)Enum.Parse(typeof(DeliveryDirection.DeliveryCountryEnum), directionModel.XtraData[Direction.XTRA_DELIVERY_COUNTRY].ToString());

            var isParcelShop = false;

            var recipient = new
            {
                City = directionModel.PartnerAddress.AddrCity,
                Contact = isParcelShop ? directionModel.PartnerAddress.AddrName : string.Empty,
                Country = country,
                Email = string.Empty,
                Name = directionModel.PartnerAddress.AddrName,
                Phone = directionModel.PartnerAddress.AddrPhone,
                Street = directionModel.PartnerAddress.AddrLine_1,
                ZipCode = directionModel.PartnerAddress.AddrLine_2.Replace(" ", "")
            };

            return recipient;
        }

        private static List<string> _SplitTextHelper(string str, int chunkSize)
        {
            return Enumerable.Range(0, (str.Length + chunkSize - 1) / chunkSize)
                .Select(i => str.Substring(i * chunkSize, (i * chunkSize + chunkSize <= str.Length) ? chunkSize : str.Length - i * chunkSize))
                .ToList();
        }
    }
}

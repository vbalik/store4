﻿using System;
using System.Collections.Generic;
using System.Text;
using NPoco;
using S4.Entities;
using S4.ProcedureModels.Delivery;
using System.Linq;
using S4.ProcedureModels.Extensions;
using S4.Entities.XtraData;
using S4.Procedures.DBHelpers;
using S4.StoreCore.Interfaces;
using S4.Core;
using S4.DAL;

namespace S4.Procedures.Delivery
{
    [ChangesDirectionStatus]
    public class LabelPrintedProc : ProceduresBase<LabelPrinted>
    {
        public LabelPrintedProc(string userID) : base(userID)
        {
        }

        protected override LabelPrinted DoIt(LabelPrinted procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new LabelPrinted() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //check
                var direction = new DAL.DirectionDAL().Get(procModel.DirectionID);
                if (direction == null)
                    return new LabelPrinted() { Success = false, ErrorText = $"Doklad nebyl nalezen; DirectionID: {procModel.DirectionID}" };

                // do tests
                if (direction.DocDirection != Direction.DOC_DIRECTION_OUT)
                    return new LabelPrinted() { Success = false, ErrorText = $"Direction nemá DocDirection DOC_DIRECTION_OUT; DirectionID: {procModel.DirectionID}" };

                if (direction.DocStatus == Direction.DR_STATUS_DELIVERY_PRINT)
                {
                    //label is reprinted - nothing to do
                    return new LabelPrinted() { Success = true };
                }

                if (direction.DocStatus != Direction.DR_STATUS_DELIVERY_SHIPMENT)
                    return new LabelPrinted() { Success = false, ErrorText = $"Direction nemá DocStatus DR_STATUS_DELIVERY_SHIPMENT; DirectionID: {procModel.DirectionID}" };

                // change status
                direction.DocStatus = Direction.DR_STATUS_DELIVERY_PRINT;

                // make history
                var history = new DirectionHistory()
                {
                    DirectionID = procModel.DirectionID,
                    DocPosition = 0,
                    EventCode = Direction.DR_STATUS_DELIVERY_PRINT,
                    EntryUserID = _userID,
                    EntryDateTime = DateTime.Now
                };

                using (var tr = db.GetTransaction())
                {
                    // save direction
                    db.Save(direction);

                    // save history
                    db.Insert(history);

                    tr.Complete();
                }


                // log
                base.Info($"LabelPrinted; DirectionID: {procModel.DirectionID}");

                return new LabelPrinted() { Success = true };
            }
        }
    }
}

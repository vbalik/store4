﻿using System;
using S4.Core.Utils;
using S4.Entities;
using S4.ProcedureModels.Delivery;


namespace S4.Procedures.Delivery
{
    [ChangesDirectionStatus]
    public class DirectionSetShipmentFailedStatusProc : ProceduresBase<DirectionSetShipmentFailedStatus>
    {
        public DirectionSetShipmentFailedStatusProc(string userID) : base(userID)
        {
        }


        protected override DirectionSetShipmentFailedStatus DoIt(DirectionSetShipmentFailedStatus procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new DirectionSetShipmentFailedStatus() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            // set direction status
            var status = Direction.DR_STATUS_DELIVERY_SHIPMENT_FAILED;

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //check
                var direction = (new DAL.DirectionDAL()).GetDirectionAllData(procModel.DirectionID);
                if (direction == null)
                    return new DirectionSetShipmentFailedStatus() { Success = false, ErrorText = $"Doklad nebyl nalezen; DirectionID: {procModel.DirectionID}" };

                // make info
                var message = new InternMessage()
                {
                    MsgClass = InternMessage.DR_STATUS_DELIVERY_SHIPMENT_FAILED,
                    MsgSeverity = InternMessage.MessageSeverityEnum.Error,
                    MsgHeader = $"Chyba zásilku nelze odeslat. Doklad '{direction.DocNumPrefix}/{direction.DocYear}/{direction.DocNumber}",
                    MsgDateTime = DateTime.Now,
                    MsgUserID = _userID,
                    MsgText = procModel.Text
                };

                // make history
                var history = new DirectionHistory()
                {
                    DirectionID = procModel.DirectionID,
                    DocPosition = 0,
                    EventCode = status,
                    EventData = procModel.Text,
                    EntryUserID = _userID,
                    EntryDateTime = DateTime.Now
                };

                // save
                using (var tr = db.GetTransaction())
                {
                    // save 
                    direction.DocStatus = status;
                    db.Update(direction);

                    //his
                    db.Insert(history);

                    // save info
                    db.Insert(message);

                    // do save
                    tr.Complete();
                }

                // log
                base.Info($"DirectionSetShipmentFailedStatusProc; DirectionID: {procModel.DirectionID}");

                return new DirectionSetShipmentFailedStatus() { Success = true };
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using NPoco;
using S4.Entities;
using S4.ProcedureModels.Delivery;
using System.Linq;
using S4.ProcedureModels.Extensions;
using S4.Entities.XtraData;
using S4.Procedures.DBHelpers;
using S4.StoreCore.Interfaces;
using S4.Core;
using S4.DAL;
using S4.DeliveryServices;

namespace S4.Procedures.Delivery
{
    [ChangesDirectionStatus]
    public class ResetShipmentProc : ProceduresBase<ResetShipment>
    {
        public ResetShipmentProc(string userID) : base(userID)
        {
        }

        protected override ResetShipment DoIt(ResetShipment procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new ResetShipment() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //check
                var direction = new DAL.DirectionDAL().GetDirectionAllData(procModel.DirectionID);
                if (direction == null)
                    return new ResetShipment() { Success = false, ErrorText = $"Doklad nebyl nalezen; DirectionID: {procModel.DirectionID}" };

                //check delivery provider
                var errorDelivery = false;
                if (!direction.XtraData.IsNull(Direction.XTRA_TRANSPORTATION_TYPE))
                {
                    var deliveryProvider = new DeliveryDirection().TTYPEToDeliveryProvider(direction.XtraData[Direction.XTRA_TRANSPORTATION_TYPE].ToString());
                    if (deliveryProvider == DeliveryDirection.DeliveryProviderEnum.NONE)
                        errorDelivery = true;
                }
                else
                    errorDelivery = true;

                if (errorDelivery)
                    return new ResetShipment() { Success = false, ErrorText = $"Doklad neobsahuje žádného přepravce; Není možné znovu vytvořit zásilku! DirectionID: {procModel.DirectionID}" };

                //try to load existing if is saved from previous unsuccessfull attempt
                var deliveryDirections = new DAL.DeliveryDirectionDAL().GetAllRowsByDirectionID(procModel.DirectionID, db);

                DeliveryDirection deliveryDirectionToCancel = null;
                if (deliveryDirections == null || deliveryDirections.Count < 1)
                    Warn($"Doklad v DeliveryDirection nebyl nalezen; DirectionID: {procModel.DirectionID}");
                else
                {
                    deliveryDirectionToCancel = deliveryDirections.Last();
                    if (deliveryDirectionToCancel.DeliveryStatus == DeliveryDirection.DD_STATUS_CANCELED)
                    {
                        direction.DocStatus = Direction.DR_STATUS_DISP_CHECK_DONE;
                        //save status direction
                        db.Save(direction);
                        return new ResetShipment() { Success = false, ErrorText = $"Doklad již čeká na vytvoření požadavku na dopravu." };
                    }

                    //change deliveryDirection status
                    deliveryDirectionToCancel.DeliveryStatus = DeliveryDirection.DD_STATUS_CANCELED;
                }

                //direction status
                direction.DocStatus = Direction.DR_STATUS_DISP_CHECK_DONE;

                //xtra Weight and BoxesCnt
                direction.XtraData[Direction.XTRA_DISPATCH_WEIGHT] = procModel.Weight;
                direction.XtraData[Direction.XTRA_DISPATCH_BOXES_CNT] = procModel.BoxesCnt;

                // make history
                var history = new DirectionHistory()
                {
                    DirectionID = procModel.DirectionID,
                    DocPosition = 0,
                    EventCode = Direction.DR_STATUS_DISP_CHECK_DONE,
                    EntryUserID = _userID,
                    EntryDateTime = DateTime.Now
                };

                //Delete Shipment
                if (!IsTesting && deliveryDirectionToCancel != null)
                {
                    Type type = Type.GetType($"S4.DeliveryServices.DS{(DeliveryDirection.DeliveryProviderEnum)deliveryDirectionToCancel.DeliveryProvider}, S4.DeliveryServices");
                    var deliveryService = (IDeliveryService)Activator.CreateInstance(type);
                    deliveryService.LogEvent += (s, m, l, e) => SendToLog(s, m, l, e);

                    //find reference by directionID
                    var referenceId = new DeliveryDirectionDAL().GetByDirectionID(procModel.DirectionID, db)?.ShipmentRefNumber;
                    if (referenceId != null)
                    {
                        base.Debug($"ResetShipment; Deleting old shipment; referenceId: {referenceId}; DirectionID: {procModel.DirectionID}");
                        var task = deliveryService.DeleteShipment(referenceId);
                        task.Wait();
                    }
                }

                //save
                using (var tr = db.GetTransaction())
                {
                    //save deliveryDirection
                    if (deliveryDirectionToCancel != null)
                        db.Save(deliveryDirectionToCancel);

                    //save direction
                    db.Save(direction);

                    //save history
                    db.Save(history);

                    //save extraData
                    direction.XtraData.Save(db, procModel.DirectionID);

                    tr.Complete();
                }

                // log
                base.Info($"ResetShipment; DirectionID: {procModel.DirectionID}; BoxesCnt: {procModel.BoxesCnt}; Weight {procModel.Weight}");

                return new ResetShipment() { Success = true };
            }
        }

        private void SendToLog(S4.DeliveryServices.IDeliveryService sender, string message, S4.DeliveryServices.LogLevelEnum logLevel, Exception ex = null)
        {
            if (ex != null)
                base.Error(message, ex);
            else
                base.Info(message);
        }


    }
}

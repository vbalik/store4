﻿using System;
using System.Collections.Generic;
using System.Text;
using NPoco;
using S4.Entities;
using S4.ProcedureModels.Delivery;
using System.Linq;
using S4.ProcedureModels.Extensions;
using S4.Entities.XtraData;
using S4.Procedures.DBHelpers;
using S4.StoreCore.Interfaces;
using S4.Core;
using S4.DAL;

namespace S4.Procedures.Delivery
{
    public class GetDirectionsForManifestProc : ProceduresBase<GetDirectionsForManifest>
    {
        public GetDirectionsForManifestProc(string userID) : base(userID)
        {
        }

        protected override GetDirectionsForManifest DoIt(GetDirectionsForManifest procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new GetDirectionsForManifest() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //check
                var sql = new Sql("select directionID from s4_direction where docStatus = @0 and directionID in (select directionID from S4_deliveryDirection where deliveryProvider = @1 and deliveryManifestID is null)",
                    Direction.DR_STATUS_DELIVERY_PRINT, procModel.DeliveryProvider);
                var directionIDs = db.Fetch<int>(sql);

                return new GetDirectionsForManifest() { Success = true, Directions = directionIDs };
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using NPoco;
using S4.Entities;
using S4.ProcedureModels.Delivery;
using System.Linq;
using S4.ProcedureModels.Extensions;
using S4.Entities.XtraData;
using S4.Procedures.DBHelpers;
using S4.StoreCore.Interfaces;
using S4.Core;
using S4.DAL;
using S4.DeliveryServices;
using System.Threading.Tasks;

namespace S4.Procedures.Delivery
{
    public class GetShipmentStatusProc : ProceduresBase<GetShipmentStatus>
    {
        public GetShipmentStatusProc(string userID) : base(userID)
        {
        }

        protected override GetShipmentStatus DoIt(GetShipmentStatus procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new GetShipmentStatus() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //check
                var direction = new DAL.DirectionDAL().Get(procModel.DirectionID);
                if (direction == null)
                    return new GetShipmentStatus() { Success = false, ErrorText = $"Doklad nebyl nalezen; DirectionID: {procModel.DirectionID}" };

                // do tests
                if (direction.DocDirection != Direction.DOC_DIRECTION_OUT)
                    return new GetShipmentStatus() { Success = false, ErrorText = $"Direction nemá DocDirection DOC_DIRECTION_OUT; DirectionID: {procModel.DirectionID}" };

                var result = new GetShipmentStatus();
                result.Success = false;
                var deliveryService = new ServicesFactory().GetDeliveryService();
                deliveryService.LogEvent += (s, m, l, e) =>
                {
                    if (l == LogLevelEnum.Error)
                    {
                        var exceptionMsg = "";
                        if (e != null)
                            exceptionMsg = $" {e.Message}";

                        var deliveryProvider = "";
                        if (s != null)
                            deliveryProvider = s.DeliveryProvider.ToString();

                        result.ErrorText += $"Pokus o dohledání zásilky {deliveryProvider} pro {direction.DocInfo()} skončil chybou: {m}{exceptionMsg} ";

                        if (e != null)
                            base.Error(result.ErrorText, e);
                    }
                };
                deliveryService.GetShipmentStatusEvent += (s, p, args) =>
                {
                    result.Success = true;

                    if (result.Content == null)
                        result.Content = new List<Entities.Models.LabeledValue>();

                    result.Content.AddRange(args.ToList());
                    result.Provider = p;
                };

                if (procModel.IncludingCanceledShipment)
                {
                    var rows = new DeliveryDirectionDAL().GetAllRowsByDirectionID(procModel.DirectionID, db).OrderByDescending(_ => _.DeliveryDirectionID)
                        .GroupBy(d => d.ShipmentRefNumber).Select(grp => grp.First()).ToList();

                    foreach (var row in rows)
                    {
                        var task = deliveryService.GetShipmentStatus(procModel.DirectionID, row.DeliveryDirectionID);
                        task.Wait();
                    }
                }
                else
                {
                    var task = deliveryService.GetShipmentStatus(procModel.DirectionID, 0);
                    task.Wait();
                }

                if (result.Success)
                    base.Info($"GetShipmentStatus; DirectionID: {procModel.DirectionID}");

                return result;
            }
        }
    }
}

﻿using System;
using S4.Entities;
using S4.ProcedureModels.Delivery;
using System.Linq;
using System.Reflection;
using System.ComponentModel.DataAnnotations;

namespace S4.Procedures.Delivery
{
    public class GetDeliveryProvidersProc : ProceduresBase<GetDeliveryProviders>
    {
        public GetDeliveryProvidersProc(string userID) : base(userID)
        {
        }

        protected override GetDeliveryProviders DoIt(GetDeliveryProviders procModel)
        {
            try
            {
                var providers = new ServicesFactory().GetDeliveryService().GetDeliveryProviders();
                return new GetDeliveryProviders() 
                { 
                    Success = true, 
                    Providers = providers.Select(i => (i, (typeof(DeliveryDirection.DeliveryProviderEnum).GetField(i.ToString()).GetCustomAttribute(typeof(DisplayAttribute)) as DisplayAttribute).Name)) 
                };
            }
            catch (Exception ex)
            {
                return new GetDeliveryProviders() { ErrorText = ex.ToString() };
            }
        }
    }
}

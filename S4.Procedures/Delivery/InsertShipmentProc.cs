﻿using System;
using System.Collections.Generic;
using System.Text;
using NPoco;
using S4.Entities;
using S4.ProcedureModels.Delivery;
using System.Linq;
using S4.ProcedureModels.Extensions;
using S4.Entities.XtraData;
using S4.Procedures.DBHelpers;
using S4.StoreCore.Interfaces;
using S4.Core;
using S4.DAL;

namespace S4.Procedures.Delivery
{
    [ChangesDirectionStatus]
    public class InsertShipmentProc : ProceduresBase<InsertShipment>
    {
        public InsertShipmentProc(string userID) : base(userID)
        {
        }

        protected override InsertShipment DoIt(InsertShipment procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new InsertShipment() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //check
                var direction = new DAL.DirectionDAL().Get(procModel.DirectionID);
                if (direction == null)
                    return new InsertShipment() { Success = false, ErrorText = $"Doklad nebyl nalezen; DirectionID: {procModel.DirectionID}" };

                // do tests
                if (direction.DocDirection != Direction.DOC_DIRECTION_OUT)
                    return new InsertShipment() { Success = false, ErrorText = $"Direction nemá DocDirection DOC_DIRECTION_OUT; DirectionID: {procModel.DirectionID}" };

                if (direction.DocStatus != Direction.DR_STATUS_DISP_CHECK_DONE)
                    return new InsertShipment() { Success = false, ErrorText = $"Direction nemá DocStatus DR_STATUS_DISP_CHECK_DONE; DirectionID: {procModel.DirectionID}" };

                // change status
                direction.DocStatus = Direction.DR_STATUS_DELIVERY_SHIPMENT;

                // make history
                var history = new DirectionHistory()
                {
                    DirectionID = procModel.DirectionID,
                    DocPosition = 0,
                    EventCode = Direction.DR_STATUS_DELIVERY_SHIPMENT,
                    EntryUserID = _userID,
                    EntryDateTime = DateTime.Now
                };

                //try to load existing if is saved from previous unsuccessfull attempt
                var deliveryDirection = new DAL.DeliveryDirectionDAL().GetByDirectionID(procModel.DirectionID, db);
                if (deliveryDirection == null)
                    deliveryDirection = new DeliveryDirection()
                    {
                        DirectionID = procModel.DirectionID,
                        DeliveryProvider = (int)procModel.DeliveryProvider,
                        DeliveryCreated = DateTime.Now,
                        DeliveryStatus = DeliveryDirection.DD_STATUS_OK
                    };
                else
                    deliveryDirection.DeliveryChanged = DateTime.Now;

                deliveryDirection.ShipmentRefNumber = procModel.ShipmentReference;
                deliveryDirection.CreateShipmentAttempt = (short)((deliveryDirection.CreateShipmentAttempt ?? 0) + 1);
                deliveryDirection.CreateShipmentError = null;

                using (var tr = db.GetTransaction())
                {
                    // save direction
                    db.Save(direction);

                    // save history
                    db.Insert(history);

                    //insert or update delivery direction
                    if (deliveryDirection.DeliveryDirectionID == 0)
                        deliveryDirection.DeliveryDirectionID = Convert.ToInt32(db.Insert(deliveryDirection));
                    else
                        db.Save(deliveryDirection);

                    //insert delivery direction items
                    foreach (var parcel in procModel.Parcels)
                        db.Insert(new DeliveryDirectionItem() { DeliveryDirectionID = deliveryDirection.DeliveryDirectionID, ParcelPosition = parcel.Item1, ParcelRefNumber = parcel.Item2 });

                    tr.Complete();
                }


                // log
                base.Info($"InsertShipment; DirectionID: {procModel.DirectionID}; Parcels: {procModel.Parcels.Count()}");

                return new InsertShipment() { Success = true };
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using NPoco;
using S4.Entities;
using S4.ProcedureModels.Delivery;
using System.Linq;
using S4.ProcedureModels.Extensions;
using S4.Entities.XtraData;
using S4.Procedures.DBHelpers;
using S4.StoreCore.Interfaces;
using S4.Core;
using S4.DAL;

namespace S4.Procedures.Delivery
{
    public class ManifestRequestProc : ProceduresBase<ManifestRequest>
    {
        public ManifestRequestProc(string userID) : base(userID)
        {
        }

        protected override ManifestRequest DoIt(ManifestRequest procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new ManifestRequest() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var deliveryDirections = new DAL.DeliveryDirectionDAL().GetByDirectionIDs(procModel.DirectionIDs.ToArray(), db);
                var deliveryManifest = new DeliveryManifest()
                {
                    DeliveryProvider = procModel.DeliveryProvider,
                    ManifestCreated = DateTime.Now,
                    ManifestStatus = DeliveryManifest.STATUS_WAIT
                };

                using (var transaction = db.GetTransaction())
                {
                    db.Insert(deliveryManifest);
                    foreach (var deliveryDirection in deliveryDirections)
                    {
                        deliveryDirection.DeliveryManifestID = deliveryManifest.DeliveryManifestID;
                        deliveryDirection.DeliveryChanged = DateTime.Now;
                        db.Save(deliveryDirection);
                    }

                    transaction.Complete();
                }
            }

            // log
            base.Info($"ManifestRequest; Items: {procModel.DirectionIDs.Count()}");

            return new ManifestRequest() { Success = true };
        }
    }
}

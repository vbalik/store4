﻿using System;
using System.Collections.Generic;
using System.Text;
using NPoco;
using S4.Entities;
using S4.ProcedureModels.Delivery;
using System.Linq;
using S4.ProcedureModels.Extensions;
using S4.Entities.XtraData;
using S4.Procedures.DBHelpers;
using S4.StoreCore.Interfaces;
using S4.Core;
using S4.DAL;
using S4.DeliveryServices;
using System.Threading.Tasks;

namespace S4.Procedures.Delivery
{
    public class PrintManifestProc : ProceduresBase<PrintManifest>
    {
        public PrintManifestProc(string userID) : base(userID)
        {
        }

        protected override PrintManifest DoIt(PrintManifest procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new PrintManifest() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var result = new PrintManifest();
                var deliveryService = new ServicesFactory().GetDeliveryService();
                deliveryService.LogEvent += (s, m, l, e) =>
                {
                    if (l == LogLevelEnum.Error)
                    {
                        var exceptionMsg = "";
                        if (e != null)
                            exceptionMsg = $" {e.Message}";

                        result.ErrorText = $"Tisk manifestu se nezdařil: {m}{exceptionMsg}";

                        if (e != null)
                            base.Error(result.ErrorText, e);
                    }
                };
                deliveryService.GetManifestReportEvent += (s, args) =>
                {
                    result.Success = true;
                    result.FileData = args.Data;
                    result.LocalPrint = procModel.LocalPrint;
                };

                var task = deliveryService.GetManifestReport(procModel.DeliveryProvider, procModel.ManifestRefNumber);
                task.Wait();

                if (!result.Success)
                    return result;

                // log
                base.Info($"PrintManifest; DeliveryProvider: {procModel.DeliveryProvider}; ManifestRefNumber: {procModel.ManifestRefNumber}");

                return result;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using NPoco;
using S4.Entities;
using S4.ProcedureModels.Delivery;
using System.Linq;
using S4.ProcedureModels.Extensions;
using S4.Entities.XtraData;
using S4.Procedures.DBHelpers;
using S4.StoreCore.Interfaces;
using S4.Core;
using S4.DAL;

namespace S4.Procedures.Delivery
{
    //[ChangesDirectionStatus]
    public class ManifestCreatedProc : ProceduresBase<ManifestCreated>
    {
        public ManifestCreatedProc(string userID) : base(userID)
        {
        }

        protected override ManifestCreated DoIt(ManifestCreated procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new ManifestCreated() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var directionList = new List<Direction>();
                var historyList = new List<DirectionHistory>();

                //get delivery manifest
                var deliveryManifest = new DAL.DeliveryManifestDAL().Get(procModel.DeliveryManifestID);
                if (deliveryManifest == null)
                    return new ManifestCreated() { Success = false, ErrorText = $"Manifest nebyl nalezen; DeliveryManifestID: {procModel.DeliveryManifestID}" };
                deliveryManifest.ManifestRefNumber = procModel.ManifestRefNumber;
                deliveryManifest.ManifestStatus = DeliveryManifest.STATUS_DONE;
                deliveryManifest.CreateManifestAttempt = (short)((deliveryManifest.CreateManifestAttempt ?? 0) + 1);
                deliveryManifest.CreateManifestError = null;
                deliveryManifest.ManifestChanged = DateTime.Now;

                var deliveryDirections = new DAL.DeliveryDirectionDAL().GetByManifestID(procModel.DeliveryManifestID, db);

                foreach (var deliveryDirection in deliveryDirections)
                {
                    //check
                    var direction = new DAL.DirectionDAL().Get(deliveryDirection.DirectionID);
                    if (direction == null)
                        return new ManifestCreated() { Success = false, ErrorText = $"Doklad nebyl nalezen; DirectionID: {deliveryDirection.DirectionID}" };

                    // do tests
                    if (direction.DocDirection != Direction.DOC_DIRECTION_OUT)
                        return new ManifestCreated() { Success = false, ErrorText = $"Direction nemá DocDirection DOC_DIRECTION_OUT; DirectionID: {deliveryDirection.DirectionID}" };

                    if (direction.DocStatus != Direction.DR_STATUS_DELIVERY_PRINT)
                        return new ManifestCreated() { Success = false, ErrorText = $"Direction nemá DocStatus DR_STATUS_DELIVERY_PRINT; DirectionID: {deliveryDirection.DirectionID}" };

                    // change status
                    direction.DocStatus = Direction.DR_STATUS_DELIVERY_MANIFEST;
                    directionList.Add(direction);

                    // make history
                    var history = new DirectionHistory()
                    {
                        DirectionID = deliveryDirection.DirectionID,
                        DocPosition = 0,
                        EventCode = Direction.DR_STATUS_DELIVERY_MANIFEST,
                        EntryUserID = _userID,
                        EntryDateTime = DateTime.Now
                    };
                    historyList.Add(history);
                }

                using (var tr = db.GetTransaction())
                {
                    // save directions
                    foreach (var direction in directionList)
                        db.Save(direction);

                    // save history
                    foreach (var history in historyList)
                        db.Insert(history);

                    db.Update(deliveryManifest);

                    tr.Complete();
                }


                // log
                base.Info($"ManifestCreated");

                return new ManifestCreated() { Success = true };
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using NPoco;
using S4.Entities;
using S4.ProcedureModels.Delivery;
using System.Linq;
using S4.ProcedureModels.Extensions;
using S4.Entities.XtraData;
using S4.Procedures.DBHelpers;
using S4.StoreCore.Interfaces;
using S4.Core;
using S4.DAL;

namespace S4.Procedures.Delivery
{
    public class GetDirectionsForWeighProc : ProceduresBase<GetDirectionsForWeigh>
    {
        public GetDirectionsForWeighProc(string userID) : base(userID)
        {
        }

        protected override GetDirectionsForWeigh DoIt(GetDirectionsForWeigh procModel)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var sql = new Sql();
                sql.Select("d.docInfo, p.partnerName + ' ' +  pa.addrLine_1 + ' ' + pa.addrCity + ' ' + pa.addrLine_2 as partner, x.xtraValue as deliveryType, d.directionID, x2.xtraValue as weighValue");
                sql.From("s4_direction d");
                sql.LeftJoin("s4_direction_xtra x on x.directionID = d.directionID");
                sql.LeftJoin("s4_direction_xtra x2 on x2.directionID = d.directionID AND x2.xtraCode = 'DI_WEIGHT'");
                sql.LeftJoin("s4_partnerList p on p.partnerID = d.partnerID");
                sql.LeftJoin("s4_partnerList_addresses pa on pa.partAddrID = d.partAddrID");
                sql.Where("docStatus = @0", Direction.DR_STATUS_DISP_CHECK_DONE); //Kontrola - hotovo

                if (procModel.DeliveryTypes != null && procModel.DeliveryTypes.Count > 0)
                    sql.Where($"x.xtraValue in (@0) AND x.xtraCode = 'TTYPE'", procModel.DeliveryTypes);

                if (!string.IsNullOrWhiteSpace(procModel.DocInfo))
                    sql.Where($"UPPER(d.docInfo) = @0", procModel.DocInfo.ToUpper());

                if (procModel.DirectionID > 0)
                    sql.Where($"d.directionID = @0", procModel.DirectionID);

                sql.OrderBy("d.directionID");
                                
                return new GetDirectionsForWeigh() { Success = true, Directions = db.Fetch<DirectionRow>(sql) };
            }
        }
    }
}

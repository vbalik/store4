﻿using S4.ProcedureModels.MessageBus;
using S4.ProcedureModels.Move;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Procedures.MessagesBus
{
    public class MessageBusSetStatusToNewProc : ProceduresBase<MessageBusSetStatusToNew>
    {

        public MessageBusSetStatusToNewProc(string userID) : base(userID)
        {
        }
        
        protected override MessageBusSetStatusToNew DoIt(MessageBusSetStatusToNew procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new MessageBusSetStatusToNew() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //check
                var messageBus = db.SingleOrDefault<S4.Entities.MessageBus>("where messageBusID = @0", procModel.MessageBusID);
                if (messageBus == null)
                    return new MessageBusSetStatusToNew() { Success = false, ErrorText = $"Záznam nebyl nalezen; MessageBusID: {procModel.MessageBusID}" };

                //set to new
                messageBus.MessageStatus = S4.Entities.MessageBus.NEW;
                messageBus.LastDateTime = null;
                messageBus.TryCount = 0;

                // save
                using (var tr = db.GetTransaction())
                {
                    // save 
                    db.Update(messageBus);
                    
                    // do save
                    tr.Complete();
                }

                // log
                Info($"MessageBusSetStatusToNewProc; MessageBusID: {procModel.MessageBusID}");

                // call exchange
                var exchangeJob = "";
                var runJob = false;
                if (messageBus.MessageType == Entities.MessageBus.MessageBusTypeEnum.ExportABRA)
                {
                    exchangeJob = "ExportABRA";
                    runJob = true;
                }
                else if (messageBus.MessageType == Entities.MessageBus.MessageBusTypeEnum.ExportDirection)
                {
                    exchangeJob = "ExportDirection";
                    runJob = true;
                }
                else if (messageBus.MessageType == Entities.MessageBus.MessageBusTypeEnum.SendMail)
                {
                    exchangeJob = "SendMail";
                    runJob = true;
                }

                if (runJob)
                {
                    Info($"Call Exchange job: {exchangeJob}");
                    SendMessageToExchange(exchangeJob);
                }
                
                return new MessageBusSetStatusToNew() { Success = true };
            }
        }

    }
}

﻿using System.Collections.Generic;
using System.Linq;
using NPoco;
using S4.Entities.Helpers;
using S4.Entities.Models;
using S4.ProcedureModels.StockTaking;
using S4.Procedures.StockTaking.Helper;

namespace S4.Procedures.StockTaking
{
    public class StockTakingRemoteScanPosProc : ProceduresBase<StockTakingRemoteScanPos>
    {

        public StockTakingRemoteScanPosProc(string userID) : base(userID)
        {
        }


        protected override StockTakingRemoteScanPos DoIt(StockTakingRemoteScanPos procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new StockTakingRemoteScanPos() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var stocktaking = db.SingleOrDefaultById<Entities.StockTaking>(procModel.StotakID);
                if (stocktaking == null)
                    return new StockTakingRemoteScanPos() { Success = false, ErrorText = $"StockTaking nebylo nalezeno; StotakID: {procModel.StotakID}" };

                if (stocktaking.StotakStatus != Entities.StockTaking.STOCKTAKING_STATUS_OPENED)
                    return new StockTakingRemoteScanPos() { Success = false, ErrorText = $"StockTaking nemá status STOCKTAKING_STATUS_OPENED; StotakID: {procModel.StotakID}" };


                // check if position is included in stocktaking
                var helper = new StockTakingHelper();
                if (!helper.DBCheckPosition(db, procModel.StotakID, procModel.PositionID))
                {
                    return new StockTakingRemoteScanPos()
                    {
                        BadPosition = true,
                        Success = true
                    };
                }

                // get items
                var stockTakingItems = db.Fetch<Entities.StockTakingItem>("where stotakID = @0 AND positionID = @1", procModel.StotakID, procModel.PositionID);

                var IDs = stockTakingItems.Select(_ => _.ArtPackID);

                var conditionsStr = "ArtPackID IN (@0)";
                object[] arguments = IDs.Cast<object>().ToArray();

                (Sql condition, Sql prepareSql) = StoreCore.Helpers.MakeListCondition(conditionsStr, IDs.ToList());

                if (prepareSql != null)
                {
                    db.KeepConnectionAlive = true;
                    db.Execute(prepareSql);
                }

                var sql = new Sql()
                    .Append(" SELECT ar.*, ap.ArtPackID FROM [dbo].[s4_articlelist_packings] ap")
                    .Append(" INNER JOIN [dbo].[s4_articlelist] ar ON ar.ArticleID = ap.ArticleID ")
                    .Append(condition.SQL, condition.Arguments);
                var articles = db.Query<StotakArticleModel>(sql).ToList();

                var stockTakingRemoteItems = new List<StockTakingRemoteItem>();

                foreach (var stotak in stockTakingItems)
                {
                    stockTakingRemoteItems.Add(new StockTakingRemoteItem
                    {
                        StotakItemID = stotak.StotakItemID,
                        StotakID = stotak.StotakID,
                        PositionID = stotak.PositionID,
                        ArtPackID = stotak.ArtPackID,
                        Quantity = stotak.Quantity,
                        CarrierNum = stotak.CarrierNum,
                        BatchNum = stotak.BatchNum,
                        ExpirationDate = stotak.ExpirationDate,
                        EntryDateTime = stotak.EntryDateTime,
                        EntryUserID = stotak.EntryUserID,
                        Article = articles.SingleOrDefault(_ => _.ArtPackID == stotak.ArtPackID)
                    });
                }

                return new StockTakingRemoteScanPos()
                {
                    StockTakingItems = stockTakingRemoteItems,
                    Success = true
                };
            }
        }

    }

}

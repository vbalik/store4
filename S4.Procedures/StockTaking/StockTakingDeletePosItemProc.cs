﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPoco;
using S4.Entities;
using S4.ProcedureModels;
using S4.ProcedureModels.StockTaking;
using S4.Procedures.StockTaking.Helper;

namespace S4.Procedures.StockTaking
{
    public class StockTakingDeletePosItemProc : ProceduresBase<StockTakingDeletePosItem>
    {

        public StockTakingDeletePosItemProc(string userID) : base(userID)
        {
        }


        protected override StockTakingDeletePosItem DoIt(StockTakingDeletePosItem procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new StockTakingDeletePosItem() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // load stocktaking
                var stocktaking = db.SingleOrDefaultById<Entities.StockTaking>(procModel.StotakID);
                if (stocktaking == null)
                    return new StockTakingDeletePosItem() { Success = false, ErrorText = $"StockTaking nebylo nalezeno; StotakID: {procModel.StotakID}" };

                if (stocktaking.StotakStatus != Entities.StockTaking.STOCKTAKING_STATUS_OPENED)
                    return new StockTakingDeletePosItem() { Success = false, ErrorText = $"StockTaking nemá status STOCKTAKING_STATUS_OPENED; StotakID: {procModel.StotakID}" };

                // check if position is included in stocktaking
                StockTakingHelper helper = new StockTakingHelper();
                if (!helper.DBCheckPosition(db, procModel.StotakID, procModel.PositionID))
                {
                    return new StockTakingDeletePosItem()
                    {
                        BadPosition = true,
                        Success = true
                    };
                }
                               
                // find item
                var stockTakingItem = db.SingleOrDefaultById<StockTakingItem>(procModel.StotakItemID);
                if (stockTakingItem == null)
                    return new StockTakingDeletePosItem() { Success = false, ErrorText = $"StockTakingItem nebyl nalezen; StotakItemID: {procModel.StotakItemID}" };
                
                // set new history

                using (var tr = db.GetTransaction())
                {
                    db.Delete<StockTakingItem>(procModel.StotakItemID);

                    // add history
                    var newHis = StockTakingHelper.CreateHistory(_userID, stockTakingItem.StotakID, stockTakingItem.PositionID, StockTakingHistory.STOCKTAKING_EVENT_CLEAR, stockTakingItem);
                    db.Insert(newHis);

                    // update check counter
                    helper.DBUpdateCheckCounter(db, procModel.StotakID, procModel.PositionID);

                    // do save
                    tr.Complete();
                }

                return new StockTakingDeletePosItem() { Success = true };
            }

        }

    }

}

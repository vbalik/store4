﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPoco;
using S4.Entities;
using S4.ProcedureModels;
using S4.ProcedureModels.StockTaking;
using S4.Procedures.StockTaking.Helper;

namespace S4.Procedures.StockTaking
{
    public class StockTakingPositionOKProc : ProceduresBase<StockTakingPositionOK>
    {

        public StockTakingPositionOKProc(string userID) : base(userID)
        {
        }


        protected override StockTakingPositionOK DoIt(StockTakingPositionOK procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new StockTakingPositionOK() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // load stocktaking
                var stocktaking = db.SingleOrDefaultById<Entities.StockTaking>(procModel.StotakID);
                if (stocktaking == null)
                    return new StockTakingPositionOK() { Success = false, ErrorText = $"StockTaking nebylo nalezeno; StotakID: {procModel.StotakID}" };

                if (stocktaking.StotakStatus != Entities.StockTaking.STOCKTAKING_STATUS_OPENED)
                    return new StockTakingPositionOK() { Success = false, ErrorText = $"StockTaking nemá status STOCKTAKING_STATUS_OPENED; StotakID: {procModel.StotakID}" };

                // check if position is included in stocktaking
                var helper = new StockTakingHelper();
                if (!helper.DBCheckPosition(db, procModel.StotakID, procModel.PositionID))
                {
                    return new StockTakingPositionOK()
                    {
                        BadPosition = true,
                        Success = true
                    };
                }

                // get already done items
                var stockTakingItems = db.Fetch<StockTakingItem>("where stotakID = @0 AND positionID = @1", procModel.StotakID, procModel.PositionID);

                // get snapshot
                var snapshotList = db.Fetch<Entities.StockTakingSnapshot>("where stotakID = @0 AND snapshotClass = @1 AND positionID = @2", procModel.StotakID, (byte)StockTakingSnapshot.StockTakingSnapshotClassEnum.Internal, procModel.PositionID);

                //save data
                using (var tr = db.GetTransaction())
                {
                    // remove old items
                    foreach (var item in stockTakingItems)
                        db.Delete<StockTakingItem>(item.StotakItemID);

                    // add items from snapshot
                    foreach (var snapshot in snapshotList)
                    {
                        // set new item
                        var stockTakingItem = new StockTakingItem();
                        stockTakingItem.ArtPackID = snapshot.ArtPackID;
                        stockTakingItem.BatchNum = snapshot.BatchNum;
                        stockTakingItem.CarrierNum = snapshot.CarrierNum;
                        stockTakingItem.ExpirationDate = snapshot.ExpirationDate;
                        stockTakingItem.PositionID = snapshot.PositionID;
                        stockTakingItem.Quantity = snapshot.Quantity;
                        stockTakingItem.StotakID = snapshot.StotakID;
                        stockTakingItem.EntryDateTime = DateTime.Now;
                        stockTakingItem.EntryUserID = _userID;
                        db.Insert(stockTakingItem);
                    }

                    // add new history
                    var newHis = StockTakingHelper.CreateHistory(_userID, procModel.StotakID, procModel.PositionID, StockTakingHistory.STOCKTAKING_EVENT_POSITION_IS_OK, null);
                    db.Insert(newHis);

                    // update check counter
                    helper.DBUpdateCheckCounter(db, procModel.StotakID, procModel.PositionID);                  

                    // do save
                    tr.Complete();
                }

                return new StockTakingPositionOK()
                {
                    Success = true
                };
            }
        }
    }
}

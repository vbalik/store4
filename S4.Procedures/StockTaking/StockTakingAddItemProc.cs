﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using NPoco;
using S4.Entities;
using S4.ProcedureModels;
using S4.ProcedureModels.StockTaking;
using S4.Procedures.StockTaking.Helper;

namespace S4.Procedures.StockTaking
{
    public class StockTakingAddItemProc : ProceduresBase<StockTakingAddItem>
    {

        public StockTakingAddItemProc(string userID) : base(userID)
        {
        }


        protected override StockTakingAddItem DoIt(StockTakingAddItem procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new StockTakingAddItem() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };


            PropertyInfo propInfo = typeof(StockTakingItem).GetProperty(nameof(StockTakingItem.BatchNum));
            MaxLengthAttribute attrMaxLength = propInfo.GetCustomAttributes(typeof(MaxLengthAttribute), false).FirstOrDefault() as MaxLengthAttribute;

            if (attrMaxLength != null && procModel.BatchNum?.Length > attrMaxLength.Length)
            {
                Error($"BatchNum is longer than allowed length {attrMaxLength.Length}", new ArgumentOutOfRangeException(), procModel.StotakID, procModel.ArtPackID, procModel.BatchNum);

                return new StockTakingAddItem() { 
                    Success = false, 
                    ErrorText = $"BatchNum je delší než povolená délka {attrMaxLength.Length}"
                };
            }


            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // load stocktaking
                var stocktaking = db.SingleOrDefaultById<Entities.StockTaking>(procModel.StotakID);
                if (stocktaking == null)
                    return new StockTakingAddItem() { Success = false, ErrorText = $"StockTaking nebylo nalezeno; StotakID: {procModel.StotakID}" };

                if (stocktaking.StotakStatus != Entities.StockTaking.STOCKTAKING_STATUS_OPENED)
                    return new StockTakingAddItem() { Success = false, ErrorText = $"StockTaking nemá status STOCKTAKING_STATUS_OPENED; StotakID: {procModel.StotakID}" };

                // set values
                var stockTakingItem = new StockTakingItem();
                stockTakingItem.StotakID = procModel.StotakID;
                stockTakingItem.PositionID = procModel.PositionID;
                stockTakingItem.ArtPackID = procModel.ArtPackID;
                stockTakingItem.Quantity = procModel.Quantity;
                stockTakingItem.CarrierNum = procModel.CarrierNum;
                stockTakingItem.BatchNum = procModel.BatchNum;
                stockTakingItem.ExpirationDate = procModel.ExpirationDate;
                stockTakingItem.EntryDateTime = DateTime.Now;
                stockTakingItem.EntryUserID = _userID;

                //save data
                using (var tr = db.GetTransaction())
                {
                    db.Insert(stockTakingItem);

                    // add new history
                    var newHis = StockTakingHelper.CreateHistory(_userID, stockTakingItem.StotakID, procModel.PositionID, StockTakingHistory.STOCKTAKING_EVENT_ITEM_ADDED, stockTakingItem);
                    db.Insert(newHis);

                    // do save
                    tr.Complete();
                }

                return new StockTakingAddItem() { Success = true };
            }

        }

    }

}

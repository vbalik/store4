﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPoco;
using S4.Entities;
using S4.ProcedureModels;
using S4.ProcedureModels.StockTaking;
using S4.Procedures.StockTaking.Helper;

namespace S4.Procedures.StockTaking
{
    public class StockTakingChangeItemProc : ProceduresBase<StockTakingChangeItem>
    {

        public StockTakingChangeItemProc(string userID) : base(userID)
        {
        }


        protected override StockTakingChangeItem DoIt(StockTakingChangeItem procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new StockTakingChangeItem() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // get item
                var stockTakingItem = db.FirstOrDefault<StockTakingItem>("where stotakitemid = @0", procModel.StotakItemID);
                if (stockTakingItem == null)
                    return new StockTakingChangeItem() { Success = false, ErrorText = $"StockTakingItem nebyl nalezen; StotakItemID: {procModel.StotakItemID}" };

                // load stocktaking
                var stocktaking = db.SingleOrDefaultById<Entities.StockTaking>(stockTakingItem.StotakID);
                if (stocktaking == null)
                    return new StockTakingChangeItem() { Success = false, ErrorText = $"StockTaking nebylo nalezeno; StotakID: {stockTakingItem.StotakID}" };

                if (stocktaking.StotakStatus != Entities.StockTaking.STOCKTAKING_STATUS_OPENED)
                    return new StockTakingChangeItem() { Success = false, ErrorText = $"StockTaking nemá status STOCKTAKING_STATUS_OPENED; StotakID: {stockTakingItem.StotakID}" };


                // change values
                stockTakingItem.PositionID = procModel.PositionID;
                stockTakingItem.ArtPackID = procModel.ArtPackID;
                stockTakingItem.Quantity = procModel.Quantity;
                stockTakingItem.CarrierNum = procModel.CarrierNum;
                stockTakingItem.BatchNum = procModel.BatchNum;
                stockTakingItem.ExpirationDate = procModel.ExpirationDate;

                // save data
                using (var tr = db.GetTransaction())
                {
                    db.Update(stockTakingItem);

                    // add new history
                    var newHis = StockTakingHelper.CreateHistory(_userID, stockTakingItem.StotakID, procModel.PositionID, StockTakingHistory.STOCKTAKING_EVENT_ITEM_CHANGED, stockTakingItem);
                    db.Insert(newHis);

                    // do save
                    tr.Complete();
                }

                return new StockTakingChangeItem() { Success = true };
            }

        }

    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPoco;
using S4.Entities;
using S4.ProcedureModels;
using S4.ProcedureModels.StockTaking;

namespace S4.Procedures.StockTaking
{
    public class StockTakingListOpenedProc : ProceduresBase<StockTakingListOpened>
    {

        public StockTakingListOpenedProc(string userID) : base(userID)
        {
        }


        protected override StockTakingListOpened DoIt(StockTakingListOpened procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new StockTakingListOpened() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var result = new StockTakingListOpened();
                result.StockTakingList = db.Fetch<Entities.StockTaking>("where stotakStatus = @0", Entities.StockTaking.STOCKTAKING_STATUS_OPENED);
                result.Success = true;
                return result;
            }            
        }   
    }
}

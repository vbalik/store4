﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPoco;
using S4.Entities;
using S4.ProcedureModels;
using S4.ProcedureModels.StockTaking;
using S4.Procedures.StockTaking.Helper;

namespace S4.Procedures.StockTaking
{
    public class StockTakingClearPosProc : ProceduresBase<StockTakingClearPos>
    {

        public StockTakingClearPosProc(string userID) : base(userID)
        {
        }


        protected override StockTakingClearPos DoIt(StockTakingClearPos procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new StockTakingClearPos() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // load stocktaking
                var stocktaking = db.SingleOrDefaultById<Entities.StockTaking>(procModel.StotakID);
                if (stocktaking == null)
                    return new StockTakingClearPos() { Success = false, ErrorText = $"StockTaking nebylo nalezeno; StotakID: {procModel.StotakID}" };

                if (stocktaking.StotakStatus != Entities.StockTaking.STOCKTAKING_STATUS_OPENED)
                    return new StockTakingClearPos() { Success = false, ErrorText = $"StockTaking nemá status STOCKTAKING_STATUS_OPENED; StotakID: {procModel.StotakID}" };


                // check if position is included in stocktaking
                StockTakingHelper helper = new StockTakingHelper();
                if (!helper.DBCheckPosition(db, procModel.StotakID, procModel.PositionID))
                {
                    return new StockTakingClearPos()
                    {
                        BadPosition = true,
                        Success = true
                    };
                }

                // get items
                var items = db.Fetch<StockTakingItem>("where stotakID = @0 AND positionID = @1", procModel.StotakID, procModel.PositionID);
                using (var tr = db.GetTransaction())
                {
                    foreach (var item in items)
                    {
                        db.Delete<StockTakingItem>(item.StotakItemID);

                        // add new history
                        var newHis = StockTakingHelper.CreateHistory(_userID, item.StotakID, procModel.PositionID, StockTakingHistory.STOCKTAKING_EVENT_CLEAR, item);
                        db.Insert(newHis);
                    }

                    // update check counter
                    helper.DBUpdateCheckCounter(db, procModel.StotakID, procModel.PositionID);
                    
                    // do save
                    tr.Complete();
                }
                                               
                return new StockTakingClearPos() { Success = true };
            }

        }

    }

}

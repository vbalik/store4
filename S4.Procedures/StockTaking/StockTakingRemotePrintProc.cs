﻿using System;
using System.Collections.Generic;
using S4.Core;
using S4.Entities;
using S4.Entities.Helpers;
using S4.ProcedureModels.StockTaking;
using S4.Procedures.StockTaking.Helper;
using S4.Report;
using static S4.Entities.StockTakingSnapshot;

namespace S4.Procedures.StockTaking
{
    public class StockTakingRemotePrintProc : ProceduresBase<StockTakingRemotePrint>
    {
        private Reports _report = null;
        private SettingMail _setting = null;

        private static string REPORT_TITLE = "Vzdálená inventura pozice";
        private static string MAIL_SUBJECT = "Vzdálená inventura pozice";

        public StockTakingRemotePrintProc(string userID) : base(userID)
        {
        }


        protected override StockTakingRemotePrint DoIt(StockTakingRemotePrint procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new StockTakingRemotePrint() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            // get report id
            var reportID = Singleton<DAL.SettingsSet>.Instance.GetValue_Int32(SettingsDictionary.STOTAK_REMOTE_REPORT_ID);

            // get settings for email send
            _setting = StockTakingHelper.GetSettingMail();

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //report from db
                _report = db.SingleOrDefaultById<Reports>(reportID);

                if (_report == null)
                    throw new Exception($"Report ID: {reportID} is not found");

                //variables
                var variables = new List<Tuple<string, object, Type>>();
                var desc = procModel.PosDesc == null || string.IsNullOrWhiteSpace(procModel.PosDesc) ? "" : $"({procModel.PosDesc})";
                variables.Add(new Tuple<string, object, Type>("Header", $"{REPORT_TITLE} {procModel.PosCode} {desc}", typeof(string)));

                var rowsCount = 0;
                var parameters = new List<Tuple<string, object>>();
                parameters.Add(new Tuple<string, object>("@stotakID", procModel.StotakID));
                parameters.Add(new Tuple<string, object>("@snapshotClass", (byte)StockTakingSnapshotClassEnum.Internal));

                Print print = new Print();
                print.Variables = variables;
                print.ReportDefinition = _report.Template;
                //print.DesignMode = true; //Design
                //print.ReportDefFilePath = @"c:\Projects\store4\Tools\Reporty\VzdalenaInventura.srt"; //Design
                //print.DoPrint("Adobe PDF", _report.Settings.SQL, parameters); //Design

                var pdfDocument = print.DoPrint2PDF(_report.Settings.SQL, _report.Settings.ReportName, parameters, out rowsCount);

                if (print.IsError)
                    throw new Exception(print.ErrorText);

                //send mail
                var subject = $"{MAIL_SUBJECT} {procModel.PosCode}";
                var body = string.Empty;
                SaveMail(pdfDocument, subject, body, procModel.Email);

                return new StockTakingRemotePrint { Success = true };
            }
        }

        private void SaveMail(byte[] pdfDocument, string subject, string body, string mailTo)
        {
            if (string.IsNullOrWhiteSpace(mailTo))
            {
                base.Info($"Mail not save, mailTo is empty");
                return;
            }

            var documentName = $"{_report.Settings.ReportName.Replace(" ", "_")}.pdf";

            var sendEmailInfo = new SendEmailInfo(
                    _setting.MailFrom,
                    mailTo,
                    subject,
                    body,
                    new string[]
                    {
                        documentName
                    });

            // save messageBus
            var messageBusDAL = new DAL.MessageBusDAL();
            var messageBusID = messageBusDAL.InsertMessageBusDocumentStatusNew(sendEmailInfo, new List<byte[]> { pdfDocument });

            base.Info($"Mail save. MessageBusID {messageBusID} Document length: {pdfDocument.Length} b");
        }

               
    }
}

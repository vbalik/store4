﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPoco;
using S4.Entities;
using S4.ProcedureModels;
using S4.ProcedureModels.StockTaking;
using S4.Procedures.Messages;
using S4.Procedures.StockTaking.Helper;

namespace S4.Procedures.StockTaking
{
    [RunAsync]
    public class StockTakingBeginProc : ProceduresBase<StockTakingBegin>
    {

        public StockTakingBeginProc(string userID) : base(userID)
        {
        }


        protected override StockTakingBegin DoIt(StockTakingBegin procModel)
        {
            var header = $"Založení inventury";
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
            {
                var errorTXT = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}";
                SendMessageToClient($"{header} skončilo s chybou", $"Chyba: {errorTXT}", color: NotifyType.Danger);
                return new StockTakingBegin() { Success = false, ErrorText = errorTXT };
            }
                
            // create stocktaking
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                header = $"{header} '{procModel.StotakDesc}'";
                var helper = new StockTakingHelper();
                var result = helper.BeginStockTaking(db, _userID, procModel.PositionIDs, procModel.StotakDesc);
                if (result.result)
                {
                    using (var tr = db.GetTransaction())
                    {
                        // save stocktaking
                        db.Insert(result.stockTaking);
                        SendMessageToClient(header, $"Probíhá založení inventury", 10);

                        // save positions
                        SendMessageToClient(header, $"Probíhá ukládání pozic", 20);
                        foreach (var stcTakingPos in result.stockTaking.StockTakingPositions)
                        {
                            stcTakingPos.StotakID = result.stockTaking.StotakID;
                            db.Save(stcTakingPos);
                        }
                        SendMessageToClient(header, $"Pozice uloženy", 30);

                        // make snapshot
                        SendMessageToClient(header, "Probíhá ukládání stavu před inventurou", 40);
                        helper.MakeSnapshot(db, result.stockTaking.StotakID, result.stockTaking.StockTakingPositions);
                        SendMessageToClient(header, "Stav před inventurou uložen", 50);

                        // lock positions
                        SendMessageToClient(header, "Probíhá zamykání pozic", 60);
                        helper.SetPositionsStatus(db, result.stockTaking.StotakID, Position.POS_STATUS_STOCKTAKING);
                        SendMessageToClient(header, "Pozice uzamčeny", 70);

                        // do save
                        tr.Complete();
                        SendMessageToClient(header, $"Inventura zahájena", 100, NotifyType.Success );
                    }

                    return new StockTakingBegin() { Success = true, StotakID = result.stockTaking.StotakID };
                }
                else
                {
                    SendMessageToClient($"{header} skončilo s chybou", $"Chyba: {result.problemDesc}", color: NotifyType.Danger);
                    return new StockTakingBegin() { Success = false, StotakID = -1, ErrorText = result.problemDesc };
                }
            }
        }


    }
}

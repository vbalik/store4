﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPoco;
using S4.Core.Utils;
using S4.Entities;
using S4.ProcedureModels;
using S4.ProcedureModels.StockTaking;
using S4.Procedures.Messages;
using S4.Procedures.StockTaking.Helper;

namespace S4.Procedures.StockTaking
{
    [RunAsync]
    public class StockTakingItemsFromSnapshotProc : ProceduresBase<StockTakingItemsFromSnapshot>
    {

        public StockTakingItemsFromSnapshotProc(string userID) : base(userID)
        {
        }


        protected override StockTakingItemsFromSnapshot DoIt(StockTakingItemsFromSnapshot procModel)
        {
            var header = $"Načtení původního stavu";
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
            {
                var errorTXT = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}";
                SendMessageToClient($"{header} skončilo s chybou", $"Chyba: {errorTXT}", color: NotifyType.Danger);
                return new StockTakingItemsFromSnapshot() { Success = false, ErrorText = errorTXT };
            }

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //check user right
                var canStoReadOrigState = db.SingleOrDefaultById<Worker>(base._userID).WorkerClass.HasFlag(Entities.Worker.WorkerClassEnum.CanStoReadOrigState); 

                if(!canStoReadOrigState)
                {
                    var errorTXT = "Nemáte právo na provedení akce";
                    SendMessageToClient($"{header} skončilo s chybou", $"Chyba: {errorTXT}", color: NotifyType.Danger);
                    return new StockTakingItemsFromSnapshot() { Success = false, ErrorText = errorTXT };
                }

                // load stocktaking
                var stocktaking = db.SingleOrDefaultById<Entities.StockTaking>(procModel.StotakID);
                if (stocktaking == null)
                {
                    var errorTXT = $"StockTaking nebylo nalezeno; StotakID: {procModel.StotakID}";
                    SendMessageToClient($"{header} skončilo s chybou", $"Chyba: {errorTXT}", color: NotifyType.Danger);
                    return new StockTakingItemsFromSnapshot() { Success = false, ErrorText = errorTXT };
                }

                header = $"{header} '{stocktaking.StotakDesc}'";

                if (stocktaking.StotakStatus != Entities.StockTaking.STOCKTAKING_STATUS_OPENED)
                {
                    var errorTXT = $"StockTaking nemá status STOCKTAKING_STATUS_OPENED; StotakID: {procModel.StotakID}";
                    SendMessageToClient($"{header} skončilo s chybou", $"Chyba: {errorTXT}", color: NotifyType.Danger);
                    return new StockTakingItemsFromSnapshot() { Success = false, ErrorText = errorTXT };
                }

                // get snapshot
                SendMessageToClient(header, "Probíhá načítání stavu před inventurou", 40);
                var snapshotList = db.Fetch<Entities.StockTakingSnapshot>("where stotakID = @0 and snapshotClass = @1", procModel.StotakID, (int)StockTakingSnapshot.StockTakingSnapshotClassEnum.Internal);

                // get items to delete
                SendMessageToClient(header, "Probíhá načítání položek které budou smazány", 50);
                var stockTakingItems = db.Fetch<StockTakingItem>("where stotakID = @0", procModel.StotakID);

                // get positions list
                SendMessageToClient(header, "Probíhá načítání pozic", 60);
                var stockTakingPositions = db.Fetch<StockTakingPosition>("where stotakID = @0", procModel.StotakID);

                //save data
                var now = DateTime.Now;
                var progressUtils = new ProgressUtils();
                using (var tr = db.GetTransaction())
                {
                    // delete items
                    SendMessageToClient($"{header}", $"Probíhá mazání položek", 70);
                    var processed = 1;
                    var stepNum = 10;
                    foreach (var item in stockTakingItems)
                    {
                        var step = progressUtils.GetProgressValue(processed, stockTakingItems.Count, stepNum);
                        if (step > 0)
                            SendMessageToClient($"{header}", $"Probíhá mazání položek {processed}/{stockTakingItems.Count} ({step}%)", step);

                        db.Delete<StockTakingItem>(item.StotakItemID);
                        processed++;
                    }
                        
                    // add items from snapshot
                    SendMessageToClient($"{header}", $"Probíhá vkládání položek", 80);
                    progressUtils.ResetLastStep();
                    processed = 1;
                    foreach (var snapshot in snapshotList)
                    {
                        var step = progressUtils.GetProgressValue(processed, snapshotList.Count, stepNum);
                        if (step > 0)
                            SendMessageToClient($"{header}", $"Probíhá vkládání položek {processed}/{snapshotList.Count} ({step}%)", step);

                        // set new item
                        var stockTakingItem = new StockTakingItem
                        {
                            ArtPackID = snapshot.ArtPackID,
                            BatchNum = snapshot.BatchNum,
                            CarrierNum = snapshot.CarrierNum,
                            ExpirationDate = snapshot.ExpirationDate,
                            PositionID = snapshot.PositionID,
                            Quantity = snapshot.Quantity,
                            StotakID = snapshot.StotakID,
                            EntryDateTime = now,
                            EntryUserID = _userID
                        };
                        db.Insert(stockTakingItem);
                        processed++;
                    }

                    // add new history
                    var newHis = StockTakingHelper.CreateHistory(_userID, procModel.StotakID, null, StockTakingHistory.STOCKTAKING_EVENT_TAKEN, null);
                    db.Insert(newHis);

                    // update positions
                    SendMessageToClient($"{header}", $"Probíhá změna pozic", 90);
                    progressUtils.ResetLastStep();
                    processed = 1;
                    foreach (var pos in stockTakingPositions)
                    {
                        var step = progressUtils.GetProgressValue(processed, snapshotList.Count, stepNum);
                        if (step > 0)
                            SendMessageToClient($"{header}", $"Probíhá změna pozic {processed}/{stockTakingPositions.Count} ({step}%)", step);

                        pos.CheckCounter++;
                        db.Update(pos);
                        processed++;
                    }

                    // do save
                    tr.Complete();
                }

                SendMessageToClient(header, $"Původní stav načten", 100, NotifyType.Success);
                return new StockTakingItemsFromSnapshot() { Success = true };
            }

        }
             

    }

}

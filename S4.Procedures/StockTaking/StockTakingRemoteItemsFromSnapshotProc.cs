﻿using S4.Entities;
using S4.ProcedureModels.StockTaking;

namespace S4.Procedures.StockTaking
{
    public class StockTakingRemoteItemsFromSnapshotProc : ProceduresBase<StockTakingRemoteItemsFromSnapshot>
    {

        public StockTakingRemoteItemsFromSnapshotProc(string userID) : base(userID)
        {
        }


        protected override StockTakingRemoteItemsFromSnapshot DoIt(StockTakingRemoteItemsFromSnapshot procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new StockTakingRemoteItemsFromSnapshot() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var items = db.Query<StockTakingSnapshot>()
                    .Where(_ => _.StotakID == procModel.StotakID)
                    .Where(_ => _.ArtPackID == procModel.ArtPackID)
                    .Where(_ => _.SnapshotClass == StockTakingSnapshot.StockTakingSnapshotClassEnum.Internal)
                    .ToList();

                return new StockTakingRemoteItemsFromSnapshot()
                {
                    Items = items,
                    Success = true
                };
            }

        }

    }

}

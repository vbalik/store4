﻿using NPoco;
using S4.Core;
using S4.Entities;
using S4.Entities.Helpers;
using S4.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;


namespace S4.Procedures.StockTaking.Helper
{
    public class StockTakingHelper
    {
        public (bool result, StockTakingModel stockTaking, string problemDesc) BeginStockTaking(IDatabase db, string userID, int[] positionIDs, string stotakDesc)
        {
            // make stocktaking
            var stockTaking = new StockTakingModel()
            {
                StotakStatus = Entities.StockTaking.STOCKTAKING_STATUS_OPENED,
                StotakDesc = stotakDesc,
                BeginDate = DateTime.Now,
                EntryUserID = userID,
                EntryDateTime = DateTime.Now,
            };


            // check and add positions
            foreach (int posID in positionIDs)
            {
                var position = db.Query<Position>().Single(p => p.PositionID == posID);

                // check status
                if (position.PosStatus == Position.POS_STATUS_STOCKTAKING)
                {
                    return (false, null, $"Na pozici '{position.PosCode}' probíhá inventura.");
                }

                // check not saved moves
                if (CheckNotSaved(position))
                {
                    return (false, null, $"Na pozici '{position.PosCode}' jsou neuložené pohyby.");
                }

                // add to list
                stockTaking.StockTakingPositions.Add(new StockTakingPosition()
                {
                    PositionID = position.PositionID,
                    StotakID = stockTaking.StotakID
                });

            }

            return (true, stockTaking, null);
        }


        public void MakeSnapshot(Database db, int stotakID, List<StockTakingPosition> stcTakingPosList)
        {
            var onStore = Singleton<ServicesFactory>.Instance.GetOnStore();

            var posIDs = stcTakingPosList.Select(p => p.PositionID).ToList();
            var onStoreItems = onStore.OnPosition(posIDs, StoreCore.Interfaces.ResultValidityEnum.Valid);

            var toSave = onStoreItems.Select(o => new StockTakingSnapshot()
            {
                ArtPackID = o.ArtPackID,
                BatchNum = o.BatchNum,
                CarrierNum = o.CarrierNum,
                ExpirationDate = o.ExpirationDate,
                PositionID = o.PositionID,
                Quantity = o.Quantity,
                SnapshotClass = StockTakingSnapshot.StockTakingSnapshotClassEnum.Internal,
                StotakID = stotakID
            });

            db.InsertBatch(toSave, new BatchOptions() { BatchSize = 16 });
        }



        public void SetPositionsStatus(Database db, int stotakID, string status)
        {
            var stockTakingPositions = db.Fetch<Entities.StockTakingPosition>("where stotakID = @0", stotakID);

            foreach (var pos in stockTakingPositions)
            {
                SetPositionStatus(db, pos.PositionID, status);
            }
        }

        public void SetPositionStatus(Database db, int positionID, string status)
        {
            var position = db.SingleById<Entities.Position>(positionID);
            position.PosStatus = status;
            db.Save(position);
        }


        public bool DBCheckPosition(Database db, int stotakID, int positionID)
        {
            var sql = new Sql();
            sql.Select("COUNT(*)");
            sql.From("s4_stocktaking_positions");
            sql.Where("stotakID = @0 AND positionID = @1", stotakID, positionID);
            var count = db.Fetch<int>(sql).First();
            return ((int)count != 0);
        }


        public void DBUpdateCheckCounter(Database db, int stotakID, int positionID)
        {
            var posList = db.Fetch<StockTakingPosition>("WHERE stotakID = @0 AND positionID = @1", stotakID, positionID);

            foreach (var item in posList)
            {
                item.CheckCounter++;
                db.Update(item);
            }

        }


        private bool CheckNotSaved(Position pos)
        {
            var onStore = Singleton<ServicesFactory>.Instance.GetOnStore();
            var posList = new List<int>
            {
                pos.PositionID
            };
            var result = onStore.CheckNotValidMoves(posList);

            return result.Count > 0 ? true : false;
        }




        public static StockTakingHistory CreateHistory(string userID, int stotakID, int? positionID, string eventCode, StockTakingItem stockTakingItem)
        {
            var newHistory = new StockTakingHistory()
            {
                EntryDateTime = DateTime.Now,
                EntryUserID = userID,
                EventCode = eventCode,
                PositionID = positionID,
                StotakID = stotakID
            };

            if (stockTakingItem != null)
            {
                newHistory.ArtPackID = stockTakingItem.ArtPackID;
                newHistory.CarrierNum = stockTakingItem.CarrierNum;
                newHistory.Quantity = stockTakingItem.Quantity;
                newHistory.BatchNum = stockTakingItem.BatchNum;
                newHistory.ExpirationDate = stockTakingItem.ExpirationDate;
            }

            return newHistory;
        }

        public static SettingMail GetSettingMail()
        {
            //read setting
            var mailSettingJson = Singleton<DAL.SettingsSet>.Instance.GetValue_String(Entities.SettingsDictionary.MAIL_SETTING);

            if (string.IsNullOrWhiteSpace(mailSettingJson))
                throw new Exception($"Entities.SettingsDictionary.MAIL_SETTING is empty");

            return Newtonsoft.Json.JsonConvert.DeserializeObject<SettingMail>(mailSettingJson);
        }
    }
}

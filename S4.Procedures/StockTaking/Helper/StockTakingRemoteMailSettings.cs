﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Procedures.StockTaking.Helper
{
    public class StockTakingRemoteMailSettings
    {
        public string MailFrom { get; set; }
        public string MailSubject { get; set; }
        public string MailBody { get; set; }

        public string MailHeslo { get; set; }
        public string MailServer { get; set; }
        public string MailUzivatel { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPoco;
using S4.Entities;
using S4.ProcedureModels;
using S4.ProcedureModels.StockTaking;
using S4.Procedures.StockTaking.Helper;

namespace S4.Procedures.StockTaking
{
    public class StockTakingAddPosItemProc : ProceduresBase<StockTakingAddPosItem>
    {

        public StockTakingAddPosItemProc(string userID) : base(userID)
        {
        }


        protected override StockTakingAddPosItem DoIt(StockTakingAddPosItem procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new StockTakingAddPosItem() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // load stocktaking
                var stocktaking = db.SingleOrDefaultById<Entities.StockTaking>(procModel.StotakID);
                if (stocktaking == null)
                    return new StockTakingAddPosItem() { Success = false, ErrorText = $"StockTaking nebylo nalezeno; StotakID: {procModel.StotakID}" };

                if (stocktaking.StotakStatus != Entities.StockTaking.STOCKTAKING_STATUS_OPENED)
                    return new StockTakingAddPosItem() { Success = false, ErrorText = $"StockTaking nemá status STOCKTAKING_STATUS_OPENED; StotakID: {procModel.StotakID}" };

                // check if position is included in stocktaking
                var helper = new StockTakingHelper();
                if (!helper.DBCheckPosition(db, procModel.StotakID, procModel.PositionID))
                {
                    return new StockTakingAddPosItem()
                    {
                        BadPosition = true,
                        Success = true
                    };
                }

                // add new item
                var newItem = new StockTakingItem();
                newItem.StotakID = procModel.StotakID;
                newItem.PositionID = procModel.PositionID;
                newItem.ArtPackID = procModel.ArtPackID;
                newItem.CarrierNum = procModel.CarrierNum;
                newItem.Quantity = procModel.Quantity;
                newItem.BatchNum = procModel.BatchNum;
                newItem.ExpirationDate = procModel.ExpirationDate;
                newItem.EntryUserID = _userID;
                newItem.EntryDateTime = DateTime.Now;


                // save
                int newItemID = 0;
                using (var tr = db.GetTransaction())
                {
                    db.Insert(newItem);
                    newItemID = newItem.StotakID;

                    // add new history
                    var newHis = StockTakingHelper.CreateHistory(_userID, newItem.StotakID, procModel.PositionID, StockTakingHistory.STOCKTAKING_EVENT_SET, newItem);
                    db.Insert(newHis);

                    // update check counter
                    helper.DBUpdateCheckCounter(db, procModel.StotakID, procModel.PositionID);

                    // do save
                    tr.Complete();
                }

                return new StockTakingAddPosItem()
                {
                    Success = true,
                    StotakItemID = newItemID
                };

            }

        }

    }
}

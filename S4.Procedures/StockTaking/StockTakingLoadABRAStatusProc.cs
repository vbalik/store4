﻿using S4.Core;
using S4.Core.Utils;
using S4.Entities;
using S4.ProcedureModels.StockTaking;
using S4.Procedures.Messages;
using S4.Procedures.StockTaking.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Procedures.StockTaking
{
    [RunAsync]
    public class StockTakingLoadABRAStatusProc : ProceduresBase<StockTakingLoadABRAStatus>
    {
        public StockTakingLoadABRAStatusProc(string userID) : base(userID)
        {
        }


        protected override StockTakingLoadABRAStatus DoIt(StockTakingLoadABRAStatus procModel)
        {
            var header = $"Načtení stavu z ABRY";
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
            {
                var errorTXT = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}";
                SendMessageToClient($"{header} skončilo s chybou", $"Chyba: {errorTXT}", color: NotifyType.Danger);
                return new StockTakingLoadABRAStatus() { Success = false, ErrorText = errorTXT };
            }

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                //load manuf
                S4.Entities.Manufact manufact = new Manufact { Source_id = "" };
                if (!string.IsNullOrWhiteSpace(procModel.ManufID))
                    manufact = Singleton<DAL.Cache.ManufactCache>.Instance.GetItem(procModel.ManufID);

                // load stocktaking
                var stocktaking = db.SingleOrDefaultById<Entities.StockTaking>(procModel.StotakID);
                if (stocktaking == null)
                {
                    var errorTXT = $"StockTaking nebylo nalezeno; StotakID: {procModel.StotakID}";
                    SendMessageToClient($"{header} skončilo s chybou", $"Chyba: {errorTXT}", color: NotifyType.Danger);
                    return new StockTakingLoadABRAStatus() { Success = false, ErrorText = errorTXT };
                }

                header = $"{header} '{stocktaking.StotakDesc}'";

                if (stocktaking.StotakStatus != Entities.StockTaking.STOCKTAKING_STATUS_OPENED)
                {
                    var errorTXT = $"StockTaking nemá status STOCKTAKING_STATUS_OPENED; StotakID: {procModel.StotakID}";
                    SendMessageToClient($"{header} skončilo s chybou", $"Chyba: {errorTXT}", color: NotifyType.Danger);
                    return new StockTakingLoadABRAStatus() { Success = false, ErrorText = errorTXT };
                }

                // get snapshot
                SendMessageToClient(header, "Probíhá načítání stavu před inventurou", 40);
                var oldSnapshotItems = db.Fetch<Entities.StockTakingSnapshot>("where stotakID = @0 and snapshotClass = @1", procModel.StotakID, (int)StockTakingSnapshot.StockTakingSnapshotClassEnum.Outer);

                SendMessageToClient(header, "Probíhá ukládání stavu před inventurou", 50);
                var abraStocktaking = new S4.Abra.Stocktaking();
                var snapshotItems = abraStocktaking.MakeSnapshotItems(procModel.AbraStoreID, procModel.StotakID, manufact.Source_id, out string errorList);

                //save data
                var now = DateTime.Now;
                var progressUtils = new ProgressUtils();
                using (var tr = db.GetTransaction())
                {
                    // delete items
                    SendMessageToClient($"{header}", $"Probíhá mazání stavu inventury", 60);
                    var processed = 1;
                    foreach (var item in oldSnapshotItems)
                    {
                        var step = progressUtils.GetProgressValue(processed, oldSnapshotItems.Count, 10);
                        if (step > 0)
                            SendMessageToClient($"{header}", $"Probíhá mazání stavu inventury {processed}/{oldSnapshotItems.Count} ({step}%)", step);
                        
                        db.Delete<StockTakingSnapshot>(item.StotakSnapID);
                        processed++;
                    }

                    SendMessageToClient(header, "Probíhá ukládání stavu inventury", 70);
                    // save new set
                    db.InsertBatch(snapshotItems, new NPoco.BatchOptions() { BatchSize = 100 });

                    // add new history
                    var newHist = StockTakingHelper.CreateHistory(_userID, procModel.StotakID, null, StockTakingHistory.STOCKTAKING_EVENT_EXTERNAL_READ, null);
                    db.Insert(newHist);

                    // do save
                    tr.Complete();
                }

                var isError = !string.IsNullOrEmpty(errorList);
                var error = string.IsNullOrEmpty(errorList) ? "OK" : $"Chyby: {errorList}";
                SendMessageToClient(header, $"Stav z ABRY načten - Stav: {error}", 100, isError ? NotifyType.Danger : NotifyType.Success);
                return new StockTakingLoadABRAStatus() { Success = true, LoadErrors = errorList };
            }

        }

    }
}

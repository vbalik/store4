﻿using System;
using System.Collections.Generic;
using System.Text;
using NPoco;
using S4.Entities;
using S4.ProcedureModels;
using System.Linq;
using S4.Procedures.StockTaking.Helper;
using S4.ProcedureModels.StockTaking;
using S4.StoreCore.Interfaces;
using S4.StoreCore.OnStore_Basic;
using S4.Procedures.DBHelpers;
using S4.Core;

namespace S4.Procedures.StockTaking
{
    public class StockTakingBeginArticleProc : ProceduresBase<StockTakingBeginArticle>
    {

        public StockTakingBeginArticleProc(string userID) : base(userID)
        {
        }


        protected override StockTakingBeginArticle DoIt(StockTakingBeginArticle procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new StockTakingBeginArticle() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };


            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // create list of positions
                var onStore = Singleton<ServicesFactory>.Instance.GetOnStore();
                var results = onStore.ByArticle(procModel.ArticleID, ResultValidityEnum.Valid);

                var articleOnPositions = results.GroupBy(k => k.PositionID, (k, i) => i.First().Position).ToList();
                int[] positionIDs = articleOnPositions.Select(p => p.PositionID).ToArray();

                // make desc
                var article = db.SingleOrDefaultById<Article>(procModel.ArticleID);

                if (article == null)
                    return new StockTakingBeginArticle() { Success = false, ErrorText = $"Article nebylo nalezeno; ArticleID: {procModel.ArticleID}" };

                string stotakDesc = $"Inventura karty '{article.ArticleCode} : {article.ArticleDesc}'";

                // create stocktaking                
                var helper = new StockTakingHelper();
                var result = helper.BeginStockTaking(db, _userID, positionIDs, stotakDesc);
                if (result.result)
                {
                    using (var tr = db.GetTransaction())
                    {
                        // save stocktaking
                        db.Insert(result.stockTaking);

                        // save positions
                        foreach (var stcTakingPos in result.stockTaking.StockTakingPositions)
                        {
                            stcTakingPos.StotakID = result.stockTaking.StotakID;
                            db.Save(stcTakingPos);
                        }

                        // make snapshot
                        helper.MakeSnapshot(db, result.stockTaking.StotakID, result.stockTaking.StockTakingPositions);

                        // lock positions
                        helper.SetPositionsStatus(db, result.stockTaking.StotakID, Position.POS_STATUS_STOCKTAKING);

                        // do save
                        tr.Complete();
                    }

                    return new StockTakingBeginArticle() { Success = true, StotakID = result.stockTaking.StotakID };
                }
                else
                {
                    return new StockTakingBeginArticle() { Success = false, StotakID = -1, ErrorText = result.problemDesc };
                }
            }
        }
    }
}

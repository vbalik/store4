﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPoco;
using S4.Entities;
using S4.ProcedureModels;
using S4.ProcedureModels.StockTaking;
using S4.Procedures.StockTaking.Helper;

namespace S4.Procedures.StockTaking
{
    public class StockTakingDeleteItemProc : ProceduresBase<StockTakingDeleteItem>
    {

        public StockTakingDeleteItemProc(string userID) : base(userID)
        {
        }


        protected override StockTakingDeleteItem DoIt(StockTakingDeleteItem procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new StockTakingDeleteItem() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // get item
                var stockTakingItem = db.FirstOrDefault<StockTakingItem>("where stotakitemid = @0", procModel.StotakItemID);
                if (stockTakingItem == null)
                    return new StockTakingDeleteItem() { Success = false, ErrorText = $"StockTakingItem nebyl nalezen; StotakItemID: {procModel.StotakItemID}" };

                // load stocktaking
                var stocktaking = db.SingleOrDefaultById<Entities.StockTaking>(stockTakingItem.StotakID);
                if (stocktaking == null)
                    return new StockTakingDeleteItem() { Success = false, ErrorText = $"StockTaking nebylo nalezeno; StotakID: {stockTakingItem.StotakID}" };

                if (stocktaking.StotakStatus != Entities.StockTaking.STOCKTAKING_STATUS_OPENED)
                    return new StockTakingDeleteItem() { Success = false, ErrorText = $"StockTaking nemá status STOCKTAKING_STATUS_OPENED; StotakID: {stockTakingItem.StotakID}" };
                               
                // save data
                using (var tr = db.GetTransaction())
                {
                    db.Delete(stockTakingItem);

                    var newHis = StockTakingHelper.CreateHistory(_userID, stockTakingItem.StotakID, stockTakingItem.PositionID, StockTakingHistory.STOCKTAKING_EVENT_ITEM_DELETED, stockTakingItem);
                    db.Insert(newHis);

                    // do save
                    tr.Complete();
                }

                return new StockTakingDeleteItem() { Success = true };
            }

        }

    }

}

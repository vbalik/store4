﻿using System.Collections.Generic;
using System.Linq;
using NPoco;
using S4.Core;
using S4.Entities.Views;
using S4.ProcedureModels.StockTaking;

namespace S4.Procedures.StockTaking
{
    public class StockTakingRemoteListOpenedProc : ProceduresBase<StockTakingRemoteListOpened>
    {

        public StockTakingRemoteListOpenedProc(string userID) : base(userID)
        {
        }


        protected override StockTakingRemoteListOpened DoIt(StockTakingRemoteListOpened procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new StockTakingRemoteListOpened() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var result = new StockTakingRemoteListOpened();
                result.StockTakingList = _CreateStockTakingList(db);
                result.Success = true;
                return result;
            }
        }

        private List<StockTakingRemotePos> _CreateStockTakingList(Database db)
        {
            var models = new List<StockTakingRemotePos>();
            var stockTakingList = db.Fetch<Entities.StockTaking>("where stotakStatus = @0 and entryUserID = @1", Entities.StockTaking.STOCKTAKING_STATUS_OPENED, _userID);

            if (stockTakingList.Count() > 2100)
                throw new System.OverflowException($"Too many params for sql IN clause.");

            var stotakPosistions = db.Fetch<StockTakingPositionView>("where stotakID IN (@0)", stockTakingList.Select(v => v.StotakID).ToArray());

            foreach (var item in stockTakingList)
            {
                // find positions for stotakID
                var positions = stotakPosistions.Where(_ => _.StotakID == item.StotakID)
                    .Select(v => Singleton<DAL.Cache.PositionCache>.Instance.GetItem(v.PositionID))
                    .ToList();

                models.Add(new StockTakingRemotePos
                {
                    StotakID = item.StotakID,
                    StotakStatus = item.StotakStatus,
                    StotakDesc = item.StotakDesc,
                    BeginDate = item.BeginDate,
                    EndDate = item.EntryDateTime,
                    EntryDateTime = item.EntryDateTime,
                    EntryUserID = item.EntryUserID,
                    Positions = positions
                });
            }

            return models;
        }
    }
}

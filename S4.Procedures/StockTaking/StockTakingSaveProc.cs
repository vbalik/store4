﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPoco;
using S4.Core;
using S4.Core.Utils;
using S4.Entities;
using S4.ProcedureModels;
using S4.ProcedureModels.StockTaking;
using S4.Procedures.Messages;
using S4.Procedures.StockTaking.Helper;
using S4.StoreCore.Interfaces;

namespace S4.Procedures.StockTaking
{
    [RunAsync]
    public class StockTakingSaveProc : ProceduresBase<StockTakingSave>
    {

        public StockTakingSaveProc(string userID) : base(userID)
        {
        }


        protected override StockTakingSave DoIt(StockTakingSave procModel)
        {
            var header = $"Uložení inventury";
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
            {
                var errorTXT = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}";
                SendMessageToClient($"{header} skončilo s chybou", $"Chyba: {errorTXT}", color: NotifyType.Danger);
                return new StockTakingSave() { Success = false, ErrorText = $"Parametry procedury nejsou platné {errorTXT}" };
            }
            
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // load stocktaking
                var stocktaking = db.SingleOrDefaultById<Entities.StockTaking>(procModel.StotakID);
                if (stocktaking == null)
                {
                    var errorTXT = $"StockTaking nebylo nalezeno; StotakID: {procModel.StotakID}";
                    SendMessageToClient($"{header} skončilo s chybou", $"Chyba: {errorTXT}", color: NotifyType.Danger);
                    return new StockTakingSave() { Success = false, ErrorText = errorTXT };
                }
                    
                if (stocktaking.StotakStatus != Entities.StockTaking.STOCKTAKING_STATUS_OPENED)
                {
                    var errorTXT = $"StockTaking nemá status STOCKTAKING_STATUS_OPENED; StotakID: {procModel.StotakID}";
                    SendMessageToClient($"{header} skončilo s chybou", $"Chyba: {errorTXT}", color: NotifyType.Danger);
                    return new StockTakingSave() { Success = false, ErrorText = errorTXT };
                }
                    
                header = $"{header} '{stocktaking.StotakDesc}'";
                var sql = new NPoco.Sql();
                sql.Where("stotakid = @0", procModel.StotakID);
                var positions = db.Fetch<Entities.StockTakingPosition>(sql);
                var items = db.Fetch<Entities.StockTakingItem>(sql);

                //save data
                var progressUtils = new ProgressUtils();
                using (var tr = db.GetTransaction())
                {
                    var save = Singleton<ServicesFactory>.Instance.GetSave();
                    // for each position in stocktaking
                    var processed = 1;
                    SendMessageToClient($"{header}", $"Probíhá ukládání pozic", 50);
                    foreach (var stPos in positions)
                    {
                        var step = progressUtils.GetProgressValue(processed, positions.Count, 1);
                        if (step > 0)
                            SendMessageToClient($"{header}", $"Probíhá ukládání pozic {processed}/{positions.Count} ({step}%)", step);

                        var stItemsOnPosition = items.Where(i => i.PositionID == stPos.PositionID);

                        // reset position to new status
                        var saveItemList = stItemsOnPosition.Select(posItem => new SaveItem()
                        {
                            ArtPackID = posItem.ArtPackID,
                            BatchNum = posItem.BatchNum,
                            CarrierNum = posItem.CarrierNum,
                            ExpirationDate = posItem.ExpirationDate,
                            Quantity = posItem.Quantity
                        }).ToList();

                        if (save.ResetPosition(db, _userID, stPos.PositionID, StoreMove.MO_PREFIX_STOCKTAKING, saveItemList, null, out string msg) == -1)
                        {
                            SendMessageToClient($"{header} skončilo s chybou", $"Chyba: {msg}", color: NotifyType.Danger);
                            return new StockTakingSave() { ErrorText = msg };
                        }
                        processed++;
                    }
                    
                    // add new history
                    var newHis = StockTakingHelper.CreateHistory(_userID, procModel.StotakID, null, StockTakingHistory.STOCKTAKING_EVENT_SAVED, null);
                    db.Insert(newHis);

                    // change status
                    stocktaking.StotakStatus = Entities.StockTaking.STOCKTAKING_STATUS_SAVED;
                    // set end date
                    stocktaking.EndDate = DateTime.Now;

                    // make positions available
                    var helper = new StockTakingHelper();
                    helper.SetPositionsStatus(db, procModel.StotakID, Position.POS_STATUS_OK);

                    //save stocktaking
                    db.Update(stocktaking);

                    SendMessageToClient(header, "Inventura uložena", 100, NotifyType.Success);

                    // do save
                    tr.Complete();
                }

                return new StockTakingSave() { Success = true };
            }

        }

    }

}

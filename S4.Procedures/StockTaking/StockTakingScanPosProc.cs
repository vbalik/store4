﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPoco;
using S4.Entities;
using S4.ProcedureModels;
using S4.ProcedureModels.StockTaking;
using S4.Procedures.StockTaking.Helper;

namespace S4.Procedures.StockTaking
{
    public class StockTakingScanPosProc : ProceduresBase<StockTakingScanPos>
    {

        public StockTakingScanPosProc(string userID) : base(userID)
        {
        }


        protected override StockTakingScanPos DoIt(StockTakingScanPos procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new StockTakingScanPos() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var stocktaking = db.SingleOrDefaultById<Entities.StockTaking>(procModel.StotakID);
                if (stocktaking == null)
                    return new StockTakingScanPos() { Success = false, ErrorText = $"StockTaking nebylo nalezeno; StotakID: {procModel.StotakID}" };

                if (stocktaking.StotakStatus != Entities.StockTaking.STOCKTAKING_STATUS_OPENED)
                    return new StockTakingScanPos() { Success = false, ErrorText = $"StockTaking nemá status STOCKTAKING_STATUS_OPENED; StotakID: {procModel.StotakID}" };


                // check if position is included in stocktaking
                var helper = new StockTakingHelper();
                if (!helper.DBCheckPosition(db, procModel.StotakID, procModel.PositionID))
                {
                    return new StockTakingScanPos()
                    {
                        BadPosition = true,
                        Success = true
                    };
                }

                // get items
                var stockTakingItems = db.Fetch<Entities.StockTakingItem>("where stotakID = @0 AND positionID = @1", procModel.StotakID, procModel.PositionID);

                return new StockTakingScanPos()
                {
                    StockTakingItems = stockTakingItems,
                    Success = true
                };
            }

        }

    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPoco;
using S4.Entities;
using S4.ProcedureModels;
using S4.ProcedureModels.StockTaking;
using S4.Procedures.Messages;
using S4.Procedures.StockTaking.Helper;

namespace S4.Procedures.StockTaking
{
    [RunAsync]
    public class StockTakingCancelProc : ProceduresBase<StockTakingCancel>
    {

        public StockTakingCancelProc(string userID) : base(userID)
        {
        }


        protected override StockTakingCancel DoIt(StockTakingCancel procModel)
        {
            var header = $"Zrušení inventury";
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
            {
                var errorTXT = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}";
                SendMessageToClient($"{header} skončilo s chybou", $"Chyba: {errorTXT}", color: NotifyType.Danger);
                return new StockTakingCancel() { Success = false, ErrorText = errorTXT };
            }
                
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // load stocktaking
                var stocktaking = db.SingleOrDefaultById<Entities.StockTaking>(procModel.StotakID);
                if (stocktaking == null)
                {
                    var errorTXT = $"StockTaking nebylo nalezeno; StotakID: {procModel.StotakID}";
                    SendMessageToClient($"{header} skončilo s chybou", $"Chyba: {errorTXT}", color: NotifyType.Danger);
                    return new StockTakingCancel() { Success = false, ErrorText = errorTXT };
                }
                    
                if (stocktaking.StotakStatus != Entities.StockTaking.STOCKTAKING_STATUS_OPENED)
                {
                    var errorTXT = $"StockTaking nemá status STOCKTAKING_STATUS_OPENED; StotakID: {procModel.StotakID}";
                    SendMessageToClient($"{header} skončilo s chybou", $"Chyba: {errorTXT}", color: NotifyType.Danger);
                    return new StockTakingCancel() { Success = false, ErrorText = errorTXT };
                }

                header = $"{header} '{stocktaking.StotakDesc}'";
                SendMessageToClient(header, "Probíhá zrušení inventury", 50);

                var positions = db.Fetch<Entities.StockTakingPosition>("where stotakid = @0", procModel.StotakID);

                // change status
                stocktaking.StotakStatus = Entities.StockTaking.STOCKTAKING_STATUS_CANCELED;

                //save data
                using (var tr = db.GetTransaction())
                {
                    // make positions available
                    var helper = new StockTakingHelper();
                    helper.SetPositionsStatus(db, procModel.StotakID, Position.POS_STATUS_OK);

                    //save stocktaking
                    db.Update(stocktaking);

                    var newHis = StockTakingHelper.CreateHistory(_userID, stocktaking.StotakID, null, StockTakingHistory.STOCKTAKING_EVENT_CANCELED, null);
                    db.Insert(newHis);


                    // do save
                    tr.Complete();
                }

                SendMessageToClient(header, "Inventura zrušena", 100, NotifyType.Success );
                return new StockTakingCancel() { Success = true };
            }
        }
    }
}

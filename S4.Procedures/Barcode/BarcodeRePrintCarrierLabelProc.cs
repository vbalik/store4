﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPoco;
using S4.Core;
using S4.Entities;
using S4.ProcedureModels.BarCode;


namespace S4.Procedures.Barcode
{
    public class BarcodeRePrintCarrierLabelProc : ProceduresBase<BarcodeRePrintCarrierLabel>
    {
        const string REPORT_TYPE = "carrLabel";

        public BarcodeRePrintCarrierLabelProc(string userID) : base(userID)
        {
        }


        protected override BarcodeRePrintCarrierLabel DoIt(BarcodeRePrintCarrierLabel procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new BarcodeRePrintCarrierLabel() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // get definition
                var printerLocation = Singleton<DAL.Cache.PrinterLocationCache>.Instance.GetItem(procModel.PrinterLocationID);
                if (printerLocation == null)
                    return new BarcodeRePrintCarrierLabel() { Success = false, ErrorText = $"Nenalezena tiskárna. (PrinterLocationID: {procModel.PrinterLocationID})" };

                var reportDef = Singleton<DAL.Cache.ReportCache_ByType>.Instance.GetItem(printerLocation.PrinterTypeID, REPORT_TYPE);
                if (reportDef == null)
                    return new BarcodeRePrintCarrierLabel() { Success = false, ErrorText = $"Nenalezen report. (reportType: {REPORT_TYPE}" };

                // print
                var report = new Report.BarCodeReport(reportDef.Template);
                var toPrint = report.Render(procModel.CarrierNum);

                if (IsTesting)
                    RenderResult = toPrint;
                else
                    Report.Printers.PrintDirect.PrintRAW(printerLocation.PrinterPath, "Barcode Print Carrier Labels", toPrint);

                return new BarcodeRePrintCarrierLabel() { Success = true };
            }

        }
    }
}

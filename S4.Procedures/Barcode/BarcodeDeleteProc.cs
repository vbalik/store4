﻿using System;
using System.Collections.Generic;
using System.Text;
using S4.Core.Utils;
using S4.Entities;
using S4.ProcedureModels.BarCode;


namespace S4.Procedures.Barcode
{
    public class BarcodeDeleteProc : ProceduresBase<BarcodeDelete>
    {
        public BarcodeDeleteProc(string userID) : base(userID)
        {
        }


        protected override BarcodeDelete DoIt(BarcodeDelete procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new BarcodeDelete() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // load packing
                var packing = db.SingleOrDefaultById<ArticlePacking>(procModel.ArtPackID);
                if (packing == null)
                    return new BarcodeDelete() { Success = false, ErrorText = $"Balení nebylo nalezeno; ArtPackID: {procModel.ArtPackID}" };
                var article = db.SingleOrDefaultById<Article>(packing.ArticleID);
                if (article == null)
                    return new BarcodeDelete() { Success = false, ErrorText = $"Karta nebyla nalezena; ArticleID: {packing.ArticleID}" };

                bool isAnotherBarCode = procModel.ArtPackBarCodeID != null;
                ArticlePackingBarCode artPackBarCode = null;
                string oldBarCode = null;
                string histData = null;

                if (isAnotherBarCode)
                {
                    artPackBarCode = db.SingleOrDefaultById<ArticlePackingBarCode>(procModel.ArtPackBarCodeID);
                    if (artPackBarCode == null)
                        return new BarcodeDelete() { Success = false, ErrorText = $"Kód nebyl nalezen; ArtPackBarCodeID: {procModel.ArtPackBarCodeID}" };

                    oldBarCode = artPackBarCode.BarCode;

                    histData = string.IsNullOrWhiteSpace(artPackBarCode.BarCode) ? null : artPackBarCode.BarCode.ToString();
                }
                else
                {
                    histData = string.IsNullOrWhiteSpace(packing.BarCode) ? null : packing.BarCode.ToString();

                    // set null bar code
                    oldBarCode = packing.BarCode;
                    packing.BarCode = null;
                }
                

                // set status
                packing.PackStatus = ArticlePacking.AP_STATUS_OK;

                // make history
                ArticleHistory history = new ArticleHistory()
                {
                    ArticleID = packing.ArticleID,
                    ArtPackID = packing.ArtPackID,
                    EventData = histData,
                    EventCode = Article.AR_EVENT_BARCODE_DEL,
                    EntryUserID = _userID,
                    EntryDateTime = DateTime.Now
                };

                // make info
                var message = new InternMessage()
                {
                    MsgClass = InternMessage.AR_BARCODE_DEL,
                    MsgSeverity = InternMessage.MessageSeverityEnum.Low,
                    MsgHeader = $"Smazání čárového kódu '{oldBarCode}' u položky '{article.ArticleCode}:{article.ArticleDesc}'",
                    MsgDateTime = DateTime.Now,
                    MsgUserID = _userID,
                    MsgText = JsonUtils.ToJsonString("artPackID", procModel.ArtPackID)
                };

                // del
                using (var tr = db.GetTransaction())
                {
                    // save barcode
                    if (isAnotherBarCode)
                        db.Delete(artPackBarCode);
                    else
                        db.Update(packing);

                    // save hist
                    db.Insert(history);

                    // save info
                    db.Insert(message);

                    // do save
                    tr.Complete();
                }

                // log
                base.Info($"BarcodeDeleteProc; ArtPackID: {procModel.ArtPackID}; BarCode: {oldBarCode}");

                return new BarcodeDelete() { Success = true };
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using S4.Core;
using S4.Core.Utils;
using S4.Entities;
using S4.ProcedureModels.BarCode;


namespace S4.Procedures.Barcode
{
    public class BarcodeMakeCodeProc : ProceduresBase<BarcodeMakeCode>
    {
        public BarcodeMakeCodeProc(string userID) : base(userID)
        {
        }


        protected override BarcodeMakeCode DoIt(BarcodeMakeCode procModel)
        {
            const int MAX_TRY = 10;
            const int NEW_CODE_LEN = 14;
            const int NEW_CODE_ARTCODE_LEN = 8;


            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new BarcodeMakeCode() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // load packing
                var packing = db.SingleOrDefaultById<ArticlePacking>(procModel.ArtPackID);
                if (packing == null)
                    return new BarcodeMakeCode() { Success = false, ErrorText = $"Balení nebylo nalezeno; ArtPackID: {procModel.ArtPackID}" };
                var article = db.SingleOrDefaultById<Article>(packing.ArticleID);
                if (article == null)
                    return new BarcodeMakeCode() { Success = false, ErrorText = $"Karta nebyla nalezena; ArticleID: {packing.ArticleID}" };

                // do tests
                if (packing.PackStatus != ArticlePacking.AP_STATUS_OK)
                    return new BarcodeMakeCode() { Success = false, ErrorText = "Balení nemá status AP_STATUS_OK" };


                // create new code
                string newCodeBase = Core.Utils.StrUtils.OnlyLetters(article.ArticleCode);
                if (newCodeBase.Length > NEW_CODE_ARTCODE_LEN)
                    newCodeBase = newCodeBase.Substring(0, NEW_CODE_ARTCODE_LEN);

                int tryCnt = MAX_TRY;
                string newCode;
                int checkRes = 0;
                do
                {
                    tryCnt--;
                    if (tryCnt == 0)
                        return new BarcodeMakeCode() { Success = false, ErrorText = "Nový kód nebyl nalezen" };

                    // make code
                    newCode = newCodeBase + Core.Utils.StrUtils.RandomDigits(NEW_CODE_LEN - newCodeBase.Length);

                    // check code
                    checkRes = db.SingleOrDefault<int>("select count(*) from s4_articlelist_packings where barcode = @0", newCode);
                }
                while (checkRes != 0);

                // change bar code
                var oldBarCode = packing.BarCode;
                packing.BarCode = newCode;
                // set status
                packing.PackStatus = ArticlePacking.AP_STATUS_OK;

                // make history
                ArticleHistory history = new ArticleHistory()
                {
                    ArticleID = packing.ArticleID,
                    ArtPackID = packing.ArtPackID,
                    EventData = newCode,
                    EventCode = Article.AR_EVENT_BARCODE_CREATED,
                    EntryUserID = _userID,
                    EntryDateTime = DateTime.Now
                };

                // make info
                var message = new InternMessage()
                {
                    MsgClass = InternMessage.AR_BARCODE_SET,
                    MsgSeverity = InternMessage.MessageSeverityEnum.Low,
                    MsgHeader = $"Vygenerován čárový kód k položce '{article.ArticleCode} - {article.ArticleDesc}'",
                    MsgDateTime = DateTime.Now,
                    MsgUserID = _userID,
                    MsgText = JsonUtils.ToJsonString("articleID", article.ArticleID, "oldBarCode", oldBarCode, "newBarCode", newCode)
                };


                // save
                using (var tr = db.GetTransaction())
                {
                    // save packing
                    db.Update(packing);

                    // save hist
                    db.Insert(history);

                    // save info
                    db.Insert(message);

                    // do save
                    tr.Complete();
                }

                // log
                base.Info($"BarcodeMakeCodeProc; ArtPackID: {procModel.ArtPackID}; New barcode: {newCode}");

                return new BarcodeMakeCode() { Success = true, BarCode = newCode };
            }
        }
    }
}

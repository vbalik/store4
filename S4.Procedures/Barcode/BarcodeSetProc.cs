﻿using System;
using NPoco;
using S4.Core;
using S4.Core.Utils;
using S4.Entities;
using S4.ProcedureModels.BarCode;


namespace S4.Procedures.Barcode
{
    public class BarcodeSetProc : ProceduresBase<BarcodeSet>
    {
        private const int MAX_BARCODE_LEN = 32;


        public BarcodeSetProc(string userID) : base(userID)
        {
        }


        protected override BarcodeSet DoIt(BarcodeSet procModel)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var check = new Core.Data.FieldsCheck();
                if (!check.DoCheck(procModel))
                    return new BarcodeSet() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

                if (procModel.BarCode.Length > MAX_BARCODE_LEN)
                {
                    base.Warn($"Čárový kód je moc dlouhý - {procModel.BarCode.Length}; '{procModel.BarCode}'");
                    return new BarcodeSet() { Success = false, ErrorText = $"Čárový kód je moc dlouhý - {procModel.BarCode.Length}" };
                }

                // check barcode exist
                var articles = new DAL.ArticleDAL().ScanByBarCode(procModel.BarCode, true);
                if (articles.Count > 0)
                    return new BarcodeSet() { Success = false, ErrorText = $"Čárový kód je již přiřazen" };

                // load packing
                var packing = db.SingleOrDefaultById<ArticlePacking>(procModel.ArtPackID);
                if (packing == null)
                    return new BarcodeSet() { Success = false, ErrorText = $"Balení nebylo nalezeno; ArtPackID: {procModel.ArtPackID}" };
                var article = db.SingleOrDefaultById<Article>(packing.ArticleID);
                if (article == null)
                    return new BarcodeSet() { Success = false, ErrorText = $"Karta nebyla nalezena; ArticleID: {packing.ArticleID}" };


                // set barcode
                ArticlePackingBarCode artPackBarcode = null;
                
                if (procModel.ArtPackBarCodeID != null || procModel.AnotherBarCodeCreation) {
                    if(procModel.ArtPackBarCodeID > 0)
                        artPackBarcode = db.SingleOrDefaultById<ArticlePackingBarCode>(procModel.ArtPackBarCodeID);

                    // set next barcode
                    _SetAnotherBarCode(db, artPackBarcode, packing, article, procModel);
                }
                else
                {
                    // set default barcode
                    _SetDefaultBarCode(db, packing, article, procModel);
                }

                // log
                base.Info($"BarcodeSetProc; ArtPackID: {procModel.ArtPackID}; BarCode: {procModel.BarCode}");

                return new BarcodeSet() { Success = true };
            }
        }

        protected void _SetDefaultBarCode(Database db, ArticlePacking packing, Article article, BarcodeSet procModel)
        {
            var oldBarCode = packing.BarCode;

            // update bar code
            packing.BarCode = procModel.BarCode;
            // set status
            packing.PackStatus = ArticlePacking.AP_STATUS_OK;

            // make history
            var history = new ArticleHistory()
            {
                ArticleID = packing.ArticleID,
                ArtPackID = packing.ArtPackID,
                EventCode = Article.AR_EVENT_BARCODE_SET,
                EventData = $"{oldBarCode}|{procModel.BarCode}",
                EntryUserID = _userID,
                EntryDateTime = DateTime.Now
            };

            // make info
            var message = new InternMessage()
            {
                MsgClass = InternMessage.AR_BARCODE_SET,
                MsgSeverity = InternMessage.MessageSeverityEnum.Low,
                MsgHeader = $"Změna čárového kódu u položky '{article.ArticleCode} - {article.ArticleDesc}'",
                MsgDateTime = DateTime.Now,
                MsgUserID = _userID,
                MsgText = JsonUtils.ToJsonString("articleID", article.ArticleID, "artPackID", procModel.ArtPackID, "oldBarCode", oldBarCode, "newBarCode", procModel.BarCode)
            };

            // save
            using (var tr = db.GetTransaction())
            {
                // save packing
                db.Update(packing);

                // save hist
                db.Insert(history);

                // save info
                db.Insert(message);

                // do save
                tr.Complete();
            }
        }

        protected void _SetAnotherBarCode(Database db, ArticlePackingBarCode artPackBarcode, ArticlePacking packing, Article article, BarcodeSet procModel)
        {
            string oldBarCode = null;
            bool isEdit = artPackBarcode != null;

            if (isEdit)
            {
                // update
                oldBarCode = artPackBarcode.BarCode;
                artPackBarcode.BarCode = procModel.BarCode;
            }
            else
            {
                // add
                artPackBarcode = new ArticlePackingBarCode
                {
                    ArticleID = packing.ArticleID,
                    ArtPackID = packing.ArtPackID,
                    BarCode = procModel.BarCode
                };
            }

            // set status
            packing.PackStatus = ArticlePacking.AP_STATUS_OK;

            // make history
            var history = new ArticleHistory()
            {
                ArticleID = packing.ArticleID,
                ArtPackID = packing.ArtPackID,
                EventCode = Article.AR_EVENT_BARCODE_SET,
                EventData = $"{oldBarCode}|{procModel.BarCode}",
                EntryUserID = _userID,
                EntryDateTime = DateTime.Now
            };

            // make info
            var message = new InternMessage()
            {
                MsgClass = InternMessage.AR_BARCODE_SET,
                MsgSeverity = InternMessage.MessageSeverityEnum.Low,
                MsgHeader = $"Změna dalšího čárového kódu u položky '{article.ArticleCode} - {article.ArticleDesc}'",
                MsgDateTime = DateTime.Now,
                MsgUserID = _userID,
                MsgText = JsonUtils.ToJsonString("articleID", article.ArticleID, "artPackID", procModel.ArtPackID, "oldBarCode", oldBarCode, "newBarCode", procModel.BarCode)
            };

            // save
            using (var tr = db.GetTransaction())
            {
                // save packing next barcode
                if (isEdit)
                    db.Update(artPackBarcode);
                else
                    db.Insert(artPackBarcode);

                // save hist
                db.Insert(history);

                // save info
                db.Insert(message);

                // do save
                tr.Complete();
            }
        }
    }
}

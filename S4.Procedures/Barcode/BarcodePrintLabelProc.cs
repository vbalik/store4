﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPoco;
using S4.Core;
using S4.Entities;
using S4.ProcedureModels.BarCode;

namespace S4.Procedures.Barcode
{
    public class BarcodePrintLabelProc : ProceduresBase<BarcodePrintLabel>
    {
        const string REPORT_TYPE = "packingLabel";
        const int MAX_QUANTITY = 100;

        public BarcodePrintLabelProc(string userID) : base(userID)
        {
        }


        protected override BarcodePrintLabel DoIt(BarcodePrintLabel procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new BarcodePrintLabel() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            if (procModel.LabelsQuant > MAX_QUANTITY)
                return new BarcodePrintLabel() { Success = false, ErrorText = $"Požadované množství překročilo maximální množství ({MAX_QUANTITY})" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                string barCode = null;

                // load packing
                var packing = db.SingleOrDefaultById<ArticlePacking>(procModel.ArtPackID);
                if (packing == null)
                    return new BarcodePrintLabel() { Success = false, ErrorText = $"Balení nebylo nalezeno; ArtPackID: {procModel.ArtPackID}" };

                var article = db.SingleOrDefaultById<Article>(packing.ArticleID);
                if (article == null)
                    return new BarcodePrintLabel() { Success = false, ErrorText = $"Karta nebyla nalezena; ArticleID: {packing.ArticleID}" };

                barCode = packing.BarCode;

                // set another barCode if exist
                if (procModel.ArtPackBarCodeID != null)
                {
                    var packBarCode = db.SingleOrDefaultById<ArticlePackingBarCode>(procModel.ArtPackBarCodeID);
                    if(packBarCode == null)
                        return new BarcodePrintLabel() { Success = false, ErrorText = $"Kód nebyl nalezen; ArticleID: {packing.ArticleID}, PackID: {procModel.ArtPackID}, ArtPackID: {procModel.ArtPackBarCodeID}" };

                    barCode = packBarCode.BarCode;
                }

                // get definition
                var printerLocation = Singleton<DAL.Cache.PrinterLocationCache>.Instance.GetItem(procModel.PrinterLocationID);
                if (printerLocation == null)
                    return new BarcodePrintLabel() { Success = false, ErrorText = $"Nenalezena tiskárna. (PrinterLocationID: {procModel.PrinterLocationID})" };

                var reportDef = Singleton<DAL.Cache.ReportCache_ByType>.Instance.GetItem(printerLocation.PrinterTypeID, REPORT_TYPE);
                if (reportDef == null)
                    return new BarcodePrintLabel() { Success = false, ErrorText = $"Nenalezen report. (reportType: {REPORT_TYPE}" };

                // print
                var report = new Report.BarCodeReport(reportDef.Template);
                var toPrint = report.Render(barCode, packing.PackDesc, article.ArticleDesc);

                try
                {
                    if (IsTesting)
                        RenderResult = toPrint;
                    else
                    {
                        for (int i = 0; i < procModel.LabelsQuant; i++)
                            Report.Printers.PrintDirect.PrintRAW(printerLocation.PrinterPath, "Barcode Print Carrier Labels", toPrint);
                    }
                }
                catch (Exception ex)
                {
                    return new BarcodePrintLabel() { ErrorText = ex.Message };
                }

                return new BarcodePrintLabel() { Success = true };

            }

        }
    }
}

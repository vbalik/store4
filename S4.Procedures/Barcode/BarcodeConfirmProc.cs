﻿using System;
using System.Collections.Generic;
using System.Text;
using S4.Core.Utils;
using S4.Entities;
using S4.ProcedureModels.BarCode;

namespace S4.Procedures.Barcode
{
    public class BarcodeConfirmProc : ProceduresBase<S4.ProcedureModels.BarCode.BarcodeConfirm>
    {
        public BarcodeConfirmProc(string userID) : base(userID)
        {
        }


        protected override BarcodeConfirm DoIt(BarcodeConfirm procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new BarcodeConfirm() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // load packing
                var packing = db.SingleOrDefaultById<ArticlePacking>(procModel.ArtPackID);
                if (packing == null)
                    return new BarcodeConfirm() { Success = false, ErrorText = $"Balení nebylo nalezeno; ArtPackID: {procModel.ArtPackID}" };
                var article = db.SingleOrDefaultById<Article>(packing.ArticleID);
                if (article == null)
                    return new BarcodeConfirm() { Success = false, ErrorText = $"Karta nebyla nalezena; ArticleID: {packing.ArticleID}" };
                
                // set status
                packing.PackStatus = ArticlePacking.AP_STATUS_OK;
                
                // make history
                ArticleHistory history = new ArticleHistory()
                {
                    ArticleID = packing.ArticleID,
                    ArtPackID = packing.ArtPackID,
                    EventData = packing.BarCode,
                    EventCode = Article.AR_EVENT_BARCODE_CONFIRMED,
                    EntryUserID = _userID,
                    EntryDateTime = DateTime.Now
                };

                // make info
                var message = new InternMessage()
                {
                    MsgClass = InternMessage.AR_UPDATE,
                    MsgSeverity = InternMessage.MessageSeverityEnum.Low,
                    MsgHeader = $"Potvrzení čárového kódu u položky '{article.ArticleCode}:{article.ArticleDesc}'",
                    MsgDateTime = DateTime.Now,
                    MsgUserID = _userID,
                    MsgText = JsonUtils.ToJsonString("artPackID", procModel.ArtPackID)
                };

                // del
                using (var tr = db.GetTransaction())
                {
                    // save packing
                    db.Update(packing);

                    // save hist
                    db.Insert(history);

                    // save info
                    db.Insert(message);

                    // do save
                    tr.Complete();
                }

                // log
                base.Info($"BarcodeConfirmProc; ArtPackID: {procModel.ArtPackID}; BarCode: {packing.BarCode}");

                return new BarcodeConfirm() { Success = true };
            }
        }
    }
}

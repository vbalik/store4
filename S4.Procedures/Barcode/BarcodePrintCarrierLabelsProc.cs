﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPoco;
using S4.Core;
using S4.Entities;
using S4.ProcedureModels.BarCode;


namespace S4.Procedures.Barcode
{
    public class BarcodePrintCarrierLabelsProc : ProceduresBase<BarcodePrintCarrierLabels>
    {
        const string REPORT_TYPE = "carrLabel";
        const int MAX_QUANTITY = 100;

        public BarcodePrintCarrierLabelsProc(string userID) : base(userID)
        {
        }


        protected override BarcodePrintCarrierLabels DoIt(BarcodePrintCarrierLabels procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new BarcodePrintCarrierLabels() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            if (procModel.LabelsQuant > MAX_QUANTITY)
                return new BarcodePrintCarrierLabels() { Success = false, ErrorText = $"Požadované množství překročilo maximální množství ({MAX_QUANTITY})" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // make label base
                string labelBase = DateTime.Now.ToString("yyMMddHHmmssff") + Core.Utils.StrUtils.RandomDigits(4) + "{0:00}";

                // get definition
                var printerLocation = Singleton<DAL.Cache.PrinterLocationCache>.Instance.GetItem(procModel.PrinterLocationID);
                if (printerLocation == null)
                    return new BarcodePrintCarrierLabels() { Success = false, ErrorText = $"Nenalezena tiskárna. (PrinterLocationID: {procModel.PrinterLocationID})" };

                var reportDef = Singleton<DAL.Cache.ReportCache_ByType>.Instance.GetItem(printerLocation.PrinterTypeID, REPORT_TYPE);
                if (reportDef == null)
                    return new BarcodePrintCarrierLabels() { Success = false, ErrorText = $"Nenalezen report. (reportType: {REPORT_TYPE}" };


                // print prefered quantity
                var report = new Report.BarCodeReport(reportDef.Template);
                for (int i = 0; i < procModel.LabelsQuant; i++)
                {
                    var newCarrierNum = String.Format(labelBase, i);

                    // print                    
                    var toPrint = report.Render(newCarrierNum);

                    if (IsTesting)
                        RenderResult = toPrint;
                    else
                        Report.Printers.PrintDirect.PrintRAW(printerLocation.PrinterPath, "Barcode Print Carrier Labels", toPrint);
                }

                return new BarcodePrintCarrierLabels() { Success = true };
            }

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPoco;
using S4.Core;
using S4.Entities;
using S4.ProcedureModels.BarCode;


namespace S4.Procedures.Barcode
{
    public class BarcodeListPrintersProc : ProceduresBase<BarcodeListPrinters>
    {
        public BarcodeListPrintersProc(string userID) : base(userID)
        {
        }


        protected override BarcodeListPrinters DoIt(BarcodeListPrinters procModel)
        {
            var check = new Core.Data.FieldsCheck();
            if (!check.DoCheck(procModel))
                return new BarcodeListPrinters() { Success = false, ErrorText = $"Parametry procedury nejsou platné {Core.Data.Formating.ParseError(check.Errors)}" };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var printerTypes = db.Query<PrinterType>().Where(pt => pt.PrinterClass == procModel.PrinterClass).ToList();
                procModel.PrintersList = db.Query<PrinterLocation>().Where(p => printerTypes.Select(pt => pt.PrinterTypeID).Contains(p.PrinterTypeID)).ToList();
            }

            procModel.Success = true;
            return procModel;            
        }
    }
}

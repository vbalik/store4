﻿using Newtonsoft.Json;
using NLog;
using NLog.Config;
using S4.Weigh.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Management;
using System.Media;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace S4.Weigh
{
    public partial class FormMain : Form
    {
        private static System.Version _version = null;
        private static NLog.Logger NLogger = null;
        private static SerialPort _serialPort = null;
        private static int _directionID;
        private static string _docInfo;
        private static LoginModel _loginModel;
        private const string ERROR_MSG = "Váha nebyla zapsána do S4!";
        private const string API_TOKEN = "S4_API_TOKEN";
        private string _token;
        private AppState _appState = AppState.WAIT_DL;
        private const int WAIT_SECOND = 600;
        private int _waitSecondRemain = 0;
        private static bool _weightOFF = false; //bez váhy
        private static string _serialPortName;

        private enum TypeMessage : byte
        {
            OK = 0,
            ERROR = 1,
            WORKING = 2,
            NOTHING = 3
        }

        private enum TypeMessageState : byte
        {
            WAIT_READ_DIR = 0,
            WAIT_WEIGH_PACK = 1,
            WAIT_WORKING = 2
        }

        private enum AppState : byte
        {
            WAIT_DL = 0,
            WAIT_WEIGH = 1
        }

        public enum TypeSound : byte
        {
            ALARM = 0,
            BEEP = 1,
            BEEP_LONG = 2
        }

        public FormMain(string[] args)
        {
            Application.ThreadException += Application_ThreadException;
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

            InitializeComponent();
            textBoxInfoText.BorderStyle = BorderStyle.None;
            labelDLInfo.Text = "";
            labelDLWeigh.Text = "";
            labelWait.Text = "";
            _waitSecondRemain = WAIT_SECOND;

            //WeightOFF
            if (GetSpecArgs(args, "weightOFF"))
                _weightOFF = true;
            else
                _weightOFF = Properties.Settings.Default.WeightOFF;

            textBoxInfoScaner.KeyDown += new KeyEventHandler(tb_KeyDown);
            this.ActiveControl = textBoxInfoScaner;

            NLogger = NLog.LogManager.GetLogger("S4.Weigh");
            NLogger.Factory.Configuration = new XmlLoggingConfiguration("Nlog.config");
            _version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
            NLogger.Info("Start up...");
            NLogger.Info(String.Format("App ver: {0}", _version));
            NLogger.Info(String.Format("OS ver: {0}", System.Environment.OSVersion));
            NLogger.Info(String.Format(".NET ver: {0}", System.Environment.Version));
            NLogger.Info($"Mód bez váhy: {_weightOFF}");

            Text = $"Váha {_version}";

            if (_weightOFF)
                Text += " MÓD BEZ PŘIPOJENÉ VÁHY!!!!";

            timerMain.Interval = 1000;
            timerMain.Start();

            timerMain.Tick += TimerMain_Tick;

            WriteToState4Delegate(TypeMessageState.WAIT_READ_DIR, "");
        }

        #region Exception

        public void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            WriteError((Exception)e.ExceptionObject);
        }

        public void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            WriteError(e.Exception);
        }

        private void WriteError(Exception ex)
        {
            MessageBox.Show($"Bohužel se vyskytla chyba v programu!{System.Environment.NewLine}{System.Environment.NewLine}Text chyby: {ex.Message}", "Chyba!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            NLogger.Error(ex);
        }

        #endregion

        #region Events

        private void buttonDirestions_Click(object sender, EventArgs e)
        {
            if (_serialPort != null)
            {
                _waitSecondRemain = WAIT_SECOND;
                StepReset();
                ClosePort();
                WriteToTextBoxInfo("", TypeMessage.NOTHING);
                WriteToState(TypeMessageState.WAIT_READ_DIR);
                labelWait.Text = "";
            }

            FormDirections form = new FormDirections(this, NLogger);
            form.ShowDialog();
        }

        private void TimerMain_Tick(object sender, EventArgs e)
        {
            //set focus to Scanner textBox
            textBoxInfoScaner.Focus();

            if (_appState == AppState.WAIT_DL)
            {
                labelWait.Text = "";
                return;
            }

            //wait
            _waitSecondRemain--;

            if (_waitSecondRemain == 0)
            {
                _waitSecondRemain = WAIT_SECOND;
                StepReset();
                ClosePort();
                WriteToTextBoxInfo("", TypeMessage.NOTHING);
                WriteToState(TypeMessageState.WAIT_READ_DIR);
                PlaySound(TypeSound.BEEP_LONG);
                NLogger.Info("Žádná akce uživatele, čekám na načtení dokladu.");
                labelWait.Text = "";
            }
            else
            {
                if (_waitSecondRemain < 10)
                    PlaySound(TypeSound.BEEP);

                TimeSpan ts = TimeSpan.FromSeconds(_waitSecondRemain);
                var remains = ts.ToString("mm\\:ss");
                labelWait.Text = $"({remains})";
            }
        }


        private void tb_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                var docInfo2Find = textBoxInfoScaner.Text.Replace(System.Environment.NewLine, "");
                textBoxInfoScaner.Text = "";
                Step(1, TypeMessage.OK);
                CheckDirection(docInfo2Find);
            }
        }

        private async void serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            _waitSecondRemain = WAIT_SECOND;

            if (_directionID == 0)
            {
                Step(3, TypeMessage.ERROR);
                return;
            }

            SerialPort sp = (SerialPort)sender;
            var indata = sp.ReadExisting().Trim();

            if (indata.Length < 1 || indata.Substring(0, 1).Equals("\u0004"))
            {
                WriteToTextBoxInfo($"Neplatná hodnota při vážení! Zkuste zvážit balíček znovu! {ERROR_MSG}", TypeMessage.ERROR);
                Step(3, TypeMessage.ERROR, "???");
                WriteToState(TypeMessageState.WAIT_WEIGH_PACK);
                return;
            }

            var weighAndUnit = FindWeighFromString(indata);

            if (!weighAndUnit.Item3)
            {
                WriteToTextBoxInfo($"Chybná hodnota z váhy. Hodnota: {indata} Zkuste zvážit balíček znovu! {ERROR_MSG}", TypeMessage.ERROR);
                Step(3, TypeMessage.ERROR, $"Hodnota: {indata}");
                WriteToState(TypeMessageState.WAIT_WEIGH_PACK);
                return;
            }

            var weigh = weighAndUnit.Item1;
            var unit = weighAndUnit.Item2;

            decimal weighKg = 0m;
            switch (unit)
            {
                case "k":
                    break;
                case "l":
                    weighKg = Math.Round(weigh * 0.45359237m, 2);
                    break;
                case "s":
                    weighKg = Math.Round(weigh * 6.35029318m, 2);
                    break;
                default:
                    break;
            }

            if (!unit.Equals("k"))
                weigh = weighKg;

            if (weigh <= 0)
            {
                WriteToTextBoxInfo($"Hmotnost: {weigh} kg{System.Environment.NewLine}Hodnota musí být větší než 0. Zkuste zvážit balíček znovu! {ERROR_MSG}", TypeMessage.ERROR);
                Step(3, TypeMessage.ERROR, $"{weigh} kg");
                WriteToState(TypeMessageState.WAIT_WEIGH_PACK);
                return;
            }

            Step(3, TypeMessage.OK, $"{weigh} kg");
            await SetWeighS4(weigh);

        }

        private void Form_Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            ClosePort();
        }

        private void buttonFake_D_Click(object sender, EventArgs e)
        {
            FormWeighInput form = new FormWeighInput(this, NLogger);
            form.ShowDialog();

        }

        public async void WeightInput(decimal weight)
        {
            if (weight > 0)
            {
                _waitSecondRemain = WAIT_SECOND;
                Step(3, TypeMessage.OK, $"{weight} kg");
                await SetWeighS4(weight);
                _appState = AppState.WAIT_DL;
            }
        }

        #endregion


        #region private

        private bool GetSpecArgs(string[] args, string arg)
        {
            if (arg != null)
            {
                return (from a in args where a.ToLower().Equals(arg.ToLower()) select true).FirstOrDefault();
            }
            else
            {
                return false;
            }
        }

        private (decimal, string, bool) FindWeighFromString(string text)
        {
            var newValue = string.Empty;
            decimal val = 0;
            string unit = "";

            for (int i = 0; i < text.Length; i++)
            {
                if (Char.IsDigit(text[i]))
                    newValue += text[i];
                else if (Char.IsPunctuation(text[i]))
                    newValue += text[i];
                else if (Char.IsLetter(text[i]))
                {
                    if (string.IsNullOrEmpty(unit))
                        unit += text[i];
                }

            }

            if (newValue.Length > 0)
            {
                if (!decimal.TryParse(newValue, System.Globalization.NumberStyles.Number, new System.Globalization.CultureInfo("en-EN"), out val))
                {
                    return (0, "", false);
                }
            }

            return (val, unit, true);
        }

        private string GetPortByName()
        {
            //USB-SERIAL CH340 (COM4)
            var searchText = Properties.Settings.Default.WeighNameSearchLike;
            var portName = "";
            ManagementObjectSearcher comPortSearcher = new ManagementObjectSearcher(@"\\localhost\root\CIMV2", "SELECT * FROM Win32_PnPEntity WHERE Caption like '%(COM%'");
            using (comPortSearcher)
            {
                string caption = null;
                foreach (ManagementObject obj in comPortSearcher.Get())
                {
                    if (obj != null)
                    {
                        object captionObj = obj["Caption"];
                        if (captionObj != null)
                        {
                            caption = captionObj.ToString();
                            if (caption.Contains(searchText))
                            {
                                portName = caption.Substring(caption.LastIndexOf("(COM")).Replace("(", string.Empty).Replace(")", string.Empty);
                                break;
                            }
                        }
                    }
                }
            }

            if (string.IsNullOrWhiteSpace(portName))
            {
                NLogger.Error($"PortName: {searchText} nebyl nalezen, použit defaultní port!");
                return Properties.Settings.Default.DefaultSerialPort;
            }
            else
            {
                NLogger.Info($"{searchText} found in {portName}");
                return portName;
            }

        }

        private void OpenPort()
        {
            if (_weightOFF)
            {
                //fake
                WriteToInfoLabel($"Mód bez váhy - fake port is open");
                _appState = AppState.WAIT_WEIGH;
                return;
            }

            if (_serialPort != null)
                ClosePort();

            _serialPortName = GetPortByName();

            try
            {
                _serialPort = new SerialPort(_serialPortName);
                _serialPort.BaudRate = Properties.Settings.Default.BaudRate;
                _serialPort.Parity = Parity.None;
                _serialPort.StopBits = StopBits.One;
                _serialPort.DataBits = Properties.Settings.Default.DataBits;
                _serialPort.Handshake = Handshake.None;
                _serialPort.RtsEnable = true;
                _serialPort.Encoding = Encoding.ASCII;
                _serialPort.DataReceived += serialPort_DataReceived;

                _serialPort.Open();
            }
            catch (Exception ex)
            {
                WriteToTextBoxInfo($"Nebyla nalezena připojená váha na portu {_serialPortName}! Chyba: {ex.Message}... Kontaktujte podporu S4.", TypeMessage.ERROR);
                WriteToState(TypeMessageState.WAIT_READ_DIR);
                Step(3, TypeMessage.ERROR);
                return;
            }

            WriteToInfoLabel($"BaudRate: {_serialPort.BaudRate}, Parity: {_serialPort.Parity}, StopBits: {_serialPort.StopBits} ----> Serial port: {_serialPort.PortName} is open");

            textBoxInfoText.SelectionStart = textBoxInfoText.Text.Length;
            textBoxInfoText.SelectionLength = 0;
            _appState = AppState.WAIT_WEIGH;
            NLogger.Info($"Port {_serialPort.PortName} otevřen.", TypeMessage.OK);
        }

        private void ClosePort()
        {
            if (_serialPort != null)
            {
                NLogger.Info($"Port {_serialPort.PortName} zavřen.", TypeMessage.OK);
                _serialPort.Close();
                _serialPort = null;
                WriteToInfoLabel($"Serial port: {_serialPortName} is close");
                _serialPort = null;
                _appState = AppState.WAIT_DL;
            }
        }

        private void WriteToInfoLabel4Delegate(string text)
        {
            labelInfo.Text = text;
        }

        private void WriteToInfoLabel(string text)
        {
            Action<string> delegateTeste_ModifyText = WriteToInfoLabel4Delegate;
            Invoke(delegateTeste_ModifyText, text);
        }

        private void WriteToTextBoxInfo4Delegate(string text, TypeMessage typeMessage, bool log)
        {
            switch (typeMessage)
            {
                case TypeMessage.OK:
                    textBoxInfoText.ForeColor = Color.White;
                    textBoxInfoText.BackColor = Color.Green;
                    break;
                case TypeMessage.WORKING:
                    textBoxInfoText.ForeColor = Color.White;
                    textBoxInfoText.BackColor = Color.Orange;
                    break;
                case TypeMessage.NOTHING:
                    textBoxInfoText.ForeColor = SystemColors.ControlText;
                    textBoxInfoText.BackColor = SystemColors.Control;
                    break;
                case TypeMessage.ERROR:
                    //play alarm
                    PlaySound(TypeSound.ALARM);
                    textBoxInfoText.ForeColor = Color.White;
                    textBoxInfoText.BackColor = Color.Red;
                    break;
                default:
                    break;
            }

            textBoxInfoText.Text = text;

            if (log)
            {
                if (typeMessage == TypeMessage.ERROR)
                    NLogger.Error(text);
                else
                    NLogger.Info(text);
            }

        }

        private void WriteToTextBoxInfo(string text, TypeMessage typeMessage, bool log = true)
        {
            Action<string, TypeMessage, bool> delegateTeste_ModifyText = WriteToTextBoxInfo4Delegate;
            Invoke(delegateTeste_ModifyText, text, typeMessage, log);
        }

        private void WriteToState(TypeMessageState typeMessageState, string message = "")
        {
            Action<TypeMessageState, string> delegateTeste_ModifyText = WriteToState4Delegate;
            Invoke(delegateTeste_ModifyText, typeMessageState, message);
        }

        private void WriteToState4Delegate(TypeMessageState typeMessageState, string message)
        {
            string text = "";
            switch (typeMessageState)
            {
                case TypeMessageState.WAIT_READ_DIR:
                    text = "Čekám na dodací list.....";
                    break;
                case TypeMessageState.WAIT_WEIGH_PACK:
                    text = "Zvažte balíček a stiskněte prosím tlačítko D, nebo znovu vyberte DL...";
                    break;
                case TypeMessageState.WAIT_WORKING:
                    text = "Čekejte prosím.....";
                    break;
                default:
                    break;
            }

            if (!string.IsNullOrWhiteSpace(message))
                text += $" {message}";

            if (string.IsNullOrWhiteSpace(textBoxInfoText.Text))
                textBoxInfoText.Text += text;
            else
            {
                if (!textBoxInfoText.Text.Contains(text))
                    textBoxInfoText.Text += $"{System.Environment.NewLine}{text}";
            }

        }

        private void WriteToTextBoxDLWeigh4Delegate(string text)
        {
            labelDLWeigh.Text = text;
        }

        private void WriteToTextBoxDLWeigh(string text)
        {
            Action<string> delegateTeste_ModifyText = WriteToTextBoxDLWeigh4Delegate;
            Invoke(delegateTeste_ModifyText, text);
        }

        public void PlaySound(TypeSound type)
        {
            System.IO.Stream str = null;

            switch (type)
            {
                case TypeSound.ALARM:
                    str = Properties.Resources.Overspeed_Warning;
                    break;
                case TypeSound.BEEP:
                    str = Properties.Resources.beep_07a;
                    break;
                case TypeSound.BEEP_LONG:
                    str = Properties.Resources.beep_09;
                    break;
                default:
                    break;
            }

            System.Media.SoundPlayer snd = new System.Media.SoundPlayer(str);
            snd.Play();
            str.Dispose();
        }

        private async void CheckDirection(string docInfo2Find)
        {
            _directionID = 0;
            _docInfo = "";
            NLogger.Info($"Dotaz do S4 na doklad {docInfo2Find}");
            WriteToState(TypeMessageState.WAIT_WORKING);
            ClosePort();
            textBoxInfoText.Text = "";
            var docInfo2FindTXT = docInfo2Find.Length > 17 ? $"{docInfo2Find.Substring(0, 17)}..." : docInfo2Find;

            //logon
            var sucess = await Logon();

            if (!sucess)
            {
                WriteToState(TypeMessageState.WAIT_READ_DIR);
                Step(2, TypeMessage.ERROR, docInfo2FindTXT);
                return;
            }

            var directions = await GetDirections(docInfo2Find);

            if (directions == null || directions.Count == 0)
            {
                //not find
                WriteToTextBoxInfo("", TypeMessage.NOTHING, false);
                WriteToTextBoxInfo($"V S4 nebyl nalezen platný doklad {docInfo2FindTXT}. {ERROR_MSG}", TypeMessage.ERROR);
                WriteToState(TypeMessageState.WAIT_READ_DIR);
                Step(2, TypeMessage.ERROR, docInfo2FindTXT);
                return;
            }
            else
            {
                _directionID = directions.OrderByDescending(_ => _.DirectionID).First().DirectionID;
                _docInfo = docInfo2Find;
            }

            NLogger.Info($"Doklad {_docInfo} v S4 nalezen.");
            WriteToState(TypeMessageState.WAIT_WEIGH_PACK);
            Step(2, TypeMessage.OK, _docInfo);
            OpenPort();
        }

        public void DirectionSelectedFromList(int directionID, string docInfo)
        {
            _directionID = directionID;
            _docInfo = docInfo;

            WriteToTextBoxInfo("", TypeMessage.NOTHING);
            WriteToState(TypeMessageState.WAIT_WEIGH_PACK);
            Step(1, TypeMessage.OK);
            Step(2, TypeMessage.OK, _docInfo);
            OpenPort();

        }

        private async Task SetWeighS4(decimal weigh)
        {
            //před zápisem váhy ještě kontrola dokladu!!!!
            var directions = await GetDirections("", _directionID);
            if (directions.Count < 1)
            {
                WriteToTextBoxInfo($"V S4 nebyl nalezen platný doklad {_docInfo}. {ERROR_MSG}", TypeMessage.ERROR);
                WriteToState(TypeMessageState.WAIT_READ_DIR);
                Step(2, TypeMessage.ERROR, _docInfo);
                return;
            }

            NLogger.Info($"Zapisuji data do S4. Doklad: {_docInfo} Hmotnost: {weigh} kg.");

            var values = new List<DispatchSetXtraValue>
            {
                new DispatchSetXtraValue {Key = "DI_WEIGHT", Value = weigh}
            };

            var uri = $"{Properties.Settings.Default.S4Server}api/procedures/dispatchsetxtravalues";
            var resultStr = await CallApi(HttpMethod.Post, uri, new DispatchSetXtraValues { DirectionID = _directionID, Values = values });

            if (!string.IsNullOrWhiteSpace(resultStr))
            {
                WriteToTextBoxInfo($"{_docInfo}{System.Environment.NewLine}Hmotnost {weigh} kg byla zapsána do S4", TypeMessage.OK, false);
                WriteToState(TypeMessageState.WAIT_READ_DIR);
                NLogger.Info($"{_docInfo} - hmotnost: {weigh} kg byla zapsána do S4");
                Step(4, TypeMessage.OK);
            }
            else
                Step(4, TypeMessage.ERROR);

            ClosePort();

        }

        public async Task<bool> Logon(bool write2TextBox = true)
        {
            var uri = $"{Properties.Settings.Default.S4Server}api/procedures/login";
            var resultStr = await CallApi(HttpMethod.Post, uri, new LoginModel { UserName = Properties.Settings.Default.UserName, Password = Properties.Settings.Default.Password }, write2TextBox);

            if (!string.IsNullOrWhiteSpace(resultStr))
            {
                try
                {
                    _loginModel = JsonConvert.DeserializeObject<LoginModel>(resultStr);
                }
                catch (Exception ex)
                {
                    NLogger.Error($"(Logon) Chyba při pokusu o deserializaci hodnoty {resultStr} - error: {ex.Message}");
                    WriteToTextBoxInfo($"Chyba při pokusu o přihlášení do S4", TypeMessage.ERROR);
                    return false;
                }

                if (_loginModel.Worker == null)
                {
                    if (write2TextBox)
                        WriteToTextBoxInfo($"Chyba při pokusu o přihlášení do S4 ---> chyba: {resultStr}", TypeMessage.ERROR);
                    else
                        NLogger.Info($"Chyba při pokusu o přihlášení do S4 ---> chyba: {resultStr}");

                    return false;
                }
                else
                {
                    NLogger.Info("Přihlášení do S4 - OK");
                    return true;
                }
            }
            return false;
        }

        public async Task<List<DirectionRow>> GetDirections(string docInfo, int directionID = 0)
        {
            var uri = $"{Properties.Settings.Default.S4Server}api/procedures/getdirectionsforweigh";
            var deliveryTypes = new List<string>() { "Doprava Česká pošta", "Doprava DPD", "Doprava DPD SK", "Doprava PPL", "Doprava PickUp" };

            //Kontrola - hotovo
            var resultStr = await CallApi(HttpMethod.Post, uri, new GetDirectionsForWeigh() { DirectionID = directionID, DocInfo = docInfo, DeliveryTypes = deliveryTypes });

            if (!string.IsNullOrWhiteSpace(resultStr))
            {
                GetDirectionsForWeigh data = null;

                try
                {
                    data = JsonConvert.DeserializeObject<GetDirectionsForWeigh>(resultStr);
                }
                catch (Exception ex)
                {
                    NLogger.Error($"(GetDirections) Chyba při pokusu o deserializaci hodnoty {resultStr} - error: {ex.Message}");
                    return new List<DirectionRow>();
                }


                return data.Directions;
            }

            return new List<DirectionRow>();

        }

        public async Task<string> CallApi(HttpMethod method, string uri, object requestBody, bool write2TextBox = true)
        {
            if (write2TextBox)
                WriteToTextBoxInfo("Moment, komunikuji se systémem S4", TypeMessage.WORKING, false);

            try
            {
                using (var requestMessage = new HttpRequestMessage(method, uri))
                {
                    var requestBodySerialized = JsonConvert.SerializeObject(requestBody);

                    if (!string.IsNullOrWhiteSpace(_token))
                        requestMessage.Headers.Add(API_TOKEN, _token);

                    requestMessage.Content = new StringContent(requestBodySerialized, Encoding.UTF8, "application/json");

                    HttpClient client = new HttpClient();

                    var response = await client.SendAsync(requestMessage);

                    if (write2TextBox)
                        WriteToTextBoxInfo("", TypeMessage.NOTHING, false);

                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        if (response.Headers.Contains(API_TOKEN))
                        {
                            _token = response.Headers.GetValues(API_TOKEN).FirstOrDefault<string>();
                        }
                        return await response.Content.ReadAsStringAsync();
                    }

                    else
                        return "";

                }
            }
            catch (Exception e)
            {
                if (write2TextBox)
                {
                    WriteToTextBoxInfo($"Chyba v komunikaci s S4: {e.Message}", TypeMessage.ERROR, false);
                    WriteToState(TypeMessageState.WAIT_READ_DIR);
                }
                NLogger.Info($"Url: {uri} Chyba v komunikaci s S4: {e.Message}");
                return "";
            }

        }

        private void Step(int step, TypeMessage type, string text = "")
        {
            if (step == 1)
            {
                //reset
                StepReset();
            }

            var pic = (PictureBox)this.Controls[$"pictureBoxStep{step}"];

            if (type == TypeMessage.ERROR)
                pic.Image = Properties.Resources.error_7_64;
            else if (type == TypeMessage.OK)
                pic.Image = Properties.Resources.ok_64;
            else if (type == TypeMessage.WORKING)
                pic.Image = Properties.Resources.question_64;


            if (step == 2)
            {
                if (type == TypeMessage.ERROR)
                    labelDLInfo.ForeColor = Color.Red;
                else if (type == TypeMessage.OK)
                    labelDLInfo.ForeColor = Color.Green;
                else if (type == TypeMessage.WORKING)
                    labelDLInfo.ForeColor = Color.Black;

                labelDLInfo.Text = text;
            }
            else if (step == 3)
            {
                if (type == TypeMessage.ERROR)
                    labelDLWeigh.ForeColor = Color.Red;
                else
                    labelDLWeigh.ForeColor = Color.Green;

                WriteToTextBoxDLWeigh(text);
            }

            if (type == TypeMessage.OK && step != 4)
            {
                var pic2 = (PictureBox)this.Controls[$"pictureBoxStep{step + 1}"];
                pic2.Image = Properties.Resources.question_64;
                PlaySound(TypeSound.BEEP);
            }

            if (type == TypeMessage.OK && step == 4)
                PlaySound(TypeSound.BEEP_LONG);

            if (step == 2 && _weightOFF && _directionID > 0)
                buttonFake_D.Visible = true;
            else
                buttonFake_D.Visible = false;
        }

        private void StepReset()
        {
            for (int i = 1; i < 5; i++)
            {
                ((PictureBox)this.Controls[$"pictureBoxStep{i}"]).Image = null;
            }
            labelDLInfo.Text = "";
            WriteToTextBoxDLWeigh("");
            labelDLInfo.ForeColor = Color.Black;
            labelDLWeigh.ForeColor = Color.Black;
            textBoxInfoText.BackColor = SystemColors.Control;
        }


    }

    #endregion


}


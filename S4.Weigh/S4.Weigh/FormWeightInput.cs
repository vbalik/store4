﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace S4.Weigh
{
    public partial class FormWeighInput : Form
    {
        FormMain _formMain = null;
        private static NLog.Logger NLogger = null;

        public FormWeighInput(FormMain formMain, NLog.Logger nLogger)
        {
            Application.ThreadException += formMain.Application_ThreadException;
            AppDomain.CurrentDomain.UnhandledException += formMain.CurrentDomain_UnhandledException;

            InitializeComponent();
            _formMain = formMain;
            NLogger = nLogger;
            numericUpDownWeightInput.Select(0, 4);
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            _formMain.WeightInput(numericUpDownWeightInput.Value);
            Close();
        }

        private void buttonStorno_Click(object sender, EventArgs e)
        {
            _formMain.WeightInput(0);
            Close();
        }
    }
}

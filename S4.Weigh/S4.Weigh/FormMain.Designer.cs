﻿
namespace S4.Weigh
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.textBoxInfoText = new System.Windows.Forms.TextBox();
            this.textBoxInfoScaner = new System.Windows.Forms.TextBox();
            this.labelInfo = new System.Windows.Forms.Label();
            this.timerMain = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.labelStep1 = new System.Windows.Forms.Label();
            this.labelStep2 = new System.Windows.Forms.Label();
            this.labelStep3 = new System.Windows.Forms.Label();
            this.labelStep4 = new System.Windows.Forms.Label();
            this.pictureBoxStep1 = new System.Windows.Forms.PictureBox();
            this.pictureBoxStep2 = new System.Windows.Forms.PictureBox();
            this.pictureBoxStep3 = new System.Windows.Forms.PictureBox();
            this.pictureBoxStep4 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelDLInfo = new System.Windows.Forms.Label();
            this.labelWait = new System.Windows.Forms.Label();
            this.buttonDirestions = new System.Windows.Forms.Button();
            this.buttonFake_D = new System.Windows.Forms.Button();
            this.labelDLWeigh = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStep1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStep2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStep3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStep4)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxInfoText
            // 
            this.textBoxInfoText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxInfoText.BackColor = System.Drawing.SystemColors.Control;
            this.textBoxInfoText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxInfoText.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxInfoText.ForeColor = System.Drawing.SystemColors.ControlText;
            this.textBoxInfoText.Location = new System.Drawing.Point(6, 30);
            this.textBoxInfoText.Multiline = true;
            this.textBoxInfoText.Name = "textBoxInfoText";
            this.textBoxInfoText.ReadOnly = true;
            this.textBoxInfoText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxInfoText.Size = new System.Drawing.Size(1404, 290);
            this.textBoxInfoText.TabIndex = 1;
            // 
            // textBoxInfoScaner
            // 
            this.textBoxInfoScaner.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxInfoScaner.BackColor = System.Drawing.SystemColors.Control;
            this.textBoxInfoScaner.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxInfoScaner.Location = new System.Drawing.Point(1251, 704);
            this.textBoxInfoScaner.Name = "textBoxInfoScaner";
            this.textBoxInfoScaner.Size = new System.Drawing.Size(177, 22);
            this.textBoxInfoScaner.TabIndex = 2;
            // 
            // labelInfo
            // 
            this.labelInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelInfo.AutoSize = true;
            this.labelInfo.Location = new System.Drawing.Point(9, 713);
            this.labelInfo.Name = "labelInfo";
            this.labelInfo.Size = new System.Drawing.Size(16, 13);
            this.labelInfo.TabIndex = 5;
            this.labelInfo.Text = "...";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1204, 709);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Skener";
            // 
            // labelStep1
            // 
            this.labelStep1.AutoSize = true;
            this.labelStep1.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelStep1.Location = new System.Drawing.Point(6, 16);
            this.labelStep1.Name = "labelStep1";
            this.labelStep1.Size = new System.Drawing.Size(398, 73);
            this.labelStep1.TabIndex = 8;
            this.labelStep1.Text = "1. Dodací list";
            // 
            // labelStep2
            // 
            this.labelStep2.AutoSize = true;
            this.labelStep2.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelStep2.Location = new System.Drawing.Point(6, 101);
            this.labelStep2.Name = "labelStep2";
            this.labelStep2.Size = new System.Drawing.Size(671, 73);
            this.labelStep2.TabIndex = 9;
            this.labelStep2.Text = "2. S4 kontrola dokladu";
            // 
            // labelStep3
            // 
            this.labelStep3.AutoSize = true;
            this.labelStep3.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelStep3.Location = new System.Drawing.Point(6, 186);
            this.labelStep3.Name = "labelStep3";
            this.labelStep3.Size = new System.Drawing.Size(378, 73);
            this.labelStep3.TabIndex = 10;
            this.labelStep3.Text = "3. Hmotnost";
            // 
            // labelStep4
            // 
            this.labelStep4.AutoSize = true;
            this.labelStep4.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelStep4.Location = new System.Drawing.Point(6, 271);
            this.labelStep4.Name = "labelStep4";
            this.labelStep4.Size = new System.Drawing.Size(509, 73);
            this.labelStep4.TabIndex = 11;
            this.labelStep4.Text = "4. Přenos do S4 ";
            // 
            // pictureBoxStep1
            // 
            this.pictureBoxStep1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxStep1.Location = new System.Drawing.Point(1357, 20);
            this.pictureBoxStep1.Name = "pictureBoxStep1";
            this.pictureBoxStep1.Size = new System.Drawing.Size(64, 64);
            this.pictureBoxStep1.TabIndex = 12;
            this.pictureBoxStep1.TabStop = false;
            // 
            // pictureBoxStep2
            // 
            this.pictureBoxStep2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxStep2.Location = new System.Drawing.Point(1357, 105);
            this.pictureBoxStep2.Name = "pictureBoxStep2";
            this.pictureBoxStep2.Size = new System.Drawing.Size(64, 64);
            this.pictureBoxStep2.TabIndex = 14;
            this.pictureBoxStep2.TabStop = false;
            // 
            // pictureBoxStep3
            // 
            this.pictureBoxStep3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxStep3.Location = new System.Drawing.Point(1357, 190);
            this.pictureBoxStep3.Name = "pictureBoxStep3";
            this.pictureBoxStep3.Size = new System.Drawing.Size(64, 64);
            this.pictureBoxStep3.TabIndex = 15;
            this.pictureBoxStep3.TabStop = false;
            // 
            // pictureBoxStep4
            // 
            this.pictureBoxStep4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxStep4.Location = new System.Drawing.Point(1357, 275);
            this.pictureBoxStep4.Name = "pictureBoxStep4";
            this.pictureBoxStep4.Size = new System.Drawing.Size(64, 64);
            this.pictureBoxStep4.TabIndex = 16;
            this.pictureBoxStep4.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.textBoxInfoText);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox1.Location = new System.Drawing.Point(12, 374);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1416, 326);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Info";
            // 
            // labelDLInfo
            // 
            this.labelDLInfo.AutoSize = true;
            this.labelDLInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelDLInfo.Location = new System.Drawing.Point(668, 101);
            this.labelDLInfo.Name = "labelDLInfo";
            this.labelDLInfo.Size = new System.Drawing.Size(204, 73);
            this.labelDLInfo.TabIndex = 21;
            this.labelDLInfo.Text = "label2";
            // 
            // labelWait
            // 
            this.labelWait.AutoSize = true;
            this.labelWait.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelWait.Location = new System.Drawing.Point(380, 212);
            this.labelWait.Name = "labelWait";
            this.labelWait.Size = new System.Drawing.Size(71, 29);
            this.labelWait.TabIndex = 23;
            this.labelWait.Text = "00:00";
            // 
            // buttonDirestions
            // 
            this.buttonDirestions.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonDirestions.Location = new System.Drawing.Point(410, 16);
            this.buttonDirestions.Name = "buttonDirestions";
            this.buttonDirestions.Size = new System.Drawing.Size(327, 73);
            this.buttonDirestions.TabIndex = 24;
            this.buttonDirestions.Text = "Seznam DL";
            this.buttonDirestions.UseVisualStyleBackColor = true;
            this.buttonDirestions.Click += new System.EventHandler(this.buttonDirestions_Click);
            // 
            // buttonFake_D
            // 
            this.buttonFake_D.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonFake_D.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonFake_D.Location = new System.Drawing.Point(1220, 193);
            this.buttonFake_D.Name = "buttonFake_D";
            this.buttonFake_D.Size = new System.Drawing.Size(111, 61);
            this.buttonFake_D.TabIndex = 25;
            this.buttonFake_D.Text = "D";
            this.buttonFake_D.UseVisualStyleBackColor = true;
            this.buttonFake_D.Visible = false;
            this.buttonFake_D.Click += new System.EventHandler(this.buttonFake_D_Click);
            // 
            // labelDLWeigh
            // 
            this.labelDLWeigh.AutoSize = true;
            this.labelDLWeigh.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelDLWeigh.Location = new System.Drawing.Point(458, 186);
            this.labelDLWeigh.Name = "labelDLWeigh";
            this.labelDLWeigh.Size = new System.Drawing.Size(204, 73);
            this.labelDLWeigh.TabIndex = 22;
            this.labelDLWeigh.Text = "label2";
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1440, 731);
            this.Controls.Add(this.buttonFake_D);
            this.Controls.Add(this.buttonDirestions);
            this.Controls.Add(this.labelWait);
            this.Controls.Add(this.labelDLWeigh);
            this.Controls.Add(this.labelDLInfo);
            this.Controls.Add(this.pictureBoxStep4);
            this.Controls.Add(this.pictureBoxStep3);
            this.Controls.Add(this.pictureBoxStep2);
            this.Controls.Add(this.pictureBoxStep1);
            this.Controls.Add(this.labelStep4);
            this.Controls.Add(this.labelStep3);
            this.Controls.Add(this.labelStep2);
            this.Controls.Add(this.labelStep1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelInfo);
            this.Controls.Add(this.textBoxInfoScaner);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Váha";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_Main_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStep1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStep2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStep3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStep4)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxInfoText;
        private System.Windows.Forms.TextBox textBoxInfoScaner;
        private System.Windows.Forms.Label labelInfo;
        private System.Windows.Forms.Timer timerMain;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelStep1;
        private System.Windows.Forms.Label labelStep2;
        private System.Windows.Forms.Label labelStep3;
        private System.Windows.Forms.Label labelStep4;
        private System.Windows.Forms.PictureBox pictureBoxStep1;
        private System.Windows.Forms.PictureBox pictureBoxStep2;
        private System.Windows.Forms.PictureBox pictureBoxStep3;
        private System.Windows.Forms.PictureBox pictureBoxStep4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label labelDLInfo;
        private System.Windows.Forms.Label labelWait;
        private System.Windows.Forms.Button buttonDirestions;
        private System.Windows.Forms.Button buttonFake_D;
        private System.Windows.Forms.Label labelDLWeigh;
    }
}


﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Weigh.Model
{
    public class GetDirectionsForWeigh 
    {
        // in
        public int DirectionID { get; set; }
        public string DocInfo { get; set; }
        public List<string> DeliveryTypes { get; set; }

        // out
        public List<DirectionRow> Directions { get; set; }
    }

    public class DirectionRow
    {
        public int DirectionID { get; set; }
        public string DocInfo { get; set; }
        public string Partner { get; set; }
        public string DeliveryType { get; set; }
        public string WeighValue { get; set; }
    }
}
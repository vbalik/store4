﻿using System.Collections.Generic;

namespace S4.Weigh.Model
{
    public class DispCheckAvailableDirectionsDocInfo
    {
        // in
        public string DocInfo { get; set; }

        // for testing
        public int DocYear { get; set; }


        // out
        public List<DocumentInfo> Directions { get; set; } = new List<DocumentInfo>();
    }
}

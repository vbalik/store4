﻿using System;
using System.Collections.Generic;

namespace S4.Weigh.Model
{
    public class DispatchSetXtraValues
    {
        // in
        public int DirectionID { get; set; }

        public List<DispatchSetXtraValue> Values { get; set; }
    }

    public class DispatchSetXtraValue
    {
        public string Key { get; set; }
        public dynamic Value { get; set; }
    }
}



namespace S4.Weigh.Model
{

	public partial class Worker
	{
		
		
		public string WorkerID {get; set;}
		public string WorkerName {get; set;}
		public object WorkerClass {get; set;}
		public string WorkerStatus {get; set;}
		public string WorkerLogon {get; set;}
		public string WorkerSecData {get; set;}
		public byte[] R_Edit {get; set;}

	}
}

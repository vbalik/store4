﻿using Newtonsoft.Json;
using S4.Weigh.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static S4.Weigh.FormMain;

namespace S4.Weigh
{
    public partial class FormDirections : Form
    {
        FormMain _formMain = null;
        private static NLog.Logger NLogger = null;

        public FormDirections(FormMain formMain, NLog.Logger nLogger)
        {
            Application.ThreadException += formMain.Application_ThreadException;
            AppDomain.CurrentDomain.UnhandledException += formMain.CurrentDomain_UnhandledException;

            InitializeComponent();
            _formMain = formMain;
            NLogger = nLogger;

            labelError.Text = "";
        }

        #region events

        private void FormDirections_Load(object sender, EventArgs e)
        {
            dataGridViewDirections.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            dataGridViewDirections.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;

            labelError.ForeColor = Color.Black;
            ReadDirections();
        }

        private void dataGridViewDirections_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var dg = (DataGridView)sender;
            if (e.RowIndex > -1 && e.ColumnIndex == dg.Rows[e.RowIndex].Cells["SelectRow"].ColumnIndex)
            {
                var directionID = (int)dg.Rows[e.RowIndex].Cells["directionID"].Value;
                var docInfo = dg.Rows[e.RowIndex].Cells["docInfo"].Value.ToString();
                _formMain.DirectionSelectedFromList(directionID, docInfo);
                Close();
            }
        }

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            ReadDirections();
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        #endregion

        #region private

        private async void ReadDirections()
        {
            EnableForm(false);
            labelError.Text = "Moment čtu dodací listy z S4.....";
            //logon
            var sucess = await _formMain.Logon(false);

            if (!sucess)
            {
                labelError.ForeColor = Color.Red;
                labelError.Text = "Chyba při pokusu o přihlášení do S4";
                _formMain.PlaySound(TypeSound.ALARM);
                EnableForm(true);
                return;
            }

            var directions = await _formMain.GetDirections("");

            if (directions == null)
            {
                labelError.Text = "Chyba při pokusu stáhnout seznam DL ze systému S4";
                labelError.ForeColor = Color.Red;
                EnableForm(true);
                return;

            }
           
            var info = $"Data z S4 stažena, počet vět: {directions.Count}";
            NLogger.Info(info);
            labelError.Text = info;
            labelError.ForeColor = Color.Black;

            var dgv = dataGridViewDirections;
            dgv.Rows.Clear();

            foreach (var row in directions)
            {
                var index = dgv.Rows.Add();
                var weighValue = string.IsNullOrWhiteSpace(row.WeighValue) ? "" : $"({ row.WeighValue} kg)";
                dgv.Rows[index].Cells["docInfo"].Value = row.DocInfo;
                dgv.Rows[index].Cells["partner"].Value = row.Partner;
                dgv.Rows[index].Cells["deliveryType"].Value = $"{row.DeliveryType.Replace("Doprava ","")}{System.Environment.NewLine}{weighValue}";
                dgv.Rows[index].Cells["directionID"].Value = row.DirectionID;
                dgv.Rows[index].Cells["SelectRow"].Value = "OK";

                if (!string.IsNullOrWhiteSpace(row.WeighValue))
                {
                    dgv.Rows[index].DefaultCellStyle.ForeColor = Color.Gray;
                }
            }

            EnableForm(true);
            return;
        }

        private void EnableForm(bool enable)
        {
            if (enable)
                Cursor.Current = Cursors.Default;
            else
                Cursor.Current = Cursors.WaitCursor;

            this.Enabled = enable;
        }

        #endregion

    }
}

﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;

namespace S4.InfoSerialPorts
{
    class Program
    {
        static void Main(string[] args)
        {
            ListSerialPorts();
            Console.ReadKey();
        }

        private static void ListSerialPorts()
        {
            ManagementObjectSearcher comPortSearcher = new ManagementObjectSearcher(@"\\localhost\root\CIMV2", "SELECT * FROM Win32_PnPEntity WHERE Caption like '%(COM%'");
            using (comPortSearcher)
            {
                var portName = "";
                string caption = null;
                foreach (ManagementObject obj in comPortSearcher.Get())
                {
                    if (obj != null)
                    {
                        object captionObj = obj["Caption"];
                        if (captionObj != null)
                        {
                            caption = captionObj.ToString();
                            portName = caption.Substring(caption.LastIndexOf("(COM")).Replace("(", string.Empty).Replace(")", string.Empty);
                            Console.WriteLine($"PortName: {portName} Description: {caption}");
                        }
                    }
                }
            }

        }
    }
}

﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Messages
{
    public class MessageRouter : IMessageRouter
    {
        private readonly RoutingTable _routingTable = new RoutingTable();
        private readonly object _lock = new object();
        private readonly ILogger<MessageRouter> _logger;

        public MessageRouter()
        { }


        public MessageRouter(ILogger<MessageRouter> logger)
        {
            _logger = logger;
        }


        public bool Subscribe<T>(string subscriberID, Action<MessageBase> deliveryTask) where T : MessageBase
        {
            lock (_lock)
            {
                var rti = new RoutingTableItem(typeof(T), subscriberID, deliveryTask);

                // check if not already routed
                if (!_routingTable.Contains(rti))
                {
                    // add to routing table
                    _routingTable.Add(rti);

                    return true;
                }

                return false;
            }
        }


        public void Publish<T>(T msg) where T : MessageBase
        {
            _logger?.LogTrace($"New message {typeof(T)}; id: {msg.MessageID}");


            List<RoutingTableItem> subs;
            lock (_lock)
            {
                // search routing table
                subs = _routingTable.Subscribers(typeof(T));
            }

            // send to subscribers
            if (subs.Any())
            {
                subs.ForEach(async i => await Task.Run(() =>
                {
                    try
                    {
                        i.DeliveryTask(msg);
                    }
                    catch (Exception exc)
                    {
                        _logger?.LogError(exc, $"Publish error; msgType: {typeof(T)}");
                    }
                }));
            }
        }
    }
}

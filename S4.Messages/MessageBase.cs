﻿using System;

namespace S4.Messages
{
    public abstract class MessageBase
    {
        public Guid MessageID { get; } = Guid.NewGuid();
        public DateTime MessageCreated { get; } = DateTime.Now;
    }
}

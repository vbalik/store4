﻿using System;

namespace S4.Messages
{
    public interface IMessageRouter
    {
        bool Subscribe<T>(string subscriberID, Action<MessageBase> deliveryTask) where T : MessageBase;

        void Publish<T>(T msg) where T : MessageBase;
    }
}

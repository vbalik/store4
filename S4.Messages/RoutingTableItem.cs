﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace S4.Messages
{
    internal class RoutingTableItem : System.IEquatable<RoutingTableItem>
    {
        public Type MessageType { get; private set; }
        public string SubscriberID { get; private set; }
        public Action<MessageBase> DeliveryTask { get; private set; }
        private int HashValue { get; }
        
        public RoutingTableItem(Type messageType, string subscriberID, Action<MessageBase> deliveryTask)
        {
            MessageType = messageType;
            SubscriberID = subscriberID;
            DeliveryTask = deliveryTask;
            HashValue = GetHashCode();
        }

        public sealed override int GetHashCode()
        {
            unchecked
            {
                // combine hash codes
                int hash = 17;
                hash = hash * 31 + MessageType.GetHashCode();
                hash = hash * 31 + SubscriberID.GetHashCode();
                return hash;
            }
        }
        
        public bool Equals(RoutingTableItem other)
        {
            return other != null && (HashValue == other.HashValue);
        }
    }
}

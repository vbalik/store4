﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Messages
{
    internal class RoutingTable
    {
        private readonly List<RoutingTableItem> _items = new List<RoutingTableItem>();
        private readonly Dictionary<Type, List<RoutingTableItem>> _itemsByType = new Dictionary<Type, List<RoutingTableItem>>();


        internal bool Contains(RoutingTableItem rti)
        {
            return _items.Contains(rti);
        }


        internal void Add(RoutingTableItem rti)
        {
            _items.Add(rti);
            if (_itemsByType.ContainsKey(rti.MessageType))
                _itemsByType[rti.MessageType].Add(rti);
            else
                _itemsByType.Add(rti.MessageType, new List<RoutingTableItem>() { rti });
        }


        internal List<RoutingTableItem> Subscribers(Type type)
        {
            if (_itemsByType.ContainsKey(type))
                return _itemsByType[type];
            
            return new List<RoutingTableItem>();
        }
    }
}

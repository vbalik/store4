﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace S4mp.DAL
{
    public interface ILocalDatabase
    {
        Task<List<T>> GetAsync<T>() where T : new();

        Task<List<T>> Query<T>(string query, params dynamic[] parameters) where T : new();

        Task<T> FindAsync<T>(object pk) where T : new();

        Task<int> SaveAsync<T>(T item) where T : new();

        Task<int> DeleteAsync<T>(T item) where T : new();

        Task<int> Execute(string cmd, params object[] args);
    }
}

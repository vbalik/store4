﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Linq;
using System.IO;
using SQLite;

using S4mp.Entities;

namespace S4mp.DAL
{
    public class LocalDatabase : ILocalDatabase
    {
        readonly SQLiteAsyncConnection _database;

        public LocalDatabase()
        {
            var dbPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "s4mp.db3");
            _database = new SQLiteAsyncConnection(dbPath);

            // create tables
            _database.CreateTableAsync<DispCheck>().Wait();
            _database.CreateTableAsync<DispCheckSerial>().Wait();
            _database.CreateTableAsync<ItemInfo>().Wait();
            
            _database.DropTableAsync<CustomEventLoggerLog>().Wait();
            _database.CreateTableAsync<CustomEventLoggerLog>().Wait();

            StartupActions().Wait();
        }

        public Task<List<T>> GetAsync<T>() where T : new()
        {
            return _database.Table<T>().ToListAsync();
        }

        public Task<List<T>> Query<T>(string query, params dynamic[] parameters) where T : new()
        {
            return _database.QueryAsync<T>(query, parameters);
        }

        public Task<T> FindAsync<T>(object pk) where T : new()
        {
            return _database.FindAsync<T>(pk);
        }

        public Task<int> SaveAsync<T>(T item) where T : new()
        {
            return Task.Run<int>(async () =>
            {
                var primaryKeyPropertyForT = typeof(T).GetProperties().Where(p => p.IsDefined(typeof(SQLite.PrimaryKeyAttribute))).FirstOrDefault();
                
                //if primary key is int
                if (int.TryParse(primaryKeyPropertyForT.GetValue(item)?.ToString(), out int value))
                {
                    if (value != 0)
                        return await _database.UpdateAsync(item);
                    else
                        return await _database.InsertAsync(item); // autoincrement
                }

                //try to get existing item by key
                var existingItem = primaryKeyPropertyForT == null ? default(T) : await FindAsync<T>(primaryKeyPropertyForT.GetValue(item));
                //insert or update
                return existingItem == null ? await _database.InsertAsync(item) : await _database.UpdateAsync(item);
            });
        }

        public Task<int> DeleteAsync<T>(T item) where T : new()
        {
            return _database.DeleteAsync(item);
        }

        public Task<int> Execute(string cmd, params object[] args)
        {
            return _database.ExecuteAsync(cmd, args);
        }

        /// <summary>
        /// operations on db openning
        /// </summary>
        /// <returns></returns>
        private Task StartupActions()
        {
            return Task.Run(async () =>
            {
                var weekBefore = DateTime.Now.AddDays(-7);
                await Execute("delete from DispCheck where Updated < @0", weekBefore);
                await Execute("delete from DispCheckSerial where Updated < @0", weekBefore);
            });
        }
    }
}

﻿#define USE_RAMDISK

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace TestHelpers
{
    public class DatabaseFixture : IDisposable
    {
        public string ConnectionString { get; private set; }
        private LocalDb _localDB = null;

        private static int COUNTER = 0;


        public DatabaseFixture()
        {
            var testConnectionString = Environment.GetEnvironmentVariable("S4_TEST_LOCALDB_CN");
            if (testConnectionString != null)
            {
                ConnectionString = testConnectionString;
            }
            else
            {
#if USE_RAMDISK
                // check ramdisk drive
                string drive = Path.GetPathRoot(@"R:\");
                if (!Directory.Exists(drive))
                    InitRamdisk();
                _localDB = new LocalDb(location: @"R:\", version: LocalDb.Versions.V11, databaseSuffixGenerator: DBNameGenerator);
#else
            _localDB = new LocalDb(version: LocalDb.Versions.V11);
#endif
                ConnectionString = _localDB.ConnectionString;
                InitDB();
            }
        }


        private string DBNameGenerator()
        {
            return Guid.NewGuid().ToString("N") + "_" + System.Threading.Interlocked.Increment(ref COUNTER).ToString("0000");
        }


        public void Dispose()
        {
            if (_localDB != null)
                _localDB.Dispose();
        }


        private void InitRamdisk()
        {
            var startInfo = new ProcessStartInfo();
            startInfo.RedirectStandardError = true;
            startInfo.UseShellExecute = false;
            startInfo.FileName = @"imdisk";
            startInfo.Arguments = @"-a -s 1G -m R: -p ""/fs:ntfs /q /y""";
            var proc = Process.Start(startInfo);
            string stderrx = proc.StandardError.ReadToEnd();
            proc.WaitForExit();

            if (!String.IsNullOrWhiteSpace(stderrx))
                throw new Exception(stderrx);
        }


        private void InitDB()
        {
            //sqlpackage "/Action:publish" "/SourceFile:MyDatabase.dacpac" "/TargetConnectionString:Server=(localdb)\mssqllocaldb;Database=MyDatabase;Integrated Security=true"

            var sqlPackagePathParam = Environment.GetEnvironmentVariable("S4_SQLPACKAGE_PATH");

            string projectPath = Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), @"..\..\..\.."));
            string dacpacPath = Path.GetFullPath(Path.Combine(projectPath, "S4.DB/bin/Debug/S4.DB.dacpac"));
            var startInfo = new ProcessStartInfo();
            startInfo.RedirectStandardError = true;
            startInfo.UseShellExecute = false;
            startInfo.FileName = sqlPackagePathParam ?? @"C:\Program Files (x86)\Microsoft SQL Server\140\DAC\bin\SqlPackage.exe";
            startInfo.Arguments = $@"""/Action:publish"" ""/p:VerifyCollationCompatibility=false"" ""/SourceFile:{dacpacPath}"" ""/TargetConnectionString:{ConnectionString}"" /v:ProjectDir=""{projectPath}""";
            var proc = Process.Start(startInfo);
            string stderrx = proc.StandardError.ReadToEnd();
            proc.WaitForExit();

            if (!String.IsNullOrWhiteSpace(stderrx))
                throw new Exception(stderrx);
        }
    }
}

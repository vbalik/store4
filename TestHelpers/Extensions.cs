﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestHelpers
{
    public static class Extensions
    {
        public static bool PublicPropertiesEquals(Type type, object self, object to, params string[] ignore)
        {
            if (self != null && to != null)
            {
                List<string> ignoreList = new List<string>(ignore);
                foreach (System.Reflection.PropertyInfo pi in type.GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance))
                {
                    if (!ignoreList.Contains(pi.Name))
                    {
                        object selfValue = type.GetProperty(pi.Name).GetValue(self, null);
                        object toValue = type.GetProperty(pi.Name).GetValue(to, null);

                        if(pi.PropertyType == typeof(System.Byte[]))
                        {
                            if(!((Byte[])selfValue).SequenceEqual((Byte[])toValue))
                                return false;
                        }
                        else if (selfValue != toValue && (selfValue == null || !selfValue.Equals(toValue)))
                        {
                            return false;
                        }
                    }
                }
                return true;
            }
            return self == to;
        }


        
    }
}

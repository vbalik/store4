﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using S4.Core;
using S4.Core.Extensions;


namespace S4.Core_Test
{
    public class ValuesCopyExtensionsTests
    {

        [Fact]
        public void ToS3()
        {
            var c1 = new C1() { X1 = 123, Text = "abc", B = 128 };
            var src = new List<C1>() { c1 };

            var dest = src.ToS3<C1, C2>((s, d) => d.BB = s.B );

            Assert.NotNull(dest);
            Assert.NotEmpty(dest);
            Assert.Equal(c1.X1, dest[0].x1);
            Assert.Equal(c1.Text, dest[0].text);
            Assert.Equal(c1.B, dest[0].BB);
        }


        [Fact]
        public void FieldsFromProperties()
        {

            var c1 = new C1() { X1 = 123, Text = "abc" };
            var c2 = new C2();

            c2.FieldsFromProperties(c1);

            Assert.Equal(c1.X1, c2.x1);
            Assert.Equal(c1.Text, c2.text);
        }
    }


    class C1
    {
        public int X1 { get; set; }
        public string Text { get; set; }
        public byte B { get; set; }
    }


    class C2
    {
#pragma warning disable 0649
        public int x1;
        public string text;
#pragma warning restore 0649
        public byte BB;
    }
}

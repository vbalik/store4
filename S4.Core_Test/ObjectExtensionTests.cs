﻿using S4.Core.Extensions;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace S4.Core_Test
{
    public class ObjectExtensionTests
    {
        [Fact]
        public void IsDefaultTest()
        {
            int integer = 0;
            Assert.True(integer.IsDefault());

            integer = -1;
            Assert.False(integer.IsDefault());

            integer = 1;
            Assert.False(integer.IsDefault());

            Assert.True(((DummyClass)null).IsDefault());

            var someClass = new DummyClass(1);
            Assert.False(someClass.IsDefault());
        }

        [Fact]
        public void PublicPropertiesEquals()
        {
            var someClass1 = new DummyClass2() { a = 1 };
            var someClass2 = new DummyClass2() { a = 2 };
            Assert.False(someClass1.PublicPropertiesEquals<DummyClass2>(someClass2));

            var someClass3 = new DummyClass2() { a = 1 };
            var someClass4 = new DummyClass2() { a = 1 };
            Assert.True(someClass3.PublicPropertiesEquals<DummyClass2>(someClass4));
        }

        private class DummyClass
        {
#pragma warning disable IDE0052 // Remove unread private members
            private readonly int _id;
#pragma warning restore IDE0052 // Remove unread private members

            public DummyClass(int id)
            {
                _id = id;
            }
        }

        private class DummyClass2
        {
           public int a { get; set; }
        }

    }
}

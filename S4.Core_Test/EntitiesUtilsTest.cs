﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Xunit;

namespace S4.Core_Test
{
    public class EntitiesUtilsTest
    {

        [Required]
        [MaxLength(1)]
        public string Col1 { get; set; }

        [Required]
        [MaxLength(2)]
        public string Col2 { get; set; }

        [Required]
        [MaxLength(3)]
        public string Col3 { get; set; }

        [Required]
        public int Col4 { get; set; }

        [Fact]
        public void RepairModelValues1()
        {
            var model = new EntitiesUtilsTest() { Col1 = "12", Col2 = "123", Col3 = "1234", Col4 = 123 };
            Core.Utils.EntitiesUtils.RepairMaxLengthModelValues(model);
            
            Assert.Equal("1", model.Col1);
            Assert.Equal("12", model.Col2);
            Assert.Equal("123", model.Col3);
            Assert.Equal(123, model.Col4);
        }

        [Fact]
        public void RepairModelValues2()
        {
            var model = new EntitiesUtilsTest() { Col1 = "1", Col2 = "12", Col3 = "123" };
            Core.Utils.EntitiesUtils.RepairMaxLengthModelValues(model);

            Assert.Equal("1", model.Col1);
            Assert.Equal("12", model.Col2);
            Assert.Equal("123", model.Col3);
        }

        [Fact]
        public void RepairModelValues3()
        {
            var model = new EntitiesUtilsTest() { Col1 = "", Col2 = null };
            Core.Utils.EntitiesUtils.RepairMaxLengthModelValues(model);

            Assert.Equal("", model.Col1);
            Assert.Null(model.Col2);
        }

        [Fact]
        public void RepairModelValues4()
        {
            var model = new EntitiesUtilsTest() { Col1 = "12", Col2 = "123", Col3 = "1234", Col4 = 123 };
            Core.Utils.EntitiesUtils.RepairMaxLengthModelValues(model, "Col1", "Col2");

            Assert.Equal("12", model.Col1);
            Assert.Equal("123", model.Col2);
            Assert.Equal("123", model.Col3);
            Assert.Equal(123, model.Col4);
        }

    }
}

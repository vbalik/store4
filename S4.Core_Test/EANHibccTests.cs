﻿using S4.Core.EAN;
using S4.Core.HIBCC;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace S4.Core_Test
{
    public class EANHibccTests
    {
        [Fact]
        public void SinglePrimary()
        {
            var code = "+A123AWWW322200";

            var result = new HIBCC(code);

            Assert.NotNull(result.Parts);

            Assert.Equal(2, result.Parts.Count);

            Assert.Equal(IdentifierEnum.ProductNumber, result.Parts[0].EANCode);
            Assert.Equal("AWWW3222", result.Parts[0].Value);

            Assert.Equal(IdentifierEnum.UnitOfMeasure, result.Parts[1].EANCode);
            Assert.Equal("0", result.Parts[1].Value);
        }

        [Fact]
        public void SingleSecondary()
        {
            var code = "+$$3170131A4452Z20P";

            var result = new HIBCC(code);

            Assert.NotNull(result.Parts);

            Assert.Equal(2, result.Parts.Count);

            Assert.Equal(IdentifierEnum.ExpirationDate, result.Parts[0].EANCode);
            Assert.Equal("170131", result.Parts[0].Value);
            Assert.Equal(new DateTime(2017, 1, 31), result.Parts[0].ValueDate);

            Assert.Equal(IdentifierEnum.Lot, result.Parts[1].EANCode);
            Assert.Equal("A4452Z2", result.Parts[1].Value);
        }

        [Fact]
        public void MultiplePrimarySecondary()
        {
            var code = "+A123AWWW32220/$$3170131A4452Z2O";

            var result = new HIBCC(code);

            Assert.Equal(4, result.Parts.Count);

            Assert.Equal(IdentifierEnum.ProductNumber, result.Parts[0].EANCode);
            Assert.Equal("AWWW3222", result.Parts[0].Value);

            Assert.Equal(IdentifierEnum.UnitOfMeasure, result.Parts[1].EANCode);
            Assert.Equal("0", result.Parts[1].Value);

            Assert.Equal(IdentifierEnum.ExpirationDate, result.Parts[2].EANCode);
            Assert.Equal("170131", result.Parts[2].Value);
            Assert.Equal(new DateTime(2017, 1, 31), result.Parts[2].ValueDate);

            Assert.Equal(IdentifierEnum.Lot, result.Parts[3].EANCode);
            Assert.Equal("A4452Z2", result.Parts[3].Value);
        }

        [Fact]
        public void RealCodeExample()
        {
            var code = "+G2021401694140/$$7CYA130623/16D20230613/14D20260612B";

            var result = new HIBCC(code);

            Assert.Equal(5, result.Parts.Count);

            Assert.Equal(IdentifierEnum.ProductNumber, result.Parts[0].EANCode);
            Assert.Equal("140169414", result.Parts[0].Value);

            Assert.Equal(IdentifierEnum.UnitOfMeasure, result.Parts[1].EANCode);
            Assert.Equal("0", result.Parts[1].Value);

            Assert.Equal(IdentifierEnum.Lot, result.Parts[2].EANCode);
            Assert.Equal("CYA130623", result.Parts[2].Value);

            Assert.Equal(IdentifierEnum.ManufacturingDate, result.Parts[3].EANCode);
            Assert.Equal("20230613", result.Parts[3].Value);
            Assert.Equal(new DateTime(2023, 6, 13), result.Parts[3].ValueDate);

            Assert.Equal(IdentifierEnum.ExpirationDate, result.Parts[4].EANCode);
            Assert.Equal("20260612", result.Parts[4].Value);
            Assert.Equal(new DateTime(2026, 6, 12), result.Parts[4].ValueDate);
        }

        [Theory]
        [InlineData("+$$4150928223C001L%")]
        [InlineData("+$$20928153C001L%")]
        [InlineData("+$$31509283C001L%")]
        [InlineData("+A99912345/$$320010510X3")]
        [InlineData("+952713C001LG")]
        [InlineData("+$$4950928223C001LP")]
        [InlineData("+H217J358G2Z")]
        [InlineData("+A123ABCDEFGHI1234567891/$$420020216LOT123456789012345/SXYZ456789012345678/16D20130202$")]
        [InlineData("+J123AQ3451/$$3231231BC34567$4012R")]
        [InlineData("+H123ABC01234567890D")]
        public void RandomCodesExample(string rawCode)
        {
            var result = new HIBCC(rawCode);

            Assert.NotNull(result);
            Assert.True(result.Parts.Count > 1);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void BadNullCodesExample(string rawCode)
        {
            Assert.Throws<ArgumentNullException>(() => new HIBCC(rawCode));
        }

        [Theory]
        [InlineData("+H12")]
        [InlineData("+$$2x28153C001L%")]
        [InlineData("+A123ABCDEFGHI1234567891$4200216LOT12345678901234")]
        [InlineData("+A99912345/$$00000010X3")]
        [InlineData("+A99912345/$$20016610X3")]
        [InlineData("+A123ABCDEFGHI1234567891/$$420020216LOT123456789012345XXXX/SXYZ456789012345678/16D20130202$")]
        public void BadCodesExample(string rawCode)
        {
            Assert.Throws<ArgumentException>(() => new HIBCC(rawCode));
        }

        [Fact]
        public void TestRealExampleCode()
        {
            var ean = new HIBCC("+G2021401696132/$$7CYA130623/16D20230613/14D20260612/Q20$");
            Assert.Equal(6, ean.Parts.Count);
        }
    }
}

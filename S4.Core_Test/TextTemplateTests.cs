﻿using S4.Core.Utils;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;


namespace S4.Core_Test
{
    public class TextTemplateTests
    {
        /*
A10,30,0,3,2,2,N,"{1.Delivery_city::0,16}"
A10,103,0,3,1,1,N,"{1.Partner_partnerName::0,30}"
A10,137,0,3,1,1,N,"{0::0,30}"
A10,190,0,4,1,1,N,"{1.docInfo}"
*/


        [Fact]
        public void TextTemplate_Basic()
        {
            string template = "{0} {1}";

            var tt = new TextTemplate(template);
            var render = tt.Render(TextTemplate.FieldSource.Property, "AAAAA", "BBBBB");

            Assert.Equal("AAAAA BBBBB", render);
        }


        [Fact]
        public void TextTemplate_Trim()
        {
            string template = "{0::0,3}_{0::3,3}_{1.Delivery_city::0,16}";
            var templData = new TempData() { Delivery_city = "012345678901234567890123456789" };

            var tt = new TextTemplate(template);
            var render = tt.Render(TextTemplate.FieldSource.Property, "0123456789", templData);

            Assert.Equal("012_345_0123456789012345", render);
        }


        [Fact]
        public void TextTemplate_Format()
        {
            string template = "{0:0.00:}_{1.Prop2:0000}";
            var templData = new TempData() { Prop2 = 26 };

            var tt = new TextTemplate(template);
            var render = tt.Render(TextTemplate.FieldSource.Property, 12.5, templData);

            Assert.Equal("12.50_0026", render);
        }


        [Fact]
        public void TextTemplate_Path()
        {
            string template = "{0.SubItem.Delivery_city}";
            var templData = new TempData() { SubItem = new TempData() { Delivery_city = "AAAAA" } };

            var tt = new TextTemplate(template);
            var render = tt.Render(TextTemplate.FieldSource.Property, templData);

            Assert.Equal("AAAAA", render);
        }

        [Fact]
        public void TextTemplate_ReplaceUnPrintableCharacter()
        {
            string template = "{0} {1}";

            var tt = new TextTemplate(template);
            var render = tt.Render(TextTemplate.FieldSource.Property, S4.Core.Utils.StrUtils.ReplaceUnPrintableCharacter, @"""AAAAA""", "BBBBB");

            Assert.Equal("AAAAA BBBBB", render);
        }

        [Fact]
        public void TextTemplate_ReplaceNonAlphaNumericCharacter()
        {
            string template = "abcd 1234";
            var data = S4.Core.Utils.StrUtils.ReplaceNonAlphaNumericCharacter(template);
            Assert.Equal("abcd1234", data);

            template = "abcd1234";
            data = S4.Core.Utils.StrUtils.ReplaceNonAlphaNumericCharacter(template);
            Assert.Equal("abcd1234", data);

            template = "1234";
            data = S4.Core.Utils.StrUtils.ReplaceNonAlphaNumericCharacter(template);
            Assert.Equal("1234", data);

            template = " ";
            data = S4.Core.Utils.StrUtils.ReplaceNonAlphaNumericCharacter(template);
            Assert.Equal(" ", data);

            template = ",.-(])a";
            data = S4.Core.Utils.StrUtils.ReplaceNonAlphaNumericCharacter(template);
            Assert.Equal("a", data);
        }

        [Fact]
        public void TextTemplate_ReplaceNonCSVCharacter()
        {
            string template = "; ";
            var data = S4.Core.Utils.StrUtils.ReplaceNonCSVCharacter(template);
            Assert.Equal("", data);

            template = "1234567 abcd ;";
            data = S4.Core.Utils.StrUtils.ReplaceNonCSVCharacter(template);
            Assert.Equal("1234567abcd", data);
        }
    }


    #region helper class

    class TempData
    {
        public string Delivery_city { get; set; }
        public int Prop2 { get; set; }
        public TempData SubItem { get; set; }
    }

    #endregion
}

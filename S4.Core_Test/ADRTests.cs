﻿using S4.Core.ADR;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace S4.Core_Test
{
    public class ADRTests
    {
        [Fact]
        public void SafetyPointCalculate()
        {
            var x1 = ADR.SafetyPointCalculate(1, 10, 0.5f);
            Assert.Equal(250, x1);
            var x2 = ADR.SafetyPointCalculate(2, 10, 1);
            Assert.Equal(30, x2);
            var x3 = ADR.SafetyPointCalculate(3, 10, 1);
            Assert.Equal(10, x3);
            var x4 = ADR.SafetyPointCalculate(4, 10, 1);
            Assert.Equal(0, x4);

            Assert.Throws<Exception>(() => ADR.SafetyPointCalculate(5, 10, 1));
        }

        [Fact]
        public void SafetyPointCalculateBulk()
        {
            var c = new List<Tuple<int, int, float>>();
            c.Add(new Tuple<int, int, float>(1, 10, 1));
            c.Add(new Tuple<int, int, float>(2, 10, 1));
            c.Add(new Tuple<int, int, float>(3, 10, 1));
            c.Add(new Tuple<int, int, float>(4, 10, 1));
            var x1 = ADR.SafetyPointCalculateBulk(c);
            Assert.Equal(540, x1);
        }
    }
}

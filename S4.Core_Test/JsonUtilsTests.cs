﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using S4.Core.EAN;
using S4.Core.Utils;
using Newtonsoft.Json;

namespace S4.Core_Test
{
    public class JsonUtilsTest
    {

        [Fact]
        public void ToJsonStringTest()
        {
            
            var json1 = JsonUtils.ToJsonString("Test", "Test1", "Test2", 123);
            var val = JsonConvert.SerializeObject(new { Test = "Test1", Test2 = 123 });

            Assert.Equal(json1, val);

            var json2 = JsonUtils.ToJsonString("Test", "Test1", "Test2", 123, "Empty");
            var val2 = JsonConvert.SerializeObject(new { Test = "Test1", Test2 = 123, Empty = "" });

            Assert.Equal(json2, val2);


        }


    }
        
}

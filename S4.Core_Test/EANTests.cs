﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using S4.Core.EAN;
using System.Text.RegularExpressions;

namespace S4.Core_Test
{
    public class EANTests
    {
        [Fact]
        public void EAN_Exception()
        {
            Assert.Throws<ArgumentNullException>(() => new EAN(null));
        }

        [Fact]
        public void EAN_Code()
        {
            var bc = "08054083001920";
            var sn = "102343138955";
            var bn = "6079652";
            var dt = new DateTime(2020, 3, 31);

            var e1 = new EAN();
            e1.Parts = new List<EANPart> {
                { new EANPart { EANCode = EANCodeEnum.ShippingContainerCode, Value = bc } },
                { new EANPart { EANCode = EANCodeEnum.SerialNumber, Value = sn } },
                { new EANPart { EANCode = EANCodeEnum.BatchNumber, Value = bn } },
                { new EANPart { EANCode = EANCodeEnum.ExpirationDate, Value = dt.Date.ToString("yyMMdd") } }
            };

            var b = e1.GetEANCode(out string ean, out string error);
            Assert.True(b);
            Assert.True(string.IsNullOrEmpty(error));

            var e2 = new EAN(ean);
            Assert.Equal("8054083001920", e2[EANCodeEnum.ShippingContainerCode].Value);
            Assert.Equal(sn, e2[EANCodeEnum.SerialNumber].Value);
            Assert.Equal(bn, e2[EANCodeEnum.BatchNumber].Value);
            Assert.Equal(dt.Date.ToString("yyMMdd"), e2[EANCodeEnum.ExpirationDate].Value);
        }


        [Theory]
        [InlineData("014046241")]
        [InlineData("14046241")]
        [InlineData("0140462412345")]
        [InlineData("140462412345")]
        [InlineData("140462412345\n\t\r")]
        public void EAN_OnePart(string barCode)
        {
            var e1 = new EAN(barCode);
            Assert.Single(e1.Parts);

            // always without zero at start
            var barCodeOrig = barCode;
            if (barCode.StartsWith("0"))
                barCodeOrig = barCode.Substring(1, barCode.Length - 1);

            barCodeOrig = Regex.Replace(barCodeOrig, @"\t|\n|\r", string.Empty);
            Assert.Equal(barCodeOrig, e1[EANCodeEnum.ShippingContainerCode].Value);
        }


        [Fact]
        public void EAN_MultipleParts()
        {
            var e1 = new EAN("010805408300192021102343138955\u001d106079652\u001d17200331");
            Assert.Equal(4, e1.Parts.Count);
            Assert.Equal("8054083001920", e1[EANCodeEnum.ShippingContainerCode].Value);
            Assert.Equal("102343138955", e1[EANCodeEnum.SerialNumber].Value);
            Assert.Equal("6079652", e1[EANCodeEnum.BatchNumber].Value);
            Assert.Equal("200331", e1[EANCodeEnum.ExpirationDate].Value);

            var e2 = new EAN("]C101059447060009862191020938534673[GS]10JE0107[GS]17230731");
            Assert.Equal(4, e2.Parts.Count);
            Assert.Equal("5944706000986", e2[EANCodeEnum.ShippingContainerCode].Value);
            Assert.Equal("91020938534673", e2[EANCodeEnum.SerialNumber].Value);
            Assert.Equal("JE0107", e2[EANCodeEnum.BatchNumber].Value);
            Assert.Equal("230731", e2[EANCodeEnum.ExpirationDate].Value);

            var e3 = new EAN("01040225366458722159217208135[GS]1723060910498318");
            Assert.Equal(4, e3.Parts.Count);
            Assert.Equal("4022536645872", e3[EANCodeEnum.ShippingContainerCode].Value);
            Assert.Equal("59217208135", e3[EANCodeEnum.SerialNumber].Value);
            Assert.Equal("498318", e3[EANCodeEnum.BatchNumber].Value);
            Assert.Equal("230609", e3[EANCodeEnum.ExpirationDate].Value);

            var e4 = new EAN("011383895702826610AHB1L76[GS]1719113021WYS6664LFLUSW");
            Assert.Equal(4, e4.Parts.Count);
            Assert.Equal("13838957028266", e4[EANCodeEnum.ShippingContainerCode].Value);
            Assert.Equal("WYS6664LFLUSW", e4[EANCodeEnum.SerialNumber].Value);
            Assert.Equal("AHB1L76", e4[EANCodeEnum.BatchNumber].Value);
            Assert.Equal("191130", e4[EANCodeEnum.ExpirationDate].Value);

            var e5 = new EAN("]C10108054083001920[GS]1000442576[GS]17270718[GS]300034[GS]240GOW.4140");
            Assert.Equal(5, e5.Parts.Count);
            Assert.Equal("8054083001920", e5[EANCodeEnum.ShippingContainerCode].Value);
            Assert.Equal("00442576", e5[EANCodeEnum.BatchNumber].Value);
            Assert.Equal("270718", e5[EANCodeEnum.ExpirationDate].Value);
            Assert.Equal("0034", e5[EANCodeEnum.QuantityEach].Value);
            Assert.Equal("GOW.4140", e5[EANCodeEnum.AdditionalProductID].Value);

            var e6 = new EAN("]C11000442576[GS]17270718[GS]300034[GS]240GOW.4140");
            Assert.Equal(4, e6.Parts.Count);
            Assert.Equal("00442576", e6[EANCodeEnum.BatchNumber].Value);
            Assert.Equal("270718", e6[EANCodeEnum.ExpirationDate].Value);
            Assert.Equal("0034", e6[EANCodeEnum.QuantityEach].Value);
            Assert.Equal("GOW.4140", e6[EANCodeEnum.ShippingContainerCode].Value);
        }

        [Fact]
        public void EAN_GAMA()
        {
            var e1 = new EAN("01859354040192021102343138955[GS]106079652[GS]17200331");
            Assert.Equal(4, e1.Parts.Count);
            Assert.Equal("8593540401920", e1[EANCodeEnum.ShippingContainerCode].Value);
            Assert.Equal("102343138955", e1[EANCodeEnum.SerialNumber].Value);
            Assert.Equal("6079652", e1[EANCodeEnum.BatchNumber].Value);
            Assert.Equal("200331", e1[EANCodeEnum.ExpirationDate].Value);
        }


        [Fact]
        public void EAN_Fresenius()
        {
            var e1 = new EAN("01404624100192021102343138955[GS]106079652[GS]17200331");
            Assert.Equal(4, e1.Parts.Count);
            Assert.Equal("4046241001920", e1[EANCodeEnum.ShippingContainerCode].Value);
            Assert.Equal("102343138955", e1[EANCodeEnum.SerialNumber].Value);
            Assert.Equal("6079652", e1[EANCodeEnum.BatchNumber].Value);
            Assert.Equal("200331", e1[EANCodeEnum.ExpirationDate].Value);
        }

        [Fact]
        public void EAN_Ecolab_Anios()
        {
            var e1 = new EAN("(01)03597610612978(17)5-1020(10)G29902S");
            Assert.Equal(3, e1.Parts.Count);
            Assert.Equal("3597610612978", e1[EANCodeEnum.ShippingContainerCode].Value);
            Assert.Equal("G29902S", e1[EANCodeEnum.BatchNumber].Value);
            Assert.Equal("311025", e1[EANCodeEnum.ExpirationDate].Value);

            var e2 = new EAN("(01)03597610612978(17)202611(10)G29902S");
            Assert.Equal(3, e2.Parts.Count);
            Assert.Equal("3597610612978", e2[EANCodeEnum.ShippingContainerCode].Value);
            Assert.Equal("G29902S", e2[EANCodeEnum.BatchNumber].Value);
            Assert.Equal("301126", e2[EANCodeEnum.ExpirationDate].Value);

            var e3 = new EAN("(01)03597610612978(17)202601(10)G29902S");
            Assert.Equal(3, e3.Parts.Count);
            Assert.Equal("3597610612978", e3[EANCodeEnum.ShippingContainerCode].Value);
            Assert.Equal("G29902S", e3[EANCodeEnum.BatchNumber].Value);
            Assert.Equal("310126", e3[EANCodeEnum.ExpirationDate].Value);

            var e4 = new EAN("(01)03597610612978(17)052026(10)G29902S");
            Assert.Equal(3, e4.Parts.Count);
            Assert.Equal("3597610612978", e4[EANCodeEnum.ShippingContainerCode].Value);
            Assert.Equal("G29902S", e4[EANCodeEnum.BatchNumber].Value);
            Assert.Equal("310526", e4[EANCodeEnum.ExpirationDate].Value);
        }

        [Fact]
        public void EAN_Batist()
        {
            var e1 = new EAN("0108591454006004172405001019000584");
            Assert.Equal(3, e1.Parts.Count);
            Assert.Equal("8591454006004", e1[EANCodeEnum.ShippingContainerCode].Value);
            Assert.Equal("240500", e1[EANCodeEnum.ExpirationDate].Value);
            Assert.Equal("19000584", e1[EANCodeEnum.BatchNumber].Value);

            var e2 = new EAN("0108591454006004172405001019000584[GS]21123456");
            Assert.Equal(4, e2.Parts.Count);
            Assert.Equal("8591454006004", e2[EANCodeEnum.ShippingContainerCode].Value);
            Assert.Equal("240500", e2[EANCodeEnum.ExpirationDate].Value);
            Assert.Equal("19000584", e2[EANCodeEnum.BatchNumber].Value);
            Assert.Equal("123456", e2[EANCodeEnum.SerialNumber].Value);

            var e3 = new EAN("0108591454006004[GS]17240500[GS]21123456");
            Assert.Equal(3, e3.Parts.Count);
            Assert.Equal("8591454006004", e3[EANCodeEnum.ShippingContainerCode].Value);
            Assert.Equal("240500", e3[EANCodeEnum.ExpirationDate].Value);
            Assert.Equal("123456", e3[EANCodeEnum.SerialNumber].Value);
        }

        [Fact]
        public void EAN_CellaVisionAB()
        {
            var e1 = new EAN("0107350040975725102308141[GS]17202608-17");
            Assert.Equal(3, e1.Parts.Count);
            Assert.Equal("7350040975725", e1[EANCodeEnum.ShippingContainerCode].Value);
            Assert.Equal("260817", e1[EANCodeEnum.ExpirationDate].Value);
            Assert.Equal("2308141", e1[EANCodeEnum.BatchNumber].Value);
        }

        [Fact]
        public void QR_CODE_Multiparts()
        {
            var e1 = new EAN("]d201035965400100522191993095571\u001d1725013110E204397");
            Assert.Equal(4, e1.Parts.Count);
            Assert.Equal("3596540010052", e1[EANCodeEnum.ShippingContainerCode].Value);
            Assert.Equal("250131", e1[EANCodeEnum.ExpirationDate].Value);
            Assert.Equal("E204397", e1[EANCodeEnum.BatchNumber].Value);
            Assert.Equal("91993095571", e1[EANCodeEnum.SerialNumber].Value);

            var e2 = new EAN("]d2010859518781302117220427109NM3D\u001d2110661Y1A2669W7");
            Assert.Equal(4, e2.Parts.Count);
            Assert.Equal("8595187813021", e2[EANCodeEnum.ShippingContainerCode].Value);
            Assert.Equal("220427", e2[EANCodeEnum.ExpirationDate].Value);
            Assert.Equal("9NM3D", e2[EANCodeEnum.BatchNumber].Value);
            Assert.Equal("10661Y1A2669W7", e2[EANCodeEnum.SerialNumber].Value);

            var e3 = new EAN("]d201034531200000111719112510ABCD1234");
            Assert.Equal(3, e3.Parts.Count);
            Assert.Equal("3453120000011", e3[EANCodeEnum.ShippingContainerCode].Value);
            Assert.Equal("191125", e3[EANCodeEnum.ExpirationDate].Value);
            Assert.Equal("ABCD1234", e3[EANCodeEnum.BatchNumber].Value);
        }

        [Theory]
        [InlineData("]d201034531200000111719112510ABCD1234")]
        [InlineData("]C101059447060009862191020938534673[GS]10JE0107[GS]17230731")]
        [InlineData("]d201035965400100522191993095571\u001d1725013110E204397")]
        [InlineData("011383895702826610AHB1L76[GS]1719113021WYS6664LFLUSW")]
        public void EAN_EANValidMultiParts(string barCode)
        {
            var e1 = new EAN(barCode);
            Assert.NotNull(e1);

            Assert.True(e1.Parts.Count > 1);
        }

        [Theory]
        [InlineData("014046241")]
        [InlineData("#014046241")]
        [InlineData("140462412345\n\t\r")]
        [InlineData("+014046241456")]
        [InlineData("+201034531200000111719112510ABCD1234")]
        [InlineData("H110034531200000111719112510ABCD1234")]
        [InlineData("#011383895702826610AHB1L76[GS]1719113021WYS6664LFLUSW")]
        public void EAN_SingleShippingCodeNotEAN(string barCode)
        {
            var e1 = new EAN(barCode);
            Assert.NotNull(e1);

            Assert.Single(e1.Parts);

            // always without zero at start
            var barCodeOrig = barCode;
            if (barCode.StartsWith("0"))
                barCodeOrig = barCode.Substring(1, barCode.Length - 1);

            barCodeOrig = Regex.Replace(barCodeOrig, @"\t|\n|\r", string.Empty);

            Assert.Equal(barCodeOrig, e1[EANCodeEnum.ShippingContainerCode].Value);
        }

        [Theory]
        [InlineData("(01)2694426290499811202303172028021020230310")]
        [InlineData("(01)26944262904998(11)202303(17)202802(10)20230310")]
        [InlineData("]d2(01)26944262904998(11)202303(17)202802(10)20230310")]
        public void EAN_DataMatrixWithValidCode(string barCode)
        {
            var e1 = new EAN(barCode);
            Assert.NotNull(e1);
            Assert.Equal(4, e1.Parts.Count);
        }


        [Theory]
        [InlineData("(01)04015630927425(10)80810601(17)250930(240)05531462190(11)240502")]
        [InlineData("(01)04015630927425(10)80810601\u001d(17)250930(240)05531462190(11)240502")]
        public void EAN_Roche(string barCode)
        {
            var e = new EAN(barCode);
            Assert.Equal("4015630927425", e.Parts[0].Value);
            Assert.Equal("80810601", e.Parts[1].Value);
            Assert.Equal("250930", e.Parts[2].Value);
            Assert.Equal("05531462190", e.Parts[3].Value);
            Assert.Equal("240502", e.Parts[4].Value);
        }
    }
}

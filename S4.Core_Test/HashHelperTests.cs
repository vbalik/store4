﻿using S4.Core.Utils;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace S4.Core_Test
{
    public class HashHelperTests
    {
        [Fact]
        public void GetHashCodeTest()
        {
            Assert.Equal(HashHelper.GetHashCode(1, 2), HashHelper.GetHashCode(1, 2));
            Assert.NotEqual(HashHelper.GetHashCode(1, 2), HashHelper.GetHashCode(2, 1));

            Assert.Equal(HashHelper.GetHashCode("X", "Y"), HashHelper.GetHashCode("X", "Y"));
            Assert.NotEqual(HashHelper.GetHashCode("X", "Y"), HashHelper.GetHashCode("Y", "X"));

            Assert.Equal(HashHelper.GetHashCode("X", null), HashHelper.GetHashCode("X", null));
            Assert.NotEqual(HashHelper.GetHashCode("X", null), HashHelper.GetHashCode("Y", "X"));


            Assert.Equal(HashHelper.GetHashCode("20062211571864577031", 1751484, (int?)null, 11454), HashHelper.GetHashCode("20062211571864577031", 1751484, (int?)null, 11454));
            //return Core.Utils.HashHelper.GetHashCode(CarrierNum, DirectionID, SrcPositionID, DestPositionID);
            //20062211571864577031; DirectionID : 1751484; SrcPositionID : ; DestPositionID : 11454
        }
    }
}

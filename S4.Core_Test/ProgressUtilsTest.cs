﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using S4.Core;
using S4.Core.Utils;

namespace S4.Core_Test
{
    public class ProgressUtilsTest
    {

        [Fact]
        public void GetProgressValueTest()
        {
            var progressUtils = new ProgressUtils();
            Assert.True(progressUtils.GetProgressValue(50, 100, 1) == 50);
            progressUtils.ResetLastStep();
            Assert.True(progressUtils.GetProgressValue(9000, 18000, 50) == 50);
            
            progressUtils.ResetLastStep();
            for (int i = 1; i < 10; i++)
            {
                Assert.True(progressUtils.GetProgressValue(i, 100, 10) == 0);
            }

            progressUtils.ResetLastStep();
            var fromNum = 10;
            var toNum = 20;
            for (int i = fromNum; i < toNum; i++)
            {
                if (i == fromNum)
                    Assert.True(progressUtils.GetProgressValue(i, 100, 10) == fromNum);
                else
                    Assert.True(progressUtils.GetProgressValue(i, 100, 10) == 0);
            }

            progressUtils.ResetLastStep();
            fromNum = 20;
            toNum = 30;
            for (int i = fromNum; i < toNum; i++)
            {
                if (i == fromNum)
                    Assert.True(progressUtils.GetProgressValue(i, 100, 10) == fromNum);
                else
                    Assert.True(progressUtils.GetProgressValue(i, 100, 10) == 0);
            }

            progressUtils.ResetLastStep();
            fromNum = 30;
            toNum = 40;
            for (int i = fromNum; i < toNum; i++)
            {
                if (i == fromNum)
                    Assert.True(progressUtils.GetProgressValue(i, 100, 10) == fromNum);
                else
                    Assert.True(progressUtils.GetProgressValue(i, 100, 10) == 0);
            }

            progressUtils.ResetLastStep();
            fromNum = 40;
            toNum = 50;
            for (int i = fromNum; i < toNum; i++)
            {
                if (i == fromNum)
                    Assert.True(progressUtils.GetProgressValue(i, 100, 10) == fromNum);
                else
                    Assert.True(progressUtils.GetProgressValue(i, 100, 10) == 0);
            }

            progressUtils.ResetLastStep();
            fromNum = 50;
            toNum = 60;
            for (int i = fromNum; i < toNum; i++)
            {
                if (i == fromNum)
                    Assert.True(progressUtils.GetProgressValue(i, 100, 10) == fromNum);
                else
                    Assert.True(progressUtils.GetProgressValue(i, 100, 10) == 0);
            }

            progressUtils.ResetLastStep();
            fromNum = 60;
            toNum = 70;
            for (int i = fromNum; i < toNum; i++)
            {
                if (i == fromNum)
                    Assert.True(progressUtils.GetProgressValue(i, 100, 10) == fromNum);
                else
                    Assert.True(progressUtils.GetProgressValue(i, 100, 10) == 0);
            }

            progressUtils.ResetLastStep();
            fromNum = 70;
            toNum = 80;
            for (int i = fromNum; i < toNum; i++)
            {
                if (i == fromNum)
                    Assert.True(progressUtils.GetProgressValue(i, 100, 10) == fromNum);
                else
                    Assert.True(progressUtils.GetProgressValue(i, 100, 10) == 0);
            }

            progressUtils.ResetLastStep();
            fromNum = 80;
            toNum = 90;
            for (int i = fromNum; i < toNum; i++)
            {
                if (i == fromNum)
                    Assert.True(progressUtils.GetProgressValue(i, 100, 10) == fromNum);
                else
                    Assert.True(progressUtils.GetProgressValue(i, 100, 10) == 0);
            }

            progressUtils.ResetLastStep();
            fromNum = 90;
            toNum = 100;
            for (int i = fromNum; i < toNum; i++)
            {
                if (i == fromNum)
                    Assert.True(progressUtils.GetProgressValue(i, 100, 10) == fromNum);
                else
                    Assert.True(progressUtils.GetProgressValue(i, 100, 10) == 0);
            }
                        
        }

    }



}


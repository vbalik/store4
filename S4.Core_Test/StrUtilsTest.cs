﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using S4.Core;
using S4.Core.Data;

namespace S4.Core_Test
{
    public class StrUtilsTest
    {

        [Fact]
        public void RandomDigitsTest()
        {

            var testVal = Core.Utils.StrUtils.RandomDigits(10);

            Assert.True(testVal != null);
            Assert.True(testVal.Length == 10);
            int n;

            for (int i = 0; i < testVal.Length; i++)
            {
                Assert.True(int.TryParse(testVal[i].ToString(), out n));
            }

        }

        [Fact]
        public void OnlyLettersTest()
        {

            var testVal = Core.Utils.StrUtils.OnlyLetters("123456789abcd987654321efghABC");

            Assert.True(testVal != null);
            bool result = System.Text.RegularExpressions.Regex.IsMatch(testVal, @"^[\p{L}\p{N}]+$");

        }

        [Fact]
        public void ToASCIITest()
        {

            var testVal = Core.Utils.StrUtils.ToASCII("Příliš žluťoučký kůň úpěl ďábelské kódy.");

            Assert.True(testVal != null);
            Assert.True(testVal == "Prilis zlutoucky kun upel dabelske kody.");


        }

        [Fact]
        public void ObjectToASCIITest()
        {
            var dataIn = new Test(){ ID = 1, Text = "Příliš žluťoučký kůň úpěl ďábelské kódy." };
            var dataOut = Core.Utils.StrUtils.ObjectToASCII(dataIn);

            Assert.True(dataOut != null);
            Assert.True(((Test)dataOut).Text == "Prilis zlutoucky kun upel dabelske kody.");


        }

        [Fact]
        public void StringExtensionTest()
        {
            for (int i = 1; i < 30; i++)
            {
                var text = "abcdefghijklmnopqrstuvwxyz";
                var result = text.GetSplitedString(i);
            }
        }
    }

    class Test
    {
        public int ID { get; set; }
        public string Text { get; set; }
    }
}

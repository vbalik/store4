﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using S4.Core;
using S4.Core.PropertiesComparer;


namespace S4.Core_Test
{
    public class PropertiesComparerTests
    {

        [Fact]
        public void TheSame()
        {
            var class1 = GetClass();
            var class2 = class1.Clone();
            var result = new PropertiesComparer<TestClass>(class1, class2).JsonText;
            Assert.Equal("{\"changes\":[]}", result);
        }

        [Fact]
        public void Bool()
        {
            var class1 = GetClass();
            var class2 = class1.Clone();
            class2.PBool = false;
            var result = new PropertiesComparer<TestClass>(class1, class2).JsonText;
            Assert.Equal("{\"changes\":[{\"property\":\"PBool\",\"originValue\":\"True\",\"currentValue\":\"False\"}]}", result);
        }

        [Fact]
        public void Int()
        {
            var class1 = GetClass();
            var class2 = class1.Clone();
            class2.PInt = class1.PInt + 1;
            var result = new PropertiesComparer<TestClass>(class1, class2).JsonText;
            var expected = $"{{\"changes\":[{{\"property\":\"PInt\",\"originValue\":\"{class1.PInt}\",\"currentValue\":\"{class2.PInt}\"}}]}}";
            Assert.Equal(expected, result);
        }

        [Fact]
        public void Double()
        {
            var class1 = GetClass();
            var class2 = class1.Clone();
            class2.PDouble = class1.PDouble / 3;
            var result = new PropertiesComparer<TestClass>(class1, class2).JsonText;
            var expected = FormattableString.Invariant($"{{\"changes\":[{{\"property\":\"PDouble\",\"originValue\":\"{class1.PDouble}\",\"currentValue\":\"{class2.PDouble}\"}}]}}");
            Assert.Equal(expected, result);
        }

        [Fact]
        public void Date()
        {
            var class1 = GetClass();
            var class2 = class1.Clone();
            class2.PDate = DateTime.Now.AddDays(66);
            var result = new PropertiesComparer<TestClass>(class1, class2).JsonText;
            var expected = FormattableString.Invariant($"{{\"changes\":[{{\"property\":\"PDate\",\"originValue\":\"{class1.PDate}\",\"currentValue\":\"{class2.PDate}\"}}]}}");
            Assert.Equal(expected, result);
        }

        [Fact]
        public void String()
        {
            var class1 = GetClass();
            var class2 = class1.Clone();
            class2.PString = "Test";
            var result = new PropertiesComparer<TestClass>(class1, class2).JsonText;
            var expected = $"{{\"changes\":[{{\"property\":\"PString\",\"originValue\":\"{class1.PString}\",\"currentValue\":\"{class2.PString}\"}}]}}";
            Assert.Equal(expected, result);
        }


        [Fact]
        public void DoCompare_DifferentTypes()
        {
            var class1 = new TestClass() { PBool = true, PInt = 85 };
            var class2 = new TestClass2();
            var comparer = new PropertiesComparer<TestClass>();
            comparer.DoCompare<TestClass2>(class1, class2, "PBool", "PInt");
            var result = comparer.JsonText;
            var expected = "{\"changes\":[{\"property\":\"PBool\",\"originValue\":\"True\",\"currentValue\":\"False\"},{\"property\":\"PInt\",\"originValue\":\"85\",\"currentValue\":\"0\"}]}";
            Assert.Equal(expected, result);
        }

        [Fact]
        public void DoCompare_AnonymousTypesSame()
        {
            var class1 = new { PBool = true, PInt = 85 };
            var class2 = new { PBool = true, PInt = 85 };
            var comparer = new PropertiesComparer<object>();
            comparer.DoCompare<object>(class1, class2, "PBool", "PInt");
            Assert.Empty(comparer.PropertyValuesCompare.Items);
            Assert.Equal("{\"changes\":[]}", comparer.JsonText);
        }

        [Fact]
        public void DoCompare_AnonymousTypesDifferent()
        {
            var class1 = new { PBool = true, PInt = 85 };
            var class2 = new { PBool = false, PInt = 1 };
            var comparer = new PropertiesComparer<object>();
            comparer.DoCompare<object>(class1, class2, "PBool", "PInt");
            Assert.Equal(2, comparer.PropertyValuesCompare.Items.Count);

            var result = comparer.JsonText;
            var expected = "{\"changes\":[{\"property\":\"PBool\",\"originValue\":\"True\",\"currentValue\":\"False\"},{\"property\":\"PInt\",\"originValue\":\"85\",\"currentValue\":\"1\"}]}";
            Assert.Equal(expected, result);
        }


        private TestClass GetClass()
        {
            return new TestClass() { PBool = true, PDate = DateTime.Now, PDouble = (double)DateTime.Now.Ticks / DateTime.Now.Day, PInt = DateTime.Now.DayOfYear, PString = DateTime.Now.ToString() };
        }
    }


    class TestClass
    {
        public bool PBool { get; set; }
        public int PInt { get; set; }
        public double PDouble { get; set; }
        public DateTime PDate { get; set; }
        public string PString { get; set; }

        public TestClass Clone()
        {
            return new TestClass() { PBool = PBool, PDate = PDate, PDouble = PDouble, PInt = PInt, PString = PString };
        }
    }


    class TestClass2
    {
        public bool PBool { get; set; }
        public int PInt { get; set; }
    }
}

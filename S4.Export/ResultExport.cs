﻿using System;
using System.Collections.Generic;
using System.Text;
using static S4.Export.Export;

namespace S4.Export
{
    public class ResultExport
    {
        public ResultEnum ResultEnum { get; set; }
        public string Message { get; set; }
        public List<int> DoneDirs { get; set; }
        public ExportResult[] ExpResults { get; set; }

        public bool IsWarning { get; set; } = false;
        public string WarningMsg {  get; set; }
    }
}

using System;
using System.Text;
using System.Globalization;




namespace S4.Export
{
    public class ExportResult
    {
        private const char SPACE = ' ';


        public string fileName;

        private int _rowCount;
        public int RowCount
        {
            get { return _rowCount; }
        }

        private StringBuilder sb = new StringBuilder();
        private CultureInfo culture = null;
        private string separator = null;


        public ExportResult()
        {
        }

        public ExportResult(CultureInfo culture)
        {
            this.culture = culture;
        }



        public void Append(object value)
        {
            this.sb.Append(value);
            _addSep();
        }

        public void Append(string text)
        {
            this.sb.Append(text);
            _addSep();
        }

        public void Append(string format, object value)
        {
            Append(format, value, -1);
        }

        public void Append(string format, object value, int emptyLen)
        {
            if ((value == null || value == DBNull.Value) && emptyLen != -1)
                this.sb.Append(new string(' ', emptyLen));
            else
            {
                format = String.Format("{{0:{0}}}", format);
                this.sb.AppendFormat(format, value);
                _addSep();
            }
        }

        public void Append(int len, object value)
        {
            string format = String.Format("{{0,{0}}}", len);
            string text = String.Format(format, value);

            len = Math.Abs(len);
            if (text.Length > len)
                text = text.Substring(0, len);

            this.sb.Append(text);
            _addSep();
        }

        public void Append(decimal value, int len1, int len2)
        {
            if (value == Decimal.MinValue)
                this.sb.Append(new string(' ', len1));
            else
            {
                string format = "{0:0." + new string('0', len2) + "}";
                string text = String.Format(culture, format, value);

                if (text.Length > len1)
                    text = text.Substring(text.Length - len1, len1);
                else if (text.Length < len1)
                    text = new string(' ', len1 - text.Length) + text;

                this.sb.Append(text);
            }
            _addSep();
        }



        public void AppendSpace(int len)
        {
            this.sb.Append(new String(SPACE, len));
            _addSep();
        }

        public void AppendSpace()
        {
            this.sb.Append(SPACE);
            _addSep();
        }




        public void AppendNewLine()
        {
            _rowCount++;
            this.sb.Append("\r\n");
        }



        public string fileContent
        {
            get
            {
                return this.sb.ToString();
            }
        }



        public void SetSeparator(string separator)
        {
            this.separator = separator;
        }



        private void _addSep()
        {
            if (this.separator != null)
                sb.Append(this.separator);
        }
    }
}

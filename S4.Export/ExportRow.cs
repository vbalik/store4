using NPoco;
using S4.Entities;
using S4.Entities.Helpers.Xml.Dispatch;
using System;
using System.Data.SqlClient;






namespace S4.Export
{
	public class ExportRow : System.Collections.IComparer
	{
		public StoreMoveItem moveItem;
		public DispatchItemS3 xmlData;



		public ExportRow()
		{
		}

		public ExportRow(StoreMoveItem stItem, DispatchS3 xmlRows)
		{
			this.moveItem = stItem;

			// find xml data
			foreach(var xmlRow in xmlRows.Items)
				if(_compare(stItem, xmlRow))
				{
					this.xmlData = xmlRow;
					break;
				}
				
			if(this.xmlData == null)
				throw new Exception(String.Format("Pro balen� s id {0} se nepoda�ilo naj�t data v XML souboru", this.moveItem.StoMoveID));
                
		}

        
		private bool _compare(StoreMoveItem stItem, DispatchItemS3 xmlRow)
		{
			if(xmlRow.Department == null)
            {
                //find lot
                using (var db = Core.Data.ConnectionHelper.GetDB())
                {
                    var storeMoveLot = db.SingleOrDefaultById<StoreMoveLot>(stItem.StoMoveLotID);
                    return (xmlRow.ArticlePackingID == storeMoveLot.ArtPackID.ToString());
                }
            }
            else
				return((xmlRow.Position + 1) == this.moveItem.Parent_docPosition);
		}



		#region IComparer Members

		public int Compare(object x, object y)
		{
			int xPos = ((ExportRow)x).xmlData.Position;
			int yPos = ((ExportRow)y).xmlData.Position;

			if(xPos == yPos)
				return 0;
			else if(xPos > yPos)
				return 1;
			else
				return -1;
		}

		#endregion
        
	}
}

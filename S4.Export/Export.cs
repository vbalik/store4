﻿using Microsoft.Extensions.Configuration;
using NLog;
using NPoco;
using Oracle.ManagedDataAccess.Client;
using S4.DAL;
using S4.Entities;
using S4.Entities.Helpers.Xml.Dispatch;
using S4.Entities.Models;
using S4.Export.Settings;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;

namespace S4.Export
{
    public class Export
    {
        private static NLog.Logger _logger = LogManager.GetLogger("Export");

        #region public

        public enum ExportFormat
        {
            None,
            MEDICOLS,
            LEKIS5,
            LEKIS6,
            PAENIUM,
            MEDIOX,
            PENTENIUM,
            PDK5,
            PDK4,
            PDK8,
            PDKX,
            PDK12,
            PDK15,
            PDK18
        }

        public enum ResultEnum
        {
            NotNeeded, // OK
            NotReady, //budeme opakovat
            Sended, // OK
            Error, //budeme opakovat
            ErrorFinal //nebudeme opakovat
        }

        public static ResultExport MakeExport(SettingsExport settingsExport, DirectionModel direction, DispatchS3 dispatchS3, ExportFormat format, out Encoding encoding)
        {
            encoding = Encoding.Default;
            // check direction status
            if (direction.DocStatus == Direction.DR_STATUS_CANCEL)
                return new ResultExport() { ResultEnum = ResultEnum.ErrorFinal, Message = "Dodací list je stornovaný" };

            // get & parse source xml
            string xmlSource = GetSource(direction.History);
            if (String.IsNullOrWhiteSpace(xmlSource))
                return new ResultExport() { ResultEnum = ResultEnum.ErrorFinal, Message = $"XML zdroj je prázdný directionID: {direction.DirectionID}" };
                      
            return MakeExportInternal(dispatchS3, settingsExport, direction, format, true, null, out encoding);
        }

        public static ResultExport MakeExport(SettingsExport settingsExport, DirectionModel direction, DispatchS3 dispatchS3, NLog.Logger logger, out Encoding encoding)
        {
            encoding = Encoding.Default;
            // check direction status
            if (direction.DocStatus == Direction.DR_STATUS_CANCEL)
                return new ResultExport() { ResultEnum = ResultEnum.ErrorFinal, Message = "Dodací list je stornovaný" };

            // get & parse source xml
            string xmlSource = GetSource(direction.History);
            if (String.IsNullOrWhiteSpace(xmlSource))
                return new ResultExport() { ResultEnum = ResultEnum.ErrorFinal, Message = $"XML zdroj je prázdný directionID: {direction.DirectionID}" };

            // get format
            ExportFormat format;
            if (AbraSend(dispatchS3))
                format = ExportFormat.PDK18;
            else
                Enum.TryParse(dispatchS3.Customer.Extradata.FormatDiskety, out format);

            return MakeExportInternal(dispatchS3, settingsExport, direction, format, true, logger, out encoding);
        }

        public static ResultExport MakeExport(SettingsExport settingsExport, int directionID, ExportFormat format, out Encoding encoding)
        {
            encoding = Encoding.Default;
            // get & parse source xml
            // get source direction
            var srcDirection = (new DirectionDAL()).GetDirectionAllData(directionID);
            string xmlSource = GetSource(srcDirection.History);
            if (String.IsNullOrWhiteSpace(xmlSource))
                return new ResultExport() { ResultEnum = ResultEnum.Error, Message = $"XML zdroj je prázdný directionID: {directionID}" };

            //load XML
            DispatchS3 dispatchS3 = null;
            var load = DispatchS3.Serializer.Deserialize(xmlSource, out dispatchS3);

            if (!load)
                return new ResultExport() { ResultEnum = ResultEnum.Error, Message = $"Špatný formát XML directionID: {directionID}" };

            //find direction
            var direction = (new DAL.DirectionDAL()).GetDirectionAllData(directionID);
            
            return MakeExportInternal(dispatchS3, settingsExport, direction, format, false, null, out encoding);
        }

        public static bool AbraSend(DispatchS3 dispatchS3)
        {
            if (dispatchS3.Customer == null || dispatchS3.Customer.Extradata == null || string.IsNullOrWhiteSpace(dispatchS3.Customer.Extradata.FormatDiskety))
                return false;

            return dispatchS3.Customer.Extradata.FormatDiskety.ToLower().Equals("odesílá abra");
        }

        #endregion

        private static ResultExport MakeExportInternal(DispatchS3 dispatchS3, SettingsExport settingsExport, DirectionModel direction, ExportFormat format, bool fromExchange, NLog.Logger logger, out Encoding encoding)
        {
            if (logger != null)
                _logger = logger;

            var info = $"DirectionID: {direction.DirectionID} DocInfo: {direction.DocInfo}";
            _logger.Info($"Start export {info} - partner {dispatchS3.Customer.Name}");

            var warningMsg = string.Empty;
            var isWarning = false;
            encoding = Encoding.Default;
            try
            {
                // check direction status
                if (direction.DocStatus == Direction.DR_STATUS_CANCEL)
                {
                    var msg = "Dodací list je ve stavu CANCEL, export nebyl dokončen";
                    _logger.Info($"{info} {msg}");
                    _logger.Info($"End export {info}");
                    return new ResultExport() { ResultEnum = ResultEnum.ErrorFinal, Message = msg};
                }
                    

                // check format
                if (format == ExportFormat.None)
                {
                    var msg = "Formát exportu je *NONE*, export nebyl dokončen";
                    _logger.Info($"{info} {msg}");
                    _logger.Info($"End export {info}");
                    return new ResultExport() { ResultEnum = ResultEnum.ErrorFinal, Message = msg };
                }

                var doScanInvoice = false;
                SettingsExportRady configRada = Helper.GetRada(settingsExport, direction.DocNumPrefix, out doScanInvoice);
                
                bool singleDL; // pokud je true, posila se jenom obsah tohoto jednoho DL
                bool exportForG4 = false;
                if (fromExchange && dispatchS3.Customer.Extradata != null && AbraSend(dispatchS3))
                {
                    exportForG4 = true;
                    singleDL = true;
                }
                else
                {                 
                    // check if send this direction separately - X_Single_DL
                    bool? poslatDisketuSouhrnne = (bool?)direction.XtraData[Direction.XTRA_SEND_DISKET_SUMMARY];
                    if (poslatDisketuSouhrnne.HasValue)
                    {
                        singleDL = !poslatDisketuSouhrnne.Value; // poslano z Abry, uloženo v extra datech - poslat disketu souhrnne = není single DL
                    }
                    else
                        singleDL = GetG4SingleDL_MSSQL(configRada.MSSQLsource, direction); // neni poslano z Abry, musíme zjistit z DB Abry
                }
                                
                _logger.Info($"{info} singleDL: {singleDL}");
                
                // get list of "brother" directions
                DataTable directListTable = null;
                bool notReady = false;
                string invInfo = null;
                string orderNumber = null;
                DataTable invoiceItemsTable = null;
                var doneDirs = new List<int>();
                                
                if (doScanInvoice)
                {
                    GetG4DirectionList_MSSQL(configRada.MSSQLsource, direction, singleDL, ref directListTable, ref invoiceItemsTable, out notReady, out invInfo, out orderNumber);
                }
                else
                {
                    GetG4DirectionList_NoInvoice(configRada.MSSQLsource, direction, singleDL, ref directListTable, ref invoiceItemsTable, out notReady, out invInfo, out orderNumber);
                }

                if (notReady)
                {
                    var msg = "Není připraveno k exportu";
                    _logger.Info($"{info} {msg}");
                    _logger.Info($"End export {info}");
                    return new ResultExport() { ResultEnum = ResultEnum.NotReady, Message = msg };
                }

                // make direction list
                Direction[] directArray = GetDirectionList(directListTable, out notReady);

                _logger.Info($"{info} ABRA directListTable count: {directArray.Where(_ => _ != null).Count()} - doScanInvoice: {doScanInvoice}");
                foreach (var item in directArray.Where(_ => _ != null))
                {
                    _logger.Info($"{info} ABRA directListTable: DirectionID: {item.DirectionID} DocNumber: {item.DocNumber}");
                }

                // make doneDirs
                if (exportForG4)
                {
                    // pokud jde o export pro Abru, oznacime jako dokonceny jenom puvodni doklad
                    doneDirs.Add(direction.DirectionID);
                }
                else
                {
                    for (int pos = 0; pos < directArray.Length; pos++)
                        if (directArray[pos] != null)
                            doneDirs.Add(directArray[pos].DirectionID);
                }

                // if any direction is not ready, skip
                if (notReady)
                {
                    var msg = "Není připraveno k exportu";
                    _logger.Info($"{info} {msg}");
                    _logger.Info($"End export {info}");
                    return new ResultExport() { ResultEnum = ResultEnum.NotReady, Message = msg };
                }
                    


                // process every direction
                var mainRowsList = new ArrayList();
                for (int pos = 0; pos < directArray.Length; pos++)
                {
                    // check if dir is not canceled
                    if (directArray[pos].DocStatus != Direction.DR_STATUS_CANCEL)
                    {
                        // preload lists
                        var his = (new DirectionDAL()).GetDirectionHistory(directArray[pos].DirectionID);

                        // get xml
                        var xmlSource = GetSource(his);
                        if (xmlSource == "")
                        {
                            var msg = "XML zdroj je prázdný";
                            _logger.Info($"{info} {msg}");
                            _logger.Info($"End export {info}");
                            return new ResultExport() { ResultEnum = ResultEnum.ErrorFinal, Message = msg };
                        }
                            

                        //load XML
                        DispatchS3 dispatchS3XML = null;
                        Exception ex = null;
                        var ok = DispatchS3.Serializer.Deserialize(xmlSource, out dispatchS3XML, out ex);

                        if (!ok)
                        {
                            var exMessage = ex != null ? ex.Message : "";
                            var msg = $"Chybný formát XML '{exMessage}'";
                            _logger.Info(msg);
                            _logger.Info($"End export directionID: {direction.DirectionID} DocInfo: {direction.DocInfo}");
                            return new ResultExport() { ResultEnum = ResultEnum.ErrorFinal, Message = msg };
                        }

                        // make export rows
                        ExportRow[] exportRows = MakeExportRows(directArray[pos].DirectionID, dispatchS3XML);

                        // check
                        if (dispatchS3XML.Items.Count > exportRows.Length)
                        {
                            if (fromExchange)
                            {
                                var msg = $"Kontrola: Export (počet vět: {exportRows.Length}) by měl méně řádků než zdrojový soubor (počet vět: {dispatchS3XML.Items.Count})";
                                _logger.Info($"{info} {msg}");
                                _logger.Info($"End export {info}");
                                return new ResultExport() { ResultEnum = ResultEnum.ErrorFinal, Message = msg };
                            }
                            else
                            {
                                var msg = $"Pozor varování! Export ({exportRows.Length}) má méně řádků než zdrojový soubor z ABRY ({dispatchS3XML.Items.Count})";
                                _logger.Info($"{info} {msg}");
                                isWarning = true;
                                warningMsg = msg;
                            }
                            
                        }
                            
                        // add to main list
                        foreach (ExportRow row in exportRows)
                            mainRowsList.Add(row);
                    }
                }

                // sort export rows 
                mainRowsList.Sort(new ExportRow());
                var expRows = (ExportRow[])mainRowsList.ToArray(typeof(ExportRow));

                _logger.Info($"{info} ABRA invoiceItemsTable count: {invoiceItemsTable.Rows.Count}");

                // make files
                var expResults = MakeExportResults(settingsExport, dispatchS3, direction, format, expRows, invInfo,
                                                              orderNumber, out encoding, invoiceItemsTable);
                                
                _logger.Info($"End export {info}");
                return new ResultExport() { ResultEnum = ResultEnum.Sended, ExpResults = expResults, DoneDirs = doneDirs, Message = "Export dokončen.", IsWarning = isWarning, WarningMsg = warningMsg };
            }
            catch (Exception exc)
            {
                var msg = $"Chyba při pokusu exportovat doklad {direction.DirectionID} - chyba {exc.Message}";
                _logger.Info($"{info} {msg}");
                _logger.Info($"End export {info}");
                _logger.Error(exc);
                return new ResultExport() { ResultEnum = ResultEnum.Error, Message = msg };
            }
        }


        #region Utils

        private static ExportResult[] MakeExportResults(SettingsExport config, DispatchS3 dispatchS3,
                                                        Direction direction, ExportFormat format,
                                                        ExportRow[] expRows, string invInfo, string orderNumber,
                                                        out Encoding encoding, DataTable invoiceItemsTable)
        {
            switch (format)
            {
                case ExportFormat.MEDICOLS:
                    return ExportyInternal.MEDICOLS(direction, expRows, invInfo, orderNumber, out encoding);

                case ExportFormat.LEKIS5:
                    return ExportyInternal.LEKIS5(config, dispatchS3, direction, expRows, invInfo, orderNumber, out encoding);

                case ExportFormat.LEKIS6:
                    return ExportyInternal.LEKIS6(config, dispatchS3, direction, expRows, invInfo, orderNumber, out encoding);

                case ExportFormat.PAENIUM:
                    return ExportyInternal.PAENIUM(config, dispatchS3, direction, expRows, invInfo, orderNumber, out encoding);

                case ExportFormat.MEDIOX:
                    return ExportyInternal.MEDIOX(config, dispatchS3, direction, expRows, invInfo, orderNumber, out encoding, invoiceItemsTable);

                case ExportFormat.PENTENIUM:
                    return ExportyInternal.PENTENIUM(config, dispatchS3, direction, expRows, invInfo, orderNumber, out encoding);

                case ExportFormat.PDK5:
                    return ExportyInternal.PDK5(config, dispatchS3, direction, expRows, invInfo, orderNumber, out encoding, invoiceItemsTable);

                case ExportFormat.PDK4:
                    return ExportyInternal.PDK4(config, dispatchS3, direction, expRows, invInfo, orderNumber, out encoding, invoiceItemsTable);

                case ExportFormat.PDK8:
                    return ExportyInternal.PDK8(config, dispatchS3, direction, expRows, invInfo, orderNumber, out encoding, invoiceItemsTable);

                case ExportFormat.PDKX:
                    return ExportyInternal.PDKX(config, dispatchS3, direction, expRows, invInfo, orderNumber, out encoding, invoiceItemsTable);

                case ExportFormat.PDK12:
                    return ExportyInternal.PDK12(config, dispatchS3, direction, expRows, invInfo, orderNumber, out encoding, invoiceItemsTable);

                case ExportFormat.PDK15:
                    return ExportyInternal.PDK15(config, dispatchS3, direction, expRows, invInfo, orderNumber, out encoding, invoiceItemsTable);

                case ExportFormat.PDK18:
                    return ExportyInternal.PDK18(config, dispatchS3, direction, expRows, invInfo, orderNumber, out encoding, invoiceItemsTable);

                default:
                    throw new NotImplementedException();
            }
        }

        private static ExportRow[] MakeExportRows(int directionID, DispatchS3 xmlRows)
        {
            var rowsList = new ArrayList();

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                // get store moves
                var sql = new Sql().Where("parent_directionID = @0 AND docNumPrefix = @1", directionID, StoreMove.MO_PREFIX_DISPATCH);
                var stMoveList = db.Fetch<StoreMove>(sql);

                // fill table
                foreach (var stMove in stMoveList)
                {
                    sql = new Sql().Where("stoMoveID = @0", stMove.StoMoveID);
                    var stMoveItems = db.Fetch<StoreMoveItem>(sql);

                    foreach (var stItem in stMoveItems)
                    {
                        if (stItem.ItemDirection == Direction.DOC_DIRECTION_OUT && stItem.ItemValidity != StoreMoveItem.MI_VALIDITY_CANCELED)
                        {
                            var newRow = new ExportRow(stItem, xmlRows);
                            rowsList.Add(newRow);
                        }
                    }
                }
            }


            var result = (ExportRow[])rowsList.ToArray(typeof(ExportRow));
            return result;
        }

        private static string GetSource(List<DirectionHistory> historyList)
        {
            var data = (from h in historyList
                        where h.EventCode.Equals(Direction.DR_STATUS_LOAD) && h.DocPosition == 0 && h.EventData.Contains("<?xml")
                        orderby h.EntryDateTime
                        select h.EventData).LastOrDefault();

            if (data != null)
                return data;
            else
                return String.Empty;

        }

        private static bool GetG4SingleDL_MSSQL(string connectionString, Direction srcDirection)
        {
            var sql = "SELECT 1 FROM sys.columns WHERE Name = 'X_SINGLE_DL' AND Object_ID = Object_ID(N'FIRMS')";
            var rows = GeDataRowFromSQL(sql, connectionString, new Dictionary<string, object>()).Rows;
            if(rows.Count == 0)
                // in this db table FIRMS not have field X_SINGLE_DL
                return false;


            sql = @" SELECT FI.X_SINGLE_DL
                         FROM STOREDOCUMENTS SD
                         JOIN DOCQUEUES DQ ON(SD.DOCQUEUE_ID = DQ.ID)
                         JOIN PERIODS PS ON(SD.PERIOD_ID = PS.ID)
                         JOIN FIRMS FI ON(SD.FIRM_ID = FI.ID) 
                         WHERE(DQ.CODE = @0) AND (SD.ORDNUMBER = @1) AND(PS.CODE = @2)";

            var parameters = new Dictionary<string, object>
            {
                { "@0", srcDirection.DocNumPrefix.TrimEnd(' ') },
                { "@1", srcDirection.DocNumber },
                { "@2", Int32.Parse(srcDirection.DocYear) + 2000 }
            };

            rows = GeDataRowFromSQL(sql, connectionString, parameters).Rows;
            var singleDL = rows.Count > 0 ? rows[0]["X_SINGLE_DL"].ToString() : "";

            if (String.Compare(singleDL, "A", true) == 0)
                return true;
            else if (String.Compare(singleDL, "N", true) == 0)
                return false;
            else
                return false;
        }

        private static void GetG4DirectionList_NoInvoice(string connectionString, Direction srcDirection, bool singleDL, ref DataTable directList, ref DataTable invoiceItemsTable,
                                               out bool notReady, out string invInfo, out string orderNumber)
        {
            notReady = false;
            invInfo = null;
            orderNumber = "";

            // get source dir G3 ID
            var srcDirID = GetID_SD(srcDirection.DocNumPrefix.TrimEnd(' '), srcDirection.DocNumber, int.Parse(srcDirection.DocYear), connectionString);
            orderNumber = GetOrderNumber(srcDirID, connectionString);

            var sql = @"SELECT  DQ.CODE AS PREFIX, PS.CODE AS PERIOD, SD.ORDNUMBER
			            FROM STOREDOCUMENTS SD
						JOIN DOCQUEUES DQ ON (SD.DOCQUEUE_ID = DQ.ID)
						JOIN PERIODS PS ON (SD.PERIOD_ID = PS.ID)
			            WHERE (SD.ID = @0)
			            GROUP BY DQ.CODE, PS.CODE, SD.ORDNUMBER";

            var parameters = new Dictionary<string, object>();
            parameters.Add("@0", srcDirID);

            directList = GeDataRowFromSQL(sql, connectionString, parameters);
            if (directList.Rows.Count == 0)
                throw new Exception(String.Format("List of directions is empty; DirectionID: {0}", srcDirection.DirectionID));


            sql = @"SELECT II2.STORECARD_ID, II2.TAMOUNTWITHOUTVAT, II2.TAMOUNT, CAST(II2.TAMOUNTWITHOUTVAT / (II2.QUANTITY / II2.UNITRATE) AS DECIMAL(10,3)) AS UNITPRICE, 
                 CAST(II2.TAMOUNT / (II2.QUANTITY / II2.UNITRATE) AS DECIMAL(10,3)) AS UNITPRICEVAT, II2.QUANTITY, CAST(II2.QUANTITY / II2.UNITRATE AS DECIMAL(10,3)) AS QUANTITY_REAL,
                 II2.UNITRATE, II2.VATRATE, SC.CUSTOMSTARIFFNUMBER, SC.CODE, SC.X_CODE_PDK, SC.X_CODE_VZP
                FROM ISSUEDINVOICES2 II2 JOIN STORECARDS SC ON (SC.ID = II2.STORECARD_ID)
                WHERE (1 = 0)";


            invoiceItemsTable = GeDataRowFromSQL(sql, connectionString);

        }

        private static void GetG4DirectionList_MSSQL(string connectionString, Direction srcDirection, bool singleDL, ref DataTable directList, ref DataTable invoiceItemsTable,
                                           out bool notReady, out string invInfo, out string orderNumber)
        {
            notReady = false;
            invInfo = null;
            orderNumber = "";

            //ID
            var srcDirID = GetID_SD(srcDirection.DocNumPrefix.TrimEnd(' '), srcDirection.DocNumber, int.Parse(srcDirection.DocYear), connectionString);

            //OrderNumber
            orderNumber = GetOrderNumber(srcDirID, connectionString);

            // get parent invoice ID
            var sql = @"SELECT MAX(II2.PARENT_ID) as PARENT_ID
				    FROM STOREDOCUMENTS2 SD2 JOIN ISSUEDINVOICES2 II2 ON (SD2.ID = II2.PROVIDEROW_ID)
				    WHERE (SD2.PARENT_ID = @0)";

            var parameters = new Dictionary<string, object>();
            parameters.Add("@0", srcDirID);

            var rows = GeDataRowFromSQL(sql, connectionString, parameters).Rows;
            var srcInvID = rows.Count > 0 ? rows[0]["PARENT_ID"].ToString() : "";

            _logger.Info($"DirectionID: {srcDirection.DirectionID} DocInfo: {srcDirection.DocNumber} ABRA get parent invoice ID: {srcInvID}");

            if (string.IsNullOrWhiteSpace(srcInvID))
            {
                //neexistuje FV
                notReady = true;
                return;
            }
            
            if (string.IsNullOrWhiteSpace(srcInvID))
            {
                // parent invoice not found

                // directio list - only source invoice
                sql = @"SELECT  DQ.CODE AS PREFIX, PS.CODE AS PERIOD, SD.ORDNUMBER
			            FROM STOREDOCUMENTS SD
						JOIN DOCQUEUES DQ ON (SD.DOCQUEUE_ID = DQ.ID)
						JOIN PERIODS PS ON (SD.PERIOD_ID = PS.ID)
			            WHERE (SD.ID = @0)
			            GROUP BY DQ.CODE, PS.CODE, SD.ORDNUMBER";

                parameters = new Dictionary<string, object>();
                parameters.Add("@0", srcDirID);

                directList = GeDataRowFromSQL(sql, connectionString, parameters);

                // fake invoice items list
                sql = @"SELECT II2.STORECARD_ID, II2.TAMOUNTWITHOUTVAT, II2.TAMOUNT, CAST(II2.TAMOUNTWITHOUTVAT / (II2.QUANTITY / II2.UNITRATE) AS DECIMAL(10,3)) AS UNITPRICE, 
                 CAST(II2.TAMOUNT / (II2.QUANTITY / II2.UNITRATE) AS DECIMAL(10,3)) AS UNITPRICEVAT, II2.QUANTITY, CAST(II2.QUANTITY / II2.UNITRATE AS DECIMAL(10,3)) AS QUANTITY_REAL,
                 II2.UNITRATE, II2.VATRATE, SC.CUSTOMSTARIFFNUMBER, SC.CODE, SC.X_CODE_PDK, SC.X_CODE_VZP
                FROM ISSUEDINVOICES2 II2 JOIN STORECARDS SC ON (SC.ID = II2.STORECARD_ID)
                WHERE (1 = 0)";

                invoiceItemsTable = GeDataRowFromSQL(sql, connectionString);
            }
            else
            {

                // get invoice info
                sql = @"SELECT ORDNUMBER
				    FROM ISSUEDINVOICES II
				    WHERE (ID = @0)";

                parameters = new Dictionary<string, object>();
                parameters.Add("@0", srcInvID);

                rows = GeDataRowFromSQL(sql, connectionString, parameters).Rows;
                invInfo = rows.Count > 0 ? rows[0]["ORDNUMBER"].ToString() : "";


                // get list of directions
                if (singleDL)
                {
                    sql = @"SELECT  DQ.CODE AS PREFIX, PS.CODE AS PERIOD, SD.ORDNUMBER
                        FROM ISSUEDINVOICES2 II2 JOIN STOREDOCUMENTS2 SD2 ON (SD2.ID = II2.PROVIDEROW_ID)
                        JOIN STOREDOCUMENTS SD ON (SD2.PARENT_ID = SD.ID)
                        JOIN DOCQUEUES DQ ON (SD.DOCQUEUE_ID = DQ.ID)
                        JOIN PERIODS PS ON (SD.PERIOD_ID = PS.ID)
                        WHERE (II2.PARENT_ID = @0) AND (SD.ID = @1)
                        GROUP BY DQ.CODE, PS.CODE, SD.ORDNUMBER";

                    parameters = new Dictionary<string, object>();
                    parameters.Add("@0", srcInvID);
                    parameters.Add("@1", srcDirID);

                }
                else
                {
                    sql = @"SELECT DQ.CODE AS PREFIX, PS.CODE AS PERIOD, SD.ORDNUMBER
                        FROM ISSUEDINVOICES2 II2 JOIN STOREDOCUMENTS2 SD2 ON (SD2.ID = II2.PROVIDEROW_ID)
                        JOIN STOREDOCUMENTS SD ON (SD2.PARENT_ID = SD.ID)
                        JOIN DOCQUEUES DQ ON (SD.DOCQUEUE_ID = DQ.ID)
                        JOIN PERIODS PS ON (SD.PERIOD_ID = PS.ID)
                        WHERE (II2.PARENT_ID = @0)
                        GROUP BY DQ.CODE, PS.CODE, SD.ORDNUMBER";

                    parameters = new Dictionary<string, object>();
                    parameters.Add("@0", srcInvID);
                }

                directList = GeDataRowFromSQL(sql, connectionString, parameters);

                if (directList.Rows.Count == 0)
                    throw new Exception(String.Format("List of directions is empty; srcInvID: '{0}'; DirectionID: {1}", srcInvID,
                                                      srcDirection.DirectionID));


                // get invoice items
                sql = @"SELECT II2.STORECARD_ID, II2.TAMOUNTWITHOUTVAT, II2.TAMOUNT, CAST(II2.TAMOUNTWITHOUTVAT / (II2.QUANTITY / II2.UNITRATE) AS DECIMAL(10,3)) AS UNITPRICE, 
                 CAST(II2.TAMOUNT / (II2.QUANTITY / II2.UNITRATE) AS DECIMAL(10,3)) AS UNITPRICEVAT, II2.QUANTITY, CAST(II2.QUANTITY / II2.UNITRATE AS DECIMAL(10,3)) AS QUANTITY_REAL,
                 II2.UNITRATE, II2.VATRATE, SC.CUSTOMSTARIFFNUMBER, SC.CODE, SC.X_CODE_PDK, SC.X_CODE_VZP
                FROM ISSUEDINVOICES2 II2 JOIN STORECARDS SC ON (SC.ID = II2.STORECARD_ID)
                WHERE (II2.PARENT_ID = @0)";

                parameters = new Dictionary<string, object>();
                parameters.Add("@0", srcInvID);

                invoiceItemsTable = GeDataRowFromSQL(sql, connectionString, parameters);

                if (invoiceItemsTable.Rows.Count == 0)
                    throw new Exception(String.Format("List of invoice items is empty; srcInvID: '{0}'; DirectionID: {1}",
                                                      srcInvID, srcDirection.DirectionID));

            }
        }


        private static Direction[] GetDirectionList(DataTable directListTable, out bool notReady)
        {

            notReady = false;
            var result = new Direction[directListTable.Rows.Count];

            for (int i = 0; i < directListTable.Rows.Count; i++)
            {
                using (var db = Core.Data.ConnectionHelper.GetDB())
                {
                    var docPrefix = (string)directListTable.Rows[i]["PREFIX"];
                    var docPeriod = (string)directListTable.Rows[i]["PERIOD"];
                    var ordNumber = decimal.Parse(directListTable.Rows[i]["ORDNUMBER"].ToString());

                    var sql = new Sql().Where("docNumPrefix = @0 AND Year(docDate) = @1 AND docNumber = @2", docPrefix, docPeriod, ordNumber);
                    var direction = db.FirstOrDefault<Direction>(sql);

                    if (direction == null)
                    {
                        // direction is not in s3
                        notReady = true;
                    }
                    else
                    {
                        // check status
                        if (
                            !(
                                 (direction.DocStatus == Direction.DR_STATUS_DISPATCH_SAVED) ||
                                 (direction.DocStatus == Direction.DR_STATUS_DISP_CHECK_WAIT) ||
                                 (direction.DocStatus == Direction.DR_STATUS_DISP_CHECK_PROC) ||
                                 (direction.DocStatus == Direction.DR_STATUS_DISP_CHECK_DONE) ||
                                 (direction.DocStatus == Direction.DR_STATUS_DISP_EXPED) ||
                                 (direction.DocStatus == Direction.DR_STATUS_CANCEL))
                            )
                        {
                            // direction is not ready (prepared)
                            notReady = true;
                        }


                        result[i] = direction;
                    }
                }
            }

            return result;

        }

        #endregion

        #region Helper

        private static DataTable GeDataRowFromSQL(string sql, string connectionString, Dictionary<string, object> parameterList = null)
        {

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand selectCMD = new SqlCommand(sql, connection);

                if (parameterList != null)
                {
                    foreach (var parameter in parameterList)
                        selectCMD.Parameters.AddWithValue(parameter.Key, parameter.Value);
                }

                selectCMD.CommandTimeout = 60;
                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = selectCMD;

                DataTable dt = new DataTable();
                dataAdapter.Fill(dt);
                return dt;
            }
        }

        private static string GetID_SD(string code, int number, int docYear, string connectionString)
        {
            //ID
            var sql = @"SELECT SD.ID 
                        FROM STOREDOCUMENTS SD
                        JOIN DOCQUEUES DQ ON(SD.DOCQUEUE_ID = DQ.ID)
                        JOIN PERIODS PS ON(SD.PERIOD_ID = PS.ID)
                        WHERE(DQ.CODE = @0) AND (SD.ORDNUMBER = @1) AND (PS.CODE = @2)";

            var parameters = new Dictionary<string, object>();
            parameters.Add("@0", code.TrimEnd(' '));
            parameters.Add("@1", number);
            parameters.Add("@2", docYear + 2000);

            var rows = GeDataRowFromSQL(sql, connectionString, parameters).Rows;
            var srcDirID = rows.Count > 0 ? rows[0]["ID"].ToString() : "";
            return srcDirID;
        }

        private static string GetOrderNumber(string srcDirID, string connectionString)
        {
            //EXTERNALNUMBER
            var sql = @"SELECT MAX(RO.EXTERNALNUMBER) as EXTERNALNUMBER
                    FROM STOREDOCUMENTS2 SD2
                    LEFT JOIN RECEIVEDORDERS RO ON (SD2.PROVIDE_ID = RO.ID)
                    WHERE (SD2.PARENT_ID = @0)";

            var parameters = new Dictionary<string, object>();
            parameters.Add("@0", srcDirID);

            var rows = GeDataRowFromSQL(sql, connectionString, parameters).Rows;
            var orderNumber = rows.Count > 0 ? rows[0]["EXTERNALNUMBER"].ToString() : "";

            _logger.Info($"ABRA orderNumber: {orderNumber}");
            return orderNumber;
        }

        #endregion

    }

}

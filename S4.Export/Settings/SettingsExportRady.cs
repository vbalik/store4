﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Export.Settings
{
    public class SettingsExportRady
    {
        public string Prefix { get; set; }
        public string UlozitG4 { get; set; }

        public bool NeedExtraData = false;

        public bool Ignore = false;
        public string MSSQLsource { get; set; }

        public bool DoScanInvoice = false;
    }
}

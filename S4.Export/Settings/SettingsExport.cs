﻿using S4.Entities.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Export.Settings
{
    public class SettingsExport
    {
        public SettingMail SettingMail { get; set; }
        public SettingExportLekarny SettingExportLekarny { get; set; }
        public SettingExportLekarny SettingExportABRA { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Export.Settings
{
    public class SettingExportLekarny
    {
        public string DodavatelICO { get; set; }
        public string DodavatelDIC { get; set; }
        public bool VysledekUlozit { get; set; }
        public string UlozitCesta { get; set; }
        public bool KontrolniKopie { get; set; }
        public string OdeslatKontrolniKopie { get; set; }
        public bool OdeslatZakaznik { get; set; }

        public List<SettingsExportRady> SettingsExportRady { get; set; }

    }
}

﻿using S4.Core;
using S4.Entities.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace S4.Export.Settings
{
    public static class Helper
    {
        public static SettingsExport GetSettingsExport()
        {
            //read setting
            var mailSettingJson = Singleton<DAL.SettingsSet>.Instance.GetValue_String(Entities.SettingsDictionary.MAIL_SETTING);
            var exportLekarnyJson = Singleton<DAL.SettingsSet>.Instance.GetValue_String(Entities.SettingsDictionary.EXPORT_LEKARNY);
            var exportABRAJson = Singleton<DAL.SettingsSet>.Instance.GetValue_String(Entities.SettingsDictionary.EXPORT_ABRA);

            if (string.IsNullOrWhiteSpace(mailSettingJson))
                throw new Exception($"Entities.SettingsDictionary.MAIL_SETTING is empty");

            if (string.IsNullOrWhiteSpace(exportABRAJson))
                throw new Exception($"Entities.SettingsDictionary.EXPORT_LEKARNY is empty");

            if (string.IsNullOrWhiteSpace(exportLekarnyJson))
                throw new Exception($"Entities.SettingsDictionary.EXPORT_ABRA is empty");

            var settingsMail = Newtonsoft.Json.JsonConvert.DeserializeObject<SettingMail>(mailSettingJson);
            var settingsLekarny = Newtonsoft.Json.JsonConvert.DeserializeObject<SettingExportLekarny>(exportLekarnyJson);
            var settingsABRA = Newtonsoft.Json.JsonConvert.DeserializeObject<SettingExportLekarny>(exportABRAJson);

            var settingsExport = new SettingsExport();
            settingsExport.SettingMail = settingsMail;
            settingsExport.SettingExportLekarny = settingsLekarny;
            settingsExport.SettingExportABRA = settingsABRA;

            return settingsExport;
        }

        public static SettingMail GetSettingMail()
        {
            //read setting
            var mailSettingJson = Singleton<DAL.SettingsSet>.Instance.GetValue_String(Entities.SettingsDictionary.MAIL_SETTING);

            if (string.IsNullOrWhiteSpace(mailSettingJson))
                throw new Exception($"Entities.SettingsDictionary.MAIL_SETTING is empty");

            return Newtonsoft.Json.JsonConvert.DeserializeObject<SettingMail>(mailSettingJson);
        }

        public static SettingsExportRady GetRada(SettingsExport settingsExport, string docNumPrefix, out bool doScanInvoice)
        {
            SettingsExportRady configRada = null;
            doScanInvoice = false;

            foreach (var rada in settingsExport.SettingExportLekarny.SettingsExportRady)
            {
                var pref = rada.Prefix.Split(',');
                var found = (from c in pref where c.Equals(docNumPrefix.TrimEnd(' ')) select true).FirstOrDefault();
                if (found)
                {
                    configRada = rada;
                    break;
                }
            }

            // default
            if (configRada == null)
            {
                configRada = (from c in settingsExport.SettingExportLekarny.SettingsExportRady where c.Prefix.Equals("*") select c).FirstOrDefault();
                doScanInvoice = true;
            }
            else
                doScanInvoice = configRada.DoScanInvoice;

            return configRada;
        }


        public static SettingsExportRady GetRadaABRAExport(SettingsExport settingsExport, string docNumPrefix)
        {
            SettingsExportRady configRada = null;

            foreach (var rada in settingsExport.SettingExportABRA.SettingsExportRady)
            {
                var pref = rada.Prefix.Split(',');
                var found = (from c in pref where c.Equals(docNumPrefix.TrimEnd(' ')) select true).FirstOrDefault();
                if (found)
                {
                    configRada = rada;
                    break;
                }
            }

            if (configRada == null)
                configRada = (from c in settingsExport.SettingExportABRA.SettingsExportRady where c.Prefix.Equals("*") select c).FirstOrDefault();

            return configRada;
        }
    }
}

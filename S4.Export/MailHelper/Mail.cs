﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace S4.Export.MailHelper
{
    public class Mail
    {
        #region public

        public bool SendMailViaSMTP(MailSetting mailSetting, out string error)
        {
            error = "";
            try
            {
                AddAddress(mailSetting.Message.To, mailSetting.To, out string _);

                if (mailSetting.Message.To.Count < 1 && mailSetting.Message.CC.Count < 1)
                {
                    error = "Adresa příjemce není vyplněna!";
                    return false;
                }

                // send
                var mailClient = new SmtpClient(mailSetting.Server);

                if (string.IsNullOrEmpty(mailSetting.User))
                    mailClient.UseDefaultCredentials = true;
                else
                {
                    mailClient.UseDefaultCredentials = false;
                    mailClient.Credentials = new NetworkCredential(mailSetting.User, mailSetting.Password);
                }

                mailClient.Send(mailSetting.Message);
            }
            catch (SmtpFailedRecipientsException smtpRecExc)
            {
                error = $"Mail nebyl odeslán některým příjemcům {smtpRecExc.FailedRecipient} chyba: {smtpRecExc.ToString()}";
                return false;
            }
            catch (SmtpFailedRecipientException smtpRecExc)
            {
                error = $"Mail nebyl odeslán některým příjemcům {smtpRecExc.FailedRecipient} chyba: {smtpRecExc.Message}";
                return false;
            }
            catch (Exception ex)
            {
                error = $"Při pokusu odeslat mail se vyskytla chyba: {ex.Message}";
                return false;
            }

            return true;
        }

        public bool AddAddress(MailAddressCollection addressColl, string mailToAdd, out string error)
        {
            error = "";

            if (string.IsNullOrWhiteSpace(mailToAdd))
            {
                error = "Žádný mail nelze přidat!";
                return false;
            }
                
            string[] tmp = mailToAdd.Split(';');
            bool isOK = true;
            
            foreach (var adr in tmp)
            {
                if (!string.IsNullOrWhiteSpace(adr))
                {
                    try
                    {
                        addressColl.Add(adr);
                    }
                    catch (FormatException)
                    {
                        isOK = false;
                        error += $"'{adr}' E-mailová adresa je neplatná nebo není podporovaná! ";
                    }
                    catch (Exception)
                    {
                        isOK = false;
                        error += $"'{adr}' Při pokusu přidat E-mailovou adresu se vyskytla chyba! ";
                    }
                }
            }

            return isOK;
        }

        #endregion

    }
}

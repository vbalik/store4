﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;

namespace S4.Export.MailHelper
{
    public class MailSetting
    {
        public string Server { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public string To { get; set; }
        public string CC { get; set; }

        public MailMessage Message { get; set; }
        
    }
}

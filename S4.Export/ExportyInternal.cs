﻿using System;
using System.Xml;
using System.Data;
using S4.Entities;
using S4.Entities.Helpers.Xml.Dispatch;
using System.Reflection;

namespace S4.Export
{
    /// <summary>
    /// Summary description for ExportLekarnyExporty.
    /// </summary>
    public class ExportyInternal
    {
        private const char SPACE = ' ';
        private const decimal DPH_1 = 12;
        private const decimal DPH_2 = 21;
        private const decimal DPH_SNIZENA_1 = 12;
        private const decimal DPH_ZAKLADNI = 21;
        private const string G3_TAMOUNTWITHOUTVAT = "TAMOUNTWITHOUTVAT";
        private const string G3_TAMOUNT = "TAMOUNT";

        private static NLog.Logger NLogger = NLog.LogManager.GetLogger("S4.Export");

        public static ExportResult[] MEDICOLS(Direction direction, ExportRow[] expRows, string invInfo, string orderNumber, out System.Text.Encoding outputEncoding)
        {
            ExportResult result = new ExportResult(System.Globalization.CultureInfo.InvariantCulture);
            outputEncoding = System.Text.Encoding.GetEncoding("windows-1250");


            // file name
            result.fileName = "DOD.TXT";


            // other lines
            foreach (ExportRow row in expRows)
            {
                using (var db = Core.Data.ConnectionHelper.GetDB())
                {
                    var lot = db.SingleOrDefaultById<StoreMoveLot>(row.moveItem.StoMoveLotID);
                    var pac = db.SingleOrDefaultById<ArticlePacking>(lot.ArtPackID);
                    var article = db.SingleOrDefaultById<Article>(pac.ArticleID);

                    // Kod VZP
                    result.Append(-13, (row.xmlData.Extradata != null && (row.xmlData.Extradata.SkupinaZbozi == "1") && !string.IsNullOrEmpty(row.xmlData.Extradata.KodVZP)) ? row.xmlData.Extradata.KodVZP : article.ArticleCode);
                    result.AppendSpace();

                    // Nazev
                    result.Append(-30, article.ArticleDesc);
                    result.AppendSpace();

                    // Mnozstvi
                    result.Append(row.moveItem.Quantity, 12, 2);
                    result.AppendSpace();

                    // MJ
                    result.Append(-3, pac.PackDesc);
                    result.AppendSpace();

                    // Vyr cena
                    result.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.VyrobniCenaDec : 0, 12, 2);
                    result.AppendSpace();

                    // Nakup s DPH - od 070926
                    // Nakup bez DPH
                    result.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.CenaSDPHDec : 0, 12, 2);
                    result.AppendSpace();

                    // Prodej bez DPH
                    result.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.ProdejniCenaBezDPHDec : 0, 12, 2);
                    result.AppendSpace();

                    // Sarze
                    result.Append(-12, string.IsNullOrWhiteSpace(lot.BatchNum) ? "?" : lot.BatchNum);
                    result.AppendSpace();

                    // Expirace
                    result.Append("dd.MM.yyyy", lot.ExpirationDate);


                    result.AppendNewLine();
                }
            }
            
            NLogger.Debug($"DirectionID: {direction.DirectionID} DocNumber: {direction.DocNumber} {MethodBase.GetCurrentMethod().Name} - exported rows: {result.RowCount}");
            return new ExportResult[] { result };
        }



        public static ExportResult[] LEKIS5(S4.Export.Settings.SettingsExport config, DispatchS3 dispatchS3, Direction direction, ExportRow[] expRows, string invInfo, string orderNumber, out System.Text.Encoding outputEncoding)
        {
            ExportResult result = new ExportResult(System.Globalization.CultureInfo.InvariantCulture);
            outputEncoding = System.Text.Encoding.GetEncoding("windows-1250");


            // file name
            result.fileName = String.Format("{0:00000000}.DL5", direction.DocNumber);


            // first line

            // ICO dodavatele
            result.Append(-20, config.SettingExportLekarny.DodavatelICO);

            // cislo DL
            result.Append(-20, direction.DocNumber.ToString());

            // ICO odberatele
            string custIDNum = dispatchS3.Customer.Extradata != null && !string.IsNullOrWhiteSpace(dispatchS3.Customer.Extradata.ICO) ? dispatchS3.Customer.Extradata.ICO.ToString() : "?";
            result.Append(-20, custIDNum);

            // Cislo objednavky
            result.AppendSpace(12);

            // Kodova stranka
            result.Append(5, 1250);

            // pocet polozek
            result.Append(8, expRows.Length);

            // Soucet bez DPH
            result.Append(dispatchS3.SoucetCenaBezDPHDec, 10, 2);

            // Soucet s DPH
            result.Append(dispatchS3.SoucetCenaSDPHDec, 10, 2);

            // Soucet bez DPH snizena
            result.Append(dispatchS3.SoucetCenaBezDPHSnizenaDec, 10, 2);

            // Soucet s DPH snizena
            result.Append(dispatchS3.SoucetCenaSDPHSnizenaDec, 10, 2);

            // Soucet bez DPH zakladni
            result.Append(dispatchS3.SoucetCenaBezDPHZakladniDec, 10, 2);

            // Soucet s DPH zakladni
            result.Append(dispatchS3.SoucetCenaSDPHZakladniDec, 10, 2);

            // ID skladu
            result.AppendSpace(10);

            result.AppendNewLine();


            // other lines
            foreach (ExportRow row in expRows)
            {
                using (var db = Core.Data.ConnectionHelper.GetDB())
                {
                    var lot = db.SingleOrDefaultById<StoreMoveLot>(row.moveItem.StoMoveLotID);
                    var pac = db.SingleOrDefaultById<ArticlePacking>(lot.ArtPackID);
                    var article = db.SingleOrDefaultById<Article>(pac.ArticleID);

                    // Kod VZP
                    var vzp = "";
                    if (row.xmlData.Extradata == null)
                        vzp = new string(SPACE, 7);
                    else
                    {
                        if (row.xmlData.Extradata.SkupinaZbozi == "1")
                            vzp = row.xmlData.Extradata.KodVZP;
                        else
                            vzp = new string(SPACE, 7);
                    }

                    result.Append(7, vzp);

                    // Nazev
                    result.Append(-60, article.ArticleDesc);

                    // Skupina
                    result.Append(1, row.xmlData.Extradata != null ? row.xmlData.Extradata.SkupinaZbozi : "");

                    // Sazba DPH
                    result.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.SazbaDPH : 0, 5, 2);

                    // Vyr cena
                    result.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.VyrobniCenaDec : 0, 9, 2);

                    // Nakup s DPH
                    result.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.CenaSDPHDec : 0, 9, 2);

                    // Prodej s DPH
                    result.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.ProdejniCenaSDPHDec : 0, 9, 2);

                    // Mnozstvi
                    result.Append(row.moveItem.Quantity, 8, 2);

                    // Sarze
                    result.Append(-20, string.IsNullOrWhiteSpace(lot.BatchNum) ? "?" : lot.BatchNum);

                    // Expirace
                    result.Append("dd.MM.yyyy", lot.ExpirationDate, 10);

                    // Carovy kod
                    result.AppendSpace(20);

                    // Kod dodavatele
                    result.Append(-20, article.ArticleCode);

                    // Druh kodu
                    result.Append("0");

                    // Nakup bez DPH
                    result.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.CenaBezDPHDec : 0, 9, 2);

                    // Certifikat
                    result.AppendSpace(20);


                    result.AppendNewLine();
                }
            }

            NLogger.Debug($"DirectionID: {direction.DirectionID} DocNumber: {direction.DocNumber} {MethodBase.GetCurrentMethod().Name} - exported rows: {result.RowCount}");
            return new ExportResult[] { result };
        }



        public static ExportResult[] LEKIS6(S4.Export.Settings.SettingsExport config, DispatchS3 dispatchS3, Direction direction, ExportRow[] expRows, string invInfo, string orderNumber, out System.Text.Encoding outputEncoding)
        {
            ExportResult result = new ExportResult(System.Globalization.CultureInfo.InvariantCulture);
            outputEncoding = System.Text.Encoding.GetEncoding("windows-1250");


            // file name
            result.fileName = String.Format("{0:00000000}.DL6", direction.DocNumber);


            // first line

            // ICO dodavatele
            result.Append(-20, config.SettingExportLekarny.DodavatelICO);

            // cislo DL
            result.Append(-20, direction.DocNumber.ToString());

            // ICO odberatele
            string custIDNum = dispatchS3.Customer.Extradata != null && !string.IsNullOrWhiteSpace(dispatchS3.Customer.Extradata.ICO) ? dispatchS3.Customer.Extradata.ICO.ToString() : "";
            result.Append(-20, custIDNum);

            // Cislo objednavky
            result.AppendSpace(12);

            // Kodova stranka
            result.Append(5, 1250);

            // pocet polozek
            result.Append(8, expRows.Length);

            // Soucet bez DPH
            result.Append(dispatchS3.SoucetCenaBezDPHDec, 10, 2);

            // Soucet s DPH
            result.Append(dispatchS3.SoucetCenaSDPHDec, 10, 2);


            // Soucet bez DPH snizena
            result.Append(dispatchS3.SoucetCenaBezDPHSnizenaDec, 10, 2);

            // Soucet s DPH snizena
            result.Append(dispatchS3.SoucetCenaSDPHSnizenaDec, 10, 2);

            //Součet bez DPH (2. snížená sazba)	není
            result.Append(0, 10, 2);

            //Součet s DPH (2. snížená sazba) není
            result.Append(0, 10, 2);

            // Soucet bez DPH zakladni
            result.Append(dispatchS3.SoucetCenaBezDPHZakladniDec, 10, 2);

            // Soucet s DPH zakladni
            result.Append(dispatchS3.SoucetCenaSDPHZakladniDec, 10, 2);


            // ID skladu
            result.AppendSpace(10);

            result.AppendNewLine();


            // other lines
            foreach (ExportRow row in expRows)
            {
                using (var db = Core.Data.ConnectionHelper.GetDB())
                {
                    var lot = db.SingleOrDefaultById<StoreMoveLot>(row.moveItem.StoMoveLotID);
                    var pac = db.SingleOrDefaultById<ArticlePacking>(lot.ArtPackID);
                    var article = db.SingleOrDefaultById<Article>(pac.ArticleID);

                    // Kod VZP
                    var vzp = "";
                    if (row.xmlData.Extradata == null)
                        vzp = new string(SPACE, 7);
                    else
                    {
                        if (row.xmlData.Extradata.SkupinaZbozi == "1")
                            vzp = row.xmlData.Extradata.KodVZP;
                        else
                            vzp = new string(SPACE, 7);
                    }

                    result.Append(7, vzp);

                    // Nazev
                    result.Append(-60, article.ArticleDesc);

                    // Skupina
                    result.Append(1, row.xmlData.Extradata != null ? row.xmlData.Extradata.SkupinaZbozi : "");

                    // Sazba DPH
                    result.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.SazbaDPH : 0, 5, 2);

                    // Vyr cena
                    result.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.VyrobniCenaDec : 0, 9, 2);

                    // Nakup s DPH
                    result.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.CenaSDPHDec : 0, 9, 2);

                    // Prodej s DPH
                    result.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.ProdejniCenaSDPHDec : 0, 9, 2);

                    // Mnozstvi
                    result.Append(row.moveItem.Quantity, 8, 2);

                    // Sarze
                    result.Append(-20, string.IsNullOrWhiteSpace(lot.BatchNum) ? "?" : lot.BatchNum);

                    // Expirace
                    result.Append("dd.MM.yyyy", lot.ExpirationDate, 10);

                    // Carovy kod
                    result.AppendSpace(20);

                    // Kod dodavatele
                    result.Append(-20, article.ArticleCode);

                    // Druh kodu
                    result.Append("0");

                    // Nakup bez DPH
                    result.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.CenaBezDPHDec : 0, 9, 2);

                    // Certifikat
                    result.AppendSpace(20);


                    result.AppendNewLine();
                }
            }

            NLogger.Debug($"DirectionID: {direction.DirectionID} DocNumber: {direction.DocNumber} {MethodBase.GetCurrentMethod().Name} - exported rows: {result.RowCount}");
            return new ExportResult[] { result };
        }



        public static ExportResult[] PAENIUM(S4.Export.Settings.SettingsExport config, DispatchS3 dispatchS3, Direction direction, ExportRow[] expRows, string invInfo, string orderNumber, out System.Text.Encoding outputEncoding)
        {
            return PENTENIUM(config, dispatchS3, direction, expRows, invInfo, orderNumber, out outputEncoding);
        }



        public static ExportResult[] MEDIOX(S4.Export.Settings.SettingsExport config, DispatchS3 dispatchS3, Direction direction, ExportRow[] expRows, string invInfo, string orderNumber, out System.Text.Encoding outputEncoding, DataTable invoiceItemsTable)
        {
            ExportResult result = new ExportResult(System.Globalization.CultureInfo.InvariantCulture);
            outputEncoding = System.Text.Encoding.GetEncoding("windows-1250");

            result.SetSeparator("|");


            // file name
            result.fileName = String.Format("{0:00000000}.DOD", direction.DocNumber);


            // first line

            // Cislo verze
            result.Append("4");

            // Kod dodavatele
            result.Append(config.SettingExportLekarny.DodavatelDIC);

            // cislo objednavky
            result.Append("");

            // cislo DL
            // 100223
            result.Append(direction.DocNumber);
            //result.Append(invInfo);

            // datum
            result.Append("yyyyMMdd", direction.DocDate);

            // ICO odberatele
            string custIDNum = dispatchS3.Customer.Extradata != null && !string.IsNullOrWhiteSpace(dispatchS3.Customer.Extradata.ICO) ? dispatchS3.Customer.Extradata.ICO.ToString() : "";
            result.Append(custIDNum);

            // pocet polozek
            result.Append(expRows.Length);

            // Soucet bez DPH
            result.Append(SoucetInv(invoiceItemsTable, G3_TAMOUNTWITHOUTVAT, 0), 10, 2);
            //result.Append(SoucetBezDPH(expRows, 0), 10, 2);

            // Soucet s DPH
            result.Append(SoucetInv(invoiceItemsTable, G3_TAMOUNT, 0), 10, 2);
            //result.Append(SoucetSDPH(expRows, 0), 10, 2);

            // Soucet bez DPH 5
            result.Append(SoucetInv(invoiceItemsTable, G3_TAMOUNTWITHOUTVAT, DPH_1), 10, 2);
            //result.Append(SoucetBezDPH(expRows, DPH_1), 10, 2);

            // Soucet s DPH 5
            result.Append(SoucetInv(invoiceItemsTable, G3_TAMOUNT, DPH_1), 10, 2);
            //result.Append(SoucetSDPH(expRows, DPH_1), 10, 2);

            // Soucet bez DPH 19
            result.Append(SoucetInv(invoiceItemsTable, G3_TAMOUNTWITHOUTVAT, DPH_2), 10, 2);
            //result.Append(SoucetBezDPH(expRows, DPH_2), 10, 2);

            // Soucet s DPH 19
            result.Append(SoucetInv(invoiceItemsTable, G3_TAMOUNT, DPH_2), 10, 2);
            //result.Append(SoucetSDPH(expRows, DPH_2), 10, 2);

            result.AppendNewLine();



            // other lines
            foreach (ExportRow row in expRows)
            {
                using (var db = Core.Data.ConnectionHelper.GetDB())
                {
                    var lot = db.SingleOrDefaultById<StoreMoveLot>(row.moveItem.StoMoveLotID);
                    var pac = db.SingleOrDefaultById<ArticlePacking>(lot.ArtPackID);
                    var article = db.SingleOrDefaultById<Article>(pac.ArticleID);

                    // get prices
                    decimal cenaFaSDPH = 0, cenaFaBezDPH = 0;
                    _findInvPrice(invoiceItemsTable, article.ArticleCode, ref cenaFaSDPH, ref cenaFaBezDPH);


                    // Kod PDK
                    // 100527 - alespon nejaky kod, kdyz je PDK prazdne
                    result.Append(GetPDK(row.xmlData.Extradata, article.ArticleCode));
                    //result.Append((row.xmlData.skupinaZbozi == "1") ? row.xmlData.kodPDK : row.moveItem.Article.articleCode);
                    //result.Append((row.xmlData.skupinaZbozi == "1") ? row.xmlData.kodVZP : row.moveItem.Article.articleCode);


                    // Mnozstvi
                    result.Append(row.moveItem.Quantity, 12, 2);

                    // Vyr cena
                    result.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.VyrobniCenaDec : 0, 12, 2);

                    // Nakup bez DPH
                    result.Append(cenaFaBezDPH, 12, 2);
                    //result.Append(row.xmlData.cenaBezDPH, 12, 2);

                    // Nakup s DPH
                    result.Append(cenaFaSDPH, 12, 2);
                    //result.Append(row.xmlData.cenaSDPH, 12, 2);

                    // sazba DPH
                    result.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.SazbaDPH : 0, 4, 1);

                    // Prodej s DPH
                    result.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.ProdejniCenaSDPHDec : 0, 12, 2);

                    // Sarze
                    result.Append(-12, string.IsNullOrWhiteSpace(lot.BatchNum) ? "?" : lot.BatchNum);

                    // Expirace
                    result.Append("yyyyMMdd", lot.ExpirationDate);

                    // kod APA
                    result.Append("");

                    // Nazev
                    result.Append(article.ArticleDesc);

                    // carovy kod
                    result.Append("");


                    result.AppendNewLine();
                }
            }

            NLogger.Debug($"DirectionID: {direction.DirectionID} DocNumber: {direction.DocNumber} {MethodBase.GetCurrentMethod().Name} - exported rows: {result.RowCount}");
            return new ExportResult[] { result };
        }



        public static ExportResult[] PENTENIUM(S4.Export.Settings.SettingsExport config, DispatchS3 dispatchS3, Direction direction, ExportRow[] expRows, string invInfo, string orderNumber, out System.Text.Encoding outputEncoding)
        {
            // zahlavi
            ExportResult resultZahlavi = new ExportResult(System.Globalization.CultureInfo.InvariantCulture);
            outputEncoding = System.Text.Encoding.GetEncoding("windows-1250");


            // file name
            resultZahlavi.fileName = "ZAHLAVI.TXT";

            // DOD_LIST
            resultZahlavi.Append(-15, invInfo);

            // ICO_DOD
            resultZahlavi.Append(-15, config.SettingExportLekarny.DodavatelICO);

            // DIC_DOD
            resultZahlavi.Append(-15, config.SettingExportLekarny.DodavatelDIC);

            // SOUBOR
            resultZahlavi.Append(-12, "POLOZKY.TXT");

            // ICO_ODBER
            string custIDNum = dispatchS3.Customer.Extradata != null && !string.IsNullOrWhiteSpace(dispatchS3.Customer.Extradata.ICO) ? dispatchS3.Customer.Extradata.ICO.ToString() : "";
            resultZahlavi.Append(-15, custIDNum);

            resultZahlavi.AppendNewLine();



            // polozky

            ExportResult resultPolozky = new ExportResult(System.Globalization.CultureInfo.InvariantCulture);

            // file name
            resultPolozky.fileName = "POLOZKY.TXT";


            // lines
            foreach (ExportRow row in expRows)
            {
                using (var db = Core.Data.ConnectionHelper.GetDB())
                {
                    var lot = db.SingleOrDefaultById<StoreMoveLot>(row.moveItem.StoMoveLotID);
                    var pac = db.SingleOrDefaultById<ArticlePacking>(lot.ArtPackID);
                    var article = db.SingleOrDefaultById<Article>(pac.ArticleID);

                    // KOD
                    var vzp = "";
                    if (row.xmlData.Extradata == null)
                        vzp = new string(SPACE, 7);
                    else
                    {
                        if (row.xmlData.Extradata.SkupinaZbozi == "1")
                            vzp = row.xmlData.Extradata.KodVZP;
                        else
                            vzp = new string(SPACE, 7);
                    }

                    resultPolozky.Append(-13, vzp);

                    // CISELNIK
                    resultPolozky.Append(1, (row.xmlData.Extradata != null && row.xmlData.Extradata.SkupinaZbozi.Equals("1")) ? "1" : "?");

                    // Nazev
                    resultPolozky.Append(-50, article.ArticleDesc);

                    // Sazba DPH
                    resultPolozky.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.SazbaDPH : 0, 4, 1);

                    // Sarze
                    resultPolozky.Append(-10, string.IsNullOrWhiteSpace(lot.BatchNum) ? "?" : lot.BatchNum);

                    // Expirace
                    resultPolozky.Append("dd/MM/yyyy", lot.ExpirationDate, 10);

                    // Mnozstvi
                    resultPolozky.Append(row.moveItem.Quantity, 10, 2);

                    // Vyr cena
                    resultPolozky.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.VyrobniCenaDec * row.moveItem.Quantity : 0, 10, 2);

                    // Nakup bez DPH
                    resultPolozky.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.CenaBezDPHDec * row.moveItem.Quantity : 0, 10, 2);

                    // Nakup s DPH
                    resultPolozky.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.CenaSDPHDec * row.moveItem.Quantity : 0, 10, 2);

                    // DPH_DOD
                    resultPolozky.Append(row.xmlData.Extradata != null ? (row.xmlData.Extradata.CenaSDPHDec * row.moveItem.Quantity) - (row.xmlData.Extradata.CenaBezDPHDec * row.moveItem.Quantity) : 0, 10, 2);

                    // MARZE DOD
                    resultPolozky.AppendSpace(4);

                    // Prodej s DPH
                    resultPolozky.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.ProdejniCenaSDPHDec : 0, 10, 2);

                    //EAN
                    resultPolozky.AppendSpace(20);



                    resultPolozky.AppendNewLine();
                }
            }

            NLogger.Debug($"DirectionID: {direction.DirectionID} DocNumber: {direction.DocNumber} {MethodBase.GetCurrentMethod().Name} - exported rows: {resultPolozky.RowCount}");
            // return results
            return new ExportResult[] { resultZahlavi, resultPolozky };
        }



        public static ExportResult[] PDK5(S4.Export.Settings.SettingsExport config, DispatchS3 dispatchS3, Direction direction, ExportRow[] expRows, string invInfo, string orderNumber, out System.Text.Encoding outputEncoding, DataTable invoiceItemsTable)
        {
            //TODO SoucetInv


            ExportResult result = new ExportResult(System.Globalization.CultureInfo.InvariantCulture);
            outputEncoding = System.Text.Encoding.GetEncoding(852);

            result.SetSeparator("|");


            // file name
            result.fileName = String.Format("{0:00000000}.DOD", direction.DocNumber);


            // first line

            // Cislo verze
            result.Append("5");

            // Kod dodavatele
            result.Append(config.SettingExportLekarny.DodavatelICO);

            // cislo objednavky
            result.Append(orderNumber == null ? "" : orderNumber);

            // cislo DL
            result.Append(direction.DocNumber);
            //result.Append(invInfo);

            // datum
            result.Append("yyyyMMdd", direction.DocDate);

            // ICO odberatele
            string custIDNum = dispatchS3.Customer.Extradata != null && !string.IsNullOrWhiteSpace(dispatchS3.Customer.Extradata.ICO) ? dispatchS3.Customer.Extradata.ICO.ToString() : "";
            result.Append(custIDNum);

            // pocet polozek
            result.Append(expRows.Length);

            // Soucet bez DPH
            result.Append(dispatchS3.SoucetCenaBezDPHDec, 10, 2);

            // Soucet s DPH
            result.Append(dispatchS3.SoucetCenaSDPHDec, 10, 2);

            // Soucet bez DPH snizena
            result.Append(dispatchS3.SoucetCenaBezDPHSnizenaDec, 10, 2);

            // Soucet s DPH snizena
            result.Append(dispatchS3.SoucetCenaSDPHSnizenaDec, 10, 2);

            // Soucet bez DPH zakladni
            result.Append(dispatchS3.SoucetCenaBezDPHZakladniDec, 10, 2);

            // Soucet s DPH zakladni
            result.Append(dispatchS3.SoucetCenaSDPHZakladniDec, 10, 2);

            // Datum dodani
            result.Append(String.Empty);

            // Misto dodani
            result.Append(String.Empty);

            // Druh objednavky
            result.Append("0");

            // Transfer firma
            result.Append(String.Empty);

            // Transfer zastupce
            result.Append(String.Empty);

            result.AppendNewLine();



            // other lines
            foreach (ExportRow row in expRows)
            {
                using (var db = Core.Data.ConnectionHelper.GetDB())
                {
                    var lot = db.SingleOrDefaultById<StoreMoveLot>(row.moveItem.StoMoveLotID);
                    var pac = db.SingleOrDefaultById<ArticlePacking>(lot.ArtPackID);
                    var article = db.SingleOrDefaultById<Article>(pac.ArticleID);

                    // get prices
                    decimal cenaFaSDPH = 0, cenaFaBezDPH = 0;
                    _findInvPrice(invoiceItemsTable, article.ArticleCode, ref cenaFaSDPH, ref cenaFaBezDPH);
                    //_findInvPrice(invoiceItemsTable, row.xmlData.kodPDK, ref cenaFaSDPH, ref cenaFaBezDPH);


                    // Kod zbozi PDK
                    result.Append(GetPDK(row.xmlData.Extradata, article.ArticleCode));
                    //result.Append(row.xmlData.kodPDK);

                    // Mnozstvi
                    result.Append(row.moveItem.Quantity, 12, 2);

                    // Vyr cena
                    result.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.VyrobniCenaDec : 0, 12, 2);

                    // Nakup bez DPH
                    result.Append(cenaFaBezDPH, 12, 2);
                    //result.Append(row.xmlData.cenaBezDPH, 12, 2);

                    // Nakup s DPH
                    result.Append(cenaFaSDPH, 12, 2);
                    //result.Append(row.xmlData.cenaSDPH, 12, 2);

                    // sazba DPH
                    result.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.SazbaDPH : 0, 4, 1);

                    // Prodej s DPH
                    result.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.ProdejniCenaSDPHDec : 0, 12, 2);

                    // Sarze
                    result.Append(-12, string.IsNullOrWhiteSpace(lot.BatchNum) ? "?" : lot.BatchNum);

                    // Expirace
                    result.Append("yyyyMMdd", lot.ExpirationDate);

                    // kod APA
                    result.Append("");

                    // Nazev
                    result.Append(article.ArticleDesc);

                    // carovy kod
                    result.Append("");


                    result.AppendNewLine();
                }
            }

            NLogger.Debug($"DirectionID: {direction.DirectionID} DocNumber: {direction.DocNumber} {MethodBase.GetCurrentMethod().Name} - exported rows: {result.RowCount}");
            return new ExportResult[] { result };
        }
               
        public static ExportResult[] PDK4(S4.Export.Settings.SettingsExport config, DispatchS3 dispatchS3, Direction direction, ExportRow[] expRows, string invInfo, string orderNumber, out System.Text.Encoding outputEncoding, DataTable invoiceItemsTable)
        {
            //TODO SoucetInv

            ExportResult result = new ExportResult(System.Globalization.CultureInfo.InvariantCulture);
            outputEncoding = System.Text.Encoding.GetEncoding(852);

            result.SetSeparator("|");


            // file name
            result.fileName = String.Format("{0:00000000}.DOD", direction.DocNumber);


            // first line

            // Cislo verze
            result.Append("4");

            // Kod dodavatele
            result.Append(config.SettingExportLekarny.DodavatelICO);

            // cislo objednavky
            result.Append(orderNumber == null ? "" : orderNumber);

            // cislo DL
            result.Append(direction.DocNumber);
            //result.Append(invInfo);

            // datum
            result.Append("yyyyMMdd", direction.DocDate);

            // ICO odberatele
            string custIDNum = dispatchS3.Customer.Extradata != null && !string.IsNullOrWhiteSpace(dispatchS3.Customer.Extradata.ICO) ? dispatchS3.Customer.Extradata.ICO.ToString() : "";
            result.Append(custIDNum);

            // pocet polozek
            result.Append(expRows.Length);

            // Soucet bez DPH
            result.Append(dispatchS3.SoucetCenaBezDPHDec, 10, 2);

            // Soucet s DPH
            result.Append(dispatchS3.SoucetCenaSDPHDec, 10, 2);

            // Soucet bez DPH snizena
            result.Append(dispatchS3.SoucetCenaBezDPHSnizenaDec, 10, 2);

            // Soucet s DPH snizena
            result.Append(dispatchS3.SoucetCenaSDPHSnizenaDec, 10, 2);

            // Soucet bez DPH zakladni
            result.Append(dispatchS3.SoucetCenaBezDPHZakladniDec, 10, 2);

            // Soucet s DPH zakladni
            result.Append(dispatchS3.SoucetCenaSDPHZakladniDec, 10, 2);

            result.AppendNewLine();



            // other lines
            foreach (ExportRow row in expRows)
            {
                using (var db = Core.Data.ConnectionHelper.GetDB())
                {
                    var lot = db.SingleOrDefaultById<StoreMoveLot>(row.moveItem.StoMoveLotID);
                    var pac = db.SingleOrDefaultById<ArticlePacking>(lot.ArtPackID);
                    var article = db.SingleOrDefaultById<Article>(pac.ArticleID);

                    // get prices
                    decimal cenaFaSDPH = 0, cenaFaBezDPH = 0;
                    _findInvPrice(invoiceItemsTable, article.ArticleCode, ref cenaFaSDPH, ref cenaFaBezDPH);
                    //_findInvPrice(invoiceItemsTable, row.xmlData.kodPDK, ref cenaFaSDPH, ref cenaFaBezDPH);


                    // Kod zbozi PDK
                    result.Append(GetPDK(row.xmlData.Extradata, article.ArticleCode));
                    //result.Append(row.xmlData.kodPDK);

                    // Mnozstvi
                    result.Append(row.moveItem.Quantity, 12, 2);

                    // Vyr cena
                    result.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.VyrobniCenaDec : 0, 12, 2);

                    // Nakup bez DPH
                    result.Append(cenaFaBezDPH, 12, 2);
                    //result.Append(row.xmlData.cenaBezDPH, 12, 2);

                    // Nakup s DPH
                    result.Append(cenaFaSDPH, 12, 2);
                    //result.Append(row.xmlData.cenaSDPH, 12, 2);

                    // sazba DPH
                    result.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.SazbaDPH : 0, 4, 1);

                    // Prodej s DPH
                    result.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.ProdejniCenaSDPHDec : 0, 12, 2);

                    // Sarze
                    result.Append(-12, string.IsNullOrWhiteSpace(lot.BatchNum) ? "?" : lot.BatchNum);

                    // Expirace
                    result.Append("yyyyMMdd", lot.ExpirationDate);

                    // kod APA
                    result.Append("");

                    // Nazev
                    result.Append(article.ArticleDesc);

                    // carovy kod
                    result.Append("");


                    result.AppendNewLine();
                }
            }

            NLogger.Debug($"DirectionID: {direction.DirectionID} DocNumber: {direction.DocNumber} {MethodBase.GetCurrentMethod().Name} - exported rows: {result.RowCount}");
            return new ExportResult[] { result };
        }
        

        public static ExportResult[] PDK8(S4.Export.Settings.SettingsExport config, DispatchS3 dispatchS3, Direction direction, ExportRow[] expRows, string invInfo, string orderNumber, out System.Text.Encoding outputEncoding, DataTable invoiceItemsTable)
        {
            //TODO SoucetInv


            ExportResult result = new ExportResult(System.Globalization.CultureInfo.InvariantCulture);
            outputEncoding = System.Text.Encoding.GetEncoding(852);

            result.SetSeparator("|");


            // file name
            result.fileName = String.Format("{0:00000000}.DOD", direction.DocNumber);


            // first line

            // Cislo verze
            result.Append("8");

            // Kod dodavatele
            result.Append(config.SettingExportLekarny.DodavatelICO);

            // cislo objednavky
            result.Append(orderNumber == null ? "" : orderNumber);

            // cislo DL
            result.Append(direction.DocNumber);
            //result.Append(invInfo);

            // datum
            result.Append("yyyyMMdd", direction.DocDate);

            // ICO odberatele
            string custIDNum = dispatchS3.Customer.Extradata != null && !string.IsNullOrWhiteSpace(dispatchS3.Customer.Extradata.ICO) ? dispatchS3.Customer.Extradata.ICO.ToString() : "";
            result.Append(custIDNum);

            // pocet polozek
            result.Append(expRows.Length);

            // Soucet bez DPH
            result.Append(dispatchS3.SoucetCenaBezDPHDec, 10, 2);

            // Soucet s DPH
            result.Append(dispatchS3.SoucetCenaSDPHDec, 10, 2);


            //datum dodání	12	"požadované datum (datum a čas) dodání
            //- převzato z objednávky"	dle objednávky
            result.Append(String.Empty);

            //místo dodání	20	- převzato z objednávky	dle objednávky
            result.Append(String.Empty);

            //druh objednávky	1	- převzato z objednávky *	dle objednávky
            result.Append("0");

            //transfer firma	16	"kód firmy, která zadala transfer objednávku
            //- povinné u transfer objednávky"	dle objednávky
            result.Append(String.Empty);

            //transfer zástupce	16	"kód zástupce, který zadala transfer objednávku
            //- povinné u transfer objednávky"	dle objednávky
            result.Append(String.Empty);

            //id akce	16	"identifikační kód akce (transferu) 
            //- převzato z objednávky"	dle objednávky
            result.Append(String.Empty);

            //počet sazeb DPH	1	"počet dále uvedených trojic údajů pro jednotlivé sazby 
            //DPH"	ano
            result.Append("3");


            //výše sazby DPH	4, 1	výše sazby DPH platná pro následující 2 pole	ano
            result.Append(DPH_SNIZENA_1);

            // Soucet bez DPH snizena
            result.Append(dispatchS3.SoucetCenaBezDPHSnizenaDec, 10, 2);

            // Soucet s DPH snizena
            result.Append(dispatchS3.SoucetCenaSDPHSnizenaDec, 10, 2);


            //výše sazby DPH	4, 1	výše sazby DPH platná pro následující 2 pole	ano
            result.Append(0); //není

            //součet bez DPH	10, 2	součet bez DPH pro sazbu DPH v předchozím poli	ano
            result.Append(0, 10, 2); //není

            //součet včetně DPH	10, 2	součet včetně DPH pro sazbu DPH v předpředchozím poli	ano
            result.Append(0, 10, 2); //není


            //výše sazby DPH	4, 1	výše sazby DPH platná pro následující 2 pole	ano
            result.Append(DPH_ZAKLADNI);

            // Soucet bez DPH zakladni
            result.Append(dispatchS3.SoucetCenaBezDPHZakladniDec, 10, 2);

            // Soucet s DPH zakladni
            result.Append(dispatchS3.SoucetCenaSDPHZakladniDec, 10, 2);


            result.AppendNewLine();



            // other lines
            foreach (ExportRow row in expRows)
            {
                using (var db = Core.Data.ConnectionHelper.GetDB())
                {
                    var lot = db.SingleOrDefaultById<StoreMoveLot>(row.moveItem.StoMoveLotID);
                    var pac = db.SingleOrDefaultById<ArticlePacking>(lot.ArtPackID);
                    var article = db.SingleOrDefaultById<Article>(pac.ArticleID);

                    // get prices
                    decimal cenaFaSDPH = 0, cenaFaBezDPH = 0;
                    _findInvPrice(invoiceItemsTable, article.ArticleCode, ref cenaFaSDPH, ref cenaFaBezDPH);
                    //_findInvPrice(invoiceItemsTable, row.xmlData.kodPDK, ref cenaFaSDPH, ref cenaFaBezDPH);

                    // Kod zbozi PDK
                    result.Append(GetPDK(row.xmlData.Extradata, article.ArticleCode));
                    //result.Append(row.xmlData.kodPDK);

                    // Mnozstvi
                    result.Append(row.moveItem.Quantity, 12, 2);

                    // Vyr cena
                    result.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.VyrobniCenaDec : 0, 12, 2);

                    // Nakup bez DPH
                    result.Append(cenaFaBezDPH, 12, 2);
                    //result.Append(row.xmlData.cenaBezDPH, 12, 2);

                    // Nakup s DPH
                    result.Append(cenaFaSDPH, 12, 2);
                    //result.Append(row.xmlData.cenaSDPH, 12, 2);

                    // sazba DPH
                    result.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.SazbaDPH : 0, 4, 1);

                    // Prodej s DPH
                    result.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.ProdejniCenaSDPHDec : 0, 12, 2);

                    // Sarze
                    result.Append(-12, string.IsNullOrWhiteSpace(lot.BatchNum) ? "?" : lot.BatchNum);

                    // Expirace
                    result.Append("yyyyMMdd", lot.ExpirationDate);

                    // kod APA
                    result.Append("");

                    // Nazev
                    result.Append(article.ArticleDesc);

                    // carovy kod
                    result.Append("");


                    result.AppendNewLine();
                }
            }

            NLogger.Debug($"DirectionID: {direction.DirectionID} DocNumber: {direction.DocNumber} {MethodBase.GetCurrentMethod().Name} - exported rows: {result.RowCount}");
            return new ExportResult[] { result };
        }
        

        public static ExportResult[] PDKX(S4.Export.Settings.SettingsExport config, DispatchS3 dispatchS3, Direction direction, ExportRow[] expRows, string invInfo, string orderNumber, out System.Text.Encoding outputEncoding, DataTable invoiceItemsTable)
        {
            //TODO SoucetInv


            ExportResult result = new ExportResult(System.Globalization.CultureInfo.InvariantCulture);
            outputEncoding = System.Text.Encoding.GetEncoding(852);

            result.SetSeparator("|");


            // file name
            result.fileName = String.Format("{0:00000000}.DOD", direction.DocNumber);


            // first line

            // Cislo verze
            result.Append("X");

            // Kod dodavatele
            result.Append(config.SettingExportLekarny.DodavatelICO);

            // cislo objednavky
            result.Append(orderNumber == null ? "" : orderNumber);

            // cislo DL
            result.Append(direction.DocNumber);
            //result.Append(invInfo);

            // datum
            result.Append("yyyyMMdd", direction.DocDate);

            // ICO odberatele
            string custIDNum = dispatchS3.Customer.Extradata != null && !string.IsNullOrWhiteSpace(dispatchS3.Customer.Extradata.ICO) ? dispatchS3.Customer.Extradata.ICO.ToString() : "";
            result.Append(custIDNum);

            // pocet polozek
            result.Append(expRows.Length);

            // Soucet bez DPH
            result.Append(dispatchS3.SoucetCenaBezDPHDec, 10, 2);

            // Soucet s DPH
            result.Append(dispatchS3.SoucetCenaSDPHDec, 10, 2);

            // Soucet bez DPH snizena
            result.Append(dispatchS3.SoucetCenaBezDPHSnizenaDec, 10, 2);

            // Soucet s DPH snizena
            result.Append(dispatchS3.SoucetCenaSDPHSnizenaDec, 10, 2);

            // Soucet bez DPH zakladni
            result.Append(dispatchS3.SoucetCenaBezDPHZakladniDec, 10, 2);

            // Soucet s DPH zakladni
            result.Append(dispatchS3.SoucetCenaSDPHZakladniDec, 10, 2);

            // Datum dodani
            result.Append(String.Empty);

            // Misto dodani
            result.Append(String.Empty);

            // Druh objednavky
            result.Append("0");

            // Transfer firma
            result.Append(String.Empty);

            // Transfer zastupce
            result.Append(String.Empty);

            result.AppendNewLine();



            // other lines
            foreach (ExportRow row in expRows)
            {
                using (var db = Core.Data.ConnectionHelper.GetDB())
                {
                    var lot = db.SingleOrDefaultById<StoreMoveLot>(row.moveItem.StoMoveLotID);
                    var pac = db.SingleOrDefaultById<ArticlePacking>(lot.ArtPackID);
                    var article = db.SingleOrDefaultById<Article>(pac.ArticleID);

                    // get prices
                    decimal cenaFaSDPH = 0, cenaFaBezDPH = 0;
                    _findInvPrice(invoiceItemsTable, article.ArticleCode, ref cenaFaSDPH, ref cenaFaBezDPH);
                    //_findInvPrice(invoiceItemsTable, row.xmlData.kodPDK, ref cenaFaSDPH, ref cenaFaBezDPH);


                    // Kod zbozi PDK
                    result.Append(GetPDK(row.xmlData.Extradata, article.ArticleCode));
                    //result.Append(row.xmlData.kodPDK);

                    // Mnozstvi
                    result.Append(row.moveItem.Quantity, 12, 2);

                    // Vyr cena
                    result.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.VyrobniCenaDec : 0, 12, 2);

                    // Nakup bez DPH
                    result.Append(cenaFaBezDPH, 12, 2);
                    //result.Append(row.xmlData.cenaBezDPH, 12, 2);

                    // Nakup s DPH
                    result.Append(cenaFaSDPH, 12, 2);
                    //result.Append(row.xmlData.cenaSDPH, 12, 2);

                    // sazba DPH
                    result.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.SazbaDPH : 0, 4, 1);

                    // Prodej s DPH
                    result.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.ProdejniCenaSDPHDec : 0, 12, 2);

                    // Sarze
                    result.Append(-12, string.IsNullOrWhiteSpace(lot.BatchNum) ? "?" : lot.BatchNum);

                    // Exspirace(8) Nepovinná(RRRRMMDD) 
                    result.Append("yyyyMMdd", lot.ExpirationDate);

                    // Kód APA(10) Nepovinný, interní kód šarže zboží z centrálního skladu pro „FaRMIS - ID“ 
                    result.Append("");

                    // Název(50) Povinný 
                    result.Append(article.ArticleDesc);

                    // Kód EAN(16) Povinný 
                    result.Append("");

                    //Kód ZP(8) Nepovinný(tvar Cxxxxxxx, kde C 1 - HVLP, 2 - IPLP, 3 - PZT, U - AUV) 
                    result.Append("");

                    //Kód dodavatelský(20) Povinný – interní kód zboží z centrálního skladu
                    result.Append(article.ArticleCode);

                    result.AppendNewLine();
                }
            }

            NLogger.Debug($"DirectionID: {direction.DirectionID} DocNumber: {direction.DocNumber} {MethodBase.GetCurrentMethod().Name} - exported rows: {result.RowCount}");
            return new ExportResult[] { result };
        }

        public static ExportResult[] PDK12(S4.Export.Settings.SettingsExport config, DispatchS3 dispatchS3, Direction direction, ExportRow[] expRows, string invInfo, string orderNumber, out System.Text.Encoding outputEncoding, DataTable invoiceItemsTable)
        {
            //TODO SoucetInv


            ExportResult result = new ExportResult(System.Globalization.CultureInfo.InvariantCulture);
            outputEncoding = System.Text.Encoding.GetEncoding(852);

            result.SetSeparator("|");


            // file name
            result.fileName = String.Format("{0:00000000}.DOD", direction.DocNumber);


            // first line

            // Cislo verze
            result.Append("12");

            // Kod dodavatele
            result.Append(config.SettingExportLekarny.DodavatelICO);

            // cislo objednavky
            result.Append(orderNumber == null ? "" : orderNumber);

            // cislo DL
            result.Append(direction.DocNumber);
            //result.Append(invInfo);

            // datum
            result.Append("yyyyMMdd", direction.DocDate);

            // ICO odberatele
            string custIDNum = dispatchS3.Customer.Extradata != null && !string.IsNullOrWhiteSpace(dispatchS3.Customer.Extradata.ICO) ? dispatchS3.Customer.Extradata.ICO.ToString() : "";
            result.Append(custIDNum);

            // pocet polozek
            result.Append(expRows.Length);

            // Soucet bez DPH
            result.Append(dispatchS3.SoucetCenaBezDPHDec, 10, 2);

            // Soucet s DPH
            result.Append(dispatchS3.SoucetCenaSDPHDec, 10, 2);


            //datum dodání	12	"požadované datum (datum a èas) dodání
            //- pøevzato z objednávky"	dle objednávky
            result.Append(String.Empty);

            //místo dodání	20	- pøevzato z objednávky	dle objednávky
            result.Append(String.Empty);

            //druh objednávky	1	- pøevzato z objednávky *	dle objednávky
            result.Append("0");

            //transfer firma	16	"kód firmy, která zadala transfer objednávku
            //- povinné u transfer objednávky"	dle objednávky
            result.Append(String.Empty);

            //transfer zástupce	16	"kód zástupce, který zadala transfer objednávku
            //- povinné u transfer objednávky"	dle objednávky
            result.Append(String.Empty);

            //id akce	16	"identifikaèní kód akce (transferu) 
            //- pøevzato z objednávky"	dle objednávky
            result.Append(String.Empty);

            //poèet sazeb DPH	1	"poèet dále uvedených trojic údajù pro jednotlivé sazby 
            //DPH"	ano
            result.Append("3");


            //výše sazby DPH	4, 1	výše sazby DPH platná pro následující 2 pole	ano
            result.Append(DPH_SNIZENA_1);

            // Soucet bez DPH snizena
            result.Append(dispatchS3.SoucetCenaBezDPHSnizenaDec, 10, 2);

            // Soucet s DPH snizena
            result.Append(dispatchS3.SoucetCenaSDPHSnizenaDec, 10, 2);

            //výše sazby DPH	4, 1	výše sazby DPH platná pro následující 2 pole	ano
            result.Append(0);

            //souèet bez DPH	10, 2	souèet bez DPH pro sazbu DPH v pøedchozím poli	ano
            result.Append(0, 10, 2); //není

            //souèet vèetnì DPH	10, 2	souèet vèetnì DPH pro sazbu DPH v pøedpøedchozím poli	ano
            result.Append(0, 10, 2); //není

            //výše sazby DPH	4, 1	výše sazby DPH platná pro následující 2 pole	ano
            result.Append(DPH_ZAKLADNI);

            // Soucet bez DPH zakladni
            result.Append(dispatchS3.SoucetCenaBezDPHZakladniDec, 10, 2);

            // Soucet s DPH zakladni
            result.Append(dispatchS3.SoucetCenaSDPHZakladniDec, 10, 2);


            result.AppendNewLine();



            // other lines
            foreach (ExportRow row in expRows)
            {
                using (var db = Core.Data.ConnectionHelper.GetDB())
                {
                    var lot = db.SingleOrDefaultById<StoreMoveLot>(row.moveItem.StoMoveLotID);
                    var pac = db.SingleOrDefaultById<ArticlePacking>(lot.ArtPackID);
                    var article = db.SingleOrDefaultById<Article>(pac.ArticleID);

                    // get prices
                    decimal cenaFaSDPH = 0, cenaFaBezDPH = 0;
                    _findInvPrice(invoiceItemsTable, article.ArticleCode, ref cenaFaSDPH, ref cenaFaBezDPH);
                    //_findInvPrice(invoiceItemsTable, row.xmlData.kodPDK, ref cenaFaSDPH, ref cenaFaBezDPH);


                    // Kod zbozi PDK
                    // 100527 - alespon nejaky kod, kdyz je PDK prazdne
                    result.Append(GetPDK(row.xmlData.Extradata, article.ArticleCode));
                    //result.Append(row.xmlData.kodPDK);

                    // Mnozstvi
                    result.Append(row.moveItem.Quantity, 12, 2);

                    // Vyr cena
                    result.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.VyrobniCenaDec : 0, 12, 2);

                    // Nakup bez DPH
                    result.Append(cenaFaBezDPH, 12, 2);
                    //result.Append(row.xmlData.cenaBezDPH, 12, 2);

                    // Nakup s DPH
                    result.Append(cenaFaSDPH, 12, 2);
                    //result.Append(row.xmlData.cenaSDPH, 12, 2);

                    // sazba DPH
                    result.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.SazbaDPH : 0, 4, 1);

                    // Prodej s DPH
                    result.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.ProdejniCenaSDPHDec : 0, 12, 2);

                    // Sarze
                    result.Append(-12, string.IsNullOrWhiteSpace(lot.BatchNum) ? "?" : lot.BatchNum);

                    // Expirace
                    result.Append("yyyyMMdd", lot.ExpirationDate);

                    // kod APA
                    result.Append("");

                    // Nazev
                    result.Append(article.ArticleDesc);

                    // 12 carovy kod
                    result.Append("");

                    // 13 Certifikát surovin 25 Údaj o certifikátu surovin / jen pro suroviny Ne
                    result.Append("");

                    // 14 Číslo objednávky 12 Číslo objednávky u odběratele, převzato z objednávky, má přednost před tímtéž údajem v hlavičče.nepovinné *
                    result.Append("");

                    // 15 Transferová firma 16 kód firmy která zadala transferovou objednávku, převzato z objednávky, má přednost před tímtéž údajem v hlavičče. povinné je-li v objednávce
                    result.Append("");

                    // 16 Transferový zástupce 16 kód zástupce který zadal transferovou objednávku, převzato z objednávky, má přednost před tímtéž údajem v hlavičče. povinné je-li v objednávce
                    result.Append("");

                    // 17 ID akce 16 Identifikační kód akce(transferu), převzato z objednávky, má přednost před tímtéž údajem v hlavičče. povinné je-li v objednávce
                    result.Append("");

                    // 18 Pořadové číslo transportní bedny 4 Pořadové číslo transportní bedny, u ostatních typů obalů se neuvádí Ne 
                    result.Append("");

                    // 19 Číslo pozice 6 Odkaz na číslo pozice dodacího listu v IS distributora Ne 
                    result.Append("");

                    // 20 Číslo podpozice 16 Odkaz na číslo podpozice dodacího listu v IS distributora Ne 
                    result.Append("");

                    // 21 EMVS 1 "A”-  Obsahuje ochranný kód,"N" - Neobsahuje ochranný kod, " " - Není známo Ne
                    result.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.ObsahujeOchrannyKod ? "A" : "N" : "");

                    // 22 SKL190209 1 Výrobek byl na skladě před a včetně dne 9. 2. 2019, "A”-  Byl," " Nebyl, není známo 
                    result.Append(" ");


                    result.AppendNewLine();
                }
            }

            NLogger.Debug($"DirectionID: {direction.DirectionID} DocNumber: {direction.DocNumber} {MethodBase.GetCurrentMethod().Name} - exported rows: {result.RowCount}");
            return new ExportResult[] { result };
        }

        public static ExportResult[] PDK15(S4.Export.Settings.SettingsExport config, DispatchS3 dispatchS3, Direction direction, ExportRow[] expRows, string invInfo, string orderNumber, out System.Text.Encoding outputEncoding, DataTable invoiceItemsTable)
        {
            //TODO SoucetInv


            ExportResult result = new ExportResult(System.Globalization.CultureInfo.InvariantCulture);
            outputEncoding = System.Text.Encoding.GetEncoding(852);

            result.SetSeparator("|");


            // file name
            result.fileName = String.Format("{0:00000000}.DOD", direction.DocNumber);


            // first line

            // Cislo verze
            result.Append("15");

            // Kod dodavatele
            result.Append(config.SettingExportLekarny.DodavatelICO);

            // cislo objednavky
            result.Append(orderNumber == null ? "" : orderNumber);

            // cislo DL
            result.Append(direction.DocNumber);
            //result.Append(invInfo);

            // datum
            result.Append("yyyyMMdd", direction.DocDate);

            // ICO odberatele
            string custIDNum = dispatchS3.Customer.Extradata != null && !string.IsNullOrWhiteSpace(dispatchS3.Customer.Extradata.ICO) ? dispatchS3.Customer.Extradata.ICO.ToString() : "";
            result.Append(custIDNum);

            // pocet polozek
            result.Append(expRows.Length);

            // Soucet bez DPH
            result.Append(dispatchS3.SoucetCenaBezDPHDec, 10, 2);

            // Soucet s DPH
            result.Append(dispatchS3.SoucetCenaSDPHDec, 10, 2);

            //datum dodání	12	"požadované datum (datum a èas) dodání
            //- pøevzato z objednávky"	dle objednávky
            result.Append(String.Empty);

            //místo dodání	20	- pøevzato z objednávky	dle objednávky
            result.Append(String.Empty);

            //druh objednávky	1	- pøevzato z objednávky *	dle objednávky
            result.Append("0");

            //transfer firma	16	"kód firmy, která zadala transfer objednávku
            //- povinné u transfer objednávky"	dle objednávky
            result.Append(String.Empty);

            //transfer zástupce	16	"kód zástupce, který zadala transfer objednávku
            //- povinné u transfer objednávky"	dle objednávky
            result.Append(String.Empty);

            //id akce	16	"identifikaèní kód akce (transferu) 
            //- pøevzato z objednávky"	dle objednávky
            result.Append(String.Empty);

            //poèet sazeb DPH	1	"poèet dále uvedených trojic údajù pro jednotlivé sazby 
            //DPH"	ano
            result.Append("3");


            //výše sazby DPH	4, 1	výše sazby DPH platná pro následující 2 pole	ano
            result.Append(DPH_SNIZENA_1);

            // Soucet bez DPH snizena
            result.Append(dispatchS3.SoucetCenaBezDPHSnizenaDec, 10, 2);

            // Soucet s DPH snizena
            result.Append(dispatchS3.SoucetCenaSDPHSnizenaDec, 10, 2);


            //výše sazby DPH	4, 1	výše sazby DPH platná pro následující 2 pole	ano
            result.Append(0); //není

            //souèet bez DPH	10, 2	souèet bez DPH pro sazbu DPH v pøedchozím poli	ano
            result.Append(0, 10, 2); //není

            //souèet vèetnì DPH	10, 2	souèet vèetnì DPH pro sazbu DPH v pøedpøedchozím poli	ano
            result.Append(0, 10, 2); //není


            //výše sazby DPH	4, 1	výše sazby DPH platná pro následující 2 pole	ano
            result.Append(DPH_ZAKLADNI);

            // Soucet bez DPH zakladni
            result.Append(dispatchS3.SoucetCenaBezDPHZakladniDec, 10, 2);

            // Soucet s DPH zakladni
            result.Append(dispatchS3.SoucetCenaSDPHZakladniDec, 10, 2);


            result.AppendNewLine();



            // other lines
            foreach (ExportRow row in expRows)
            {
                using (var db = Core.Data.ConnectionHelper.GetDB())
                {
                    var lot = db.SingleOrDefaultById<StoreMoveLot>(row.moveItem.StoMoveLotID);
                    var pac = db.SingleOrDefaultById<ArticlePacking>(lot.ArtPackID);
                    var article = db.SingleOrDefaultById<Article>(pac.ArticleID);

                    // get prices
                    decimal cenaFaSDPH = 0, cenaFaBezDPH = 0;
                    _findInvPrice(invoiceItemsTable, article.ArticleCode, ref cenaFaSDPH, ref cenaFaBezDPH);
                    //_findInvPrice(invoiceItemsTable, row.xmlData.kodPDK, ref cenaFaSDPH, ref cenaFaBezDPH);


                    // Kod zbozi PDK
                    // 100527 - alespon nejaky kod, kdyz je PDK prazdne
                    result.Append(GetPDK(row.xmlData.Extradata, article.ArticleCode));
                    //result.Append(row.xmlData.kodPDK);

                    // Mnozstvi
                    result.Append(row.moveItem.Quantity, 12, 2);

                    // Vyr cena
                    result.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.VyrobniCenaDec : 0, 12, 2);

                    // Nakup bez DPH
                    result.Append(cenaFaBezDPH, 12, 2);
                    //result.Append(row.xmlData.cenaBezDPH, 12, 2);

                    // Nakup s DPH
                    result.Append(cenaFaSDPH, 12, 2);
                    //result.Append(row.xmlData.cenaSDPH, 12, 2);

                    // sazba DPH
                    result.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.SazbaDPH : 0, 4, 1);

                    // Prodej s DPH
                    result.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.ProdejniCenaSDPHDec : 0, 12, 2);

                    // Sarze
                    result.Append(-12, string.IsNullOrWhiteSpace(lot.BatchNum) ? "?" : lot.BatchNum);

                    // Expirace
                    result.Append("yyyyMMdd", lot.ExpirationDate);

                    // kod APA
                    result.Append("");

                    // Nazev
                    result.Append(article.ArticleDesc);

                    // 12 carovy kod
                    result.Append("");

                    // 13 Certifikát surovin 25 Údaj o certifikátu surovin / jen pro suroviny Ne
                    result.Append("");

                    // 14 Číslo objednávky 12 Číslo objednávky u odběratele, převzato z objednávky, má přednost před tímtéž údajem v hlavičče.nepovinné *
                    result.Append("");

                    // 15 Transferová firma 16 kód firmy která zadala transferovou objednávku, převzato z objednávky, má přednost před tímtéž údajem v hlavičče. povinné je-li v objednávce
                    result.Append("");

                    // 16 Transferový zástupce 16 kód zástupce který zadal transferovou objednávku, převzato z objednávky, má přednost před tímtéž údajem v hlavičče. povinné je-li v objednávce
                    result.Append("");

                    // 17 ID akce 16 Identifikační kód akce(transferu), převzato z objednávky, má přednost před tímtéž údajem v hlavičče. povinné je-li v objednávce
                    result.Append("");

                    // 18 Pořadové číslo transportní bedny 4 Pořadové číslo transportní bedny, u ostatních typů obalů se neuvádí Ne 
                    result.Append("");

                    // 19 Číslo pozice 6 Odkaz na číslo pozice dodacího listu v IS distributora Ne 
                    result.Append("");

                    // 20 Číslo podpozice 16 Odkaz na číslo podpozice dodacího listu v IS distributora Ne 
                    result.Append("");

                    // 21 EMVS 1 "A”-  Obsahuje ochranný kód,"N" - Neobsahuje ochranný kod, " " - Není známo Ne
                    result.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.ObsahujeOchrannyKod ? "A" : "N" : "");

                    // 22 SKL190209 1 Výrobek byl na skladě před a včetně dne 9. 2. 2019, "A”-  Byl," " Nebyl, není známo 
                    result.Append(" ");

                    // 23 Pořadí transportní bedny - Pořadí Bendy k dodávce
                    result.Append("");

                    // 24 Recyklační příspěvek - 
                    result.Append("");

                    // 25 Výše Recyklačního příspěvku - výše recyklačního příspěvku na 1 ks či 1 kg daného elektrozařízení bez DPH
                    result.Append("");


                    result.AppendNewLine();
                }
            }

            NLogger.Debug($"DirectionID: {direction.DirectionID} DocNumber: {direction.DocNumber} {MethodBase.GetCurrentMethod().Name} - exported rows: {result.RowCount}");
            return new ExportResult[] { result };
        }

        public static ExportResult[] PDK18(S4.Export.Settings.SettingsExport config, DispatchS3 dispatchS3, Direction direction, ExportRow[] expRows, string invInfo, string orderNumber, out System.Text.Encoding outputEncoding, DataTable invoiceItemsTable)
        {
            //TODO SoucetInv


            ExportResult result = new ExportResult(System.Globalization.CultureInfo.InvariantCulture);
            outputEncoding = System.Text.Encoding.GetEncoding(852);

            result.SetSeparator("|");


            // file name
            result.fileName = String.Format("{0:00000000}.DOD", direction.DocNumber);


            // first line

            // Cislo verze
            result.Append("18");

            // Kod dodavatele
            result.Append(config.SettingExportLekarny.DodavatelICO);

            // cislo objednavky
            result.Append(orderNumber == null ? "" : orderNumber);

            // cislo DL
            result.Append(direction.DocNumber);
            //result.Append(invInfo);

            // datum
            result.Append("yyyyMMdd", direction.DocDate);

            // ICO odberatele
            string custIDNum = dispatchS3.Customer.Extradata != null && !string.IsNullOrWhiteSpace(dispatchS3.Customer.Extradata.ICO) ? dispatchS3.Customer.Extradata.ICO.ToString() : "";
            result.Append(custIDNum);

            // pocet polozek
            result.Append(expRows.Length);

            // Soucet bez DPH
            result.Append(dispatchS3.SoucetCenaBezDPHDec, 10, 2);

            // Soucet s DPH
            result.Append(dispatchS3.SoucetCenaSDPHDec, 10, 2);

            //datum dodání	12	"požadované datum (datum a èas) dodání
            //- pøevzato z objednávky"	dle objednávky
            result.Append(String.Empty);

            //místo dodání	20	- pøevzato z objednávky	dle objednávky
            result.Append(String.Empty);

            //druh objednávky	1	- pøevzato z objednávky *	dle objednávky
            result.Append("0");

            //transfer firma	16	"kód firmy, která zadala transfer objednávku
            //- povinné u transfer objednávky"	dle objednávky
            result.Append(String.Empty);

            //transfer zástupce	16	"kód zástupce, který zadala transfer objednávku
            //- povinné u transfer objednávky"	dle objednávky
            result.Append(String.Empty);

            //id akce	16	"identifikaèní kód akce (transferu) 
            //- pøevzato z objednávky"	dle objednávky
            result.Append(String.Empty);

            //Číslo veřejné zakázky 36 Číslo veřejné zakázky. Definuje objekt vyhlašující VZ Ne (new verze 17)
            result.Append(String.Empty);

            //poèet sazeb DPH	1	"poèet dále uvedených trojic údajù pro jednotlivé sazby 
            //DPH"	ano
            result.Append("3");


            //výše sazby DPH	4, 1	výše sazby DPH platná pro následující 2 pole	ano
            result.Append(DPH_SNIZENA_1);

            // Soucet bez DPH snizena
            result.Append(dispatchS3.SoucetCenaBezDPHSnizenaDec, 10, 2);

            // Soucet s DPH snizena
            result.Append(dispatchS3.SoucetCenaSDPHSnizenaDec, 10, 2);

            //výše sazby DPH	4, 1	výše sazby DPH platná pro následující 2 pole	ano
            result.Append(0); //není

            //souèet bez DPH	10, 2	souèet bez DPH pro sazbu DPH v pøedchozím poli	ano
            result.Append(0, 10, 2); //není

            //souèet vèetnì DPH	10, 2	souèet vèetnì DPH pro sazbu DPH v pøedpøedchozím poli	ano
            result.Append(0, 10, 2); //není


            //výše sazby DPH	4, 1	výše sazby DPH platná pro následující 2 pole	ano
            result.Append(DPH_ZAKLADNI);

            // Soucet bez DPH zakladni
            result.Append(dispatchS3.SoucetCenaBezDPHZakladniDec, 10, 2);

            // Soucet s DPH zakladni
            result.Append(dispatchS3.SoucetCenaSDPHZakladniDec, 10, 2);

            result.AppendNewLine();



            // other lines
            foreach (ExportRow row in expRows)
            {
                using (var db = Core.Data.ConnectionHelper.GetDB())
                {
                    var lot = db.SingleOrDefaultById<StoreMoveLot>(row.moveItem.StoMoveLotID);
                    var pac = db.SingleOrDefaultById<ArticlePacking>(lot.ArtPackID);
                    var article = db.SingleOrDefaultById<Article>(pac.ArticleID);

                    // get prices
                    decimal cenaFaSDPH = 0, cenaFaBezDPH = 0;
                    _findInvPrice(invoiceItemsTable, article.ArticleCode, ref cenaFaSDPH, ref cenaFaBezDPH);
                    //_findInvPrice(invoiceItemsTable, row.xmlData.kodPDK, ref cenaFaSDPH, ref cenaFaBezDPH);


                    // Kod zbozi PDK
                    // 100527 - alespon nejaky kod, kdyz je PDK prazdne
                    result.Append(GetPDK(row.xmlData.Extradata, article.ArticleCode));
                    //result.Append(row.xmlData.kodPDK);

                    // Mnozstvi
                    result.Append(row.moveItem.Quantity, 12, 2);

                    // Vyr cena
                    result.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.VyrobniCenaDec : 0, 12, 2);

                    // Nakup bez DPH
                    result.Append(cenaFaBezDPH, 12, 2);
                    //result.Append(row.xmlData.cenaBezDPH, 12, 2);

                    // Nakup s DPH
                    result.Append(cenaFaSDPH, 12, 2);
                    //result.Append(row.xmlData.cenaSDPH, 12, 2);

                    // sazba DPH
                    result.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.SazbaDPH : 0, 4, 1);

                    // Prodej s DPH
                    result.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.ProdejniCenaSDPHDec : 0, 12, 2);

                    // Sarze
                    result.Append(-12, string.IsNullOrWhiteSpace(lot.BatchNum) ? "?" : lot.BatchNum);

                    // Expirace
                    result.Append("yyyyMMdd", lot.ExpirationDate);

                    // kod APA
                    result.Append("");

                    // Nazev
                    result.Append(article.ArticleDesc);

                    // 12 carovy kod
                    result.Append("");

                    // 13 Certifikát surovin 25 Údaj o certifikátu surovin / jen pro suroviny Ne
                    result.Append("");

                    // 14 Číslo objednávky 12 Číslo objednávky u odběratele, převzato z objednávky, má přednost před tímtéž údajem v hlavičče.nepovinné *
                    result.Append("");

                    // 15 Transferová firma 16 kód firmy která zadala transferovou objednávku, převzato z objednávky, má přednost před tímtéž údajem v hlavičče. povinné je-li v objednávce
                    result.Append("");

                    // 16 Transferový zástupce 16 kód zástupce který zadal transferovou objednávku, převzato z objednávky, má přednost před tímtéž údajem v hlavičče. povinné je-li v objednávce
                    result.Append("");

                    // 17 ID akce 16 Identifikační kód akce(transferu), převzato z objednávky, má přednost před tímtéž údajem v hlavičče. povinné je-li v objednávce
                    result.Append("");

                    // 18 Pořadové číslo transportní bedny 4 Pořadové číslo transportní bedny, u ostatních typů obalů se neuvádí Ne 
                    result.Append("");

                    // 19 Číslo pozice 6 Odkaz na číslo pozice dodacího listu v IS distributora Ne 
                    result.Append("");

                    // 20 Číslo podpozice 16 Odkaz na číslo podpozice dodacího listu v IS distributora Ne 
                    result.Append("");

                    // 21 EMVS 1 "A”-  Obsahuje ochranný kód,"N" - Neobsahuje ochranný kod, " " - Není známo Ne
                    result.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.ObsahujeOchrannyKod ? "A" : "N" : "");

                    // 22 SKL190209 1 Výrobek byl na skladě před a včetně dne 9. 2. 2019, "A”-  Byl," " Nebyl, není známo 
                    result.Append(" ");

                    // 23 Pořadí transportní bedny - Pořadí bedny k dodávce
                    result.Append("");

                    // 24 Recyklační příspěvek - 
                    result.Append("");

                    // 25 Výše Recyklačního příspěvku - výše recyklačního příspěvku na 1 ks či 1 kg daného elektrozařízení bez DPH
                    result.Append("");

                    // 26 Distribuční poplatek 10,2 Výše distribučního poplatku na jedno baleni Ne (new v.18)
                    result.Append(row.xmlData.Extradata != null ? row.xmlData.Extradata.DistributionSurcharge: String.Empty);

                    result.AppendNewLine();
                }
            }

            NLogger.Debug($"DirectionID: {direction.DirectionID} DocNumber: {direction.DocNumber} {MethodBase.GetCurrentMethod().Name} - exported rows: {result.RowCount}");
            return new ExportResult[] { result };
        }

        #region Helpers

        private static void _findInvPrice(DataTable invoiceItemsTable, string kod, ref decimal nakupSDPH, ref decimal nakupBezDPH)
        {

            DataRow[] result = invoiceItemsTable.Select(String.Format("CODE='{0}'", kod));
            //DataRow[] result = invoiceItemsTable.Select(String.Format("CUSTOMSTARIFFNUMBER='{0}'", kod));
            //DataRow[] result = invoiceItemsTable.Select(String.Format("X_CODE_PDK='{0}'", kodPDK));
            if (result.Length > 0)
            {
                nakupSDPH = decimal.Parse(result[0]["UNITPRICEVAT"].ToString());
                nakupBezDPH = decimal.Parse(result[0]["UNITPRICE"].ToString());
            }
            else
            {
                nakupSDPH = -1;
                nakupBezDPH = -1;
            }
        }


        private static decimal SoucetInv(DataTable invoiceItemsTable, string priceFld, decimal sazbaDPH)
        {
            decimal result = 0;

            foreach (DataRow row in invoiceItemsTable.Rows)
            {
                decimal vat = decimal.Parse(row["VATRATE"].ToString());
                if (sazbaDPH == 0 || sazbaDPH == vat)
                    result += decimal.Parse(row[priceFld].ToString());
            }

            return result;
        }


        private static DateTime? LastDayOfMonth(object val)
        {
            if (val != null && val != DBNull.Value)
            {
                DateTime d = (DateTime)val;
                return new DateTime(d.Year, d.Month, DateTime.DaysInMonth(d.Year, d.Month));
            }
            else
                return null;
        }

        private static string GetPDK(DispatchItemExtradataS3 extraData, string articleCode)
        {
            var pdk = "";
            if (extraData != null)
                pdk = !string.IsNullOrEmpty(extraData.KodPDK) ? extraData.KodPDK : !string.IsNullOrEmpty(extraData.KodSUKL) ? extraData.KodSUKL
                    : !string.IsNullOrEmpty(extraData.KodVZP) ? extraData.KodVZP : !string.IsNullOrEmpty(extraData.ArticleCode) ? extraData.ArticleCode : articleCode;

            return pdk;
        }

        #endregion
    }
}

using S4.StoreCore.Interfaces;
using S4.StoreCore.OnStore_Basic;
using System;
using System.Collections.Generic;
using System.Linq;
using TestHelpers;
using Xunit;

namespace S4.StoreCore.Tests
{
    [Collection("StoreCoreTests_OnStore")]
    [Trait("Category", "StoreCoreTests_OnStore")]
    public class OnStore_Basic_Tests : CoreTestBase
    {

        public OnStore_Basic_Tests(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void OnStore_Basic_ByArticle_single()
        {
            var onStore = new OnStore();

            var result = onStore.ByArticle(ARTICLE1_ID, ResultValidityEnum.Valid);

            Assert.Single(result);
            var r1 = result[0];

            Assert.Equal(ARTICLE1_QUANTITY, r1.Quantity);
            Assert.Equal(POSITION1_ID, r1.PositionID);

            Assert.NotNull(r1.Article);
            Assert.Equal(r1.ArticleID, r1.Article.ArticleID);
            Assert.NotNull(r1.ArticlePacking);
            Assert.Equal(r1.ArtPackID, r1.ArticlePacking.ArtPackID);
            Assert.NotNull(r1.Position);
            Assert.Equal(r1.PositionID, r1.Position.PositionID);
        }


        [Fact]
        public void OnStore_Basic_ByArticle_multiple()
        {
            var onStore = new OnStore();

            var result = onStore.ByArticle(ARTICLE2_ID, ResultValidityEnum.Valid);

            Assert.Equal(2, result.Count);
            var ordered = result.OrderBy(x => x.PositionID).ToList();
            var r1 = ordered[0];
            var r2 = ordered[1];

            Assert.Equal(ARTICLE2_QUANTITY1, r1.Quantity);
            Assert.Equal(POSITION1_ID, r1.PositionID);

            Assert.Equal(ARTICLE2_QUANTITY2, r2.Quantity);
            Assert.Equal(POSITION2_ID, r2.PositionID);
        }


        [Fact]
        public void OnStore_Basic_ByArticle_over2100()
        {
            var onStore = new OnStore();

            var ids = Enumerable.Range(1, 2500).ToList();

            var result = onStore.ByArticle(ids, ResultValidityEnum.Valid);

            Assert.NotEmpty(result);
        }


        [Fact]
        public void OnStore_Basic_ByArticle_notValid()
        {
            var onStore = new OnStore();

            var result_valid = onStore.ByArticle(ARTICLE3_ID, ResultValidityEnum.Valid);
            var result_notValid = onStore.ByArticle(ARTICLE3_ID, ResultValidityEnum.NotValid);

            Assert.Single(result_valid);
            Assert.Single(result_notValid);
            CheckOnStore(result_notValid[0], ARTICLE3_ID, POSITION1_ID, ARTICLE3_QUANTITY_NOTVALID_1);
        }


        [Fact]
        public void OnStore_Basic_ByArticle_Reservation()
        {
            var onStore = new OnStore();

            var result_reservation = onStore.ByArticle(ARTICLE3_ID, ResultValidityEnum.Reservation);

            Assert.Equal(2, result_reservation.Count);
            var ordered = result_reservation.OrderBy(x => x.PositionID).ToList();
            CheckOnStore(ordered[0], ARTICLE3_ID, POSITION1_ID, -5);
            CheckOnStore(ordered[1], ARTICLE3_ID, POSITION2_ID, 5);
        }


        [Fact]
        public void OnStore_Basic_ByArticle_array()
        {
            var onStore = new OnStore();

            var result = onStore.ByArticle(new List<int>() { ARTICLE1_ID, ARTICLE2_ID }, ResultValidityEnum.Valid);

            var res2 = result.GroupBy(i => i.ArticleID).Select(g => g.Key).OrderBy(i => i).ToList();
            Assert.Equal(2, res2.Count);
            Assert.Equal(ARTICLE1_ID, res2[0]);
            Assert.Equal(ARTICLE2_ID, res2[1]);            
        }


        [Fact]
        public void OnStore_Basic_ByArticle_onlyOnStorePositions()
        {
            var onStore = new OnStore();

            var result_reservation = onStore.ByArticle(ARTICLE2_ID, ResultValidityEnum.Valid, onlyOnStorePositions: true);

            Assert.Single(result_reservation);
            CheckOnStore(result_reservation[0], ARTICLE2_ID, POSITION1_ID, 6);
        }


        [Fact]
        public void OnStore_Basic_ByArticle_limitSectionID()
        {
            var onStore = new OnStore();

            var result_reservation = onStore.ByArticle(ARTICLE2_ID, ResultValidityEnum.Valid, limitSectionID: SECTION1);

            Assert.Single(result_reservation);
            CheckOnStore(result_reservation[0], ARTICLE2_ID, POSITION1_ID, 6);
        }


        [Fact]
        public void OnStore_Basic_ByManufact()
        {
            var onStore = new OnStore();

            var result = onStore.ByManufact(MANUFACT, ResultValidityEnum.Valid);

            Assert.Equal(4, result.Count);
            var ordered = result.OrderBy(x => x.ArtPackID).ThenBy(x => x.PositionID).ToList();
            CheckOnStore(ordered[0], ARTICLE1_ID, POSITION1_ID, ARTICLE1_QUANTITY);
            CheckOnStore(ordered[1], ARTICLE2_ID, POSITION1_ID, ARTICLE2_QUANTITY1);
            CheckOnStore(ordered[2], ARTICLE2_ID, POSITION2_ID, ARTICLE2_QUANTITY2);
            CheckOnStore(ordered[3], ARTICLE3_ID, POSITION1_ID, ARTICLE3_QUANTITY);
        }


        [Fact]
        public void OnStore_Basic_ByCarrier()
        {
            var onStore = new OnStore();

            var result = onStore.ByCarrier(CARRIER1, ResultValidityEnum.Valid);

            Assert.NotEmpty(result);
        }


        [Fact]
        public void OnStore_Basic_OnPosition()
        {
            var onStore = new OnStore();

            var result = onStore.OnPosition(POSITION1_ID, ResultValidityEnum.Valid);

            Assert.Equal(3, result.Count);
            var ordered = result.OrderBy(x => x.ArtPackID).ToList();
            CheckOnStore(ordered[0], ARTICLE1_ID, POSITION1_ID, ARTICLE1_QUANTITY);
            CheckOnStore(ordered[1], ARTICLE2_ID, POSITION1_ID, ARTICLE2_QUANTITY1);
            CheckOnStore(ordered[2], ARTICLE3_ID, POSITION1_ID, ARTICLE3_QUANTITY);
        }


        [Fact]
        public void OnStore_Basic_OnPosition_array()
        {
            var onStore = new OnStore();

            var result = onStore.OnPosition(new List<int>() { POSITION1_ID, POSITION2_ID }, ResultValidityEnum.Valid);

            var res2 = result.GroupBy(i => i.PositionID).Select(g => g.Key).OrderBy(i => i).ToList();
            Assert.Equal(2, res2.Count);
            Assert.Equal(POSITION1_ID, res2[0]);
            Assert.Equal(POSITION2_ID, res2[1]);
        }


        [Fact]
        public void OnStore_Basic_ByArticle_emptyDataProxy()
        {
            var onStore = new OnStore();

            var result = onStore.OnPosition(POSITION_EMPTY_DATA_PROXY, ResultValidityEnum.Valid);

            Assert.Single(result);
            var r1 = result[0];

            Assert.Equal(5, r1.Quantity);
            Assert.Equal(POSITION_EMPTY_DATA_PROXY, r1.PositionID);

            Assert.Null(r1.CarrierNum);
            Assert.Null(r1.BatchNum);
            Assert.Null(r1.ExpirationDate);
        }


        [Fact]
        public void OnStore_Basic_CheckNotValidMoves_empty()
        {
            var onStore = new OnStore();

            var result = onStore.CheckNotValidMoves(new List<int>() { POSITION_DISPATCH_ID });

            Assert.Empty(result);
        }


        [Fact]
        public void OnStore_Basic_CheckNotValidMoves_notEmpty()
        {
            var onStore = new OnStore();

            var result = onStore.CheckNotValidMoves(new List<int>() { POSITION2_ID });

            Assert.Single(result);
            var r1 = result[0];

            Assert.Equal(1, r1.MovesCount);
        }


        [Fact]
        public void OnStore_Basic_ScanSMPositionCarrierDirection()
        {
            var onStore = new OnStore();

            var result = onStore.ScanSMPositionCarrierDirection(POSITION1_ID, "_NONE_", DIRECTION_TO_CANCEL_MOVES, false);
            Assert.Empty(result);
        }


        [Fact]
        public void OnStore_Basic_ScanSMPositionDirection()
        {
            var onStore = new OnStore();

            var result = onStore.ScanSMPositionDirection(POSITION1_ID, DIRECTION_TO_CANCEL_MOVES, false);
            Assert.Empty(result);
        }


        internal void CheckOnStore(OnStoreItemFull item, int articleID, int positionID, decimal quantity)
        {
            Assert.Equal(articleID, item.ArticleID);
            Assert.Equal(positionID, item.PositionID);
            Assert.Equal(quantity, item.Quantity);
        }
    }
}

using S4.Entities;
using S4.StoreCore.Interfaces;
using S4.StoreCore.OnStore_Basic;
using System;
using System.Collections.Generic;
using System.Linq;
using TestHelpers;
using Xunit;

namespace S4.StoreCore.Tests
{
    [Collection("StoreCoreTests_Save2")]
    [Trait("Category", "StoreCoreTests_Save2")]
    public class Save_Basic_Tests2 : CoreTestBase
    {

        public Save_Basic_Tests2(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void Save_Basic_MoveResult_ByDirection()
        {
            var save = new Save_Basic.Save();
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var result = save.MoveResult_ByDirection(db, DIRECTION_TO_CANCEL_MOVES, StoreMove.MO_DOCTYPE_RECEIVE, (mi) => mi.PositionID == POSITION_TO_CANCEL_BY_DIRECTION);
                Assert.Single(result);

                var result2 = save.MoveResult_ByDirection(db, 0, StoreMove.MO_DOCTYPE_DISPATCH, (mi) => mi.PositionID == POSITION_TO_CANCEL_BY_DIRECTION); // not existing direction
                Assert.Empty(result2);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using TestHelpers;
using Xunit;


namespace S4.StoreCore.Tests
{
    public class CoreTestBase
    {
        protected const string USER_ID = "USER1";

        protected const int POSITION1_ID = 21;
        protected const int POSITION2_ID = 22;
        protected const int POSITION_REC_ID = 23;
        protected const int POSITION_DISPATCH_ID = 24;
        protected const int POSITION_MOVE_FROM = 25;
        protected const int POSITION_MOVE_TO = 26;
        protected const int POSITION_EMPTY_DATA_PROXY = 27;
        protected const int POSITION_RESET = 28;
        protected const int POSITION_TO_CANCEL = 29;
        protected const int POSITION_TO_CANCEL_ITEM = 30;
        protected const int POSITION_TO_VALIDATE = 31;
        protected const int POSITION_TO_CANCEL_BY_DIRECTION = 32;

        protected const string SECTION1 = "SEC1";

        protected const int ARTICLE1_ID = 200;
        protected const int ARTICLE2_ID = 201;
        protected const int ARTICLE3_ID = 202;

        protected const int ARTICLE1_PACKING_ID = 994;
        protected const int ARTICLE2_PACKING_ID = 995;
        protected const int ARTICLE4_PACKING_ID = 997;

        protected const string BATCH1 = "BATCH1";
        protected const string CARRIER1 = "CARRIER1";

        protected const decimal ARTICLE1_QUANTITY = 1;
        protected const decimal ARTICLE2_QUANTITY1 = 6;
        protected const decimal ARTICLE2_QUANTITY2 = 4;
        protected const decimal ARTICLE3_QUANTITY = 100;
        protected const decimal ARTICLE3_QUANTITY_NOTVALID_1 = 95;
        protected const decimal ARTICLE3_QUANTITY_NOTVALID_2 = 5;

        protected const string MANUFACT = "OnStoreMan";

        protected const int MOVE_CANCELED_ID = 11;
        protected const int MOVE_NOT_CANCELED_ID = 12;
        protected const int MOVE_TO_CANCEL_ID = 13;
        protected const int MOVE_TO_CANCEL_ITEM_ID = 14;
        protected const int MOVE_TO_VALIDATE = 15;
        protected const int MOVE_TO_CANCEL_BY_DIR = 16;
        protected const int ITEM_CANCELED_ID = 32;
        protected const int ITEM_WRONG_BROTHER_ID = 33;
        protected const int ITEM_TO_CANCEL = 35;
        protected const int ITEM_TO_VALIDATE = 37;
        

        protected const int DIRECTION_TO_CANCEL_MOVES = 20;

        protected DatabaseFixture fixture;


        public CoreTestBase(DatabaseFixture fixture)
        {
            this.fixture = fixture;
            Core.Data.ConnectionHelper.ConnectionString = fixture.ConnectionString;
        }
    }
}

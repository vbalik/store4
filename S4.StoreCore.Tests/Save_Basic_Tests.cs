using S4.Entities;
using S4.StoreCore.Interfaces;
using S4.StoreCore.OnStore_Basic;
using System;
using System.Collections.Generic;
using System.Linq;
using TestHelpers;
using Xunit;

namespace S4.StoreCore.Tests
{
    [Collection("StoreCoreTests_Save")]
    [Trait("Category", "StoreCoreTests_Save")]
    public class Save_Basic_Tests : CoreTestBase
    {

        public Save_Basic_Tests(DatabaseFixture fixture) : base(fixture)
        { }


        [Fact]
        public void Save_Basic_StoreIn_valid()
        {
            var saveItems = new List<SaveItem>()
            {
                new SaveItem() { ArtPackID = ARTICLE1_PACKING_ID, BatchNum = BATCH1, CarrierNum = CARRIER1, ExpirationDate = new DateTime(2020, 1, 1), Quantity = 45 }
            };

            int id = 0;
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var save = new Save_Basic.Save();
                id = save.StoreIn(db, USER_ID, POSITION_REC_ID, "REC123", saveItems, true, null, null, out string msg);
            }

            // assert
            Assert.True(id != 0);

            var onStore = new OnStore();
            var result = onStore.OnPosition(POSITION_REC_ID, ResultValidityEnum.Valid);

            Assert.Single(result);
            CheckOnStore(result[0], saveItems[0].ArtPackID, POSITION_REC_ID, saveItems[0].Quantity, saveItems[0].BatchNum, saveItems[0].ExpirationDate);
        }


        [Fact]
        public void Save_Basic_StoreIn_notValid()
        {
            var saveItems = new List<SaveItem>()
            {
                new SaveItem() { ArtPackID = ARTICLE2_PACKING_ID, BatchNum = BATCH1, CarrierNum = CARRIER1, ExpirationDate = new DateTime(2020, 1, 1), Quantity = 50 }
            };

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var save = new Save_Basic.Save();
                save.StoreIn(db, USER_ID, POSITION_REC_ID, "REC123", saveItems, false, null, null, out string msg);
            }

            // assert
            var onStore = new OnStore();
            var resultValid = onStore.OnPosition(POSITION_REC_ID, ResultValidityEnum.Valid).Where(i => i.ArtPackID == ARTICLE2_PACKING_ID).ToList();
            var resultNotValid = onStore.OnPosition(POSITION_REC_ID, ResultValidityEnum.Reservation).Where(i => i.ArtPackID == ARTICLE2_PACKING_ID).ToList();

            Assert.Empty(resultValid);
            Assert.Single(resultNotValid);
            CheckOnStore(resultNotValid[0], saveItems[0].ArtPackID, POSITION_REC_ID, saveItems[0].Quantity, saveItems[0].BatchNum, saveItems[0].ExpirationDate);
        }


        [Fact]
        public void Save_Basic_StoreIn_CheckExceptions()
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var save = new Save_Basic.Save();
                Assert.Throws<ArgumentNullException>("docNumPrefix", () => save.StoreIn(db, USER_ID, POSITION_REC_ID, null, null, true, null, null, out string msg));
                Assert.Throws<ArgumentException>(() => save.StoreIn(db, USER_ID, POSITION_REC_ID, "REC123", new List<SaveItem>(), true, null, null, out string msg));
                Assert.Throws<ArgumentException>(() => save.StoreIn(db, USER_ID, -100, "REC123", new List<SaveItem>() { new SaveItem() }, true, null, null, out string msg));
                Assert.Throws<ArgumentException>(() => save.StoreIn(db, USER_ID, POSITION1_ID, "REC123", new List<SaveItem>() { new SaveItem() }, true, null, null, out string msg));
            }
        }


        [Fact]
        public void Save_Basic_StoreOut()
        {
            var saveItems = new List<SaveItem>()
            {
                new SaveItem() { ArtPackID = ARTICLE4_PACKING_ID, BatchNum = BATCH1, CarrierNum = CARRIER1, ExpirationDate = new DateTime(1900, 1, 1), Quantity = 33 }
            };

            int id = 0;
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var save = new Save_Basic.Save();
                id = save.StoreOut(db, USER_ID, POSITION_DISPATCH_ID, "DISP123", saveItems, true, null, null, out string msg);
            }

            // assert
            Assert.True(id != 0);

            var onStore = new OnStore();
            var result = onStore.OnPosition(POSITION_DISPATCH_ID, ResultValidityEnum.Valid);

            Assert.Empty(result);
        }


        [Fact]
        public void Save_Basic_StoreOut_CheckExceptions()
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var save = new Save_Basic.Save();
                Assert.Throws<ArgumentNullException>("docNumPrefix", () => save.StoreOut(db, USER_ID, POSITION_REC_ID, null, null, true, null, null, out string msg));
                Assert.Throws<ArgumentException>(() => save.StoreOut(db, USER_ID, POSITION_REC_ID, "REC123", new List<SaveItem>(), true, null, null, out string msg));
                Assert.Throws<ArgumentException>(() => save.StoreOut(db, USER_ID, -100, "REC123", new List<SaveItem>() { new SaveItem() }, true, null, null, out string msg));
                Assert.Throws<ArgumentException>(() => save.StoreOut(db, USER_ID, POSITION1_ID, "REC123", new List<SaveItem>() { new SaveItem() }, true, null, null, out string msg));
            }
        }


        [Fact]
        public void Save_Basic_Move()
        {
            var moveItems = new List<MoveItem>()
            {
                new MoveItem()
                {
                    ArtPackID = ARTICLE4_PACKING_ID, BatchNum = BATCH1, CarrierNum = CARRIER1, ExpirationDate = null, Quantity = 10,
                    SourcePositionID = POSITION_MOVE_FROM, DestinationPositionID = POSITION_MOVE_TO
                }
            };

            int id = 0;
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var save = new Save_Basic.Save();
                id = save.Move(db, USER_ID, "MOVE", 4, moveItems, true, 
                    (sm) =>
                    {
                        sm.DocRemark = "remark";
                    },
                    (si, smi) =>
                    {
                        smi.DocPosition = smi.DocPosition * 10;
                    },
                    out string msg);
            }

            // assert
            Assert.True(id != 0);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var stMove = db.SingleOrDefaultById<StoreMove>(id);
                Assert.NotNull(stMove);
                Assert.Equal("remark", stMove.DocRemark);

                var items = db.Query<StoreMoveItem>().Where(i => i.StoMoveID == id).OrderBy(i => i.DocPosition).ToList();
                Assert.Equal(10, items[0].DocPosition);
                Assert.Equal(20, items[1].DocPosition);
            }


                var onStore = new OnStore();

            var resultFrom = onStore.OnPosition(POSITION_MOVE_FROM, ResultValidityEnum.Valid);
            Assert.Single(resultFrom);
            CheckOnStore(resultFrom[0], moveItems[0].ArtPackID, POSITION_MOVE_FROM, 40, moveItems[0].BatchNum, null);

            var resultTo = onStore.OnPosition(POSITION_MOVE_TO, ResultValidityEnum.Valid);
            Assert.Single(resultTo);
            CheckOnStore(resultTo[0], moveItems[0].ArtPackID, POSITION_MOVE_TO, 10, moveItems[0].BatchNum, null);
        }


        [Fact]
        public void Save_Basic_Move_CheckExceptions()
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var save = new Save_Basic.Save();
                Assert.Throws<ArgumentNullException>("docNumPrefix", () => save.Move(db, USER_ID, null, 0, null, true, null, null, out string msg));
                Assert.Throws<ArgumentException>(() => save.Move(db, USER_ID, "MOVE", 0, new List<MoveItem>(), true, null, null, out string msg));
            }
        }


        [Fact]
        public void Save_Basic_ResetPosition()
        {
            var resetItems = new List<SaveItem>()
            {
                new SaveItem() { ArtPackID = ARTICLE4_PACKING_ID, BatchNum = BATCH1, CarrierNum = CARRIER1, ExpirationDate = new DateTime(2025, 1, 1), Quantity = 10 }
            };
            var onStore = new OnStore();
            var initStatus = onStore.OnPosition(POSITION_RESET, ResultValidityEnum.Valid);
            Assert.Single(initStatus);

            int id = 0;
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var save = new Save_Basic.Save();
                id = save.ResetPosition(db, USER_ID, POSITION_RESET, "MOVE", resetItems, null, out string msg);
            }

            // assert
            Assert.True(id != 0);

            var resultTo = onStore.OnPosition(POSITION_RESET, ResultValidityEnum.Valid);
            Assert.Single(resultTo);
            CheckOnStore(resultTo[0], resetItems[0].ArtPackID, POSITION_RESET, resetItems[0].Quantity, resetItems[0].BatchNum, resetItems[0].ExpirationDate);
        }


        [Fact]
        public void Save_Basic_ResetPosition_CheckExceptions()
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var save = new Save_Basic.Save();
                Assert.Throws<ArgumentNullException>("docNumPrefix", () => save.ResetPosition(db, USER_ID, POSITION_REC_ID, null, null, null, out string msg));
                Assert.Throws<ArgumentException>(() => save.ResetPosition(db, USER_ID, -100, "REC123", new List<SaveItem>() { new SaveItem() }, null, out string msg));
                Assert.Throws<ArgumentException>(() => save.ResetPosition(db, USER_ID, POSITION1_ID, "REC123", new List<SaveItem>() { new SaveItem() }, null, out string msg));
            }
        }


        [Fact]
        public void Save_Basic_CancelMove()
        {
            var onStore = new OnStore();
            var initStatus = onStore.OnPosition(POSITION_TO_CANCEL, ResultValidityEnum.Valid);
            Assert.Single(initStatus);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var save = new Save_Basic.Save();
                save.CancelMove(db, USER_ID, MOVE_TO_CANCEL_ID, out string msg);
            }

            // assert
            var resultTo = onStore.OnPosition(POSITION_TO_CANCEL, ResultValidityEnum.Valid);
            Assert.Empty(resultTo);
            var items = GetMoveItems(MOVE_TO_CANCEL_ID);
            Assert.Empty(items.Where(i => i.ItemValidity != StoreMoveItem.MI_VALIDITY_CANCELED));
        }


        [Fact]
        public void Save_Basic_CancelMove_CheckExceptions()
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var save = new Save_Basic.Save();
                Assert.Throws<ArgumentException>(() => save.CancelMove(db, USER_ID, -1, out string msg));  // not found
                Assert.Throws<ArgumentException>(() => save.CancelMove(db, USER_ID, MOVE_CANCELED_ID, out string msg));  // canceled move
            }
        }


        [Fact]
        public void Save_Basic_CancelMoveItem()
        {
            var onStore = new OnStore();
            var initStatus = onStore.OnPosition(POSITION_TO_CANCEL_ITEM, ResultValidityEnum.Valid);
            Assert.Single(initStatus);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var save = new Save_Basic.Save();
                save.CancelMoveItem(db, USER_ID, MOVE_TO_CANCEL_ITEM_ID, ITEM_TO_CANCEL, out string msg);
            }

            // assert
            var resultTo = onStore.OnPosition(POSITION_TO_CANCEL_ITEM, ResultValidityEnum.Valid);
            Assert.Empty(resultTo);
            var items = GetMoveItems(MOVE_TO_CANCEL_ITEM_ID);
            Assert.Empty(items.Where(i => i.ItemValidity != StoreMoveItem.MI_VALIDITY_CANCELED));
        }


        [Fact]
        public void Save_Basic_CancelMoveItem_CheckExceptions()
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var save = new Save_Basic.Save();
                Assert.Throws<ArgumentException>(() => save.CancelMoveItem(db, USER_ID, -1, 0, out string msg));  // not found
                Assert.Throws<ArgumentException>(() => save.CancelMoveItem(db, USER_ID, MOVE_CANCELED_ID, 0, out string msg));  // canceled move
                Assert.Throws<ArgumentException>(() => save.CancelMoveItem(db, USER_ID, MOVE_NOT_CANCELED_ID, -1, out string msg));  // item not found
                Assert.Throws<ArgumentException>(() => save.CancelMoveItem(db, USER_ID, MOVE_NOT_CANCELED_ID, ITEM_CANCELED_ID, out string msg));  // item canceled
                Assert.Throws<Exception>(() => save.CancelMoveItem(db, USER_ID, MOVE_NOT_CANCELED_ID, ITEM_WRONG_BROTHER_ID, out string msg));  // items inconsistency
            }
        }


        [Fact]
        public void Save_Basic_CancelByDirection()
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var save = new Save_Basic.Save();
                save.CancelByDirection(db, USER_ID, DIRECTION_TO_CANCEL_MOVES, false, out string msg);
            }

            // assert
            var onStore = new OnStore();
            var resultTo = onStore.OnPosition(POSITION_TO_CANCEL_BY_DIRECTION, ResultValidityEnum.Valid);
            Assert.Empty(resultTo);
            var items = GetMoveItems(MOVE_TO_CANCEL_BY_DIR);
            Assert.Empty(items.Where(i => i.ItemValidity != StoreMoveItem.MI_VALIDITY_CANCELED));
        }


        [Fact]
        public void Save_Basic_CancelByDirection_CheckExceptions()
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var save = new Save_Basic.Save();
                Assert.Throws<ArgumentException>(() => save.CancelByDirection(db, USER_ID, -1, false, out string msg));  // not found          
            }
        }


        [Fact]
        public void Save_Basic_ValidateMoveItem()
        {
            var onStore = new OnStore();
            var initStatus = onStore.OnPosition(POSITION_TO_VALIDATE, ResultValidityEnum.Valid);
            Assert.Empty(initStatus);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var save = new Save_Basic.Save();
                save.ValidateMoveItem(db, USER_ID, MOVE_TO_VALIDATE, ITEM_TO_VALIDATE, null, out string msg);
            }

            // assert
            var resultTo = onStore.OnPosition(POSITION_TO_VALIDATE, ResultValidityEnum.Valid);
            Assert.Single(resultTo);
            var items = GetMoveItems(MOVE_TO_VALIDATE);
            Assert.Empty(items.Where(i => i.ItemValidity != StoreMoveItem.MI_VALIDITY_VALID));
        }


        [Fact]
        public void Save_Basic_ValidateMoveItem_CheckExceptions()
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var save = new Save_Basic.Save();
                Assert.Throws<ArgumentException>(() => save.ValidateMoveItem(db, USER_ID, -1, 0, null, out string msg));  // not found
                Assert.Throws<ArgumentException>(() => save.ValidateMoveItem(db, USER_ID, MOVE_CANCELED_ID, 0, null, out string msg));  // canceled move
                Assert.Throws<ArgumentException>(() => save.ValidateMoveItem(db, USER_ID, MOVE_NOT_CANCELED_ID, -1, null, out string msg));  // item not found
                Assert.Throws<ArgumentException>(() => save.ValidateMoveItem(db, USER_ID, MOVE_NOT_CANCELED_ID, ITEM_CANCELED_ID, null, out string msg));  // item canceled
                Assert.Throws<Exception>(() => save.ValidateMoveItem(db, USER_ID, MOVE_NOT_CANCELED_ID, ITEM_WRONG_BROTHER_ID, null, out string msg));  // items inconsistency
            }
        }


        [Fact]
        public void Save_Basic_ChangeMoveItems()
        {
            const int STOMOVE_ID = 517;

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var moveItems = db.Query<StoreMoveItem>().Where(mi => mi.StoMoveID == STOMOVE_ID).ToList();

                var save = new Save_Basic.Save();
                save.ChangeMoveItems(db, USER_ID, moveItems, 1012, 10, out string msg);

                // assert
                var result = db.Query<StoreMoveItem>().Where(mi => mi.StoMoveID == STOMOVE_ID).ToList();
                Assert.NotEmpty(result);
                Assert.All(result, i =>
                {
                    Assert.Equal(10, i.Quantity);
                    var lot = db.SingleById<StoreMoveLot>(i.StoMoveLotID);
                    Assert.Equal(1012, lot.ArtPackID);

                    var hist = db.Query<StoreMoveHistory>().Where(h => h.StoMoveID == i.StoMoveID && h.DocPosition == i.DocPosition).ToList();
                    Assert.NotEmpty(hist);
                    Assert.Equal(StoreMove.MO_EVENT_ITEM_CHANGED, hist[0].EventCode);
                    Assert.Equal("{\"changes\":[{\"property\":\"StoMoveLotID\",\"originValue\":\"44\",\"currentValue\":\"64\"},{\"property\":\"Quantity\",\"originValue\":\"5.0000\",\"currentValue\":\"10\"}]}", hist[0].EventData);
                });
            }

        }


        private void CheckOnStore(OnStoreItemFull item, int artPackID, int positionID, decimal quantity, string batchNum, DateTime? expirationDate)
        {
            Assert.Equal(artPackID, item.ArtPackID);
            Assert.Equal(positionID, item.PositionID);
            Assert.Equal(quantity, item.Quantity);
            Assert.Equal(batchNum, item.BatchNum);
            Assert.Equal(expirationDate, item.ExpirationDate);
        }


        private List<StoreMoveItem> GetMoveItems(int stoMoveID)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                return db.Fetch<StoreMoveItem>(new NPoco.Sql().Where("stoMoveID = @0", stoMoveID));
            }
        }
    }
}

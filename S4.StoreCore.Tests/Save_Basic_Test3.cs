﻿using S4.Entities;
using S4.StoreCore.Interfaces;
using S4.StoreCore.OnStore_Basic;
using System;
using System.Collections.Generic;
using System.Linq;
using TestHelpers;
using Xunit;

namespace S4.StoreCore.Tests
{
    [Trait("Category", "StoreCoreTests_Save3")]
    public class Save_Basic_Test3
    {
        public Save_Basic_Test3()
        {
            CreateData();
        }

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        [InlineData(4)]
        [InlineData(5)]
        public void Save_Basic_ResetPosition(int testVariant)
        {
            var save = new Save_Basic.Save();

            var result = save.GetRepairSaveItems(_data, _testVariants[testVariant].Item1, _positionID, _user, _entryDateTime);
            Assert.Equal(_testVariants[testVariant].Item2.Count(), result.Count());
            for (int i = 0; i < _testVariants[testVariant].Item2.Count(); i++)
            {
                Assert.Equal(_testVariants[testVariant].Item2[i].Item1.ArtPackID, result.ToArray()[i].Item1.ArtPackID);
                Assert.Equal(_testVariants[testVariant].Item2[i].Item1.BatchNum, result.ToArray()[i].Item1.BatchNum);
                Assert.Equal(_testVariants[testVariant].Item2[i].Item1.CarrierNum, result.ToArray()[i].Item1.CarrierNum);
                Assert.Equal(_testVariants[testVariant].Item2[i].Item1.CarrierNum, result.ToArray()[i].Item2._carrierNum);
                Assert.Equal(_testVariants[testVariant].Item2[i].Item1.ExpirationDate, result.ToArray()[i].Item1.ExpirationDate);
                Assert.Equal(_testVariants[testVariant].Item2[i].Item1.Quantity, result.ToArray()[i].Item1.Quantity);
                Assert.Equal(_testVariants[testVariant].Item2[i].Item2.Quantity, result.ToArray()[i].Item2.Quantity);
                Assert.Equal(_testVariants[testVariant].Item2[i].Item2.ItemDirection, result.ToArray()[i].Item2.ItemDirection);
                Assert.Equal(_testVariants[testVariant].Item2[i].Item2.ItemValidity, result.ToArray()[i].Item2.ItemValidity);
            }
        }

        private DateTime _entryDateTime = DateTime.Now;
        private string _user = "user";
        private int _positionID = 1;
        private static SaveItem[] _data;
        private Tuple<SaveItem[], Tuple<SaveItem, StoreMoveItem>[]>[] _testVariants;

        private void CreateData()
        {
            _data = new SaveItem[]
            {
                new SaveItem() { ArtPackID = 1, BatchNum = "b1", ExpirationDate = new DateTime(2021, 1, 1), Quantity = 6 },//0
                new SaveItem() { ArtPackID = 1, BatchNum = "b2", ExpirationDate = new DateTime(2021, 1, 1), Quantity = 7 },//1
                new SaveItem() { ArtPackID = 1, BatchNum = "b1", ExpirationDate = new DateTime(2021, 1, 2), Quantity = 8 },//2
                new SaveItem() { ArtPackID = 1, BatchNum = "b2", ExpirationDate = new DateTime(2021, 1, 2), Quantity = 9 },//3
                new SaveItem() { ArtPackID = 1, BatchNum = "b1", ExpirationDate = new DateTime(2021, 1, 2), CarrierNum = "c1", Quantity = 10 },//4
                new SaveItem() { ArtPackID = 1, BatchNum = "b2", ExpirationDate = new DateTime(2021, 1, 2), CarrierNum = "c2", Quantity = 11 },//5
                new SaveItem() { ArtPackID = 2, Quantity = 3 },//6
                new SaveItem() { ArtPackID = 3, Quantity = 5 }//7
            };

            _testVariants = new Tuple<SaveItem[], Tuple<SaveItem, StoreMoveItem>[]>[]
            {
                //////////////
                //testcase 1//
                //////////////
                new Tuple<SaveItem[], Tuple<SaveItem, StoreMoveItem>[]>(
                    //required
                    new SaveItem[] {
                        new SaveItem() { ArtPackID = 1, BatchNum = "b1", ExpirationDate = new DateTime(2021, 1, 1), Quantity = 6 },
                        new SaveItem() { ArtPackID = 1, BatchNum = "b2", ExpirationDate = new DateTime(2021, 1, 1), Quantity = 7 },
                        new SaveItem() { ArtPackID = 1, BatchNum = "b1", ExpirationDate = new DateTime(2021, 1, 2), Quantity = 8 },
                        new SaveItem() { ArtPackID = 1, BatchNum = "b2", ExpirationDate = new DateTime(2021, 1, 2), Quantity = 9 },
                        new SaveItem() { ArtPackID = 1, BatchNum = "b1", ExpirationDate = new DateTime(2021, 1, 2), CarrierNum = "c1", Quantity = 21 },
                        new SaveItem() { ArtPackID = 2, Quantity = 3 },
                        new SaveItem() { ArtPackID = 3, Quantity = 5 }
                    },
                    //data for save
                    new Tuple<SaveItem, StoreMoveItem>[] {
                        new Tuple<SaveItem, StoreMoveItem>(
                        _data[4],
                        new StoreMoveItem()
                        {
                            CarrierNum = _data[4].CarrierNum,
                            DocPosition = 1,
                            EntryDateTime = _entryDateTime,
                            ItemDirection = StoreMoveItem.MI_DIRECTION_IN,
                            EntryUserID = _user,
                            ItemValidity = StoreMoveItem.MI_VALIDITY_VALID,
                            PositionID = _positionID,
                            Quantity = _data[5].Quantity
                        }),
                        new Tuple<SaveItem, StoreMoveItem>(
                        _data[5],
                        new StoreMoveItem()
                        {
                            CarrierNum = _data[4].CarrierNum,
                            DocPosition = 2,
                            EntryDateTime = _entryDateTime,
                            ItemDirection = StoreMoveItem.MI_DIRECTION_OUT,
                            EntryUserID = _user,
                            ItemValidity = StoreMoveItem.MI_VALIDITY_VALID,
                            PositionID = _positionID,
                            Quantity = _data[5].Quantity
                        })
                    }),
                //////////////
                //testcase 2//
                //////////////
                new Tuple<SaveItem[], Tuple<SaveItem, StoreMoveItem>[]>(
                    //required
                    new SaveItem[] {
                        new SaveItem() { ArtPackID = 1, BatchNum = "b1", ExpirationDate = new DateTime(2021, 1, 1), Quantity = 13 },//0
                        new SaveItem() { ArtPackID = 1, BatchNum = "b1", ExpirationDate = new DateTime(2021, 1, 2), Quantity = 8 },//2
                        new SaveItem() { ArtPackID = 1, BatchNum = "b2", ExpirationDate = new DateTime(2021, 1, 2), Quantity = 9 },//3
                        new SaveItem() { ArtPackID = 1, BatchNum = "b1", ExpirationDate = new DateTime(2021, 1, 2), CarrierNum = "c1", Quantity = 10 },//4
                        new SaveItem() { ArtPackID = 1, BatchNum = "b2", ExpirationDate = new DateTime(2021, 1, 2), CarrierNum = "c2", Quantity = 11 },//5
                        new SaveItem() { ArtPackID = 2, Quantity = 3 },
                        new SaveItem() { ArtPackID = 3, Quantity = 5 }
                    },
                    //data for save
                    new Tuple<SaveItem, StoreMoveItem>[] {
                        new Tuple<SaveItem, StoreMoveItem>(
                        _data[0],
                        new StoreMoveItem()
                        {
                            CarrierNum = _data[0].CarrierNum,
                            DocPosition = 1,
                            EntryDateTime = _entryDateTime,
                            ItemDirection = StoreMoveItem.MI_DIRECTION_IN,
                            EntryUserID = _user,
                            ItemValidity = StoreMoveItem.MI_VALIDITY_VALID,
                            PositionID = _positionID,
                            Quantity = _data[1].Quantity
                        }),
                        new Tuple<SaveItem, StoreMoveItem>(
                        _data[1],
                        new StoreMoveItem()
                        {
                            CarrierNum = _data[0].CarrierNum,
                            DocPosition = 2,
                            EntryDateTime = _entryDateTime,
                            ItemDirection = StoreMoveItem.MI_DIRECTION_OUT,
                            EntryUserID = _user,
                            ItemValidity = StoreMoveItem.MI_VALIDITY_VALID,
                            PositionID = _positionID,
                            Quantity = _data[1].Quantity
                        })
                    }),
                //////////////
                //testcase 3//
                //////////////
                new Tuple<SaveItem[], Tuple<SaveItem, StoreMoveItem>[]>(
                    //required
                    new SaveItem[] {
                        new SaveItem() { ArtPackID = 1, BatchNum = "b1", ExpirationDate = new DateTime(2021, 1, 1), Quantity = 6 },//0
                        new SaveItem() { ArtPackID = 1, BatchNum = "b2", ExpirationDate = new DateTime(2021, 1, 1), Quantity = 7 },//1
                        new SaveItem() { ArtPackID = 1, BatchNum = "b1", ExpirationDate = new DateTime(2021, 1, 2), Quantity = 17 },//2
                        new SaveItem() { ArtPackID = 1, BatchNum = "b1", ExpirationDate = new DateTime(2021, 1, 2), CarrierNum = "c1", Quantity = 10 },//4
                        new SaveItem() { ArtPackID = 1, BatchNum = "b2", ExpirationDate = new DateTime(2021, 1, 2), CarrierNum = "c2", Quantity = 11 },//5
                        new SaveItem() { ArtPackID = 2, Quantity = 3 },
                        new SaveItem() { ArtPackID = 3, Quantity = 5 }
                    },
                    //data for save
                    new Tuple<SaveItem, StoreMoveItem>[] {
                        new Tuple<SaveItem, StoreMoveItem>(
                        _data[2],
                        new StoreMoveItem()
                        {
                            CarrierNum = _data[2].CarrierNum,
                            DocPosition = 1,
                            EntryDateTime = _entryDateTime,
                            ItemDirection = StoreMoveItem.MI_DIRECTION_IN,
                            EntryUserID = _user,
                            ItemValidity = StoreMoveItem.MI_VALIDITY_VALID,
                            PositionID = _positionID,
                            Quantity = _data[3].Quantity
                        }),
                        new Tuple<SaveItem, StoreMoveItem>(
                        _data[3],
                        new StoreMoveItem()
                        {
                            CarrierNum = _data[2].CarrierNum,
                            DocPosition = 2,
                            EntryDateTime = _entryDateTime,
                            ItemDirection = StoreMoveItem.MI_DIRECTION_OUT,
                            EntryUserID = _user,
                            ItemValidity = StoreMoveItem.MI_VALIDITY_VALID,
                            PositionID = _positionID,
                            Quantity = _data[3].Quantity
                        })
                    }),
                //////////////
                //testcase 4//
                //////////////
                new Tuple<SaveItem[], Tuple<SaveItem, StoreMoveItem>[]>(
                    //required
                    new SaveItem[] {
                        new SaveItem() { ArtPackID = 1, BatchNum = "b1", ExpirationDate = new DateTime(2021, 1, 1), Quantity = 6 },//0
                        new SaveItem() { ArtPackID = 1, BatchNum = "b2", ExpirationDate = new DateTime(2021, 1, 1), Quantity = 7 },//1
                        new SaveItem() { ArtPackID = 1, BatchNum = "b1", ExpirationDate = new DateTime(2021, 1, 2), Quantity = 8 },//2
                        new SaveItem() { ArtPackID = 1, BatchNum = "b2", ExpirationDate = new DateTime(2021, 1, 2), Quantity = 9 },//3
                        new SaveItem() { ArtPackID = 1, BatchNum = "b1", ExpirationDate = new DateTime(2021, 1, 2), CarrierNum = "c1", Quantity = 10 },//4
                        new SaveItem() { ArtPackID = 1, BatchNum = "b2", ExpirationDate = new DateTime(2021, 1, 2), CarrierNum = "c2", Quantity = 11 },//5
                        new SaveItem() { ArtPackID = 2, Quantity = 5 },//6
                        new SaveItem() { ArtPackID = 3, Quantity = 2 }//7
                    },
                    //data for save
                    new Tuple<SaveItem, StoreMoveItem>[] {
                        new Tuple<SaveItem, StoreMoveItem>(
                        _data[6],
                        new StoreMoveItem()
                        {
                            CarrierNum = _data[6].CarrierNum,
                            DocPosition = 1,
                            EntryDateTime = _entryDateTime,
                            ItemDirection = StoreMoveItem.MI_DIRECTION_IN,
                            EntryUserID = _user,
                            ItemValidity = StoreMoveItem.MI_VALIDITY_VALID,
                            PositionID = _positionID,
                            Quantity = 2
                        }),
                        new Tuple<SaveItem, StoreMoveItem>(
                        _data[7],
                        new StoreMoveItem()
                        {
                            CarrierNum = _data[7].CarrierNum,
                            DocPosition = 2,
                            EntryDateTime = _entryDateTime,
                            ItemDirection = StoreMoveItem.MI_DIRECTION_OUT,
                            EntryUserID = _user,
                            ItemValidity = StoreMoveItem.MI_VALIDITY_VALID,
                            PositionID = _positionID,
                            Quantity = 3
                        })
                    }),
                //////////////
                //testcase 5//
                //////////////
                new Tuple<SaveItem[], Tuple<SaveItem, StoreMoveItem>[]>(
                    //required
                    new SaveItem[] {
                        new SaveItem() { ArtPackID = 1, BatchNum = "b1", ExpirationDate = new DateTime(2021, 1, 1), Quantity = 6 },//0
                        new SaveItem() { ArtPackID = 1, BatchNum = "b2", ExpirationDate = new DateTime(2021, 1, 1), Quantity = 7 },//1
                        new SaveItem() { ArtPackID = 1, BatchNum = "b1", ExpirationDate = new DateTime(2021, 1, 2), Quantity = 8 },//2
                        new SaveItem() { ArtPackID = 1, BatchNum = "b1", ExpirationDate = new DateTime(2021, 1, 2), CarrierNum = "c1", Quantity = 10 },//4
                        new SaveItem() { ArtPackID = 1, BatchNum = "b2", ExpirationDate = new DateTime(2021, 1, 2), CarrierNum = "c2", Quantity = 11 },//5
                        new SaveItem() { ArtPackID = 2, Quantity = 3 },//6
                        new SaveItem() { ArtPackID = 3, Quantity = 5 }//7
                    },
                    //data for save
                    new Tuple<SaveItem, StoreMoveItem>[] {
                        new Tuple<SaveItem, StoreMoveItem>(
                        _data[3],
                        new StoreMoveItem()
                        {
                            CarrierNum = _data[3].CarrierNum,
                            DocPosition = 1,
                            EntryDateTime = _entryDateTime,
                            ItemDirection = StoreMoveItem.MI_DIRECTION_OUT,
                            EntryUserID = _user,
                            ItemValidity = StoreMoveItem.MI_VALIDITY_VALID,
                            PositionID = _positionID,
                            Quantity = _data[3].Quantity
                        })
                    }),
                //////////////
                //testcase 6//
                //////////////
                new Tuple<SaveItem[], Tuple<SaveItem, StoreMoveItem>[]>(
                    //required
                    new SaveItem[] {
                        new SaveItem() { ArtPackID = 1, BatchNum = "b1", ExpirationDate = new DateTime(2021, 1, 1), Quantity = 6 },//0
                        new SaveItem() { ArtPackID = 1, BatchNum = "b2", ExpirationDate = new DateTime(2021, 1, 1), Quantity = 7 },//1
                        new SaveItem() { ArtPackID = 1, BatchNum = "b1", ExpirationDate = new DateTime(2021, 1, 2), Quantity = 8 },//2
                        new SaveItem() { ArtPackID = 1, BatchNum = "b2", ExpirationDate = new DateTime(2021, 1, 2), Quantity = 9 },//3
                        new SaveItem() { ArtPackID = 1, BatchNum = "b1", ExpirationDate = new DateTime(2021, 1, 2), CarrierNum = "c1", Quantity = 10 },//4
                        new SaveItem() { ArtPackID = 1, BatchNum = "b2", ExpirationDate = new DateTime(2021, 1, 2), CarrierNum = "c2", Quantity = 11 },//5
                        new SaveItem() { ArtPackID = 2, Quantity = 3 },//6
                        new SaveItem() { ArtPackID = 3, Quantity = 5 },//7
                        new SaveItem() { ArtPackID = 1, BatchNum = "b2", ExpirationDate = new DateTime(2021, 1, 2), CarrierNum = "c3", Quantity = 12 }
                    },
                    //data for save
                    new Tuple<SaveItem, StoreMoveItem>[] {
                        new Tuple<SaveItem, StoreMoveItem>(
                        new SaveItem() { ArtPackID = 1, BatchNum = "b2", ExpirationDate = new DateTime(2021, 1, 2), CarrierNum = "c3", Quantity = 12 },
                        new StoreMoveItem()
                        {
                            CarrierNum = "c3",
                            DocPosition = 1,
                            EntryDateTime = _entryDateTime,
                            ItemDirection = StoreMoveItem.MI_DIRECTION_IN,
                            EntryUserID = _user,
                            ItemValidity = StoreMoveItem.MI_VALIDITY_VALID,
                            PositionID = _positionID,
                            Quantity = 12
                        })
                    })
            };
        }
    }
}

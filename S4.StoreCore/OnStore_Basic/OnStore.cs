﻿using System;
using System.Collections.Generic;

using NPoco;
using S4.Core.Data;
using S4.Entities;
using S4.StoreCore.Interfaces;


namespace S4.StoreCore.OnStore_Basic
{

    public class OnStore : BasicBase, IOnStore
    {
        /// <summary>Return store status by position.</summary>
        /// <param name="positionID">The position identifier.</param>
        /// <param name="resultValidity">The required validity.</param>
        /// <returns></returns>
        public List<OnStoreItemFull> OnPosition(int positionID, ResultValidityEnum resultValidity)
        {
            var sql = new Sql()
                .Where("onStore.positionID = @0", positionID);
            return GetOnStore(resultValidity, sql);
        }


        /// <summary>Return store status by position.</summary>
        /// <param name="positionIDs">List of position identifiers.</param>
        /// <param name="resultValidity">The required validity.</param>
        /// <returns></returns>
        public List<OnStoreItemFull> OnPosition(List<int> positionIDs, ResultValidityEnum resultValidity)
        {
            (Sql condition, Sql prepareSql) = Helpers.MakeListCondition("onStore.positionID IN (@0)", positionIDs);
            return GetOnStore(resultValidity, condition, prepareSql);
        }


        /// <summary>Return store status by article.</summary>
        /// <param name="articleID">The article identifier.</param>
        /// <param name="resultValidity">The required validity.</param>
        /// <param name="onlyOnStorePositions">Limit result - only Store positions with OK status.</param>
        /// <param name="limitSectionID">Limit result - only selected section.</param>
        /// <returns></returns>
        public List<OnStoreItemFull> ByArticle(int articleID, ResultValidityEnum resultValidity, bool onlyOnStorePositions = false, string limitSectionID = null)
        {
            var sql = new Sql()
                .Where("ar.articleID = @0", articleID);
            if (onlyOnStorePositions)
                sql.Where($"(pos.posCateg = {(int)Position.PositionCategoryEnum.Store}) AND (pos.posStatus = '{Position.POS_STATUS_OK}')");
            if (limitSectionID != null)
                sql.Where($"(onStore.positionID IN (SELECT positionID FROM [dbo].[s4_positionList_sections] WHERE sectID = @0))", limitSectionID);

            return GetOnStore(resultValidity, sql);
        }


        /// <summary>Return store status by article.</summary>
        /// <param name="articleIDs">List of article identifiers.</param>
        /// <param name="resultValidity">The required validity.</param>
        /// <returns></returns>
        public List<OnStoreItemFull> ByArticle(List<int> articleIDs, ResultValidityEnum resultValidity)
        {
            (Sql condition, Sql prepareSql) = Helpers.MakeListCondition("ar.articleID IN (@0)", articleIDs);
            return GetOnStore(resultValidity, condition, prepareSql);
        }


        /// <summary>Return store status by manufacturer.</summary>
        /// <param name="manufID">The manufacturer identifier.</param>
        /// <param name="resultValidity">The required validity.</param>
        /// <returns></returns>
        public List<OnStoreItemFull> ByManufact(string manufID, ResultValidityEnum resultValidity)
        {
            var sql = new Sql()
                .Where("ar.manufID = @0", manufID);
            return GetOnStore(resultValidity, sql);
        }


        /// <summary>Return store status by manufacturer.</summary>
        /// <param name="carrierNum">The carrier number.</param>
        /// <param name="resultValidity">The required validity.</param>
        /// <returns></returns>
        public List<OnStoreItemFull> ByCarrier(string carrierNum, ResultValidityEnum resultValidity)
        {
            var sql = new Sql()
                .Where("onStore.carrierNum = @0", carrierNum);
            return GetOnStore(resultValidity, sql);
        }


        /// <summary>Return number of NotValid items on positions</summary>
        /// <param name="positionIDs">List of position identifiers.</param>
        /// <returns></returns>
        public List<CheckNotValidItem> CheckNotValidMoves(List<int> positionIDs)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var sql = new Sql()
                    .Append("SELECT positionID, COUNT(*) AS movesCount ")
                    .Append("FROM [dbo].[s4_storeMove_items] ")
                    .Append("WHERE itemValidity = @0", StoreMoveItem.MI_VALIDITY_NOT_VALID)
                    .Append("AND positionID IN (@0)", positionIDs)
                    .Append("GROUP BY positionID")
                    .Append("HAVING SUM(quantity * itemDirection) <> 0");

                var result = db.Fetch<CheckNotValidItem>(sql);
                return result;
            }
        }

        /// <summary>
        /// Return number of NotValid items by article
        /// </summary>
        /// <param name="articleID"></param>
        /// <returns></returns>
        public List<CheckNotValidItem> CheckNotValidMovesByArticle(int articleID)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var sql = new Sql()
                    .Append("SELECT positionID, COUNT(*) AS movesCount")
                    .Append("FROM[dbo].[s4_storeMove_items] i")
                    .Append("LEFT JOIN s4_storeMove_lots l on l.stoMoveLotID = i.stoMoveLotID")
                    .Append("WHERE itemValidity = @0", StoreMoveItem.MI_VALIDITY_NOT_VALID)
                    .Append("AND l.artPackID IN(SELECT artPackID FROM s4_articleList_packings WHERE articleID = @0)", articleID)
                    .Append("GROUP BY positionID")
                    .Append("HAVING SUM(quantity * itemDirection) <> 0");

                var result = db.Fetch<CheckNotValidItem>(sql);
                return result;
            }
        }

        //TODO123 - testy
        public List<OnPosition> ScanSMPositionCarrierDirection(int positionID, string carrierNum, int directionID, bool inclNotSaved)
        {
            var validitySet = inclNotSaved ? $"{StoreMoveItem.MI_VALIDITY_NOT_VALID}, {StoreMoveItem.MI_VALIDITY_VALID}" : StoreMoveItem.MI_VALIDITY_VALID.ToString();

            var sql = new Sql()
                .Append("SELECT lot.artPackID, lot.batchNum, lot.expirationDate, itm.carrierNum, SUM(itm.quantity * itm.itemDirection) AS quantity ")
                .Append("FROM s4_storeMove doc")
                .Append("JOIN s4_storeMove_items itm ON (doc.stoMoveID = itm.stoMoveID) ")
                .Append("JOIN s4_storeMove_lots lot ON (itm.stoMoveLotID = lot.stoMoveLotID) ")
                .Append($"WHERE (itm.itemValidity IN ({validitySet})) ")
                .Append("AND (itm.positionID = @0) ", positionID)
                .Append($"AND (doc.parent_directionID = @0) ", directionID);

            if (!string.IsNullOrWhiteSpace(carrierNum))
                sql.Append($"AND (itm.carrierNum = @0) ", carrierNum);

            sql                
                .Append("GROUP BY lot.artPackID, lot.batchNum, lot.expirationDate, itm.carrierNum ")
                .Append("HAVING (SUM(itm.quantity * itm.itemDirection) != 0) ");

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                return db.Fetch<OnPosition>(sql);
            }
        }


        public List<OnPosition> ScanSMPositionDirection(int positionID, int directionID, bool inclNotSaved)
        {
            return ScanSMPositionCarrierDirection(positionID, null, directionID, inclNotSaved);
        }

        public List<int> GetStoreMoveItemIDsInSummary(List<int> storeMoveItemIDs)
        {
            throw new NotImplementedException();
        }

        #region private


        private List<OnStoreItemFull> GetOnStore(ResultValidityEnum resultValidity, Sql sqlWhere, Sql prepareSql = null)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                db.KeepConnectionAlive = true;
                try
                {

                    if (prepareSql != null)
                        db.Execute(prepareSql);

                    var result = GetOnStoreInternal(db, sqlWhere, resultValidity);
                    RepairEmptyFields_Show(result);
                    return result;
                }
                finally
                {
                    db.KeepConnectionAlive = false;
                }
            }
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NPoco;
using S4.Core;
using S4.Core.Data;
using S4.Core.PropertiesComparer;
using S4.Entities;
using S4.StoreCore.Interfaces;


namespace S4.StoreCore.Save_Basic
{
    public class Save : BasicBase, ISave
    {
        #region ISave

        public List<MoveResult> MoveResult_ByDirection(IDatabase db, int parentDirectionID, int docType, Func<StoreMoveItem, bool> moveFilter)
        {
            // get all connected move items
            var sql = new Sql();
            sql.Append("SELECT stoMoveID FROM s4_storeMove WHERE parent_directionID = @0 AND docType = @1", parentDirectionID, docType);
            var movesIDs = db.Fetch<int>(sql);
            var moveItems = db.Query<StoreMoveItem>()
                .Where(i => movesIDs.Contains(i.StoMoveID))
                .Where(i => i.ItemValidity == StoreMoveItem.MI_VALIDITY_VALID)
                //.Where(i => i.PositionID == positionID)
                .ToList();

            // apply filter function
            if (moveFilter != null)
                moveItems = moveItems.Where(mi => moveFilter(mi)).ToList();

            // make summary
            var result = moveItems
                .GroupBy(
                    k => new { k.Parent_docPosition, k.PositionID, k.StoMoveLotID, k.CarrierNum, k.StoMoveItemID },
                    (k, e) => new MoveResult() { DocPosition = k.Parent_docPosition, PositionID = k.PositionID, StoMoveLotID = k.StoMoveLotID, CarrierNum = k.CarrierNum, Quantity = e.Sum(i => i.Quantity * i.ItemDirection), StoMoveItemID = k.StoMoveItemID })
                .ToList();

            // find lots
            result.ForEach(item =>
            {
                var lot = db.Query<StoreMoveLot>().Where(l => l.StoMoveLotID == item.StoMoveLotID).Single();
                item.ArtPackID = lot.ArtPackID;
                item.BatchNum = lot.BatchNum;
                item.ExpirationDate = lot.ExpirationDate;
            });

            RepairEmptyFields_Show(result);
            return result;
        }

        public int ResetPosition(IDatabase db, string userID, int positionID, string docNumPrefix, List<SaveItem> items, Action<StoreMove> beforeSave, out string message, bool selectedArticlesOnly = false)
        {
            message = null;

            // check
            if (String.IsNullOrWhiteSpace(docNumPrefix))
                throw new ArgumentNullException("docNumPrefix");
            var position = db.SingleOrDefaultById<Position>(positionID);
            if (position == null)
                throw new ArgumentException("Position not found");
            if (position.PosStatus != Position.POS_STATUS_STOCKTAKING)
                throw new ArgumentException("Position is not in STOCKTAKING status");

            // prepare
            var saveTime = DateTime.Now;

            // create storeMove
            var move = new StoreMove()
            {
                DocStatus = StoreMove.MO_STATUS_SAVED_STOCKTAKING,
                DocNumPrefix = docNumPrefix,
                DocType = StoreMove.MO_DOCTYPE_STOCKTAKING,
                DocDate = saveTime,
                Parent_directionID = StoreMove.NO_PARENT_ID,
                EntryDateTime = saveTime,
                EntryUserID = userID
            };

            // create out items - to reset position
            // scan current status
            var sql = new Sql().Where("onStore.positionID = @0", positionID);
            var currentOnStore = GetOnStoreInternal(db, sql, ResultValidityEnum.Valid);
            //reset only particular items - defined by artPackID in items
            if (selectedArticlesOnly)
            {
                var artPackIDS = items.Select(i => i.ArtPackID).Distinct().ToArray();
                currentOnStore = currentOnStore.Where(i => artPackIDS.Contains(i.ArtPackID)).ToList();
            }
            var onStore_saveItems = currentOnStore.Select(i => new SaveItem()
            {
                ArtPackID = i.ArtPackID,
                CarrierNum = i.CarrierNum,
                Quantity = i.Quantity,
                ExpirationDate = i.ExpirationDate,
                BatchNum = i.BatchNum
            });

            // save
            var allItemsTuple = GetRepairSaveItems(onStore_saveItems, items, positionID, userID, saveTime).ToList();
            try
            {
                DoSave(db, move, allItemsTuple, beforeSave, null);
            }
            catch (Exception ex)
            {
                if (IsInSummaryException(ex))
                {
                    message = "Inventura obsahuje položky, které jsou v již uzavřeném období, operaci nelze provést!";
                    return -1;
                }
                throw ex;
            }

            return move.StoMoveID;
        }

        public IEnumerable<Tuple<SaveItem, StoreMoveItem>> GetRepairSaveItems(IEnumerable<SaveItem> current, IEnumerable<SaveItem> required, int positionID, string userID, DateTime saveTime)
        {
            RepairEmptyFields_Save(current.ToList());
            RepairEmptyFields_Save(required.ToList());
            var result = new List<Tuple<SaveItem, StoreMoveItem>>();
            var docPosCounter = 1;

            Func<IEnumerable<SaveItem>, List<SaveItem>> getSummarized = (items) =>
            {
                return items
                    .GroupBy(k => new { k.ArtPackID, k.BatchNum, k.CarrierNum, ExpirationDate = k.ExpirationDate.Value.Date },
                             (k, i) => new SaveItem()
                             {
                                 ArtPackID = k.ArtPackID,
                                 BatchNum = k.BatchNum,
                                 CarrierNum = k.CarrierNum,
                                 ExpirationDate = k.ExpirationDate,
                                 Quantity = i.Sum(s => s.Quantity)
                             })
                    .ToList();
            };

            Action<SaveItem, decimal> addDiffItem = (item, quantity) =>
            {
                result.Add(new Tuple<SaveItem, StoreMoveItem>(item, new StoreMoveItem()
                {
                    DocPosition = docPosCounter++,
                    ItemValidity = StoreMoveItem.MI_VALIDITY_VALID,
                    ItemDirection = quantity < 0 ? StoreMoveItem.MI_DIRECTION_OUT : StoreMoveItem.MI_DIRECTION_IN,
                    PositionID = positionID,
                    CarrierNum = item.CarrierNum,
                    Quantity = Math.Abs(quantity),
                    BrotherID = StoreMoveItem.NO_BROTHER_ID,
                    EntryDateTime = saveTime,
                    EntryUserID = userID
                }));
            };

            //process differences on items, where quantity has changed or in required state are missing
            var currentSum = getSummarized(current);
            var requiredSum = getSummarized(required);
            foreach (var item in currentSum)
            {
                var requiredItem = requiredSum
                    .Where(i => i.ArtPackID == item.ArtPackID && i.BatchNum == item.BatchNum && i.ExpirationDate == item.ExpirationDate && i.CarrierNum == item.CarrierNum)
                    .FirstOrDefault();

                decimal diffQuantity = 0;
                if (requiredItem == null)
                    diffQuantity = -item.Quantity;
                else
                    diffQuantity = requiredItem.Quantity - item.Quantity;

                if (diffQuantity != 0)
                {
                    addDiffItem(item, diffQuantity);
                }
            }

            //process new items in required state
            foreach (var item in requiredSum)
            {
                var currentItem = currentSum
                    .Where(i => i.ArtPackID == item.ArtPackID && i.BatchNum == item.BatchNum && i.ExpirationDate == item.ExpirationDate && i.CarrierNum == item.CarrierNum)
                    .FirstOrDefault();

                if (currentItem == null)
                    addDiffItem(item, item.Quantity);
            }

            return result;
        }

        public int StoreIn(IDatabase db, string userID, int positionID, string docNumPrefix, List<SaveItem> items, bool asValid, Action<StoreMove> beforeSave, Action<SaveItem, StoreMoveItem> beforeSaveItem, out string message)
        {
            message = null;

            // check
            if (String.IsNullOrWhiteSpace(docNumPrefix))
                throw new ArgumentNullException("docNumPrefix");
            if (!items.Any())
                throw new ArgumentException("No items to save");
            var position = db.SingleOrDefaultById<Position>(positionID);
            if (position == null)
                throw new ArgumentException("Position not found");
            if (!(position.PosCateg.HasFlag(Position.PositionCategoryEnum.Receive) || position.PosCateg.HasFlag(Position.PositionCategoryEnum.Special)))
                throw new ArgumentException("Position is not Receive position");


            // prepare
            RepairEmptyFields_Save(items);
            var saveTime = DateTime.Now;

            // create storeMove
            var move = new StoreMove()
            {
                DocStatus = StoreMove.MO_STATUS_SAVED_RECEIVE,
                DocNumPrefix = docNumPrefix,
                DocType = StoreMove.MO_DOCTYPE_RECEIVE,
                DocDate = saveTime,
                Parent_directionID = StoreMove.NO_PARENT_ID,
                EntryDateTime = saveTime,
                EntryUserID = userID
            };

            // create items
            var docPosCounter = 1;
            var moveItemsTuple = items.Select(i => new Tuple<SaveItem, StoreMoveItem>(i, new StoreMoveItem()
            {
                DocPosition = docPosCounter++,
                ItemValidity = (asValid) ? StoreMoveItem.MI_VALIDITY_VALID : StoreMoveItem.MI_VALIDITY_NOT_VALID,
                ItemDirection = StoreMoveItem.MI_DIRECTION_IN,
                PositionID = positionID,
                CarrierNum = i.CarrierNum,
                Quantity = i.Quantity,
                Parent_docPosition = i.Parent_docPosition,
                BrotherID = StoreMoveItem.NO_BROTHER_ID,
                EntryDateTime = saveTime,
                EntryUserID = userID
            }
            )).ToList();

            // save
            try
            {
                DoSave(db, move, moveItemsTuple, beforeSave, beforeSaveItem);
            }
            catch (Exception ex)
            {
                if (IsInSummaryException(ex))
                {
                    message = "Doklad obsahuje položky, které jsou v již uzavřeném období, operaci nelze provést!";
                    return -1;
                }
                throw ex;
            }

            return move.StoMoveID;
        }


        public int Move(IDatabase db, string userID, string docNumPrefix, int docType, List<MoveItem> items, bool asValid, Action<StoreMove> beforeSave, Action<SaveItem, StoreMoveItem> beforeSaveItem, out string message)
        {
            message = null;

            // check
            if (String.IsNullOrWhiteSpace(docNumPrefix))
                throw new ArgumentNullException("docNumPrefix");
            if (!items.Any())
                throw new ArgumentException("No items to save");

            // prepare
            RepairEmptyFields_Save(items);
            var saveTime = DateTime.Now;

            // create storeMove
            var move = new StoreMove()
            {
                DocStatus = StoreMove.MO_STATUS_SAVED_MOVE,
                DocNumPrefix = docNumPrefix,
                DocType = docType,
                DocDate = saveTime,
                Parent_directionID = StoreMove.NO_PARENT_ID,
                EntryDateTime = saveTime,
                EntryUserID = userID
            };

            // create items
            var moveItemsTuple = items.Select(i => new Tuple<MoveItem, StoreMoveItem>(i, new StoreMoveItem()
            {
                ItemValidity = (asValid) ? StoreMoveItem.MI_VALIDITY_VALID : StoreMoveItem.MI_VALIDITY_NOT_VALID,
                ItemDirection = StoreMoveItem.MI_DIRECTION_OUT,
                PositionID = i.SourcePositionID,
                CarrierNum = i.CarrierNum,
                Quantity = i.Quantity,
                Parent_docPosition = i.Parent_docPosition,
                EntryDateTime = saveTime,
                EntryUserID = userID
            }
            )).ToList();

            // save
            move.DocNumber = Singleton<DAL.DocNumbers.NextNumberBase>.Instance.NextNumber_StoreMove.GetNextDocNumber(db, move.DocNumPrefix);
            beforeSave?.Invoke(move);
            db.Insert(move);

            // complete items
            moveItemsTuple.ForEach(i =>
            {
                i.Item2.StoMoveID = move.StoMoveID;

                // try to find lot
                var lot = FindOrCreateLot(db, i.Item1);
                i.Item2.StoMoveLotID = lot.StoMoveLotID;
            });

            // save items
            try
            {
                var docPosCounter = 1;
                moveItemsTuple.ForEach(i =>
                {
                    // insert 'from' item
                    var from = i.Item2;
                    from.StoMoveID = move.StoMoveID;
                    from.DocPosition = docPosCounter++;

                    beforeSaveItem?.Invoke(i.Item1, from);
                    db.Insert(from);

                    // create 'to' item
                    var to = new StoreMoveItem()
                    {
                        StoMoveID = move.StoMoveID,
                        DocPosition = docPosCounter++,
                        ItemValidity = from.ItemValidity,
                        ItemDirection = StoreMoveItem.MI_DIRECTION_IN,
                        PositionID = i.Item1.DestinationPositionID,
                        CarrierNum = from.CarrierNum,
                        StoMoveLotID = from.StoMoveLotID,
                        Quantity = from.Quantity,
                        Parent_docPosition = from.Parent_docPosition,
                        BrotherID = from.StoMoveItemID,
                        EntryDateTime = saveTime,
                        EntryUserID = userID
                    };


                    // insert 'to' item
                    beforeSaveItem?.Invoke(i.Item1, to);
                    db.Insert(to);

                    // update 'from' item
                    from.BrotherID = to.StoMoveItemID;
                    db.Update(from);
                });
            }
            catch (Exception ex)
            {
                if (IsInSummaryException(ex))
                {
                    message = "Doklad obsahuje položky, které jsou v již uzavřeném období, operaci nelze provést!";
                    return -1;
                }
                throw ex;
            }

            return move.StoMoveID;
        }


        public int StoreOut(IDatabase db, string userID, int positionID, string docNumPrefix, List<SaveItem> items, bool asValid, Action<StoreMove> beforeSave, Action<SaveItem, StoreMoveItem> beforeSaveItem, out string message)
        {
            message = null;

            // check
            if (String.IsNullOrWhiteSpace(docNumPrefix))
                throw new ArgumentNullException("docNumPrefix");
            if (!items.Any())
                throw new ArgumentException("No items to save");
            var position = db.SingleOrDefaultById<Position>(positionID);
            if (position == null)
                throw new ArgumentException("Position not found");
            if (!position.PosCateg.HasFlag(Position.PositionCategoryEnum.Dispatch))
                throw new ArgumentException("Position is not Dispatch position");

            // prepare
            RepairEmptyFields_Save(items);
            var saveTime = DateTime.Now;

            // create storeMove
            var move = new StoreMove()
            {
                DocStatus = StoreMove.MO_STATUS_SAVED_EXPED,
                DocNumPrefix = docNumPrefix,
                DocType = StoreMove.MO_DOCTYPE_EXPED,
                DocDate = saveTime,
                Parent_directionID = StoreMove.NO_PARENT_ID,
                EntryDateTime = saveTime,
                EntryUserID = userID
            };

            // create items
            var docPosCounter = 1;
            var moveItemsTuple = items.Select(i => new Tuple<SaveItem, StoreMoveItem>(i, new StoreMoveItem()
            {
                DocPosition = docPosCounter++,
                ItemValidity = (asValid) ? StoreMoveItem.MI_VALIDITY_VALID : StoreMoveItem.MI_VALIDITY_NOT_VALID,
                ItemDirection = StoreMoveItem.MI_DIRECTION_OUT,
                PositionID = positionID,
                CarrierNum = i.CarrierNum,
                Quantity = i.Quantity,
                Parent_docPosition = i.Parent_docPosition,
                BrotherID = StoreMoveItem.NO_BROTHER_ID,
                EntryDateTime = saveTime,
                EntryUserID = userID
            }
            )).ToList();

            // save
            try
            {
                DoSave(db, move, moveItemsTuple, beforeSave, beforeSaveItem);
            }
            catch (Exception ex)
            {
                if (IsInSummaryException(ex))
                {
                    message = "Doklad obsahuje položky, které jsou v již uzavřeném období, operaci nelze provést!";
                    return -1;
                }
                throw ex;
            }

            return move.StoMoveID;
        }


        public bool CancelMove(IDatabase db, string userID, int stoMoveID, out string message)
        {
            message = null;

            // checks
            var move = db.SingleOrDefaultById<StoreMove>(stoMoveID);
            if (move == null)
                throw new ArgumentException($"Store move not found; stoMoveID: {stoMoveID}");
            if (move.DocStatus == StoreMove.MO_STATUS_CANCEL)
                throw new ArgumentException($"Store move is canceled; stoMoveID: {stoMoveID}");

            var items = db.Fetch<StoreMoveItem>(new Sql().Where("stoMoveID = @0", stoMoveID));

            // do cancel move
            move.DocStatus = StoreMove.MO_STATUS_CANCEL;
            items.ForEach(i => i.ItemValidity = StoreMoveItem.MI_VALIDITY_CANCELED);

            // save
            db.Update(move);
            try
            {
                items.ForEach(i => db.Update(i));
            }
            catch (Exception ex)
            {
                if (IsInSummaryException(ex))
                {
                    message = "Pohyb obsahuje položky, které jsou v již uzavřeném období, operaci nelze provést!";
                    return false;
                }
                throw ex;
            }

            return true;
        }


        public bool CancelMoveItem(IDatabase db, string userID, int stoMoveID, int stoMoveItemID, out string message)
        {
            message = null;

            // checks
            var move = db.SingleOrDefaultById<StoreMove>(stoMoveID);
            if (move == null)
                throw new ArgumentException($"Store move not found; stoMoveID: {stoMoveID}");
            if (move.DocStatus == StoreMove.MO_STATUS_CANCEL)
                throw new ArgumentException($"Store move is canceled; stoMoveID: {stoMoveID}");

            var firstItem = db.SingleOrDefault<StoreMoveItem>(new Sql().Where("stoMoveID = @0 AND stoMoveItemID = @1", stoMoveID, stoMoveItemID));
            if (firstItem == null)
                throw new ArgumentException($"Store move item not found; stoMoveItemID: {stoMoveItemID}");
            if (firstItem.ItemValidity == StoreMoveItem.MI_VALIDITY_CANCELED)
                throw new ArgumentException($"Store move item is already canceled; stoMoveItemID: {stoMoveItemID}");

            // try to find secondItem
            StoreMoveItem secondItem = null;
            if (firstItem.BrotherID != StoreMoveItem.NO_BROTHER_ID)
            {
                secondItem = db.SingleOrDefault<StoreMoveItem>(new Sql().Where("stoMoveID = @0 AND stoMoveItemID = @1", stoMoveID, firstItem.BrotherID));
                if (secondItem == null)
                    throw new Exception($"Store move items inconsistency - BrotherID item not found; stoMoveItemID: {stoMoveItemID}; brotherID: {firstItem.BrotherID}");
            }

            // do cancel move items
            firstItem.ItemValidity = StoreMoveItem.MI_VALIDITY_CANCELED;
            if (secondItem != null)
                secondItem.ItemValidity = StoreMoveItem.MI_VALIDITY_CANCELED;

            try
            {
                // save
                db.Update(firstItem);
                if (secondItem != null)
                    db.Update(secondItem);
            }
            catch (Exception ex)
            {
                if (IsInSummaryException(ex))
                {
                    message = "Pohyb je v již uzavřeném období, operaci nelze provést!";
                    return false;
                }
                throw ex;
            }

            return true;
        }


        public bool CancelByDirection(IDatabase db, string userID, int directionID, bool onlyNotValid, out string message)
        {
            message = null;

            // checks 
            var direction = db.SingleOrDefaultById<Direction>(directionID);
            if (direction == null)
                throw new ArgumentException($"Direction not found; directionID: {directionID}");

            var moves = db.Query<StoreMove>().Where(sm => sm.Parent_directionID == directionID).ToList();

            try
            {
                // save
                moves.ForEach(storeMove =>
                {
                    var items = db.Query<StoreMoveItem>().Where(si => si.StoMoveID == storeMove.StoMoveID).ToList();
                    items.ForEach(item =>
                    {
                        var doCancel = (onlyNotValid) ? (item.ItemValidity == StoreMoveItem.MI_VALIDITY_NOT_VALID) : (item.ItemValidity != StoreMoveItem.MI_VALIDITY_CANCELED);
                        if (doCancel)
                        {
                            item.ItemValidity = StoreMoveItem.MI_VALIDITY_CANCELED;
                            db.Update(item);
                        }
                    });

                    // check storemove - if not canceled & has only canceled items -> cancel it
                    if (storeMove.DocStatus != StoreMove.MO_STATUS_CANCEL && items.All(i => i.ItemValidity == StoreMoveItem.MI_VALIDITY_CANCELED))
                    {
                        storeMove.DocStatus = StoreMove.MO_STATUS_CANCEL;
                        db.Update(storeMove);
                    }
                });
            }
            catch (Exception ex)
            {
                if (IsInSummaryException(ex))
                {
                    message = "Příkaz obsahuje položky, které jsou v již uzavřeném období, operaci nelze provést!";
                    return false;
                }
                throw ex;
            }

            return true;
        }


        public bool ValidateMoveItem(IDatabase db, string userID, int stoMoveID, int stoMoveItemID, string destCarrierNum, out string message)
        {
            message = null;

            // checks
            var move = db.SingleOrDefaultById<StoreMove>(stoMoveID);
            if (move == null)
                throw new ArgumentException($"Store move not found; stoMoveID: {stoMoveID}");
            if (move.DocStatus == StoreMove.MO_STATUS_CANCEL)
                throw new ArgumentException($"Store move is canceled; stoMoveID: {stoMoveID}");

            var firstItem = db.SingleOrDefault<StoreMoveItem>(new Sql().Where("stoMoveID = @0 AND stoMoveItemID = @1", stoMoveID, stoMoveItemID));
            if (firstItem == null)
                throw new ArgumentException($"Store move item not found; stoMoveItemID: {stoMoveItemID}");
            if (firstItem.ItemValidity == StoreMoveItem.MI_VALIDITY_CANCELED)
                throw new ArgumentException($"Store move item is canceled; stoMoveItemID: {stoMoveItemID}");

            // try to find secondItem
            StoreMoveItem secondItem = null;
            if (firstItem.BrotherID != StoreMoveItem.NO_BROTHER_ID)
            {
                secondItem = db.SingleOrDefault<StoreMoveItem>(new Sql().Where("stoMoveID = @0 AND stoMoveItemID = @1", stoMoveID, firstItem.BrotherID));
                if (secondItem == null)
                    throw new Exception($"Store move items inconsistency - BrotherID item not found; stoMoveItemID: {stoMoveItemID}; brotherID: {firstItem.BrotherID}");
            }

            // make items valid
            firstItem.ItemValidity = StoreMoveItem.MI_VALIDITY_VALID;
            if (secondItem != null)
            {
                secondItem.ItemValidity = StoreMoveItem.MI_VALIDITY_VALID;
                if (!string.IsNullOrWhiteSpace(destCarrierNum))
                    secondItem.CarrierNum = destCarrierNum;
            }

            try
            {
                // save
                db.Update(firstItem);
                if (secondItem != null)
                    db.Update(secondItem);
            }
            catch (Exception ex)
            {
                if (IsInSummaryException(ex))
                {
                    message = "Pohyb je v již uzavřeném období, operaci nelze provést!";
                    return false;
                }
                throw ex;
            }

            return true;
        }


        public bool ChangeMoveItems(Database db, string userID, List<StoreMoveItem> moveItems, int artPackID, decimal quantity, out string message)
        {
            message = null;

            try
            {
                // change move items                
                moveItems.ForEach(mi =>
                {
                    var newLotID = GetNewLotID(db, mi.StoMoveLotID, artPackID);

                    // create history
                    var movePropertiesComparer = new PropertiesComparer<StoreMoveItem>();
                    movePropertiesComparer.DoCompare(mi, new { StoMoveLotID = newLotID, Quantity = quantity }, "StoMoveLotID", "Quantity");

                    var moveHistory = new StoreMoveHistory()
                    {
                        StoMoveID = mi.StoMoveID,
                        DocPosition = mi.DocPosition,
                        EventCode = StoreMove.MO_EVENT_ITEM_CHANGED,
                        EventData = movePropertiesComparer.JsonText,
                        EntryUserID = userID,
                        EntryDateTime = DateTime.Now
                    };

                    // update
                    mi.StoMoveLotID = newLotID;
                    mi.Quantity = quantity;
                    db.Update(mi);

                    // save history
                    db.Insert(moveHistory);
                });
            }
            catch (Exception ex)
            {
                if (IsInSummaryException(ex))
                {
                    message = "Pohyb je v již uzavřeném období, operaci nelze provést!";
                    return false;
                }
                throw ex;
            }

            return true;
        }

        public bool ChangeMoveItem2(Database db, string userID, StoreMoveItem moveItem, int artPackID, decimal quantity, string batchNum, DateTime? expirationDate, string carrierNum, out string message)
        {
            message = null;

            // create history changes
            var movePropertiesComparer = new PropertiesComparer<object>();

            try
            {
                // get new lot 
                var newLot = FindOrCreateLot(db,
                    new SaveItem()
                    {
                        ArtPackID = artPackID,
                        BatchNum = batchNum,
                        ExpirationDate = expirationDate
                    });

                // no changes
                if (moveItem.StoMoveLotID == newLot.StoMoveLotID && moveItem.Quantity == quantity) return true;

                // get old lot values
                var oldLot = db.SingleById<StoreMoveLot>(moveItem.StoMoveLotID);
                
                // compare changes
                movePropertiesComparer.DoCompare(new {
                    oldLot.ArtPackID,
                    oldLot.StoMoveLotID,
                    oldLot.BatchNum,
                    oldLot.ExpirationDate,
                    moveItem.Quantity
                }, new {
                    newLot.ArtPackID,
                    newLot.StoMoveLotID,
                    newLot.BatchNum,
                    newLot.ExpirationDate,
                    Quantity = quantity
                }, nameof(oldLot.ArtPackID), nameof(oldLot.StoMoveLotID), nameof(oldLot.BatchNum), nameof(oldLot.ExpirationDate), nameof(moveItem.Quantity));

                var moveHistory = new StoreMoveHistory()
                {
                    StoMoveID = moveItem.StoMoveID,
                    DocPosition = moveItem.DocPosition,
                    EventCode = StoreMove.MO_EVENT_ITEM_CHANGED,
                    EventData = movePropertiesComparer.JsonText,
                    EntryUserID = userID,
                    EntryDateTime = DateTime.Now
                };

                // cancel old move and create new
                moveItem.ItemValidity = StoreMoveItem.MI_VALIDITY_CANCELED;
                db.Update(moveItem);

                var lastMoveItem = db.Query<StoreMoveItem>().Where(_ => _.StoMoveID == moveItem.StoMoveID).OrderByDescending(_ => _.DocPosition).FirstOrDefault();
                var newDocPosition = lastMoveItem.DocPosition + 1;

                var newMoveItem = new StoreMoveItem
                {
                    StoMoveID = moveItem.StoMoveID,
                    DocPosition = newDocPosition,
                    ItemValidity = StoreMoveItem.MI_VALIDITY_VALID,
                    ItemDirection = moveItem.ItemDirection,
                    StoMoveLotID = newLot.StoMoveLotID,
                    PositionID = moveItem.PositionID,
                    _carrierNum = !string.IsNullOrWhiteSpace(carrierNum) ? carrierNum : StoreMoveItem.EMPTY_CARRIERNUM,
                    Quantity = quantity,
                    Parent_docPosition = moveItem.Parent_docPosition,
                    BrotherID = moveItem.BrotherID,
                    EntryDateTime = moveItem.EntryDateTime,
                    EntryUserID = moveItem.EntryUserID
                };

                // add new move item
                db.Insert(newMoveItem);

                // save history
                db.Insert(moveHistory);

                return true;
            }
            catch (Exception ex)
            {
                if (IsInSummaryException(ex))
                {
                    message = "Pohyb je v již uzavřeném období, operaci nelze provést!";
                    return false;
                }
                throw ex;
            }
        }


        public void InvalidateStorageLot(IDatabase db, string userID, int stoMoveLotID)
        {
            // do nothing - for future use
        }

        #endregion

        #region helpers

        /// save UDICode during lot creation only
        public StoreMoveLot FindOrCreateLot(IDatabase db, SaveItem saveItem, string udiCode = "")
        {
            var expirationDate = (saveItem.ExpirationDate == null) ? StoreMoveItem.EMPTY_EXPIRATION : saveItem.ExpirationDate?.Date;
            var batchNum = String.IsNullOrWhiteSpace(saveItem.BatchNum) ? StoreMoveItem.EMPTY_BATCHNUM : saveItem.BatchNum;
            var lot = db.SingleOrDefault<StoreMoveLot>(new Sql().Where("artPackID = @0 AND batchNum = @1 AND expirationDate = @2", saveItem.ArtPackID, batchNum, expirationDate));
            if (lot == null)
            {
                lot = new StoreMoveLot()
                {
                    ArtPackID = saveItem.ArtPackID,
                    BatchNum = saveItem.BatchNum,
                    ExpirationDate = expirationDate,
                    UDICode = udiCode
                };
                db.Insert(lot);
            }
            return lot;
        }

        private int GetNewLotID(Database db, int stoMoveLotID, int artPackID)
        {
            var oldLot = db.SingleById<StoreMoveLot>(stoMoveLotID);

            var newLot = FindOrCreateLot(db,
                new StoreCore.Interfaces.SaveItem()
                {
                    ArtPackID = artPackID,
                    BatchNum = oldLot._batchNum,
                    ExpirationDate = oldLot._expirationDate
                });

            return newLot.StoMoveLotID;
        }

        #endregion

        #region private

        private void DoSave(IDatabase db, StoreMove move, List<Tuple<SaveItem, StoreMoveItem>> moveItemsTuple, Action<StoreMove> beforeSave, Action<SaveItem, StoreMoveItem> beforeSaveItem)
        {
            move.DocNumber = Singleton<DAL.DocNumbers.NextNumberBase>.Instance.NextNumber_StoreMove.GetNextDocNumber(db, move.DocNumPrefix);
            beforeSave?.Invoke(move);
            db.Insert(move);

            // complete items
            moveItemsTuple.ForEach(i =>
            {
                i.Item2.StoMoveID = move.StoMoveID;

                // try to find lot
                var lot = FindOrCreateLot(db, i.Item1);
                i.Item2.StoMoveLotID = lot.StoMoveLotID;
            });

            // save items           
            if (beforeSaveItem != null)
            {
                foreach (var tuple in moveItemsTuple)
                    beforeSaveItem(tuple.Item1, tuple.Item2);
            }

            var toSave = moveItemsTuple.Select(i => i.Item2);
            db.InsertBatch(toSave, new BatchOptions() { BatchSize = 16 });
        }

        private bool IsInSummaryException(Exception ex)
        {
            return ex.Message.IndexOf(DAL.Consts.OnSummaryErrorText) == 0;
        }

        #endregion
    }
}

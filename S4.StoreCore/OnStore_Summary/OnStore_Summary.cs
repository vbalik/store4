﻿using NPoco;
using S4.Entities;
using S4.StoreCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace S4.StoreCore.OnStore_Summary
{
    public class OnStore_Summary : BasicBase, IOnStore
    {
        #region IOnStore

        public List<OnStoreItemFull> ByArticle(int articleID, ResultValidityEnum resultValidity, bool onlyOnStorePositions = false, string limitSectionID = null)
        {
            var sql = new Sql()
                .Where("ar.articleID = @0", articleID);
            if (onlyOnStorePositions)
                sql.Where($"(pos.posCateg = {(int)Position.PositionCategoryEnum.Store}) AND (pos.posStatus = '{Position.POS_STATUS_OK}')");
            if (limitSectionID != null)
                sql.Where($"(onStore.positionID IN (SELECT positionID FROM [dbo].[s4_positionList_sections] WHERE sectID = @0))", limitSectionID);

            return GetOnStore(resultValidity, sql);
        }


        public List<OnStoreItemFull> ByArticle(List<int> articleIDs, ResultValidityEnum resultValidity)
        {
            (Sql condition, Sql prepareSql) = Helpers.MakeListCondition("ar.articleID IN (@0)", articleIDs);
            return GetOnStore(resultValidity, condition, prepareSql);
        }


        public List<OnStoreItemFull> ByCarrier(string carrierNum, ResultValidityEnum resultValidity)
        {
            throw new NotImplementedException();
        }

        public List<OnStoreItemFull> ByManufact(string manufID, ResultValidityEnum resultValidity)
        {
            throw new NotImplementedException();
        }

        public List<CheckNotValidItem> CheckNotValidMoves(List<int> positionIDs)
        {
            throw new NotImplementedException();
        }

        public List<CheckNotValidItem> CheckNotValidMovesByArticle(int articleID)
        {
            throw new NotImplementedException();
        }

        public List<OnStoreItemFull> OnPosition(int positionID, ResultValidityEnum resultValidity)
        {
            var sql = new Sql()
                .Where("onStore.positionID = @0", positionID);
            return GetOnStore(resultValidity, sql);
        }


        public List<OnStoreItemFull> OnPosition(List<int> positionIDs, ResultValidityEnum resultValidity)
        {
            (Sql condition, Sql prepareSql) = Helpers.MakeListCondition("onStore.positionID IN (@0)", positionIDs);
            return GetOnStore(resultValidity, condition, prepareSql);
        }


        public List<OnPosition> ScanSMPositionCarrierDirection(int positionID, string carrierNum, int directionID, bool inclNotSaved)
        {
            throw new NotImplementedException();
        }

        public List<OnPosition> ScanSMPositionDirection(int positionID, int directionID, bool inclNotSaved)
        {
            throw new NotImplementedException();
        }

        public List<int> GetStoreMoveItemIDsInSummary(List<int> storeMoveItemIDs)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var sql = new Sql("select max(lastStoMoveItemID) from s4_onStoreSummary where onStoreSummStatus = 100");
                var maxStoMoveItemID = db.ExecuteScalar<int>(sql);

                return storeMoveItemIDs.Where(i => i <= maxStoMoveItemID).ToList();
            }
        }

        #endregion

        private List<OnStoreItemFull> GetOnStore(ResultValidityEnum resultValidity, Sql sqlWhere, Sql prepareSql = null)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                db.KeepConnectionAlive = true;
                try
                {
                    if (prepareSql != null)
                        db.Execute(prepareSql);


                    var sql = new Sql()
                        .Append("SELECT	onStore.positionID, onStore.carrierNum, onStore.quantity, onStore.stoMoveLotID, onStore.artPackID, onStore.batchNum, onStore.expirationDate, ar.articleID, ar.artPackID, ")
                        .Append("ar.articleID AS Article__articleID, ar.articleCode AS Article__articleCode, ar.articleDesc AS Article__articleDesc, ar.articleStatus AS Article__articleStatus, ar.articleType AS Article__articleType, ar.sectID AS Article__sectID, ar.unitDesc AS Article__unitDesc, ar.manufID AS Article__manufID, ar.useBatch AS Article__useBatch, ar.useExpiration AS Article__useExpiration,  ")
                        .Append("ar.artPackID AS ArticlePacking__artPackID, ar.packDesc AS ArticlePacking__packDesc, ar.packRelation AS ArticlePacking__packRelation, ar.movablePack AS ArticlePacking__movablePack, ar.packStatus AS ArticlePacking__packStatus, ar.barCodeType AS ArticlePacking__barCodeType, ar.barCode AS ArticlePacking__barCode, ar.inclCarrier AS ArticlePacking__inclCarrier, ar.packWeight AS ArticlePacking__packWeight, ar.packVolume AS ArticlePacking__packVolume, ")
                        .Append("pos.positionID AS Position__positionID, pos.posCode AS Position__posCode, pos.posDesc AS Position__posDesc, pos.houseID AS Position__houseID, pos.posCateg AS Position__posCateg, pos.posStatus AS Position__posStatus, pos.posHeight AS Position__posHeight, pos.posX AS Position__posX, pos.posY AS Position__posY, pos.posZ AS Position__posZ, pos.mapPos AS Position__mapPos ");

                    switch (resultValidity)
                    {
                        case ResultValidityEnum.Valid:
                            sql.Append("FROM [dbo].[s4_vw_onStore_Summary_Valid] onStore ");
                            break;

                        case ResultValidityEnum.NotValid:
                        case ResultValidityEnum.Reservation:
                        default:
                            throw new NotImplementedException(resultValidity.ToString());
                    }

                    sql
                        .Append("JOIN [dbo].[s4_vw_article_base] ar WITH (NOLOCK) ON (onStore.artPackID = ar.artPackID) ")
                        .Append("JOIN [dbo].[s4_positionList] pos WITH (NOLOCK) ON (onStore.positionID = pos.positionID) ")
                        .Append(sqlWhere);

                    var result = db.Fetch<OnStoreItemFull>(sql);

                    RepairEmptyFields_Show(result);
                    return result;
                }
                finally
                {
                    db.KeepConnectionAlive = false;
                }
            }
        }
    }
}

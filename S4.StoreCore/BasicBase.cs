﻿using System;
using System.Collections.Generic;
using System.Text;
using NPoco;
using S4.Core.Data;
using S4.Entities;
using S4.StoreCore.Interfaces;


namespace S4.StoreCore
{
    public abstract class BasicBase
    {
        protected List<OnStoreItemFull> GetOnStoreInternal(IDatabase db, Sql sqlWhere, ResultValidityEnum resultValidity)
        {
            var sql = new Sql()
                .Append("SELECT	onStore.positionID, onStore.carrierNum, onStore.quantity, onStore.stoMoveLotID, onStore.artPackID, onStore.batchNum, onStore.expirationDate, ar.articleID, ar.artPackID, ")
                .Append("ar.articleID AS Article__articleID, ar.articleCode AS Article__articleCode, ar.articleDesc AS Article__articleDesc, ar.articleStatus AS Article__articleStatus, ar.articleType AS Article__articleType, ar.sectID AS Article__sectID, ar.unitDesc AS Article__unitDesc, ar.manufID AS Article__manufID, ar.useBatch AS Article__useBatch, ar.useExpiration AS Article__useExpiration,  ")
                .Append("ar.artPackID AS ArticlePacking__artPackID, ar.packDesc AS ArticlePacking__packDesc, ar.packRelation AS ArticlePacking__packRelation, ar.movablePack AS ArticlePacking__movablePack, ar.packStatus AS ArticlePacking__packStatus, ar.barCodeType AS ArticlePacking__barCodeType, ar.barCode AS ArticlePacking__barCode, ar.inclCarrier AS ArticlePacking__inclCarrier, ar.packWeight AS ArticlePacking__packWeight, ar.packVolume AS ArticlePacking__packVolume, ")
                .Append("ar.specFeatures, ")
                .Append("pos.positionID AS Position__positionID, pos.posCode AS Position__posCode, pos.posDesc AS Position__posDesc, pos.houseID AS Position__houseID, pos.posCateg AS Position__posCateg, pos.posStatus AS Position__posStatus, pos.posHeight AS Position__posHeight, pos.posX AS Position__posX, pos.posY AS Position__posY, pos.posZ AS Position__posZ, pos.mapPos AS Position__mapPos ");

            switch (resultValidity)
            {
                case ResultValidityEnum.Valid:
                    sql.Append("FROM [dbo].[s4_vw_onStore_Valid] onStore ");
                    break;
                case ResultValidityEnum.NotValid:
                    sql.Append("FROM [dbo].[s4_vw_onStore_NotValid] onStore ");
                    break;
                case ResultValidityEnum.Reservation:
                    sql.Append("FROM [dbo].[s4_vw_onStore_Reservation] onStore ");
                    break;
                default:
                    throw new NotImplementedException(resultValidity.ToString());
            }

            sql
                .Append("JOIN [dbo].[s4_vw_article_base] ar WITH (NOLOCK) ON (onStore.artPackID = ar.artPackID) ")
                .Append("JOIN [dbo].[s4_positionList] pos WITH (NOLOCK) ON (onStore.positionID = pos.positionID) ")
                .Append(sqlWhere);

            var result = db.Fetch<OnStoreItemFull>(sql);
            return result;
        }


        protected void RepairEmptyFields_Save<T>(List<T> items) where T : SaveItem
        {
            var proxy = new EmptyDataProxy();
            items.ForEach(i =>
            {
                proxy.ToSave(i.CarrierNum, StoreMoveItem.EMPTY_CARRIERNUM, value => i.CarrierNum = value);
                proxy.ToSave(i.BatchNum, StoreMoveItem.EMPTY_BATCHNUM, value => i.BatchNum = value);
                proxy.ToSave(i.ExpirationDate, StoreMoveItem.EMPTY_EXPIRATION, value => i.ExpirationDate = value);
            });
        }



        protected void RepairEmptyFields_Show<T>(List<T> result) where T : OnStoreItem
        {
            var proxy = new EmptyDataProxy();
            result.ForEach(i =>
            {
                proxy.ToShow(i.CarrierNum, StoreMoveItem.EMPTY_CARRIERNUM, value => i.CarrierNum = value);
                proxy.ToShow(i.BatchNum, StoreMoveItem.EMPTY_BATCHNUM, value => i.BatchNum = value);
                proxy.ToShow(i.ExpirationDate, StoreMoveItem.EMPTY_EXPIRATION, value => i.ExpirationDate = value);
            });
        }
    }
}

﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace S4.StoreCore
{
    public class Helpers
    {
        private const int IN_CAUSE_LIMIT = 100;

        public static (Sql condition, Sql prepareSql) MakeListCondition(string where, List<int> ids)
        {
            if (ids.Count <= IN_CAUSE_LIMIT)
            {
                return (
                    new Sql().Where(where, ids),
                    null
                    );
            }
            else
            {
                // create temp table in db
                var tmpTableName = $"#{Guid.NewGuid().ToString().Replace('-', '_')}";
                var prepareSql = new Sql();
                prepareSql.Append($"CREATE TABLE {tmpTableName} (intValue int NOT NULL);");

                // split ids list to chunks (size 25)
                var idsParts = ids
                    .Select((x, i) => new { Index = i, Value = x })
                    .GroupBy(x => x.Index / 25)
                    .Select(x => x.Select(v => v.Value).ToList())
                    .ToList();

                // make insert sql-s
                var sb = new StringBuilder();
                foreach (var chunk in idsParts)
                {
                    sb.Append($"INSERT INTO {tmpTableName} (intValue) VALUES ");
                    bool first = true;
                    foreach (var id in chunk)
                    {
                        if (first)
                            first = false;
                        else
                            sb.Append(",");
                        sb.Append($"({id})");
                    }
                    sb.Append(";");
                }
                prepareSql.Append(sb.ToString());

                // change where string - IN from temp table
                where = where.Replace("@0", $"SELECT intValue FROM {tmpTableName}");

                return (
                    new Sql().Where(where),
                    prepareSql
                    );
            }
        }
    }
}

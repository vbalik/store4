﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.StoreCore.Interfaces
{
    public class CheckNotValidItem
    {
        public int PositionID { get; set; }
        public int MovesCount { get; set; }
    }
}

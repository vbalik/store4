﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.StoreCore.Interfaces
{
    public class OnStoreItemFull : OnStoreItem
    {
        public Entities.ArticlePacking ArticlePacking { get; set; }
        public Entities.Article Article { get; set; }
        public Entities.Position Position { get; set; }
    }
}

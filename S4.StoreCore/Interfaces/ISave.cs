﻿using System;
using System.Collections.Generic;
using NPoco;
using S4.Entities;


namespace S4.StoreCore.Interfaces
{
    public interface ISave
    {

        List<MoveResult> MoveResult_ByDirection(IDatabase db, int parentDirectionID, int docType, Func<StoreMoveItem, bool> moveFilter);

        int ResetPosition(IDatabase db, string userID, int positionID, string docNumPrefix, List<SaveItem> items, Action<StoreMove> beforeSave, out string message, bool lotsRedirection = false);

        int StoreIn(IDatabase db, string userID, int positionID, string docNumPrefix, List<SaveItem> items, bool asValid, Action<StoreMove> beforeSave, Action<SaveItem, StoreMoveItem> beforeSaveItem, out string message);

        int Move(IDatabase db, string userID, string docNumPrefix, int docType, List<MoveItem> items, bool asValid, Action<StoreMove> beforeSave, Action<SaveItem, StoreMoveItem> beforeSaveItem, out string message);

        int StoreOut(IDatabase db, string userID, int positionID, string docNumPrefix, List<SaveItem> items, bool asValid, Action<StoreMove> beforeSave, Action<SaveItem, StoreMoveItem> beforeSaveItem, out string message);

        bool CancelMove(IDatabase db, string userID, int stoMoveID, out string message);

        bool CancelMoveItem(IDatabase db, string userID, int stoMoveID, int stoMoveItemID, out string message);

        bool CancelByDirection(IDatabase db, string userID, int directionID, bool onlyNotValid, out string message);

        bool ValidateMoveItem(IDatabase db, string userID, int stoMoveID, int stoMoveItemID, string destCarrierNum, out string message);

        bool ChangeMoveItems(Database db, string userID, List<StoreMoveItem> moveItems, int artPackID, decimal quantity, out string message);

        bool ChangeMoveItem2(Database db, string userID, StoreMoveItem moveItem, int artPackID, decimal quantity, string batchNum, DateTime? expirationDate, string carrierNum, out string message);

        void InvalidateStorageLot(IDatabase db, string userID, int stoMoveLotID);
    }
}

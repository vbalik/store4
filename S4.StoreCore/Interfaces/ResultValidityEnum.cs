﻿namespace S4.StoreCore.Interfaces
{
    public enum ResultValidityEnum
    {
        Valid,
        NotValid,
        Reservation
    }
}
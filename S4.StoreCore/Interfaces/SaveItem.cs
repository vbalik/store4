﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.StoreCore.Interfaces
{
    public class SaveItem : OnPosition
    {
        public int Parent_docPosition { get; set; } = Entities.StoreMoveItem.NO_PARENT_DOC_POSITION;
        public int Xtra_PositionID { get; set; }
    }
}

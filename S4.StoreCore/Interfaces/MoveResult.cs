﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.StoreCore.Interfaces
{
    public class MoveResult : OnStoreItem
    {
        public int DocPosition { get; set; }
    }
}
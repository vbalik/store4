﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.StoreCore.Interfaces
{
    public class OnStoreItem : OnPosition
    {
        public int ArticleID { get; set; }
        public int PositionID { get; set; }
        public int StoMoveLotID { get; set; }
        public int StoMoveItemID { get; set; }
    }
}

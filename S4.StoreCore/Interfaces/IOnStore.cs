﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.StoreCore.Interfaces
{
    public interface IOnStore
    {
        /// <summary>Return store status by position.</summary>
        /// <param name="positionID">The position identifier.</param>
        /// <param name="resultValidity">The required validity.</param>
        /// <returns></returns>
        List<OnStoreItemFull> OnPosition(int positionID, ResultValidityEnum resultValidity);

        /// <summary>Return store status by position.</summary>
        /// <param name="positionIDs">List of position identifiers.</param>
        /// <param name="resultValidity">The required validity.</param>
        /// <returns></returns>
        List<OnStoreItemFull> OnPosition(List<int> positionIDs, ResultValidityEnum resultValidity);

        /// <summary>Return store status by article.</summary>
        /// <param name="articleID">The article identifier.</param>
        /// <param name="resultValidity">The required validity.</param>
        /// <param name="onlyOnStorePositions">Limit result - only Store positions with OK status.</param>
        /// <param name="limitSectionID">Limit result - only selected section.</param>
        /// <returns></returns>
        List<OnStoreItemFull> ByArticle(int articleID, ResultValidityEnum resultValidity, bool onlyOnStorePositions = false, string limitSectionID = null);

        /// <summary>Return store status by article.</summary>
        /// <param name="articleIDs">List of article identifiers.</param>
        /// <param name="resultValidity">The required validity.</param>
        /// <returns></returns>
        List<OnStoreItemFull> ByArticle(List<int> articleIDs, ResultValidityEnum resultValidity);

        /// <summary>Return store status by manufacturer.</summary>
        /// <param name="manufID">The manufacturer identifier.</param>
        /// <param name="resultValidity">The required validity.</param>
        /// <returns></returns>
        List<OnStoreItemFull> ByManufact(string manufID, ResultValidityEnum resultValidity);

        /// <summary>Return store status by manufacturer.</summary>
        /// <param name="carrierNum">The carrier number.</param>
        /// <param name="resultValidity">The required validity.</param>
        /// <returns></returns>
        List<OnStoreItemFull> ByCarrier(string carrierNum, ResultValidityEnum resultValidity);

        /// <summary>Return number of NotValid items on positions</summary>
        /// <param name="positionIDs">List of position identifiers.</param>
        /// <returns></returns>
        List<CheckNotValidItem> CheckNotValidMoves(List<int> positionIDs);

        /// <summary>
        /// Return number of NotValid items by article
        /// </summary>
        /// <param name="articleID"></param>
        /// <returns></returns>
        List<CheckNotValidItem> CheckNotValidMovesByArticle(int articleID);

        /// <summary>
        /// Returns list of storeMoveItemsID, which are not in summary
        /// </summary>
        /// <param name="storeMoveItemIDs">List of StoreMoveItemID</param>
        /// <returns></returns>
        List<int> GetStoreMoveItemIDsInSummary(List<int> storeMoveItemIDs);
     
        List<OnPosition> ScanSMPositionCarrierDirection(int positionID, string carrierNum, int directionID, bool inclNotSaved);
        List<OnPosition> ScanSMPositionDirection(int positionID, int directionID, bool inclNotSaved);
    }
}

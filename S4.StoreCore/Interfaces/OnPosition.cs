﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.StoreCore.Interfaces
{
    public class OnPosition
    {
        public int ArtPackID { get; set; }
        public string BatchNum { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public string CarrierNum { get; set; }
        public decimal Quantity { get; set; }
    }
}

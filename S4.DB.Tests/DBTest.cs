﻿using NLog.Fluent;
using S4.Core;
using S4.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestHelpers;
using Xunit;


namespace S4.DB.Tests
{
    public class DBTest : IClassFixture<DatabaseFixture>
    {
        private Random random = new Random(Guid.NewGuid().GetHashCode());
        private DatabaseFixture fixture;

        private Dictionary<string, Func<object>> fieldDefaults = new Dictionary<string, Func<object>>()
        {
            { "ExpirationDate", () => new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0) },
            { "DocPosition", () => 1 },
            { "ItemDirection", () => 1 },
            { "StotakStatus", () => StockTaking.STOCKTAKING_STATUS_OPENED },
            { "PosStatus", () => Position.POS_STATUS_OK },
            { "ManufStatus", () => Manufact.MAN_STATUS_OK },
            { "WorkerStatus", () => Worker.WK_STATUS_ONDUTY }
        };


        public DBTest(DatabaseFixture fixture)
        {
            this.fixture = fixture;
        }


        [Fact]
        public void DBConnection()
        {
            using (var db = GetDB())
            {
                var x = db.Fetch<Truck>();
                Assert.NotNull(x);
                Assert.True(x.Count > 0);
            }
        }

        [Theory]
        [InlineData("Truck")]
        [InlineData("Worker")]
        [InlineData("WorkerHistory", "Worker")]
        [InlineData("Section")]
        [InlineData("Manufact", "Section")]
        [InlineData("Article", "Manufact", "Section")]
        [InlineData("ArticlePacking", "Article", "Manufact", "Section")]
        [InlineData("ArticleHistory", "Article", "Manufact", "Section")]
        [InlineData("InternMessage")]
        [InlineData("Direction", "Partner")]
        [InlineData("DirectionAssign", "Direction", "Partner", "Worker")]
        [InlineData("DirectionHistory", "Direction", "Partner")]
        [InlineData("DirectionItem", "Direction", "Partner", "ArticlePacking", "Article", "Manufact", "Section")]
        [InlineData("DirectionXtra", "Direction", "Partner")]
        [InlineData("HouseList")]
        [InlineData("Partner")]
        [InlineData("PartnerAddress", "Partner")]
        [InlineData("PartnerHistory", "PartnerAddress", "Partner")]
        [InlineData("Position", "HouseList")]
        [InlineData("PositionSection", "Position", "Section", "HouseList")]
        [InlineData("Prefix")]
        [InlineData("Setting")]
        [InlineData("StockTaking")]
        [InlineData("StockTakingHistory", "StockTaking", "ArticlePacking", "Article", "Manufact", "Position", "Section", "HouseList")]
        [InlineData("StockTakingItem", "StockTaking", "ArticlePacking", "Article", "Manufact", "Section", "Position", "HouseList")]
        [InlineData("StockTakingPosition", "StockTaking", "Position", "HouseList")]
        [InlineData("StockTakingSnapshot", "ArticlePacking", "Article", "Position", "HouseList", "StockTaking", "Manufact", "Section")]
        [InlineData("StoreMove")]
        [InlineData("StoreMoveHistory", "StoreMove")]
        [InlineData("StoreMoveItem", "StoreMove", "Position", "HouseList", "StoreMoveLot", "ArticlePacking", "Article", "Manufact", "Section")]
        [InlineData("StoreMoveXtra", "StoreMove")]
        [InlineData("ClientError")]
        [InlineData("RemoteAction")]
        [InlineData("ExchangeError")]
        [InlineData("Certificate")]
        [InlineData("Booking", "PartnerAddress", "Partner", "StoreMoveLot", "ArticlePacking", "Article", "Manufact", "Section")]
        public void EntityTest(string entityName, params string[] parentEntityNames)
        {
            EntityTest_Switches(entityName, false, false, parentEntityNames);
        }


        [Theory]
        [InlineData("StoreMoveLot", true, true, "ArticlePacking", "Article", "Manufact", "Section")]
        public void EntityTest_Switches(string entityName, bool noUpdate, bool noDelete, params string[] parentEntityNames)
        {
            List<object> parentPocos = null;

            var type = Type.GetType($"S4.Entities.{entityName}, S4.Entities");
            Assert.NotNull(type);

            var notTestFields = new List<string>() { "R_Edit" };
            var ignoredProps = type.GetProperties().Where(prop => Attribute.IsDefined(prop, typeof(DBTestIgnoreAttribute))).Select(prop => prop.Name).ToList();
            var notTestFields2 = notTestFields.Union(ignoredProps).ToArray();

            // create parents, if needed
            if (parentEntityNames != null)
            {
                parentPocos = InitParents(parentEntityNames);
            }

            // create poco
            object poco = CreatePOCO(type);

            using (var db = GetDB())
            {                
                if (parentEntityNames != null)
                {
                    // set foreign keys
                    foreach (var parentPoco in parentPocos)                        
                        CopyPK(parentPoco, type, poco);
                }

                // CRUD

                // insert
                db.Insert(poco);
                var list = db.Fetch(type, new NPoco.Sql());
                Assert.True(list.Count > 0);

                // read
                var poco2 = db.Fetch(type, GetSQLWhere(type, poco));
                Assert.True(poco2.Count == 1);
                Assert.True(Extensions.PublicPropertiesEquals(type, poco, poco2[0], notTestFields2));

                if (!noUpdate)
                {
                    // update - set nullable to null
                    foreach (var property in type.GetProperties())
                    {
                        var requiredAttr = Attribute.IsDefined(property, typeof(System.ComponentModel.DataAnnotations.RequiredAttribute));
                        if (!requiredAttr && !property.PropertyType.IsArray)
                        {
                            var setMethod = property.GetSetMethod();
                            if (setMethod != null)
                                setMethod.Invoke(poco, new object[] { null });
                        }

                    }
                    db.Update(poco);
                    var poco4 = db.Fetch(type, GetSQLWhere(type, poco));
                    Assert.True(poco4.Count == 1);
                    Assert.True(Extensions.PublicPropertiesEquals(type, poco, poco4[0], notTestFields2));

                    // update - set enums
                    foreach (var property in type.GetProperties())
                    {
                        if (property.PropertyType.IsEnum)
                        {
                            var values = Enum.GetValues(property.PropertyType);
                            var newVal = values.GetValue(values.Length - 1);
                            property.GetSetMethod().Invoke(poco, new object[] { newVal });
                        }
                    }
                    db.Update(poco);
                    var poco5 = db.Fetch(type, GetSQLWhere(type, poco));
                    Assert.True(poco5.Count == 1);
                    Assert.True(Extensions.PublicPropertiesEquals(type, poco, poco5[0], notTestFields2));
                }

                if (!noDelete)
                {
                    // delete
                    db.Delete(poco);
                    var poco3 = db.Fetch(type, GetSQLWhere(type, poco));
                    Assert.True(poco3.Count == 0);
                }
            }
        }


        private List<object> InitParents(string[] parentEntityNames)
        {
            var result = new List<object>();
            using (var db = GetDB())
            {
                foreach (var entityName in parentEntityNames.Reverse())
                {
                    // create poco
                    var type = Type.GetType($"S4.Entities.{entityName}, S4.Entities");
                    Assert.NotNull(type);
                    var parentPoco = CreatePOCO(type);

                    // try to copy primary keys
                    foreach (var item in result)
                        CopyPK(item, type, parentPoco);

                    // save
                    db.Insert(parentPoco);

                    result.Add(parentPoco);
                }
            }
            return result;
        }


        private object CreatePOCO(Type type)
        {
            var ignoredProps = type.GetProperties().Where(prop => Attribute.IsDefined(prop, typeof(DBTestIgnoreAttribute))).Select(prop => prop.Name).ToList();
            var poco = Activator.CreateInstance(type);

            foreach (var property in type.GetProperties())
            {
                if (ignoredProps.Contains(property.Name))
                    continue;

                if (fieldDefaults.ContainsKey(property.Name))
                {
                    var val = fieldDefaults[property.Name]();
                    property.GetSetMethod().Invoke(poco, new object[] { val });
                }
                else
                {
                    switch (property.PropertyType.ToString())
                    {
                        case "System.String":
                            var maxLenAttr = property.GetCustomAttributes(typeof(System.ComponentModel.DataAnnotations.MaxLengthAttribute), false);
                            var maxLen = (maxLenAttr.Length == 0) ? 256 : ((System.ComponentModel.DataAnnotations.MaxLengthAttribute)maxLenAttr[0]).Length;
                            property.GetSetMethod()?.Invoke(poco, new object[] { GetRandomString(maxLen) });
                            break;

                        case "System.Boolean":
                            property.GetSetMethod()?.Invoke(poco, new object[] { (random.Next(1) == 0) });
                            break;

                        case "System.Decimal":
                            property.GetSetMethod()?.Invoke(poco, new object[] { (decimal)Math.Floor(random.NextDouble()) });
                            break;

                        case "System.DateTime":
                            var date = DateTime.Now;
                            date = new DateTime(date.Year, date.Month, date.Day, date.Hour, date.Minute, date.Second, date.Kind);
                            property.GetSetMethod()?.Invoke(poco, new object[] { date });
                            break;
                        case "System.Byte[]":
                            property.GetSetMethod()?.Invoke(poco, new object[] { new byte[] { 1 } });
                            break;
                        default:
                            Log.Warn($"Unknown type: {property.PropertyType.ToString()}");
                            break;
                    }
                }
            }

            return poco;
        }


        private void CopyPK(object parentPoco, Type type, object poco)
        {
            var parentType = parentPoco.GetType();
            var primaryKeys = PKFields(parentType);
            foreach (var part in primaryKeys)
            {
                var part2 = part.Trim();
                
                // get
                var propertyGet = parentType.GetProperty(part2, System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.IgnoreCase | System.Reflection.BindingFlags.GetProperty | System.Reflection.BindingFlags.Public);
                if (propertyGet == null)
                    continue;
                var value = propertyGet.GetGetMethod().Invoke(parentPoco, new object[] { });
                
                // set
                var propertySet = type.GetProperty(part2, System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.IgnoreCase | System.Reflection.BindingFlags.SetProperty | System.Reflection.BindingFlags.Public);
                if (propertySet == null)
                    continue;
                propertySet.GetSetMethod().Invoke(poco, new object[] { value });
            }
        }


        private static List<string> PKFields(Type type)
        {
            var primaryKeyAttr = type.GetCustomAttributes(typeof(NPoco.PrimaryKeyAttribute), false);
            var primaryKeyAttr2 = (NPoco.PrimaryKeyAttribute)primaryKeyAttr[0];
            var parts = primaryKeyAttr2.Value.Split(',');
            return parts.ToList();
        }


        private NPoco.Sql GetSQLWhere(Type type, object poco)
        {
            var primaryKeys = PKFields(type);

            var res = new NPoco.Sql();
            foreach (var part in primaryKeys)
            {
                var part2 = part.Trim();
                var property = type.GetProperty(part2, System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.IgnoreCase | System.Reflection.BindingFlags.GetProperty | System.Reflection.BindingFlags.Public);
                res.Where($"{part2} = @0", property.GetGetMethod().Invoke(poco, new object[] { }));
            }
            return res;
        }


        private string GetRandomString(int len)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[len];

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            return new String(stringChars);
        }


        private NPoco.Database GetDB()
        {
            var db = new NPoco.Database(this.fixture.ConnectionString, NPoco.DatabaseType.SqlServer2012, System.Data.SqlClient.SqlClientFactory.Instance);
            db.Mappers.Add(new Core.Data.NPocoEnumMapper());
            return db;
        }
    }
}

﻿using S4.Entities.XtraData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestHelpers;
using Xunit;


namespace S4.DB.Tests
{
    public class XtraProxyDBTests : IClassFixture<DatabaseFixture>
    {
        private DatabaseFixture fixture;

        public const string XTRA_EXPED_TRUCK = "EXPT";
        public const string XTRA_DISPATCH_POSITION = "DIPO";


        public static XtraDataDict XtraDataDict = new XtraDataDict(x =>
        {
            x.AddField(XTRA_EXPED_TRUCK, "Vozidlo", typeof(string));
            x.AddField(XTRA_DISPATCH_POSITION, "yyyyy", typeof(int), true);

        });

        public XtraProxyDBTests(DatabaseFixture fixture)
        {
            this.fixture = fixture;
        }


        [Fact]
        public void XtraProxyDBTests_DirectionXtra_Load()
        {
            using (var db = GetDB())
            {
                var xtraData = new XtraDataProxy<Entities.DirectionXtra>(XtraDataDict);
                xtraData.Load(db, new NPoco.Sql().Where("DirectionID = 1"));
                Assert.NotEmpty(xtraData.Data);
                Assert.Equal("ABA-1213", xtraData[XTRA_EXPED_TRUCK]);
                Assert.Equal(123, xtraData[XTRA_DISPATCH_POSITION, 1]);
            }
        }


        [Fact]
        public void XtraProxyDBTests_DirectionXtra_Save()
        {
            // create value
            using (var db = GetDB())
            {
                var xtraData = new XtraDataProxy<Entities.DirectionXtra>(XtraDataDict);
                xtraData[XTRA_EXPED_TRUCK] = "ABCD";
                xtraData[XTRA_DISPATCH_POSITION, 1] = 999;
                xtraData.Save(db, 2);
            }

            using (var db = GetDB())
            {
                var xtraData = new XtraDataProxy<Entities.DirectionXtra>(XtraDataDict);
                xtraData.Load(db, new NPoco.Sql().Where("DirectionID = 2"));
                Assert.Equal("ABCD", xtraData[XTRA_EXPED_TRUCK]);
                Assert.Equal(999, xtraData[XTRA_DISPATCH_POSITION, 1]);
            }

            // update value
            using (var db = GetDB())
            {
                var xtraData = new XtraDataProxy<Entities.DirectionXtra>(XtraDataDict);
                xtraData.Load(db, new NPoco.Sql().Where("DirectionID = 2"));
                xtraData[XTRA_EXPED_TRUCK] = "GHIJ";
                xtraData[XTRA_DISPATCH_POSITION, 1] = 666;
                xtraData.Save(db, 2);
            }

            using (var db = GetDB())
            {
                var xtraData = new XtraDataProxy<Entities.DirectionXtra>(XtraDataDict);
                xtraData.Load(db, new NPoco.Sql().Where("DirectionID = 2"));
                Assert.Equal("GHIJ", xtraData[XTRA_EXPED_TRUCK]);
                Assert.Equal(666, xtraData[XTRA_DISPATCH_POSITION, 1]);
            }

            //check null
            using (var db = GetDB())
            {
                var xtraData = new XtraDataProxy<Entities.DirectionXtra>(XtraDataDict);
                xtraData.Load(db, new NPoco.Sql().Where("DirectionID = 54"));
                Assert.Equal(1234567891, xtraData["DIPO"]);
                xtraData["DIPO"] = null;
                xtraData.Save(db, 54);
                xtraData.Load(db, new NPoco.Sql().Where("DirectionID = 54"));
                Assert.Null(xtraData["DIPO"]);

            }
        }


        private NPoco.Database GetDB()
        {
            var db = new NPoco.Database(this.fixture.ConnectionString, NPoco.DatabaseType.SqlServer2012, System.Data.SqlClient.SqlClientFactory.Instance);
            db.Mappers.Add(new Core.Data.NPocoEnumMapper());
            return db;
        }
    }
}

﻿using System;
using System.Linq;
using System.Threading;
using NUnit.Framework;
using Xamarin.UITest;

namespace UITests
{
    [TestFixture(Platform.Android)]
    public class BarcodePrintCarrierLabelsTests : TestBase
    {
        public BarcodePrintCarrierLabelsTests(Platform platform) : base(platform)
        {

        }

        [Test]
        public void PrintLabelsTest()
        {
            // login
            LoginApp();
 
            // tap tisk paletovych stitku
            app.ScrollDownTo(c => c.Marked("Pages.Print.PrintBarcodeCarrierLabelsPage"));
            app.Tap("Pages.Print.PrintBarcodeCarrierLabelsPage");

            // dialog load
            app.WaitForElement("Tisk paletových štítků");
            Assert.AreEqual(GetButton_Ok.Enabled, false);

            // select printer 1
            app.WaitForElement("reportPicker");
            app.Tap("reportPicker");
            app.Tap("Report 1");
            Assert.AreEqual(GetButton_Ok.Enabled, false);

            Thread.Sleep(600);

            app.Tap("printerPicker");
            app.Tap("Printer 1");
            Assert.AreEqual(GetButton_Ok.Enabled, true);

            // select printer 2
            app.WaitForElement("reportPicker");
            app.Tap("reportPicker");
            app.Tap("Report 2");

            Thread.Sleep(600);

            app.Tap("printerPicker");
            app.Tap("Printer 2");
            Assert.AreEqual(GetButton_Ok.Enabled, true);

            // set wrong quantity
            app.ClearText("edQuantity");
            app.EnterText("edQuantity", "500");
            Assert.AreEqual(app.Query("edQuantity").First().Text, "500");
            Assert.AreEqual(GetButton_Ok.Enabled, false);
            
            // set quantity
            app.ClearText("edQuantity");
            app.EnterText("edQuantity", "5");
            Assert.AreEqual(app.Query("edQuantity").First().Text, "5");
            Assert.AreEqual(GetButton_Ok.Enabled, true);

            // tap ok
            TapButtonOkWithAlertTitle("Informace");
        }
    }
}

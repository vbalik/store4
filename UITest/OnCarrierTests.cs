﻿using NUnit.Framework;
using System.Linq;
using Xamarin.UITest;

namespace UITests
{
    [TestFixture(Platform.Android)]
    public class OnCarrierTests : TestBase
    {
        public OnCarrierTests(Platform platform) : base(platform)
        {

        }

        [Test]
        public void OnCarrierTest()
        {
            // login
            LoginApp();

            // tap expedice
            app.ScrollDownTo(_ => _.Marked("Pages.OnStock.OnCarrierPage"));
            app.Tap("Pages.OnStock.OnCarrierPage");

            // select position
            app.WaitForElement("Zadejte paletu"); // header
            var carrEl = app.Query(x => x.Marked("edCarrierNumber")).First();

            // set bad carrier
            Assert.AreEqual(GetButton_Ok.Enabled, false);
            app.ClearText(carrEl.Id);
            app.EnterText(carrEl.Id, "1234567890123456789");
            
            Assert.AreEqual(GetButton_Ok.Enabled, false);

            // set good carrier
            var carrierNumber = "12345678901234567890";
            app.ClearText(carrEl.Id);
            app.EnterText(carrEl.Id, carrierNumber);
            
            Assert.AreEqual(GetButton_Ok.Enabled, true);
            
            app.Tap(GetButton_Ok.Id);

            app.WaitForElement($"Obsah palety {carrierNumber}"); // header

            Assert.AreEqual(2, app.Query(x => x.Class("ListView").Child("ConditionalFocusLayout")).Length);
        }
    }
}

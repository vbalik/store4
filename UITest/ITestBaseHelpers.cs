﻿using Xamarin.Forms;
using Xamarin.UITest.Queries;

namespace UITests
{
    public interface ITestBaseHelpers
    {
        /// <summary>
        /// Login to app by credentials
        /// </summary>
        void LoginApp();

        /// <summary>
        /// Get dialog buttons AppResult object
        /// </summary>
        /*  
         *  GetButton_Ok
         */

        /// <summary>
        /// Get int from xamarin color
        /// </summary>
        int GetIntColor(Color color);

        /// <summary>
        /// Tap on button OK and assert alert title
        /// </summary>
        /// <param name="title"></param>
        void TapButtonOkWithAlertTitle(string title);
    }
}

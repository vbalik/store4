﻿using System;
using System.Linq;
using System.Threading;
using NUnit.Framework;
using Xamarin.UITest;

namespace UITests
{
    [TestFixture(Platform.Android)]
    public class ReceiveTests : TestBase
    {
        private const string _PAGE = "Pages.Direction.Receive.ReceivePage";

        public ReceiveTests(Platform platform) : base(platform)
        {

        }

        [Test]
        public void ReceiveCancelItemTest()
        {
            // login
            LoginApp();

            app.ScrollDownTo(c => c.Marked(_PAGE));
            app.Tap(_PAGE);

            // dialog load
            app.WaitForElement("Příjem test");

            Assert.AreEqual(1, app.Query(x => x.Class("ListView").Child("ConditionalFocusLayout")).Length);

            var menu = app.Query(x => x.Class("ListView").Descendant("ConditionalFocusLayout").Index(0).Descendant("LabelRenderer")).First().Id;

            app.TouchAndHold(menu);

            app.Tap("Detail");
            app.WaitForElement("CODE1");
            app.Back();

            app.TouchAndHold(menu);

            app.Tap("Vytisknout štítek");
            app.WaitForElement("Výběr tiskárny");
            app.Back();

            app.TouchAndHold(menu);

            app.Tap("Odebrat položku");
            app.WaitForElement("alertTitle");
            app.Tap("button1");

            Assert.AreEqual(0, app.Query(x => x.Class("ListView").Child("ConditionalFocusLayout")).Length);
            
            app.Tap("Hotovo");
            app.WaitForElement("alertTitle");

            Assert.AreEqual("Informace", app.Query(_ => _.Marked("alertTitle")).First().Text);
            Assert.AreEqual("Uložit příjem?", app.Query(_ => _.Marked("message")).First().Text);
            
            app.Tap("button1");

            app.WaitForElement("alertTitle");

            Assert.AreEqual("Informace", app.Query(_ => _.Marked("alertTitle")).First().Text);
            Assert.AreEqual("Příjem dokončen. Tisknout doklad?", app.Query(_ => _.Marked("message")).First().Text);
            
            app.Tap("button1");

            app.WaitForElement("Výběr tiskárny");
        }

        [Test]
        public void ReceiveTest()
        {
            // login
            LoginApp();

            app.ScrollDownTo(c => c.Marked(_PAGE));
            app.Tap(_PAGE);

            // dialog load
            app.WaitForElement("Příjem test");

            Assert.AreEqual(1, app.Query(x => x.Class("ListView").Child("ConditionalFocusLayout")).Length);
            Assert.AreEqual("()", app.Query(x => x.Marked("carrier").Child())[1].Text);

            // check if first open and no carrier/without selected
            app.Tap(_ => _.Marked("Nová paleta"));
            app.WaitForElement("Zadejte paletu");
            Assert.AreEqual(GetButton_Ok.Enabled, false);

            var edCarrierNumber = app.Query(_ => _.Marked("edCarrierNumber")).First();
            app.EnterText(edCarrierNumber.Id, "1234567890123456789");
            Assert.AreEqual(GetButton_Ok.Enabled, false);

            app.ClearText(edCarrierNumber.Id);
            app.EnterText(edCarrierNumber.Id, "12345678901234567890");
            Assert.AreEqual(GetButton_Ok.Enabled, true);

            app.Tap(GetButton_Ok.Id);
            app.WaitForElement("Zadejte kartu");
            app.Back();

            Assert.AreEqual("(12345678901234567890)", app.Query(x => x.Marked("carrier").Child())[1].Text);

            app.Tap("Nová položka");
            app.WaitForElement("Zadejte kartu");
            app.Back();

            // end carrier and clear
            app.Tap("Ukončit paletu");
            Assert.AreEqual("()", app.Query(x => x.Marked("carrier").Child())[1].Text);
        }

        [Test]
        public void ReceiveWithoutCarrier()
        {
            // login
            LoginApp();

            app.ScrollDownTo(c => c.Marked(_PAGE));
            app.Tap(_PAGE);

            // dialog load
            app.WaitForElement("Příjem test");

            Assert.AreEqual(1, app.Query(x => x.Class("ListView").Child("ConditionalFocusLayout")).Length);
            Assert.AreEqual("()", app.Query(x => x.Marked("carrier").Child())[1].Text);

            // check if first open and no carrier/without selected
            app.Tap("Nová paleta");
            app.WaitForElement("Zadejte paletu");
            Assert.AreEqual(GetButton_Ok.Enabled, false);

            app.Tap("Bez palety");
            app.WaitForElement("Zadejte kartu");
            app.Back();

            Assert.AreEqual("(bez palety)", app.Query(x => x.Marked("carrier").Child())[1].Text);

            // new item
            app.Tap("Nová položka");
            app.WaitForElement("Zadejte kartu");

            app.EnterText(app.Query(_ => _.Marked("edBarCode")).First().Id, "55556666");
            app.PressEnter();

            app.WaitForElement("Množství");

            // check recount
            Assert.AreEqual("0 ks (1 bal = 10 ks)", app.Query(x => x.Marked("reCountQuantity")).First().Text);
            app.EnterText(app.Query(_ => _.Marked("edQuantity")).First().Id, "10");
            Assert.AreEqual("100 ks (1 bal = 10 ks)", app.Query(x => x.Marked("reCountQuantity")).First().Text);

            // check empty fields
            app.EnterText(app.Query(_ => _.Marked("edQuantity")).First().Id, "");
            app.PressEnter();

            app.WaitForElement("Množství"); // if error it is same page
            app.Tap("Nová položka");
            app.WaitForElement("Množství"); // if error it is same page

            app.ClearText(app.Query(_ => _.Marked("edQuantity")).First().Id);
            app.EnterText(app.Query(_ => _.Marked("edQuantity")).First().Id, "2");
            app.EnterText(app.Query(_ => _.Marked("edBatchNum")).First().Id, "222");
            app.EnterText(app.Query(_ => _.Marked("edExpirationDateShort")).First().Id, "2.2.28");

            app.Tap("Nová položka"); // save
        }
    }
}

﻿using System;
using System.IO;
using System.Linq;
using NUnit.Framework;
using Xamarin.Forms;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace UITests
{
    [TestFixture(Platform.Android)]
    //[TestFixture(Platform.iOS)]
    public class TestsLogin : TestBase
    {
        protected const string _USERNAME = "UserName";
        protected const string _PASSWORD = "Password";
        protected const string _BTN_LOGIN = "btnLogin";

        protected const string _MAIN_MENU = "MainMenu";

        public TestsLogin(Platform platform) : base(platform)
        {

        }

        [Test]
        public void LoginTest()
        {
            app.WaitForElement(_USERNAME);

            // check empty
            app.ClearText(_USERNAME);
            app.ClearText(_PASSWORD);
            app.Tap(_BTN_LOGIN);

            Assert.IsTrue(app.Query(x => x.Property("text").Contains("Vyplňte prosím")).Length == 1);
            Assert.AreEqual(GetIntColor(Color.Red), app.Query(x => x.Property("text").Contains("Vyplňte prosím").Invoke("getBackground").Invoke("getColor")).First());

            // check bad password 
            app.ClearText(_USERNAME);
            app.ClearText(_PASSWORD);
            app.EnterText(_USERNAME, "w1");
            app.EnterText(_PASSWORD, "w1");
            app.Tap(_BTN_LOGIN);

            Assert.IsTrue(app.Query(x => x.Property("text").Contains("Nepodařilo se")).Length == 1);
            Assert.AreEqual(GetIntColor(Color.Red), app.Query(x => x.Property("text").Contains("Nepodařilo se").Invoke("getBackground").Invoke("getColor")).First());

            // good password
            app.ClearText(_USERNAME);
            app.ClearText(_PASSWORD);
            app.EnterText(_USERNAME, "w1");
            app.EnterText(_PASSWORD, "p1");
            app.Tap(_BTN_LOGIN);

            app.WaitForElement(_MAIN_MENU);

            // logout
            app.Tap(x => x.Property("contentDescription").Contains("More"));
            app.Tap(app.Query(x => x.Marked("title").Property("text").Contains("Odhl")).First().Text);

            app.WaitForElement(x => x.Class("AppCompatButton"));
            app.Tap(app.Query(x => x.Class("AppCompatButton").Marked("Ano")).First().Text);
            app.WaitForElement(_USERNAME);
        }

        [Test]
        public void PingTest()
        {
            AppResult[] results = app.WaitForElement("UserName");

            Assert.IsTrue(results.Any());
        }
    }
}

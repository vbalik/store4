﻿using System.Linq;
using NUnit.Framework;
using Xamarin.Forms;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace UITests
{
    public class TestBase : ITestBaseHelpers
    {
        protected IApp app;
        protected Platform platform;

        public TestBase(Platform platform)
        {
            this.platform = platform;
        }

        [SetUp]
        public virtual void BeforeEachTest()
        {
            app = AppInitializer.StartApp(platform);
        }

        /// <see cref="ITestBaseHelpers"/>
        public void LoginApp()
        {
            app.WaitForElement("UserName");
            app.ClearText("UserName");
            app.EnterText("UserName", "w1");
            app.ClearText("Password");
            app.EnterText("Password", "p1");
            app.Tap("btnLogin");

            AppResult[] results = app.WaitForElement("MainMenu");

            Assert.IsTrue(results.Any());
        }

        /// <see cref="ITestBaseHelpers"/>
        public AppResult GetButton_Ok => app.Query(x => x.Marked("btnOK")).First();
        
        /// <see cref="ITestBaseHelpers"/>
        public int GetIntColor(Color color)
        {
            return int.Parse(color.ToHex().Replace("#", ""), System.Globalization.NumberStyles.HexNumber);
        }

        /// <see cref="ITestBaseHelpers"/>
        public void TapButtonOkWithAlertTitle(string title)
        {
            app.Tap("btnOK");

            app.WaitForElement("alertTitle");
            Assert.AreEqual(title, app.Query("alertTitle").First().Text);
        }
    }
}

﻿using S4mp.Droid.Interfaces;
using System;
using Xamarin.Forms;
using Xamarin.UITest;

namespace UITests
{
    public class AppInitializer
    {
        public static IApp StartApp(Platform platform)
        {
            if (platform == Platform.Android)
            {
                return ConfigureApp
                    .Android
                    .InstalledApp("S4mp.S4mp")
                    .PreferIdeSettings()
                    .StartApp();
            }

            return ConfigureApp.iOS.StartApp();
        }
    }
}
﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace UITests
{
    [TestFixture(Platform.Android)]
    //[TestFixture(Platform.iOS)]
    public class TestStockTaking : TestBase
    {
        private const string _PAGE = "Pages.StockTaking.StockTakingListPage";
        private const string _SEARCHPOS = "edPosition";
        private readonly string[] _POSITIONS = new string[] { "a1a01", "a1a02", "a1a03" };

        public TestStockTaking(Platform platform) : base(platform)
        {

        }

        [Test]
        public void NewStotakTest()
        {
            // login
            LoginApp();

            app.ScrollDownTo(_PAGE);
            app.Tap(_PAGE);
            app.WaitForElement("+");

            app.Tap("+");

            //not finished
            Assert.IsFalse(TestIsFinished());

            app.EnterText(_SEARCHPOS, _POSITIONS[0]);
            app.PressEnter();

            //finished
            Assert.IsTrue(TestIsFinished());
            Assert.AreEqual(1, app.Query("positionItem").Length);

            app.EnterText(_SEARCHPOS, _POSITIONS[0]);
            app.PressEnter();

            app.WaitForElement($"Pozice {_POSITIONS[0]} již byla přidána!");
            app.Back();

            Assert.AreEqual(1, app.Query("positionItem").Length);
            app.TouchAndHold("positionItem");

            var delete = "Vymazat";
            app.WaitForElement(delete);
            app.Tap(delete);

            Assert.AreEqual(0, app.Query("positionItem").Length);
            //not finished
            Assert.IsFalse(TestIsFinished());

            app.EnterText(_SEARCHPOS, _POSITIONS[1]);
            app.PressEnter();

            //finished
            Assert.IsTrue(TestIsFinished());
            Assert.AreEqual(1, app.Query("positionItem").Length);

            app.EnterText(_SEARCHPOS, _POSITIONS[2]);
            app.PressEnter();

            //finished
            Assert.IsTrue(TestIsFinished());
            Assert.AreEqual(2, app.Query("positionItem").Length);

            app.Tap(GetButton_Ok.Id);

            app.WaitForElement($"Inventura pozic {_POSITIONS[1]}, {_POSITIONS[2]}");

            app.Tap("+");

            //not finished
            Assert.IsFalse(TestIsFinished());

            app.EnterText(_SEARCHPOS, _POSITIONS[1]);
            app.PressEnter();

            app.WaitForElement($"Pozice {_POSITIONS[1]} je již v jiné inventuře!");
            app.Back();
        }

        private bool TestIsFinished()
        {
            return GetButton_Ok.Enabled;
        }
    }
}

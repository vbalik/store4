﻿using NUnit.Framework;
using System.Linq;
using Xamarin.UITest;

namespace UITests
{
    [TestFixture(Platform.Android)]
    public class OnPositionTests : TestBase
    {
        public OnPositionTests(Platform platform) : base(platform)
        {

        }

        [Test]
        public void SearchPositionMultipleTest()
        {
            // login
            LoginApp();

            SearchPositionMultiple();

            app.Back(); // go to menu

            SearchPositionSingle();
        }

        private void SearchPositionMultiple() 
        {
            // tap expedice
            app.ScrollDownTo(_ => _.Marked("Pages.OnStock.OnPositionPage"));
            app.Tap("Pages.OnStock.OnPositionPage");

            // select position
            app.WaitForElement("Zadejte pozici"); // header
            var positionEl = app.Query(x => x.Marked("edPosition")).First();

            var SEARCH_BUTTON = app.Query(x => x.Marked("edPosition").Parent().Child().Property("class").Contains("Button")).First().Id;

            // set bad position
            app.ClearText(positionEl.Id);
            app.EnterText(positionEl.Id, "badposition");
            app.Tap(SEARCH_BUTTON);
            app.WaitForElement("Zadejte pozici"); // same dialog

            // set multiple
            app.ClearText(positionEl.Id);
            app.EnterText(positionEl.Id, "a1");
            app.Tap(SEARCH_BUTTON);

            Assert.AreEqual(3, app.Query(x => x.Class("ListView").Child("ConditionalFocusLayout")).Length);

            app.Tap(app.Query(x => x.Class("ListView").Child("ConditionalFocusLayout")).First().Id);
            app.WaitForElement("Obsah pozice a1a02"); // header

            Assert.AreEqual(2, app.Query(x => x.Class("ListView").Child("ConditionalFocusLayout")).Length);
        }

        private void SearchPositionSingle()
        {
            // tap expedice
            app.ScrollDownTo(_ => _.Marked("Pages.OnStock.OnPositionPage"));
            app.Tap("Pages.OnStock.OnPositionPage");

            // select position
            app.WaitForElement("Zadejte pozici"); // header
            var positionEl = app.Query(x => x.Marked("edPosition")).First();

            var SEARCH_BUTTON = app.Query(x => x.Marked("edPosition").Parent().Child().Property("class").Contains("Button")).First().Id;

            // set position
            app.ClearText(positionEl.Id);
            app.EnterText(positionEl.Id, "b1");
            app.Tap(SEARCH_BUTTON);

            app.WaitForElement("Obsah pozice b1b01"); // header

            Assert.AreEqual(1, app.Query(x => x.Class("ListView").Child("ConditionalFocusLayout")).Length);
        }
    }
}

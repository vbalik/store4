﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace UITests
{
    [TestFixture(Platform.Android)]
    //[TestFixture(Platform.iOS)]
    public class TestDispCheck : TestBase
    {
        private const string _PAGE = "Pages.Direction.DispatchCheck.DispatchCheckPage";
        private const string _SEARCHBUTTON = "SearchButton";
        private const string _CODEENTRY = "CodeEntry";
        private const string _HEADER = "HeaderLabel";
        private const string _SETSERIALSBUTTON = "SetSerialsButton";
        private const string _ADDBUTTON = "AddButton";

        public TestDispCheck(Platform platform) : base(platform)
        {
            
        }

        [Test]
        public void DispCheckTest()
        {
            // login
            LoginApp();

            app.ScrollDownTo(_PAGE);
            app.Tap(_PAGE);
            app.WaitForElement(_PAGE.Split('.').Last());

            //test rows count
            Assert.AreEqual(1, app.Query(x => x.Class("ListView").Descendant("ConditionalFocusLayout")).Length);
            //open first
            app.Tap(app.Query(x => x.Class("ListView").Descendant("ConditionalFocusLayout").Index(0).Descendant("LabelRenderer")).First().Id);

            //not finished
            Assert.IsFalse(TestIsFinished(_SEARCHBUTTON));
            Assert.AreEqual(GetHeaderText(0, 2), app.Query(_HEADER).First().Text);

            SetItem("CODE1", "Article 1", "Packing 12", 3);

            //not finished
            Assert.IsFalse(TestIsFinished(_SEARCHBUTTON));
            Assert.AreEqual(GetHeaderText(1, 2), app.Query(_HEADER).First().Text);

            SetItem("CODE2", "Article 2", "Packing 22", 4);

            //not finished
            Assert.IsFalse(TestIsFinished(_SEARCHBUTTON));
            Assert.AreEqual(GetHeaderText(2, 2), app.Query(_HEADER).First().Text);

            app.Tap(_SETSERIALSBUTTON);
            app.WaitForElement("SCAN");

            //not finished
            Assert.IsFalse(TestIsFinished(_ADDBUTTON));

            app.Tap("SCAN");

            //not finished
            Assert.IsFalse(TestIsFinished(_ADDBUTTON));
            Assert.AreEqual(GetSerialsHeaderText(1, 4), app.Query(_HEADER).Last().Text);

            SetSerial("123");

            //not finished
            Assert.IsFalse(TestIsFinished(_ADDBUTTON));
            Assert.AreEqual(GetSerialsHeaderText(2, 4), app.Query(_HEADER).Last().Text);

            //set the same serial
            SetSerial("123");

            app.WaitForElement("Sériové číslo 123 již bylo přidáno!");
            app.Back();

            SetSerial("456");

            //not finished
            Assert.IsFalse(TestIsFinished(_ADDBUTTON));
            Assert.AreEqual(GetSerialsHeaderText(3, 4), app.Query(_HEADER).Last().Text);

            SetSerial("789");

            //finished
            Assert.IsTrue(TestIsFinished(_ADDBUTTON));
            Assert.AreEqual(GetSerialsHeaderText(4, 4), app.Query(_HEADER).Last().Text);

            app.Tap(GetButton_Ok.Id);

            //finished
            Assert.IsTrue(TestIsFinished(_SEARCHBUTTON));

            TapButtonOkWithAlertTitle("Informace");

            app.Back();

            Assert.AreEqual(0, app.Query(x => x.Class("ListView").Descendant("ConditionalFocusLayout")).Length);
        }

        private bool TestIsFinished(string waitFor)
        {
            app.WaitForElement(waitFor);
            return GetButton_Ok.Enabled;
        }

        private string GetHeaderText(int finished, int all)
        {
            return $"Kontrola expedice skenováním {finished}/{all}";
        }

        private void SetItem(string code, string name, string packing, int count)
        {
            //wait for CheckDispatchControl and open GetArticleView
            app.WaitForElement(_SEARCHBUTTON);
            app.Tap(_SEARCHBUTTON);

            app.WaitForElement(_CODEENTRY);
            app.EnterText(_CODEENTRY, code);
            app.PressEnter();

            app.WaitForElement($"Zadejte množství ({packing}) položky \"{code} - {name}\"");
            app.EnterText(count.ToString());
            app.PressEnter();

            app.WaitForElement(_SEARCHBUTTON);
        }

        private void SetSerial(string code)
        {
            //wait for setserialsdialog
            app.WaitForElement(_ADDBUTTON);
            app.Tap(_ADDBUTTON);

            app.WaitForElement($"Zadejte sériové číslo");
            app.EnterText(code);
            app.PressEnter();

            app.WaitForElement(_ADDBUTTON);
        }

        private string GetSerialsHeaderText(int count, int all)
        {
            return $"Zbývá {all - count} z {all} sériových čísel ...";
        }
    }
}

﻿using NUnit.Framework;
using System.Linq;
using Xamarin.UITest;

namespace UITests
{
    [TestFixture(Platform.Android)]
    public class ExpeditionTests : TestBase
    {
        public ExpeditionTests(Platform platform) : base(platform)
        {

        }

        [Test]
        public void ExpeditionTest()
        {
            // login
            LoginApp();

            // tap expedice
            app.ScrollDownTo(_ => _.Marked("Pages.Direction.Exped.ExpeditionPage"));
            app.Tap("Pages.Direction.Exped.ExpeditionPage");

            // select second document
            app.WaitForElement("Seznam dokladů"); // header
            Assert.AreEqual(app.Query(x => x.Class("ListView").Descendant("ConditionalFocusLayout")).Length, 3);
            app.Tap(app.Query(x => x.Class("ListView").Descendant("ConditionalFocusLayout").Index(1).Descendant("LabelRenderer")).First().Id);
            
            // set spz
            app.WaitForElement("Zadejte SPZ"); // header
            var spzEl = app.Query(x => x.Marked("edSpz")).First();

            Assert.IsEmpty(spzEl.Text);
            Assert.AreEqual(GetButton_Ok.Enabled, false);

            app.EnterText(spzEl.Id, "abcd1234");
            
            Assert.IsNotEmpty(app.Query(x => x.Marked("edSpz")).First().Text);
            Assert.AreEqual(GetButton_Ok.Enabled, true);

            // tap ok
            TapButtonOkWithAlertTitle("Informace");
        }
    }
}

using System;
using Xunit;
using System.Collections.Generic;

using S4.Entities;
using S4.Entities.Models;
using static S4.DeliveryServices.DataParserHelper;
using System.IO;
using S4.DeliveryServices.Models.CPost;

namespace S4.DeliveryServices.Tests
{
    public class DataParserTest
    {
        public DataParserTest()
        {
            CreateData();
        }

        [Theory]
        [InlineData(null, null, null)]
        [InlineData("1", "1", null)]
        [InlineData("+45 4 5456 456545", "+4545456456545", null)]
        [InlineData("+420 123 456 7894", "+4201234567894", null)]
        [InlineData("+420 222 456 789", "+420222456789", null)]
        [InlineData("+420 666 456 789", null, "+420666456789")]
        [InlineData("777 456 7894", "7774567894", null)]
        [InlineData("777 456 789", null, "777456789")]
        [InlineData("+420 605", "+420605", null)]
        public void GetPhoneNumbersTest(string number, string phoneNumber, string cellPhoneNumber)
        {
            var phones = S4.DeliveryServices.DataParserHelper.GetPhoneNumbers(number);
            Assert.Equal(phoneNumber, phones.PhoneNumber);
            Assert.Equal(cellPhoneNumber, phones.CellPhoneNumber);
        }

        [Theory]
        [InlineData(0, null)]
        [InlineData(1, "email@email")]
        [InlineData(2, "email@email.com")]
        [InlineData(3, "email@email.cz")]
        public void GetEmailTest(int id, string expectedResult)
        {
            var result = S4.DeliveryServices.DataParserHelper.GetEmail(_data[id].Item1, _data[id].Item2);
            Assert.Equal(expectedResult, result);
        }

        [Theory]
        [InlineData(0, "")]
        [InlineData(1, "email@email\nremark1")]
        [InlineData(4, "remark4")]
        public void GetShipInfoTest(int id, string expectedResult)
        {
            var result = S4.DeliveryServices.DataParserHelper.GetShipInfo(_data[id].Item1, _data[id].Item2);
            Assert.Equal(expectedResult, result);
        }

        [Fact]
        public void SaveDeliveryXMLTest_NotSucces1()
        {
            var saveRequest = DataParserHelper.SaveDeliveryXML(DataParserHelper.SaveXMLEnum.REQUEST, DeliveryDirection.DeliveryProviderEnum.NONE, null, null, "c:\\temp", true);
            Assert.False(saveRequest.Item1);
            Assert.Equal("Unexpected provider NONE", saveRequest.Item2);
        }

        [Fact]
        public void SaveDeliveryXMLTest_NotSucces2()
        {
            var saveRequest = DataParserHelper.SaveDeliveryXML(DataParserHelper.SaveXMLEnum.REQUEST, DeliveryDirection.DeliveryProviderEnum.NONE, null, null, "", true);
            Assert.False(saveRequest.Item1);
            Assert.Equal("Parameter 'path' must not be empty!", saveRequest.Item2);
        }

        [Fact]
        public void SaveDeliveryXMLTest_DPD_Request_Succes()
        {
            var xml = File.ReadAllText(@"..\..\..\XmlTestData\DPD_Request.xml");
            var saveRequest = SaveDeliveryXML(SaveXMLEnum.REQUEST, DeliveryDirection.DeliveryProviderEnum.DPD, new DPDShipmentService.createShipment(), "Test", "c:\\temp", true);
            Assert.True(saveRequest.Item1);
            Assert.Equal(xml, saveRequest.Item3);
        }

        [Fact]
        public void SaveDeliveryXMLTest_DPD_Response_Succes()
        {
            var xml = File.ReadAllText(@"..\..\..\XmlTestData\DPD_Response.xml");
            var saveRequest = SaveDeliveryXML(SaveXMLEnum.RESPONSE, DeliveryDirection.DeliveryProviderEnum.DPD, new DPDShipmentService.createShipmentResponse1(), "Test", "c:\\temp", true);
            Assert.True(saveRequest.Item1);
            Assert.Equal(xml, saveRequest.Item3);
        }

        [Fact]
        public void SaveDeliveryXMLTest_PPL_Request_Succes()
        {
            var xml = File.ReadAllText(@"..\..\..\XmlTestData\PPL_Request.xml");
            var saveRequest = SaveDeliveryXML(SaveXMLEnum.REQUEST, DeliveryDirection.DeliveryProviderEnum.PPL, new List<PPLWebService.MyApiPackageIn>(), "Test", "c:\\temp", true);
            Assert.True(saveRequest.Item1);
            Assert.Equal(xml, saveRequest.Item3);
        }

        [Fact]
        public void SaveDeliveryXMLTest_PPL_Response_Succes()
        {
            var xml = File.ReadAllText(@"..\..\..\XmlTestData\PPL_Response.xml");
            var saveRequest = SaveDeliveryXML(SaveXMLEnum.RESPONSE, DeliveryDirection.DeliveryProviderEnum.PPL, new PPLWebService.CreatePackagesResult(), "Test", "c:\\temp", true);
            Assert.True(saveRequest.Item1);
            Assert.Equal(xml, saveRequest.Item3);
        }

        [Fact]
        public void SaveDeliveryXMLTest_CPOST_Request_Succes()
        {
            var xml = File.ReadAllText(@"..\..\..\XmlTestData\CPOST_Request.xml");
            var saveRequest = SaveDeliveryXML(SaveXMLEnum.REQUEST, DeliveryDirection.DeliveryProviderEnum.CPOST, new parcelServiceSyncRequest(), "Test", "c:\\temp", true);
            Assert.True(saveRequest.Item1);
            Assert.Equal(xml, saveRequest.Item3);
        }

        [Fact]
        public void SaveDeliveryXMLTest_CPOST_Response_Succes()
        {
            var xml = File.ReadAllText(@"..\..\..\XmlTestData\CPOST_Response.xml");
            var saveRequest = SaveDeliveryXML(SaveXMLEnum.RESPONSE, DeliveryDirection.DeliveryProviderEnum.CPOST, new b2bSyncResponse(), "Test", "c:\\temp", true);
            Assert.True(saveRequest.Item1);
            Assert.Equal(xml, saveRequest.Item3);
        }

        private static List<Tuple<DirectionModel, PartnerAddress>> _data;
        private void CreateData()
        {
            _data = new List<Tuple<DirectionModel, PartnerAddress>>()
            {
                //0
                new Tuple<DirectionModel, PartnerAddress>(
                    new DirectionModel()
                    {

                    },
                    new PartnerAddress()
                    {

                    }
                ),
                //1
                new Tuple<DirectionModel, PartnerAddress>(
                    new DirectionModel()
                    {
                        DocRemark = "emailemail.cz"
                    },
                    new PartnerAddress()
                    {
                        AddrRemark = "email@email"
                    }
                ),
                //2
                new Tuple<DirectionModel, PartnerAddress>(
                    new DirectionModel()
                    {
                        DocRemark = "email@email.com"
                    },
                    new PartnerAddress()
                    {
                        AddrRemark = "email@email"
                    }
                ),
                //3
                new Tuple<DirectionModel, PartnerAddress>(
                    new DirectionModel()
                    {
                        DocRemark = "emailemail.com"
                    },
                    new PartnerAddress()
                    {
                        AddrRemark = "email@email.cz"
                    }
                ),
                //4
                new Tuple<DirectionModel, PartnerAddress>(
                    new DirectionModel()
                    {

                    },
                    new PartnerAddress()
                    {

                    }
                )
            };
            _data[1].Item1.XtraData.Data.Add(new DirectionXtra() { XtraCode = Direction.XTRA_DELIVERY_SHIPMENTINFO, XtraValue = "remark1" });
            _data[4].Item1.XtraData.Data.Add(new DirectionXtra() { XtraCode = Direction.XTRA_DELIVERY_SHIPMENTINFO, XtraValue = "remark4" });
        }
    }
}

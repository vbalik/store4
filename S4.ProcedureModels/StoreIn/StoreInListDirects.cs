﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.StoreIn
{
    public class StoreInListDirects : ProceduresModel
    {
              
        // in
        [Required]
        [ParameterIn]
        public string WorkerID { get; set; }

        // out
        [ParameterOut]
        public List<Entities.Helpers.DocumentInfo> Directions { get; set; }

    }
}

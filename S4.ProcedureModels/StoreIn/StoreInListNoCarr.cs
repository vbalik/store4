﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.StoreIn
{
    public class StoreInListNoCarr : ProceduresModel
    {
        // in
        [ParameterIn]
        [Required]
        public int SrcPositionID { get; set; }
        [ParameterIn]
        [Required]
        public int DirectionID { get; set; }

        // out
        [ParameterOut]
        public List<Entities.Helpers.ItemInfo> ArticleList { get; set; }
    }
}

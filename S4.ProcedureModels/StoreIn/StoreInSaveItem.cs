﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.StoreIn
{
    public class StoreInSaveItem : ProceduresModel
    {
              
        // in
        [Required]
        [ParameterIn]
        public int StoMoveID { get; set; }

        [Required]
        [ParameterIn]
        public int FinalPositionID;

        [Required]
        [ParameterIn]
        public string FinalCarrierNum;

    }
}

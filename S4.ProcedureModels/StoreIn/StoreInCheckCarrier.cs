﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.StoreIn
{
    public class StoreInCheckCarrier : ProceduresModel
    {
        // in
        [ParameterIn]
        [Required]
        public int DirectionID { get; set; }
        [ParameterIn]
        [Required]
        public string CarrierNum { get; set; }
        [ParameterIn]
        [Required]
        public int SrcPositionID { get; set; }

        // out
        [ParameterOut]
        public bool IsCarrierFound { get; set; }
        [ParameterOut]
        public List<Entities.Helpers.ItemInfo> ArticleList { get; set; }
    }
}

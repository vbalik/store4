﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.StoreIn
{
    public class StoreInCheckPos : ProceduresModel
    {
        public enum PosEnabled
        {
            Yes,
            Warn,
            No
        }

        // in
        [ParameterIn]
        [Required]
        public int PositionID { get; set; }
        [ParameterIn]
        [Required]
        public int ArtPackID { get; set; }

        // out
        [ParameterOut]
        public PosEnabled Enabled { get; set; }
        [ParameterOut]
        public string ResReason { get; set; }
    }
}

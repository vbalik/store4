﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;


namespace S4.ProcedureModels.StoreIn
{
    public class StoreInAcceptPos : ProceduresModel
    {
        // in
        [ParameterIn]
        [Required]
        public string CarrierNum { get; set; }

        [ParameterIn]
        [Required]
        public int DirectionID { get; set; }
        [ParameterIn]
        public int? SrcPositionID { get; set; }
        [ParameterIn]
        [Required]
        public int DestPositionID { get; set; }

        // out
        [ParameterOut]
        public int StoMoveID { get; set; }


        public override int GetHashCode()
        {
            return Core.Utils.HashHelper.GetHashCode(CarrierNum, DirectionID, SrcPositionID, DestPositionID);
        }
    }
}

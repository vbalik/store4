﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.StoreIn
{
    public class StoreInProc : ProceduresModel
    {
              
        // in
        [Required]
        [ParameterIn]
        public int DirectionID { get; set; }

    }
}

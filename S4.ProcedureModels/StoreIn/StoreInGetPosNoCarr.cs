﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.StoreIn
{
    public class StoreInGetPosNoCarr : ProceduresModel
    {
        // in
        [ParameterIn]
        [Required]
        public int ArtPackID { get; set; }

        // out
        [ParameterOut]
        public bool IsPositionFound { get; set; }
        [ParameterOut]
        public Entities.Position DestPosition { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.StoreIn
{
    public class StoreInAcceptPosNoCarr : ProceduresModel
    {
              
        // in
        [Required]
        [ParameterIn]
        public S4.Entities.Helpers.ArticleInfo Article { get; set; }

        [Required]
        [ParameterIn]
        public int DirectionID { get; set; }

        [ParameterIn]
        public int? SrcPositionID { get; set; }

        [Required]
        [ParameterIn]
        public int DestPositionID { get; set; }

        // out
        [ParameterOut]
        public int StoMoveID { get; set; }

    }
}

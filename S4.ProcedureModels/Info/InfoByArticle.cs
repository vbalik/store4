﻿using S4.StoreCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Info
{
    public class InfoByArticle : ProceduresModel
    {
        // in
        [ParameterIn]
        [Required]
        public int ArticleID { get; set; }

        //out
        [ParameterOut]
        public List<OnStoreItemFull> OnStoreItemFull { get; set; }
    }
}

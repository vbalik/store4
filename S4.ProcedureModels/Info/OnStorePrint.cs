﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Info
{
    public class OnStorePrint : PrintProceduresModel
    {
        public enum OnStorePrintSourceEnum
        {
            OnStorePrintSourcePosition,
            OnStorePrintSourceArticle
        }


        // in
        [Required]
        [ParameterIn]
        public OnStorePrintSourceEnum OnStorePrintSource { get; set; }

        [Required]
        [ParameterIn]
        public int SourceID { get; set; }

        [Required]
        [ParameterIn]
        public int PrinterLocationID { get; set; }

        [ParameterIn]
        public int ReportID { get; set; }

    }
}

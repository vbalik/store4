﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Info
{
    public class PrintShippingDocumentADR : PrintProceduresModel
    {
        // in
        [Required]
        [ParameterIn]
        public int DirectionID { get; set; }

        [ParameterIn]
        public int PrinterLocationID { get; set; }

        [ParameterIn]
        public int ReportID { get; set; }

    }
}

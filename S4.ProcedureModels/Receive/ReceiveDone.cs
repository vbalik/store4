﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Receive
{
    public class ReceiveDone : ProceduresModel
    {
        // in
        [ParameterIn]
        [Required]
        public int StoMoveID { get; set; }

        // out
        // none
    }
}

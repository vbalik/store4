﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Receive
{
    public class ReceiveSaveItem : ProceduresModel
    {
        // in
        [ParameterIn]
        [Required]
        public int StoMoveID { get; set; }
        [ParameterIn]
        public int? PositionID { get; set; }
        [ParameterIn]
        [Required]
        public int ArtPackID { get; set; }
        [ParameterIn]
        public string CarrierNum { get; set; }
        [ParameterIn]
        [Required]
        public decimal Quantity { get; set; }
        [ParameterIn]
        public string BatchNum { get; set; }
        [ParameterIn]
        public DateTime? ExpirationDate { get; set; }
        [ParameterIn]
        public string UDICode { get; set; }

        // out
        [ParameterOut]
        public bool RepeatedItem { get; set; } = false;
        [ParameterOut]
        public int StoMoveItemID { get; set; }
    }
}

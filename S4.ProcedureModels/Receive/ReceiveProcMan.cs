﻿using S4.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Receive
{
    public class ReceiveProcMan : ProceduresModel
    {
        // in
        [ParameterIn]
        [Required]
        public string WorkerID { get; set; }
        [ParameterIn]
        public string DocNumPrefix { get; set; }

        // out
        [ParameterOut]
        public int DirectionID { get; set; }
        [ParameterOut]
        public string DocInfo { get; set; }
        [ParameterOut]
        public int StoMoveID { get; set; }
        [ParameterOut]
        public Partner Partner { get; set; }
        [ParameterOut]
        public string PartnerRef { get; set; }
        [ParameterOut]
        public string DocRemark { get; set; }
        [ParameterOut]
        public List<Entities.Helpers.ItemInfo> Items { get; set; }
    }
}

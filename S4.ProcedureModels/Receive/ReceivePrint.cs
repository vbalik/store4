﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Receive
{
    public class ReceivePrint : PrintProceduresModel
    {
        // in
        [ParameterIn]
        [Required]
        public int StoMoveID { get; set; }

        [ParameterIn]
        public int PrinterLocationID { get; set; }

        [ParameterIn]
        public int ReportID { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace S4.ProcedureModels.Receive
{
    public class ReceiveSimple : ProceduresModel
    {
        [ParameterIn]
        [Required]
        public int ArticleID { get; set; }
        [ParameterIn]
        [Required]
        public int PositionID { get; set; }
        [ParameterIn]
        [Required]
        public int Quantity { get; set; }
        [ParameterIn]
        [Required]
        public string ExpirationDate { get; set; }
        [ParameterIn]
        public string DocumentRemark { get; set; }
        [ParameterOut]
        public int StoreMoveID { get; set; }
    }
}

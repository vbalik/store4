﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Receive
{
    public class ReceiveChangePartner : ProceduresModel
    {
        // in
        [ParameterIn]
        [Required]
        public int DirectionID { get; set; }
        [ParameterIn]
        public int? PartnerID { get; set; }
        [ParameterIn]
        public string PartnerRef { get; set; }
        [ParameterIn]
        public string DocRemark { get; set; }

        // out
        // none
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Receive
{
    public class ReceivePrintByDirection : PrintProceduresModel
    {
        // in
        [ParameterIn]
        [Required]
        public int DirectionID { get; set; }

        [Required]
        [ParameterIn]
        public int PrinterLocationID { get; set; }

        [ParameterIn]
        public int ReportID { get; set; }

        [ParameterIn]
        public int Count { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Receive
{
    public class ReceiveCancel : ProceduresModel
    {
        // in
        [ParameterIn]
        [Required]
        public int StoMoveID { get; set; }
        [ParameterIn]
        public int DirectionID { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Dispatch
{
    public class DispatchTestDirect : ProceduresModel
    {
        // in
        [Required]
        [ParameterIn]
        public int DirectionID { get; set; }

        // out
        [ParameterOut]
        public bool IsCompleted { get; set; } = false;

        [ParameterOut]
        public bool IsMandatoryDirectionPrint { get; set; } = false;
    }
}

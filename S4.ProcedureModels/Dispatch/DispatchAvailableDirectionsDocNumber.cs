﻿using S4.Entities.Helpers;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace S4.ProcedureModels.Dispatch
{
    public class DispatchAvailableDirectionsDocNumber : ProceduresModel
    {
        // in
        [Required]
        [ParameterIn]
        public string DocumentID { get; set; }


        // out
        [ParameterOut]
        public List<DocumentInfo> Directions { get; set; } = new List<DocumentInfo>();
    }
}

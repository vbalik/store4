﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Dispatch
{
    public class DispatchItemInstruct : ProceduresModel
    {   
        // in
        [Required]
        [ParameterIn]
        public int StoMoveID { get; set; }
        [Required]
        [ParameterIn]
        public int StoMoveItemID { get; set; }

        // out
        [ParameterOut]
        public Entities.Position ScrPosition { get; set; }
        [ParameterOut]
        public Entities.Position DestPosition { get; set; }
        [ParameterOut]
        public Entities.Helpers.ItemInfo Item { get; set; }
    }
}

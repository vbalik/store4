﻿using System.ComponentModel.DataAnnotations;

namespace S4.ProcedureModels.Dispatch
{
    public class DispatchManualBegin : ProceduresModel
    {      
        // in
        [Required]
        [ParameterIn]
        public int DirectionID { get; set; }
        [Required]
        [ParameterIn]
        public int PositionID { get; set; }
    }
}

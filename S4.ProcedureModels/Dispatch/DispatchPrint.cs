﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Dispatch
{
    public class DispatchPrint : PrintProceduresModel
    {
        // in
        [Required]
        [ParameterIn]
        public int DirectionID { get; set; }

        [Required]
        [ParameterIn]
        public bool PrinterByPosition { get; set; }

        [ParameterIn]
        public int PrinterLocationID { get; set; }

        [ParameterIn]
        public int ReportID { get; set; }

        [ParameterIn]
        public TypeDispatchPrint TypeDispatchPrint { get; set; }

    }

    public enum TypeDispatchPrint : byte
    {
        BOTH = 0,
        BATCH = 1,
        LABEL = 2
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Dispatch
{
    public class DispatchConfirmItem : ProceduresModel
    {
        //in
        [ParameterIn]
        [Required]
        public int StoMoveID { get; set; }

        [ParameterIn]
        [Required]
        public int StoMoveItemID { get; set; }
    }
}

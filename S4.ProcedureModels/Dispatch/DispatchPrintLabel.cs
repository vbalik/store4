﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Dispatch
{
    public class DispatchPrintLabel : ProceduresModel
    {
        // in
        [Required]
        [ParameterIn]
        public int DirectionID { get; set; }

        [Required]
        [ParameterIn]
        public int Count { get; set; }

        [Required]
        [ParameterIn]
        public int PrinterLocationID { get; set; }

        [ParameterIn]
        public int ReportID { get; set; }

        [ParameterIn]
        [Required]
        public TypePrint TypePrint { get; set; }

    }

    public enum TypePrint
    {
        MALE_STITKY,
        VELKE_STITKY
        
    }
}

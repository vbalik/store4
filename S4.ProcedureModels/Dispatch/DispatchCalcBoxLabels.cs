﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Dispatch
{
    public class DispatchCalcBoxLabels : ProceduresModel
    {
        public class BoxLabel
        {
            public string LabelText { get; set; }
            public int LabelQuantity { get; set; }
            public int DocPosition { get; set; }
            public int DirectionID { get; set; }
        }

        // in
        [ParameterIn]
        [Required]
        public int DirectionID { get; set; }

        // out
        [ParameterOut]
        public List<BoxLabel> BoxLabels { get; set; }

        [ParameterOut]
        public List<Entities.Reports> Reports { get; set; }

        [ParameterOut]
        public List<Entities.PrinterLocation> Printers { get; set; }
    }
}

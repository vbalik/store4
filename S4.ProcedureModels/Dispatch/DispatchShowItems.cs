﻿using S4.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Dispatch
{
    public class DispatchShowItems : ProceduresModel
    {
        // in
        [Required]
        [ParameterIn]
        public int StoMoveID { get; set; }

        // out
        [ParameterOut]
        public List<Entities.Helpers.ItemInfo> Items { get; set; }
        [ParameterOut]
        public List<Entities.Helpers.ItemInfo> DoneItems { get; set; }
        [ParameterOut]
        public bool UseRemarks { get; set; }
        [ParameterOut]
        public Position DestPosition { get; set; }
    }
}

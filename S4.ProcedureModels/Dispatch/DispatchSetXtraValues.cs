﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace S4.ProcedureModels.Dispatch
{
    public class DispatchSetXtraValues : PrintProceduresModel
    {
        // in
        [Required]
        [ParameterIn]
        public int DirectionID { get; set; }

        [Required]
        [ParameterIn]
        public List<DispatchSetXtraValue> Values { get; set; }
    }

    public class DispatchSetXtraValue
    {
        public string Key { get; set; }
        public dynamic Value { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Dispatch
{
    public class DispatchReassign : ProceduresModel
    {
        // in
        [Required]
        [ParameterIn]
        public int DirectionID { get; set; }

        [Required]
        [ParameterIn]
        public string WorkerID { get; set; }

        [Required]
        [ParameterIn]
        public int DestPositionID { get; set; }
    }
}

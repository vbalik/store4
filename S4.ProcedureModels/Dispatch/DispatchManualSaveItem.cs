﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Dispatch
{
    public class DispatchManualSaveItem : ProceduresModel
    {      
        // in
        [Required]
        [ParameterIn]
        public int DirectionID { get; set; }
        [Required]
        [ParameterIn]
        public int PositionID { get; set; }
        [Required]
        [ParameterIn]
        public int ArtPackID { get; set; }
        [Required]
        [ParameterIn]
        public decimal Quantity { get; set; }
        [Required]
        [ParameterIn]
        public int ParentDocPosition { get; set; }
        [ParameterIn]
        public string CarrierNum { get; set; }

        [ParameterIn]
        public string BatchNum { get; set; }
        [ParameterIn]
        public DateTime? ExpirationDate { get; set; }

        // out
        [ParameterOut]
        public int StoMoveID { get; set; }
    }
}

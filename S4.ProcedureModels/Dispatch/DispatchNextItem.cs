﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Dispatch
{
    public class DispatchNextItem : ProceduresModel
    {
        // in
        [ParameterIn]
        [Required]
        public int StoMoveID { get; set; }

        // out
        [ParameterOut]
        public bool WasFound;

        [ParameterOut]
        public Entities.Position ScrPosition;
        [ParameterOut]
        public Entities.Position DestPosition;
        [ParameterOut]
        public Entities.Helpers.ItemInfo Article;

        [ParameterOut]
        public int StoMoveItemID;
    }
}

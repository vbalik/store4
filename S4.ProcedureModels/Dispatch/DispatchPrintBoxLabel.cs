﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Dispatch
{
    public class DispatchPrintBoxLabel : ProceduresModel
    {
        // in
        [ParameterIn]
        public string LabelText { get; set; }
        [Required]
        [ParameterIn]
        public int LabelsQuant { get; set; }
        [Required]
        [ParameterIn]
        public int DirectionID { get; set; }
        [Required]
        [ParameterIn]
        public int DocPosition { get; set; }
        [Required]
        [ParameterIn]
        public int PrinterLocationID { get; set; }
        [ParameterIn]
        public int ReportID { get; set; }

        // out
        // none
    }
}

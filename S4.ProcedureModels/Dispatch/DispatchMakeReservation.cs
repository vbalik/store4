﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Dispatch
{
    public class DispatchMakeReservation : ProceduresModel
    {      
        // in
        [Required]
        [ParameterIn]
        public int DirectionID { get; set; }


        // out
        [ParameterOut]
        public bool IsPrepared { get; set; }
        [ParameterOut]
        public bool IsFromPrepPos { get; set; }
        [ParameterOut]
        public string NotFoundInfo { get; set; }
        [ParameterOut]
        public int StoMoveID { get; set; }
        [ParameterOut]
        public bool UseRemarks { get; set; }
        [ParameterOut]
        public string[] MoveRemarks { get; set; }
        [ParameterOut]
        public string DispatchLogText { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Dispatch
{
    public class DispatchUserAssignment : ProceduresModel
    {
        // in
        [Required]
        [ParameterIn]
        public string DocInfo { get; set; }
        
        [Required]
        [ParameterIn]
        public string WorkerID { get; set; }

    }
       
}

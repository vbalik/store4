﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace S4.ProcedureModels
{
    public class ParameterOutAttribute : Attribute
    {
        public bool IsResult { get; set; }
        public bool IsErrorText { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Admin
{
    public class RemoveExpBatch : ProceduresModel
    {
        // in
        [Required]
        [ParameterIn]
        public int ArticleID { get; set; }
    }
}

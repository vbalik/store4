﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Admin
{
    public class WorkerSetStatus : ProceduresModel
    {
        // in
        [Required]
        [ParameterIn]
        public string WorkerID { get; set; }
        
        [Required]
        [ParameterIn]
        public string NewStatus { get; set; }

    }
}

﻿using System;
using System.ComponentModel.DataAnnotations;

namespace S4.ProcedureModels.Admin
{
    public class DirectionStoMoveItem : ProceduresModel
    {
        // in
        [ParameterIn]
        [Required]
        public int DocPosition { get; set; }
        
        [ParameterIn]
        [Required]
        public decimal Quantity { get; set; }

        [ParameterIn]
        public string BatchNum { get; set; } = string.Empty;

        [ParameterIn]
        public DateTime? ExpirationDate { get; set; }

        [ParameterIn]
        public string CarrierNum { get; set; } = string.Empty;

        [ParameterIn]
        [Required]
        public int StoMoveID { get; set; }

        [ParameterIn]
        [Required]
        public int StoMoveItemID { get; set; }

        [ParameterIn]
        [Required]
        public int StoMoveLotID { get; set; }
    }
}

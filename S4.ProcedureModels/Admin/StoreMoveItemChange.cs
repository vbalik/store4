﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Admin
{
    public class StoreMoveItemChange : ProceduresModel
    {
        // in
        [Required]
        [ParameterIn]
        public int StoMoveItemID { get; set; }

        [Required]
        [ParameterIn]
        public int ArtPackID { get; set; }

        [Required]
        [ParameterIn]
        public decimal Quantity { get; set; }

        [ParameterIn]
        public string CarrierNum { get; set; }

        [ParameterIn]
        public string BatchNum { get; set; }

        [ParameterIn]
        public DateTime? ExpirationDate { get; set; }
    }
}

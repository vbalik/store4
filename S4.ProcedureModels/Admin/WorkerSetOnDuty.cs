﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Admin
{
    public class WorkerSetOnDuty : WorkerSetStatus
    {
        public WorkerSetOnDuty()
        {
            NewStatus = Entities.Worker.WK_STATUS_ONDUTY;
        }
    }
}

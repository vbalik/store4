﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Admin
{
    public class StoreMoveCancel : ProceduresModel
    {
        // in
        [Required]
        [ParameterIn]
        public int StoMoveID { get; set; }

        [Required]
        [ParameterIn]
        public string CancelReason { get; set; }

    }
}

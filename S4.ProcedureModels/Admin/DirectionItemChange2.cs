﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Admin
{
    public class DirectionItemChange2 : ProceduresModel
    {
        // in
        [Required]
        [ParameterIn]
        public int DirectionID { get; set; }

        [Required]
        [ParameterIn]
        public int DirectionItemID { get; set; }

        [Required]
        [ParameterIn]
        public int ArtPackID { get; set; }

        [Required]
        [ParameterIn]
        public string ArticleCode { get; set; }

        [ParameterIn]
        public List<DirectionStoMoveItem> DirectionStoMoveItems { get; set; } = new List<DirectionStoMoveItem>();
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Admin
{
    public class DirectionCancel : ProceduresModel
    {
        // in
        [Required]
        [ParameterIn]
        public int DirectionID { get; set; }

        [Required]
        [ParameterIn]
        public string CancelReason { get; set; }

    }
}

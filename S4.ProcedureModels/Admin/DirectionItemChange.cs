﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Admin
{
    public class DirectionItemChange : ProceduresModel
    {

        // in
        [Required]
        [ParameterIn]
        public int DirectionItemID { get; set; }

        [Required]
        [ParameterIn]
        public int ArtPackID { get; set; }

        [Required]
        [ParameterIn]
        public string ArticleCode { get; set; }

        [Required]
        [ParameterIn]
        public decimal Quantity { get; set; }

        [ParameterIn]
        public string UnitDesc { get; set; }

        [ParameterIn]
        public string PartnerDepart { get; set; }
    }
}

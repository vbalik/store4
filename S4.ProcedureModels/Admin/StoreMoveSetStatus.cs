﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Admin
{
    public class StoreMoveSetStatus : ProceduresModel
    {
        // in
        [Required]
        [ParameterIn]
        public int StoMoveID { get; set; }

        [Required]
        [ParameterIn]
        public string Status { get; set; }

        [Required]
        [ParameterIn]
        public string ChangeReason { get; set; }
    }
}

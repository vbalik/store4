﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Admin
{
    public class DirectionSetDispSection : ProceduresModel
    {

        // in
        [Required]
        [ParameterIn]
        public int DirectionID { get; set; }

        [Required]
        [ParameterIn]
        public string ChangeReason { get; set; }

        [Required]
        [ParameterIn]
        public string SectID { get; set; }

    }
}

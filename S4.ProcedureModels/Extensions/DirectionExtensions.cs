﻿using System;
using System.Collections.Generic;
using System.Text;

using S4.Entities;


namespace S4.ProcedureModels.Extensions
{
    public static class DirectionExtensions
    {
        public static string DocInfo(this Direction dir)
        {
            return $"{dir.DocNumPrefix} / {dir.DocYear} / {dir.DocNumber}";
        }
    }
}

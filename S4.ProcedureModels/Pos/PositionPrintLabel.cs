﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Pos
{
    public class PositionPrintLabel : ProceduresModel
    {
        // in
        [Required]
        [ParameterIn]
        public int PositionID { get; set; }
        [Required]
        [ParameterIn]
        public int PrinterLocationID { get; set; }
        [ParameterIn]
        public int ReportID { get; set; }

        // out
        // none
    }
}

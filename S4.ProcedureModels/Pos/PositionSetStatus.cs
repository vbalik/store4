﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Pos
{
    public class PositionSetStatus : ProceduresModel
    {
        // in
        [Required]
        [ParameterIn]
        public int PositionID { get; set; }

        [Required]
        [ParameterIn]
        public string Status { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.StockTaking
{
    public class StockTakingLoadABRAStatus : ProceduresModel
    {
        [Required]
        [ParameterIn]
        public int StotakID { get; set; }

        [Required]
        [ParameterIn]
        public string AbraStoreID { get; set; }

        [ParameterIn]
        public string ManufID { get; set; }

        [ParameterOut]
        public string LoadErrors { get; set; }
    }
}

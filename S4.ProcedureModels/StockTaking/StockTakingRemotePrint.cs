﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.StockTaking
{
    public class StockTakingRemotePrint : ProceduresModel
    {
              
        // in
        [Required]
        [ParameterIn]
        public int StotakID { get; set; }

        [Required]
        [ParameterIn]
        public string Email { get; set; }

        [Required]
        [ParameterIn]
        public string PosCode { get; set; }

        [ParameterIn]
        public string PosDesc { get; set; }
    }
}

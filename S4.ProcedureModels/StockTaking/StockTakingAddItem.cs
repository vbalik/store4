﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.StockTaking
{
    public class StockTakingAddItem : ProceduresModel
    {
              
        // in
        [Required]
        [ParameterIn]
        public int StotakID { get; set; }

        [Required]
        [ParameterIn]
        public int PositionID { get; set; }

        [Required]
        [ParameterIn]
        public int ArtPackID { get; set; }

        [ParameterIn]
        public string CarrierNum { get; set; }

        [Required]
        [ParameterIn]
        public decimal Quantity { get; set; }

        [ParameterIn]
        public string BatchNum { get; set; }

        [ParameterIn]
        public DateTime? ExpirationDate { get; set; }
    }
}

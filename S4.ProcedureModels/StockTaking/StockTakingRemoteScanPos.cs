﻿using S4.Entities.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.StockTaking
{
    public class StockTakingRemoteScanPos : ProceduresModel
    {
        // in
        [Required]
        [ParameterIn]
        public int StotakID { get; set; }

        [Required]
        [ParameterIn]
        public int PositionID { get; set; }


        // out
        [ParameterOut]
        public bool BadPosition { get; set; }

        [ParameterOut]
        public List<StockTakingRemoteItem> StockTakingItems { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.StockTaking
{
    public class StockTakingRemoteListOpened : ProceduresModel
    {
      
        // out
        [ParameterOut]
        public List<StockTakingRemotePos> StockTakingList { get; set; }
    }
}

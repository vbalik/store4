﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.StockTaking
{
    public class StockTakingBegin : ProceduresModel
    {
        // in
        private string _stotakDesc;
        [Required]
        [ParameterIn]
        public string StotakDesc
        {
            get
            {
                //from stotak list
                if (!string.IsNullOrWhiteSpace(_stotakDesc))
                    return _stotakDesc;
                //from position list
                if (PositionID.HasValue)
                    return $"Inventura pozice {PosCode}";
                return null;
            }
            set { _stotakDesc = value; }
        }


        private int[] _positionIDs;
        [Required]
        [ParameterIn]
        public int[] PositionIDs
        {
            get
            {
                //from stotak list
                if (_positionIDs != null)
                    return _positionIDs;
                //from position list
                if (PositionID.HasValue)
                    return new int[] { PositionID.Value };
                return null;
            }
            set { _positionIDs = value; }
        }


        /// <summary>
        /// in case of beginning from pos - automatic binding
        /// </summary>
        [ParameterIn]
        public int? PositionID { get; set; }

        /// <summary>
        /// in case of beginning from pos - automatic binding
        /// </summary>
        [ParameterIn]
        public string PosCode { get; set; }
        
        // out
        [ParameterOut]
        public int StotakID;
    }
}

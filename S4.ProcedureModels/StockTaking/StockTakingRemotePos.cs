﻿using S4.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.StockTaking
{
    public class StockTakingRemotePos : ProceduresModel
    {
        public int StotakID { get; set; }
        public string StotakStatus { get; set; }
        public string StotakDesc { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime EntryDateTime { get; set; }
        public string EntryUserID { get; set; }

        public List<Position> Positions { get; set; }
    }
}

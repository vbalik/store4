﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.StockTaking
{
    public class StockTakingDeletePosItem : ProceduresModel
    {
      
        // in
        [Required]
        [ParameterIn]
        public int StotakID { get; set; }

        [Required]
        [ParameterIn]
        public int PositionID { get; set; }

        [Required]
        [ParameterIn]
        public int StotakItemID { get; set; }
              
                          
        // out
        [ParameterOut]
        public bool BadPosition;
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.StockTaking
{
    public class StockTakingListOpened : ProceduresModel
    {
      
        // out
        [ParameterOut]
        public List<Entities.StockTaking> StockTakingList { get; set; }
      
    }
}

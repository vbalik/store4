﻿using S4.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.StockTaking
{
    public class StockTakingRemoteItemsFromSnapshot : ProceduresModel
    {
        // in
        [Required]
        [ParameterIn]
        public int StotakID { get; set; }

        [Required]
        [ParameterIn]
        public int ArtPackID { get; set; }

        // out
        [ParameterOut]
        public List<StockTakingSnapshot> Items { get; set; } = new List<StockTakingSnapshot>();
    }
}

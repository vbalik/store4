﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.DispCheck
{
    public class DispCheckDone2 : ProceduresModel
    {
    
        public class SerialNumberItem
        {
            public int ArtPackID { get; set; }
            public string BatchNum { get; set; }
            public DateTime? ExpirationDate { get; set; }
            public string SerialNumber { get; set; }
        }



        // in
        [Required]
        [ParameterIn]
        public int DirectionID { get; set; }

        [ParameterIn]
        public List<SerialNumberItem> SerialNumbers { get; set; }
    }
}

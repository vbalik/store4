﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.DispCheck
{
    public class DispCheckSkip : ProceduresModel
    {

        // in
        [Required]
        [ParameterIn]
        public int DirectionID { get; set; }

        [ParameterIn]
        public string Reason { get; set; }
    }
}

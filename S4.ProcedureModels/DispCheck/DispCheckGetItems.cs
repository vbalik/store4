﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.DispCheck
{
    public class DispCheckGetItems : ProceduresModel
    {
        public class CheckDetail
        {
            public int ArtPackID { get; set; }
            public string BatchNum { get; set; }
            public DateTime? ExpirationDate { get; set; }
            public decimal Quantity { get; set; }
        }


        // in
        [Required]
        [ParameterIn]
        public int DirectionID { get; set; }

        // out
        [ParameterOut]
        public bool UseDepartments { get; set; }

        [ParameterOut]
        public List<Entities.Helpers.ItemInfo> ItemInfoList { get; set; }

        [ParameterOut]
        public List<CheckDetail> CheckDetails { get; set; }
    }
}

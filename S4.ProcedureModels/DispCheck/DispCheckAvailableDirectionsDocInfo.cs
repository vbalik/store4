﻿using S4.Entities.Helpers;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace S4.ProcedureModels.DispCheck
{
    public class DispCheckAvailableDirectionsDocInfo : ProceduresModel
    {
        // in
        [Required]
        [ParameterIn]
        public string DocInfo { get; set; }

        // for testing
        [ParameterIn]
        public int DocYear { get; set; }


        // out
        [ParameterOut]
        public List<DocumentInfo> Directions { get; set; } = new List<DocumentInfo>();
    }
}

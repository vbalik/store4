﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.DispCheck
{
    public class DispCheckForceDone : ProceduresModel
    {
        public const byte CHECK_METHOD_NOT_SET = 255;

    
        // in
        [Required]
        [ParameterIn]
        public int DirectionID { get; set; }
            
    }
}

﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using static S4.Entities.Direction;


namespace S4.ProcedureModels.DispCheck 
{
    public class DispCheckWait : ProceduresModel
    {
        // in
        [Required]
        [ParameterIn]
        public int DirectionID { get; set; }

        [Required]
        [ParameterIn]
        public string WorkerID { get; set; }

        [ParameterIn]
        [JsonConverter(typeof(StringEnumConverter))]
        public DispCheckMethodsEnum CheckMethod { get; set; } = DispCheckMethodsEnum.NotSet;
       
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.MessageBus
{
    public class MessageBusSetStatusToNew : ProceduresModel
    {
        // in
        [Required]
        [ParameterIn]
        public int MessageBusID { get; set; }
    }
}

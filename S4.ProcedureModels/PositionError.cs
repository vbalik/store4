﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.ProcedureModels
{
    public enum PositionErrorReason : byte
    {
        IsFullOrEmpty = 1,
        NotUsable = 2,
        Other = 3
    }
}

﻿using S4.Entities.Helpers.Xml.Article;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Load
{
    public class ArticleLoad : ProceduresModel
    {
       
        // in
        [ParameterIn]
        [Required]
        public ArticleS3 ArticleS3 { get; set; }

        // in
        [ParameterIn]
        [Required]
        public string Xml { get; set; }

        // in
        [ParameterIn]
        public string Remark { get; set; }

        // out
        public bool Saved { get; set; } = false;
       
    }
}

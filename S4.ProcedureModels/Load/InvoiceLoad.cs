﻿using S4.Entities.Helpers.Xml.Invoice;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Load
{
    public class InvoiceLoad : ProceduresModel
    {
        // in
        [ParameterIn]
        [Required]
        public InvoiceS3 InvoiceS3 { get; set; }

        // in
        [ParameterIn]
        [Required]
        public string Xml { get; set; }

        // in
        [ParameterIn]
        public string Remark { get; set; }

        //out
        [ParameterOut]
        public int InvoceID { get; set; }

        [ParameterOut]
        public string MessageText { get; set; }

        [ParameterOut]
        public string WarningText { get; set; }
    }
}

﻿using S4.Entities.Helpers.Xml.Receive;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Load
{
    public class ReceiveDirLoad : ProceduresModel
    {

        public enum ResultEnum
        {
            Saved,
            AlreadyLoaded,
            None
        }

        // in
        [ParameterIn]
        [Required]
        public ReceiveS3 ReceiveS3 { get; set; }

        // in
        [ParameterIn]
        [Required]
        public string Xml { get; set; }

        // in
        [ParameterIn]
        public string Remark { get; set; }

        // out
        [ParameterOut(IsResult = true)]
        public ResultEnum Result { get; set; }
    }
}

﻿using S4.Entities.Helpers.Xml.Dispatch;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Load
{
    public class DispatchDirLoad : ProceduresModel
    {

        public enum ResultEnum
        {
            Saved,
            AlreadyLoaded,
            NoItems,
            Error
        }

        // in
        [ParameterIn]
        [Required]
        public DispatchS3 DispatchS3 { get; set; }

        // in
        [ParameterIn]
        [Required]
        public string Xml { get; set; }

        // in
        [ParameterIn]
        public string Remark { get; set; }

        // out
        [ParameterOut(IsResult = true)]
        public ResultEnum Result { get; set; }

        [ParameterOut]
        public int? InvoiceID { get; set; }

        [ParameterOut]
        public string MessageText { get; set; }

    }
}

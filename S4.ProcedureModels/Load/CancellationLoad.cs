﻿using S4.Entities.Helpers.Xml.Article;
using S4.Entities.Helpers.Xml.Cancellation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Load
{
    public class CancellationLoad : ProceduresModel
    {
       
        // in
        [ParameterIn]
        [Required]
        public CancellationS3 CancellationS3 { get; set; }

        // in
        [ParameterIn]
        [Required]
        public string Xml { get; set; }

        // in
        [ParameterIn]
        public string Remark { get; set; }
                
    }
}

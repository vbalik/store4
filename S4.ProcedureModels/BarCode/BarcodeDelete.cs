﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.BarCode
{
    public class BarcodeDelete : ProceduresModel
    {
        // in
        [Required]
        [ParameterIn]
        public int ArtPackID { get; set; }

        [ParameterIn]
        public int? ArtPackBarCodeID { get; set; }
    }
}

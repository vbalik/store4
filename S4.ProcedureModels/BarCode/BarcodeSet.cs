﻿using System.ComponentModel.DataAnnotations;

namespace S4.ProcedureModels.BarCode
{
    public class BarcodeSet : ProceduresModel
    {
        // in
        [Required]
        [ParameterIn]
        public int ArtPackID { get; set; }
        [Required]
        [ParameterIn]
        public string BarCode { get; set; }

        [ParameterIn]
        public int? ArtPackBarCodeID { get; set; }
        [ParameterIn]
        public bool AnotherBarCodeCreation { get; set; }
    }
}

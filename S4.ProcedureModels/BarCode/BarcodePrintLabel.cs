﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.BarCode
{
    public class BarcodePrintLabel : ProceduresModel
    {
        // in
        [Required]
        [ParameterIn]
        public int ArtPackID { get; set; }
        [ParameterIn]
        public int LabelsQuant { get; set; }
        [Required]
        [ParameterIn]
        public int PrinterLocationID { get; set; }
        [ParameterIn]
        public int? ArtPackBarCodeID { get; set; }
    }
}

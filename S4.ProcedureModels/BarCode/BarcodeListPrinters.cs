﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.BarCode
{
    public class BarcodeListPrinters : ProceduresModel
    {
    
        // in
        [Required]
        [ParameterIn]
        public Entities.PrinterType.PrinterClassEnum PrinterClass { get; set; }
        
        // out
        [ParameterOut]
        public List<Entities.PrinterLocation> PrintersList;
    }
}

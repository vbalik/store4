﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.BarCode
{
    public class BarcodePrintCarrierLabels : ProceduresModel
    {
        // in
        [Required]
        [ParameterIn]
        public int LabelsQuant;

        [Required]
        [ParameterIn]
        public int PrinterLocationID;

        // out
        // none
    }
}

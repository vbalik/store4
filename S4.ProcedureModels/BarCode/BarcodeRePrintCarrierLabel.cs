﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.BarCode
{
    public class BarcodeRePrintCarrierLabel : ProceduresModel
    {
        // in
        [Required]
        [ParameterIn]
        public string CarrierNum;

        [Required]
        [ParameterIn]
        public int PrinterLocationID;

        // out
        // none
    }
}

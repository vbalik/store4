﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.ProcedureModels
{
    public abstract class PrintProceduresModel : ProceduresModel
    {
        [ParameterIn]
        public bool LocalPrint { get; set; }

        [ParameterOut]
        [JsonIgnore]
        public byte[] FileData { get; set; }

        [ParameterOut]
        public string FileID { get; set; }
    }
}

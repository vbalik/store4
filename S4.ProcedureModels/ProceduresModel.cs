﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.ProcedureModels
{
    public abstract class ProceduresModel
    {
        /// <summary>
        /// Get unique external id of request
        /// </summary>
        [ParameterIn]
        public long ExternalRequestId { get; set; }

        [ParameterOut]
        public bool Success { get; set; }
        [ParameterOut]
        public string ErrorText { get; set; }
    }
}

﻿using S4.Entities.Helpers;
using System.Collections.Generic;

namespace S4.ProcedureModels.Exped
{
    public class ExpedListDirects : ProceduresModel
    {
        [ParameterOut]
        public List<DocumentInfo> Directions { get; set; }
    }
}

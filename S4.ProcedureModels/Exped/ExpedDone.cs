﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;


namespace S4.ProcedureModels.Exped
{
    public class ExpedDone : ProceduresModel
    {
        // in
        [ParameterIn]
        [Required]
        public int DirectionID { get; set; }

        [ParameterIn]
        public string ExpedTruck { get; set; }

        // out
        [ParameterOut]
        public List<Entities.Helpers.DocumentInfo> Directions { get; set; }

        [ParameterOut]
        public int StoMoveID { get; set; }
    }
}

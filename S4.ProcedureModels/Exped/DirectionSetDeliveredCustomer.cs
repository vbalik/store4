﻿using System;
using System.ComponentModel.DataAnnotations;

namespace S4.ProcedureModels.Exped
{
    public class DirectionSetDeliveredCustomer : ProceduresModel
    {
        // in
        [Required]
        [ParameterIn]
        public int DirectionID { get; set; }

        // in
        [Required]
        [ParameterIn]
        public DateTime DateTimeDeliveredCustomer { get; set; }
                
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Move
{
    public class MoveSave : ProceduresModel
    {
        // in
        [Required]
        [ParameterIn]
        public int SourcePositionID { get; set; }

        [Required]
        [ParameterIn]
        public int DestPositionID;

        [Required]
        [ParameterIn]
        public bool ChangeCarrier;

        [ParameterIn]
        public string DestCarrierNum;

        [Required]
        [ParameterIn]
        public List<Entities.Helpers.ArticleInfo> Articles { get; set; }

        [ParameterIn]
        public string Remark;


        // out
        [ParameterOut]
        public MoveStatusEnum PositionResultSource { get; set; }

        [ParameterOut]
        public MoveStatusEnum PositionResultDest { get; set; }

        [ParameterOut]
        public int StoMoveID;
    }
}

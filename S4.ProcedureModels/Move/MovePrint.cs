﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Move
{
    public class MovePrint : PrintProceduresModel
    {
        // in
        [Required]
        [ParameterIn]
        public int StoMoveID { get; set; }

        [Required]
        [ParameterIn]
        public int PrinterLocationID { get; set; }

        [ParameterIn]
        public int ReportID { get; set; }

    }
}

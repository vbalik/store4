﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Move
{
    public class MoveGetPosition : ProceduresModel
    {
        // in
        [Required]
        [ParameterIn]
        public int PositionID { get; set; }

        // out
        [ParameterOut]
        public List<Entities.Helpers.ArticleInfo> Articles { get; set; }
                
        [ParameterOut(IsResult = true)]
        public MoveStatusEnum PositionResult { get; set; }
    }
}

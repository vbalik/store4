﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Move
{
    public class MoveCheckItems : ProceduresModel
    {
        // in
        [Required]
        [ParameterIn]
        public int PositionID { get; set; }

        [Required]
        [ParameterIn]
        public List<Entities.Helpers.ArticleInfo> Articles { get; set; }


        // out
        [ParameterOut(IsResult = true)]
        public MoveStatusEnum PositionResult { get; set; }
        
        [ParameterOut]
        public bool ItemExists { get; set; }

    }
}

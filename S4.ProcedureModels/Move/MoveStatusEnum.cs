﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.ProcedureModels.Move
{
    public enum MoveStatusEnum
    {
        OK = 0,
        Locked = 1,
        NotSaved = 2,
        NotFound = 3
    }
}

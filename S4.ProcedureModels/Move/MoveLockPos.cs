﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Move
{
    public class MoveLockPos : ProceduresModel
    {
        

        // in
        [Required]
        [ParameterIn]
        public int PositionID { get; set; }
        
        // out
        [ParameterOut(IsResult = true)]
        public MoveStatusEnum PositionResult { get; set; }
               
    }
}

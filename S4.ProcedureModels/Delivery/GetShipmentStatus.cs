﻿using S4.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Delivery
{
    public class GetShipmentStatus : PrintProceduresModel
    {
        // in
        [ParameterIn]
        [Required]
        public int DirectionID { get; set; }

        // in
        [ParameterIn]
        [Required]
        public bool IncludingCanceledShipment { get; set; }

        // out
        [ParameterOut]
        public List<Entities.Models.LabeledValue> Content { get; set; }

        [ParameterOut]
        public Entities.DeliveryDirection.DeliveryProviderEnum Provider { get; set; }
    }
}
﻿using S4.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Delivery
{
    public class InsertShipment : ProceduresModel
    {
        // in
        [ParameterIn]
        [Required]
        public int DirectionID { get; set; }

        [ParameterIn]
        [Required]
        public string ShipmentReference { get; set; }

        [ParameterIn]
        [Required]
        public DeliveryDirection.DeliveryProviderEnum? DeliveryProvider { get; set; }

        [ParameterIn]
        [Required]
        public IEnumerable<Tuple<int, string>> Parcels { get; set; }
    }
}

﻿using S4.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Delivery
{
    public class PrintManifest : PrintProceduresModel
    {
        // in
        [ParameterIn]
        [Required]
        public Entities.DeliveryDirection.DeliveryProviderEnum DeliveryProvider { get; set; }

        [ParameterIn]
        [Required]
        public string ManifestRefNumber { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Delivery
{
    public class PrintRawLabel : ProceduresModel
    {
        // in
        [ParameterIn]
        public int? LabelsQuant { get; set; }
        [Required]
        [ParameterIn]
        public int DirectionID { get; set; }
        [Required]
        [ParameterIn]
        public int PrinterLocationID { get; set; }

        // out
        // none
    }
}

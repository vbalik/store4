﻿using S4.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Delivery
{
    public class PrintLabel : PrintProceduresModel
    {
        // in
        [Required]
        [ParameterIn]
        public int DirectionID { get; set; }
        [ParameterIn]
        public int? LabelsQuant { get; set; }
        [ParameterIn]
        public int? PrinterLocationID { get; set; }
    }
}
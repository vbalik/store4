﻿using S4.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Delivery
{
    public class ManifestCreated : ProceduresModel
    {
        // in
        [ParameterIn]
        [Required]
        public int DeliveryManifestID { get; set; }

        [ParameterIn]
        [Required]
        public string ManifestRefNumber { get; set; }

        [ParameterIn]
        [Required]
        public DeliveryDirection.DeliveryProviderEnum DeliveryProvider { get; set; }
    }
}

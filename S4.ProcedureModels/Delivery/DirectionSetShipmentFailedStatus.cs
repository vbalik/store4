﻿using System.ComponentModel.DataAnnotations;

namespace S4.ProcedureModels.Delivery
{
    public class DirectionSetShipmentFailedStatus : ProceduresModel
    {
        // in
        [Required]
        [ParameterIn]
        public int DirectionID { get; set; }

        [Required]
        [ParameterIn]
        public string Text { get; set; }

    }
}

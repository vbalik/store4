﻿using S4.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Delivery
{
    public class ManifestRequest : ProceduresModel
    {
        // in
        [ParameterIn]
        [Required]
        public IEnumerable<int> DirectionIDs { get; set; }

        [ParameterIn]
        [Required]
        public DeliveryDirection.DeliveryProviderEnum DeliveryProvider { get; set; }
    }
}

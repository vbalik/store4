﻿using S4.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Delivery
{
    public class ResetShipment : ProceduresModel
    {
        // in
        [ParameterIn]
        [Required]
        public int DirectionID { get; set; }

        [ParameterIn]
        public int? BoxesCnt { get; set; }

        [ParameterIn]
        public decimal? Weight { get; set; }

    }
}

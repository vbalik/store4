﻿using S4.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Delivery
{
    public class GetDeliveryProviders : ProceduresModel
    {
        // out
        [ParameterOut]
        public IEnumerable<(Entities.DeliveryDirection.DeliveryProviderEnum providerCode, string providerName)> Providers { get; set; }
    }
}

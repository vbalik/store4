﻿using S4.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Delivery
{
    public class GetDirectionsForWeigh : ProceduresModel
    {
        // in
        [ParameterIn]
        public int DirectionID { get; set; }
        [ParameterIn]
        public string DocInfo { get; set; }
        [ParameterIn]
        public List<string> DeliveryTypes { get; set; }

        // out
        [ParameterOut]
        public List<DirectionRow> Directions { get; set; }
    }

    public class DirectionRow
    {
        public int DirectionID { get; set; }
        public string DocInfo { get; set; }
        public string Partner { get; set; }
        public string DeliveryType { get; set; }
        public string WeighValue { get; set; }
    }
}
﻿using S4.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Delivery
{
    public class LabelPrinted : ProceduresModel
    {
        // in
        [ParameterIn]
        [Required]
        public int DirectionID { get; set; }
    }
}

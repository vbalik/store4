﻿using S4.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Delivery
{
    public class GetDirectionsForManifest : ProceduresModel
    {
        // in
        [ParameterIn]
        [Required]
        public DeliveryDirection.DeliveryProviderEnum? DeliveryProvider { get; set; }

        // out
        [ParameterOut]
        public IEnumerable<int> Directions { get; set; }
    }
}
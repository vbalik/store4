﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Logistics
{
    public class LogisticsDispToStoIn : ProceduresModel
    {
        // in
        [Required]
        [ParameterIn]
        public int DirectionID { get; set; }

        [Required]
        [ParameterIn]
        public int ReceivePositionID { get; set; }

        [Required]
        public string WorkerID { get; set; }
    }
}

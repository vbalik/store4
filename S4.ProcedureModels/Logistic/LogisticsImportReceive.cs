﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Logistics
{
    public class LogisticsImportReceive : ProceduresModel
    {
        public class LogisticsImportReceiveItem
        {
            public int ArtPackID { get; set; }
            public decimal Quantity { get; set; }
        }


        // in
        [ParameterIn]
        public List<LogisticsImportReceiveItem> Items { get; set; }

        [ParameterIn]
        public string PartnerRef { get; set; }

        [ParameterIn]
        public int ReceivePositionID { get; set; }

        [Required]
        [ParameterIn]
        public int DestPositionID { get; set; }

        [Required]
        public string WorkerID { get; set; }

        // out
        [ParameterOut]
        public int DirectionID { get; set; }
        [ParameterOut]
        public int StoMoveID { get; set; }

    }
}

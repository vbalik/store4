﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Logistics
{
    public class LogisticsDispatchToPosition : ProceduresModel
    {
        // in
        [Required]
        [ParameterIn]
        public int DirectionID { get; set; }

        [ParameterIn]
        public int ReceivePositionID { get; set; }

        [Required]
        [ParameterIn]
        public int DestPositionID { get; set; }

        [Required]
        public string WorkerID { get; set; }

        //out
        [ParameterOut]
        public int StoMoveID { get; set; }
    }
}

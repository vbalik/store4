﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Login
{
    public class LoginModel : ProceduresModel
    {
        // in
        [Required]
        [ParameterIn]
        public string UserName { get; set; }

        // in
        [Required]
        [ParameterIn]
        public string Password { get; set; }

        // out
        [ParameterOut]
        public S4.Entities.Worker Worker { get; set; }
    }
}

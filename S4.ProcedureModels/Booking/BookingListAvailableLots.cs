﻿using S4.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace S4.ProcedureModels.Booking
{
    public class BookingListAvailableLots : ProceduresModel
    {
        // in
        [Required]
        [ParameterIn]
        public int ArtPackID { get; set; }
        
        // out
        [Required]
        [ParameterOut]
        public List<StoreMoveLotModel> StoreMoveLots { get; set; } = new List<StoreMoveLotModel>();
    }
}

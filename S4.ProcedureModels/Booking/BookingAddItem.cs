﻿using System;
using System.ComponentModel.DataAnnotations;

namespace S4.ProcedureModels.Booking
{
    public class BookingAddItem : ProceduresModel
    {
        // in
        [Required]
        [ParameterIn]
        public int StoMoveLotID { get; set; }

        [Required]
        [ParameterIn]
        public int PartnerID { get; set; }

        [Required]
        [ParameterIn]
        public int PartAddrID { get; set; }

        [Required]
        [ParameterIn]
        public decimal RequiredQuantity { get; set; }
        
        [Required]
        [ParameterIn]
        public DateTime BookingTimeout { get; set; }


        [ParameterOut]
        public int BookingID { get; set; }
}
}

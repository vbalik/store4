﻿using System;
using System.ComponentModel.DataAnnotations;

namespace S4.ProcedureModels.Booking
{
    public class BookingDeleteItem : ProceduresModel
    {
        // in
        [Required]
        [ParameterIn]
        public int BookingID { get; set; }
    }
}

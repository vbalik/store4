﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.ProcedureModels.Booking
{
    public class BookingFromReceive : ProceduresModel
    {
        // in
        [Required]
        [ParameterIn]
        public int DirectionID { get; set; }

        [Required]
        [ParameterIn]
        public int PartnerID { get; set; }
        
        [Required]
        [ParameterIn]
        public int PartAddrID { get; set; }

        [ParameterIn]
        public DateTime? BookingTimeout { get; set; }


        // out
    }
}

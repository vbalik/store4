﻿using S4.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.ProcedureModels.Booking
{
    public class StoreMoveLotModel : StoreMoveLot
    {
        public decimal Quantity { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace S4mp.Entities
{
    public class DispCheckSerial
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        
        public int DirectionID { get; set; }

        public int ArtPackID { get; set; }
        public int DirectionItemID { get; set; }
        public int Quantity { get; set; }

        public string BatchNum { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public string SerialNumber { get; set; }

        public DateTime? Updated { get; set; }

    }
}

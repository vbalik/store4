﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4mp.Entities
{
    public class ItemInfo
    {
        public int DirectionItemID { get; set; }

        public int StoMoveLotID { get; set; }

        public int StoMoveItemID { get; set; }

        public string ArticleInfo => $"{ArticleCode} - {ArticleDesc}";

        public int ArticleID { get; set; }
        public int ArtPackID { get; set; }
        public string ArticleCode { get; set; }
        public string ArticleDesc { get; set; }
        public string PackDesc { get; set; }
        public bool UseBatch { get; set; }
        public bool UseExpiration { get; set; }
        public bool UseSerialNumber { get; set; }
        public string UserRemark { get; set; }

        public decimal Quantity { get; set; }
        public string BatchNum { get; set; } = null;
        public DateTime? ExpirationDate { get; set; }
        public string CarrierNum { get; set; } = null;

        public bool WholeCarrier { get; set; }

        public int From_positionID { get; set; }
        public string From_posCode { get; set; }

        public int To_positionID { get; set; }
        public string To_posCode { get; set; }

        public string Remark { get; set; }
        public string XtraString { get; set; }
        public int XtraInt { get; set; }
        public bool XtraBool { get; set; }
        public DateTime XtraDate { get; set; }

        public int DocPosition { get; set; }
        public int StoMoveID { get; set; }

        public bool SpecialDelivery { get; set; }

    }
}

﻿using System;
using SQLite;

namespace S4mp.Entities
{
    public class CustomEventLoggerLog
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string Type { get; set; }
        public string Message { get; set; }
        public string Parameters { get; set; }
        public DateTime Created { get; set; }
    }
}
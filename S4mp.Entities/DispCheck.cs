﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace S4mp.Entities
{
    public class DispCheck
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        
        public int ArtPackID { get; set; }

        public int DirectionID { get; set; }

        public decimal? FoundQuantity { get; set; }

        public DateTime? Updated { get; set; }
    }
}

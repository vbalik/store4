using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_storemove_xtra")]
	[PrimaryKey("stomoveexid")]
	[ExplicitColumns]
	public partial class StoreMoveXtra
	{
		[Required]
		[Column("stoMoveExID")]
		public int StoMoveExID {get; set;}

		[Required]
		[Column("stoMoveID")]
		public int StoMoveID {get; set;}

		[Required]
		[MaxLength(16)]
		[Column("xtraCode")]
		public string XtraCode {get; set;}

		[Required]
		[Column("xtraValue")]
		public string XtraValue {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

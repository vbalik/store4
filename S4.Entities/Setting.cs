using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_settings")]
	[PrimaryKey("setfieldid", AutoIncrement = false)]
	[ExplicitColumns]
	public partial class Setting
	{
		[Required]
		[MaxLength(32)]
		[Column("setFieldID")]
		public string SetFieldID {get; set;}

		[Required]
		[MaxLength(64)]
		[Column("setDataType")]
		public string SetDataType {get; set;}

		[Required]
		[Column("setValue")]
		public string SetValue {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace S4.Entities
{
    public class SettingsDictionary
    {
        public const string DISPATCH_REPORT_ID = "dispatch/report/id";
        public const string DISPATCH_SN_REPORT_ID = "dispatch_sn/report/id";
        public const string DISPATCH_REPORT_DEFAULT_PRINTER_ID = "dispatch/report/default_printer_id";
        public const string DISPATCH_BOX_REPORT_ID = "dispatch/boxReport/id";
        public const string DISPATCH_LABEL_REPORT_SMALL_ID = "dispatch/labelReportSmall/id";
        public const string DISPATCH_LABEL_REPORT_BIG_ID = "dispatch/labelReportBig/id";
        public const string DISPATCH_LABEL_REPORT_ADDRESS = "dispatch/labelReportAddress";
        public const string DISPATCH_SET_WEIGHT_BOXES_CNT = "dispatch/setWeightBoxesCnt";
        public const string DISPATCH_PD_ADR_REPORT_ID = "dispatch_pdadr/report/id";

        public const string DISP_CHECK_METHOD_DEFAULT = "dispCheck/methoddefault";

        public const string RECEIVE_POSITION_ID = "receive/position_id";
        public const string RECEIVE_DOC_NUM_PREFIX = "receive/doc_num_prefix";
        public const string RECEIVE_REPORT_ID = "receive/report/id";
        public const string RECEIVE_REPORT_DEFAULT_PRINTER_ID = "receive/report/default_printer_id";

        public const string POSITION_LABEL_REPORT_ID = "position/labelReport/id";

        public const string MOVE_REPORT_ID = "move/report/id";
        public const string ON_STORE_REPORT_ID = "onstore/report/id";

        public const string PRINTER_TYPE_LASERJET = "printer_type/laserjet";

        public const string PRINT_EXPEDITION_REPORT_ID = "printexpedition/report/id";

        public const string MAIL_SETTING = "mailsetting";
        public const string EXPORT_LEKARNY = "exportlekarny";
        public const string EXPORT_ABRA = "exportABRA";
        public const string DELIVERY_SERVICES_SETTING = "deliveryservicessetting";
        public const string STOTAK_REMOTE_REPORT_ID = "stotakRemote/report/id";
        
        public const string TERMINAL_MENU_ITEMS = "terminal/menu_items";
        public const string TERMINAL_ENABLE_ASSIGN_DIRECTION_USER = "dispatch/enableAssignDirUser";
        public const string TERMINAL_BARCODE_DISALLOWED_VALUE_PATTERN = "barcodeDisallowedValuePattern"; // REGEX string pattern / NOT ALLOWED

        public const string ABRA_CONNECTION_STRING = "abra/connection_string";

        public const string DELIVERY_PRINT_S4_LABEL_REPORT_ID = "delivery/print_label_report_id";
        public const string DELIVERY_PRINT_S4_RAW_LABEL_REPORT_PPL_ID = "delivery/pplDeliveryReport/id";
        public const string DELIVERY_PACKAGES_REPORT_ID = "deliverypackages/report/id";
        public const string DISPATCH_DEFAULT_POSITION_ID = "dispatch/default/position/id";
        public const string DELIVERY_SHOW_COLUMN_GRID = "delivery/showColumnGrid";
        
        public const string SHOW_SPEC_FEATURES_ALERT_INFO_LIST = "showSpecFeaturesAlertInfoList";

        public const string EXPORT_ADVANTIS_DEFAULT_PARTNER_IDs = "exportadvantis/partner/ids";

        public const string BOOKING_TIMEOUT_HOURS = "bookingTimeoutHours";

        public class SettingsDictionaryItem
        {
            public string SettingPath { get; internal set; }
            public Type SettingValueType { get; internal set; }
            public object DefaultValue { get; internal set; }
        }


        public static readonly List<SettingsDictionaryItem> Values = new List<SettingsDictionaryItem>()
        {
            new SettingsDictionaryItem() { SettingPath = DISPATCH_REPORT_ID, SettingValueType = typeof(System.Int32), DefaultValue = 1 },
            new SettingsDictionaryItem() { SettingPath = DISPATCH_SN_REPORT_ID, SettingValueType = typeof(System.Int32), DefaultValue = 33 },
            new SettingsDictionaryItem() { SettingPath = DISPATCH_PD_ADR_REPORT_ID, SettingValueType = typeof(System.Int32), DefaultValue = 49 },
            new SettingsDictionaryItem() { SettingPath = DISPATCH_REPORT_DEFAULT_PRINTER_ID, SettingValueType = typeof(System.Int32), DefaultValue = 1 },
            new SettingsDictionaryItem() { SettingPath = DISPATCH_BOX_REPORT_ID, SettingValueType = typeof(System.Int32), DefaultValue = 0 },

            new SettingsDictionaryItem() { SettingPath = DISPATCH_LABEL_REPORT_SMALL_ID, SettingValueType = typeof(System.Int32), DefaultValue = 2 },
            new SettingsDictionaryItem() { SettingPath = DISPATCH_LABEL_REPORT_BIG_ID, SettingValueType = typeof(System.Int32), DefaultValue = 28 },
            new SettingsDictionaryItem() { SettingPath = DISPATCH_LABEL_REPORT_ADDRESS, SettingValueType = typeof(System.Int32), DefaultValue = 30 },
            new SettingsDictionaryItem() { SettingPath = DISPATCH_SET_WEIGHT_BOXES_CNT, SettingValueType = typeof(System.Boolean), DefaultValue = false },
            new SettingsDictionaryItem() { SettingPath = DISPATCH_DEFAULT_POSITION_ID, SettingValueType = typeof(System.Int32), DefaultValue = 18 },

            new SettingsDictionaryItem() { SettingPath = DISP_CHECK_METHOD_DEFAULT, SettingValueType = typeof(System.Int32), DefaultValue = 0 },
        
            new SettingsDictionaryItem() { SettingPath = RECEIVE_POSITION_ID, SettingValueType = typeof(System.Int32), DefaultValue = 1 },
            new SettingsDictionaryItem() { SettingPath = RECEIVE_DOC_NUM_PREFIX, SettingValueType = typeof(System.String), DefaultValue = "PRIJ" },
            new SettingsDictionaryItem() { SettingPath = RECEIVE_REPORT_ID, SettingValueType = typeof(System.Int32), DefaultValue = 8 },
            new SettingsDictionaryItem() { SettingPath = RECEIVE_REPORT_DEFAULT_PRINTER_ID, SettingValueType = typeof(System.Int32), DefaultValue = 1 },

            new SettingsDictionaryItem() { SettingPath = POSITION_LABEL_REPORT_ID, SettingValueType = typeof(System.Int32), DefaultValue = 7 },

            new SettingsDictionaryItem() { SettingPath = MOVE_REPORT_ID, SettingValueType = typeof(System.Int32), DefaultValue = 20 },
            new SettingsDictionaryItem() { SettingPath = ON_STORE_REPORT_ID, SettingValueType = typeof(System.Int32), DefaultValue = 21 },

            new SettingsDictionaryItem() { SettingPath = PRINTER_TYPE_LASERJET, SettingValueType = typeof(System.Int32), DefaultValue = 1 },

            new SettingsDictionaryItem() { SettingPath = PRINT_EXPEDITION_REPORT_ID, SettingValueType = typeof(System.Int32), DefaultValue = 22 },

            new SettingsDictionaryItem() { SettingPath = MAIL_SETTING, SettingValueType = typeof(System.String), DefaultValue = "" },
            new SettingsDictionaryItem() { SettingPath = EXPORT_LEKARNY, SettingValueType = typeof(System.String), DefaultValue = "" },
            new SettingsDictionaryItem() { SettingPath = EXPORT_ABRA, SettingValueType = typeof(System.String), DefaultValue = "" },
            new SettingsDictionaryItem() { SettingPath = DELIVERY_SERVICES_SETTING, SettingValueType = typeof(System.String), DefaultValue = "" },
            new SettingsDictionaryItem() { SettingPath = STOTAK_REMOTE_REPORT_ID, SettingValueType = typeof(System.Int32), DefaultValue = 37 }, 
            
            new SettingsDictionaryItem() { SettingPath = TERMINAL_MENU_ITEMS, SettingValueType = typeof(System.String), DefaultValue = null },
            new SettingsDictionaryItem() { SettingPath = TERMINAL_ENABLE_ASSIGN_DIRECTION_USER, SettingValueType = typeof(System.Boolean), DefaultValue = false },
            new SettingsDictionaryItem() { SettingPath = TERMINAL_BARCODE_DISALLOWED_VALUE_PATTERN, SettingValueType = typeof(System.String), DefaultValue = default(string) },

            new SettingsDictionaryItem() { SettingPath = ABRA_CONNECTION_STRING, SettingValueType = typeof(System.String), DefaultValue = null },

            new SettingsDictionaryItem() { SettingPath = DELIVERY_PRINT_S4_LABEL_REPORT_ID, SettingValueType = typeof(System.Int32), DefaultValue = 28 },
            new SettingsDictionaryItem() { SettingPath = DELIVERY_PRINT_S4_RAW_LABEL_REPORT_PPL_ID, SettingValueType = typeof(System.Int32), DefaultValue = 36 },
            new SettingsDictionaryItem() { SettingPath = DELIVERY_PACKAGES_REPORT_ID, SettingValueType = typeof(System.Int32), DefaultValue = 39 },
            new SettingsDictionaryItem() { SettingPath = DELIVERY_SHOW_COLUMN_GRID, SettingValueType = typeof(System.Boolean), DefaultValue = false },
            new SettingsDictionaryItem() { SettingPath = SHOW_SPEC_FEATURES_ALERT_INFO_LIST, SettingValueType = typeof(System.Boolean), DefaultValue = false },
            new SettingsDictionaryItem() { SettingPath = EXPORT_ADVANTIS_DEFAULT_PARTNER_IDs, SettingValueType = typeof(System.String) },
            new SettingsDictionaryItem() { SettingPath = BOOKING_TIMEOUT_HOURS, SettingValueType = typeof(System.Int32), DefaultValue = 24 },
        };


        public static SettingsDictionaryItem ByPath(string settingPath)
        {
            return Values.Where(s => String.Equals(s.SettingPath, settingPath, Core.Data.PortableStringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
        }
               
    }
}

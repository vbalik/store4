﻿using System;
using System.Collections.Generic;
using System.Text;

using S4.Entities.StatusEngine;

namespace S4.Entities
{
    public partial class ArticlePacking
    {
        public const string AP_STATUS_OK = "AP_OK";
        public const string AP_STATUS_DISABLED = "AP_DISABLED";


        public static StatusSet Statuses = new StatusSet(s =>
        {
            s.AddStatus(AP_STATUS_OK, "Povoleno");
            s.AddStatus(AP_STATUS_DISABLED, "Zablokováno");
        });
    }
}

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_messagebus")]
	[PrimaryKey("messagebusid")]
	[ExplicitColumns]
	public partial class MessageBus
	{
		public enum MessageBusTypeEnum
		{
			[Display(Name = "Export dokladů")]
			ExportDirection = 0,
			[Display(Name = "Export šarží")]
			ExportABRA = 10,
			[Display(Name = "Odeslání mailu")]
			SendMail = 20,
			[Display(Name = "Export NemStore")]
			ExportNemStore = 30,
		}

		[Required]
		[Display(Name = "ID")]
		[Column("messageBusID")]
		public int MessageBusID {get; set;}

		[Required]
		[MaxLength(32)]
		[Display(Name = "Stav")]
		[StatusSet("Statuses")]
		[Column("messageStatus")]
		public string MessageStatus {get; set;}

		[Required]
		[Display(Name = "Typ")]
		[Column("messageType")]
		public MessageBusTypeEnum MessageType {get; set;}

		[Display(Name = "Data")]
		[Column("messageData")]
		public string MessageData {get; set;}

		[Required]
		[Display(Name = "Počet pokusů")]
		[Column("tryCount")]
		public int TryCount {get; set;}

		[Display(Name = "Další spuštění")]
		[Column("nextTryTime")]
		public DateTime? NextTryTime {get; set;}

		[Display(Name = "Popis chyby")]
		[Column("errorDesc")]
		public string ErrorDesc {get; set;}

		[Display(Name = "Poslední zápis")]
		[Column("lastDateTime")]
		public DateTime? LastDateTime {get; set;}

		[Required]
		[Display(Name = "Založeno")]
		[RawColumnName("m.entryDateTime")]
		[Column("entryDateTime")]
		public DateTime EntryDateTime {get; set;}

		[Display(Name = "ID dokladu")]
		[Column("directionID")]
		public int DirectionID {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

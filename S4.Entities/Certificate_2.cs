using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{

    public partial class Certificate
    {
        public string CertificateInfo
        {
            get
            {
                if (DateTime.Now.Ticks > ExpirationDateFrom.Ticks && DateTime.Now.Ticks < ExpirationDateTo.Ticks)
                    return "platný";
                else
                {
                    if (DateTime.Now.Ticks < ExpirationDateFrom.Ticks)
                        return "ještě není platný";
                    else
                        return "expirovaný";
                }
            }
        }

    }
}

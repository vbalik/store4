using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_article_xtra")]
	[PrimaryKey("articleexid")]
	[ExplicitColumns]
	public partial class ArticleXtra
	{
		[Required]
		[Column("articleExID")]
		public int ArticleExID {get; set;}

		[Required]
		[Column("articleID")]
		public int ArticleID {get; set;}

		[Required]
		[MaxLength(16)]
		[Column("xtraCode")]
		public string XtraCode {get; set;}

		[Required]
		[Display(Name = "Poz.")]
		[Column("docPosition")]
		public int DocPosition {get; set;}

		[Required]
		[Column("xtraValue")]
		public string XtraValue {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

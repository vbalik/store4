using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_partnerlist_addresses")]
	[PrimaryKey("partaddrid")]
	[ExplicitColumns]
	public partial class PartnerAddress
	{
		[Required]
		[Column("partAddrID")]
		public int PartAddrID {get; set;}

		[Required]
		[Column("partnerID")]
		public int PartnerID {get; set;}

		[Required]
		[MaxLength(256)]
		[Column("addrName")]
		public string AddrName {get; set;}

		[Required]
		[MaxLength(32)]
		[Column("source_id")]
		public string Source_id {get; set;}

		[MaxLength(256)]
		[Column("addrLine_1")]
		public string AddrLine_1 {get; set;}

		[MaxLength(256)]
		[Column("addrLine_2")]
		public string AddrLine_2 {get; set;}

		[MaxLength(128)]
		[Column("addrCity")]
		public string AddrCity {get; set;}

		[MaxLength(32)]
		[Column("addrPostCode")]
		public string AddrPostCode {get; set;}

		[MaxLength(128)]
		[Column("addrPhone")]
		public string AddrPhone {get; set;}

		[Column("addrRemark")]
		public string AddrRemark {get; set;}

		[MaxLength(256)]
		[Column("deliveryInst")]
		public string DeliveryInst {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

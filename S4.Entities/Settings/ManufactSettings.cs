﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Entities.Settings
{
    public class ManufactSettings
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public enum DispatchAlgoritmEnum
        {
            ByExpiration,
            BigOrSmallFirst,
            OneBatchFirst,
            Under6ThenOneBatchFirst
        }


        [JsonProperty(PropertyName = "dispatchAlgoritm")]
        [JsonConverter(typeof(StringEnumConverter))]
        public DispatchAlgoritmEnum DispatchAlgoritm { get; set; } = DispatchAlgoritmEnum.ByExpiration;
    }
}

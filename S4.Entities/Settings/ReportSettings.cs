﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using static S4.Entities.Worker;

namespace S4.Entities.Settings
{
    public class SQL4Filters
    {
        public enum TypeFilterEnum : byte
        {
            SQL,
            Date,
            Input,
            PositionSeek,
            ArticleSeek,
            SQLMulti
        }

        public enum DataTypeFilterEnum : byte
        {
            DateTime,
            Number,
            Any
        }


        [JsonProperty(PropertyName = "sql")]
        public string SQL { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "id")]
        public string ID { get; set; }
        
        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty(PropertyName = "type")]
        public TypeFilterEnum Type { get; set; } = TypeFilterEnum.SQL;

        [JsonProperty(PropertyName = "width")]
        public string Width { get; set; }

        [JsonProperty(PropertyName = "mandatory")]
        public bool Mandatory { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty(PropertyName = "datatype")]
        public DataTypeFilterEnum DataType { get; set; } = DataTypeFilterEnum.Any;

        //filters values
        public string Text { get; set; }
        public object Value { get; set; }

    }

    public class ReportSettings
    {

        public enum TypeQueryEnum
        {
            None,
            LastPeriod,
            DaysBack
        }

        public enum TypeExportEnum 
        {
            Pdf,
            Xlsx
        }

        [JsonProperty(PropertyName = "sql")]
        public string SQL { get; set; }

        [JsonProperty(PropertyName = "sql4Filters")]
        public List<SQL4Filters> SQL4FiltersList { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty(PropertyName = "typeQuery")]
        public TypeQueryEnum TypeQuery { get; set; } = TypeQueryEnum.None;

        [JsonProperty(PropertyName = "reportName")]
        public string ReportName { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty(PropertyName = "flag")]
        public WorkerClassEnum Flag { get; set; } = WorkerClassEnum.Any;

        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty(PropertyName = "typeExport")]
        public TypeExportEnum TypeExport { get; set; } = TypeExportEnum.Pdf;
    }
}

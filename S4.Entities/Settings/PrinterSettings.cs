﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Entities.Settings
{
    public class PrinterSettings
    {
        [JsonProperty(PropertyName = "dispatchPositionCodes")]
        public List<string> DispatchPositionCodes { get; set; } = new List<string>();
    }
}

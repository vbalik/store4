﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;


namespace S4.Entities.Settings
{
    public class PrefixSettings
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public enum DispatchDistributionEnum
        {
            OneFromOneSection,
            AllFromOneSection            
        }


        public class DispatchSection
        {
            [JsonProperty(PropertyName = "sectID")]
            public string SectID { get; set; }

            [JsonProperty(PropertyName = "order")]
            public int Order { get; set; }
        }


        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty(PropertyName = "dispatchDistribution")]
        public DispatchDistributionEnum DispatchDistribution { get; set; } = DispatchDistributionEnum.OneFromOneSection;

        [JsonProperty(PropertyName = "dispatchSections")]
        public List<DispatchSection> DispatchSections { get; set; } = new List<DispatchSection>();

        [JsonProperty(PropertyName = "dispatchcheckmethod")]
        public int DispatchCheckMethod { get; set; } = 255;
    }
}

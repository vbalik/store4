using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

namespace S4.Entities
{
    public partial class InvoiceXtra : XtraData.IXtraDataStore<InvoiceXtra>
    {

        #region IXtraDataStore

        public (string, int) GetIDs() => (this.XtraCode, 0);


        public string GetValue() => this.XtraValue;


        public void Set(string code, int position, string strValue)
        {
            XtraCode = code;
            XtraValue = strValue;
        }


        public void SetParentID(int parentID)
        {
            InvoiceID = parentID;
        }

        #endregion
    }
}

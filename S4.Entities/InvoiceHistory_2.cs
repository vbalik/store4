using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;
using S4.Entities.StatusEngine;

namespace S4.Entities
{
	public partial class InvoiceHistory
	{
		public static StatusSet Statuses = Invoice.Statuses;
	}
}

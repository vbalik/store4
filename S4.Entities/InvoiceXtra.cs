using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_invoice_xtra")]
	[PrimaryKey("invoiceexid")]
	[ExplicitColumns]
	public partial class InvoiceXtra
	{
		[Required]
		[Display(Name = "ID")]
		[Column("invoiceExID")]
		public int InvoiceExID {get; set;}

		[Required]
		[Column("invoiceID")]
		public int InvoiceID {get; set;}

		[Required]
		[MaxLength(16)]
		[Column("xtraCode")]
		public string XtraCode {get; set;}

		[Required]
		[Column("xtraValue")]
		public string XtraValue {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

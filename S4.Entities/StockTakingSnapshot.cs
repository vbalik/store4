using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_stocktaking_snapshots")]
	[PrimaryKey("stotaksnapid")]
	[ExplicitColumns]
	public partial class StockTakingSnapshot
	{
		public enum StockTakingSnapshotClassEnum
		{
			Internal = 0,
			Outer = 1,
		}

		[Required]
		[Column("stotakSnapID")]
		public int StotakSnapID {get; set;}

		[Required]
		[Column("stotakID")]
		public int StotakID {get; set;}

		[Required]
		[Column("snapshotClass")]
		public StockTakingSnapshotClassEnum SnapshotClass {get; set;}

		[Required]
		[Column("positionID")]
		public int PositionID {get; set;}

		[Required]
		[Display(Name = "Balení")]
		[Column("artPackID")]
		public int ArtPackID {get; set;}

		[MaxLength(32)]
		[Display(Name = "Paleta")]
		[Column("carrierNum")]
		public string CarrierNum {get; set;}

		[Required]
		[Display(Name = "Množství")]
		[Column("quantity")]
		public decimal Quantity {get; set;}

		[MaxLength(32)]
		[Display(Name = "Šarže")]
		[Column("batchNum")]
		public string BatchNum {get; set;}

		[Display(Name = "Expirace")]
		[Column("expirationDate")]
		public DateTime? ExpirationDate {get; set;}

		[Display(Name = "Jednotková cena")]
		[Column("unitPrice")]
		public decimal? UnitPrice {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

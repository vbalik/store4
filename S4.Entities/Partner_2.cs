﻿using System;
using System.Collections.Generic;

using S4.Entities.StatusEngine;


namespace S4.Entities
{
    public partial class Partner
    {
        public const int DEFAULT_PARTNER_ID = 0;

        public const string PA_STATUS_OK = "PA_OK";
        public const string PA_STATUS_DISABLED = "PA_DISABLED";

        public static StatusSet Statuses = new StatusSet(s =>
        {
            s.AddStatus(PA_STATUS_OK, "OK");
            s.AddStatus(PA_STATUS_DISABLED, "Zablokováno");
        });
    }
}

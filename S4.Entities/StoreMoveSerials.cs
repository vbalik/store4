using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_storemove_serials")]
	[PrimaryKey("serialid")]
	[ExplicitColumns]
	public partial class StoreMoveSerials
	{
		[Required]
		[Column("serialID")]
		public int SerialID {get; set;}

		[Required]
		[Column("stoMoveItemID")]
		public int StoMoveItemID {get; set;}

		[Required]
		[MaxLength(64)]
		[Column("serialNumber")]
		public string SerialNumber {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

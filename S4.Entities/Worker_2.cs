﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

using S4.Entities.StatusEngine;

namespace S4.Entities
{
    public partial class Worker
    {
        public const string WK_STATUS_ONDUTY = "ONDUTY";
        public const string WK_STATUS_SET_TO_RECEIVE = "RECEIVE";
        public const string WK_STATUS_SET_TO_STOMOVE = "STOMOVE";
        public const string WK_STATUS_SET_TO_DISPATCH = "DISPATCH";
        public const string WK_STATUS_SET_TO_DISPCHECK = "DISPCHECK";
        public const string WK_STATUS_ON_HOLIDAY = "HOLIDAY";
        public const string WK_STATUS_DISABLED = "DISABLED";

        [Core.DBTestIgnore]
        public bool IsNew { get; set; }

        public static StatusSet Statuses = new StatusSet(s =>
        {
            var enabledActions = new List<ActionDef>()
            {
                new ActionDef() { Name = "Pracuje", ActionProcedure = "WorkerSetOnDuty" },
                new ActionDef() { Name = "Dovolená", ActionProcedure = "WorkerSetOutOfOrder" },
                new ActionDef() { Name = "Skončil", ActionProcedure = "WorkerSetDisabled" },
                new ActionDef() { Name = "Změna stavu", ActionProcedure = "setstatus_dialog", ActionType = ActionTypeEnum.Dialog }
            };


            s.AddStatus(WK_STATUS_ONDUTY, "V práci")
                .SetActions(enabledActions);

            s.AddStatus(WK_STATUS_SET_TO_RECEIVE, "Příjem")
                .SetActions(enabledActions);

            s.AddStatus(WK_STATUS_SET_TO_STOMOVE, "Přeskladnění")
                .SetActions(enabledActions);

            s.AddStatus(WK_STATUS_SET_TO_DISPATCH, "Vyskladnění")
                .SetActions(enabledActions);

            s.AddStatus(WK_STATUS_SET_TO_DISPCHECK, "Kontrola")
                .SetActions(enabledActions);

            s.AddStatus(WK_STATUS_ON_HOLIDAY, "Dovolená")
                .SetActions(enabledActions)
                .SetColor(System.Drawing.Color.Blue);

            s.AddStatus(WK_STATUS_DISABLED, "Zrušeno")
                .SetActions(enabledActions)
                .SetColor(System.Drawing.Color.Gray);
        });

        [Core.DBTestIgnore]
        [Display(Name = "Zařazení")]
        public string WorkerClassString
        {
            get
            {
                var captions = WorkerClassList.Select(c => (typeof(WorkerClassEnum).GetField(c).GetCustomAttribute(typeof(DisplayAttribute)) as DisplayAttribute).Name).ToArray();
                return string.Join(", ", captions);
            }
        }

        [Core.DBTestIgnore]
        public string[] WorkerClassList
        {
            get
            {
                var result = new List<string>();

                foreach (var item in Enum.GetValues(typeof(WorkerClassEnum)).Cast<int>().Where(i => i > 0))
                {
                    if ((item & (int)WorkerClass) == item)
                        result.Add(((WorkerClassEnum)item).ToString());
                }

                return result.ToArray();
            }
            set
            {
                int val = 0;
                foreach (var item in value)
                {
                    val = val | (int)Enum.Parse(typeof(WorkerClassEnum), item);
                }
                WorkerClass = (WorkerClassEnum)val;
            }
        }

        [Core.DBTestIgnore]
        public string Password { get; set; }
    }
}

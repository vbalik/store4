using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_invoice_history")]
	[PrimaryKey("invoicehistid")]
	[ExplicitColumns]
	public partial class InvoiceHistory
	{
		[Required]
		[Display(Name = "ID")]
		[Column("invoiceHistID")]
		public long InvoiceHistID {get; set;}

		[Required]
		[Column("invoiceID")]
		public int InvoiceID {get; set;}

		[Required]
		[MaxLength(16)]
		[Display(Name = "Událost")]
		[StatusSet("Statuses", true)]
		[Column("eventCode")]
		public string EventCode {get; set;}

		[Display(Name = "Data")]
		[Column("eventData")]
		public string EventData {get; set;}

		[Required]
		[Display(Name = "Zadáno")]
		[Column("entryDateTime")]
		public DateTime EntryDateTime {get; set;}

		[Required]
		[MaxLength(16)]
		[Display(Name = "Uživatel")]
		[Column("entryUserID")]
		public string EntryUserID {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

﻿using Newtonsoft.Json;
using NPoco;
using System;


namespace S4.Entities
{
    public partial class Reports
    {
        public const string REPORT_TYPE_BOX_LABEL = "boxLabel";


        [Core.DBTestIgnore]
        [Column("settings")]
        [JsonIgnore]
        public string _settingsJson
        {
            get
            {
                if (Settings == null)
                    return null;
                else
                    return JsonConvert.SerializeObject(Settings, Formatting.Indented);
            }

            set
            {
                if (String.IsNullOrWhiteSpace(value))
                    Settings = new Settings.ReportSettings();
                else
                    Settings = JsonConvert.DeserializeObject<Settings.ReportSettings>(value);
            }
        }

        [Core.DBTestIgnore]
        public Settings.ReportSettings Settings { get; set; } = new Settings.ReportSettings();

    }
}

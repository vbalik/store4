using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_articlelist_packing_barcodes")]
	[PrimaryKey("artpackbarcodeid")]
	[ExplicitColumns]
	public partial class ArticlePackingBarCode
	{
		[Required]
		[Display(Name = "ID")]
		[Column("artPackBarCodeID")]
		public int ArtPackBarCodeID {get; set;}

		[Required]
		[Column("articleID")]
		public int ArticleID {get; set;}

		[Required]
		[Display(Name = "Balení")]
		[Column("artPackID")]
		public int ArtPackID {get; set;}

		[Required]
		[Display(Name = "Kód")]
		[Column("barCode")]
		public string BarCode {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

﻿using System;
using System.Collections.Generic;
using System.Text;

using S4.Entities.StatusEngine;

namespace S4.Entities
{
    public partial class DirectionItem
    {
        public const string DI_STATUS_LOAD = "LOAD";
        public const string DI_STATUS_RECEIVE_IN_PROC = "PREC";
        public const string DI_STATUS_RECEIVE_COMPLET = "CREC";
        public const string DI_STATUS_STOIN_COMPLET = "CSTI";
        public const string DI_STATUS_DIR_MOVE_SAVED = "DDIM";
        public const string DI_STATUS_DISPATCH_SAVED = "SDIS";
        public const string DI_STATUS_DISP_CHECKED = "DCHE";
        public const string DI_STATUS_CANCEL = "CANC";

        public const string DI_EVENT_ITEM_CHANGE = "ITCH";


        public static StatusSet Statuses = new StatusSet(s =>
        {
            s.AddStatus(DI_STATUS_LOAD, "Nahráno")
                .AddAction("Změnit položku", ActionTypeEnum.Dialog, "directionItemChangeDispatch_dialog").AuthorizationSuperUser();

            s.AddStatus(DI_STATUS_RECEIVE_IN_PROC, "Příjem - nový")
                .AddAction("Změnit položku", ActionTypeEnum.Dialog, "directionItemChange_dialog").AuthorizationSuperUser();
            s.AddStatus(DI_STATUS_RECEIVE_COMPLET, "Příjem - hotovo")
                .AddAction("Změnit položku", ActionTypeEnum.Dialog, "directionItemChange_dialog").AuthorizationSuperUser();

            s.AddStatus(DI_STATUS_STOIN_COMPLET, "Naskladnění - hotovo")
                .SetColor(System.Drawing.Color.FromArgb(136, 136, 136));

            s.AddStatus(DI_STATUS_DIR_MOVE_SAVED, "Příprava - hotovo");
            s.AddStatus(DI_STATUS_DISPATCH_SAVED, "Vychystání - hotovo");
            s.AddStatus(DI_STATUS_DISP_CHECKED, "Kontrola - hotovo");
            s.AddStatus(DI_STATUS_CANCEL, "Zrušeno");

            // events
            s.AddEvent(DI_EVENT_ITEM_CHANGE, "Změna položky");
        });
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Entities.XtraData
{
    public interface IXtraDataStore<T>
    {
        (string, int) GetIDs();
        string GetValue();
        void Set(string code, int position, string strValue);
        void SetParentID(int parentID);
    }
}

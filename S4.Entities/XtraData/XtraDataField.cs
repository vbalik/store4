﻿using System;


namespace S4.Entities.XtraData
{
    public class XtraDataField
    {
        public string Code { get; set; }

        public string Name { get; set; }

        public Type XtraType { get; set; }

        public bool WithPositions { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;


namespace S4.Entities.XtraData
{
    public class XtraDataDict
    {
        public Dictionary<string, XtraDataField> _fields = new Dictionary<string, XtraDataField>();


        public XtraDataDict(Action<XtraDataDict> initFunction)
        {
            initFunction(this);
        }



        public XtraDataDict AddField(XtraDataField field)
        {
            _fields.Add(field.Code, field);
            return this;
        }

        public XtraDataDict AddField(string code, string name, Type type, bool withPositions = false)
        {
            var field = new XtraDataField() { Code = code, Name = name, XtraType = type, WithPositions = withPositions };
            return AddField(field);
        }


        public XtraDataField this[string code]
        {
            get
            {
                XtraDataField fld;
                if (_fields.TryGetValue(code, out fld))
                    return fld;
                else
                    throw new Exception($"XtraData field with code '{code}' is not defined");
            }
        }
    }
}

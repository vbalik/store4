﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace S4.Entities.XtraData
{
    public class XtraDataProxy<T> where T : IXtraDataStore<T>, new()
    {
        private readonly XtraDataDict _xtraDataDict;
        private List<T> _data = new List<T>();
        private List<T> _toRemove = new List<T>();

        public XtraDataProxy(XtraDataDict xtraDataDict)
        {
            _xtraDataDict = xtraDataDict;
        }

        public List<T> Data { get => _data; }


        public object this [string code]
        {
            set
            {
                // get field from dictionary
                var fld = _xtraDataDict[code];

                // check type
                if(value != null && value.GetType() != fld.XtraType)
                    throw new Exception($"XtraData field '{fld.Name}' value type must be {fld.XtraType}");

                // save value
                SetValue(fld, 0, value);
            }

            get
            {
                // get field from dictionary
                var fld = _xtraDataDict[code];

                // return value
                return GetValue(fld, 0);
            }
        }


        public object this[string code, int position]
        {
            set
            {
                // get field from dictionary
                var fld = _xtraDataDict[code];

                // check WithPositions
                if (!fld.WithPositions)
                    throw new Exception($"XtraData field '{fld.Name}' value can't be used with position");

                // check type
                if (value != null && value.GetType() != fld.XtraType)
                    throw new Exception($"XtraData field '{fld.Name}' value type must be {fld.XtraType}");

                // save value
                SetValue(fld, position, value);
            }

            get
            {
                // get field from dictionary
                var fld = _xtraDataDict[code];
                
                // check WithPositions
                if (!fld.WithPositions)
                    throw new Exception($"XtraData field '{fld.Name}' value can't be used with position");

                // return value
                return GetValue(fld, position);
            }
        }


        public bool IsNull(string code)
        {
            return IsNull(code, 0);
        }

        public bool IsNull(string code, int position)
        {
            var fld = _xtraDataDict[code];
            return (GetValue(fld, position) == null);
        }


        public void Load(NPoco.IDatabase db, NPoco.Sql where)
        {
            _data = db.Fetch<T>(where);
        }


        public void Save(NPoco.IDatabase db, int parentID)
        {
            //remove items
            _toRemove.ForEach(i => db.Delete(i));
            _toRemove.Clear();

            //save items
            _data.ForEach(i => i.SetParentID(parentID));
            _data.ForEach(i => db.Save(i));
        }


        private void SetValue(XtraDataField fld, int position, object value)
        {
            T item = GetItem(fld, position);

            if (value == null)
            {
                // if exists, remove it
                if (item != null)
                {
                    _toRemove.Add(item);
                    _data.Remove(item); //hash list
                }
            }
            else
            {
                var strValue = (string)Convert.ChangeType(value, typeof(string), System.Globalization.CultureInfo.InvariantCulture);                
                if (item == null)
                {
                    // add new item                
                    item = new T();
                    item.Set(fld.Code, position, strValue);
                    _data.Add(item);
                }
                else
                {
                    // update value
                    item.Set(fld.Code, position, strValue);
                }
            }
        }


        private object GetValue(XtraDataField fld, int position)
        {
            T item = GetItem(fld, position);

            if (item == null)
                return null;
            else
            {
                var strValue = item.GetValue();
                return Convert.ChangeType(strValue, fld.XtraType, System.Globalization.CultureInfo.InvariantCulture);
            }
        }


        private T GetItem(XtraDataField fld, int position)
        {
            return _data.Where(x =>
            {
                var id = x.GetIDs();
                return (fld.Code.Equals(id.Item1, StringComparison.InvariantCultureIgnoreCase) && position == id.Item2);
            })
            .SingleOrDefault();
        }
    }
}

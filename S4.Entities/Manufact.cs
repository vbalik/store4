using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_manufactlist")]
	[PrimaryKey("manufid", AutoIncrement = false)]
	[ExplicitColumns]
	public partial class Manufact
	{
		[Required]
		[MaxLength(16)]
		[Display(Name = "ID")]
		[Column("manufID")]
		public string ManufID {get; set;}

		[Required]
		[MaxLength(64)]
		[Display(Name = "Název výrobce")]
		[Column("manufName")]
		public string ManufName {get; set;}

		[Required]
		[MaxLength(16)]
		[Display(Name = "Stav")]
		[StatusSet("Statuses")]
		[Column("manufStatus")]
		public string ManufStatus {get; set;}

		[MaxLength(32)]
		[Display(Name = "ID zdroje")]
		[Column("source_id")]
		public string Source_id {get; set;}

		[Required]
		[Display(Name = "Šarže")]
		[Column("useBatch")]
		public bool UseBatch {get; set;}

		[Required]
		[Display(Name = "Expirace")]
		[Column("useExpiration")]
		public bool UseExpiration {get; set;}

		[MaxLength(16)]
		[Display(Name = "Sekce")]
		[Column("sectID")]
		public string SectID {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

		[MaxLength(32)]
		[Display(Name = "Ikona")]
		[Column("manufSign")]
		public string ManufSign {get; set;}

	}
}

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_storemove_lots")]
	[PrimaryKey("stomovelotid")]
	[ExplicitColumns]
	public partial class StoreMoveLot
	{
		[Required]
		[Column("stoMoveLotID")]
		public int StoMoveLotID {get; set;}

		[Required]
		[Column("artPackID")]
		public int ArtPackID {get; set;}
       
        [Column("udiCode")]
        public string UDICode { get; set; }

        [Required]
		[MaxLength(32)]
		[Column("batchNum")]
		public string _batchNum {get; set;}

		[Required]
		[Column("expirationDate")]
		public DateTime _expirationDate {get; set;}

	}
}

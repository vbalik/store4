using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_desclist")]
	[PrimaryKey("descfield,descvalue", AutoIncrement = false)]
	[ExplicitColumns]
	public partial class DescList
	{
		[Required]
		[MaxLength(32)]
		[Column("descField")]
		public string DescField {get; set;}

		[Required]
		[MaxLength(32)]
		[Column("descValue")]
		public string DescValue {get; set;}

		[Required]
		[Column("descText")]
		public string DescText {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

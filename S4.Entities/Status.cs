using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_statuslist")]
	[PrimaryKey("docclass", AutoIncrement = false)]
	[ExplicitColumns]
	public partial class Status
	{
		[Required]
		[MaxLength(32)]
		[Column("docClass")]
		public string DocClass {get; set;}

		[Required]
		[MaxLength(16)]
		[Column("docStatus")]
		public string DocStatus {get; set;}

		[Required]
		[MaxLength(128)]
		[Column("statDesc")]
		public string StatDesc {get; set;}

		[Required]
		[Column("statEnabled")]
		public bool StatEnabled {get; set;}

		[Required]
		[Column("statOnStock")]
		public bool StatOnStock {get; set;}

		[Required]
		[Column("statIsSaved")]
		public bool StatIsSaved {get; set;}

		[Required]
		[Column("statInProc")]
		public bool StatInProc {get; set;}

		[MaxLength(4)]
		[Column("statJobID")]
		public string StatJobID {get; set;}

		[Column("statSettings")]
		public string StatSettings {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Entities
{
    public partial class DirectionAssign
    {
        public const string JOB_ID_RECEIVE = "RECE";
        //public const string JOB_ID_REPACK = "REPA";
        public const string JOB_ID_STORE_IN = "STIN";
        //public const string JOB_ID_DIR_MOVE = "DIMO";
        public const string JOB_ID_DISPATCH = "DISP";
        public const string JOB_ID_DISPATCH_CHECK = "DICH";



        public static readonly Tuple<string, string[]>[] JOB_AND_STATUS_COMBINATION = new Tuple<string, string[]>[]
        {
            // receive
            new Tuple<string, string[]>(JOB_ID_RECEIVE, new string[] { Direction.DR_STATUS_STOIN_WAIT, Direction.DR_STATUS_STOIN_PROC } ),
            // store in
            new Tuple<string, string[]>(JOB_ID_STORE_IN, new string[] { Direction.DR_STATUS_STOIN_WAIT, Direction.DR_STATUS_STOIN_PROC } ),
            // dispatch
            new Tuple<string, string[]>(JOB_ID_DISPATCH, new string[] { Direction.DR_STATUS_DISPATCH_WAIT, Direction.DR_STATUS_DISPATCH_PROC } ),
            // dispatch check
            new Tuple<string, string[]>(JOB_ID_DISPATCH_CHECK, new string[] { Direction.DR_STATUS_DISP_CHECK_WAIT, Direction.DR_STATUS_DISP_CHECK_PROC } ),
        };
    }
}

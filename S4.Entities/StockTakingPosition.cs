using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_stocktaking_positions")]
	[PrimaryKey("stotakposid")]
	[ExplicitColumns]
	public partial class StockTakingPosition
	{
		[Required]
		[Column("stotakPosID")]
		public int StotakPosID {get; set;}

		[Required]
		[Column("stotakID")]
		public int StotakID {get; set;}

		[Required]
		[Column("positionID")]
		public int PositionID {get; set;}

		[Required]
		[Display(Name = "Počet kontrol")]
		[Column("checkCounter")]
		public int CheckCounter {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

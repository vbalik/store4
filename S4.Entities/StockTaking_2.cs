﻿using System;
using System.Collections.Generic;
using System.Text;

using S4.Entities.StatusEngine;

namespace S4.Entities
{
    public partial class StockTaking
    {
        public const string STOCKTAKING_STATUS_OPENED = "OPENED";
        public const string STOCKTAKING_STATUS_SAVED = "SAVED";
        public const string STOCKTAKING_STATUS_CANCELED = "CANCELED";

        public static StatusSet Statuses = new StatusSet(s =>
        {
            s.AddStatus(STOCKTAKING_STATUS_OPENED, "V běhu")
                .AddAction("Načíst stav z ABRY", ActionTypeEnum.Dialog, "loadABRAStatus_dialog")
                .AddAction("Uložit", ActionTypeEnum.ProcedureWithConfirmationValidation, "StockTakingSave")
                .AddAction("Zrušit", ActionTypeEnum.ProcedureWithConfirmationValidation, "StockTakingCancel")
                .SetColor(System.Drawing.Color.Blue);

            s.AddStatus(STOCKTAKING_STATUS_SAVED, "Ukončeno");

            s.AddStatus(STOCKTAKING_STATUS_CANCELED, "Zrušeno")
                .SetColor(System.Drawing.Color.Gray);

        });
    }
}

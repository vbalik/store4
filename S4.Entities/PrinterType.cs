using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_printertype")]
	[PrimaryKey("printertypeid")]
	[ExplicitColumns]
	public partial class PrinterType
	{
		public enum PrinterClassEnum
		{
			[Display(Name = "A4")]
			PrinterClassA4 = 0,
			[Display(Name = "Štítky - úzké")]
			PrinterBarcodeNarrow = 1,
			[Display(Name = "Štítky - široké")]
			PrinterBarcodeWide = 2,
		}

		[Required]
		[Column("printerTypeID")]
		public int PrinterTypeID {get; set;}

		[Required]
		[Display(Name = "Třída")]
		[Column("printerClass")]
		public PrinterClassEnum PrinterClass {get; set;}

		[Column("printerDesc")]
		public string PrinterDesc {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

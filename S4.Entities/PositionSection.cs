using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_positionlist_sections")]
	[PrimaryKey("possectid")]
	[ExplicitColumns]
	public partial class PositionSection
	{
		[Required]
		[Column("posSectID")]
		public int PosSectID {get; set;}

		[Required]
		[MaxLength(16)]
		[Column("sectID")]
		public string SectID {get; set;}

		[Required]
		[Column("positionID")]
		public int PositionID {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

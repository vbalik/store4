using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_internmessages")]
	[PrimaryKey("intmessid")]
	[ExplicitColumns]
	public partial class InternMessage
	{
		public enum MessageSeverityEnum
		{
			Low = 0,
			High = 10,
			Error = 20,
		}

		[Required]
		[Column("intMessID")]
		public int IntMessID {get; set;}

		[Required]
		[MaxLength(32)]
		[Display(Name = "Typ")]
		[StatusSet("Statuses")]
		[Column("msgClass")]
		public string MsgClass {get; set;}

		[Required]
		[Display(Name = "Důležitost")]
		[Column("msgSeverity")]
		public MessageSeverityEnum MsgSeverity {get; set;}

		[Required]
		[Display(Name = "Hlavička")]
		[Column("msgHeader")]
		public string MsgHeader {get; set;}

		[Column("msgText")]
		public string MsgText {get; set;}

		[Required]
		[Display(Name = "Datum")]
		[Column("msgDateTime")]
		public DateTime MsgDateTime {get; set;}

		[MaxLength(16)]
		[Column("msgUserID")]
		public string MsgUserID {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

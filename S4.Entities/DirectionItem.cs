using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_direction_items")]
	[PrimaryKey("directionitemid")]
	[ExplicitColumns]
	public partial class DirectionItem
	{
		[Required]
		[Display(Name = "ID")]
		[Column("directionItemID")]
		public int DirectionItemID {get; set;}

		[Required]
		[Column("directionID")]
		public int DirectionID {get; set;}

		[Required]
		[Display(Name = "Poz.")]
		[Column("docPosition")]
		public int DocPosition {get; set;}

		[Required]
		[MaxLength(16)]
		[Display(Name = "Stav")]
		[StatusSet("Statuses")]
		[Column("itemStatus")]
		public string ItemStatus {get; set;}

		[Required]
		[Display(Name = "Balení")]
		[Column("artPackID")]
		public int ArtPackID {get; set;}

		[Required]
		[MaxLength(64)]
		[Display(Name = "Kód")]
		[Column("articleCode")]
		public string ArticleCode {get; set;}

		[MaxLength(32)]
		[Display(Name = "Jedn.")]
		[Column("unitDesc")]
		public string UnitDesc {get; set;}

		[Required]
		[Display(Name = "Množství")]
		[Column("quantity")]
		public decimal Quantity {get; set;}

		[Required]
		[Display(Name = "Cena")]
		[Column("unitPrice")]
		public decimal UnitPrice {get; set;}

		[Display(Name = "Oddělení")]
		[Column("partnerDepart")]
		public string PartnerDepart {get; set;}

		[Required]
		[Display(Name = "Zadáno")]
		[Column("entryDateTime")]
		public DateTime EntryDateTime {get; set;}

		[Required]
		[MaxLength(16)]
		[Display(Name = "Uživatel")]
		[Column("entryUserID")]
		public string EntryUserID {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

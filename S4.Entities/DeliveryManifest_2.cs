﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Drawing;

using NPoco;

using S4.Entities.Attributes;
using S4.Entities.StatusEngine;

namespace S4.Entities
{
    public partial class DeliveryManifest
    {
        public const string STATUS_NONE = "NONE";
        public const string STATUS_WAIT = "WAIT";
        public const string STATUS_ERROR = "ERROR";
        public const string STATUS_DONE = "DONE";
        public static StatusSet Statuses = new StatusSet(s =>
        {
            s.AddStatus(STATUS_NONE, "Neznámý").SetColor(Color.Gray);
            s.AddStatus(STATUS_WAIT, "Čeká na zpracování").SetColor(Color.Blue);
            s.AddStatus(STATUS_ERROR, "Chyba").SetColor(Color.Violet);
            s.AddStatus(STATUS_DONE, "OK")
                .AddAction(new ActionDef()
                {
                    Name = "Vyisknout manifest",
                    ActionType = ActionTypeEnum.Dialog,
                    ActionProcedure = "print_manifest_dialog"
                });
        });
    }
}

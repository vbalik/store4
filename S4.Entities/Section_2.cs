﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Entities
{
    public partial class Section
    {
        [Core.DBTestIgnore]
        public bool IsNew { get; set; }
    }
}

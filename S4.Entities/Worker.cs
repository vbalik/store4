using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_workerlist")]
	[PrimaryKey("workerid", AutoIncrement = false)]
	[ExplicitColumns]
	public partial class Worker
	{
		public enum WorkerClassEnum
		{
			[Display(Name = "Nic")]
			Any = 0,
			[Display(Name = "Vyskladnění")]
			CanDispatch = 1,
			[Display(Name = "Naskladnění")]
			CanReceive = 2,
			[Display(Name = "Dispečer")]
			CanManage = 4,
			[Display(Name = "Přeskladnění")]
			CanMove = 8,
			[Display(Name = "Inventura")]
			CanStoIn = 16,
			[Display(Name = "Seznam pracovníků")]
			CanEditUsers = 32,
			[Display(Name = "Přehledy")]
			CanViewReports = 64,
			[Display(Name = "Léky")]
			CanDispatchMedicines = 128,
			[Display(Name = "Cytostatika")]
			CanDispatchCytostatics = 256,
			[Display(Name = "Správce")]
			IsSuperUser = 512,
			[Display(Name = "Report práce skladníků")]
			CanPrintWorkerReport = 1024,
			[Display(Name = "Inventura - načtení původního stavu")]
			CanStoReadOrigState = 2048,
		}

		[Required]
		[MaxLength(16)]
		[Display(Name = "ID")]
		[Column("workerID")]
		public string WorkerID {get; set;}

		[Required]
		[MaxLength(256)]
		[Display(Name = "Jméno")]
		[Column("workerName")]
		public string WorkerName {get; set;}

		[Required]
		[Display(Name = "Zařazení")]
		[Column("workerClass")]
		public WorkerClassEnum WorkerClass {get; set;}

		[Required]
		[MaxLength(16)]
		[Display(Name = "Stav")]
		[StatusSet("Statuses")]
		[Column("workerStatus")]
		public string WorkerStatus {get; set;}

		[Required]
		[MaxLength(32)]
		[Display(Name = "Přihlášení")]
		[Column("workerLogon")]
		public string WorkerLogon {get; set;}

		[Required]
		[MaxLength(256)]
		[Column("workerSecData")]
		public string WorkerSecData {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

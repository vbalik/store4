﻿using S4.Entities.StatusEngine;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Entities
{
    public partial class RemoteAction
    {
        public const string RA_STATUS_NEW = "NEW";
        public const string RA_STATUS_DONE = "DONE";
        public const string RA_STATUS_ERROR = "ERROR";


        public static StatusSet Statuses = new StatusSet(s =>
        {
            // store in
            s.AddStatus(RA_STATUS_NEW, "Přijato");
            s.AddStatus(RA_STATUS_DONE, "Dokončeno");
            s.AddStatus(RA_STATUS_ERROR, "Chyba").SetColor(System.Drawing.Color.Red); 
        });
    }
}

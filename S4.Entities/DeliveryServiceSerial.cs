using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_deliveryservice_series")]
	[PrimaryKey("deliveryserviceseriesid")]
	[ExplicitColumns]
	public partial class DeliveryServiceSerial
	{
		[Required]
		[Display(Name = "ID")]
		[Column("deliveryServiceSeriesID")]
		public int DeliveryServiceSeriesID {get; set;}

		[Required]
		[Display(Name = "Dopravce")]
		[Column("deliveryPartnerName")]
		public string DeliveryPartnerName {get; set;}

		[Display(Name = "Typ")]
		[Column("productType")]
		public string ProductType {get; set;}

		[Required]
		[Display(Name = "Poslední použité číslo")]
		[Column("currentNumber")]
		public long CurrentNumber {get; set;}

		[Required]
		[Display(Name = "Řada od")]
		[Column("seriesFrom")]
		public long SeriesFrom {get; set;}

		[Required]
		[Display(Name = "Řada do")]
		[Column("seriesTo")]
		public long SeriesTo {get; set;}

		[Required]
		[Display(Name = "Stav")]
		[Column("status")]
		public string Status {get; set;}

		[Display(Name = "Datum a čas vytvoření")]
		[Column("seriesCreated")]
		public DateTime? SeriesCreated {get; set;}

		[Display(Name = "Datum a čas změny")]
		[Column("seriesChanged")]
		public DateTime? SeriesChanged {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

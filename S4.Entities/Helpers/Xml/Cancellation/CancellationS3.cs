﻿using N7.Entities.Helpers.Xml;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace S4.Entities.Helpers.Xml.Cancellation
{
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", ElementName = "s3_cancellation", IsNullable = false)]
    public class CancellationS3
    {
        [XmlAttribute("doc_prefix")]
        public string DocPrefix { get; set; }

        [XmlAttribute("doc_number")]
        public int DocNumber { get; set; }
        
        [XmlElement("doc_date", DataType = "date")]
        public System.DateTime DocDate { get; set; }

        #region serializer

        public static SerializerBase<CancellationS3> Serializer = new SerializerBase<CancellationS3>();
        
        #endregion
    }

}
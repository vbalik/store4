﻿using N7.Entities.Helpers.Xml;
using System.Xml.Serialization;
namespace S4.Entities.Helpers.Xml.Article
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", ElementName = "s3_articleAdrExtradata", IsNullable = false)]
    public class ArticleExtradataS3Adr : IAdrXtraDataS3
    {

        [XmlElement("un_kod")]
        public string UnKod { get; set; }

        [XmlElement("oficialni_nazev")]
        public string OficialniNazev { get; set; }

        [XmlElement("bezpecnosti_znacka")]
        public string BezpecnostiZnacka { get; set; }

        [XmlElement("obalova_skupina")]
        public string ObalovaSkupina { get; set; }

        [XmlElement("kod_tunelu")]
        public string KodTunelu { get; set; }

        [XmlElement("druh_obalu")]
        public string DruhObalu { get; set; }

        [XmlElement("prepravni_kategorie")]
        public string PrepravniKategorie { get; set; }

        [XmlElement("ohrozuje_prostredi")]
        public bool? OhrozujeProstredi { get; set; }

        [XmlElement("objem_koef")]
        public string ObjemovyKoeficient { get; set; }
        [XmlElement("merna_jednotka")]
        public string MernaJednotka { get; set; }

        #region serializer

        public static SerializerBase<ArticleExtradataS3Adr> Serializer = new SerializerBase<ArticleExtradataS3Adr>();

        #endregion


    }


}

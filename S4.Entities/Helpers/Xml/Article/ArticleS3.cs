﻿using N7.Entities.Helpers.Xml;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace S4.Entities.Helpers.Xml.Article
{
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", ElementName = "s3_article", IsNullable = false)]
    public class ArticleS3
    {
                
        [XmlElement("article_code")]
        public string ArticleCode { get; set; }

        [XmlElement("article_name")]
        public string ArticleName { get; set; }

        [XmlElement("article_unit")]
        public string ArticleUnit { get; set; }

        [XmlElement("article_manuf")]
        public string ArticleManuf { get; set; }

        [XmlElement("article_use_batch")]
        public bool ArticleUseBatch { get; set; }

        [XmlElement("article_use_expiration")]
        public bool ArticleUseExpiration { get; set; }

        [XmlElement("article_brandname")]
        public string ArticleBrandname { get; set; }

        [XmlElement("article_packing")]
        public List<ArticlePackingS3> ArticlePacking { get; set; }
        
        [XmlAttribute("article_id")]
        public string ArticleID { get; set; }

        [XmlElement("article_status")]
        public string ArticleStatus { get; set; }

        [XmlElement("article_cooled")]
        public bool? ArticleCooled { get; set; }

        [XmlElement("article_medicines")]
        public bool? ArticleMedicines { get; set; }

        [XmlElement("article_cytostatics")]
        public bool? ArticleCytostatics { get; set; }

        [XmlElement("article_do_check_on_receive")]
        public bool? ArticleCheckOnReceive { get; set; }

        [XmlElement("adr")]
        public ArticleExtradataS3Adr Adr { get; set; }


        #region serializer

        public static SerializerBase<ArticleS3> Serializer = new SerializerBase<ArticleS3>();

        #endregion
    }

}
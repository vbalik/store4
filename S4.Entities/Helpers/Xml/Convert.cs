﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Entities.Helpers.Xml
{
    public class Convert
    {
        public decimal StringToDecimal(string value)
        {
            if (decimal.TryParse(value, out decimal d))
                return d;
            else
            {
                value = value.Replace(".", ",");
                return System.Convert.ToDecimal(value, new System.Globalization.CultureInfo("cs-CZ"));
            }
                
        }
    }
}

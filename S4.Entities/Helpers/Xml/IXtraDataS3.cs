﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Entities.Helpers.Xml
{
    public interface IAdrXtraDataS3
    {
        string UnKod { get; set; }
        string OficialniNazev { get; set; }
        string BezpecnostiZnacka { get; set; }
        string ObalovaSkupina { get; set; }
        string KodTunelu { get; set; }
        string DruhObalu { get; set; }
        string PrepravniKategorie { get; set; }
        bool? OhrozujeProstredi { get; set; }
        string ObjemovyKoeficient { get; set; }
        string MernaJednotka { get; set; }
    }
}

﻿using S4.Entities.Helpers.Xml.Article;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace S4.Entities.Helpers.Xml.Receive
{
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", ElementName = "s3_receiveItemsItem", IsNullable = false)]
    public class ReceiveItemsS3
    {

        [XmlElement("article")]
        public ReceiveArticleS3 Article { get; set; }

        [XmlElement("quantity")]
        public int Quantity { get; set; }

        [XmlElement("position")]
        public int Position { get; set; }

        [XmlAttribute("article_packing_id")]
        public string ArticlePackingID { get; set; }
    }
}

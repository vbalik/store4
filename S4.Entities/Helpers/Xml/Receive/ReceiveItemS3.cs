﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace S4.Entities.Helpers.Xml.Receive
{
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", ElementName = "s3_receiveItems", IsNullable = false)]
    public class ReceiveItemS3
    {

        [XmlAttribute("item")]
        public ReceiveItemsS3 Item { get; set; }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace S4.Entities.Helpers.Xml.Receive
{
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", ElementName = "s3_receiveItemsItemArticleArticle_packing", IsNullable = false)]
    public class ReceiveArticlePackingS3
    {

        [XmlElement("packing_name")]
        public string PackingName { get; set; }

        [XmlElement("packing_relation")]
        public string PackingRelation { get; set; }

        [XmlIgnore]
        public decimal PackingRelationDec
        {
            get
            {
                Convert convert = new Convert();
                if (PackingRelation == null)
                    return 0;
                else
                    return convert.StringToDecimal(PackingRelation);
            }
        }

        [XmlElement("packing_bar_cod")]
        public string PackingBarCode { get; set; }

        [XmlAttribute("article_packing_id")]
        public string ArticlePackingID { get; set; }
    }
}

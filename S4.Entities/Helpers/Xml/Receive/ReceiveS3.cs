﻿using N7.Entities.Helpers.Xml;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace S4.Entities.Helpers.Xml.Receive
{

    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", ElementName = "s3_receive", IsNullable = false)]
    public class ReceiveS3
    {

        [XmlElement("doc_date", DataType = "date")]
        public System.DateTime DocDate { get; set; }

        [XmlElement("doc_reference")]
        public string DocReference { get; set; }

        [XmlElement("doc_remark")]
        public string DocRemark { get; set; }

        [XmlArray("items")]
        [XmlArrayItem("item", IsNullable = false, Type = typeof(ReceiveItemsS3))]
        public List<ReceiveItemsS3> Items { get; set; }

        [XmlAttribute("doc_prefix")]
        public string DocPrefix { get; set; }

        [XmlAttribute("doc_number")]
        public uint DocNumber { get; set; }

        #region serializer

        public static SerializerBase<ReceiveS3> Serializer = new SerializerBase<ReceiveS3>();

        #endregion


    }


}
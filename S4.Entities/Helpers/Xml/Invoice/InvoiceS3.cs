﻿
// NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
using N7.Entities.Helpers.Xml;
using S4.Entities.Helpers.Xml.Dispatch;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Xml.Serialization;


namespace S4.Entities.Helpers.Xml.Invoice
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", ElementName = "s3_invoice", IsNullable = false)]
    public class InvoiceS3
    {
        #region XmlAttribute
                
        [XmlAttribute("doc_number")]
        public int DocNumber { get; set; }

        [XmlAttribute("doc_prefix")]
        public string DocPrefix { get; set; }

        #endregion

        #region XmlElement

        [XmlElement("payment_reference")]
        public string PaymentReference { get; set; }
        
        [XmlElement("doc_date", DataType = "date")]
        public System.DateTime DocDate { get; set; }

        [XmlElement("payment_type")]
        public string PaymentType { get; set; }

        [XmlElement("cash_on_delivery")]
        public decimal CashOnDelivery { get; set; }

        [XmlElement("transportation_type")]
        public string TransportationType { get; set; }

        [XmlElement("currency")]
        public string Currency { get; set; }

        [XmlElement("country")]
        public string Country { get; set; }

        [XmlArray("dispatch_docs")]
        [XmlArrayItem("s3_dispatch", IsNullable = false, Type = typeof(DispatchS3))]
        public List<DispatchS3> DispatchDocs { get; set; }

        #endregion


        #region serializer

        public static SerializerBase<InvoiceS3> Serializer = new SerializerBase<InvoiceS3>();

        #endregion


    }
}




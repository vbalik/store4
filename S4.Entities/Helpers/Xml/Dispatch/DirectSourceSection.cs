﻿using N7.Entities.Helpers.Xml;
using System.Xml.Serialization;


namespace S4.Entities.Helpers.Xml.Dispatch
{
    
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", ElementName = "s3_dispatchItemDirect_source_section", IsNullable = false)]
    public class DirectSourceSection
    {
        [XmlAttribute("number")]
        public int Number { get; set; }

        [XmlText]
        public string Value { get; set; }
    }

}
﻿using N7.Entities.Helpers.Xml;
using System.Xml.Serialization;

namespace S4.Entities.Helpers.Xml.Dispatch
{
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", ElementName = "s3_dispatchItem", IsNullable = false)]
    public class DispatchItemS3
    {

        [XmlElement("quantity")]
        public int Quantity { get; set; }

        [XmlElement("department")]
        public string Department { get; set; }

        [XmlElement("extradata")]
        public DispatchItemExtradataS3 Extradata { get; set; }

        [XmlAttribute("position")]
        public int Position { get; set; }

        [XmlAttribute("article_packing_id")]
        public string ArticlePackingID { get; set; }

        [XmlElement("direct_source_section")]
        public DirectSourceSection[] DirectSourceSection { get; set; }

        [XmlElement("alter_source_section")]
        public string AlterSourceSection { get; set; }

        [XmlElement("required_batch_number")]
        public string RequiredBatchNumber { get; set; }

        #region serializer

        public static SerializerBase<DispatchItemS3> Serializer = new SerializerBase<DispatchItemS3>();

        #endregion


    }

}

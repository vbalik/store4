﻿using N7.Entities.Helpers.Xml;
using System.Xml.Serialization;

namespace S4.Entities.Helpers.Xml.Dispatch
{

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", ElementName = "s3_dispatchDelivery_address", IsNullable = false)]
    public class DispatchDeliveryAddressS3
    {

        [XmlElement("name")]
        public string Name { get; set; }

        [XmlElement("adress_1")]
        public string Adress1 { get; set; }

        [XmlElement("adress_2")]
        public string Adress2 { get; set; }

        [XmlElement("city")]
        public string City { get; set; }

        [XmlElement("phone")]
        public string Phone { get; set; }

        [XmlElement("deliveryinstructions")]
        public string Deliveryinstructions { get; set; }

        [XmlElement("remark")]
        public string Remark { get; set; }

        [XmlElement("receiving_person")]
        public string ReceivingPerson { get; set; }

        [XmlAttribute("address_id")]
        public string AddressID { get; set; }


        #region serializer

        public static SerializerBase<DispatchDeliveryAddressS3> Serializer = new SerializerBase<DispatchDeliveryAddressS3>();

        #endregion

    }

}

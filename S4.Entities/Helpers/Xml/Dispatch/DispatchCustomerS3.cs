﻿using N7.Entities.Helpers.Xml;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace S4.Entities.Helpers.Xml.Dispatch
{
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", ElementName = "s3_customer", IsNullable = false)]
    public class DispatchCustomerS3
    {

        [XmlElement("name")]
        public string Name { get; set; }

        [XmlElement("adress_1")]
        public string Adress1 { get; set; }

        [XmlElement("adress_2")]
        public string Adress2 { get; set; }

        [XmlElement("city")]
        public string City { get; set; }

        [XmlElement("phone")]
        public string Phone { get; set; }

        [XmlElement("extradata")]
        public DispatchCustomerExtradataS3 Extradata { get; set; }

        [XmlAttribute("customer_id")]
        public string CustomerID { get; set; }

        [XmlElement("orgidentnumber")]
        public string OrgIdentNumber { get; set; }
        
        [XmlArray("delivery_addresses")]
        [XmlArrayItem("delivery_address", IsNullable = true, Type = typeof(DispatchDeliveryAddressS3))]
        public List<DispatchDeliveryAddressS3> DeliveryAddresses { get; set; }
        
        #region serializer

        public static SerializerBase<DispatchCustomerS3> Serializer = new SerializerBase<DispatchCustomerS3>();

        #endregion


    }

}

﻿using N7.Entities.Helpers.Xml;
using System.Xml.Serialization;

namespace S4.Entities.Helpers.Xml.Dispatch
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", ElementName = "s3_dispatchCustomerExtradata", IsNullable = false)]
    public class DispatchCustomerExtradataS3
    {

        [XmlElement("formatDiskety")]
        public string FormatDiskety { get; set; }

        [XmlElement("poslatMailem")]
        public string PoslatMailem { get; set; }

        [XmlElement("mailovaAdresa")]
        public string MailovaAdresa { get; set; }

        [XmlElement("ico")]
        public string ICO { get; set; }

        [XmlElement("dic")]
        public string DIC { get; set; }

        [XmlElement("poslatDisketuSouhrne")]
        public string PoslatDisketuSouhrnne { get; set; }


        #region serializer

        public static SerializerBase<DispatchCustomerExtradataS3> Serializer = new SerializerBase<DispatchCustomerExtradataS3>();

        #endregion


    }

}

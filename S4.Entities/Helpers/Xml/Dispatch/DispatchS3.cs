﻿
// NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
using N7.Entities.Helpers.Xml;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Xml.Serialization;


namespace S4.Entities.Helpers.Xml.Dispatch
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", ElementName = "s3_dispatch", IsNullable = false)]
    public class DispatchS3
    {
        [XmlElement("extnumber")]
        public string ExtNumber { get; set; }

        [XmlElement("doc_barcode")]
        public string DocBarcode { get; set; }

        [XmlElement("doc_date", DataType = "date")]
        public System.DateTime DocDate { get; set; }
                
        [XmlElement("doc_reference")]
        public string DocReference { get; set; }

        [XmlElement("invoice_number")]
        public string InvoiceNumber { get; set; }

        [XmlElement("payment_reference")]
        public string PaymentReference { get; set; }

        [XmlElement("transportation_type")]
        public string TransportationType { get; set; }

        [XmlElement("payment_type")]
        public string PaymentType { get; set; }

        [XmlElement("doc_remark")]
        public string DocRemark { get; set; }

        [XmlElement("mandatory_direction_print")]
        public string MandatoryDirectionPrint { get; set; }

        [XmlElement("soucetCenaSDPH")]
        public string SoucetCenaSDPH { get; set; }

        [XmlIgnore]
        public decimal SoucetCenaSDPHDec
        {
            get
            {
                Convert convert = new Convert();
                if (SoucetCenaSDPH == null)
                    return 0;
                else
                    return convert.StringToDecimal(SoucetCenaSDPH);
            }
        }

        [XmlElement("soucetCenaBezDPH")]
        public string SoucetCenaBezDPH { get; set; }

        [XmlIgnore]
        public decimal SoucetCenaBezDPHDec
        {
            get
            {
                Convert convert = new Convert();
                if (SoucetCenaBezDPH == null)
                    return 0;
                else
                    return convert.StringToDecimal(SoucetCenaBezDPH);
            }
        }

        [XmlElement("soucetCenaSDPHSnizena")]
        public string SoucetCenaSDPHSnizena { get; set; }

        [XmlIgnore]
        public decimal SoucetCenaSDPHSnizenaDec
        {
            get
            {
                Convert convert = new Convert();
                if (SoucetCenaSDPHSnizena == null)
                    return 0;
                else
                    return convert.StringToDecimal(SoucetCenaSDPHSnizena);
            }
        }

        [XmlElement("soucetCenaBezDPHSnizena")]
        public string SoucetCenaBezDPHSnizena { get; set; }

        [XmlIgnore]
        public decimal SoucetCenaBezDPHSnizenaDec
        {
            get
            {
                Convert convert = new Convert();
                if (SoucetCenaBezDPHSnizena == null)
                    return 0;
                else
                    return convert.StringToDecimal(SoucetCenaBezDPHSnizena);
            }
        }

        [XmlElement("soucetCenaSDPHZakladni")]
        public string SoucetCenaSDPHZakladni { get; set; }

        [XmlIgnore]
        public decimal SoucetCenaSDPHZakladniDec
        {
            get
            {
                Convert convert = new Convert();
                if (SoucetCenaSDPHZakladni == null)
                    return 0;
                else
                    return convert.StringToDecimal(SoucetCenaSDPHZakladni);
            }
        }

        [XmlElement("soucetCenaBezDPHZakladni")]
        public string SoucetCenaBezDPHZakladni { get; set; }

        [XmlIgnore]
        public decimal SoucetCenaBezDPHZakladniDec
        {
            get
            {
                Convert convert = new Convert();
                if (SoucetCenaBezDPHZakladni == null)
                    return 0;
                else
                    return convert.StringToDecimal(SoucetCenaBezDPHZakladni);
            }
        }

        [XmlElement("customer", Type = typeof(DispatchCustomerS3))]
        public DispatchCustomerS3 Customer { get; set; }

        [XmlElement("delivery_address")]
        public DispatchDeliveryAddressS3 DeliveryAddress { get; set; }

        [XmlArray("items")]
        [XmlArrayItem("item", IsNullable = false, Type = typeof(DispatchItemS3))]
        public List<DispatchItemS3> Items { get; set; }

        [XmlAttribute("doc_prefix")]
        public string DocPrefix { get; set; }

        [XmlAttribute("doc_number")]
        public int DocNumber { get; set; }

        [XmlElement("cash_on_delivery")]
        public decimal CashOnDelivery { get; set; }

        [XmlElement("country")]
        public string Country { get; set; }

        [XmlElement("currency")]
        public string Currency { get; set; }

        [XmlElement("parcel_shop_code")]
        public string ParcelShopCode { get; set; }

        [XmlElement("shipmentinformation")]
        public string Shipmentinformation { get; set; }

        #region serializer

        public static SerializerBase<DispatchS3> Serializer = new SerializerBase<DispatchS3>();

        #endregion


    }
}




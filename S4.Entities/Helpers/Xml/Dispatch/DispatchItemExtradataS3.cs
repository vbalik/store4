﻿using N7.Entities.Helpers.Xml;
using System.Xml.Serialization;
namespace S4.Entities.Helpers.Xml.Dispatch
{


    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", ElementName = "s3_dispatchItemExtradata", IsNullable = false)]
    public class DispatchItemExtradataS3
    {

        [XmlElement("skupinaZbozi")]
        public string SkupinaZbozi { get; set; }

        [XmlElement("kod")]
        public string ArticleCode { get; set; }

        [XmlElement("kodVZP")]
        public string KodVZP { get; set; }

        [XmlElement("kodPDK")]
        public string KodPDK { get; set; }

        [XmlElement("kodSUKL")]
        public string KodSUKL { get; set; }

        [XmlElement("sazbaDPH")]
        public int SazbaDPH { get; set; }

        [XmlElement("vyrobniCena")]
        public string VyrobniCena { get; set; }

        [XmlIgnore]
        public decimal VyrobniCenaDec
        {
            get
            {
                Convert convert = new Convert();
                if (VyrobniCena == null)
                    return 0;
                else
                    return convert.StringToDecimal(VyrobniCena);
            }
        }

        [XmlElement("cenaBezDPH")]
        public string CenaBezDPH { get; set; }

        [XmlIgnore]
        public decimal CenaBezDPHDec
        {
            get
            {
                Convert convert = new Convert();
                if (CenaBezDPH == null)
                    return 0;
                else
                    return convert.StringToDecimal(CenaBezDPH);
            }
        }

        [XmlElement("cenaSDPH")]
        public string CenaSDPH { get; set; }

        [XmlIgnore]
        public decimal CenaSDPHDec
        {
            get
            {
                Convert convert = new Convert();
                if (CenaSDPH == null)
                    return 0;
                else
                    return convert.StringToDecimal(CenaSDPH);
            }
        }

        [XmlElement("prodejniCenaBezDPH")]
        public string ProdejniCenaBezDPH { get; set; }
        
        [XmlIgnore]
        public decimal ProdejniCenaBezDPHDec
        {
            get
            {
                Convert convert = new Convert();
                if (ProdejniCenaBezDPH == null)
                    return 0;
                else
                    return convert.StringToDecimal(ProdejniCenaBezDPH);
            }
        }

        [XmlElement("prodejniCenaSDPH")]
        public string ProdejniCenaSDPH { get; set; }

        [XmlIgnore]
        public decimal ProdejniCenaSDPHDec
        {
            get
            {
                Convert convert = new Convert();
                if (ProdejniCenaSDPH == null)
                    return 0;
                else
                    return convert.StringToDecimal(ProdejniCenaSDPH);
            }
        }

        [XmlElement("marzeDistrib")]
        public int MarzeDistrib { get; set; }

        [XmlElement("marzeDistribSpecified")]
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool MarzeDistribSpecified { get; set; }

        [XmlElement("article_medicines")]
        public string ArticleMedicines { get; set; }

        [XmlElement("article_cooled")]
        public string ArticleCooled { get; set; }

        [XmlElement("obsahujeOchrannyKod")]
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ObsahujeOchrannyKod { get; set; }

        [XmlElement("providerow_id")]
        public string ProvideRowID { get; set; }

        [XmlElement("risk_class")]
        public string RiskClass { get; set; }

        [XmlElement("udidi")]
        public string Udidi { get; set; }

        [XmlElement("adr")]
        public DispatchItemExtradataS3Adr Adr { get; set; }

        [XmlElement("distributionSurcharge")]
        public string DistributionSurcharge { get; set; }
                
        #region serializer

        public static SerializerBase<DispatchItemExtradataS3> Serializer = new SerializerBase<DispatchItemExtradataS3>();

        #endregion


    }


}

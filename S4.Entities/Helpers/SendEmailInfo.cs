﻿using System;

namespace S4.Entities.Helpers
{
    public class SendEmailInfo
    {
        public string MessageFrom { get; set; }
        public string MessageTo { get; set; }
        public string MessageSubject { get; set; }
        public string MessageBody { get; set; }
        public string[] MessageAttachmentNames { get; set; } = Array.Empty<string>();
        public int MessageAttachmentCount => MessageAttachmentNames.Length;

        public SendEmailInfo(string messageFrom, string messageTo, string messageSubject, string messageBody, string[] messageAttachmentNames)
        {
            MessageFrom = messageFrom;
            MessageTo = messageTo;
            MessageSubject = messageSubject;
            MessageBody = messageBody;
            MessageAttachmentNames = messageAttachmentNames;
        }
    }
}

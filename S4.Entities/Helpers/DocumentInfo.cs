﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Entities.Helpers
{
    public class DocumentInfo
    {
        public int DocumentID { get; set; }
        public string DocumentName { get; set; }
        public string DocumentDetail { get; set; }
        public string DocumentDetail2 { get; set; }
        public string DocumentDetail3 { get; set; }
        public int StoMoveID { get; set; }
        public Position DispatchPosition { get; set; }
    }
}

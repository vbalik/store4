﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Entities.Helpers
{
    public class ArticleInfo
    {
        public int ArticleID { get; set; }
        public int ArtPackID { get; set; }
        public string ArticleCode { get; set; }
        public string ArticleDesc { get; set; }
        public string PackDesc { get; set; }
       
        public decimal Quantity { get; set; } = Decimal.MinValue;
        public string BatchNum { get; set; } = null;
        public DateTime? ExpirationDate { get; set; }
        public string CarrierNum { get; set; } = null;
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Entities.Helpers
{
    public class SettingMail
    {
        [JsonProperty(PropertyName= "mailServer")]
        public string MailServer { get; set; }

        [JsonProperty(PropertyName = "mailFrom")]
        public string MailFrom { get; set; }

        [JsonProperty(PropertyName = "mailTo")]
        public string MailTo { get; set; }

        [JsonProperty(PropertyName = "mailUzivatel")]
        public string MailUzivatel { get; set; }

        [JsonProperty(PropertyName = "mailHeslo")]
        public string MailHeslo { get; set; }
    }
}

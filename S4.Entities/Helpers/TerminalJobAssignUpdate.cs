﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Entities.Helpers
{
    public class TerminalJobAssignUpdate
    {
        public string WorkerID { get; set; }
        public List<TerminalJobAssignItem> Items { get; set; }                
    }
}

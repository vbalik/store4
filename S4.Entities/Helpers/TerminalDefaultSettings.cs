﻿using Newtonsoft.Json;

namespace S4.Entities.Helpers
{
    public class TerminalDefaultSettings
    {
        [JsonProperty(PropertyName = "enableAssignDirectionUser")]
        public bool EnableAssignDirectionUser { get; set; }
        [JsonProperty(PropertyName = "barcodeDisallowedValuePattern")]
        public string BarcodeDisallowedValuePattern { get; set; }
        [JsonProperty(PropertyName = "dispatchDefaultPositionID")]
        public int DispatchDefaultPositionID { get; set; }
        [JsonProperty(PropertyName = "showSpecFeaturesAlertInfoList")]
        public bool ShowSpecFeaturesAlertInfoList { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Entities.Helpers
{
    public class TerminalConnectionInfo
    {
        public TerminalConnectionInfo(string connectionId)
        {
            ConnectionId = connectionId;
            ConnectionStart = DateTime.Now;
        }

        public string ConnectionId { get; private set; }
        public string WorkerID { get; set; }
        public string ClientVersion { get; set; }
        public string TerminalType { get; set; }
        public DateTime ConnectionStart { get; private set; }

    }
}

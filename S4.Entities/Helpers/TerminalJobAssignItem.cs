﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Entities.Helpers
{
    public class TerminalJobAssignItem
    {
        public TerminalJobAssignItem(string jobID, int cntDocs, int cntItems)
        {
            JobID = jobID;
            CntDocs = cntDocs;
            CntItems = cntItems;
        }

        public string JobID { get; set; }
        public int CntDocs { get; set; }
        public int CntItems { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace S4.Entities.Helpers
{
    public static class XmlHelper
    {

        /// <summary>
        /// Get string from XML file (convert 1250 to UTF8)
        /// </summary>
        /// <param name="xmlPath"></param>
        /// <returns>UTF8 string</returns>
        public static string GetStringFromXMLFile(string xmlPath)
        {
            var numberOfRetries = 5;
            int delayOnRetry = 1000;
            var xmlString = "";
            IOException lastIOException = null;

            for (int i = 1; i <= numberOfRetries; ++i)
            {
                try
                {
                    xmlString = GetStringFromFile(xmlPath);
                    lastIOException = null;
                    break; 
                }
                catch (IOException ex) when (i <= numberOfRetries)
                {
                    lastIOException = ex;
                    System.Threading.Thread.Sleep(delayOnRetry);
                }
            }

            if (lastIOException != null)
                throw lastIOException;

            return xmlString;
        }

        /// <summary>
        /// Get string from XML string (convert 1250 to UTF8)
        /// </summary>
        /// <param name="xmlData"></param>
        /// <returns>UTF8 string</returns>
        public static string GetStringFromXMLString(string xmlData)
        {
            //encoding 1250 ---> UTF8
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            var xmlBytes = Encoding.ASCII.GetBytes(xmlData);
            var encoding1250 = Encoding.GetEncoding("Windows-1250");
            var encodingBytes = Encoding.Convert(encoding1250, Encoding.UTF8, xmlBytes);
            return Encoding.UTF8.GetString(encodingBytes);
        }

        private static string GetStringFromFile(string xmlPath)
        {
            //encoding 1250 ---> UTF8
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            var xmlBytes = System.IO.File.ReadAllBytes(xmlPath);
            var encoding1250 = Encoding.GetEncoding("Windows-1250");
            var encodingBytes = Encoding.Convert(encoding1250, Encoding.UTF8, xmlBytes);
            return Encoding.UTF8.GetString(encodingBytes);
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace S4.Entities.Helpers
{
    public static class PocoExtension
    {
        private static object _lock = new object();
        public static string[] GetKeyColumns(this Type source)
        {
            lock (_lock)
            {
                if (source.GetCustomAttributes(true).OfType<NPoco.PrimaryKeyAttribute>().Count() == 0)
                    return new string[] { };

                var keyStr = NPoco.TableInfo.FromPoco(source).PrimaryKey;
                if (string.IsNullOrEmpty(keyStr))
                    return new string[] { };

                var keysFields = keyStr.Split(',');
                for (int i = 0; i < keysFields.Length; i++)
                {
                    var propName = source.GetProperties().Where(p => p.Name.ToUpper() == keysFields[i].ToUpper()).First().Name;
                    keysFields[i] = propName[0].ToString().ToLower() + propName.Substring(1);
                }

                return keysFields;
            }
        }
    }
}

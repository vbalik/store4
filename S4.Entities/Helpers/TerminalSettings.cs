﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace S4.Entities.Helpers
{
    public class TerminalSettings
    {
        public const string SETTINGS_MARK = "S4TerminalSettings: ";


        [JsonProperty(PropertyName = "api_url")]
        public string ApiUrl { get; set; }

        [JsonProperty(PropertyName = "instance_name")]
        public string InstanceName { get; set; }

        [JsonProperty(PropertyName = "user")]
        [DefaultValue(null)]
        public string User { get; set; }

        [JsonProperty(PropertyName = "password")]
        [DefaultValue(null)]
        public string Password { get; set; }

        [JsonProperty(PropertyName = "textSize")]
        [DefaultValue(null)]
        public string TextSize { get; set; }
    }
}
﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Entities.Helpers
{
    public class TerminalMenuItem
    {
        [JsonProperty(PropertyName = "group")]
        public string Group { get; set; }
        [JsonProperty(PropertyName = "text")]
        public string Text { get; set; }
        [JsonProperty(PropertyName = "image")]
        public string Image { get; set; }
        [JsonProperty(PropertyName = "pageName")]
        public string PageName { get; set; }
        [JsonProperty(PropertyName = "pageParams")]
        public string PageParams { get; set; }
        [JsonProperty(PropertyName = "jobID")]
        public string JobID { get; set; }
    }
}

﻿using System;

namespace S4.Entities.Helpers
{
    public class StockTakingRemoteItem
    {
        public int StotakItemID { get; set; }
        public int StotakID { get; set; }
        public int PositionID { get; set; }
        public int ArtPackID { get; set; }
        public decimal Quantity { get; set; }
        public string CarrierNum { get; set; }
        public string BatchNum { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public DateTime EntryDateTime { get; set; }
        public string EntryUserID { get; set; }
        public Article Article { get; set; }
    }
}

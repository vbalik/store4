using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_articlelist_packings")]
	[PrimaryKey("artpackid")]
	[ExplicitColumns]
	public partial class ArticlePacking
	{
		[Required]
		[Display(Name = "ID")]
		[Column("artPackID")]
		public int ArtPackID {get; set;}

		[Required]
		[Column("articleID")]
		public int ArticleID {get; set;}

		[Required]
		[MaxLength(32)]
		[Display(Name = "Balení")]
		[Column("packDesc")]
		public string PackDesc {get; set;}

		[Required]
		[Display(Name = "Vztah")]
		[Column("packRelation")]
		public decimal PackRelation {get; set;}

		[Required]
		[Display(Name = "Hlavní")]
		[Column("movablePack")]
		public bool MovablePack {get; set;}

		[Required]
		[MaxLength(32)]
		[Display(Name = "Zdroj")]
		[Column("source_id")]
		public string Source_id {get; set;}

		[Required]
		[MaxLength(16)]
		[Display(Name = "Stav")]
		[StatusSet("Statuses")]
		[Column("packStatus")]
		public string PackStatus {get; set;}

		[Required]
		[Column("barCodeType")]
		public byte BarCodeType {get; set;}

		[MaxLength(32)]
		[Display(Name = "Čár. kód")]
		[Column("barCode")]
		public string BarCode {get; set;}

		[Required]
		[Display(Name = "Paleta")]
		[Column("inclCarrier")]
		public bool InclCarrier {get; set;}

		[Required]
		[Display(Name = "Váha")]
		[Column("packWeight")]
		public decimal PackWeight {get; set;}

		[Required]
		[Display(Name = "Objem")]
		[Column("packVolume")]
		public decimal PackVolume {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

namespace S4.Entities
{
    public partial class ArticleXtra : XtraData.IXtraDataStore<ArticleXtra>
    {

        #region IXtraDataStore

        public (string, int) GetIDs() => (this.XtraCode, this.DocPosition);


        public string GetValue() => this.XtraValue;


        public void Set(string code, int position, string strValue)
        {
            XtraCode = code;
            DocPosition = position;
            XtraValue = strValue;
        }


        public void SetParentID(int parentID)
        {
            ArticleID = parentID;
        }

        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_deliverymanifest")]
	[PrimaryKey("deliverymanifestid")]
	[ExplicitColumns]
	public partial class DeliveryManifest
	{
		[Required]
		[Display(Name = "ID")]
		[Column("deliveryManifestID")]
		public int DeliveryManifestID {get; set;}

		[Required]
		[MaxLength(16)]
		[Display(Name = "Stav")]
		[StatusSet("Statuses")]
		[Column("manifestStatus")]
		public string ManifestStatus {get; set;}

		[Required]
		[Display(Name = "Dopravce")]
		[Column("deliveryProvider")]
		public DeliveryDirection.DeliveryProviderEnum DeliveryProvider {get; set;}

		[Display(Name = "Pokus číslo")]
		[Column("createManifestAttempt")]
		public short? CreateManifestAttempt {get; set;}

		[Display(Name = "Chyba")]
		[Column("createManifestError")]
		public string CreateManifestError {get; set;}

		[MaxLength(128)]
		[Display(Name = "Ref. číslo zásilky")]
		[Column("manifestRefNumber")]
		public string ManifestRefNumber {get; set;}

		[Display(Name = "Datum a čas vytvoření")]
		[Column("manifestCreated")]
		public DateTime? ManifestCreated {get; set;}

		[Display(Name = "Datum a čas změny")]
		[Column("manifestChanged")]
		public DateTime? ManifestChanged {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

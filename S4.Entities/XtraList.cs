using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_xtralist")]
	[PrimaryKey("docclass,xtracode", AutoIncrement = false)]
	[ExplicitColumns]
	public partial class XtraList
	{
		[Required]
		[MaxLength(32)]
		[Column("docClass")]
		public string DocClass {get; set;}

		[Required]
		[MaxLength(16)]
		[Column("xtraCode")]
		public string XtraCode {get; set;}

		[Required]
		[MaxLength(32)]
		[Column("xtraField")]
		public string XtraField {get; set;}

		[Required]
		[MaxLength(32)]
		[Column("xtraType")]
		public string XtraType {get; set;}

		[Required]
		[Column("xtraNullable")]
		public bool XtraNullable {get; set;}

		[Required]
		[Column("xtraMultiple")]
		public bool XtraMultiple {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

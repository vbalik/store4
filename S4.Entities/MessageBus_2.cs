﻿using Newtonsoft.Json;
using S4.Entities.StatusEngine;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace S4.Entities
{
    public partial class MessageBus
    {
        public const string NEW = "NEW";
        public const string WAITING_FOR_REPEAT = "WAITING_FOR_REPEAT";
        public const string DONE = "DONE";
        public const string ERROR = "ERROR";
        public const string NOCOMPLETE = "NOCOMPLETE";
        public const string WAITING_FOR_COMPLETE = "WAITING_FOR_COMPLETE";

        public static StatusSet Statuses = new StatusSet(s =>
        {
            s.AddStatus(NEW, "Nový");

            s.AddStatus(WAITING_FOR_REPEAT, "Čeká na opakování")
                .SetEnabledStatuses()
                .AddAction("Nastavit stav na nový", ActionTypeEnum.ProcedureWithConfirmation, "messagebussetstatustonew").AuthorizationSuperUser();

            s.AddStatus(DONE, "Hotovo")
                .SetEnabledStatuses()
                .AddAction("Nastavit stav na nový", ActionTypeEnum.ProcedureWithConfirmation, "messagebussetstatustonew").AuthorizationSuperUser();

            s.AddStatus(ERROR, "Chyba")
                .SetEnabledStatuses()
                .AddAction("Nastavit stav na nový", ActionTypeEnum.ProcedureWithConfirmation, "messagebussetstatustonew").AuthorizationSuperUser();

            s.AddStatus(NOCOMPLETE, "Hotovo neodesláno")
                .SetEnabledStatuses()
                .AddAction("Nastavit stav na nový", ActionTypeEnum.ProcedureWithConfirmation, "messagebussetstatustonew").AuthorizationSuperUser();

            s.AddStatus(WAITING_FOR_COMPLETE, "Čeká na doplnění")
                .SetEnabledStatuses()
                .AddAction("Nastavit stav na nový", ActionTypeEnum.ProcedureWithConfirmation, "messagebussetstatustonew").AuthorizationSuperUser();
        });


    }
}

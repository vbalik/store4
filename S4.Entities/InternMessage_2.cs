﻿using System;
using System.Collections.Generic;
using S4.Entities.Settings;
using S4.Entities.StatusEngine;
using S4.Entities.XtraData;

namespace S4.Entities
{
    public partial class InternMessage
    {


        public const string AR_BARCODE_DEL = "AR_BCODE_DEL";
        public const string AR_BARCODE_SET = "AR_BCODE_SET";
        public const string AR_UPDATE = "AR_UPDATE";

        public const string POS_NOT_FOUND = "POS_NOT_FOUND";
        public const string POS_STATUS_CHANGE = "POSITION_CHANGE";
        public const string POS_STATUS_BLOCKED = "POS_BLOCKED";
        public const string POS_STATUS_OK = "POS_OK";

        public const string MO_STATUS_CHANGED = "MO_STATUS_CHANGED";
        public const string MO_ITEM_CHANGED = "MO_ITEM_CHANGED";
        public const string MO_STATUS_CANCEL = "MO_STATUS_CANCEL";

        public const string DR_RESERVATION_NOT_FOUND = "RESF";
        public const string DR_STATUS_CHANGED = "DR_STATUS_CHANGED";
        public const string DR_ITEM_CHANGED = "DR_ITEM_CHANGED";
        public const string DR_DELETE = "DR_DELETE";
        public const string DR_CANCEL = "DR_CANCEL";
        public const string DR_STATUS_DELIVERY_SHIPMENT_FAILED = "SHDELF";
        

        public const string IMPORT_XML = "ImportXML";

        public static StatusSet Statuses = new StatusSet(s =>
        {
            s.AddStatus(AR_BARCODE_DEL, "Smazán čárový kód").SetColor(System.Drawing.Color.Blue);
            s.AddStatus(AR_BARCODE_SET, "Nastaven čárový kód").SetColor(System.Drawing.Color.Blue);
            s.AddStatus(POS_NOT_FOUND, "Pozice nenalezena").SetColor(System.Drawing.Color.Blue);
            s.AddStatus(POS_STATUS_CHANGE, "Změna stavu pozice").SetColor(System.Drawing.Color.Blue);
            s.AddStatus(IMPORT_XML, "Import XML").SetColor(System.Drawing.Color.Blue);
            s.AddStatus(DR_DELETE, "Doklad smazán").SetColor(System.Drawing.Color.Blue);
            s.AddStatus(DR_CANCEL, "Doklad stornován").SetColor(System.Drawing.Color.Blue);
            s.AddStatus(POS_STATUS_BLOCKED, "Pozice uzamčena").SetColor(System.Drawing.Color.Blue);
            s.AddStatus(POS_STATUS_OK, "Pozice odemčena").SetColor(System.Drawing.Color.Blue);
            s.AddStatus(AR_UPDATE, "Karta změněna").SetColor(System.Drawing.Color.Blue);
            s.AddStatus(MO_STATUS_CHANGED, "Změna stavu").SetColor(System.Drawing.Color.Blue);
            s.AddStatus(MO_ITEM_CHANGED, "Změna položky").SetColor(System.Drawing.Color.Blue);
            s.AddStatus(MO_STATUS_CANCEL, "Skladový pohyb zrušen").SetColor(System.Drawing.Color.Blue);
            s.AddStatus(DR_RESERVATION_NOT_FOUND, "Rezervace není možná").SetColor(System.Drawing.Color.Blue);
            s.AddStatus(DR_STATUS_CHANGED, "Změna stavu dokladu").SetColor(System.Drawing.Color.Blue);
            s.AddStatus(DR_ITEM_CHANGED, "Změna položky").SetColor(System.Drawing.Color.Blue);
            s.AddStatus(DR_STATUS_DELIVERY_SHIPMENT_FAILED, "Zásilku nelze odeslat").SetColor(System.Drawing.Color.Red);
            

        });
    }
}

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_remoteaction")]
	[PrimaryKey("raid")]
	[ExplicitColumns]
	public partial class RemoteAction
	{
		[Required]
		[Display(Name = "ID")]
		[Column("raID")]
		public int RaID {get; set;}

		[Required]
		[MaxLength(16)]
		[Display(Name = "Stav")]
		[StatusSet("Statuses")]
		[Column("runStatus")]
		public string RunStatus {get; set;}

		[Required]
		[MaxLength(32)]
		[Display(Name = "Typ akce")]
		[Column("actionType")]
		public string ActionType {get; set;}

		[Required]
		[Display(Name = "Parametry akce")]
		[Column("actionParams")]
		public string ActionParams {get; set;}

		[Required]
		[MaxLength(16)]
		[Display(Name = "Uživatel")]
		[Column("sourceUserID")]
		public string SourceUserID {get; set;}

		[Required]
		[MaxLength(32)]
		[Display(Name = "Zařízení")]
		[Column("sourceDeviceID")]
		public string SourceDeviceID {get; set;}

		[Required]
		[Display(Name = "Datum akce")]
		[Column("actionDate")]
		public DateTime ActionDate {get; set;}

		[Required]
		[Display(Name = "Přijato")]
		[Column("received")]
		public DateTime Received {get; set;}

		[Display(Name = "Zpracováno")]
		[Column("completed")]
		public DateTime? Completed {get; set;}

		[Display(Name = "Popis chyby")]
		[Column("errorDesc")]
		public string ErrorDesc {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

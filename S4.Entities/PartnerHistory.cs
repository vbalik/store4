using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_partnerlist_history")]
	[PrimaryKey("partnerhistid")]
	[ExplicitColumns]
	public partial class PartnerHistory
	{
		[Required]
		[Column("partnerHistID")]
		public int PartnerHistID {get; set;}

		[Required]
		[Column("partnerID")]
		public int PartnerID {get; set;}

		[Required]
		[Column("partAddrID")]
		public int PartAddrID {get; set;}

		[Required]
		[MaxLength(16)]
		[Column("eventCode")]
		public string EventCode {get; set;}

		[Column("eventData")]
		public string EventData {get; set;}

		[Required]
		[Column("entryDateTime")]
		public DateTime EntryDateTime {get; set;}

		[Required]
		[MaxLength(16)]
		[Column("entryUserID")]
		public string EntryUserID {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

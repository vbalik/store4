namespace S4.Entities
{
	public partial class DeliveryServiceSerial
	{
		public const string STATUS_UNUSED = "UNUSED";
		public const string STATUS_ACTIVE = "ACTIVE";
		public const string STATUS_CLOSED = "CLOSED";
	}
}

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_stocktaking_items")]
	[PrimaryKey("stotakitemid")]
	[ExplicitColumns]
	public partial class StockTakingItem
	{
		[Required]
		[Display(Name = "ID")]
		[Column("stotakItemID")]
		public int StotakItemID {get; set;}

		[Required]
		[Column("stotakID")]
		public int StotakID {get; set;}

		[Required]
		[Column("positionID")]
		public int PositionID {get; set;}

		[Required]
		[Display(Name = "Balení")]
		[Column("artPackID")]
		public int ArtPackID {get; set;}

		[Required]
		[Display(Name = "Množství")]
		[Column("quantity")]
		public decimal Quantity {get; set;}

		[MaxLength(32)]
		[Display(Name = "Paleta")]
		[Column("carrierNum")]
		public string CarrierNum {get; set;}

		[MaxLength(32)]
		[Display(Name = "Šarže")]
		[Column("batchNum")]
		public string BatchNum {get; set;}

		[Display(Name = "Expirace")]
		[Column("expirationDate")]
		public DateTime? ExpirationDate {get; set;}

		[Required]
		[Display(Name = "Zadáno")]
		[Column("entryDateTime")]
		public DateTime EntryDateTime {get; set;}

		[Required]
		[MaxLength(16)]
		[Display(Name = "Uživatel")]
		[Column("entryUserID")]
		public string EntryUserID {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

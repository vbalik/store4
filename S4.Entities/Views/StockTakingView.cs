﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.StatusEngine;

namespace S4.Entities.Views
{
    [TableName("s4_vw_stockTaking")]
    public class StockTakingView : StockTaking
    {
        [Column("items")]
        [Display(Name = "Položek")]
        public int Items { get; set; }

        [Column("checked")]
        [Display(Name = "Zkontrolováno")]
        public int Checked { get; set; }

        [Column("nonChecked")]
        [Display(Name = "Nezkontrolováno")]
        public int NonChecked { get; set; }
    }
}

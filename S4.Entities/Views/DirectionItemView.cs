﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities.Views
{
    public class DirectionItemView : DirectionItem
    {
        [Column("articleID")]
        public int ArticleID { get; set; }

        [Display(Name = "Popis")]
        [Column("articleDesc")]
        public string ArticleDesc { get; set; }

        [Display(Name = "Kód")]
        [Column("articleCode")]
        [RawColumnName("i.articleCode")]
        public new string ArticleCode { get; set; }

        [Display(Name = "Jedn.")]
        [Column("unitDesc")]
        [RawColumnName("i.unitDesc")]
        public new string UnitDesc { get; set; }

        [Column("useBatch")]
        public bool ArticleUseBatch { get; set; }

        [Column("useExpiration")]
        public bool ArticleUseExpiration { get; set; }
    }
}

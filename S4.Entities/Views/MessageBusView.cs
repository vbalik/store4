﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities.Views
{
    public class MessageBusView : MessageBus
    {
        [Display(Name = "Prefix")]
        [Column("docNumPrefix")]
        public string DocNumPrefix { get; set; }

        [Display(Name = "Rok")]
        [Column("docYear")]
        public string DocYear { get; set; }

        [Display(Name = "Číslo dokladu")]
        [Column("docNumber")]
        public string DocNumber { get; set; }

    }
}

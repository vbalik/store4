﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities.Views
{
    public class InvoiceView : Invoice
    {
        [Display(Name = "Počet dokladů")]
        [Column("directionCount")]
        public int DirectionCount { get; set; }

    }
}

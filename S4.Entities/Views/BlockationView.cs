﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.StatusEngine;
using S4.Entities.Attributes;

namespace S4.Entities.Views
{
    public class BlockationView
    {
        public static StatusSet Statuses = StoreMove.Statuses;

        [Column("stoMoveItemID")]
        [Display(Name = "ID")]
        public int StoMoveItemID { get; set; }

        [Column("artPackID")]
        [Display(Name = "Balení")]
        public string ArtPackID { get; set; }

        [Column("doc")]
        [Display(Name = "Doklad")]
        public string Doc { get; set; }

        [Column("docStatus")]
        [Display(Name = "Stav")]
        [StatusSet("Statuses")]
        public string DocStatus { get; set; }

        public int ArticleID { get; set; }
        [Column("articleCode")]
        [Display(Name = "Kód")]
        public string ArticleCode { get; set; }

        [Column("articleDesc")]
        [Display(Name = "Popis")]
        public string ArticleDesc { get; set; }

        public int PositionIDFrom { get; set; }
        [Column("posCodeFrom")]
        [Display(Name = "Z pozice")]
        public string PosCodeFrom { get; set; }

        public int PositionIDTo { get; set; }
        [Column("posCodeTo")]
        [Display(Name = "Na pozici")]
        public string PosCodeTo { get; set; }

        [Column("quantity")]
        [Display(Name = "Množství")]
        public decimal Quantity { get; set; }

        public string _carrierNum { get; set; }
        [Display(Name = "Paleta")]
        public string CarrierNum
        {
            get
            {
                return (String.Equals(_carrierNum, StoreMoveItem.EMPTY_CARRIERNUM, StringComparison.InvariantCultureIgnoreCase)) ? null : _carrierNum;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                    _carrierNum = StoreMoveItem.EMPTY_CARRIERNUM;
                else
                    _carrierNum = value;
            }
        }

        public string _batchNum { get; set; }
        [Display(Name = "Šarže")]
        public string BatchNum
        {
            get
            {
                return (String.Equals(_batchNum, StoreMoveItem.EMPTY_BATCHNUM, StringComparison.InvariantCultureIgnoreCase)) ? null : _batchNum;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                    _batchNum = StoreMoveItem.EMPTY_BATCHNUM;
                else
                    _batchNum = value;
            }
        }

        public DateTime? ExpirationDate { get; set; }
        [Display(Name = "Expirace")]
        public string ExpirationDesc
        {
            get
            {
                return Core.Data.Formating.DateTimeToExpStr((DateTime.Equals(ExpirationDate, StoreMoveItem.EMPTY_EXPIRATION)) ? (DateTime?)null : ExpirationDate);
            }
        }

        [Column("dir")]
        [Display(Name = "Příkaz")]
        public string Dir { get; set; }

        [Display(Name = "Uživatel")]
        [Column("entryUserID")]
        public string EntryUserID { get; set; }
    }
}

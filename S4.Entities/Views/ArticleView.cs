﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

using NPoco;

namespace S4.Entities.Views
{
    [ExplicitColumns]
    public class ArticleView : Article
    {
        //[Ignore]
        [Column("quantity")]
        [Display(Name = "Skladem")]
        public decimal Quantity { get; set; }
    }
}

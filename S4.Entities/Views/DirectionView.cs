﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.StatusEngine;
using static S4.Entities.DeliveryDirection;

namespace S4.Entities.Views
{
    public class DirectionView : Direction
    {
        [Display(Name = "Partner")]
        [Column("partnerName")]
        public string PartnerName { get; set; }

        [Display(Name = "Doručení")]
        [Column("deliveryInst")]
        public string DeliveryInst { get; set; }

        [Display(Name = "Provozovna")]
        [Column("addrInfo")]
        public string AddrInfo { get; set; }

        [Display(Name = "Adresa")]
        [Column("addrInfo2")]
        public string AddrInfo2 { get; set; }

        [Display(Name = "Průběh")]
        [Column("directionOutCompleted")]
        public string DirectionOutCompleted => $"{ItemsCompleted}/{ItemsCount}";

        [Column("itemsCount")]
        public int ItemsCount { get; set; }
        
        [Column("itemsCompleted")]
        public int ItemsCompleted { get; set; }
        
        // not from db
        [Display(Name = "Ikony")]
        public string Signs { get; set; }

        // not from db
        public string WorkerID { get; set; }
              
        public bool DeliveryImplemented { get; set; }

        [Display(Name = "Dopravce")]
        public string DeliveryProvider { get; set; } = string.Empty;

        [Display(Name = "Doručovací adresa 1")]
        [Column("deliveryAddress1")]
        public string DeliveryAddress1 { get; set; }

        [Display(Name = "Doručovací adresa 2")]
        [Column("deliveryAddress2")]
        public string DeliveryAddress2 { get; set; }

        [Display(Name = "Doručovací adresa město")]
        [Column("deliveryAddressCity")]
        public string DeliveryAddressCity { get; set; }
    }
}

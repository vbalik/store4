﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

using NPoco;

namespace S4.Entities.Views
{
    [TableName("s4_vw_stockTakingItem")]
    [ExplicitColumns]
    public class StockTakingItemView : StockTakingItem
    {
        [Display(Name = "Pozice")]
        [Column("posCode")]
        public string PosCode { get; set; }

        [Display(Name = "Kód")]
        [Column("articleCode")]
        public string ArticleCode { get; set; }

        [Display(Name = "Popis")]
        [Column("articleDesc")]
        public string ArticleDesc { get; set; }

        [Column("articleID")]
        public int ArticleID { get; set; }
    }
}

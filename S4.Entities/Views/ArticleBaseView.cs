﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.StatusEngine;
using S4.Entities.Attributes;

namespace S4.Entities.Views
{
    [TableName("s4_vw_article_base")]
    [ExplicitColumns]
    public class ArticleBaseView : Article
    {
        [Ignore]
        public new string Source_id { get; set; }

        [Ignore]
        public new byte[] R_Edit { get; set; }

        [Column("articleInfo")]
        public new string ArticleInfo { get; set; }

        [Required]
        [Display(Name = "ID")]
        [Column("artPackID")]
        public int ArtPackID { get; set; }

        [Display(Name = "Balení")]
        [Column("packDesc")]
        public string PackDesc { get; set; }

        [Display(Name = "Vztah")]
        [Column("packRelation")]
        public decimal PackRelation { get; set; }

        [Display(Name = "Hlavní")]
        [Column("movablePack")]
        public bool MovablePack { get; set; }

        [Display(Name = "Stav")]
        [StatusSet("PackStatuses")]
        [Column("packStatus")]
        public string PackStatus { get; set; }

        [Column("barCodeType")]
        public byte BarCodeType { get; set; }

        [Display(Name = "Čár. kód")]
        [Column("barCode")]
        public string BarCode { get; set; }

        [Display(Name = "Paleta")]
        [Column("inclCarrier")]
        public bool InclCarrier { get; set; }

        [Display(Name = "Váha")]
        [Column("packWeight")]
        public decimal PackWeight { get; set; }

        [Display(Name = "Objem")]
        [Column("packVolume")]
        public decimal PackVolume { get; set; }

        [Column("packInfo")]
        public string PackInfo { get; set; }

        public static StatusSet PackStatuses = ArticlePacking.Statuses;
    }
}

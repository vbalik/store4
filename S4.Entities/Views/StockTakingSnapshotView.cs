﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

using NPoco;

namespace S4.Entities.Views
{
    [TableName("s4_vw_stockTakingSnapshot")]
    [ExplicitColumns]
    public class StockTakingSnapshotView : StockTakingSnapshot
    {
        [Display(Name = "Pozice")]
        [Column("posCode")]
        public string PosCode { get; set; }

        [Display(Name = "Kód")]
        [Column("articleCode")]
        public string ArticleCode { get; set; }

        [Display(Name = "Popis")]
        [Column("articleDesc")]
        public string ArticleDesc { get; set; }
    }
}

﻿using NPoco;
using System.ComponentModel.DataAnnotations;


namespace S4.Entities.Views
{
    public class DeliveryPackagesView 
    {
        [Display(Name = "ID")]
        [Column("directionID")]
        public int DirectionID { get; set; }

        [Display(Name = "Z. Pozn.")]
        [Column("docInfo")]
        public string DocInfo { get; set; }

        [Display(Name = "Kontakt")]
        [Column("contact")]
        public string Contact { get; set; }

        [Display(Name = "Tel")]
        [Column("phone")]
        public string Phone { get; set; }

        [Display(Name = "Adresa")]
        [Column("address")]
        public string Address { get; set; }

        [Display(Name = "PSČ")]
        [Column("zip")]
        public string Zip { get; set; }

        [Display(Name = "Č. balíku")]
        [Column("parcelRefNumber")]
        public string ParcelRefNumber { get; set; }

        [Display(Name = "Cena")]
        [Column("price")]
        public string Price { get; set; }

        [Display(Name = "Var. symb")]
        [Column("invoiceNumber")]
        public string InvoiceNumber { get; set; }
    }
}

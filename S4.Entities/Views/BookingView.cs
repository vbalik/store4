﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities.Views
{
    public class BookingView : Booking
    {
        [Display(Name = "Šarže")]
        [Column("batchNum")]
        public string BatchNum { get; set; }

        [Display(Name = "Expirace")]
        [Column("expirationDate")]
        public string ExpirationDate { get; set; }

        [Display(Name = "Kód karty")]
        [Column("articleCode")]
        public string ArticleCode { get; set; }

        [Display(Name = "Název karty")]
        [Column("articleDesc")]
        public string ArticleDesc { get; set; }

        [Display(Name = "Název partnera")]
        [Column("partnerName")]
        public string PartnerName { get; set; }

        [Display(Name = "Adresa")]
        [Column("addrInfo2")]
        public string AddrInfo2 { get; set; }

        [Display(Name = "Prefix")]
        [Column("docNumPrefix")]
        public string DocNumPrefix { get; set; }

        [Display(Name = "Rok")]
        [Column("docYear")]
        public string DocYear { get; set; }

        [Display(Name = "Číslo dokladu")]
        [Column("docInfo")]
        public string DocInfo { get; set; }

        [Display(Name = "Založil")]
        [Column("entryUserName")]
        public string EntryUserName { get; set; }

        [Display(Name = "Smazal")]
        [Column("deletedByEntryUserName")]
        public string DeletedByEntryUserName { get; set; }
    }
}

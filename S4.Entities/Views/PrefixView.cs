﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

using NPoco;

namespace S4.Entities.Views
{
    public class PrefixView : Entities.Prefix
    {
        
        [Column]
        [Display(Name = "Název sekce")]
        public string SectDesc { get; set; }

    }
}

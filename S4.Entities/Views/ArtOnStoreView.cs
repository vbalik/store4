﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities.Views
{
    public class ArtOnStoreView : ArticleView
    {
        [Ignore]
        [Column]
        public int PositionID { get; set; }

        [Ignore]
        [Display(Name = "Pozice")]
        [Column]
        public string PosCode { get; set; }

        [Ignore]
        [Column]
        public string _batchNum { get; set; }

        [Ignore]
        [Display(Name = "Šarže")]
        public string BatchNum
        {
            get
            {
                return (String.Equals(_batchNum, StoreMoveItem.EMPTY_BATCHNUM, StringComparison.InvariantCultureIgnoreCase)) ? null : _batchNum;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                    _batchNum = StoreMoveItem.EMPTY_BATCHNUM;
                else
                    _batchNum = value;
            }
        }

        [Ignore]
        [Display(Name = "Expirace")]
        [Column]
        public DateTime? ExpirationDate { get; set; }

        [Ignore]
        [Column("carrierNum")]
        public string _carrierNum { get; set; }

        [Ignore]
        [Display(Name = "Paleta")]
        public string CarrierNum
        {
            get
            {
                return (String.Equals(_carrierNum, StoreMoveItem.EMPTY_CARRIERNUM, StringComparison.InvariantCultureIgnoreCase)) ? null : _carrierNum;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                    _carrierNum = StoreMoveItem.EMPTY_CARRIERNUM;
                else
                    _carrierNum = value;
            }
        }

        [Display(Name = "Expirace")]
        [Core.DBTestIgnore]
        [RawColumnName("ExpirationDate")]
        public string ExpirationDesc
        {
            get
            {
                return Core.Data.Formating.DateTimeToExpStr((DateTime.Equals(ExpirationDate, StoreMoveItem.EMPTY_EXPIRATION)) ? (DateTime?)null : ExpirationDate);
            }
        }


        [Ignore]
        [Core.DBTestIgnore]
        public string PackDesc { get; set; }

        [Ignore]
        [Core.DBTestIgnore]
        public int ArtPackID { get; set; }
    }
}

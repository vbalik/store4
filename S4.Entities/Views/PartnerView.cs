﻿using System;

namespace S4.Entities.Views
{
    public class PartnerView
    {
        public int PartnerID { get; set; }
        public string PartnerName { get; set; }
        public string PartnerPhone { get; set; }
    }
}

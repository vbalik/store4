﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Drawing;

using NPoco;

using S4.Entities.Attributes;
using S4.Entities.StatusEngine;

namespace S4.Entities.Views
{
    public class DeliveryManifestView : DeliveryManifest
    {
        [Display(Name = "Dokladů")]
        [Column("docs")]
        public short Docs { get; set; }

        [Display(Name = "Balíků")]
        [Column("parcels")]
        public short Parcels { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.StatusEngine;

namespace S4.Entities.Views
{
    [TableName("s4_vw_stockTakingPosition")]
    public class StockTakingPositionView : StockTakingPosition
    {
        [Display(Name = "Pozice")]
        [Column("posCode")]
        public string PosCode { get; set; }

        [Display(Name = "Hala")]
        [Column("houseID")]
        public string HouseID { get; set; }
    }
}

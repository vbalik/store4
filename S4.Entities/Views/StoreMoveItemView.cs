﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.StatusEngine;
using S4.Entities.Attributes;

namespace S4.Entities.Views
{
    public class StoreMoveItemView : StoreMoveItem
    {
        public static StatusSet Statuses = new StatusSet(s =>
        {
            foreach (var status in StoreMove.Statuses.Items)
            {
                var newStatus = s.AddStatus(status.Code, status.Name, status.DocDirection);
                if (newStatus.Code != StoreMove.MO_STATUS_CANCEL)
                    newStatus.AddAction("Změnit položku", ActionTypeEnum.Dialog, "changeitem_dialog").AuthorizationSuperUser();
            }
        });

        [Column("posCode")]
        [Display(Name = "Pozice")]
        public string PosCode { get; set; }

        [Column("artPackID")]
        [Display(Name = "Balení")]
        public string ArtPackID { get; set; }

        [Display(Name = "ID")]
        [Column("articleID")]
        public int ArticleID { get; set; }

        [Display(Name = "Balení")]
        [Column("packDesc")]
        public string PackDesc { get; set; }

        [Display(Name = "Kód")]
        [Column("articleCode")]
        public string ArticleCode { get; set; }

        [Display(Name = "Popis")]
        [Column("articleDesc")]
        public string ArticleDesc { get; set; }

        [Column("batchNum")]
        public string _batchNum { get; set; }
        [Display(Name = "Šarže")]
        public string BatchNum
        {
            get
            {
                return (String.Equals(_batchNum, StoreMoveItem.EMPTY_BATCHNUM, StringComparison.InvariantCultureIgnoreCase)) ? null : _batchNum;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                    _batchNum = StoreMoveItem.EMPTY_BATCHNUM;
                else
                    _batchNum = value;
            }
        }

        [Column("expirationDate")]
        [Display(Name = "Expirace")]
        public DateTime? ExpirationDate { get; set; }

        [Display(Name = "Expirace")]
        [RawColumnName("ExpirationDate")]
        public string ExpirationDesc
        {
            get
            {
                return Core.Data.Formating.DateTimeToExpStr((DateTime.Equals(ExpirationDate, StoreMoveItem.EMPTY_EXPIRATION)) ? (DateTime?)null : ExpirationDate);
            }
        }

        [Column("doc")]
        [RawColumnName("smo.docInfo")]
        [Display(Name = "Doklad")]
        public string Doc { get; set; }

        [Column("dir")]
        [RawColumnName("dir.docInfo")]
        [Display(Name = "Příkaz")]
        public string Dir { get; set; }

        [Column("directionID")]
        public int DirectionID { get; set; }

        [Column("docStatus")]
        [RawColumnName("smo.docStatus")]
        [Display(Name = "Stav")]
        [StatusSet("Statuses")]
        public string DocStatus { get; set; }

        [Column("docDate")]
        [RawColumnName("smo.docDate")]
        [Display(Name = "Datum")]
        public DateTime DocDate { get; set; }

        [Required]
        [Column("docType")]
        [RawColumnName("smo.docType")]
        public int DocType { get; set; }

        public bool IsInSummary { get; set; }
    }
}

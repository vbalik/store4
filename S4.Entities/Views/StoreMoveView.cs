﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.StatusEngine;
using S4.Entities.Attributes;

namespace S4.Entities.Views
{
    public class StoreMoveView : StoreMove
    {
        [Column("parentDoc")]
        [Display(Name = "Nadřízený doklad")]
        public string ParentDoc { get; set; }

        public bool OnSummary { get; set; }
    }
}

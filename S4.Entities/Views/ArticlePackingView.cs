﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

using NPoco;

namespace S4.Entities.Views
{
    [ExplicitColumns]
    public class ArticlePackingView : ArticlePacking
    {
        protected List<string> _barCodes = new List<string>();

        [Ignore]
        [Column("barcodes")]
        [Display(Name = "Čárové kódy")]
        public List<string> BarCodes
        {
            get
            {
                var result = _barCodes;

                // add main barCode to first position
                if(!string.IsNullOrEmpty(BarCode))
                    result.Insert(0, BarCode);

                return result;
            }
            set => _barCodes = value;
        }
    }
}

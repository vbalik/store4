﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

using NPoco;

namespace S4.Entities.Views
{
    public class PositionView : Position
    {
        //[Ignore]
        [Column("itemsCount")]
        [Display(Name = "Položek")]
        public int ItemsCount { get; set; }

        /// <summary>
        /// only for updating data on server - cliend sends the section IDs to server by this property
        /// </summary>
        [Ignore]
        public string[] SectionIDs { get; set; }
    }
}

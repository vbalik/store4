using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_partnerlist")]
	[PrimaryKey("partnerid")]
	[ExplicitColumns]
	public partial class Partner
	{
		[Required]
		[Column("partnerID")]
		public int PartnerID {get; set;}

		[Required]
		[MaxLength(128)]
		[Column("partnerName")]
		public string PartnerName {get; set;}

		[Required]
		[MaxLength(32)]
		[Column("source_id")]
		public string Source_id {get; set;}

		[Required]
		[MaxLength(16)]
		[Display(Name = "Stav")]
		[StatusSet("Statuses")]
		[Column("partnerStatus")]
		public string PartnerStatus {get; set;}

		[Required]
		[Column("partnerDispCheck")]
		public int PartnerDispCheck {get; set;}

		[MaxLength(256)]
		[Column("partnerAddr_1")]
		public string PartnerAddr_1 {get; set;}

		[MaxLength(256)]
		[Column("partnerAddr_2")]
		public string PartnerAddr_2 {get; set;}

		[MaxLength(48)]
		[Column("partnerCity")]
		public string PartnerCity {get; set;}

		[MaxLength(16)]
		[Column("partnerPostCode")]
		public string PartnerPostCode {get; set;}

		[MaxLength(48)]
		[Column("partnerPhone")]
		public string PartnerPhone {get; set;}

		[MaxLength(16)]
		[Display(Name = "IČO")]
		[Column("partnerOrgIdentNumber")]
		public string PartnerOrgIdentNumber {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

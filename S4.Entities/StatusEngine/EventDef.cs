﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace S4.Entities.StatusEngine
{
    public class EventDef
    {
        public EventDef()
        {
        }

        internal EventDef(Status status)
        {
            Code = status.Code;
            Name = status.Name;
            Color = status.Color;
        }

        public string Code { get; set; }
        public string Name { get; set; }
        public Color Color { get; set; } = Color.Empty;
        public Func<string, object> Formater { get; set; }
    }
}

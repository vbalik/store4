﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Newtonsoft.Json;

namespace S4.Entities.StatusEngine
{
    public class Status
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public int  DocDirection { get; set; }
        public Color Color { get; set; } = Color.Empty;
        public string CssClass { get; set; }
        public string CssStyle { get; set; }
        public string Html { get; set; }
        public string FilterSubquery { get; set; }

        public List<string> NextStatuses { get; set; } = new List<string>();
        public List<ActionDef> Actions { get; set; } = new List<ActionDef>();


        public Status SetColor(Color color)
        {
            Color = color;
            return this;
        }

        public Status SetCssClass(string cssClass)
        {
            CssClass = cssClass;
            return this;
        }

        public Status SetCssStyle(string cssStyle)
        {
            CssStyle = cssStyle;
            return this;
        }

        public Status SetHtml(string html)
        {
            Html = html;
            return this;
        }

        public Status SetEnabledStatuses(params string[] statuses)
        {
            NextStatuses = new List<string>(statuses);
            return this;
        }


        public Status AddAction(ActionDef action)
        {
            Actions.Add(action);
            return this;
        }

        public Status AddAction(string name, string procedureName)
        {
            return AddAction(name, ActionTypeEnum.Procedure, procedureName);
        }

        public Status AddAction(string name, ActionTypeEnum actionType, string procedureName)
        {
            var action = new ActionDef() { Name = name, ActionType = actionType, ActionProcedure = procedureName };
            return AddAction(action);
        }


        public Status AuthorizationSuperUser()
        {
            if (Actions.Count == 0)
                throw new Exception("Status not set");
            Actions.Last().ActionAuthorization = ActionAuthorizationEnum.SuperUser;
            return this;
        }


        public Status AuthorizationCreatorSuperUser()
        {
            if (Actions.Count == 0)
                throw new Exception("Status not set");
            Actions.Last().ActionAuthorization = ActionAuthorizationEnum.CreatorSuperUser;
            return this;
        }


        public Status SetActions(List<ActionDef> actions)
        {
            Actions = actions;
            return this;
        }

        public Status SetFilterSubquery(string subquery)
        {
            FilterSubquery = subquery;
            return this;
        }
    }
}

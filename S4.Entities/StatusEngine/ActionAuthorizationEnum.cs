﻿namespace S4.Entities.StatusEngine
{
    public enum ActionAuthorizationEnum
    {
        User,
        SuperUser,
        CreatorSuperUser // super user or record creator
    }
}

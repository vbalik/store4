﻿namespace S4.Entities.StatusEngine
{
    public enum ActionTypeEnum
    {
        Procedure,
        ProcedureWithConfirmation,
        Dialog,
        Link,
        ProcedureWithConfirmationValidation
    }
}

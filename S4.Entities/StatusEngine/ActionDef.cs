﻿using System;
using System.Collections.Generic;
using System.Text;


namespace S4.Entities.StatusEngine
{
    public class ActionDef
    {
        public string Name { get; set; }
        public ActionTypeEnum ActionType { get; set; } = ActionTypeEnum.Procedure;
        public ActionAuthorizationEnum ActionAuthorization { get; set; } = ActionAuthorizationEnum.User;
        public string ActionProcedure { get; set; }
    }
}

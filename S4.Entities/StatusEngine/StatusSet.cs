﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace S4.Entities.StatusEngine
{
    public class StatusSet
    {
        private Dictionary<string, Status> _statuses = new Dictionary<string, Status>();
        private Dictionary<string, EventDef> _events = new Dictionary<string, EventDef>();
        public StatusSet(Action<StatusSet> initFunction)
        {
            initFunction(this);
        }

        public List<Status> Items { get { return new List<Status>(_statuses.Values); } }
        public List<EventDef> Events { get { return new List<EventDef>(_events.Values); } }

        public Status AddStatus(Status status)
        {
            _statuses.Add(status.Code, status);
            _events.Add(status.Code, new EventDef(status));
            return status;
        }

        public Status AddStatus(string code, string name, int docDirection = 0)
        {
            var status = new Status() { Code = code, Name = name, DocDirection = docDirection };
            return AddStatus(status);
        }


        public EventDef AddEvent(EventDef @event)
        {
            _events.Add(@event.Code, @event);
            return @event;
        }

        public EventDef AddEvent(string code, string name)
        {
            var @event = new EventDef() { Code = code, Name = name };
            return AddEvent(@event);
        }


        public bool NextStatusEnabled(string currentStatus, string nextStatus)
        {
            Status status;
            if (_statuses.TryGetValue(currentStatus, out status))
            {
                return status.NextStatuses.Contains(nextStatus);
            }
            else
                throw new Exception($"Status '{currentStatus}' not found");
        }
    }
}

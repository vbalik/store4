﻿using Newtonsoft.Json;
using NPoco;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Entities
{
    public partial class PrinterLocation
    {
        [Core.DBTestIgnore]
        [Column("printerSettings")]
        [JsonIgnore]
        public string _printerSettingsJson
        {
            get
            {
                if (PrinterSettings == null)
                    return null;
                else
                    return JsonConvert.SerializeObject(PrinterSettings, Formatting.Indented);
            }

            set
            {
                if (String.IsNullOrWhiteSpace(value))
                    PrinterSettings = new Settings.PrinterSettings();
                else
                    PrinterSettings = JsonConvert.DeserializeObject<Settings.PrinterSettings>(value);
            }
        }

        [Core.DBTestIgnore]
        public Settings.PrinterSettings PrinterSettings { get; set; } = new Settings.PrinterSettings();
    }
}

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_houselist")]
	[PrimaryKey("houseid", AutoIncrement = false)]
	[ExplicitColumns]
	public partial class HouseList
	{
		[Required]
		[MaxLength(16)]
		[Column("houseID")]
		public string HouseID {get; set;}

		[Required]
		[Column("houseDesc")]
		public string HouseDesc {get; set;}

		[Column("houseMap")]
		public byte[] HouseMap {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_positionlist")]
	[PrimaryKey("positionid")]
	[ExplicitColumns]
	public partial class Position
	{
		public enum PositionCategoryEnum
		{
			[Display(Name = "Sklad")]
			Store = 0,
			[Display(Name = "Příjem")]
			Receive = 1,
			[Display(Name = "Expedice")]
			Dispatch = 2,
			[Display(Name = "Speciální")]
			Special = 3,
		}

		public enum PositionAttributeEnum
		{
			[Display(Name = "Nic")]
			None = 0,
			[Display(Name = "Lednice")]
			Cooling = 1,
			[Display(Name = "Mrazák")]
			FreezeBox = 2,
		}

		[Required]
		[Display(Name = "ID")]
		[Column("positionID")]
		public int PositionID {get; set;}

		[Required]
		[MaxLength(16)]
		[Display(Name = "Kód")]
		[Column("posCode")]
		public string PosCode {get; set;}

		[MaxLength(128)]
		[Display(Name = "Popis")]
		[Column("posDesc")]
		public string PosDesc {get; set;}

		[Required]
		[MaxLength(16)]
		[Display(Name = "Hala")]
		[Column("houseID")]
		public string HouseID {get; set;}

		[Required]
		[Display(Name = "Categ.")]
		[Column("posCateg")]
		public PositionCategoryEnum PosCateg {get; set;}

		[Required]
		[MaxLength(16)]
		[Display(Name = "Stav")]
		[StatusSet("Statuses")]
		[Column("posStatus")]
		public string PosStatus {get; set;}

		[Required]
		[Display(Name = "Možnosti")]
		[Column("posAttributes")]
		public PositionAttributeEnum PosAttributes {get; set;}

		[Required]
		[Display(Name = "Výška")]
		[Column("posHeight")]
		public int PosHeight {get; set;}

		[Required]
		[Display(Name = "Poloha X")]
		[Column("posX")]
		public int PosX {get; set;}

		[Required]
		[Display(Name = "Poloha Y")]
		[Column("posY")]
		public int PosY {get; set;}

		[Required]
		[Display(Name = "Poloha Z")]
		[Column("posZ")]
		public int PosZ {get; set;}

		[MaxLength(32)]
		[Column("mapPos")]
		public string MapPos {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

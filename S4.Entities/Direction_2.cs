﻿using System;
using System.Collections.Generic;
using S4.Entities.Settings;
using S4.Entities.StatusEngine;
using S4.Entities.XtraData;

namespace S4.Entities
{
    public partial class Direction
    {
        public const string DOC_CLASS = "DI";

        public const byte DR_DOCTYPE_FREE = 1;

        public const Int16 DOC_DIRECTION_IN = 1;
        public const Int16 DOC_DIRECTION_OUT = -1;
        public const Int16 DOC_DIRECTION_MOVE = 0;

        //public const string DR_PREFIX_BATCH_PREP = "DIBP";

        public const string DR_STATUS_CANCEL = "CANC";

        // sto in
        public const string DR_STATUS_RECEIVE_PROC = "PREC";
        public const string DR_STATUS_RECEIVE_COMPLET = "CREC";
        //public const string DR_STATUS_REPACK_WAIT = "WRPC";
        //public const string DR_STATUS_REPACK_PROC = "PRPC";
        public const string DR_STATUS_STOIN_WAIT = "WSTI";
        public const string DR_STATUS_STOIN_PROC = "PSTI";
        public const string DR_STATUS_STOIN_COMPLET = "CSTI";
        //public const string DR_STATUS_DIR_MOVE_WAIT = "WDIM";
        //public const string DR_STATUS_DIR_MOVE_PREP = "RDIM";
        //public const string DR_STATUS_DIR_MOVE_PROC = "PDIM";
        //public const string DR_STATUS_DIR_MOVE_DONE = "DDIM";
        //public const string DR_STATUS_DISPATCH_BAT_PREP = "BDIS";
        //public const string DR_STATUS_DISPATCH_BP_DONE = "DDIS";

        // dispatch
        public const string DR_STATUS_LOAD = "LOAD";
        public const string DR_STATUS_RES_IN_PROC = "RES_IN_PROC";
        public const string DR_STATUS_RES_NOT_FOUND = "RES_NOT_FOUND";
        public const string DR_STATUS_DISPATCH_WAIT = "WDIS";
        public const string DR_STATUS_DISPATCH_PROC = "PDIS";
        public const string DR_STATUS_DISPATCH_SAVED = "SDIS";
        public const string DR_STATUS_DISPATCH_MANUAL = "MDIS";
        // check
        public const string DR_STATUS_DISP_CHECK_WAIT = "WCHE";
        public const string DR_STATUS_DISP_CHECK_PROC = "PCHE";
        public const string DR_STATUS_DISP_CHECK_DONE = "DCHE";
        // exped
        public const string DR_STATUS_DISP_EXPED = "EXPE";
        public const string DR_STATUS_DELIVERY_SHIPMENT = "SHDEL";
        public const string DR_STATUS_DELIVERY_SHIPMENT_FAILED = "SHDELF";
        public const string DR_STATUS_DELIVERY_PRINT = "PRDEL";
        public const string DR_STATUS_DELIVERY_MANIFEST = "MADEL";
        // do not process
        public const string DR_STATUS_NOT_PROCESSED = "NOT_PROC";

        // events
        public const string DR_EVENT_STATUS_CHANGED = "STATUS_CHANGED";
        public const string DR_EVENT_FIELD_CHANGE = "FLCH";
        public const string DR_EVENT_DISP_SECT_CHANGE = "DSCH";
        public const string DR_EVENT_RESERVATION_DONE = "RESR";
        public const string DR_EVENT_RESERVATION_NOT_FOUND = "RESF";
        public const string DR_EVENT_RESERVATION_CANCELED = "RES_CANCELED";
        public const string DR_EVENT_RESERVATION_ERROR = "RES_ERROR";
        public const string DR_EVENT_DISP_TO_STOIN = "DTSI";
        public const string DR_EVENT_EXPORT_DONE = "EXPORT_DONE";
        public const string DR_EVENT_PICKING_HISTORY = "DDLO";
        public const string DR_EVENT_ITEM_CHANGED = "ITCH";
        public const string DR_EVENT_ITEM_DISPATCHED = "ITEM_DISPATCHED";
        public const string DR_EVENT_POSITION_ERROR = "POSITION_ERROR";
        public const string DR_EVENT_DISP_REPEAT = "DISP_REPEAT";
        public const string DR_EVENT_EXPORT_ABRA = "EXPORT_ABRA_DONE";
        public const string DR_EVENT_IMPORT_RECEIVE = "IMPORT_RECEIVE";
        public const string DR_EVENT_DISP_TO_POSITION = "DISP_TO_POSITION";
        public const string DR_EVENT_CHECK_SKIPPED = "CHECK_SKIPPED";
        public const string DR_EVENT_RELOAD = "RELOAD";
        public const string DR_EVENT_NOITEM = "NOITEM";
        public const string DR_EVENT_UNSUCCESSFUL_CANCEL = "UNSUCC_CANC";
        public const string DR_EVENT_DELIVERED = "DELIVERED";
        public const string DR_EVENT_TO_BOOKINGS = "TO_BOOKINGS";



        // extra data
        public const string XTRA_EXPED_TRUCK = "EXPT";
        public const string XTRA_DISPATCH_POSITION_ID = "DIPO";
        public const string XTRA_DISPATCH_DIRECT_SECTION = "DIDS";
        public const string XTRA_DISPATCH_DIRECT_SECTION_LIST = "DID";
        public const string XTRA_DISPATCH_ALTER_SECTION = "DIAS";
        public const string XTRA_DISPATCH_STORE_MOVE_ID = "STO_MOVE_ID";
        public const string XTRA_DISPATCH_WEIGHT = "DI_WEIGHT";
        public const string XTRA_DISPATCH_BOXES_CNT = "DI_BOX_CNT";
        public const string XTRA_DISP_CHECK_METHOD = "DCHM";
        public const string XTRA_BATCH_PREP_POSITION = "BPPO";
        public const string XTRA_BATCH_PREP_SECTION = "BPSE";
        public const string XTRA_BATCH_PREP_STMOVE_ID = "BPMO";
        public const string XTRA_REC_EXPECT_CARRS = "EXPC";
        public const string XTRA_ARTICLE_COOLED = "ARCO";
        public const string XTRA_ARTICLE_MEDICINES = "ARME";
        public const string XTRA_ARTICLE_CYTOSTATICS = "ARCY";
        public const string XTRA_ARTICLE_CHECK_ON_RECEIVE = "ARCHREC";
        public const string XTRA_MANUF_ICON = "MFIC";
        public const string XTRA_INVOICE_NUMBER = "INUM";
        public const string XTRA_PAYMENT_REFERENCE = "PREF";
        public const string XTRA_TRANSPORTATION_TYPE = "TTYPE";
        public const string XTRA_PAYMENT_TYPE = "PTYPE";
        public const string XTRA_CASH_ON_DELIVERY = "DOD";
        public const string XTRA_DELIVERY_COUNTRY = "DCOUN";
        public const string XTRA_DELIVERY_CURRENCY = "DCURR";
        public const string XTRA_DELIVERY_PARCEL_SHOP = "PSHOP";
        public const string XTRA_DELIVERY_SHIPMENTINFO = "SHIPI";
        public const string XTRA_ABRA_DOC_ID = "ABRADOCID";
        public const string XTRA_DELIVERY_ROUTE_CODE = "DROUTE_CODE";
        public const string XTRA_DELIVERY_DEPO_CODE = "DDEPO_CODE";
        public const string XTRA_DELIVERY_PACK_NUMBER = "DPACK_NUMBER";
        public const string XTRA_DELIVERY_ZIP_CODE = "DZIP_NUMBER";
        public const string XTRA_DELIVERY_ROUTING_SQUARE = "DROUTING_SQUARE";
        public const string XTRA_DELIVERY_RISK_CLASS = "RISK_CLASS";
        public const string XTRA_DELIVERY_RECEIVING_PERSON = "RECV_PERSON";
        public const string XTRA_DELIVERY_UDIDI = "UDIDI";
        public const string XTRA_ARTICLE_ADR_SUM = "ADR_SUM";
        public const string XTRA_EXTNUMBER = "EXT_NUMBER";
        public const string XTRA_SEND_DISKET_SUMMARY = "SEND_DISKET_SUM";
        public const string XTRA_MANDATORY_DIRECTION_PRINT = "MANDAT_DIR_PRINT";

        public static ActionDef dispatchAction = new ActionDef() { Name = "Vychystání", ActionType = ActionTypeEnum.Dialog, ActionProcedure = "dispatch_dialog" };
        public static ActionDef testReservationAction = new ActionDef() { Name = "Test vyskladnění", ActionType = ActionTypeEnum.Dialog, ActionProcedure = "picking_dialog" };
        public static ActionDef getShipmentStatus = new ActionDef() { Name = "Stav zásilky", ActionType = ActionTypeEnum.Dialog, ActionProcedure = "shipmentStatus_dialog" };
        public static StatusSet Statuses = new StatusSet(s =>
        {
            // store in
            s.AddStatus(DR_STATUS_RECEIVE_PROC, "Příjem - v běhu", 1)
                .SetColor(System.Drawing.Color.Blue)
                .SetEnabledStatuses()
                .AddAction("Ukončit příjem", ActionTypeEnum.ProcedureWithConfirmation, "ReceiveTestDirection").AuthorizationSuperUser()
                .AddAction("Změnit stav", ActionTypeEnum.Dialog, "setStatus_dialog").AuthorizationSuperUser()
                .AddAction("Tisk přepravního dokladu", ActionTypeEnum.Dialog, "print_adr_dialog")
                .AddAction("Zrušit doklad", ActionTypeEnum.Dialog, "cancel_dialog").AuthorizationCreatorSuperUser();

            s.AddStatus(DR_STATUS_RECEIVE_COMPLET, "Příjem - hotovo", 1)
                .SetEnabledStatuses(DR_STATUS_STOIN_WAIT, DR_STATUS_CANCEL)
                .AddAction("Tisk", ActionTypeEnum.Dialog, "print_dialog")
                .AddAction("Tisk sériových čísel", ActionTypeEnum.Dialog, "print_sn_dialog")
                .AddAction("Tisk přepravního dokladu", ActionTypeEnum.Dialog, "print_adr_dialog")
                .AddAction("Naskladnění", ActionTypeEnum.Dialog, "storeIn_dialog")
                .AddAction("Změnit stav", ActionTypeEnum.Dialog, "setStatus_dialog").AuthorizationSuperUser()
                .AddAction("Zrušit doklad", ActionTypeEnum.Dialog, "cancel_dialog").AuthorizationCreatorSuperUser();

            s.AddStatus(DR_STATUS_STOIN_WAIT, "Naskladnění - přiřazeno", 1)
                .SetColor(System.Drawing.Color.Green)
                .SetEnabledStatuses()
                .AddAction("Tisk", ActionTypeEnum.Dialog, "print_dialog")
                .AddAction("Tisk sériových čísel", ActionTypeEnum.Dialog, "print_sn_dialog")
                .AddAction("Tisk přepravního dokladu", ActionTypeEnum.Dialog, "print_adr_dialog")
                .AddAction("Jiný pracovník", ActionTypeEnum.Dialog, "storeIn_dialog")
                .AddAction("Změnit stav", ActionTypeEnum.Dialog, "setStatus_dialog").AuthorizationSuperUser()
                .AddAction("Zrušit doklad", ActionTypeEnum.Dialog, "cancel_dialog").AuthorizationCreatorSuperUser();

            s.AddStatus(DR_STATUS_STOIN_PROC, "Naskladnění - v běhu", 1)
                .SetColor(System.Drawing.Color.Blue)
                .SetEnabledStatuses()
                .AddAction("Tisk", ActionTypeEnum.Dialog, "print_dialog")
                .AddAction("Tisk sériových čísel", ActionTypeEnum.Dialog, "print_sn_dialog")
                .AddAction("Tisk přepravního dokladu", ActionTypeEnum.Dialog, "print_adr_dialog")
                .AddAction("Změnit stav", ActionTypeEnum.Dialog, "setStatus_dialog").AuthorizationSuperUser()
                .AddAction("Zrušit doklad", ActionTypeEnum.Dialog, "cancel_dialog").AuthorizationCreatorSuperUser();

            s.AddStatus(DR_STATUS_STOIN_COMPLET, "Naskladnění - hotovo", 1)
                .SetColor(System.Drawing.Color.Gray)
                .SetEnabledStatuses()
                .AddAction("Tisk", ActionTypeEnum.Dialog, "print_dialog")
                .AddAction("Tisk sériových čísel", ActionTypeEnum.Dialog, "print_sn_dialog")
                .AddAction("Tisk přepravního dokladu", ActionTypeEnum.Dialog, "print_adr_dialog")
                .AddAction("Vytvořit rezervace", ActionTypeEnum.Dialog, "create_booking_dialog")
                .AddAction("Změnit stav", ActionTypeEnum.Dialog, "setStatus_dialog").AuthorizationSuperUser()
                .AddAction("Zrušit doklad", ActionTypeEnum.Dialog, "cancel_dialog").AuthorizationCreatorSuperUser();

            s.AddStatus(DR_STATUS_LOAD, "Nahráno", -1)
                .SetEnabledStatuses(DR_STATUS_DISPATCH_WAIT, DR_STATUS_CANCEL)
                .AddAction("Změnit stav", ActionTypeEnum.Dialog, "setStatus_dialog").AuthorizationSuperUser()
                .AddAction("Zrušit doklad", ActionTypeEnum.Dialog, "cancel_dialog").AuthorizationCreatorSuperUser()
                .AddAction("Tisk pořadových štítků", ActionTypeEnum.Dialog, "print_labels_dialog")
                .AddAction("Tisk přepravního dokladu", ActionTypeEnum.Dialog, "print_adr_dialog")
                .AddAction("Manuální vychystání", ActionTypeEnum.Link, "/action/dispatchmanual?directionID={directionID}");

            s.AddStatus(DR_STATUS_RES_IN_PROC, "Rezervace...", -1)
                .AddAction("Změnit stav", ActionTypeEnum.Dialog, "setStatus_dialog").AuthorizationSuperUser()
                .AddAction("Zrušit doklad", ActionTypeEnum.Dialog, "cancel_dialog").AuthorizationCreatorSuperUser()
                .AddAction("Tisk přepravního dokladu", ActionTypeEnum.Dialog, "print_adr_dialog")
                .AddAction("Tisk pořadových štítků", ActionTypeEnum.Dialog, "print_labels_dialog");

            s.AddStatus(DR_STATUS_RES_NOT_FOUND, "Chyba rezervace", -1)
                .AddAction("Vychystání", ActionTypeEnum.Dialog, "dispatch_dialog")
                .AddAction("Test rezervace", ActionTypeEnum.Dialog, "picking_dialog")
                .AddAction("Změnit stav", ActionTypeEnum.Dialog, "setStatus_dialog").AuthorizationSuperUser()
                .AddAction("Zrušit doklad", ActionTypeEnum.Dialog, "cancel_dialog").AuthorizationCreatorSuperUser()
                .AddAction("Tisk přepravního dokladu", ActionTypeEnum.Dialog, "print_adr_dialog")
                .AddAction("Tisk pořadových štítků", ActionTypeEnum.Dialog, "print_labels_dialog");

            s.AddStatus(DR_STATUS_NOT_PROCESSED, "Nevyskladňuje se", -1)
                .SetColor(System.Drawing.Color.Red);

            // dispatch 
            s.AddStatus(DR_STATUS_DISPATCH_WAIT, "Vychystání - přiřazeno", -1)
                .SetColor(System.Drawing.Color.Green)
                .SetEnabledStatuses()
                .AddAction("Jiný pracovník", ActionTypeEnum.Dialog, "dispatch_dialog")
                .AddAction("Změnit stav", ActionTypeEnum.Dialog, "setStatus_dialog").AuthorizationSuperUser()
                .AddAction("Zrušit doklad", ActionTypeEnum.Dialog, "cancel_dialog").AuthorizationCreatorSuperUser()
                .AddAction("Tisk pořadových štítků", ActionTypeEnum.Dialog, "print_labels_dialog")
                .AddAction("Tisk přepravního dokladu", ActionTypeEnum.Dialog, "print_adr_dialog")
                .AddAction("Tisk balících štítků", ActionTypeEnum.Dialog, "print_box_labels_dialog");

            s.AddStatus(DR_STATUS_DISPATCH_MANUAL, "Vychystání - manuální", -1)
                .SetColor(System.Drawing.Color.Green)
                .SetEnabledStatuses()
                .AddAction("Změnit stav", ActionTypeEnum.Dialog, "setStatus_dialog").AuthorizationSuperUser()
                .AddAction("Zrušit doklad", ActionTypeEnum.Dialog, "cancel_dialog").AuthorizationCreatorSuperUser()
                .AddAction("Tisk pořadových štítků", ActionTypeEnum.Dialog, "print_labels_dialog")
                .AddAction("Manuální vychystání", ActionTypeEnum.Link, "/action/dispatchmanual?directionID={directionID}")
                .AddAction("Tisk přepravního dokladu", ActionTypeEnum.Dialog, "print_adr_dialog")
                .AddAction("Tisk balících štítků", ActionTypeEnum.Dialog, "print_box_labels_dialog");

            s.AddStatus(DR_STATUS_DISPATCH_PROC, "Vychystání - v běhu", -1)
                .SetColor(System.Drawing.Color.Blue)
                .SetEnabledStatuses()
                .AddAction("Zrušení rezervace", ActionTypeEnum.ProcedureWithConfirmation, "DispatchCancelReservation")
                .AuthorizationSuperUser()
                .AddAction("Znovu rezervovat", ActionTypeEnum.ProcedureWithConfirmation, "DispatchRepeatReservation")
                .AuthorizationSuperUser()
                .AddAction("Změnit stav", ActionTypeEnum.Dialog, "setStatus_dialog").AuthorizationSuperUser()
                .AddAction("Jiný pracovník", ActionTypeEnum.Dialog, "reassign_dialog")
                .AddAction("Zrušit doklad", ActionTypeEnum.Dialog, "cancel_dialog").AuthorizationCreatorSuperUser()
                .AddAction("Tisk pořadových štítků", ActionTypeEnum.Dialog, "print_labels_dialog")
                .AddAction("Tisk přepravního dokladu", ActionTypeEnum.Dialog, "print_adr_dialog")
                .AddAction("Tisk balících štítků", ActionTypeEnum.Dialog, "print_box_labels_dialog");

            s.AddStatus(DR_STATUS_DISPATCH_SAVED, "Vychystání - hotovo", -1)
                .SetEnabledStatuses()
                .AddAction("Tisk", ActionTypeEnum.Dialog, "print_dialog")
                .AddAction("Tisk sériových čísel", ActionTypeEnum.Dialog, "print_sn_dialog")
                .AddAction("Tisk přepravního dokladu", ActionTypeEnum.Dialog, "print_adr_dialog")
                .AddAction("Kontrola", ActionTypeEnum.Dialog, "dispatchCheck_dialog")
                .AddAction("Přeskočit kontrolu", ActionTypeEnum.ProcedureWithConfirmation, "DispCheckSkip")
                .AddAction("Změnit stav", ActionTypeEnum.Dialog, "setStatus_dialog").AuthorizationSuperUser()
                .AddAction("Export", ActionTypeEnum.Dialog, "export_dialog")
                .AddAction("Zrušit doklad", ActionTypeEnum.Dialog, "cancel_dialog").AuthorizationCreatorSuperUser()
                .AddAction("Tisk pořadových štítků", ActionTypeEnum.Dialog, "print_labels_dialog")
                .AddAction("Tisk balících štítků", ActionTypeEnum.Dialog, "print_box_labels_dialog");

            // check
            s.AddStatus(DR_STATUS_DISP_CHECK_WAIT, "Kontrola - přiřazeno", -1)
                .SetColor(System.Drawing.Color.Green)
                .SetEnabledStatuses()
                .AddAction("Jiný pracovník", ActionTypeEnum.Dialog, "dispatchCheck_dialog")
                .AddAction("Tisk", ActionTypeEnum.Dialog, "print_dialog")
                .AddAction("Změnit stav", ActionTypeEnum.Dialog, "setStatus_dialog").AuthorizationSuperUser()
                .AddAction("Export", ActionTypeEnum.Dialog, "export_dialog")
                .AddAction("Zrušit doklad", ActionTypeEnum.Dialog, "cancel_dialog").AuthorizationCreatorSuperUser()
                .AddAction("Tisk přepravního dokladu", ActionTypeEnum.Dialog, "print_adr_dialog")
                .AddAction("Tisk pořadových štítků", ActionTypeEnum.Dialog, "print_labels_dialog")
                .AddAction("Tisk balících štítků", ActionTypeEnum.Dialog, "print_box_labels_dialog");

            s.AddStatus(DR_STATUS_DISP_CHECK_PROC, "Kontrola - v běhu", -1)
                .SetColor(System.Drawing.Color.Blue)
                .SetEnabledStatuses()
                .AddAction("Tisk", ActionTypeEnum.Dialog, "print_dialog")
                .AddAction("Tisk sériových čísel", ActionTypeEnum.Dialog, "print_sn_dialog")
                .AddAction("Tisk přepravního dokladu", ActionTypeEnum.Dialog, "print_adr_dialog")
                .AddAction("Změnit stav", ActionTypeEnum.Dialog, "setStatus_dialog").AuthorizationSuperUser()
                .AddAction("Export", ActionTypeEnum.Dialog, "export_dialog")
                .AddAction("Zrušit doklad", ActionTypeEnum.Dialog, "cancel_dialog").AuthorizationCreatorSuperUser()
                .AddAction("Tisk pořadových štítků", ActionTypeEnum.Dialog, "print_labels_dialog")
                .AddAction("Tisk balících štítků", ActionTypeEnum.Dialog, "print_box_labels_dialog");

            s.AddStatus(DR_STATUS_DISP_CHECK_DONE, "Kontrola - hotovo", -1)
                .SetEnabledStatuses()
                .AddAction("Expedice", ActionTypeEnum.Dialog, "expedition_dialog")
                .AddAction("Přesunout na příjem", ActionTypeEnum.Dialog, "dispToStoIn_dialog")
                .AddAction("Přijmout na pozici", ActionTypeEnum.Dialog, "dispToPos_dialog")
                .AddAction("Tisk", ActionTypeEnum.Dialog, "print_dialog")
                .AddAction("Tisk sériových čísel", ActionTypeEnum.Dialog, "print_sn_dialog")
                .AddAction("Tisk přepravního dokladu", ActionTypeEnum.Dialog, "print_adr_dialog")
                .AddAction("Změnit stav", ActionTypeEnum.Dialog, "setStatus_dialog").AuthorizationSuperUser()
                .AddAction("Export", ActionTypeEnum.Dialog, "export_dialog")
                .AddAction("Zrušit doklad", ActionTypeEnum.Dialog, "cancel_dialog").AuthorizationCreatorSuperUser()
                .AddAction("Tisk pořadových štítků", ActionTypeEnum.Dialog, "print_labels_dialog")
                .AddAction("Tisk balících štítků", ActionTypeEnum.Dialog, "print_box_labels_dialog");

            // exped
            s.AddStatus(DR_STATUS_DISP_EXPED, "Expedováno", -1)
                .SetColor(System.Drawing.Color.Gray)
                .SetEnabledStatuses()
                .AddAction("Tisk", ActionTypeEnum.Dialog, "print_dialog")
                .AddAction("Tisk sériových čísel", ActionTypeEnum.Dialog, "print_sn_dialog")
                .AddAction("Tisk přepravního dokladu", ActionTypeEnum.Dialog, "print_adr_dialog")
                .AddAction("Změnit stav", ActionTypeEnum.Dialog, "setStatus_dialog").AuthorizationSuperUser()
                .AddAction("Export", ActionTypeEnum.Dialog, "export_dialog")
                .AddAction("Zrušit doklad", ActionTypeEnum.Dialog, "cancel_dialog").AuthorizationCreatorSuperUser()
                .AddAction("Tisk pořadových štítků", ActionTypeEnum.Dialog, "print_labels_dialog")
                .AddAction("Tisk balících štítků", ActionTypeEnum.Dialog, "print_box_labels_dialog")
                .AddAction(getShipmentStatus);
            s.AddStatus(DR_STATUS_DELIVERY_SHIPMENT, "Zásilka vytvořena", -1)
                .SetColor(System.Drawing.Color.Violet)
                .AddAction("Vytisknout štítky", ActionTypeEnum.Dialog, "print_delivery_labels_dialog")
                .AddAction("Změnit stav", ActionTypeEnum.Dialog, "setStatus_dialog").AuthorizationSuperUser()
                .AddAction("Znovu vytvořit zásilku", ActionTypeEnum.Dialog, "resetShipment_dialog")
                .AddAction("Tisk přepravního dokladu", ActionTypeEnum.Dialog, "print_adr_dialog")
                .AddAction("Tisk balících štítků", ActionTypeEnum.Dialog, "print_box_labels_dialog")
                .AddAction(getShipmentStatus);
            s.AddStatus(DR_STATUS_DELIVERY_SHIPMENT_FAILED, "Zásilku nelze odeslat", -1)
                .SetColor(System.Drawing.Color.Red)
                .AddAction("Expedice", ActionTypeEnum.Dialog, "expedition_dialog")
                .AddAction("Znovu vytvořit zásilku", ActionTypeEnum.Dialog, "resetShipment_dialog")
                .AddAction("Tisk přepravního dokladu", ActionTypeEnum.Dialog, "print_adr_dialog")
                .AddAction("Tisk balících štítků", ActionTypeEnum.Dialog, "print_box_labels_dialog")
                .AddAction("Změnit stav", ActionTypeEnum.Dialog, "setStatus_dialog").AuthorizationSuperUser();
            s.AddStatus(DR_STATUS_DELIVERY_PRINT, "Štítky vytištěny", -1)
                .SetColor(System.Drawing.Color.Violet)
                .AddAction("Expedice", ActionTypeEnum.Dialog, "expedition_dialog")
                .AddAction("Opakovat tisk štítků", ActionTypeEnum.Dialog, "print_delivery_labels_dialog")
                .AddAction("Tisk balících štítků", ActionTypeEnum.Dialog, "print_box_labels_dialog")
                .AddAction("Znovu vytvořit zásilku", ActionTypeEnum.Dialog, "resetShipment_dialog")
                .AddAction("Změnit stav", ActionTypeEnum.Dialog, "setStatus_dialog").AuthorizationSuperUser()
                .AddAction("Tisk přepravního dokladu", ActionTypeEnum.Dialog, "print_adr_dialog")
                .AddAction(getShipmentStatus);
            s.AddStatus(DR_STATUS_DELIVERY_MANIFEST, "Svoz připraven", -1)
                .SetColor(System.Drawing.Color.Violet)
                .AddAction("Expedice", ActionTypeEnum.Dialog, "expedition_dialog")
                .AddAction("Změnit stav", ActionTypeEnum.Dialog, "setStatus_dialog").AuthorizationSuperUser()
                .AddAction("Tisk přepravního dokladu", ActionTypeEnum.Dialog, "print_adr_dialog")
                .AddAction(getShipmentStatus);

            // cancel
            s.AddStatus(DR_STATUS_CANCEL, "Zrušeno")
                .SetColor(System.Drawing.Color.Red)
                .AddAction("Změnit stav", ActionTypeEnum.Dialog, "setStatus_dialog").AuthorizationSuperUser()
                .AddAction("Smazat doklad", ActionTypeEnum.ProcedureWithConfirmation, "DirectionDelete").AuthorizationSuperUser();


            // events
            s.AddEvent(DR_EVENT_STATUS_CHANGED, "Změna stavu");
            s.AddEvent(DR_EVENT_FIELD_CHANGE, "Změna pole");
            s.AddEvent(DR_EVENT_DISP_SECT_CHANGE, "Změna sekce vyskladnění");
            s.AddEvent(DR_EVENT_RESERVATION_DONE, "Rezervace vytvořena");
            s.AddEvent(DR_EVENT_RESERVATION_NOT_FOUND, "Rezervace není možná");
            s.AddEvent(DR_EVENT_RESERVATION_CANCELED, "Rezervace zrušena");
            s.AddEvent(DR_EVENT_RESERVATION_ERROR, "Interní chyba rezervace");
            s.AddEvent(DR_EVENT_DISP_TO_STOIN, "Logistika - převod na příjemku");
            s.AddEvent(DR_EVENT_PICKING_HISTORY, "Průběh vychystání");
            s.AddEvent(DR_EVENT_ITEM_CHANGED, "Změna položky");
            s.AddEvent(DR_EVENT_ITEM_DISPATCHED, "Položka vychystána");
            s.AddEvent(DR_EVENT_POSITION_ERROR, "Chyba pozice");
            s.AddEvent(DR_EVENT_DISP_REPEAT, "Znovu rezervováno");
            s.AddEvent(DR_EVENT_EXPORT_DONE, "Export dat");
            s.AddEvent(DR_EVENT_EXPORT_ABRA, "Export expirace a šarže");
            s.AddEvent(DR_EVENT_IMPORT_RECEIVE, "Logistika - import příjmu");
            s.AddEvent(DR_EVENT_DISP_TO_POSITION, "Logistika - příjem na pozici");
            s.AddEvent(DR_EVENT_CHECK_SKIPPED, "Kontrola přeskočena");
            s.AddEvent(DR_EVENT_RELOAD, "Znovu nahráno");
            s.AddEvent(DR_EVENT_NOITEM, "Žádné položky");
            s.AddEvent(DR_EVENT_UNSUCCESSFUL_CANCEL, "Neúspěšné storno");
            s.AddEvent(DR_EVENT_DELIVERED, "Doručeno zákazníkovi");
            s.AddEvent(DR_EVENT_TO_BOOKINGS, "Rezervace vytvořeny");

        });



        #region misc

        [Core.DBTestIgnore]
        public static readonly string[] STATUSES_IN_ACTION = new string[] {
            DR_STATUS_STOIN_PROC,
            DR_STATUS_DISPATCH_WAIT, DR_STATUS_DISPATCH_PROC,
            DR_STATUS_DISP_CHECK_WAIT, DR_STATUS_DISP_CHECK_PROC
        };

        [Core.DBTestIgnore]
        public static readonly string[] STATUSES_RECEIVE = new string[] {
            DR_STATUS_RECEIVE_PROC,
            DR_STATUS_RECEIVE_COMPLET
        };

        [Core.DBTestIgnore]
        public static readonly string[] STATUSES_STOIN = new string[] {
            DR_STATUS_STOIN_WAIT,
            DR_STATUS_STOIN_PROC,
            DR_STATUS_STOIN_COMPLET
        };

        [Core.DBTestIgnore]
        public static readonly string[] STATUSES_DISPATCH = new string[] {
            DR_STATUS_LOAD,
            DR_STATUS_RES_IN_PROC,
            DR_STATUS_RES_NOT_FOUND,
            DR_STATUS_DISPATCH_WAIT,
            DR_STATUS_DISPATCH_PROC,
            DR_STATUS_DISPATCH_SAVED,
            DR_STATUS_DISPATCH_MANUAL,
            DR_STATUS_DELIVERY_SHIPMENT_FAILED,
            DR_STATUS_DELIVERY_PRINT,
            DR_STATUS_DELIVERY_MANIFEST
        };

        [Core.DBTestIgnore]
        public static readonly string[] STATUSES_CHECK = new string[] {
            DR_STATUS_DISP_CHECK_WAIT,
            DR_STATUS_DISP_CHECK_PROC,
            DR_STATUS_DISP_CHECK_DONE
        };





        public enum DispCheckMethodsEnum
        {
            EnterQuantity = 0,
            ReadBarCodes = 1,
            ReadBarCodes2 = 2,

            NotSet = 255
        }


        [Core.DBTestIgnore]
        public static readonly Dictionary<int, string> DISP_CHECK_METHODS = new Dictionary<int, string>()
        {
            { (int)DispCheckMethodsEnum.EnterQuantity, "Zadat množství" },
            { (int)DispCheckMethodsEnum.ReadBarCodes, "Načíst kódy" },
            { (int)DispCheckMethodsEnum.ReadBarCodes2, "Načíst kódy 2" }
        };

        #endregion


        #region extra data

        public static readonly string[] XTRA_ITEMS_SIGNS = new string[] { XTRA_ARTICLE_COOLED, XTRA_ARTICLE_MEDICINES, XTRA_ARTICLE_CYTOSTATICS };

        public static XtraDataDict XtraDataDict = new XtraDataDict(x =>
        {
            x.AddField(XTRA_DISPATCH_DIRECT_SECTION, "Sekce vyskladnění", typeof(string));
            x.AddField(XTRA_DISPATCH_POSITION_ID, "Pozice vyskladnění", typeof(int));
            x.AddField(XTRA_DISPATCH_DIRECT_SECTION_LIST, "Seznam sekcí vyskladnění", typeof(string), true);
            x.AddField(XTRA_DISPATCH_ALTER_SECTION, "Alternativní sekce vyskladnění", typeof(string), true);
            x.AddField(XTRA_DISPATCH_STORE_MOVE_ID, "Vyskladnění - ID skladového pohybu", typeof(int));
            x.AddField(XTRA_DISPATCH_WEIGHT, "Váha", typeof(decimal), true);
            x.AddField(XTRA_DISPATCH_BOXES_CNT, "Počet balíků", typeof(int));
            x.AddField(XTRA_DISP_CHECK_METHOD, "Způsob kontroly", typeof(int));
            x.AddField(XTRA_ARTICLE_MEDICINES, "Léky", typeof(bool), true);
            x.AddField(XTRA_ARTICLE_CYTOSTATICS, "Cytostatika", typeof(bool), true);
            x.AddField(XTRA_ARTICLE_CHECK_ON_RECEIVE, "Kontrola při příjmu", typeof(bool), true);
            x.AddField(XTRA_ARTICLE_COOLED, "Chlazené zboží", typeof(bool), true);
            x.AddField(XTRA_EXPED_TRUCK, "Vozidlo", typeof(string));
            x.AddField(XTRA_MANUF_ICON, "Ikona dodavatele", typeof(string), true);
            x.AddField(XTRA_INVOICE_NUMBER, "Číslo faktury", typeof(string));
            x.AddField(XTRA_PAYMENT_REFERENCE, "Variabilní symbol", typeof(string));
            x.AddField(XTRA_TRANSPORTATION_TYPE, "Způsob dopravy", typeof(string));
            x.AddField(XTRA_PAYMENT_TYPE, "Způsob platby", typeof(string));
            x.AddField(XTRA_CASH_ON_DELIVERY, "Dobírka", typeof(decimal));
            x.AddField(XTRA_DELIVERY_COUNTRY, "Země doručení", typeof(string));
            x.AddField(XTRA_DELIVERY_CURRENCY, "Měna dobírky", typeof(string));
            x.AddField(XTRA_DELIVERY_PARCEL_SHOP, "Výdejní místo", typeof(string));
            x.AddField(XTRA_DELIVERY_SHIPMENTINFO, "Informace k doručení", typeof(string));
            x.AddField(XTRA_ABRA_DOC_ID, "Abra ID", typeof(string));
            x.AddField(XTRA_DELIVERY_ROUTE_CODE, "Směrovací kód", typeof(string));
            x.AddField(XTRA_DELIVERY_DEPO_CODE, "Depo", typeof(string));
            x.AddField(XTRA_DELIVERY_PACK_NUMBER, "Číslo balíku", typeof(string), true);
            x.AddField(XTRA_DELIVERY_ZIP_CODE, "PSČ", typeof(string));
            x.AddField(XTRA_DELIVERY_ROUTING_SQUARE, "Routovací čtverec depo", typeof(bool));
            x.AddField(XTRA_DELIVERY_RISK_CLASS, "Riziková třída", typeof(string), true);
            x.AddField(XTRA_DELIVERY_RECEIVING_PERSON, "Přebírající osoba", typeof(string));
            x.AddField(XTRA_DELIVERY_UDIDI, "UDI-DI", typeof(string), true);
            x.AddField(XTRA_ARTICLE_ADR_SUM, "ADR součet", typeof(int));
            x.AddField(XTRA_EXTNUMBER, "Externí číslo objednávky", typeof(string), true);
            x.AddField(XTRA_SEND_DISKET_SUMMARY, "Poslat disketu souhrnně", typeof(bool));
            x.AddField(XTRA_MANDATORY_DIRECTION_PRINT, "Nutný tisk šaržového listu", typeof(bool), false);
        });

        [Core.DBTestIgnore]
        public XtraDataProxy<DirectionXtra> XtraData { get; } = new XtraDataProxy<DirectionXtra>(XtraDataDict);

        #endregion


        public readonly static PrefixSettings DefaultPrefixSettings = new Settings.PrefixSettings()
        {
            DispatchDistribution = Entities.Settings.PrefixSettings.DispatchDistributionEnum.OneFromOneSection,
            DispatchSections = new List<Entities.Settings.PrefixSettings.DispatchSection>()
                    {
                        new Entities.Settings.PrefixSettings.DispatchSection() { Order = 0, SectID = "VYSK" }
                    }
        };
    }
}

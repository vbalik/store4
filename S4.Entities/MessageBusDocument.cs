using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_messagebus_document")]
	[PrimaryKey("messagebusdocumentid")]
	[ExplicitColumns]
	public partial class MessageBusDocument
	{
		[Required]
		[Display(Name = "ID")]
		[Column("messageBusDocumentID")]
		public int MessageBusDocumentID {get; set;}

		[Required]
		[Display(Name = "messageBusID")]
		[Column("messageBusID")]
		public int MessageBusID {get; set;}

		[Required]
		[MaxLength(512)]
		[Display(Name = "Název dokumentu")]
		[Column("documentName")]
		public string DocumentName {get; set;}

		[Required]
		[MaxLength(256)]
		[Display(Name = "MIME typ")]
		[Column("mimeType")]
		public string MimeType {get; set;}

		[Required]
		[Column("document")]
		public byte[] Document {get; set;}

		[Required]
		[Display(Name = "Založeno")]
		[Column("entryDateTime")]
		public DateTime EntryDateTime {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

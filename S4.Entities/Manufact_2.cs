﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using NPoco;
using S4.Entities.StatusEngine;

namespace S4.Entities
{
    public partial class Manufact
    {
        public const string MAN_STATUS_OK = "MAN_OK";
        public const string MAN_STATUS_LOCKED = "MAN_LOCKED";

        [Core.DBTestIgnore]
        public bool IsNew { get; set; }

        public static StatusSet Statuses = new StatusSet(s =>
        {
            s.AddStatus(MAN_STATUS_OK, "OK");
            s.AddStatus(MAN_STATUS_LOCKED, "Uzamčen");
        });


        [Core.DBTestIgnore]
        [Column("manufSettings")]
        public string _manufactSettingsJson
        {
            get
            {
                if (ManufactSettings == null)
                    return null;
                else
                    return JsonConvert.SerializeObject(ManufactSettings, Formatting.Indented);
            }

            set
            {
                if (String.IsNullOrWhiteSpace(value))
                    ManufactSettings = new Settings.ManufactSettings();
                else
                    ManufactSettings = JsonConvert.DeserializeObject<Settings.ManufactSettings>(value);
            }
        }

        [Core.DBTestIgnore]
        public Settings.ManufactSettings ManufactSettings { get; set; } = new Settings.ManufactSettings();
    }
}

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;
using S4.Entities.StatusEngine;
using S4.Entities.XtraData;

namespace S4.Entities
{
    public partial class Invoice
    {
        public const string INV_STATUS_LOAD = "LOAD";
        public const string INV_STATUS_RELOAD = "RELOAD";

        public static StatusSet Statuses = new StatusSet(s =>
        {
            s.AddStatus(INV_STATUS_LOAD, "Nahráno");

            s.AddStatus(INV_STATUS_RELOAD, "Znovu nahráno")
                .SetColor(System.Drawing.Color.Blue);


        });

        // extra data
        public const string XTRA_PAYMENT_REFERENCE = "PREF";
        public const string XTRA_DOC_DATE = "DDATE";
        public const string XTRA_PAYMENT_TYPE = "PTYPE";
        public const string XTRA_CASH_ON_DELIVERY = "DOD";
        public const string XTRA_TRANSPORTATION_TYPE = "TTYPE";
        public const string XTRA_DELIVERY_CURRENCY = "DCURR";
        public const string XTRA_DELIVERY_COUNTRY = "DCOUN";


        #region extra data

        public static XtraDataDict XtraDataDict = new XtraDataDict(x =>
        {
            x.AddField(XTRA_TRANSPORTATION_TYPE, "Způsob dopravy", typeof(string));
            x.AddField(XTRA_PAYMENT_TYPE, "Způsob platby", typeof(string));
            x.AddField(XTRA_PAYMENT_REFERENCE, "Variabilní symbol ", typeof(string));
            x.AddField(XTRA_CASH_ON_DELIVERY, "Dobírka", typeof(decimal));
            x.AddField(XTRA_DELIVERY_COUNTRY, "Země doručení", typeof(string));
            x.AddField(XTRA_DELIVERY_CURRENCY, "Měna dobírky", typeof(string));
            x.AddField(XTRA_DOC_DATE, "Datum vystavení dokladu", typeof(DateTime));


        });

        [Core.DBTestIgnore]
        public XtraDataProxy<InvoiceXtra> XtraData { get; } = new XtraDataProxy<InvoiceXtra>(XtraDataDict);

        #endregion




    }
}

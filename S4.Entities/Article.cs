using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_articlelist")]
	[PrimaryKey("articleid")]
	[ExplicitColumns]
	public partial class Article
	{
		public enum SpecFeaturesEnum
		{
			[Display(Name = "Nic")]
			Any = 0,
			[Display(Name = "Lednice")]
			Cooled = 1,
			[Display(Name = "Léky")]
			Medicines = 2,
			[Display(Name = "Cytostatika")]
			Cytostatics = 4,
			[Display(Name = "Ověřit příjem")]
			CheckOnReceive = 8,
		}

		[Required]
		[Display(Name = "ID")]
		[Column("articleID")]
		public int ArticleID {get; set;}

		[Required]
		[MaxLength(64)]
		[Display(Name = "Kód")]
		[Column("articleCode")]
		public string ArticleCode {get; set;}

		[MaxLength(256)]
		[Display(Name = "Popis")]
		[Column("articleDesc")]
		public string ArticleDesc {get; set;}

		[Required]
		[MaxLength(16)]
		[Display(Name = "Stav")]
		[StatusSet("Statuses")]
		[Column("articleStatus")]
		public string ArticleStatus {get; set;}

		[Required]
		[Display(Name = "Typ")]
		[Column("articleType")]
		public byte ArticleType {get; set;}

		[Required]
		[MaxLength(32)]
		[Display(Name = "ID zdroje")]
		[Column("source_id")]
		public string Source_id {get; set;}

		[MaxLength(16)]
		[Display(Name = "Sekce")]
		[Column("sectID")]
		public string SectID {get; set;}

		[MaxLength(32)]
		[Display(Name = "Jednotka")]
		[Column("unitDesc")]
		public string UnitDesc {get; set;}

		[Required]
		[MaxLength(16)]
		[Display(Name = "Výrobce")]
		[Column("manufID")]
		public string ManufID {get; set;}

		[Required]
		[Display(Name = "Šarže")]
		[Column("useBatch")]
		public bool UseBatch {get; set;}

		[Required]
		[Display(Name = "Míchat šarže")]
		[Column("mixBatch")]
		public bool MixBatch {get; set;}

		[Required]
		[Display(Name = "Expirace")]
		[Column("useExpiration")]
		public bool UseExpiration {get; set;}

		[Required]
		[Display(Name = "Speciální vlastnosti")]
		[RawProperty("specFeaturesInt")]
		[IsFlag]
		[Column("specFeatures")]
		public SpecFeaturesEnum SpecFeatures {get; set;}

		[Required]
		[Display(Name = "Použít sériová čísla")]
		[Column("useSerialNumber")]
		public bool UseSerialNumber {get; set;}

		[Display(Name = "Poznámka")]
		[Column("userRemark")]
		public string UserRemark {get; set;}

		[MaxLength(64)]
		[Display(Name = "Značka")]
		[Column("brand")]
		public string Brand {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

        [MaxLength(64)]
        [Display(Name = "Znak")]
        [Column("sign")]
        public string Sign { get; set; }
    }
}

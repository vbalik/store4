using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_trucklist")]
	[PrimaryKey("truckid")]
	[ExplicitColumns]
	public partial class Truck
	{
		public enum TruckRegionEnum
		{
			[Display(Name = "Žádný")]
			None = 0,
			[Display(Name = "Čechy")]
			Czech = 1,
			[Display(Name = "Morava")]
			Moravia = 2,
			[Display(Name = "Praha")]
			Prag = 3,
		}

		[Required]
		[Display(Name = "ID")]
		[Column("truckID")]
		public int TruckID {get; set;}

		[Required]
		[MaxLength(32)]
		[Display(Name = "SPZ")]
		[Column("truckRegist")]
		public string TruckRegist {get; set;}

		[Required]
		[Display(Name = "Kapacita")]
		[Column("truckCapacity")]
		public decimal TruckCapacity {get; set;}

		[Required]
		[Display(Name = "Komentář")]
		[Column("truckComment")]
		public string TruckComment {get; set;}

		[Required]
		[Display(Name = "Region")]
		[Column("truckRegion")]
		public TruckRegionEnum TruckRegion {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

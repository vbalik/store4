using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;
using S4.Entities.StatusEngine;

namespace S4.Entities
{
    public partial class Booking
    {

        public const string USED = "USED";
        public const string VALID = "VALID";
        public const string DELETED = "DELETED";

        public static StatusSet Statuses = new StatusSet(s =>
        {

            s.AddStatus(USED, "Použito")
                .SetEnabledStatuses()
                .AddAction("Zrušit", ActionTypeEnum.ProcedureWithConfirmation, "BookingDeleteItem");

            s.AddStatus(VALID, "Platná")
                .SetEnabledStatuses()
                .AddAction("Zrušit", ActionTypeEnum.ProcedureWithConfirmation, "BookingDeleteItem");

            s.AddStatus(DELETED, "Zrušená")
                .SetEnabledStatuses();

        });
    }
}

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_exchangeerror")]
	[PrimaryKey("exchangeerrorid")]
	[ExplicitColumns]
	public partial class ExchangeError
	{
		[Required]
		[Display(Name = "ID")]
		[Column("exchangeErrorID")]
		public int ExchangeErrorID {get; set;}

		[Required]
		[MaxLength(64)]
		[Display(Name = "Verze")]
		[Column("appVersion")]
		public string AppVersion {get; set;}

		[Required]
		[MaxLength(128)]
		[Display(Name = "Třída chyby")]
		[Column("errorClass")]
		public string ErrorClass {get; set;}

		[Required]
		[Display(Name = "Datum a čas")]
		[Column("errorDateTime")]
		public DateTime ErrorDateTime {get; set;}

		[Required]
		[Display(Name = "Text chyby")]
		[Column("errorMessage")]
		public string ErrorMessage {get; set;}

		[Required]
		[MaxLength(128)]
		[Display(Name = "Job class")]
		[Column("jobClass")]
		public string JobClass {get; set;}

		[Required]
		[MaxLength(128)]
		[Display(Name = "Job name")]
		[Column("jobName")]
		public string JobName {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

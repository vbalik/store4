﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

using S4.Entities.StatusEngine;
using S4.Entities.Attributes;

namespace S4.Entities
{
    public partial class StockTakingHistory
    {
        #region events

        public const string STOCKTAKING_EVENT_SET = "SET";
        public const string STOCKTAKING_EVENT_CLEAR = "CLEAR";
        public const string STOCKTAKING_EVENT_POSITION_IS_OK = "OK";
        public const string STOCKTAKING_EVENT_SAVED = "SAVED";
        public const string STOCKTAKING_EVENT_CANCELED = "CANCELED";
        public const string STOCKTAKING_EVENT_TAKEN = "TAKEN";
        public const string STOCKTAKING_EVENT_EXTERNAL_READ = "EXTERNAL_READ";
        public const string STOCKTAKING_EVENT_ITEM_CHANGED = "CHANGED";
        public const string STOCKTAKING_EVENT_ITEM_ADDED = "ADDED";
        public const string STOCKTAKING_EVENT_ITEM_DELETED = "DELETED";
        //public const string STOCKTAKING_EVENT_ITEM_COPIED = "COPIED"; //TODO

        public static StatusSet Statuses = new StatusSet(s =>
        {
            s.AddStatus(STOCKTAKING_EVENT_SET, "Pozice nastavena");
            s.AddStatus(STOCKTAKING_EVENT_CLEAR, "Pozice smazána");
            s.AddStatus(STOCKTAKING_EVENT_POSITION_IS_OK, "Pozice převzata");
            s.AddStatus(STOCKTAKING_EVENT_SAVED, "Uloženo");
            s.AddStatus(STOCKTAKING_EVENT_CANCELED, "Zrušeno");
            s.AddStatus(STOCKTAKING_EVENT_TAKEN, "Vše převzato");
            s.AddStatus(STOCKTAKING_EVENT_EXTERNAL_READ, "Načten externí stav");
            s.AddStatus(STOCKTAKING_EVENT_ITEM_CHANGED, "Položka změněna");
            s.AddStatus(STOCKTAKING_EVENT_ITEM_ADDED, "Položka přidána");
            s.AddStatus(STOCKTAKING_EVENT_ITEM_DELETED, "Položka smazána");
            //s.AddStatus(STOCKTAKING_EVENT_ITEM_COPIED, "COPIED");
        });

        #endregion


        #region display helpers

        [Display(Name = "Paleta")]
        [RawColumnName("carrierNum")]
        [Core.DBTestIgnore]
        [MaxLength(32)]
        public string CarrierNumView
        {
            get
            {
                return (String.Equals(CarrierNum, StoreMoveItem.EMPTY_CARRIERNUM, StringComparison.InvariantCultureIgnoreCase)) ? null : CarrierNum;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                    CarrierNum = StoreMoveItem.EMPTY_CARRIERNUM;
                else
                    CarrierNum = value;
            }
        }


        [Display(Name = "Šarže")]
        [RawColumnName("batchNum")]
        [Core.DBTestIgnore]
        [MaxLength(32)]
        public string BatchNumView
        {
            get
            {
                return (String.Equals(BatchNum, StoreMoveItem.EMPTY_BATCHNUM, StringComparison.InvariantCultureIgnoreCase)) ? null : BatchNum;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                    BatchNum = StoreMoveItem.EMPTY_BATCHNUM;
                else
                    BatchNum = value;
            }
        }
        
        #endregion
    }
}

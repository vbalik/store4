﻿using System;
using System.Collections.Generic;

using S4.Entities.StatusEngine;
using S4.Entities.XtraData;

namespace S4.Entities
{
    public partial class Article
    {
        
        public const string AR_STATUS_OK = "AR_OK";
        public const string AR_STATUS_DISABLED = "AR_DISABLED";

        public const string AR_EVENT_UPDATE = "AR_UPDATE";
        public const string AR_EVENT_BARCODE_SET = "AR_BCODE_SET";
        public const string AR_EVENT_BARCODE_DEL = "AR_BCODE_DEL";
        public const string AR_EVENT_BARCODE_CONFIRMED = "AR_BCODE_CONF";
        public const string AR_EVENT_BARCODE_CREATED = "AR_BCODE_CREATED";

        public static StatusSet Statuses = new StatusSet(s =>
        {
            var editExpBatchAction = new ActionDef()
            {
                Name = "Nastavení šarží a expirací",
                ActionType = ActionTypeEnum.Dialog,
                ActionProcedure = "editExpBatch_dialog"
            };
            s.AddStatus(AR_STATUS_OK, "OK")
                .AddAction(editExpBatchAction);
            s.AddStatus(AR_STATUS_DISABLED, "Zrušeno")
                .AddAction(editExpBatchAction)
                .SetColor(System.Drawing.Color.Gray);

            s.AddEvent(AR_EVENT_UPDATE, "Změněno");
            s.AddEvent(AR_EVENT_BARCODE_SET, "Nastaven čárový kód");
            s.AddEvent(AR_EVENT_BARCODE_DEL, "Smazán čárový kód");
            s.AddEvent(AR_EVENT_BARCODE_CONFIRMED, "Čárový kód potvrzen");
            s.AddEvent(AR_EVENT_BARCODE_CREATED, "Čárový kód vygenerován");
        });


        public string ArticleInfo
        {
            get => $"{ArticleCode} - {ArticleDesc}";
        }

        public int SpecFeaturesInt
        {
            get { return (int)SpecFeatures; }
        }


        /// <summary>
        /// Xtra data definition
        /// </summary>

        #region extra data
        
        public const string XTRA_UN_KOD = "UN_KOD";
        public const string XTRA_OFICIALNI_NAZEV = "OFICIALNI_NAZEV";
        public const string XTRA_BEZPECNOSTI_ZNACKA = "BEZPEC_ZNACKA";
        public const string XTRA_OBALOVA_SKUPINA = "OBALOVA_SKUPINA";
        public const string XTRA_KOD_TUNELU = "KOD_TUNELU";
        public const string XTRA_DRUH_OBALU = "DRUH_OBALU";
        public const string XTRA_PREPRAVNI_KATEGORIE = "PREPRAVNI_KATEG";
        public const string XTRA_OHROZUJE_PROSTREDI = "OHROZUJE_PROSTR";
        public const string XTRA_OBJEM_KOEF = "OBJEM_KOEF";
        public const string XTRA_MERNA_JEDNOTKA = "MERNA_JEDNOTKA";

        public static XtraDataDict XtraDataDict = new XtraDataDict(x =>
        {
            x.AddField(XTRA_UN_KOD, "UN číslo", typeof(string));
            x.AddField(XTRA_OFICIALNI_NAZEV, "Oficiální název látky", typeof(string));
            x.AddField(XTRA_BEZPECNOSTI_ZNACKA, "Číslo bezpečnostní značky", typeof(string));
            x.AddField(XTRA_OBALOVA_SKUPINA, "Obalová skupina", typeof(string));
            x.AddField(XTRA_KOD_TUNELU, "Kód omezení tunelu", typeof(string));
            x.AddField(XTRA_DRUH_OBALU, "Druh obalu", typeof(string));
            x.AddField(XTRA_PREPRAVNI_KATEGORIE, "Přepravní kategorie", typeof(string));
            x.AddField(XTRA_OHROZUJE_PROSTREDI, "Ohrožující životní prostředí", typeof(bool));
            x.AddField(XTRA_OBJEM_KOEF, "Koeficient objemu", typeof(float));
            x.AddField(XTRA_MERNA_JEDNOTKA, "Měrná jednotka", typeof(string));
        });

        [Core.DBTestIgnore]
        public XtraDataProxy<ArticleXtra> XtraData { get; } = new XtraDataProxy<ArticleXtra>(XtraDataDict);

        #endregion
    }
}

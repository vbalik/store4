using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_stocktaking_history")]
	[PrimaryKey("stotakhistid")]
	[ExplicitColumns]
	public partial class StockTakingHistory
	{
		[Required]
		[Column("stotakHistID")]
		public int StotakHistID {get; set;}

		[Required]
		[Column("stotakID")]
		public int StotakID {get; set;}

		[Required]
		[MaxLength(16)]
		[Display(Name = "Událost")]
		[StatusSet("Statuses", true)]
		[Column("eventCode")]
		public string EventCode {get; set;}

		[Column("positionID")]
		public int? PositionID {get; set;}

		[Column("artPackID")]
		public int? ArtPackID {get; set;}

		[Required]
		[MaxLength(32)]
		[Display(Name = "Paleta")]
		[Column("carrierNum")]
		public string CarrierNum {get; set;}

		[Display(Name = "Množství")]
		[Column("quantity")]
		public decimal? Quantity {get; set;}

		[MaxLength(32)]
		[Display(Name = "Šarže")]
		[Column("batchNum")]
		public string BatchNum {get; set;}

		[Display(Name = "Expirace")]
		[Column("expirationDate")]
		public DateTime? ExpirationDate {get; set;}

		[Required]
		[Display(Name = "Zadáno")]
		[Column("entryDateTime")]
		public DateTime EntryDateTime {get; set;}

		[Required]
		[MaxLength(16)]
		[Display(Name = "Uživatel")]
		[Column("entryUserID")]
		public string EntryUserID {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

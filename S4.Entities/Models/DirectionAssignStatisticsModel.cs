﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Entities.Models
{
    public class DirectionAssignStatisticsModel
    {
        public string JobID { get; set; }
        public string WorkerID { get; set; }        
        public string WorkerName { get; set; }
        public int JobsCnt { get; set; }
    }
}

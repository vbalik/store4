﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Entities.Models
{
    public class DirectionModel : Direction
    {
        public List<DirectionHistory> History { get; set; }
        public List<DirectionItem> Items { get; set; }
        public List<DirectionAssign> Assigns { get; set; }


        #region report helpers

        public Partner Partner { get; set; }
        public PartnerAddress PartnerAddress { get; set; }

        public string Partner_partnerName
        {
            get
            {
                return Partner?.PartnerName;
            }
        }

        public string Delivery_city
        {
            get
            {
                if (PartnerAddress != null)
                    return PartnerAddress.AddrCity;
                else if (Partner != null)
                    return Partner.PartnerCity;
                else
                    return null;
            }
        }

        public string DocInfo
        {
            get
            {
                return $"{DocNumPrefix}/{DocYear}/{DocNumber}";
            }
        }

        #endregion

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Entities.Models
{
    public class SectionModel : Section
    {
        public HashSet<int> PositionIDs { get; set; } = new HashSet<int>();
    }
}

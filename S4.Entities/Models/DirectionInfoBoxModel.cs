﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Entities.Models
{
    public class DirectionInfoBoxModel
    {
        //assigns
        public string DispatchWorker { get; set; }
        public string CheckWorker { get; set; }
        public string ReceiveWorker { get; set; }
        public string StoreInWorker { get; set; }
        //xtra
        public string PosCode { get; set; }
        public string CheckMethod { get; set; }
        public string ExpedTruck { get; set; }
        public WeightBoxCount WeightBoxCount { get; set; }
        public string TransportationType { get; set; }
        public List<string> ParcelRefNumbers { get; set; } = new List<string>();
        public List<string> TrackURLs { get; set; } = new List<string>();
        public string Warning { get; set; }
        public bool RequestResponseFile { get; set; } = false;
        
    }

    public class WeightBoxCount
    {
        public decimal? Weight { get; set; }
        public int? BoxCount { get; set; }
    }
}

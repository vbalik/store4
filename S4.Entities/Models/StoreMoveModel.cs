﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Entities.Models
{
    public class StoreMoveModel : StoreMove
    {
        public List<StoreMoveItemModel> Items { get; set; }

    }
}

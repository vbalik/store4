﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities.Models
{
    [ExplicitColumns]
    public partial class StotakArticleDifferences : StotakDifferences
    {
        [Display(Name = "Cena Mj")]
        [Column("unitPrice")]
        public decimal UnitPrice { get; set; }

        [Display(Name = "Cena rozdíl")]
        [Column("priceDiff")]
        public decimal PriceDiff { get; set; }
    }
}

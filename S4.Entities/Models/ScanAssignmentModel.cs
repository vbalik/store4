﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Entities.Models
{
    public class ScanAssignmentModel
    {
        public string WorkerID { get; set; }
        public string JobID { get; set; }
        public int CntDocs { get; set; }
        public int CntItems { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities.Models
{
    public class DirectionsForManifestModel : Views.DirectionView
    {
        [Display(Name = "Balíků")]
        [Column("parcels")]
        public short Parcels { get; set; }
    }
}

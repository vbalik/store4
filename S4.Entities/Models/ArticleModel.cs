﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace S4.Entities.Models
{
    public class ArticleModel : Article
    {
        public List<ArticlePacking> Packings { get; set; } = new List<ArticlePacking>();

        public List<ArticlePackingBarCode> PackingBarCodes { get; set; } = new List<ArticlePackingBarCode>();

        public bool ShowUserRemark => string.IsNullOrEmpty(UserRemark);

        public string PackDesc => (Packings ?? new List<ArticlePacking>()).SingleOrDefault(x => x.MovablePack).PackDesc ?? string.Empty;

    }
}

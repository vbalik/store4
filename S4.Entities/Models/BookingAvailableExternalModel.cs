﻿using NPoco;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Xml.Linq;

namespace S4.Entities.Models
{
    public class BookingAvailableExternalModel
    {
        public int ArtPackID { get; set; }
        public int ArticleID { get; set; }
        public string ArticleCode { get; set; }
        public string ArticleDesc { get; set; }
        public string BatchNum { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public decimal Quantity { get; set; }
        public int StoMoveLotID { get; set; }
    }
}

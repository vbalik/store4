﻿using NPoco;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace S4.Entities.Models
{
    public class StotakArticleModel : Article
    {
        [ResultColumn]
        public int ArtPackID { get; set; }
    }
}

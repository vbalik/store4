﻿using System;
using System.Collections.Generic;
using System.Text;

using S4.Entities;

namespace S4.Entities.Models
{
    public class DeliveryDirectionModel : DeliveryDirection
    {
        public IEnumerable<DeliveryDirectionItem> Items { get; set; }
    }
}

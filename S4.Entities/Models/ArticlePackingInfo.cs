﻿using System.Linq;

namespace S4.Entities.Models
{
    public class ArticlePackingInfo : Article
    {
        public static ArticlePackingInfo FromArticleModel(ArticleModel articleModel)
        {
            return new ArticlePackingInfo()
            {
#pragma warning disable CA1062 // Validate arguments of public methods
                ArticleID = articleModel.ArticleID,
#pragma warning restore CA1062 // Validate arguments of public methods
                ArticleCode = articleModel.ArticleCode,
                ArticleDesc = articleModel.ArticleDesc,
                ArticleStatus = articleModel.ArticleStatus,
                ArticleType = articleModel.ArticleType,
                Source_id = articleModel.Source_id,
                SectID = articleModel.SectID,
                UnitDesc = articleModel.UnitDesc,
                UseBatch = articleModel.UseBatch,
                MixBatch = articleModel.MixBatch,
                UseExpiration = articleModel.UseExpiration,
                SpecFeatures = articleModel.SpecFeatures,

                ArticlePacking = articleModel.Packings.FirstOrDefault(p => p.MovablePack)
            };
        }

        public ArticlePacking ArticlePacking { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Entities.Models
{
    public class PositionByCategoryModel
    {
        public Position.PositionCategoryEnum PositionCategory { get; set; }
        public HashSet<int> PositionIDs { get; set; } = new HashSet<int>();
    }
}

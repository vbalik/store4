﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Entities.Models
{
    public class LabeledValue
    {
        public LabeledValue()
        {

        }

        public LabeledValue(string key, string label, object value)
        {
            Key = key; Label = label; Value = value;
        }

        public string Key { get; set; }
        public string Label { get; set; }
        public object Value { get; set; }
    }
}

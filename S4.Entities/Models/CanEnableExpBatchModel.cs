﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Entities.Models
{
    public class CanEnableExpBatchModel
    {
        public bool CanEnable { get; set; }
        public string Reason { get; set; }
    }
}

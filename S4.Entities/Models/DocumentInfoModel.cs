﻿namespace S4.Entities.Models
{
    public class DocumentInfoModel
    {
        public int ID { get; set; }
        public string Description { get; set; }
        public string XtraDescription { get; set; }
        public string DocStatus { get; set; }
    }
}

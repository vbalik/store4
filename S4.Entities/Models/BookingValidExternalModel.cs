﻿using NPoco;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Xml.Linq;

namespace S4.Entities.Models
{
    public class BookingValidExternalModel
    {
        public string ArticleCode { get; set; }
        public string ArticleDesc { get; set; }
        public string BatchNum { get; set; }
        public decimal RequiredQuantity { get; set; }
        public DateTime ExpirationDate { get; set; }
        public string PartnerName { get; set; }
        public DateTime BookingTimeout { get; set; }
        public string WorkerName { get; set; }
        public DateTime EntryDateTime { get; set; }
        public int StoMoveLotID { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Entities.Models
{
    public class PositionModel : Position
    {
        public List<PositionSection> PositionSections { get; set; } = new List<PositionSection>();
    }
}

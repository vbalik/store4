﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities.Models
{
    [ExplicitColumns]
    public partial class StotakDifferences
    {
        [Column("artPackID")]
        public int ArtPackID { get; set; }

        [Column("articleID")]
        public int ArticleID { get; set; }

        [Display(Name = "Kód")]
        [Column("articleCode")]
        public string ArticleCode { get; set; }

        [Display(Name = "Popis")]
        [Column("articleDesc")]
        public string ArticleDesc { get; set; }

        [Display(Name = "Balení")]
        [Column("packDesc")]
        public string PackDesc { get; set; }

        [Display(Name = "Skladem")]
        [Column("quantOnSt")]
        public decimal QuantOnSt { get; set; }

        [Display(Name = "Inventura")]
        [Column("quantStTa")]
        public decimal QuantStTa { get; set; }

        [Display(Name = "Rozdíl")]
        [Column("quantDiff")]
        public decimal QuantDiff { get; set; }
    }
}

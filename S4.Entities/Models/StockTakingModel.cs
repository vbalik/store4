﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Entities.Models
{
    public class StockTakingModel : StockTaking
    {
        public List<StockTakingItem> StockTakingItems { get; set; } = new List<StockTakingItem>();
        public List<StockTakingPosition> StockTakingPositions { get; set; } = new List<StockTakingPosition>();
    }
}

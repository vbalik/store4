﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Entities.Models
{
    public class StoreMoveItemModel : StoreMoveItem
    {
        public Position Position { get; set; }
        public StoreMoveLot StoreMoveLot { get; set; }
        public ArticlePacking ArticlePacking { get; set; }
        public Article Article { get; set; }
    }
}

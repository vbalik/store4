﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4.Entities.Models
{
    public class GetListModel
    {
        public string TypeName { get; set; }
        public string Where { get; set; }
        public string Order { get; set; }
        public object[] Args { get; set; }
    }
}

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_direction_xtra")]
	[PrimaryKey("directionexid")]
	[ExplicitColumns]
	public partial class DirectionXtra
	{
		[Required]
		[Column("directionExID")]
		public int DirectionExID {get; set;}

		[Required]
		[Column("directionID")]
		public int DirectionID {get; set;}

		[Required]
		[MaxLength(16)]
		[Column("xtraCode")]
		public string XtraCode {get; set;}

		[Required]
		[Column("docPosition")]
		public int DocPosition {get; set;}

		[Required]
		[Column("xtraValue")]
		public string XtraValue {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

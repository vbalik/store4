using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_sectionlist")]
	[PrimaryKey("sectid", AutoIncrement = false)]
	[ExplicitColumns]
	public partial class Section
	{
		[Required]
		[MaxLength(16)]
		[Display(Name = "ID")]
		[Column("sectID")]
		public string SectID {get; set;}

		[Required]
		[MaxLength(64)]
		[Display(Name = "Sekce")]
		[Column("sectDesc")]
		public string SectDesc {get; set;}

		[MaxLength(32)]
		[Column("source_id")]
		public string Source_id {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

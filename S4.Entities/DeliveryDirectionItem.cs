using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_deliverydirection_items")]
	[PrimaryKey("deliverydirectionid,directionid")]
	[ExplicitColumns]
	public partial class DeliveryDirectionItem
	{
		[Required]
		[Display(Name = "ID")]
		[Column("deliveryDirectionID")]
		public int DeliveryDirectionID {get; set;}

		[Required]
		[Display(Name = "DirectionID")]
		[Column("parcelPosition")]
		public int ParcelPosition {get; set;}

		[MaxLength(128)]
		[Display(Name = "Referenční číslo balíku")]
		[Column("parcelRefNumber")]
		public string ParcelRefNumber {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

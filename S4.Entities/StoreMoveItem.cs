using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_storemove_items")]
	[PrimaryKey("stomoveitemid")]
	[ExplicitColumns]
	public partial class StoreMoveItem
	{
		[Required]
		[Display(Name = "ID")]
		[Column("stoMoveItemID")]
		public int StoMoveItemID {get; set;}

		[Required]
		[Column("stoMoveID")]
		public int StoMoveID {get; set;}

		[Required]
		[Display(Name = "Poz.")]
		[Column("docPosition")]
		public int DocPosition {get; set;}

		[Required]
		[Display(Name = "Platnost")]
		[Column("itemValidity")]
		public byte ItemValidity {get; set;}

		[Required]
		[Display(Name = "+/-")]
		[Column("itemDirection")]
		public int ItemDirection {get; set;}

		[Required]
		[Column("stoMoveLotID")]
		public int StoMoveLotID {get; set;}

		[Required]
		[Column("positionID")]
		public int PositionID {get; set;}

		[Required]
		[MaxLength(32)]
		[Display(Name = "Paleta")]
		[Column("carrierNum")]
		public string _carrierNum {get; set;}

		[Required]
		[Display(Name = "Množství")]
		[Column("quantity")]
		public decimal Quantity {get; set;}

		[Required]
		[Column("parent_docPosition")]
		public int Parent_docPosition {get; set;}

		[Required]
		[Column("brotherID")]
		public int BrotherID {get; set;}

		[Required]
		[Display(Name = "Zadáno")]
		[Column("entryDateTime")]
		public DateTime EntryDateTime {get; set;}

		[Required]
		[MaxLength(16)]
		[Display(Name = "Uživatel")]
		[RawColumnName("mit.entryUserID")]
		[Column("entryUserID")]
		public string EntryUserID {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_direction")]
	[PrimaryKey("directionid")]
	[ExplicitColumns]
	public partial class Direction
	{
		[Required]
		[Display(Name = "ID")]
		[Column("directionID")]
		public int DirectionID {get; set;}

		[Required]
		[MaxLength(16)]
		[Display(Name = "Stav")]
		[StatusSet("Statuses")]
		[Column("docStatus")]
		public string DocStatus {get; set;}

		[Required]
		[MaxLength(16)]
		[Display(Name = "Prefix")]
		[Column("docNumPrefix")]
		public string DocNumPrefix {get; set;}

		[Required]
		[Column("docDirection")]
		public int DocDirection {get; set;}

		[Required]
		[MaxLength(2)]
		[Display(Name = "Rok")]
		[Column("docYear")]
		public string DocYear {get; set;}

		[Required]
		[Display(Name = "Číslo")]
		[Column("docNumber")]
		public int DocNumber {get; set;}

		[Required]
		[Display(Name = "Datum")]
		[Column("docDate")]
		public DateTime DocDate {get; set;}

		[Required]
		[Column("partnerID")]
		public int PartnerID {get; set;}

		[Required]
		[Column("partAddrID")]
		public int PartAddrID {get; set;}

		[Required]
		[Column("prepare_directionID")]
		public int Prepare_directionID {get; set;}

		[Column("partnerRef")]
		public string PartnerRef {get; set;}

		[Column("docRemark")]
		public string DocRemark {get; set;}

		[Required]
		[Column("entryDateTime")]
		public DateTime EntryDateTime {get; set;}

		[Required]
		[MaxLength(16)]
		[Column("entryUserID")]
		public string EntryUserID {get; set;}

		[Column("invoiceID")]
		public int? InvoiceID {get; set;}

		[Required]
		[Display(Name = "Nezpracovávat")]
		[Column("doNotProcess")]
		public bool DoNotProcess {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

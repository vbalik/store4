using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_printerlocation")]
	[PrimaryKey("printerlocationid")]
	[ExplicitColumns]
	public partial class PrinterLocation
	{
		[Required]
		[Column("printerLocationID")]
		public int PrinterLocationID {get; set;}

		[MaxLength(128)]
		[Column("printerLocationDesc")]
		public string PrinterLocationDesc {get; set;}

		[Required]
		[MaxLength(256)]
		[Column("printerPath")]
		public string PrinterPath {get; set;}

		[Required]
		[Column("printerTypeID")]
		public int PrinterTypeID {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

﻿using Newtonsoft.Json;
using NPoco;
using System;


namespace S4.Entities
{
    public partial class Prefix
    {
        [Core.DBTestIgnore]
        public bool IsNew { get; set; }

        [Core.DBTestIgnore]
        [Column("prefixSettings")]
        [JsonIgnore]
        public string _prefixSettingsJson
        {
            get
            {
                if (PrefixSettings == null)
                    return null;
                else
                    return JsonConvert.SerializeObject(PrefixSettings, Formatting.Indented);
            }

            set
            {
                if (String.IsNullOrWhiteSpace(value))
                    PrefixSettings = new Settings.PrefixSettings();
                else
                    PrefixSettings = JsonConvert.DeserializeObject<Settings.PrefixSettings>(value);
            }
        }

        [Core.DBTestIgnore]
        public Settings.PrefixSettings PrefixSettings { get; set; } = new Settings.PrefixSettings();

    }
}

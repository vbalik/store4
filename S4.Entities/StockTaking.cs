using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_stocktaking")]
	[PrimaryKey("stotakid")]
	[ExplicitColumns]
	public partial class StockTaking
	{
		[Required]
		[Display(Name = "ID")]
		[Column("stotakID")]
		public int StotakID {get; set;}

		[Required]
		[MaxLength(16)]
		[Display(Name = "Stav")]
		[StatusSet("Statuses")]
		[Column("stotakStatus")]
		public string StotakStatus {get; set;}

		[Required]
		[Display(Name = "Popis")]
		[Column("stotakDesc")]
		public string StotakDesc {get; set;}

		[Required]
		[Display(Name = "Začátek")]
		[Column("beginDate")]
		public DateTime BeginDate {get; set;}

		[Display(Name = "Konec")]
		[Column("endDate")]
		public DateTime? EndDate {get; set;}

		[Required]
		[Column("entryDateTime")]
		public DateTime EntryDateTime {get; set;}

		[Required]
		[MaxLength(16)]
		[Column("entryUserID")]
		public string EntryUserID {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Entities
{
    public partial class StoreMoveXtra : XtraData.IXtraDataStore<StoreMoveXtra>
    {

        #region IXtraDataStore

        public (string, int) GetIDs() => (this.XtraCode, 0);


        public string GetValue() => this.XtraValue;


        public void Set(string code, int position, string strValue)
        {
            XtraCode = code;
            XtraValue = strValue;
        }


        public void SetParentID(int parentID)
        {
            StoMoveID = parentID;
        }

        #endregion
    }
}

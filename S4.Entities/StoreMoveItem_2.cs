﻿using System;
using System.ComponentModel.DataAnnotations;

using S4.Entities.StatusEngine;
using S4.Entities.Attributes;

namespace S4.Entities
{
    public partial class StoreMoveItem
    {
        public const short MI_DIRECTION_IN = 1;
        public const short MI_DIRECTION_OUT = -1;

        public const byte MI_VALIDITY_NOT_VALID = 0;
        public const byte MI_VALIDITY_VALID = 100;
        public const byte MI_VALIDITY_CANCELED = 255;

        public const int NO_BROTHER_ID = -1;
        public const int NO_PARENT_DOC_POSITION = -1;


        public const string EMPTY_BATCHNUM = "_NONE_";
        public const string EMPTY_CARRIERNUM = "_NONE_";
        public static readonly DateTime EMPTY_EXPIRATION = new DateTime(1900, 1, 1, 0, 0, 0, DateTimeKind.Local);



        public StoreMoveItem()
        {
            _carrierNum = EMPTY_CARRIERNUM;
            BrotherID = NO_BROTHER_ID;
            Parent_docPosition = NO_PARENT_DOC_POSITION;
        }



        #region display helpers

        [MaxLength(32)]
        [Display(Name = "Paleta")]
        public string CarrierNum
        {
            get
            {
                return (String.Equals(_carrierNum, StoreMoveItem.EMPTY_CARRIERNUM, StringComparison.InvariantCultureIgnoreCase)) ? null : _carrierNum;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                    _carrierNum = StoreMoveItem.EMPTY_CARRIERNUM;
                else
                    _carrierNum = value;
            }
        }

        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_clienterror")]
	[PrimaryKey("clienterrorid")]
	[ExplicitColumns]
	public partial class ClientError
	{
		[Required]
		[Display(Name = "ID")]
		[Column("clientErrorID")]
		public int ClientErrorID {get; set;}

		[Required]
		[MaxLength(64)]
		[Display(Name = "Verze")]
		[Column("appVersion")]
		public string AppVersion {get; set;}

		[Required]
		[MaxLength(128)]
		[Display(Name = "Třída chyby")]
		[Column("errorClass")]
		public string ErrorClass {get; set;}

		[Required]
		[Display(Name = "Datum a čas")]
		[Column("errorDateTime")]
		public DateTime ErrorDateTime {get; set;}

		[Required]
		[Display(Name = "Text")]
		[Column("errorMessage")]
		public string ErrorMessage {get; set;}

		[Required]
		[Display(Name = "Cesta")]
		[Column("path")]
		public string Path {get; set;}

		[Display(Name = "Data")]
		[Column("statusData")]
		public string StatusData {get; set;}

		[Display(Name = "Body")]
		[Column("requestBody")]
		public string RequestBody {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

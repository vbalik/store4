﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using S4.Entities.StatusEngine;

namespace S4.Entities
{
    public partial class DeliveryDirection
    {
        #region delivery statuses

        public const string DD_STATUS_OK = "DD_OK";
        public const string DD_STATUS_CANCELED = "DD_CANCELED";

        public static StatusSet Statuses = new StatusSet(s =>
        {
            s.AddStatus(DD_STATUS_OK, "OK");
            s.AddStatus(DD_STATUS_CANCELED, "Zrušeno");

        });

        #endregion

        #region delivery providers

        private const string _TTYPE_DPD = "Doprava DPD";
        private const string _TTYPE_DPD_SK = "Doprava DPD SK";
        private const string _TTYPE_DPD_PICKUP = "Doprava PickUp";
        private const string _TTYPE_DPD_PICKUP_SK = "Doprava PickUp SK";
        private const string _TTYPE_PPL = "Doprava PPL";
        private const string _TTYPE_PPL_PARCEL_SHOP = "Doprava PPL Parcel Shop";
        private const string _TTYPE_PPL_EVENING_DEL = "Doprava PPL večerní doručení";
        private const string _TTYPE_CPOST = "Doprava Česká pošta";

        public DeliveryProviderEnum TTYPEToDeliveryProvider(string tType)
        {
            switch (tType)
            {
                case _TTYPE_DPD:
                case _TTYPE_DPD_SK:
                case _TTYPE_DPD_PICKUP_SK:
                case _TTYPE_DPD_PICKUP: 
                    //return DeliveryProviderEnum.DPD;
                    return DeliveryProviderEnum.DPDNew;

                case _TTYPE_PPL: 
                case _TTYPE_PPL_PARCEL_SHOP: 
                case _TTYPE_PPL_EVENING_DEL: 
                    return DeliveryProviderEnum.PPL;

                case _TTYPE_CPOST: 
                    return DeliveryProviderEnum.CPOST;

                default: 
                    return DeliveryProviderEnum.NONE;
            }
        }

        #endregion

        #region payment

        private const string _PTYPE_SUPP = "Dodavatelem";
        private const string _PTYPE_ACC = "Na bankovní účet";
        private const string _PTYPE_CHOD = "Platba dobírkou";
        private const string _PTYPE_CAOD = "Platba dobírkou kartou";
        private const string _PTYPE_CARD = "Platba platební kartou";
        private const string _PTYPE_ADV = "Zálohovým listem";

        public DeliveryPaymentEnum PTYPEToDeliveryPayment(string pType)
        {
            switch (pType)
            {
                case _PTYPE_SUPP: return DeliveryPaymentEnum.SUPP;
                case _PTYPE_ACC: return DeliveryPaymentEnum.ACC;
                case _PTYPE_CHOD: return DeliveryPaymentEnum.CHOD;
                case _PTYPE_CAOD: return DeliveryPaymentEnum.CAOD;
                case _PTYPE_CARD: return DeliveryPaymentEnum.CARD;
                case _PTYPE_ADV: return DeliveryPaymentEnum.ADV;
                default: throw new NotImplementedException($"Payment type \"{pType}\" is not implemented!");
            }
        }

        public string DeliveryPaymentToPTYPE(DeliveryPaymentEnum deliveryPayment)
        {
            switch (deliveryPayment)
            {
                case DeliveryPaymentEnum.SUPP: return _PTYPE_SUPP;
                case DeliveryPaymentEnum.ACC: return _PTYPE_ACC;
                case DeliveryPaymentEnum.CHOD: return _PTYPE_CHOD;
                case DeliveryPaymentEnum.CAOD: return _PTYPE_CAOD;
                case DeliveryPaymentEnum.CARD: return _PTYPE_CARD;
                case DeliveryPaymentEnum.ADV: return _PTYPE_ADV;
                default: throw new NotImplementedException($"Payment type \"{deliveryPayment}\" is not implemented!");
            }
        }

        public bool IsPickUp(string tType)
        {
            return tType == _TTYPE_DPD_PICKUP || tType == _TTYPE_DPD_PICKUP_SK || tType == _TTYPE_PPL_PARCEL_SHOP;
        }

        public bool IsSK(string tType)
        {
            return tType == _TTYPE_DPD_SK || tType == _TTYPE_DPD_PICKUP_SK;
        }

        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_direction_assign")]
	[PrimaryKey("directassignid")]
	[ExplicitColumns]
	public partial class DirectionAssign
	{
		[Required]
		[Column("directAssignID")]
		public int DirectAssignID {get; set;}

		[Required]
		[Column("directionID")]
		public int DirectionID {get; set;}

		[Required]
		[MaxLength(4)]
		[Column("jobID")]
		public string JobID {get; set;}

		[Required]
		[MaxLength(16)]
		[Column("workerID")]
		public string WorkerID {get; set;}

		[Required]
		[Column("assignParams")]
		public string AssignParams {get; set;}

		[Required]
		[Column("entryDateTime")]
		public DateTime EntryDateTime {get; set;}

		[Required]
		[MaxLength(16)]
		[Column("entryUserID")]
		public string EntryUserID {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

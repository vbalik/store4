using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_certificate")]
	[PrimaryKey("certificateid")]
	[ExplicitColumns]
	public partial class Certificate
	{
		[Required]
		[Display(Name = "ID")]
		[Column("certificateID")]
		public int CertificateID {get; set;}

		[Required]
		[MaxLength(50)]
		[Display(Name = "Typ certifikátu")]
		[Column("certificateType")]
		public string CertificateType {get; set;}

		[Required]
		[Display(Name = "Datum expirace od")]
		[Column("expirationDateFrom")]
		public DateTime ExpirationDateFrom {get; set;}

		[Required]
		[Display(Name = "Datum expirace do")]
		[Column("expirationDateTo")]
		public DateTime ExpirationDateTo {get; set;}

		[Required]
		[Column("certificateBinary")]
		public byte[] CertificateBinary {get; set;}

		[MaxLength(100)]
		[Display(Name = "Heslo certifikátu")]
		[Column("certificatePassword")]
		public string CertificatePassword {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_storemove_history")]
	[PrimaryKey("stomovehistid")]
	[ExplicitColumns]
	public partial class StoreMoveHistory
	{
		[Required]
		[Display(Name = "ID")]
		[Column("stoMoveHistID")]
		public int StoMoveHistID {get; set;}

		[Required]
		[Column("stoMoveID")]
		public int StoMoveID {get; set;}

		[Required]
		[Display(Name = "Poz.")]
		[Column("docPosition")]
		public int DocPosition {get; set;}

		[Required]
		[MaxLength(16)]
		[Display(Name = "Stav")]
		[StatusSet("Statuses", true)]
		[Column("eventCode")]
		public string EventCode {get; set;}

		[Display(Name = "Data")]
		[Column("eventData")]
		public string EventData {get; set;}

		[Required]
		[Display(Name = "Zadáno")]
		[Column("entryDateTime")]
		public DateTime EntryDateTime {get; set;}

		[Required]
		[MaxLength(16)]
		[Display(Name = "Uživatel")]
		[Column("entryUserID")]
		public string EntryUserID {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

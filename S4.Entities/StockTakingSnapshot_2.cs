﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

using S4.Entities.Attributes;

namespace S4.Entities
{
    public partial class StockTakingSnapshot
    {
        [Display(Name = "Paleta")]
        [RawColumnName("carrierNum")]
        [Core.DBTestIgnore]
        [MaxLength(32)]
        public string CarrierNumView
        {
            get
            {
                return (String.Equals(CarrierNum, StoreMoveItem.EMPTY_CARRIERNUM, StringComparison.InvariantCultureIgnoreCase)) ? null : CarrierNum;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                    CarrierNum = StoreMoveItem.EMPTY_CARRIERNUM;
                else
                    CarrierNum = value;
            }
        }

        [Display(Name = "Šarže")]
        [RawColumnName("batchNum")]
        [Core.DBTestIgnore]
        [MaxLength(32)]
        public string BatchNumView
        {
            get
            {
                return (String.Equals(BatchNum, StoreMoveItem.EMPTY_BATCHNUM, StringComparison.InvariantCultureIgnoreCase)) ? null : BatchNum;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                    BatchNum = StoreMoveItem.EMPTY_BATCHNUM;
                else
                    BatchNum = value;
            }
        }
    }
}

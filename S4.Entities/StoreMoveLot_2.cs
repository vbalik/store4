using System;
using System.ComponentModel.DataAnnotations;


namespace S4.Entities
{
    public partial class StoreMoveLot
    {

        public StoreMoveLot()
        {
            _batchNum = StoreMoveItem.EMPTY_BATCHNUM;
            _expirationDate = StoreMoveItem.EMPTY_EXPIRATION;
        }



        [Required]
        [MaxLength(32)]
        public string BatchNum
        {
            get
            {
                return (String.Equals(_batchNum, StoreMoveItem.EMPTY_BATCHNUM, StringComparison.InvariantCultureIgnoreCase)) ? null : _batchNum;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                    _batchNum = StoreMoveItem.EMPTY_BATCHNUM;
                else
                    _batchNum = value;
            }
        }


        [Required]
        public DateTime? ExpirationDate
        {
            get
            {
                return (DateTime.Equals(_expirationDate, StoreMoveItem.EMPTY_EXPIRATION)) ? (DateTime ?)null : _expirationDate;
            }
            set
            {
                if (value == null || !value.HasValue)
                    _expirationDate = StoreMoveItem.EMPTY_EXPIRATION;
                else
                    _expirationDate = value.Value;
            }
        }
    }
}

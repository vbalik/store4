﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.StatusEngine;
using S4.Entities.Attributes;

namespace S4.Entities
{

    public partial class StoreMove
    {
        public const byte MO_DOCTYPE_RECEIVE = 1;
        public const byte MO_DOCTYPE_STORE_IN = 3;
        public const byte MO_DOCTYPE_MOVE = 4;
        public const byte MO_DOCTYPE_DISPATCH = 5;
        public const byte MO_DOCTYPE_EXPED = 6;
        public const byte MO_DOCTYPE_DIR_MOVE = 9;
        public const byte MO_DOCTYPE_STOCKTAKING = 10;

        public const int NO_PARENT_ID = -1;
      
        public const string MO_PREFIX_RECEIVE = "MREC";
        public const string MO_PREFIX_STORE_IN = "MSTI";
        public const string MO_PREFIX_DIR_MOVE = "MDIM";
        public const string MO_PREFIX_DISPATCH = "MDIS";
        public const string MO_PREFIX_EXPED = "MEXE";
        public const string MO_PREFIX_MOVE = "MOVE";
        public const string MO_PREFIX_STOCKTAKING = "STOK";
        public const string MO_PREFIX_ARTCHANGE = "ARCH";
        public const string MO_PREFIX_CARIER_REMOVED = "CREM";

        public const string MO_STATUS_CANCEL = "CANC";

        //	receive
        public const string MO_STATUS_NEW_RECEIVE = "NREC";
        public const string MO_STATUS_SAVED_RECEIVE = "SREC";
        //	store in
        public const string MO_STATUS_NEW_STOIN = "NSTI";
        public const string MO_STATUS_SAVED_STOIN = "SSTI";
        //  dir move
        public const string MO_STATUS_NEW_DIR_MOVE = "NDIM";
        public const string MO_STATUS_SAVED_DIR_MOVE = "SDIM";
        //	dispatch
        public const string MO_STATUS_NEW_DISPATCH = "NDIS";
        public const string MO_STATUS_SAVED_DISPATCH = "SDIS";
        //	exped
        public const string MO_STATUS_SAVED_EXPED = "SEXP";
        //	move
        public const string MO_STATUS_SAVED_MOVE = "SMOV";
        //	move
        public const string MO_STATUS_SAVED_STOCKTAKING = "STOK";


        //	events
        public const string MO_EVENT_DIR_ATTACH = "DIAT";
        public const string MO_EVENT_STIN_POS_ERROR = "SIPE";
        public const string MO_EVENT_STIN_POS_CHANGED = "SIPC";
        public const string MO_EVENT_DISP_POS_ERROR = "DIPE";

        public const string MO_EVENT_STATUS_CHANGED = "STATUS_CHANGED";
        public const string MO_EVENT_ITEM_CHANGED = "ITEM_CHANGED";
        public const string MO_EVENT_INSTR_DISPATCH = "IDIS";
        public const string MO_EVENT_CONF_DISPATCH = "CDIS";


        public static StatusSet Statuses = new StatusSet(s =>
        {
            var changeStatusAction = new ActionDef()
            {
                ActionProcedure = "setStatus_dialog",
                ActionType = ActionTypeEnum.Dialog,
                Name = "Změnit stav",
                ActionAuthorization = ActionAuthorizationEnum.SuperUser
            };
            //actions
            var actions = new List<ActionDef>()
            {
                new ActionDef() { ActionProcedure = "cancel_dialog", ActionType = ActionTypeEnum.Dialog, Name = "Zrušit pohyb" },
                changeStatusAction,
                new ActionDef() { ActionProcedure = "print_dialog", ActionType = ActionTypeEnum.Dialog, Name = "Tisk" },

            };
            //statuses
            s.AddStatus(MO_STATUS_CANCEL, "Zrušeno")
                .AddAction(changeStatusAction)
                .SetColor(System.Drawing.Color.Gray);
            s.AddStatus(MO_STATUS_NEW_RECEIVE, "Nový příjem").Actions = actions;
            s.AddStatus(MO_STATUS_SAVED_RECEIVE, "Příjem uložen").Actions = actions;
            s.AddStatus(MO_STATUS_NEW_STOIN, "Nové naskladnění").Actions = actions;
            s.AddStatus(MO_STATUS_SAVED_STOIN, "Naskladnění uloženo").Actions = actions;
            s.AddStatus(MO_STATUS_NEW_DIR_MOVE, "NDIM").Actions = actions;
            s.AddStatus(MO_STATUS_SAVED_DIR_MOVE, "SDIM").Actions = actions;
            s.AddStatus(MO_STATUS_NEW_DISPATCH, "Nové vychystání").Actions = actions;
            s.AddStatus(MO_STATUS_SAVED_DISPATCH, "Vychystání uloženo").Actions = actions;
            s.AddStatus(MO_STATUS_SAVED_EXPED, "Expedice").Actions = actions;
            s.AddStatus(MO_STATUS_SAVED_MOVE, "Přeskladnění").Actions = actions;
            s.AddStatus(MO_STATUS_SAVED_STOCKTAKING, "Inventura").Actions = actions;

            //events
            s.AddEvent(MO_EVENT_DIR_ATTACH, "DIAT");
            s.AddEvent(MO_EVENT_STIN_POS_ERROR, "Chyba pozice");
            s.AddEvent(MO_EVENT_STIN_POS_CHANGED, "SIPC");
            s.AddEvent(MO_EVENT_DISP_POS_ERROR, "Chyba pozice");
            s.AddEvent(MO_EVENT_STATUS_CHANGED, "Změna stavu");
            s.AddEvent(MO_EVENT_ITEM_CHANGED, "Změna položky");
            s.AddEvent(MO_EVENT_INSTR_DISPATCH, "IDIS");
            s.AddEvent(MO_EVENT_CONF_DISPATCH, "Vychystání potvrzeno");
        });



        [Core.DBTestIgnore]
        [ComputedColumn("docDate_d")]
        public DateTime DocDate_d { get; set; }

        [Core.DBTestIgnore]
        [ComputedColumn("docDate_p")]
        public int DocDate_p { get; set; }

        [Core.DBTestIgnore]
        [Display(Name = "Doklad")]
        [ComputedColumn("docInfo")]
        public string DocInfo { get; set; }

        [Core.DBTestIgnore]
        [Display(Name = "Poznámka")]
        [ComputedColumn("docRemark_prev")]
        public string DocRemark_prev { get; set; }

        [Core.DBTestIgnore]
        [Display(Name = "Typ dokladu")]
        public string DocTypeDesc
        {
            get
            {
                switch (DocType)
                {
                    case MO_DOCTYPE_DIR_MOVE: return "Přesun";
                    case MO_DOCTYPE_DISPATCH: return "Vyskladnění";
                    case MO_DOCTYPE_EXPED: return "Expedice";
                    case MO_DOCTYPE_MOVE: return "Přesun";
                    case MO_DOCTYPE_RECEIVE: return "Příjem";
                    case MO_DOCTYPE_STOCKTAKING: return "Inventura";
                    case MO_DOCTYPE_STORE_IN: return "Naskladnění";
                    default: return null;
                }
            }
        }
    }
}

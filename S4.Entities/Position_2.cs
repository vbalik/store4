﻿using System;
using System.Collections.Generic;
using System.Text;

using S4.Entities.StatusEngine;

namespace S4.Entities
{
    public partial class Position
    {
        public const string POS_STATUS_OK = "POS_OK";
        public const string POS_STATUS_BLOCKED = "POS_BLOCKED";
        public const string POS_STATUS_STOCKTAKING = "POS_STOCTAKING";
        // events
        public const string POS_EVENT_STATUS_CHANGED = "STATUS_CHANGED";
        public const string POS_EVENT_UPDATE = "POS_UPDATE";

        public static StatusSet Statuses = new StatusSet(s =>
        {
            var actionStockTakingBegin = new ActionDef()
            {
                ActionType = ActionTypeEnum.ProcedureWithConfirmation,
                ActionProcedure = "StockTakingBegin",
                Name = "Zahájit inventuru na pozici"
            };
            var actionPrint = new ActionDef()
            {
                ActionType = ActionTypeEnum.Dialog,
                ActionProcedure = "print_position_dialog",
                Name = "Tisk štítku pozice"
            };
            var actionPrintToPosition = new ActionDef()
            {
                ActionType = ActionTypeEnum.Dialog,
                ActionProcedure = "print_to_position_dialog",
                Name = "Tisk na pozici"
            };
            var actionPositionSetStatus = new ActionDef()
            {
                ActionType = ActionTypeEnum.Dialog,
                ActionProcedure = "setStatus_dialog",
                Name = "Změna stavu"
            };
            var actionRemoveCarriers = new ActionDef()
            {
                ActionType = ActionTypeEnum.ProcedureWithConfirmation,
                ActionProcedure = "RemoveCarrier",
                Name = "Zrušit paletové štítky",
                ActionAuthorization = ActionAuthorizationEnum.SuperUser
            };

            s.AddStatus(POS_STATUS_OK, "OK")
                .AddAction(actionStockTakingBegin)
                .AddAction(actionPositionSetStatus)
                .AddAction(actionPrint)
                .AddAction(actionPrintToPosition)
                .AddAction(actionRemoveCarriers);

            s.AddStatus(POS_STATUS_BLOCKED, "Blokováno")
                .AddAction(actionStockTakingBegin)
                .AddAction(actionPositionSetStatus)
                .AddAction("Odblokovat", ActionTypeEnum.ProcedureWithConfirmation, "PositionResetStatus")
                .AddAction(actionPrint)
                .AddAction(actionPrintToPosition)
                .SetColor(System.Drawing.Color.Red);

            s.AddStatus(POS_STATUS_STOCKTAKING, "V inventuře")
                .AddAction(actionPrint)
                .AddAction(actionPrintToPosition)
                .AddAction(actionPositionSetStatus)
                .SetColor(System.Drawing.Color.Blue);

            // events
            s.AddEvent(POS_EVENT_STATUS_CHANGED, "Změna stavu");
            s.AddEvent(POS_EVENT_UPDATE, "Změněno");


        });
    }
}

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_storemove")]
	[PrimaryKey("stomoveid")]
	[ExplicitColumns]
	public partial class StoreMove
	{
		[Required]
		[Display(Name = "ID")]
		[Column("stoMoveID")]
		public int StoMoveID {get; set;}

		[Required]
		[MaxLength(16)]
		[Display(Name = "Stav")]
		[RawColumnName("m.docStatus")]
		[StatusSet("Statuses")]
		[Column("docStatus")]
		public string DocStatus {get; set;}

		[Required]
		[MaxLength(16)]
		[Display(Name = "Prefix")]
		[RawColumnName("m.docNumPrefix")]
		[Column("docNumPrefix")]
		public string DocNumPrefix {get; set;}

		[Required]
		[Column("docType")]
		public int DocType {get; set;}

		[Required]
		[Display(Name = "Číslo")]
		[RawColumnName("m.docNumber")]
		[Column("docNumber")]
		public int DocNumber {get; set;}

		[Required]
		[Display(Name = "Datum")]
		[RawColumnName("m.docDate")]
		[Column("docDate")]
		public DateTime DocDate {get; set;}

		[Required]
		[Column("parent_directionID")]
		public int Parent_directionID {get; set;}

		[Display(Name = "Poznámka")]
		[RawColumnName("m.docRemark")]
		[Column("docRemark")]
		public string DocRemark {get; set;}

		[Required]
		[Column("entryDateTime")]
		public DateTime EntryDateTime {get; set;}

		[Required]
		[MaxLength(16)]
		[Display(Name = "Uživatel")]
		[RawColumnName("m.entryUserID")]
		[Column("entryUserID")]
		public string EntryUserID {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

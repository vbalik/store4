using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_prefixlist")]
	[PrimaryKey("docclass,docnumprefix", AutoIncrement = false)]
	[ExplicitColumns]
	public partial class Prefix
	{
		public enum StoreMoveDocType
		{
			[Display(Name = "Neurčeno")]
			MoDocTypeNotSpecified = 0,
			[Display(Name = "Příjem")]
			MoDocTypeReceive = 1,
			[Display(Name = "RPC")]
			MoDocTypeRPC = 2,
			[Display(Name = "Naskladnění")]
			MoDocTypeStoreIN = 3,
			[Display(Name = "Přesun")]
			MoDocTypeMove = 4,
			[Display(Name = "Vyskladnění")]
			MoDocTypeDispatch = 5,
			[Display(Name = "Expedice")]
			MoDocTypeExped = 6,
			[Display(Name = "Inventura")]
			MoDocTypeDirMove = 9,
			[Display(Name = "Přesun")]
			MoDocTypeStockTaking = 10,
		}

		public enum DocDirectionType
		{
			[Display(Name = "Příjem")]
			In = 1,
			[Display(Name = "Výdej")]
			Out = -1,
		}

		[Required]
		[MaxLength(32)]
		[Display(Name = "Třída")]
		[Column("docClass")]
		public string DocClass {get; set;}

		[Required]
		[MaxLength(16)]
		[Display(Name = "Prefix")]
		[Column("docNumPrefix")]
		public string DocNumPrefix {get; set;}

		[Required]
		[MaxLength(64)]
		[Display(Name = "Popis")]
		[Column("prefixDesc")]
		public string PrefixDesc {get; set;}

		[Required]
		[Display(Name = "Směr")]
		[Column("docDirection")]
		public DocDirectionType DocDirection {get; set;}

		[Required]
		[Display(Name = "Typ")]
		[Column("docType")]
		public StoreMoveDocType DocType {get; set;}

		[Required]
		[Display(Name = "Povoleno")]
		[Column("prefixEnabled")]
		public bool PrefixEnabled {get; set;}

		[MaxLength(16)]
		[Display(Name = "Sekce")]
		[Column("prefixSection")]
		public string PrefixSection {get; set;}

		[Required]
		[Display(Name = "Nezpracovávat")]
		[Column("doNotProcess")]
		public bool DoNotProcess {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

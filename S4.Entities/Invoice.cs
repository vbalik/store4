using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_invoice")]
	[PrimaryKey("invoiceid")]
	[ExplicitColumns]
	public partial class Invoice
	{
		[Required]
		[Display(Name = "ID")]
		[Column("invoiceID")]
		public int InvoiceID {get; set;}

		[Required]
		[MaxLength(16)]
		[Display(Name = "Stav")]
		[StatusSet("Statuses")]
		[Column("docStatus")]
		public string DocStatus {get; set;}

		[Required]
		[Display(Name = "Číslo faktury")]
		[Column("docNumber")]
		public int DocNumber {get; set;}

		[Required]
		[MaxLength(16)]
		[Display(Name = "Dokladová řada faktury")]
		[Column("docPrefix")]
		public string DocPrefix {get; set;}

		[Required]
		[MaxLength(2)]
		[Display(Name = "Rok")]
		[Column("docYear")]
		public string DocYear {get; set;}

		[Required]
		[Display(Name = "Poslední zápis")]
		[Column("lastDateTime")]
		public DateTime LastDateTime {get; set;}

		[Required]
		[Display(Name = "Založeno")]
		[Column("entryDateTime")]
		public DateTime EntryDateTime {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_bookings")]
	[PrimaryKey("bookingid")]
	[ExplicitColumns]
	public partial class Booking
	{
		[Required]
		[Display(Name = "ID")]
		[Column("bookingID")]
		public int BookingID {get; set;}

		[Required]
		[MaxLength(16)]
		[Display(Name = "Stav")]
		[StatusSet("Statuses")]
		[Column("bookingStatus")]
		public string BookingStatus {get; set;}

		[Required]
		[Column("stoMoveLotID")]
		public int StoMoveLotID {get; set;}

		[Required]
		[Column("partnerID")]
		public int PartnerID {get; set;}

		[Required]
		[Column("partAddrID")]
		public int PartAddrID { get; set; }

		[Required]
		[Display(Name = "Množství")]
		[Column("requiredQuantity")]
		public decimal RequiredQuantity {get; set;}

		[Required]
		[Display(Name = "Konec platnosti")]
		[Column("bookingTimeout")]
		public DateTime BookingTimeout {get; set;}

		[Column("usedInDirectionID")]
		public int? UsedInDirectionID {get; set;}

        [Column("parentBookingID")]
        public int? ParentBookingID { get; set; }

        [MaxLength(16)]
		[Display(Name = "Smazal")]
		[Column("deletedByEntryUserID")]
		public string DeletedByEntryUserID {get; set;}

		[Display(Name = "Smazáno")]
		[Column("deletedDateTime")]
		public DateTime? DeletedDateTime {get; set;}

		[Required]
		[Display(Name = "Zadáno")]
		[Column("entryDateTime")]
		public DateTime EntryDateTime {get; set;}

		[Required]
		[MaxLength(16)]
		[Display(Name = "Uživatel")]
		[Column("entryUserID")]
		public string EntryUserID {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_deliverydirection")]
	[PrimaryKey("deliverydirectionid")]
	[ExplicitColumns]
	public partial class DeliveryDirection
	{
		public enum DeliveryProviderEnum
		{
			[Display(Name = "Není")]
			NONE = 0,
			[Display(Name = "PPL")]
			PPL = 1,
			[Display(Name = "DPD")]
			DPD = 2,
			[Display(Name = "Česká pošta")]
			CPOST = 3,

            [Display(Name = "DPDNew")]
            DPDNew = 4,
        }

		public enum DeliveryPaymentEnum
		{
			[Display(Name = "Dodavatelem")]
			SUPP = 1,
			[Display(Name = "Na bankovní účet")]
			ACC = 2,
			[Display(Name = "Platba dobírkou")]
			CHOD = 3,
			[Display(Name = "Platba dobírkou kartou")]
			CAOD = 4,
			[Display(Name = "Platba platební kartou")]
			CARD = 5,
			[Display(Name = "Zálohovým listem")]
			ADV = 6,
		}

		public enum DeliveryCountryEnum
		{
			[Display(Name = "Česká republika")]
			CZ = 1,
			[Display(Name = "Slovensko")]
			SK = 2,
		}

		public enum DeliveryCurrencyEnum
		{
			[Display(Name = "Česká koruna")]
			CZK = 1,
			[Display(Name = "Euro")]
			EUR = 2,
		}

		[Required]
		[Display(Name = "ID")]
		[Column("deliveryDirectionID")]
		public int DeliveryDirectionID {get; set;}

		[Required]
		[Display(Name = "DirectionID")]
		[Column("directionID")]
		public int DirectionID {get; set;}

		[Required]
		[MaxLength(16)]
		[Display(Name = "Stav")]
		[StatusSet("Statuses")]
		[Column("deliveryStatus")]
		public string DeliveryStatus {get; set;}

		[Required]
		[Display(Name = "Dopravce")]
		[Column("deliveryProvider")]
		public int DeliveryProvider {get; set;}

		[Display(Name = "Pokus číslo")]
		[Column("createShipmentAttempt")]
		public short? CreateShipmentAttempt {get; set;}

		[Display(Name = "Chyba")]
		[Column("createShipmentError")]
		public string CreateShipmentError {get; set;}

		[MaxLength(128)]
		[Display(Name = "Referenční číslo zásilky")]
		[Column("shipmentRefNumber")]
		public string ShipmentRefNumber {get; set;}

		[Display(Name = "ID manifestu")]
		[Column("deliveryManifestID")]
		public int? DeliveryManifestID {get; set;}

		[Display(Name = "Datum a čas vytvoření")]
		[Column("deliveryCreated")]
		public DateTime? DeliveryCreated {get; set;}

		[Display(Name = "Datum a čas změny")]
		[Column("deliveryChanged")]
		public DateTime? DeliveryChanged {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

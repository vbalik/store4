using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using NPoco;

using S4.Entities.Attributes;

namespace S4.Entities
{
	[TableName("s4_reports")]
	[PrimaryKey("reportid")]
	[ExplicitColumns]
	public partial class Reports
	{
		[Required]
		[Column("reportID")]
		public int ReportID {get; set;}

		[Required]
		[MaxLength(32)]
		[Column("reportType")]
		public string ReportType {get; set;}

		[Required]
		[Column("template")]
		public string Template {get; set;}

		[Required]
		[Column("printerTypeID")]
		public int PrinterTypeID {get; set;}

		[Required]
		[ComputedColumn("r_Edit")]
		public byte[] R_Edit {get; set;}

	}
}

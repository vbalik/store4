﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace S4.Entities.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class RawColumnNameAttribute : Attribute
    {
        public RawColumnNameAttribute()
        {

        }

        public RawColumnNameAttribute(string name)
        {
            Name = name;
        }

        public string Name { get; set; }
    }
}


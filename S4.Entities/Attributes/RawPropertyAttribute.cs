﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Entities.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class RawPropertyAttribute : Attribute
    {
        public RawPropertyAttribute()
        {

        }

        public RawPropertyAttribute(string name)
        {
            Name = name;
        }

        public string Name { get; set; }
    }
}

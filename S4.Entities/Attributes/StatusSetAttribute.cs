﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace S4.Entities.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class StatusSetAttribute : Attribute
    {
        public StatusSetAttribute()
        {

        }

        public StatusSetAttribute(string listProperty, bool events = false)
        {
            ListProperty = listProperty;
            Events = events;
        }

        public string ListProperty { get; set; }
        public bool Events { get; set; }
    }
}

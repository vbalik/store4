﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Entities.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class IsFlagAttribute : Attribute
    {

    }
}

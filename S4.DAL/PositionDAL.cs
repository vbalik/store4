﻿using NPoco;
using S4.Entities;
using S4.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace S4.DAL
{
    public class PositionDAL : DAL<Entities.Position, int>
    {
        public PositionModel GetModel(int id)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var model = db.SingleOrDefaultById<PositionModel>(id);
                if (model != null)
                    model.PositionSections = db.Fetch<PositionSection>(new Sql().Where("positionID = @0", id));
                return model;
            }
        }


        public List<PositionModel> ScanByPosCode(string posCode)
        {
            var sql = new Sql().Where("posCode = @0", posCode);
            return FetchModelList(sql);
        }


        public List<PositionModel> ListByCategory(Position.PositionCategoryEnum category)
        {
            var sql = new Sql().Where("posCateg = @0", (byte)category);
            return FetchModelList(sql);
        }


        private List<PositionModel> FetchModelList(Sql sqlWhere)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {                
                var result = db.Fetch<PositionModel>(sqlWhere);
                foreach (var item in result)
                    item.PositionSections = db.Fetch<PositionSection>(new Sql().Where("positionID = @0", item.PositionID));
                return result;
            }
        }

        public int ScanFreePosition(string sectID, byte posHeight)
        {
            var sql = new Sql()
                .Append("SELECT TOP 1 pos.positionID FROM s4_positionList pos WITH(NOLOCK) ")
                .Append("JOIN (SELECT positionID FROM s4_storeMove_items itm ")
                .Append("WHERE itm.itemValidity = 100 AND positionID IN ")
                .Append("   (SELECT positionID FROM s4_positionList_sections sec WITH(NOLOCK) WHERE (sec.sectID = @0)) ", sectID)
                .Append("   GROUP BY positionID ")
                .Append("   HAVING (SUM(itm.quantity * itemDirection) = 0) ")
                .Append("   ) fp ON (pos.positionID = fp.positionID) ")
                .Append("WHERE (pos.posHeight >= @0) AND (pos.posCateg = 0) AND (pos.posStatus = 'POS_OK') ", posHeight)
                .Append("ORDER BY pos.posHeight, pos.posX, pos.posY, pos.posZ");

            int? positionID;
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                positionID = db.ExecuteScalar<int?>(sql);
            }

            return positionID ?? -1;
        }
    }
}

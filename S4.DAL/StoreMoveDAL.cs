﻿using NPoco;
using S4.Entities;
using S4.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.DAL
{
    public class StoreMoveDAL: DAL<StoreMove, int>
    {
        public StoreMoveModel GetStoreMoveAllData(int stoMoveID)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var model = db.SingleOrDefaultById<StoreMoveModel>(stoMoveID);
                if (model != null)
                {
                    var sql = new Sql().Where("stoMoveID = @0", stoMoveID);
                    model.Items = db.Fetch<StoreMoveItemModel>(sql);
                }

                return model;
            }
        }

        public List<StoreMoveModel> GetStoreMoveAllDataByDirection(int directionID)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var sql = new NPoco.Sql().Where($"{nameof(StoreMoveModel.Parent_directionID)} = @0", directionID);
                var models = db.Fetch<StoreMoveModel>(sql);
                foreach (var model in models)
                {
                    var sql2 = new Sql().Where("stoMoveID = @0", model.StoMoveID);
                    model.Items = db.Fetch<StoreMoveItemModel>(sql2);
                }

                return models;
            }
        }
    }
}

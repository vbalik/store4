﻿using NPoco;
using S4.Entities;
using System;
using System.Collections.Generic;

namespace S4.DAL
{
    public class ReportDAL : DAL<Entities.Reports, int>
    {
        public List<Entities.Reports> ListReportType(string reportType)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var sql = new Sql().Where($"reportType = @0", reportType);
                var result = db.Fetch<Entities.Reports>(sql);                
                return result;
            }
        }
    }
}

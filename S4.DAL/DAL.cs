﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.DAL
{
    public class DAL<TEntity, Tid>
    {
        public TEntity Get(Tid id)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                return db.SingleOrDefaultById<TEntity>(id);
            }
        }

        public List<TEntity> List(NPoco.Sql sql = null)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                if(sql == null)
                    return db.Fetch<TEntity>();
                else
                    return db.Fetch<TEntity>(sql);
            }
        }

        public void Update(TEntity entity)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                db.Update(entity);
            }
        }

        public object Insert(TEntity entity)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                return db.Insert(entity);
            }
        }
    }
}

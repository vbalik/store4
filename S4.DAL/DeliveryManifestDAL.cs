﻿using S4.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using NPoco;

namespace S4.DAL
{
    public class DeliveryManifestDAL : DAL<DeliveryManifest, int>
    {
        public IEnumerable<DeliveryManifest> GetWaitingManifests(Database db)
        {
            var sql = new Sql("select * from s4_deliveryManifest where manifestStatus = @0", DeliveryManifest.STATUS_WAIT);
            return db.Fetch<DeliveryManifest>(sql);
        }
    }
}

﻿using NPoco;
using S4.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using MimeMapping;
using S4.Core.Utils;
using S4.Entities.Helpers;
using Newtonsoft.Json;

namespace S4.DAL
{
    public class MessageBusDAL : DAL<Entities.MessageBus, int>
    {
        public List<Tuple<DateTime, int, string>> Last10DaysCount()
        {
            var sql = new Sql();
            sql.Append("SELECT CONVERT(date,CASE messageStatus WHEN 'NEW' THEN MAX(entryDateTime) ELSE MAX(lastDateTime) END) AS [date], COUNT(*) AS cnt, messageStatus");
            sql.Append("FROM[dbo].[s4_messageBus]");
            sql.Append("WHERE CASE messageStatus WHEN 'NEW' THEN entryDateTime ELSE lastDateTime END > DATEADD(day, -10, GETDATE())");
            sql.Append("GROUP BY CONVERT(date,[lastDateTime]), messageStatus");

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                return db.Fetch<object[]>(sql).Select(i => new Tuple<DateTime, int, string>((DateTime)i[0], (int)i[1], i[2].ToString())).ToList();
            }
        }

        public bool TryInsertMessageBusStatusNew(int directionID, MessageBus.MessageBusTypeEnum messageType, string messageData, Database db)
        {
            //check if exists
            var sql = new Sql();
            sql.Select("1");
            sql.From("s4_messageBus");
            sql.Where("directionID = @0 AND messageType = @1", directionID, messageType);

            if (db.FirstOrDefault<int>(sql) != 1)
            {
                var message = new MessageBus()
                {
                    DirectionID = directionID,
                    EntryDateTime = DateTime.Now,
                    ErrorDesc = null,
                    LastDateTime = null,
                    MessageData = messageData,
                    MessageStatus = MessageBus.NEW,
                    MessageType = messageType,
                    NextTryTime = null,
                    TryCount = 0
                };

                db.Insert(message);

                return true;
            }
            else
            {
                return false;
            }

        }

        public bool TryInsertMessageBusStatusNew(int directionID, MessageBus.MessageBusTypeEnum messageType, string messageData)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                return TryInsertMessageBusStatusNew(directionID, messageType, messageData, db);
            }
        }

        public int InsertMessageBusDocumentStatusNew(SendEmailInfo sendEmailInfo, List<byte[]> documents)
        {
            var documentWithNames = new List<Tuple<string, byte[]>>();

            if(sendEmailInfo.MessageAttachmentNames.Length != documents.Count)
                throw new IndexOutOfRangeException($"Names do not match documents. Names: {sendEmailInfo.MessageAttachmentNames.Length}, docs: {documents.Count}");

            // assign name to attachment
            for (int i = 0; i < sendEmailInfo.MessageAttachmentNames.Length; i++)
            {
                documentWithNames.Add(new Tuple<string, byte[]>(sendEmailInfo.MessageAttachmentNames[i], documents[i]));
            }

            return InsertMessageBusDocumentStatusNew(JsonConvert.SerializeObject(sendEmailInfo), documentWithNames);
        }

        /// <summary>
        /// You sould use InsertMessageBusDocumentStatusNew(SendEmailInfo sendEmailInfo, List<byte[]> documents) instead.
        /// </summary>
        private int InsertMessageBusDocumentStatusNew(string messageBusDocumentData, List<Tuple<string, byte[]>> documents)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                using (var tr = db.GetTransaction())
                {
                    var message = new MessageBus()
                    {
                        DirectionID = 0,
                        EntryDateTime = DateTime.Now,
                        ErrorDesc = null,
                        LastDateTime = null,
                        MessageData = messageBusDocumentData,
                        MessageStatus = MessageBus.NEW,
                        MessageType = MessageBus.MessageBusTypeEnum.SendMail,
                        NextTryTime = null,
                        TryCount = 0
                    };

                    db.Insert(message);

                    foreach (var document in documents)
                    {
                        var messageDocument = new MessageBusDocument
                        {
                            EntryDateTime = message.EntryDateTime,
                            MessageBusID = message.MessageBusID,
                            DocumentName = document.Item1,
                            MimeType = MimeUtility.GetMimeMapping(document.Item1),
                            Document = document.Item2
                        };

                        db.Insert(messageDocument);

                    }

                    tr.Complete();
                    return message.MessageBusID;
                }
            }
        }

        public List<MessageBus> GetDataNoProcessedByType(MessageBus.MessageBusTypeEnum type, bool orderByDirectionID = false)
        {
            var sql = new Sql();
            //sql.Where("MessageBusID = 1393904"); //test!!!!
            //sql.Where("MessageBusID = 1387970"); //test!!!!
            sql.Where("messageType = @0 AND (messageStatus = @1 OR (messageStatus = @2 AND nextTryTime < GETDATE()) OR (messageStatus = @3 AND nextTryTime < GETDATE()))", (byte)type, MessageBus.NEW, MessageBus.WAITING_FOR_REPEAT, MessageBus.WAITING_FOR_COMPLETE);

            if (orderByDirectionID)
                sql.OrderBy("directionID");

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var data = db.Fetch<MessageBus>(sql);
                return data;
            }
        }
        
    }
}

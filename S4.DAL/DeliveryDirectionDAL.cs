﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPoco;
using S4.Entities;
using S4.Entities.Models;

namespace S4.DAL
{
    public class DeliveryDirectionDAL : DAL<DeliveryDirection, int>
    {
        public DeliveryDirection GetByPK(int deliveryDirectionID, Database db)
        {
            var sql = new Sql().Where("deliveryDirectionID = @0", deliveryDirectionID);
            return db.FirstOrDefault<DeliveryDirection>(sql);
        }

        public List<DeliveryDirection> GetAllRowsByDirectionID(int directionID, Database db)
        {
            var sql = new Sql().Where("directionID = @0 ", directionID).OrderBy("deliveryDirectionID");
            return db.Fetch<DeliveryDirection>(sql);
        }

        public DeliveryDirection GetByDirectionID(int directionID, Database db )
        {
            var sql = new Sql().Where("directionID = @0 AND deliveryStatus = @1", directionID, DeliveryDirection.DD_STATUS_OK);
            return db.FirstOrDefault<DeliveryDirection>(sql);
        }

        public DeliveryDirection GetByRefNumber(DeliveryDirection.DeliveryProviderEnum deliveryProvider, string shipmentRefNumber, Database db)
        {
            return db.Query<DeliveryDirection>().Where(_ => _.DeliveryProvider == (int)deliveryProvider && _.ShipmentRefNumber == shipmentRefNumber ).FirstOrDefault();
        }

        public List<DeliveryDirection> GetByDirectionIDs(int[] directionIDs, Database db)
        {
            var sql = new Sql().Where("directionID IN (@0) AND deliveryStatus = @1", directionIDs, DeliveryDirection.DD_STATUS_OK);
            return db.Fetch<DeliveryDirection>(sql);
        }

        public List<DeliveryDirection> GetByManifestID(int deliveryManifestID, Database db)
        {
            var sql = new Sql().Where("deliveryManifestID = @0 AND deliveryStatus = @1", deliveryManifestID, DeliveryDirection.DD_STATUS_OK);
            return db.Fetch<DeliveryDirection>(sql);
        }      

        public IEnumerable<DirectionModel> GetDirectionsWithErrors(short errorsLimit, Database db)
        {
            var sql = new Sql("select directionID from s4_deliveryDirection where createShipmentError is not NULL and createShipmentAttempt < @0 AND deliveryStatus = @1", errorsLimit, DeliveryDirection.DD_STATUS_OK);
            var deliveryDirectionIDs = db.Fetch<int>(sql);
            return deliveryDirectionIDs.Select(i => new DAL.DirectionDAL().GetDirectionAllData(i, db));
        }

        public IEnumerable<int> GetDirectionsForManifest(DeliveryDirection.DeliveryProviderEnum deliveryProvider, short errorsLimit, Database db)
        {
            var sql = new Sql("select directionID from s4_deliveryDirection where manifestRefNumber is null and deliveryProvider = @0 and createManifestAttempt is not null and createManifestAttempt < @1 AND deliveryStatus = @2",
                deliveryProvider, errorsLimit, DeliveryDirection.DD_STATUS_OK);
            var deliveryDirectionIDs = db.Fetch<int>(sql);
            return deliveryDirectionIDs;
        }

        public DeliveryDirectionModel GetDeliveryDirectionAllData(int directionID, Database db)
        {
            var model = db.Fetch<DeliveryDirectionModel>("select * from s4_DeliveryDirection where directionID = @0 AND deliveryStatus = @1", directionID, DeliveryDirection.DD_STATUS_OK).FirstOrDefault();
            if (model != null)
            {
                var sql = new Sql().Where("deliveryDirectionID = @0", model.DeliveryDirectionID);
                model.Items = db.Fetch<DeliveryDirectionItem>(sql);
            }

            return model;
        }

        public List<DeliveryDirectionModel> GetDeliveryDirectionsAllData(int directionID, Database db)
        {
            var rows = db.Fetch<DeliveryDirectionModel>("select * from s4_DeliveryDirection where directionID = @0", directionID);
            if (rows != null)
            {
                foreach (var row in rows)
                {
                    var sql = new Sql().Where("deliveryDirectionID = @0", row.DeliveryDirectionID);
                    row.Items = db.Fetch<DeliveryDirectionItem>(sql);
                }
                
            }
            return rows;
        }
    }
}

﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.DAL.Interfaces
{
    public interface IBookings
    {
        List<BookingInfo> ListValidBookings(int artPackID);

        void BookingUsed(IDatabase db, int bookingID, decimal usedQuantity, int usedInDirectionID);

        void BookingUseCanceled(Database db, int directionID);
    }
}

﻿using S4.Entities.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace S4.DAL.Interfaces
{
    public class BookingInfo
    {
        public int BookingID { get; set; }

        public int StoMoveLotID { get; set; }

        public int PartnerID { get; set; }
        public int PartAddrID { get; set; }

        public decimal RequiredQuantity { get; set; }
    }
}

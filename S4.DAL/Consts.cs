﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.DAL
{
    public class Consts
    {
        private static object _lock = new object();
        private static string _onSummaryErrorText;
        public static string OnSummaryErrorText
        {
            get
            {
                lock(_lock)
                {
                    if (_onSummaryErrorText == null)
                    {
                        using (var db = Core.Data.ConnectionHelper.GetDB())
                        {
                            _onSummaryErrorText = db.ExecuteScalar<string>("SELECT [dbo].[s4_getOnSummaryErrorText] ()");
                        }
                    }
                    return _onSummaryErrorText;
                }
            }
        }
    }
}

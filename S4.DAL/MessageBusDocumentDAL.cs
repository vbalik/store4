﻿using NPoco;
using S4.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using MimeMapping;


namespace S4.DAL
{
    public class MessageBusDocumentDAL : DAL<Entities.MessageBusDocument, int>
    {
        public List<MessageBusDocument> GetDocumentsByMessageBusID(int messageBusID)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var sql = new Sql();
                sql.Where("messageBusID = @0", messageBusID);

                return db.Fetch<MessageBusDocument>(sql);
            }
        }

        public MessageBusDocument GetDocumentByID(int id)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var sql = new Sql();
                sql.Where("messageBusDocumentID = @0", id);

                return db.FirstOrDefault<MessageBusDocument>(sql);
            }
        }
    }
}

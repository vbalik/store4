﻿using S4.Entities;
using S4.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace S4.DAL.Cache 
{
    public class SectionModelCache : Core.Cache.CacheBase<SectionModel>, Core.Cache.ICache<SectionModel, string>
    {
        public SectionModelCache() : base(180)
        { }


        public SectionModel GetItem(string sectID)
        {
            return base.GetOrAddExisting(sectID, () =>
            {
                var section = (new DAL.DAL<SectionModel, string>()).Get(sectID);
                if (section != null)
                {
                    var sql = new NPoco.Sql();
                    sql.Where("sectID = @0", sectID);
                    var positionSections = (new DAL.DAL<PositionSection, int>()).List(sql);
                    section.PositionIDs = new HashSet<int>(positionSections.Select(i => i.PositionID));
                }
                return section;
            });
        }


        public new void Remove(string sectID)
        {
            Remove(sectID);
        }
    }
}

﻿using S4.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.DAL.Cache
{
    public class ArticlePackingCache : Core.Cache.CacheBase<ArticlePacking>, Core.Cache.ICache<ArticlePacking, int>
    {
        public ArticlePackingCache() : base(5)
        { }


        public ArticlePacking GetItem(int artPackID)
        {
            var key = artPackID.ToString();
            return base.GetOrAddExisting(key, () =>
            {
                var articlePacking = (new DAL.DAL<ArticlePacking, int>()).Get(artPackID);
                return articlePacking;
            });
        }


        public void Remove(int artPackID)
        {
            var key = artPackID.ToString();
            Remove(key);
        }
    }
}

﻿using S4.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.DAL.Cache 
{
    public class PartnerAddressCache : Core.Cache.CacheBase<PartnerAddress>, Core.Cache.ICache<PartnerAddress, int>
    {
        public PartnerAddressCache() : base(60)
        { }


        public PartnerAddress GetItem(int partAddrID)
        {
            return base.GetOrAddExisting(partAddrID.ToString(), () =>
            {
                var partnerAddress = (new DAL<PartnerAddress, int>()).Get(partAddrID);
                return partnerAddress;
            });
        }


        public void Remove(int partAddrID)
        {
            Remove(partAddrID.ToString());
        }
    }
}

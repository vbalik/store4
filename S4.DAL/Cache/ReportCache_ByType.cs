﻿using S4.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.DAL.Cache 
{
    public class ReportCache_ByType : Core.Cache.CacheBase<Reports>
    {
        public ReportCache_ByType() : base(180)
        { }


        public Reports GetItem(int printerTypeID, string reportType)
        {
            var key = $"{printerTypeID}_{reportType}";
            return base.GetOrAddExisting(key, () =>
            {
                using (var db = Core.Data.ConnectionHelper.GetDB())
                {
                    var sql = new NPoco.Sql();
                    sql.Where("printerTypeID = @0 AND reportType = @1", printerTypeID, reportType);
                    var report = db.FirstOrDefault<Reports>(sql);
                    return report;
                }
            });
        }


        public void Remove(int printerTypeID, string reportType)
        {
            var key = $"{printerTypeID}_{reportType}";
            Remove(key);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.DAL.Cache
{
    public class TemporaryConsts : Core.Cache.CacheBase<object>
    {
        public enum ConstTypeEnum
        {
            LastStoMoveItemID
        }

        public TemporaryConsts() : base(3600)
        { }

        public T GetConst<T>(ConstTypeEnum type)
        {
            return (T)GetOrAddExisting(type.ToString(), () =>
            {
                using (var db = Core.Data.ConnectionHelper.GetDB())
                {
                    var lastStoMoveItemID = db.ExecuteScalar<int>("SELECT MAX(lastStoMoveItemID) FROM s4_onStoreSummary WHERE onStoreSummStatus = 100");
                    return lastStoMoveItemID;
                }
            });
        }
    }
}

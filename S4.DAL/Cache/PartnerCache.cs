﻿using S4.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.DAL.Cache 
{
    public class PartnerCache : Core.Cache.CacheBase<Partner>, Core.Cache.ICache<Partner, int>
    {
        public PartnerCache() : base(60)
        { }


        public Partner GetItem(int partnerID)
        {
            return base.GetOrAddExisting(partnerID.ToString(), () =>
            {
                var partner = (new DAL<Partner, int>()).Get(partnerID);
                return partner;
            });
        }


        public void Remove(int partnerID)
        {
            Remove(partnerID.ToString());
        }
    }
}

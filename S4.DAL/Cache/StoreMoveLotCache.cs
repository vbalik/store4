﻿using S4.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.DAL.Cache 
{
    public class StoreMoveLotCache : Core.Cache.CacheBase<StoreMoveLot>, Core.Cache.ICache<StoreMoveLot, int>
    {
        public StoreMoveLotCache() : base(300)
        { }


        public StoreMoveLot GetItem(int stoMoveLotID)
        {
            return base.GetOrAddExisting(stoMoveLotID.ToString(), () =>
            {
                var partner = (new DAL<StoreMoveLot, int>()).Get(stoMoveLotID);
                return partner;
            });
        }


        public void Remove(int stoMoveLotID)
        {
            Remove(stoMoveLotID.ToString());
        }
    }
}

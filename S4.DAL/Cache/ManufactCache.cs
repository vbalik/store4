﻿using S4.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.DAL.Cache 
{
    public class ManufactCache : Core.Cache.CacheBase<Manufact>, Core.Cache.ICache<Manufact, string>
    {
        public ManufactCache() : base(5)
        { }


        public Manufact GetItem(string manufID)
        {
            return base.GetOrAddExisting(manufID, () =>
            {
                var article = (new DAL.DAL<Manufact, string>()).Get(manufID);
                return article;
            });
        }


        public new void Remove(string manufID)
        {
            Remove(manufID);
        }
    }
}

﻿using S4.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.DAL.Cache 
{
    public class PositionCache : Core.Cache.CacheBase<Position>, Core.Cache.ICache<Position, int>
    {
        public PositionCache() : base(5)
        { }


        public Position GetItem(int positionID)
        {
            return base.GetOrAddExisting(positionID.ToString(), () =>
            {
                var position = (new DAL<Position, int>()).Get(positionID);
                return position;
            });
        }


        public void Remove(int positionID)
        {
            Remove(positionID.ToString());
        }
    }
}

﻿using S4.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.DAL.Cache 
{
    public class PrinterLocationCache : Core.Cache.CacheBase<PrinterLocation>
    {
        public PrinterLocationCache() : base(180)
        { }


        public PrinterLocation GetItem(int printerLocationID)
        {
            var key = $"{printerLocationID}";
            return base.GetOrAddExisting(key, () =>
            {
                using (var db = Core.Data.ConnectionHelper.GetDB())
                {
                    var sql = new NPoco.Sql();
                    var printerLocation = db.SingleOrDefaultById<PrinterLocation>(printerLocationID);
                    return printerLocation;
                }
            });
        }


        public void Remove(int printerLocationID)
        {
            var key = $"{printerLocationID}";
            Remove(key);
        }
    }
}

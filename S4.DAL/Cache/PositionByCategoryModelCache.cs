﻿using S4.Entities;
using S4.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace S4.DAL.Cache
{
    public class PositionByCategoryModelCache : Core.Cache.CacheBase<PositionByCategoryModel>, Core.Cache.ICache<PositionByCategoryModel, Position.PositionCategoryEnum>
    {
        public PositionByCategoryModelCache() : base(180)
        { }


        public PositionByCategoryModel GetItem(Position.PositionCategoryEnum positionCategory)
        {
            var key = positionCategory.ToString();
            return base.GetOrAddExisting(key, () =>
            {
                var model = new PositionByCategoryModel() { PositionCategory = positionCategory };
                var positions = (new DAL.PositionDAL()).ListByCategory(positionCategory);
                model.PositionIDs = new HashSet<int>(positions.Select(i => i.PositionID));
                return model;
            });
        }


        public void Remove(Position.PositionCategoryEnum positionCategory)
        {
            Remove(positionCategory.ToString());
        }
    }
}

﻿using S4.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.DAL.Cache 
{
    public class ReportCache : Core.Cache.CacheBase<Reports>
    {
        public ReportCache() : base(180)
        { }


        public Reports GetItem(int reportID)
        {
            var key = $"{reportID}";
            return base.GetOrAddExisting(key, () =>
            {
                using (var db = Core.Data.ConnectionHelper.GetDB())
                {
                    var report = db.SingleOrDefaultById<Reports>(reportID);
                    return report;
                }
            });
        }


        public void Remove(int reportID)
        {
            var key = $"{reportID}";
            Remove(key);
        }
    }
}

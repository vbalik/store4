﻿using S4.Entities;
using S4.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.DAL.Cache
{
    public class ArticleModelCache : Core.Cache.CacheBase<ArticleModel>, Core.Cache.ICache<ArticleModel, int>
    {
        public ArticleModelCache() : base(5)
        { }


        public ArticleModel GetItem(int articleID)
        {
            var key = articleID.ToString();
            return base.GetOrAddExisting(key, () =>
            {
                return (new DAL.ArticleDAL()).GetModel(articleID);
            });
        }


        public void Remove(int articleID)
        {
            var key = articleID.ToString();
            Remove(key);
        }
    }
}

﻿using S4.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.DAL.Cache 
{
    public class CertificateCache : Core.Cache.CacheBase<Certificate>
    {
        public CertificateCache() : base(3600) //1 hour
        { }


        public Certificate GetItem(string certificateType)
        {
            return base.GetOrAddExisting(certificateType, () =>
            {
                using (var db = Core.Data.ConnectionHelper.GetDB())
                {
                    var certificate = new DAL.CertificateDAL().GetByType(certificateType, db);
                    return certificate;
                }
            });
        }
               
    }
}

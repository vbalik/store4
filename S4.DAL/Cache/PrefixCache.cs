﻿using S4.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.DAL.Cache 
{
    public class PrefixCache : Core.Cache.CacheBase<Prefix>
    {
        public PrefixCache() : base(60)
        { }


        public Prefix GetItem(string docClass, string docNumPrefix)
        {
            var key = $"{docClass}_{docNumPrefix}";
            return base.GetOrAddExisting(key, () =>
            {
                using (var db = Core.Data.ConnectionHelper.GetDB())
                {
                    var sql = new NPoco.Sql();
                    sql.Where("docClass = @0 AND docNumPrefix = @1", docClass, docNumPrefix);
                    var prefix = db.SingleOrDefault<Prefix>(sql);
                    return prefix;
                }
            });
        }


        public void Remove(string docClass, string docNumPrefix)
        {
            var key = $"{docClass}_{docNumPrefix}";
            Remove(key);
        }
    }
}

﻿using NPoco;
using S4.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace S4.DAL.Cache 
{
    public class PositionCache_ByCode : Core.Cache.CacheBase<Position>, Core.Cache.ICache<Position, string>
    {
        public PositionCache_ByCode() : base(5)
        { }


        public Position GetItem(string posCode)
        {
            return base.GetOrAddExisting(posCode, () =>
            {
                var positions = (new PositionDAL()).ScanByPosCode(posCode);
                return positions.FirstOrDefault();
            });
        }


        public new void Remove(string posCode)
        {
            Remove(posCode);
        }
    }
}

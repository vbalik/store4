﻿using NPoco;
using S4.Entities;
using System;
using System.Collections.Generic;

namespace S4.DAL
{
    public class InternMessageDAL : DAL<Entities.InternMessage, int>
    {
        public List<InternMessage> Last20()
        {
            var sql = new Sql().OrderBy("IntMessID DESC");
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                return db.Page<Entities.InternMessage>(1, 20, sql).Items;
            }
        }
        
    }
}

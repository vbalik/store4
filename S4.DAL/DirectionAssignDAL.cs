﻿using NPoco;
using S4.Entities;
using S4.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;


namespace S4.DAL
{
    public class DirectionAssignDAL : DAL<Entities.DirectionAssign, int>
    {
        public List<ScanAssignmentModel> ScanAssignments(string workerID = null)
        {
            // prepare select conditions - from DirectionAssign.JOB_AND_STATUS_COMBINATION
            var where = new StringBuilder();
            where.Append("(");
            for (var condPos = 0; condPos < DirectionAssign.JOB_AND_STATUS_COMBINATION.Length; condPos++)
            {
                if (condPos > 0)
                    where.Append(" OR ");
                var tuple = DirectionAssign.JOB_AND_STATUS_COMBINATION[condPos];
                where.Append($"(jobID = '{tuple.Item1}' AND [docStatus] IN (");
                for (var pos = 0; pos < tuple.Item2.Length; pos++)
                {
                    if (pos > 0)
                        where.Append(",");
                    where.Append($"'{tuple.Item2[pos]}'");
                }
                where.Append("))");
            }
            where.Append(")");

            // get result
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var sql = new Sql();
                sql.Append("SELECT da.workerID, da.jobID, COUNT(DISTINCT di.directionID) AS cntDocs, COUNT(DISTINCT it.directionItemID) AS cntItems ");
                sql.Append("FROM [dbo].[s4_direction] di WITH (NOLOCK) JOIN[dbo].[s4_direction_assign] da ON(di.directionID = da.directionID) JOIN[dbo].[s4_direction_items] it ON(di.directionID = it.directionID) ");
                sql.Where(where.ToString());
                if (workerID != null)
                    sql.Where("da.workerID = @0", workerID);
                sql.Append("GROUP BY da.workerID, da.jobID");
                return db.Fetch<ScanAssignmentModel>(sql);
            }
        }


        public List<DirectionAssignStatisticsModel> Statistics(int daysBack)
        {
            var limitDate = DateTime.Today.AddDays(-daysBack);

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var sql = new Sql()
                    .Append("SELECT j.jobID, j.workerID, j.jobsCnt, w.workerName ")
                    .Append("FROM (")
                    .Append("SELECT	da.jobID, da.workerID, COUNT(*) AS jobsCnt ")
                    .Append("FROM	[dbo].[s4_direction] d WITH (NOLOCK) JOIN [dbo].[s4_direction_assign] da ON (d.directionID = da.directionID) ")
                    .Append("WHERE	d.docDate_d > @0 ", limitDate)
                    .Append("GROUP BY da.jobID, da.workerID ")
                    .Append(") j LEFT JOIN [dbo].[s4_workerList] w ON (j.workerID = w.workerID) ");

                return db.Fetch<DirectionAssignStatisticsModel>(sql);
            }
        }
    }
}

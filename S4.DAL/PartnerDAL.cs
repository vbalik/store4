﻿using NPoco;
using S4.Entities;
using S4.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace S4.DAL
{
    public class PartnerDAL : DAL<Entities.Partner, int>
    {
        public Partner GetModel(int id)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                return db.Query<Partner>()
                    .Where(x => x.PartnerStatus == Partner.PA_STATUS_OK)
                    .Where(x => x.PartnerID == id)
                    .SingleOrDefault();
            }
        }

        public List<Partner> FindAllByName(string name)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var result = db.Query<Partner>()
                    .Where(x => x.PartnerStatus == Partner.PA_STATUS_OK)
                    .Where(x => x.PartnerName.Contains(name))
                    .OrderBy(x => x.PartnerName)
                    .Limit(100)
                    .ToList();
                return result;
            }
        }
    }
}

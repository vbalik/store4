﻿using NPoco;
using S4.Entities;
using S4.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.DAL
{
    public class StoreMoveSerialsDAL : DAL<StoreMoveSerials, int>
    {
        public List<StoreMoveSerials> GetStoreMoveSerialsByStoMoveItemID(int stoMoveItemID)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var sql = new Sql().Where("stoMoveItemID = @0", stoMoveItemID);
                return db.Fetch<StoreMoveSerials>(sql);
            }
        }

    }
}

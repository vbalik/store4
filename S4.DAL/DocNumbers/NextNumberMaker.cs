﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.DAL.DocNumbers
{
    public class NextNumberMaker
    {
        private readonly string _sqlQuery;
        private object _lock = new object();
        private Dictionary<string, int> _maxValues = new Dictionary<string, int>(16);


        public NextNumberMaker(string tableName)
        {
            _sqlQuery = $"SELECT MAX(docNumber) FROM {tableName} WHERE docNumPrefix = @0";
        }


        public int GetNextDocNumber(IDatabase db, string docNumPrefix)
        {
            lock(_lock)
            {
                var nextNumber = DBNextNumber(db, docNumPrefix);

                int lastNumber = -1;
                if (_maxValues.TryGetValue(docNumPrefix, out lastNumber))
                {
                    if (lastNumber >= nextNumber)
                        nextNumber = lastNumber + 1;
                    _maxValues[docNumPrefix] = nextNumber;
                }
                else
                {
                    _maxValues.Add(docNumPrefix, nextNumber);
                }
                
                return nextNumber;
            }
        }


        private int DBNextNumber(IDatabase db, string docNumPrefix)
        {
            var nextNumber = db.SingleOrDefault<int?>(_sqlQuery, docNumPrefix);
            return (nextNumber == null) ? 1 : nextNumber.Value + 1;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4.DAL.DocNumbers
{
    public class NextNumberBase
    {
        public NextNumberMaker NextNumber_StoreMove { get; } = new NextNumberMaker("s4_storemove");
        public NextNumberMaker NextNumber_Direction{ get; } = new NextNumberMaker("s4_direction");
    }
}

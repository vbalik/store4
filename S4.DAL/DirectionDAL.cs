﻿using NPoco;
using S4.Entities;
using S4.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace S4.DAL
{
    public class DirectionDAL : DAL<Entities.Direction, int>
    {
        public DirectionModel GetDirectionAllData(int directionID)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                return GetDirectionAllData(directionID, db);
            }
        }

        public DirectionModel GetDirectionAllData(int directionID, Database db)
        {
            var model = db.SingleOrDefaultById<DirectionModel>(directionID);
            if (model != null)
            {
                var sql = new Sql().Where("directionID = @0", directionID);
                model.History = db.Fetch<DirectionHistory>(sql);
                model.Items = db.Fetch<DirectionItem>(sql);
                model.Assigns = db.Fetch<DirectionAssign>(sql);
                model.XtraData.Load(db, sql);
            }

            return model;
        }


        public DirectionModel GetDirectionXtraData(int directionID)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var model = db.SingleOrDefaultById<DirectionModel>(directionID);
                if (model != null)
                {
                    var sql = new Sql().Where("directionID = @0", directionID);
                    model.XtraData.Load(db, sql);
                }

                return model;
            }
        }

        public List<Direction> ScanUnprocessed(int dayLimit)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                using (var tr = db.GetTransaction(System.Data.IsolationLevel.ReadUncommitted))
                {
                    var limitDate = DateTime.Today.AddDays(-dayLimit);
                    var data = db.Query<Direction>().Where(d =>
                        d.DocDirection == Direction.DOC_DIRECTION_OUT
                        && d.DocStatus == Direction.DR_STATUS_LOAD
                        && d.DocDate < limitDate
                        ).ToList();

                    return data;
                }
            }
        }


        public int AddDirectionHistory(DirectionHistory directionHistory)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var id = db.Insert(directionHistory);
                return int.Parse(id.ToString());
            }
        }



        public List<Direction> ListByStatus(string docStatus)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                return db.Query<Direction>().Where(d => d.DocStatus == docStatus).ToList();
            }
        }

        public List<DirectionHistory> GetDirectionHistory(int directionID)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var sql = new Sql().Where("directionID = @0", directionID);
                return db.Fetch<DirectionHistory>(sql);
            }
        }


        public string GetStatus(int directionID)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                return db.SingleOrDefault<string>("SELECT docStatus FROM [dbo].[s4_direction] WHERE directionID = @0", directionID);
            }
        }


        public class GetItemsStatusModel
        {
            public string ItemStatus { get; set; }
            public int Cnt { get; set; }
        }


        public List<GetItemsStatusModel> GetItemsStatus(int directionID)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                return db.Fetch<GetItemsStatusModel>("SELECT itemStatus, COUNT(*) AS Cnt FROM [dbo].[s4_direction_items] WHERE directionID = @0 GROUP BY itemStatus", directionID);
            }
        }

        public List<DirectionModel> FindDirectionsByDocInfo(string docInfo, int? docYear, int items = 50)
        {
            var directions = new List<DirectionModel>();

            if (docInfo == null || string.IsNullOrEmpty(docInfo)) return directions;

            var parts = docInfo.Split('/');

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var year = docYear != null && docYear > 0 ? docYear.ToString() : DateTime.Now.ToString("yy");

                var sql = new Sql();

                if (parts.Length == 1)
                {
                    if (!int.TryParse(parts[0], out _))
                        return new List<DirectionModel>();
                    else
                        sql.Where("docYear = @0 AND docNumber LIKE @1", year, "%" + parts[0]);
                }
                if (parts.Length == 2)
                {
                    if (!int.TryParse(parts[1], out _))
                        return new List<DirectionModel>();
                    else
                        sql.Where("docYear = @0 AND docNumber = @1", parts[0], parts[1]);
                }
                if (parts.Length == 3)
                {
                    if (!int.TryParse(parts[2], out _))
                        return new List<DirectionModel>();
                    else
                        sql.Where("docNumPrefix = @0 AND docYear = @1 AND docNumber = @2", parts[0].ToUpper(), parts[1], parts[2]);
                }

                sql.OrderBy("directionID DESC");

                directions = db.Fetch<DirectionModel>(1, items, sql);
            }

            return directions;
        }

        public List<DirectionModel> GetDirectionsByAbraDocID(string abraDocID)
        {
            //// Abra has multiple number variations with leading zeros

            // add leading zero
            abraDocID = abraDocID.StartsWith("0") ? abraDocID : "0" + abraDocID;
            // remove leading zero
            var abraDocIdWithoutZeroLeading = abraDocID.Substring(1);

            //// end Abra

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var sql = new Sql();

                // get directionID from xtra by abraID
                sql.Where("xtraCode = @0 AND ((xtraValue_checksum = checksum(@1) AND xtraValue = @1) OR (xtraValue = @2 AND xtraValue_checksum = checksum(@2)))", Direction.XTRA_ABRA_DOC_ID, abraDocID, abraDocIdWithoutZeroLeading);
                var xtra = db.FirstOrDefault<DirectionXtra>(sql);

                // get direction
                if (xtra != null)
                {
                    var sqldir = new Sql().Where("directionID = @0", xtra.DirectionID);
                    return db.Fetch<DirectionModel>(sqldir);
                }

                return new List<DirectionModel>();
            }
        }

        public List<DirectionModel> GetDirectionByInvoiceNumber(string invoiceNumber)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var xtraSql = new Sql().Where("xtraCode = @0 AND xtraValue_checksum = checksum(@1) AND xtraValue = @1", Direction.XTRA_INVOICE_NUMBER, invoiceNumber);
                var xtras = db.Fetch<DirectionXtra>(xtraSql);

                if (xtras.Count > 0)
                {
                    var ids = xtras.Select(_ => _.DirectionID).ToArray();
                    return db.Query<DirectionModel>().Where(_ => ids.Contains(_.DirectionID)).ToList();
                }

                return new List<DirectionModel>();
            }
        }

        public List<Direction> ListByDocDirection(int docDirection, int records)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                return db.Fetch<Direction>().Where(d => d.DocDirection == docDirection).OrderByDescending(_ => _.DirectionID).Take(records).ToList();
            }
        }
    }
}

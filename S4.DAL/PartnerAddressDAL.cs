﻿using NPoco;
using S4.Entities;
using S4.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace S4.DAL
{
    public class PartnerAddressDAL : DAL<Entities.PartnerAddress, int>
    {
        public PartnerAddress GetModel(int id)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                return db.Query<PartnerAddress>()
                    .Where(x => x.PartAddrID == id)
                    .SingleOrDefault();
            }
        }


        public List<PartnerAddress> FindAllByName(string name)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var result = db.Query<PartnerAddress>()
                    .Where(x => x.AddrName.Contains(name) || x.DeliveryInst.Contains(name))
                    .OrderBy(x => x.PartAddrID)
                    .Limit(100)
                    .ToList();
                return result;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPoco;
using S4.DAL.Interfaces;
using S4.Entities;
using S4.Entities.Models;
using static S4.Entities.DeliveryDirection;

namespace S4.DAL
{
    public class BookingDAL : DAL<Booking, int>, IBookings
    {
        public List<BookingValidExternalModel> GetValid()
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var sql = new Sql();
                sql.Select("a.articleCode, a.articleDesc, l.batchNum, b.requiredQuantity, l.expirationDate, p.partnerName, b.bookingTimeout, w1.workerName, b.entryDateTime, b.stoMoveLotID");
                sql.From("s4_bookings b");
                sql.LeftJoin("s4_storeMove_lots l on l.stoMoveLotID = b.stoMoveLotID");
                sql.LeftJoin("s4_vw_article_base a on l.artPackID = a.artPackID");
                sql.LeftJoin("s4_direction dir on dir.directionID = b.usedInDirectionID");
                sql.LeftJoin("s4_partnerList p on p.partnerID = b.partnerID");
                sql.LeftJoin("s4_workerList w1 on w1.workerID = b.entryUserID");
                sql.Where("bookingStatus = @0", Booking.VALID);
                sql.OrderBy("a.articleCode");
                return db.Fetch<BookingValidExternalModel>(sql);
            }
        }


        public List<BookingInfo> ListValidBookings(int artPackID)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var sql = new Sql();
                sql.Select("b.*");
                sql.From("s4_bookings b");
                sql.LeftJoin("s4_storeMove_lots l on l.stoMoveLotID = b.stoMoveLotID");
                sql.Where("b.bookingStatus = @0", Booking.VALID);
                sql.Where("b.bookingTimeout >= @0", DateTime.Today);
                sql.Where("l.artPackID = @0", artPackID);
                var list = db.Fetch<Booking>(sql);

                return list.Select(_ => new BookingInfo() 
                {  
                    BookingID = _.BookingID,
                    StoMoveLotID = _.StoMoveLotID,
                    PartnerID = _.PartnerID,
                    PartAddrID = _.PartAddrID,
                    RequiredQuantity = _.RequiredQuantity
                }).ToList();
            }
        }


        public void BookingUsed(IDatabase db, int bookingID, decimal usedQuantity, int usedInDirectionID)
        {
            var booking = db.SingleById<Booking>(bookingID);

            booking.BookingStatus = Booking.USED;
            booking.UsedInDirectionID = usedInDirectionID;
            db.Update(booking);

            var rest = booking.RequiredQuantity - usedQuantity;
            if (rest > 0)
            {
                var newBooking = new Booking()
                { 
                    BookingStatus = Booking.VALID,
                    StoMoveLotID = booking.StoMoveLotID,
                    PartnerID = booking.PartnerID,
                    PartAddrID = booking.PartAddrID,
                    RequiredQuantity = rest,
                    BookingTimeout = booking.BookingTimeout,
                    ParentBookingID = booking.BookingID,
                    EntryDateTime = DateTime.Now,
                    EntryUserID = booking.EntryUserID
                };

                db.Insert(newBooking);
            }                           
        }


        public void BookingUseCanceled(Database db, int directionID)
        {
            var bookings = db.Query<Booking>().Where(_ => _.UsedInDirectionID == directionID && _.BookingStatus == Booking.USED).ToList();
            foreach (var booking in bookings)
            {
                // reset use
                booking.BookingStatus = Booking.VALID;
                booking.UsedInDirectionID = null;
                db.Update(booking);

                // delete folowups
                db.Execute("DELETE FROM s4_bookings WHERE [parentBookingID] = @0", booking.BookingID);
            }
        }
    }
}

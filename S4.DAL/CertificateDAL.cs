﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPoco;
using S4.Entities;
using S4.Entities.Models;
using static S4.Entities.DeliveryDirection;

namespace S4.DAL
{
    public class CertificateDAL : DAL<Certificate, int>
    {
        public Certificate GetByType(string certificateType, Database db)
        {
            var sql = new Sql().Where("certificateType = @0 AND GETDATE() BETWEEN expirationDateFrom AND expirationDateTo", certificateType);
            return db.FirstOrDefault<Certificate>(sql);
        }

       
    }
}

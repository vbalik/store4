﻿using S4.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.DAL
{
    public class SettingsSet
    {
        
        private readonly SettingCache _settingCache = new SettingCache();


        public bool GetValue_Boolean(string settigPath)
        {
            var (settingsDictItem, resultValue) = GetValue(settigPath);
            return (bool)resultValue;
        }

        public int GetValue_Int32(string settigPath)
        {
            var (settingsDictItem, resultValue) = GetValue(settigPath);
            return (Int32)resultValue;
        }

        public string GetValue_String(string settigPath)
        {
            var (settingsDictItem, resultValue) = GetValue(settigPath);
            return (string)resultValue;
        }


        private (SettingsDictionary.SettingsDictionaryItem settingsDictItem, object resultValue) GetValue(string settigPath)
        {
            var settingsDictItem = SettingsDictionary.ByPath(settigPath);
            if (settingsDictItem == null)
                throw new Exception($"Settings value {settigPath} is not defined");

            var setting = _settingCache.GetItem(settigPath);
            if (setting == null)
                return (settingsDictItem, settingsDictItem.DefaultValue);
            else
            {
                var result = Convert.ChangeType(setting.SetValue, settingsDictItem.SettingValueType);
                return (settingsDictItem, result);
            }
        }

    }
}

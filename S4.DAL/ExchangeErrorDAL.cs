﻿using NPoco;
using S4.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace S4.DAL
{
    public class ExchangeErrorDAL : DAL<Entities.ExchangeError, int>
    {
        public List<Tuple<DateTime, int>> Last10DaysCount()
        {
            var sql = new Sql();
            sql.Append("SELECT CONVERT(date,[errorDateTime]) AS [date], COUNT(*) AS cnt ");
            sql.Append("FROM [dbo].[s4_exchangeError] ");
            sql.Append("WHERE ([errorDateTime] > DATEADD (day, -10, GETDATE())) ");
            sql.Append("GROUP BY CONVERT(date,[errorDateTime]) ");
            sql.Append("ORDER BY CONVERT(date,[errorDateTime])");

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                return db.Fetch<object[]>(sql).Select(i => new Tuple<DateTime, int>((DateTime)i[0], (int)i[1])).ToList();
            }
        }


        public List<Tuple<string, int>> Top10In10Days()
        {
            var sql = new Sql();
            sql.Append("SELECT [errorClass], COUNT(*) AS cnt ");
            sql.Append("FROM [dbo].[s4_exchangeError] ");
            sql.Append("WHERE ([errorDateTime] > DATEADD (day, -10, GETDATE())) ");
            sql.Append("GROUP BY [errorClass] ");
            sql.Append("ORDER BY COUNT(*) DESC");
            
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                return db.Fetch<object[]>(sql).Select(i => new Tuple<string, int>((string)i[0], (int)i[1])).Take(10).ToList();
            }
        }

        public List<Tuple<string, int>> JobNameIn10Days()
        {
            var sql = new Sql();
            sql.Append("SELECT [jobName], COUNT(*) AS cnt ");
            sql.Append("FROM [dbo].[s4_exchangeError] ");
            sql.Append("WHERE ([errorDateTime] > DATEADD (day, -10, GETDATE())) ");
            sql.Append("GROUP BY [jobName] ");
            sql.Append("ORDER BY COUNT(*) DESC");

            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                return db.Fetch<object[]>(sql).Select(i => new Tuple<string, int>((string)i[0], (int)i[1])).ToList();
            }
        }
    }
}

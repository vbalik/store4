﻿using S4.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.DAL 
{
    internal class SettingCache : Core.Cache.CacheBase<Setting>, Core.Cache.ICache<Setting, string>
    {
        public SettingCache() : base(5)
        { }


        public Setting GetItem(string setFieldID)
        {
            return base.GetOrAddExisting(setFieldID, () =>
            {
                var setting = (new DAL.DAL<Setting, string>()).Get(setFieldID);
                return setting;
            });
        }


        public new void Remove(string setFieldID)
        {
            Remove(setFieldID);
        }
    }
}

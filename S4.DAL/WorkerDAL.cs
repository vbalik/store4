﻿using NPoco;
using S4.Entities;
using System;
using System.Collections.Generic;

namespace S4.DAL
{
    public class WorkerDAL : DAL<Entities.Worker, string>
    {
        public Worker GetByLogon(string workerLogon)
        {
            var sql = new Sql().Where("workerLogon = @0", workerLogon);
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                return db.SingleOrDefault<Entities.Worker>(sql);
            }
        }


        public List<Worker> ListByWorkerClass(Worker.WorkerClassEnum workerClass)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var sql = new Sql().Where($"workerClass & {(long)workerClass} = {(long)workerClass}");
                var result = db.Fetch<Worker>(sql);                
                return result;
            }
        }
    }
}

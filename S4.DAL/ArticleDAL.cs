﻿using NPoco;
using S4.Entities;
using S4.Entities.Models;
using System.Collections.Generic;
using System.Linq;

namespace S4.DAL
{
    public class ArticleDAL : DAL<Entities.Article, int>
    {
        public ArticleModel GetModel(int id)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var model = db.SingleOrDefaultById<ArticleModel>(id);
                if (model != null)
                {
                    model.Packings = db.Fetch<ArticlePacking>(new Sql().Where("articleID = @0", id));

                    // find another packing barcodes
                    var artPackIDs = model.Packings.Select(_ => _.ArtPackID).ToList();
                    if (!artPackIDs.Any()) return model;

                    model.PackingBarCodes = db.Fetch<ArticlePackingBarCode>(new Sql().Where("artPackID IN (@0)", artPackIDs));

                    model.XtraData.Load(db, new Sql().Where("articleID = @0", id));
                }

                return model;
            }
        }


        public ArticleModel GetBySourceID(string sourceID)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var model = db.SingleOrDefault<ArticleModel>(new Sql().Where("source_id = @0", sourceID));
                if (model != null)
                    model.Packings = db.Fetch<ArticlePacking>(new Sql().Where("articleID = @0", model.ArticleID));
                return model;
            }
        }

        public ArticleModel GetArticleXtraData(int articleID)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                var model = db.SingleOrDefaultById<ArticleModel>(articleID);
                if (model != null)
                {
                    model.XtraData.Load(db, new Sql().Where("directionID = @0", articleID));
                }

                return model;
            }
        }

        public List<ArticleModel> ScanByArticleCode(string articleCode, bool incLocked)
        {
            var sql = new Sql().Where("articleCode LIKE @0", articleCode);
            return FetchModelList(sql, incLocked);
        }


        public List<ArticleModel> ScanByArticleDesc(string articleDesc, bool incLocked)
        {
            // search - ignore czech characters
            var sql = new Sql().Where("articleDesc LIKE @0 COLLATE Latin1_General_CI_AI", articleDesc);
            return FetchModelList(sql, incLocked);
        }


        public List<ArticleModel> ScanByBarCode(string barCode, bool incLocked)
        {
            var sql = new Sql().Where("articleID IN (SELECT articleID FROM s4_articlelist_packings WITH (NOLOCK) WHERE barCode LIKE @0)", barCode);
            var result = FetchModelList(sql, incLocked);

            if (result.Any()) return result;

            // try find another barcode
            var sql2 = new Sql().Where("articleID IN (SELECT articleID FROM s4_articlelist_packing_barcodes WITH (NOLOCK) WHERE barCode LIKE @0)", barCode);
            var result2 = FetchModelList(sql2, incLocked);

            if (result2.Any()) return result2;


            // try find barCode with zero at start
            var sql3 = new Sql().Where("articleID IN (SELECT articleID FROM s4_articlelist_packings WITH (NOLOCK) WHERE barCode LIKE @0)", "%" + barCode);

            return FetchModelList(sql3, incLocked);
        }


        private List<ArticleModel> FetchModelList(Sql sqlWhere, bool incLocked)
        {
            using (var db = Core.Data.ConnectionHelper.GetDB())
            {
                if (!incLocked)
                    sqlWhere.Where("articleStatus = @0", Article.AR_STATUS_OK);
                var result = db.Fetch<ArticleModel>(sqlWhere);

                if (!result.Any()) return new List<ArticleModel>();

                foreach (var item in result)
                    item.Packings = db.Query<ArticlePacking>().Where(_ => _.ArticleID == item.ArticleID).Where(_ => _.PackStatus == ArticlePacking.AP_STATUS_OK).ToList();

                // find another packing barcodes
                var artPackIDs = result.Select(a => a.Packings.Select(_ => _.ArtPackID)).ToList();
                var packingBarCodes = db.Fetch<ArticlePackingBarCode>(new Sql().Where("artPackID IN (@0)", artPackIDs.ToArray()));

                // set another packing barcodes
                result.ForEach(a => a.PackingBarCodes = packingBarCodes.Where(_ => _.ArticleID == a.ArticleID).ToList());

                return result;
            }
        }
    }
}

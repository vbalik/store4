﻿using System;
using System.Collections.Generic;
using System.Text;
using TestHelpers;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using System.Threading;
using System.Threading.Tasks;

namespace S4.Report.Tests.Helpers
{
    public class ProcTestBase
    {
        protected const string USER_ID = "USER1";

        protected DatabaseFixture fixture;


        public ProcTestBase(DatabaseFixture fixture)
        {
            this.fixture = fixture;
            Core.Data.ConnectionHelper.ConnectionString = fixture.ConnectionString;


            // setup ServicesFactory.ServiceProvider
            var sc = new ServiceCollection();
            // IBackgroundTaskQueue
            var backgroundTaskQueue = new Mock<Core.Interfaces.IBackgroundTaskQueue>();
            backgroundTaskQueue.Setup(m => m.QueueBackgroundWorkItem(It.IsAny<Func<CancellationToken, Task>>(), It.IsAny<Func<CancellationToken, Task>>())).Returns("123");
            sc.AddSingleton<Core.Interfaces.IBackgroundTaskQueue>(backgroundTaskQueue.Object);

            var serviceProvider = sc.BuildServiceProvider();
            S4.Procedures.ServicesFactory.ServiceProvider = serviceProvider;
        }


        protected NPoco.Database GetDB()
        {
            var db = new NPoco.Database(fixture.ConnectionString, NPoco.DatabaseType.SqlServer2012, System.Data.SqlClient.SqlClientFactory.Instance);
            db.Mappers.Add(new Core.Data.NPocoEnumMapper());
            return db;
        }
    }
}

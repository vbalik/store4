﻿using Xunit;
using S4.ProcedureModels.Dispatch;
using TestHelpers;
using S4.Procedures.Dispatch;
using System.IO;
using S4.Procedures.Tests;
using S4.Procedures.Info;
using S4.ProcedureModels.Info;

namespace S4.Report.Tests.ProcedureTests
{
    [Collection("ProcedureTests")]
    public class PrintShippingDocumentADRProcTest : ProcTestBase
    {
        public PrintShippingDocumentADRProcTest(DatabaseFixture fixture) : base(fixture) 
        { }


        [Fact]
        public void PrintShippingDocumentADRProcTest_Success()
        {
            var model = new PrintShippingDocumentADR()
            {
                DirectionID = 74,
                ReportID = 10,
                PrinterLocationID = 1
            };

            var proc = new PrintShippingDocumentADRProc(USER_ID) { IsTesting = true };
            var res = proc.DoProcedure(model);
            Assert.True(res.Success);

            //File.WriteAllBytes(@"c:\temp\PrintShippingDocumentADRProcTestData.txt", res.FileData);
                                 
            //read test data
            var path = @"..\..\..\ProcedureTests\TestData\PrintShippingDocumentADRProcTestData.txt";
            var testData = File.ReadAllBytes(path);

            Assert.Equal(System.Text.Encoding.Unicode.GetString(testData), System.Text.Encoding.Unicode.GetString(res.FileData));

        }
    }
}

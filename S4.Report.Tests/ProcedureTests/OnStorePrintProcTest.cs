﻿using Xunit;
using TestHelpers;
using S4.Procedures.Dispatch;
using System.IO;
using S4.Procedures.Tests;
using S4.ProcedureModels.Info;
using S4.Procedures.Info;

namespace S4.Report.Tests.ProcedureTests
{
    [Collection("ProcedureTests")]
    public class OnStorePrintProcTest : ProcTestBase
    {
        public OnStorePrintProcTest(DatabaseFixture fixture) : base(fixture) 
        { }


        [Fact]
        public void OnStorePrintProcTest_Success()
        {
            var model = new OnStorePrint()
            {
                OnStorePrintSource = OnStorePrint.OnStorePrintSourceEnum.OnStorePrintSourcePosition,
                SourceID = 21,
                PrinterLocationID = 2,
                ReportID = 6
            };
            var proc = new OnStorePrintProc(USER_ID) { IsTesting = true };
            var res = proc.DoProcedure(model);
            Assert.True(res.Success);
            //read test data
            var path = @"..\..\..\ProcedureTests\TestData\OnStorePrintProcTestData.txt";
            var testData = File.ReadAllBytes(path);

            Assert.Equal(System.Text.Encoding.Unicode.GetString(testData), System.Text.Encoding.Unicode.GetString(res.FileData));

        }
    }
}

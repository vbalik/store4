﻿using Xunit;
using S4.ProcedureModels.Dispatch;
using TestHelpers;
using System.IO;
using S4.Procedures.Tests;
using S4.Procedures.Move;
using S4.ProcedureModels.Move;

namespace S4.Report.Tests.ProcedureTests
{
    [Collection("ProcedureTests")]
    public class MovePrintProcTest : ProcTestBase
    {
        public MovePrintProcTest(DatabaseFixture fixture) : base(fixture) 
        { }


        [Fact]
        public void MovePrintProcTest_Success()
        {
            var model = new MovePrint()
            {
                StoMoveID = 518,
                PrinterLocationID = 2,
                ReportID = 7
            };
            var proc = new MovePrintProc(USER_ID) { IsTesting = true };
            var res = proc.DoProcedure(model);
            Assert.True(res.Success);
            
            //read test data
            var path = @"..\..\..\ProcedureTests\TestData\MovePrintProcTestData.txt";
            var testData = File.ReadAllBytes(path);

            Assert.Equal(System.Text.Encoding.Unicode.GetString(testData), System.Text.Encoding.Unicode.GetString(res.FileData));

        }
    }
}

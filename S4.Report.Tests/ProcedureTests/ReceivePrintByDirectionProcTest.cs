﻿using Xunit;
using TestHelpers;
using System.IO;
using S4.Procedures.Tests;
using S4.Procedures.Move;
using S4.ProcedureModels.Move;
using S4.ProcedureModels.Receive;
using S4.Procedures.Receive;

namespace S4.Report.Tests.ProcedureTests
{
    [Collection("ProcedureTests")]
    public class ReceivePrintByDirectionProcTest : ProcTestBase
    {
        public ReceivePrintByDirectionProcTest(DatabaseFixture fixture) : base(fixture) 
        { }


        [Fact]
        public void ReceivePrintByDirectionProcTest_Success()
        {
            var model = new ReceivePrintByDirection()
            {
                DirectionID = 32,
                PrinterLocationID = 2,
                ReportID = 8
            };
            var proc = new ReceivePrintByDirectionProc(USER_ID) { IsTesting = true };
            var res = proc.DoProcedure(model);
            Assert.True(res.Success);

            //read test data
            var path = @"..\..\..\ProcedureTests\TestData\ReceivePrintByDirectionProcTestData.txt";
            var testData = File.ReadAllBytes(path);

            Assert.Equal(System.Text.Encoding.Unicode.GetString(testData), System.Text.Encoding.Unicode.GetString(res.FileData));

        }
    }
}

﻿using Xunit;
using S4.ProcedureModels.Dispatch;
using TestHelpers;
using S4.Procedures.Dispatch;
using System.IO;
using S4.Procedures.Tests;

namespace S4.Report.Tests.ProcedureTests
{
    [Collection("ProcedureTests")]
    public class DispatchPrintProcTest : ProcTestBase
    {
        public DispatchPrintProcTest(DatabaseFixture fixture) : base(fixture) 
        { }


        [Fact]
        public void DispatchPrintProcTest_Success()
        {
            var model = new DispatchPrint()
            {
                DirectionID = 32,
                PrinterLocationID = 2,
                ReportID = 5
            };
            var proc = new DispatchPrintProc(USER_ID) { IsTesting = true };
            var res = proc.DoProcedure(model);
            Assert.True(res.Success);

            //read test data
            var path = @"..\..\..\ProcedureTests\TestData\DispatchPrintProcTestData.txt";
            var testData = File.ReadAllBytes(path);

            Assert.Equal(System.Text.Encoding.Unicode.GetString(testData), System.Text.Encoding.Unicode.GetString(res.FileData));

        }
    }
}

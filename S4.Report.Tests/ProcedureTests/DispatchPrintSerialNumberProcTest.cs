﻿using Xunit;
using S4.ProcedureModels.Dispatch;
using TestHelpers;
using S4.Procedures.Dispatch;
using System.IO;
using S4.Procedures.Tests;

namespace S4.Report.Tests.ProcedureTests
{
    [Collection("ProcedureTests")]
    public class DispatchPrintSerialNumberProcTest : ProcTestBase
    {
        public DispatchPrintSerialNumberProcTest(DatabaseFixture fixture) : base(fixture) 
        { }


        [Fact]
        public void DispatchPrintSerialNumberProcTest_Success()
        {
            var model = new DispatchPrintSerialNumber()
            {
                DirectionID = 74,
                ReportID = 9,
                PrinterLocationID = 1
            };

            var proc = new DispatchPrintSerialNumberProc(USER_ID) { IsTesting = true };
            var res = proc.DoProcedure(model);
            Assert.True(res.Success);

            //File.WriteAllBytes(@"c:\temp\DispatchPrintSerialNumberProcTestData.txt", res.FileData);
                                 
            //read test data
            var path = @"..\..\..\ProcedureTests\TestData\DispatchPrintSerialNumberProcTestData.txt";
            var testData = File.ReadAllBytes(path);

            Assert.Equal(System.Text.Encoding.Unicode.GetString(testData), System.Text.Encoding.Unicode.GetString(res.FileData));

        }
    }
}

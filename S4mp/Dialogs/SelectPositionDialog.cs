﻿using Xamarin.Forms;
using S4mp.Controls;
using S4mp.Controls.Common;
using S4mp.InternalControls.Positions;

namespace S4mp.Dialogs
{
    public class SelectPositionDialog : Controls.Dialogs.EditFormDialogBase
    {
        private SearchPositionControl _searchPositionControl;

        protected override ButtonsBar CreateButtonsBar()
        {
            var buttonsBar = new ButtonsBar(ButtonsBar.ButtonsEnum.Cancel);
            return buttonsBar;
        }

        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await OnButtonClickedAsync(ButtonsBar.ButtonsEnum.Cancel);
            });
            return true;
        }

        public S4.Entities.Position Position { get; private set; }

        public void UnsuccessfullExtScan(string barCode)
        {
            _searchPositionControl.UnsuccessfullExtScan(barCode);
        }

        protected override void Init(bool scrolled)
        {
            base.Init(scrolled);

            _searchPositionControl = new SearchPositionControl();
            _searchPositionControl.OnSuccessCommand = new Command<S4.Entities.Position>((pos) =>
            {
                Position = pos;
                Device.BeginInvokeOnMainThread(async () =>
                {
                    await OnButtonClickedAsync(ButtonsBar.ButtonsEnum.OK);
                });
            });

            Content = new StackLayout()
            {
                Children =
                {
                    _searchPositionControl,
                    ButtonsBar
                }
            };
        }

        public void SetHeaderText(string headerText, string headerTextSub = null)
        {
            ((SearchPositionControlVM)_searchPositionControl.BindingContext).HeaderText = headerText;
            ((SearchPositionControlVM)_searchPositionControl.BindingContext).HeaderTextSub = headerTextSub;
        }
    }
}

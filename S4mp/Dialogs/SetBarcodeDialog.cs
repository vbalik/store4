﻿using S4mp.Controls;
using S4mp.Controls.Common;
using Xamarin.Forms;
using static S4mp.InternalControls.SetArticle.SetArticleView;

namespace S4mp.Dialogs
{
    public class SetBarcodeDialog : Controls.Dialogs.EditFormDialogBase
    {
        private InternalControls.SetArticle.SetArticleView _setArticleView;

        public string Barcode => _setArticleView.BarCode;
        public int? ArtPackID { get; set; } // tempVal for multiple usage on receive

        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await OnButtonClickedAsync(ButtonsBar.ButtonsEnum.Cancel);
            });
            return true;
        }

        protected override ButtonsBar CreateButtonsBar()
        {
            var buttonsBar = new ButtonsBar(ButtonsBar.ButtonsEnum.Cancel, ButtonsBar.ButtonsEnum.OK);
            return buttonsBar;
        }

        protected override void Init(bool scrolled)
        {
            base.Init(scrolled);

            ArtPackID = null;

            _setArticleView = new InternalControls.SetArticle.SetArticleView();
            _setArticleView.ValueChanged += (s, e) => SetChanged(!string.IsNullOrWhiteSpace(_setArticleView.BarCode));
            _setArticleView.ValueCommited += (s, e) =>
            {
                var btn = e is ButtonPressedEventArgs ? ((ButtonPressedEventArgs) e).Button : ButtonsBar.ButtonsEnum.OK;

                Device.BeginInvokeOnMainThread(async () =>
                {
                    await OnButtonClickedAsync(btn);
                });
            };

            Content = new StackLayout()
            {
                Children =
                {
                    _setArticleView,
                    ButtonsBar
                }
            };
        }
    }
}

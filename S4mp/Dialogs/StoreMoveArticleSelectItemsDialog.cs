﻿using S4.Core;
using S4.Entities;
using S4.Entities.Views;
using S4mp.Controls.Common;
using S4mp.InternalControls.StoreMove;
using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace S4mp.Dialogs
{
    /// <summary>
    /// Show items on positions by ARTICLE CARD
    /// </summary>
    public class StoreMoveArticleSelectItemsDialog : Controls.Dialogs.EditFormDialogBase
    {
        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await OnButtonClickedAsync(ButtonsBar.ButtonsEnum.Cancel);
            });
            return base.OnBackButtonPressed();
        }

        protected override ButtonsBar CreateButtonsBar()
        {
            var buttonsBar = new ButtonsBar(ButtonsBar.ButtonsEnum.Cancel, ButtonsBar.ButtonsEnum.OK);
            return buttonsBar;
        }

        public Position SourcePosition => Control.SourcePosition;
        public List<ArtOnStoreView> SelectedItems => Control.SelectedItems;

        public StoreMoveArticleItemsControl Control { get; set; }

        protected override void Init(bool scrolled)
        {
            base.Init(scrolled);

            Control = new StoreMoveArticleItemsControl();
            Control.NoItemsFoundEvent += SelectStoreMoveItemsDialog_NoItemsFoundEvent;
            Control.SelectItemEvent += (s, e) =>
            {
                SetChanged(Control.SelectedItems.Count > 0);
            };

            Content = new StackLayout()
            {
                Children =
                {
                    Control,
                    ButtonsBar
                }
            };
        }

        private async void SelectStoreMoveItemsDialog_NoItemsFoundEvent(object sender, EventArgs e)
        {
            if (Control.SourceItems.Count == 0)
            {
                Singleton<Helpers.Audio>.Instance.PlayError();
                await App.Current.MainPage.DisplayAlert("Informace", "Žádné položky", "Zavřít");
                Device.BeginInvokeOnMainThread(async () =>
                {
                    await OnButtonClickedAsync(ButtonsBar.ButtonsEnum.None);
                });
            }
        }

        public void SetHeaderText(string headerText, string headerTextSub = null)
        {
            Control.HeaderText = headerText;
            Control.HeaderTextSub = headerTextSub;
        }

        public void SetData(int articleID)
        {
            Control.LoadData(articleID);
        }
    }
}

﻿using S4mp.Controls.Common;
using S4mp.InternalControls.Dispatch;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace S4mp.Dialogs
{
    public class DispatchPrintLabelsDialog : Controls.Dialogs.EditFormDialogBase
    {
        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await OnButtonClickedAsync(ButtonsBar.ButtonsEnum.Cancel);
            });
            return true;
        }

        protected override ButtonsBar CreateButtonsBar()
        {
            var buttonsBar = new ButtonsBar(ButtonsBar.ButtonsEnum.Cancel, ButtonsBar.ButtonsEnum.OK);
            return buttonsBar;
        }

        public PrintDispatchLabelsControl Control { get; set; }

        protected override void Init(bool scrolled)
        {

            base.Init(scrolled);

            Control = new PrintDispatchLabelsControl();
            Control.PrinterChangedEvent += (s, e) => SetChanged(e);

            Content = new StackLayout()
            {
                Children =
                {
                    Control,
                    ButtonsBar
                }
            };
        }

        public void SetHeaderText(string headerText, string headerTextSub = null)
        {
            Control.HeaderText = headerText;
            Control.HeaderTextSub = headerTextSub;
        }

        public async Task SetData(int directionID)
        {
            await Control.LoadData(directionID);
        }
    }
}

﻿using S4mp.Controls.Common;
using S4mp.InternalControls.StoreMove;
using Xamarin.Forms;

namespace S4mp.Dialogs
{
    public class SetStoreMoveRemarkDialog : Controls.Dialogs.EditFormDialogBase
    {
        protected override ButtonsBar CreateButtonsBar()
        {
            var buttonsBar = new ButtonsBar(ButtonsBar.ButtonsEnum.Cancel, ButtonsBar.ButtonsEnum.OK);
            return buttonsBar;
        }

        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await OnButtonClickedAsync(ButtonsBar.ButtonsEnum.Cancel);
            });
            return true;
        }

        public string Remark => _setStockMoveRemarkControl.Remark;

        private SetStoreMoveRemarkControl _setStockMoveRemarkControl;

        protected async override void Init(bool scrolled)
        {
            base.Init(scrolled);

            _setStockMoveRemarkControl = new SetStoreMoveRemarkControl();

            Content = new StackLayout()
            {
                Children =
                {
                    _setStockMoveRemarkControl,
                    ButtonsBar
                }
            };

            SetChanged(true);
        }

        public void SetHeaderText(string headerText, string headerTextSub = null)
        {
            _setStockMoveRemarkControl.HeaderText = headerText;
            _setStockMoveRemarkControl.HeaderTextSub = headerTextSub;
        }
    }
}

﻿using S4mp.Controls.Common;
using S4mp.InternalControls.StoreMove;
using Xamarin.Forms;

namespace S4mp.Dialogs
{
    public class StoreMoveNavigationDialog : Controls.Dialogs.EditFormDialogBase
    {
        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await OnButtonClickedAsync(ButtonsBar.ButtonsEnum.Cancel);
            });
            return base.OnBackButtonPressed();
        }

        protected override ButtonsBar CreateButtonsBar()
        {
            var buttonsBar = new ButtonsBar(ButtonsBar.ButtonsEnum.Cancel);
            return buttonsBar;
        }

        public StoreMoveNavigationControl Control { get; set; }

        protected override void Init(bool scrolled)
        {

            base.Init(scrolled);

            Control = new StoreMoveNavigationControl();
            Control.ButtonPressedEvent += Control_ButtonPressedEvent;

            Content = new StackLayout()
            {
                Children =
                {
                    Control,
                    ButtonsBar
                }
            };
        }

        private void Control_ButtonPressedEvent(object sender, int e)
        {
            var btn = e == 1 ? ButtonsBar.ButtonsEnum.OK : ButtonsBar.ButtonsEnum.No;
            Device.BeginInvokeOnMainThread(async () =>
            {
                await OnButtonClickedAsync(btn);
            });
        }

        public void SetHeaderText(string headerText, string headerTextSub = null)
        {
            Control.HeaderText = headerText;
            Control.HeaderTextSub = headerTextSub;
        }
    }
}

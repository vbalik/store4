﻿using S4.Entities.Helpers;
using S4mp.Controls.Common;
using S4mp.InternalControls.Dispatch;
using S4mp.InternalControls.GetArticle;
using Xamarin.Forms;

namespace S4mp.Dialogs
{
    public class DispatchConfirmArticleDialog : Controls.Dialogs.EditFormDialogBase
    {
        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await OnButtonClickedAsync(ButtonsBar.ButtonsEnum.Cancel);
            });

            return true;
        }

        protected override ButtonsBar CreateButtonsBar()
        {
            var buttonsBar = new ButtonsBar(ButtonsBar.ButtonsEnum.Cancel, ButtonsBar.ButtonsEnum.OK);
            return buttonsBar;
        }

        protected override void OnClosing(ButtonsBar.ButtonsEnum button, ref bool canClose, ref string msg)
        {
            if (button != ButtonsBar.ButtonsEnum.Cancel)
            {
                canClose = _control.IsValid();
                msg = null;
                base.OnClosing(button, ref canClose, ref msg);
            }
        }

        private ConfirmDispatchArticleControl _control;

        protected override void Init(bool scrolled)
        {

            base.Init(scrolled);

            _control = new ConfirmDispatchArticleControl();
            _control.OnClosingEvent += _control_OnClosingEvent;

            Content = new StackLayout()
            {
                Children =
                {
                    _control,
                    ButtonsBar
                }
            };

            SetChanged(true);
        }

        private void _control_OnClosingEvent(object sender, System.EventArgs e)
        {
            var canClose = false;
            var msg = string.Empty;
            OnClosing(ButtonsBar.ButtonsEnum.OK, ref canClose, ref msg);
        }

        public void SetHeaderText(string headerText, string headerTextSub = null)
        {
            _control.SetHeaderText(headerText, headerTextSub);
        }

        public void SetData(ItemInfo item, GetArticleResultModel articleResultModel)
        {
            _control.LoadData(item, articleResultModel);
        }
    }
}

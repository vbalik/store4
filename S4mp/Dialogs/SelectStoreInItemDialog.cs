﻿using S4.Core;
using S4.Entities.Helpers;
using S4mp.Controls.Common;
using S4mp.InternalControls.StoreIn;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace S4mp.Dialogs
{
    public class SelectStoreInItemDialog : Controls.Dialogs.EditFormDialogBase
    {
        protected override ButtonsBar CreateButtonsBar()
        {
            var buttonsBar = new ButtonsBar(ButtonsBar.ButtonsEnum.Cancel);
            return buttonsBar;
        }

        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await OnButtonClickedAsync(ButtonsBar.ButtonsEnum.Cancel);
            });
            return true;
        }

        public ItemInfo Item { get; private set; }
        public int LastPositionIndex { get; private set; }

        private SelectStoreInItemControl _selectStoreInItemControl;

        protected async override void Init(bool scrolled)
        {
            base.Init(scrolled);

            _selectStoreInItemControl = new SelectStoreInItemControl();
            _selectStoreInItemControl.NoItemsFoundEvent += SelectStoreInItemDialog_NoItemsFoundEvent;
            _selectStoreInItemControl.OnSuccessCommand = new Command<SelectStoreInItemResultModel>((selectedItem) =>
            {
                Item = selectedItem.ItemInfo;
                LastPositionIndex = selectedItem.LastPositionIndex;
                Device.BeginInvokeOnMainThread(async () =>
                {
                    await OnButtonClickedAsync(ButtonsBar.ButtonsEnum.OK);
                });
            });

            Content = new StackLayout()
            {
                Children =
                {
                    _selectStoreInItemControl,
                    ButtonsBar
                }
            };
        }

        private async void SelectStoreInItemDialog_NoItemsFoundEvent(object sender, EventArgs e)
        {
            if (_selectStoreInItemControl.SourceItems.Count == 0)
            {
                Singleton<Helpers.Audio>.Instance.PlayError();
                await App.Current.MainPage.DisplayAlert("Informace", "Žádné položky", "Zavřít");
                Device.BeginInvokeOnMainThread(async () =>
                {
                    await OnButtonClickedAsync(ButtonsBar.ButtonsEnum.None);
                });
            }
        }

        public async Task SetData(int directionID, int positionID, int lastItemIndex)
        {
            await _selectStoreInItemControl.LoadData(directionID, positionID, lastItemIndex);
        }

        public void SetHeaderText(string headerText, string headerTextSub = null)
        {
            _selectStoreInItemControl.HeaderText = headerText;
            _selectStoreInItemControl.HeaderTextSub = headerTextSub;
        }
    }
}

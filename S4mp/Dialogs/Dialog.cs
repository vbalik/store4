﻿using System;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace S4mp.Dialogs
{
	public class Dialog
	{
		public event EventHandler<bool> ResultHandler;

		public static void ShowError(Exception ex, Page context)
		{
			var theEx = ex is AggregateException ? ex.InnerException : ex;
			string msg = null;
			if (theEx is System.Net.Http.HttpRequestException)
			{
				if (!DependencyService.Get<Client.Interfaces.IPing>().PingWSSync(S4mp.Client.ClientBase.ApiUrl))
				{
					msg = "Server je nedostupný";
				}
			}
			else
			{
				msg = theEx is S4mp.Exceptions.ServerUnavailableException ? "Server je nedostupný" : theEx.Message;
			}

			Device.BeginInvokeOnMainThread(async() => await context.DisplayAlert("Chyba", msg, "Potvrdit"));
		}

		public async Task DisplayConfirmation(string msg, Page context)
		{
			var result = await App.Instance.MainPage.DisplayAlert("Potvrzení", msg, "Ano", "Ne");
			if (ResultHandler != null)
				ResultHandler.Invoke(this, result);
		}
	}
}


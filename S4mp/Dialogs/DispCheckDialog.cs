﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

using S4mp.Controls.Common;
using S4.Core;

namespace S4mp.Dialogs
{
    public class DispCheckDialog : Controls.Dialogs.EditFormDialogBase
    {
        private StackLayout _contentStack;
        private InternalControls.DispCheck.DispCheckControl _control;

        public int DirectionID { get; private set; }

        public InternalControls.DispCheck.DispCheckViewModel DispCheckViewModel => _control.ViewModel;

        public async void LoadItems(S4.Entities.Direction.DispCheckMethodsEnum checkMethod, int directionID)
        {
            DirectionID = directionID;

            _control = new InternalControls.DispCheck.DispCheckControl() { CheckMethod = checkMethod, DirectionID = directionID };
            _control.Done += (s, e) =>
            {
                SetChanged(e);
                if(e) ButtonsBar.BackgroundColor = Color.LimeGreen;
            };
            _contentStack.Children.Add(_control);

            if (checkMethod == S4.Entities.Direction.DispCheckMethodsEnum.ReadBarCodes || checkMethod == S4.Entities.Direction.DispCheckMethodsEnum.ReadBarCodes2)
            {
                var btnSearch = new Button() { ImageSource = "search.png", AutomationId = "SearchButton" };
                btnSearch.Clicked += (s, e) => _control.Search();
                (ButtonsBar.Content as StackLayout).Children.Insert(0, btnSearch);
            }

            var dispCheckGetItems = await DependencyService.Get<Client.Interfaces.IDispatchCheckClient>().DispCheckGetItems(new S4.ProcedureModels.DispCheck.DispCheckGetItems() { DirectionID = directionID });
            if (!dispCheckGetItems.Success)
            {
                await DisplayAlert("Upozornění", dispCheckGetItems.ErrorText, "OK");
                Singleton<Helpers.Audio>.Instance.PlayError();

                return;
            }

            _control.SetItems(dispCheckGetItems.ItemInfoList, dispCheckGetItems.CheckDetails ?? new List<S4.ProcedureModels.DispCheck.DispCheckGetItems.CheckDetail>());
        }

        protected override ButtonsBar CreateButtonsBar()
        {
            return new ButtonsBar(ButtonsBar.ButtonsEnum.Cancel, ButtonsBar.ButtonsEnum.OK);
        }

        protected override void Init(bool scrolled)
        {
            base.Init(scrolled);

            _contentStack = new StackLayout() { VerticalOptions = LayoutOptions.FillAndExpand };

            Content = new StackLayout()
            {
                Children =
                {
                    _contentStack,
                    ButtonsBar
                }
            };
        }
    }
}

﻿using S4.Core;
using S4.Entities.Helpers;
using S4mp.Controls.Common;
using S4mp.InternalControls;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace S4mp.Dialogs
{
    public class SelectDocumentDialog : Controls.Dialogs.EditFormDialogBase
    {
        protected override ButtonsBar CreateButtonsBar()
        {
            var buttonsBar = new ButtonsBar(ButtonsBar.ButtonsEnum.Cancel);
            return buttonsBar;
        }

        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await OnButtonClickedAsync(ButtonsBar.ButtonsEnum.Cancel);
            });
            return true;
        }

        public DocumentInfo Document { get; private set; }

        private bool _showDocumentDetail = true;
        public bool ShowDocumentDetail { 
            get => _showDocumentDetail; 
            set {
                _showDocumentDetail = value;
                if(_control != null)
                    _control.ShowDocumentDetail = value;
            }
        }

        private SelectDocumentControl _control;

        protected async override void Init(bool scrolled)
        {
            base.Init(scrolled);

            _control = new SelectDocumentControl();
            _control.ShowDocumentDetail = _showDocumentDetail;
            _control.NoItemsFoundEvent += SelectStoreInDocumentDialog_NoItemsFoundEvent;
            _control.OnSelectItemCmd = new Command<DocumentInfo>((document) =>
            {
                Document = document;
                Device.BeginInvokeOnMainThread(async () =>
                {
                    await OnButtonClickedAsync(ButtonsBar.ButtonsEnum.OK);
                });
            });

            Content = new StackLayout()
            {
                Children =
                {
                    _control,
                    ButtonsBar
                }
            };
        }

        private async void SelectStoreInDocumentDialog_NoItemsFoundEvent(object sender, EventArgs e)
        {
            if (_control.SourceItems.Count == 0)
            {
                Singleton<Helpers.Audio>.Instance.PlayError();
                await App.Current.MainPage.DisplayAlert("Informace", "Žádné doklady", "Zavřít");

                // document scan assign to user is disabled then close document list
                if (!_control.EnableDocumentScan)
                {
                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        await OnButtonClickedAsync(ButtonsBar.ButtonsEnum.Cancel);
                    });
                }
            }
        }

        public void SetHeaderText(string headerText, string headerTextSub = null)
        {
            _control.HeaderText = headerText;
            _control.HeaderTextSub = headerTextSub;
        }

        public void SetRefreshData(Func<Task<List<DocumentInfo>>> data)
        {
            _control.DataWithRefreshTask = data;
            _control.RefreshData();
        }

        public void EnableDocumentScan(bool flag, Func<string, Task<List<DocumentInfo>>> func)
        {
            _control.EnableDocumentScan = flag;
            _control.EnableDocumentScanFunc = func;
        }

        public void LoadData(List<DocumentInfo> data)
        {
            _control.LoadData(data);
        }
    }
}
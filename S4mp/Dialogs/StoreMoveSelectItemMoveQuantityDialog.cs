﻿using S4mp.Controls.Common;
using S4mp.Core;
using S4mp.InternalControls.StoreMove;
using Xamarin.Forms;

namespace S4mp.Dialogs
{
    public class StoreMoveSelectItemMoveQuantityDialog : Controls.Dialogs.EditFormDialogBase
    {
        protected override ButtonsBar CreateButtonsBar()
        {
            var buttonsBar = new ButtonsBar(ButtonsBar.ButtonsEnum.Cancel, ButtonsBar.ButtonsEnum.OK);
            return buttonsBar;
        }

        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await OnButtonClickedAsync(ButtonsBar.ButtonsEnum.Cancel);
            });
            return true;
        }

        public decimal QuantityMoveSelected => (decimal) _control.QuantityMoveSelected;
        public SelectableListViewItem<StoreMoveArticleInfo> Item => _control.Item;

        private StoreMoveSelectItemMoveQuantityControl _control;

        protected async override void Init(bool scrolled)
        {
            base.Init(scrolled);

            _control = new StoreMoveSelectItemMoveQuantityControl();
            _control.OnClosingEvent += (object sender, bool e) => {
                SetChanged(e);
            };

            Content = new StackLayout()
            {
                Children =
                {
                    _control,
                    ButtonsBar
                }
            };
        }

        public void SetHeaderText(string headerText, string headerTextSub = null)
        {
            _control.HeaderText = headerText;
            _control.HeaderTextSub = headerTextSub;
        }

        public void SetData(SelectableListViewItem<StoreMoveArticleInfo> item)
        {
            _control.LoadData(item);
        }
    }
}

﻿using S4.ProcedureModels;
using S4mp.Controls.Common;
using S4mp.InternalControls.Dispatch;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace S4mp.Dialogs
{
    public class DispatchSelectBadPositionReasonDialog : Controls.Dialogs.EditFormDialogBase
    {
        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await OnButtonClickedAsync(ButtonsBar.ButtonsEnum.Cancel);
            });
            return true;
        }

        protected override ButtonsBar CreateButtonsBar()
        {
            var buttonsBar = new ButtonsBar(ButtonsBar.ButtonsEnum.Cancel, ButtonsBar.ButtonsEnum.OK);
            return buttonsBar;
        }

        public PositionErrorReason Reason { get; set; }
        private SelectDispatchBadPositionReasonControl _control;

        protected override void Init(bool scrolled)
        {
            base.Init(scrolled);

            _control = new SelectDispatchBadPositionReasonControl();
            _control.OnSuccessCommand = new Command<bool>((enableBtnOk) =>
            {
                Reason = _control.Reason;
                SetChanged(enableBtnOk);
            });

            Content = new StackLayout()
            {
                Children =
                {
                    _control,
                    ButtonsBar
                }
            };
        }

        public void SetHeaderText(string headerText, string headerTextSub = null)
        {
            _control.HeaderText = headerText;
            _control.HeaderTextSub = headerTextSub;
        }
    }
}

﻿using Xamarin.Forms;
using S4mp.Controls;
using S4mp.Controls.Common;
using S4mp.InternalControls.Positions;
using S4.ProcedureModels.Dispatch;
using S4.Core;

namespace S4mp.Dialogs
{
    public class DispatchConfirmSourcePositionDialog : Controls.Dialogs.EditFormDialogBase
    {
        private SearchPositionControl _searchPositionControl;

        protected override ButtonsBar CreateButtonsBar()
        {
            var buttonsBar = new ButtonsBar(ButtonsBar.ButtonsEnum.Cancel);
            return buttonsBar;
        }

        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await OnButtonClickedAsync(ButtonsBar.ButtonsEnum.Cancel);
            });

            return true;
        }

        public S4.Entities.Position Position { get; private set; }

        public void UnsuccessfullExtScan(string barCode)
        {
            _searchPositionControl.UnsuccessfullExtScan(barCode);
        }

        public DispatchItemInstruct ItemInstruct { get; set; }

        protected async override void Init(bool scrolled)
        {
            base.Init(scrolled);

            _searchPositionControl = new SearchPositionControl();
            _searchPositionControl.OnSuccessCommand = new Command<S4.Entities.Position>(async(pos) =>
            {
                Position = pos;

                if(ItemInstruct.ScrPosition.PositionID != pos.PositionID)
                {
                    Singleton<Helpers.Audio>.Instance.PlayError();
                    await App.Current.MainPage.DisplayAlert("Upozornění", "Pozice není výchozí pozicí položky", "OK");
                }
                else
                {
                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        await OnButtonClickedAsync(ButtonsBar.ButtonsEnum.OK);
                    });
                }
            });

            Content = new StackLayout()
            {
                Children =
                {
                    _searchPositionControl,
                    ButtonsBar
                }
            };
        }

        public void SetHeaderText(string headerText, string headerTextSub = null)
        {
            ((SearchPositionControlVM)_searchPositionControl.BindingContext).HeaderText = headerText;
            ((SearchPositionControlVM)_searchPositionControl.BindingContext).HeaderTextSub = headerTextSub;
        }

        public void SetData(DispatchItemInstruct itemInstruct)
        {
            ItemInstruct = itemInstruct;
        }
    }
}

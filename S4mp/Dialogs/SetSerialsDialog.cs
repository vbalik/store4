﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

using S4mp.Controls.Common;
using S4.Core;

namespace S4mp.Dialogs
{
    public class SetSerialsDialog : Controls.Dialogs.EditFormDialogBase
    {
        private InternalControls.DispCheck.SetSerialsControl _control;
        private InternalControls.DispCheck.DispCheckItem _dispCheckItem;

        public void SetItem(InternalControls.DispCheck.DispCheckItem dispCheckItem, int directionID)
        {
            _dispCheckItem = dispCheckItem;
            _control.SetItem(dispCheckItem, directionID);
            dispCheckItem.PropertyChanged += DispCheckItem_PropertyChanged;
        }

        protected override ButtonsBar CreateButtonsBar()
        {
            // @ DEPRECATED v22
            // var btnAdd = new Button() { ImageSource = "pluscirclesolid22.png", AutomationId = "AddButton" };
            // btnAdd.Clicked += (s, e) => _control.AddItem();
            var buttonsBar = new ButtonsBar(ButtonsBar.ButtonsEnum.Cancel, ButtonsBar.ButtonsEnum.OK);
            // @ DEPRECATED v22 (buttonsBar.Content as StackLayout).Children.Insert(0, btnAdd);
            return buttonsBar;
        }

        protected override void Init(bool scrolled)
        {
            base.Init(scrolled);

            _control = new InternalControls.DispCheck.SetSerialsControl();

            Content = new StackLayout()
            {
                Children =
                {
                    _control,
                    ButtonsBar
                }
            };
        }

        private void DispCheckItem_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            SetChanged(_dispCheckItem.SerialsOK);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using Xamarin.Forms;

using S4mp.Controls.Common;
using S4mp.InternalControls.StockTaking;

namespace S4mp.Dialogs
{
    public class NewStockTakingDialog : Controls.Dialogs.EditFormDialogBase
    {
        private NewStockTakingControl _newStockTakingControl;

        public S4.Entities.Position[] Positions => _newStockTakingControl?.ViewModel.Positions.ToArray();
        public string StotakDesc => _newStockTakingControl == null ? 
            null : 
            (
                string.IsNullOrWhiteSpace(_newStockTakingControl.ViewModel.StotakDesc) ? 
                $"Inventura pozic {string.Join(", ", _newStockTakingControl.ViewModel.Positions.Select(i => i.PosCode))}" :
                _newStockTakingControl.ViewModel.StotakDesc
            );

        protected override ButtonsBar CreateButtonsBar()
        {
            return new ButtonsBar(ButtonsBar.ButtonsEnum.Cancel, ButtonsBar.ButtonsEnum.OK);
        }

        protected override void Init(bool scrolled)
        {
            base.Init(scrolled);

            _newStockTakingControl = new NewStockTakingControl() { VerticalOptions = LayoutOptions.FillAndExpand };
            _newStockTakingControl.PositionListChanged += (s, e) => SetChanged(_newStockTakingControl.ViewModel.Positions.Count > 0);

            Content = new StackLayout()
            {
                Children =
                {
                    _newStockTakingControl,
                    ButtonsBar
                }
            };
        }
    }
}

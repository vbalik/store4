﻿using S4mp.Controls.Common;
using S4mp.InternalControls.Dispatch;
using Xamarin.Forms;

namespace S4mp.Dialogs
{
    public class DispatchSetWeightBoxCountDialog : Controls.Dialogs.EditFormDialogBase
    {
        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await OnButtonClickedAsync(ButtonsBar.ButtonsEnum.Cancel);
            });
            return true;
        }

        protected override ButtonsBar CreateButtonsBar()
        {
            var buttonsBar = new ButtonsBar(ButtonsBar.ButtonsEnum.Cancel, ButtonsBar.ButtonsEnum.OK);
            return buttonsBar;
        }

        public DispatchSetWeightBoxCountControl Control { get; set; }

        protected override void Init(bool scrolled)
        {
            base.Init(scrolled);

            Control = new DispatchSetWeightBoxCountControl();
            Control.ValuesChangedEvent += (s, e) => SetChanged(e);
            Control.OnSelectItemCmd = new Command<bool>((v) =>
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    await OnButtonClickedAsync(ButtonsBar.ButtonsEnum.OK);
                });
            });

            Content = new StackLayout()
            {
                Children =
                {
                    Control,
                    ButtonsBar
                }
            };

            SetChanged(true);
        }

        public void SetHeaderText(string headerText, string headerTextSub = null)
        {
            Control.HeaderText = headerText;
            Control.HeaderTextSub = headerTextSub;
        }
    }
}

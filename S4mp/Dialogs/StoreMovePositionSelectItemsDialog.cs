﻿using S4.Core;
using S4.Entities.Helpers;
using S4mp.Controls.Common;
using S4mp.InternalControls.StoreMove;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace S4mp.Dialogs
{
    /// <summary>
    /// Show items on POSITION
    /// </summary>
    public class StoreMovePositionSelectItemsDialog : Controls.Dialogs.EditFormDialogBase
    {
        protected override ButtonsBar CreateButtonsBar()
        {
            var buttonsBar = new ButtonsBar(ButtonsBar.ButtonsEnum.Cancel, ButtonsBar.ButtonsEnum.OK);
            return buttonsBar;
        }

        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await OnButtonClickedAsync(ButtonsBar.ButtonsEnum.Cancel);
            });
            return true;
        }

        public List<ArticleInfo> SelectedItems => _selectStoreMoveItemsControl.SelectedItems;

        private SelectStoreMovePositionItemsControl _selectStoreMoveItemsControl;

        protected async override void Init(bool scrolled)
        {
            base.Init(scrolled);

            _selectStoreMoveItemsControl = new SelectStoreMovePositionItemsControl();
            _selectStoreMoveItemsControl.NoItemsFoundEvent += SelectStoreMovePositionItemsDialog_NoItemsFoundEvent;
            _selectStoreMoveItemsControl.SelectItemEvent += (s, e) =>
            {
                SetChanged(_selectStoreMoveItemsControl.SelectedItems.Count > 0);
            };

            Content = new StackLayout()
            {
                Children =
                {
                    _selectStoreMoveItemsControl,
                    ButtonsBar
                }
            };
        }

        private async void SelectStoreMovePositionItemsDialog_NoItemsFoundEvent(object sender, EventArgs e)
        {
            if (_selectStoreMoveItemsControl.SourceItems.Count == 0)
            {
                Singleton<Helpers.Audio>.Instance.PlayError();
                await App.Current.MainPage.DisplayAlert("Informace", "Žádné položky", "Zavřít");
                Device.BeginInvokeOnMainThread(async () =>
                {
                    await OnButtonClickedAsync(ButtonsBar.ButtonsEnum.None);
                });
            }
        }

        public Task SetData(List<ArticleInfo> items)
        {
            _selectStoreMoveItemsControl.LoadData(items);
            return Task.CompletedTask;
        }

        public void SetHeaderText(string headerText, string headerTextSub = null)
        {
            _selectStoreMoveItemsControl.HeaderText = headerText;
            _selectStoreMoveItemsControl.HeaderTextSub = headerTextSub;
        }
    }
}

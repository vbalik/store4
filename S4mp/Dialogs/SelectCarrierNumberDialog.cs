﻿using S4mp.Controls;
using S4mp.Controls.Common;
using S4mp.InternalControls.Carrier;
using Xamarin.Forms;

namespace S4mp.Dialogs
{
    public class SelectCarrierNumberDialog : Controls.Dialogs.EditFormDialogBase
    {
        private SearchCarrierNumberControl _selectCarrierNumberControl;

        public SelectCarrierNumberDialog() { }

        public string CarrierNum { get => _selectCarrierNumberControl.CarrierNum; }

        public void ExternalScan(IScannerResolver scannerResolver)
        {
            _selectCarrierNumberControl.ExternalScan(scannerResolver);
        }

        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await OnButtonClickedAsync(ButtonsBar.ButtonsEnum.Cancel);
            });
            return true;
        }

        protected override ButtonsBar CreateButtonsBar()
        {
            return new ButtonsBar(ButtonsBar.ButtonsEnum.Cancel, ButtonsBar.ButtonsEnum.OK);
        }

        protected override void Init(bool scrolled)
        {
            base.Init(scrolled);

            _selectCarrierNumberControl = new SearchCarrierNumberControl();
            _selectCarrierNumberControl.ConfirmButtonEvent += (sender, e) =>
            {
                if (_selectCarrierNumberControl.Valid)
                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        await OnButtonClickedAsync(ButtonsBar.ButtonsEnum.OK);
                    });
            };
            _selectCarrierNumberControl.TextChanged += (s, e) =>
            {
                SetChanged(_selectCarrierNumberControl.Valid);
            };

            Content = new StackLayout()
            {
                Children =
                {
                    _selectCarrierNumberControl,
                    ButtonsBar
                }
            };
        }

        public void SetHeaderText(string headerText, string headerTextSub)
        {
            _selectCarrierNumberControl.HeaderText = headerText;
            _selectCarrierNumberControl.HeaderTextSub = headerTextSub;
        }

        public void SetHideWithoutCarrier()
        {
            _selectCarrierNumberControl.ShowWithoutCarrierBtn = false;
        }
    }
}

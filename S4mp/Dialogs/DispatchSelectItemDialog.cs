﻿using S4.Core;
using S4.Entities.Helpers;
using S4mp.Controls.Common;
using S4mp.InternalControls.Dispatch;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace S4mp.Dialogs
{
    public class DispatchSelectItemDialog : Controls.Dialogs.EditFormDialogBase
    {
        protected override ButtonsBar CreateButtonsBar()
        {
            var buttonsBar = new ButtonsBar(ButtonsBar.ButtonsEnum.Cancel);
            return buttonsBar;
        }

        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await OnButtonClickedAsync(ButtonsBar.ButtonsEnum.Cancel);
            });
            return true;
        }

        public ItemInfo Item { get; private set; }
        public int LastPositionIndex { get; private set; }

        private SelectDispatchItemControl _selectDispatchItemControl;

        protected async override void Init(bool scrolled)
        {
            base.Init(scrolled);

            _selectDispatchItemControl = new SelectDispatchItemControl();
            _selectDispatchItemControl.NoItemsFoundEvent += DispatchSelectItemDialog_NoItemsFoundEvent;
            _selectDispatchItemControl.OnSuccessCommand = new Command<SelectDispatchItemResultModel>((selectedItem) =>
            {
                Item = selectedItem.ItemInfo;
                LastPositionIndex = selectedItem.LastPositionIndex;
                Device.BeginInvokeOnMainThread(async () =>
                {
                    await OnButtonClickedAsync(ButtonsBar.ButtonsEnum.OK);
                });
            });

            Content = new StackLayout()
            {
                Children =
                {
                    _selectDispatchItemControl,
                    ButtonsBar
                }
            };
        }

        private async void DispatchSelectItemDialog_NoItemsFoundEvent(object sender, EventArgs e)
        {
            if (_selectDispatchItemControl.SourceItems.Count == 0)
            {
                Singleton<Helpers.Audio>.Instance.PlayError();
                await App.Current.MainPage.DisplayAlert("Informace", "Žádné další položky", "Zavřít");
                Device.BeginInvokeOnMainThread(async () =>
                {
                    await OnButtonClickedAsync(ButtonsBar.ButtonsEnum.Cancel);
                });
            }
        }

        public async Task SetData(List<ItemInfo> items, int lastItemIndex)
        {
            await _selectDispatchItemControl.LoadData(items, lastItemIndex);
        }

        public void SetHeaderText(string headerText, string headerTextSub = null)
        {
            _selectDispatchItemControl.HeaderText = headerText;
            _selectDispatchItemControl.HeaderTextSub = headerTextSub;
        }
    }
}

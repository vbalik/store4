﻿using S4mp.Controls.Common;
using S4mp.InternalControls.Direction;
using Xamarin.Forms;

namespace S4mp.Dialogs
{
    public class ExpeditionTruckDialog : Controls.Dialogs.EditFormDialogBase
    {
        protected override ButtonsBar CreateButtonsBar()
        {
            var buttonsBar = new ButtonsBar(ButtonsBar.ButtonsEnum.Cancel, ButtonsBar.ButtonsEnum.OK);
            return buttonsBar;
        }

        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await OnButtonClickedAsync(ButtonsBar.ButtonsEnum.Cancel);
            });
            return true;
        }

        public string Spz { get; private set; }

        private ExpeditionTruckControl _expeditionTruckControl;

        protected override void Init(bool scrolled)
        {
            base.Init(scrolled);

            _expeditionTruckControl = new ExpeditionTruckControl();
            _expeditionTruckControl.OnSuccessCommand = new Command<string>((spz) =>
            {
                Spz = spz;
                Device.BeginInvokeOnMainThread(async () =>
                {
                    await OnButtonClickedAsync(ButtonsBar.ButtonsEnum.OK);
                });
            });
            _expeditionTruckControl.EnableBtnOkEvent += (sender, e) =>
            {
                SetChanged(e);
            };

            Content = new StackLayout()
            {
                Children =
                {
                    _expeditionTruckControl,
                    ButtonsBar
                }
            };
        }

        public void SetHeaderText(string headerText, string headerTextSub = null)
        {
            _expeditionTruckControl.HeaderText = headerText;
            _expeditionTruckControl.HeaderTextSub = headerTextSub;
        }
    }
}

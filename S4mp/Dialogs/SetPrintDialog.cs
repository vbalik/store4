﻿using S4mp.Controls;
using S4mp.Controls.Common;
using Xamarin.Forms;

namespace S4mp.Dialogs
{
    public class SetPrintDialog : Controls.Dialogs.EditFormDialogBase
    {
        private InternalControls.SetArticle.SetPrintView _setPrintView;

        public int PrinterID => _setPrintView.PrinterID.Value;
        public short Count => _setPrintView.Count;

        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await OnButtonClickedAsync(ButtonsBar.ButtonsEnum.Cancel);
            });
            return true;
        }

        protected override ButtonsBar CreateButtonsBar()
        {
            var buttonsBar = new ButtonsBar(ButtonsBar.ButtonsEnum.Cancel, ButtonsBar.ButtonsEnum.OK);
            return buttonsBar;
        }

        protected override void Init(bool scrolled)
        {
            base.Init(scrolled);

            _setPrintView = new InternalControls.SetArticle.SetPrintView();
            _setPrintView.DataChanged += (s, e) => SetChanged(e);

            Content = new StackLayout()
            {
                Children =
                {
                    _setPrintView,
                    ButtonsBar
                }
            };
        }
    }
}

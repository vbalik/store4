﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


using S4mp.Controls;
using S4mp.Controls.Common;
using Xamarin.Forms;

namespace S4mp.Dialogs
{
    public class SelectArticleDialog : Controls.Dialogs.EditFormDialogBase
    {
        private InternalControls.GetArticle.GetArticleView _getArticleView;
        private Button _btnShowSearch;

        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await OnButtonClickedAsync(ButtonsBar.ButtonsEnum.Cancel);
            });
            return true;
        }

        public InternalControls.GetArticle.GetArticleResultModel GetArticleResult { get; private set; }

        public void UnsuccessfullExtScan(string barCode)
        {
            _getArticleView.UnsuccessfullExtScan(barCode);
        }

        public void SetHeaderText(string headerText, string headerTextSub = null)
        {
            _getArticleView.SetHeaderText(headerText, headerTextSub);
        }

        protected override ButtonsBar CreateButtonsBar()
        {
            var buttonsBar = new ButtonsBar(ButtonsBar.ButtonsEnum.Cancel);
            _btnShowSearch = new Button() { Image = "search.png", IsVisible = false };
            _btnShowSearch.Clicked += (s, e) => _getArticleView.ShowSearch(true);
            (buttonsBar.Content as StackLayout).Children.Add(_btnShowSearch);
            return buttonsBar;
        }

        protected override void Init(bool scrolled)
        {
            base.Init(scrolled);

            _getArticleView = new InternalControls.GetArticle.GetArticleView();
            _getArticleView.ShowSearchEvent += (s, e) => _btnShowSearch.IsVisible = e;
            _getArticleView.ArticleResult += (s, e) =>
            {
                GetArticleResult = e;
                Device.BeginInvokeOnMainThread(async () =>
                {
                    await OnButtonClickedAsync(ButtonsBar.ButtonsEnum.OK);
                });
            };

            Content = new StackLayout()
            {
                Children =
                {
                    _getArticleView,
                    ButtonsBar
                }
            };
        }

        public void ResetForm()
        {
            _getArticleView.ResetForm();
        }
    }
}

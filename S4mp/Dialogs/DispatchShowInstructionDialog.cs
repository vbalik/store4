﻿using S4.ProcedureModels.Dispatch;
using S4mp.Controls.Common;
using S4mp.InternalControls.Dispatch;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace S4mp.Dialogs
{
    public class DispatchShowInstructionDialog : Controls.Dialogs.EditFormDialogBase
    {
        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await OnButtonClickedAsync(ButtonsBar.ButtonsEnum.Cancel);
            });
            return true;
        }

        protected override ButtonsBar CreateButtonsBar()
        {
            var buttonsBar = new ButtonsBar(ButtonsBar.ButtonsEnum.Cancel);
            return buttonsBar;
        }

        public DispatchItemInstruct ItemInstruct { get; set; }

        private ShowDispatchInstructionControl _showDispatchInstructionControl;

        protected override void Init(bool scrolled)
        {
            base.Init(scrolled);

            _showDispatchInstructionControl = new ShowDispatchInstructionControl();
            _showDispatchInstructionControl.BadPositionCommand = new Command<DispatchItemInstruct>((item) =>
            {
                ItemInstruct = item;
                Device.BeginInvokeOnMainThread(async () =>
                {
                    await OnButtonClickedAsync(ButtonsBar.ButtonsEnum.No);
                });
            });
            _showDispatchInstructionControl.LoadUpCommand = new Command<DispatchItemInstruct>((item) =>
            {
                ItemInstruct = item;
                Device.BeginInvokeOnMainThread(async () =>
                {
                    await OnButtonClickedAsync(ButtonsBar.ButtonsEnum.OK);
                });
            });

            Content = new StackLayout()
            {
                Children =
                {
                    _showDispatchInstructionControl,
                    ButtonsBar
                }
            };
        }

        public async Task SetData(int stoMoveID, int stoMoveItemID)
        {
            await _showDispatchInstructionControl.LoadData(stoMoveID, stoMoveItemID);
        }

        public void SetHeaderText(string headerText, string headerTextSub = null)
        {
            _showDispatchInstructionControl.HeaderText = headerText;
            _showDispatchInstructionControl.HeaderTextSub = headerTextSub;
        }
    }
}

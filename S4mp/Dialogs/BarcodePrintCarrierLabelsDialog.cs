﻿using S4mp.Controls.Common;
using S4mp.InternalControls.Printer;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace S4mp.Dialogs
{
    public class BarcodePrintCarrierLabelsDialog : Controls.Dialogs.EditFormDialogBase
    {
        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await OnButtonClickedAsync(ButtonsBar.ButtonsEnum.Cancel);
            });

            return true;
        }

        protected override ButtonsBar CreateButtonsBar()
        {
            var buttonsBar = new ButtonsBar(ButtonsBar.ButtonsEnum.Cancel, ButtonsBar.ButtonsEnum.OK);
            return buttonsBar;
        }

        public PrintBarcodeCarrierLabelsControl Control { get; set; }

        protected override void Init(bool scrolled)
        {

            base.Init(scrolled);

            Control = new PrintBarcodeCarrierLabelsControl();
            Control.PrinterChangedEvent += (s, e) => SetChanged(e);

            Content = new StackLayout()
            {
                Children =
                {
                    Control,
                    ButtonsBar
                }
            };
        }

        public void SetHeaderText(string headerText, string headerTextSub = null)
        {
            Control.HeaderText = headerText;
            Control.HeaderTextSub = headerTextSub;
        }

        public async void LoadData()
        {
            await Control.LoadData();
        }
    }
}

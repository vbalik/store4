﻿using S4mp.Controls.Common;
using S4mp.InternalControls.Articles;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace S4mp.Dialogs
{
    public class GetArticleDetailDialog : Controls.Dialogs.EditFormDialogBase
    {
        private ArticleDetailControl _articleDetailControl;

        public GetArticleDetailDialog() { }

        protected override ButtonsBar CreateButtonsBar()
        {
            return new ButtonsBar(ButtonsBar.ButtonsEnum.Cancel);
        }

        protected override void Init(bool scrolled)
        {
            base.Init(scrolled);

            _articleDetailControl = new ArticleDetailControl();

            Content = new StackLayout()
            {
                Children =
                {
                    _articleDetailControl,
                    ButtonsBar
                }
            };
        }

        public async Task SetArticleID(int articleID)
        {
            await _articleDetailControl.LoadArticleData(articleID);
        }

        public void SetHeaderText(string headerText, string headerTextSub)
        {
            _articleDetailControl.HeaderText = headerText;
            _articleDetailControl.HeaderTextSub = headerTextSub;
        }
    }
}
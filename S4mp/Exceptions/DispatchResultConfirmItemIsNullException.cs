﻿using System;

namespace S4mp.Exceptions
{
	public class DispatchResultConfirmItemIsNullException : Exception
	{
        public DispatchResultConfirmItemIsNullException(int stoMoveID, int stoMoveItemID, int directionID) 
            : base($"Data stoMoveID:{stoMoveID}, stoMoveItemID: {stoMoveItemID}, directionID: {directionID}") { }
    }
}


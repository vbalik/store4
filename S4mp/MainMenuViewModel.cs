﻿using S4.Entities.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace S4mp
{
    public class MainMenuViewModel : TerminalMenuItem, INotifyPropertyChanged
    {

        public MainMenuViewModel(TerminalMenuItem i)
        {
            Group = i.Group;
            Text = i.Text;
            Image = i.Image;
            PageName = i.PageName;
            PageParams = i.PageParams;
            JobID = i.JobID;
        }

        public int TodoCounter { get; set; }
        public bool ShowTodoCounter
        {
            get => TodoCounter != 0;
        }


        public event PropertyChangedEventHandler PropertyChanged;
        public virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

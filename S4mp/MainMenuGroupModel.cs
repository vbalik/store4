﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4mp
{
    public class MainMenuGroupModel : List<MainMenuViewModel>
    {
        public MainMenuGroupModel(string groupName, IEnumerable<MainMenuViewModel> items) : base(items)
        {
            GroupName = groupName;
        }


        public string GroupName { get; set; }
    }
}

﻿using S4.Core;
using S4.Entities.Helpers;
using S4mp.Core;
using S4mp.Pages;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace S4mp
{
    public partial class MainPage : ContentPage
    {
        public static ObservableCollection<MainMenuGroupModel> MenuItems = new ObservableCollection<MainMenuGroupModel>();

        public static void CreateMenuStructure(List<TerminalMenuItem> menuData)
        {
            // create menu structure
            var menuStruct = menuData.GroupBy(k => k.Group, (k, items) => new MainMenuGroupModel(k, items.Select(i => new MainMenuViewModel(i))));
            MenuItems = new ObservableCollection<MainMenuGroupModel>(menuStruct);
        }


        public static void UpdateAssigmnments(TerminalJobAssignUpdate update)
        {
            var doSound = false;
            foreach (var group in MenuItems)
            {
                foreach (var menuItem in group)
                {
                    // hide counter
                    if (update == null)
                    {
                        menuItem.TodoCounter = 0;
                        menuItem.OnPropertyChanged(nameof(MainMenuViewModel.TodoCounter));
                        menuItem.OnPropertyChanged(nameof(MainMenuViewModel.ShowTodoCounter));
                        continue;
                    }

                    var updateItem = update.Items.SingleOrDefault(i => i.JobID == menuItem.JobID);
                    var newValue = (updateItem == null) ? 0 : updateItem.CntDocs;
                    if (newValue > menuItem.TodoCounter)
                        doSound = true;
                    menuItem.TodoCounter = newValue;
                    menuItem.OnPropertyChanged(nameof(MainMenuViewModel.TodoCounter));
                    menuItem.OnPropertyChanged(nameof(MainMenuViewModel.ShowTodoCounter));
                }
            }

            if (doSound)
                Singleton<Helpers.Audio>.Instance.PlayBell();
        }


        public MainPage()
        {
            InitializeComponent();
            BindingContext = this;
            var ver = Application.Current.Properties[S4mp.Core.Consts.PROP_APP_VERSION];
            new Controls.OptionsMenu(this).FormatTitle("S4", $"Verze: {ver}");

            MainMenu.ItemsSource = MenuItems;

            NavigateCommand = new Command<TerminalMenuItem>(
                async (TerminalMenuItem TerminalMenuItem) =>
                {
                    var pageType = typeof(S4mp.MainPage).Assembly.GetType($"S4mp.{TerminalMenuItem.PageName}");
                    Page page = (Page)Activator.CreateInstance(pageType);

                    if (page is IPageWithParams)
                        ((IPageWithParams)page).SetParams(TerminalMenuItem.PageParams);

                    if (page is IPageLoadData)
                        ((IPageLoadData)page).PageLoadData();

                    await Navigation.PushAsync(page);
                });
        }

        public ICommand NavigateCommand { private set; get; }

        public static void SetDefaultSettings(TerminalDefaultSettings settings)
        {
            // set default settings values from server
            Singleton<S4mp.Settings.LocalSettings>.Instance[Consts.XMLPATH_DISPATCH_SCANNER_ASSIGN_USER] = string.Format("{0}", settings.EnableAssignDirectionUser);
            Singleton<S4mp.Settings.LocalSettings>.Instance[Consts.XMLPATH_BARCODE_DISALLOWED_VALUE_PATTERN] = string.Format("{0}", settings.BarcodeDisallowedValuePattern);
            Singleton<S4mp.Settings.LocalSettings>.Instance[Consts.XMLPATH_DISPATCH_DEFAULT_POSITION_ID] = string.Format("{0}", settings.DispatchDefaultPositionID);
            Singleton<S4mp.Settings.LocalSettings>.Instance[Consts.XMLPATH_SHOW_SPEC_FEATURES_ALERT_INFO_LIST] = string.Format("{0}", settings.ShowSpecFeaturesAlertInfoList);
        }
    }
}

﻿using System;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;
using Newtonsoft.Json;
using S4.Core;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Linq;

using S4mp.DAL;
using S4mp.Core.Logger;
using S4mp.Core;
using S4mp.Pages;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace S4mp
{
    public partial class App : Application
    {
        private static ICustomEventLogger _logger;

        public static App Instance;
        public static bool MockData;

        private bool settingHoneywellScannerWeb = false;

        public App(string hwDescription, string appVersion, string osDescription, string monoDescription)
        {
            InitializeComponent();

            SetProperty(S4mp.Core.Consts.PROP_HW_DESCRIPTION, hwDescription);
            SetProperty(S4mp.Core.Consts.PROP_APP_VERSION, appVersion);
            SetProperty(S4mp.Core.Consts.PROP_OS_DESCRIPTION, osDescription);
            SetProperty(S4mp.Core.Consts.PROP_MONO_DESCRIPTION, monoDescription);
            SetProperty(S4mp.Core.Consts.PROP_STARTUP_TIME, DateTime.Now);

#if DEBUG
            MockData = debug_settings.MockData;
#endif
            if (MockData)
            {
                DependencyService.Register<MockData.LocalDatabase>();
                DependencyService.Register<MockData.Ping>();
                DependencyService.Register<MockData.WorkerClient>();
                DependencyService.Register<MockData.TerminalClient>();
                DependencyService.Register<MockData.TerminalHub>();
                DependencyService.Register<MockData.HoneywellScannerHub>();
                DependencyService.Register<MockData.ArticleClient>();
                DependencyService.Register<MockData.DispatchCheckClient>();
                DependencyService.Register<MockData.ReportClient>();
                DependencyService.Register<MockData.BarcodeClient>();
                DependencyService.Register<MockData.DirectionClient>();
                DependencyService.Register<MockData.PositionClient>();
                DependencyService.Register<MockData.ArtOnStoreClient>();
                DependencyService.Register<MockData.StockTakingClient>();
                DependencyService.Register<MockData.ReceiveClient>();
            }
            else
            {
                DependencyService.Register<LocalDatabase>();
                DependencyService.Register<Client.Utils.Ping>();
                DependencyService.Register<Client.WorkerClient>();
                DependencyService.Register<Client.TerminalClient>();
                DependencyService.Register<Helpers.TerminalHub>();
                DependencyService.Register<Helpers.HoneywellScannerHub>();
                DependencyService.Register<Client.ArticleClient>();
                DependencyService.Register<Client.DispatchCheckClient>();
                DependencyService.Register<Client.DispatchClient>();
                DependencyService.Register<Client.ReportClient>();
                DependencyService.Register<Client.BarcodeClient>();
                DependencyService.Register<Client.DirectionClient>();
                DependencyService.Register<Client.PositionClient>();
                DependencyService.Register<Client.ArtOnStoreClient>();
                DependencyService.Register<Client.StockTakingClient>();
                DependencyService.Register<Client.ReceiveClient>();
                DependencyService.Register<Client.StoreInClient>();
            }

            // register logger
            DependencyService.Register<Client.SystemClient>();
            DependencyService.Register<S4mp.Core.Logger.CustomEventLogger>();
            _logger = DependencyService.Get<ICustomEventLogger>();

            // client navigation service to login
            DependencyService.Register<LoginPage>();
            DependencyService.Register<Client.ClientNavigation.ClientNavigationService>();

            Instance = this;
        }

        public event EventHandler SaveSettingDelegate;

        public static async void SendClientError(Exception exc)
        {
            var errorMessage = exc.ToString();
            var errorClass = exc.GetType().Name;
            var isClosing = false;
            await SendClientError(errorClass, errorMessage, isClosing);
        }

        public static async Task SendClientError(string errorClass, string errorMessage, bool isClosing)
        {
            try
            {
                var navigation = $"\n{string.Join("\n", Application.Current.MainPage.Navigation.NavigationStack.Select(i => $"\t{i.GetType().Name}"))}";
                var modals = $"\n{string.Join("\n", Application.Current.MainPage.Navigation.ModalStack.Select(i => $"\t{i.GetType().Name}"))}";
                var clientError = new S4.Entities.ClientError()
                {
                    ErrorClass = errorClass,
                    ErrorDateTime = DateTime.Now,
                    ErrorMessage = errorMessage,
                    AppVersion = Application.Current.Properties[S4mp.Core.Consts.PROP_APP_VERSION].ToString(),
                    Path = Application.Current.Properties[S4mp.Core.Consts.PROP_HW_DESCRIPTION].ToString(),
                    StatusData = JsonConvert.SerializeObject(new
                    {
                        UserID = Singleton<BaseInfo>.Instance.CurrentWorkerID,
                        OS_ver = Application.Current.Properties[S4mp.Core.Consts.PROP_OS_DESCRIPTION],
                        MONO_ver = Application.Current.Properties[S4mp.Core.Consts.PROP_MONO_DESCRIPTION],
                        RunTime = (DateTime.Now - (DateTime)Application.Current.Properties[S4mp.Core.Consts.PROP_STARTUP_TIME]),
                        IsClosing = isClosing,
                        MainPage = Application.Current.MainPage.GetType().Name,
                        Pages = navigation,
                        Modals = modals
                    })
                };
               
                var clientErrorClient = new S4mp.Client.ClientErrorClient();
                await clientErrorClient.Insert(clientError);
            }
            catch(Exception ex)
            {
                await _logger.LogError($"{nameof(App)}.{nameof(SendClientError)}: {ex.Message}", ex);
            }
        }


        public async void DoStart()
        {
            var textSize = Singleton<Settings.LocalSettings>.Instance[S4mp.Core.Consts.XMLPATH_TEXTSIZE];
            NamedSize size = NamedSize.Default;
            Enum.TryParse<NamedSize>(textSize, out size);
            Core.Consts.TEXT_SIZE = Device.GetNamedSize(size, typeof(Label));
            switch (size)
            {
                case NamedSize.Micro:
                    Core.Consts.HEADER_TEXT_SIZE = Device.GetNamedSize(NamedSize.Small, typeof(Label));
                    Core.Consts.RULLER_TEXT_SIZE = Device.GetNamedSize(NamedSize.Micro, typeof(Label));
                    break;
                case NamedSize.Small:
                case NamedSize.Default:
                    Core.Consts.HEADER_TEXT_SIZE = Device.GetNamedSize(NamedSize.Medium, typeof(Label));
                    Core.Consts.RULLER_TEXT_SIZE = Device.GetNamedSize(NamedSize.Micro, typeof(Label));
                    break;
                case NamedSize.Medium:
                    Core.Consts.HEADER_TEXT_SIZE = Device.GetNamedSize(NamedSize.Large, typeof(Label));
                    Core.Consts.RULLER_TEXT_SIZE = Device.GetNamedSize(NamedSize.Small, typeof(Label));
                    break;
                case NamedSize.Large:
                    Core.Consts.HEADER_TEXT_SIZE = Device.GetNamedSize(NamedSize.Large, typeof(Label));
                    Core.Consts.RULLER_TEXT_SIZE = Device.GetNamedSize(NamedSize.Medium, typeof(Label));
                    break;
            }

            //connecting server
            var loadingPage = new Pages.LoadingPage();
            MainPage = new NavigationPage(loadingPage);
            await loadingPage.ProgressStart();

            try
            {
                if (!await DependencyService.Get<Client.Interfaces.IPing>().PingWSAsync(S4mp.Client.ClientBase.ApiUrl))
                {
                    if (await S4mp.Controls.Dialogs.Confirmation.Confirm("Server je nedostupný, chcete načíst nastavení skenováním?", App.Current.MainPage))
                    {
                        var scanSettingsPage = new Pages.Settings.ScanSettingsPage();
                        await MainPage.Navigation.PushModalAsync(scanSettingsPage);
                        scanSettingsPage.SettingLoaded += (s, e) =>
                        {
                            DoStart();
                        };
                        return;
                    }

                    if (await S4mp.Controls.Dialogs.Confirmation.Confirm("Server je nedostupný, chcete otevřít kartu nastavení?", App.Current.MainPage))
                    {
                        var dialog = new Pages.Settings.LocalSettingsPage();
                        dialog.Closed += (sender, e) =>
                        {
                            if (e == Controls.Common.ButtonsBar.ButtonsEnum.OK)
                            {
                                S4mp.App.Instance.SaveSetting();
                                DoStart();
                            }
                        };
                        await Current.MainPage.Navigation.PushModalAsync(dialog);
                        return;
                    }
                    else
                    {
                        throw new S4mp.Exceptions.ServerUnavailableException();
                    }
                }

                // clear custom event logs
                DependencyService.Get<ICustomEventLogger>().ClearLogs();
             
                Login();
            }
            catch (Exception ex)
            {
                await _logger.LogError($"{nameof(App)}.{nameof(DoStart)}: {ex.Message}", ex);
                await loadingPage.ProgressStop();
                throw ex;
            }
        }

        public void SaveSetting()
        {
            if (SaveSettingDelegate != null)
                SaveSettingDelegate.Invoke(this, new EventArgs());
        }

        public void Login()
        {
            // change app status
            Singleton<BaseInfo>.Instance.AppStatus = BaseInfo.AppStatusEnum.Started;
            //login
            var logingPage = new Pages.LoginPage();
            MainPage = logingPage;
        }

        protected override void OnStart()
        {
            DoStart();

            if (bool.TryParse(Singleton<Settings.LocalSettings>.Instance[Core.Consts.XMLPATH_CONNECT_SCANNER_WEB] ?? "False", out var connectScannerWeb))
                settingHoneywellScannerWeb = connectScannerWeb;


            DependencyService.Get<Interfaces.ITerminalHub>().InitHubConnection();
            
            if(settingHoneywellScannerWeb)
                DependencyService.Get<Interfaces.IHoneywellScannerHub>().InitHubConnection();
        }

        protected override void OnSleep()
        {
            DependencyService.Get<Interfaces.ITerminalHub>().DisconnectAsync();

            if (settingHoneywellScannerWeb)
                DependencyService.Get<Interfaces.IHoneywellScannerHub>().DisconnectAsync();
        }

        protected override void OnResume()
        {
            DependencyService.Get<Interfaces.ITerminalHub>().ConnectAsync();

            if (settingHoneywellScannerWeb)
                DependencyService.Get<Interfaces.IHoneywellScannerHub>().ConnectAsync();
        }

        private void SetProperty(string propertyName, object value)
        {
            if (Application.Current.Properties.ContainsKey(propertyName))
                Application.Current.Properties.Remove(propertyName);
            Application.Current.Properties.Add(propertyName, value);
        }
    }
}

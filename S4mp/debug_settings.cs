﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4mp
{
    public static class debug_settings
    {
        public const bool MockData = false;

        //app settings
        //public static string ApiUrl = "http://10.0.0.13:5000";
        //public static string ApiUrl = "http://172.16.0.7:5000";
        //public static string ApiUrl = "http://localhost:3333";
        //public static string ApiUrl = "http://192.168.0.100:5000";
        //public static string ApiUrl = "http://192.168.1.5:5000";
        //public static string ApiUrl = "http://localhost:5001";
        //public static string ApiUrl = "http://192.168.40.130:5000";
        //public static string ApiUrl = "http://localhost:5000";
        //public static string ApiUrl = "http://192.168.0.101:5000";
        public static string ApiUrl = "http://10.0.2.2:5000"; // Special emulator IP redirecting to machine's localhost
        public static string User = "a";
        public static string Password = "1111";
        public static string TextSize = "Medium";

        //scanner test data
        public static string CodeID = "Code128";
        //public static string CodeText = "84627470501466";
        //public static string CodeText = "01846274705014661720090110DP8JQHC01C";
        public static string CodeText = "01085947392651652111E6YWRC66EX6917251031102R04472A"; //"1A01A";
    }
}

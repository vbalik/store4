﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using S4.Core;
using S4mp.Client;
using S4mp.Core;

namespace S4mp.StartUp
{
    static class LogOn
    {

        internal static async Task<bool> TryToLog(string userName, string password)
        {
            var result = await DependencyService.Get<Client.Interfaces.IWorkerClient>().TryLogOn(new S4.ProcedureModels.Login.LoginModel() { UserName = userName, Password = password });
            if (result.Success && !string.IsNullOrEmpty(result.Worker.WorkerID))
            {
				Singleton<BaseInfo>.Instance.CurrentWorkerID = result.Worker.WorkerID;
                return true;
            }
            else
            {
                return false;
            }
        }



        internal static async Task AfterLogOn()
        {
            // change app status
			Singleton<BaseInfo>.Instance.AppStatus = BaseInfo.AppStatusEnum.LoggedOn;

            await DependencyService.Get<Interfaces.ITerminalHub>().LogOn();
        }
    }
}

﻿using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using S4.Entities.Views;
using S4mp.Dialogs;
using S4mp.Controls;
using S4mp.Controls.Scanner;
using S4.Core;
using System.Linq;

namespace S4mp.Pages.OnStock
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ArtOnStockPage : ContentPage
	{
        //dependent resolver for scanning
        private IScannerResolver _scannerResolver;

        public ArtOnStockPage()
		{
			InitializeComponent ();

#if !DATALOGIC && !HONEYWELL
            var btnScan = new Button
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                Text = "SCAN",
                Command = new Command(() =>
                {
                    _scannerResolver.Scan();
                })
            };
            (Content as StackLayout).Children.Insert(0, btnScan);
#endif

            new Controls.OptionsMenu(this).FormatTitle("Skladem");


            ToolbarItems.Add(new ToolbarItem("Řazení", "", () => FilterActionMenu(), ToolbarItemOrder.Secondary));

            //create scanning resolver
            _scannerResolver = DependencyService.Get<IScannerResolver>();
            //open dialog for article searching
            ShowSelectArticleDialog(null);
        }

        private ArtOnStockViewModel ViewModel => (ArtOnStockViewModel)BindingContext;

        protected override void OnAppearing()
        {
            base.OnAppearing();

            ViewModel.ArtOnStockActionMenuCmd = new Command<ArtOnStoreView>(ArtOnStockActionMenu);
        }

        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);

            if (propertyName.Equals("Renderer", StringComparison.OrdinalIgnoreCase))
            {
                //stop listening on closing
                var rr = DependencyService.Get<IRendererResolver>();
                if (!rr.HasRenderer(this))
                    _scannerResolver.Init(false, ScannerResolver_Scanned);
            }
        }

        private async Task SetResult(S4.Entities.Models.ArticlePackingInfo articleInfo)
        {
            activityIndicator.Show();
            try
            {
                ViewModel.Article = articleInfo;
                var client = new Client.ArtOnStoreClient();
                ViewModel.ArtOnStoreViewList = (await client.GetByArticle(ViewModel.Article.ArticleID))?.OrderBy(_ => _.PosCode).ToList();

                if (ViewModel.ArtOnStoreViewList.Count == 0)
                {
                    Singleton<Helpers.Audio>.Instance.PlayError();
                    await DisplayAlert("Upozornění", $"Položka \"{ViewModel.Article.ArticleInfo}\" není skladem", "OK");
                    await Navigation.PopAsync();
                    return;
                }
            }
            finally
            {
                activityIndicator.Hide();
            }
        }

        private void ClearForm()
        {
            ViewModel.Article = null;
            ViewModel.ArtOnStoreViewList = new System.Collections.Generic.List<ArtOnStoreView>();
        }

        private async void GetArticleDialog_Closed(object sender, Controls.Common.ButtonsBar.ButtonsEnum e)
        {
            if (e == Controls.Common.ButtonsBar.ButtonsEnum.OK)
            {
                await SetResult((sender as SelectArticleDialog).GetArticleResult.ArticlePackingInfo);
                //start scanner listening
                _scannerResolver.Init(true, ScannerResolver_Scanned);
            }
            else
            {
                await Navigation.PopAsync();
            }
        }

        protected async void ArtOnStockActionMenu(ArtOnStoreView item)
        {
            var action = await DisplayActionSheet(null, "Zrušit", null, "Detail");

            if (action == "Detail")
            {
                var articleDetailDlg = new GetArticleDetailDialog();
                await articleDetailDlg.SetArticleID(item.ArticleID);

                await Navigation.PushModalAsync(articleDetailDlg, false);
            }
        }

        private async void ScannerResolver_Scanned(object sender, BarCodeTypeEnum e)
        {
            activityIndicator.Show();
            try
            {
                var articleInfoList = await DependencyService.Get<S4mp.Client.Interfaces.IArticleClient>().SearchByBarCode((sender as IScannerResolver).Text);
                if (articleInfoList.Count == 0)
                {
                    Singleton<Helpers.Audio>.Instance.PlayError();
                    ClearForm();
                    ShowSelectArticleDialog(_scannerResolver);
                }
                else
                {
                    await SetResult(articleInfoList[0]);
                }
            }
            finally
            {
                activityIndicator.Hide();
            }
        }

        /// <summary>
        /// opens dialog for article searching
        /// </summary>
        /// <param name="scannerResolver">null or resolver if calling after scanning</param>
        private async void ShowSelectArticleDialog(IScannerResolver scannerResolver)
        {
            if (scannerResolver != null)
            {
                //stop listening
                scannerResolver.Init(false, ScannerResolver_Scanned);
            }

            //create and show dialog
            var getArticleDialog = new Dialogs.SelectArticleDialog();
            getArticleDialog.SetHeaderText("Skladem", "Zadejte kartu");
            getArticleDialog.Closed += GetArticleDialog_Closed;
            await Navigation.PushModalAsync(getArticleDialog, false);

            //do article searching if called after scanning
            if (scannerResolver != null)
                getArticleDialog.UnsuccessfullExtScan(scannerResolver.Text);
        }

        protected async void FilterActionMenu()
        {
            var action = await DisplayActionSheet("Seřadit podle", "Zrušit", null, 
                "Pozice A-Z",
                "Pozice Z-A",
                "Šarže",
                "Expirace 9-0",
                "Expirace 0-9");

            var list = ViewModel.ArtOnStoreViewList;

            switch (action)
            {
                case "Pozice A-Z":
                    list = list.OrderBy(_ => _.PosCode).ToList();
                    break;
                case "Pozice Z-A":
                    list = list.OrderByDescending(_ => _.PosCode).ToList();
                    break;
                case "Šarže":
                    list = list.OrderBy(_ => _.BatchNum).ToList();
                    break;
                case "Expirace 9-0":
                    list = list.OrderBy(_ => _.ExpirationDate.Value.Ticks).ToList();
                    break;
                case "Expirace 0-9":
                    list = list.OrderByDescending(_ => _.ExpirationDate.Value.Ticks).ToList();
                    break;
            }

            ViewModel.ArtOnStoreViewList = list;
        }
    }
}
﻿using System.Windows.Input;

namespace S4mp.Pages.OnStock
{
    public class OnPositionViewModel : OnStockViewModel
    {
        private S4.Entities.Position _position;
        public S4.Entities.Position Position
        {
            get => _position;
            set
            {
                SetField(ref _position, value);
                Header = _position == null ? null : _position.PosCode;
                HeaderText = _position == null ? null : $"Obsah pozice {_position.PosCode}";
            }
        }

        private string _headerText;
        public string HeaderText
        {
            get => _headerText;
            set => SetField(ref _headerText, value);
        }
    }
}

﻿using System;
using System.Runtime.CompilerServices;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using S4.Entities.Views;
using S4mp.Dialogs;
using S4mp.Controls;
using S4mp.Controls.Scanner;
using S4mp.Client;

namespace S4mp.Pages.OnStock
{
	public partial class OnPositionPage : ContentPage
	{
        // dependent resolver for scanning
        private IScannerResolver _scannerResolver;
        private OptionsMenu _optionsMenu;

        public OnPositionPage()
		{
			InitializeComponent();

            _optionsMenu = new OptionsMenu(this);

            // create scanning resolver
            _scannerResolver = DependencyService.Get<IScannerResolver>();
            // open dialog for position searching
            ShowSelectPositionDialog(null);
        }

        private OnPositionViewModel ViewModel => (OnPositionViewModel)BindingContext;

        protected override void OnAppearing()
        {
            base.OnAppearing();

            ViewModel.ArtOnStockActionMenuCmd = new Command<ArtOnStoreView>(ArtOnStockActionMenu);
        }

        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);

            if (propertyName.Equals("Renderer", StringComparison.OrdinalIgnoreCase))
            {
                // stop listening on closing
                var rr = DependencyService.Get<IRendererResolver>();
                if (!rr.HasRenderer(this))
                    _scannerResolver.Init(false, ScannerResolver_Scanned);
            }
        }

        private async void SelectPositionDialog_Closed(object sender, Controls.Common.ButtonsBar.ButtonsEnum e)
        {
            if (e != Controls.Common.ButtonsBar.ButtonsEnum.OK)
            {
                await Navigation.PopAsync();
                return;
            }

            activityIndicator.Show();
            try
            {
                ViewModel.Position = (sender as SelectPositionDialog).Position;
                var client = DependencyService.Get<IArtOnStoreClient>();
                ViewModel.ArtOnStoreViewList = await client.GetOnPosition(ViewModel.Position.PositionID);
            }
            finally
            {
                _optionsMenu.FormatTitle(ViewModel.HeaderText);
                activityIndicator.Hide();
            }

            // start scanner listening
            _scannerResolver.Init(true, ScannerResolver_Scanned);
        }

        protected async void ArtOnStockActionMenu(ArtOnStoreView item)
        {
            var action = await DisplayActionSheet(null, "Zrušit", null, "Detail");

            if(action == "Detail")
            {
                var articleDetailDlg = new GetArticleDetailDialog();
                await articleDetailDlg.SetArticleID(item.ArticleID);

                await Navigation.PushModalAsync(articleDetailDlg, false);
            }
        }

        private async void ScannerResolver_Scanned(object sender, BarCodeTypeEnum e)
        {
            if (Navigation.ModalStack.Count > 0 && Navigation.ModalStack[0] is GetArticleDetailDialog)
                await Navigation.PopModalAsync();
            ShowSelectPositionDialog(_scannerResolver);
        }

        /// <summary>
        /// opens dialog for position searching
        /// </summary>
        /// <param name="scannerResolver">null or resolver if calling after scanning</param>
        private async void ShowSelectPositionDialog(IScannerResolver scannerResolver)
        {
            if (scannerResolver != null)
            {
                // stop listening
                scannerResolver.Init(false, ScannerResolver_Scanned);
            }

            // create and show dialog
            var selectPositionDlg = new SelectPositionDialog();
            selectPositionDlg.SetHeaderText("Obsah pozice", "Zadejte pozici");
            selectPositionDlg.Closed += SelectPositionDialog_Closed;
            await Navigation.PushModalAsync(selectPositionDlg, false);

            // do article searching if called after scanning
            if (scannerResolver != null)
                selectPositionDlg.UnsuccessfullExtScan(scannerResolver.Text);
        }
    }
}
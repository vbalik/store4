﻿namespace S4mp.Pages.OnStock
{
    public class OnCarrierViewModel : OnStockViewModel
    {
        private string _carrierNum;
        public string CarrierNum
        {
            get => _carrierNum;
            set
            {
                SetField(ref _carrierNum, value);
                HeaderText = _carrierNum;
            }
        }

        private string _headerText;
        public string HeaderText
        {
            get => $"Obsah palety {_carrierNum}";
            set => SetField(ref _headerText, value);
        }
    }
}

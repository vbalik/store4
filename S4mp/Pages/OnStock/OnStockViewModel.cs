﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

using S4mp.Core.ViewModel;

namespace S4mp.Pages.OnStock
{
    public class OnStockViewModel : BaseViewModel
    {
        public OnStockViewModel()
        {
            _artOnStoreViewList = new List<S4.Entities.Views.ArtOnStoreView>();
        }

        private ICommand _artOnStockActionMenuCmd;
        public ICommand ArtOnStockActionMenuCmd
        {
            get => _artOnStockActionMenuCmd;
            set => SetField(ref _artOnStockActionMenuCmd, value);
        }

        private List<S4.Entities.Views.ArtOnStoreView> _artOnStoreViewList;
        public List<S4.Entities.Views.ArtOnStoreView> ArtOnStoreViewList
        {
            get => _artOnStoreViewList;
            set => SetField(ref _artOnStoreViewList, value);
        }

        private string _header;
        public string Header
        {
            get => _header;
            set => SetField(ref _header, value);
        }
    }
}

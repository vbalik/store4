﻿using System;
using System.Runtime.CompilerServices;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using S4.Entities.Views;
using S4mp.Dialogs;
using S4mp.Controls;
using S4mp.Controls.Scanner;
using S4mp.Client;

namespace S4mp.Pages.OnStock
{
	public partial class OnCarrierPage : ContentPage
	{
        //dependent resolver for scanning
        private IScannerResolver _scannerResolver;
        private OptionsMenu _optionsMenu;

        public OnCarrierPage()
		{
			InitializeComponent ();

            _optionsMenu = new OptionsMenu(this);

            ViewModel.ArtOnStockActionMenuCmd = new Command<ArtOnStoreView>(ArtOnStockActionMenu);

            // create scanning resolver
            _scannerResolver = DependencyService.Get<IScannerResolver>();
            // open dialog for carrier searching
            ShowSelectCarrierDialog(null);
        }

        private OnCarrierViewModel ViewModel { get => (OnCarrierViewModel)BindingContext; }

        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);

            if (propertyName.Equals("Renderer", StringComparison.OrdinalIgnoreCase))
            {
                //stop listening on closing
                var rr = DependencyService.Get<IRendererResolver>();
                if (!rr.HasRenderer(this))
                    _scannerResolver.Init(false, ScannerResolver_Scanned);
            }
        }

        private async void SelectCarrierDialog_Closed(object sender, Controls.Common.ButtonsBar.ButtonsEnum e)
        {
            if (e != Controls.Common.ButtonsBar.ButtonsEnum.OK)
            {
                await Navigation.PopAsync();
                return;
            }

            activityIndicator.Show();
            try
            {
                ViewModel.CarrierNum = (sender as SelectCarrierNumberDialog).CarrierNum;
                var client = DependencyService.Get<IArtOnStoreClient>();
                ViewModel.ArtOnStoreViewList = await client.GetByCarrier(ViewModel.CarrierNum);
            }
            finally
            {
                _optionsMenu.FormatTitle(ViewModel.HeaderText);
                activityIndicator.Hide();
            }

            //start scanner listening
            _scannerResolver.Init(true, ScannerResolver_Scanned);
        }

        protected async void ArtOnStockActionMenu(ArtOnStoreView item)
        {
            var action = await DisplayActionSheet(null, "Zrušit", null, "Detail");

            if (action == "Detail")
            {
                var articleDetailDlg = new GetArticleDetailDialog();
                await articleDetailDlg.SetArticleID(item.ArticleID);

                await Navigation.PushModalAsync(articleDetailDlg, false);
            }
        }

        private async void ScannerResolver_Scanned(object sender, BarCodeTypeEnum e)
        {
            if (Navigation.ModalStack.Count > 0 && Navigation.ModalStack[0] is GetArticleDetailDialog)
                await Navigation.PopModalAsync();
            ShowSelectCarrierDialog(_scannerResolver);
        }

        /// <summary>
        /// opens dialog for carrier searching
        /// </summary>
        /// <param name="scannerResolver">null or resolver if calling after scanning</param>
        private async void ShowSelectCarrierDialog(IScannerResolver scannerResolver)
        {
            if (scannerResolver != null)
            {
                //stop listening
                scannerResolver.Init(false, ScannerResolver_Scanned);
            }

            //create and show dialog
            var selectCarrierDialog = new SelectCarrierNumberDialog();
            selectCarrierDialog.SetHeaderText("Obsah palety", "Zadejte paletu");
            selectCarrierDialog.SetHideWithoutCarrier();
            selectCarrierDialog.Closed += SelectCarrierDialog_Closed;
            await Navigation.PushModalAsync(selectCarrierDialog, false);

            //do article searching if called after scanning
            if (scannerResolver != null)
                selectCarrierDialog.ExternalScan(scannerResolver);
        }
    }
}
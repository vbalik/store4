﻿namespace S4mp.Pages.OnStock
{
    public class ArtOnStockViewModel : OnStockViewModel
    {
        private S4.Entities.Article _article;
        public S4.Entities.Article Article
        {
            get => _article;
            set
            {
                SetField(ref _article, value);
                Header = _article?.ArticleInfo;
            }
        }
    }
}

﻿using S4.Core;
using S4mp.Client.ClientNavigation;
using System;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace S4mp.Pages
{
    public class LoginPage : ContentPage, ILoginPage
    {
        private Entry enUserName;
        private Entry enPassword;
        private Button btnLogin;
        private Label lbResult;
        private string password;
        private Switch swShowPassword;
        private Label lbShowPassword;

        public LoginPage()
        {
            Init();

            var user = Singleton<S4mp.Settings.LocalSettings>.Instance[S4mp.Core.Consts.XMLPATH_USER];
            password = Singleton<S4mp.Settings.LocalSettings>.Instance[S4mp.Core.Consts.XMLPATH_PASSWORD];

            enUserName.Text = user;

            if (!string.IsNullOrWhiteSpace(user) && !string.IsNullOrWhiteSpace(password))
            {
                swShowPassword.IsVisible = false;
                lbShowPassword.IsVisible = false;
                DoLogin(user.ToString(), password.ToString());
            }
        }

        private void ShowAlert(string text, bool error)
        {
            lbResult.IsVisible = !string.IsNullOrEmpty(text);

            if (lbResult.IsVisible)
            {
                if (error)
                {
                    lbResult.BackgroundColor = Color.Red;
                    lbResult.TextColor = Color.White;
                }
                else
                {
                    lbResult.BackgroundColor = Color.Transparent;
                    //lbResult.TextColor = S4mp.Controls.Common.CommonHelper.DialogLabelColor;
                }

                lbResult.Text = text;
            }
        }

        private async void BtnLogin_Clicked(object sender, EventArgs e)
        {
            await DoLogin(enUserName.Text, enPassword.Text);
        }

        private async Task DoLogin(string user, string pass)
        {
            // checks
            if (String.IsNullOrWhiteSpace(user))
            {
                ShowAlert("Vyplňte prosím uživatelské jméno", true);
                enUserName.Focus();
                return;
            }

            if (String.IsNullOrWhiteSpace(pass))
            {
                ShowAlert("Vyplňte prosím heslo", true);
                enPassword.Focus();
                return;
            }

            password = pass;

            ShowAlert("Přihlašování", false);

            if (await S4mp.StartUp.LogOn.TryToLog(user, password))
            {
                // ok
                await S4mp.StartUp.LogOn.AfterLogOn();

                await Logged();
            }
            else
            {
                // not ok
                ShowAlert("Nepodařilo se přihlásit", true);
                swShowPassword.IsVisible = true;
                lbShowPassword.IsVisible = true;
            }
        }

        private async Task Logged()
        {
            var menuData = await DependencyService.Get<Client.Interfaces.ITerminalClient>().MenuItems();
            MainPage.CreateMenuStructure(menuData);

            // default settings from server
            var defaultSettings = await DependencyService.Get<Client.Interfaces.ITerminalClient>().GetDefaultSettings();
            MainPage.SetDefaultSettings(defaultSettings);

            var mainPage = new MainPage();
            //mainPage.SetTitle();
            App.Current.MainPage = new NavigationPage(mainPage);
        }

        private void Init()
        {
            enUserName = new Entry() { Placeholder = Core.Consts.USER_NAME_PLACEHOLDER, FontSize = Core.Consts.TEXT_SIZE, AutomationId = "UserName" };
            enPassword = new Entry() { Placeholder = Core.Consts.PASSWORD_PLACEHOLDER, IsPassword = true, FontSize = Core.Consts.TEXT_SIZE, AutomationId = "Password" };
            enPassword.Completed += BtnLogin_Clicked;

            lbResult = new Label() { IsVisible = false };

            btnLogin = new Button() { Text = "Přihlásit", AutomationId = "btnLogin" };
            btnLogin.Clicked += BtnLogin_Clicked;

            swShowPassword = new Switch();
            lbShowPassword = new Label() { Text = "Zobrazit heslo" };
            swShowPassword.Toggled += SwShowPassword_Toggled;

            var grid = new Grid
            {
                ColumnDefinitions =
                {
                    new ColumnDefinition() { Width = GridLength.Auto },
                    new ColumnDefinition() { Width = GridLength.Auto }
                },

                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.StartAndExpand

            };

            grid.Children.Add(lbShowPassword, 0, 0);
            grid.Children.Add(swShowPassword, 1, 0);

            Content = new StackLayout
            {
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.Center,
                WidthRequest = 500.0,
                Children = {
                    enUserName,
                    enPassword,
                    btnLogin,
                    lbResult,
                    grid
                }
            };

            // Accomodate iPhone status bar.
            this.Padding = new Thickness(30, Device.OnPlatform(20, 0, 0), 30, 5);
        }

        void SwShowPassword_Toggled(object sender, ToggledEventArgs e)
        {
            var sw = sender as Switch;
            bool b = true;
            if (sw.IsToggled)
            {
                b = false;
            }

            enPassword.IsPassword = b;
        }
    }
}



﻿using Xamarin.Forms;
using S4mp.Dialogs;
using S4mp.Controls;
using S4.Core;
using S4mp.Client.Interfaces;

namespace S4mp.Pages.Print
{
    public partial class PrintBarcodeCarrierLabelsPage : ContentPage
	{
        public PrintBarcodeCarrierLabelsPage()
		{
			InitializeComponent();

            new OptionsMenu(this);

            // create and show dialog
            var printCarrierLabelsDlg = new BarcodePrintCarrierLabelsDialog();
            printCarrierLabelsDlg.SetHeaderText("Tisk paletových štítků", string.Empty);
            printCarrierLabelsDlg.LoadData();
            printCarrierLabelsDlg.Closed += BarcodePrintCarrierLabelsDlg_Closed;
            
            Navigation.PushModalAsync(printCarrierLabelsDlg, false);
        }

        private async void BarcodePrintCarrierLabelsDlg_Closed(object sender, Controls.Common.ButtonsBar.ButtonsEnum e)
        {
            if (e != Controls.Common.ButtonsBar.ButtonsEnum.OK)
            {
                await Navigation.PopAsync();
                return;
            }

            var printCarrierLabels = sender as BarcodePrintCarrierLabelsDialog;

            // client
            var client = DependencyService.Get<IBarcodeClient>();
            var printResult = await client.BarcodePrintCarrierLabels(new S4.ProcedureModels.BarCode.BarcodePrintCarrierLabels
            {
                LabelsQuant = (int) printCarrierLabels.Control.Quantity,
                PrinterLocationID = (int) printCarrierLabels.Control.PrinterLocationID
            });

            if (printResult == null || !printResult.Success)
            {
                Singleton<Helpers.Audio>.Instance.PlayError();
                await DisplayAlert("Chyba!", "Nepodařilo se dokončit tisk štítků. =Error=:" + printResult.ErrorText, "Zavřít");
            }
            else
            {
                await DisplayAlert("Informace", "Tisk štítků byl dokončen.", "Zavřít");
            }

            await Navigation.PopToRootAsync();
        }
    }
}
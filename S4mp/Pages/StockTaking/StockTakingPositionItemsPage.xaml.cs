﻿using S4.Core;
using S4.Entities;
using S4.ProcedureModels.StockTaking;
using S4mp.Dialogs;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace S4mp.Pages.StockTaking
{
    public partial class StockTakingPositionItemsPage : ContentPage, IPageLoadData
    {
        // temporary prop from first dialog
        private string _temporaryCarrierNum { get; set; }
        private Controls.OptionsMenu _optionsMenu;

        public StockTakingPositionItemsPage()
        {
            InitializeComponent();

            _optionsMenu = new Controls.OptionsMenu(this);

            ToolbarItems.Add(new ToolbarItem("Načíst předchozí položky", "download16W.png", () => _rollbackItems(), ToolbarItemOrder.Primary));
            ToolbarItems.Add(new ToolbarItem("+", "plus.png", async () => await _showCarrierNumberDialog(), ToolbarItemOrder.Primary));

            // update item list on add new item
            MessagingCenter.Subscribe<StockTakingAddItemPage, StockTakingPositionItemVM>(this, "AddItem", (sender, item) =>
            {
                ViewModel.ItemsList.Add(item);
            });
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            _optionsMenu.FormatTitle(ViewModel.HeaderText);
        }

        private StockTakingPositionItemsVM ViewModel => (StockTakingPositionItemsVM)BindingContext;

        private void SelectCarrierNumberDlg_Closed(object sender, Controls.Common.ButtonsBar.ButtonsEnum e)
        {
            if (e == Controls.Common.ButtonsBar.ButtonsEnum.Cancel)
                return;

            var dialog = sender as SelectCarrierNumberDialog;
            _temporaryCarrierNum = dialog.CarrierNum;

            // next dialog
            var getArticleDlg = new SelectArticleDialog();
            getArticleDlg.SetHeaderText(ViewModel.StockTaking.StotakDesc, "Zadejte kartu");
            getArticleDlg.Closed += GetArticleDlg_Closed;
            Navigation.PushModalAsync(getArticleDlg, false);
        }

        private async void GetArticleDlg_Closed(object sender, Controls.Common.ButtonsBar.ButtonsEnum e)
        {
            if (e == Controls.Common.ButtonsBar.ButtonsEnum.OK)
            {
                var dialog = sender as SelectArticleDialog;

                var stockTakingAddItemPage = new StockTakingAddItemPage
                {
                    BindingContext = new StockTakingAddItemVM
                    {
                        StotakID = ViewModel.StockTaking.StotakID,
                        PositionID = ViewModel.Position.PositionID,
                        EAN = dialog.GetArticleResult.EAN,
                        ArticlePackingInfo = dialog.GetArticleResult.ArticlePackingInfo,
                        CarrierNum = _temporaryCarrierNum,
                        StockTakingPositionItemsVM = ViewModel
                    }
                };
                stockTakingAddItemPage.PageLoadData();

                await Navigation.PushAsync(stockTakingAddItemPage);
            }
            else
            {
                // show carrier dialog
                await _showCarrierNumberDialog();
            }
        }

        private async Task _showCarrierNumberDialog()
        {
            var selectCarrierNumberDlg = new SelectCarrierNumberDialog();
            selectCarrierNumberDlg.SetHeaderText(ViewModel.StockTaking.StotakDesc, "Zadejte paletu");
            selectCarrierNumberDlg.Closed += SelectCarrierNumberDlg_Closed;
            await Navigation.PushModalAsync(selectCarrierNumberDlg, false);
        }

        public async void PageLoadData()
        {
            ViewModel.ItemActionMenuCmd = new Command<StockTakingPositionItemVM>(ItemActionMenu);

            activityIndicator.Show();
            try
            {
                ViewModel.ItemsList.Clear();
                foreach (var item in ViewModel.StockTakingScanPos.StockTakingItems)
                {
                    ViewModel.ItemsList.Add(await GetPositionItem(item));
                }

            }
            finally
            {
                activityIndicator.Hide();
            }
        }

        #region action menu

        protected async void ItemActionMenu(StockTakingPositionItemVM item)
        {
            var action = await DisplayActionSheet(null, "Zrušit", null, "Detail", "Odebrat položku");

            switch (action)
            {
                case "Detail":
                    _showItemDetail(item);
                    break;
                case "Odebrat položku":
                    _removeItem(item);
                    break;
            }
        }

        private async void _removeItem(StockTakingPositionItemVM item)
        {
            if (await DisplayAlert("Smazat položku?", $"Opravdu chcete odebrat položku {item.Code}?", "Ano", "Ne"))
            {
                await DependencyService.Get<Client.Interfaces.IStockTakingClient>().DeleteStockTakingItem(new S4.ProcedureModels.StockTaking.StockTakingDeletePosItem
                {
                    StotakID = ViewModel.StockTaking.StotakID,
                    PositionID = ViewModel.Position.PositionID,
                    StotakItemID = item.StotakItemID
                });

                ViewModel.ItemsList.Remove(item);
            }
        }

        private async void _showItemDetail(StockTakingPositionItemVM item)
        {
            var articleDetailDlg = new GetArticleDetailDialog();
            await articleDetailDlg.SetArticleID(item.ArticleID);

            await Navigation.PushModalAsync(articleDetailDlg, false);
        }

        #endregion

        protected async Task<StockTakingPositionItemVM> GetPositionItem(StockTakingItem stotakItem)
        {
            var articleInfo = await DependencyService.Get<S4mp.Client.Interfaces.IArticleClient>().GetArtPacking(stotakItem.ArtPackID);

            return new StockTakingPositionItemVM
            {
                StotakItemID = stotakItem.StotakItemID,
                Code = articleInfo.ArticleCode,
                ArticleInfo = articleInfo.ArticleInfo,
                BatchNum = stotakItem.BatchNum,
                ExpirationDate = stotakItem.ExpirationDate,
                Quantity = stotakItem.Quantity,
                PackDesc = articleInfo.ArticlePacking.PackDesc,
                ArtPackID = articleInfo.ArticlePacking.ArtPackID,
                ArticleID = articleInfo.ArticleID
            };
        }

        protected async void _rollbackItems()
        {
            if (!await DisplayAlert("Upozornění", "Načíst stav před inventůrou?", "OK", "Zrušit"))
            {
                return;
            }

            // rollback previous items on position
            var rollbackItems = await DependencyService.Get<Client.Interfaces.IStockTakingClient>().RollbackStockTakingPositionItems(new StockTakingPositionOK
            {
                StotakID = ViewModel.StockTaking.StotakID,
                PositionID = ViewModel.Position.PositionID
            });

            if(!rollbackItems.Success)
            {
                await DisplayAlert("Upozornění", rollbackItems.ErrorText, "OK");
                Singleton<Helpers.Audio>.Instance.PlayError();
                return;
            }

            // get new items
            var stoPosition = await DependencyService.Get<Client.Interfaces.IStockTakingClient>().GetStockTakingPositionItems(new StockTakingScanPos
            {
                StotakID = ViewModel.StockTaking.StotakID,
                PositionID = ViewModel.Position.PositionID
            });

            if(!stoPosition.Success)
            {
                await DisplayAlert("Upozornění", stoPosition.ErrorText, "OK");
                Singleton<Helpers.Audio>.Instance.PlayError();
                return;
            }

            // update items
            ViewModel.ItemsList.Clear();
            foreach (var item in stoPosition.StockTakingItems)
            {
                ViewModel.ItemsList.Add(await GetPositionItem(item));
            }
        }
    }
}
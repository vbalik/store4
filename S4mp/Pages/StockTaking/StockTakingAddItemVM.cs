﻿using S4mp.Core;
using S4mp.Core.EANCode;
using S4mp.Core.Misc;
using S4mp.Core.ViewModel;
using System;
using System.Globalization;
using System.Windows.Input;

namespace S4mp.Pages.StockTaking
{
    class StockTakingAddItemVM : BaseViewModel
    {
        #region expose through the view

        private ICommand _addItemCmd;
        public ICommand AddItemCmd
        {
            get => _addItemCmd;
            set => SetField(ref _addItemCmd, value);
        }

        private decimal _packRelation;
        public decimal PackRelation
        {
            get => _packRelation;
            set => SetField(ref _packRelation, value);
        }

        private string _articleInfo;
        public string ArticleInfo
        {
            get => _articleInfo;
            set => SetField(ref _articleInfo, value);
        }

        private string _headerText;
        public string HeaderText
        {
            get => $"Inventura {_headerText} - nová položka";
            set => SetField(ref _headerText, value);
        }

        private string _packDesc;
        public string PackDesc
        {
            get => _packDesc;
            set => SetField(ref _packDesc, value);
        }

        #endregion

        #region form

        private string _quantity;
        public string Quantity
        {
            get => _quantity;
            set
            {
                SetField(ref _quantity, value);
                if (int.TryParse(value, out var result))
                    ReCountQuantity = (result * PackRelation).ToString();
                else
                    ReCountQuantity = string.Empty;
            }
        }

        private string _reCountQuantity;
        public string ReCountQuantity
        {
            get => _reCountQuantity;
            set => SetField(ref _reCountQuantity, value);
        }

        private string _batchNum;
        public string BatchNum
        {
            get => _batchNum;
            set => SetField(ref _batchNum, value);
        }

        private string _expirationDateShort;
        public string ExpirationDateShort
        {
            get => _expirationDateShort;
            set => SetField(ref _expirationDateShort, _validateAndSetExpirationDate(value));
        }

        #endregion


        private StockTakingPositionItemsVM _stockTakingPositionsItemsVM;
        public StockTakingPositionItemsVM StockTakingPositionItemsVM
        {
            get => _stockTakingPositionsItemsVM;
            set
            {
                _stockTakingPositionsItemsVM = value;
                HeaderText = value.Position.PosCode;
            }
        }

        private int _stotakID;
        public int StotakID
        {
            get => _stotakID;
            set => SetField(ref _stotakID, value);
        }

        private int _positionID;
        public int PositionID
        {
            get => _positionID;
            set => SetField(ref _positionID, value);
        }

        private int _artPackID;
        public int ArtPackID
        {
            get => _artPackID;
            set => SetField(ref _artPackID, value);
        }

        private string _carrierNum;
        public string CarrierNum
        {
            get => _carrierNum;
            set => SetField(ref _carrierNum, value);
        }

        private DateTime? _expirationDate;
        public DateTime? ExpirationDate
        {
            get => _expirationDate;
            set => SetField(ref _expirationDate, value);
        }

        private S4.Entities.Models.ArticlePackingInfo _articlePackingInfo;
        public S4.Entities.Models.ArticlePackingInfo ArticlePackingInfo
        {
            get => _articlePackingInfo;
            set
            {
                _articlePackingInfo = value;

                ArticleInfo = value.ArticleInfo;
                ArtPackID = value.ArticlePacking.ArtPackID;
                PackRelation = (value.ArticlePacking.MovablePack) ? 1 : value.ArticlePacking.PackRelation;
                PackDesc = value.ArticlePacking.PackDesc;
            }
        }

        private EANCode _ean;
        public EANCode EAN
        {
            get => _ean;
            set
            {
                _ean = value;
                if (value == null) return;

                BatchNum = ViewModelHelpers.GetBatchNumber(value);

                // if empty then return
                if (value[EANCodePartEnum.ExpirationDate] == null) return;

                ExpirationDateShort = value[EANCodePartEnum.ExpirationDate]?.ValueDate?.ToString("d.M.yy");
            }
        }

        private string _validateAndSetExpirationDate(string value)
        {
            ExpirationDate = ViewModelHelpers.ParseExpirationDate(value);

            return value;
        }
    }
}
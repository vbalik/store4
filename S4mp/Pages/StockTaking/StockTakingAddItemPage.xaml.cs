﻿
using S4.ProcedureModels.StockTaking;
using S4mp.Core.Misc;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace S4mp.Pages.StockTaking
{
    public partial class StockTakingAddItemPage : ContentPage, IPageLoadData
    {
        public StockTakingAddItemPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            new Controls.OptionsMenu(this).FormatTitle(ViewModel.HeaderText);

            Entry_Focus();
        }

        private StockTakingAddItemVM ViewModel => (StockTakingAddItemVM)BindingContext;

        public void PageLoadData()
        {
            ViewModel.AddItemCmd = new Command(AddItem);

            if(ViewModel.ArticlePackingInfo.ArticlePacking.MovablePack) reCountQuantityBlock.IsVisible = false;
            if(!ViewModel.ArticlePackingInfo.UseBatch) batchNumBlock.IsVisible = false;
            if(!ViewModel.ArticlePackingInfo.UseExpiration) expirationDateBlock.IsVisible = false;
        }

        protected async void AddItem()
        {
            activityIndicator.Show();
            try
            {
                if (_isFormValid())
                {
                    var newItem = await DependencyService.Get<Client.Interfaces.IStockTakingClient>().AddStockTakingItem(new StockTakingAddPosItem
                    {
                        StotakID = ViewModel.StotakID,
                        PositionID = ViewModel.PositionID,
                        ArtPackID = ViewModel.ArtPackID,
                        CarrierNum = ViewModel.CarrierNum,
                        Quantity = decimal.Parse(ViewModel.Quantity),
                        BatchNum = ViewModel.BatchNum,
                        ExpirationDate = ViewModel.ExpirationDate
                    });

                    var articleInfo = await DependencyService.Get<S4mp.Client.Interfaces.IArticleClient>().GetArtPacking(ViewModel.ArtPackID);

                    activityIndicator.Hide();

                    await Navigation.PopAsync();

                    MessagingCenter.Send<StockTakingAddItemPage, StockTakingPositionItemVM>(this, "AddItem", new StockTakingPositionItemVM
                    {
                        StotakItemID = newItem.StotakItemID,
                        Code = articleInfo.ArticleCode,
                        ArticleInfo = articleInfo.ArticleInfo,
                        Quantity = decimal.Parse(ViewModel.Quantity),
                        BatchNum = ViewModel.BatchNum,
                        ExpirationDate = ViewModel.ExpirationDate,
                        PackDesc = articleInfo.ArticlePacking.PackDesc
                    });
                }
            }
            finally
            {
                activityIndicator.Hide();
            }
        }

        private bool _isFormValid()
        {
            var isValid = true;

            if (ViewModel.ArticlePackingInfo.UseBatch && string.IsNullOrEmpty(ViewModel.BatchNum))
            {
                isValid = false;
                edBatchNumErrorFrame.HasError();
            }
            if (string.IsNullOrEmpty(ViewModel.Quantity) || !int.TryParse(ViewModel.Quantity, out int batchNum))
            {
                isValid = false;
                edQuantityErrorFrame.HasError();
            }
            if (ViewModel.ArticlePackingInfo.UseExpiration && string.IsNullOrEmpty(ViewModel.ExpirationDateShort))
            {
                isValid = false;
                edExpirationDateErrorFrame.HasError();
            }

            return isValid;
        }

        #region UI handles

        /// <summary>
        /// On focus entry reset error fields
        /// </summary>
        private void Entry_Focused(object sender, FocusEventArgs e)
        {
            edQuantityErrorFrame.HasError(false);
            edBatchNumErrorFrame.HasError(false);
            edExpirationDateErrorFrame.HasError(false);
        }

        private void EntryExpirationDateShort_Unfocused(object sender, FocusEventArgs e)
        {
            var date = ((Entry)sender).Text;
            ViewModel.ExpirationDateShort = ExpirationDateHelper.ParseExpirationDateEntry(date);
        }

        /// <summary>
        /// Focus entry
        /// </summary>
        private async void Entry_Focus()
        {
            await Task.Delay(600);
            edQuantity.Focus();
        }

        #endregion
    }
}
﻿using S4mp.Core.ViewModel;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace S4mp.Pages.StockTaking
{
    public class StockTakingPositionItemsVM : BaseViewModel
    {
        #region expose to view

        private ICommand _addItemCmd;
        public ICommand AddItemCmd
        {
            get => _addItemCmd;
            set => SetField(ref _addItemCmd, value);
        }

        private ICommand _itemActionMenuCmd;
        public ICommand ItemActionMenuCmd
        {
            get => _itemActionMenuCmd;
            set => SetField(ref _itemActionMenuCmd, value);
        }

        #endregion

        private S4.Entities.StockTaking _stockTaking;
        public S4.Entities.StockTaking StockTaking
        {
            get => _stockTaking;
            set => SetField(ref _stockTaking, value);
        }

        private S4.Entities.Position _position;
        public S4.Entities.Position Position
        {
            get => _position;
            set {
                SetField(ref _position, value);
                HeaderText = value.PosCode;
            }
        }

        private S4.ProcedureModels.StockTaking.StockTakingScanPos _stockTakingScanPos;
        public S4.ProcedureModels.StockTaking.StockTakingScanPos StockTakingScanPos
        {
            get => _stockTakingScanPos;
            set {
                SetField(ref _stockTakingScanPos, value);
                ItemsList = new ObservableCollection<StockTakingPositionItemVM>();
            }
        }

        private string _headerText;
        public string HeaderText
        {
            get => $"Inventura: {_headerText}";
            set => SetField(ref _headerText, value);
        }
        

        public ObservableCollection<StockTakingPositionItemVM> ItemsList { get; set; }
    }
}

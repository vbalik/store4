﻿using System;

namespace S4mp.Pages.StockTaking
{
    public class StockTakingPositionItemVM
    {
        public int StotakItemID { get; set; }
        public string Code { get; set; }
        public string ArticleInfo { get; set; }
        public string BatchNum { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public decimal Quantity { get; set; }
        public string PackDesc { get; set; }
        public int ArtPackID { get; set; }
        public int ArticleID { get; set; }
    }
}

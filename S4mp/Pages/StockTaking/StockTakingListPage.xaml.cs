﻿using S4.Core;
using S4.ProcedureModels.StockTaking;
using S4mp.Dialogs;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Linq;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace S4mp.Pages.StockTaking
{
    public partial class StockTakingListPage : ContentPage, IPageLoadData, IRefreshable
    {
        public static readonly BindableProperty RefreshDataCommandProperty = BindableProperty.Create(nameof(LoadDataCommand), typeof(ICommand), typeof(StockTakingListPage), null);
        public ICommand LoadDataCommand
        {
            get { return (ICommand)GetValue(RefreshDataCommandProperty); }
            set { SetValue(RefreshDataCommandProperty, value); }
        }

        public static readonly BindableProperty IsBussyProperty = BindableProperty.Create(nameof(IsBussy), typeof(bool), typeof(StockTakingListPage), false);
        public bool IsBussy
        {
            get { return (bool)GetValue(IsBussyProperty); }
            set { SetValue(IsBussyProperty, value); }
        }

        private S4.Entities.StockTaking _stockTaking;

        public StockTakingListPage()
        {
            InitializeComponent();

            new Controls.OptionsMenu(this).FormatTitle("Seznam inventur");

            ToolbarItems.Add(new ToolbarItem("+", "plus.png", async () => await ShowNewStockTakingDialog(), ToolbarItemOrder.Primary));
            LoadDataCommand = new Command(PageLoadData);
        }

        public async void PageLoadData()
        {
            if (IsBussy == true) return;
            IsBussy = true;
            activityIndicator.Show();
            
            try
            {
                var stockTakingList = await DependencyService.Get<Client.Interfaces.IStockTakingClient>().GetStockTakingListOpened();
                stockTakingListView.ItemsSource = new ObservableCollection<S4.Entities.StockTaking>(stockTakingList.StockTakingList.OrderBy(_ => _.BeginDate.Ticks).ToList());
            }
            finally
            {
                IsBussy = false;
                activityIndicator.Hide();
            }
        }

        public void RefreshData()
        {
            PageLoadData();
        }

        private async Task ShowNewStockTakingDialog()
        {
            var newStockTakingDlg = new NewStockTakingDialog();
            newStockTakingDlg.Closed += NewStockTakingDlg_Closed;
            await Navigation.PushModalAsync(newStockTakingDlg, false);
        }

        private async void NewStockTakingDlg_Closed(object sender, Controls.Common.ButtonsBar.ButtonsEnum e)
        {
            if (e != Controls.Common.ButtonsBar.ButtonsEnum.OK)
                return;

            var dlg = sender as NewStockTakingDialog;
            var model = new S4.ProcedureModels.StockTaking.StockTakingBegin()
            {
                StotakDesc = dlg.StotakDesc,
                PositionIDs = dlg.Positions.Select(i => i.PositionID).ToArray()
            };
            model = await DependencyService.Get<Client.Interfaces.IStockTakingClient>().StockTakingBegin(model);
            if (!model.Success)
            {
                await DisplayAlert("Upozornění", model.ErrorText, "OK");
                Singleton<Helpers.Audio>.Instance.PlayError();

                return;
            }
            RefreshData();
        }

        private async void StockTakingListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item == null) return;

            // Deselect Item
            ((ListView)sender).SelectedItem = null;

            _stockTaking = (e.Item as S4.Entities.StockTaking);

            // position dialog
            await _showPositionDialog();
        }

        private async void SelectPositionDialog_Closed(object sender, Controls.Common.ButtonsBar.ButtonsEnum e)
        {
            if (e != Controls.Common.ButtonsBar.ButtonsEnum.OK)
            {
                return;
            }

            var position = (sender as SelectPositionDialog).Position;

            var stoPosition = await DependencyService.Get<Client.Interfaces.IStockTakingClient>().GetStockTakingPositionItems(new StockTakingScanPos {
                StotakID = _stockTaking.StotakID,
                PositionID = position.PositionID
            });

            if (stoPosition.BadPosition)
            {
                Singleton<Helpers.Audio>.Instance.PlayError();
                await DisplayAlert("Pozice nebyla nalezena", $"Pozice {position.PosCode} není v inventůře.", "OK");

                // show position dialog again
                await _showPositionDialog();

                return;
            }

            var stockTakingPositionItemsPage = new StockTakingPositionItemsPage
            {
                BindingContext = new StockTakingPositionItemsVM
                {
                    StockTaking = _stockTaking,
                    Position = position,
                    StockTakingScanPos = stoPosition
                }
            };

            stockTakingPositionItemsPage.PageLoadData();

            await Navigation.PushAsync(stockTakingPositionItemsPage);
        }

        private async Task _showPositionDialog()
        {
            var selectPositionDlg = new SelectPositionDialog();
            selectPositionDlg.SetHeaderText(_stockTaking.StotakDesc, "Zadejte pozici");
            selectPositionDlg.Closed += SelectPositionDialog_Closed;
            await Navigation.PushModalAsync(selectPositionDlg, false);
        }
    }
}

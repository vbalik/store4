﻿using S4.Core;
using S4mp.Client.Interfaces;
using Xamarin.Forms;
using S4mp.Core.Logger;
using System;
using S4mp.Core;

namespace S4mp.Pages.ReportError
{
    public class ReportErrorPage : ContentPage, IPageLoadData
    {
		public ReportErrorPage()
		{
			new Controls.OptionsMenu(this).FormatTitle("Nahlásit chybu");

			var label = new Label
			{
				HorizontalTextAlignment = TextAlignment.Center,
				Text = "Odesílání chyb ...",
				FontSize = 20,
				TextColor = Color.DarkRed
			};

			Content = new StackLayout
			{
				VerticalOptions = LayoutOptions.Center,
				Children = {
					label,
				}
			};
		}

		public async void PageLoadData()
		{
			var logs = await DependencyService.Get<ICustomEventLogger>().GetLogs(null);

			var device = DependencyService.Get<IDevice>();

			await DependencyService.Get<ISystemClient>().SendErrorLogs(new ReportErrorModel { 
				AppInfo = new AppInfoModel
				{
					Created = DateTime.Now,
					Version = Application.Current.Properties[S4mp.Core.Consts.PROP_APP_VERSION].ToString(),
					UserID = Singleton<BaseInfo>.Instance.CurrentWorkerID,
					OS_ver = Application.Current.Properties[S4mp.Core.Consts.PROP_OS_DESCRIPTION].ToString(),
					MONO_ver = Application.Current.Properties[S4mp.Core.Consts.PROP_MONO_DESCRIPTION].ToString(),
					DeviceID = device != null ? device.GetIdentifier() : string.Empty
				},
				Logs = logs
			});

			await Navigation.PopToRootAsync();
		}
	}
}



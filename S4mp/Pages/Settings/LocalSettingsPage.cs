﻿using System;

using Xamarin.Forms;

using S4mp.Controls.Common;
using S4.Core;
using S4mp.Core;

namespace S4mp.Pages.Settings
{
	public class LocalSettingsPage : S4mp.Controls.Dialogs.EditFormDialog
	{
		public LocalSettingsPage ()
		{
            Caption = "Nastavení systému";

            var itApiUrl = new SettingsItem("Server:", S4mp.Core.Consts.XMLPATH_API_URL, ValueRequired, "http://10.0.0.41:5000/") { ButtonsBar = this.ButtonsBar };
            itApiUrl.ValueChanged += ValueChanged;
			itApiUrl.SaveRequired += SaveRequired;

            var scrWeb = new SettingsItem("Skener Web:", Core.Consts.XMLPATH_CONNECT_SCANNER_WEB, ValueRequired, false, false, true) { ButtonsBar = this.ButtonsBar };
			scrWeb.ValueChanged += ValueChanged;
			scrWeb.SaveRequired += SaveRequired;

            var argomedFeatures = new SettingsItem("Enable Argomed feaures:", Core.Consts.XMLPATH_ARGOMED_FEATURES, ValueRequired, false, false, true) { ButtonsBar = this.ButtonsBar };
            argomedFeatures.ValueChanged += ValueChanged;
            argomedFeatures.SaveRequired += SaveRequired;

            var itUser = new SettingsItem("Uživatel:", Core.Consts.XMLPATH_USER, ValueRequired, "a") { ButtonsBar = this.ButtonsBar };
			itUser.ValueChanged += ValueChanged;
			itUser.SaveRequired += SaveRequired;

			var itTextSize = new SettingsItem("Velikost písma:", Core.Consts.XMLPATH_TEXTSIZE, ValueRequired, "Medium") { ButtonsBar = this.ButtonsBar };
			itTextSize.ValueChanged += ValueChanged;
			itTextSize.SaveRequired += SaveRequired;

            var innerStack = new StackLayout();
            innerStack.Children.Add(itApiUrl);
            innerStack.Children.Add(scrWeb);
            innerStack.Children.Add(argomedFeatures);
            innerStack.Children.Add(itUser);
            if (Singleton<BaseInfo>.Instance.AppStatus == BaseInfo.AppStatusEnum.LoggedOn/* && Singleton<Base.BaseInfo>.Instance.ApprovalSet.IsAdministrator()*/)
            {
                var itPassword = new SettingsItem("Heslo:", Core.Consts.XMLPATH_PASSWORD, ValueRequired, isPassword: true) { ButtonsBar = this.ButtonsBar };
                itPassword.ValueChanged += ValueChanged;
                itPassword.SaveRequired += SaveRequired;
                innerStack.Children.Add(itPassword);
            }
            innerStack.Children.Add(itTextSize);

            ScrollView scrollView = new ScrollView
            {
                VerticalOptions = LayoutOptions.FillAndExpand,
                Content = innerStack
            };

			AddFullSizeChild(scrollView);
		}

		private void SaveRequired (string path, object value)
		{
			Singleton<S4mp.Settings.LocalSettings>.Instance [path] = string.Format ("{0}", value);
		}

		private void ValueChanged (string path, object value)
		{
            SetChanged(true);
		}

		private object ValueRequired(string path)
		{
			return Singleton<S4mp.Settings.LocalSettings>.Instance [path];
		}

	}
}



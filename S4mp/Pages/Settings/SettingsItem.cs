﻿using System;
using System.ComponentModel;

using Xamarin.Forms;

using S4mp.Controls.Common;

namespace S4mp
{
	public class SettingsItem : ContentView
	{
		private Label lbName;
		private Entry enValue;
		private CheckBox elCheckBox;
		private string _path;
		private ButtonsBar _buttonsBar;

		private bool _isPassword;

		public SettingsItem (string text, string path, ValueRequiredEventHandler valueRequired, object defaultValue = null, bool isPassword = false, bool isCheckBox = false)
		{
			_isPassword = isPassword;

            Init();

			_path = path;

			lbName.Text = text;

			var stackLayout = new StackLayout
			{
				Padding = new Thickness(4),
				Orientation = StackOrientation.Vertical,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = {
					lbName
				}
			};


			if (isCheckBox)
			{
				var value = valueRequired.Invoke(_path);
				if (bool.TryParse(value?.ToString(), out var valueBool))
					value = valueBool;

				elCheckBox.IsChecked = (bool) (value ?? defaultValue);

				elCheckBox.CheckedChanged += OnValueCheckedChanged;

				stackLayout.Children.Add(elCheckBox);
			}
			else
			{
				var value = valueRequired.Invoke(_path);
				enValue.Text = string.Format("{0}", value ?? defaultValue);

				enValue.TextChanged += OnValueChanged;

				stackLayout.Children.Add(enValue);
			}


			Content = stackLayout;
		}

		public delegate object ValueRequiredEventHandler(string path);

		public delegate void ValueChangedEventHandler(string path, object value);
		[DefaultValue(null)]
		public event ValueChangedEventHandler ValueChanged;

		protected virtual void OnValueChanged(object sender, EventArgs e)
		{
			if (ValueChanged != null)
				ValueChanged.Invoke (_path, (sender as Entry).Text);
		}

		protected virtual void OnValueCheckedChanged(object sender, EventArgs e)
		{
			if (ValueChanged != null)
				ValueChanged.Invoke(_path, (sender as CheckBox).IsChecked.ToString());
		}

		public delegate void SaveRequiredEventHandler(string path, object value);
		[DefaultValue(null)]
		public event SaveRequiredEventHandler SaveRequired;

		public ButtonsBar ButtonsBar
		{
			get { return _buttonsBar; }
			set {
				if (value == _buttonsBar)
					return;

				if (value == null && _buttonsBar != null)
					_buttonsBar.ButtonClicked -= ButtonsBarClicked;

				_buttonsBar = value;

				if (_buttonsBar != null)
					_buttonsBar.ButtonClicked += ButtonsBarClicked;
			}
		}

		protected virtual void ButtonsBarClicked(ButtonsBar.ButtonsEnum button)
		{
			if (SaveRequired == null)
				return;

			if (button == ButtonsBar.ButtonsEnum.OK)
			{
				if(enValue.Text != null)
					SaveRequired.Invoke(_path, enValue.Text);
				else if(elCheckBox != null)
					SaveRequired.Invoke(_path, elCheckBox.IsChecked.ToString());
			}
		}

        private void Init()
        {
            lbName = new Label() { FontSize = Core.Consts.TEXT_SIZE, /*TextColor = CommonHelper.DialogLabelColor, VerticalOptions = LayoutOptions.Center, */HorizontalOptions = LayoutOptions.Start/*, WidthRequest = 200*/ };
			enValue = new Entry() { FontSize = Core.Consts.TEXT_SIZE, HorizontalOptions = LayoutOptions.FillAndExpand, IsPassword = _isPassword, MinimumWidthRequest = 350 };
			elCheckBox = new CheckBox() { HorizontalOptions = LayoutOptions.Center };
        }
	}
}



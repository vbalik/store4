﻿using Newtonsoft.Json;
using S4.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace S4mp.Pages.Settings
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ScanSettingsPage : ContentPage
    {
        public ScanSettingsPage()
        {
            InitializeComponent();

            ValueScannedCmd = new Command(ValueScanned);
        }


        public static readonly BindableProperty ValueScannedCmdProperty = BindableProperty.Create(nameof(ValueScannedCmd), typeof(ICommand), typeof(ScanSettingsPage));
        public ICommand ValueScannedCmd
        {
            get => (ICommand)GetValue(ValueScannedCmdProperty);
            set => SetValue(ValueScannedCmdProperty, value);
        }

        public event EventHandler SettingLoaded;


        private async void CloseButton_Clicked(object sender, EventArgs e)
        {
            await Application.Current.MainPage.Navigation.PopModalAsync();
        }

        private async void ValueScanned(object sender)
        {
            var text = scannerView.Text;
            if(String.IsNullOrEmpty(text))
            {
                Singleton<Helpers.Audio>.Instance.PlayError();
                await DisplayAlert("Upozornění", "Načtená data jsou prázdná", "OK");
            }
            else if(!text.StartsWith(S4.Entities.Helpers.TerminalSettings.SETTINGS_MARK))
            {
                Singleton<Helpers.Audio>.Instance.PlayError();
                await DisplayAlert("Upozornění", "Načtená data nejsou ve správném formátu", "OK");
            }
            else
            {
                // try to parse JSON data
                var json = text.Substring(S4.Entities.Helpers.TerminalSettings.SETTINGS_MARK.Length);
                var settings = JsonConvert.DeserializeObject<S4.Entities.Helpers.TerminalSettings>(json);

                if (settings.ApiUrl != null)
                    Singleton<S4mp.Settings.LocalSettings>.Instance[Core.Consts.XMLPATH_API_URL] = settings.ApiUrl;
                if (settings.InstanceName != null)
                    Singleton<S4mp.Settings.LocalSettings>.Instance[Core.Consts.XMLPATH_INSTANCE_NAME] = settings.InstanceName;
                if (settings.User != null)
                    Singleton<S4mp.Settings.LocalSettings>.Instance[Core.Consts.XMLPATH_USER] = settings.User;
                if (settings.Password != null)
                    Singleton<S4mp.Settings.LocalSettings>.Instance[Core.Consts.XMLPATH_PASSWORD] = settings.Password;
                if (settings.TextSize != null)
                    Singleton<S4mp.Settings.LocalSettings>.Instance[Core.Consts.XMLPATH_TEXTSIZE] = settings.TextSize;

                S4mp.App.Instance.SaveSetting();
                await DisplayAlert("Informace", "Nastavení načteno", "Zavřít");
                await Navigation.PopModalAsync();
                SettingLoaded?.Invoke(this, new EventArgs());
            }
        }
    }
}
﻿using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Xamarin.Forms;
using S4mp.Controls;
using S4mp.Controls.Scanner;
using S4.Core;

namespace S4mp.Pages.Article
{
    public partial class ShowArticlePage : ContentPage
	{
        //dependent resolver for scanning
        private IScannerResolver _scannerResolver;
        private OptionsMenu _optionsMenu;

        public ShowArticlePage()
		{
			InitializeComponent();

#if !DATALOGIC && !HONEYWELL
            var btnScan = new Button
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                Text = "SCAN",
                Command = new Command(() =>
                {
                    _scannerResolver.Scan();
                })
            };
            (Content as StackLayout).Children.Insert(0, btnScan);
#endif

            _optionsMenu = new OptionsMenu(this);

            //create scanning resolver
            _scannerResolver = DependencyService.Get<IScannerResolver>();
            //open dialog for article searching
            ShowSelectArticleDialog(null);
        }

        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);

            if (propertyName.Equals("Renderer", StringComparison.OrdinalIgnoreCase))
            {
                //stop listening on closing
                var rr = DependencyService.Get<IRendererResolver>();
                if (!rr.HasRenderer(this))
                    _scannerResolver.Init(false, ScannerResolver_Scanned);
            }
        }

        private async void ScannerResolver_Scanned(object sender, BarCodeTypeEnum e)
        {
            activityIndicator.Show();
            try
            {
                var articleInfoList = await DependencyService.Get<S4mp.Client.Interfaces.IArticleClient>().SearchByBarCode((sender as IScannerResolver).Text);
                if (articleInfoList.Count == 0)
                {
                    Singleton<Helpers.Audio>.Instance.PlayError();
                    ClearForm();
                    ShowSelectArticleDialog(_scannerResolver);
                }
                else
                {
                    await SetResult(articleInfoList[0]);
                }
            }
            finally
            {
                activityIndicator.Hide();
            }
        }

        private async Task SetResult(S4.Entities.Models.ArticlePackingInfo articleInfo)
        {
            //load article info
            await articleDetail.LoadArticleData(articleInfo.ArticleID, false);
            _optionsMenu.FormatTitle(articleInfo.ArticleCode);
        }

        private void ClearForm()
        {
            articleDetail.ClearForm();
            _optionsMenu.FormatTitle(null);
        }

        private async void GetArticleDialog_Closed(object sender, Controls.Common.ButtonsBar.ButtonsEnum e)
        {
            if (e == Controls.Common.ButtonsBar.ButtonsEnum.OK)
            {
                var articleInfo = (sender as Dialogs.SelectArticleDialog).GetArticleResult.ArticlePackingInfo;
                await SetResult(articleInfo);

                //start scanner listening
                _scannerResolver.Init(true, ScannerResolver_Scanned);
                DependencyService.Get<IKeyboardHelper>().HideKeyboard();
            }
            else
            {
                //this event is fired only by cancel btn
                //back to the MainPage
                await Navigation.PopAsync();
            }
        }

        /// <summary>
        /// opens dialog for article searching
        /// </summary>
        /// <param name="scannerResolver">null or resolver if calling after scanning</param>
        private async void ShowSelectArticleDialog(IScannerResolver scannerResolver)
        {
            if (scannerResolver != null)
            {
                //stop listening
                scannerResolver.Init(false, ScannerResolver_Scanned);
            }

            //create and show dialog
            var getArticleDialog = new Dialogs.SelectArticleDialog();
            getArticleDialog.SetHeaderText("Ukázat kartu", "Zadejte kartu");
            getArticleDialog.Closed += GetArticleDialog_Closed;

            await Navigation.PushModalAsync(getArticleDialog, false);

            //do article searching if called after scanning
            if (scannerResolver != null)
                getArticleDialog.UnsuccessfullExtScan(scannerResolver.Text);
        }
    }
}
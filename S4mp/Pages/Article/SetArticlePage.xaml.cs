﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
using System.Windows.Input;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using S4mp.Controls;
using S4mp.Controls.Scanner;
using S4.Core;
using S4mp.InternalControls.Articles;
using S4.ProcedureModels.BarCode;

namespace S4mp.Pages.Article
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SetArticlePage : ContentPage
	{
        //dependent resolver for scanning
        private IScannerResolver _scannerResolver;
        private OptionsMenu _optionsMenu;
        private ArticleDetailControlVM.ArticlePackingModel _packing;
        private bool _anotherBarCodeAdd = false;

        public SetArticlePage ()
		{
			InitializeComponent (); _optionsMenu = new OptionsMenu(this);

#if !DATALOGIC && !HONEYWELL
            var btnScan = new Button
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                Text = "SCAN",
                Command = new Command(() =>
                {
                    _scannerResolver.Scan();
                })
            };
            (Content as StackLayout).Children.Insert(0, btnScan);
#endif

            //create scanning resolver
            _scannerResolver = DependencyService.Get<IScannerResolver>();

            ItemActionMenuCmd = new Command<ArticleDetailControlVM.ArticlePackingModel>(ItemActionCommand);

            ToolbarItems.Add(new ToolbarItem("Hledat", "searchW.png", Button_Clicked, ToolbarItemOrder.Primary));

            //open dialog for article searching
            ShowSelectArticleDialog(null);
        }

        public static readonly BindableProperty ItemActionMenuCmdProperty = BindableProperty.Create(nameof(ItemActionMenuCmd), typeof(ICommand), typeof(SetArticlePage));
        public ICommand ItemActionMenuCmd
        {
            get => (ICommand)GetValue(ItemActionMenuCmdProperty);
            set => SetValue(ItemActionMenuCmdProperty, value);
        }

        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);

            if (propertyName.Equals("Renderer", StringComparison.OrdinalIgnoreCase))
            {
                //stop listening on closing
                var rr = DependencyService.Get<IRendererResolver>();
                if (!rr.HasRenderer(this))
                    _scannerResolver.Init(false, ScannerResolver_Scanned);
            }
        }

        private async void ScannerResolver_Scanned(object sender, BarCodeTypeEnum e)
        {
            activityIndicator.Show();
            try
            {
                var articleInfoList = await DependencyService.Get<S4mp.Client.Interfaces.IArticleClient>().SearchByBarCode((sender as IScannerResolver).Text);
                if (articleInfoList.Count == 0)
                {
                    Singleton<Helpers.Audio>.Instance.PlayError();
                    ClearForm();
                    ShowSelectArticleDialog(_scannerResolver);
                }
                else
                {
                    await SetResult(articleInfoList[0]);
                }
            }
            finally
            {
                activityIndicator.Hide();
            }
        }

        private void ClearForm()
        {
            articleDetail.ClearForm();
            _optionsMenu.FormatTitle(null);
        }

        private async void GetArticleDialog_Closed(object sender, Controls.Common.ButtonsBar.ButtonsEnum e)
        {
            if (e == Controls.Common.ButtonsBar.ButtonsEnum.OK)
            {
                var articleInfo = (sender as Dialogs.SelectArticleDialog).GetArticleResult.ArticlePackingInfo;
                await SetResult(articleInfo);

                //start scanner listening
                _scannerResolver.Init(true, ScannerResolver_Scanned);
                DependencyService.Get<IKeyboardHelper>().HideKeyboard();
            }
            else
            {
                //this event is fired only by cancel btn
                //back to the MainPage
                await Navigation.PopAsync();
            }
        }

        private async Task SetResult(S4.Entities.Models.ArticlePackingInfo articleInfo)
        {
            //load article info
            await articleDetail.LoadArticleData(articleInfo.ArticleID, false);
            _optionsMenu.FormatTitle(articleInfo.ArticleCode);
        }

        /// <summary>
        /// opens dialog for article searching
        /// </summary>
        /// <param name="scannerResolver">null or resolver if calling after scanning</param>
        private async void ShowSelectArticleDialog(IScannerResolver scannerResolver)
        {
            if (scannerResolver != null)
            {
                //stop listening
                scannerResolver.Init(false, ScannerResolver_Scanned);
            }

            //create and show dialog
            var getArticleDialog = new Dialogs.SelectArticleDialog();
            getArticleDialog.SetHeaderText("Nastavení kódů", "Zadejte kartu");
            //start scanner listening
            getArticleDialog.Closed += GetArticleDialog_Closed;
            await Navigation.PushModalAsync(getArticleDialog);

            //do article searching if called after scanning
            if (scannerResolver != null)
                getArticleDialog.UnsuccessfullExtScan(scannerResolver.Text);
        }

        private enum ActionEnum { BarcodeSet, BarcodeMakeCode, BarcodeDelete, AnotherBarcodeAdd, PrintLabel };
        private async void ItemActionCommand(ArticleDetailControlVM.ArticlePackingModel packing)
        {
            _packing = packing;
            _anotherBarCodeAdd = false;

            Dictionary<ActionEnum, string> buttons = new Dictionary<ActionEnum, string>();
            if (string.IsNullOrEmpty(packing.BarCode))
            {
                buttons.Add(ActionEnum.BarcodeSet, "Nastavit čárový kód");
                buttons.Add(ActionEnum.BarcodeMakeCode, "Vygenerovat čárový kód");
            }
            else
            {
                buttons.Add(ActionEnum.BarcodeDelete, "Vymazat čárový kód");
                buttons.Add(ActionEnum.PrintLabel, "Vytisknout štítek");
                buttons.Add(ActionEnum.AnotherBarcodeAdd, "Nastavit další čárový kód");
            }

            var button = await DisplayActionSheet($"Akce pro balení {packing.PackDesc}", "Zavřít", null, buttons.Values.ToArray());
            if (!buttons.ContainsValue(button))
                return;

            Device.BeginInvokeOnMainThread(async () =>
            {
                switch (buttons.Where(b => b.Value == button).First().Key)
                {
                    case ActionEnum.BarcodeDelete:
                        DeleteBarcode();
                        break;
                    case ActionEnum.BarcodeMakeCode:
                        BarcodeMakeCode();
                        break;
                    case ActionEnum.BarcodeSet:
                        //stop scanner listening
                        _scannerResolver.Init(false, ScannerResolver_Scanned);
                        //create and show dialog
                        var setBarcodeDialog = new Dialogs.SetBarcodeDialog();
                        setBarcodeDialog.Closed += SetBarcodeDialog_Closed;
                        await Navigation.PushModalAsync(setBarcodeDialog, false);
                        break;
                    case ActionEnum.AnotherBarcodeAdd:
                        _anotherBarCodeAdd = true;
                        //stop scanner listening
                        _scannerResolver.Init(false, ScannerResolver_Scanned);
                        //create and show dialog
                        var setBarcodeDialog2 = new Dialogs.SetBarcodeDialog();
                        setBarcodeDialog2.Closed += SetBarcodeDialog_Closed;
                        await Navigation.PushModalAsync(setBarcodeDialog2, false);
                        break;
                    case ActionEnum.PrintLabel:
                        //stop scanner listening
                        _scannerResolver.Init(false, ScannerResolver_Scanned);
                        //create and show dialog
                        var setPrintDialog = new Dialogs.SetPrintDialog();
                        setPrintDialog.Closed += SetPrintDialog_Closed;
                        await Navigation.PushModalAsync(setPrintDialog, false);
                        break;
                    default:
                        throw new NotImplementedException($"Action {button} is not implemented!");
                }
            });
        }

        private async void SetPrintDialog_Closed(object sender, Controls.Common.ButtonsBar.ButtonsEnum e)
        {
            if (e == Controls.Common.ButtonsBar.ButtonsEnum.OK)
            {
                var printerID = (sender as Dialogs.SetPrintDialog).PrinterID;
                var count = (sender as Dialogs.SetPrintDialog).Count;

                var model = new BarcodePrintLabel
                {
                    ArtPackID = _packing.ArtPackID,
                    LabelsQuant = count,
                    PrinterLocationID = printerID,
                    ArtPackBarCodeID = _packing.PackingBarCodeID
                };

                var result = await DependencyService.Get<S4mp.Client.Interfaces.IBarcodeClient>().BarcodePrintLabel(model);
                if (await ProcessResult(result))
                    //load article info
                    await articleDetail.LoadArticleData(_packing.ArticleID, false);
            }

            //start scanner listening
            _scannerResolver.Init(true, ScannerResolver_Scanned);
        }

        private async void SetBarcodeDialog_Closed(object sender, Controls.Common.ButtonsBar.ButtonsEnum e)
        {
            if (e == Controls.Common.ButtonsBar.ButtonsEnum.OK)
            {
                var barcode = (sender as Dialogs.SetBarcodeDialog).Barcode;

                var model = new BarcodeSet {
                    ArtPackID = _packing.ArtPackID,
                    BarCode = barcode,
                    ArtPackBarCodeID = _packing.PackingBarCodeID,
                    AnotherBarCodeCreation = _anotherBarCodeAdd
                };

                var result = await DependencyService.Get<S4mp.Client.Interfaces.IBarcodeClient>().BarcodeSet(model);

                if (await ProcessResult(result))
                {
                    _anotherBarCodeAdd = false;
                    //load article info
                    await articleDetail.LoadArticleData(_packing.ArticleID, false);
                }
            }

            //start scanner listening
            _scannerResolver.Init(true, ScannerResolver_Scanned);
        }

        private async void DeleteBarcode()
        {
            if (await Controls.Dialogs.Confirmation.Confirm($"Opravdu chcete vymazat kód {_packing.BarCode} balení {_packing.PackDesc}?", this))
            {
                var model = new BarcodeDelete
                {
                    ArtPackID = _packing.ArtPackID,
                    ArtPackBarCodeID = _packing.PackingBarCodeID
                };

                var result = await DependencyService.Get<S4mp.Client.Interfaces.IBarcodeClient>().BarcodeDelete(model);

                if (!await ProcessResult(result))
                    return;

                //load article info
                await articleDetail.LoadArticleData(_packing.ArticleID, false);
            }
        }

        private async void BarcodeMakeCode()
        {
            var client = new Client.BarcodeClient();
            var result = await client.BarcodeMakeCode(new S4.ProcedureModels.BarCode.BarcodeMakeCode() { ArtPackID = _packing.ArtPackID });
            if (!await ProcessResult(result))
                return;

            //load article info
            await articleDetail.LoadArticleData(_packing.ArticleID, false);
        }

        private async Task<bool> ProcessResult(S4.ProcedureModels.ProceduresModel result)
        {
            if (!result.Success)
            {
                await DisplayAlert("Akce skončila s chybou!", result.ErrorText, "OK");
                return false;
            }

            return true;
        }

        private void Button_Clicked()
        {
            //open dialog for article searching
            ShowSelectArticleDialog(null);
        }
    }
}
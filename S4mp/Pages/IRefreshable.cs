﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4mp.Pages
{
    interface IRefreshable
    {
        void RefreshData();
    }
}

﻿using S4.Core;
using S4.Entities;
using S4.Entities.Helpers;
using S4.ProcedureModels.Move;
using S4mp.Client;
using S4mp.Controls.Common;
using S4mp.Dialogs;
using S4mp.Exceptions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace S4mp.Pages.Direction.StoreMove
{
    public partial class StoreMovePage : ContentPage
    {
        private List<ArticleInfo> _temporary_SelectedItemsForMove = new List<ArticleInfo>();
        private List<ArticleInfo> _temporary_ItemsForMove = new List<ArticleInfo>();
        private Position _temporary_PositionSource;
        private Position _temporary_PositionDest;
        private string _temporary_ArticleCode;
        private int? _temporary_ArticleID;

        public StoreMovePage()
        {
            InitializeComponent();

            new Controls.OptionsMenu(this);

            _showNavigationDialog();
        }

        private async void SelectPositionSourceDialog_Closed(object sender, ButtonsBar.ButtonsEnum e)
        {
            if (e != ButtonsBar.ButtonsEnum.OK)
            {
                await _showNavigationDialog();
                return;
            }

            _temporary_PositionSource = (sender as SelectPositionDialog).Position;

            // select items for move dialog
            await _showPositionSelectItemsDialog();
        }

        private async void StoreMoveSelectPositionItemsDialog_Closed(object sender, ButtonsBar.ButtonsEnum e)
        {
            if (e != ButtonsBar.ButtonsEnum.OK)
            {
                await _showPositionDialog(SelectPositionSourceDialog_Closed, "Přeskladnění", "Zadejte výchozí pozici");
                return;
            }

            _temporary_SelectedItemsForMove = (sender as StoreMovePositionSelectItemsDialog).SelectedItems;

            // next dest position dialog
            await _showPositionDialog(SelectPositionDestDialog_Closed, "Přeskladnění", "Zadejte cílovou pozici");
        }

        private async void SelectPositionDestDialog_Closed(object sender, ButtonsBar.ButtonsEnum e)
        {
            if (e != ButtonsBar.ButtonsEnum.OK)
            {
                // show items by article
                if (_temporary_ArticleID != null)
                {
                    await _showArticleSelectItemsDialog();
                    return;
                }
                
                // show items by position
                await _showPositionSelectItemsDialog();
                return;
            }

            _temporary_PositionDest = (sender as SelectPositionDialog).Position;

            //client
            var client = new StoreMoveClient();
            var resultLockPosition = await client.MoveLockPosition(new MoveLockPos
            {
                PositionID = _temporary_PositionDest.PositionID
            });

            if (!resultLockPosition.Success)
            {
                Singleton<Helpers.Audio>.Instance.PlayError();
                await DisplayAlert("Upozornění", resultLockPosition.ErrorText, "OK");
                return;
            }

            if (resultLockPosition.PositionResult == MoveStatusEnum.Locked)
            {
                Singleton<Helpers.Audio>.Instance.PlayError();
                await DisplayAlert("Upozornění", "Cílová pozice je zablokovaná.", "Zadat jinou pozici");
                await _showPositionDialog(SelectPositionDestDialog_Closed, "Přeskladnění", "Zadejte cílovou pozici");
                return;
            }

            Device.BeginInvokeOnMainThread(async () =>
            {
                var setStockMoveRemarkDlg = new SetStoreMoveRemarkDialog();
                setStockMoveRemarkDlg.SetHeaderText("Přeskladnění", "Zadejte poznámku");
                setStockMoveRemarkDlg.Closed += SetStoreMoveRemarkDialog_Closed;
                await Navigation.PushModalAsync(setStockMoveRemarkDlg, false);
            });
        }

        private async void SetStoreMoveRemarkDialog_Closed(object sender, ButtonsBar.ButtonsEnum e)
        {
            if (e != ButtonsBar.ButtonsEnum.OK)
            {
                // show dest position dialog
                await _showPositionDialog(SelectPositionDestDialog_Closed, "Přeskladnění", "Zadejte cílovou pozici");
                return;
            }

            // validation
            if(_temporary_SelectedItemsForMove.Count < 1)
            {
                Singleton<Helpers.Audio>.Instance.PlayError();
                await DisplayAlert("Upozornění", "Nejsou vybrány položky k přeskladnění", "OK");
                return;
            }

            var remark = (sender as SetStoreMoveRemarkDialog).Remark;

            //client
            var client = new StoreMoveClient();
            var resultSave = await client.MoveSave(new MoveSave
            {
                SourcePositionID = _temporary_PositionSource.PositionID,
                DestPositionID = _temporary_PositionDest.PositionID,
                ChangeCarrier = false,
                DestCarrierNum = string.Empty,
                Articles = _temporary_SelectedItemsForMove,
                Remark = remark
            });

            if(resultSave == null)
            {
                throw new StoreMoveResultMoveSaveIsNullException();
            }

            if (!resultSave.Success)
            {
                Singleton<Helpers.Audio>.Instance.PlayError();
                await DisplayAlert("Upozornění", resultSave.ErrorText, "OK");
                return;
            }

            Device.BeginInvokeOnMainThread(async () =>
            {
                // clear temp values
                _temporary_SelectedItemsForMove = new List<ArticleInfo>();
                _temporary_ItemsForMove = new List<ArticleInfo>();
                _temporary_PositionSource = null;
                _temporary_PositionDest = null;
                _temporary_ArticleCode = null;
                _temporary_ArticleID = null;

                await DisplayAlert("Informace", "Přeskladnění provedeno", "Zavřít");

                await _showNavigationDialog();
            });
        }

        private async void NavigationDialog_Closed(object sender, ButtonsBar.ButtonsEnum e)
        {
            if (e == ButtonsBar.ButtonsEnum.Cancel)
            {
                await Navigation.PopAsync();
                return;
            }

            // clear navigation values
            _temporary_ArticleCode = null;
            _temporary_ArticleID = null;
            _temporary_PositionSource = null;

            if (e == ButtonsBar.ButtonsEnum.OK)
            {
                await _showArticleDialog();
            }
            else
            {
                await _showPositionDialog(SelectPositionSourceDialog_Closed, "Přeskladnění", "Zadejte výchozí pozici");
            }
        }

        private async void GetArticleDialog_Closed(object sender, ButtonsBar.ButtonsEnum e)
        {
            (sender as SelectArticleDialog).ResetForm();

            if (e == ButtonsBar.ButtonsEnum.Cancel)
            {
                await _showNavigationDialog();
                return;
            }
            
            var articleResult = (sender as SelectArticleDialog).GetArticleResult;
            _temporary_ArticleCode = articleResult.ArticlePackingInfo.ArticleCode;
            _temporary_ArticleID = articleResult.ArticlePackingInfo.ArticleID;

            await _showArticleSelectItemsDialog();
        }

        private async void StoreMoveArticleItemsDialog_Closed(object sender, ButtonsBar.ButtonsEnum e)
        {
            if (e == ButtonsBar.ButtonsEnum.Cancel || e == ButtonsBar.ButtonsEnum.None)
            {
                await _showArticleDialog();
                return;
            }

            _temporary_PositionSource = (sender as StoreMoveArticleSelectItemsDialog).SourcePosition;
            var items = (sender as StoreMoveArticleSelectItemsDialog).SelectedItems;

            _temporary_SelectedItemsForMove = new List<ArticleInfo>();
            items.ForEach(item =>
            {
                _temporary_SelectedItemsForMove.Add(new ArticleInfo
                {
                    ArticleID = item.ArticleID,
                    ArtPackID = item.ArtPackID,
                    ArticleCode = item.ArticleCode,
                    ArticleDesc = item.ArticleDesc,
                    PackDesc = item.PackDesc,
                    Quantity = item.Quantity,
                    BatchNum = item.BatchNum,
                    ExpirationDate = item.ExpirationDate,
                    CarrierNum = item.CarrierNum
                });
            });

            await _showPositionDialog(SelectPositionDestDialog_Closed, "Přeskladnění", "Zadejte cílovou pozici");
        }

        private async Task _showPositionDialog(EventHandler<ButtonsBar.ButtonsEnum> onDialogClosedCallback, string headerText, string headerTextSub)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                var selectPositionTargetDlg = new SelectPositionDialog();
                selectPositionTargetDlg.SetHeaderText(headerText, headerTextSub);
                selectPositionTargetDlg.Closed += onDialogClosedCallback;
                await Navigation.PushModalAsync(selectPositionTargetDlg, false);
            });
        }

        private async Task _showPositionSelectItemsDialog()
        {
            //client
            var client = new StoreMoveClient();
            var resultGetPosition = await client.MoveGetPosition(new MoveGetPosition
            {
                PositionID = _temporary_PositionSource.PositionID
            });

            if (!resultGetPosition.Success)
            {
                Singleton<Helpers.Audio>.Instance.PlayError();
                await DisplayAlert("Upozornění", resultGetPosition.ErrorText, "OK");
                return;
            }

            if (resultGetPosition.PositionResult != MoveStatusEnum.OK)
            {
                Singleton<Helpers.Audio>.Instance.PlayError();
                await DisplayAlert("Informace", "Pozice je zablokovaná.", "Zadat jinou pozici");
                await _showPositionDialog(SelectPositionSourceDialog_Closed, "Přeskladnění", "Zadejte výchozí pozici");
                return;
            }

            _temporary_ItemsForMove = resultGetPosition.Articles;

            var selectStockMoveItemsDlg = new StoreMovePositionSelectItemsDialog();
            selectStockMoveItemsDlg.SetHeaderText($"Přeskladnění - {_temporary_PositionSource.PosCode}", "Vybrat položky");
            await selectStockMoveItemsDlg.SetData(_temporary_ItemsForMove);
            selectStockMoveItemsDlg.Closed += StoreMoveSelectPositionItemsDialog_Closed;
            await Navigation.PushModalAsync(selectStockMoveItemsDlg, false);
        }

        private async Task _showNavigationDialog()
        {
            var navigationDlg = new StoreMoveNavigationDialog();
            navigationDlg.SetHeaderText("Přeskladnění", "Vyberte typ hledání");
            navigationDlg.Closed += NavigationDialog_Closed;
            await Navigation.PushModalAsync(navigationDlg, false);
        }

        private async Task _showArticleDialog()
        {
            var getArticleDialog = new SelectArticleDialog();
            getArticleDialog.Closed += GetArticleDialog_Closed;
            getArticleDialog.SetHeaderText("Přeskladnění", "Zadejte kartu");
            await Navigation.PushModalAsync(getArticleDialog, false);
        }

        private async Task _showArticleSelectItemsDialog()
        {
            var storeMoveArticleItemsDlg = new StoreMoveArticleSelectItemsDialog();
            storeMoveArticleItemsDlg.SetHeaderText($"Přeskladnění - {_temporary_ArticleCode}", "Vyberte položky");
            storeMoveArticleItemsDlg.SetData((int) _temporary_ArticleID);
            storeMoveArticleItemsDlg.Closed += StoreMoveArticleItemsDialog_Closed;
            await Navigation.PushModalAsync(storeMoveArticleItemsDlg, false);
        }
    }
}
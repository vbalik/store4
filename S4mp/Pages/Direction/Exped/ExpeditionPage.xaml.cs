﻿using S4.Core;
using S4.Entities.Helpers;
using S4.ProcedureModels.Exped;
using S4mp.Client;
using S4mp.Client.Interfaces;
using S4mp.Controls.Common;
using S4mp.Dialogs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace S4mp.Pages.Direction.Exped
{
    public partial class ExpeditionPage : ContentPage
    {
        private int _temporary_DirectionID;

        public ExpeditionPage()
        {
            InitializeComponent();

            _ShowDocumentDialog();
        }

        private async void _ExpeditionSelectListDialog_Closed(object sender, ButtonsBar.ButtonsEnum e)
        {
            if (e != ButtonsBar.ButtonsEnum.OK)
            {
                await Navigation.PopAsync();
                return;
            }

            _temporary_DirectionID = (sender as SelectDocumentDialog).Document.DocumentID;

            var expeditionTruckDlg = new ExpeditionTruckDialog();
            expeditionTruckDlg.SetHeaderText("Expedice", "Zadejte SPZ");
            expeditionTruckDlg.Closed += _ExpeditionTruckDialog_Closed;
            await Navigation.PushModalAsync(expeditionTruckDlg, false);
        }

        private async void _ExpeditionTruckDialog_Closed(object sender, ButtonsBar.ButtonsEnum e)
        {
            if (e != ButtonsBar.ButtonsEnum.OK)
            {
                await Navigation.PopAsync();
                return;
            }

            var client = DependencyService.Get<IDirectionClient>();
            var result = await client.ExpeditionDone(new ExpedDone
            {
                DirectionID = _temporary_DirectionID,
                ExpedTruck = (sender as ExpeditionTruckDialog).Spz
            });

            if (result == null || !result.Success)
            {
                Singleton<Helpers.Audio>.Instance.PlayError();
                await DisplayAlert("Chyba!", "Nepodařilo se dokončit expedici. =Error=:" + result.ErrorText, "Zavřít");
            }
            else
            {
                await DisplayAlert("Informace", "Expedice dokončena.", "Zavřít");
            }

            await Navigation.PopAsync();
        }

        private async void _ShowDocumentDialog()
        {
            Func<Task<List<DocumentInfo>>> getDataFnc = () => {
                var client = DependencyService.Get<IDirectionClient>();
                var list = client.ExpeditionListDirects();

                return list;
            };

            var expeditionSelectListDlg = new SelectDocumentDialog();
            expeditionSelectListDlg.SetHeaderText("Expedice", "Seznam dokladů");
            expeditionSelectListDlg.SetRefreshData(getDataFnc);
            expeditionSelectListDlg.Closed += _ExpeditionSelectListDialog_Closed;
            await Navigation.PushModalAsync(expeditionSelectListDlg, false);
        }
    }
}
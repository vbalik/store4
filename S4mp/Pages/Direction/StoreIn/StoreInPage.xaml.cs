﻿using S4.Core;
using S4.Entities;
using S4.Entities.Helpers;
using S4.ProcedureModels.StoreIn;
using S4mp.Controls.Common;
using S4mp.Core.Logger;
using S4mp.Dialogs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;
using Xamarin.Forms;
using S4mp.Core;

namespace S4mp.Pages.Direction.StoreIn
{
    public partial class StoreInPage : ContentPage, IPageLoadData
    {
        private StoreInCheckCarrier _temporary_CheckCarrier;
        private int _temporary_LastItemIndex = 0;
        private Controls.OptionsMenu _optionsMenu;

        protected static long _saveCallbackCounter = 1;

        public StoreInPage()
        {
            InitializeComponent();

            _optionsMenu = new Controls.OptionsMenu(this);
            _optionsMenu.FormatTitle("Naskladnění");

            _showDocumentDialog();
        }

        private StoreInVM ViewModel => (StoreInVM)BindingContext;

        public void PageLoadData()
        {
            ViewModel.NextCarrierCmd = new Command(NextCarrier);
            ViewModel.CompletedCmd = new Command(Completed);
        }

        private async void NextCarrier()
        {
            // is carrier was already selected then show next dialog
            if (ViewModel.IsCarrierWasSelected) {
                if (string.IsNullOrEmpty(ViewModel.CarrierNum))
                {
                    // select item
                    await _showItemDialog();
                }
                else
                {
                    // select position
                    await _showSelectPositionDialog();
                }
            }
            else
            {
                // next default dialog
                await _showSelectCarrierNumberDialog();
            }
        }

        private async void Completed(object obj)
        {
            var result = await DependencyService.Get<S4mp.Client.Interfaces.IStoreInClient>().TestDirect(new StoreInTestDirect
            {
                DirectionID = ViewModel.Document.DocumentID
            });

            if (!result.Success)
            {
                Singleton<Helpers.Audio>.Instance.PlayError();
                await DisplayAlert("Upozornění", result.ErrorText, "OK");
                return;
            }

            await Navigation.PopToRootAsync();
        }

        private async void SelectStoreInDocumentDlg_Closed(object sender, ButtonsBar.ButtonsEnum e)
        {
            if (e == ButtonsBar.ButtonsEnum.Cancel)
            {
                await Navigation.PopToRootAsync();
                return;
            }

            var dialog = sender as SelectDocumentDialog;
            ViewModel.Document = dialog.Document;

            // start store in document
            var resultStoreIn = await DependencyService.Get<S4mp.Client.Interfaces.IStoreInClient>().StoreInDocument(new StoreInProc
            {
                DirectionID = dialog.Document.DocumentID
            });

            if (!resultStoreIn.Success)
            {
                Singleton<Helpers.Audio>.Instance.PlayError();
                await DisplayAlert("Upozornění", resultStoreIn.ErrorText, "OK");
                return;
            }

            _optionsMenu.FormatTitle("Naskladnění " + ViewModel.Document.DocumentName);
        }

        #region Next carrier dialogs

        private async void SelectCarrierNumberDlg_Closed(object sender, ButtonsBar.ButtonsEnum e)
        {
            if (e != ButtonsBar.ButtonsEnum.OK)
                return;

            var dialog = sender as SelectCarrierNumberDialog;
            ViewModel.CarrierNum = dialog.CarrierNum;
            
            // next dialog
            if (string.IsNullOrEmpty(ViewModel.CarrierNum))
            {
                // is without carrier

                ViewModel.IsCarrierWasSelected = true;

                // select item
                await _showItemDialog();
            }
            else
            {
                // is whole carrier

                // check carrier
                var resultCheckCarrier = await DependencyService.Get<S4mp.Client.Interfaces.IStoreInClient>().CheckCarrier(new StoreInCheckCarrier
                {
                    DirectionID = ViewModel.Document.DocumentID,
                    CarrierNum = ViewModel.CarrierNum,
                    SrcPositionID = (int)SettingsDictionary.ByPath(SettingsDictionary.RECEIVE_POSITION_ID).DefaultValue
                });

                if (!resultCheckCarrier.Success)
                {
                    Singleton<Helpers.Audio>.Instance.PlayError();
                    await DisplayAlert("Upozornění", resultCheckCarrier.ErrorText, "OK");

                    // repeat dialog
                    await _showSelectCarrierNumberDialog();

                    return;
                }
                else if (resultCheckCarrier.Success && !resultCheckCarrier.IsCarrierFound)
                {
                    Singleton<Helpers.Audio>.Instance.PlayError();
                    await DisplayAlert("Upozornění", $"Paleta {ViewModel.CarrierNum} nebyla nalezena.", "OK");

                    // repeat dialog
                    await _showSelectCarrierNumberDialog();

                    return;
                }

                _temporary_CheckCarrier = resultCheckCarrier;

                // select position
                await _showSelectPositionDialog();
            }
        }

        private async void SelectStoreInItemDlg_Closed(object sender, ButtonsBar.ButtonsEnum e)
        {
            if (e != ButtonsBar.ButtonsEnum.OK)
            {
                if(!ViewModel.IsCarrierWasSelected)
                    await _showSelectCarrierNumberDialog();
                return;
            }

            var dialog = sender as SelectStoreInItemDialog;
            ViewModel.Item = dialog.Item;
            _temporary_LastItemIndex = dialog.LastPositionIndex > 0 ? dialog.LastPositionIndex : 0;

            // next dialog
            await _showSelectPositionDialog();
        }

        private async void SelectPositionDlg_Closed(object sender, ButtonsBar.ButtonsEnum e)
        {
            if (e == ButtonsBar.ButtonsEnum.Cancel)
            {
                await _showItemDialog();
                return;
            }

            // log
            _saveCallbackCounter = _GetSaveCallbackId();

            var dialog = sender as SelectPositionDialog;
            ViewModel.Position = dialog.Position;

            var checkPos = new StoreInCheckPos
            {
                PositionID = ViewModel.Position.PositionID,
                ArtPackID = (_temporary_CheckCarrier == null) ? ViewModel.Item.ArtPackID : _temporary_CheckCarrier.ArticleList[0].ArtPackID
            };

            // log
            await DependencyService.Get<ICustomEventLogger>().LogInfo($"{nameof(StoreInCheckPos)} -- start", checkPos);

            // check position
            var resultCheckPos = await DependencyService.Get<S4mp.Client.Interfaces.IStoreInClient>().CheckPos(checkPos);

            // log
            await DependencyService.Get<ICustomEventLogger>().LogInfo($"{nameof(StoreInCheckPos)} -- done", resultCheckPos);

            if (!resultCheckPos.Success)
            {
                Singleton<Helpers.Audio>.Instance.PlayError();
                await DisplayAlert("Upozornění", resultCheckPos.ErrorText, "OK");

                // repeat dialog
                await _showSelectPositionDialog();

                return;
            }

            switch (resultCheckPos.Enabled)
            {
                case StoreInCheckPos.PosEnabled.No:
                    Singleton<Helpers.Audio>.Instance.PlayError();
                    await DisplayAlert("Upozornění", resultCheckPos.ResReason, "OK");
                    await _showSelectPositionDialog();
                    return;
                case StoreInCheckPos.PosEnabled.Warn:
                    Singleton<Helpers.Audio>.Instance.PlayError();
                    if (!await DisplayAlert("Varování", resultCheckPos.ResReason, "Pokračovat", "Zavřít"))
                    {
                        await _showSelectPositionDialog();
                        return;
                    }
                    break;
                case StoreInCheckPos.PosEnabled.Yes:
                    // continue
                    break;
            }

            if (!string.IsNullOrEmpty(ViewModel.CarrierNum))
            {
                // is whole carrier


                var acceptPos = new StoreInAcceptPos
                {
                    CarrierNum = ViewModel.CarrierNum,
                    DirectionID = ViewModel.Document.DocumentID,
                    SrcPositionID = null,
                    DestPositionID = ViewModel.Position.PositionID
                };

                // log
                await DependencyService.Get<ICustomEventLogger>().LogInfo($"{nameof(StoreInAcceptPos)} -- start", acceptPos);
                await DependencyService.Get<ICustomEventLogger>().LogInfo($"{nameof(StoreInAcceptPos)}, callback ID", _saveCallbackCounter);

                // client
                var resultAcceptPos = await DependencyService.Get<S4mp.Client.Interfaces.IStoreInClient>().AcceptPos(acceptPos);

                // log
                await DependencyService.Get<ICustomEventLogger>().LogInfo($"{nameof(StoreInAcceptPos)} -- done", resultAcceptPos);

                if (!resultAcceptPos.Success)
                {
                    Singleton<Helpers.Audio>.Instance.PlayError();
                    await DisplayAlert("Upozornění", resultAcceptPos.ErrorText, "OK");
                    return;
                }

                ViewModel.StoMoveID = resultAcceptPos.StoMoveID;

                ViewModel.IsCarrierWasSelected = false;

                await _DoStoreIn();
            }
            else
            {

                // is item continue


                var acceptPosNoCarr = new StoreInAcceptPosNoCarr
                {
                    Article = new S4.Entities.Helpers.ArticleInfo
                    {
                        ArtPackID = ViewModel.Item.ArtPackID,
                        BatchNum = ViewModel.Item.BatchNum,
                        CarrierNum = ViewModel.Item.CarrierNum,
                        ExpirationDate = ViewModel.Item.ExpirationDate,
                        Quantity = ViewModel.Item.Quantity
                    },
                    DirectionID = ViewModel.Document.DocumentID,
                    SrcPositionID = null,
                    DestPositionID = ViewModel.Position.PositionID
                };

                // log
                await DependencyService.Get<ICustomEventLogger>().LogInfo($"{nameof(StoreInAcceptPosNoCarr)} -- start", acceptPosNoCarr);
                await DependencyService.Get<ICustomEventLogger>().LogInfo($"{nameof(StoreInAcceptPosNoCarr)}, callback ID", _saveCallbackCounter);

                // client
                var resultAcceptPosNoCarr = await DependencyService.Get<S4mp.Client.Interfaces.IStoreInClient>().AcceptPosNoCarr(acceptPosNoCarr);

                // log
                await DependencyService.Get<ICustomEventLogger>().LogInfo($"{nameof(StoreInAcceptPosNoCarr)} -- done", resultAcceptPosNoCarr);

                if (!resultAcceptPosNoCarr.Success)
                {
                    Singleton<Helpers.Audio>.Instance.PlayError();
                    await DisplayAlert("Upozornění", resultAcceptPosNoCarr.ErrorText, "OK");
                    return;
                }

                ViewModel.StoMoveID = resultAcceptPosNoCarr.StoMoveID;

                // next dialog
                var selectCarrierNumberDlg = new SelectCarrierNumberDialog();
                selectCarrierNumberDlg.SetHeaderText("Naskladnění", "Cílová paleta");
                selectCarrierNumberDlg.Closed += SelectDestCarrierNumberDlg_Closed;

                await Navigation.PushModalAsync(selectCarrierNumberDlg, false);
            }
        }

        private async void SelectDestCarrierNumberDlg_Closed(object sender, ButtonsBar.ButtonsEnum e)
        {
            if (e == ButtonsBar.ButtonsEnum.Cancel)
                return;

            var dialog = sender as SelectCarrierNumberDialog;
            ViewModel.CarrierNum = dialog.CarrierNum;

            await _DoStoreIn();
        }

        #endregion

        private async Task _DoStoreIn()
        {
            var itemToSave = new StoreInSaveItem
            {
                StoMoveID = ViewModel.StoMoveID,
                FinalPositionID = ViewModel.Position.PositionID,
                FinalCarrierNum = ViewModel.CarrierNum
            };

            // log
            await DependencyService.Get<ICustomEventLogger>().LogInfo($"{nameof(StoreInSaveItem)} -- start", itemToSave);

            var resultSaveItem = await DependencyService.Get<S4mp.Client.Interfaces.IStoreInClient>().SaveItem(itemToSave);

            // log
            await DependencyService.Get<ICustomEventLogger>().LogInfo($"{nameof(StoreInSaveItem)} -- done", resultSaveItem);

            if (!resultSaveItem.Success)
            {
                Singleton<Helpers.Audio>.Instance.PlayError();
                await DisplayAlert("Upozornění", resultSaveItem.ErrorText, "OK");
                return;
            }

            await DisplayAlert("Informace", $"Položka byla naskladněna na pozici {ViewModel.Position.PosCode}", "Zavřít");

            // clear vm data
            ViewModel.ClearData();

            NextCarrier();
        }

        private async Task _showSelectPositionDialog()
        {
            var selectPositionDlg = new SelectPositionDialog();
            selectPositionDlg.SetHeaderText("Naskladnění", "Cílová pozice");
            selectPositionDlg.Closed += SelectPositionDlg_Closed;
            await Navigation.PushModalAsync(selectPositionDlg, false);
        }

        private async Task _showSelectCarrierNumberDialog()
        {
            var selectCarrierNumberDlg = new SelectCarrierNumberDialog();
            selectCarrierNumberDlg.SetHeaderText("Naskladnění", "Zadejte paletu");
            selectCarrierNumberDlg.Closed += SelectCarrierNumberDlg_Closed;

            await Navigation.PushModalAsync(selectCarrierNumberDlg, false);
        }

        private async Task _showItemDialog()
        {
            var storeInItemDlg = new SelectStoreInItemDialog();
            storeInItemDlg.SetHeaderText("Naskladnění", "Vybrat položku");
            await storeInItemDlg.SetData(ViewModel.Document.DocumentID, (int) SettingsDictionary.ByPath(SettingsDictionary.RECEIVE_POSITION_ID).DefaultValue, _temporary_LastItemIndex);
            storeInItemDlg.Closed += SelectStoreInItemDlg_Closed;
            await Navigation.PushModalAsync(storeInItemDlg, false);
        }

        private async void _showDocumentDialog()
        {
            var client = DependencyService.Get<S4mp.Client.Interfaces.IStoreInClient>();

            Func<Task<List<DocumentInfo>>> getDataFnc = async() => {
                var list = await client.GetListDirects(new StoreInListDirects
                {
                    WorkerID = Singleton<BaseInfo>.Instance.CurrentWorkerID
                });

                return list.Directions;
            };

            var selectStoreInDirectionDlg = new SelectDocumentDialog();
            selectStoreInDirectionDlg.SetHeaderText("Naskladnění", "Seznam dokladů");
            selectStoreInDirectionDlg.ShowDocumentDetail = false;
            selectStoreInDirectionDlg.SetRefreshData(getDataFnc);
            selectStoreInDirectionDlg.Closed += SelectStoreInDocumentDlg_Closed;

            await Navigation.PushModalAsync(selectStoreInDirectionDlg, false);
        }

        internal static long _GetSaveCallbackId()
        {
            return Interlocked.Increment(ref _saveCallbackCounter);
        }
    }
}
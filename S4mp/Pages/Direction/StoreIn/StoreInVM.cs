﻿using S4.Entities;
using S4.Entities.Helpers;
using S4mp.Core.ViewModel;
using System.Windows.Input;

namespace S4mp.Pages.Direction.StoreIn
{
    class StoreInVM : BaseViewModel
    {
        #region expose through the view

        private ICommand _nextCarrierCmd;
        public ICommand NextCarrierCmd
        {
            get => _nextCarrierCmd;
            set => SetField(ref _nextCarrierCmd, value);
        }

        private ICommand _completedCmd;
        public ICommand CompletedCmd
        {
            get => _completedCmd;
            set => SetField(ref _completedCmd, value);
        }

        #endregion

        private DocumentInfo _document;
        public DocumentInfo Document
        {
            get => _document;
            set => SetField(ref _document, value);
        }

        private bool _isCarrierWasSelected;
        public bool IsCarrierWasSelected
        {
            get => _isCarrierWasSelected;
            set => SetField(ref _isCarrierWasSelected, value);
        }

        private string _carrierNum;
        public string CarrierNum
        {
            get => _carrierNum ?? string.Empty;
            set
            {
                SetField(ref _carrierNum, value);
                CarrierNumText = value;
            }
        }

        private string _carrierNumText;
        public string CarrierNumText
        {
            get => _carrierNumText;
            set
            {
                var val = !string.IsNullOrEmpty(value)
                    ? value
                    : (value == string.Empty
                        ? "bez palety"
                        : null);
                SetField(ref _carrierNumText, val);
            }
        }

        private Position _position;
        public Position Position
        {
            get => _position;
            set => SetField(ref _position, value);
        }

        private ItemInfo _item;
        public ItemInfo Item
        {
            get => _item;
            set => SetField(ref _item, value);
        }

        private int _stoMoveID;
        public int StoMoveID
        {
            get => _stoMoveID;
            set => SetField(ref _stoMoveID, value);
        }

        /// <summary>
        /// Clear data after item store in
        /// </summary>
        public void ClearData()
        {
            IsCarrierWasSelected = false;
            CarrierNum = null;
            CarrierNumText = null;
            Position = null;
            Item = null;
            StoMoveID = default;
        }
    }
}

﻿using S4.Core;
using S4.Core.EAN;
using S4.Entities;
using S4.Entities.Helpers;
using S4.ProcedureModels.Dispatch;
using S4mp.Client;
using S4mp.Controls.Common;
using S4mp.Core;
using S4mp.Core.EANCode;
using S4mp.Dialogs;
using S4mp.Exceptions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace S4mp.Pages.Direction.Dispatch
{
    public partial class DispatchPage : ContentPage, IPageLoadData
    {
        public static readonly BindableProperty LoadUpItemsCmdProperty = BindableProperty.Create(nameof(LoadUpItemsCmd), typeof(ICommand), typeof(DispatchPage));
        public ICommand LoadUpItemsCmd
        {
            get => (ICommand) GetValue(LoadUpItemsCmdProperty);
            set => SetValue(LoadUpItemsCmdProperty, value);
        }

        public static readonly BindableProperty UnLoadItemsCmdProperty = BindableProperty.Create(nameof(UnLoadItemsCmd), typeof(ICommand), typeof(DispatchPage));
        public ICommand UnLoadItemsCmd
        {
            get => (ICommand) GetValue(UnLoadItemsCmdProperty);
            set => SetValue(UnLoadItemsCmdProperty, value);
        }

        public static readonly BindableProperty DocumentDoneCmdProperty = BindableProperty.Create(nameof(DocumentDoneCmd), typeof(ICommand), typeof(DispatchPage));
        public ICommand DocumentDoneCmd
        {
            get => (ICommand) GetValue(DocumentDoneCmdProperty);
            set => SetValue(DocumentDoneCmdProperty, value);
        }

        public static readonly BindableProperty ItemsLeftCountProperty = BindableProperty.Create(nameof(ItemsLeftCount), typeof(int), typeof(DispatchPage));
        public int ItemsLeftCount
        {
            get => (int) GetValue(ItemsLeftCountProperty);
            set => SetValue(ItemsLeftCountProperty, value);
        }

        public static readonly BindableProperty ItemActionMenuCmdProperty = BindableProperty.Create(nameof(ItemActionMenuCmd), typeof(ICommand), typeof(DispatchPage));
        public ICommand ItemActionMenuCmd
        {
            get => (ICommand) GetValue(ItemActionMenuCmdProperty);
            set => SetValue(ItemActionMenuCmdProperty, value);
        }

        public static readonly BindableProperty CarrierNumTextProperty = BindableProperty.Create(nameof(CarrierNumText), typeof(string), typeof(DispatchPage));
        public string CarrierNumText
        {
            get => (string) GetValue(CarrierNumTextProperty);
            set
            {
                var val = !string.IsNullOrEmpty(value) && value != StoreMoveItem.EMPTY_CARRIERNUM
                    ? value
                    : (value == string.Empty || value == StoreMoveItem.EMPTY_CARRIERNUM
                        ? "bez palety"
                        : null);
                SetValue(CarrierNumTextProperty, val);
            }
        }

        public bool IsBtnUnLoadEnabled = false;
        public bool SkipItemsDialogOnPageInit = true;

        private DocumentInfo _direction;
        private DocumentInfo _temporary_Direction
        {
            get => _direction;
            set
            {
                _direction = value;
                _optionsMenu.FormatTitle("Vychystání", value?.DocumentName);
            }
        }
        private bool _temporary_SetWeightBoxCountData { get; set; } = false;
        private List<ItemInfo> _temporary_Items { get; set; } = new List<ItemInfo>();
        private ItemInfo _temporary_Item { get; set; }
        private DispatchItemInstruct _temporary_ItemInstruction { get; set; }
        private Position _temporary_DestPosition { get; set; }
        private bool _temporary_showConfirmDestPositionDlg { get; set; } = false;
        private bool _temporary_IsTransportCarrierNumberSelected = false;

        private string _transportCarrierNumber;
        private int _temporary_LastItemIndex = 0;
        private string _temporary_TransportCarrierNumber
        {
            get => _transportCarrierNumber;
            set
            {
                _transportCarrierNumber = value;
                CarrierNumText = value;
                if (value == null)
                    _temporary_IsTransportCarrierNumberSelected = false;
            }
        }

        private ObservableCollection<ItemInfo> _loaded_Items = new ObservableCollection<ItemInfo>();
        private Controls.OptionsMenu _optionsMenu;

        private ItemInfo _temporary_printLabelItem;

        // ARGOMED ONLY
        private bool _argomedFeatures = false;

        public DispatchPage()
        {
            InitializeComponent();

            _optionsMenu = new Controls.OptionsMenu(this);
            _optionsMenu.FormatTitle("Vychystání");

            loadedItemListView.ItemsSource = _loaded_Items;

            LoadUpItemsCmd = new Command(LoadUpItems);
            UnLoadItemsCmd = new Command(UnLoadItems);
            DocumentDoneCmd = new Command(DocumentDone);

            ItemActionMenuCmd = new Command<ItemInfo>(ItemActionMenu);
            
        }

        public void PageLoadData()
        {
            _temporary_Direction = null;
            _temporary_Item = null;
            _temporary_ItemInstruction = null;
            _temporary_Items.Clear();
            _temporary_TransportCarrierNumber = null;
            _temporary_IsTransportCarrierNumberSelected = false;
            IsBtnUnLoadEnabled = false;

            // [Argomed Only]
            // they don't use carrier number on dispatch action
            if (bool.TryParse(Singleton<S4mp.Settings.LocalSettings>.Instance[Core.Consts.XMLPATH_ARGOMED_FEATURES], out _argomedFeatures) && _argomedFeatures)
            {
                // set transport carrier number
                _temporary_IsTransportCarrierNumberSelected = true;
                _temporary_TransportCarrierNumber = StoreMoveItem.EMPTY_CARRIERNUM;
            }

            _showSelectDocumentDialog().Wait();
        }

        private async void LoadUpItems()
        {
            await _showSelectItemsDialog();
        }

        private async void UnLoadItems()
        {
            if (!IsBtnUnLoadEnabled) return;
            if (_temporary_IsTransportCarrierNumberSelected)
            {
                await _showConfirmDestPositionDialog();
            }
            else
            {
                _temporary_showConfirmDestPositionDlg = true;
                await _showSelectCarrierNumberDialog();
            }
        }

        private async void DocumentDone()
        {
            // any items unloaded dont test doc
            if (_loaded_Items.Count > 0)
            {
                await Navigation.PopToRootAsync();
                return;
            }

            // Client
            var client = DependencyService.Get<S4mp.Client.Interfaces.IDispatchClient>();
            var result = await client.TestDirect(new DispatchTestDirect
            {
                DirectionID = _temporary_Direction.DocumentID
            });

            if (!result.Success)
            {
                Singleton<Helpers.Audio>.Instance.PlayError();
                await DisplayAlert("Upozornění", result.ErrorText, "OK");

                return;
            }


            if (result.IsCompleted)
            {
                Func<Task> doPrintFncAsync = async () =>
                {
                    var resultPrint = await client.DispatchPrint(new DispatchPrint
                    {
                        DirectionID = _temporary_Direction.DocumentID,
                        PrinterByPosition = true
                    });

                    if (!resultPrint.Success)
                    {
                        Singleton<Helpers.Audio>.Instance.PlayError();
                        await DisplayAlert("Upozornění", resultPrint.ErrorText, "OK");
                    }

                    await Task.CompletedTask;
                };

                // if direction print is mandatory then print it everytime
                if (result.IsMandatoryDirectionPrint)
                {
                    await DisplayAlert("Informace", "Doklad dokončen", "Ok");
                    await DisplayAlert("Důležité", "Tisk šaržového listu", "Tisk");
                    await doPrintFncAsync();
                }
                else if (await DisplayAlert("Informace", "Doklad dokončen. Tisknout šaržový list?", "Tisk", "Zrušit"))
                {
                    await doPrintFncAsync();
                }

                // show weight box count dialog from settings
                if (_temporary_SetWeightBoxCountData)
                {
                    await _showDispatchSetWeightBoxCountDialog();
                    return; // stop redirect
                }
            }

            await Navigation.PopToRootAsync();
        }

        private async void _DispatchSelectDocumentDialog_Closed(object sender, ButtonsBar.ButtonsEnum e)
        {
            if (e != ButtonsBar.ButtonsEnum.OK)
            {
                await Navigation.PopToRootAsync();
                return;
            }

            _temporary_Direction = ((SelectDocumentDialog) sender).Document;
            _temporary_Items.Clear();

            // set if it should open weight box / count dialog 
            // Client
            var client = DependencyService.Get<S4mp.Client.Interfaces.IDispatchClient>();
            var canSetWeightBoxCountData = await client.GetSetWeightBoxCountDataSetting();
            _temporary_SetWeightBoxCountData = canSetWeightBoxCountData;

            await _showPrintLabelsDialog();
        }

        private async void _DispatchSelectItemsDialog_Closed(object sender, ButtonsBar.ButtonsEnum e)
        {
            if (e != ButtonsBar.ButtonsEnum.OK)
                return;

            var dialog = sender as DispatchSelectItemDialog;
            _temporary_Item = dialog.Item;
            _temporary_LastItemIndex = dialog.LastPositionIndex > 0 ? dialog.LastPositionIndex : 0;

            await _showItemInstructionDialog();
        }

        private async void _DispatchShowInstructionDialog_Closed(object sender, ButtonsBar.ButtonsEnum e)
        {
            _temporary_ItemInstruction = (sender as DispatchShowInstructionDialog).ItemInstruct;

            // button cancel
            if (e == ButtonsBar.ButtonsEnum.Cancel)
            {
                await _showSelectItemsDialog();
                return;
            }

            // button no
            if (e == ButtonsBar.ButtonsEnum.No)
            {
                var badPositionReasonDlg = new DispatchSelectBadPositionReasonDialog();
                badPositionReasonDlg.SetHeaderText("Vychystání", "Špatná pozice - důvod");
                badPositionReasonDlg.Closed += _DispatchBadPositionReasonDialog_Closed;
                await Navigation.PushModalAsync(badPositionReasonDlg, false);

                return;
            }

            // set destination position
            if (_temporary_DestPosition == null)
                _temporary_DestPosition = _temporary_ItemInstruction.DestPosition;

            await _showConfirmSourcePositionDialog();
        }

        private async void _DispatchBadPositionReasonDialog_Closed(object sender, ButtonsBar.ButtonsEnum e)
        {
            if (e != ButtonsBar.ButtonsEnum.OK)
            {
                await _showItemInstructionDialog();
                return;
            }

            var reason = (sender as DispatchSelectBadPositionReasonDialog).Reason;

            // Client
            var client = DependencyService.Get<S4mp.Client.Interfaces.IDispatchClient>();
            var resultPosError = await client.PosError(new DispatchPosError
            {
                StoMoveID = _temporary_Direction.StoMoveID,
                PositionID = _temporary_ItemInstruction.ScrPosition.PositionID,
                Reason = reason
            });

            if (!resultPosError.Success)
            {
                Singleton<Helpers.Audio>.Instance.PlayError();
                await DisplayAlert("Upozornění", resultPosError.ErrorText, "OK");

                return;
            }

            // clear temp
            _temporary_Item = null;
            _temporary_ItemInstruction = null;

            await DisplayAlert("Informace", "Pozice byla zneplatněna", "OK");

            // show items again
            await _showSelectItemsDialog();
        }

        private async void _DispatchConfirmSrcPositionDialog_Closed(object sender, ButtonsBar.ButtonsEnum e)
        {
            if (e != ButtonsBar.ButtonsEnum.OK)
            {
                await _showItemInstructionDialog();
                return;
            }

            await _showSelectArticleDialog();
        }

        private async void _DispatchConfirmArticleDialog_Closed(object sender, ButtonsBar.ButtonsEnum e)
        {
            if (e != ButtonsBar.ButtonsEnum.OK)
            {
                await _showSelectArticleDialog();
                return;
            }

            // Client
            var client = DependencyService.Get<S4mp.Client.Interfaces.IDispatchClient>();
            var resultConfirmItem = await client.ConfirmItem(new DispatchConfirmItem
            {
                StoMoveID = _temporary_Direction.StoMoveID,
                StoMoveItemID = _temporary_Item.StoMoveItemID
            });

            if (resultConfirmItem == null)
            {
                throw new DispatchResultConfirmItemIsNullException(_temporary_Direction.StoMoveID, _temporary_Item.StoMoveItemID, _temporary_Direction.DocumentID);
            }

            if (!resultConfirmItem.Success)
            {
                Singleton<Helpers.Audio>.Instance.PlayError();
                await DisplayAlert("Upozornění", resultConfirmItem.ErrorText, "OK");

                return;
            }

            // load up item
            if (_temporary_Item.WholeCarrier && !string.IsNullOrEmpty(_temporary_Item.CarrierNum))
            {
                _temporary_IsTransportCarrierNumberSelected = true;
                _temporary_TransportCarrierNumber = _temporary_Item.CarrierNum;
                await _loadUpItem();
                return;
            }
            else if (!_temporary_Item.WholeCarrier && _temporary_IsTransportCarrierNumberSelected)
            {
                await _loadUpItem();
                return;
            }

            // select transport carrier
            await _showSelectCarrierNumberDialog();
        }

        private async void _DispatchTransportCarrierNumberDialog_Closed(object sender, ButtonsBar.ButtonsEnum e)
        {
            if (e != ButtonsBar.ButtonsEnum.OK)
            {
                _temporary_showConfirmDestPositionDlg = false;
                await _showSelectArticleDialog();
                return;
            }

            // set transport carrier number
            _temporary_IsTransportCarrierNumberSelected = true;

            var carrierNum = (sender as SelectCarrierNumberDialog).CarrierNum;
            _temporary_TransportCarrierNumber = string.IsNullOrWhiteSpace(carrierNum) ? StoreMoveItem.EMPTY_CARRIERNUM : carrierNum;

            // transport carrier was not selected before open the destination position dialog again
            if (_temporary_showConfirmDestPositionDlg)
            {
                await _showConfirmDestPositionDialog();
                return;
            }

            // or load up items
            await _loadUpItem();
        }

        private async void _DispatchConfirmDestPositionDialog_Closed(object sender, ButtonsBar.ButtonsEnum e)
        {
            if (e != ButtonsBar.ButtonsEnum.OK)
                return;

            if (_temporary_Direction == null || _temporary_DestPosition == null)
                return;

            var destPosition = (sender as SelectPositionDialog).Position;

            if (_temporary_DestPosition.PositionID != destPosition?.PositionID)
            {
                Singleton<Helpers.Audio>.Instance.PlayError();
                await DisplayAlert("Upozornění", "Cílová pozice není shodná", "OK");

                await _showConfirmDestPositionDialog();
                return;
            }

            var client = DependencyService.Get<S4mp.Client.Interfaces.IDispatchClient>();

            foreach (var item in _loaded_Items)
            {
                // Client
                var resultSaveItem = await client.SaveItem(new DispatchSaveItem
                {
                    StoMoveID = _temporary_Direction.StoMoveID,
                    StoMoveItemID = item.StoMoveItemID,
                    DestCarrierNum = _temporary_TransportCarrierNumber
                });

                if (!resultSaveItem.Success)
                {
                    Singleton<Helpers.Audio>.Instance.PlayError();
                    await DisplayAlert("Upozornění", resultSaveItem.ErrorText, "OK");
                }
            }

            _loaded_Items.Clear();
            IsBtnUnLoadEnabled = false;
            _temporary_TransportCarrierNumber = null;
            _temporary_showConfirmDestPositionDlg = false;

            await DisplayAlert("Informace", "Položky byly vyloženy", "OK");

            // Client
            var result = await client.GetListItems(new DispatchShowItems
            {
                StoMoveID = _temporary_Direction.StoMoveID
            });

            // any items remain then continue in load up
            if (result.Items.Count > 0)
            {
                await _showSelectItemsDialog();
                return;
            }
        }

        private async void _DispatchPrintLabelsDialog_Closed(object sender, ButtonsBar.ButtonsEnum e)
        {
            if (e != ButtonsBar.ButtonsEnum.OK)
            {
                await _showSelectItemsDialog();
                return;
            }

            // do print labels
            var dlg = (sender as DispatchPrintLabelsDialog);
            var printerLocationID = dlg.Control.PrinterLocationID;
            var reportID = dlg.Control.PrinterReportID;
            var boxLabels = dlg.Control.BoxLabels;

            var client = DependencyService.Get<S4mp.Client.Interfaces.IDispatchClient>();

            foreach (var label in boxLabels)
            {
                if (label.LabelQuantity == 0) continue;

                await client.PrintBoxLabel(new DispatchPrintBoxLabel
                {
                    LabelText = label.LabelText,
                    LabelsQuant = label.LabelQuantity,
                    DirectionID = _temporary_Direction.DocumentID,
                    DocPosition = label.DocPosition,
                    PrinterLocationID = (int) printerLocationID,
                    ReportID = reportID ?? 0
                });
            }

            await _showSelectItemsDialog();
        }

        private async void _DispatchSelectArticleDialog_Closed(object sender, ButtonsBar.ButtonsEnum e)
        {
            if (e == ButtonsBar.ButtonsEnum.OK)
            {
                var articleResult = ((SelectArticleDialog) sender).GetArticleResult;

                if (articleResult == null || articleResult.ArticlePackingInfo?.ArticleID != _temporary_Item.ArticleID)
                {
                    Singleton<Helpers.Audio>.Instance.PlayError();
                    await DisplayAlert("Upozornění", $"Kód položky nesouhlasí.", "OK");

                    await _showSelectArticleDialog();
                    return;
                }

                if (_temporary_ItemInstruction.Item.UseBatch
                    && articleResult.EAN != null
                    && articleResult.EAN[EANCodePartEnum.ExpirationDate] != null
                    && (_temporary_ItemInstruction.Item.BatchNum != articleResult.EAN[EANCodePartEnum.BatchNumber]?.Value
                        && _temporary_ItemInstruction.Item.BatchNum != articleResult.EAN[EANCodePartEnum.ShippingCode]?.Value))
                {
                    Singleton<Helpers.Audio>.Instance.PlayError();
                    await DisplayAlert("Upozornění", $"Šarže položky nesouhlasí.", "OK");

                    await _showSelectArticleDialog();
                    return;
                }

                // show next dialog
                var confirmArticleDlg = new DispatchConfirmArticleDialog();
                confirmArticleDlg.SetHeaderText("Vychystání", "Ověření karty");
                confirmArticleDlg.SetData(_temporary_Item, articleResult);
                confirmArticleDlg.Closed += _DispatchConfirmArticleDialog_Closed;
                await Navigation.PushModalAsync(confirmArticleDlg, false);
            }
            else
            {
                await _showConfirmSourcePositionDialog();
            }
        }

        private async void _DispatchSetWeightBoxCountDialog_Closed(object sender, ButtonsBar.ButtonsEnum e)
        {
            if (e != ButtonsBar.ButtonsEnum.OK)
            {
                await Navigation.PopToRootAsync();
                return;
            }

            var data = (DispatchSetWeightBoxCountDialog) sender;

            var values = new List<DispatchSetXtraValue>
            {
                new DispatchSetXtraValue { Key = S4.Entities.Direction.XTRA_DISPATCH_WEIGHT, Value = data.Control.Weight },
                new DispatchSetXtraValue { Key = S4.Entities.Direction.XTRA_DISPATCH_BOXES_CNT, Value = data.Control.BoxesCount },
            };

            var client = DependencyService.Get<S4mp.Client.Interfaces.IDispatchClient>();
            var result = await client.SetXtraValues(new DispatchSetXtraValues
            {
                DirectionID = _temporary_Direction.DocumentID,
                Values = values
            });

            if (!result.Success)
            {
                Singleton<Helpers.Audio>.Instance.PlayError();
                await DisplayAlert("Upozornění", result.ErrorText, "OK");
                return;
            }

            await Navigation.PopToRootAsync();
        }

        private async Task _showPrintLabelsDialog()
        {
            var printLabelsDlg = new DispatchPrintLabelsDialog();
            printLabelsDlg.SetHeaderText("Vychystání", "Tisk štítků");
            await printLabelsDlg.SetData(_temporary_Direction.DocumentID);
            printLabelsDlg.Closed += _DispatchPrintLabelsDialog_Closed;
            await Navigation.PushModalAsync(printLabelsDlg, false);
        }

        private async Task _showSelectItemsDialog()
        {
            if (_temporary_Items.Count == 0)
            {
                // Client
                var client = DependencyService.Get<S4mp.Client.Interfaces.IDispatchClient>();
                var result = await client.GetListItems(new DispatchShowItems
                {
                    StoMoveID = _temporary_Direction.StoMoveID
                });

                _temporary_Items = result.Items;
                ItemsLeftCount = _temporary_Items.Count;
                _temporary_DestPosition = result.DestPosition;

                // any items are loaded get dest position
                if (result.DoneItems.Count > 0)
                {
                    _loaded_Items.Clear();
                    result.DoneItems.ForEach(item => _loaded_Items.Add(item));

                    IsBtnUnLoadEnabled = true;
                }

                // don't show items dialog if no items left
                if (SkipItemsDialogOnPageInit && _temporary_Items.Count == 0)
                {
                    SkipItemsDialogOnPageInit = false;
                    return;
                }
            }

            var dispatchSelectItemDlg = new DispatchSelectItemDialog();
            dispatchSelectItemDlg.SetHeaderText("Vychystání", "Vybrat položku");
            await dispatchSelectItemDlg.SetData(_temporary_Items, _temporary_LastItemIndex);
            dispatchSelectItemDlg.Closed += _DispatchSelectItemsDialog_Closed;
            await Navigation.PushModalAsync(dispatchSelectItemDlg, false);
        }

        private async Task _showSelectDocumentDialog()
        {
            Func<Task<List<DocumentInfo>>> getDataFnc = () =>
            {
                var client = DependencyService.Get<S4mp.Client.Interfaces.IDispatchClient>();
                var list = client.GetListDirections(new DispatchListDirections
                {
                    WorkerID = Singleton<BaseInfo>.Instance.CurrentWorkerID
                });

                return list;
            };

            Func<string, Task<List<DocumentInfo>>> onScanExecFnc = async (string docID) =>
            {
                var client = DependencyService.Get<S4mp.Client.Interfaces.IDispatchClient>();
                var result = await client.FindDirectionsByDocNumber(new DispatchAvailableDirectionsDocNumber
                {
                    DocumentID = docID
                });

                var directions = new List<DocumentInfo>();

                if (result.Directions.Count > 0)
                {
                    foreach (var direction in result.Directions)
                    {
                        // assign dispatch to user
                        var resAssignToUser = await DependencyService.Get<Client.Interfaces.IDispatchClient>().AssignDispatchToUser(new DispatchWait
                        {
                            WorkerID = Singleton<BaseInfo>.Instance.CurrentWorkerID,
                            DirectionID = direction.DocumentID,
                            DestPositionID = int.Parse(Singleton<S4mp.Settings.LocalSettings>.Instance[Core.Consts.XMLPATH_DISPATCH_DEFAULT_POSITION_ID])
                        });

                        if (resAssignToUser.Success)
                            directions.Add(direction);
                    }
                }

                return directions;
            };

            // enable assign document to user scan
            bool.TryParse(Singleton<S4mp.Settings.LocalSettings>.Instance[Core.Consts.XMLPATH_DISPATCH_SCANNER_ASSIGN_USER] ?? "False", out var enableDocumentScan);

            var dispatchSelectDocumentDlg = new SelectDocumentDialog();
            dispatchSelectDocumentDlg.SetHeaderText("Vychystání", "Seznam dokladů");
            dispatchSelectDocumentDlg.SetRefreshData(getDataFnc);
            dispatchSelectDocumentDlg.EnableDocumentScan(enableDocumentScan, onScanExecFnc);
            dispatchSelectDocumentDlg.Closed += _DispatchSelectDocumentDialog_Closed;
            await Navigation.PushModalAsync(dispatchSelectDocumentDlg, false);
        }

        private async Task _showItemInstructionDialog()
        {
            var instructionDlg = new DispatchShowInstructionDialog();
            instructionDlg.SetHeaderText("Vychystání", "Instrukce");
            await instructionDlg.SetData(_temporary_Direction.StoMoveID, _temporary_Item.StoMoveItemID);
            instructionDlg.Closed += _DispatchShowInstructionDialog_Closed;
            await Navigation.PushModalAsync(instructionDlg, false);
        }

        private async Task _showConfirmSourcePositionDialog()
        {
            var confirmSrcPositionDlg = new DispatchConfirmSourcePositionDialog();
            confirmSrcPositionDlg.SetHeaderText("Vychystání", "Ověření výchozí pozice");
            confirmSrcPositionDlg.SetData(_temporary_ItemInstruction);
            confirmSrcPositionDlg.Closed += _DispatchConfirmSrcPositionDialog_Closed;
            await Navigation.PushModalAsync(confirmSrcPositionDlg, false);
        }

        private async Task _showSelectArticleDialog()
        {
            var selectArticleDlg = new SelectArticleDialog();
            selectArticleDlg.SetHeaderText("Vychystání", "Načíst kartu");
            selectArticleDlg.Closed += _DispatchSelectArticleDialog_Closed;
            await Navigation.PushModalAsync(selectArticleDlg, false);
        }

        private async Task _showConfirmDestPositionDialog()
        {
            var destPositionDlg = new SelectPositionDialog();
            destPositionDlg.SetHeaderText("Vychystání", "Cílová pozice");
            destPositionDlg.Closed += _DispatchConfirmDestPositionDialog_Closed;
            await Navigation.PushModalAsync(destPositionDlg, false);
        }

        private async Task _showDispatchSetWeightBoxCountDialog()
        {
            var dataDlg = new DispatchSetWeightBoxCountDialog();
            dataDlg.SetHeaderText("Vychystání", "Zadejte data");
            dataDlg.Closed += _DispatchSetWeightBoxCountDialog_Closed;
            await Navigation.PushModalAsync(dataDlg, false);
        }

        private async Task _showSelectCarrierNumberDialog()
        {
            var transportCarrierDlg = new SelectCarrierNumberDialog();
            transportCarrierDlg.SetHeaderText("Vychystání", "Vybrat převážecí paletu");
            transportCarrierDlg.Closed += _DispatchTransportCarrierNumberDialog_Closed;
            await Navigation.PushModalAsync(transportCarrierDlg, false);
        }

        private async Task _loadUpItem()
        {
            // add to load list
            _loaded_Items.Add(_temporary_Item);

            // remove from item list
            _temporary_Items.RemoveAll(_ => _.ArtPackID == _temporary_Item.ArtPackID
                && _.XtraString == _temporary_Item.XtraString
                && _.DocPosition == _temporary_Item.DocPosition);
            ItemsLeftCount = _temporary_Items.Count;

            IsBtnUnLoadEnabled = true;

            // clear temp
            _temporary_Item = null;
            _temporary_ItemInstruction = null;

            if (_temporary_Items.Count > 0)
            {
                await _showSelectItemsDialog();
            }
        }

        protected async void ItemActionMenu(ItemInfo item)
        {
            var action = await DisplayActionSheet(null, "Zrušit", null, "Vytisknout štítek");

            switch (action)
            {
                case "Vytisknout štítek":
                    _printItemLabel(item);
                    break;
            }
        }

        private async void _printItemLabel(ItemInfo item)
        {
            _temporary_printLabelItem = item;

            Device.BeginInvokeOnMainThread(async () =>
            {
                var setPrintDialog = new SetPrintDialog();
                setPrintDialog.Closed += SetPrintDialog_Closed;
                await Navigation.PushModalAsync(setPrintDialog, false);
            });
        }

        private async void SetPrintDialog_Closed(object sender, Controls.Common.ButtonsBar.ButtonsEnum e)
        {
            if (e == Controls.Common.ButtonsBar.ButtonsEnum.OK)
            {
                var printerID = (sender as SetPrintDialog).PrinterID;
                var count = (sender as SetPrintDialog).Count;
                var client = new BarcodeClient();
                var result = await client.BarcodePrintLabel(new S4.ProcedureModels.BarCode.BarcodePrintLabel
                {
                    ArtPackID = _temporary_printLabelItem.ArtPackID,
                    LabelsQuant = count,
                    PrinterLocationID = printerID
                });

                if (!result.Success)
                {
                    await DisplayAlert("Akce skončila s chybou!", result.ErrorText, "OK");
                    return;
                }
            }

            // clear temp print item
            _temporary_printLabelItem = null;
        }
    }
}
﻿using S4.Entities;
using S4mp.Core;
using S4mp.Core.EANCode;
using S4mp.Core.Misc;
using S4mp.Core.ViewModel;
using System;
using System.Globalization;
using System.Windows.Input;

namespace S4mp.Pages.Direction.Receive
{
    public class ReceiveAddItemVM : BaseViewModel
    {
        #region expose through the view

        private ICommand _addItemCmd;
        public ICommand AddItemCmd
        {
            get => _addItemCmd;
            set => SetField(ref _addItemCmd, value);
        }

        private string _reCountQuantityText;
        public string ReCountQuantityText
        {
            get => _reCountQuantityText;
            set {
                if (_reCountPacking == null) return; // it is movable pack
                
                var txt = $"{QuantityToSave} {_articlePackingInfo.ArticlePacking.PackDesc} (1 {_reCountPacking.PackDesc} = {_reCountPacking.PackRelation} {_articlePackingInfo.ArticlePacking.PackDesc})";

                SetField(ref _reCountQuantityText, txt);
            }
        }

        private string _articleInfo;
        public string ArticleInfo
        {
            get => _articleInfo;
            set => SetField(ref _articleInfo, value);
        }

        private string _headerText;
        public string HeaderText
        {
            get => $"Nová položka";
            set => SetField(ref _headerText, value);
        }

        private string _packDesc;
        public string PackDesc
        {
            get => _reCountPacking != null ? _reCountPacking.PackDesc : _packDesc;
            set => SetField(ref _packDesc, value);
        }

        #endregion


        #region form

        private string _quantity = "";
        public string Quantity
        {
            get => _quantity;
            set
            {
                SetField(ref _quantity, value);
                QuantityToSave = 1; // update
            }
        }

        private decimal _quantityToSave;
        public decimal QuantityToSave
        {
            get => _quantityToSave;
            set
            {
                decimal qty = 0;
                if (!string.IsNullOrEmpty(Quantity) && decimal.TryParse(Quantity, out var qty2))
                    qty = qty2;

                // it is movable pack
                if (_reCountPacking == null)
                {
                    SetField(ref _quantityToSave, qty);
                    return;
                }

                var newVal = _reCountPacking.PackRelation * qty;

                SetField(ref _quantityToSave, newVal);
                ReCountQuantityText = "update";
            }
        }

        private ArticlePacking _reCountPacking;
        public ArticlePacking ReCountPacking
        {
            get => _reCountPacking;
            set
            {
                SetField(ref _reCountPacking, value);
                ReCountQuantityText = "update";
            }
        }

        private string _batchNum = "";
        public string BatchNum
        {
            get => _batchNum;
            set => SetField(ref _batchNum, value ?? string.Empty);
        }

        private string _expirationDateShort;
        public string ExpirationDateShort
        {
            get => _expirationDateShort;
            set => SetField(ref _expirationDateShort, _validateAndSetExpirationDate(value ?? string.Empty));
        }

        #endregion

        private int _stoMoveID;
        public int StoMoveID
        {
            get => _stoMoveID;
            set => SetField(ref _stoMoveID, value);
        }

        private int _artPackID;
        public int ArtPackID
        {
            get => _artPackID;
            set => SetField(ref _artPackID, value);
        }

        private int _directionID;
        public int DirectionID
        {
            get => _directionID;
            set => SetField(ref _directionID, value);
        }

        private string _carrierNum;
        public string CarrierNum
        {
            get => _carrierNum;
            set => SetField(ref _carrierNum, value ?? string.Empty);
        }

        private DateTime? _expirationDate;
        public DateTime? ExpirationDate
        {
            get => _expirationDate;
            set => SetField(ref _expirationDate, value);
        }

        private S4.Entities.Models.ArticlePackingInfo _articlePackingInfo;
        public S4.Entities.Models.ArticlePackingInfo ArticlePackingInfo
        {
            get => _articlePackingInfo;
            set
            {
                _articlePackingInfo = value;

                ArticleInfo = value.ArticleInfo;
                ArtPackID = value.ArticlePacking.ArtPackID;
                PackDesc = value.ArticlePacking.PackDesc;
            }
        }

        private EANCode _ean;
        public EANCode EAN
        {
            get => _ean;
            set
            {
                _ean = value;
                if (value == null) return;

                BatchNum = ViewModelHelpers.GetBatchNumber(value);

                // if empty then return
                if (value[EANCodePartEnum.ExpirationDate] == null) return;

                ExpirationDateShort = value[EANCodePartEnum.ExpirationDate]?.ValueDate?.ToString("d.M.yy") ?? string.Empty;
            }
        }

        private string _validateAndSetExpirationDate(string value)
        {
            ExpirationDate = ViewModelHelpers.ParseExpirationDate(value);

            return value ?? string.Empty;
        }
    }
}

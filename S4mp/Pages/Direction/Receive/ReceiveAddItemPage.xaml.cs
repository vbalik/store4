﻿using System;
using S4.Core;
using S4.Entities.Helpers;
using S4mp.Core.Misc;
using System.Threading.Tasks;
using Xamarin.Forms;
using S4mp.Core.Logger;
using S4mp.Interfaces;

namespace S4mp.Pages.Direction.Receive
{
    public partial class ReceiveAddItemPage : ContentPage, IPageLoadData
    {
        private ICustomEventLogger _logger;

        public ReceiveAddItemPage()
        {
            InitializeComponent();

            _logger = DependencyService.Get<ICustomEventLogger>();
            new Controls.OptionsMenu(this).FormatTitle("Příjem - nová položka");
        }

        private ReceiveAddItemVM ViewModel => (ReceiveAddItemVM)BindingContext;

        public void PageLoadData()
        {
            ViewModel.AddItemCmd = new Command(AddItem);

            if (ViewModel.ReCountPacking == null) reCountQuantityBlock.IsVisible = false;
            if (!ViewModel.ArticlePackingInfo.UseBatch) batchNumBlock.IsVisible = false;
            if (!ViewModel.ArticlePackingInfo.UseExpiration) expirationDateBlock.IsVisible = false;

            Entry_Focus(edQuantity);
        }

        protected async void AddItem()
        {
            activityIndicator.Show();
            try
            {
                if (_isFormValid())
                {
                    var newItem = await DependencyService.Get<S4mp.Client.Interfaces.IReceiveClient>().SaveItem(new S4.ProcedureModels.Receive.ReceiveSaveItem
                    {
                        StoMoveID = ViewModel.StoMoveID,
                        ArtPackID = ViewModel.ArtPackID,
                        CarrierNum = string.IsNullOrEmpty(ViewModel.CarrierNum) ? null : ViewModel.CarrierNum,
                        Quantity = ViewModel.QuantityToSave,
                        BatchNum = string.IsNullOrEmpty(ViewModel.BatchNum) ? null : ViewModel.BatchNum,
                        ExpirationDate = ViewModel.ExpirationDate,
                        UDICode = (ViewModel.EAN ?? new Core.EANCode.EANCode("")).RawData
                    });

                    if (!newItem.Success)
                    {
                        activityIndicator.Hide();
                        Singleton<Helpers.Audio>.Instance.PlayError();
                        await DisplayAlert("Upozornění", newItem.ErrorText, "OK");
                        return;
                    }

                    if(newItem.RepeatedItem)
                    {
                        await DisplayAlert("Upozornění", "Položka byla přičtena k existující položce", "OK");
                    }

                    activityIndicator.Hide();

                    await Navigation.PopAsync();

                    MessagingCenter.Send<ReceiveAddItemPage>(this, "ItemAdded");
                }
            }
            catch(Exception ex)
            {
                await _logger?.LogError($"{nameof(ReceiveAddItemPage)}.{nameof(AddItem)}: {ex.Message}", ex);
                DependencyService.Get<IMessage>().ErrorAlert(ex.Message);
            }
            finally
            {
                activityIndicator.Hide();
            }
        }

        private bool _isFormValid()
        {
            var isValid = true;

            if (string.IsNullOrEmpty(ViewModel.Quantity) || !int.TryParse(ViewModel.Quantity, out int batchNum))
            {
                if (isValid) Entry_Focus(edQuantity);
                isValid = false;
                edQuantityErrorFrame.HasError();
            }
            if (ViewModel.ArticlePackingInfo.UseBatch && string.IsNullOrEmpty(ViewModel.BatchNum))
            {
                if (isValid) Entry_Focus(edBatchNum);
                isValid = false;
                edBatchNumErrorFrame.HasError();
            }
            if (ViewModel.ArticlePackingInfo.UseExpiration && ViewModel.ExpirationDate == null)
            {
                if (isValid) Entry_Focus(edExpirationDateShort);
                isValid = false;
                edExpirationDateErrorFrame.HasError();
            }

            return isValid;
        }

        #region UI handles

        /// <summary>
        /// On focus entry reset error fields
        /// </summary>
        private void Entry_Focused(object sender, FocusEventArgs e)
        {
            if (sender == edQuantity)
                edQuantityErrorFrame.HasError(false);
            else if (sender == edBatchNum)
                edBatchNumErrorFrame.HasError(false);
            else if (sender == edExpirationDateShort)
                edExpirationDateErrorFrame.HasError(false);
        }

        private void EntryExpirationDateShort_Unfocused(object sender, FocusEventArgs e)
        {
            var date = ((Entry) sender).Text;
            if(ViewModel.ExpirationDate != null)
                ViewModel.ExpirationDateShort = ExpirationDateHelper.ParseExpirationDateEntry(date);
        }

        /// <summary>
        /// Focus entry
        /// </summary>
        private async void Entry_Focus(Entry field)
        {
            await Task.Delay(800);
            field.Focus();
        }

        private void AddItem_Completed(object sender, System.EventArgs e)
        {
            ViewModel.AddItemCmd.Execute(null);
        }

        #endregion
    }
}
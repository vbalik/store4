﻿using S4.Core;
using S4.Core.Utils;
using S4.Entities;
using S4.Entities.Helpers;
using S4.ProcedureModels.Receive;
using S4mp.Client;
using S4mp.Controls.Common;
using S4mp.Core;
using S4mp.Dialogs;
using System;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace S4mp.Pages.Direction.Receive
{
    public partial class ReceivePage : ContentPage, IPageLoadData, IPageWithParams
    {
        private string _docNumPrefix;
        private Controls.OptionsMenu _optionsMenu;
        private ItemInfo _temporary_printLabelItem;
        private SelectArticleDialog _temporary_articleDialog;
        private ArticlePacking _temporary_recountPacking;

        public ReceivePage()
        {
            InitializeComponent();

            _optionsMenu = new Controls.OptionsMenu(this);
            _optionsMenu.FormatTitle("Příjem");

            BindingContext = new ReceiveVM();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            // update item list on add new item
            MessagingCenter.Subscribe<ReceiveAddItemPage>(this, "ItemAdded", async (sender) =>
            {
                await _LoadItems();
                AddItemDialog();
            });

            // set show specFeatures on list
            bool.TryParse(Singleton<S4mp.Settings.LocalSettings>.Instance[Core.Consts.XMLPATH_SHOW_SPEC_FEATURES_ALERT_INFO_LIST] ?? "False", out var showSpecFeaturesAlertInfoList);
            ViewModel.ShowSpecFeaturesAlertInfoList = showSpecFeaturesAlertInfoList;
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            MessagingCenter.Unsubscribe<ReceiveAddItemPage>(this, "ItemAdded");
        }

        public void SetParams(string pageParams)
        {
            _docNumPrefix = pageParams;
        }

        private ReceiveVM ViewModel => (ReceiveVM)BindingContext;

        public async void PageLoadData()
        {
            ViewModel.CarrierCmd = new Command(Carrier_Btn);
            ViewModel.AddItemDialogCmd = new Command(AddItemDialog);
            ViewModel.ItemActionMenuCmd = new Command<ItemInfo>(ItemActionMenu);
            ViewModel.FinishDocumentCmd = new Command(FinishDocument);

            ViewModel.ItemsList.CollectionChanged += (object sender, NotifyCollectionChangedEventArgs e) =>
            {
                if (ViewModel.ItemsList.Count == 0)
                    lblNoItems.IsVisible = true;
                else
                    lblNoItems.IsVisible = false;
            };

            activityIndicator.Show();
            try
            {
                await _LoadItems();
            }
            finally
            {
                activityIndicator.Hide();
            }
        }

        private async Task _LoadItems()
        {
            ViewModel.ReceiveDocument = await DependencyService.Get<S4mp.Client.Interfaces.IReceiveClient>().GetDocument(new ReceiveProcMan
            {
                WorkerID = Singleton<BaseInfo>.Instance.CurrentWorkerID,
                DocNumPrefix = _docNumPrefix
            });

            if (ViewModel.ReceiveDocument == null)
            {
                await DisplayAlert("Upozornění", "Příjem nenalezen", "OK");
                return;
            }

            _optionsMenu.FormatTitle("Příjem " + ViewModel.ReceiveDocument.DocInfo);

            if (ViewModel.ReceiveDocument.Items.Count == 0)
                lblNoItems.IsVisible = true;

            ViewModel.ItemsList.Clear();
            foreach (var item in ViewModel.ReceiveDocument.Items)
            {
                ViewModel.ItemsList.Add(item);
            }
        }

        private async void SelectCarrierNumberDlg_Closed(object sender, Controls.Common.ButtonsBar.ButtonsEnum e)
        {
            if (e == Controls.Common.ButtonsBar.ButtonsEnum.Cancel)
                return;

            var dialog = sender as SelectCarrierNumberDialog;
            ViewModel.CarrierNum_temporary = dialog.CarrierNum ?? string.Empty;
            Carrier_Btn();

            // next dialog
            var getArticleDlg = new SelectArticleDialog();
            getArticleDlg.SetHeaderText("Příjem", "Zadejte kartu");
            getArticleDlg.Closed += GetArticleDlg_Closed;

            await Navigation.PushModalAsync(getArticleDlg, false);
        }

        private async void GetArticleDlg_Closed(object sender, Controls.Common.ButtonsBar.ButtonsEnum e)
        {
            if (e == Controls.Common.ButtonsBar.ButtonsEnum.OK)
            {
                var dialog = sender as SelectArticleDialog;
                dialog.GetArticleResult.ArticlePackingInfo.ArticlePacking.MovablePack = false;

                ArticlePacking reCountPacking = null;
                if (!string.IsNullOrEmpty(dialog.GetArticleResult.BarCodeText))
                {
                    var article = await DependencyService.Get<Client.Interfaces.IArticleClient>().GetArticleModel(dialog.GetArticleResult.ArticlePackingInfo.ArticleID);

                    // barcode LIKE '%abc' because doesn't need to start with zero
                    var packing = article.Packings.FirstOrDefault(_ => (_.BarCode ?? string.Empty).EndsWith(dialog.GetArticleResult.BarCodeText));
                    if (packing != null && !packing.MovablePack) reCountPacking = packing;
                }
                else
                {
                    if (string.IsNullOrEmpty(dialog.GetArticleResult.ArticlePackingInfo.ArticlePacking.BarCode))
                    {
                        if (await DisplayAlert("Informace", "Přejete si nastavit kód?", "Nastavit kód", "Zrušit"))
                        {
                            _temporary_articleDialog = dialog;
                            _temporary_recountPacking = reCountPacking;

                            // open set barcode dialog
                            var setBarcodeDialog = new SetBarcodeDialog();
                            setBarcodeDialog.Closed += SetBarcodeDialog_Closed;
                            await Navigation.PushModalAsync(setBarcodeDialog, false);

                            return;
                        }
                    }
                }

                await _showAddItemPage(dialog, reCountPacking);
            }
        }

        private async void _ReceivePrintDirectionDocDialog_Closed(object sender, ButtonsBar.ButtonsEnum e)
        {
            if (e != ButtonsBar.ButtonsEnum.OK)
            {
                await Navigation.PopToRootAsync();
                return;
            }

            // do print document
            var printerID = (sender as SetPrintDialog).PrinterID;
            var printCount = (sender as SetPrintDialog).Count;

            var client = new DirectionClient();
            var result = await client.ReceivePrintByDirection(new ReceivePrintByDirection
            {
                DirectionID = ViewModel.ReceiveDocument.DirectionID,
                PrinterLocationID = printerID,
                Count = printCount
            });

            if (!result.Success)
            {
                Singleton<Helpers.Audio>.Instance.PlayError();
                await DisplayAlert("Upozornění", result.ErrorText, "OK");
            }

            await Navigation.PopToRootAsync();
        }

        private async void Carrier_Btn()
        {
            // IMPORTANT used as condition
            const string NEW_CARRIER_TEXT = "Nová paleta";
            const string CLOSE_CARRIER_TEXT = "Ukončit paletu";

            if (ViewModel.CarrierNum_temporary is null)
            {
                if (btnCarrier.Text.Equals(NEW_CARRIER_TEXT))
                {
                    var selectCarrierNumberDlg = new SelectCarrierNumberDialog();
                    selectCarrierNumberDlg.SetHeaderText("Příjem", "Zadejte paletu");
                    selectCarrierNumberDlg.Closed += SelectCarrierNumberDlg_Closed;

                    await Navigation.PushModalAsync(selectCarrierNumberDlg, false);
                }

                btnCarrier.Text = NEW_CARRIER_TEXT;
            }
            else
            {
                if (btnCarrier.Text.Equals(CLOSE_CARRIER_TEXT))
                {
                    ViewModel.CarrierNum_temporary = null;
                    btnCarrier.Text = NEW_CARRIER_TEXT;
                }
                else
                {
                    btnCarrier.Text = CLOSE_CARRIER_TEXT;
                }
            }
        }

        protected async void AddItemDialog()
        {
            // clear temp data
            _temporary_articleDialog = null;
            _temporary_recountPacking = null;

            if (ViewModel.CarrierNum_temporary is null)
            {
                var selectCarrierNumberDlg = new SelectCarrierNumberDialog();
                selectCarrierNumberDlg.SetHeaderText("Příjem", "Zadejte paletu");
                selectCarrierNumberDlg.Closed += SelectCarrierNumberDlg_Closed;

                await Navigation.PushModalAsync(selectCarrierNumberDlg, false);
            }
            else
            {
                var getArticleDlg = new SelectArticleDialog();
                getArticleDlg.SetHeaderText("Příjem", "Zadejte kartu");
                getArticleDlg.Closed += GetArticleDlg_Closed;

                await Navigation.PushModalAsync(getArticleDlg, false);
            }
        }

        private async void FinishDocument()
        {
            if (!await DisplayAlert("Informace", $"Uložit příjem?", "Ano", "Ne"))
                return;

            var client = DependencyService.Get<S4mp.Client.Interfaces.IReceiveClient>();
            await client.Done(new ReceiveDone
            {
                StoMoveID = ViewModel.ReceiveDocument.StoMoveID
            });

            await client.TestDirection(new ReceiveTestDirection
            {
                DirectionID = ViewModel.ReceiveDocument.DirectionID
            });

            if (await DisplayAlert("Informace", "Příjem dokončen. Tisknout doklad?", "Tisk", "Zrušit"))
            {
                await _showReceivePrintDirectionDocDialog();
                return;
            }

            await Navigation.PopToRootAsync();
        }

        protected async void ItemActionMenu(ItemInfo item)
        {
            var action = await DisplayActionSheet(null, "Zrušit", null, "Detail", "Vytisknout štítek", "Odebrat položku", "Nastavit čárový kód");

            switch (action)
            {
                case "Detail":
                    _showItemDetail(item);
                    break;
                case "Odebrat položku":
                    _removeItem(item);
                    break;
                case "Vytisknout štítek":
                    _printItemLabel(item);
                    break;
                case "Nastavit čárový kód":
                    _setBarcodes(item);
                    break;
            }
        }

        private async void _removeItem(ItemInfo item)
        {
            if (await DisplayAlert("Smazat položku?", $"Opravdu chcete odebrat položku {item.ArticleCode}?", "Ano", "Ne"))
            {
                await DependencyService.Get<S4mp.Client.Interfaces.IReceiveClient>().CancelItem(new ReceiveCancelItem
                {
                    StoMoveID = item.StoMoveID,
                    DocPosition = item.DocPosition
                });

                ViewModel.ItemsList.Remove(item);
            }
        }

        private async void _showItemDetail(ItemInfo item)
        {
            var articleDetailDlg = new GetArticleDetailDialog();
            await articleDetailDlg.SetArticleID(item.ArticleID);

            await Navigation.PushModalAsync(articleDetailDlg, false);
        }

        private void _printItemLabel(ItemInfo item)
        {
            _temporary_printLabelItem = item;

            Device.BeginInvokeOnMainThread(async () =>
            {
                var setPrintDialog = new SetPrintDialog();
                setPrintDialog.Closed += SetPrintDialog_Closed;
                await Navigation.PushModalAsync(setPrintDialog, false);
            });
        }

        private async void _setBarcodes(ItemInfo item)
        {
            var setBarcodeDialog = new SetBarcodeDialog();
            setBarcodeDialog.Closed += SetBarcodeDlg_Closed;
            setBarcodeDialog.ArtPackID = item.ArtPackID;
            await Navigation.PushModalAsync(setBarcodeDialog, false);
        }

        private async void SetPrintDialog_Closed(object sender, Controls.Common.ButtonsBar.ButtonsEnum e)
        {
            if (e == Controls.Common.ButtonsBar.ButtonsEnum.OK)
            {
                var printerID = (sender as SetPrintDialog).PrinterID;
                var count = (sender as SetPrintDialog).Count;
                var client = new BarcodeClient();
                var result = await client.BarcodePrintLabel(new S4.ProcedureModels.BarCode.BarcodePrintLabel
                {
                    ArtPackID = _temporary_printLabelItem.ArtPackID,
                    LabelsQuant = count,
                    PrinterLocationID = printerID
                });

                if (!result.Success)
                {
                    await DisplayAlert("Akce skončila s chybou!", result.ErrorText, "OK");
                    return;
                }
            }

            // clear temp print item
            _temporary_printLabelItem = null;
        }

        private async Task _showReceivePrintDirectionDocDialog()
        {
            var printDirectionDlg = new SetPrintDialog();
            printDirectionDlg.Closed += _ReceivePrintDirectionDocDialog_Closed;
            await Navigation.PushModalAsync(printDirectionDlg, false);
        }

        private async Task _showAddItemPage(SelectArticleDialog dialog, ArticlePacking reCountPacking)
        {
            var receiveAddItemPage = new ReceiveAddItemPage
            {
                BindingContext = new ReceiveAddItemVM
                {
                    EAN = dialog.GetArticleResult.EAN,
                    ArticlePackingInfo = dialog.GetArticleResult.ArticlePackingInfo,
                    CarrierNum = ViewModel.CarrierNum_temporary,
                    StoMoveID = ViewModel.ReceiveDocument.StoMoveID,
                    ReCountPacking = reCountPacking
                }
            };

            receiveAddItemPage.PageLoadData();

            // clear temp data
            _temporary_articleDialog = null;
            _temporary_recountPacking = null;

            await Navigation.PushAsync(receiveAddItemPage, false);
        }

        private async void SetBarcodeDialog_Closed(object sender, Controls.Common.ButtonsBar.ButtonsEnum e)
        {
            if (e != ButtonsBar.ButtonsEnum.OK && e != ButtonsBar.ButtonsEnum.New) return;

            if (e == ButtonsBar.ButtonsEnum.New)
            {
                var client = new BarcodeClient();
                var result = await client.BarcodeMakeCode(new S4.ProcedureModels.BarCode.BarcodeMakeCode()
                {
                    ArtPackID = _temporary_articleDialog.GetArticleResult.ArticlePackingInfo.ArticlePacking.ArtPackID
                });

                if (!result.Success)
                {
                    await DisplayAlert("Akce skončila s chybou!", result.ErrorText, "OK");
                }
            }

            if (e == ButtonsBar.ButtonsEnum.OK)
            {
                var barcode = (sender as SetBarcodeDialog).Barcode;
                if (string.IsNullOrWhiteSpace(barcode) || barcode == Consts.BARCODE_UNKNOWN_VALUE)
                {
                    await DisplayAlert("Akce skončila s chybou!", "Kód nebyl rozpoznán", "OK");
                    return;
                }

                var client = new BarcodeClient();
                var result = await client.BarcodeSet(new S4.ProcedureModels.BarCode.BarcodeSet()
                {
                    ArtPackID = _temporary_articleDialog.GetArticleResult.ArticlePackingInfo.ArticlePacking.ArtPackID,
                    BarCode = barcode
                });

                if (!result.Success)
                {
                    await DisplayAlert("Akce skončila s chybou!", result.ErrorText, "OK");
                }
            }

            await _showAddItemPage(_temporary_articleDialog, _temporary_recountPacking);
        }

        private async void SetBarcodeDlg_Closed(object sender, Controls.Common.ButtonsBar.ButtonsEnum e)
        {
            if (e != Controls.Common.ButtonsBar.ButtonsEnum.OK) return;

            var barcode = (sender as SetBarcodeDialog).Barcode;
            if (string.IsNullOrWhiteSpace(barcode) || barcode == Consts.BARCODE_UNKNOWN_VALUE)
            {
                await DisplayAlert("Akce skončila s chybou!", "Kód nebyl rozpoznán", "OK");
                return;
            }

            var artPackID = (sender as SetBarcodeDialog).ArtPackID;
            var client = new BarcodeClient();
            var result = await client.BarcodeSet(new S4.ProcedureModels.BarCode.BarcodeSet()
            {
                ArtPackID = artPackID ?? _temporary_articleDialog.GetArticleResult.ArticlePackingInfo.ArticlePacking.ArtPackID,
                BarCode = barcode,
                AnotherBarCodeCreation = true
            });

            if (!result.Success)
            {
                await DisplayAlert("Akce skončila s chybou!", result.ErrorText, "OK");
            }
            else
            {
                await DisplayAlert("Akce byla úspěšná", "Kód byl nastaven", "OK");
            }
        }
    }
}
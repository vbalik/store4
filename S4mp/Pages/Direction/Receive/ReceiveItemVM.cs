﻿using System;

namespace S4mp.Pages.Direction.Receive
{
    public class ReceiveItemVM
    {
        public int StoMoveID { get; set; }
        public int PositionID { get; set; }
        public int ArtPackID { get; set; }
        public string CarrierNum { get; set; }
        public decimal Quantity { get; set; }
        public string BatchNum { get; set; }
        public DateTime? ExpirationDate { get; set; }

        public bool RepeatedItem { get; set; } = false;
        public int StoMoveItemID { get; set; }
    }
}
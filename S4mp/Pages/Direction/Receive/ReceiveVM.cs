﻿using S4.Entities.Helpers;
using S4.ProcedureModels.Receive;
using S4mp.Core.ViewModel;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows.Input;

namespace S4mp.Pages.Direction.Receive
{
    class ReceiveVM : BaseViewModel
    {
        public ReceiveVM()
        {
            ItemsList = new ObservableCollection<ItemInfo>();
            ItemsList.CollectionChanged += (object sender, NotifyCollectionChangedEventArgs e) => ItemsListCount = ItemsList.Count;
            ItemsListCount = 0;
        }

        #region expose through the view

        private ICommand _addItemDialogCmd;
        public ICommand AddItemDialogCmd
        {
            get => _addItemDialogCmd;
            set => SetField(ref _addItemDialogCmd, value);
        }

        private ICommand _itemActionMenuCmd;
        public ICommand ItemActionMenuCmd
        {
            get => _itemActionMenuCmd;
            set => SetField(ref _itemActionMenuCmd, value);
        }

        private ICommand _finishDocumentCmd;
        public ICommand FinishDocumentCmd
        {
            get => _finishDocumentCmd;
            set => SetField(ref _finishDocumentCmd, value);
        }

        private ICommand _carrierCmd;
        public ICommand CarrierCmd
        {
            get => _carrierCmd;
            set => SetField(ref _carrierCmd, value);
        }

        private string _carrierNumText;
        public string CarrierNumText
        {
            get => _carrierNumText;
            set {
                var val = !string.IsNullOrEmpty(value) 
                    ? value 
                    : (value == string.Empty 
                        ? "bez palety" 
                        : null);
                SetField(ref _carrierNumText, val);
            }
        }

        private int _itemsListCount;
        public int ItemsListCount
        {
            get => _itemsListCount;
            set => SetField(ref _itemsListCount, value);
        }

        #endregion

        public ObservableCollection<ItemInfo> ItemsList { get; set; }

        private ReceiveProcMan _receiveDocument;
        public ReceiveProcMan ReceiveDocument
        {
            get => _receiveDocument;
            set => SetField(ref _receiveDocument, value);
        }
        
        private bool _showSpecFeaturesAlertInfoList;
        public bool ShowSpecFeaturesAlertInfoList
        {
            get => _showSpecFeaturesAlertInfoList;
            set => SetField(ref _showSpecFeaturesAlertInfoList, value);
        }

        private string _carrierNum_temporary;
        public string CarrierNum_temporary
        {
            get => _carrierNum_temporary;
            set {
                SetField(ref _carrierNum_temporary, value);
                CarrierNumText = value;
            }
        }
    }
}

﻿using S4.Core;
using S4.Entities.Helpers;
using S4.Entities.Models;
using S4.ProcedureModels.DispCheck;
using S4mp.Controls;
using S4mp.Controls.Common;
using S4mp.Controls.Scanner;
using S4mp.Core;
using S4mp.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static S4.ProcedureModels.DispCheck.DispCheckDone;

namespace S4mp.Pages.Direction.DispatchCheck
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DispatchCheckPage : ContentPage, IPageLoadData, IRefreshable
    {
        //dependent resolver for scanning
        private IScannerResolver _scannerResolver;

        public DispatchCheckPage()
        {
            InitializeComponent();

#if !DATALOGIC && !HONEYWELL
            var btnScan = new Button
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                Text = "SCAN",
                Command = new Command(() =>
                {
                    _scannerResolver.Scan();
                })
            };
            (Content as StackLayout).Children.Insert(0, btnScan);
#endif

            //create scanning resolver
            _scannerResolver = DependencyService.Get<IScannerResolver>();
            _scannerResolver.Init(true, ScannerResolver_Scanned);

            new Controls.OptionsMenu(this).FormatTitle("Vychystání - kontrola");
            LoadItemsCommand = new Command(PageLoadData);
            SearchDocInfoCmd = new Command(
                execute: () =>
                {
                    AssignDispCheckByDocInfo(SearchDocInfoText);
                },
                canExecute: () =>
                {
                    return !string.IsNullOrEmpty(SearchDocInfoText);
                }
            );
        }

        public static readonly BindableProperty LoadItemsCommandProperty = BindableProperty.Create(nameof(LoadItemsCommand), typeof(ICommand), typeof(DispatchCheckPage), null);
        public ICommand LoadItemsCommand
        {
            get { return (ICommand)GetValue(LoadItemsCommandProperty); }
            set { SetValue(LoadItemsCommandProperty, value); }
        }

        public static readonly BindableProperty IsBussyProperty = BindableProperty.Create(nameof(IsBussy), typeof(bool), typeof(DispatchCheckPage), false);
        public bool IsBussy
        {
            get { return (bool)GetValue(IsBussyProperty); }
            set { SetValue(IsBussyProperty, value); }
        }

        /// <summary>
        /// Search doc info for adding dispCheck
        /// </summary>
        public static readonly BindableProperty SearchDocInfoCmdProperty = BindableProperty.Create(nameof(SearchDocInfoCmd), typeof(ICommand), typeof(DispatchCheckPage), null);
        public ICommand SearchDocInfoCmd
        {
            get { return (ICommand)GetValue(SearchDocInfoCmdProperty); }
            set { SetValue(SearchDocInfoCmdProperty, value); }
        }

        public static readonly BindableProperty SearchDocInfoTextProperty = BindableProperty.Create(nameof(SearchDocInfoText), typeof(string), typeof(DispatchCheckPage), null);
        public string SearchDocInfoText
        {
            get { return (string)GetValue(SearchDocInfoTextProperty); }
            set { SetValue(SearchDocInfoTextProperty, value); }
        }

        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);

            if (propertyName.Equals("Renderer", StringComparison.OrdinalIgnoreCase))
            {
                //stop listening on closing
                var rr = DependencyService.Get<IRendererResolver>();
                if (!rr.HasRenderer(this))
                    _scannerResolver.Init(false, ScannerResolver_Scanned);
            }
        }

        private async void AssignDispCheckByDocInfo(string value)
        {
            // get direction by docInfo
            var directions = await DependencyService.Get<Client.Interfaces.IDispatchCheckClient>().FindDirectionsByDocNumber(new DispCheckAvailableDirectionsDocNumber
            {
                DocumentID = value
            });

            if (directions == null || directions.Directions == null || directions.Directions.Count == 0)
            {
                Singleton<Helpers.Audio>.Instance.PlayError();
                docInfoInputFrame.HasError();
                return;
            }

            if (directions.Directions.Count > 1)
            {
                // show document list dialog
                await _showSelectDocumentDialog(directions.Directions);
                return;
            }

            await AssignDispCheckToCurrentUser(directions.Directions.FirstOrDefault());
        }

        private async Task AssignDispCheckByDocNumber(string docNumber)
        {
            // get direction by docInfo
            var result = await DependencyService.Get<Client.Interfaces.IDispatchCheckClient>().FindDirectionsByDocNumber(new DispCheckAvailableDirectionsDocNumber
            {
                DocumentID = docNumber
            });

            if (!result.Success)
            {
                Singleton<Helpers.Audio>.Instance.PlayError();
                await DisplayAlert("Upozornění", result.ErrorText, "OK");
                docInfoInputFrame.HasError();
                return;
            }
            else if (result.Directions == null || result.Directions.Count == 0)
            {
                Singleton<Helpers.Audio>.Instance.PlayError();
                return;
            }
            else if (result.Directions.Count > 1)
            {
                // show document list dialog
                await _showSelectDocumentDialog(result.Directions);
                return;
            }

            await AssignDispCheckToCurrentUser(result.Directions.FirstOrDefault());
        }

        private async Task AssignDispCheckToCurrentUser(DocumentInfo direction)
        {
            if (direction == null) return;

            // assign dispcheck to user
            var resAssignToUser = await DependencyService.Get<Client.Interfaces.IDispatchCheckClient>().DispCheckWait(new DispCheckWait()
            {
                WorkerID = Singleton<BaseInfo>.Instance.CurrentWorkerID,
                DirectionID = direction.DocumentID
            });
            if (!resAssignToUser.Success)
            {
                Singleton<Helpers.Audio>.Instance.PlayError();
                await DisplayAlert("Upozornění", resAssignToUser.ErrorText, "OK");
                return;
            }

            docInfoInputFrame.HasError(false);
            edSearchDocInfo.Text = null;

            // refresh list
            RefreshData();
        }

        private async void ScannerResolver_Scanned(object sender, BarCodeTypeEnum e)
        {
            var value = (sender as IScannerResolver).Text;
            if (!string.IsNullOrEmpty(value))
                await AssignDispCheckByDocNumber(value);
        }

        public async void PageLoadData()
        {
            if (IsBussy == true)
                return;
            IsBussy = true;
            try
            {
                var resModel = await DependencyService.Get<Client.Interfaces.IDispatchCheckClient>().DispCheckListDirections(new S4.ProcedureModels.DispCheck.DispCheckListDirections() { WorkerID = Singleton<BaseInfo>.Instance.CurrentWorkerID });
                if (!resModel.Success)
                {
                    Singleton<Helpers.Audio>.Instance.PlayError();
                    await DisplayAlert("Upozornění", resModel.ErrorText, "OK");
                    return;
                }

                documents.ItemsSource = resModel.Directions.OrderByDescending(_ => _.DocumentID);
            }
            finally
            {
                IsBussy = false;
            }
        }

        public void RefreshData()
        {
            PageLoadData();
        }

        private async void Documents_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            IsBussy = true;
            try
            {
                await _showDispatchCheckDialog((e.Item as DocumentInfo).DocumentID);
            }
            finally
            {
                IsBussy = false;
            }
        }

        private async void Dlg_Closed(object sender, Controls.Common.ButtonsBar.ButtonsEnum e)
        {
            _scannerResolver.Init(true, ScannerResolver_Scanned);

            var dialog = sender as Dialogs.DispCheckDialog;
            var directionID = dialog.DirectionID;

            if (e != Controls.Common.ButtonsBar.ButtonsEnum.OK)
            {
                await DependencyService.Get<DAL.ILocalDatabase>().Execute($"delete from {nameof(S4mp.Entities.DispCheck)} where {nameof(S4mp.Entities.DispCheck.DirectionID)} = @0", directionID);
                return;
            }

            Dictionary<int, List<SerialNumberItem>> serialNumbers = new Dictionary<int, List<SerialNumberItem>>();

            foreach (var item in dialog.DispCheckViewModel.Items)
            {
                var serialsGrouped = item.DirectionValues.GroupBy(_ => _.DirectionItemID, (k, g) => new {
                    directionItemID = k,
                    serialNumbers = g.SelectMany(_ => _.SerialNumbers).ToList()
                }).ToList();

                foreach (var directionValue in serialsGrouped)
                {
                    serialNumbers.Add(directionValue.directionItemID, directionValue.serialNumbers.Select(_ => new SerialNumberItem
                    {
                        SerialNumber = _.SerialNumber,
                        BatchNum = _.BatchNum,
                        ExpirationDate = _.ExpirationDate
                    }).ToList());
                }
            }

            var dispCheckDone = await DependencyService.Get<Client.Interfaces.IDispatchCheckClient>()
                .DispCheckDone(new DispCheckDone()
                {
                    DirectionID = directionID,
                    SerialNumbers = serialNumbers
                });

            if (!dispCheckDone.Success)
            {
                Singleton<Helpers.Audio>.Instance.PlayError();
                await DisplayAlert("Upozornění", dispCheckDone.ErrorText, "OK");
                return;
            }

            // remove serials from local storage for direction
            await DependencyService.Get<DAL.ILocalDatabase>().Execute($"delete from {nameof(S4mp.Entities.DispCheckSerial)} where {nameof(S4mp.Entities.DispCheckSerial.DirectionID)} = @0", directionID);

            await DisplayAlert("Informace", "Kontrola dokončena", "OK");

            PageLoadData();
        }

        private async void _DispatchSelectDocumentDialog_Closed(object sender, ButtonsBar.ButtonsEnum e)
        {
            var direction = ((SelectDocumentDialog)sender).Document;
            await AssignDispCheckToCurrentUser(direction);
        }

        private async Task _showDispatchCheckDialog(int documentID)
        {
            var dispCheckProc = await DependencyService.Get<Client.Interfaces.IDispatchCheckClient>()
                    .DispCheckProc(new S4.ProcedureModels.DispCheck.DispCheckProc() { DirectionID = documentID });
            if (!dispCheckProc.Success)
            {
                Singleton<Helpers.Audio>.Instance.PlayError();
                await DisplayAlert("Upozornění", dispCheckProc.ErrorText, "OK");
                return;
            }

            _scannerResolver.Init(false, ScannerResolver_Scanned);

            var dlg = new Dialogs.DispCheckDialog();
            dlg.Closed += Dlg_Closed;
            await Navigation.PushModalAsync(dlg);
            dlg.LoadItems(dispCheckProc.CheckMethod, documentID);
        }

        private async Task _showSelectDocumentDialog(List<DocumentInfo> directionInfos)
        {
            var dispatchSelectDocumentDlg = new SelectDocumentDialog();
            dispatchSelectDocumentDlg.SetHeaderText("Kontrola", "Seznam dokladů");
            dispatchSelectDocumentDlg.LoadData(directionInfos);
            dispatchSelectDocumentDlg.Closed += _DispatchSelectDocumentDialog_Closed;
            await Navigation.PushModalAsync(dispatchSelectDocumentDlg, false);
        }

        #region UI

        private void EdSearchDocInfo_Completed(object sender, EventArgs e)
        {
            if (SearchDocInfoCmd.CanExecute(null))
                SearchDocInfoCmd.Execute(null);
        }

        private void edSearchDocInfo_Focused(object sender, FocusEventArgs e)
        {
            docInfoInputFrame.HasError(false);
        }

        private void edSearchDocInfo_TextChanged(object sender, TextChangedEventArgs e)
        {
            ((Command)SearchDocInfoCmd).ChangeCanExecute();
        }

        #endregion
    }
}
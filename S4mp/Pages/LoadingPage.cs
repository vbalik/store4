﻿using System;
using System.Threading.Tasks;

using Xamarin.Forms;

using S4.Entities;

namespace S4mp.Pages
{
	public class LoadingPage : ContentPage
	{
		private ProgressBar progressBar = new ProgressBar();
		private Task progressAnimation;
		private Label label = null;

		public LoadingPage()
		{
			new Controls.OptionsMenu(this).FormatTitle("Načítám");

			label = new Label
			{
				HorizontalTextAlignment = TextAlignment.Center,
				Text = "Připojování k serveru ...",
				FontSize = 15,
				//TextColor = Controls.Common.CommonHelper.DialogLabelColor
			};

			Content = new StackLayout
			{
				VerticalOptions = LayoutOptions.Center,
				Children = {
					label,
					/*new Image {
						Source = "N7_flashScreen.jpg"
					},*/
					progressBar
				}
			};
		}

		public Task ProgressStart()
		{
			progressAnimation = progressBar.ProgressTo(1, 6000, Easing.Linear);
			return progressAnimation;
		}

		public Task ProgressStop()
		{
			return progressBar.ProgressTo(1, 1, Easing.Linear);
		}

		public void SetMessage(string message, Color backgroundColor, Color textColor)
		{
			label.Text = message;
			label.BackgroundColor = backgroundColor;
			label.TextColor = textColor;
		}

	}
}



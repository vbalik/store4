﻿using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Plugin.LocalNotifications;
using S4.Core;
using S4mp.Core;
using S4mp.Core.Logger;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using Xamarin.Forms;

namespace S4mp.Helpers
{
    class TerminalHub : Interfaces.ITerminalHub
    {
        private ICustomEventLogger _logger;
        private HubConnection _hubConnection;

        public async Task InitHubConnection()
        {
            _logger = DependencyService.Get<ICustomEventLogger>();

            _hubConnection = new HubConnectionBuilder()
              .WithUrl(S4mp.Client.ClientBase.ApiUrl + "terminal-hub")
              //.WithAutomaticReconnect()
              .AddNewtonsoftJsonProtocol()
              //.ConfigureLogging(e => e.SetMinimumLevel(LogLevel.Debug))
              .Build();

            // events
            _hubConnection.Closed += async (exc) =>
            {
                Singleton<BaseInfo>.Instance.TerminalHubConnected = false;
            };

            _hubConnection.Reconnected += async (connectionID) =>
            {
                Singleton<BaseInfo>.Instance.TerminalHubConnected = true;
                await LogOn();
            };

            // listeners
            // AssignmentUpdated
            _hubConnection.On<S4.Entities.Helpers.TerminalJobAssignUpdate>("AssignmentUpdated", (update) =>
            {
                if (update.WorkerID == Singleton<BaseInfo>.Instance.CurrentWorkerID)
                {
                    S4mp.MainPage.UpdateAssigmnments(update);
                }
            });
            // ResetAssignment
            _hubConnection.On<S4.Entities.Helpers.TerminalJobAssignUpdate>("ResetAssignment", (update) =>
            {
                S4mp.MainPage.UpdateAssigmnments(null);
            });
            // InfoToTerminal
            _hubConnection.On<string>("InfoToTerminal", (message) =>
            {
                CrossLocalNotifications.Current.Show("Zpráva S4", message);
            });


            await ConnectAsync();
        }


        public async Task LogOn()
        {
            if (!Singleton<BaseInfo>.Instance.TerminalHubConnected)
                return;

            try
            {
                await _hubConnection.InvokeAsync("LogOn",
                    Singleton<BaseInfo>.Instance.CurrentWorkerID,
                    (string) Application.Current.Properties[S4mp.Core.Consts.PROP_APP_VERSION],
                    (string) Application.Current.Properties[S4mp.Core.Consts.PROP_HW_DESCRIPTION]);
            }
            catch (Exception ex)
            {
                await _logger?.LogError($"{nameof(TerminalHub)}.{nameof(LogOn)}: {ex.Message}", ex);
                System.Console.Write(ex.Message);
            }
        }

        public async Task ConnectAsync()
        {
            int[] intervals = { 1000, 3000, 5000, 10000 };
            int currentIndex = 0;

            System.Timers.Timer timer = new System.Timers.Timer();
            timer.Interval = intervals[currentIndex];
            timer.Elapsed += (sender, e) =>
            {
                _ConnectAsync().Wait();

                currentIndex++;
                if (currentIndex >= intervals.Length || _hubConnection.State.Equals(HubConnectionState.Connected))
                {
                    timer.Stop();
                }
                else
                {
                    // try next interval
                    timer.Interval = intervals[currentIndex];
                }
            };
            timer.Start();


            await Task.CompletedTask;
        }

        public async Task DisconnectAsync()
        {
            if (_hubConnection.State.Equals(HubConnectionState.Connected) || _hubConnection.State.Equals(HubConnectionState.Connecting))
                await _hubConnection.StopAsync();
        }

        private async Task _ConnectAsync()
        {
            if (_hubConnection.State.Equals(HubConnectionState.Connected) || _hubConnection.State.Equals(HubConnectionState.Connecting))
                return;

            try
            {
                await _hubConnection.StartAsync();
                Singleton<BaseInfo>.Instance.TerminalHubConnected = true;
            }
            catch (Exception ex)
            {
                await _logger?.LogError($"{nameof(TerminalHub)}.{nameof(_ConnectAsync)}: {ex.Message}", ex);
                System.Console.Write(ex.Message);
                Singleton<BaseInfo>.Instance.TerminalHubConnected = false;
            }
        }
    }
}

﻿using Plugin.SimpleAudioPlayer;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4mp.Helpers
{
    class Audio
    {
        private static object _lock = new object();
        private ISimpleAudioPlayer _playerError = null;
        private ISimpleAudioPlayer _playerBell = null;


        public void PlayError()
        {
            PlayAudio(ref _playerError, "Robot_blip_2-Marianne_Gagnon-299056732.wav");
        }


        public void PlayBell()
        {
            PlayAudio(ref _playerBell, "Metal_Gong-Dianakc-109711828.wav");
        }


        private static void PlayAudio(ref ISimpleAudioPlayer player, string resourceName)
        {
            lock (_lock)
            {
                if (player == null)
                {
                    var assembly = typeof(App).Assembly;
                    using (var audioStream = assembly.GetManifestResourceStream($"S4mp.Resources.{resourceName}"))
                    {
                        player = CrossSimpleAudioPlayer.CreateSimpleAudioPlayer();
                        player.Load(audioStream);
                    }
                }
            }
            player.Play();
        }
    }
}

﻿using Microsoft.AspNetCore.SignalR.Client;
using System.Threading.Tasks;

namespace S4mp.Helpers
{
    class HoneywellScannerHub : Interfaces.IHoneywellScannerHub
    {
        private HubConnection _hubConnection;
        private string WS_METHOD = "honeywell-hub";

        public async Task InitHubConnection()
        {
            _hubConnection = new HubConnectionBuilder()
                    .WithUrl(S4mp.Client.ClientBase.ApiUrl + WS_METHOD, httpConnectionOptions =>
                    {
                        httpConnectionOptions.Headers.Add("user-agent", "honeywellScanner");
                    })
                    .Build();

            await ConnectAsync();
        }

        public async Task ConnectAsync()
        {
            if (_hubConnection.State.Equals(HubConnectionState.Disconnected))
                await _hubConnection.StartAsync();
        }

        public async Task DisconnectAsync()
        {
            if (_hubConnection.State.Equals(HubConnectionState.Connected) || _hubConnection.State.Equals(HubConnectionState.Connecting))
                await _hubConnection.StopAsync();
        }

        // Unused for feature maybe
        //public async Task SendAsync(string methodName, object data)
        //{
        //    if (_hubConnection.State.Equals(HubConnectionState.Connected))
        //        await _hubConnection.InvokeAsync(methodName, data);
        //}
    }
}

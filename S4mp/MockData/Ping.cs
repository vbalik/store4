﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace S4mp.MockData
{
    public class Ping : Client.Interfaces.IPing
    {
        public Task<bool> PingWSAsync(string apiUrl)
        {
            return Task<bool>.Run(() => { return true;  });
        }

        public bool PingWSSync(string apiUrl)
        {
            return true;
        }
    }
}

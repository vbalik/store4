﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4mp.MockData
{
    public abstract class DataBase
    {
        public DataBase()
        {
            CreateData();
        }

        protected virtual void CreateData()
        {

        }
    }
}

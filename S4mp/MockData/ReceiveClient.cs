﻿using System.Collections.Generic;
using System.Threading.Tasks;
using S4.Entities.Helpers;
using S4.ProcedureModels.Receive;
using S4mp.Client.Interfaces;

namespace S4mp.MockData
{
    public class ReceiveClient : DataBase, IReceiveClient
    {
        private const string CONTROLLER_NAME = "procedures";

        public virtual async Task<ReceiveProcMan> GetDocument(ReceiveProcMan model)
        {
            return new ReceiveProcMan
            {
                DirectionID = 1,
                DocInfo = "test",
                StoMoveID = 1,
                Items = new List<ItemInfo>
                {
                    new ItemInfo
                    {
                        ArticleID = 1,
                        ArtPackID = 1,
                        ArticleCode = "CODE1",
                        ArticleDesc = "Article 1",
                        PackDesc = "ks",
                        UseBatch = true,
                        Quantity = 10,
                        BatchNum = "asd123",
                        WholeCarrier = false,
                        DocPosition = 0,
                        StoMoveID = 1
                    }
                }
            };
        }

        public virtual async Task<ReceiveSaveItem> SaveItem(ReceiveSaveItem model)
        {
            return new ReceiveSaveItem
            {
                Success = true
            };
        }

        public virtual async Task<ReceiveCancelItem> CancelItem(ReceiveCancelItem model)
        {
            return new ReceiveCancelItem
            {
                Success = true
            };
        }

        public virtual async Task Done(ReceiveDone model)
        {
        }

        public virtual async Task TestDirection(ReceiveTestDirection model)
        {

        }
    }
}

﻿using System;
using S4mp.Client.Interfaces;
using System.Threading.Tasks;
using S4.ProcedureModels.BarCode;

namespace S4mp.MockData
{
    public class BarcodeClient : DataBase, IBarcodeClient
    {
        public Task<BarcodeSet> BarcodeSet(BarcodeSet model)
        {
            throw new NotImplementedException();
        }

        public Task<BarcodeDelete> BarcodeDelete(BarcodeDelete model)
        {
            throw new NotImplementedException();
        }

        public Task<BarcodeMakeCode> BarcodeMakeCode(BarcodeMakeCode model)
        {
            throw new NotImplementedException();
        }

        public Task<BarcodeListPrinters> BarcodeListPrinters(BarcodeListPrinters model)
        {
            throw new NotImplementedException();
        }

        public Task<BarcodePrintLabel> BarcodePrintLabel(BarcodePrintLabel model)
        {
            throw new NotImplementedException();
        }

        public Task<BarcodePrintCarrierLabels> BarcodePrintCarrierLabels(BarcodePrintCarrierLabels model)
        {
            return Task.FromResult(new BarcodePrintCarrierLabels {
                Success = true
            });
        }
    }
}

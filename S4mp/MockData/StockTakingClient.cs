﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Xamarin.Forms;

using Models = S4.ProcedureModels.StockTaking;
using S4.Entities;
using S4.ProcedureModels.Extensions;

namespace S4mp.MockData
{
    public class StockTakingClient : DataBase, Client.Interfaces.IStockTakingClient
    {
        private List<StockTaking> _stockTakings;

        public Task<Models.StockTakingListOpened> GetStockTakingListOpened()
        {
            return Task<Models.StockTakingListOpened>.Run(() =>
            {
                return new Models.StockTakingListOpened() { StockTakingList = _stockTakings };
            });
        }

        public Task<Models.StockTakingScanPos> GetStockTakingPositionItems(Models.StockTakingScanPos model)
        {
            return Task<Models.StockTakingScanPos>.Run(() =>
            {
                return new Models.StockTakingScanPos();
            });
        }

        public Task<Models.StockTakingAddPosItem> AddStockTakingItem(Models.StockTakingAddPosItem model)
        {
            return Task<Models.StockTakingAddPosItem>.Run(() =>
            {
                return new Models.StockTakingAddPosItem();
            });
        }

        public Task<Models.StockTakingDeletePosItem> DeleteStockTakingItem(Models.StockTakingDeletePosItem model)
        {
            return Task<Models.StockTakingDeletePosItem>.Run(() =>
            {
                return new Models.StockTakingDeletePosItem();
            });
        }

        public Task<Models.StockTakingBegin> StockTakingBegin(Models.StockTakingBegin model)
        {
            return Task<Models.StockTakingBegin>.Run(() =>
            {
                var newItem = new StockTaking()
                {
                    BeginDate = DateTime.Now,
                    EntryDateTime = DateTime.Now,
                    EntryUserID = new WorkerClient()._data.First().WorkerID,
                    StotakDesc = model.StotakDesc,
                    StotakID = _stockTakings.Max(i => i.StotakID) + 1,
                    StotakStatus = StockTaking.STOCKTAKING_STATUS_OPENED
                };
                _stockTakings.Add(newItem);
                foreach (var posID in model.PositionIDs)
                    (DependencyService.Get<Client.Interfaces.IPositionClient>() as PositionClient)._positions.Where(i => i.PositionID == posID).First().PosStatus = Position.POS_STATUS_STOCKTAKING;
                model.Success = true;
                model.StotakID = newItem.StotakID;
                return model;
            });
        }

        public async Task<Models.StockTakingPositionOK> RollbackStockTakingPositionItems(Models.StockTakingPositionOK model)
        {
            return new Models.StockTakingPositionOK
            {
                Success = true,
                BadPosition = false
            };
        }

        protected override void CreateData()
        {
            var workerClient = new WorkerClient();

            _stockTakings = new List<StockTaking>()
            {
                new StockTaking()
                {
                    StotakID = 1,
                    StotakStatus = StockTaking.STOCKTAKING_STATUS_OPENED,
                    StotakDesc = "Stock Taking 1",
                    BeginDate = DateTime.Today.AddDays(-1),
                    EntryUserID = workerClient._data.First().WorkerID
                },
                new StockTaking()
                {
                    StotakID = 2,
                    StotakStatus = StockTaking.STOCKTAKING_STATUS_OPENED,
                    StotakDesc = "Stock Taking 2",
                    BeginDate = DateTime.Today,
                    EntryUserID = workerClient._data.First().WorkerID
                }
            };
        }
    }
}

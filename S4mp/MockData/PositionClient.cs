﻿using S4.Entities;
using S4mp.Client.Interfaces;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace S4mp.MockData
{
    public class PositionClient : DataBase, IPositionClient
    {
        internal ObservableCollection<Position> _positions;

        public virtual Task<ObservableCollection<Position>> FindPosition(string posCodePart)
        {
            return Task.FromResult(new ObservableCollection<Position>(_positions.Where(_ => _.PosCode.Contains(posCodePart))));
        }

        public virtual Task<Position> GetPosition(string posCode)
        {
            return Task.FromResult(_positions.SingleOrDefault(_ => _.PosCode.Equals(posCode)));
        }

        protected override void CreateData()
        {
            _positions = new ObservableCollection<Position>
            {
                // multiple
                new Position
                {
                    PositionID = 1,
                    PosDesc = "pos a1a01",
                    PosCode = "a1a01",
                },
                new Position
                {
                    PositionID = 2,
                    PosDesc = "pos a1a02",
                    PosCode = "a1a02",
                },
                new Position
                {
                    PositionID = 3,
                    PosDesc = "pos a1a03",
                    PosCode = "a1a03",
                },
                // single
                new Position
                {
                    PositionID = 4,
                    PosDesc = "pos b1b01",
                    PosCode = "b1b01",
                }
            };
        }
    }
}

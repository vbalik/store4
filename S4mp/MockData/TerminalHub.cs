﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace S4mp.MockData
{
    class TerminalHub : Interfaces.ITerminalHub
    {
        public Task ConnectAsync()
        {
            return Task.Run(() => { });
        }

        public Task DisconnectAsync()
        {
            return Task.Run(() => { });
        }

        public Task InitHubConnection()
        {
            return Task.Run(() => { });
        }

        public Task LogOn()
        {
            return Task.Run(() => { });
        }
    }
}

﻿using System;
using S4.Entities.Helpers;
using S4.ProcedureModels.Exped;
using S4mp.Client.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using S4.Entities.Models;

namespace S4mp.MockData
{
    public class DirectionClient : DataBase, IDirectionClient
    {
        public virtual Task<List<DocumentInfo>> ExpeditionListDirects()
        {
            return Task.FromResult(new List<DocumentInfo>
            {
                new DocumentInfo
                {
                    DocumentID = 1,
                    DocumentName = "DOC 1",
                    DocumentDetail = "DOC 1",
                    DocumentDetail2 = "DOC 1",
                    StoMoveID = 0
                },
                new DocumentInfo
                {
                    DocumentID = 2,
                    DocumentName = "DOC 2",
                    DocumentDetail = "DOC 2",
                    DocumentDetail2 = "DOC 2",
                    StoMoveID = 0
                },
                new DocumentInfo
                {
                    DocumentID = 3,
                    DocumentName = "DOC 3",
                    DocumentDetail = "DOC 3",
                    DocumentDetail2 = "DOC 3",
                    StoMoveID = 0
                }
            });
        }

        public virtual Task<ExpedDone> ExpeditionDone(ExpedDone model)
        {
            return Task.FromResult(new ExpedDone
            {
                Success = true
            });
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace S4mp.MockData
{
    public class WorkerClient : DataBase, Client.Interfaces.IWorkerClient
    {
        internal List<S4.Entities.Worker> _data;

        public Task<S4.ProcedureModels.Login.LoginModel> TryLogOn(S4.ProcedureModels.Login.LoginModel loginModel)
        {
            return Task<S4.ProcedureModels.Login.LoginModel>.Run(() =>
            {
                var worker = _data.Where(i => i.WorkerLogon == loginModel.UserName && i.Password == loginModel.Password).FirstOrDefault();
                if (worker == null)
                    return new S4.ProcedureModels.Login.LoginModel();

                return new S4.ProcedureModels.Login.LoginModel()
                {
                    Success = true,
                    Worker = worker
                };
            });
        }

        protected override void CreateData()
        {
            _data = new List<S4.Entities.Worker>()
            {
                { new S4.Entities.Worker() { WorkerID = "1", WorkerLogon = "w1", Password = "p1", WorkerName = "worker1"} },
                { new S4.Entities.Worker() { WorkerID = "2", WorkerLogon = "a", Password = "1111", WorkerName = "Administrator"} }
            };
        }
    }
}

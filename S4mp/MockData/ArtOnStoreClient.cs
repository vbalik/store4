﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using S4.Entities.Views;
using S4mp.Client;

namespace S4mp.MockData
{
    public class ArtOnStoreClient : DataBase, IArtOnStoreClient
    {
        private List<ArtOnStoreView> _list;

        public virtual Task<List<ArtOnStoreView>> GetOnPosition(int positionID)
        {
            return Task.FromResult(_list.Where(_ => _.PositionID == positionID).ToList());
        }

        public virtual Task<List<ArtOnStoreView>> GetByArticle(int articleID)
        {
            throw new NotImplementedException();
        }

        public virtual Task<List<ArtOnStoreView>> GetByCarrier(string carrierNum)
        {
            return Task.FromResult(_list.Where(_ => _.CarrierNum == carrierNum).ToList());
        }

        protected override void CreateData()
        {
            _list = new List<ArtOnStoreView>
            {
                new ArtOnStoreView
                {
                    PositionID = 2,
                    PosCode = "a1a01",
                    BatchNum = "101010",
                    ExpirationDate = DateTime.Parse("2020-01-01"),
                    CarrierNum = "12345678901234567890",
                    PackDesc = "ks",
                    ArtPackID = 1,
                    ArticleCode = "CODE123",
                    ArticleDesc = "article",
                    Quantity = (decimal) 1.2,
                    UseBatch = true,
                    UseExpiration = true
                },
                new ArtOnStoreView
                {
                    PositionID = 2,
                    PosCode = "a1a02",
                    BatchNum = "202020",
                    ExpirationDate = DateTime.Parse("2020-01-01"),
                    CarrierNum = "12345678901234567890",
                    PackDesc = "bal",
                    ArtPackID = 2,
                    ArticleCode = "CODE456",
                    ArticleDesc = "article 2",
                    Quantity = (decimal) 1.2,
                    UseBatch = true,
                    UseExpiration = true
                },
                new ArtOnStoreView
                {
                    PositionID = 4,
                    PosCode = "a1a02",
                    BatchNum = "202020",
                    ExpirationDate = DateTime.Parse("2020-01-01"),
                    CarrierNum = "1234567890",
                    PackDesc = "bal",
                    ArtPackID = 2,
                    ArticleCode = "CODE456",
                    ArticleDesc = "article 2",
                    Quantity = (decimal) 1.2,
                    UseBatch = true,
                    UseExpiration = true
                }
            };
        }
    }
}

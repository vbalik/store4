﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace S4mp.MockData
{
    public class LocalDatabase : DAL.ILocalDatabase
    {
        public Task<List<T>> GetAsync<T>() where T : new()
        {
            return Task<List<T>>.Run(() => new List<T>());
        }

        public Task<List<T>> Query<T>(string query, params dynamic[] parameters) where T : new()
        {
            throw new NotImplementedException();
        }

        public Task<T> FindAsync<T>(object pk) where T : new()
        {
            return Task<T>.Run(() => default(T));
        }

        public Task<int> SaveAsync<T>(T item) where T : new()
        {
            return Task<int>.Run(() => default(int));
        }

        public Task<int> DeleteAsync<T>(T item) where T : new()
        {
            return Task<int>.Run(() => default(int));
        }

        public Task<int> Execute(string cmd, params object[] args)
        {
            return Task<int>.Run(() => default(int));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using S4.Entities.Helpers;

namespace S4mp.MockData
{
    public class TerminalClient : DataBase, Client.Interfaces.ITerminalClient
    {
        private List<S4.Entities.Helpers.TerminalMenuItem> _data;

        public Task<TerminalDefaultSettings> GetDefaultSettings()
        {
            return Task.FromResult(new TerminalDefaultSettings
            {
                EnableAssignDirectionUser = true
            });
        }

        public Task<List<S4.Entities.Helpers.TerminalMenuItem>> MenuItems()
        {
            return Task<List<S4.Entities.Helpers.TerminalMenuItem>>.Run(() =>
            {
                return _data;
            });
        }

        protected override void CreateData()
        {
            _data = new List<S4.Entities.Helpers.TerminalMenuItem>()
            {
                new TerminalMenuItem() { Group = "Příjem", Text = "Volný příjem", Image = "arrowcirclerightsolid.png", PageName = "Pages.Direction.Receive.ReceivePage" },
                new TerminalMenuItem() { Group = "Příjem", Text = "Naskladnění", Image = "arrowcirclerightsolid.png", PageName = "Pages.Direction.StoreIn.StoreInPage", JobID = S4.Entities.DirectionAssign.JOB_ID_STORE_IN },
                new TerminalMenuItem() { Group = "Výdej", Text = "Vychystání", Image = "arrowcircleleftsolid.png", PageName = "Pages.Direction.Dispatch.DispatchPage", JobID = S4.Entities.DirectionAssign.JOB_ID_DISPATCH },
                new TerminalMenuItem() { Group = "Výdej", Text = "Kontrola", Image = "taskssolid.png", PageName = "Pages.Direction.DispatchCheck.DispatchCheckPage", JobID = S4.Entities.DirectionAssign.JOB_ID_DISPATCH_CHECK },
                new TerminalMenuItem() { Group = "Výdej", Text = "Expedice", Image = "truckloadingsolid.png", PageName = "Pages.Direction.Exped.ExpeditionPage" },
                new TerminalMenuItem() { Group = "Ukázat", Text = "Obsah pozice", Image = "stackexchangebrands.png", PageName = "Pages.OnStock.OnPositionPage" },
                new TerminalMenuItem() { Group = "Ukázat", Text = "Obsah palety", Image = "stackexchangebrands.png", PageName = "Pages.OnStock.OnCarrierPage" },
                new TerminalMenuItem() { Group = "Ukázat", Text = "Ukázat kartu", Image = "cubesolid.png", PageName = "Pages.Article.ShowArticlePage" },
                new TerminalMenuItem() { Group = "Ukázat", Text = "Skladem", Image = "warehousesolid.png", PageName = "Pages.OnStock.ArtOnStockPage" },
                new TerminalMenuItem() { Group = "Servis", Text = "Vratky", Image = "arrowcirclerightsolid.png", PageName = "Pages.Direction.Receive.ReceivePage", PageParams = "VRAT" },
                new TerminalMenuItem() { Group = "Servis", Text = "Inventury", Image = "questioncirclesolid.png", PageName = "Pages.StockTaking.StockTakingListPage" },
                new TerminalMenuItem() { Group = "Servis", Text = "Přeskladnění", Image = "arrowsaltsolid.png", PageName = "Pages.Direction.StoreMove.StoreMovePage" },
                new TerminalMenuItem() { Group = "Servis", Text = "Nastavení kódů", Image = "cog.png", PageName = "Pages.Article.SetArticlePage" },
                new TerminalMenuItem() { Group = "Servis", Text = "Tisk paletových štítků", Image = "print.png", PageName = "Pages.Print.PrintBarcodeCarrierLabelsPage" }
            };
        }
    }
}

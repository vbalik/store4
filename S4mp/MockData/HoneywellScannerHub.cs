﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace S4mp.MockData
{
    class HoneywellScannerHub : Interfaces.IHoneywellScannerHub
    {
        public Task ConnectAsync()
        {
            return Task.Run(() => { });
        }

        public Task DisconnectAsync()
        {
            return Task.Run(() => { });
        }

        public Task InitHubConnection()
        {
            return Task.Run(() => { });
        }
    }
}

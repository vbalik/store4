﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

using Models = S4.ProcedureModels.DispCheck;
using S4.Entities;
using S4.ProcedureModels.Extensions;
using S4.ProcedureModels.DispCheck;

namespace S4mp.MockData
{
    public class DispatchCheckClient : DataBase, Client.Interfaces.IDispatchCheckClient
    {
        private List<Direction> _directions;
        private List<DirectionAssign> _assigns;
        private List<DirectionItem> _items;
        private ArticleClient _articleClient;

        public Task<Models.DispCheckListDirections> DispCheckListDirections(Models.DispCheckListDirections model)
        {
            return Task<Models.DispCheckListDirections>.Run(() =>
            {
                var directions = _directions
                .Where(d => (d.DocStatus == Direction.DR_STATUS_DISP_CHECK_WAIT || d.DocStatus == Direction.DR_STATUS_DISP_CHECK_PROC) &&
                            _assigns.Where(a => a.WorkerID == model.WorkerID).Select(a => a.DirectionID).Contains(d.DirectionID));

                var docs = directions.Select(i => new S4.Entities.Helpers.DocumentInfo()
                {
                    DocumentID = i.DirectionID,
                    DocumentName = i.DocInfo(),
                    DocumentDetail = "Partner"
                }).ToList();

                return new Models.DispCheckListDirections() { Directions = docs, Success = true };
            });
        }

        public Task<Models.DispCheckProc> DispCheckProc(Models.DispCheckProc model)
        {
            return Task<Models.DispCheckProc>.Run(() =>
            {
                var direction = _directions.Where(d => d.DirectionID == model.DirectionID && (d.DocStatus == Direction.DR_STATUS_DISP_CHECK_WAIT || d.DocStatus == Direction.DR_STATUS_DISP_CHECK_PROC)).FirstOrDefault();
                if (direction == null)
                    return new Models.DispCheckProc() { ErrorText = $"Doklad nebyl nalezen; DirectionID: {model.DirectionID}" };

                direction.DocStatus = Direction.DR_STATUS_DISP_CHECK_PROC;

                return new Models.DispCheckProc() { CheckMethod = Direction.DispCheckMethodsEnum.ReadBarCodes, Success = true };
            });
        }

        public Task<Models.DispCheckGetItems> DispCheckGetItems(Models.DispCheckGetItems model)
        {
            return Task<Models.DispCheckGetItems>.Run(() =>
            {
                var items = _items.Where(i => i.DirectionID == model.DirectionID);
                var result = new Models.DispCheckGetItems();
                result.ItemInfoList = new List<S4.Entities.Helpers.ItemInfo>();
                foreach (var item in items)
                {
                    var articlePacking = _articleClient._articlePackings.Where(i => i.ArtPackID == item.ArtPackID).First();
                    var articleModel = _articleClient.GetArticleModel(articlePacking.ArticleID);
                    articleModel.Wait();
                    var itemInfo = new S4.Entities.Helpers.ItemInfo()
                    {
                        DirectionItemID = item.DirectionItemID,
                        ArtPackID = item.ArtPackID,
                        Quantity = item.Quantity,
                        PackDesc = articlePacking.PackDesc,
                        ArticleCode = item.ArticleCode,
                        ArticleDesc = articleModel.Result.ArticleDesc,
                        UseBatch = articleModel.Result.UseBatch,
                        UseExpiration = articleModel.Result.UseExpiration,
                        UseSerialNumber = articleModel.Result.UseSerialNumber
                    };

                    itemInfo.ArticlePackingList = articleModel.Result.Packings;

                    if (!string.IsNullOrWhiteSpace(item.PartnerDepart))
                    {
                        model.UseDepartments = true;
                        itemInfo.Remark = item.PartnerDepart;
                    }

                    result.ItemInfoList.Add(itemInfo);
                }

                result.Success = true;
                return result;
            });
        }

        public Task<Models.DispCheckDone> DispCheckDone(Models.DispCheckDone model)
        {
            return Task<Models.DispCheckDone>.Run(() =>
            {
                var direction = _directions.Where(d => d.DirectionID == model.DirectionID && (d.DocStatus == Direction.DR_STATUS_DISP_CHECK_WAIT || d.DocStatus == Direction.DR_STATUS_DISP_CHECK_PROC)).FirstOrDefault();
                if (direction == null)
                    return new Models.DispCheckDone() { ErrorText = $"Doklad nebyl nalezen; DirectionID: {model.DirectionID}" };

                direction.DocStatus = Direction.DR_STATUS_DISP_CHECK_DONE;

                return new Models.DispCheckDone() { Success = true };
            });
        }

        public Task<DispCheckWait> DispCheckWait(DispCheckWait model)
        {
            throw new NotImplementedException();
        }

        public Task<DispCheckAvailableDirectionsDocNumber> FindDirectionsByDocInfo(DispCheckAvailableDirectionsDocNumber model)
        {
            throw new NotImplementedException();
        }

        public Task<DispCheckAvailableDirectionsDocNumber> FindDirectionsByDocNumber(DispCheckAvailableDirectionsDocNumber model)
        {
            throw new NotImplementedException();
        }

        protected override void CreateData()
        {
            _articleClient = new ArticleClient();

            _directions = new List<Direction>()
            {
                new Direction()
                {
                    DirectionID = 1,
                    DocDate = DateTime.Today,
                    DocDirection = -1,
                    DocNumber = 1,
                    DocNumPrefix = "DISP",
                    DocRemark = "Remark",
                    DocStatus = Direction.DR_STATUS_DISP_CHECK_WAIT,
                    DocYear = DateTime.Today.ToString("yy"),
                    EntryDateTime = DateTime.Now,
                    EntryUserID = "1",
                    PartAddrID = 1,
                    PartnerID = 1
                }
            };

            _assigns = new List<DirectionAssign>()
            {
                new DirectionAssign() { DirectionID = 1, WorkerID = "1", JobID = DirectionAssign.JOB_ID_DISPATCH_CHECK }
            };

            _items = new List<DirectionItem>();
            var taskArticle1 = _articleClient.GetArticleModel(_articleClient._articles[0].ArticleID);
            taskArticle1.Wait();
            _items.Add(new DirectionItem()
            {
                ArticleCode = taskArticle1.Result.ArticleCode,
                ArtPackID = taskArticle1.Result.Packings.Where(p => p.MovablePack).First().ArtPackID,
                DirectionID = 1,
                DirectionItemID = 1,
                DocPosition = 1,
                EntryDateTime = DateTime.Now,
                EntryUserID = "1",
                ItemStatus = DirectionItem.DI_STATUS_DISPATCH_SAVED,
                Quantity = 2,
                UnitDesc = "ks",
                UnitPrice = 10
            });
            var taskArticle2 = _articleClient.GetArticleModel(_articleClient._articles[1].ArticleID);
            taskArticle2.Wait();
            _items.Add(new DirectionItem()
            {
                ArticleCode = taskArticle2.Result.ArticleCode,
                ArtPackID = taskArticle2.Result.Packings.Where(p => p.MovablePack).First().ArtPackID,
                DirectionID = 1,
                DirectionItemID = 2,
                DocPosition = 2,
                EntryDateTime = DateTime.Now,
                EntryUserID = "1",
                ItemStatus = DirectionItem.DI_STATUS_DISPATCH_SAVED,
                Quantity = 3,
                UnitDesc = "ks",
                UnitPrice = 5
            });
            _items.Add(new DirectionItem()
            {
                ArticleCode = taskArticle1.Result.ArticleCode,
                ArtPackID = taskArticle1.Result.Packings.Where(p => p.MovablePack).First().ArtPackID,
                DirectionID = 1,
                DirectionItemID = 3,
                DocPosition = 3,
                EntryDateTime = DateTime.Now,
                EntryUserID = "1",
                ItemStatus = DirectionItem.DI_STATUS_DISPATCH_SAVED,
                Quantity = 1,
                UnitDesc = "ks",
                UnitPrice = 10
            });
            _items.Add(new DirectionItem()
            {
                ArticleCode = taskArticle2.Result.ArticleCode,
                ArtPackID = taskArticle2.Result.Packings.Where(p => p.MovablePack).First().ArtPackID,
                DirectionID = 1,
                DirectionItemID = 4,
                DocPosition = 4,
                EntryDateTime = DateTime.Now,
                EntryUserID = "1",
                ItemStatus = DirectionItem.DI_STATUS_DISPATCH_SAVED,
                Quantity = 1,
                UnitDesc = "ks",
                UnitPrice = 5
            });
        }
    }
}

﻿using Newtonsoft.Json;
using S4.Entities;
using S4.Entities.Settings;
using S4mp.Client.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S4mp.MockData
{
    public class ReportClient : DataBase, IReportClient
    {
        internal List<PrinterLocation> _printerLocations;
        internal List<Reports> _reports;

        public Task<List<PrinterLocation>> GetPrinterLocation(string reportType)
        {
            return Task.FromResult(_printerLocations);
        }

        public Task<List<Reports>> GetReports(string reportType)
        {
            return Task.FromResult(_reports.Where(_ => _.ReportType == reportType).ToList());
        }

        protected override void CreateData()
        {
            _reports = new List<Reports>
            {
                new Reports
                {
                    ReportID = 1,
                    ReportType = "carrLabel",
                    Template = "",
                    PrinterTypeID = 1,
                    Settings = JsonConvert.DeserializeObject<ReportSettings>(@"{""reportName"": ""Report 1"" }")
                },
                new Reports
                {
                    ReportID = 2,
                    ReportType = "carrLabel",
                    Template = "",
                    PrinterTypeID = 2,
                    Settings = JsonConvert.DeserializeObject<ReportSettings>(@"{""reportName"": ""Report 2"" }")
                }
            };

            _printerLocations = new List<PrinterLocation>
            {
                new PrinterLocation
                {
                    PrinterLocationID = 1,
                    PrinterLocationDesc = "Printer 1",
                    PrinterPath = "path/1",
                    PrinterTypeID = 1
                },
                new PrinterLocation
                {
                    PrinterLocationID = 2,
                    PrinterLocationDesc = "Printer 2",
                    PrinterPath = "path/2",
                    PrinterTypeID = 2
                }
            };
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

using S4.Entities;
using S4.Entities.Models;

namespace S4mp.MockData
{
    public class ArticleClient : DataBase, Client.Interfaces.IArticleClient
    {
        internal List<Article> _articles;
        internal List<ArticlePacking> _articlePackings;

        public Task<List<ArticlePackingInfo>> SearchByCode(string articleCodePart)
        {
            return Task<List<ArticlePackingInfo>>.Run(() =>
            {
                var items = _articles.Where(i => i.ArticleCode.ToUpper().Contains(articleCodePart.ToUpper()));
                return items.Select(i => ArticlePackingInfo.FromArticleModel(GetModel(i))).ToList();
            });
        }

        public Task<List<ArticlePackingInfo>> SearchByDesc(string articleDescPart)
        {
            return Task<List<ArticlePackingInfo>>.Run(() =>
            {
                var items = _articles.Where(i => i.ArticleDesc.ToUpper().Contains(articleDescPart.ToUpper()));
                return items.Select(i => ArticlePackingInfo.FromArticleModel(GetModel(i))).ToList();
            });
        }

        public Task<List<ArticlePackingInfo>> SearchByBarCode(string barCode)
        {
            return Task<List<ArticlePackingInfo>>.Run(() =>
            {
                var items = _articlePackings.Where(i => i.BarCode.ToUpper().Contains(barCode.ToUpper()));
                return items.Select(i => ArticlePackingInfo.FromArticleModel(GetModel(_articles.Where(a => a.ArticleID == i.ArticleID).Single()))).ToList();
            });
        }

        public Task<ArticlePacking> GetMovablePacking(int articleID)
        {
            return Task<ArticlePackingInfo>.Run(() =>
            {
                return _articlePackings.Where(i => i.ArticleID == articleID && i.MovablePack).FirstOrDefault();
            });
        }

        public Task<ArticlePackingInfo> GetArtPacking(int artPackingID)
        {
            return Task<ArticlePackingInfo>.Run(() =>
            {
                var item = _articlePackings.Where(i => i.ArtPackID == artPackingID).FirstOrDefault();
                return item == null ? null : ArticlePackingInfo.FromArticleModel(GetModel(_articles.Where(a => a.ArticleID == item.ArticleID).Single()));
            });
        }

        public Task<ArticleModel> GetArticleModel(int articleID)
        {
            return Task<ArticleModel>.Run(() =>
            {
                var item = _articles.Where(i => i.ArticleID == articleID).FirstOrDefault();
                return item == null ? null : GetModel(item);
            });
        }

        protected override void CreateData()
        {
            _articles = new List<Article>()
            {
                { new Article() { ArticleCode = "CODE1", ArticleDesc = "Article 1", ArticleID = 1, ArticleStatus = Article.AR_STATUS_OK, UnitDesc = "ks"  } },
                { new Article() { ArticleCode = "CODE2", ArticleDesc = "Article 2", ArticleID = 2, ArticleStatus = Article.AR_STATUS_OK, UnitDesc = "ks", UseSerialNumber = true  } },
                
                { new Article() { ArticleCode = "RECEIVE1", ArticleDesc = "Receive Article 1", ArticleID = 3, ArticleStatus = Article.AR_STATUS_OK, UnitDesc = "ks", UseBatch = true, UseExpiration = true } }
            };
            _articlePackings = new List<ArticlePacking>()
            {
                { new ArticlePacking() { ArticleID = 1, ArtPackID = 1, BarCode = "11232121", BarCodeType = 1, MovablePack = false, PackDesc = "Packing 11", PackStatus = ArticlePacking.AP_STATUS_OK } },
                { new ArticlePacking() { ArticleID = 1, ArtPackID = 2, BarCode = "84627470501466", BarCodeType = 1, MovablePack = true, PackDesc = "Packing 12", PackStatus = ArticlePacking.AP_STATUS_OK } },
                { new ArticlePacking() { ArticleID = 2, ArtPackID = 3, BarCode = "55555", BarCodeType = 1, MovablePack = false, PackDesc = "Packing 21", PackStatus = ArticlePacking.AP_STATUS_OK } },
                { new ArticlePacking() { ArticleID = 2, ArtPackID = 4, BarCode = "66666", BarCodeType = 1, MovablePack = true, PackDesc = "Packing 22", PackStatus = ArticlePacking.AP_STATUS_OK } },
                
                { new ArticlePacking() { ArticleID = 3, ArtPackID = 5, BarCode = "66666666", BarCodeType = 1, MovablePack = true, PackDesc = "ks", PackStatus = ArticlePacking.AP_STATUS_OK } },
                { new ArticlePacking() { ArticleID = 3, ArtPackID = 6, BarCode = "55556666", BarCodeType = 1, MovablePack = false, PackDesc = "bal", PackRelation = 10, PackStatus = ArticlePacking.AP_STATUS_OK } }
            };
        }

        private ArticleModel GetModel(Article article)
        {
            return new ArticleModel()
            {
                ArticleCode = article.ArticleCode,
                ArticleDesc = article.ArticleDesc,
                ArticleID = article.ArticleID,
                ArticleStatus = article.ArticleStatus,
                ArticleType = article.ArticleType,
                ManufID = article.ManufID,
                MixBatch = article.MixBatch,
                Packings = _articlePackings.Where(i => i.ArticleID == article.ArticleID).ToList(),
                SectID = article.SectID,
                Source_id = article.Source_id,
                SpecFeatures = article.SpecFeatures,
                UnitDesc = article.UnitDesc,
                UseBatch = article.UseBatch,
                UseExpiration = article.UseExpiration,
                UseSerialNumber = article.UseSerialNumber
            };
        }
    }
}

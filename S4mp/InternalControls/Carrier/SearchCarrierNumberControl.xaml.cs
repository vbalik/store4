﻿using System;
using System.Text.RegularExpressions;
using System.Windows.Input;

using Xamarin.Forms;

using S4mp.Controls;
using System.Threading.Tasks;
using S4.Core;
using System.Text;

namespace S4mp.InternalControls.Carrier
{
    public partial class SearchCarrierNumberControl : ContentView
    {
        public static readonly BindableProperty ConfirmCarrierNumberCmdProperty = BindableProperty.Create(nameof(ConfirmCarrierNumberCmd), typeof(ICommand), typeof(SearchCarrierNumberControl));
        public ICommand ConfirmCarrierNumberCmd
        {
            get => (ICommand)GetValue(ConfirmCarrierNumberCmdProperty);
            set => SetValue(ConfirmCarrierNumberCmdProperty, value);
        }

        public static readonly BindableProperty WithoutCarrierCmdProperty = BindableProperty.Create(nameof(WithoutCarrierCmd), typeof(ICommand), typeof(SearchCarrierNumberControl));
        public ICommand WithoutCarrierCmd
        {
            get => (ICommand)GetValue(WithoutCarrierCmdProperty);
            set => SetValue(WithoutCarrierCmdProperty, value);
        }

        public static readonly BindableProperty ValueScannedCmdProperty = BindableProperty.Create(nameof(ValueScannedCmd), typeof(ICommand), typeof(SearchCarrierNumberControl));
        public ICommand ValueScannedCmd
        {
            get => (ICommand)GetValue(ValueScannedCmdProperty);
            set => SetValue(ValueScannedCmdProperty, value);
        }

        public static readonly BindableProperty CarrierNumProperty = BindableProperty.Create(nameof(CarrierNum), typeof(string), typeof(SearchCarrierNumberControl));
        public string CarrierNum
        {
            get => (string)GetValue(CarrierNumProperty);
            set => SetValue(CarrierNumProperty, value);
        }
        
        public static readonly BindableProperty ShowWithoutCarrierBtnProperty = BindableProperty.Create(nameof(ShowWithoutCarrierBtn), typeof(bool), typeof(SearchCarrierNumberControl));
        public bool ShowWithoutCarrierBtn
        {
            get => (bool)GetValue(ShowWithoutCarrierBtnProperty);
            set => SetValue(ShowWithoutCarrierBtnProperty, value);
        }

        public static readonly BindableProperty ValidProperty = BindableProperty.Create(nameof(Valid), typeof(bool), typeof(SearchCarrierNumberControl));
        public bool Valid
        {
            get => (bool)GetValue(ValidProperty);
            set => SetValue(ValidProperty, value);
        }

        public static readonly BindableProperty HeaderTextProperty = BindableProperty.Create(nameof(HeaderText), typeof(string), typeof(SearchCarrierNumberControl));
        public string HeaderText
        {
            get => (string)GetValue(HeaderTextProperty);
            set => SetValue(HeaderTextProperty, value);
        }

        public static readonly BindableProperty HeaderTextSubProperty = BindableProperty.Create(nameof(HeaderTextSub), typeof(string), typeof(SearchCarrierNumberControl));
        public string HeaderTextSub
        {
            get => (string)GetValue(HeaderTextSubProperty);
            set => SetValue(HeaderTextSubProperty, value);
        }

        public event EventHandler<string> ConfirmButtonEvent;
        public event EventHandler TextChanged;

        public SearchCarrierNumberControl()
        {
            InitializeComponent();

            ConfirmCarrierNumberCmd = new Command(ConfirmCarrierNumber);
            WithoutCarrierCmd = new Command(WithoutCarrier);
            ValueScannedCmd = new Command(ValueScanned);

            ShowWithoutCarrierBtn = true;

            Entry_Focus();
        }

        public void ExternalScan(IScannerResolver scannerResolver)
        {
            scannerView.ID = scannerResolver.ID;
            scannerView.Text = scannerResolver.Text;
            scannerView.RawData = scannerResolver.RawData;
            scannerView.EAN = scannerResolver.EAN;
            scannerView.Error = scannerResolver.Error;

            ValueScanned(scannerView);
        }

        private bool Validate()
        {
            var isNumeric = Regex.IsMatch(edCarrierNumber.Text ?? string.Empty, @"^\d+$");

            if (!isNumeric || (isNumeric && edCarrierNumber.Text.Length != 20))
            {
                carrierNumberInputFrame.HasError();
                return false;
            }
            carrierNumberInputFrame.HasError(false);
            return true;
        }

        private void ValueScanned(object sender)
        {
            var scannedValue = Encoding.ASCII.GetString(scannerView.RawData);

            // is numeric value
            if(Regex.IsMatch(scannedValue ?? string.Empty, @"^\d+$"))
                edCarrierNumber.Text = scannedValue;

            if (string.IsNullOrEmpty(edCarrierNumber.Text))
                return;

            ConfirmCarrierNumberCmd.Execute(null);
        }

        private void ConfirmCarrierNumber()
        {
            ConfirmButtonEvent.Invoke(this, edCarrierNumber.Text);
        }

        private void WithoutCarrier()
        {
            Valid = true;
            CarrierNum = string.Empty;
            ConfirmButtonEvent.Invoke(this, string.Empty);
        }

        #region UI Handles

        private void EdCarrierNumber_Completed(object sender, EventArgs e)
        {
            ConfirmCarrierNumberCmd.Execute(null);
        }

        /// <summary>
        /// On focus entry reset error fields
        /// </summary>
        private void Entry_Focused(object sender, FocusEventArgs e)
        {
            edCarrierNumber.Text = string.Empty;
            carrierNumberInputFrame.HasError(false);
        }

        private void EdCarrierNumber_TextChanged(object sender, TextChangedEventArgs e)
        {
            CarrierNum = e.NewTextValue;
            Valid = Validate();
            TextChanged?.Invoke(this, e);
        }

        /// <summary>
        /// Focus entry
        /// </summary>
        private async void Entry_Focus()
        {
            await Task.Delay(600);
            edCarrierNumber.Focus();
        }

        #endregion


    }
}
﻿using S4.Entities.Helpers;
using S4mp.Core.ViewModel;
namespace S4mp.InternalControls
{
    public class SelectDocumentControlVM : BaseViewModel
    {
        private DocumentInfo _document;
        public DocumentInfo Document
        {
            get => _document;
            set => SetField(ref _document, value);
        }

        private bool _setWeightBoxCountData;
        public bool SetWeightBoxCountData
        {
            get => _setWeightBoxCountData;
            set => SetField(ref _setWeightBoxCountData, value);
        }
    }
}

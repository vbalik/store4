﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using System.Runtime.CompilerServices;

using Xamarin.Forms;

using S4mp.Controls;
using S4mp.Controls.Dialogs;
using S4mp.Controls.Scanner;
using S4.Core;
using S4mp.Dialogs;
using S4mp.Client;
using static S4.ProcedureModels.DispCheck.DispCheckGetItems;
using static S4.ProcedureModels.DispCheck.DispCheckDone;

namespace S4mp.InternalControls.DispCheck
{
    public partial class DispCheckControl : ContentView
    {
        private DispCheckItem _temporary_printLabelItem = null;

        //dependent resolver for scanning
        private IScannerResolver _scannerResolver;
        //button for scanning if camera scanner
        private Button _btnScan;

        public DispCheckControl()
        {
            InitializeComponent();

            //create scanning resolver
            _scannerResolver = DependencyService.Get<IScannerResolver>();
            ViewModel.QuantityChanged += (s, e) => TestCheckDone();
            ViewModel.QuantityChanged += SaveItem;

            MenuCommand = new Command<DispCheckItem>(ItemActionMenu);
        }

        public event EventHandler<bool> Done;

        public S4.Entities.Direction.DispCheckMethodsEnum CheckMethod
        {
            get => ViewModel.DispCheckMethod;
            set
            {
                ViewModel.DispCheckMethod = value;
                //start or stop scanner listening
                _scannerResolver.Init(value == S4.Entities.Direction.DispCheckMethodsEnum.ReadBarCodes || value == S4.Entities.Direction.DispCheckMethodsEnum.ReadBarCodes2, ScannerResolver_Scanned2);
#if !DATALOGIC && !HONEYWELL
                if (value == S4.Entities.Direction.DispCheckMethodsEnum.ReadBarCodes || value == S4.Entities.Direction.DispCheckMethodsEnum.ReadBarCodes2)
                {
                    if (_btnScan == null)
                        _btnScan = new Button
                        {
                            HorizontalOptions = LayoutOptions.CenterAndExpand,
                            Text = "SCAN",
                            Command = new Command(() =>
                            {
                                _scannerResolver.Scan();
                            })
                        };
                    if (!(Content as StackLayout).Children.Contains(_btnScan))
                        (Content as StackLayout).Children.Insert(0, _btnScan);
                }
#endif
            }
        }

        public int DirectionID { get; set; }

        public DispCheckViewModel ViewModel => (DispCheckViewModel)BindingContext;

        public async void SetItems(List<S4.Entities.Helpers.ItemInfo> items, List<CheckDetail> checkDetails)
        {
            try
            {
                var savedItems = await DependencyService.Get<DAL.ILocalDatabase>().GetAsync<S4mp.Entities.DispCheck>();
                var savedItemSerials = await DependencyService.Get<DAL.ILocalDatabase>()
                    .Query<Entities.DispCheckSerial>($"select * from {nameof(Entities.DispCheckSerial)} where {nameof(Entities.DispCheckSerial.DirectionID)} = @0", DirectionID);

                var dispCheckItems = new System.Collections.ObjectModel.ObservableCollection<DispCheckItem>(items
                    .GroupBy(i => i.ArtPackID, i => i, (artPackID, itemInfos) => {

                        var item = new DispCheckItem(ViewModel)
                        {
                            ArticleCode = itemInfos.First().ArticleCode,
                            ArticleDesc = itemInfos.First().ArticleDesc,
                            ArticlePackingList = itemInfos.First().ArticlePackingList,
                            ArtPackID = artPackID,
                            PackDesc = itemInfos.First().PackDesc,
                            SavedID = savedItems.Where(si => si.DirectionID == DirectionID && si.ArtPackID == itemInfos.First().ArtPackID).FirstOrDefault()?.ID ?? 0,
                            FoundQuantity = savedItems.Where(si => si.DirectionID == DirectionID && si.ArtPackID == itemInfos.First().ArtPackID).FirstOrDefault()?.FoundQuantity,
                            UseSerialNumber = itemInfos.First().UseSerialNumber,
                            UseBatch = itemInfos.First().UseBatch,
                            UseExpiration = itemInfos.First().UseExpiration
                        };

                        // add serials (onchange event on init)
                        _GetDirectionValues(itemInfos, checkDetails, savedItemSerials).ToList().ForEach(_ => item.DirectionValues.Add(_));

                        return item;
                    }));

                ViewModel.Items = dispCheckItems;

                TestCheckDone();
            }
            finally
            {
                Items.IsRefreshing = false;
            }
        }

        private List<DirectionValue> _GetDirectionValues(IEnumerable<S4.Entities.Helpers.ItemInfo> itemInfos,
            List<CheckDetail> checkDetails,
            List<Entities.DispCheckSerial> savedItemSerials)
        {
            var result = new List<DirectionValue>();

            var item = itemInfos.FirstOrDefault();

            var checkDetailsItem = checkDetails.Where(_ => _.ArtPackID == item.ArtPackID).ToList();

            foreach (var checkDetail in checkDetailsItem)
            {
                var directionValue = new DirectionValue()
                {
                    DirectionItemID = item.DirectionItemID,
                    Quantity = checkDetail.Quantity,
                    BatchNum = checkDetail.BatchNum,
                    ExpirationDate = checkDetail.ExpirationDate
                };

                // add serials from local storage
                var serials = savedItemSerials
                    .Where(_ => _.DirectionItemID == item.DirectionItemID)
                    .Select(_ => _.SerialNumber)
                    .ToList();

                if (serials.Any())
                    foreach (var serial in serials)
                    {
                        directionValue.SerialNumbers.Add(new SerialNumberItem
                        {
                            BatchNum = item.BatchNum,
                            ExpirationDate = item.ExpirationDate,
                            SerialNumber = serial
                        });
                    }

                result.Add(directionValue);
            }

            return result;
        }

        public void Search()
        {
            ShowSelectArticleDialog(_scannerResolver, false);
        }

        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);

            if (propertyName.Equals("Renderer", StringComparison.OrdinalIgnoreCase))
            {
                //stop listening on closing
                var rr = DependencyService.Get<IRendererResolver>();
                if (!rr.HasRenderer(this))
                    _scannerResolver.Init(false, ScannerResolver_Scanned2);
            }
        }

        private async void ScannerResolver_Scanned2(object sender, BarCodeTypeEnum e)
        {
            activityIndicator.Show();
            try
            {
                var articleInfoList = await DependencyService.Get<S4mp.Client.Interfaces.IArticleClient>().SearchByBarCode((sender as IScannerResolver).Text);
                if (articleInfoList.Count == 0)
                {
                    Singleton<Helpers.Audio>.Instance.PlayError();
                    ShowSelectArticleDialog(_scannerResolver, true);
                }
                else
                {
                    ProcessArticle(articleInfoList[0]);
                }
            }
            finally
            {
                activityIndicator.Hide();
            }
        }

        /// <summary>
        /// opens dialog for article searching
        /// </summary>
        /// <param name="scannerResolver">null or resolver if calling after scanning</param>
        private async void ShowSelectArticleDialog(IScannerResolver scannerResolver, bool unsuccessfullSearch)
        {
            if (scannerResolver != null)
            {
                //stop listening
                scannerResolver.Init(false, ScannerResolver_Scanned2);
            }

            //create and show dialog
            var getArticleDialog = new Dialogs.SelectArticleDialog();
            getArticleDialog.Closed += GetArticleDialog_Closed;
            getArticleDialog.SetHeaderText("Kontrola", "Zadejte kartu");
            await Navigation.PushModalAsync(getArticleDialog, false);

            //do article searching if called after scanning
            if (scannerResolver != null && unsuccessfullSearch)
                getArticleDialog.UnsuccessfullExtScan(scannerResolver.Text);
        }

        private void GetArticleDialog_Closed(object sender, Controls.Common.ButtonsBar.ButtonsEnum e)
        {
            if (e == Controls.Common.ButtonsBar.ButtonsEnum.OK)
            {
                ProcessArticle((sender as Dialogs.SelectArticleDialog).GetArticleResult.ArticlePackingInfo);
            }

            //start scanner listening
            _scannerResolver.Init(true, ScannerResolver_Scanned2);
        }

        private void ProcessArticle(S4.Entities.Models.ArticlePackingInfo result)
        {
            var articleItem = ViewModel.Items.Where(i => i.ArticlePackingList.Any(p => p.ArtPackID == result.ArticlePacking.ArtPackID)).FirstOrDefault();
            if (articleItem == null)
            {
                Singleton<Helpers.Audio>.Instance.PlayError();
                App.Current.MainPage.DisplayAlert("Kontrola", $"Položka ({result.ArticlePacking.PackDesc}) \"{result.ArticleInfo}\" nebyla v dokladu nalezena!", "OK");
                return;
            }

            var doneItem = ViewModel.Items.Where(i => i.ArtPackID == articleItem.ArtPackID && i.Checked).FirstOrDefault();
            if (doneItem != null)
            {
                Singleton<Helpers.Audio>.Instance.PlayError();
                App.Current.MainPage.DisplayAlert("Kontrola", $"Položka ({result.ArticlePacking.PackDesc}) \"{result.ArticleInfo}\" již byla zpracována!", "OK");
                return;
            }

            ShowQuantityPopup(articleItem, result);
        }

        private void ShowQuantityPopup(DispCheckItem articleItem, S4.Entities.Models.ArticlePackingInfo result)
        {
            var popup = new EntryPopup($"Zadejte množství ({result.ArticlePacking.PackDesc}) položky \"{result.ArticleInfo}\"", string.Empty);
            decimal quantity = 0;
            popup.PopupClosed += (o, closedArgs) =>
            {
                if (closedArgs.Button == Core.Consts.OK_BUTTON)
                {
                    articleItem.FoundQuantity = quantity;
                }
            };

            popup.Show((text) =>
            {
                if (!decimal.TryParse(text, out quantity))
                    return false;
                if (quantity != articleItem.Quantity)
                {
                    popup.Dismiss(); // dismiss current opened

                    Singleton<Helpers.Audio>.Instance.PlayError();

                    ShowQuantityPopup(articleItem, result);

                    App.Current.MainPage.DisplayAlert("Kontrola", "Nesprávné množství!", "OK");

                    return false;
                }

                return true;
            });
        }

        private void TestCheckDone()
        {
            Done?.Invoke(this, !ViewModel.Items.Any(i => !i.Checked || (i.UseSerialNumber ^ i.SerialsOK)));
        }

        private async void SaveItem(object sender, EventArgs e)
        {
            var dispCheckItem = sender as DispCheckItem;
            if ((e as System.ComponentModel.PropertyChangedEventArgs).PropertyName != nameof(dispCheckItem.FoundQuantity))
                return;

            var dispCheck = new Entities.DispCheck()
            {
                ID = dispCheckItem.SavedID,
                DirectionID = DirectionID,
                ArtPackID = dispCheckItem.ArtPackID,
                FoundQuantity = dispCheckItem.FoundQuantity,
                Updated = DateTime.Now
            };
            await DependencyService.Get<DAL.ILocalDatabase>().SaveAsync(dispCheck);
            dispCheckItem.SavedID = dispCheck.ID;
        }

        private async void Entry_Unfocused(object sender, FocusEventArgs e)
        {
            var entry = (Entry)sender;
            if (!decimal.TryParse(entry.Text, out decimal quantity))
            {
                Singleton<Helpers.Audio>.Instance.PlayError();
                await App.Current.MainPage.DisplayAlert("Kontrola", "Hodnota musí být číslo!", "OK");
                entry.Text = null;
                return;
            }
            (entry.BindingContext as DispCheckItem).FoundQuantity = quantity;
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            //stop listening
            _scannerResolver.Init(false, ScannerResolver_Scanned2);

            var setSerialsDialog = new Dialogs.SetSerialsDialog();
            setSerialsDialog.Closed += SetSerialsDialog_Closed;
            setSerialsDialog.SetItem(((Button)sender).BindingContext as DispCheckItem, DirectionID);
            await Navigation.PushModalAsync(setSerialsDialog, false);
        }

        private void SetSerialsDialog_Closed(object sender, Controls.Common.ButtonsBar.ButtonsEnum e)
        {
            //start scanner listening
            _scannerResolver.Init(true, ScannerResolver_Scanned2);
        }


        /// <summary>
        /// Item menu
        /// </summary>

        public static readonly BindableProperty MenuCommandProperty =
            BindableProperty.Create(nameof(MenuCommand), typeof(ICommand), typeof(DispCheckControl), null);

        public ICommand MenuCommand
        {
            get { return (ICommand)GetValue(MenuCommandProperty); }
            set { SetValue(MenuCommandProperty, value); }
        }


        // helper method for invoking commands safely
        public static void Execute(ICommand command, object value)
        {
            if (command == null) return;
            if (command.CanExecute(value))
            {
                command.Execute(value);
            }
        }

        public Command OnLongPress => new Command((value) => Execute(MenuCommand, value));

        protected async void ItemActionMenu(DispCheckItem item)
        {
            var action = await App.Current.MainPage.DisplayActionSheet(null, "Zrušit", null, "Vytisknout štítek");

            switch (action)
            {
                case "Vytisknout štítek":
                    _printItemLabel(item);
                    break;
            }
        }

        private async void _printItemLabel(DispCheckItem item)
        {
            _temporary_printLabelItem = item;

            Device.BeginInvokeOnMainThread(async () =>
            {
                var setPrintDialog = new SetPrintDialog();
                setPrintDialog.Closed += SetPrintDialog_Closed;
                await Navigation.PushModalAsync(setPrintDialog, false);
            });
        }

        private async void SetPrintDialog_Closed(object sender, Controls.Common.ButtonsBar.ButtonsEnum e)
        {
            if (e == Controls.Common.ButtonsBar.ButtonsEnum.OK)
            {
                var printerID = (sender as SetPrintDialog).PrinterID;
                var count = (sender as SetPrintDialog).Count;
                var client = new BarcodeClient();
                var result = await client.BarcodePrintLabel(new S4.ProcedureModels.BarCode.BarcodePrintLabel
                {
                    ArtPackID = _temporary_printLabelItem.ArtPackID,
                    LabelsQuant = count,
                    PrinterLocationID = printerID
                });

                if (!result.Success)
                {
                    await App.Current.MainPage.DisplayAlert("Akce skončila s chybou!", result.ErrorText, "OK");
                    return;
                }
            }

            // clear temp print item
            _temporary_printLabelItem = null;
        }
    }
}
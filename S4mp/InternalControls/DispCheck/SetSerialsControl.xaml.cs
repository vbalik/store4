﻿using System;
using System.Linq;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using S4mp.Controls.Scanner;
using static S4.ProcedureModels.DispCheck.DispCheckDone;
using System.Globalization;
using S4mp.Entities;
using S4mp.Core;
using S4mp.Core.EANCode;

namespace S4mp.InternalControls.DispCheck
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SetSerialsControl : ContentView
    {
        private int _directionID { get; set; }

        public SetSerialsControl()
        {
            InitializeComponent();

            scannerView.ValueScannedCmd = new Command(SerialScanned);

            lbStatus.Text = $"Naskenujte sériové číslo ...";
        }

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();

            SetText(ViewModel.SerialNumbers.Count(), ViewModel.Quantity);
        }

        public void SetItem(DispCheckItem dispCheckItem, int directionID)
        {
            BindingContext = dispCheckItem;
            _directionID = directionID;
            HeaderControl.Text = $"Sériová čísla položky \"{ViewModel.ArticleInfo}\"";
        }

        // @ DEPRECATED v22
        //
        //public void AddItem()
        //{
        //    var popup = new EntryPopup($"Zadejte sériové číslo", string.Empty);
        //    string serialNumber = null;
        //    popup.PopupClosed += (o, closedArgs) =>
        //    {
        //        if (closedArgs.Button == Core.Consts.OK_BUTTON)
        //        {
        //            /// TODO /////
        //            //AddSerialNumber(serialNumber);
        //        }
        //    };

        //    popup.Show((text) =>
        //    {
        //        serialNumber = text;
        //        return !string.IsNullOrWhiteSpace(serialNumber);
        //    });
        //}

        public void SetText(int actual, decimal total)
        {
            if (actual == total)
            {
                lbStatus.BackgroundColor = Color.LimeGreen;
                lbStatus.Text = $"HOTOVO čísla načtena";
            }
            else
            {
                lbStatus.BackgroundColor = Color.Blue;
                lbStatus.Text = $"Zbývá {total-actual:n0} z {total:n0} čísel..";
            }
        }

        private DispCheckItem ViewModel => (DispCheckItem)BindingContext;

        private async void Button_Clicked(object sender, EventArgs e)
        {
            var serialNumber = ((SerialNumberItem)((Button)sender).BindingContext);

            foreach (var directionValue in ViewModel.DirectionValues)
            {
                directionValue.SerialNumbers.Remove(serialNumber);

                // remove serial from local storage
                await DependencyService.Get<DAL.ILocalDatabase>()
                .Query<DispCheckSerial>($"delete from {nameof(DispCheckSerial)} where {nameof(DispCheckSerial.DirectionItemID)} = @0 and {nameof(DispCheckSerial.SerialNumber)} = @1", directionValue.DirectionItemID, serialNumber.SerialNumber);

                SetText(ViewModel.SerialNumbers.Count(), ViewModel.Quantity);
                break;
            }
        }

        private void SerialScanned(object sender)
        {
            var scanner = (ScannerView)sender;

            string serialNumber;
            string batchNum = null;
            DateTime? expirationDate = null;

            if (scanner.EAN == null)
                serialNumber = scanner.Text;
            else
            {
                serialNumber = scanner.EAN[EANCodePartEnum.ShippingCode] == null ? string.Empty : scanner.EAN[EANCodePartEnum.ShippingCode].Value;
                batchNum = scanner.EAN[EANCodePartEnum.BatchNumber] == null ? string.Empty : scanner.EAN[EANCodePartEnum.BatchNumber].Value;
                expirationDate = scanner.EAN[EANCodePartEnum.ExpirationDate] == null ? null : scanner.EAN[EANCodePartEnum.ExpirationDate].ValueDate;
            }

            AddSerialNumber(serialNumber, batchNum, expirationDate);
        }

        private async void AddSerialNumber(string serialNumber, string batchNum, DateTime? expirationDate)
        {
            if (ViewModel.SerialNumbers.Any(_ => _.SerialNumber == serialNumber))
            {
                await Application.Current.MainPage.DisplayAlert("Upozornění ...", $"Sériové číslo {serialNumber} již bylo přidáno!", "Zavřít");
                return;
            }

            var directionValues = ViewModel.DirectionValues.AsQueryable();

            // filter
            if (ViewModel.UseBatch)
                directionValues = directionValues.Where(_ => _.BatchNum == batchNum);

            if (ViewModel.UseExpiration)
                directionValues = directionValues.Where(_ => _.ExpirationDate.Value.ToString("YYYYMDD") == expirationDate.Value.ToString("YYYYMDD"));
            // /filter

            var item = directionValues.FirstOrDefault();

            if (item == null)
            {
                await Application.Current.MainPage.DisplayAlert("Upozornění ...", $"Sériové číslo neodpovídá položce.", "Zavřít");
                return;
            }

            if (item.SerialNumbers.Count() == item.Quantity)
            {
                await Application.Current.MainPage.DisplayAlert("Upozornění ...", $"Všechna sériová čísla již byla vyplněna!", "Zavřít");
                return;
            }

            item.SerialNumbers.Add(new SerialNumberItem
            {
                BatchNum = batchNum,
                ExpirationDate = expirationDate,
                SerialNumber = serialNumber
            });

            // save serial number to local storage
            var serial = new DispCheckSerial
            {
                DirectionID = _directionID,
                DirectionItemID = item.DirectionItemID,
                BatchNum = batchNum,
                ExpirationDate = expirationDate,
                SerialNumber = serialNumber,
                Updated = DateTime.Now
            };

            await DependencyService.Get<DAL.ILocalDatabase>().SaveAsync(serial);

            SetText(ViewModel.SerialNumbers.Count(), ViewModel.Quantity);
        }
    }
}
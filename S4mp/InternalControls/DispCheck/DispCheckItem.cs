﻿using System;
using System.Globalization;
using System.Windows.Input;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;

using S4.Entities;
using static S4.ProcedureModels.DispCheck.DispCheckDone;

namespace S4mp.InternalControls.DispCheck
{
    public class DispCheckItem : System.ComponentModel.INotifyPropertyChanged
    {
        public DispCheckItem(DispCheckViewModel parent)
        {
            _directionValues = new ObservableCollection<DirectionValue>();
            _directionValues.CollectionChanged += _directionValues_CollectionChanged;
            QuantityEnabled = parent.QuantityEnabled;
        }

        public string ArticleCode { get; set; }
        public string ArticleDesc { get; set; }
        public int ArtPackID { get; set; }
        public string PackDesc { get; set; }
        public bool UseSerialNumber { get; set; }
        public bool UseBatch { get; set; }
        public bool UseExpiration { get; set; }
        public decimal Quantity => DirectionValues.Select(i => i.Quantity).Sum();
        public string ArticleInfo => $"{ArticleCode} - {ArticleDesc}";
        public List<ArticlePacking> ArticlePackingList { get; set; }
        private ObservableCollection<DirectionValue> _directionValues;
        public ObservableCollection<DirectionValue> DirectionValues
        {
            get => _directionValues;
            set
            {
                _directionValues = value;
                _directionValues.CollectionChanged += _directionValues_CollectionChanged;
                PropertyChanged?.Invoke(this, new System.ComponentModel.PropertyChangedEventArgs(nameof(DirectionValues)));
                foreach (var item in _directionValues)
                    item.PropertyChanged += (s, e) => 
                    { 
                        PropertyChanged?.Invoke(this, e); 
                        SetSerialsOK(); 
                    };
            }
        }

        public int SavedID { get; set; }

        private decimal? _foundQuantity;
        public decimal? FoundQuantity
        {
            get => _foundQuantity;
            set
            {
                _foundQuantity = value;
                PropertyChanged?.Invoke(this, new System.ComponentModel.PropertyChangedEventArgs(nameof(FoundQuantity)));
                Checked = FoundQuantity == Quantity;
            }
        }

        public List<SerialNumberItem> SerialNumbers => DirectionValues.SelectMany(_ => _.SerialNumbers).ToList();

        private bool _checked;
        public bool Checked
        {
            get => Quantity == FoundQuantity;
            set
            {
                _checked = value;
                PropertyChanged?.Invoke(this, new System.ComponentModel.PropertyChangedEventArgs(nameof(Checked)));
            }
        }

        private bool _quantityEnabled;
        public bool QuantityEnabled
        {
            get => _quantityEnabled;
            set
            {
                _quantityEnabled = value;
                PropertyChanged?.Invoke(this, new System.ComponentModel.PropertyChangedEventArgs(nameof(QuantityEnabled)));
            }
        }

        private bool _serialsOK;
        public bool SerialsOK
        {
            get => _serialsOK;
            set
            {
                _serialsOK = value;
                PropertyChanged?.Invoke(this, new System.ComponentModel.PropertyChangedEventArgs(nameof(SerialsOK)));
            }
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        private void _directionValues_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            PropertyChanged?.Invoke(this, new System.ComponentModel.PropertyChangedEventArgs(nameof(DirectionValues)));
            SetSerialsOK();
        }

        private void SetSerialsOK()
        {
            SerialsOK = SerialNumbers.Count() == Quantity;
        }
    }

    public class DirectionValue : System.ComponentModel.INotifyPropertyChanged
    {
        public DirectionValue()
        {
            _serialNumbers = new ObservableCollection<SerialNumberItem>();
            _serialNumbers.CollectionChanged += _serialNumbers_CollectionChanged;
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;



        public int DirectionItemID { get; set; }
        public string BatchNum { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public decimal Quantity { get; set; }


        private ObservableCollection<SerialNumberItem> _serialNumbers;
        public ObservableCollection<SerialNumberItem> SerialNumbers
        {
            get => _serialNumbers;
            set
            {
                _serialNumbers = value;
                _serialNumbers.CollectionChanged += _serialNumbers_CollectionChanged;
                PropertyChanged?.Invoke(this, new System.ComponentModel.PropertyChangedEventArgs(nameof(SerialNumbers)));
            }
        }

        private void _serialNumbers_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            PropertyChanged?.Invoke(this, new System.ComponentModel.PropertyChangedEventArgs(nameof(SerialNumbers)));
        }
    }
}

﻿using System;
using System.Globalization;
using System.Windows.Input;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;

using S4mp.Core.ViewModel;

namespace S4mp.InternalControls.DispCheck
{
    public class DispCheckViewModel : BaseViewModel
    {
        public DispCheckViewModel()
        {
            _items = new ObservableCollection<DispCheckItem>();
            _items.CollectionChanged += _doneItems_CollectionChanged;
        }

        public event EventHandler QuantityChanged;

        private ObservableCollection<DispCheckItem> _items;
        public ObservableCollection<DispCheckItem> Items
        {
            get => _items;
            set
            {
                SetField(ref _items, value);
                FormatHeader();
                foreach (var item in value)
                    item.PropertyChanged += (s, e) =>
                    {
                        FormatHeader();
                        QuantityChanged?.Invoke(s, e);
                    };
            }
        }

        private string _header;
        public string Header
        {
            get => _header;
            set => SetField(ref _header, value);
        }

        private bool _showError;
        public bool ShowError
        {
            get => _showError;
            set => SetField(ref _showError, value);
        }

        private S4.Entities.Direction.DispCheckMethodsEnum _dispCheckMethod;
        public S4.Entities.Direction.DispCheckMethodsEnum DispCheckMethod
        {
            get => _dispCheckMethod;
            set
            {
                SetField(ref _dispCheckMethod, value);
                QuantityEnabled = value == S4.Entities.Direction.DispCheckMethodsEnum.EnterQuantity;
            }
        }

        private bool _quantityEnabled;
        public bool QuantityEnabled
        {
            get => _quantityEnabled;
            set
            {
                SetField(ref _quantityEnabled, value);
                foreach (var item in Items)
                    item.QuantityEnabled = value;
            }
        }

        private void FormatHeader()
        {
            Header = $"Kontrola expedice skenováním {Items.Where(i => i.Checked).Count()}/{Items.Count}";
        }

        private void _doneItems_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            FormatHeader();
        }
    }
}

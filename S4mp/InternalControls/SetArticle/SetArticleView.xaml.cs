﻿using S4.Core;
using S4mp.Controls.Common;
using S4mp.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace S4mp.InternalControls.SetArticle
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SetArticleView : ContentView
    {
        public SetArticleView()
        {
            InitializeComponent();

            BindingContext = this;
            SetBarCodeCmd = new Command(SetBarcodeCommand);
            Entry_Focus();
        }

        public static readonly BindableProperty SetBarCodeCmdProperty = BindableProperty.Create(nameof(SetBarCodeCmd), typeof(ICommand), typeof(SetArticleView));
        public ICommand SetBarCodeCmd
        {
            get => (ICommand)GetValue(SetBarCodeCmdProperty);
            set => SetValue(SetBarCodeCmdProperty, value);
        }

        public static readonly BindableProperty BarCodeProperty = BindableProperty.Create(nameof(BarCode), typeof(string), typeof(SetArticleView));
        public string BarCode
        {
            get => (string)GetValue(BarCodeProperty);
            set => SetValue(BarCodeProperty, value);
        }
        
        public static readonly BindableProperty GenBarCodeProperty = BindableProperty.Create(nameof(GenBarCode), typeof(string), typeof(SetArticleView));
        public string GenBarCode
        {
            get => (string)GetValue(GenBarCodeProperty);
            set => SetValue(GenBarCodeProperty, value);
        }

        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);

            if (propertyName == BarCodeProperty.PropertyName)
                ValueChanged?.Invoke(this, new ButtonPressedEventArgs { Button = ButtonsBar.ButtonsEnum.OK });
        }

        public class ButtonPressedEventArgs : EventArgs
        {
            public ButtonsBar.ButtonsEnum Button { get; set; }
        }

        public event EventHandler ValueChanged;
        public event EventHandler ValueCommited;
        private void EdBarCode_Completed(object sender, EventArgs e)
        {
            ValueCommited?.Invoke(this, new ButtonPressedEventArgs { Button = ButtonsBar.ButtonsEnum.OK });
        }
        
        private void EdGenBarCode_Completed(object sender, EventArgs e)
        {
            ValueCommited?.Invoke(this, new ButtonPressedEventArgs { Button = ButtonsBar.ButtonsEnum.New });
        }

        private void SetBarcodeCommand()
        {
            // check disallowed value by regex
            var regexValue = Singleton<S4mp.Settings.LocalSettings>.Instance[Consts.XMLPATH_BARCODE_DISALLOWED_VALUE_PATTERN];
            if (!string.IsNullOrEmpty(regexValue) && new Regex(regexValue).IsMatch(scannerView.Text))
            {
                errorText.IsVisible = true;
                setBarCodeInputFrame.HasError();
                return; // skip ean parse if not valid
            }
            else
            {
                errorText.IsVisible = false;
                setBarCodeInputFrame.HasError(false);
            }
            

            BarCode = scannerView.Text;

            if (!string.IsNullOrWhiteSpace(BarCode))
            {
                ValueCommited?.Invoke(this, new ButtonPressedEventArgs { Button = ButtonsBar.ButtonsEnum.OK });
            }
        }

        /// <summary>
        /// Focus entry
        /// </summary>
        private async void Entry_Focus()
        {
            await Task.Delay(600);
            edBarCode.Focus();
        }
    }
}
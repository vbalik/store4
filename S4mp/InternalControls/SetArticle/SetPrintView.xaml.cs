﻿using S4mp.InternalControls.Printer;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace S4mp.InternalControls.SetArticle
{
    public partial class SetPrintView : ContentView
    {
        public SetPrintView()
        {
            InitializeComponent();

            BindingContext = this;

            setPrinter.SuccessCommand = new Command<PrinterResultModel>(GetPrinter);
            setPrinter.PrinterCacheType = Core.PrinterCacheTypeEnum.PrintCode;
            (setPrinter as SetPrinterControl).LoadPrinterClasses();
        }

        public static readonly BindableProperty PrinterIDProperty = BindableProperty.Create(nameof(PrinterID), typeof(int?), typeof(SetPrintView));
        public int? PrinterID
        {
            get => (int?)GetValue(PrinterIDProperty);
            set => SetValue(PrinterIDProperty, value);
        }

        public static readonly BindableProperty CountProperty = BindableProperty.Create(nameof(Count), typeof(short), typeof(SetPrintView), (short)1);
        public short Count
        {
            get => (short)GetValue(CountProperty);
            set => SetValue(CountProperty, value);
        }

        public event EventHandler<bool> DataChanged;

        private async Task LoadPrinters(S4.Entities.PrinterType.PrinterClassEnum? printerClass)
        {
            activityIndicator.Show();
            try
            {
                var client = new Client.BarcodeClient();
                var result = await client.BarcodeListPrinters(new S4.ProcedureModels.BarCode.BarcodeListPrinters() { PrinterClass = printerClass.Value });
                if (!result.Success)
                {
                    await App.Current.MainPage.DisplayAlert("Načítání tiskáren skončilo s chybou!", result.ErrorText, "OK");
                    return;
                }

                // set printer control
                setPrinter.PrinterLocations = result.PrintersList;
                
                (setPrinter as SetPrinterControl).LoadPrinterClasses();
            }
            finally
            {
                activityIndicator.Hide();
            }
        }

        private async void GetPrinter(PrinterResultModel printerResult)
        {
            PrinterID = printerResult.PrinterLocationID;

            if(printerResult.PrinterClass != null && printerResult.PrinterLocationID == null)
                await LoadPrinters(printerResult.PrinterClass);

            if (printerResult.PrinterLocationID != null)
                InvokeChanged();
        }

        private void InvokeChanged()
        {
            DataChanged?.Invoke(this, PrinterID.HasValue);
        }
    }
}
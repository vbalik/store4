﻿using S4.Core;
using S4.Entities.Helpers;
using S4mp.Controls;
using S4mp.Controls.Scanner;
using S4mp.Core;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using S4.ProcedureModels.Dispatch;

namespace S4mp.InternalControls
{
    public partial class SelectDocumentControl : ContentView, IRefreshable
    {
        /// <summary>
        /// Bindable OnSelectItemCmdProperty<ICommand>
        /// </summary>
        public static readonly BindableProperty OnSelectItemCmdProperty = BindableProperty.Create(nameof(OnSelectItemCmd), typeof(ICommand), typeof(SelectDocumentControl));
        public ICommand OnSelectItemCmd
        {
            get => (ICommand)GetValue(OnSelectItemCmdProperty);
            set => SetValue(OnSelectItemCmdProperty, value);
        }

        public static readonly BindableProperty HeaderTextProperty = BindableProperty.Create(nameof(HeaderText), typeof(string), typeof(SelectDocumentControl));
        public string HeaderText
        {
            get => (string)GetValue(HeaderTextProperty);
            set => SetValue(HeaderTextProperty, value);
        }

        public static readonly BindableProperty HeaderTextSubProperty = BindableProperty.Create(nameof(HeaderTextSub), typeof(string), typeof(SelectDocumentControl));
        public string HeaderTextSub
        {
            get => (string)GetValue(HeaderTextSubProperty);
            set => SetValue(HeaderTextSubProperty, value);
        }

        public static readonly BindableProperty ShowDocumentDetailProperty = BindableProperty.Create(nameof(ShowDocumentDetail), typeof(bool), typeof(DocListItemView), true);
        public bool ShowDocumentDetail
        {
            get => (bool)GetValue(ShowDocumentDetailProperty);
            set => SetValue(ShowDocumentDetailProperty, value);
        }

        public static readonly BindableProperty RefreshDataCommandProperty = BindableProperty.Create(nameof(RefreshDataCommand), typeof(ICommand), typeof(SelectDocumentControl), null);
        public ICommand RefreshDataCommand
        {
            get { return (ICommand)GetValue(RefreshDataCommandProperty); }
            set { SetValue(RefreshDataCommandProperty, value); }
        }

        public static readonly BindableProperty IsBussyProperty = BindableProperty.Create(nameof(IsBussy), typeof(bool), typeof(SelectDocumentControl), false);
        public bool IsBussy
        {
            get { return (bool)GetValue(IsBussyProperty); }
            set { SetValue(IsBussyProperty, value); }
        }

        //dependent resolver for scanning
        private IScannerResolver _scannerResolver;
        //button for scanning if camera scanner

        public ObservableCollection<DocumentInfo> SourceItems = new ObservableCollection<DocumentInfo>();
        public event EventHandler NoItemsFoundEvent;
        public Func<Task<List<DocumentInfo>>> DataWithRefreshTask { get; set; }
        public bool EnableDocumentScan { get; set; } = false;
        public Func<string, Task<List<DocumentInfo>>> EnableDocumentScanFunc { get; set; }

        public SelectDocumentControl()
        {
            InitializeComponent();

            RefreshDataCommand = new Command(RefreshData);

            //create scanning resolver
            _scannerResolver = DependencyService.Get<IScannerResolver>();
        }

        public async void RefreshData()
        {
            if (IsBussy == true) return;
            IsBussy = true;
            activityIndicator.IsVisible = true;

            try
            {
                LoadData(await DataWithRefreshTask());
            }
            finally
            {
                IsBussy = false;
                activityIndicator.IsVisible = false;
            }
        }

        public void LoadData(List<DocumentInfo> data)
        {
            SourceItems.Clear();

            data.ForEach(item => SourceItems.Add(item));

            dispatchListView.ItemsSource = SourceItems;

            NoItemsFoundEvent.Invoke(this, null);


            // scan document
            if(EnableDocumentScan)
            {
                _scannerResolver.Init(true, ScannerResolver_Scanned2);
            }
        }

        private async void ScannerResolver_Scanned2(object sender, BarCodeTypeEnum e)
        {
            activityIndicator.Show();
            try
            {
                var result = await EnableDocumentScanFunc((sender as IScannerResolver).Text);                    

                if (result.Count == 0)
                {
                    Singleton<Helpers.Audio>.Instance.PlayError();
                }
            }
            finally
            {
                activityIndicator.Hide();
            }
        }

        #region UI handles

        private void DispatchListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            OnSelectItemCmd.Execute((DocumentInfo)e.Item);
        }

        #endregion
    }
}
﻿using S4.Entities.Models;
using S4mp.Core.ViewModel;
using System.Collections.Generic;
using Xamarin.Forms;

namespace S4mp.InternalControls.Articles
{
    public class ArticleDetailControlVM : BaseViewModel
    {
        private ArticleModel _articleModel;
        public ArticleModel ArticleModel
        {
            get => _articleModel;
            set => SetField(ref _articleModel, value);
        }

        private List<ArticlePackingModel> _articlePackings;
        public List<ArticlePackingModel> ArticlePackings
        {
            get => _articlePackings;
            set => SetField(ref _articlePackings, value);
        }

        public class ArticlePackingModel : BaseViewModel
        {
            public int ArtPackID { get; set; }
            public int ArticleID { get; set; }
            public string PackDesc { get; set; }
            public decimal PackRelation { get; set; }
            public bool MovablePack { get; set; }
            public string Source_id { get; set; }
            public string PackStatus { get; set; }
            public bool InclCarrier { get; set; }
            public decimal PackWeight { get; set; }
            public decimal PackVolume { get; set; }

            public byte BarCodeType { get; set; }
            public string BarCode { get; set; }

            public int? PackingBarCodeID { get; set; }
            public bool AnotherPackingBarCodeCreation { get; set; }

            public Color AnotherPackingBarCodeColor => PackingBarCodeID != null ? Color.DarkViolet : Color.Black;
        }
    }
}

﻿using S4.Entities.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace S4mp.InternalControls.Articles
{
    public partial class ArticleDetailControl : ContentView
    {
        public static readonly BindableProperty HeaderTextProperty = BindableProperty.Create(nameof(HeaderText), typeof(string), typeof(ArticleDetailControl));
        public string HeaderText
        {
            get => (string)GetValue(HeaderTextProperty);
            set => SetValue(HeaderTextProperty, value);
        }

        public static readonly BindableProperty HeaderTextSubProperty = BindableProperty.Create(nameof(HeaderTextSub), typeof(string), typeof(ArticleDetailControl));
        public string HeaderTextSub
        {
            get => (string)GetValue(HeaderTextSubProperty);
            set => SetValue(HeaderTextSubProperty, value);
        }

        public static readonly BindableProperty ItemActionMenuCmdProperty = BindableProperty.Create(nameof(ItemActionMenuCmd), typeof(ICommand), typeof(ArticleDetailControl));
        public ICommand ItemActionMenuCmd
        {
            get => (ICommand)GetValue(ItemActionMenuCmdProperty);
            set => SetValue(ItemActionMenuCmdProperty, value);
        }

        public static readonly BindableProperty ShowUserRemarkProperty = BindableProperty.Create(nameof(ShowUserRemark), typeof(bool), typeof(ArticleDetailControl));
        public bool ShowUserRemark
        {
            get => (bool)GetValue(ShowUserRemarkProperty);
            set => SetValue(ShowUserRemarkProperty, value);
        }

        public ArticleDetailControl()
        {
            InitializeComponent();

            ClearForm();
        }

        public async Task LoadArticleData(int articleID, bool headerVisibility = true)
        {
            var article = await DependencyService.Get<S4mp.Client.Interfaces.IArticleClient>().GetArticleModel(articleID);

            if (article == null)
            {
                error.IsVisible = true;
                form.IsVisible = false;
                return;
            }

            HeaderText = article.ArticleCode;

            if (!headerVisibility)
                headerBlock.IsVisible = false;

            ShowUserRemark = !string.IsNullOrEmpty(article.UserRemark);

            // get article packings
            var articlePackings = article.Packings.Select(_ => new ArticleDetailControlVM.ArticlePackingModel
            {
                ArtPackID = _.ArtPackID,
                ArticleID = _.ArticleID,
                PackDesc = _.PackDesc,
                PackRelation = _.PackRelation,
                MovablePack = _.MovablePack,
                Source_id = _.Source_id,
                PackStatus = _.PackStatus,
                InclCarrier = _.InclCarrier,
                PackWeight = _.PackWeight,
                PackVolume = _.PackVolume,
                BarCodeType = _.BarCodeType,
                BarCode = _.BarCode
            }).ToList();

            // get article packings another barcode
            article.PackingBarCodes.ForEach(_ =>
            {
                var packing = article.Packings.SingleOrDefault(p => p.ArtPackID == _.ArtPackID);
                var model = new ArticleDetailControlVM.ArticlePackingModel
                {
                    ArtPackID = packing.ArtPackID,
                    ArticleID = packing.ArticleID,
                    PackDesc = packing.PackDesc,
                    PackRelation = packing.PackRelation,
                    MovablePack = packing.MovablePack,
                    Source_id = packing.Source_id,
                    PackStatus = packing.PackStatus,
                    InclCarrier = packing.InclCarrier,
                    PackWeight = packing.PackWeight,
                    PackVolume = packing.PackVolume,
                    BarCodeType = packing.BarCodeType,
                    BarCode = _.BarCode,
                    PackingBarCodeID = _.ArtPackBarCodeID
                };

                articlePackings.Add(model);
            });

            BindingContext = new ArticleDetailControlVM
            {
                ArticleModel = article,
                ArticlePackings = articlePackings.OrderBy(_ => _.ArtPackID).ToList()
            };
        }

        public void ClearForm()
        {
            error.IsVisible = false;
            form.IsVisible = true;
            HeaderText = "Detail karty";

            BindingContext = null;
        }
    }
}
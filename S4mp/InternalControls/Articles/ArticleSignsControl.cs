﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using S4Article = S4.Entities.Article;
using System.Text;

using Xamarin.Forms;

namespace S4mp.InternalControls.Articles
{
    public class ArticleSignsControl : RelativeLayout
    {
        private readonly BoxView _cooled;
        private readonly BoxView _cytostatics;
        private readonly BoxView _medicines;
        private readonly BoxView _single;

        public Dictionary<S4Article.SpecFeaturesEnum, Color> Colors = new Dictionary<S4Article.SpecFeaturesEnum, Color>
        {
            { S4Article.SpecFeaturesEnum.Cooled, Color.LightSkyBlue },
            { S4Article.SpecFeaturesEnum.Cytostatics, Color.FromHex("#f4d03f") },
            { S4Article.SpecFeaturesEnum.Medicines, Color.FromHex("#ec7063") }
        };

        public ArticleSignsControl()
        {
            WidthRequest = 10;

            _cooled = new BoxView() { BackgroundColor = Colors[S4Article.SpecFeaturesEnum.Cooled], IsVisible = false };
            _cytostatics = new BoxView() { BackgroundColor = Colors[S4Article.SpecFeaturesEnum.Cytostatics], IsVisible = false };
            _medicines = new BoxView() { BackgroundColor = Colors[S4Article.SpecFeaturesEnum.Medicines], IsVisible = false };
            _single = new BoxView() { BackgroundColor = Color.Transparent, IsVisible = false };

            var thConstraint = Constraint.RelativeToParent((p) => (p.Height / 3) - 1);
            Children.Add(_cooled, Constraint.Constant(0), Constraint.Constant(0), Constraint.RelativeToParent((p) => p.Width), thConstraint);
            Children.Add(_medicines, Constraint.Constant(0), thConstraint, Constraint.RelativeToParent((p) => p.Width), thConstraint);
            Children.Add(_cytostatics, Constraint.Constant(0), Constraint.RelativeToParent((p) => 2 * ((p.Height / 3))), Constraint.RelativeToParent((p) => p.Width), thConstraint);

            Children.Add(_single, Constraint.Constant(0), Constraint.Constant(0), Constraint.RelativeToParent((p) => p.Width), Constraint.RelativeToParent((p) => p.Height));
        }

        public static readonly BindableProperty SpecFeaturesIntProperty = BindableProperty.Create(nameof(SpecFeaturesInt), typeof(int), typeof(ArticleSignsControl));
        public int SpecFeaturesInt
        {
            get => (int) GetValue(SpecFeaturesIntProperty);
            set => SetValue(SpecFeaturesIntProperty, value);
        }

        public static readonly BindableProperty ShowSpecFeaturesAlertInfoProperty = BindableProperty.Create(nameof(ShowSpecFeaturesAlertInfo), typeof(bool), typeof(ArticleSignsControl));
        public bool ShowSpecFeaturesAlertInfo
        {
            get => (bool) GetValue(ShowSpecFeaturesAlertInfoProperty);
            set => SetValue(ShowSpecFeaturesAlertInfoProperty, value);
        }

        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);

            if (propertyName == SpecFeaturesIntProperty.PropertyName)
            {
                var specFeatures = (S4Article.SpecFeaturesEnum) SpecFeaturesInt;

                var matchedFeatures = new S4Article.SpecFeaturesEnum[] {
                        S4Article.SpecFeaturesEnum.Cooled,
                        S4Article.SpecFeaturesEnum.Cytostatics,
                        S4Article.SpecFeaturesEnum.Medicines
                    }
                    .Cast<S4Article.SpecFeaturesEnum>()
                    .Where(v => specFeatures.HasFlag(v));

                if (ShowSpecFeaturesAlertInfo
                    && (matchedFeatures.Count() == 1
                    || specFeatures.HasFlag(S4Article.SpecFeaturesEnum.Cooled)))
                {
                    _single.IsVisible = true;
                    _single.BackgroundColor = specFeatures.HasFlag(S4Article.SpecFeaturesEnum.Cooled)
                        ? Colors[S4Article.SpecFeaturesEnum.Cooled]
                        : Colors[matchedFeatures.First()];

                    _cooled.IsVisible = false;
                    _cytostatics.IsVisible = false;
                    _medicines.IsVisible = false;
                }
                else
                {
                    _single.IsVisible = false;

                    _cooled.IsVisible = specFeatures.HasFlag(S4Article.SpecFeaturesEnum.Cooled);
                    _cytostatics.IsVisible = specFeatures.HasFlag(S4Article.SpecFeaturesEnum.Cytostatics);
                    _medicines.IsVisible = specFeatures.HasFlag(S4Article.SpecFeaturesEnum.Medicines);
                }
            }
        }
    }
}

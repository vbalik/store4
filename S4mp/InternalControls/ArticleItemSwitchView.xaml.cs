﻿using System.Windows.Input;
using Xamarin.Forms;

namespace S4mp.InternalControls
{
    public partial class ArticleItemSwitchView : ContentView
    {
        public static readonly BindableProperty ShowArticleInfoProperty = BindableProperty.Create(nameof(ShowArticleInfo), typeof(bool), typeof(ArticleItemSwitchView), true);
        public bool ShowArticleInfo
        {
            get => (bool)GetValue(ShowArticleInfoProperty);
            set  => SetValue(ShowArticleInfoProperty, value);
        }

        public static readonly BindableProperty ShowPosCodeProperty = BindableProperty.Create(nameof(ShowPosCode), typeof(bool), typeof(ArticleItemSwitchView), false);
        public bool ShowPosCode
        {
            get => (bool)GetValue(ShowPosCodeProperty);
            set => SetValue(ShowPosCodeProperty, value);
        }

        public static readonly BindableProperty ShowCarrierNumProperty = BindableProperty.Create(nameof(ShowCarrierNum), typeof(bool), typeof(ArticleItemSwitchView), false);
        public bool ShowCarrierNum
        {
            get => (bool)GetValue(ShowCarrierNumProperty);
            set => SetValue(ShowCarrierNumProperty, value);
        }
        
        public static readonly BindableProperty ShowQuantitySelectedProperty = BindableProperty.Create(nameof(ShowQuantitySelected), typeof(bool), typeof(ArticleItemSwitchView), false);
        public bool ShowQuantitySelected
        {
            get => (bool)GetValue(ShowQuantitySelectedProperty);
            set => SetValue(ShowQuantitySelectedProperty, value);
        }

        public static readonly BindableProperty MenuCommandProperty = BindableProperty.Create(nameof(MenuCommand), typeof(ICommand), typeof(ArticleItemSwitchView), null);
        public ICommand MenuCommand
        {
            get { return (ICommand)GetValue(MenuCommandProperty); }
            set { SetValue(MenuCommandProperty, value); }
        }

        public static readonly BindableProperty TapCommandProperty = BindableProperty.Create(nameof(TapCommand), typeof(ICommand), typeof(ArticleItemView), null);
        public ICommand TapCommand
        {
            get { return (ICommand)GetValue(TapCommandProperty); }
            set { SetValue(TapCommandProperty, value); }
        }

        public ArticleItemSwitchView()
        {
            InitializeComponent();
        }

        // helper method for invoking commands safely
        public static void Execute(ICommand command, object value)
        {
            if (command == null) return;
            if (command.CanExecute(value))
            {
                command.Execute(value);
            }
        }

        public Command OnLongPress => new Command((value) => Execute(MenuCommand, value));
        public Command OnTapped => new Command((value) => TapCommand.Execute(value));
    }
}
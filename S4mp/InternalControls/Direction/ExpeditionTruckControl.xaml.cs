﻿using S4.Core;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace S4mp.InternalControls.Direction
{
    public partial class ExpeditionTruckControl : ContentView
	{
        public static readonly BindableProperty ExpeditionDoneCmdProperty = BindableProperty.Create(nameof(ExpeditionDoneCmd), typeof(ICommand), typeof(ExpeditionTruckControl));
        public ICommand ExpeditionDoneCmd
        {
            get => (ICommand)GetValue(ExpeditionDoneCmdProperty);
            set => SetValue(ExpeditionDoneCmdProperty, value);
        }

        public static readonly BindableProperty OnSuccessCommandProperty = BindableProperty.Create(nameof(OnSuccessCommand), typeof(ICommand), typeof(ExpeditionTruckControl));
        public ICommand OnSuccessCommand
        {
            get => (ICommand)GetValue(OnSuccessCommandProperty);
            set => SetValue(OnSuccessCommandProperty, value);
        }

        public static readonly BindableProperty HeaderTextProperty = BindableProperty.Create(nameof(HeaderText), typeof(string), typeof(ExpeditionTruckControl));
        public string HeaderText
        {
            get => (string)GetValue(HeaderTextProperty);
            set => SetValue(HeaderTextProperty, value);
        }

        public static readonly BindableProperty HeaderTextSubProperty = BindableProperty.Create(nameof(HeaderTextSub), typeof(string), typeof(ExpeditionTruckControl));
        public string HeaderTextSub
        {
            get => (string)GetValue(HeaderTextSubProperty);
            set => SetValue(HeaderTextSubProperty, value);
        }

        public event EventHandler<bool> EnableBtnOkEvent;

        public ExpeditionTruckControl()
		{
			InitializeComponent();

            ExpeditionDoneCmd = new Command(ExpeditionDone);
            Entry_Focus();

            edSpz.TextChanged += (object sender, TextChangedEventArgs e) =>
            {
                EnableBtnOkEvent.Invoke(this, !string.IsNullOrEmpty(e.NewTextValue));
            };
        }

        public void ExpeditionDone()
        {
            if(string.IsNullOrEmpty(edSpz.Text))
            {
                spzInputFrame.HasError();
                Singleton<Helpers.Audio>.Instance.PlayError();
                return;
            }

            OnSuccessCommand.Execute(edSpz.Text);
        }

        #region UI handles

        private void EdSpz_Completed(object sender, System.EventArgs e)
        {
            ExpeditionDoneCmd.Execute(sender);
        }

        /// <summary>
        /// On focus entry reset error fields
        /// </summary>
        private void Entry_Focused(object sender, FocusEventArgs e)
        {
            edSpz.Text = string.Empty;
            spzInputFrame.HasError(false);
        }

        /// <summary>
        /// Focus entry
        /// </summary>
        private async void Entry_Focus()
        {
            await Task.Delay(600);
            edSpz.Focus();
        }

        #endregion
    }
}
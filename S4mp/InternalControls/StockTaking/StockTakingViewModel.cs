﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;

using S4mp.Core.ViewModel;
using S4.Entities;

namespace S4mp.InternalControls.StockTaking
{
    public class StockTakingViewModel : BaseViewModel
    {
        public StockTakingViewModel()
        {
            _positions = new ObservableCollection<Position>();
            _positions.CollectionChanged += _positions_CollectionChanged;
        }

        public event EventHandler PositionListChanged;

        private string _stotakDesc;
        public string StotakDesc
        {
            get => _stotakDesc;
            set
            {
                SetField(ref _stotakDesc, value ?? string.Empty);
            }
        }


        private ObservableCollection<Position> _positions;
        public ObservableCollection<Position> Positions
        {
            get => _positions;
            set
            {
                SetField(ref _positions, value);
                _positions.CollectionChanged += _positions_CollectionChanged;
            }
        }

        private string _headerSubText;
        public string HeaderSubText
        {
            get => _headerSubText;
            set
            {
                SetField(ref _headerSubText, value ?? string.Empty);
            }
        }

        private void _positions_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            HeaderSubText = $"Pozic: {Positions.Count}";
            PositionListChanged?.Invoke(this, new EventArgs());
        }
    }
}

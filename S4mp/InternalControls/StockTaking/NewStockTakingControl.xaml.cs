﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace S4mp.InternalControls.StockTaking
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewStockTakingControl : ContentView
    {
        public NewStockTakingControl()
        {
            InitializeComponent();

            ((InternalControls.Positions.SearchPositionControlVM)searchPositionControl.BindingContext).HeaderText = "Přidat pozici ...";
            searchPositionControl.OnSuccessCommand = new Command<S4.Entities.Position>((position) => Add(position));
        }

        public event EventHandler PositionListChanged;

        public StockTakingViewModel ViewModel => (StockTakingViewModel)BindingContext;

        public Command OnLongPress => new Command((value) => OnActionMenu(value));

        private async void OnActionMenu(object item)
        {
            var position = (S4.Entities.Position)item;
            var delete = "Vymazat";
            var button = await Application.Current.MainPage.DisplayActionSheet($"Pozice {position.PosCode}", "Zavřít", null, delete);
            if (button == delete)
                ViewModel.Positions.Remove(position);
        }

        private void StockTakingViewModel_PositionListChanged(object sender, EventArgs e)
        {
            PositionListChanged?.Invoke(this, e);
        }

        private async void Add(S4.Entities.Position position)
        {
            var positionFound = ViewModel.Positions.SingleOrDefault(_ => _.PosCode == position.PosCode);
            if (positionFound != null)
            {
                await Application.Current.MainPage.DisplayAlert("Nelze přidat", $"Pozice {position.PosCode} již byla přidána!", "Zavřít");
                return;
            }
            if (position.PosStatus == S4.Entities.Position.POS_STATUS_STOCKTAKING)
            {
                await Application.Current.MainPage.DisplayAlert("Nelze přidat", $"Pozice {position.PosCode} je již v jiné inventuře!", "Zavřít");
                return;
            }
            ViewModel.Positions.Add(position);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace S4mp.InternalControls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ArticleItemViewSmall : ContentView
    {
        public ArticleItemViewSmall()
        {
            InitializeComponent();
        }

        public static readonly BindableProperty ShowArticleInfoProperty = BindableProperty.Create(nameof(ShowArticleInfo), typeof(bool), typeof(ArticleItemView), true);
        public bool ShowArticleInfo
        {
            get => (bool) GetValue(ShowArticleInfoProperty);
            set => SetValue(ShowArticleInfoProperty, value);
        }

        public static readonly BindableProperty MenuCommandProperty =
                            BindableProperty.Create(nameof(MenuCommand), typeof(ICommand), typeof(ArticleItemView), null);

        public ICommand MenuCommand
        {
            get { return (ICommand) GetValue(MenuCommandProperty); }
            set { SetValue(MenuCommandProperty, value); }
        }

        public static readonly BindableProperty TapCommandProperty =
                    BindableProperty.Create(nameof(TapCommand), typeof(ICommand), typeof(ArticleItemView), null);

        public ICommand TapCommand
        {
            get { return (ICommand) GetValue(TapCommandProperty); }
            set { SetValue(TapCommandProperty, value); }
        }

        // helper method for invoking commands safely
        public static void Execute(ICommand command, object value)
        {
            if (command == null) return;
            if (command.CanExecute(value))
            {
                command.Execute(value);
            }
        }

        public Command OnLongPress => new Command((value) => Execute(MenuCommand, value));
        public Command OnTapped => new Command((value) => Execute(TapCommand, value));
    }
}
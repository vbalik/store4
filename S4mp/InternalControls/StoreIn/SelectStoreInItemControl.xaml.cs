﻿using S4.Entities.Helpers;
using S4.ProcedureModels.StoreIn;
using S4mp.Client;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace S4mp.InternalControls.StoreIn
{
    public partial class SelectStoreInItemControl : ContentView
    {
        /// <summary>
        /// Bindable OnSuccessCommand<ICommand>
        /// </summary>
        public static readonly BindableProperty OnSuccessCommandProperty = BindableProperty.Create(nameof(OnSuccessCommand), typeof(ICommand), typeof(SelectStoreInItemControl));
        public ICommand OnSuccessCommand
        {
            get => (ICommand)GetValue(OnSuccessCommandProperty);
            set => SetValue(OnSuccessCommandProperty, value);
        }

        public static readonly BindableProperty HeaderTextProperty = BindableProperty.Create(nameof(HeaderText), typeof(string), typeof(SelectStoreInItemControl));
        public string HeaderText
        {
            get => (string)GetValue(HeaderTextProperty);
            set => SetValue(HeaderTextProperty, value);
        }

        public static readonly BindableProperty HeaderTextSubProperty = BindableProperty.Create(nameof(HeaderTextSub), typeof(string), typeof(SelectStoreInItemControl));
        public string HeaderTextSub
        {
            get => (string)GetValue(HeaderTextSubProperty);
            set => SetValue(HeaderTextSubProperty, value);
        }

        public SelectStoreInItemControl()
        {
            InitializeComponent();
        }

        public event EventHandler NoItemsFoundEvent;

        public ObservableCollection<ItemInfo> SourceItems = new ObservableCollection<ItemInfo>();

        public async Task LoadData(int directionID, int positionID, int lastItemIndex)
        {
            // load items
            activityIndicator.Show();
            try
            {
                var client = new StoreInClient();
                var resultNoCarr = await client.ListNoCarr(new StoreInListNoCarr
                {
                    SrcPositionID = positionID,
                    DirectionID = directionID
                });

                resultNoCarr.ArticleList.ForEach(item => SourceItems.Add(item));

                itemsListView.ItemsSource = SourceItems;

                // scroll to last item position
                if (SourceItems.Count > 3 && lastItemIndex >= 3)
                {
                    var scrollToItem = SourceItems[lastItemIndex];
                    if (scrollToItem != null)
                    {
                        itemsListView.ScrollTo(scrollToItem, ScrollToPosition.MakeVisible, false);
                    }
                }

                NoItemsFoundEvent.Invoke(this, null);
            }
            finally
            {
                activityIndicator.Hide();
            }
        }

        private void OnTappedExec(ItemInfo item)
        {
            var lastPositionIndex = SourceItems.IndexOf(item);

            OnSuccessCommand.Execute(new SelectStoreInItemResultModel
            {
                ItemInfo = item,
                LastPositionIndex = lastPositionIndex - 1
            });
        }

        #region UI Handles

        public Command OnTapped => new Command<ItemInfo>(OnTappedExec);

        #endregion
    }

    public class SelectStoreInItemResultModel
    {
        public ItemInfo ItemInfo { get; set; }
        public int LastPositionIndex { get; set; }
    }
}
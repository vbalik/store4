﻿using S4.Core;
using S4.Entities.Helpers;
using S4mp.Client;
using S4mp.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace S4mp.InternalControls.Dispatch
{
    public partial class SelectDispatchItemControl : ContentView
    {
        private static string DISPATCH_ITEMS_SORT = "DISPATCH_ITEMS_SORT";
        private const string SORT_OPTION_A = "Karta";
        private const string SORT_OPTION_B = "Pozice";

        /// <summary>
        /// Bindable OnSuccessCommand<ICommand>
        /// </summary>
        public static readonly BindableProperty OnSuccessCommandProperty = BindableProperty.Create(nameof(OnSuccessCommand), typeof(ICommand), typeof(SelectDispatchItemControl));
        public ICommand OnSuccessCommand
        {
            get => (ICommand)GetValue(OnSuccessCommandProperty);
            set => SetValue(OnSuccessCommandProperty, value);
        }

        public static readonly BindableProperty HeaderTextProperty = BindableProperty.Create(nameof(HeaderText), typeof(string), typeof(SelectDispatchItemControl));
        public string HeaderText
        {
            get => (string)GetValue(HeaderTextProperty);
            set => SetValue(HeaderTextProperty, value);
        }

        public static readonly BindableProperty HeaderTextSubProperty = BindableProperty.Create(nameof(HeaderTextSub), typeof(string), typeof(SelectDispatchItemControl));
        public string HeaderTextSub
        {
            get => (string)GetValue(HeaderTextSubProperty);
            set => SetValue(HeaderTextSubProperty, value);
        }

        public static readonly BindableProperty ShowSpecFeaturesAlertInfoListProperty = BindableProperty.Create(nameof(ShowSpecFeaturesAlertInfoList), typeof(bool), typeof(SelectDispatchItemControl), false);
        public bool ShowSpecFeaturesAlertInfoList
        {
            get => (bool) GetValue(ShowSpecFeaturesAlertInfoListProperty);
            set => SetValue(ShowSpecFeaturesAlertInfoListProperty, value);
        }

        public event EventHandler NoItemsFoundEvent;

        public ObservableCollection<DispatchItemInfoModel> SourceItems = new ObservableCollection<DispatchItemInfoModel>();
        private int? _temporary_printLabelArtPackID;

        public SelectDispatchItemControl()
        {
            InitializeComponent();
        }

        public async Task LoadData(List<ItemInfo> items, int lastItemIndex)
        {
            // set show specFeatures on list
            bool.TryParse(Singleton<S4mp.Settings.LocalSettings>.Instance[Core.Consts.XMLPATH_SHOW_SPEC_FEATURES_ALERT_INFO_LIST] ?? "False", out var showSpecFeaturesAlertInfoList);
            ShowSpecFeaturesAlertInfoList = showSpecFeaturesAlertInfoList;

            items.ForEach(item => SourceItems.Add(new DispatchItemInfoModel(item)));

            // default sort
            var defaultSort = Xamarin.Essentials.Preferences.Get(DISPATCH_ITEMS_SORT, null);
            if (defaultSort != null)
            {
                itemsListView.ItemsSource = _SortItems(SourceItems.ToList(), defaultSort);
            }
            else
            {
                itemsListView.ItemsSource = SourceItems;
            }

            // scroll to last item position
            if (SourceItems.Count > 3 && lastItemIndex >= 3)
            {
                var scrollToItem = SourceItems[lastItemIndex];
                if (scrollToItem != null)
                {
                    itemsListView.ScrollTo(scrollToItem, ScrollToPosition.MakeVisible, false);
                }
            }

            NoItemsFoundEvent.Invoke(this, null);
        }

        private void _printItemLabel(DispatchItemInfoModel item)
        {
            _temporary_printLabelArtPackID = item.ArtPackID;

            Device.BeginInvokeOnMainThread(async () =>
            {
                var setPrintDialog = new SetPrintDialog();
                setPrintDialog.Closed += _SetPrintDialog_Closed;
                await Navigation.PushModalAsync(setPrintDialog, false);
            });
        }

        private async void _SetPrintDialog_Closed(object sender, Controls.Common.ButtonsBar.ButtonsEnum e)
        {
            if (e == Controls.Common.ButtonsBar.ButtonsEnum.OK)
            {
                var printerID = (sender as SetPrintDialog).PrinterID;
                var count = (sender as SetPrintDialog).Count;
                var client = new BarcodeClient();
                var result = await client.BarcodePrintLabel(new S4.ProcedureModels.BarCode.BarcodePrintLabel
                {
                    ArtPackID = (int) _temporary_printLabelArtPackID,
                    LabelsQuant = count,
                    PrinterLocationID = printerID
                });

                if (!result.Success)
                {
                    await App.Current.MainPage.DisplayAlert("Akce skončila s chybou!", result.ErrorText, "OK");
                    return;
                }
            }

            // clear temp print item
            _temporary_printLabelArtPackID = null;
        }

        private void OnTappedExec(DispatchItemInfoModel item)
        {
            var lastPositionIndex = SourceItems.IndexOf(item);

            OnSuccessCommand.Execute(new SelectDispatchItemResultModel
            {
                ItemInfo = item,
                LastPositionIndex = lastPositionIndex - 1
            });
        }

        private async void OnItemActionMenuExec(DispatchItemInfoModel item)
        {
            var action = await App.Current.MainPage.DisplayActionSheet(null, "Zrušit", null, "Vytisknout štítek");

            switch (action)
            {
                case "Vytisknout štítek":
                    _printItemLabel(item);
                    break;
            }
        }

        protected async void FilterActionMenu()
        {
            var action = await (Parent.Parent as ContentPage).DisplayActionSheet("Seřadit podle", "Zrušit", null,
                SORT_OPTION_A,
                SORT_OPTION_B);

            var list = _SortItems(SourceItems.ToList(), action);

            itemsListView.ItemsSource = list;

            Xamarin.Essentials.Preferences.Set(DISPATCH_ITEMS_SORT, action);
        }

        private List<DispatchItemInfoModel> _SortItems(List<DispatchItemInfoModel> items, string action)
        {
            switch (action)
            {
                case SORT_OPTION_A:
                    items = items
                        .OrderBy(i => i.SpecialDelivery)
                        .ThenBy(i => i.Remark ?? String.Empty)
                        .ThenBy(i => i.ArticleCode)
                        .ThenBy(i => i.WholeCarrier)
                        .ThenByDescending(i => i.Quantity)
                        .ToList();
                    break;
                case SORT_OPTION_B:
                    items = items
                        .OrderBy(i => i.SpecialDelivery)
                        .ThenBy(i => i.Remark ?? string.Empty)
                        .ThenBy(i => i.XtraString ?? string.Empty)
                        .ThenBy(i => i.ArticleCode)
                        .ThenBy(i => i.WholeCarrier)
                        .ThenByDescending(i => i.Quantity)
                        .ToList();
                    break;
            }

            return items;
        }

        #region UI Handles

        public Command OnTapped => new Command<DispatchItemInfoModel>(OnTappedExec);

        public Command OnSort => new Command(FilterActionMenu);

        public Command ItemActionMenuCmd => new Command<DispatchItemInfoModel>(OnItemActionMenuExec);

        #endregion
    }

    public class SelectDispatchItemResultModel
    {
        public ItemInfo ItemInfo { get; set; }
        public int LastPositionIndex { get; set; }
    }
}
﻿using S4.Core.EAN;
using S4.Entities.Helpers;
using S4mp.Core;
using S4mp.Core.EANCode;
using S4mp.Core.Misc;
using S4mp.Core.ViewModel;
using S4mp.InternalControls.GetArticle;

namespace S4mp.InternalControls.Dispatch
{
    class ConfirmDispatchArticleVM : BaseViewModel
    {
        #region expose through the view

        private string _articleInfo;
        public string ArticleInfo
        {
            get => _articleInfo;
            set => SetField(ref _articleInfo, value ?? string.Empty);
        }

        private string _headerText;
        public string HeaderText
        {
            get => _headerText;
            set => SetField(ref _headerText, value ?? string.Empty);
        }
        
        private string _headerTextSub;
        public string HeaderTextSub
        {
            get => _headerTextSub;
            set => SetField(ref _headerTextSub, value ?? string.Empty);
        }

        private string _packDesc;
        public string PackDesc
        {
            get => _packDesc;
            set => SetField(ref _packDesc, value ?? string.Empty);
        }

        #endregion

        #region form

        private string _quantity;
        public string Quantity
        {
            get => _quantity;
            set
            {
                SetField(ref _quantity, value);
            }
        }

        private string _batchNum;
        public string BatchNum
        {
            get => _batchNum;
            set => SetField(ref _batchNum, value);
        }

        #endregion



        private GetArticleResultModel _articleResultModel;
        public GetArticleResultModel ArticleResultModel
        {
            get => _articleResultModel;
            set
            {
                _articleResultModel = value;

                if (value.ArticlePackingInfo.UseBatch && value.EAN != null)
                    BatchNum = ViewModelHelpers.GetBatchNumber(value.EAN);
            }
        }

        private ItemInfo _itemInfo;
        public ItemInfo ItemInfo
        {
            get => _itemInfo;
            set
            {
                _itemInfo = value;

                ArticleInfo = value.ArticleInfo;
                PackDesc = value.PackDesc;
                if(string.IsNullOrEmpty(BatchNum)) BatchNum = value.BatchNum; // if batch is not set from EAN 
            }
        }

        private EANCode _ean;
        public EANCode EAN
        {
            get => _ean;
            set
            {
                _ean = value;
                if (value == null) return;

                BatchNum = ViewModelHelpers.GetBatchNumber(value);
            }
        }
    }
}
﻿using S4.ProcedureModels.Dispatch;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace S4mp.InternalControls.Dispatch
{
    #region Commands
    public partial class ShowDispatchInstructionControl : ContentView
    {
        public static readonly BindableProperty BadPositionCommandProperty = BindableProperty.Create(nameof(BadPositionCommand), typeof(ICommand), typeof(SelectDispatchItemControl));
        public ICommand BadPositionCommand
        {
            get => (ICommand)GetValue(BadPositionCommandProperty);
            set => SetValue(BadPositionCommandProperty, value);
        }

        public static readonly BindableProperty LoadUpCommandProperty = BindableProperty.Create(nameof(LoadUpCommand), typeof(ICommand), typeof(SelectDispatchItemControl));
        public ICommand LoadUpCommand
        {
            get => (ICommand)GetValue(LoadUpCommandProperty);
            set => SetValue(LoadUpCommandProperty, value);
        }

        #endregion

        public static readonly BindableProperty HeaderTextProperty = BindableProperty.Create(nameof(HeaderText), typeof(string), typeof(ShowDispatchInstructionControl));
        public string HeaderText
        {
            get => (string)GetValue(HeaderTextProperty);
            set => SetValue(HeaderTextProperty, value);
        }

        public static readonly BindableProperty HeaderTextSubProperty = BindableProperty.Create(nameof(HeaderTextSub), typeof(string), typeof(ShowDispatchInstructionControl));
        public string HeaderTextSub
        {
            get => (string)GetValue(HeaderTextSubProperty);
            set => SetValue(HeaderTextSubProperty, value);
        }

        public static readonly BindableProperty ItemInstructProperty = BindableProperty.Create(nameof(ItemInstruct), typeof(DispatchItemInstruct), typeof(ShowDispatchInstructionControl));
        public DispatchItemInstruct ItemInstruct
        {
            get => (DispatchItemInstruct)GetValue(ItemInstructProperty);
            set => SetValue(ItemInstructProperty, value);
        }

        public static readonly BindableProperty ShowUserRemarkProperty = BindableProperty.Create(nameof(ShowUserRemark), typeof(bool), typeof(ShowDispatchInstructionControl));
        public bool ShowUserRemark
        {
            get => (bool)GetValue(ShowUserRemarkProperty);
            set => SetValue(ShowUserRemarkProperty, value);
        }

        public ShowDispatchInstructionControl()
        {
            InitializeComponent();
        }

        public async Task LoadData(int stoMoveID, int stoMoveItemID)
        {
            // Client
            var client = new Client.DispatchClient();
            var result = await client.GetItemInstruction(new DispatchItemInstruct { 
                StoMoveID = stoMoveID,
                StoMoveItemID = stoMoveItemID
            });

            ShowUserRemark = !string.IsNullOrEmpty(result.Item.UserRemark);
            ItemInstruct = result;
        }
    }
}
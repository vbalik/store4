﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace S4mp.InternalControls.Dispatch
{
    public partial class DispatchSetWeightBoxCountControl : ContentView
    {
        /// <summary>
        /// Bindable OnSelectItemCmdProperty<ICommand>
        /// </summary>
        public static readonly BindableProperty OnSelectItemCmdProperty = BindableProperty.Create(nameof(OnSelectItemCmd), typeof(ICommand), typeof(PrintDispatchLabelsControl));
        public ICommand OnSelectItemCmd
        {
            get => (ICommand)GetValue(OnSelectItemCmdProperty);
            set => SetValue(OnSelectItemCmdProperty, value);
        }

        public static readonly BindableProperty HeaderTextProperty = BindableProperty.Create(nameof(HeaderText), typeof(string), typeof(PrintDispatchLabelsControl));
        public string HeaderText
        {
            get => (string)GetValue(HeaderTextProperty);
            set => SetValue(HeaderTextProperty, value);
        }

        public static readonly BindableProperty HeaderTextSubProperty = BindableProperty.Create(nameof(HeaderTextSub), typeof(string), typeof(PrintDispatchLabelsControl));
        public string HeaderTextSub
        {
            get => (string)GetValue(HeaderTextSubProperty);
            set => SetValue(HeaderTextSubProperty, value);
        }

        /// <summary>
        /// Values
        /// </summary>
        public static readonly BindableProperty WeightProperty = BindableProperty.Create(nameof(Weight), typeof(decimal?), typeof(PrintDispatchLabelsControl));
        public decimal? Weight
        {
            get => (decimal?)GetValue(WeightProperty);
            set => SetValue(WeightProperty, value);
        }

        public static readonly BindableProperty BoxesCountProperty = BindableProperty.Create(nameof(BoxesCount), typeof(int?), typeof(PrintDispatchLabelsControl));
        public int? BoxesCount
        {
            get => (int?)GetValue(BoxesCountProperty);
            set => SetValue(BoxesCountProperty, value);
        }


        public event EventHandler<bool> ValuesChangedEvent;

        public DispatchSetWeightBoxCountControl()
        {
            InitializeComponent();
            FirstEntry_Focus();
        }

        #region UI handles

        private void EdWeight_TextChanged(object sender, TextChangedEventArgs e)
        {
            weightFrame.HasError(!decimal.TryParse(e.NewTextValue, out var val) || val < 0);
        }

        private void EdBoxesCount_TextChanged(object sender, TextChangedEventArgs e)
        {
            boxesCountFrame.HasError(!int.TryParse(e.NewTextValue, out var val) || val < 0);
        }

        private async void EdWeight_Completed(object sender, EventArgs e)
        {
            await Task.Delay(600);
            edBoxesCount.Focus();
        }

        private void EdBoxesCount_Completed(object sender, EventArgs e)
        {
            OnSelectItemCmd.Execute(true);
        }

        /// <summary>
        /// Focus entry
        /// </summary>
        private async void FirstEntry_Focus()
        {
            await Task.Delay(600);
            edWeight.Focus();
        }

        #endregion
    }
}
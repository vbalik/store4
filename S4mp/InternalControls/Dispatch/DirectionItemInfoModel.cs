﻿using S4.Entities.Helpers;

namespace S4mp.InternalControls.Dispatch
{
    public class DispatchItemInfoModel : ItemInfo
    {
        public string PosCode => XtraString;

        public DispatchItemInfoModel()
        {

        }

        public DispatchItemInfoModel(ItemInfo model)
        {
            StoMoveLotID = model.StoMoveLotID;
            StoMoveItemID = model.StoMoveItemID;
            ArticleID = model.ArticleID;
            ArtPackID = model.ArtPackID;
            ArticleCode = model.ArticleCode;
            ArticleDesc = model.ArticleDesc;
            PackDesc = model.PackDesc;
            UseBatch = model.UseBatch;
            UseExpiration = model.UseExpiration;
            Quantity = model.Quantity;
            BatchNum = model.BatchNum;
            ExpirationDate = model.ExpirationDate;
            CarrierNum = model.CarrierNum;
            WholeCarrier = model.WholeCarrier;
            From_positionID = model.From_positionID;
            From_posCode = model.From_posCode;
            To_positionID = model.To_positionID;
            To_posCode = model.To_posCode;
            Remark = model.Remark;
            XtraString = model.XtraString;
            XtraInt = model.XtraInt;
            XtraBool = model.XtraBool;
            XtraDate = model.XtraDate;
            DocPosition = model.DocPosition;
            StoMoveID = model.StoMoveID;
            SpecialDelivery = model.SpecialDelivery;
            SpecFeaturesInt = model.SpecFeaturesInt;
        }
    }
}

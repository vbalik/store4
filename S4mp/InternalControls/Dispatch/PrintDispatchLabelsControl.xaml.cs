﻿using S4.Core;
using S4.Entities;
using S4.ProcedureModels.Dispatch;
using S4mp.Client;
using S4mp.InternalControls.Printer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using static S4.ProcedureModels.Dispatch.DispatchCalcBoxLabels;

namespace S4mp.InternalControls.Dispatch
{
    public partial class PrintDispatchLabelsControl : ContentView
    {
        /// <summary>
        /// Bindable OnSelectItemCmdProperty<ICommand>
        /// </summary>
        public static readonly BindableProperty OnSelectItemCmdProperty = BindableProperty.Create(nameof(OnSelectItemCmd), typeof(ICommand), typeof(PrintDispatchLabelsControl));
        public ICommand OnSelectItemCmd
        {
            get => (ICommand)GetValue(OnSelectItemCmdProperty);
            set => SetValue(OnSelectItemCmdProperty, value);
        }

        public static readonly BindableProperty HeaderTextProperty = BindableProperty.Create(nameof(HeaderText), typeof(string), typeof(PrintDispatchLabelsControl));
        public string HeaderText
        {
            get => (string)GetValue(HeaderTextProperty);
            set => SetValue(HeaderTextProperty, value);
        }

        public static readonly BindableProperty HeaderTextSubProperty = BindableProperty.Create(nameof(HeaderTextSub), typeof(string), typeof(PrintDispatchLabelsControl));
        public string HeaderTextSub
        {
            get => (string)GetValue(HeaderTextSubProperty);
            set => SetValue(HeaderTextSubProperty, value);
        }

        public int? PrinterReportID { get; set; }
        public int? PrinterLocationID { get; set; }

        public ObservableCollection<BoxLabel> BoxLabels = new ObservableCollection<BoxLabel>();
        public List<S4.Entities.PrinterLocation> Printers = new List<S4.Entities.PrinterLocation>();

        public event EventHandler<bool> PrinterChangedEvent;

        public PrintDispatchLabelsControl()
        {
            InitializeComponent();
        }

        public async Task LoadData(int directionID)
        {
            BoxLabels.Clear();

            // Client
            var client = new DispatchClient();
            var calcBoxLabels = await client.GetCalcBoxLabels(new DispatchCalcBoxLabels
            {
                DirectionID = directionID
            });

            if (!calcBoxLabels.Success)
            {
                Singleton<Helpers.Audio>.Instance.PlayError();
                await App.Current.MainPage.DisplayAlert("Upozornění", calcBoxLabels.ErrorText, "OK");
                return;
            }

            // compress box labels
            calcBoxLabels.BoxLabels = calcBoxLabels.BoxLabels.GroupBy(
                k => k.LabelText,
                (k, i) => new BoxLabel() {
                    LabelText = k,
                    LabelQuantity = i.Sum(_ => _.LabelQuantity),
                    DirectionID = directionID,
                    DocPosition = i.Min(_ => _.DocPosition)
                })
                .ToList();

            calcBoxLabels.BoxLabels.ForEach(item => BoxLabels.Add(item));
            boxLabelsListView.ItemsSource = BoxLabels;

            setPrinter.PrinterLocations = new ObservableCollection<PrinterLocation>(calcBoxLabels.Printers);
            setPrinter.Reports = calcBoxLabels.Reports;
            setPrinter.PrinterCacheType = Core.PrinterCacheTypeEnum.Dispatch;
            setPrinter.SuccessCommand = new Command<PrinterByReportResultModel>(GetPrinter);
            (setPrinter as SetPrinterByReportControl).Initialization();
        }

        private void GetPrinter(PrinterByReportResultModel printerResult)
        {
            PrinterReportID = printerResult.PrinterReportID;
            PrinterLocationID = printerResult.PrinterLocationID;

            PrinterChangedEvent?.Invoke(this, printerResult.PrinterLocationID != null);
        }

        #region UI handles

        private void Quantity_Completed(object sender, EventArgs e)
        {
            var entry = sender as Entry; // TODO focus on next entry
        }

        #endregion
    }
}
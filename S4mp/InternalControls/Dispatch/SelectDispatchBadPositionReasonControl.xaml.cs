﻿using S4.ProcedureModels;
using System;
using System.Collections.Generic;
using System.Windows.Input;
using Xamarin.Forms;

namespace S4mp.InternalControls.Dispatch
{
    public partial class SelectDispatchBadPositionReasonControl : ContentView
    {
        /// <summary>
        /// Bindable OnSuccessCommand<ICommand>
        /// </summary>
        public static readonly BindableProperty OnSuccessCommandProperty = BindableProperty.Create(nameof(OnSuccessCommand), typeof(ICommand), typeof(SelectDispatchItemControl));
        public ICommand OnSuccessCommand
        {
            get => (ICommand)GetValue(OnSuccessCommandProperty);
            set => SetValue(OnSuccessCommandProperty, value);
        }

        public static readonly BindableProperty HeaderTextProperty = BindableProperty.Create(nameof(HeaderText), typeof(string), typeof(SelectDispatchBadPositionReasonControl));
        public string HeaderText
        {
            get => (string)GetValue(HeaderTextProperty);
            set => SetValue(HeaderTextProperty, value);
        }

        public static readonly BindableProperty HeaderTextSubProperty = BindableProperty.Create(nameof(HeaderTextSub), typeof(string), typeof(SelectDispatchBadPositionReasonControl));
        public string HeaderTextSub
        {
            get => (string)GetValue(HeaderTextSubProperty);
            set => SetValue(HeaderTextSubProperty, value);
        }

        public static readonly BindableProperty ReasonProperty = BindableProperty.Create(nameof(Reason), typeof(PositionErrorReason), typeof(SelectDispatchBadPositionReasonControl));
        public PositionErrorReason Reason
        {
            get => (PositionErrorReason)GetValue(ReasonProperty);
            set => SetValue(ReasonProperty, value);
        }

        public SelectDispatchBadPositionReasonControl()
        {
            InitializeComponent();

            edReason.SelectedIndexChanged += Reason_SelectedIndexChanged;

            var options = new List<string>();
            foreach(var name in Enum.GetNames(typeof(PositionErrorReason)))
            {
                options.Add(name);
            }

            edReason.ItemsSource = options;
        }

        private void Reason_SelectedIndexChanged(object sender, EventArgs e)
        {
            var picker = (Picker)sender;
            int selectedIndex = picker.SelectedIndex;
            Reason = (PositionErrorReason) Enum.Parse(typeof(PositionErrorReason), (string)picker.ItemsSource[selectedIndex]);

            OnSuccessCommand.Execute(selectedIndex != -1);
        }
    }
}
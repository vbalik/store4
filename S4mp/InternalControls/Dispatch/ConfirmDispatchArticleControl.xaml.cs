﻿using S4.Core;
using S4.Entities.Helpers;
using S4mp.InternalControls.GetArticle;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace S4mp.InternalControls.Dispatch
{
    public partial class ConfirmDispatchArticleControl : ContentView
    {
        public ConfirmDispatchArticleControl()
        {
            InitializeComponent();
        }

        private ConfirmDispatchArticleVM ViewModel => (ConfirmDispatchArticleVM)BindingContext;

        public void LoadData(ItemInfo item, GetArticleResultModel articleResultModel)
        {
            ViewModel.ArticleResultModel = articleResultModel;
            ViewModel.ItemInfo = item;
            if (!item.UseBatch) batchNumBlock.IsVisible = false;

            Entry_Focus(edQuantity);
        }

        public void SetHeaderText(string headerText, string headerTextSub = null)
        {
            ViewModel.HeaderText = headerText;
            ViewModel.HeaderTextSub = headerTextSub;
        }

        public event EventHandler OnClosingEvent;

        public bool IsValid()
        {
            if (!_isFormValid())
                return false;

            // validation
            decimal.TryParse(ViewModel.Quantity, out var qty);

            var errorExist = false;
            var errorMessage = string.Empty;

            if (ViewModel.ArticleResultModel.ArticlePackingInfo.ArticleID != ViewModel.ItemInfo.ArticleID)
            {
                errorMessage += (string.IsNullOrEmpty(errorMessage) ? string.Empty : ", ") + "kód";
                errorExist = true;
            }

            if(qty != ViewModel.ItemInfo.Quantity)
            {
                errorMessage += (string.IsNullOrEmpty(errorMessage) ? string.Empty : ", ") + "množství";
                errorExist = true;
            }

            if (ViewModel.ItemInfo.UseBatch && !ViewModel.BatchNum.Equals(ViewModel.ItemInfo.BatchNum, System.StringComparison.OrdinalIgnoreCase))
            {
                errorMessage += (string.IsNullOrEmpty(errorMessage) ? string.Empty : ", ") + "šarže";
                errorExist = true;
            }

            if (errorExist)
            {
                Singleton<Helpers.Audio>.Instance.PlayError();
                App.Current.MainPage.DisplayAlert("Upozornění", $"{errorMessage.Substring(0, 1).ToUpper() + errorMessage.Substring(1)} položky nesouhlasí.", "OK");
                return false;
            }

            return true;
        }

        private bool _isFormValid()
        {
            var isValid = true;

            if (string.IsNullOrEmpty(ViewModel.Quantity) || !int.TryParse(ViewModel.Quantity, out int batchNum))
            {
                if (isValid) Entry_Focus(edQuantity);
                isValid = false;
                edQuantityErrorFrame.HasError();
            }
            if (ViewModel.ItemInfo.UseBatch && string.IsNullOrEmpty(ViewModel.BatchNum))
            {
                if (isValid) Entry_Focus(edBatchNum);
                isValid = false;
                edBatchNumErrorFrame.HasError();
            }

            return isValid;
        }

        #region UI handles

        /// <summary>
        /// On focus entry reset error fields
        /// </summary>
        private void Entry_Focused(object sender, FocusEventArgs e)
        {
            if (sender == edQuantity)
                edQuantityErrorFrame.HasError(false);
            else if (sender == edBatchNum)
                edBatchNumErrorFrame.HasError(false);
        }

        /// <summary>
        /// Focus entry
        /// </summary>
        private async void Entry_Focus(Entry field)
        {
            await Task.Delay(800);
            field.Focus();
        }

        private void ConfirmItem_Completed(object sender, EventArgs e)
        {
            OnClosingEvent.Invoke(this, null);
        }

        #endregion
    }
}
﻿using System.Windows.Input;
using Xamarin.Forms;

namespace S4mp.InternalControls.Common.Test
{
    public partial class SingleSearchControl : ContentView
    {
        public SingleSearchControl()
        {
            InitializeComponent();

            CustomCmdParameter = this;
        }

        public static readonly BindableProperty LblTitleProperty = BindableProperty.Create(nameof(LblTitle), typeof(string), typeof(SingleSearchControl));
        public string LblTitle
        {
            get => (string) GetValue(LblTitleProperty);
            set => SetValue(LblTitleProperty, value);
        }

        public static readonly BindableProperty ValueProperty = BindableProperty.Create(nameof(Value), typeof(string), typeof(SingleSearchControl));
        public string Value
        {
            get => (string)GetValue(ValueProperty);
            set => SetValue(ValueProperty, value);
        }

        public static readonly BindableProperty CustomCmdProperty = BindableProperty.Create(nameof(CustomCmd), typeof(ICommand), typeof(SingleSearchControl));
        public ICommand CustomCmd
        {
            get => (ICommand)GetValue(CustomCmdProperty);
            set => SetValue(CustomCmdProperty, value);
        }

        #region Command parameter

        public static readonly BindableProperty CustomCmdParameterProperty = BindableProperty.Create(nameof(CustomCmdParameter), typeof(object), typeof(SingleSearchControl));
        public object CustomCmdParameter
        {
            get => (object)GetValue(CustomCmdParameterProperty);
            set => SetValue(CustomCmdParameterProperty, value);
        }

        public static void Execute(ICommand command, object parameters)
        {
            if (command == null) return;
            if (command.CanExecute(null))
            {
                command.Execute(parameters);
            }
        }
        #endregion

        #region events

        public ICommand OnSearchCmd => new Command(() => Execute(CustomCmd, CustomCmdParameter));

        #endregion

    }
}
﻿using S4mp.Core;
using System;
using Xamarin.Forms;
using System.Threading.Tasks;

namespace S4mp.InternalControls.StoreMove
{
    public partial class StoreMoveSelectItemMoveQuantityControl : ContentView
    {
        public static readonly BindableProperty HeaderTextProperty = BindableProperty.Create(nameof(HeaderText), typeof(string), typeof(SetStoreMoveRemarkControl));
        public string HeaderText
        {
            get => (string)GetValue(HeaderTextProperty);
            set => SetValue(HeaderTextProperty, value);
        }

        public static readonly BindableProperty HeaderTextSubProperty = BindableProperty.Create(nameof(HeaderTextSub), typeof(string), typeof(SetStoreMoveRemarkControl));
        public string HeaderTextSub
        {
            get => (string)GetValue(HeaderTextSubProperty);
            set => SetValue(HeaderTextSubProperty, value);
        }

        public static readonly BindableProperty QuantityMoveSelectedProperty = BindableProperty.Create(nameof(QuantityMoveSelected), typeof(decimal?), typeof(SetStoreMoveRemarkControl));
        public decimal? QuantityMoveSelected
        {
            get => (decimal?)GetValue(QuantityMoveSelectedProperty);
            set => SetValue(QuantityMoveSelectedProperty, value);
        }

        public event EventHandler<bool> OnClosingEvent;

        public SelectableListViewItem<StoreMoveArticleInfo> Item { get; private set; }

        public StoreMoveSelectItemMoveQuantityControl()
        {
            InitializeComponent();
        }

        public void LoadData(SelectableListViewItem<StoreMoveArticleInfo> item)
        {
            Item = item;
            QuantityMoveSelected = item.Item.Quantity;
        }

        private bool _isFormValid()
        {
            var isValid = true;

            if(QuantityMoveSelected == null || QuantityMoveSelected < 1 || QuantityMoveSelected > Item.Item.Quantity)
            {
                isValid = false;
            }

            edQuantityErrorFrame.HasError(!isValid);

            return isValid;
        }

        #region UI handles

        /// <summary>
        /// On focus entry reset error fields
        /// </summary>
        private void Entry_Focused(object sender, FocusEventArgs e)
        {
            QuantityMoveSelected = null;
            OnClosingEvent.Invoke(this, false); // disable ok btn
            edQuantityErrorFrame.HasError(false);
        }

        private void Entry_Completed(object sender, EventArgs e)
        {
            OnClosingEvent.Invoke(this, _isFormValid());
        }

        private void edQuantityMoveSelected_TextChanged(object sender, TextChangedEventArgs e)
        {
            if(string.IsNullOrEmpty(e.NewTextValue)) QuantityMoveSelected = null;
            OnClosingEvent.Invoke(this, _isFormValid());
        }

        #endregion
    }
}
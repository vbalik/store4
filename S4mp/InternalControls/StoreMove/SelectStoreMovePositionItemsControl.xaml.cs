﻿using System;
using Xamarin.Forms;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using S4mp.Core;
using S4.Entities.Helpers;
using System.Linq;
using S4mp.Dialogs;
using System.Threading.Tasks;
using S4mp.Controls.Common;

namespace S4mp.InternalControls.StoreMove
{
    public partial class SelectStoreMovePositionItemsControl : ContentView
    {
        public static readonly BindableProperty HeaderTextProperty = BindableProperty.Create(nameof(HeaderText), typeof(string), typeof(SelectStoreMovePositionItemsControl));
        public string HeaderText
        {
            get => (string)GetValue(HeaderTextProperty);
            set => SetValue(HeaderTextProperty, value);
        }

        public static readonly BindableProperty HeaderTextSubProperty = BindableProperty.Create(nameof(HeaderTextSub), typeof(string), typeof(SelectStoreMovePositionItemsControl));
        public string HeaderTextSub
        {
            get => (string)GetValue(HeaderTextSubProperty);
            set => SetValue(HeaderTextSubProperty, value);
        }

        private List<ArticleInfo> _loadItems = new List<ArticleInfo>();
        public List<ArticleInfo> SelectedItems => GetSelectedItems();
        public ObservableCollection<SelectableListViewItem<StoreMoveArticleInfo>> SourceItems = new ObservableCollection<SelectableListViewItem<StoreMoveArticleInfo>>();

        public event EventHandler SelectItemEvent;
        public event EventHandler NoItemsFoundEvent;

        public SelectStoreMovePositionItemsControl()
        {
            InitializeComponent();
        }

        public void LoadData(List<ArticleInfo> items)
        {
            _loadItems = items;

            activityIndicator.Show();
            try
            {
                items.ForEach(item =>
                {
                    SourceItems.Add(new SelectableListViewItem<StoreMoveArticleInfo>
                    {
                        Item = new StoreMoveArticleInfo(item)
                    });
                });

                itemListView.ItemsSource = SourceItems;

                NoItemsFoundEvent.Invoke(this, null);
            }
            finally
            {
                activityIndicator.Hide();
            }
        }

        private List<ArticleInfo> GetSelectedItems()
        {
            var selectedItems = new List<ArticleInfo>();
            foreach (var item in SourceItems as ObservableCollection<SelectableListViewItem<StoreMoveArticleInfo>>)
                if (item.IsSelected)
                {
                    var articleInfo = _loadItems.SingleOrDefault(_ => _.ArtPackID == item.Item.ArtPackID
                        && _.BatchNum == item.Item.BatchNum
                        && _.CarrierNum == item.Item.CarrierNum
                        && _.ExpirationDate?.ToString("yyMMdd") == item.Item.ExpirationDate?.ToString("yyMMdd"));
                    articleInfo.Quantity = item.Item.QuantityMoveSelected;
                    selectedItems.Add(articleInfo);
                }

            return selectedItems;
        }

        public void StoreMoveSelectItemMoveQuantityDialog_Closed(object sender, ButtonsBar.ButtonsEnum e)
        {
            var dialog = sender as StoreMoveSelectItemMoveQuantityDialog;

            if (e != ButtonsBar.ButtonsEnum.OK)
            {
                dialog.Item.IsSelected = false;
                SelectItemEvent?.Invoke(this, null);

                return;
            }

            var sitem = SourceItems.SingleOrDefault(_ => _.Item.ArticleID == dialog.Item.Item.ArticleID
                && _.Item.ExpirationDate?.ToString("yyMMdd") == dialog.Item.Item.ExpirationDate?.ToString("yyMMdd")
                && _.Item.BatchNum == dialog.Item.Item.BatchNum
                && _.Item.CarrierNum == dialog.Item.Item.CarrierNum);
            sitem.Item.QuantityMoveSelected = dialog.QuantityMoveSelected;

            _RefreshItemListView();
        }

        private async Task _showSelectMoveQuantityDialog(SelectableListViewItem<StoreMoveArticleInfo> item)
        {
            var qtyDlg = new StoreMoveSelectItemMoveQuantityDialog();
            qtyDlg.SetHeaderText($"Přeskladnění - {item.Item.ArticleCode}", "Zadejte množství");
            qtyDlg.SetData(item);
            qtyDlg.Closed += StoreMoveSelectItemMoveQuantityDialog_Closed;
            await Navigation.PushModalAsync(qtyDlg, false);
        }

        private void _RefreshItemListView()
        {
            itemListView.ItemsSource = null;
            itemListView.ItemsSource = SourceItems;
        }

        private async void storeMoveItemListView_ItemTapped(object value)
        {
            var item = (value as SelectableListViewItem<StoreMoveArticleInfo>);

            item.IsSelected = item.IsSelected ? false : true;
            itemListView.SelectedItem = null;

            if (item.IsSelected)
            {
                await _showSelectMoveQuantityDialog(item);
            }
            else
            {
                item.Item.QuantityMoveSelected = 0;
                _RefreshItemListView();
            }

            SelectItemEvent?.Invoke(this, null);
        }

        #region UI Handlers

        public Command OnTapped => new Command(storeMoveItemListView_ItemTapped);

        #endregion
    }
}
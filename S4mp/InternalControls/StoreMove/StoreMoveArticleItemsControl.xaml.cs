﻿using S4.Entities;
using S4.Entities.Views;
using S4mp.Client;
using S4mp.Controls.Common;
using S4mp.Core;
using S4mp.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace S4mp.InternalControls.StoreMove
{
    public partial class StoreMoveArticleItemsControl : ContentView
    {
        public static readonly BindableProperty HeaderTextProperty = BindableProperty.Create(nameof(HeaderText), typeof(string), typeof(StoreMoveNavigationControl));
        public string HeaderText
        {
            get => (string)GetValue(HeaderTextProperty);
            set => SetValue(HeaderTextProperty, value);
        }

        public static readonly BindableProperty HeaderTextSubProperty = BindableProperty.Create(nameof(HeaderTextSub), typeof(string), typeof(StoreMoveNavigationControl));
        public string HeaderTextSub
        {
            get => (string)GetValue(HeaderTextSubProperty);
            set => SetValue(HeaderTextSubProperty, value);
        }

        private List<ArtOnStoreView> _loadItems = new List<ArtOnStoreView>();
        public List<ArtOnStoreView> SelectedItems => GetSelectedItems();
        public ObservableCollection<SelectableListViewItem<StoreMoveArticleInfo>> SourceItems = new ObservableCollection<SelectableListViewItem<StoreMoveArticleInfo>>();
        public Position SourcePosition { get; set; }

        public event EventHandler SelectItemEvent;
        public event EventHandler NoItemsFoundEvent;

        public StoreMoveArticleItemsControl()
        {
            InitializeComponent();
        }

        public async void LoadData(int articleID)
        {
            articleItemsListView.ItemsSource = SourceItems;

            activityIndicator.Show();
            try
            {
                var client = new ArtOnStoreClient();
                _loadItems = await client.GetByArticle(articleID);

                _loadItems.ForEach(item => SourceItems.Add(new SelectableListViewItem<StoreMoveArticleInfo>
                {
                    Item = new StoreMoveArticleInfo(item)
                }));

                NoItemsFoundEvent.Invoke(this, null);
            }
            finally
            {
                activityIndicator.Hide();
            }
        }

        private List<ArtOnStoreView> GetSelectedItems()
        {
            var selectedItems = new List<ArtOnStoreView>();
            foreach (var item in articleItemsListView.ItemsSource as ObservableCollection<SelectableListViewItem<StoreMoveArticleInfo>>)
                if (item.IsSelected)
                {
                    var artOnStore = _loadItems.SingleOrDefault(_ => _.ArtPackID == item.Item.ArtPackID
                        && _.PosCode == item.Item.PosCode
                        && _.ExpirationDate?.ToString("yyMMdd") == item.Item.ExpirationDate?.ToString("yyMMdd")
                        && _.BatchNum == item.Item.BatchNum
                        && _.CarrierNum == item.Item.CarrierNum);
                    artOnStore.Quantity = item.Item.QuantityMoveSelected;
                    selectedItems.Add(artOnStore);
                }

            return selectedItems;
        }

        private async Task SetPositionID()
        {
            var selectedItems = GetSelectedItems();
            if (SourcePosition == null && selectedItems.Count > 0)
            {
                var client = new PositionClient();
                var positions = await client.FindPosition(selectedItems[0].PosCode);
                foreach (var p in positions)
                {
                    if (SourcePosition != null) return; // skip

                    if (p.PosCode == selectedItems[0].PosCode)
                    {
                        SourcePosition = p;
                        HeaderTextSub = $"Vybete položky na pozici {p.PosCode}";
                    }
                }
            }
            else if (selectedItems.Count == 0)
            {
                SourcePosition = null;
                HeaderTextSub = "Vybete položky";
            }

            DisableSourceItems();
        }

        private void DisableSourceItems()
        {
            foreach (var item in SourceItems)
            {
                item.IsDisabled = SourcePosition != null && SourcePosition.PosCode != item.Item.PosCode;
            }
        }

        public void StoreMoveSelectItemMoveQuantityDialog_Closed(object sender, ButtonsBar.ButtonsEnum e)
        {
            var dialog = sender as StoreMoveSelectItemMoveQuantityDialog;

            if (e != ButtonsBar.ButtonsEnum.OK)
            {
                dialog.Item.IsSelected = false;
                return;
            }

            var sitem = SourceItems.SingleOrDefault(_ => _.Item.ArtPackID == dialog.Item.Item.ArtPackID
                            && _.Item.PosCode == dialog.Item.Item.PosCode
                            && _.Item.ExpirationDate == dialog.Item.Item.ExpirationDate
                            && _.Item.ExpirationDate?.ToString("yyMMdd") == dialog.Item.Item.ExpirationDate?.ToString("yyMMdd")
                            && _.Item.BatchNum == dialog.Item.Item.BatchNum
                            && _.Item.CarrierNum == dialog.Item.Item.CarrierNum);

            sitem.Item.QuantityMoveSelected = dialog.QuantityMoveSelected;

            _RefreshItemListView();
        }

        private async Task _showSelectMoveQuantityDialog(SelectableListViewItem<StoreMoveArticleInfo> item)
        {
            var qtyDlg = new StoreMoveSelectItemMoveQuantityDialog();
            qtyDlg.SetHeaderText($"Přeskladnění - {item.Item.ArticleCode}", "Zadejte množství");
            qtyDlg.SetData(item);
            qtyDlg.Closed += StoreMoveSelectItemMoveQuantityDialog_Closed;
            await Navigation.PushModalAsync(qtyDlg, false);
        }

        private void _RefreshItemListView()
        {
            articleItemsListView.ItemsSource = null;
            articleItemsListView.ItemsSource = SourceItems;
        }

        private async void storeMoveItemListView_ItemTapped(object value)
        {
            var item = (value as SelectableListViewItem<StoreMoveArticleInfo>);

            // skip if position not match
            if (SourcePosition != null && item.Item.PosCode != SourcePosition.PosCode) return;

            item.IsSelected = item.IsSelected ? false : true;
            articleItemsListView.SelectedItem = null;

            await SetPositionID();

            if (item.IsSelected)
            {
                await _showSelectMoveQuantityDialog(item);
            }
            else
            {
                item.Item.QuantityMoveSelected = 0;
                _RefreshItemListView();
            }

            SelectItemEvent?.Invoke(this, null);
        }

        #region UI Handlers

        public Command OnTapped => new Command(storeMoveItemListView_ItemTapped);

        #endregion
    }
}
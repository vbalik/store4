﻿using S4.Entities.Helpers;
using S4.Entities.Views;

namespace S4mp.InternalControls.StoreMove
{
    public class StoreMoveArticleInfo : ArticleInfo
    {
        /// <summary>
        /// Quantity selected for move
        /// </summary>
        public decimal QuantityMoveSelected { get; set; }

        public string ArticleInfo => $"{ArticleCode} - {ArticleDesc}";
        public string PosCode { get; set; }

        public StoreMoveArticleInfo()
        {

        }

        public StoreMoveArticleInfo(ArticleInfo model)
        {
            ArticleID = model.ArticleID;
            ArtPackID = model.ArtPackID;
            ArticleCode = model.ArticleCode;
            ArticleDesc = model.ArticleDesc;
            PackDesc = model.PackDesc;
            Quantity = model.Quantity;
            BatchNum = model.BatchNum;
            ExpirationDate = model.ExpirationDate;
            CarrierNum = model.CarrierNum;
        }

        public StoreMoveArticleInfo(ArtOnStoreView model)
        {
            ArticleID = model.ArticleID;
            ArtPackID = model.ArtPackID;
            ArticleCode = model.ArticleCode;
            ArticleDesc = model.ArticleDesc;
            PackDesc = model.PackDesc;
            Quantity = model.Quantity;
            BatchNum = model.BatchNum;
            ExpirationDate = model.ExpirationDate;
            CarrierNum = model.CarrierNum;
            PosCode = model.PosCode;
        }
    }
}

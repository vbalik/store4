﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using Xamarin.Forms;

namespace S4mp.InternalControls.StoreMove
{
    public partial class StoreMoveNavigationControl : ContentView
    {
        public static readonly BindableProperty HeaderTextProperty = BindableProperty.Create(nameof(HeaderText), typeof(string), typeof(StoreMoveNavigationControl));
        public string HeaderText
        {
            get => (string)GetValue(HeaderTextProperty);
            set => SetValue(HeaderTextProperty, value);
        }

        public static readonly BindableProperty HeaderTextSubProperty = BindableProperty.Create(nameof(HeaderTextSub), typeof(string), typeof(StoreMoveNavigationControl));
        public string HeaderTextSub
        {
            get => (string)GetValue(HeaderTextSubProperty);
            set => SetValue(HeaderTextSubProperty, value);
        }

        public static readonly BindableProperty SelectOptionCmdProperty = BindableProperty.Create(nameof(SelectOptionCmd), typeof(ICommand), typeof(StoreMoveNavigationControl));
        public ICommand SelectOptionCmd
        {
            get => (ICommand)GetValue(SelectOptionCmdProperty);
            set => SetValue(SelectOptionCmdProperty, value);
        }

        public event EventHandler<int> ButtonPressedEvent;

        public StoreMoveNavigationControl()
        {
            InitializeComponent();
            SelectOptionCmd = new Command<string>(SelectOption);

            buttonsListView.ItemsSource = new List<StoreMoveNavigationButton>() {
                new StoreMoveNavigationButton { Text = "karty", Parameter = "1", Image = "cubesolid.png" },
                new StoreMoveNavigationButton { Text = "pozice", Parameter = "2", Image = "stackexchangebrands.png" }
            };
        }

        private void SelectOption(string option)
        {
            if (int.TryParse(option, out var result))
                ButtonPressedEvent?.Invoke(this, result);
        }
    }

    public class StoreMoveNavigationButton
    {
        public string Text { get; set; }
        public string Parameter { get; set; }
        public string Image { get; set; }
    }
}
﻿using Xamarin.Forms;

namespace S4mp.InternalControls.StoreMove
{
    public partial class SetStoreMoveRemarkControl : ContentView
    {
        public static readonly BindableProperty HeaderTextProperty = BindableProperty.Create(nameof(HeaderText), typeof(string), typeof(SetStoreMoveRemarkControl));
        public string HeaderText
        {
            get => (string)GetValue(HeaderTextProperty);
            set => SetValue(HeaderTextProperty, value);
        }

        public static readonly BindableProperty HeaderTextSubProperty = BindableProperty.Create(nameof(HeaderTextSub), typeof(string), typeof(SetStoreMoveRemarkControl));
        public string HeaderTextSub
        {
            get => (string)GetValue(HeaderTextSubProperty);
            set => SetValue(HeaderTextSubProperty, value);
        }

        public static readonly BindableProperty RemarkProperty = BindableProperty.Create(nameof(Remark), typeof(string), typeof(SetStoreMoveRemarkControl));
        public string Remark
        {
            get => (string)GetValue(RemarkProperty);
            set => SetValue(RemarkProperty, value);
        }

        public SetStoreMoveRemarkControl()
        {
            InitializeComponent();
        }
    }
}
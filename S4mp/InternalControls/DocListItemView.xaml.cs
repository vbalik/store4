﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace S4mp.InternalControls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DocListItemView : ContentView
    {
        public DocListItemView()
        {
            InitializeComponent();
        }


        public static readonly BindableProperty ShowDocumentDetailProperty = BindableProperty.Create(nameof(ShowDocumentDetail), typeof(bool), typeof(DocListItemView), true);
        public bool ShowDocumentDetail
        {
            get => (bool)GetValue(ShowDocumentDetailProperty);
            set => SetValue(ShowDocumentDetailProperty, value);
        }

        public static readonly BindableProperty ShowDispatchPositionProperty = BindableProperty.Create(nameof(ShowDispatchPosition), typeof(bool), typeof(DocListItemView), false);
        public bool ShowDispatchPosition
        {
            get => (bool)GetValue(ShowDispatchPositionProperty);
            set => SetValue(ShowDispatchPositionProperty, value);
        }
    }
}
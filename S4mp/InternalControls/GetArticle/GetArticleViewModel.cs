﻿using System.Collections.Generic;
using System.Windows.Input;

using S4mp.Core.ViewModel;

namespace S4mp.InternalControls.GetArticle
{
    public class GetArticleViewModel : BaseViewModel
    {
        public GetArticleViewModel()
        {
            _articleInfoList = new List<S4.Entities.Models.ArticlePackingInfo>();
        }

        private string _articleCodePart;
        public string ArticleCodePart
        {
            get => _articleCodePart;
            set => SetField(ref _articleCodePart, value ?? string.Empty);
        }

        private string _articleDescPart;
        public string ArticleDescPart
        {
            get => _articleDescPart;
            set => SetField(ref _articleDescPart, value ?? string.Empty);
        }

        private string _barCode;
        public string BarCode
        {
            get => _barCode;
            set => SetField(ref _barCode, value ?? string.Empty);
        }

        private ICommand _searchCodeCmd;
        public ICommand SearchCodeCmd
        {
            get => _searchCodeCmd;
            set => SetField(ref _searchCodeCmd, value);
        }

        private ICommand _searchDescCmd;
        public ICommand SearchDescCmd
        {
            get => _searchDescCmd;
            set => SetField(ref _searchDescCmd, value);
        }

        private ICommand _searchBarCodeCmd;
        public ICommand SearchBarCodeCmd
        {
            get => _searchBarCodeCmd;
            set => SetField(ref _searchBarCodeCmd, value);
        }
        
        private string _headerText;
        public string HeaderText
        {
            get => _headerText;
            set => SetField(ref _headerText, value ?? string.Empty);
        }

        private string _headerTextSub;
        public string HeaderTextSub
        {
            get => _headerTextSub;
            set => SetField(ref _headerTextSub, value ?? string.Empty);
        }

        private List<S4.Entities.Models.ArticlePackingInfo> _articleInfoList;
        public List<S4.Entities.Models.ArticlePackingInfo> ArticleInfoList
        {
            get => _articleInfoList;
            set => SetField(ref _articleInfoList, value);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

using S4mp.Controls;
using System.Threading.Tasks;
using S4.Core;
using System.Linq;
using S4.Entities;

namespace S4mp.InternalControls.GetArticle
{
    public partial class GetArticleView : ContentView
    {
        public GetArticleView()
        {
            InitializeComponent();

            ViewModel.SearchCodeCmd = new Command(SearchByCode);
            ViewModel.SearchDescCmd = new Command(SearchByDesc);
            ViewModel.SearchBarCodeCmd = new Command(SearchByBarCode);
            ResetForm();
            Entry_Focus();

            ViewModel.HeaderText = "Hledat kartu";
        }

        public event EventHandler<GetArticleResultModel> ArticleResult;
        public event EventHandler<bool> ShowSearchEvent;

        public void ShowSearch(bool show)
        {
            if (show)
            {
                articleListView.IsVisible = false;
                Entry_Focus();
            }

            searchPanel.IsVisible = show;
            ShowSearchEvent?.Invoke(this, !show);
        }

        public void SetHeaderText(string headerText, string headerTextSub = null)
        {
            ViewModel.HeaderText = headerText;
            ViewModel.HeaderTextSub = headerTextSub;
        }

        public void UnsuccessfullExtScan(string barCode)
        {
            ViewModel.BarCode = barCode;
            searchBarCodeInputFrame.HasError();
        }


        private GetArticleViewModel ViewModel => (GetArticleViewModel)BindingContext;

        private async void SearchByCode()
        {
            ResetForm();
            ViewModel.ArticleDescPart = null;

            if (string.IsNullOrEmpty(ViewModel.ArticleCodePart))
                return;

            activityIndicator.Show();
            try
            {
                SetResult(searchCodeInputFrame, await DependencyService.Get<S4mp.Client.Interfaces.IArticleClient>().SearchByCode(ViewModel.ArticleCodePart));
            }
            finally
            {
                activityIndicator.Hide();
            }
        }

        private async void SearchByDesc()
        {
            ResetForm();
            ViewModel.ArticleCodePart = null;

            if (string.IsNullOrEmpty(ViewModel.ArticleDescPart))
                return;

            activityIndicator.Show();
            try
            {
                SetResult(searchDescInputFrame, await DependencyService.Get<S4mp.Client.Interfaces.IArticleClient>().SearchByDesc(ViewModel.ArticleDescPart));
            }
            finally
            {
                activityIndicator.Hide();
            }
        }

        private async void SearchByBarCode(object sender)
        {
            ResetForm();
            ViewModel.ArticleCodePart = null;
            ViewModel.ArticleDescPart = null;

            if (sender == scannerView)
            {
                ViewModel.BarCode = scannerView.Text;
            }
            if (string.IsNullOrEmpty(ViewModel.BarCode))
                return;

            activityIndicator.Show();
            try
            {
                SetResult(searchBarCodeInputFrame, await DependencyService.Get<S4mp.Client.Interfaces.IArticleClient>().SearchByBarCode(ViewModel.BarCode));
            }
            finally
            {
                activityIndicator.Hide();
            }
        }

        private void SetResult(Controls.Common.ErrorFrame frame, List<S4.Entities.Models.ArticlePackingInfo> articleInfoList)
        {
            articleListView.IsVisible = false;

            // filter only active article
            ViewModel.ArticleInfoList = articleInfoList.Where(_ => _.ArticleStatus != Article.AR_STATUS_DISABLED).ToList();

            if (articleInfoList == null || articleInfoList.Count == 0)
            {
                frame.HasError(true);
                Singleton<Helpers.Audio>.Instance.PlayError();
            }
            else
            {
                if (articleInfoList.Count == 1)
                {
                    OnArticleResult(articleInfoList[0]);
                    return;
                }



                activityIndicator.Hide();
                searchPanel.IsVisible = false;
                articleListView.IsVisible = true;
                ShowSearch(false);
            }
        }

        public void ResetForm()
        {
            ViewModel.ArticleInfoList = new List<S4.Entities.Models.ArticlePackingInfo>();
            searchCodeInputFrame.HasError(false);
            searchDescInputFrame.HasError(false);
            searchBarCodeInputFrame.HasError(false);
        }

        private async void OnArticleResult(S4.Entities.Models.ArticlePackingInfo result)
        {
            if (result.ArticlePacking == null)
            {
                result.ArticlePacking = await DependencyService.Get<S4mp.Client.Interfaces.IArticleClient>().GetMovablePacking(result.ArticleID);
            }

            var arg = new GetArticleResultModel()
            {
                ArticlePackingInfo = result,
                BarCodeID = scannerView.ID,
                BarCodeText = ViewModel.BarCode,
                EAN = scannerView.EAN
            };
            ArticleResult?.Invoke(this, arg);
        }

        #region UI handles

        /// <summary>
        /// Focus entry
        /// </summary>
        private async void Entry_Focus()
        {
            await Task.Delay(600);
            edArticleCode.Focus();
        }

        private void EdArticleCode_Completed(object sender, EventArgs e)
        {
            ViewModel.SearchCodeCmd.Execute(sender);
        }

        private void EdArticleDesc_Completed(object sender, EventArgs e)
        {
            ViewModel.SearchDescCmd.Execute(sender);
        }

        private void EdBarCode_Completed(object sender, EventArgs e)
        {
            ViewModel.SearchBarCodeCmd.Execute(sender);
        }

        private void ArticleListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            OnArticleResult(e.Item as S4.Entities.Models.ArticlePackingInfo);
        }

        private void EdArticlePart_Focused(object sender, FocusEventArgs e)
        {
            edBarCode.Text = string.Empty; // clear barcode if write text part
            searchBarCodeInputFrame.HasError(false);
        }

        #endregion
    }
}
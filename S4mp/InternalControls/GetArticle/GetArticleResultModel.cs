﻿using S4mp.Core;
using S4mp.Core.EANCode;

namespace S4mp.InternalControls.GetArticle
{
    public class GetArticleResultModel
    {
        public string BarCodeText { get; set; }
        public string BarCodeID { get; set; }
        public EANCode EAN { get; set; }
        public S4.Entities.Models.ArticlePackingInfo ArticlePackingInfo { get; set; }
    }
}

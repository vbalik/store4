﻿using System;

using Xamarin.Forms;
using Plugin.Battery;
using S4.Core;
using S4mp.Core;

namespace S4mp.Controls
{
    public class OptionsMenu
    {
        private ContentPage _page;
        private string _title;
        private string _subTitle;
        private ToolbarItem _lowBatteryBtn;

        public OptionsMenu(ContentPage page)
        {
            _page = page;

            FillToolBarItems();

            if (CrossBattery.IsSupported)
            {
                CrossBattery.Current.BatteryChanged += Current_BatteryChanged;
                SetBatteryStatus();
            }
        }

        public void FormatTitle(string title, string subTitle = null)
        {
            _title = title;
            _subTitle = subTitle;
            _page.Title = _title;

            if (_subTitle != null)
                FillToolBarItems();
        }

        public void FillToolBarItems()
        {
            _page.ToolbarItems.Clear();

            if (_subTitle != null)
            {
                _page.ToolbarItems.Add(new ToolbarItem() { Text = _subTitle, Order = ToolbarItemOrder.Primary, IsEnabled = false });
            }

            // battery indicator
            if (CrossBattery.IsSupported)
            {
                _lowBatteryBtn = new ToolbarItem("Slabá baterie", "batteryempty32.png", async () =>
                {
                    Singleton<Helpers.Audio>.Instance.PlayError();
                    await _page.DisplayAlert("Slabá baterie", $"Baterie je skoro vybitá, zbývá {CrossBattery.Current.RemainingChargePercent} %", "OK");
                }, ToolbarItemOrder.Primary);
            }

            // refresh
            if (_page is Pages.IRefreshable)
            {
                _page.ToolbarItems.Add(new ToolbarItem("Obnovit", "refresh16W.png", () =>
                {
                    (_page as Pages.IRefreshable).RefreshData();
                }, ToolbarItemOrder.Primary));
            }

            // device id
            var device = DependencyService.Get<IDevice>();
            var deviceID = device != null ? device.GetIdentifier() : "unknown";
            var tbiDeviceId = new ToolbarItem() { Text = "ID zař.:" + deviceID, Order = ToolbarItemOrder.Secondary };
            _page.ToolbarItems.Add(tbiDeviceId);

            // scan settings
            var tbiScanSettings = new ToolbarItem() { Text = "Načíst nastavení", Order = ToolbarItemOrder.Secondary };
            var scanSettingsPage = new Pages.Settings.ScanSettingsPage();
            tbiScanSettings.Clicked += async (object sender, EventArgs e) => await _page.Navigation.PushModalAsync(scanSettingsPage);
            _page.ToolbarItems.Add(tbiScanSettings);
            scanSettingsPage.SettingLoaded += async(s, e) =>
            {
                App.Instance.DoStart();
            };

            // settings
            var tbiSettings = new ToolbarItem() { Text = "Nastavení", Order = ToolbarItemOrder.Secondary };
            var settingDialog = new Pages.Settings.LocalSettingsPage();

            settingDialog.Closed += (sender, e) =>
            {
                if (e == Common.ButtonsBar.ButtonsEnum.OK)
                    S4mp.App.Instance.SaveSetting();
            };
            tbiSettings.Clicked += async (object sender, EventArgs e) => await _page.Navigation.PushModalAsync(settingDialog);
            _page.ToolbarItems.Add(tbiSettings);

            // logout
            var tbiLogon = new ToolbarItem() { Text = "Odhlásit uživatele", Order = ToolbarItemOrder.Secondary };
            tbiLogon.Clicked += async (sender, e) =>
            {
                var res = await App.Instance.MainPage.DisplayAlert("Odhlášení uživatele", "Opravdu se chcete odhlásit?", "Ano", "Ne");
                if (res)
                {
                    Singleton<BaseInfo>.Instance.Clear();
                    App.Instance.Login();
                }
            };
            _page.ToolbarItems.Add(tbiLogon);
        }

        private static bool _isLow;
        private void Current_BatteryChanged(object sender, Plugin.Battery.Abstractions.BatteryChangedEventArgs e)
        {
            _isLow = e.IsLow;
            SetBatteryStatus();
        }

        private void SetBatteryStatus()
        {
            if (_isLow && !_page.ToolbarItems.Contains(_lowBatteryBtn))
                _page.ToolbarItems.Add(_lowBatteryBtn);
            else if (!_isLow && _page.ToolbarItems.Contains(_lowBatteryBtn))
                _page.ToolbarItems.Remove(_lowBatteryBtn);
        }
    }
}


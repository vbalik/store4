﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Xamarin.Forms;
using S4mp.Controls;
using System.Threading.Tasks;
using S4.Core;
using S4mp.Client;

namespace S4mp.InternalControls.Positions
{
    public partial class SearchPositionControl : ContentView
    {
        /// <summary>
        /// Bindable OnSuccessCommand<ICommand>
        /// </summary>
        public static readonly BindableProperty OnSuccessCommandProperty = BindableProperty.Create(nameof(OnSuccessCommand), typeof(ICommand), typeof(SearchPositionControl));
        public ICommand OnSuccessCommand
        {
            get => (ICommand)GetValue(OnSuccessCommandProperty);
            set => SetValue(OnSuccessCommandProperty, value);
        }

        private SearchPositionControlVM ViewModel => (SearchPositionControlVM)BindingContext;

        public SearchPositionControl()
        {
            InitializeComponent();

            ViewModel.SearchCmd = new Command(SearchPosition);
            ViewModel.BackToSearchCmd = new Command(BackToSearch);
            ViewModel.SearchBarCodeCmd = new Command(SearchByBarCode);
            ViewModel.Positions = new ObservableCollection<S4.Entities.Position>();

            Entry_Focus();
        }

        public void UnsuccessfullExtScan(string barCode)
        {
            ViewModel.SearchPart = barCode;
            searchPartInputFrame.HasError();
        }

        private async void SearchPosition()
        {
            if (string.IsNullOrEmpty(ViewModel.SearchPart)) return;

            searchPartInputFrame.HasError(false);
            activityIndicator.Show();

            try
            {
                var client = DependencyService.Get<Client.Interfaces.IPositionClient>();
                var positions = await client.FindPosition(ViewModel.SearchPart);

                if (positions.Count == 0)
                {
                    searchPartInputFrame.HasError();
                    ViewModel.Positions.Clear();
                    Singleton<Helpers.Audio>.Instance.PlayError();
                    return;
                }

                if (positions.Count > 1)
                {
                    // exact position code found
                    foreach (var p in positions)
                    {
                        if (p.PosCode.Equals(ViewModel.SearchPart, StringComparison.OrdinalIgnoreCase))
                        {
                            ExecuteOnSuccessCommand(p);
                            return;
                        }
                    }

                    ViewModel.Positions = positions;
                    searchPanel.IsVisible = false;
                    btnBackToSearch.IsVisible = true;
                    return;
                }

                ExecuteOnSuccessCommand(positions[0]);
            }
            finally
            {
                activityIndicator.Hide();
            }
        }

        private async void SearchByBarCode(object sender)
        {
            ViewModel.SearchPart = scannerView.Text;

            if (string.IsNullOrEmpty(ViewModel.SearchPart))
                return;

            activityIndicator.Show();
            try
            {
                var client = DependencyService.Get<Client.Interfaces.IPositionClient>();
                var position = await client.GetPosition(ViewModel.SearchPart);
                if (position == null)
                {
                    searchPartInputFrame.HasError();
                    ViewModel.Positions.Clear();
                    return;
                }

                ExecuteOnSuccessCommand(position);
            }
            finally
            {
                activityIndicator.Hide();
            }
        }

        protected void ExecuteOnSuccessCommand(S4.Entities.Position position)
        {
            // Deselect Item
            positionListView.SelectedItem = null;
            //reset control
            ViewModel.Positions.Clear();
            ViewModel.SearchPart = null;
            searchPanel.IsVisible = true;
            btnBackToSearch.IsVisible = false;

            if (OnSuccessCommand == null) return;
            if (OnSuccessCommand.CanExecute(null))
            {
                OnSuccessCommand.Execute(position);
            }
        }

        protected void BackToSearch()
        {
            searchPanel.IsVisible = true;
            ViewModel.Positions.Clear();
            Entry_Focus();
        }

        #region UI Handles

        private void PositionListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            ExecuteOnSuccessCommand((S4.Entities.Position)e.Item);
        }

        private async void BtnShowSearchPanel_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }

        private void EdSearchPart_Completed(object sender, EventArgs e)
        {
            ViewModel.SearchCmd.Execute(sender);
        }

        /// <summary>
        /// Focus entry
        /// </summary>
        private async void Entry_Focus()
        {
            await Task.Delay(600);
            edPosition.Focus();
        }

        #endregion
    }
}
﻿using S4mp.Core.ViewModel;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace S4mp.InternalControls.Positions
{
    class SearchPositionControlVM : BaseViewModel
    {
        private string _searchPart;
        public string SearchPart
        {
            get => _searchPart;
            set => SetField(ref _searchPart, value ?? string.Empty);
        }

        private ICommand _searchCmd;
        public ICommand SearchCmd
        {
            get => _searchCmd;
            set => SetField(ref _searchCmd, value);
        }

        private ICommand _backToSearchCmd;
        public ICommand BackToSearchCmd
        {
            get => _backToSearchCmd;
            set => SetField(ref _backToSearchCmd, value);
        }

        private ICommand _searchBarCodeCmd;
        public ICommand SearchBarCodeCmd
        {
            get => _searchBarCodeCmd;
            set => SetField(ref _searchBarCodeCmd, value);
        }

        private ObservableCollection<S4.Entities.Position> _positions;
        public ObservableCollection<S4.Entities.Position> Positions
        {
            get => _positions;
            set => SetField(ref _positions, value);
        }

        private string _headerText;
        public string HeaderText
        {
            get => _headerText;
            set => SetField(ref _headerText, value ?? string.Empty);
        }

        private string _headerTextSub;
        public string HeaderTextSub
        {
            get => _headerTextSub;
            set => SetField(ref _headerTextSub, value ?? string.Empty);
        }
    }
}

﻿using S4mp.Client.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace S4mp.InternalControls.Printer
{
    public partial class PrintBarcodeCarrierLabelsControl : ContentView
    {
        /// <summary>
        /// Bindable OnSelectItemCmdProperty<ICommand>
        /// </summary>
        public static readonly BindableProperty OnSelectItemCmdProperty = BindableProperty.Create(nameof(OnSelectItemCmd), typeof(ICommand), typeof(PrintBarcodeCarrierLabelsControl));
        public ICommand OnSelectItemCmd
        {
            get => (ICommand)GetValue(OnSelectItemCmdProperty);
            set => SetValue(OnSelectItemCmdProperty, value);
        }

        public static readonly BindableProperty HeaderTextProperty = BindableProperty.Create(nameof(HeaderText), typeof(string), typeof(PrintBarcodeCarrierLabelsControl));
        public string HeaderText
        {
            get => (string)GetValue(HeaderTextProperty);
            set => SetValue(HeaderTextProperty, value);
        }

        public static readonly BindableProperty HeaderTextSubProperty = BindableProperty.Create(nameof(HeaderTextSub), typeof(string), typeof(PrintBarcodeCarrierLabelsControl));
        public string HeaderTextSub
        {
            get => (string)GetValue(HeaderTextSubProperty);
            set => SetValue(HeaderTextSubProperty, value);
        }

        public static readonly BindableProperty QuantityProperty = BindableProperty.Create(nameof(Quantity), typeof(int?), typeof(PrintBarcodeCarrierLabelsControl));
        public int? Quantity
        {
            get => (int?)GetValue(QuantityProperty);
            set => SetValue(QuantityProperty, value);
        }

        public int? PrinterReportID { get; set; }
        public int? PrinterLocationID { get; set; }

        public List<S4.Entities.Reports> Reports = new List<S4.Entities.Reports>();
        public List<S4.Entities.PrinterLocation> Printers = new List<S4.Entities.PrinterLocation>();

        public event EventHandler<bool> PrinterChangedEvent;

        private IReportClient _reportClient;

        public PrintBarcodeCarrierLabelsControl()
        {
            InitializeComponent();
            _reportClient = DependencyService.Get<IReportClient>();
        }

        public async Task LoadData()
        {
            // Client
            Reports = await _reportClient.GetReports("carrLabel");

            Quantity = 1;

            setPrinter.Reports = Reports;
            setPrinter.PrinterCacheType = Core.PrinterCacheTypeEnum.PrintCarrierLabels;
            setPrinter.SuccessCommand = new Command<PrinterByReportResultModel>(GetPrinter);
            (setPrinter as SetPrinterByReportControl).Initialization();
        }

        private async void GetPrinter(PrinterByReportResultModel printerResult)
        {
            PrinterReportID = printerResult.PrinterReportID;
            PrinterLocationID = printerResult.PrinterLocationID;

            if (printerResult.PrinterReportID != PrinterReportID || printerResult.PrinterLocationID == null)
            {
                var report = Reports.Find(r => r.ReportID == PrinterReportID);
                if (report == null) return;
;
                var printerLocations = await _reportClient.GetPrinterLocation(report.ReportType);
                setPrinter.PrinterLocations.Clear();
                printerLocations.ForEach(l => setPrinter.PrinterLocations.Add(l));
                return;
            }

            Completed();
        }

        private void Completed()
        {
            PrinterChangedEvent?.Invoke(this,
                PrinterLocationID != null
                    && PrinterReportID != null
                    && Quantity > 0
                    && Quantity < 100
            );
        }

        #region UI handles

        private void EdQuantity_TextChanged(object sender, TextChangedEventArgs e)
        {
            quantityFrame.HasError(!int.TryParse(e.NewTextValue, out int val) || val < 1 || val > 100);
            Completed();
        }

        #endregion
    }
}
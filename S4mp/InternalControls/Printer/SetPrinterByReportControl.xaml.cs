﻿using S4.Entities;
using S4mp.Core;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;

namespace S4mp.InternalControls.Printer
{
    public partial class SetPrinterByReportControl : ContentView
    {
        public SetPrinterByReportControl()
        {
            InitializeComponent();

            BindingContext = this;
        }

        /// <summary>
        /// Bindable outside
        /// </summary>
        public static readonly BindableProperty SuccessCommandProperty = BindableProperty.Create(nameof(SuccessCommand), typeof(ICommand), typeof(SetPrinterByReportControl));
        public ICommand SuccessCommand
        {
            get => (ICommand)GetValue(SuccessCommandProperty);
            set => SetValue(SuccessCommandProperty, value);
        }

        public ObservableCollection<PrinterLocation> PrinterLocations = new ObservableCollection<PrinterLocation>();

        /// <summary>
        /// Bindable outside
        /// </summary>
        public static readonly BindableProperty ReportsProperty = BindableProperty.Create(nameof(Reports), typeof(List<Reports>), typeof(SetPrinterByReportControl), new List<Reports>());
        public List<Reports> Reports
        {
            get => (List<Reports>)GetValue(ReportsProperty);
            set => SetValue(ReportsProperty, value);
        }



        public static readonly BindableProperty PrinterReportProperty = BindableProperty.Create(nameof(PrinterReport), typeof(Reports), typeof(SetPrinterByReportControl));
        public Reports PrinterReport
        {
            get => (Reports)GetValue(PrinterReportProperty);
            set => SetValue(PrinterReportProperty, value);
        }

        public static readonly BindableProperty PrinterIDProperty = BindableProperty.Create(nameof(PrinterID), typeof(int?), typeof(SetPrinterByReportControl));
        public int? PrinterID
        {
            get => (int?)GetValue(PrinterIDProperty);
            set => SetValue(PrinterIDProperty, value);
        }


        /// <summary>
        /// Printer type used for caching
        /// </summary>
        public PrinterCacheTypeEnum PrinterCacheType { get; set; } = PrinterCacheTypeEnum.Default;

        public event EventHandler<bool> PrinterChangedEvent;

        private void LoadPrinters()
        {
            activityIndicator.Show();
            try
            {
                edPrinter.ItemsSource = PrinterLocations.Where(_ => _.PrinterTypeID == PrinterReport.PrinterTypeID).Select(i => i.PrinterLocationDesc).ToArray();

                var printerID = S4.Core.Singleton<Settings.LocalSettings>.Instance[$"{Consts.PRINTER_REPORT}_{PrinterCacheType.ToString()}_{PrinterReport}"];
                if (!string.IsNullOrEmpty(printerID))
                {
                    var printerLocation = PrinterLocations.Where(i => i.PrinterLocationID == int.Parse(printerID)).FirstOrDefault();
                    if (printerLocation != null)
                    {
                        edPrinter.SelectedItem = printerLocation.PrinterLocationDesc;
                        SuccessCommand.Execute(new PrinterByReportResultModel
                        {
                            PrinterReportID = PrinterReport.ReportID,
                            PrinterLocationID = printerLocation.PrinterLocationID
                        });
                    }
                }
            }
            finally
            {
                activityIndicator.Hide();
            }
        }

        /// <summary>
        /// Call manual Initialization
        /// </summary>
        public void Initialization()
        {
            Func<object, Reports> GetReportObj = (object value) =>
            {
                if (value == null) return null;
                return Reports.Where(_ => _.Settings.ReportName == value.ToString()).First();
            };

            edPrinterReport.ItemsSource = Reports.Select(r => r.Settings.ReportName).ToArray();

            edPrinterReport.SelectedIndexChanged += async (s, e) =>
            {
                PrinterReport = GetReportObj(edPrinterReport.SelectedItem);
                S4.Core.Singleton<Settings.LocalSettings>.Instance[$"{Consts.PRINTER_REPORT}_{PrinterCacheType.ToString()}"] = PrinterReport.Settings.ReportName;
                S4.Core.Singleton<Settings.LocalSettings>.Instance.Save();

                SuccessCommand.Execute(new PrinterByReportResultModel
                {
                    PrinterReportID = PrinterReport.ReportID
                });

                LoadPrinters();
            };

            // on init
            var storedReportName = S4.Core.Singleton<Settings.LocalSettings>.Instance[$"{Consts.PRINTER_REPORT}_{ PrinterCacheType.ToString()}"];
            var report = GetReportObj(storedReportName);
            if (report != null)
            {
                edPrinterReport.SelectedItem = report.Settings.ReportName;
            }

            PrinterLocations.CollectionChanged += PrinterLocations_CollectionChanged;
        }

        private void PrinterLocations_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            edPrinter.ItemsSource = PrinterLocations.Where(_ => _.PrinterTypeID == PrinterReport.PrinterTypeID).Select(i => i.PrinterLocationDesc).ToArray();
        }

        private void EdPrinter_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (edPrinter.SelectedItem != null)
            {
                PrinterID = PrinterLocations.Where(i => i.PrinterLocationDesc == edPrinter.SelectedItem.ToString()).First().PrinterLocationID;
                S4.Core.Singleton<Settings.LocalSettings>.Instance[$"{Consts.PRINTER_REPORT}_{PrinterCacheType.ToString()}_{PrinterReport}"] = PrinterID.ToString();
                S4.Core.Singleton<Settings.LocalSettings>.Instance.Save();

                SuccessCommand.Execute(new PrinterByReportResultModel
                {
                    PrinterReportID = PrinterReport.ReportID,
                    PrinterLocationID = PrinterID
                });
            }
            InvokeChanged();
        }

        private void InvokeChanged()
        {
            PrinterChangedEvent?.Invoke(this, PrinterID.HasValue);
        }
    }

    /// <summary>
    /// Result OnSuccessCommand
    /// </summary>
    public class PrinterByReportResultModel
    {
        public int? PrinterReportID { get; set; }
        public int? PrinterLocationID { get; set; }
    }
}
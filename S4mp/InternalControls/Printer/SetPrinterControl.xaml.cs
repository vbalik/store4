﻿using S4mp.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;

namespace S4mp.InternalControls.Printer
{
    public partial class SetPrinterControl : ContentView
    {
        public SetPrinterControl()
        {
            InitializeComponent();

            BindingContext = this;
        }

        /// <summary>
        /// Bindable outside
        /// </summary>
        public static readonly BindableProperty SuccessCommandProperty = BindableProperty.Create(nameof(SuccessCommand), typeof(ICommand), typeof(SetPrinterControl));
        public ICommand SuccessCommand
        {
            get => (ICommand)GetValue(SuccessCommandProperty);
            set => SetValue(SuccessCommandProperty, value);
        }

        /// <summary>
        /// Bindable outside
        /// </summary>
        public static readonly BindableProperty PrinterLocationsProperty = BindableProperty.Create(nameof(PrinterLocations), typeof(List<S4.Entities.PrinterLocation>), typeof(SetPrinterControl), new List<S4.Entities.PrinterLocation>());
        public List<S4.Entities.PrinterLocation> PrinterLocations
        {
            get => (List<S4.Entities.PrinterLocation>)GetValue(PrinterLocationsProperty);
            set => SetValue(PrinterLocationsProperty, value);
        }

        public static readonly BindableProperty PrinterClassProperty = BindableProperty.Create(nameof(PrinterClass), typeof(S4.Entities.PrinterType.PrinterClassEnum?), typeof(SetPrinterControl));
        public S4.Entities.PrinterType.PrinterClassEnum? PrinterClass
        {
            get => (S4.Entities.PrinterType.PrinterClassEnum?)GetValue(PrinterClassProperty);
            set => SetValue(PrinterClassProperty, value);
        }

        public static readonly BindableProperty PrinterIDProperty = BindableProperty.Create(nameof(PrinterID), typeof(int?), typeof(SetPrinterControl));
        public int? PrinterID
        {
            get => (int?)GetValue(PrinterIDProperty);
            set => SetValue(PrinterIDProperty, value);
        }

        /// <summary>
        /// Printer type used for caching
        /// </summary>
        public PrinterCacheTypeEnum PrinterCacheType { get; set; } = PrinterCacheTypeEnum.Default;

        public event EventHandler<bool> DataChanged;

        private void LoadPrinters()
        {
            activityIndicator.Show();
            try
            {
                PrinterLocations = PrinterLocations ?? new List<S4.Entities.PrinterLocation>();

                edPrinter.ItemsSource = PrinterLocations.Select(i => i.PrinterLocationDesc).ToArray();

                var printerID = S4.Core.Singleton<Settings.LocalSettings>.Instance[$"{Consts.PRINTER_CLASS}_{PrinterCacheType.ToString()}_{PrinterClass}"];
                if (!string.IsNullOrEmpty(printerID))
                {
                    var printerLocation = PrinterLocations.Where(i => i.PrinterLocationID == int.Parse(printerID)).FirstOrDefault();
                    if (printerLocation != null)
                    {
                        edPrinter.SelectedItem = printerLocation.PrinterLocationDesc;
                        SuccessCommand.Execute(new PrinterResultModel
                        {
                            PrinterClass = PrinterClass,
                            PrinterLocationID = printerLocation.PrinterLocationID
                        });
                    }
                }
            }
            finally
            {
                activityIndicator.Hide();
            }
        }

        /// <summary>
        /// Call manual LoadPrinterClasses after initialization
        /// </summary>
        public void LoadPrinterClasses()
        {
            if (edPrinterClass.SelectedItem != null)
            {
                LoadPrinters();
                return;
            }

            var printerClasses = new Dictionary<S4.Entities.PrinterType.PrinterClassEnum, string>()
            {
                { S4.Entities.PrinterType.PrinterClassEnum.PrinterBarcodeNarrow, "Úzká tiskárna" },
                { S4.Entities.PrinterType.PrinterClassEnum.PrinterBarcodeWide, "Široká tiskárna" },
                { S4.Entities.PrinterType.PrinterClassEnum.PrinterClassA4, "Tiskárna A4" }
            };

            Func<S4.Entities.PrinterType.PrinterClassEnum?> GetClassBySelected = () =>
            {
                if (edPrinterClass.SelectedItem == null)
                    return null;
                return printerClasses.Where(i => i.Value == edPrinterClass.SelectedItem.ToString()).First().Key;
            };

            edPrinterClass.ItemsSource = printerClasses.Values.ToArray();
            edPrinterClass.SelectedIndexChanged += async (s, e) =>
            {
                PrinterClass = GetClassBySelected();
                S4.Core.Singleton<Settings.LocalSettings>.Instance[$"{Consts.PRINTER_CLASS}_{PrinterCacheType.ToString()}"] = PrinterClass.ToString();
                S4.Core.Singleton<Settings.LocalSettings>.Instance.Save();

                SuccessCommand.Execute(new PrinterResultModel { PrinterClass = PrinterClass });

                LoadPrinters();
            };

            var storedClass = S4.Core.Singleton<Settings.LocalSettings>.Instance[$"{Consts.PRINTER_CLASS}_{ PrinterCacheType.ToString()}"];
            if (!string.IsNullOrEmpty(storedClass) &&
                Enum.TryParse<S4.Entities.PrinterType.PrinterClassEnum>(storedClass, out S4.Entities.PrinterType.PrinterClassEnum printerClass) &&
                printerClasses.ContainsKey(printerClass))
            {
                edPrinterClass.SelectedItem = printerClasses[printerClass];
            }
        }

        private void EdPrinter_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (edPrinter.SelectedItem != null)
            {
                PrinterID = PrinterLocations.Where(i => i.PrinterLocationDesc == edPrinter.SelectedItem.ToString()).First().PrinterLocationID;
                S4.Core.Singleton<Settings.LocalSettings>.Instance[$"{Consts.PRINTER_CLASS}_{PrinterCacheType.ToString()}_{PrinterClass}"] = PrinterID.ToString();
                S4.Core.Singleton<Settings.LocalSettings>.Instance.Save();

                SuccessCommand.Execute(new PrinterResultModel
                {
                    PrinterClass = PrinterClass,
                    PrinterLocationID = PrinterID
                });
            }
            InvokeChanged();
        }

        private void InvokeChanged()
        {
            DataChanged?.Invoke(this, PrinterID.HasValue);
        }
    }

    /// <summary>
    /// Result OnSuccessCommand
    /// </summary>
    public class PrinterResultModel
    {
        public S4.Entities.PrinterType.PrinterClassEnum? PrinterClass { get; set; }
        public int? PrinterLocationID { get; set; }
    }
}
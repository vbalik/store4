﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace S4mp.InternalControls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ArticleItemView : ContentView
    {
        public ArticleItemView()
        {
            InitializeComponent();
        }


        public static readonly BindableProperty ShowArticleInfoProperty = BindableProperty.Create(nameof(ShowArticleInfo), typeof(bool), typeof(ArticleItemView), true);
        public bool ShowArticleInfo
        {
            get => (bool)GetValue(ShowArticleInfoProperty);
            set  => SetValue(ShowArticleInfoProperty, value);
        }

        public static readonly BindableProperty ShowPosCodeProperty = BindableProperty.Create(nameof(ShowPosCode), typeof(bool), typeof(ArticleItemView), false);
        public bool ShowPosCode
        {
            get => (bool)GetValue(ShowPosCodeProperty);
            set => SetValue(ShowPosCodeProperty, value);
        }

        public static readonly BindableProperty ShowCarrierNumProperty = BindableProperty.Create(nameof(ShowCarrierNum), typeof(bool), typeof(ArticleItemView), false);
        public bool ShowCarrierNum
        {
            get => (bool)GetValue(ShowCarrierNumProperty);
            set => SetValue(ShowCarrierNumProperty, value);
        }

        public static readonly BindableProperty ShowDepartmentProperty = BindableProperty.Create(nameof(ShowDepartment), typeof(bool), typeof(ArticleItemView), false);
        public bool ShowDepartment
        {
            get => (bool)GetValue(ShowDepartmentProperty);
            set => SetValue(ShowDepartmentProperty, value);
        }
        
        public static readonly BindableProperty ShowSpecFeaturesAlertInfoListProperty = BindableProperty.Create(nameof(ShowSpecFeaturesAlertInfoList), typeof(bool), typeof(ArticleItemView), false);
        public bool ShowSpecFeaturesAlertInfoList
        {
            get => (bool)GetValue(ShowSpecFeaturesAlertInfoListProperty);
            set => SetValue(ShowSpecFeaturesAlertInfoListProperty, value);
        }

        public static readonly BindableProperty MenuCommandProperty =
            BindableProperty.Create(nameof(MenuCommand), typeof(ICommand), typeof(ArticleItemView), null);

        public ICommand MenuCommand
        {
            get { return (ICommand)GetValue(MenuCommandProperty); }
            set { SetValue(MenuCommandProperty, value); }
        }
        
        public static readonly BindableProperty TapCommandProperty =
            BindableProperty.Create(nameof(TapCommand), typeof(ICommand), typeof(ArticleItemView), null);

        public ICommand TapCommand
        {
            get { return (ICommand)GetValue(TapCommandProperty); }
            set { SetValue(TapCommandProperty, value); }
        }


        // helper method for invoking commands safely
        public static void Execute(ICommand command, object value)
        {
            if (command == null) return;
            if (command.CanExecute(value))
            {
                command.Execute(value);
            }
        }

        public Command OnLongPress => new Command((value) => Execute(MenuCommand, value));
        public Command OnTapped => new Command((value) => Execute(TapCommand, value));
    }
}
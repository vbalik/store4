﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace S4mp.Interfaces
{
    interface ITerminalHub
    {
        Task InitHubConnection();
        Task LogOn();
        Task ConnectAsync();
        Task DisconnectAsync();
    }
}

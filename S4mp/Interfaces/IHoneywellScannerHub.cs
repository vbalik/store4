﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace S4mp.Interfaces
{
    interface IHoneywellScannerHub
    {
        Task InitHubConnection();

        Task ConnectAsync();

        Task DisconnectAsync();
    }
}

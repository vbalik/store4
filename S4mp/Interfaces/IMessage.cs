﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S4mp.Interfaces
{
    public interface IMessage
    {
        void LongAlert(string message);
        void ShortAlert(string message);
        void ErrorAlert(string message);
    }
}

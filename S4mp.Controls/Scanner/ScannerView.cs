﻿using S4.ProcedureModels.StoreIn;
using S4mp.Core;
using S4mp.Core.EANCode;
using S4mp.Core.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Input;

using Xamarin.Forms;
using ZXing.Net.Mobile.Forms;

namespace S4mp.Controls.Scanner
{
	public class ScannerView : ContentView
	{
        private ICustomEventLogger _logger;

        public static readonly BindableProperty TextProperty =
            BindableProperty.Create(nameof(Text), typeof(string), typeof(ScannerView));
        public static readonly BindableProperty IDProperty =
            BindableProperty.Create(nameof(ID), typeof(string), typeof(ScannerView));
        public static readonly BindableProperty RawDataProperty =
            BindableProperty.Create(nameof(RawData), typeof(byte[]), typeof(ScannerView));
        public static readonly BindableProperty EANProperty =
            BindableProperty.Create(nameof(EAN), typeof(EANCode), typeof(ScannerView));
        public static readonly BindableProperty ErrorProperty =
            BindableProperty.Create(nameof(Error), typeof(string), typeof(ScannerView));
        public static readonly BindableProperty ValueScannedCmdProperty = 
            BindableProperty.Create(nameof(ValueScannedCmd), typeof(ICommand), typeof(ScannerView));

        private IScannerResolver _scanner;

        public ScannerView()
        {
            _logger = DependencyService.Get<ICustomEventLogger>();

            _scanner = DependencyService.Get<IScannerResolver>();
            //_scanner.Scanned += _scanner_Scanned;
#if !DATALOGIC && !HONEYWELL
            var btnScan = new Button
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                Text = "SCAN",
                Command = new Command(async () =>
                {
                    try
                    {
                        _scanner.Scan();
                    }
                    catch(Exception ex)
                    {
                        await _logger.LogError($"{nameof(ScannerView)}: Scanning failed. {ex.Message}", ex.StackTrace);
                    }
                })
            };
            Content = new StackLayout()
            {
                Children = {
                    btnScan
                }
            };
#endif
        }

        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);

            if (propertyName.Equals("Renderer", StringComparison.OrdinalIgnoreCase))
            {
                var rr = DependencyService.Get<IRendererResolver>();
                _scanner.Init(rr.HasRenderer(this), _scanner_Scanned);
            }
        }

        private void _scanner_Scanned(object sender, BarCodeTypeEnum e)
        {
            _logger.LogInfo($"{nameof(ScannerView)}: Scanned barcode: {_scanner.Text}.", _scanner.RawData);

            Clear();
            ID = _scanner.ID;
            Text = _scanner.Text;
            RawData = _scanner.RawData;
            EAN = _scanner.EAN;
            OnValueScanned();    
        }

        private void Clear()
        {
            ID = null;
            Text = null;
            RawData = null;
            EAN = null;
        }

        public string Text
        {
            get => (string)GetValue(TextProperty);
            set => SetValue(TextProperty, value);
        }

        public string ID
        {
            get => (string)GetValue(IDProperty);
            set => SetValue(IDProperty, value);
        }

        public byte[] RawData
        {
            get => (byte[])GetValue(RawDataProperty);
            set => SetValue(RawDataProperty, value);
        }

        public EANCode EAN
        {
            get => (EANCode)GetValue(EANProperty);
            set => SetValue(EANProperty, value);
        }

        public string Error
        {
            get => (string)GetValue(ErrorProperty);
            set => SetValue(ErrorProperty, value);
        }

        public ICommand ValueScannedCmd
        {
            get => (ICommand)GetValue(ValueScannedCmdProperty);
            set => SetValue(ValueScannedCmdProperty, value);
        }

        public void OnValueScanned()
        {
            DependencyService.Get<IKeyboardHelper>().HideKeyboard();

            if (ValueScannedCmd?.CanExecute(null) ?? false)
                ValueScannedCmd.Execute(this);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

using Xamarin.Forms;

namespace S4mp.Controls.Common
{
    public class ImgButton : Button
    {
        public const double _PADDING = 4;

        public static readonly BindableProperty EnabledColorProperty =
            BindableProperty.Create("EnabledColor", typeof(Color), typeof(ImageButton), Common.CommonHelper.EnabledButtonColor);

        public static readonly BindableProperty DisabledColorProperty =
            BindableProperty.Create("DisabledColor", typeof(Color), typeof(ImageButton), Common.CommonHelper.DisabledButtonColor);

        public static readonly BindableProperty EnabledTextColorProperty =
            BindableProperty.Create("EnabledTextColor", typeof(Color), typeof(ImageButton), Common.CommonHelper.EnabledButtonTextColor);

        public static readonly BindableProperty DisabledTextColorProperty =
            BindableProperty.Create("DisabledTextColor", typeof(Color), typeof(ImageButton), Common.CommonHelper.DisabledButtonTextColor);

        public static readonly BindableProperty EnabledImageProperty =
            BindableProperty.Create("EnabledImage", typeof(ImageSource), typeof(ImgButton), default(ImageSource));

        public static readonly BindableProperty DisabledImageProperty =
            BindableProperty.Create("DisabledImage", typeof(ImageSource), typeof(ImgButton), default(ImageSource));

        public static readonly BindableProperty EnabledImageResourceProperty =
            BindableProperty.Create("EnabledImageResource", typeof(string), typeof(ImgButton), default(string));

        public static readonly BindableProperty DisabledImageResourceProperty =
            BindableProperty.Create("DisabledImageResource", typeof(string), typeof(ImgButton), default(string));

        public static readonly BindableProperty CaptionProperty =
            BindableProperty.Create("Caption", typeof(string), typeof(ImgButton), default(string));

        public static readonly BindableProperty TextAlignmentProperty =
            BindableProperty.Create("TextAlignment", typeof(TextAlignment), typeof(ImgButton), default(TextAlignment));

        public static readonly BindableProperty ImageAlignmentProperty =
            BindableProperty.Create("ImageAlignment", typeof(TextAlignment), typeof(ImgButton), default(TextAlignment));

        public static readonly BindableProperty ImgSizeProperty =
            BindableProperty.Create("ImgSize", typeof(double), typeof(ImgButton), default(double));

        public static readonly BindableProperty ShowImageProperty =
            BindableProperty.Create("ShowImage", typeof(bool), typeof(ImgButton), default(bool));

        public ImgButton() : this(string.Empty, TextAlignment.Center)
        {

        }

        public ImgButton(string caption, TextAlignment textAlignment) : this(false, 0, TextAlignment.Start, caption, textAlignment)
        {

        }

        public ImgButton(bool showImage, double imgSize, TextAlignment imageAlignment, string caption, TextAlignment textAlignment)
        {
		    ShowImage = showImage;
            ImgSize = imgSize;
            ImageAlignment = imageAlignment;
			Caption = caption;
            TextAlignment = textAlignment;
			FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Button));

			SetBackground();
        }

		public event EventHandler RedrawRequest;

        public string Caption
        {
            get { return (string)GetValue(CaptionProperty); }
            set { SetValue(CaptionProperty, value); }
        }

        protected override void OnPropertyChanged(string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);

			if (propertyName == ImageButton.IsEnabledProperty.PropertyName)
			{
				SetBackground();
			}
			else if (propertyName == EnabledColorProperty.PropertyName)
			{
				if (IsEnabled)
					SetBackground();
			}
			else if (propertyName == DisabledColorProperty.PropertyName)
			{
				if (!IsEnabled)
					SetBackground();
			}
			else if (propertyName == EnabledTextColorProperty.PropertyName)
			{
				OnRedrawRequest();
			}
			else if (propertyName == DisabledTextColorProperty.PropertyName)
			{
				OnRedrawRequest();
			}
			else if (propertyName == EnabledImageProperty.PropertyName)
			{
				if (IsEnabled && ShowImage)
					OnRedrawRequest();
			}
			else if (propertyName == DisabledImageProperty.PropertyName)
			{
				if (!IsEnabled && ShowImage)
					OnRedrawRequest();
			}
			else if (propertyName == EnabledImageResourceProperty.PropertyName)
			{
				if (IsEnabled && ShowImage)
					EnabledImage = ImageSource.FromResource(EnabledImageResource);
			}
			else if (propertyName == DisabledImageResourceProperty.PropertyName)
			{
				if (!IsEnabled && ShowImage)
					DisabledImage = ImageSource.FromResource(DisabledImageResource);
			}
			else if (propertyName == CaptionProperty.PropertyName)
			{
				OnRedrawRequest();
			}
			else if (propertyName == TextAlignmentProperty.PropertyName)
			{
				OnRedrawRequest();
			}
			else if (propertyName == ImageAlignmentProperty.PropertyName)
			{
				OnRedrawRequest();
			}
			else if (propertyName == ImgSizeProperty.PropertyName)
			{
				OnRedrawRequest();
			}
			else if (propertyName == ShowImageProperty.PropertyName)
			{
				OnRedrawRequest();
			}
			else if (propertyName == BackgroundColorProperty.PropertyName)
			{
				OnRedrawRequest();
			}
			else if (propertyName == "Renderer")
			{
				OnRedrawRequest();
			}
        }

        public TextAlignment TextAlignment
        {
            get { return (TextAlignment)GetValue(TextAlignmentProperty); }
            set { SetValue(TextAlignmentProperty, value); }
        }

        public TextAlignment ImageAlignment
        {
            get { return (TextAlignment)GetValue(ImageAlignmentProperty); }
            set { SetValue(ImageAlignmentProperty, value); }
        }

        public Color EnabledColor
        {
            get { return (Color)GetValue(EnabledColorProperty); }
            set { SetValue(EnabledColorProperty, value); }
        }

        public Color DisabledColor
        {
            get { return (Color)GetValue(DisabledColorProperty); }
            set { SetValue(DisabledColorProperty, value); }
        }

        public Color EnabledTextColor
        {
            get { return (Color)GetValue(EnabledTextColorProperty); }
            set { SetValue(EnabledTextColorProperty, value); }
        }

        public Color DisabledTextColor
        {
            get { return (Color)GetValue(DisabledTextColorProperty); }
            set { SetValue(DisabledTextColorProperty, value); }
        }

        public ImageSource EnabledImage
        {
            get { return (ImageSource)GetValue(EnabledImageProperty); }
            set { SetValue(EnabledImageProperty, value); }
        }

        public ImageSource DisabledImage
        {
            get { return (ImageSource)GetValue(DisabledImageProperty); }
            set { SetValue(DisabledImageProperty, value); }
        }

        public string EnabledImageResource
        {
            get { return (string)GetValue(EnabledImageResourceProperty); }
            set { SetValue(EnabledImageResourceProperty, value); }
        }

        public string DisabledImageResource
        {
            get { return (string)GetValue(DisabledImageResourceProperty); }
            set { SetValue(DisabledImageResourceProperty, value); }
        }

        public double ImgSize
        {
            get { return (double)GetValue(ImgSizeProperty); }
            set { SetValue(ImgSizeProperty, value); }
        }

        public bool ShowImage
        {
            get { return (bool)GetValue(ShowImageProperty); }
            set { SetValue(ShowImageProperty, value); }
        }

		protected virtual void OnRedrawRequest()
		{
			if (RedrawRequest != null)
				RedrawRequest.Invoke(this, new EventArgs());
		}

		#region private

		private void SetBackground()
		{
			this.BackgroundColor = this.IsEnabled ? EnabledColor : DisabledColor;
		}

		#endregion
    }
}

﻿using System.Windows.Input;
using Xamarin.Forms;

namespace S4mp.Controls.Common
{
    public class ButtonBackToSearch : StackLayout
    {
        /// <summary>
        /// Callback command after button click
        /// </summary>
        public static readonly BindableProperty CallbackCommandProperty = BindableProperty.Create(nameof(CallbackCommand), typeof(ICommand), typeof(ButtonBackToSearch));
        public ICommand CallbackCommand
        {
            get => (ICommand)GetValue(CallbackCommandProperty);
            set => SetValue(CallbackCommandProperty, value);
        }


        private Button _btnElement;


        public ButtonBackToSearch()
        {
            IsVisible = false;

            _btnElement = new Button
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Image = "search.png",
                Command = new Command(BtnClick)
            };

            Children.Add(_btnElement);
        }

        protected void BtnClick()
        {
            IsVisible = false;
            CallbackCommand.Execute(null);
        }
    }
}

﻿using S4mp.Core;
using System.Windows.Input;
using Xamarin.Forms;

namespace S4mp.Controls.Common
{
    // <StackLayout BackgroundColor = "{x:Static core:Colors.ColorPrimary}" >
    //    <Label Text="{Binding Header}" FontSize="{x:Static core:Consts.TEXT_SIZE}" FontAttributes="Bold" TextColor="{x:Static core:Colors.TooltipForeground}"
    //      ShowRefreshBtn="true"
    //      ShowSortBtn="true"
    //      SortCommand="{Binding OnSort, Source={x:Reference this}}" />
    // </StackLayout>

    public class Header : StackLayout
    {
        public static readonly BindableProperty TextProperty = BindableProperty.Create(nameof(Text), typeof(string), typeof(Header));
        public static readonly BindableProperty TextSubProperty = BindableProperty.Create(nameof(TextSub), typeof(string), typeof(Header));
        public static readonly BindableProperty ShowRefreshBtnProperty = BindableProperty.Create(nameof(ShowRefreshBtn), typeof(bool), typeof(Header), false);
        public static readonly BindableProperty ShowSortBtnProperty = BindableProperty.Create(nameof(ShowSortBtn), typeof(bool), typeof(Header), false);
        public static readonly BindableProperty SortCommandProperty = BindableProperty.Create(nameof(SortCommand), typeof(ICommand), typeof(Header), new Command(() => { }));

        public string Text
        {
            get => (string)GetValue(TextProperty);
            set
            {
                SetValue(TextProperty, value);
                OnPropertyChanged(nameof(Text));
            }
        }

        public string TextSub
        {
            get => (string)GetValue(TextSubProperty);
            set
            {
                SetValue(TextSubProperty, value);
                OnPropertyChanged(nameof(TextSub));
            }
        }

        /// <summary>
        /// Show refresh button if parent is using IRefreshable
        /// </summary>
        public bool ShowRefreshBtn
        {
            get => (bool)GetValue(ShowRefreshBtnProperty);
            set
            {
                SetValue(ShowRefreshBtnProperty, value);
                OnPropertyChanged(nameof(ShowRefreshBtn));
            }
        }

        public bool ShowSortBtn
        {
            get => (bool)GetValue(ShowSortBtnProperty);
            set
            {
                SetValue(ShowSortBtnProperty, value);
                OnPropertyChanged(nameof(ShowSortBtn));
            }
        }

        /// <summary>
        /// Bindable SortCommand<ICommand>
        /// </summary>
        public ICommand SortCommand
        {
            get => (ICommand)GetValue(SortCommandProperty);
            set
            {
                SetValue(SortCommandProperty, value);
                OnPropertyChanged(nameof(SortCommand));
            }
        }

        public Label _label;
        public Label _labelSub;
        public Button _refreshBtn;
        public Button _sortBtn;
        public StackLayout _btnsBox;

        public Header()
        {
            BackgroundColor = Colors.ColorPrimary;

            _label = new Label
            {
                AutomationId = "HeaderLabel",
                Text = Text,
                FontSize = Consts.TEXT_SIZE,
                FontAttributes = FontAttributes.Bold,
                TextColor = Colors.TooltipForeground
            };

            _labelSub = new Label
            {
                Text = TextSub,
                FontSize = 12,
                FontAttributes = FontAttributes.Bold,
                TextColor = Colors.TooltipForeground,
                IsVisible = (TextSub != null)
            };

            _refreshBtn = new Button
            {
                IsVisible = false,
                Text = "",
                WidthRequest = 32,
                Command = new Command(RefreshButton),
                ImageSource = "refresh16W.png",
                BackgroundColor = Color.Transparent
            };

            _sortBtn = new Button
            {
                IsVisible = false,
                Text = "",
                WidthRequest = 32,
                Command = SortCommand,
                ImageSource = "sortW.png",
                BackgroundColor = Color.Transparent
            };

            _btnsBox = new StackLayout
            {
                HorizontalOptions = LayoutOptions.EndAndExpand,
                WidthRequest = 70,
                BackgroundColor = Color.Transparent,
                Orientation = StackOrientation.Horizontal,
                Children =
                {
                    _refreshBtn,
                    _sortBtn
                }
            };

            var stackLayout = new StackLayout
            {
                Padding = new Thickness(4, 4, 4, 4),
                Orientation = StackOrientation.Horizontal,
                Children = {
                    new StackLayout
                    {
                        HorizontalOptions = LayoutOptions.StartAndExpand,
                        BackgroundColor = Color.Transparent,
                        Children =
                        {
                            _label,
                            _labelSub
                        }
                    },
                    _btnsBox
                }
            };

            Children.Add(stackLayout);
        }

        protected override void OnPropertyChanged(string propertyName = null)
        {
            if (propertyName == nameof(Text))
                _label.Text = Text;
            else if (propertyName == nameof(TextSub))
            {
                _labelSub.Text = TextSub;
                _labelSub.IsVisible = (TextSub != null);
            }
            else if (propertyName == nameof(ShowRefreshBtn))
            {
                _refreshBtn.IsVisible = ShowRefreshBtn;
            }
            else if (propertyName == nameof(ShowSortBtn))
            {
                _sortBtn.IsVisible = ShowSortBtn;
            }
            else if (propertyName == nameof(SortCommand))
            {
                _sortBtn.Command = SortCommand;
            }

            base.OnPropertyChanged(propertyName);
        }

        /// <summary>
        /// Refresh button action
        /// </summary>
        private void RefreshButton()
        {
            var parent = Parent.Parent;
            if (parent is IRefreshable)
            {
                (parent as IRefreshable).RefreshData();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Globalization;

using Xamarin.Forms;

namespace S4mp.Controls.Common
{
	public class NumericUpDown : StackLayout
	{
		private Entry entry;
		private Stepper stepper;

		public static readonly BindableProperty ValueProperty =
			BindableProperty.Create(nameof(Value), typeof(double), typeof(NumericUpDown), default(double), BindingMode.TwoWay, propertyChanged: OnValueChanged, coerceValue: CoerceValue);
		
		public NumericUpDown() : this(0, 100, 0, 1)
		{

		}

		public NumericUpDown(double min, double max, double val, double step, double entryWidthRequest = -1)
		{
			this.Orientation = StackOrientation.Horizontal;

			entry = new Entry() { HorizontalOptions = LayoutOptions.FillAndExpand, VerticalOptions = LayoutOptions.Center, FontSize = Core.Consts.TEXT_SIZE, Keyboard = Keyboard.Numeric };
			if (entryWidthRequest > -1) entry.WidthRequest = entryWidthRequest;
			entry.TextChanged += entry_TextChanged;
			stepper = new Stepper(min, max, val, step);
			stepper.ValueChanged += stepper_ValueChanged;

			entry.Text = stepper.Value.ToString();
			SetValue(ValueProperty, stepper.Value);

			Children.Add(entry);
			Children.Add(stepper);
		}

		public event EventHandler ValueChanged;

		public double Value
		{
			get
			{
				return S4.Core.Utils.MathUtils.CustomRound((double)GetValue(ValueProperty), stepper.Increment);                
			}
			set
			{
				var oldValue = stepper.Value;

				SetValue(ValueProperty, value);

				OnValueChanged(this, oldValue, value);
			}
		}

		public double EntryFontSize
		{
			get { return entry.FontSize; }
			set { entry.FontSize = value; }
		}

		public Color EntryTextColor
		{
			get { return entry.TextColor; }
			set { entry.TextColor = value; }
		}

		public Color EntryBackground
		{
			get { return entry.BackgroundColor; }
			set { entry.BackgroundColor = value; }
		}

		public Double MaxValue
		{
			get { return stepper.Maximum; }
			set
			{
				stepper.Maximum = value;
				if (Value > value)
					Value = value;
			}
		}

		public Double MinValue
		{
			get { return stepper.Minimum; }
			set
			{
				stepper.Minimum = value;
				if (Value < value)
					Value = value;
			}
		}

		public Double Step
		{
			get { return stepper.Increment; }
			set
			{
				stepper.Increment = value;
			}
		}

		protected override void OnPropertyChanged(string propertyName = null)
		{
			base.OnPropertyChanged(propertyName);

			if (propertyName == NumericUpDown.ValueProperty.PropertyName)
			{
				stepper.Value = Value;
			}

			if (propertyName == NumericUpDown.IsEnabledProperty.PropertyName)
			{
				stepper.IsEnabled = this.IsEnabled;
				entry.IsEnabled = this.IsEnabled;
			}
		}

		private static void OnValueChanged(BindableObject bindable, object oldValue, object newValue)
		{
			if (bindable == null)
				return;

			var numericUpDn = bindable as NumericUpDown;

			if (numericUpDn.ValueChanged != null)
				numericUpDn.ValueChanged.Invoke(numericUpDn, new EventArgs());
		}

		static object CoerceValue(BindableObject bindable, object value)
		{
			var numericUpDn = bindable as NumericUpDown;

			double input = (double)value;

			if (input > numericUpDn.MaxValue)
			{
				input = numericUpDn.MaxValue;
			}
			else if (input < numericUpDn.MinValue)
			{
				input = numericUpDn.MinValue;
			}

			return input;
		}

		private void stepper_ValueChanged(object sender, ValueChangedEventArgs e)
		{
			var oldValue = Value;

			SetEntryTextNoEvent(stepper.Value.ToString());

			SetValue(ValueProperty, stepper.Value);

			OnValueChanged(sender as NumericUpDown, oldValue, stepper.Value);
		}

		private void entry_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (string.IsNullOrWhiteSpace(entry.Text))
				return;

			double value;

			var oldValue = Value;

			var formatedText = entry.Text.Replace(".", NumberFormatInfo.CurrentInfo.CurrencyDecimalSeparator);
			if (formatedText != entry.Text)
			{
				entry.Text = formatedText;
				return;
			}

			if (!double.TryParse(entry.Text, out value))
			{
				entry.Text = "0";
				return;
			}

			value = S4.Core.Utils.MathUtils.CustomRound(value, stepper.Increment);
			SetStepperValueNoEvent(value);

			SetValue(ValueProperty, stepper.Value);

			bool addSeparator = false;
			if (!string.IsNullOrEmpty(entry.Text))
				addSeparator = entry.Text[entry.Text.Length - 1].ToString() == NumberFormatInfo.CurrentInfo.CurrencyDecimalSeparator;

			var entryText = value.ToString();

			if (addSeparator)
				entryText += NumberFormatInfo.CurrentInfo.CurrencyDecimalSeparator;

			SetEntryTextNoEvent(entryText);

			OnValueChanged(sender as NumericUpDown, oldValue, Value);
		}

		private void SetEntryTextNoEvent(string text)
		{
			entry.TextChanged -= entry_TextChanged;
			try
			{
				entry.Text = text;
			}
			finally
			{
				entry.TextChanged += entry_TextChanged;
			}
		}

		private void SetStepperValueNoEvent(double value)
		{
			stepper.ValueChanged -= stepper_ValueChanged;
			try
			{
				stepper.Value = value;
			}
			finally
			{
				stepper.ValueChanged += stepper_ValueChanged;
			}
		}
	}
}

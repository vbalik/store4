﻿using Xamarin.Forms;

namespace S4mp.Controls.Common
{
    public class ActivityIndicator : StackLayout
    {
        private readonly Xamarin.Forms.ActivityIndicator _indicator;
        public ActivityIndicator()
        {
            IsVisible = false;

            _indicator = new Xamarin.Forms.ActivityIndicator
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand
            };

            Children.Add(_indicator);
        }

        public void Show(bool flag = true)
        {
            IsVisible = flag;
            _indicator.IsRunning = flag;
        }

        public void Hide()
        {
            IsVisible = false;
            _indicator.IsRunning = false;
        }
    }
}

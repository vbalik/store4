﻿using Xamarin.Forms;

namespace S4mp.Controls.Common
{
    public class MainStackLayout : StackLayout
    {
        public MainStackLayout()
        {
            Padding = new Thickness(4, 0, 4, 0); 
        }
    }
}

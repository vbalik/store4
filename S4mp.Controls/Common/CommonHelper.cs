﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace S4mp.Controls.Common
{
    public class CommonHelper
    {
        public static Color EnabledButtonColor = Color.LightGray;
        public static Color DisabledButtonColor = Color.Gray;
        public static Color EnabledButtonTextColor = Color.White;
        public static Color DisabledButtonTextColor = Color.Silver;
    }
}

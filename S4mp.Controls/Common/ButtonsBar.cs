﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using Xamarin.Forms;

namespace S4mp.Controls.Common
{
	public class ButtonsBar : ContentView
	{
        private const double _BUTTON_WIDTH = 100;
		public Button btnOK;
		public Button btnCancel;
		public Button btnEdit;
		public Button btnNew;
		public Button btnDelete;
		public Button btnRefresh;
		public Button btnNo;

		public ButtonsBar(params ButtonsEnum[] visibleButtons)
		{
			var content = new StackLayout
			{
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.Center,
				Padding = new Thickness(4)
			};

			foreach (ButtonsEnum item in visibleButtons)
			{

				if (item.Equals(ButtonsEnum.OK))
				{
					btnOK = new Button() { Text = "Potvrdit", WidthRequest = _BUTTON_WIDTH };
					btnOK.Clicked += OnButtonClicked;
                    btnOK.AutomationId = "btnOK";
					content.Children.Add(btnOK);
				}

				if (item.Equals(ButtonsEnum.Cancel))
				{
					btnCancel = new Button() { Text = "Zrušit", WidthRequest = _BUTTON_WIDTH };
					btnCancel.Clicked += OnButtonClicked;
					content.Children.Add(btnCancel);
				}

				if (item.Equals(ButtonsEnum.Edit))
				{
					btnEdit = new Button() { Text = "Upravit", WidthRequest = _BUTTON_WIDTH };
					btnEdit.Clicked += OnButtonClicked;
					content.Children.Add(btnEdit);
				}

				if (item.Equals(ButtonsEnum.New))
				{
					btnNew = new Button() { Text = "Nový", WidthRequest = _BUTTON_WIDTH };
					btnNew.Clicked += OnButtonClicked;
					content.Children.Add(btnNew);
				}

				if (item.Equals(ButtonsEnum.Delete))
				{
					btnDelete = new Button() { Text = "Vymazat", WidthRequest = _BUTTON_WIDTH };
					btnDelete.Clicked += OnButtonClicked;
					content.Children.Add(btnDelete);
				}

				if (item.Equals(ButtonsEnum.Refresh))
				{
					btnRefresh = new Button() { Text = "Obnovit", WidthRequest = _BUTTON_WIDTH };
					btnRefresh.Clicked += OnButtonClicked;
					content.Children.Add(btnRefresh);
				}

                if (item.Equals(ButtonsEnum.No))
                {
                    btnNo = new Button() { Text = "Ne", WidthRequest = _BUTTON_WIDTH };
                    btnNo.Clicked += OnButtonClicked;
                    content.Children.Add(btnNo);
                }

            }

			Content = content;
		}

		public enum ButtonsEnum { OK, Cancel, Edit, New, Delete, Refresh, None, No }

		public void SetButtonEnabled(ButtonsEnum button, bool enabled)
		{
			switch (button)
			{
				case ButtonsEnum.OK:
					if (btnOK != null)
						btnOK.IsEnabled = enabled;
					break;
				case ButtonsEnum.Cancel:
					if (btnCancel != null)
						btnCancel.IsEnabled = enabled;
					break;
				case ButtonsEnum.Edit:
					if (btnEdit != null)
						btnEdit.IsEnabled = enabled;
					break;
				case ButtonsEnum.New:
					if (btnNew != null)
						btnNew.IsEnabled = enabled;
					break;
				case ButtonsEnum.Delete:
					if (btnDelete != null)
						btnDelete.IsEnabled = enabled;
					break;
				case ButtonsEnum.Refresh:
					if (btnRefresh != null)
						btnRefresh.IsEnabled = enabled;
					break;
                case ButtonsEnum.No:
                    if (btnNo != null)
                        btnNo.IsEnabled = enabled;
                    break;
            }
		}

		public void SetButtonsVisible(params ButtonsEnum[] visibleButtons)
		{
			if (btnOK != null)
				btnOK.IsVisible = visibleButtons.Contains(ButtonsEnum.OK);
			if (btnNew != null)
				btnNew.IsVisible = visibleButtons.Contains(ButtonsEnum.New);
			if (btnEdit != null)
				btnEdit.IsVisible = visibleButtons.Contains(ButtonsEnum.Edit);
			if (btnDelete != null)
				btnDelete.IsVisible = visibleButtons.Contains(ButtonsEnum.Delete);
			if (btnCancel != null)
				btnCancel.IsVisible = visibleButtons.Contains(ButtonsEnum.Cancel);
			if (btnRefresh != null)
				btnRefresh.IsVisible = visibleButtons.Contains(ButtonsEnum.Refresh);
            if (btnNo != null)
                btnNo.IsVisible = visibleButtons.Contains(ButtonsEnum.No);
        }

		public delegate void ButtonClickedEventHandler(ButtonsEnum button);
		[DefaultValue(null)]
		public event ButtonClickedEventHandler ButtonClicked;

		protected virtual void OnButtonClicked(object sender, EventArgs e)
		{
			if (ButtonClicked == null)
				return;

			if (sender == btnOK)
			{
				ButtonClicked.Invoke(ButtonsEnum.OK);
			}
			else if (sender == btnCancel)
			{
				ButtonClicked.Invoke(ButtonsEnum.Cancel);
			}
			else if (sender == btnEdit)
			{
				ButtonClicked.Invoke(ButtonsEnum.Edit);
			}
			else if (sender == btnNew)
			{
				ButtonClicked.Invoke(ButtonsEnum.New);
			}
			else if (sender == btnDelete)
			{
				ButtonClicked.Invoke(ButtonsEnum.Delete);
			}
			else if (sender == btnRefresh)
			{
				ButtonClicked.Invoke(ButtonsEnum.Refresh);
			}
            else if (sender == btnNo)
            {
                ButtonClicked.Invoke(ButtonsEnum.No);
            }
        }
	}
}



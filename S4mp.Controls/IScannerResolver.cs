﻿using S4mp.Core;
using S4mp.Core.EANCode;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4mp.Controls
{
    public interface IScannerResolver
    {
        /// <summary>
        /// Starts or stop listening, in dependency of start parameter
        /// </summary>
        /// <param name="start"></param>
        /// <param name="handler">listening handler</param>
        void Init(bool start, EventHandler<Scanner.BarCodeTypeEnum> handler);

        /// <summary>
        /// do scanning manually
        /// </summary>
        void Scan();

        //barcode results
        string ID { get; set; }
        byte[] RawData { get; set; }
        string Text { get; set; }
        EANCode EAN { get; set; }
        string Error { get; set; }
    }
}

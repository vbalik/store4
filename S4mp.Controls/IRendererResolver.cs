﻿using System;
using Xamarin.Forms;

namespace S4mp.Controls
{
    public interface IRendererResolver
    {
        object GetRenderer(VisualElement element);

        bool HasRenderer(VisualElement element);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace S4mp.Controls.Dialogs
{
    public class EditFormDialog : EditFormDialogBase
    {
        private Label lbHeader;
        private RelativeLayout content;
        private RelativeLayout stack;
        private Frame frame;

        internal double _padding_coef = .05;

        public string Caption
        {
            get { return lbHeader.Text; }
            set { lbHeader.Text = value; }
        }

        protected override void Init(bool scrolled)
        {
            base.Init(scrolled);

            this.BackgroundColor = Color.Black.MultiplyAlpha(0.7);

            lbHeader = new Label() { FontSize = Core.Consts.HEADER_TEXT_SIZE, FontAttributes = FontAttributes.Bold, HorizontalOptions = LayoutOptions.Center };

            content = new RelativeLayout() { BackgroundColor = Color.Transparent };
            Content = content;

            stack = new RelativeLayout() { Padding = new Thickness(0) };
            stack.Children.Add(
                lbHeader, 
                Constraint.Constant(0), 
                Constraint.Constant(0), 
                Constraint.RelativeToParent((parent) => { return parent.Width; }));
            stack.Children.Add(
                buttonsBar, 
                Constraint.Constant(0),
                Constraint.RelativeToParent((parent) => { return parent.Height - buttonsBar.Height; }), 
                Constraint.RelativeToParent((parent) => { return parent.Width; }));
            var innerView = scrolled ? (View)new ScrollView() { Content = innerContent } : innerContent;
            stack.Children.Add(
                innerView,
                Constraint.Constant(0),
                Constraint.RelativeToParent((parent) => { return lbHeader.Height; }),
                Constraint.RelativeToParent((parent) => { return parent.Width; }),
                Constraint.RelativeToParent((parent) => { return parent.Height - lbHeader.Height - buttonsBar.Height; }));

            frame = new Frame()
            {
                BackgroundColor = Color.Black.MultiplyAlpha(0.3),
                Padding = new Thickness(1),

                Content = new Frame()
                {
                    //BackgroundColor = Controls.Common.CommonHelper.DialogBgColor,
                    HasShadow = false,
                    BorderColor = Color.Black,
                    Content = stack
                }
            };

            content.Children.Add(frame, () => GetFrameRectangle());
        }

        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);

            stack.ForceLayout();
        }

        private Rectangle GetFrameRectangle()
        {
            var paddingX = this.Bounds.Width * _padding_coef;
            var paddingY = this.Bounds.Height * _padding_coef;

            return new Rectangle(
                    this.Bounds.X + paddingX,
                    this.Bounds.Y + paddingY,
                    this.Width - paddingX * 2,
                    this.Height - paddingY * 2);
        }
    }
}

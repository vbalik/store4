﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using Xamarin.Forms;

using S4mp.Core;

namespace S4mp.Controls.Dialogs
{
    public class EntryPopup
    {
        public string Text { get; set; }
        public string Title { get; set; }
        public List<string> Buttons { get; set; }

        public EntryPopup(string title, string text, params string[] buttons)
        {
            Title = title;
            Text = text;
            Buttons = buttons.ToList();
        }

        public EntryPopup(string title, string text) : this(title, text, Consts.OK_BUTTON, Consts.CANCEL_BUTTON)
        {
        }

        public event EventHandler<EntryPopupClosedArgs> PopupClosed;
        public void OnPopupClosed(EntryPopupClosedArgs e)
        {
            var handler = PopupClosed;
            if (handler != null)
                handler(this, e);
        }

        public void Show(Func<string, bool> validateResult)
        {
            DependencyService.Get<IEntryPopupLoader>().ShowPopup(this, validateResult);
        }

        public void Dismiss()
        {
            DependencyService.Get<IEntryPopupLoader>().Dismiss();
        }
    }

    public class EntryPopupClosedArgs : EventArgs
    {
        public string Text { get; set; }
        public string Button { get; set; }
    }

    public interface IEntryPopupLoader
    {
        void ShowPopup(EntryPopup reference, Func<string, bool> validateResult);
        void Dismiss();
    }
}

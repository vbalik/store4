﻿using System;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace S4mp.Controls.Dialogs
{
    public class Confirmation
    {
        public static Task<bool> Confirm(string msg, Page context)
        {
            return context.DisplayAlert("Potvrzení", msg, "Ano", "Ne");
        }

		public static Task<bool> Confirm(string msg, Element element)
		{
			return Confirm(msg, new S4mp.Core.Misc.FormsHelper().GetParentPage(element));
		}
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

using S4mp.Controls.Common;

namespace S4mp.Controls.Dialogs
{
	public class EditFormDialogBase : ContentPage
	{
		internal ButtonsBar buttonsBar;
		internal RelativeLayout innerContent;
		private ButtonsBar.ButtonsEnum _clickedButton = ButtonsBar.ButtonsEnum.Cancel;

		public EditFormDialogBase()
		{
			Init(true);
		}

		public event EventHandler<ButtonsBar.ButtonsEnum> Closed;

		public ButtonsBar ButtonsBar
		{
			get { return buttonsBar; }
		}

		public RelativeLayout InnerContent
		{
			get { return innerContent; }
		}

		public void AddFullSizeChild(View view)
		{
			innerContent.Children.Add(view, Constraint.Constant(0), Constraint.Constant(0), Constraint.RelativeToParent((parent) => { return parent.Width; }), Constraint.RelativeToParent((parent) => { return parent.Height; }));
		}

		public ButtonsBar.ButtonsEnum ClickedButton
		{
			get
			{
				return _clickedButton;
			}
		}

		protected override void OnSizeAllocated(double width, double height)
		{
			base.OnSizeAllocated(width, height);

			innerContent.ForceLayout();
		}

		protected virtual void SetChanged(bool changed)
		{
			buttonsBar.SetButtonEnabled(ButtonsBar.ButtonsEnum.OK, changed);
		}

		protected virtual void OnClosing(ButtonsBar.ButtonsEnum button, ref bool canClose, ref string msg)
		{

		}

		protected virtual async Task Close()
		{
			if (Application.Current.MainPage.Navigation.ModalStack.Count > 0)
				await Application.Current.MainPage.Navigation.PopModalAsync(false);
		}

		protected virtual void Init(bool scrolled)
		{
			innerContent = new RelativeLayout();

            buttonsBar = CreateButtonsBar();
			buttonsBar.SetButtonEnabled(ButtonsBar.ButtonsEnum.OK, false);
			buttonsBar.ButtonClicked += OnButtonClicked;
		}

        protected virtual ButtonsBar CreateButtonsBar()
        {
            return new ButtonsBar(ButtonsBar.ButtonsEnum.OK, ButtonsBar.ButtonsEnum.Cancel);
        }

		protected void OnButtonClicked(ButtonsBar.ButtonsEnum button)
		{
			Device.BeginInvokeOnMainThread(async () =>
			{
				await OnButtonClickedAsync(button);
			});
		}

		protected virtual async Task OnButtonClickedAsync(ButtonsBar.ButtonsEnum button)
		{
			_clickedButton = button;

			bool canClose = true;
			string msg = null;

			OnClosing(button, ref canClose, ref msg);

			if (canClose)
			{
				if (!string.IsNullOrWhiteSpace(msg))
					await this.DisplayAlert("Zpráva", msg, "Potvrdit");

				await Close();

				if (Closed != null)
				{
					// #STO4-632 vicenasobne volani Close[]
					// unsubscribe all other delegates except first one
					var delegates = Closed.GetInvocationList();
					if (delegates.Length > 1)
					{
						for(var i=1; i<delegates.Length; i++)
							Closed -= delegates[i] as EventHandler<ButtonsBar.ButtonsEnum>;
					}

					Closed.Invoke(this, button);
				}
			}
			else
			{
                if (string.IsNullOrEmpty(msg)) return;
				await this.DisplayAlert("Upozornění", msg, "Potvrdit");
			}
		}
	}
}

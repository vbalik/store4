﻿using S4.Core;
using S4.Core.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace S4.Abra
{
    public class ABRAConnectionHelper
    {
        private static object _lock = new object();

        public static string ConnectionString
        {
            get => Singleton<DAL.SettingsSet>.Instance.GetValue_String(Entities.SettingsDictionary.ABRA_CONNECTION_STRING);
        }

        public static NPoco.Database GetDB()
        {
            lock (_lock)
            {
                var db = new NPoco.Database(ConnectionString, NPoco.DatabaseType.SqlServer2012, System.Data.SqlClient.SqlClientFactory.Instance);
                db.Mappers.Add(new NPocoEnumMapper());
                return db;
            }
        }
    }
}

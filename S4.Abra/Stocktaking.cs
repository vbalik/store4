﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace S4.Abra
{
    public class Stocktaking
    {
        public List<Tuple<string, string>> ListStores()
        {
            var sql = @"SELECT ID, CODE, NAME FROM STORES ORDER BY CODE";

            using (var db = ABRAConnectionHelper.GetDB())
            {
                var data = db.Fetch<string[]>(sql);

                var result = data.Select(i => new Tuple<string, string>(i[0], $"{i[1]} - {i[2]}")).ToList();
                return result;
            }

        }



        internal List<ABRAStatusItem> LoadStatus(string storeID, string sourceID)
        {
            var sql = @"
            SELECT
                SB.ID AS kartaSkladID, 
	            SB.STORECARD_ID AS abraKartaID, 
	            SB.QUANTITY / COALESCE(PR.UNITRATE, 1) AS stav, 
	            SB.PURCHASEPRICE * COALESCE(PR.UNITRATE, 1) AS nakupCena, 
	            SB.PURCHASECURRRATE AS nakupKurz
            FROM
	            STORESUBCARDS SB 
	            LEFT JOIN ABMEXCHANGE_KARTY_PREPOCET PR ON (SB.STORECARD_ID = PR.ID)
                LEFT JOIN STORECARDS STC ON (STC.ID = SB.STORECARD_ID)
            WHERE
                SB.QUANTITY <> 0
                AND SB.STORE_ID = @0";

            if (!string.IsNullOrWhiteSpace(sourceID))
                sql += " AND STC.FOREIGNNAME = @1";
            
            using (var db = ABRAConnectionHelper.GetDB())
            {
                var data = db.Fetch<ABRAStatusItem>(sql, storeID, sourceID);
                return data;
            }
        }


        public List<Entities.StockTakingSnapshot> MakeSnapshotItems(string storeID, int stotakID, string sourceID, out string errorList)
        {
            // get ABRA onstore status
            var status = LoadStatus(storeID, sourceID);

            // create list of items to save
            var artDAL = new DAL.ArticleDAL();
            var toSave = new List<Entities.StockTakingSnapshot>();
            var errors = new List<string>();
            foreach (var item in status)
            {
                var article = artDAL.GetBySourceID(item.AbraKartaID);
                if (article == null)
                {
                    errors.Add($"Karta z inventury nebyla nalezena ve skladu; id: {item.AbraKartaID}");
                    continue;
                }

                var packing = article.Packings.Single(p => p.MovablePack);

                var newItem = new Entities.StockTakingSnapshot()
                {
                    StotakID = stotakID,
                    SnapshotClass = Entities.StockTakingSnapshot.StockTakingSnapshotClassEnum.Outer,
                    ArtPackID = packing.ArtPackID,
                    PositionID = 1, //TODO
                    Quantity = item.Stav,
                    UnitPrice = item.NakupCena * item.NakupKurz
                };
                toSave.Add(newItem);
            }

            errorList = string.Join("\r\n", errors.ToArray());
            return toSave;
        }



        internal class ABRAStatusItem
        {
            public string AbraKartaID { get; set; }
            public decimal Stav { get; set; }
            public decimal NakupCena { get; set; }
            public decimal NakupKurz { get; set; }
        }

    }
}
